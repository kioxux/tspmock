package com.gengyun.senscloud.listener.excel.read.impl;

import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import com.alibaba.fastjson.JSON;
import com.gengyun.senscloud.common.Constants;
import com.gengyun.senscloud.common.LangConstant;
import com.gengyun.senscloud.common.SensConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.listener.excel.read.BaseExcelListener;
import com.gengyun.senscloud.model.ImportLog;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.model.PlanWorkCalendarModel;
import com.gengyun.senscloud.service.PlanWorkService;
import com.gengyun.senscloud.service.SecurityManageService;
import com.gengyun.senscloud.service.TaskItemService;
import com.gengyun.senscloud.service.asset.AssetInfoService;
import com.gengyun.senscloud.service.business.UserService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.impl.PlanWorkCalendarServiceImpl;
import com.gengyun.senscloud.service.system.LogsService;
import com.gengyun.senscloud.service.system.SelectOptionService;
import com.gengyun.senscloud.service.system.WorkFlowTemplateService;
import com.gengyun.senscloud.util.DataChangeUtil;
import com.gengyun.senscloud.util.LangUtil;
import com.gengyun.senscloud.util.RegexUtil;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 维保行事历导入
 */
public class PlanWorkCalendarExcelListener extends BaseExcelListener {

    private static final Logger logger = LoggerFactory.getLogger(AssetBomExcelListener.class);

    private AssetInfoService assetInfoService;
    private UserService userService;
    private SelectOptionService selectOptionService;
    private WorksService worksService;
    private WorkFlowTemplateService workFlowTemplateService;
    private PlanWorkService planWorkService;
    private TaskItemService taskItemService;
    private PlanWorkCalendarServiceImpl planWorkCalendarService;
    private SecurityManageService securityManageService;
    private LogsService logsService;
    MethodParam methodParam;
    // Map<String, Map<String, Object>> taskTemplateCodeMap = new HashMap<>();
    Map<String, Map<String, Object>> workTypeMap = new HashMap<>();
    Map<String, Map<String, Object>> priorityLevelMap = new HashMap<>();
    Map<String, Map<String, Object>> resultTypeMap = new HashMap<>();
    Map<String, Map<String, Object>> workFlowMap = new HashMap<>();
    Map<String, Map<String, Object>> repairRatesMap = new HashMap<>();
    Map<String, Map<String, Object>> workLevelsMap = new HashMap<>();
    Map<String, Map<String, Object>> feeTypeMap = new HashMap<>();
    Map<String, String> taskItemPkMap = new HashMap<>();
    Map<String, String> safeItemPkMap = new HashMap<>();
    Map<String, String> bomItemPkMap = new HashMap<>();
    Map<String, String> toolsItemPkMap = new HashMap<>();
    Map<String, String> feeItemPkMap = new HashMap<>();
    Set<String> calendarIndexSet = new HashSet<>();
    Map<String, JSONArray> taskItemsMap = new HashMap<>();
    Map<String, JSONArray> safeItemsMap = new HashMap<>();
    Map<String, JSONArray> bomItemsMap = new HashMap<>();
    Map<String, JSONArray> toolsItemsMap = new HashMap<>();
    Map<String, JSONArray> feeItemsMap = new HashMap<>();
    String sheetName1 = "行事历";
    String sheetName2 = "任务项";
    String sheetName3 = "职安健";
    String sheetName4 = "所需资源-备件材料";
    String sheetName5 = "所需资源-工具";
    String sheetName6 = "所需资源-费用";

    public PlanWorkCalendarExcelListener(MethodParam methodParam, String type, String schemaName, String account, List<ImportLog> logList) {
        super(methodParam, type, schemaName, account, logList);
        this.methodParam = methodParam;
        assetInfoService = SpringContextHolder.getBean("assetInfoServiceImpl");
        userService = SpringContextHolder.getBean("userServiceImpl");
        selectOptionService = SpringContextHolder.getBean("selectOptionServiceImpl");
        worksService = SpringContextHolder.getBean("worksServiceImpl");
        workFlowTemplateService = SpringContextHolder.getBean("workFlowTemplateServiceImpl");
        planWorkService = SpringContextHolder.getBean("planWorkServiceImpl");
        taskItemService = SpringContextHolder.getBean("taskItemServiceImpl");
        planWorkCalendarService = SpringContextHolder.getBean("planWorkCalendarServiceImpl");
        securityManageService = SpringContextHolder.getBean("securityManageServiceImpl");
        logsService = SpringContextHolder.getBean("logsServiceImpl");
        // List<Map<String, Object>> taskTemplateList = selectOptionService.getSelectOptionList(methodParam, "task_template_list", null);
        List<Map<String, Object>> workTypes = selectOptionService.getSelectOptionList(methodParam, "work_type_all", null);
        List<Map<String, Object>> priorityLevels = worksService.queryCloudDataList(schemaName, methodParam.getCompanyId(), methodParam.getUserLang(), "priority_level");
        List<Map<String, Object>> resultTypes = worksService.queryCloudDataList(schemaName, methodParam.getCompanyId(), methodParam.getUserLang(), "resultTypes");
        List<Map<String, Object>> workFlowInfos = workFlowTemplateService.getWorkFlowInfoList(methodParam);//流程信息
        List<Map<String, Object>> repairRates = selectOptionService.getSelectOptionList(methodParam, "repair_rate", null);//维护频率
        List<Map<String, Object>> workLevels = selectOptionService.getSelectOptionList(methodParam, "work_level", null);//工作优先级
        List<Map<String, Object>> feeTypes = selectOptionService.getSelectOptionList(methodParam, "fee_category", null);
        // if(taskTemplateList != null) {
        //    taskTemplateList.forEach(t -> taskTemplateCodeMap.put(String.valueOf(t.get("value")), t));//查询任务模板列表
        // }

        if (workTypes != null) {
            workTypes.forEach(t -> workTypeMap.put(String.valueOf(t.get("text")), t));//查询导入的工单类型在系统中是否已录入
        }

        if (priorityLevels != null) {
            priorityLevels.forEach(l -> priorityLevelMap.put(String.valueOf(l.get("type_name")), l));//查询导入的紧急度在系统中是否已录入
        }

        if (resultTypes != null) {
            resultTypes.forEach(r -> resultTypeMap.put(String.valueOf(r.get("type_name")), r));
        }

        if (workFlowInfos != null) {
            workFlowInfos.forEach(f -> workFlowMap.put(String.valueOf(f.get("work_type_id")), f));
        }

        if (repairRates != null) {
            repairRates.forEach(t -> repairRatesMap.put(String.valueOf(t.get("text")), t));
        }

        if (workLevels != null) {
            workLevels.forEach(t -> workLevelsMap.put(String.valueOf(t.get("text")), t));
        }

        if (feeTypes != null) {
            feeTypes.forEach(t -> feeTypeMap.put(String.valueOf(t.get("text")), t));
        }

        msgInfo = new HashMap<>();
        String msgError = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFJ);//错误
        msgInfo.put("msgError", msgError); // 错误
        if (HANDLE_TYPE_CHECK.equals(type)) {
            String sheet1 = sheetName1 + "-"; //sheet页
            String sheet2 = sheetName2 + "-"; //sheet页
            String sheet3 = sheetName3 + "-"; //sheet页
            String sheet4 = sheetName4 + "-"; //sheet页
            String sheet5 = sheetName5 + "-"; //sheet页
            String sheet6 = sheetName6 + "-"; //sheet页
            String msgColumnNo = msgSplit + commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_AAJ_U); // 列号
            String msgErrorNull = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFH); // 为空或格式有误
            String msgErrorData = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_BFI); // 请先在系统中添加
            String msgErrorDate = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DATE_T);//日期
            String msgErrorNotExist = commonUtilService.getLanguageInfoByUserLang(methodParam, LangConstant.TITLE_DC);//信息不存在
            //sheet 1
            msgInfo.put("msgErrorAssetNameNull", sheet1 + msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorAssetNameData", sheet1 + msgColumnNo + "（B）" + msgErrorData);
            msgInfo.put("msgErrorWorkTypeNull", sheet1 + msgColumnNo + "（C）" + msgErrorNull);
            msgInfo.put("msgErrorWorkTypeData", sheet1 + msgColumnNo + "（C）" + msgErrorData);
            msgInfo.put("msgErrorPriorityLevelNull", sheet1 + msgColumnNo + "（D）" + msgErrorNull);
            msgInfo.put("msgErrorPriorityLevelData", sheet1 + msgColumnNo + "（D）" + msgErrorData);
            msgInfo.put("msgErrorStartDateNull", sheet1 + msgColumnNo + "（E）" + msgErrorNull);
            msgInfo.put("msgErrorStartDate", sheet1 + msgColumnNo + "（E）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorEndDateNull", sheet1 + msgColumnNo + "（F）" + msgErrorNull);
            msgInfo.put("msgErrorEndDate", sheet1 + msgColumnNo + "（F）" + msgErrorDate + msgError);
            msgInfo.put("msgErrorAutoGenerateNull", sheet1 + msgColumnNo + "（G）" + msgErrorNull);
            msgInfo.put("msgErrorAutoGenerateData", sheet1 + msgColumnNo + "（G）" + msgErrorData);
            msgInfo.put("msgErrorItemCodeData", sheet1 + msgColumnNo + "（H）" + msgErrorNull);
            msgInfo.put("msgErrorReceiveUserData", sheet1 + msgColumnNo + "（I）" + msgErrorData);
            msgInfo.put("msgErrorTitleNull", sheet1 + msgColumnNo + "（J）" + msgErrorNull);
            msgInfo.put("msgErrorRepairRateData", sheet1 + msgColumnNo + "（K）" + msgErrorData);
            msgInfo.put("msgErrorWorkLevelData", sheet1 + msgColumnNo + "（L）" + msgErrorData);
            //sheet 2
            msgInfo.put("msgErrorCalendarIndexNull", sheet2 + msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCalendarIndexData", sheet2 + msgColumnNo + "（A）" + msgErrorNotExist);
            msgInfo.put("msgErrorTaskTemplateCodeData", sheet2 + msgColumnNo + "（F）" + msgErrorNotExist);
            //sheet 3
            msgInfo.put("msgErrorCalendarIndexNull", sheet3 + msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCalendarIndexData", sheet3 + msgColumnNo + "（A）" + msgErrorNotExist);
            //sheet 4
            msgInfo.put("msgErrorCalendarIndexNull", sheet4 + msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCalendarIndexData", sheet4 + msgColumnNo + "（A）" + msgErrorNotExist);
            //sheet 5
            msgInfo.put("msgErrorCalendarIndexNull", sheet5 + msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCalendarIndexData", sheet5 + msgColumnNo + "（A）" + msgErrorNotExist);
            //sheet 6
            msgInfo.put("msgErrorCalendarIndexNull", sheet6 + msgColumnNo + "（A）" + msgErrorNull);
            msgInfo.put("msgErrorCalendarIndexData", sheet6 + msgColumnNo + "（A）" + msgErrorNotExist);
            msgInfo.put("msgErrorFeeTypeNull", sheet6 + msgColumnNo + "（B）" + msgErrorNull);
            msgInfo.put("msgErrorFeeTypeData", sheet6 + msgColumnNo + "（B）" + msgErrorData);
        }
    }

    /**
     * 校验数据
     */
    @Override
    public void checkData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        String sheetName = sheetHolder.getSheetName();
        if (sheetName1.equals(sheetName)) {
            HashSet<String> assetNameSet = new HashSet<>();
            HashSet<String> userNameSet = new HashSet<>();
            for (LinkedHashMap row : dataList) {
                calendarIndexSet.add(RegexUtil.optStrOrBlank(row.get(0)));
                assetNameSet.add(RegexUtil.optStrOrBlank(row.get(1)));
                userNameSet.add(RegexUtil.optStrOrBlank(row.get(8)));
            }
            Map<String, Map<String, Object>> assetNameMap = new HashMap<>();//查询导入的设备名称在系统中是否已录入
            if (assetNameSet.size() > 0) {
                assetNameMap = assetInfoService.queryAssetNameByName(schemaName, new ArrayList<>(assetNameSet));
            }

            Map<String, Map<String, Object>> userNameMap = new HashMap<>();//查询导入的用户名称在系统中是否已录入
            if (userNameSet.size() > 0) {
                userNameMap = userService.queryUsersByName(schemaName, new ArrayList<>(userNameSet));
            }

            assetNameSet.clear();
            userNameSet.clear();
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String assetName = RegexUtil.optStrOrBlank(row.get(1)).trim();//设备名称
                String workType = RegexUtil.optStrOrBlank(row.get(2)).trim();//工单类型
                String priorityLevel = RegexUtil.optStrOrBlank(row.get(3)).trim();//紧急程度
                String startDate = RegexUtil.optStrOrBlank(row.get(4)).trim();//计划开始时间
                String endDate = RegexUtil.optStrOrBlank(row.get(5)).trim();//计划截止时间
                String autoGenerate = RegexUtil.optStrOrBlank(row.get(6)).trim();//自动生成工单
                String generateTimePoint = RegexUtil.optStrOrBlank(row.get(7)).trim();//自动生成工单时间点
                String receiveUser = RegexUtil.optStrOrBlank(row.get(8)).trim();//负责人
                String title = RegexUtil.optStrOrBlank(row.get(9)).trim();//计划名称
                String repairRate = RegexUtil.optStrOrBlank(row.get(10)).trim();//维护频率
                String workLevel = RegexUtil.optStrOrBlank(row.get(11)).trim();//工作优先级
                if (RegexUtil.optIsBlankStr(assetName)) {
                    msg.append(msgInfo.get("msgErrorAssetNameNull"));
                } else if (!assetNameMap.containsKey(assetName)) {
                    msg.append(msgInfo.get("msgErrorAssetNameData"));
                }
                if (RegexUtil.optIsBlankStr(workType)) {
                    msg.append(msgInfo.get("msgErrorWorkTypeNull"));
                } else if (!workTypeMap.containsKey(workType)) {
                    msg.append(msgInfo.get("msgErrorWorkTypeData"));
                }
                if (RegexUtil.optIsBlankStr(priorityLevel)) {
                    msg.append(msgInfo.get("msgErrorPriorityLevelNull"));
                } else if (!priorityLevelMap.containsKey(priorityLevel)) {
                    msg.append(msgInfo.get("msgErrorPriorityLevelData"));
                }
                if (RegexUtil.optIsBlankStr(startDate)) {
                    msg.append(msgInfo.get("msgErrorStartDateNull"));
                } else if (parseDate(startDate) == null) {
                    msg.append(msgInfo.get("msgErrorStartDate") + "(" + startDate + ")");
                }
                if (RegexUtil.optIsBlankStr(endDate)) {
                    msg.append(msgInfo.get("msgErrorEndDateNull"));
                } else if (parseDate(endDate) == null) {
                    msg.append(msgInfo.get("msgErrorEndDate") + "(" + endDate + ")");
                }
                if (RegexUtil.optIsBlankStr(autoGenerate)) {
                    msg.append(msgInfo.get("msgErrorAutoGenerateNull"));
                } else if (!"是".equals(autoGenerate) && !"否".equals(autoGenerate)) {
                    msg.append(msgInfo.get("msgErrorAutoGenerateData"));
                }
                if ("是".equals(autoGenerate)) {
                    if (RegexUtil.optIsPresentStr(generateTimePoint) && !(RegexUtil.isInteger(generateTimePoint) && Integer.valueOf(generateTimePoint).intValue() >= 0 && Integer.valueOf(generateTimePoint).intValue() <= 23)) {
                        msg.append(msgInfo.get("msgErrorGenerateTimePointData"));
                    }
                }
                if (RegexUtil.optIsPresentStr(receiveUser) && !userNameMap.containsKey(receiveUser)) {
                    msg.append(msgInfo.get("msgErrorReceiveUserData"));
                }
                if (RegexUtil.optIsBlankStr(title)) {
                    msg.append(msgInfo.get("msgErrorTitleNull"));
                }
                if (!RegexUtil.optIsBlankStr(repairRate) && !repairRatesMap.containsKey(repairRate)) {
                    msg.append(msgInfo.get("msgErrorRepairRateData"));
                }
                if (!RegexUtil.optIsBlankStr(workLevel) && !workLevelsMap.containsKey(workLevel)) {
                    msg.append(msgInfo.get("msgErrorWorkLevelData"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
            if (null != assetNameMap) {
                assetNameMap.clear();
            }
            if (null != userNameMap) {
                userNameMap.clear();
            }
        } else if (sheetName2.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                String taskTemplateCode = RegexUtil.optStrOrBlank(row.get(5)).trim();//模板编号
                if (RegexUtil.optIsBlankStr(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexNull"));
                } else if (!calendarIndexSet.contains(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexData"));
                }
                // if (RegexUtil.optIsPresentStr(taskTemplateCode) && !taskTemplateCodeMap.containsKey(taskTemplateCode)) {
                //    msg.append(msgInfo.get("msgErrorTaskTemplateCodeData"));
                // }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        } else if (sheetName3.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsBlankStr(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexNull"));
                } else if (!calendarIndexSet.contains(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexData"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        } else if (sheetName4.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsBlankStr(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexNull"));
                } else if (!calendarIndexSet.contains(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexData"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        } else if (sheetName5.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsBlankStr(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexNull"));
                } else if (!calendarIndexSet.contains(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexData"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        } else if (sheetName6.equals(sheetName)) {
            for (LinkedHashMap row : dataList) {
                StringBuffer msg = new StringBuffer();//记录单条数据的所有错误信息
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                String feeType = RegexUtil.optStrOrBlank(row.get(1)).trim();//费用类别
                if (RegexUtil.optIsBlankStr(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexNull"));
                } else if (!calendarIndexSet.contains(calendarIndex)) {
                    msg.append(msgInfo.get("msgErrorCalendarIndexData"));
                }
                if (RegexUtil.optIsBlankStr(feeType)) {
                    msg.append(msgInfo.get("msgErrorFeeTypeNull"));
                } else if (!feeTypeMap.containsKey(feeType)) {
                    msg.append(msgInfo.get("msgErrorFeeTypeData"));
                }
                RegexUtil.optNotBlankStrOpt(msg).ifPresent(m -> addLog(row.get(EXCEL_ROW_NO), msg));
            }
        }
    }

    /**
     * 存储数据
     */
    @Override
    public void saveData(List<LinkedHashMap> dataList, ReadSheetHolder sheetHolder) {
        String sheetName = sheetHolder.getSheetName();
        if ("行事历".equals(sheetName)) {
            HashSet<String> assetNameSet = new HashSet<>();
            HashSet<String> userNameSet = new HashSet<>();
            for (LinkedHashMap row : dataList) {
                assetNameSet.add(RegexUtil.optStrOrBlank(row.get(1)));
                userNameSet.add(RegexUtil.optStrOrBlank(row.get(8)));
            }
            Map<String, Map<String, Object>> assetNameMap = new HashMap<>();//查询导入的设备名称在系统中是否已录入
            if (assetNameSet.size() > 0) {
                assetNameMap = assetInfoService.queryAssetNameByName(schemaName, new ArrayList<>(assetNameSet));
            }

            Map<String, Map<String, Object>> userNameMap = new HashMap<>();//查询导入的用户名称在系统中是否已录入
            if (userNameSet.size() > 0) {
                userNameMap = userService.queryUsersByName(schemaName, new ArrayList<>(userNameSet));
            }

            assetNameSet.clear();
            userNameSet.clear();
            Date now = new Date();
            for (LinkedHashMap row : dataList) {
                row.put(2, workTypeMap.get(RegexUtil.optStrOrBlank(row.get(2))).get("value"));
                row.put(3, priorityLevelMap.get(RegexUtil.optStrOrBlank(row.get(3))).get("code"));
                PlanWorkCalendarModel calendarModel = getPlanWorkCalendarBean(row, now, assetNameMap, userNameMap);
                Map<String, Object> map = planWorkCalendarService.findPlanWorkCalendarInfoByCode(methodParam, calendarModel.getWork_calendar_code());
                planWorkService.insertPlanWorkCalendar(schemaName, calendarModel);
                Map<String, Object> newMap = planWorkCalendarService.getPlanWorkCalendarInfoByCode(methodParam, calendarModel.getWork_calendar_code());
                taskItemPkMap.put(RegexUtil.optStrOrBlank(row.get(0)), calendarModel.getWork_calendar_code());
                safeItemPkMap.put(RegexUtil.optStrOrBlank(row.get(0)), calendarModel.getWork_calendar_code());
                bomItemPkMap.put(RegexUtil.optStrOrBlank(row.get(0)), calendarModel.getWork_calendar_code());
                toolsItemPkMap.put(RegexUtil.optStrOrBlank(row.get(0)), calendarModel.getWork_calendar_code());
                feeItemPkMap.put(RegexUtil.optStrOrBlank(row.get(0)), calendarModel.getWork_calendar_code());
                if (map.isEmpty()) {
                    logsService.newLog(methodParam, SensConstant.BUSINESS_NO_10006, calendarModel.getWork_calendar_code(), "[".concat(LangUtil.doSetLogArray("导入了维保计划", LangConstant.TEXT_AJ, new String[]{LangConstant.BTN_IMPORT_B, LangConstant.BTN_BC, RegexUtil.optStrOrVal(calendarModel.getTitle(), "")})).concat("]"));
                } else {
                    net.sf.json.JSONArray logArr = new net.sf.json.JSONArray();
                    String dbBodyProperty = RegexUtil.optStrOrNull(map.get("body_property"));
                    String newBodyProperty = RegexUtil.optStrOrNull(newMap.get("body_property"));
                    newMap.putAll(net.sf.json.JSONObject.fromObject(newBodyProperty));
                    map.putAll(net.sf.json.JSONObject.fromObject(dbBodyProperty));
                    String dataFieldStr =
                            //1-输入框 ,2-下拉框, 3-集合 4-勾选判断
                            "{'name': 'type_name', 'checkType': '1', 'typeKey':'" + LangConstant.TITLE_CATEGORY_X + "'}," +
                                    "{'name': 'priority_level', 'checkType': '2','typeKey': '" + LangConstant.TITLE_AL_V + "','selectKey':'priority_level'}, " +
                                    "{'name': 'repair_rate', 'checkType': '2','typeKey': '" + LangConstant.TITLE_AAW_U + "','selectKey':'repair_rate'}, " +
                                    "{'name': 'work_level', 'checkType': '2','typeKey': '" + LangConstant.TITLE_AI_F + "','selectKey':'work_level'}, " +
                                    "{'name': 'auto_generate_bill', 'checkType': '4','typeKey': '" + LangConstant.TITLE_ALM + "','selectKey':'auto_generate_bill'}," +
                                    "{'name': 'generate_time_point',  'checkType': '1','typeKey': '" + LangConstant.LOG_AB + "'}, " +
                                    "{'name': 'occur_time', 'checkType': '1','typeKey': '" + LangConstant.TITLE_AK_H + "'}," +
                                    "{'name': 'deadline_time', 'checkType':'1','typeKey': '" + LangConstant.DP_M + "'}," +
                                    "{'name': 'group_name', 'checkType':'1','typeKey': '" + LangConstant.TITLE_BA_Z + "','selectKey':'group_with_type'}, " +
                                    "{'name': 'receive_user_name', 'checkType':'1','typeKey': '" + LangConstant.TITLE_AD_K + "','selectKey':'group_role_user'}";
                    commonUtilService.doSetLogInfo(methodParam, "[" + dataFieldStr + "]", newMap, map, logArr, calendarModel.getWork_calendar_code(), SensConstant.BUSINESS_NO_10006, "计划行事历属性更新");
                }
                count++;//记录导入条数
            }
            assetNameMap.clear();
            userNameMap.clear();
        } else if ("任务项".equals(sheetName)) {
            HashSet<String> taskItemCodeSet = new HashSet<>();
            for (LinkedHashMap row : dataList) {
                taskItemCodeSet.add(RegexUtil.optStrOrBlank(row.get(1)));
            }
            Map<String, Map<String, Object>> taskItemCodeMap = new HashMap<>();
            if (taskItemCodeSet.size() > 0) {
                taskItemCodeMap = taskItemService.findItemsByItemCode(schemaName, new ArrayList<>(taskItemCodeSet));
            }

            taskItemCodeSet.clear();
            Date now = new Date();
            List<Map<String, Object>> taskItems = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsPresentStr(calendarIndex)) {
                    JSONObject taskItemJson = getTaskItemObject(taskItemCodeMap, row, now);
                    if (taskItemsMap.containsKey(calendarIndex)) {
                        taskItemsMap.get(calendarIndex).put(taskItemJson);
                    } else {
                        JSONArray taskItemArray = new JSONArray();
                        taskItemArray.put(taskItemJson);
                        taskItemsMap.put(calendarIndex, taskItemArray);
                    }
                }
            }
            if (!taskItemsMap.isEmpty()) {
                for (String key : taskItemsMap.keySet()) {
                    Map<String, Object> taskItem = new HashMap<>();
                    Map<String, Object> info = RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(schemaName, key));
                    Map<String, Object> bodyPropertyMap = DataChangeUtil.scdObjToMapOrNew(info.get("body_property"));
                    net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(bodyPropertyMap);
                    json.put("task_list", taskItemsMap.get(key).toString());
                    taskItem.put("body_property", json.toString());
                    taskItem.put("work_calendar_code", taskItemPkMap.get(key));
                    taskItems.add(taskItem);
                    //日志处理
                    //1-输入框 ,2-下拉框, 3-集合 4-勾选判断=
                    String dataFieldStr = "{'name': 'task_list', 'checkType': '3', 'typeKey': '" + LangConstant.TITLE_AAQ_Q + "','joinPrm':'group_name,task_item_name'}";
                    handlePlanLogs(key, info, json, dataFieldStr);
                }
            }
            if (!taskItems.isEmpty()) {
                planWorkService.updatePlanWorkCalendarTaskItems(schemaName, taskItems);
            }
        } else if ("职安健".equals(sheetName)) {
            HashSet<String> safeItemCodeSet = new HashSet<>();
            for (LinkedHashMap row : dataList) {
                safeItemCodeSet.add(RegexUtil.optStrOrBlank(row.get(1)));
            }
            Map<String, Map<String, Object>> safeItemCodeMap = new HashMap<>();
            if (safeItemCodeSet.size() > 0) {
                safeItemCodeMap = securityManageService.findSafeItemsByItemCode(schemaName, new ArrayList<>(safeItemCodeSet));
            }

            safeItemCodeSet.clear();
            List<Map<String, Object>> safeItems = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsPresentStr(calendarIndex)) {
                    JSONObject safeItemJson = getSafeItemObject(safeItemCodeMap, row);
                    if (safeItemsMap.containsKey(calendarIndex)) {
                        safeItemsMap.get(calendarIndex).put(safeItemJson);
                    } else {
                        JSONArray safeItemArray = new JSONArray();
                        safeItemArray.put(safeItemJson);
                        safeItemsMap.put(calendarIndex, safeItemArray);
                    }
                }
            }
            if (!safeItemsMap.isEmpty()) {
                for (String key : safeItemsMap.keySet()) {
                    Map<String, Object> safeItem = new HashMap<>();
                    Map<String, Object> info = RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(schemaName, key));
                    Map<String, Object> bodyPropertyMap = DataChangeUtil.scdObjToMapOrNew(info.get("body_property"));
                    net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(bodyPropertyMap);
                    json.put("safe_list", safeItemsMap.get(key).toString());
                    safeItem.put("body_property", json.toString());
                    safeItem.put("work_calendar_code", safeItemPkMap.get(key));
                    safeItems.add(safeItem);
                    //日志处理
                    //1-输入框 ,2-下拉框, 3-集合 4-勾选判断=
                    String dataFieldStr = "{'name': 'safe_list', 'checkType': '3','typeKey': '" + LangConstant.TITLE_FILE_J + "','joinPrm':'security_item_name'}";
                    handlePlanLogs(key, info, json, dataFieldStr);
                }
            }
            if (!safeItems.isEmpty()) {
                planWorkService.updatePlanWorkCalendarTaskItems(schemaName, safeItems);
            }
        } else if ("所需资源-备件材料".equals(sheetName)) {
            Map<String, Map<String, Object>> bomItemCodeMap = new HashMap<>();
            List<Map<String, Object>> bomItems = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsPresentStr(calendarIndex)) {
                    JSONObject bomItemJson = getBomItemObject(bomItemCodeMap, row);
                    if (bomItemsMap.containsKey(calendarIndex)) {
                        bomItemsMap.get(calendarIndex).put(bomItemJson);
                    } else {
                        JSONArray bomItemArray = new JSONArray();
                        bomItemArray.put(bomItemJson);
                        bomItemsMap.put(calendarIndex, bomItemArray);
                    }
                }
            }
            if (!bomItemsMap.isEmpty()) {
                for (String key : bomItemsMap.keySet()) {
                    Map<String, Object> bomItem = new HashMap<>();
                    Map<String, Object> info = RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(schemaName, key));
                    Map<String, Object> bodyPropertyMap = DataChangeUtil.scdObjToMapOrNew(info.get("body_property"));
                    net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(bodyPropertyMap);
                    json.put("bom_list", bomItemsMap.get(key).toString());
                    bomItem.put("body_property", json.toString());
                    bomItem.put("work_calendar_code", safeItemPkMap.get(key));
                    bomItems.add(bomItem);
                    //日志处理
                    //1-输入框 ,2-下拉框, 3-集合 4-勾选判断=
                    String dataFieldStr = "{'name': 'bom_list', 'checkType': '3','typeKey': '" + LangConstant.TITLE_AAAAY + "','joinPrm':'bom_name,material_code'}";
                    handlePlanLogs(key, info, json, dataFieldStr);
                }
            }
            if (!bomItems.isEmpty()) {
                planWorkService.updatePlanWorkCalendarTaskItems(schemaName, bomItems);
            }
        } else if ("所需资源-工具".equals(sheetName)) {
            Map<String, Map<String, Object>> toolsItemCodeMap = new HashMap<>();
            List<Map<String, Object>> toolsItems = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsPresentStr(calendarIndex)) {
                    JSONObject toolsItemJson = getToolsItemObject(toolsItemCodeMap, row);
                    if (toolsItemsMap.containsKey(calendarIndex)) {
                        toolsItemsMap.get(calendarIndex).put(toolsItemJson);
                    } else {
                        JSONArray toolsItemArray = new JSONArray();
                        toolsItemArray.put(toolsItemJson);
                        toolsItemsMap.put(calendarIndex, toolsItemArray);
                    }
                }
            }
            if (!toolsItemsMap.isEmpty()) {
                for (String key : toolsItemsMap.keySet()) {
                    Map<String, Object> toolsItem = new HashMap<>();
                    Map<String, Object> info = RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(schemaName, key));
                    Map<String, Object> bodyPropertyMap = DataChangeUtil.scdObjToMapOrNew(info.get("body_property"));
                    net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(bodyPropertyMap);
                    json.put("tools_list", toolsItemsMap.get(key).toString());
                    toolsItem.put("body_property", json.toString());
                    toolsItem.put("work_calendar_code", toolsItemPkMap.get(key));
                    toolsItems.add(toolsItem);
                    //日志处理
                    //1-输入框 ,2-下拉框, 3-集合 4-勾选判断=
                    String dataFieldStr = "{'name': 'tools_list',  'checkType': '3','typeKey': '" + LangConstant.TITLE_BAAAB_I + "','joinPrm':'type_name,tools_name'}";
                    handlePlanLogs(key, info, json, dataFieldStr);
                }
            }
            if (!toolsItems.isEmpty()) {
                planWorkService.updatePlanWorkCalendarTaskItems(schemaName, toolsItems);
            }
        } else if ("所需资源-费用".equals(sheetName)) {
            Map<String, Map<String, Object>> feeItemCodeMap = new HashMap<>();
            List<Map<String, Object>> feeItems = new ArrayList<>();
            for (LinkedHashMap row : dataList) {
                String calendarIndex = RegexUtil.optStrOrBlank(row.get(0)).trim();//行事历序号
                if (RegexUtil.optIsPresentStr(calendarIndex)) {
                    JSONObject feeItemJson = getFeeItemObject(feeItemCodeMap, row);
                    if (feeItemsMap.containsKey(calendarIndex)) {
                        feeItemsMap.get(calendarIndex).put(feeItemJson);
                    } else {
                        JSONArray feeItemArray = new JSONArray();
                        feeItemArray.put(feeItemJson);
                        feeItemsMap.put(calendarIndex, feeItemArray);
                    }
                }
            }
            if (!feeItemsMap.isEmpty()) {
                for (String key : feeItemsMap.keySet()) {
                    Map<String, Object> feeItem = new HashMap<>();
                    Map<String, Object> info = RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(schemaName, key));
                    Map<String, Object> bodyPropertyMap = DataChangeUtil.scdObjToMapOrNew(info.get("body_property"));
                    net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(bodyPropertyMap);
                    json.put("fee_list", feeItemsMap.get(key).toString());
                    feeItem.put("body_property", json.toString());
                    feeItem.put("work_calendar_code", feeItemPkMap.get(key));
                    feeItems.add(feeItem);
                    //日志处理
                    //1-输入框 ,2-下拉框, 3-集合 4-勾选判断=
                    String dataFieldStr = "{'name': 'fee_list', 'checkType': '3','typeKey': '" + LangConstant.TITLE_AGI + "','joinPrm':'type_name,fee','keyInfo':{'0':'fee_category'}}";
                    handlePlanLogs(key, info, json, dataFieldStr);
                }
            }
            if (!feeItems.isEmpty()) {
                planWorkService.updatePlanWorkCalendarTaskItems(schemaName, feeItems);
            }
        }
    }

    private void handlePlanLogs(String key, Map<String, Object> info, net.sf.json.JSONObject json, String dataFieldStr) {
        net.sf.json.JSONArray logArr = new net.sf.json.JSONArray();
        String dbBodyProperty = RegexUtil.optStrOrNull(info.get("body_property"));
        String newBodyProperty = RegexUtil.optStrOrNull(json.toString());
        commonUtilService.doSetLogInfo(methodParam, "[" + dataFieldStr + "]", new HashMap<String, Object>() {{
            putAll(net.sf.json.JSONObject.fromObject(newBodyProperty));
        }}, new HashMap<String, Object>() {{
            putAll(net.sf.json.JSONObject.fromObject(dbBodyProperty));
        }}, logArr, key, SensConstant.BUSINESS_NO_10006, "计划行事历属性更新");
    }

    /**
     * 字符串转日期格式
     *
     * @param value
     * @return
     */
    private Date parseDate(String value) {
        if (RegexUtil.optIsPresentStr(value)) {
            try {
                if (value.contains("-")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS);
                    return simpleDateFormat.parse(value);
                } else if (value.contains("/")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_SS_2);
                    return simpleDateFormat.parse(value);
                } else if (value.contains("年")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FMT_YEAR_MON_5);
                    return simpleDateFormat.parse(value);
                }
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    private PlanWorkCalendarModel getPlanWorkCalendarBean(LinkedHashMap row, Date now,
                                                          Map<String, Map<String, Object>> assetNameMap,
                                                          Map<String, Map<String, Object>> userNameMap) {
        PlanWorkCalendarModel planWorkCalendarModel = new PlanWorkCalendarModel();

        planWorkCalendarModel.setWork_calendar_code(RegexUtil.optStrOrVal(row.get(0), planWorkCalendarService.findPlanWorkSeq(methodParam)));
        planWorkCalendarModel.setRelation_id(String.valueOf(assetNameMap.get(RegexUtil.optStrOrBlank(row.get(1))).get("id")));
        planWorkCalendarModel.setPosition_code(String.valueOf(assetNameMap.get(RegexUtil.optStrOrBlank(row.get(1))).get("position_code")));
        planWorkCalendarModel.setWork_type_id((Integer) row.get(2));
        planWorkCalendarModel.setPriority_level(Integer.parseInt(String.valueOf(row.get(3))));
        planWorkCalendarModel.setOccur_time(new Timestamp(parseDate(RegexUtil.optStrOrBlank(row.get(4))).getTime()));
        planWorkCalendarModel.setDeadline_time(new Timestamp(parseDate(RegexUtil.optStrOrBlank(row.get(5))).getTime()));
        planWorkCalendarModel.setAuto_generate_bill("是".equals(RegexUtil.optStrOrBlank(row.get(6))) ? 1 : 0);
        if (planWorkCalendarModel.getAuto_generate_bill() == 1 && RegexUtil.optIsPresentStr(row.get(7))) {
            planWorkCalendarModel.setGenerate_time_point(Integer.valueOf(String.valueOf(row.get(7))));
        } else {
            planWorkCalendarModel.setGenerate_time_point(0);
        }
        Map<String, Object> poprty = DataChangeUtil.scdObjToMapOrNew(RegexUtil.optMapOrNew(planWorkService.findPlanWorkPropertyInfo(schemaName, planWorkCalendarModel.getWork_calendar_code())).get("body_property"));
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(row.get(10)).trim())) {
            poprty.put("repair_rate", RegexUtil.optStrOrBlank(repairRatesMap.get(RegexUtil.optStrOrBlank(row.get(10))).get("value")).trim());
        }
        if (RegexUtil.optIsPresentStr(RegexUtil.optStrOrBlank(row.get(11)).trim())) {
            poprty.put("work_level", RegexUtil.optStrOrBlank(workLevelsMap.get(RegexUtil.optStrOrBlank(row.get(11))).get("value")).trim());
        }
        if (!poprty.isEmpty()) {
            planWorkCalendarModel.setPlan_work_property(JSON.toJSONString(poprty));
        }
        planWorkCalendarModel.setReceive_user_id(String.valueOf(userNameMap.get(RegexUtil.optStrOrBlank(row.get(8))).get("id")));
        planWorkCalendarModel.setGroup_id((Integer) userNameMap.get(RegexUtil.optStrOrBlank(row.get(8))).get("group_id"));
        planWorkCalendarModel.setTitle(RegexUtil.optStrOrNull(row.get(9)));//计划名称
        planWorkCalendarModel.setRelation_type(2);//设备
        planWorkCalendarModel.setStatus(1);//未转工单
        planWorkCalendarModel.setPlan_code("");
        planWorkCalendarModel.setCreate_time(new Timestamp(now.getTime()));
        planWorkCalendarModel.setCreate_user_id(methodParam.getUserId());
        Map<String, Object> stringObjectMap = workFlowMap.get(planWorkCalendarModel.getWork_type_id());
        planWorkCalendarModel.setWork_template_code(stringObjectMap == null ? "" : String.valueOf(stringObjectMap.get("work_template_code")));
        return planWorkCalendarModel;
    }

    private JSONObject getTaskItemObject(Map<String, Map<String, Object>> taskItemCodeMap, LinkedHashMap row, Date now) {
        String taskItemCode = RegexUtil.optStrOrBlank(row.get(1)).trim();//任务项编号
        String taskItemName = RegexUtil.optStrOrBlank(row.get(2)).trim();//任务项名称
        String resultTypeName = RegexUtil.optStrOrBlank(row.get(3)).trim();//结果类型
        String groupName = RegexUtil.optStrOrBlank(row.get(4)).trim();//组名称
        String taskTemplateCode = RegexUtil.optStrOrBlank(row.get(5)).trim();//模板编号
        String requirements = RegexUtil.optStrOrBlank(row.get(6)).trim();//任务描述

        JSONObject taskItemJson = new JSONObject();
        if (taskItemCodeMap.containsKey(taskItemCode)) {
            taskItemJson = new JSONObject(taskItemCodeMap.get(taskItemCode));
        }

        taskItemJson.put("task_item_code", taskItemCode);
        taskItemJson.put("task_item_name", taskItemName);
        taskItemJson.put("create_user_id", methodParam.getUserId());
        taskItemJson.put("requirements", requirements);
        taskItemJson.put("create_time", now.getTime());
        taskItemJson.put("task_template_code", taskTemplateCode);
        taskItemJson.put("group_name", groupName);
        taskItemJson.put("result_type", RegexUtil.optNotBlankStrOpt(resultTypeName).map(resultTypeMap::get).map(m -> m.get("code")).orElse(StringUtils.EMPTY));
        return taskItemJson;
    }

    private JSONObject getSafeItemObject(Map<String, Map<String, Object>> safeItemCodeMap, LinkedHashMap row) {
        String securityItemCode = RegexUtil.optStrOrBlank(row.get(1)).trim();//职安健编号
        String securityItemName = RegexUtil.optStrOrBlank(row.get(2)).trim();//风险评估
        String raskNote = RegexUtil.optStrOrBlank(row.get(3)).trim();//预防措施
        String resultTypeName = RegexUtil.optStrOrBlank(row.get(4)).trim();//职安健审批
        String createTime = RegexUtil.optStrOrBlank(row.get(5)).trim();//创建时间

        JSONObject safeItemJson = new JSONObject();
        if (safeItemCodeMap.containsKey(securityItemCode)) {
            safeItemJson = new JSONObject(safeItemCodeMap.get(securityItemCode));
        }

        safeItemJson.put("security_item_code", securityItemCode);
        safeItemJson.put("security_item_name", securityItemName);
        safeItemJson.put("rask_note", raskNote);
        safeItemJson.put("result_type_name", resultTypeName);
        safeItemJson.put("create_time", createTime);
        return safeItemJson;
    }

    private JSONObject getBomItemObject(Map<String, Map<String, Object>> bomItemCodeMap, LinkedHashMap row) {
        String bomName = RegexUtil.optStrOrBlank(row.get(1)).trim();//备件名称
        String typeName = RegexUtil.optStrOrBlank(row.get(2)).trim();//备件类型
        String bomModel = RegexUtil.optStrOrBlank(row.get(3)).trim();//备件型号
        String materialCode = RegexUtil.optStrOrBlank(row.get(4)).trim();//物料编码
        String count = RegexUtil.optStrOrBlank(row.get(5)).trim();//数量

        JSONObject bomItemJson = new JSONObject();
        if (bomItemCodeMap.containsKey(materialCode)) {
            bomItemJson = new JSONObject(bomItemCodeMap.get(materialCode));
        }

        bomItemJson.put("bom_name", bomName);
        bomItemJson.put("type_name", typeName);
        bomItemJson.put("bom_model", bomModel);
        bomItemJson.put("material_code", materialCode);
        bomItemJson.put("count", count);
        return bomItemJson;
    }

    private JSONObject getToolsItemObject(Map<String, Map<String, Object>> toolsItemCodeMap, LinkedHashMap row) {
        String toolsName = RegexUtil.optStrOrBlank(row.get(1)).trim();//工具名称
        String typeName = RegexUtil.optStrOrBlank(row.get(2)).trim();//工具类型
        String count = RegexUtil.optStrOrBlank(row.get(3)).trim();//数量

        JSONObject toolsItemJson = new JSONObject();
        if (toolsItemCodeMap.containsKey(toolsName)) {
            toolsItemJson = new JSONObject(toolsItemCodeMap.get(toolsName));
        }

        toolsItemJson.put("tools_name", toolsName);
        toolsItemJson.put("type_name", typeName);
        toolsItemJson.put("count", count);
        return toolsItemJson;
    }

    private JSONObject getFeeItemObject(Map<String, Map<String, Object>> feeItemCodeMap, LinkedHashMap row) {
        String typeName = RegexUtil.optStrOrBlank(feeTypeMap.get(RegexUtil.optStrOrBlank(row.get(1))).get("value")).trim();//费用类别
        String fee = RegexUtil.optStrOrBlank(row.get(2)).trim();//金额

        JSONObject feeItemJson = new JSONObject();
        if (feeItemCodeMap.containsKey(typeName)) {
            feeItemJson = new JSONObject(feeItemCodeMap.get(typeName));
        }

        feeItemJson.put("type_name", typeName);
        feeItemJson.put("fee", fee);
        return feeItemJson;
    }
}
