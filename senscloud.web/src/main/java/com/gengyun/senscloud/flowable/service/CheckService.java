package com.gengyun.senscloud.flowable.service;

import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.model.FlowData;
import com.gengyun.senscloud.util.JsonUtils;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.BpmnAutoLayout;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.*;
import org.flowable.bpmn.model.Process;
import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.flowable.engine.impl.bpmn.listener.ScriptTaskListener;
import org.flowable.engine.repository.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CheckService implements JavaDelegate {
    Logger logger = LoggerFactory.getLogger(CheckService.class);
    private Expression fieldFlowData;
    private Expression fieldTypeName;

    private void addSquenceFlow(Process process, String sourceElementId, String targetElementId) {
        SequenceFlow sequenceFlow = new SequenceFlow(sourceElementId, targetElementId);
        process.addFlowElement(sequenceFlow);
    }

    @Override
    public void execute(DelegateExecution execution) {
        String strFlowData = (String) fieldFlowData.getValue(execution);
        String strTypeName = (String) fieldTypeName.getValue(execution);
        String tenantId = execution.getTenantId();
        TenantInfoHolder tenantInfoHolder = SpringContextHolder.getBean("tenantInfoHolder");
        RepositoryService repositoryService = SpringContextHolder.getBean(RepositoryService.class);
        RuntimeService runtimeService = SpringContextHolder.getBean(RuntimeService.class);
        new Thread(() -> {
            if (StringUtils.isNotEmpty(tenantId) && tenantInfoHolder != null && repositoryService != null && runtimeService != null
                    && StringUtils.isNotEmpty(strFlowData)) {
                List<FlowData> flowDataList = JsonUtils.jsonToList(strFlowData, FlowData.class);
                if (flowDataList != null && flowDataList.size() > 0) {
                    tenantInfoHolder.setCurrentTenantId(tenantId);
                    String businessKey = repositoryService.createProcessDefinitionQuery().processDefinitionTenantId(tenantId)
                            .processDefinitionId(execution.getProcessDefinitionId()).singleResult().getKey();
                    tenantInfoHolder.clearCurrentTenantId();
                    if (StringUtils.isNotEmpty(businessKey)) {
                        businessKey = businessKey.split("_")[0];
                    }
                    String name = strTypeName + "子任务";
                    Process process = new Process();
                    process.setId(businessKey + "_subflow");
                    process.setName(name);
                    StartEvent startEvent = new StartEvent();
                    startEvent.setName("开始");
                    startEvent.setId("start_event");
                    process.addFlowElement(startEvent);
                    flowDataList.sort(Comparator.comparingInt(FlowData::getOrder));
                    FlowData fData = flowDataList.get(0);
                    FlowElement source = startEvent;
                    boolean parallel = false;
                    if (flowDataList.size() > 1 && fData.getOrder() == -1) {
                        parallel = true;
                    }
                    ParallelGateway inParallelGateway = null;
                    ParallelGateway outParallelGateway = null;
                    if (parallel) {
                        inParallelGateway = new ParallelGateway();
                        inParallelGateway.setId("in_parallel_gateway");
                        process.addFlowElement(inParallelGateway);
                        addSquenceFlow(process, source.getId(), inParallelGateway.getId());
                        source = inParallelGateway;
                        outParallelGateway = new ParallelGateway();
                        outParallelGateway.setId("out_parallel_gateway");
                        process.addFlowElement(outParallelGateway);
                    }
                    for (FlowData flowData : flowDataList) {
                        UserTask userTask = new UserTask();
                        userTask.setName(strTypeName + "任务");
                        userTask.setId("sid_" + UUID.randomUUID().toString());
                        if (StringUtils.isNotEmpty(flowData.getReceive_user_id())) {
                            userTask.setAssignee(flowData.getReceive_user_id());
                        }
                        if (flowData.getCandidateUsers() != null && flowData.getCandidateUsers().size() > 0) {
                            userTask.setCandidateUsers(flowData.getCandidateUsers());
                        }
                        if (flowData.getCandidateGroups() != null && flowData.getCandidateGroups().size() > 0) {
                            userTask.setCandidateGroups(flowData.getCandidateGroups());
                        }
                        FlowableListener listener = new FlowableListener();
                        listener.setEvent("create");
                        listener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_CLASS);
                        listener.setImplementation(ScriptTaskListener.class.getName());
                        List<FieldExtension> extensions = new ArrayList<>();
                        FieldExtension scriptFieldExtension = new FieldExtension();
                        scriptFieldExtension.setFieldName("script");
                        scriptFieldExtension.setStringValue(JSONObject.fromObject(flowData).toString());
                        scriptFieldExtension.setStringValue("task.setVariableLocal(\"sub_work_code\", \"" + flowData.getSub_work_code() +
                                "\");task.setVariableLocal(\"title_page\",\"" + flowData.getName() + "\");task.setVariableLocal(\"facility_id\",\"" +
                                flowData.getFacility_id() + "\");task.setVariableLocal(\"deadline_time\",\"" + flowData.getDeadline_time() +
                                "\"); task.setVariableLocal(\"request_param\",\"" + flowData.getRequest_param() + "\");" + "task.setVariableLocal(\"sub_token\",\"" + flowData.getSub_token() + "\");"
                                + "task.setVariableLocal(\"main_sub_work_code\",\"" + flowData.getMain_sub_work_code() + "\");"
                                + "task.setVariableLocal(\"pref_id\",\"" + flowData.getPref_id() + "\");"
                                + "task.setVariableLocal(\"position_code\",\"" + flowData.getPosition_code() + "\");"
                                + "task.setVariableLocal(\"do_flow_key\",\"" + execution.getVariables().get("do_flow_key") + "\");"
                                + "task.setVariableLocal(\"work_type_id\",\"" + execution.getVariables().get("work_type_id") + "\");"
                                + "task.setVariableLocal(\"business_type_id\",\"" + execution.getVariables().get("business_type_id") + "\");"
                                + "task.setVariableLocal(\"receive_user_id\",\"" + execution.getVariables().get("receive_user_id") + "\");"
                                + "task.setVariableLocal(\"next_user_id\",\"" + flowData.getReceive_user_id() + "\");"
                                + "task.setVariableLocal(\"position_code\",\"\");"
                                + "task.setVariableLocal(\"work_code\",\"" + flowData.getWork_code() + "\");"
                                + "task.setVariableLocal(\"title_page\",\"" + flowData.getTitle_page() + "\");"
                                + "task.setVariableLocal(\"priority_level\",\"" + flowData.getPriority_level() + "\");"
                                + "task.setVariableLocal(\"execution_id\",\"" + execution.getId() + "\");"
                                + "task.setVariableLocal(\"do_flow_key\",\"" + execution.getVariables().get("do_flow_key") + "\");"
                                + "task.setVariableLocal(\"create_time\",\"" + flowData.getCreate_time() + "\");");
                        extensions.add(scriptFieldExtension);
                        FieldExtension languageFieldExtension = new FieldExtension();
                        languageFieldExtension.setFieldName("language");
                        languageFieldExtension.setStringValue("groovy");
                        extensions.add(languageFieldExtension);
                        listener.setFieldExtensions(extensions);
                        List<FlowableListener> listeners = new ArrayList<>();
                        listeners.add(listener);
                        userTask.setTaskListeners(listeners);
                        process.addFlowElement(userTask);
                        addSquenceFlow(process, source.getId(), userTask.getId());
                        if (parallel) {
                            addSquenceFlow(process, userTask.getId(), outParallelGateway.getId());
                        } else {
                            source = userTask;
                        }
                    }
                    EndEvent endEvent = new EndEvent();
                    endEvent.setName("结束");
                    endEvent.setId("end_event");
                    process.addFlowElement(endEvent);
                    if (parallel) {
                        addSquenceFlow(process, outParallelGateway.getId(), endEvent.getId());
                    } else {
                        addSquenceFlow(process, source.getId(), endEvent.getId());
                    }
                    tenantInfoHolder.setCurrentTenantId(tenantId);
                    try {
                        BpmnModel model = new BpmnModel();
                        model.addProcess(process);
                        model.setTargetNamespace(businessKey + "_subflow");
                        new BpmnAutoLayout(model).execute();
                        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
                        byte[] convertToXML = bpmnXMLConverter.convertToXML(model);
                        String bytes = new String(convertToXML, "UTF-8");
                        Deployment deployment = repositoryService.createDeployment().addString(name + ".bpmn20.xml", bytes)
                                .tenantId(tenantId).name(name).key(businessKey).category(businessKey).enableDuplicateFiltering().deploy();
                        Map<String, Object> variables = new HashMap<>();
                        variables.put("calledElement", process.getId());
                        variables.put("deploymentId", deployment.getId());
                        runtimeService.triggerAsync(execution.getId(), variables);
                    } catch (Exception e) {
                        logger.error(e.getLocalizedMessage());
                    }
                    tenantInfoHolder.clearCurrentTenantId();
                }
            }
        }).start();
    }
}
