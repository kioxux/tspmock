package com.gengyun.senscloud.flowable.listener;

import com.alibaba.fastjson.JSONObject;
import com.gengyun.senscloud.common.FlowParmsConstant;
import com.gengyun.senscloud.config.SpringContextHolder;
import com.gengyun.senscloud.model.MethodParam;
import com.gengyun.senscloud.service.cache.CacheUtilService;
import com.gengyun.senscloud.service.dynamic.WorksService;
import com.gengyun.senscloud.service.system.WorkflowService;
import com.gengyun.senscloud.util.RegexUtil;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.common.engine.impl.event.FlowableEntityEventImpl;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 工单处理全局事件监听器
 */
@Component
public class WorkHandleEventListener implements FlowableEventListener {
    Logger logger = LoggerFactory.getLogger(WorkHandleEventListener.class);

    /**
     * Called when an event has been fired
     *
     * @param event the event
     */
    @Override
    public void onEvent(FlowableEvent event) {
        boolean isEnd = false;
        String endNodeId = null;
        if (event.getType().equals(FlowableEngineEventType.TASK_COMPLETED)) {
            //判断是否是最后一个节点
            TaskEntity taskEntity;
            if (event instanceof FlowableEntityEventImpl) {
                taskEntity = (TaskEntity) ((FlowableEntityEventImpl) event).getEntity();
            } else if (event instanceof org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl) {
                taskEntity = (TaskEntity) ((org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl) event).getEntity();
            } else {
                return;
            }
//            String subWorkCode = RegexUtil.optStrAndValOrBlank(taskEntity.getVariableLocal(FlowParmsConstant.SUB_WORK_CODE), taskEntity.getVariable(FlowParmsConstant.SUB_WORK_CODE));
            WorkflowService workflowService = SpringContextHolder.getBean(WorkflowService.class);
            WorksService worksService = SpringContextHolder.getBean(WorksService.class);
            String requestParam = RegexUtil.optStrAndValOrBlank(taskEntity.getVariableLocal(FlowParmsConstant.REQUEST_PARAM), taskEntity.getVariable(FlowParmsConstant.REQUEST_PARAM));
            String sub_token = RegexUtil.optStrAndValOrBlank(taskEntity.getVariableLocal("sub_token"), taskEntity.getVariable("sub_token"));
            if (RegexUtil.optIsPresentStr(requestParam)) {
                MethodParam methodParam = JSONObject.parseObject(requestParam, MethodParam.class);
//                worksService.doCheckFlowInfo(methodParam, subWorkCode);
                worksService.callBackStartWithForm(methodParam, taskEntity, null, isEnd, FlowableEngineEventType.TASK_COMPLETED);
            } else if (RegexUtil.optIsPresentStr(sub_token)) {
                CacheUtilService cacheUtilService = SpringContextHolder.getBean(CacheUtilService.class);
                String method = cacheUtilService.getSubToken(null, sub_token);
                cacheUtilService.deleteSubToken(sub_token);
                MethodParam methodParam = JSONObject.parseObject(method, MethodParam.class);
                worksService.callBackStartWithForm(methodParam, taskEntity, null, isEnd, FlowableEngineEventType.TASK_COMPLETED);
            }
            endNodeId = workflowService.isEndEvent(taskEntity);
            if (RegexUtil.optNotNull(endNodeId).isPresent()) {
                isEnd = true;
            }
        }
        if (event.getType().equals(FlowableEngineEventType.TASK_CREATED) || isEnd) {
            TaskEntity taskEntity;
            if ("end_event".equals(endNodeId)) {
                return;
            }
            if (event instanceof FlowableEntityEventImpl) {
                taskEntity = (TaskEntity) ((FlowableEntityEventImpl) event).getEntity();
            } else if (event instanceof org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl) {
                taskEntity = (TaskEntity) ((org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl) event).getEntity();
            } else {
                return;
            }
            String requestParam = RegexUtil.optStrAndValOrBlank(taskEntity.getVariableLocal(FlowParmsConstant.REQUEST_PARAM), taskEntity.getVariable(FlowParmsConstant.REQUEST_PARAM));
            String sub_token = RegexUtil.optStrAndValOrBlank(taskEntity.getVariableLocal("sub_token"), taskEntity.getVariable("sub_token"));
            WorksService worksService = SpringContextHolder.getBean(WorksService.class);
            if (RegexUtil.optNotNull(endNodeId).isPresent()) {
                taskEntity.setTaskDefinitionKey(endNodeId);
            }
            if (RegexUtil.optIsPresentStr(requestParam)) {
                MethodParam methodParam = JSONObject.parseObject(requestParam, MethodParam.class);
                worksService.callBackStartWithForm(methodParam, taskEntity, null, isEnd, FlowableEngineEventType.TASK_CREATED);
            } else if (RegexUtil.optIsPresentStr(sub_token)) {
                CacheUtilService cacheUtilService = SpringContextHolder.getBean(CacheUtilService.class);
                String method = cacheUtilService.getSubToken(null, sub_token);
                cacheUtilService.deleteSubToken(sub_token);
                MethodParam methodParam = JSONObject.parseObject(method, MethodParam.class);
                worksService.callBackStartWithForm(methodParam, taskEntity, null, isEnd, FlowableEngineEventType.TASK_CREATED);
            }
        } else {
            logger.debug(event.getType().name());
        }
    }

    /**
     * @return whether or not the current operation should fail when this listeners execution throws an exception.
     */
    @Override
    public boolean isFailOnException() {
        return true;
    }

    /**
     * @return Returns whether this event listener fires immediately when the event occurs or
     * on a transaction lifecycle event (before/after commit or rollback).
     */
    @Override
    public boolean isFireOnTransactionLifecycleEvent() {
        return false;//如果返回了False，事件就会包裹在事务内。
    }

    /**
     * @return if non-null, indicates the point in the lifecycle of the current transaction when the event should be fired.
     */
    @Override
    public String getOnTransaction() {
        return null;
        //return TransactionState.COMMITTED.name();
    }
}
