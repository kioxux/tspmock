package com.gengyun.senscloud.flowable;

import java.io.IOException;
import java.util.zip.ZipInputStream;

import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.spring.configurator.AbstractAutoDeploymentStrategy;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.DeploymentBuilder;
import org.springframework.core.io.Resource;


public class DefaultAutoDeploymentStrategy extends AbstractAutoDeploymentStrategy {
    protected TenantInfoHolder tenantInfoHolder;
    /**
     * The deployment mode this strategy handles.
     */
    public static final String DEPLOYMENT_MODE = "default";

    public DefaultAutoDeploymentStrategy(TenantInfoHolder tenantInfoHolder) {
        this.tenantInfoHolder = tenantInfoHolder;
    }

    @Override
    protected String getDeploymentMode() {
        return DEPLOYMENT_MODE;
    }

    @Override
    public void deployResources(final String deploymentNameHint, final Resource[] resources, final RepositoryService repositoryService) {
        for (String tenantId : tenantInfoHolder.getAllTenants()) {
            // Create a single deployment for all resources using the name hint as the literal name
            tenantInfoHolder.setCurrentTenantId(tenantId);
            final DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().enableDuplicateFiltering().name(deploymentNameHint);

            for (final Resource resource : resources) {
                final String resourceName = determineResourceName(resource);

                try {
                    if (resourceName.endsWith(".bar") || resourceName.endsWith(".zip") || resourceName.endsWith(".jar")) {
                        deploymentBuilder.addZipInputStream(new ZipInputStream(resource.getInputStream()));
                    } else {
                        deploymentBuilder.addInputStream(resourceName, resource.getInputStream());
                    }
                } catch (IOException e) {
                    throw new FlowableException("couldn't auto deploy resource '" + resource + "': " + e.getMessage(), e);
                }
            }
            deploymentBuilder.tenantId(tenantId).deploy();
            tenantInfoHolder.clearCurrentTenantId();
        }

    }

}