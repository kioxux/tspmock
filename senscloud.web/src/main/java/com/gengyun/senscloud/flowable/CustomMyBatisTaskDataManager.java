package com.gengyun.senscloud.flowable;

import org.flowable.task.api.Task;
import org.flowable.task.service.impl.TaskQueryImpl;
import org.flowable.task.service.impl.persistence.entity.data.impl.MybatisTaskDataManager;
import org.flowable.task.service.impl.util.CommandContextUtil;

import java.util.Collections;
import java.util.List;

public class CustomMyBatisTaskDataManager extends MybatisTaskDataManager {
    @SuppressWarnings("unchecked")
    public List<Task> findTasksWithRelatedEntities(CustomTaskQueryImpl taskQuery) {
        final String query = "selectTasksWithRelatedEntities";
        // paging doesn't work for combining task instances and variables due to
        // an outer join, so doing it in-memory

        int firstResult = taskQuery.getFirstResult();
        int maxResults = taskQuery.getMaxResults();

        // setting max results, limit to 20000 results for performance reasons
        if (taskQuery.getTaskVariablesLimit() != null) {
            taskQuery.setMaxResults(taskQuery.getTaskVariablesLimit());
        } else {
            taskQuery.setMaxResults(CommandContextUtil.getTaskServiceConfiguration().getTaskQueryLimit());
        }
        taskQuery.setMaxResults(Integer.MAX_VALUE);
        taskQuery.setFirstResult(0);

        List<Task> instanceList = getDbSqlSession().selectListWithRawParameterNoCacheCheck(query, taskQuery);

        if (instanceList != null && !instanceList.isEmpty()) {
            if (firstResult > 0) {
                if (firstResult <= instanceList.size()) {
                    int toIndex = firstResult + Math.min(maxResults, instanceList.size() - firstResult);
                    return instanceList.subList(firstResult, toIndex);
                } else {
                    return Collections.EMPTY_LIST;
                }
            } else {
                int toIndex = maxResults > 0 ?  Math.min(maxResults, instanceList.size()) : instanceList.size();
                return instanceList.subList(0, toIndex);
            }
        }
        return Collections.EMPTY_LIST;
    }

    public long findTaskCount(CustomTaskQueryImpl taskQuery) {
        return (Long) getDbSqlSession().selectOne("selectTaskCount", taskQuery);
    }
}
