package com.gengyun.senscloud.flowable.listener;

import com.gengyun.senscloud.config.SpringContextHolder;
import org.apache.commons.lang.StringUtils;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckEndListener implements ExecutionListener {
    Logger logger = LoggerFactory.getLogger(CheckEndListener.class);
    @Override
    public void notify(DelegateExecution execution) {
        String tenantId = execution.getTenantId();
        String deploymentId = (String) execution.getVariable("deploymentId");
        TenantInfoHolder tenantInfoHolder = SpringContextHolder.getBean("tenantInfoHolder");
        RepositoryService repositoryService = SpringContextHolder.getBean(RepositoryService.class);
        if (StringUtils.isNotEmpty(tenantId) && StringUtils.isNotEmpty(deploymentId) && tenantInfoHolder != null && repositoryService != null) {
            tenantInfoHolder.setCurrentTenantId(tenantId);
            try {
                repositoryService.deleteDeployment(deploymentId, true);
            } catch (Exception e) {
                logger.error(e.getLocalizedMessage());
            }
            tenantInfoHolder.clearCurrentTenantId();
        }
    }
}
