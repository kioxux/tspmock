package com.gengyun.senscloud.flowable;

import java.io.IOException;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang.StringUtils;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.spring.configurator.AbstractAutoDeploymentStrategy;
import org.springframework.core.io.Resource;

/**
 * Implementation of {@link AutoDeploymentStrategy} that performs a separate deployment for each resource by name.
 * 
 * @author Tiese Barrell
 */
public class SingleResourceAutoDeploymentStrategy extends AbstractAutoDeploymentStrategy {
    protected TenantInfoHolder tenantInfoHolder;

    public SingleResourceAutoDeploymentStrategy(TenantInfoHolder tenantInfoHolder) {
        this.tenantInfoHolder = tenantInfoHolder;
    }

    /**
     * The deployment mode this strategy handles.
     */
    public static final String DEPLOYMENT_MODE = "single-resource";

    @Override
    protected String getDeploymentMode() {
        return DEPLOYMENT_MODE;
    }

    @Override
    public void deployResources(final String deploymentNameHint, final Resource[] resources, final RepositoryService repositoryService) {
        for (String tenantId : tenantInfoHolder.getAllTenants()) {
            // Create a separate deployment for each resource using the resource name
            tenantInfoHolder.setCurrentTenantId(tenantId);
            for (final Resource resource : resources) {

                final String resourceName = determineResourceName(resource);
                final DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().enableDuplicateFiltering().name(resourceName);

                try {
                    if (resourceName.endsWith(".bar") || resourceName.endsWith(".zip") || resourceName.endsWith(".jar")) {
                        deploymentBuilder.addZipInputStream(new ZipInputStream(resource.getInputStream()));
                    } else {
                        deploymentBuilder.addInputStream(resourceName, resource.getInputStream());
                    }
                } catch (IOException e) {
                    throw new FlowableException("couldn't auto deploy resource '" + resource + "': " + e.getMessage(), e);
                }
                deploymentBuilder.tenantId(tenantId).deploy();
            }
            tenantInfoHolder.clearCurrentTenantId();
        }
    }

}