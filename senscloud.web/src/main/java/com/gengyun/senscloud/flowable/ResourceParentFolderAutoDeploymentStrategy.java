package com.gengyun.senscloud.flowable;

import org.apache.commons.lang.StringUtils;
import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.flowable.spring.configurator.AbstractAutoDeploymentStrategy;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipInputStream;

import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.DeploymentBuilder;
import org.springframework.core.io.Resource;

public class ResourceParentFolderAutoDeploymentStrategy extends AbstractAutoDeploymentStrategy {
    protected TenantInfoHolder tenantInfoHolder;

    public ResourceParentFolderAutoDeploymentStrategy(TenantInfoHolder tenantInfoHolder) {
        this.tenantInfoHolder = tenantInfoHolder;
    }

    /**
     * The deployment mode this strategy handles.
     */
    public static final String DEPLOYMENT_MODE = "resource-parent-folder";

    private static final String DEPLOYMENT_NAME_PATTERN = "%s.%s";

    @Override
    protected String getDeploymentMode() {
        return DEPLOYMENT_MODE;
    }

    @Override
    public void deployResources(final String deploymentNameHint, final Resource[] resources, final RepositoryService repositoryService) {
        for (String tenantId : tenantInfoHolder.getAllTenants()) {
            // Create a deployment for each distinct parent folder using the name
            // hint
            // as a prefix
            tenantInfoHolder.setCurrentTenantId(tenantId);
            final Map<String, Set<Resource>> resourcesMap = createMap(resources);

            for (final Entry<String, Set<Resource>> group : resourcesMap.entrySet()) {

                final String deploymentName = determineDeploymentName(deploymentNameHint, group.getKey());

                final DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().enableDuplicateFiltering().name(deploymentName);

                for (final Resource resource : group.getValue()) {
                    final String resourceName = determineResourceName(resource);

                    try {
                        if (resourceName.endsWith(".bar") || resourceName.endsWith(".zip") || resourceName.endsWith(".jar")) {
                            deploymentBuilder.addZipInputStream(new ZipInputStream(resource.getInputStream()));
                        } else {
                            deploymentBuilder.addInputStream(resourceName, resource.getInputStream());
                        }
                    } catch (IOException e) {
                        throw new FlowableException("couldn't auto deploy resource '" + resource + "': " + e.getMessage(), e);
                    }
                }
                deploymentBuilder.tenantId(tenantId).deploy();
            }
            tenantInfoHolder.clearCurrentTenantId();
        }

    }

    private Map<String, Set<Resource>> createMap(final Resource[] resources) {
        final Map<String, Set<Resource>> resourcesMap = new HashMap<>();

        for (final Resource resource : resources) {
            final String parentFolderName = determineGroupName(resource);
            if (resourcesMap.get(parentFolderName) == null) {
                resourcesMap.put(parentFolderName, new HashSet<>());
            }
            resourcesMap.get(parentFolderName).add(resource);
        }
        return resourcesMap;
    }

    private String determineGroupName(final Resource resource) {
        String result = determineResourceName(resource);
        try {
            if (resourceParentIsDirectory(resource)) {
                result = resource.getFile().getParentFile().getName();
            }
        } catch (IOException e) {
            // no-op, fallback to resource name
        }
        return result;
    }

    private boolean resourceParentIsDirectory(final Resource resource) throws IOException {
        return resource.getFile() != null && resource.getFile().getParentFile() != null && resource.getFile().getParentFile().isDirectory();
    }

    private String determineDeploymentName(final String deploymentNameHint, final String groupName) {
        return String.format(DEPLOYMENT_NAME_PATTERN, deploymentNameHint, groupName);
    }
}