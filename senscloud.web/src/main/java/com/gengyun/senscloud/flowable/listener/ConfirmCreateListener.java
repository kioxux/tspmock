package com.gengyun.senscloud.flowable.listener;

import com.gengyun.senscloud.util.RegexUtil;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;

@Deprecated
public class ConfirmCreateListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        String receiveAccount = RegexUtil.optStrAndValOrBlank(delegateTask.getVariableLocal("receive_account"), delegateTask.getVariable("receive_account"));
        RegexUtil.optNotBlankStrOpt(receiveAccount).ifPresent(ra -> {
            delegateTask.setVariableLocal("receive_account", receiveAccount);
            ((TaskEntity) delegateTask).setAssigneeValue(receiveAccount);
        });
    }
}
