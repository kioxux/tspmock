package com.gengyun.senscloud.flowable;

import org.flowable.task.api.Task;
import org.flowable.task.service.TaskServiceConfiguration;
import org.flowable.task.service.impl.persistence.entity.TaskEntityManagerImpl;
import org.flowable.task.service.impl.persistence.entity.data.TaskDataManager;

import java.util.List;

public class CustomTaskEntityManagerImpl extends TaskEntityManagerImpl {
    public CustomTaskEntityManagerImpl(TaskServiceConfiguration taskServiceConfiguration, TaskDataManager taskDataManager) {
        super(taskServiceConfiguration, taskDataManager);
    }

    public List<Task> findTasksWithRelatedEntities(CustomTaskQueryImpl taskQuery) {
        return ((CustomMyBatisTaskDataManager)taskDataManager).findTasksWithRelatedEntities(taskQuery);
    }

    public long findTaskCount(CustomTaskQueryImpl taskQuery) {
        return ((CustomMyBatisTaskDataManager)taskDataManager).findTaskCount(taskQuery);
    }
}
