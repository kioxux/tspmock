package com.gengyun.senscloud.flowable;

import org.flowable.common.engine.api.FlowableIllegalArgumentException;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.common.engine.impl.interceptor.CommandExecutor;
import org.flowable.task.api.Task;
import org.flowable.task.service.TaskServiceConfiguration;
import org.flowable.task.service.impl.TaskQueryImpl;
import org.flowable.task.service.impl.util.CommandContextUtil;

import java.util.List;

public class CustomTaskQueryImpl extends TaskQueryImpl {
    private static final long serialVersionUID = 1L;

    protected List<String> facilityIds;
    protected List<String> subWorkCodes;
    protected List<String> workTypeIds;
    protected List<String> businessTypeIds;
    protected Boolean showAssigneeName;
    protected String assigneeNameLike;
    protected String titlePageLike;
    protected String strcodeLike;
    protected String loginUserId;
    protected String startTime;
    protected String endTime;
    protected String workTypeId;
    protected String keyword;

    public CustomTaskQueryImpl(){}

    public CustomTaskQueryImpl(CommandExecutor commandExecutor, String databaseType) {
        super(commandExecutor, databaseType);
    }

    public String getKeyword() { return keyword; }

    public String getWorkTypeId() { return workTypeId; }

    public String getStartTime() { return startTime; }

    public String getEndTime() { return endTime; }

    public String getLoginUserId() { return loginUserId; }

    public List<String> getFacilityIds() {
        return facilityIds;
    }

    public Boolean getShowAssigneeName() {
        return showAssigneeName;
    }

    public List<String> getSubWorkCodes() {
        return subWorkCodes;
    }

    public String getTitlePageLike() {
        return titlePageLike;
    }

    public String getStrcodeLike() {
        return strcodeLike;
    }

    public List<String> getWorkTypeIds() { return workTypeIds; }

    public List<String> getBusinessTypeIds() { return businessTypeIds; }

    @Override
    public CustomTaskQueryImpl taskTenantId(String tenantId) {
        if (tenantId == null) {
            throw new FlowableIllegalArgumentException("task tenant id is null");
        }
        this.tenantId = tenantId;
        return this;
    }

    @Override
    public CustomTaskQueryImpl taskAssignee(String assignee) {
        if (assignee == null) {
            throw new FlowableIllegalArgumentException("Assignee is null");
        }
        this.assignee = assignee;
        return this;
    }

    @Override
    public CustomTaskQueryImpl taskCandidateGroup(String candidateGroup) {
        if (candidateGroup == null) {
            throw new FlowableIllegalArgumentException("Candidate group is null");
        }
        if (candidateGroups != null) {
            throw new FlowableIllegalArgumentException("Invalid query usage: cannot set both candidateGroup and candidateGroupIn");
        }
        this.candidateGroup = candidateGroup;
        return this;
    }

    @Override
    public CustomTaskQueryImpl taskCandidateGroupIn(List<String> candidateGroups) {
        if (candidateGroups == null) {
            throw new FlowableIllegalArgumentException("Candidate group list is null");
        }

        if (candidateGroups.isEmpty()) {
            throw new FlowableIllegalArgumentException("Candidate group list is empty");
        }

        if (candidateGroup != null) {
            throw new FlowableIllegalArgumentException("Invalid query usage: cannot set both candidateGroupIn and candidateGroup");
        }

        this.candidateGroups = candidateGroups;
        return this;
    }

    public CustomTaskQueryImpl keyword(String keyword) {
        if (keyword == null) {
            throw new FlowableIllegalArgumentException("keyword is null");
        }
        this.keyword = keyword;
        return this;
    }

    public CustomTaskQueryImpl workTypeId(String workTypeId) {
        if (workTypeId == null) {
            throw new FlowableIllegalArgumentException("workTypeId is null");
        }
        this.workTypeId = workTypeId;
        return this;
    }

    public CustomTaskQueryImpl startTime(String startTime) {
        if (startTime == null) {
            throw new FlowableIllegalArgumentException("startTime is null");
        } else {
            this.startTime = startTime;
        }
        return this;
    }

    public CustomTaskQueryImpl endTime(String endTime) {
        if (endTime == null) {
            throw new FlowableIllegalArgumentException("endTime is null");
        } else {
            this.endTime = endTime;
        }
        return this;
    }

    public CustomTaskQueryImpl loginUserId(String loginUserId) {
        if (loginUserId == null) {
            throw new FlowableIllegalArgumentException("LoginUserId is null");
        } else {
            this.loginUserId = loginUserId;
        }
        return this;
    }

    public CustomTaskQueryImpl facilityIdIn(List<String> facilityIds) {
        if (facilityIds == null) {
            throw new FlowableIllegalArgumentException("FacilityIds is null");
        }
        this.facilityIds = facilityIds;
        return this;
    }

    public CustomTaskQueryImpl workTypeIdIn(List<String> workTypeIds) {
        if (workTypeIds == null) {
            throw new FlowableIllegalArgumentException("workTypeIds is null");
        }
        this.workTypeIds = workTypeIds;
        return this;
    }

    public CustomTaskQueryImpl businessTypeIdsIn(List<String> businessTypeIds) {
        if (businessTypeIds == null) {
            throw new FlowableIllegalArgumentException("businessTypeIds is null");
        }
        this.businessTypeIds = businessTypeIds;
        return this;
    }

    public CustomTaskQueryImpl subWorkCodes(List<String> subWorkCodes) {
        if (subWorkCodes == null) {
            throw new FlowableIllegalArgumentException("SubWorkCodes is null");
        }
        this.subWorkCodes = subWorkCodes;
        return this;
    }

    public CustomTaskQueryImpl showAssigneeName(Boolean showAssigneeName) {
        if (showAssigneeName == null) {
            this.showAssigneeName = false;
        } else {
            this.showAssigneeName = showAssigneeName;
        }
        return this;
    }

    public CustomTaskQueryImpl assigneeNameLike(String assigneeNameLike) {
        if (assigneeNameLike == null) {
            throw new FlowableIllegalArgumentException("AssigneeNameLike is null");
        } else {
            this.assigneeNameLike = assigneeNameLike;
        }
        return this;
    }

    public CustomTaskQueryImpl titlePageLike(String titlePageLike) {
        if (titlePageLike == null) {
            throw new FlowableIllegalArgumentException("TitlePageLike is null");
        } else {
            this.titlePageLike = titlePageLike;
        }
        return this;
    }

    public CustomTaskQueryImpl strcodeLike(String strcodeLike) {
        if (strcodeLike == null) {
            throw new FlowableIllegalArgumentException("StrcodeLike is null");
        } else {
            this.strcodeLike = strcodeLike;
        }
        return this;
    }

    @Override
    public List<Task> executeList(CommandContext commandContext) {
        ensureVariablesInitialized();
        checkQueryOk();
        List<Task> tasks = null;
        TaskServiceConfiguration taskServiceConfiguration = CommandContextUtil.getTaskServiceConfiguration(commandContext);
        if (taskServiceConfiguration.getTaskQueryInterceptor() != null) {
            taskServiceConfiguration.getTaskQueryInterceptor().beforeTaskQueryExecute(this);
        }

        CustomTaskEntityManagerImpl customTaskEntityManager = (CustomTaskEntityManagerImpl) CommandContextUtil.getTaskEntityManager(commandContext);
        tasks = customTaskEntityManager.findTasksWithRelatedEntities(this);

        if (tasks != null && taskServiceConfiguration.getInternalTaskLocalizationManager() != null && taskServiceConfiguration.isEnableLocalization()) {
            for (Task task : tasks) {
                taskServiceConfiguration.getInternalTaskLocalizationManager().localize(task, locale, withLocalizationFallback);
            }
        }

        if (taskServiceConfiguration.getTaskQueryInterceptor() != null) {
            taskServiceConfiguration.getTaskQueryInterceptor().afterTaskQueryExecute(this, tasks);
        }

        return tasks;
    }

    @Override
    public long executeCount(CommandContext commandContext) {
        ensureVariablesInitialized();
        checkQueryOk();

        TaskServiceConfiguration taskServiceConfiguration = CommandContextUtil.getTaskServiceConfiguration(commandContext);
        if (taskServiceConfiguration.getTaskQueryInterceptor() != null) {
            taskServiceConfiguration.getTaskQueryInterceptor().beforeTaskQueryExecute(this);
        }
        CustomTaskEntityManagerImpl customTaskEntityManager = (CustomTaskEntityManagerImpl) CommandContextUtil.getTaskEntityManager(commandContext);
        return customTaskEntityManager.findTaskCount(this);
    }
}
