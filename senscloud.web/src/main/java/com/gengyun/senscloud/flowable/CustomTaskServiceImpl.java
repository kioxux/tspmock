package com.gengyun.senscloud.flowable;


import org.flowable.engine.impl.TaskServiceImpl;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;

public class CustomTaskServiceImpl extends TaskServiceImpl {

    public CustomTaskServiceImpl(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    public CustomTaskQueryImpl createCustomTaskQuery() {
        return new CustomTaskQueryImpl(commandExecutor, configuration.getDatabaseType());
    }
}
