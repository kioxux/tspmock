//package com.gengyun.senscloud.flowable.service;
//
//import com.gengyun.senscloud.util.Constants;
//import com.gengyun.senscloud.util.SmsUtil;
//import org.flowable.common.engine.api.delegate.Expression;
//import org.flowable.engine.delegate.DelegateExecution;
//import org.flowable.engine.delegate.JavaDelegate;
//
//import java.util.Map;
//
//public class SMSService implements JavaDelegate {
//    private Expression fieldContent;
//    private Expression fieldPhone;
//    private Expression fieldResult;
//
//    @Override
//    public void execute(DelegateExecution execution) {
//        String content = (String) fieldContent.getValue(execution);
//        String phone = (String) fieldPhone.getValue(execution);
//        Map<String, Object> resultMap = SmsUtil.sendSms(Constants.SENSCLOUD_SMS_YUNDA, phone, content);
//        Boolean result = (Boolean)resultMap.get("result");
//        if (result == null) {
//            result = false;
//        }
//        execution.setVariable(fieldResult.getValue(execution).toString(), result);
//    }
//}
