package com.gengyun.senscloud.flowable;

import com.gengyun.senscloud.common.Constants;
import org.flowable.task.service.impl.persistence.entity.TaskEntityImpl;

import java.text.SimpleDateFormat;

public class CustomTaskEntityImpl extends TaskEntityImpl {
    protected String assigneeName;

    private String createTimeStr;

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getCreateTimeStr() {
        if(this.createTime == null) {
            return null;
        }

        return new SimpleDateFormat(Constants.DATE_FMT_SS).format(this.createTime);
    }
}
