package com.gengyun.senscloud.flowable;

import org.flowable.common.engine.impl.cfg.multitenant.TenantInfoHolder;
import org.springframework.context.annotation.Bean;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class TenantInfoHolderImpl implements TenantInfoHolder {
    protected static ThreadLocal<String> tenantKey = new ThreadLocal<String>();
    protected static Set<String> tenants = new HashSet<>();

    @Override
    public Collection<String> getAllTenants() {
        return tenants;
    }

    @Override
    public void setCurrentTenantId(String tenantid) {
        tenantKey.set(tenantid);
        tenants.add(tenantid);
    }

    @Override
    public String getCurrentTenantId() {
        return tenantKey.get();
    }

    @Override
    public void clearCurrentTenantId() {
        tenantKey.set(null);
    }
}
