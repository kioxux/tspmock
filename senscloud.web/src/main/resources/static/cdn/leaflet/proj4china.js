;(function(factory) {
  if (
    typeof window.L === 'undefined' ||
    typeof window.proj4 === 'undefined' ||
    typeof window.L.Proj === 'undefined'
  ) {
    throw 'Leaflet, proj4, proj4leaflet must be loaded first'
  } else {
    factory(window.L)
  }
})(function(L) {
  'use strict'

  var PI = 3.1415926535897932384626
  var R = 6378245.0
  var EE = 0.00669342162296594323
  var XPI = (PI * 3000.0) / 180.0

  function transformLat(x, y) {
    var ret =
      -100.0 +
      2.0 * x +
      3.0 * y +
      0.2 * y * y +
      0.1 * x * y +
      0.2 * Math.sqrt(Math.abs(x))
    ret +=
      ((20.0 * Math.sin(6.0 * x * PI) + 20.0 * Math.sin(2.0 * x * PI)) * 2.0) /
      3.0
    ret +=
      ((20.0 * Math.sin(y * PI) + 40.0 * Math.sin((y / 3.0) * PI)) * 2.0) / 3.0
    ret +=
      ((160.0 * Math.sin((y / 12.0) * PI) + 320 * Math.sin((y * PI) / 30.0)) *
        2.0) /
      3.0
    return ret
  }
  function transformLng(x, y) {
    var ret =
      300.0 +
      x +
      2.0 * y +
      0.1 * x * x +
      0.1 * x * y +
      0.1 * Math.sqrt(Math.abs(x))
    ret +=
      ((20.0 * Math.sin(6.0 * x * PI) + 20.0 * Math.sin(2.0 * x * PI)) * 2.0) /
      3.0
    ret +=
      ((20.0 * Math.sin(x * PI) + 40.0 * Math.sin((x / 3.0) * PI)) * 2.0) / 3.0
    ret +=
      ((150.0 * Math.sin((x / 12.0) * PI) + 300.0 * Math.sin((x / 30.0) * PI)) *
        2.0) /
      3.0
    return ret
  }
  function transform(x, y) {
    var tx = transformLat(x - 105.0, y - 35.0),
      ty = transformLng(x - 105.0, y - 35.0),
      r = (y / 180.0) * PI,
      sr = Math.sin(r),
      m = 1 - EE * sr * sr,
      sm = Math.sqrt(m)
    return {
      lng: x + (ty * 180.0) / ((R / sm) * Math.cos(r) * PI),
      lat: y + (tx * 180.0) / (((R * (1 - EE)) / (m * sm)) * PI)
    }
  }

  function wgs84_to_gcj02(x, y) {
    return transform(x, y)
  }
  function gcj02_to_bd09(x, y) {
    var z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * XPI),
      t = Math.atan2(y, x) + 0.000003 * Math.cos(x * XPI)
    return {
      lng: z * Math.cos(t) + 0.0065,
      lat: z * Math.sin(t) + 0.006
    }
  }

  function bd09_to_gcj02(x, y) {
    x -= 0.0065
    y -= 0.006
    var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * XPI),
      t = Math.atan2(y, x) - 0.000003 * Math.cos(x * XPI)
    return {
      lng: z * Math.cos(t),
      lat: z * Math.sin(t)
    }
  }
  function gcj02_to_wgs84(x, y) {
    var txy = transform(x, y)
    return {
      lng: x * 2 - txy.lng,
      lat: y * 2 - txy.lat
    }
  }

  function wgs84_to_bd09(x, y) {
    var gcj02 = wgs84_to_gcj02(x, y)
    return gcj02_to_bd09(gcj02.lng, gcj02.lat)
  }
  function bd09_to_wgs84(x, y) {
    var gcj02 = bd09_to_gcj02(x, y)
    return gcj02_to_wgs84(gcj02.lng, gcj02.lat)
  }
  function resolutions() {
    var res = []
    let level = 19
    while (level--) res.push(Math.pow(2, level))
    return res
  }
  // if (L.Proj)
  L.CRS.Baidu = new L.Proj.CRS(
    'EPSG:900913',
    '+proj=merc +a=6378206 +b=6356584.314245179 +lat_ts=0.0 +lon_0=0.0 +x_0=0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs',
    {
      resolutions: resolutions(),
      origin: [0, 0],
      bounds: L.bounds([20037508.342789244, 0], [0, 20037508.342789244])
    }
  )

  L.GridLayer.include({
    _setZoomTransform(level, center, zoom) {
      var isGCJ02 = this.options.isGCJ02,
        isBaidu = this.options.isBaidu,
        lng = center.lng,
        lat = center.lat
      if (isGCJ02) center = wgs84_to_gcj02(lng, lat)
      if (isBaidu) center = wgs84_to_bd09(lng, lat)

      var scale = this._map.getZoomScale(zoom, level.zoom),
        translate = level.origin
          .multiplyBy(scale)
          .subtract(this._map._getNewPixelOrigin(center, zoom))
          .round()

      if (L.Browser.any3d) {
        L.DomUtil.setTransform(level.el, translate, scale)
      } else {
        L.DomUtil.setPosition(level.el, translate)
      }
    },
    _getTiledPixelBounds(center) {
      var isGCJ02 = this.options.isGCJ02,
        isBaidu = this.options.isBaidu,
        lng = center.lng,
        lat = center.lat
      if (isGCJ02) center = wgs84_to_gcj02(lng, lat)
      if (isBaidu) center = wgs84_to_bd09(lng, lat)

      var map = this._map,
        mapZoom = map._animatingZoom
          ? Math.max(map._animateToZoom, map.getZoom())
          : map.getZoom(),
        scale = map.getZoomScale(mapZoom, this._tileZoom),
        pixelCenter = map.project(center, this._tileZoom).floor(),
        halfSize = map.getSize().divideBy(scale * 2)

      return new L.Bounds(
        pixelCenter.subtract(halfSize),
        pixelCenter.add(halfSize)
      )
    }
  })
})
