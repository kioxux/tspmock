-- 长安预审数据
select ab.order_number as '申请单号',ab.client_name as '客户姓名',ab.id_card as '身份证号',ab.remark as '原因',case when ab.is_complete_pre_apply=1 then '精准预审通过' else '精准预审拒绝'
end as '精准预审状态',ab.create_time as '订单创建时间' from tclient_apply_basic ab left join tclient_order_product_relationship rp on ab.id=rp.order_id where rp.capital='changan' and rp.zb_child_status='1' and year(ab.create_time) = year(CURDATE()) order by ab.id desc;

-- S6预审数据
select ab.order_number as '申请单号',ab.client_name as '客户姓名',ab.id_card as '身份证号',ab.remark as '原因',case when ab.pretrial_loan_status=1 and ab.is_complete_auick_review=1 then '资方预审通过' when ab.pretrial_loan_status=2 and ab.is_complete_auick_review !=1 then '资方预审拒绝' when pretrial_loan_status=0 and ab.is_complete_auick_review !=1 then '资方预审中' else '资方待预审'
end as '资方预审状态',ab.create_time as '订单创建时间' from tclient_apply_basic ab left join tclient_order_product_relationship rp on ab.id=rp.order_id where rp.capital='hebei' and rp.zb_child_status='1' and year(ab.create_time) = year(CURDATE()) order by ab.id desc;

-- 电销数据
select ab.order_number as '申请单号',ab.client_name as '客户名称',CONCAT(SUBSTRING(ab.mobile, 1, 3), '****', SUBSTRING(ab.mobile, 8)) as '客户手机号',pm.product_name as '产品名称',case ab.apply_status
	when '300110' then '预审中'
	when '300111' then '预审通过'
	when '300112' then '预审拒绝'
	when '300113' then '资料完善中'
	when '300114' then '产品选择'
	when '300115' then '弃单'
	when '300120' then '风控审核中'
	when '300121' then '风控驳回'
	when '300122' then '风控失败'
	when '300123' then '风控通过'
	when '300125' then '放款申请中'
	when '300126' then '放款拒绝'
	when '300127' then '放款成功'
	when '300130' then '放款驳回'
	when '300131' then '待快审'
	when '300132' then '已失效'
	else '未知' end as '订单状态',ab.create_time as '订单创建时间',ao.load_time as '放款时间' from client.tclient_apply_basic ab left join client.tclient_order_product_relationship rp on ab.id=rp.order_id left join base.product_max_info pm on pm.id=rp.product_id left join client.tclient_apply_order ao on ab.order_number=ao.order_number where rp.zb_child_status='1' and pm.product_name like '%荣耀%';

-- 电销数据  
with ab as (select ab.client_name as '客户名称',ab.mobile as '手机号',pm.id as '产品id',pm.product_name as '产品name',ab.order_number as 'order_number',ab.apply_status as 'apply_status',ab.financing_amount as '审批金额',ab.audit_amount as '批款金额',ao.load_time as '放款时间' from client.tclient_apply_basic ab left join client.tclient_order_product_relationship rp on ab.id=rp.order_id left join base.product_max_info pm on pm.id=rp.product_id left join client.tclient_apply_order ao on ab.order_number=ao.order_number where rp.zb_child_status='1' and (pm.product_name like '%S2%' or pm.product_name like '%S5%')),tl as (select group_concat(distinct tl.oper_reason_remark) as oper_reason_remark,group_concat(distinct cp.project_code) as project_code from workflow.task_list tl left join base.client_project cp on cp.project_id=tl.project_id where tl.project_id in (select project_id from base.client_project where project_code in (select ab.order_number from ab)) and tl.task_code !='CLF10010015' group by tl.project_id)

select ab.*,tl.oper_reason_remark as '审批结果' from ab left join tl on tl.project_code=ab.order_number;

-- 进半年放款数据
select ab.order_number as '申请单号',ab.client_name as '客户名称',ab.id_card as '身份证号',pm.product_name as '产品名称',ab.store as '渠道名称',ab.create_time as '订单创建时间',ao.load_time as '放款时间',TIMESTAMPDIFF(HOUR, ab.create_time, ao.load_time) as '时效（小时）',DATEDIFF(ao.load_time, ab.create_time) as '时效（天）' from client.tclient_apply_basic ab left join client.tclient_order_product_relationship rp on ab.id=rp.order_id left join base.product_max_info pm on pm.id=rp.product_id left join client.tclient_apply_order ao on ab.order_number=ao.order_number where ab.apply_status='300127' and ab.create_time >= DATE_SUB(CURDATE(), INTERVAL 6 MONTH) and rp.zb_child_status='1' and rp.zf_child_status='3';

-- 档案要求公户车信息
select distinct lr.unique_customer_id as '客户编码', ab.store as '门店', lr.product_name as '产品', ab.client_name as '客户姓名', ac.car_number as '车牌号', '是' as '公司户' from client.tclient_apply_basic ab left join loaning1119.loan_record lr on ab.id_card=lr.client_identify left join client.tclient_apply_car ac on ab.id=ac.order_id and lr.client_plate=ac.car_number where ac.ownership=0;