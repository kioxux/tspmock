-- NAME: tclient_apply_company_bondsmon 订单信息-担保人企业表
select * from client.tclient_apply_company_bondsmon where order_id in (select id from client.tclient_apply_basic where order_number in ('QR2024112607363033'));
-- NAME: tclient_apply_person_bondsmon 订单信息-个人担保人表
select * from client.tclient_apply_person_bondsmon where order_id in (select id from client.tclient_apply_basic where order_number in ('QR2024112607363033'));
-- NAME: tclient_audit_log 资方审批记录表
select * from client.tclient_audit_log where order_id in (select id from client.tclient_apply_basic where order_number in ('QR2024112607363033'));
-- NAME: act_hi_actinst 表 历史的流程实例
select * from workflowserver.act_hi_actinst where PROC_INST_ID_ in (select instance_id from workflow.task_list where project_id in (select project_id from base.client_project where project_code in ('QR2024112607363033'))) order by START_TIME_ asc,ID_ asc;
-- NAME: act_hi_taskinst 表 历史的任务实例
select * from workflowserver.act_hi_taskinst where PROC_INST_ID_ in (select instance_id from workflow.task_list where project_id in (select project_id from base.client_project where project_code in ('QR2024112607363033'))) order by START_TIME_ asc;
-- NAME: act_hi_varinst 表 历史的任务参数
select * from workflowserver.act_hi_varinst where PROC_INST_ID_ in (select instance_id from workflow.task_list where project_id in (select project_id from base.client_project where project_code in ('QR2024112607363033'))) order by ID_ asc;
-- NAME: act_ge_bytearray 表 流程定义数据表 act_re_procdef 流程定义信息表
-- 在Activiti中，如果你多次部署相同的流程定义（具有相同的key），则Activiti默认将启动最新版本的流程定义
select agb.BYTES_,arp.* from workflowserver.act_ge_bytearray agb join workflowserver.act_re_procdef arp on agb.DEPLOYMENT_ID_=arp.DEPLOYMENT_ID_ and agb.NAME_=arp.RESOURCE_NAME_ where arp.ID_ in (select PROC_DEF_ID_ from workflowserver.act_hi_actinst where PROC_INST_ID_ in (select instance_id from workflow.task_list where project_id in (select project_id from base.client_project where project_code in ('QR2024112607363033'))));
-- NAME: 获取当前启用的流程模型的信息
select * from workflowserver.ACT_RE_PROCDEF pd where pd.VERSION_ = (select MAX(pd2.VERSION_) from workflowserver.ACT_RE_PROCDEF pd2 where pd2.KEY_ = pd.KEY_) order by pd.KEY_, pd.VERSION_ desc;
-- NAME: 流程部署涉及的相关表
-- ACT_RE_MODEL：存储流程模型的基本信息，如模型ID、名称、关键字等。ACT_GE_BYTEARRAY：存储流程定义的XML数据。ACT_RE_DEPLOYMENT：存储流程部署的信息。ACT_RE_PROCDEF：存储流程定义的详细信息。
select * from workflowserver.act_ge_bytearray agb where agb.DEPLOYMENT_ID_ in (select ard.ID_ from workflowserver.act_re_deployment ard where ard.ID_=(select arp.DEPLOYMENT_ID_ from workflowserver.act_re_procdef arp where arp.KEY_ in (select arm.KEY_ from workflowserver.act_re_model arm where arm.ID_=114965) order by arp.VERSION_ desc limit 1));
-- NAME: 判断是否可进入结算系统
-- select * from base.loan_product_rate where loan_product_id=215 and period=36 and FIND_IN_SET('d_1937', sales_store_ids)>0 and amount_loan_min <= 100000 and amount_loan_max >= 100000 and state=1