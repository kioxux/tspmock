-- ps：数据库锁表时间一般为50s。
-- 查看那些表锁到了
show OPEN TABLES where In_use > 0;
-- 查看进程号
show processlist;
select * from information_schema.innodb_trx;
select * from performance_schema.data_locks;
-- 删除进程
kill 445196;