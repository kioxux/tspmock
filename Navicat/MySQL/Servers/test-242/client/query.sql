-- 身份证号码相同姓名不同
select group_concat(order_number) as orderNumbers,id_card,json_object('client_name',group_concat(client_name)),count(*) from tclient_apply_basic where id_card='420101199909299050' and client_name is not null and client_name !='' and client_name !='王民赫' group by id_card;

-- 身份证号码相同居住地址不同
select group_concat(tab.order_number) as orderNumbers,tab.id_card,group_concat(tab.address) as address,group_concat(tab.living_address) as living_address,group_concat(tab.living_province) as living_province,group_concat(tab.living_city) as living_city,group_concat((select dist_name from base.district where id = tab.living_county)) as living_county,count(*) from (
select tb.order_number,tc.id_card,tc.address as address,null as living_address,null as living_province,null as living_city,null as living_county
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' and tc.address is not null and tc.address != ''
where tc.address !='湖北省武汉市汉阳区城隍大道622号南山花园'
union
select tb.order_number,tc.id_card,null as address,tad.living_address,null as living_province,null as living_city,null as living_county 
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_address is not null and tad.living_address != ''
where tad.living_address !='湖北省武汉市汉阳区城隍大道622号南山花园'
union
select tb.order_number,tc.id_card,null as address,null as living_address,tad.living_province,null as living_city,null as living_county
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_province is not null and tad.living_province != ''
where tad.living_province !='420000'
union
select tb.order_number,tc.id_card,null as address,null as living_address,null as living_province,tad.living_city as living_city,null as living_county
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_city is not null and tad.living_city != ''
where tad.living_city !='420100'
union
select tb.order_number,tc.id_card,null as address,null as living_address,null as living_province,null as living_city,tad.living_county as living_county 
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_county is not null and tad.living_county != ''
where tad.living_county !='420105') tab group by tab.id_card;

-- 身份证号码相同单位地址不同
select group_concat(tab.order_number) as orderNumbers,tab.id_card,group_concat(tab.company_address) as company_address,group_concat(tab.company_privince) as company_privince,group_concat(tab.company_city) as company_city,group_concat(tab.company_area) as company_area,count(*) from (
select tb.order_number,tc.id_card,taw.company_address,null as company_privince,null as company_city,null as company_area
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_address is not null and taw.company_address != ''
where taw.company_address !='湖北省武汉市汉阳区城隍大道150号'
union
select tb.order_number,tc.id_card,null as company_address,taw.company_privince,null as company_city,null as company_area
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_privince is not null and taw.company_privince != ''
where taw.company_privince !='420000'
union
select tb.order_number,tc.id_card,null as company_address,null as company_privince,taw.company_city,null as company_area
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_city is not null and taw.company_city != ''
where taw.company_city !='420100'
union
select tb.order_number,tc.id_card,null as company_address,null as company_privince,null as company_city,taw.company_area
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_area is not null and taw.company_area != ''
where taw.company_area !='420105') tab group by tab.id_card;

-- 半年内身份证号码相同手机号码不同
select group_concat(tab.order_number) as orderNumbers,tab.id_card,group_concat(tab.b_mobile) as b_mobile,group_concat(tab.c_mobile) as c_mobile,group_concat(tab.living_phone) as living_phone,count(*) from (
select order_number,id_card,mobile as b_mobile,null as c_mobile,null as living_phone from tclient_apply_basic where id_card='420101199909299050' and create_time >= date_sub(curdate(), interval 6 month) and mobile is not null and mobile !='' and mobile != '15792015263'
union
select tb.order_number,tc.id_card,null as b_mobile,tc.mobile as c_mobile,null as living_phone from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' and tc.create_time >= date_sub(curdate(), interval 6 month) and tc.mobile is not null and tc.mobile !=''
where tc.mobile != '15792015263'
union
select tb.order_number,tc.id_card,null as b_mobile,null as c_mobile,tad.living_phone from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card='420101199909299050' 
left join tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.create_time >= date_sub(curdate(), interval 6 month) and tad.living_phone is not null and tad.living_phone !=''
where tad.living_phone != '15792015263') tab group by tab.id_card;

-- 同一客户联系人相同号码不同
select tb.order_number,tac.name,group_concat(tac.mobile) as mobile from tclient_apply_contacts tac 
join tclient_apply_basic tb on tac.order_id=tb.id and tb.order_number ='DD2023091307235008' group by tac.name having count(distinct tac.mobile) > 1;

-- 同一客户联系人手机号码相同联系人不同
select tb.order_number,tac.mobile,group_concat(tac.name) as name from tclient_apply_contacts tac 
join tclient_apply_basic tb on tac.order_id=tb.id and tb.order_number ='DD2023091307235008' group by tac.mobile having count(distinct tac.name) > 1;

-- 申请人姓名与联系人姓名不同手机号码相同
select tb.order_number,tac.mobile,group_concat(tac.name) as name from tclient_apply_contacts tac 
join tclient_apply_basic tb on tac.order_id=tb.id and tb.order_number ='DD2023091307235008' where tac.name != tb.client_name and tac.mobile=tb.mobile group by tac.mobile;

-- 申请人与其他申请件联系人姓名相同
select group_concat(tb.order_number) as orderNumbers,tac.name as name from tclient_apply_contacts tac 
join tclient_apply_basic tb on tac.order_id=tb.id where tac.name = '王民赫' and tb.order_number !='DD2023091307235008' group by tac.name;

-- 申请人与其他申请件联系人电话相同
select group_concat(tb.order_number) as orderNumbers,tac.mobile as mobile from tclient_apply_contacts tac 
join tclient_apply_basic tb on tac.order_id=tb.id where tac.mobile = '15792015263' and tb.order_number !='DD2023091307235008' group by tac.mobile;

-- 不同申请件单位名称相同
select group_concat(tb.order_number) as orderNumbers,taw.org
from tclient_apply_cust tc 
join tclient_apply_basic tb on tc.order_id=tb.id
left join tclient_apply_cust_work taw on taw.cust_id=tc.id 
where tb.order_number !='DD2023091307235008' and taw.org ='武汉市光华集团' group by taw.org;