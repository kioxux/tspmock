-- 进件成功
-- NAME: tclient_apply_basic 订单基础表
select * from client.tclient_apply_basic where order_number in ('DD2025022807245117');
-- NAME: tclient_apply_cost 融资费用信息表
select * from client.tclient_apply_cost where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_bank 银行卡信息表
select * from client.tclient_apply_bank where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_cust 客户信息表
select * from client.tclient_apply_cust where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_company 企业用户信息表
select * from client.tclient_apply_company where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_file 申请附件信息表
select * from client.tclient_apply_file where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117')) order by id;
-- NAME: app_message app信息通知表
select * from message.app_message where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_sign 申请签署表
select * from client.tclient_apply_sign where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117')) ORDER BY id desc;
-- NAME: tclient_order_product_relationship 订单产品对照表
select * from client.tclient_order_product_relationship where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_cust_address 客户信息-居住信息表
select * from client.tclient_apply_cust_address where cust_id in (select id from client.tclient_apply_cust where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117')));
-- NAME: tclient_apply_car 车辆信息表
select * from client.tclient_apply_car where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_contacts 客户信息-联系人表
select * from client.tclient_apply_contacts where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));
-- NAME: tclient_apply_cust_work 客户信息-职场信息表
select * from client.tclient_apply_cust_work where cust_id in (select id from client.tclient_apply_cust where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117')));
-- 仲利推送相关的
-- NAME1: tclient_apply_zhongli_file 仲利推送附件信息表
-- select * from client.tclient_apply_zhongli_file where order_number in ('DD2025022807245117');
-- NAME1: tclient_apply_zhongli_data 仲利推送数据表
-- select * from client.tclient_apply_zhongli_data where order_number in ('DD2025022807245117');
-- 提交后风控相关的
-- NAME: client_basic 客户基本信息表
select * from client.client_basic where client_code in ('DD2025022807245117');
-- NAME: client_credit 授信表
select * from client.client_credit where credit_code in ('DD2025022807245117');
-- NAME: client_project 客户项目表
select * from base.client_project where project_code in ('DD2025022807245117');
-- NAME: task_list 待办任务列表
select * from workflow.task_list where project_id = (select project_id from base.client_project where project_code in ('DD2025022807245117')) order by create_time,task_id;
-- NAME: tclient_apply_order 车辆借款订单表
select * from client.tclient_apply_order where order_number in ('DD2025022807245117');
-- NAME: tclient_audit_log 资方审批记录表
select * from client.tclient_audit_log where order_id in (select id from client.tclient_apply_basic where order_number in ('DD2025022807245117'));