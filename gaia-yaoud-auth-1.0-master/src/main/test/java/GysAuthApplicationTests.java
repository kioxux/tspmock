import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.GysAuthApplication;
import com.gys.business.mapper.GaiaWfDefineProcessMapper;
import com.gys.business.mapper.entity.ClientParam;
import com.gys.business.mapper.entity.SdParam;
import com.gys.business.service.ClientParamService;
import com.gys.business.service.DepartmentService;
import com.gys.business.service.LoginService;
import com.gys.business.service.MemberService;
import com.gys.business.service.data.GetResourceOutData;
import com.gys.business.service.data.GetWorkflowInData;
import com.gys.business.service.data.GetWorkflowOutData;
import com.gys.business.service.data.MemberTypeOutData;
import com.gys.business.service.data.dep.dto.DepDto;
import com.gys.business.service.data.dep.vo.DepVo;
import com.gys.business.service.impl.WorkflowServiceImpl;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.redis.RedisManager;
import com.gys.util.UtilConst;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={GysAuthApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GysAuthApplicationTests {
    @Autowired
    private GaiaWfDefineProcessMapper wfDefineProcessMapper;
    @Autowired
    private WorkflowServiceImpl workflowService;
    @Autowired
    private LoginService loginService;
    @Autowired
    protected RedisManager redisManager;
    @Autowired
    private ClientParamService clientParamService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private DepartmentService departmentService;

    @Test
    public void test1(){
        List list= wfDefineProcessMapper.selectDefineApproveList("21020006","GAIA_WF_007");
        System.out.println(list);

        GetWorkflowInData getWorkflowInData=new GetWorkflowInData();
        getWorkflowInData.setClient("21020006");
        getWorkflowInData.setUserId("10000046");
        getWorkflowInData.setWfCode("2009010001");
        GetWorkflowOutData getWorkflowOutData= workflowService.selectOne(getWorkflowInData);
        System.out.println(getWorkflowOutData);
    }

    @Test
    public void testgetResByUser(){
        GetLoginOutData outData=new GetLoginOutData();
        outData.setClient("21020001");
        outData.setUserId("10000005");
        List<GetResourceOutData> list= loginService.getResByUser(outData);
        System.out.println(list);
    }

    @Test
    public void testJson(){
        String token01="ifeiwknciw001";
        this.redisManager.set(token01, "QQ001", UtilConst.TOKEN_EXPIRE);



        String token="eyJhbGciOiJIUzI1NiJ9.eyJjbGllbnQiOiIyMTAyMDAwNiIsInVzZXJJZCI6IjEwMDAwMDY3IiwiaWF0IjoxNjAwODQzNjU2LCJwbGF0Zm9ybSI6IjEifQ.8zv_wRDqVkdzGc5woogo-HxoE_b9BTCQMwV-gwCwixs";
        String userInfo = (String)this.redisManager.get(token);
        JSONObject jsonObject = JSON.parseObject(userInfo);
        GetLoginOutData data = JSONObject.toJavaObject(jsonObject, GetLoginOutData.class);
        System.out.println(data);
    }

    @Test
    public void testMemberConfig(){
        String client="10000003";
        ClientParam config = clientParamService.getConfig(client);
        System.out.println(config);
    }

    @Test
    public void testMemberClass(){
        String client="10000005";
        GetLoginOutData data=new GetLoginOutData();
        data.setClient(client);
        List<MemberTypeOutData> memberClass = memberService.getMemberClass(data);
        System.out.println(memberClass);
    }

    @Test
    public void testreceivePriceConfig(){
        String client="10000016";
        String depId="10001";
        SdParam config = clientParamService.getReceivePriceConfig(client, depId);
        System.out.println(config);
    }

    @Test
    public void testDep(){
        String client="10000003";
        DepDto depDto=new DepDto();
        depDto.setClient(client);
        List<DepVo> allDeptList = departmentService.getAllDeptList(depDto);
        System.out.println(allDeptList);
    }


    @Test
    public void test3(){
           // "wfCode": "2112200002"
        GetWorkflowInData data=new GetWorkflowInData();
        data.setWfCode("2112210005");
        data.setClient("10000003");
        GetWorkflowOutData getWorkflowOutData = workflowService.selectOne(data);

        System.out.println(getWorkflowOutData);
    }
}
