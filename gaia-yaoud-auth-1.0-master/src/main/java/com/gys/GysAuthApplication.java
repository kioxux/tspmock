package com.gys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import tk.mybatis.spring.annotation.MapperScan;

@EnableAspectJAutoProxy(proxyTargetClass = true)
@SpringBootApplication
@MapperScan({"com.gys.business.mapper"})
@EnableCaching
@EnableEurekaClient
@EnableFeignClients
public class GysAuthApplication {
   public static void main(String[] args) {
      SpringApplication.run(GysAuthApplication.class, args);
   }
}
