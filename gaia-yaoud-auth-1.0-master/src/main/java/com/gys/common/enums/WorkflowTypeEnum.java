package com.gys.common.enums;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/28 10:57
 */
public enum WorkflowTypeEnum {

    GAIA_WF_032("GAIA_WF_032", "供应商付款申请"),
    GAIA_WF_043("GAIA_WF_043", "供应商预付账款申请"),
    GAIA_WF_045("GAIA_WF_045", ""),;

    public final String code;
    public final String name;

    WorkflowTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
