package com.gys.common.enums;

/**
 * @desc: 加盟商参数枚举类
 * @author: Ryan
 * @createTime: 2021/11/2 10:55
 */
public enum ClientParmaEnum {

    WEB_STORE_SELECT_LIMIT("WEB_STORE_SELECT_LIMIT", "0", "", "是否禁止登录账号切换门店", "0-否；1-是"),;

    /**键值ID*/
    public final String id;
    /**参数1*/
    public final String param1;
    /**参数2*/
    public final String param2;
    /**参数名称*/
    public final String name;
    /**参数备注*/
    public final String remark;

    ClientParmaEnum(String id, String param1, String param2, String name, String remark) {
        this.id = id;
        this.param1 = param1;
        this.param2 = param2;
        this.name = name;
        this.remark = remark;
    }
}
