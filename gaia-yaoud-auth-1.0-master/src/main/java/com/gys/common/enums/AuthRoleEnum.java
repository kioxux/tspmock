package com.gys.common.enums;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/25 13:51
 */
public enum AuthRoleEnum {

    COMPANY_MASTER("GAIA_AL_MANAGER", "公司负责人【查看|推送|淘汰|补货】", "APP_OUT_STOCK_MASTER_PERMISSION", "1|1|1|1"),
    SCZG("GAIA_MM_SCZG", "商采主管【查看|推送|淘汰|补货】", "APP_OUT_STOCK_SCZG_PERMISSION", "1|1|1|1"),
    STORE_MASTER("SD_01", "门店店长【查看|推送|淘汰|补货】", "APP_OUT_STOCK_SD01_PERMISSION", "1|1|1|1");

    public final String type;
    public final String name;
    /**系统参数-权限名称*/
    public final String permissionCode;
    /**系统参数-初始权限*/
    public final String permissionValue;

    AuthRoleEnum(String type, String name, String permissionCode, String permissionValue) {
        this.type = type;
        this.name = name;
        this.permissionCode = permissionCode;
        this.permissionValue = permissionValue;
    }
}
