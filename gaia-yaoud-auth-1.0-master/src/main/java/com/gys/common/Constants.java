package com.gys.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/12/2218:22
 */
public interface Constants {

    String BASIC_ROLE = "1";

    String CUSTOM_ROLE = "2";

    String NOT_DEL_FLAG = "N";

    /**
     * 系统模块参数
     */
    String SYSTEM_PARAM_MODEL = "MENU_MODEL";

    /**
     * 类型（1 模块 2 页面 3 按钮 4 列表项）
     */
    Map<String, String> AUTH_TYPE_MAP = new HashMap<String, String>() {{
        put("1", "模块");
        put("2", "页面");
        put("3", "按钮");
        put("4", "列表项");
    }};

}
