package com.gys.common.exception;

import com.gys.common.data.JsonResult;
import com.gys.util.Util;
import com.gys.util.UtilConst;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@ResponseBody
public class ExceptionAdvice {
   private static final Logger log = LoggerFactory.getLogger(ExceptionAdvice.class);

   @ExceptionHandler({RuntimeException.class})
   public JsonResult handleException(HttpServletRequest request, Exception e) {
      log.info(Util.getExceptionInfo(e));
      return JsonResult.error();
   }

   @ExceptionHandler({BusinessException.class})
   public JsonResult handleException(BusinessException e) {
      return JsonResult.fail(UtilConst.CODE_1001, e.getMessage());
   }
}
