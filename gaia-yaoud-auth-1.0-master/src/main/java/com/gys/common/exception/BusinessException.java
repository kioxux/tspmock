package com.gys.common.exception;

public class BusinessException extends RuntimeException {
   public BusinessException(String message) {
      super(message);
   }
}
