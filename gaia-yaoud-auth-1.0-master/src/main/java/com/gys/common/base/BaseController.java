package com.gys.common.base;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.UserLoginModelClient;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.redis.RedisManager;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseController {
   public static String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALiz8gxsQQ1at9X7fSr93F+u7QezKxdtBWLzIm68+OdFc7yKFhCk3SJd7XeLiqs5Z49d9Moei7Oik4jE4CbjLm1+LfrSks90pzfNwQVmEcLCxKOC9+1dNwTpPhijdpjoVzBc+ewji0q4h+i16tWIzEMCqsam3UjChHWaOzT0l00rAgMBAAECgYBcZG8s9a3ozztsh9vJ+KkXF/qrwl1I35k1MShFOrRLg/tsEasN8lpHybJy/VOFaTa7iaVcwcDiP+4LGWiDK1taQo2axpeb9UU2yB9YScBB5U1NFRelPdy3OlbrsOH9kH2UwewRQQ9v8HcVDI1A1g9Fjy3bf8T9Ez+tt+ISBJUkAQJBAN46dJY8FGSh/P4el6iFdr311mbtP1iDvCBCWBY9vpL6DeUI4ZKElCZNwaJvz2+/NVYCxm9vAACXhYRgYHgomysCQQDUxZr1hEKgX/gSyE57BQCUh5CwVdiKs6qPajOMI53tewR4c1TyDfW5BZA15GL5ioe7c28FMTI6F1kual/cyxYBAkBrGr1KExa5EaF/UHtDi1+ZWjzFSp73KDapTZBq18NbzwyXUFp3ZVKJy3wBr+XIM92e4KOG3Xag0mZVUKXuA5QVAkEAs8EJzBfjzeemBgAxxV6RgeoRw9eSrINjOewoBmHkv0fpm/Ubs681hA1+Kvl64IU3bJb2GTXZ0Bf/oOX/fOIaAQJBAIuZsGOXiEZ1SONzEqkJh8NBIr1B64M9hNTMffPEnZ0HtG9/Rr4Axc/SNamiHV/seQj5p3Bw0to4tZFAjznNvOI=";

   @Autowired
   protected RedisManager redisManager;

   @InitBinder
   public void initBinder(ServletRequestDataBinder binder) {
      binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
   }

   public GetLoginOutData getLoginUser(HttpServletRequest request) {
      String token = request.getHeader("X-Token");
      String userInfo = (String) this.redisManager.get(token);
      // 解析失败 PC登录
      if (getToken(token) == null) {
         JSONObject jsonObject = JSON.parseObject(userInfo);
         GetLoginOutData data = JSONObject.toJavaObject(jsonObject, GetLoginOutData.class);
         return data;
      } else {
         GetLoginOutData getLoginOutData = new GetLoginOutData();
         JSONObject jsonObject = JSON.parseObject(userInfo);
         UserLoginModelClient data = JSONObject.toJavaObject(jsonObject, UserLoginModelClient.class);
         getLoginOutData.setClient(data.getClient());
         getLoginOutData.setUserId(data.getUserId());
         return getLoginOutData;
      }
   }

   public GetLoginOutData getLoginUser(String token) {
      String userInfo = (String)this.redisManager.get(token);
      JSONObject jsonObject = JSON.parseObject(userInfo);
      GetLoginOutData data = JSONObject.toJavaObject(jsonObject, GetLoginOutData.class);
      return data;
   }

   protected void exportExcel(HttpServletResponse response, List<?> list, String[] header, ExportExcelInterface export) throws Exception {
      Date dt = new Date();
      String fileName = (new Long(dt.getTime())).toString();
      response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
      ServletOutputStream out = response.getOutputStream();

      try {
         export.exportExcel(header, list, out, "yyyy-MM-dd");
         out.flush();
      } catch (Exception var12) {
         throw var12;
      } finally {
         out.close();
      }

   }

   /**
    * 获取token
    *
    * @return
    */
   public Claims getToken(String token) {
      try {
         return Jwts.parser()
                 .setSigningKey(DatatypeConverter.parseBase64Binary(privateKey))
                 .parseClaimsJws(token).getBody();
      } catch (Exception ex) {
         return null;
      }
   }

   private String getCellValue(Cell cell) {
      String firstCell = "";
      if (cell == null) {
         firstCell = "";
      } else if (cell.getCellType() == 0) {
         if (HSSFDateUtil.isCellDateFormatted(cell)) {
            SimpleDateFormat sdf = null;
            if (cell.getCellStyle().getDataFormat() == HSSFDataFormat.getBuiltinFormat("h:mm")) {
               sdf = new SimpleDateFormat("HH:mm");
            } else {
               sdf = new SimpleDateFormat("yyyy-MM-dd");
            }

            Date date = cell.getDateCellValue();
            firstCell = sdf.format(date);
         } else {
            DecimalFormat df = new DecimalFormat("0");
            firstCell = df.format(cell.getNumericCellValue());
         }
      } else if (cell.getCellType() == 1) {
         firstCell = cell.getStringCellValue();
      }

      return firstCell;
   }

   public <T> List<T> importExcel(MultipartFile uploadExcel, Class<T> t, Integer sheetNo, Integer firstRowNum) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
      Workbook workbook = null;
      String fileName = uploadExcel.getOriginalFilename();
      String fileEnd = fileName.substring(fileName.indexOf(".") + 1);
      Sheet sheet = null;
      if ("xls".equals(fileEnd)) {
         workbook = new HSSFWorkbook(uploadExcel.getInputStream());
         sheet = (HSSFSheet)workbook.getSheetAt(sheetNo);
      } else {
         workbook = new XSSFWorkbook(uploadExcel.getInputStream());
         sheet = (XSSFSheet)workbook.getSheetAt(sheetNo);
      }

      List<T> list = new ArrayList();

      for(int i = firstRowNum; i <= ((Sheet)sheet).getLastRowNum(); ++i) {
         Row row = ((Sheet)sheet).getRow(i);
         if (row != null) {
            T instance = t.newInstance();
            Field[] fields = instance.getClass().getDeclaredFields();
            int count = 0;
            int length = fields.length;

            for(int k = 0; k < length; ++k) {
               Method setMethod = instance.getClass().getMethod("set" + StringUtils.capitalize(fields[k].getName()), String.class);
               if (k == length - 1) {
                  String index = i + 1 + "";
                  setMethod.invoke(instance, index);
               } else {
                  Cell cell = row.getCell(k);
                  if (cell == null) {
                     setMethod.invoke(instance, "");
                     ++count;
                  } else {
                     cell.setCellType(CellType.STRING);
                     if (StrUtil.isBlank(cell.getStringCellValue())) {
                        ++count;
                     }

                     setMethod.invoke(instance, cell.getStringCellValue());
                  }
               }
            }

            if (count != length - 1) {
               list.add(instance);
            }
         }
      }

      return list;
   }
}
