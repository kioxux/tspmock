package com.gys.common.interceptor;

import com.gys.common.redis.RedisManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class SecurityInterceptor implements HandlerInterceptor {
   @Autowired
   private RedisManager redisManager;

   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
      return true;
   }

   public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception exception) throws Exception {
   }

   public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView) throws Exception {
   }
}
