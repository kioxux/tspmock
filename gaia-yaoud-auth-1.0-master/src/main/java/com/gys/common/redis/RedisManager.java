package com.gys.common.redis;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class RedisManager {
   @Autowired
   private RedisTemplate redisTemplate;

   public void set(String key, Object value) {
      ValueOperations<String, Object> operation = this.redisTemplate.opsForValue();
      operation.set(key, value);
   }

   public void set(String key, Object value, Long second) {
      ValueOperations<String, Object> operation = this.redisTemplate.opsForValue();
      operation.set(key, value, second, TimeUnit.SECONDS);
   }

   public Object get(String key) {
      ValueOperations<String, Object> operation = this.redisTemplate.opsForValue();
      return operation.get(key);
   }

   public void setHash(String key, Map map) {
      HashOperations operation = this.redisTemplate.opsForHash();
      operation.putAll(key, map);
   }

   public void setHash(String key, Map map, Long hour) {
      HashOperations operation = this.redisTemplate.opsForHash();
      operation.putAll(key, map);
      this.redisTemplate.expire(key, hour, TimeUnit.HOURS);
   }

   public Object getHash(String key) {
      HashOperations operation = this.redisTemplate.opsForHash();
      return operation.entries(key);
   }

   public void delete(String key) {
      this.redisTemplate.delete(key);
   }

   public Boolean hasKey(String key) {
      return this.redisTemplate.hasKey(key);
   }

   /**
    * spring-data-redis 2.1+ 后才有 setIfAbsent（key,value,seconds,timeunit）方法
    * 此处临时使用 set +expire 命令替代：存在一定风险
    * @param key key
    * @param value value
    * @param second 秒
    */
   public Boolean setNx(String key, Object value, long second) {
      Boolean flag = redisTemplate.opsForValue().setIfAbsent(key, value);
      if (Boolean.TRUE.equals(flag)) {
         redisTemplate.expire(key, second, TimeUnit.SECONDS);
      }
      return flag;
   }
}
