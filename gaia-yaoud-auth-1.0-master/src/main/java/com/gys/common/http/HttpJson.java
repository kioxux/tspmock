package com.gys.common.http;

import com.alibaba.fastjson.JSONObject;

public class HttpJson {
   private boolean success = false;
   private String msg = "";
   private String code = "";
   private JSONObject data = null;

   public boolean isSuccess() {
      return this.success;
   }

   public void setSuccess(boolean success) {
      this.success = success;
   }

   public String getMsg() {
      return this.msg;
   }

   public void setMsg(String msg) {
      this.msg = msg;
   }

   public String getCode() {
      return this.code;
   }

   public void setCode(String code) {
      this.code = code;
   }

   public JSONObject getData() {
      return this.data;
   }

   public void setData(JSONObject data) {
      this.data = data;
   }
}
