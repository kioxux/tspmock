package com.gys.common.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.util.UtilConst;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpClientUtil {
   private static String TOKEN = "e7666fea-a017-4cb8-b6dc-7a510dc3f881";

   public static HttpJson doGet(String url) {
      HttpJson httpJson = new HttpJson();
      CloseableHttpClient httpclient = HttpClients.createDefault();

      try {
         HttpGet httpget = new HttpGet(url);
         httpget.setHeader("Authorization", TOKEN);
         HttpResponse response = httpclient.execute(httpget);
         int statusCode = response.getStatusLine().getStatusCode();
         if (statusCode == 200) {
            String conResult = EntityUtils.toString(response.getEntity());
            JSONObject returnValue = JSON.parseObject(conResult);
            httpJson.setSuccess(true);
            httpJson.setCode(UtilConst.CODE_0.toString());
            httpJson.setData(returnValue.getJSONObject("result"));
            httpJson.setMsg(returnValue.getString("reason"));
         } else {
            httpJson.setMsg("接口请求未响应！;接口地址:" + url);
         }
      } catch (Exception var8) {
         httpJson.setMsg("接口请求出错！");
      }

      return httpJson;
   }
}
