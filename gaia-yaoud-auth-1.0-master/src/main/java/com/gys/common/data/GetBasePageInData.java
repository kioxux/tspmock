package com.gys.common.data;

import lombok.Data;

import java.beans.ConstructorProperties;
import java.io.Serializable;
@Data
public class GetBasePageInData implements Serializable {
   private static final long serialVersionUID = 7401087426616024367L;
   public int pageSize;
   private int pageNum;
   private String createTime;
   private String updateTime;
   private String loginAccount;

   public GetBasePageInData() {
   }

   @ConstructorProperties({"pageSize", "pageNum", "createTime", "updateTime", "loginAccount"})
   public GetBasePageInData(final int pageSize, final int pageNum, final String createTime, final String updateTime, final String loginAccount) {
      this.pageSize = pageSize;
      this.pageNum = pageNum;
      this.createTime = createTime;
      this.updateTime = updateTime;
      this.loginAccount = loginAccount;
   }
}
