package com.gys.common.data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/25 10:38
 */
@Data
public class AppPermissionVO {
    /**
     * 缺断货权限列表
     */
    private List<StockPermission> stockPermissions;

    public AppPermissionVO() {
        this.stockPermissions = new ArrayList<>();
    }

    /**
     * 缺断货权限实体
     */
    @Data
    public static class StockPermission {
        /**角色编码*/
        private String roleCode;
        /**角色名称*/
        private String roleName;
        /**权限对应站点*/
        private String authSite;
        /**查看权限*/
        private Integer viewFlag;
        /**删除权限*/
        private Integer deleteFlag;
        /**推送权限*/
        private Integer pushFlag;
        /**补货权限*/
        private Integer replenishFlag;

        public StockPermission() {
            this.roleCode = "";
            this.roleName = "";
            this.viewFlag = 0;
            this.pushFlag = 0;
            this.deleteFlag = 0;
            this.replenishFlag = 0;
        }

    }
}
