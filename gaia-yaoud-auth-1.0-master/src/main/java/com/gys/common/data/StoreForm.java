package com.gys.common.data;

import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/4 16:36
 */
@Data
public class StoreForm {
    private String client;
    private String userId;
    private String storeCode;
}
