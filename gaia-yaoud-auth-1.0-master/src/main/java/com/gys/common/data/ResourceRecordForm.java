package com.gys.common.data;

import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/16 13:14
 */
@Data
public class ResourceRecordForm {
    private String client;
    private String userId;
    private String pageId;
    private String pagePath;
    private String userName;
}
