package com.gys.common.data;

import lombok.Data;

@Data
public class SelectModel {
    private String key;
    private String value;
    private String type;
}
