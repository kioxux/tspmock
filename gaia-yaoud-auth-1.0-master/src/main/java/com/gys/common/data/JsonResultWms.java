package com.gys.common.data;

import lombok.Data;

@Data
public class JsonResultWms {
   private boolean success;
   private Integer code;
   private Object data;
   private String msg;

   public boolean isSuccess() {
      return this.success;
   }

   public Integer getCode() {
      return this.code;
   }

   public Object getData() {
      return this.data;
   }

   public String getMsg() {
      return this.msg;
   }

   public void setSuccess(final boolean success) {
      this.success = success;
   }

   public void setCode(final Integer code) {
      this.code = code;
   }

   public void setData(final Object data) {
      this.data = data;
   }

   public void setMsg(final String msg) {
      this.msg = msg;
   }

}
