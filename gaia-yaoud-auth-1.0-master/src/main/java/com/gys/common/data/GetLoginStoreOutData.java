package com.gys.common.data;

import lombok.Data;

@Data
public class GetLoginStoreOutData {
   private String client;
   private String userId;
   private String stoCode;
   private String stoName;
   private String stoYbcode;
}
