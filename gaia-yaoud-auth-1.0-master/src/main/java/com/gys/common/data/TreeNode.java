package com.gys.common.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 树结构
 * @date 2021/12/2218:08
 */
@Data
public class TreeNode implements Serializable {

    @ApiModelProperty("子节点")
    private String id;

    @ApiModelProperty("父节点")
    private String pId;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("标签")
    private String label;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("是否选中")
    private Boolean choose;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("path")
    private String path;

    @ApiModelProperty("pagePath")
    private String pagePath;

    @ApiModelProperty("requestPath")
    private String requestPath;

    @ApiModelProperty("icon")
    private String icon;

    @ApiModelProperty("buttonType")
    private String buttonType;

    @ApiModelProperty("类型（1模块 2页面 3按钮 4列表项）")
    private String authType;

    @ApiModelProperty("子节点")
    private List<TreeNode> child = new ArrayList<>();

}
