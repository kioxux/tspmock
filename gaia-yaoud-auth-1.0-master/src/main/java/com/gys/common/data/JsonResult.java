package com.gys.common.data;

import com.gys.util.UtilConst;
import lombok.Data;

import java.io.Serializable;
@Data
public class JsonResult implements Serializable {
   private static final long serialVersionUID = 1L;
   private static final String ERROR_MESSAGE = "服务器出现了一些小状况，正在修复中！";
   private Integer code;
   private Object data;
   private String message;

   public static JsonResult success(Object data, String desc) {
      return new JsonResult(UtilConst.CODE_0, data, desc);
   }

   public static JsonResult fail(Integer code, String desc) {
      return new JsonResult(code, null, desc);
   }

   public static JsonResult process(Integer code, Object data, String desc) {
      return new JsonResult(code, data, desc);
   }

   public static JsonResult error() {
      return new JsonResult(UtilConst.CODE_500, (Object)null, "服务器出现了一些小状况，正在修复中！");
   }

   public static JsonResult success(Object data) {
      return new JsonResult(UtilConst.CODE_0, data, "成功");
   }

   public static JsonResult success() {
      return success(null, "成功");
   }


   public JsonResult() {
   }

   public JsonResult(Integer code, Object data, String message) {
      this.data = data;
      this.message = message;
      this.code = code;
   }
}
