package com.gys.common.data;

import lombok.Data;

@Data
public class QRCodeModel {
    private String qrCode;
    private String model;
    private String token;
    private int statusCode;
    private GetLoginOutData loginData;
}
