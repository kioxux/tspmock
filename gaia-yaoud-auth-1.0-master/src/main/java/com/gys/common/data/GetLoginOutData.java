package com.gys.common.data;

import lombok.Data;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.List;
@Data
public class GetLoginOutData implements Serializable {
   private static final long serialVersionUID = 1L;
   private String userId;
   private String loginAccount;
   private String client;
   private String loginName;
   private String userSex;
   private String userTel;
   private String userIdc;
   private String userYsId;
   private String userYsZyfw;
   private String userYsZylb;
   private String depId;
   private String depName;
   private String userAddr;
   private String userSta;
   private String userLoginSta;
   private String stoAttribute;
   private String stoDeliveryMode;
   private String token;
   private String password;
   private String userSoleSaleNo;
   private List<GetLoginFrancOutData> franchiseeList;
   private List<GetLoginStoreOutData> storeList;
   /**登录账号是否禁止切换门店：0-否 1-是*/
   private Integer storeSelectLimit;
   /**是否后台人员：0-否 1-是*/
   private Integer adminFlag;
   /**加盟商名称*/
   private String clientName;
   /**总部名称：加盟商名称或部门名称*/
   private String showName;

   private String tokenUUID;

   @ConstructorProperties({"userId", "loginAccount", "client", "loginName", "userSex", "userTel", "userIdc", "userYsId", "userYsZyfw", "userYsZylb", "depId", "depName", "userAddr", "userSta", "userLoginSta","stoAttribute", "token", "franchiseeList", "storeList"})
   public GetLoginOutData(final String userId, final String loginAccount, final String client, final String loginName, final String userSex, final String userTel, final String userIdc, final String userYsId, final String userYsZyfw, final String userYsZylb, final String depId, final String depName, final String userAddr, final String userSta, final String userLoginSta,final String stoAttribute,final String token, final List<GetLoginFrancOutData> franchiseeList, final List<GetLoginStoreOutData> storeList) {
      this.userId = userId;
      this.loginAccount = loginAccount;
      this.client = client;
      this.loginName = loginName;
      this.userSex = userSex;
      this.userTel = userTel;
      this.userIdc = userIdc;
      this.userYsId = userYsId;
      this.userYsZyfw = userYsZyfw;
      this.userYsZylb = userYsZylb;
      this.depId = depId;
      this.depName = depName;
      this.userAddr = userAddr;
      this.userSta = userSta;
      this.userLoginSta = userLoginSta;
      this.stoAttribute = stoAttribute;
      this.token = token;
      this.franchiseeList = franchiseeList;
      this.storeList = storeList;
   }

   public GetLoginOutData() {
   }
}
