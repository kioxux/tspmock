package com.gys.common.data;

import lombok.Data;

@Data
public class GetLoginFrancOutData {
   private String userId;
   private String userName;
   private String client;
   private String francName;
}
