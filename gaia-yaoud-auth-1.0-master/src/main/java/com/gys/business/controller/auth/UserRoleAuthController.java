package com.gys.business.controller.auth;

import com.gys.business.service.UserRoleAuthService;
import com.gys.business.service.data.auth.AuthConfWithUser;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午11:07
 */
@Api("用户角色权限分配")
@RestController
@RequestMapping({"/v2/userRoleAuth"})
public class UserRoleAuthController extends BaseController {

    @Resource
    private HttpServletRequest request;

    @Resource
    private UserRoleAuthService userRoleAuthService;

    @ApiOperation("根据加盟商和用户id查询权限内门店信息")
    @GetMapping({"/selectPermStoreAndRoleByUser"})
    public JsonResult selectPermStoreAndRoleByUser(String userId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return userRoleAuthService.selectPermStoreAndRoleByUser(userInfo.getClient(), userId);
    }

    @ApiOperation("查询角色权限树")
    @GetMapping({"/selectRolePermTree"})
    public JsonResult selectRolePermTree(String roleType, String roleId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return userRoleAuthService.selectRolePermTree(userInfo.getClient(), roleType, roleId);
    }

    @ApiOperation("查询门店下分配角色信息")
    @GetMapping({"/selectRoleInfoByStore"})
    public JsonResult selectRoleInfoByStore(String stoCode, String userId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return userRoleAuthService.selectRoleInfoByStore(userInfo.getClient(), stoCode, userId);
    }

    @ApiOperation("保存用户门店角色权限")
    @PostMapping({"/saveAuthConfWithUser"})
    public JsonResult saveAuthConfWithUser(@RequestBody AuthConfWithUser authConfWithUser) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        authConfWithUser.setClient(userInfo.getClient());
        authConfWithUser.setCreator(userInfo.getUserId());
        return userRoleAuthService.saveAuthConfWithUser(authConfWithUser);
    }

}

