package com.gys.business.controller.auth;

import com.gys.business.service.UserRestrictService;
import com.gys.business.service.data.auth.UserRestrictAddReq;
import com.gys.business.service.data.auth.UserRestrictListReq;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 用于分配用户数据权限
 * @author: flynn
 * @date: 2021年12月27日 下午1:59
 */
@RestController
@RequestMapping({"/v2/userRestrict"})
public class UserRestrictController extends BaseController {

    @Autowired
    private UserRestrictService userRestrictService;

//    @Autowired
//    private StoreService storeService;

    @PostMapping({"getStoreList"})
    public JsonResult getStoreList(HttpServletRequest request, @RequestBody UserRestrictListReq inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.userRestrictService.getStoreList(inData,userInfo), "提示：获取数据成功！");
    }

    /**
     * 初始化数据权限分配需要初始化数据
     *
     * @param request
     * @return
     */
    @GetMapping({"/init"})
    public JsonResult init(HttpServletRequest request, String userId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (userInfo == null) {
            throw new BusinessException("提示：请重新登陆！");
        }
        return JsonResult.success(this.userRestrictService.init(userInfo,userId), "提示：获取数据成功！");
    }


    @PostMapping({"/add"})
    public JsonResult addUserRestrict(HttpServletRequest request, @Validated @RequestBody UserRestrictAddReq inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.userRestrictService.addUserRestrict(userInfo, inData), "提示：获取数据成功！");
    }


}

