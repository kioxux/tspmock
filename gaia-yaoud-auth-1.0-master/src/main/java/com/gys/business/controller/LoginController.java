package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.LoginService;
import com.gys.business.service.StoreService;
import com.gys.business.service.data.GetLoginInData;
import com.gys.business.service.data.GetResourceOutData;
import com.gys.business.service.data.LoginQRCodeInData;
import com.gys.business.service.data.StoreOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.*;
import com.gys.util.Util;
import com.gys.util.UtilConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/"})
@Slf4j
public class LoginController extends BaseController {
    @Resource
    private LoginService loginService;

    @Resource
    private StoreService storeService;

    @Resource
    private GaiaUserDataMapper userDataMapper;

    @PostMapping({"login"})
    public JsonResult login(@RequestBody GetLoginInData inData) {
        GetLoginOutData outData = this.loginService.login(inData);
        return JsonResult.success(outData, "提示：登录成功！");
    }

    @PostMapping({"loginByPhone"})
    public JsonResult loginByPhone(@RequestBody GetLoginInData inData) {
        GetLoginOutData outData = this.loginService.loginByPhone(inData);
        return JsonResult.success(outData, "提示：登录成功！");
    }

    @PostMapping({"getResByUser"})
    public JsonResult getResByUser(HttpServletRequest request) {
        GetLoginOutData loginOutData = this.getLoginUser(request);
        String resKey = request.getHeader("X-Token") + loginOutData.getClient() + loginOutData.getUserId();
        String resValue = (String) redisManager.get(resKey);
        List<GetResourceOutData> outData = null;
        if (StringUtils.isEmpty(resValue)) {
            outData = this.loginService.getResByUser(loginOutData);
            redisManager.set(resKey, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);
        } else {
            outData = JSON.parseArray(resValue, GetResourceOutData.class);
        }
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"getSmsCode"})
    public JsonResult getSmsCode(@RequestBody GetLoginInData inData) {
        return JsonResult.success(this.loginService.getSmsCode(inData), "提示：验证码发送成功！");
    }

    @PostMapping({"selectFrancList"})
    public JsonResult selectFrancList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetLoginFrancOutData> list = this.loginService.selectFrancList(userInfo);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"chooseFranc"})
    public JsonResult chooseFranc(HttpServletRequest request, @RequestBody GetLoginInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.loginService.chooseFranc(inData, userInfo);
        return JsonResult.success(userInfo, "提示：保存成功！");
    }

    @PostMapping({"updatePassword"})
    public JsonResult updatePassword(HttpServletRequest request, @RequestBody GetLoginInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.loginService.updatePassword(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"operation/login"})
    public JsonResult operationLogin(@RequestBody GetLoginInData inData) {
        GetLoginOutData outData = this.loginService.operationLogin(inData);
        return JsonResult.success(outData, "提示：登录成功！");
    }

    @PostMapping("operation/FXRefreshToken")
    public JsonResult FXRefreshToken(HttpServletRequest request) {
        return this.loginService.FXRefreshToken(request);
    }

    @PostMapping("operation/FXRefreshTokenV2")
    public JsonResult FXRefreshTokenV2(HttpServletRequest request) {
        return this.loginService.FXRefreshToken(request);
    }

    @PostMapping({"operation/FXCacheLogin"})
    public JsonResult FXCacheLogin(@RequestBody GetLoginInData inData) {
        GetLoginOutData outData = this.loginService.FXCacheLogin(inData);
        return JsonResult.success(outData, "success");
    }

    @PostMapping({"operation/loginByPhone"})
    public JsonResult operationLoginByPhone(@RequestBody GetLoginInData inData) {
        GetLoginOutData outData = this.loginService.operationLoginByPhone(inData);
        return JsonResult.success(outData, "提示：登录成功！");
    }

    @PostMapping({"operation/getSmsCode"})
    public JsonResult operationSmsCode(@RequestBody GetLoginInData inData) {
        return JsonResult.success(this.loginService.operationSmsCode(inData), "提示：验证码发送成功！");
    }

    @PostMapping({"operation/getResByUser"})
    public JsonResult operationResByUser(HttpServletRequest request) {
        GetLoginOutData loginOutData = this.getLoginUser(request);
        List<GetResourceOutData> outData = this.loginService.operationResByUser(loginOutData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"getResByUserV2"})
    public JsonResult getResByUserV2(HttpServletRequest request) {
        GetLoginOutData loginOutData = this.getLoginUser(request);
        String resKey = request.getHeader("X-Token") + loginOutData.getClient() + loginOutData.getUserId();
        String resValue = (String) redisManager.get(resKey);
        resValue = "";
        loginOutData.setDepId("10000");
        List<TreeNode> outData = null;
        if (StringUtils.isEmpty(resValue)) {
            outData = this.loginService.getResByUserV2(loginOutData);
            redisManager.set(resKey, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);
        } else {
            outData = JSON.parseArray(resValue, TreeNode.class);
        }
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"operation/chooseStore"})
    public JsonResult operationChooseStore(HttpServletRequest request, @RequestBody GetLoginInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.loginService.operationChooseStore(inData, userInfo);
        return JsonResult.success(userInfo, "提示：保存成功！");
    }

    @PostMapping({"operation/chooseStoreWeb"})
    public JsonResult operationChooseStoreWeb(HttpServletRequest request, @RequestBody GetLoginInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.loginService.operationChooseStoreWeb(inData, userInfo), "提示：保存成功！");
    }

    @PostMapping({"operation/getLoginUser"})
    public JsonResult operationGetLoginUser(@RequestBody GetLoginOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(inData.getToken());
        return JsonResult.success(userInfo, "提示：获取数据成功！");
    }

    @PostMapping({"operation/selectAuthStoreList"})
    public JsonResult selectAuthStoreList(@RequestBody GetLoginOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(inData.getToken());
        List<StoreOutData> list = this.storeService.selectAuthStoreList(userInfo.getClient(), userInfo.getUserId());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"operation/selectDefaultAuthStore"})
    public JsonResult selectDefaultAuthStore(@RequestBody GetLoginOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(inData.getToken());
        StoreOutData list = this.storeService.selectDefaultAuthStore(userInfo.getClient(), userInfo.getUserId(), userInfo);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"operation/selectDefaultAuthStoreByToken"})
    public JsonResult selectDefaultAuthStoreByToken(@RequestBody GetLoginOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(inData.getToken());
        StoreOutData list = this.storeService.selectDefaultAuthStoreByToken(userInfo.getClient(), userInfo.getUserId(), userInfo);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"/qrcode/getQRCodeUUID"})
    public JsonResult getQRCodeUUID(HttpServletRequest request) {
        String qrCodeUUID = Util.generateUUID();
        QRCodeModel model = new QRCodeModel();
        model.setModel(QRCodeModelType.WAIT_SCAN_LOGIN);
        model.setLoginData(null);
        model.setQrCode(qrCodeUUID);
        redisManager.set(qrCodeUUID, JSON.toJSONString(model), UtilConst.PHONE_CODE_EXPIRE);
        return JsonResult.success(qrCodeUUID, "提示：获取数据成功！");
    }

    @PostMapping({"/qrcode/getQRCodeStatus"})
    public JsonResult getQRCodeStatus(HttpServletRequest request, @RequestBody QRCodeModel model) {
        String userInfo = (String)this.redisManager.get(model.getQrCode());
        QRCodeModel data = new QRCodeModel();
        if (StringUtils.isEmpty(userInfo)){
            data.setStatusCode(-1);
            return JsonResult.success(data, "提示：二维码已过期！");
        }

        JSONObject jsonObject = JSON.parseObject(userInfo);
        data = JSONObject.toJavaObject(jsonObject, QRCodeModel.class);
        Example example = new Example(GaiaUserData.class);
        GetLoginOutData loginOutData = data.getLoginData();
        if (StringUtils.isEmpty(loginOutData)){
            data.setStatusCode(-2);
            return JsonResult.success(data, "提示：未扫码");
        }
        example.createCriteria().andEqualTo("client", loginOutData.getClient()).andEqualTo("userId", loginOutData.getUserId()).andEqualTo("userTel", loginOutData.getUserTel());
        GaiaUserData user = this.userDataMapper.selectOneByExample(example);
        loginOutData.setPassword(user.getUserPassword());
        data.setLoginData(loginOutData);
        data.setStatusCode(0);
        return JsonResult.success(data, "提示：获取数据成功！");

    }

    @PostMapping({"/qrcode/loginByQRCode"})
    public JsonResult loginByQRCode(HttpServletRequest request, @RequestBody LoginQRCodeInData inData) {
        String userInfo = (String)this.redisManager.get(inData.getQrCode());
        int action = inData.getAction();
        String token = request.getHeader("X-Token");
        QRCodeModel model = new QRCodeModel();

        //取消登录
        if(action == 2){
            model.setStatusCode(-4);
            return JsonResult.success(model, "提示：取消登录");
        }

        if (StringUtils.isEmpty(userInfo)){
            model.setStatusCode(-1);
            return JsonResult.success(model, "提示：二维码已过期！");
        }

        GetLoginOutData loginOutData = super.getLoginUser(token);
        if(loginOutData == null){
            return JsonResult.fail(UtilConst.CODE_1001, "登录信息无效");
        }

        JSONObject jsonObject = JSON.parseObject(userInfo);
        model = JSONObject.toJavaObject(jsonObject, QRCodeModel.class);
        model.setToken(token);
        model.setLoginData(loginOutData);
        redisManager.set(inData.getQrCode(), JSON.toJSONString(model), UtilConst.PHONE_CODE_EXPIRE);
        return JsonResult.success(model, "提示：获取数据成功！");
    }

    @PostMapping({"/operation/copyToken"})
    public JsonResult copyToken(HttpServletRequest request,@RequestBody GetLoginOutData inData){
        GetLoginOutData loginOutData = this.getLoginUser(inData.getToken());
        String newToken = this.loginService.copyToken(loginOutData);
        return JsonResult.success(newToken, "提示：获取数据成功！");
    }

}
