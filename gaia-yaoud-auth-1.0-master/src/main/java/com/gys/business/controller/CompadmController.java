package com.gys.business.controller;

import com.gys.business.service.CompadmService;
import com.gys.business.service.data.CompadmInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/compadm/"})
public class CompadmController extends BaseController {
   @Autowired
   private CompadmService compadmService;

   @PostMapping({"getCompadmById"})
   public JsonResult getCompadmById(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.compadmService.getCompadmById(inData), "提示：获取数据成功！");
   }

   @PostMapping({"compadmInfoByClientId"})
   public JsonResult compadmInfoByClientId(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.compadmService.compadmInfoByClientId(inData), "提示：获取数据成功！");
   }

   @PostMapping({"updateCompadm"})
   public JsonResult updateDictionary(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.compadmService.updateCompadm(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"updateCompadmStatus"})
   public JsonResult updateCompadmStatus(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.compadmService.updateCompadmStatus(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"insertCompadm"})
   public JsonResult insertCompadm(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.compadmService.insertCompadm(inData, userInfo);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"getCompadmList"})
   public JsonResult getCompadmList(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.compadmService.getCompadmList(inData), "提示：获取数据成功！");
   }
   @PostMapping({"getCompadmAndCompadmWmsAndStore"})
   public JsonResult getCompadmAndCompadmWmsAndStore(HttpServletRequest request, @RequestBody CompadmInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.compadmService.getCompadmAndCompadmWmsAndStore(inData), "提示：获取数据成功！");
   }

}
