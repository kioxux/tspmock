package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.service.DcService;
import com.gys.business.service.data.DcIndata;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/dc/"})
public class DcController extends BaseController {
   @Autowired
   private DcService dcService;

   @PostMapping({"insertDc"})
   public JsonResult insertDc(HttpServletRequest request, @RequestBody DcIndata inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.dcService.insertDc(inData, userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"upDateDc"})
   public JsonResult upDateDc(HttpServletRequest request, @RequestBody DcIndata inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.dcService.upDateDc(inData, userInfo);
      return JsonResult.success("", "提示：获取数据成功！");
   }

   @PostMapping({"dcInfoByClientId"})
   public JsonResult dcInfoByClientId(HttpServletRequest request, @RequestBody DcIndata inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.dcService.dcInfoByClientId(inData), "提示：获取数据成功！");
   }

   @PostMapping({"wholesaleInfoByClientId"})
   public JsonResult wholesaleInfoByClientId(HttpServletRequest request, @RequestBody DcIndata inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.dcService.wholesaleInfoByClientId(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getDcById"})
   public JsonResult getDcById(HttpServletRequest request, @RequestBody GaiaDcData gaiaDcData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.dcService.getDcById(gaiaDcData, userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"dcDeptInfo"})
   public JsonResult dcDeptInfo(HttpServletRequest request, @RequestBody GaiaDepData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.dcService.dcDeptInfo(userInfo, inData), "提示：获取数据成功！");
   }
}
