package com.gys.business.controller;

import com.gys.business.service.ResourceService;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/role/"})
public class ResourceController extends BaseController {
   @Autowired
   private ResourceService resourceService;

   @PostMapping({"menu/select"})
   public JsonResult selectMenu() {
      return JsonResult.success(this.resourceService.selectMenu(), "提示：获取数据成功！");
   }
}
