package com.gys.business.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.GaiaWfRecordMapper;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.mapper.entity.GaiaWfRecord;
import com.gys.business.service.UserService;
import com.gys.business.service.WorkflowService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/workflow/"})
@Slf4j
public class WorkflowController extends BaseController {
    @Resource
    private WorkflowService workflowService;

    @Resource
    private UserService userService;

    @Resource
    private GaiaWfRecordMapper wfRecordMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    /**
     * 代办列表
     * @param request
     * @param inData
     * @return
     */
   @PostMapping({"/selectApprovingList"})
   public JsonResult selectApprovingList(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setUserId(userInfo.getUserId());
      inData.setClient(userInfo.getClient());
      log.info("selectApprovingList:{}", JSONObject.toJSONString(inData));
      PageInfo<GetWorkflowOutData> list = this.workflowService.selectApprovingList(inData);
      return JsonResult.success(list, "提示：获取数据成功！");
   }

    /**
     * 已办列表
     * @param request
     * @param inData
     * @return
     */
   @PostMapping({"/selectApprovedList"})
   public JsonResult selectApprovedList(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setUserId(userInfo.getUserId());
      inData.setClient(userInfo.getClient());
      log.info("selectApprovedList:{}", JSONObject.toJSONString(inData));
      PageInfo<GetWorkflowOutData> list = this.workflowService.selectApprovedList(inData);
      return JsonResult.success(list, "提示：获取数据成功！");
   }

    /**
     * 我的
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"/getMyApprovalList"})
    public JsonResult getMyApprovalList(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setUserId(userInfo.getUserId());
        inData.setClient(userInfo.getClient());
        log.info("getMyApprovalList:{}", JSONObject.toJSONString(inData));
        PageInfo<GetWorkflowOutData> list = this.workflowService.getMyApprovalList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    /**
     * 查询门店、部门下拉列表
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"/getStoDepList"})
    public JsonResult getStoDepList(HttpServletRequest request, @RequestBody GetStoDepInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setUserId(userInfo.getUserId());
        inData.setClient(userInfo.getClient());
        log.info("getStoDepList:{}", JSONObject.toJSONString(inData));
        return JsonResult.success(this.workflowService.getStoDepList(inData),"提示:获取数据成功");
    }

    /**
     * 查询加盟商下某首营客户的所有工作流记录
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"/getByClientAndTitle"})
    public JsonResult getByClientAndTitle(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
        String client = inData.getClient();
        if (client == null) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            inData.setClient(userInfo.getClient());
        }
        log.info("getByClientAndTitle:{}", JSONObject.toJSONString(inData));
        List<GetWorkflowOutData> list = this.workflowService.getByClientAndTitle(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    /**
     * 工作流详情
     * @param request
     * @param inData
     * @return
     */
   @PostMapping({"/selectOne"})
   public JsonResult selectOne(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setUserId(userInfo.getUserId());
      inData.setClient(userInfo.getClient());
      inData.setDepId(userInfo.getDepId());
      log.info("selectOne:{}", JSONObject.toJSONString(inData));
      GetWorkflowOutData outData = this.workflowService.selectOne(inData);
      return JsonResult.success(outData, "提示：获取数据成功！");
   }

    @PostMapping({"/selectDetailList"})
    public JsonResult selectDetailList(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setUserId(userInfo.getUserId());
        inData.setClient(userInfo.getClient());
        inData.setDepId(userInfo.getDepId());
        log.info("selectDetailList:{}", JSONObject.toJSONString(inData));
        List<GetWorkflowDetailListData> outData = this.workflowService.selectDetailList(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"/refreshWorkflow"})
    @Transactional
    public JsonResult refreshWorkflow(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
        Example example = new Example(GaiaWfRecord.class);
        List<String> list = Lists.newArrayList("GAIA_WF_035", "GAIA_WF_036", "GAIA_WF_037", "GAIA_WF_038");
        example.createCriteria().andIn("wfDefineCode", list);
        List<GaiaWfRecord> wfList = wfRecordMapper.selectByExample(example);


        for(GaiaWfRecord record : wfList){
            List<GetWfCreateStoreBsInData> storeBsInDataList = JSONArray.parseArray(record.getWfJsonString(), GetWfCreateStoreBsInData.class);
            List<GetWfCreateStoreBsInData> list2 = Lists.newArrayList(CollectionUtil.getFirst(storeBsInDataList));
            for(int i = 1; i < storeBsInDataList.size(); i++){
                GetWfCreateStoreBsInData storeBsInData = storeBsInDataList.get(i);
                String client = record.getClient();
                String brId = record.getWfSite();
                String batch = storeBsInData.getBatch();
                String proId = storeBsInData.getProCode();
                example = new Example(GaiaSdStockBatch.class);
                example.createCriteria().andEqualTo("clientId", client).andEqualTo("gssbBrId", brId).andEqualTo("gssbBatchNo", batch)
                        .andEqualTo("gssbProId", proId);
                List<GaiaSdStockBatch> sbList = stockBatchMapper.selectByExample(example);
                if(CollectionUtil.isNotEmpty(sbList)){
                    GaiaSdStockBatch stockBatch = sbList.get(0);
                    storeBsInData.setValidUntil(stockBatch.getGssbVaildDate());
                    list2.add(storeBsInData);
                }
            }
            record.setWfJsonString(JSON.toJSONString(list2));
            wfRecordMapper.updateByPrimaryKeySelective(record);
        }
        return JsonResult.success("outData", "提示：获取数据成功！");
    }

   @PostMapping({"/approve"})
   public JsonResult approve(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setUserId(userInfo.getUserId());
      inData.setClient(userInfo.getClient());
      log.info("approve:{}", JSONObject.toJSONString(inData));
      this.workflowService.approve(inData);
      return JsonResult.success("", "提示：保存成功！");
   }

   @PostMapping({"/cc"})
   public JsonResult cc(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setUserId(userInfo.getUserId());
      inData.setClient(userInfo.getClient());
      log.info("cc:{}", JSONObject.toJSONString(inData));
      this.workflowService.cc(inData);
      return JsonResult.success("", "提示：保存成功！");
   }

    @PostMapping({"/cancel"})
    public JsonResult cancel(HttpServletRequest request, @RequestBody GetWorkflowInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setUserId(userInfo.getUserId());
        inData.setClient(userInfo.getClient());
        log.info("cancel:{}", JSONObject.toJSONString(inData));
        this.workflowService.cancel(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    /**
     * 待办
     *
     * @param inData  pageNum 页码  pageSize  页长度
     * @return
     */
    @PostMapping({"/selectApprovingListApp"})
    public JsonResult selectApprovingListApp(@RequestBody GetWorkflowInData inData) {
        log.info("selectApprovingListApp:{}", JSONObject.toJSONString(inData));
        PageInfo<GetWorkflowOutData> list = this.workflowService.selectApprovingList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    /**
     * 已办
     *
     * @param inData  pageNum 页码  pageSize  页长度
     * @return
     */
    @PostMapping({"/selectApprovedListApp"})
    public JsonResult selectApprovedListApp(@RequestBody GetWorkflowInData inData) {
        log.info("selectApprovedListApp:{}", JSONObject.toJSONString(inData));
        PageInfo<GetWorkflowOutData> list = this.workflowService.selectApprovedList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    /**
     * 详情
     *
     * @param inData  wfCode  工作流编码
     * @return
     */
    @PostMapping({"/selectOneApp"})
    public JsonResult selectOneApp(@RequestBody GetWorkflowInData inData) {
        log.info("selectOneApp:{}", JSONObject.toJSONString(inData));
        GetWorkflowOutData outData = this.workflowService.selectOne(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    /**
     * 审批
     * @param inData
     * wfCode  工作流编码
     * result  "PASS"：同意 FAIL：驳回
     * memo  意见
     * rejectSeq  驳回节点
     * @return
     */
    @PostMapping({"/approveApp"})
    public JsonResult approveApp(@RequestBody GetWorkflowInData inData) {
        log.info("approveApp:{}", JSONObject.toJSONString(inData));
        this.workflowService.approve(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    /**
     * 会签
     * @param inData
     * wfCode  工作流编码
     * cc  会签员工
     * @return
     */
    @PostMapping({"/ccApp"})
    public JsonResult ccApp(@RequestBody GetWorkflowInData inData) {
        log.info("ccApp:{}", JSONObject.toJSONString(inData));
        this.workflowService.cc(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    /**
     * 取消
     * @param inData
     * wfCode  工作流编码
     * @return
     */
    @PostMapping({"/cancelApp"})
    public JsonResult cancelApp(@RequestBody GetWorkflowInData inData) {
        log.info("cancelApp:{}", JSONObject.toJSONString(inData));
        this.workflowService.cancel(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    /**
     * 会签员工集合
     * @param inData
     * @return
     */
    @PostMapping({"getUserListByClientId"})
    public JsonResult getUserListByClientId(@RequestBody GetWorkflowInData inData) {
        GetLoginOutData userInfo = new GetLoginOutData();
        userInfo.setClient(inData.getClient());
        log.info("getUserListByClientId:{}", JSONObject.toJSONString(inData));
        return JsonResult.success(this.userService.getUserListByClientId(userInfo), "提示：获取数据成功！");
    }
}
