package com.gys.business.controller.auth;


import cn.hutool.core.util.StrUtil;
import com.gys.business.service.RoleAuthService;
import com.gys.business.service.UserRoleAuthService;
import com.gys.business.service.data.auth.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api("角色权限配置")
@RestController
@RequestMapping({"/v2/roleAuth"})
public class RoleAuthController extends BaseController {
    @Autowired
    private RoleAuthService roleAuthService;

    @Resource
    private UserRoleAuthService userRoleAuthService;


//    @PostMapping({"/getRoleAuthDefaultName"})
//    public JsonResult getRoleAuthDefaultName(HttpServletRequest request, @RequestBody StroeRoleDelDetail inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        return JsonResult.success(this.roleAuthService.getRoleAuthDefaultName(userInfo, inData), "提示：获取数据成功！");
//    }

    /**
     * 初始化加盟商下所有的门店，并带出门店对应
     *
     * @param request
     * @return
     */
    @GetMapping({"/getStoreRoles"})
    public JsonResult getStoreRoles(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.roleAuthService.getStoreAndRols(userInfo), "提示：获取数据成功！");
    }

    @ApiOperation("查询门店下分配角色信息")
    @GetMapping({"/selectRoleInfoByStore"})
    public JsonResult selectRoleInfoByStore(HttpServletRequest request, String stoCode) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return userRoleAuthService.selectRoleInfoByStore(userInfo.getClient(), stoCode, null);
    }


    /**
     * 点击添加角色，初始化接口，渲染默认的参考角色页面
     *
     * @param request
     * @return
     */
    @GetMapping({"/addStoreRoleAuthInit"})
    public JsonResult addStoreRoleAuthInit(HttpServletRequest request, String stoCode) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (StrUtil.isBlank(stoCode)) {
            throw new BusinessException("请传递门店编码");
        }

        return this.roleAuthService.selectRoleInfoByStoreWithoutPerson(userInfo.getClient(), stoCode);
//        return this.roleAuthService.selectRoleInfoByStore(userInfo.getClient(), stoCode);
    }


    /**
     * 添加角色
     *
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"/addStoreRoleAuth"})
    public JsonResult addStoreRoleAuth(HttpServletRequest request, @Validated @RequestBody List<RoleAuthAddReq> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.roleAuthService.addRoleAuth(userInfo, inData), "提示：获取数据成功！");
    }


    /**
     * 删除角色
     *
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"/deleteStoreRole"})
    public JsonResult deleteStoreRole(HttpServletRequest request, @Validated @RequestBody List<StoreRoleReq> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.roleAuthService.deleteStoreRole(userInfo, inData), "提示：删除数据成功！");
    }


    /**
     * 将角色重制为参考角色的权限
     *
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"/resetStoreRole"})
    public JsonResult resetStoreRole(HttpServletRequest request, @Validated @RequestBody StoreRoleReq inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.roleAuthService.resetStoreRole(userInfo, inData), "提示：获取数据成功！");
    }


    @PostMapping({"/updateStoreRole"})
    public JsonResult updateStoreRole(HttpServletRequest request, @Validated @RequestBody StoreRoleUpdateReq inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.roleAuthService.updateStoreRole(userInfo, inData), "提示：获取数据成功！");
    }

}
