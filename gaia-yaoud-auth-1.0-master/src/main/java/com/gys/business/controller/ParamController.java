package com.gys.business.controller;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.service.ParamService;
import com.gys.business.service.data.DefultParamInData;
import com.gys.business.service.data.ParamDetailInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/param"})
public class ParamController extends BaseController {
   @Autowired
   private ParamService paramService;
   @Autowired
   private HttpServletRequest request;

   @PostMapping({"/default/add"})
   public JsonResult addDefaultParam(HttpServletRequest request, @RequestBody DefultParamInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.paramService.addDefaultParam(inData, userInfo);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"/default/update"})
   public JsonResult updateDefaultParam(HttpServletRequest request, @RequestBody DefultParamInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.paramService.updateDefaultParam(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"/default/list"})
   public JsonResult getDefaultParamList(HttpServletRequest request, @RequestBody DefultParamInData inData) {
      return JsonResult.success(this.paramService.getDefaultParamList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"/detail/list"})
   public JsonResult getParamDetailList(@RequestBody ParamDetailInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.paramService.getParamDetailList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"/detail/update"})
   public JsonResult updateParamDetail(HttpServletRequest request, @RequestBody ParamDetailInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.paramService.updateParamDetail(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"/detail/bath/update"})
   public JsonResult updateBathParamDetail(HttpServletRequest request, @RequestBody List<ParamDetailInData> inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      if (!CollUtil.isEmpty(inData)) {
         Iterator var4 = inData.iterator();

         while(var4.hasNext()) {
            ParamDetailInData data = (ParamDetailInData)var4.next();
            String gsspPara = data.getGsspPara();
            //去除参数空格 
            if(StringUtils.isNotEmpty(gsspPara)) {
               String trim = gsspPara.trim();
               data.setGsspPara(trim);
            }
            this.paramService.updateParamDetail(data, userInfo);
         }
      }

      return JsonResult.success((Object)null, "提示：更新成功！");
   }


   @PostMapping({"/default/listExt"})
   public JsonResult getDefaultParamListExt(HttpServletRequest request, @RequestBody DefultParamInData inData) {
      return JsonResult.success(this.paramService.getDefaultParam(null), "提示：获取数据成功！");
   }

}
