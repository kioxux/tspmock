package com.gys.business.controller;

import com.gys.business.service.DomainConfigService;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeartbeatController {
    @Autowired
    private DomainConfigService domainConfigService;

    @GetMapping({"/heartbeat"})
    public JsonResult heartbeat(){
        return JsonResult.success("Alive", "");
    }

    @PostMapping({"/checkServerState"})
    public JsonResult checkServerState(){
        return JsonResult.success(domainConfigService.getAll(), "查询成功!");
    }
}