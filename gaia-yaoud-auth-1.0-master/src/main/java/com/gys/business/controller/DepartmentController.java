package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.service.DepartmentService;
import com.gys.business.service.data.DeptExportInData;
import com.gys.business.service.data.DeptInData;
import com.gys.business.service.data.dep.dto.DepDto;
import com.gys.business.service.data.dep.vo.DepVo;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping({"/dept/"})
public class DepartmentController extends BaseController {
   @Autowired
   private DepartmentService departmentService;

   @PostMapping({"getDeptList"})
   public JsonResult getDeptList(@RequestBody DeptInData inData) {
      return JsonResult.success(this.departmentService.getDeptList(inData), "提示：获取数据成功！");
   }

   @GetMapping({"getDeptById"})
   public JsonResult getDeptById(HttpServletRequest request, @RequestParam(name = "depId", required = true) String depId) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.departmentService.getDeptById(depId, userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"updateDept"})
   public JsonResult updateDictionary(HttpServletRequest request, @RequestBody DeptInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.departmentService.updateDept(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"updateDeptFeign"})
   public JsonResult updateDeptFeign(@RequestBody DeptInData inData) {
      log.info("<修改部门调用成功>");
      GetLoginOutData userInfo = new GetLoginOutData();
      userInfo.setUserId(inData.getDepModiId());
      userInfo.setClient(inData.getClient());
      this.departmentService.updateDept(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"insertDept"})
   public JsonResult insertDept(HttpServletRequest request, @RequestBody DeptInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      GaiaDepData depData = this.departmentService.insertDept(inData, userInfo);
      return JsonResult.success(depData, "提示：保存成功！");
   }

   @PostMapping({"insertDeptFeign"})
   public JsonResult insertDept(@RequestBody DeptInData inData) {
      log.info("<新增部门调用成功>");
      GetLoginOutData userInfo = new GetLoginOutData();
      userInfo.setClient(inData.getClient());
      userInfo.setUserId(inData.getDepCreId());
      GaiaDepData depData = this.departmentService.insertDept(inData, userInfo);
      return JsonResult.success(depData, "提示：保存成功！");
   }

   @PostMapping({"disableDept"})
   public JsonResult disableDept(HttpServletRequest request, @RequestBody DeptInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.departmentService.disableDept(inData, userInfo);
      return JsonResult.success((Object)null, "提示：删除成功！");
   }

   @PostMapping({"enableDept"})
   public JsonResult enableDept(HttpServletRequest request, @RequestBody DeptInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.departmentService.enableDept(inData, userInfo);
      return JsonResult.success((Object)null, "提示：删除成功！");
   }

   @PostMapping({"getDeptListByType"})
   public JsonResult getDeptListByType(HttpServletRequest request, @RequestBody DeptInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.departmentService.getDeptListByType(inData), "提示：删除成功！");
   }

   @PostMapping({"exportIn"})
   public JsonResult exportIn(HttpServletRequest request, MultipartFile file) {
      try {
         GetLoginOutData outData = this.getLoginUser(request);
         List<DeptExportInData> inData = this.importExcel(file, DeptExportInData.class, 0, 1);
         return this.departmentService.exportIn(inData, outData);
      } catch (Exception var5) {
         throw new BusinessException(var5.toString());
      }
   }

   @PostMapping({"depts"})
   public JsonResult getDeptListByType(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      DepDto dto=new DepDto();
      dto.setClient(userInfo.getClient());
     List<DepVo> voList= departmentService.getAllDeptList(dto);
      return JsonResult.success(voList, "查询成功");
   }
}
