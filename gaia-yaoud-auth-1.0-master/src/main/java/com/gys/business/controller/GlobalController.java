package com.gys.business.controller;

import com.gys.business.service.GlobalService;
import com.gys.business.service.data.GlobalInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/global/"})
public class GlobalController extends BaseController {
   @Autowired
   private GlobalService globalService;

   @PostMapping({"authorization"})
   public JsonResult authorization(HttpServletRequest request, @RequestBody GlobalInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.globalService.authorization(inData), "提示：保存成功！");
   }

   @PostMapping({"authorList"})
   public JsonResult authorList(HttpServletRequest request, @RequestBody GlobalInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.globalService.authorList(inData), "提示：获取数据成功！");
   }
}
