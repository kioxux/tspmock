package com.gys.business.controller;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.mapper.entity.UserResourceRecord;
import com.gys.business.service.UserService;
import com.gys.business.service.data.SaveUserInData;
import com.gys.business.service.data.UserExportInData;
import com.gys.business.service.data.UserInData;
import com.gys.business.service.data.UserOutData;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.ResourceRecordForm;
import com.gys.common.data.StoreForm;
import com.gys.common.exception.BusinessException;
import com.gys.util.UtilConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping({"/user/"})
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @PostMapping({"getUserList"})
    public JsonResult getUserList(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.userService.getUserList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getUserAllList"})
    public JsonResult getUserAllList(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.userService.getUserAllList(inData, userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"getUserById"})
    public JsonResult getUserById(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.userService.getUserById(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getCheckPath"})
    public JsonResult getCheckPath(HttpServletRequest request, @RequestBody Map inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client",userInfo.getClient());
        inData.put("userCode",userInfo.getUserId());
        inData.put("stoCode",userInfo.getDepId());
        return JsonResult.success(this.userService.getCheckPath(inData), "提示：获取数据成功！");
    }

    @PostMapping({"updateUser"})
    public JsonResult updateDictionary(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.userService.updateUser(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"updateUserFeign"})
    public JsonResult updateUserFeign(@RequestBody SaveUserInData inData) {
        log.info("<修改员工调用成功>");
        GetLoginOutData userInfo = new GetLoginOutData();
        userInfo.setUserId(inData.getOperateUser());
        userInfo.setClient(inData.getClient());
        this.userService.updateUser(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"addDeptUser"})
    public JsonResult addDeptUser(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.userService.addDeptUser(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"addDeptUserFeign"})
    public JsonResult addDeptUserFeign(@RequestBody SaveUserInData inData) {
        log.info("<新增员工调用成功>");
        GetLoginOutData userInfo = new GetLoginOutData();
        userInfo.setClient(inData.getClient());
        userInfo.setUserId(inData.getOperateUser());
        this.userService.addDeptUser(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"insertUser"})
    public JsonResult insertUser(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.userService.insertUser(inData, userInfo);
        return JsonResult.success((Object)null, "提示：保存成功！");
    }

    @PostMapping({"deleteUser"})
    public JsonResult deleteUser(HttpServletRequest request, @RequestBody UserInData inData) {
        this.userService.deleteUser(inData);
        return JsonResult.success((Object)null, "提示：删除成功！");
    }

    @PostMapping({"getUserListByClientId"})
    public JsonResult getUserListByClientId(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.userService.getUserListByClientId(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"getUsersByClientId"})
    public JsonResult getUsersByClientId(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.userService.getUsersByClientId(userInfo, inData), "提示：获取数据成功！");
    }

    @PostMapping({"exportUser"})
    public void exportUser(HttpServletRequest request, HttpServletResponse response, @RequestBody UserInData inData) {
        try {
            GetLoginOutData userInfo = this.getLoginUser(request);
            inData.setClient(userInfo.getClient());
            this.userService.exportUser(response, inData);
        } catch (Exception var5) {
            throw new BusinessException(var5.toString());
        }
    }

    @PostMapping({"exportIn"})
    public JsonResult exportIn(HttpServletRequest request, MultipartFile file) {
        try {
            GetLoginOutData outData = this.getLoginUser(request);
            List<UserExportInData> inData = this.importExcel(file, UserExportInData.class, 0, 1);
            return this.userService.exportIn(inData, outData);
        } catch (Exception var5) {
            throw new BusinessException(var5.toString());
        }
    }

    @GetMapping({"exportExcel"})
    public void exportExcel(HttpServletResponse res) {
        ExportExcel.exportModelFile(res, "人员导入模板", "modelFile/userModelFile.xls");
    }

    @PostMapping({"selectPharmacistByBrId"})
    public JsonResult selectPharmacistByBrId(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setDepId(userInfo.getDepId());
        List<UserOutData> list = this.userService.selectPharmacistByBrId(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @GetMapping({"getDepAndStore"})
    public JsonResult getDepAndStore(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.userService.getDepAndStore(null,userInfo), "提示：获取数据成功！");
    }

    @PostMapping("getResourceRecord")
    public JsonResult getResourceRecord(HttpServletRequest request, @RequestBody ResourceRecordForm resourceRecordForm) {
        log.info("<用户模块><操作指引><查询操作指引记录，请求参数：{}>", JSON.toJSONString(resourceRecordForm));
        if (StringUtils.isEmpty(resourceRecordForm.getPageId())) {
            return JsonResult.fail(UtilConst.CODE_500, "pageId不能为空");
        }
        GetLoginOutData loginUser = getLoginUser(request);
        resourceRecordForm.setClient(loginUser.getClient());
        resourceRecordForm.setUserId(loginUser.getUserId());
        UserResourceRecord resourceRecord = userService.getResourceRecord(resourceRecordForm);
        Map<String, Object> data = new HashMap<>();
        data.put("operateFlag", resourceRecord == null ? 0 : 1);
        return JsonResult.success(data, "查询成功");
    }

    @FrequencyValid
    @PostMapping("updateResourceRecord")
    public JsonResult updateResourceRecord(HttpServletRequest request, @RequestBody ResourceRecordForm resourceRecordForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        resourceRecordForm.setClient(loginUser.getClient());
        resourceRecordForm.setUserId(loginUser.getUserId());
        resourceRecordForm.setUserName(loginUser.getLoginName());
        userService.updateResourceRecord(resourceRecordForm);
        return JsonResult.success(null, "更新成功");
    }

    @PostMapping("autoOperate")
    public JsonResult autoOperate(@RequestBody List<String> pageIdList) {
        if (pageIdList == null || pageIdList.size() == 0) {
            return JsonResult.fail(1000, "参数不能为空");
        }
        userService.autoOperate(pageIdList);
        return JsonResult.success(null, "处理成功");
    }

    @PostMapping("setDefaultStore")
    public JsonResult setDefaultStore(HttpServletRequest request, @RequestBody StoreForm storeForm) {
        log.info("<用户管理><设置默认门店><请求参数：{}>", JSON.toJSONString(storeForm));
        GetLoginOutData loginUser = getLoginUser(request);
        storeForm.setClient(loginUser.getClient());
        storeForm.setUserId(loginUser.getUserId());
        userService.setDefaultStore(storeForm);
        return JsonResult.success("");
    }

    @PostMapping("getPharmacist")
    public JsonResult getPharmacist(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);

        return JsonResult.success(userService.getPharmacist(loginUser), "处理成功");
    }

    @PostMapping("updateNbybInfo")
    public JsonResult updateNbybInfo(HttpServletRequest request, @RequestBody GaiaUserData userData) {
        GetLoginOutData loginUser = getLoginUser(request);
        userData.setClient(loginUser.getClient());
        userService.updateNbybInfo(userData);
        return JsonResult.success(null, "处理成功");
    }

}
