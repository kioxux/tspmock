package com.gys.business.controller;

import com.gys.business.service.StoreService;
import com.gys.business.service.data.StoreExportInData;
import com.gys.business.service.data.StoreInData;
import com.gys.business.service.data.StoreOutData;
import com.gys.business.service.data.auth.UserRestrictInfo;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping({"/store/"})
public class StoreController extends BaseController {
   @Autowired
   private StoreService storeService;

   @PostMapping({"getRestrictStoreList"})
   public JsonResult getRestrictStoreList(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.storeService.getRestrictStoreList(userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"getRestrictStoreListWithType"})
   public UserRestrictInfo getRestrictStoreListWithType(@RequestBody GetLoginOutData userInfo) {
//      GetLoginOutData userInfo = this.getLoginUser(request);
      return this.storeService.getRestrictStoreListWithType(userInfo);
   }

   @PostMapping({"getStoreList"})
   public JsonResult getStoreList(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoreList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getLoginUserStoreList"})
   public JsonResult getLoginUserStoreList(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      StoreInData inData = new StoreInData();
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoreList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getStoreById"})
   public JsonResult getStoreById(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoreById(inData), "提示：获取数据成功！");
   }

   @PostMapping({"updateStore"})
   public JsonResult updateDictionary(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.storeService.updateStore(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"insertStore"})
   public JsonResult insertStore(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.storeService.insertStore(inData, userInfo);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"updateStoreStatus"})
   public JsonResult updateStoreStatus(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.storeService.updateStoreStatus(inData, userInfo);
      return JsonResult.success((Object)null, "提示：删除成功！");
   }

   @PostMapping({"insertStoreGroup"})
   public JsonResult insertStoreGroup(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.storeService.insertStoreGroup(inData);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"getStogList"})
   public JsonResult getStogList(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStogList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getStoGroupList"})
   public JsonResult getStoGroupList(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoGroupList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getStogPage"})
   public JsonResult getStogPage(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStogPage(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getStoList"})
   public JsonResult getStoList(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getStoListByClientId"})
   public JsonResult getStoListByClientId(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoListByClientId(inData), "提示：获取数据成功！");
   }

   @PostMapping({"deleteStog"})
   public JsonResult deleteStog(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.storeService.deleteStog(inData);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"getStoreCode"})
   public JsonResult getStoreCode(HttpServletRequest request, @RequestBody StoreInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.storeService.getStoreCode(inData), "提示：获取数据成功！");
   }

   @PostMapping({"exportIn"})
   public JsonResult exportIn(HttpServletRequest request, MultipartFile file) {
      try {
         GetLoginOutData outData = this.getLoginUser(request);
         List<StoreExportInData> inData = this.importExcel(file, StoreExportInData.class, 0, 1);
         return this.storeService.exportIn(inData, outData);
      } catch (Exception var5) {
         throw new BusinessException(var5.toString());
      }
   }

   @PostMapping({"selectAuthStoreList"})
   public JsonResult selectAuthStoreList(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      List<StoreOutData> list = this.storeService.selectAuthStoreList(userInfo.getClient(), userInfo.getUserId());
      return JsonResult.success(list, "提示：获取数据成功！");
   }
}
