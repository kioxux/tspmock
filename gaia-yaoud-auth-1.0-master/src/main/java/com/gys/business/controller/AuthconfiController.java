package com.gys.business.controller;

import com.gys.business.service.AuthconfiService;
import com.gys.business.service.data.AuthGroupOutData;
import com.gys.business.service.data.AuthInData;
import com.gys.business.service.data.AuthListInData;
import com.gys.business.service.data.AuthSiteOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/auth/"})
public class AuthconfiController extends BaseController {
   @Autowired
   private AuthconfiService authconfiService;

   @PostMapping({"authorization"})
   public JsonResult authorization(HttpServletRequest request, @RequestBody AuthListInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.authconfiService.authorization(inData);
      return JsonResult.success("", "提示：保存成功！");
   }

   @PostMapping({"deleteAuth"})
   public JsonResult deleteAuth(HttpServletRequest request, @RequestBody AuthInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.authconfiService.deleteAuth(inData);
      return JsonResult.success("", "提示：删除成功！");
   }

   @PostMapping({"authorizationList"})
   public JsonResult authorizationList(HttpServletRequest request, @RequestBody AuthInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.authconfiService.authorizationList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"selectSiteList"})
   public JsonResult selectSiteList(HttpServletRequest request, @RequestBody AuthInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      List<AuthSiteOutData> list = this.authconfiService.selectSiteList(inData);
      return JsonResult.success(list, "提示：获取数据成功！");
   }

   @PostMapping({"selectStoreList"})
   public JsonResult selectStoreList(HttpServletRequest request, @RequestBody AuthInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      List<AuthSiteOutData> list = this.authconfiService.selectStoreList(inData);
      return JsonResult.success(list, "提示：获取数据成功！");
   }

   @PostMapping({"selectDepList"})
   public JsonResult selectDepList(HttpServletRequest request, @RequestBody AuthInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      List<AuthSiteOutData> list = this.authconfiService.selectDepList(inData);
      return JsonResult.success(list, "提示：获取数据成功！");
   }

   @PostMapping({"selectGroupList"})
   public JsonResult selectGroupList(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      List<AuthGroupOutData> list = this.authconfiService.selectGroupList(userInfo);
      return JsonResult.success(list, "提示：获取数据成功！");
   }
}
