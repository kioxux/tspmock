package com.gys.business.controller;

import com.gys.business.service.SyncService;
import com.gys.business.service.data.GetSyncOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/sync/"})
public class SyncController extends BaseController {
   @Resource
   private SyncService syncService;

   @PostMapping({"selectBaseInfo"})
   public JsonResult selectBaseInfo(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      GetSyncOutData outData = this.syncService.selectBaseInfo(userInfo);
      return JsonResult.success(outData, "提示：获取数据成功！");
   }
}
