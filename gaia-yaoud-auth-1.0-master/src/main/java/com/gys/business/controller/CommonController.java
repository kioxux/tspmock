package com.gys.business.controller;

import com.gys.business.service.CommonService;
import com.gys.business.service.data.GetFileOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping({"/common/"})
public class CommonController {
   @Resource
   private CommonService commonService;

   @PostMapping({"file/upload"})
   public JsonResult fileUpload(HttpServletRequest request, MultipartFile file) {
      GetFileOutData outData = this.commonService.fileUpload(file);
      return JsonResult.success(outData, "提示：上传成功！");
   }
}
