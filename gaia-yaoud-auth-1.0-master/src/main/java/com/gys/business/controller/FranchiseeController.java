package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.service.FranchiseeService;
import com.gys.business.service.UserService;
import com.gys.business.service.data.FranchiseeInData;
import com.gys.business.service.data.GetLoginInData;
import com.gys.business.service.data.UserLoginModelClient;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/franchisee/"})
public class FranchiseeController extends BaseController {
   @Autowired
   private FranchiseeService franchiseeService;
   @Autowired
   private UserService userService;

   @PostMapping({"getFranchiseeById"})
   public JsonResult getFranchiseeById(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      if (ObjectUtil.isEmpty(inData.getClient())) {
         inData.setClient(userInfo.getClient());
      }

      return JsonResult.success(this.franchiseeService.getFranchiseeById(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getLoginUserFranchisee"})
   public JsonResult getLoginUserFranchisee(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      FranchiseeInData inData = new FranchiseeInData();
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.franchiseeService.getFranchiseeById(inData), "提示：获取数据成功！");
   }

   @PostMapping({"updateFranchisee"})
   public JsonResult updateDictionary(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.franchiseeService.updateFranchisee(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"insertFranchisee"})
   public JsonResult insertFranchisee(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.franchiseeService.insertFranchisee(inData, userInfo);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"deleteFranchisee"})
   public JsonResult deleteFranchisee(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
      this.franchiseeService.deleteFranchisee(inData);
      return JsonResult.success((Object)null, "提示：删除成功！");
   }

   @PostMapping({"baseInfo"})
   public JsonResult baseInfo(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
      return JsonResult.success(this.franchiseeService.baseInfo(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getFranchiseeList"})
   public JsonResult getFranchiseeList(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
      return JsonResult.success(this.franchiseeService.getFranchiseeList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"home/getFranchiseeInfo"})
   public JsonResult getHomeFranchiseeInfo(HttpServletRequest request) {
      GetLoginOutData loginOutData = this.getLoginUser(request);
      return JsonResult.success(this.franchiseeService.getHomeFranchiseeInfo(loginOutData), "提示：获取数据成功！");
   }

   @PostMapping({"getSmsAuthenticationCode"})
   public JsonResult getSmsAuthenticationCode(HttpServletRequest request, GetLoginInData inData) {
      this.franchiseeService.getSmsAuthenticationCode(inData);
      return JsonResult.success(null, "提示：获取数据成功！");
   }

   /**
    * 注册加盟商
    * @param request
    * @param inData
    * @return
    */
   @PostMapping({"insertFranchiseeByPhone"})
   public JsonResult insertFranchiseeByPhone(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
//      GetLoginOutData userInfo = this.getLoginUser(request);
      this.franchiseeService.insertFranchiseeByPhone(inData);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   /**
    * 修改加盟商
    * @param inData
    * @return
    */
   @PostMapping({"updateFranchiseeByPhone"})
   public JsonResult updateFranchiseeByPhone(@RequestBody FranchiseeInData inData) {
      UserLoginModelClient currentUser = userService.getCurrentUser();
      this.franchiseeService.updateFranchiseeByPhone(currentUser,inData);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }
   /**
    * 查询加盟商
    * @param inData
    * @return
    */
   @PostMapping({"getFranchiseeByIdPhone"})
   public JsonResult getFranchiseeByIdPhone(@RequestBody FranchiseeInData inData) {
      UserLoginModelClient currentUser = userService.getCurrentUser();
      if (ObjectUtil.isEmpty(inData.getClient())) {
         inData.setClient(currentUser.getClient());
      }
      return JsonResult.success(this.franchiseeService.getFranchiseeByIdPhone(currentUser,inData), "提示：获取数据成功！");
   }

   /**
    * 移动端加盟商注册发送短信验证码
    * @param request
    * @param inData
    * @return
    */
   @PostMapping({"getSmsAuthenticationCodeByPhone"})
   public JsonResult getSmsAuthenticationCodeByPhone(HttpServletRequest request,@RequestBody FranchiseeInData inData) {
      this.franchiseeService.getSmsAuthenticationCodeByPhone(inData);
      return JsonResult.success(null, "提示：获取数据成功！");
   }
}
