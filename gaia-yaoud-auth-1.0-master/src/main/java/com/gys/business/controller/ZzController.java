package com.gys.business.controller;

import com.gys.business.service.ZzService;
import com.gys.business.service.data.ZzInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/zz/"})
public class ZzController extends BaseController {
   @Autowired
   private ZzService zzService;

   @PostMapping({"getZzList"})
   public JsonResult getZzList(HttpServletRequest request, @RequestBody ZzInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.zzService.getZzList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getZzById"})
   public JsonResult getZzById(HttpServletRequest request, @RequestBody ZzInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.zzService.getZzById(inData, userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"editZz"})
   public JsonResult updateDictionary(HttpServletRequest request, @RequestBody ZzInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.zzService.updateZz(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"insertZz"})
   public JsonResult insertZz(HttpServletRequest request, @RequestBody ZzInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.zzService.insertZz(inData, userInfo);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"deleteZz"})
   public JsonResult deleteZz(HttpServletRequest request, @RequestBody ZzInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.zzService.deleteZz(inData, userInfo);
      return JsonResult.success((Object)null, "提示：删除成功！");
   }

   @PostMapping({"getZzooList"})
   public JsonResult getZzooList() {
      return JsonResult.success(this.zzService.getZzooList(), "提示：获取数据成功！");
   }
}
