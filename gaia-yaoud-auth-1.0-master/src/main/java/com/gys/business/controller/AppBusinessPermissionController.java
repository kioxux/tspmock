package com.gys.business.controller;

import com.gys.business.service.AppAuthSetService;
import com.gys.common.base.BaseController;
import com.gys.common.data.AppPermissionVO;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/25 10:36
 */
@RequestMapping("app")
@RestController
public class AppBusinessPermissionController extends BaseController {

    @Resource
    private AppAuthSetService appAuthSetService;

    /**
     * APP业务权限控制类
     *
     * @return 权限列表
     */
    @PostMapping("business/permission")
    public JsonResult businessPermission(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);
        AppPermissionVO permission = appAuthSetService.getBusinessPermission(loginUser);
        return JsonResult.success(permission);
    }

}
