package com.gys.business.controller;

import com.gys.business.service.AreaService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(tags = "省市区列表")
@RestController
@RequestMapping("area")
public class AreaController extends BaseController {

    @Resource
    private AreaService areaService;

    @ApiOperation("获取省市区信息")
    @GetMapping("getAreaList")
    public JsonResult getAreaList(HttpServletRequest request, String userId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (userInfo == null) {
            throw new BusinessException("提示：请重新登陆！");
        }
        return areaService.getAreaList(userInfo.getClient(), userId);
    }


}
