package com.gys.business.controller;

import com.gys.business.mapper.entity.ClientParam;
import com.gys.business.mapper.entity.SdParam;
import com.gys.business.service.ClientParamService;
import com.gys.business.service.MemberService;
import com.gys.business.service.data.CashInData;
import com.gys.business.service.data.ExchangeExportInData;
import com.gys.business.service.data.ExchangeInData;
import com.gys.business.service.data.GetMemberOutData;
import com.gys.business.service.data.GetMemberPreSetImportInData;
import com.gys.business.service.data.GetMemberPreSetInData;
import com.gys.business.service.data.GetMemberPreSetOutData;
import com.gys.business.service.data.GetMemberSetImportInData;
import com.gys.business.service.data.IntegralInData;
import com.gys.business.service.data.MemberInData;
import com.gys.common.base.BaseController;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping({"/member/"})
public class MemberController extends BaseController {
   @Autowired
   private MemberService memberService;
   @Autowired
   private ClientParamService clientParamService;

   @PostMapping({"typeSet"})
   public JsonResult typeSet(HttpServletRequest request, @RequestBody MemberInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      inData.setUserId(userInfo.getUserId());
      this.memberService.typeSet(inData);
      return JsonResult.success("", "提示：保存成功！");
   }

   @PostMapping({"getMemberClass"})
   public JsonResult getMemberClass(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.memberService.getMemberClass(userInfo), "提示：获取数据成功！");
   }

   @GetMapping({"exportExcel"})
   public void exportExcel(HttpServletResponse res) {
      ExportExcel.exportModelFile(res, "会员类型设置模板", "modelFile/memberTypeModelFile.xls");
   }

   @PostMapping({"integralSet"})
   public JsonResult integralSet(HttpServletRequest request, @RequestBody IntegralInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.integralSet(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getIntegralSet"})
   public JsonResult getIntegralSet(HttpServletRequest request, @RequestBody IntegralInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.getIntegralSet(inData), "提示：获取数据成功！");
   }

   @PostMapping({"exchangeSetList"})
   public JsonResult exchangeSetList(HttpServletRequest request, @RequestBody ExchangeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.exchangeSetList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getExchangeSet"})
   public JsonResult getExchangeSet(HttpServletRequest request, @RequestBody ExchangeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.getExchangeSet(inData), "提示：更新成功！");
   }

   @PostMapping({"approveExchangeSet"})
   public JsonResult approveExchangeSet(HttpServletRequest request, @RequestBody ExchangeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.approveExchangeSet(inData), "提示：更新成功！");
   }

   @PostMapping({"updateExchangeSet"})
   public JsonResult updateExchangeSet(HttpServletRequest request, @RequestBody ExchangeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.updateExchangeSet(inData), "提示：更新成功！");
   }

   @PostMapping({"addExchangeSet"})
   public JsonResult addExchangeSet(HttpServletRequest request, @RequestBody ExchangeInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.addExchangeSet(inData), "提示：保存成功！");
   }

   @PostMapping({"cashSetList"})
   public JsonResult cashSetList(HttpServletRequest request, @RequestBody CashInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClientId(userInfo.getClient());
      return JsonResult.success(this.memberService.cashSetList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"updateCashSet"})
   public JsonResult updateCashSet(HttpServletRequest request, @RequestBody CashInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.memberService.updateCashSet(inData, userInfo), "提示：更新成功！");
   }

   @PostMapping({"addCashSet"})
   public JsonResult addCashSet(HttpServletRequest request, @RequestBody CashInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.memberService.addCashSet(inData, userInfo), "提示：保存成功！");
   }

   @PostMapping({"exportOut"})
   public void exportOut(HttpServletRequest request,HttpServletResponse response, @RequestBody ExchangeInData inData) {
      try {
         GetLoginOutData userInfo = this.getLoginUser(request);
         inData.setClientId(userInfo.getClient());
         this.memberService.exportOut(response, inData);
      } catch (Exception var4) {
         throw new BusinessException(var4.toString());
      }
   }

   /**
    * 积分抵现导出
    * @param request
    * @param response
    * @param inData
    */
   @PostMapping({"export"})
   public void export(HttpServletRequest request,HttpServletResponse response, @RequestBody CashInData inData) {
      try {
         GetLoginOutData userInfo = this.getLoginUser(request);
         inData.setClientId(userInfo.getClient());
         this.memberService.export(response, inData);
      } catch (Exception var4) {
         throw new BusinessException(var4.toString());
      }
   }

   @PostMapping({"exportIn"})
   public JsonResult exportIn(HttpServletRequest request, MultipartFile file) {
      try {
         GetLoginOutData outData = this.getLoginUser(request);
         String client = outData.getClient();
         List<ExchangeExportInData> inData = this.importExcel(file, ExchangeExportInData.class, 0, 1);
         return this.memberService.exportIn(inData, client);
      } catch (Exception var6) {
         throw new BusinessException(var6.toString());
      }
   }

   @PostMapping({"getVirtualMaxCard"})
   public JsonResult getVirtualMaxCard(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      return JsonResult.success(this.memberService.getVirtualMaxCard(userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"getVirtualStoreSet"})
   public JsonResult getVirtualStoreSet(HttpServletRequest request, @RequestBody GetMemberPreSetInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      GetMemberPreSetOutData outData = this.memberService.getVirtualStoreSet(inData, userInfo);
      return JsonResult.success(outData, "提示：获取数据成功！");
   }

   @PostMapping({"saveMemberPre"})
   public JsonResult saveMemberPre(HttpServletRequest request, @RequestBody GetMemberPreSetInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.memberService.saveMemberPre(inData, userInfo);
      return JsonResult.success("", "提示：保存成功！");
   }

   @GetMapping({"exportMemberPreExcel"})
   public void exportMemberPreExcel(HttpServletResponse response) {
      ExportExcel.exportModelFile(response, "会员卡导入模板", "modelFile/member.xls");
   }

   @PostMapping({"importMember"})
   public JsonResult importMember(HttpServletRequest request, MultipartFile file) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      new ArrayList();

      List inData;
      try {
         inData = this.importExcel(file, GetMemberPreSetImportInData.class, 0, 1);
      } catch (Exception var6) {
         throw new BusinessException(var6.getMessage());
      }

      this.memberService.importMember(inData, userInfo);
      return JsonResult.success("", "提示：保存成功！");
   }

   @PostMapping({"importMemberSet"})
   public JsonResult importMemberSet(HttpServletRequest request, MultipartFile file) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      new ArrayList();

      List inData;
      try {
         inData = this.importExcel(file, GetMemberSetImportInData.class, 0, 1);
      } catch (Exception var6) {
         throw new BusinessException(var6.getMessage());
      }

      this.memberService.importMemberSet(inData, userInfo);
      return JsonResult.success("", "提示：保存成功！");
   }

   @PostMapping({"selectNewCardList"})
   public JsonResult selectNewCardList(HttpServletRequest request, @RequestBody GetMemberPreSetInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      PageInfo<GetMemberOutData> pageInfo = this.memberService.selectNewCardList(inData, userInfo);
      return JsonResult.success(pageInfo, "提示：获取数据成功！");
   }

   @GetMapping({"memberCardConfig"})
   // 会员卡配置 0/NULL关闭，1开始，缺省为NULL
   public JsonResult getMemberCardConfig(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      ClientParam param= clientParamService.getConfig(userInfo.getClient());
      if(param!=null){
         return JsonResult.success(param.getGcspPara1()) ;
      }
      return JsonResult.success(0);
   }

   @GetMapping({"receivePriceConfig"})
   // 【0-否（缺省）；1-是（不显示）】 receivePrice
   public JsonResult getReceivePriceConfig(HttpServletRequest request) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      SdParam param= clientParamService.getReceivePriceConfig(userInfo.getClient(),userInfo.getDepId());
      if(param!=null){
         return JsonResult.success(param.getGsspPara()) ;
      }
      return JsonResult.success(0);
   }


}
