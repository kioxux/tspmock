package com.gys.business.controller;

import com.gys.business.service.JyfwService;
import com.gys.business.service.data.JyfwInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/jyfw/"})
public class JyfwController extends BaseController {
   @Autowired
   private JyfwService jyfwService;

   @PostMapping({"getJyfwList"})
   public JsonResult getJyfwList(HttpServletRequest request, @RequestBody JyfwInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.jyfwService.getJyfwList(inData), "提示：获取数据成功！");
   }

   @PostMapping({"getJyfwById"})
   public JsonResult getJyfwById(HttpServletRequest request, @RequestBody JyfwInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      return JsonResult.success(this.jyfwService.getJyfwById(inData, userInfo), "提示：获取数据成功！");
   }

   @PostMapping({"editJyfw"})
   public JsonResult updateDictionary(HttpServletRequest request, @RequestBody JyfwInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.jyfwService.updateJyfw(inData, userInfo);
      return JsonResult.success((Object)null, "提示：更新成功！");
   }

   @PostMapping({"insertJyfw"})
   public JsonResult insertJyfw(HttpServletRequest request, @RequestBody JyfwInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.jyfwService.insertJyfw(inData, userInfo);
      return JsonResult.success((Object)null, "提示：保存成功！");
   }

   @PostMapping({"deleteJyfw"})
   public JsonResult deleteJyfw(HttpServletRequest request, @RequestBody JyfwInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      inData.setClient(userInfo.getClient());
      this.jyfwService.deleteJyfw(inData, userInfo);
      return JsonResult.success((Object)null, "提示：删除成功！");
   }

   @PostMapping({"getJyfwooList"})
   public JsonResult getJyfwooList() {
      return JsonResult.success(this.jyfwService.getJyfwooList(), "提示：获取数据成功！");
   }
}
