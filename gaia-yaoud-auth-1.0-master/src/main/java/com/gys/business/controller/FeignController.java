package com.gys.business.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.LoginService;
import com.gys.business.service.MemberService;
import com.gys.business.service.UserService;
import com.gys.business.service.WorkflowService;
import com.gys.business.service.data.GetButtonAuthObjOutData;
import com.gys.business.service.data.GetWfCreateInData;
import com.gys.business.service.data.GetWorkflowApproveOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping({"/"})
@Slf4j
public class FeignController extends BaseController {
   @Resource
   private UserService userService;

   @Resource
   private LoginService loginService;

   @Resource
   private WorkflowService workflowService;

   @Resource
   private MemberService memberService;

   @GetMapping({"checkResource"})
   public String checkResource(@RequestParam("param") String param) {
      if (StrUtil.isBlank(param)) {
         return "0";
      } else {
         JSONObject jsonObject = JSON.parseObject(param);
         String token = jsonObject.getString("token");
         String path = jsonObject.getString("path");
         if (!StrUtil.isBlank(token) && !StrUtil.isBlank(path)) {
            GetLoginOutData loginOutData = this.getLoginUser(token);
            String result = this.userService.checkResource(loginOutData.getUserId(), path);
            log.info("getLoginInfo result:{}", JSON.toJSONString(result));
            return result;
         } else {
            return "0";
         }
      }
   }

   @PostMapping({"getLoginInfo"})
   public GetLoginOutData getLoginInfo(@RequestBody JSONObject param) {
      String token = param.getString("token");
      GetLoginOutData loginOutData = this.getLoginUser(token);
      loginOutData.setFranchiseeList(null);
      log.info("getLoginInfo result:{}", JSON.toJSONString(loginOutData));
      return loginOutData;
   }

   @PostMapping({"selectButtonAuthObj"})
   public List<GetButtonAuthObjOutData> selectAuthObj(@RequestBody JSONObject param) {
      String token = param.getString("token");
      String parentId = param.getString("parentId");
      GetLoginOutData loginOutData = this.getLoginUser(token);
      List<GetButtonAuthObjOutData> result = this.loginService.selectAuthObj(parentId, loginOutData);
      return result;
   }

   @PostMapping({"createWorkflow"})
   public JsonResult createWorkflow(@RequestBody JSONObject param) {
      String token = param.getString("token");
      if (StrUtil.isBlank(token)) {
         throw new BusinessException("提示：token不能为空！");
      } else {
         GetLoginOutData loginOutData = this.getLoginUser(token);
         GetWfCreateInData wfCreateInData = param.toJavaObject(GetWfCreateInData.class);
         return JsonResult.success(this.workflowService.createWorkflow(wfCreateInData, loginOutData).getWfCode(), "提示：保存成功！");
      }
   }

   @PostMapping({"createWorkflowNew"})
   public JsonResult createWorkflowNew(@RequestBody JSONObject param) {
      GetLoginOutData loginOutData = new GetLoginOutData();
      loginOutData.setClient(param.getString("client"));
      loginOutData.setUserId(param.getString("userId"));
      GetWfCreateInData wfCreateInData = param.toJavaObject(GetWfCreateInData.class);
      return JsonResult.success(this.workflowService.createWorkflow(wfCreateInData, loginOutData).getWfCode(), "提示：保存成功！");
   }

   @PostMapping({"selectWorkflowApproveProcess"})
   public JsonResult selectWorkflowApproveProcess(@RequestBody JSONObject param) {
      GetWfCreateInData wfCreateInData = param.toJavaObject(GetWfCreateInData.class);
      log.info("selectWorkflowApproveProcess in:{}", JSON.toJSONString(wfCreateInData));
      List<GetWorkflowApproveOutData> list = workflowService.selectWorkflowApproveProcess(wfCreateInData);
      log.info("selectWorkflowApproveProcess result:{}", JSON.toJSONString(list));
      return JsonResult.success(list, "提示：保存成功！");
   }

   @PostMapping({"initMemberCard"})
   public void initMemberCard() {
      this.memberService.autoCreateVirtualCard();
   }
}
