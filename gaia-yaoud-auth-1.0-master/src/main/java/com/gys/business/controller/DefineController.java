package com.gys.business.controller;

import com.gys.business.service.DefineService;
import com.gys.business.service.data.GetDefineInData;
import com.gys.business.service.data.GetDefineOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/define/"})
public class DefineController extends BaseController {
   @Resource
   private DefineService defineService;

   @PostMapping({"/selectDefaultList"})
   public JsonResult selectDefaultList(HttpServletRequest request, @RequestBody GetDefineInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      List<GetDefineOutData> list = this.defineService.selectDefaultList(inData, userInfo);
      return JsonResult.success(list, "提示：获取数据成功！");
   }

   @PostMapping({"updateDefaultGroup"})
   public JsonResult updateDefaultGroup(HttpServletRequest request, @RequestBody GetDefineInData inData) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      this.defineService.updateDefaultGroup(inData, userInfo);
      return JsonResult.success("", "提示：保存成功！");
   }
}
