package com.gys.business.feign;

import com.gys.business.feign.fallback.StoreAppFallback;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "gys-store-app",
        fallback = StoreAppFallback.class
//        ,url = "172.19.1.120:10112"
)
public interface StoreAppService {
    @PostMapping({"/approval/callback"})
    JsonResult getApprovalResult(@RequestBody GetWfApproveInData inData);
}
