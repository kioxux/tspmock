package com.gys.business.feign.fallback;

import com.gys.business.feign.StoreWebService;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class StoreWebFallback implements StoreWebService {
    @Override
    public JsonResult getApprovalResult(GetWfApproveInData inData) {
        return null;
    }
}
