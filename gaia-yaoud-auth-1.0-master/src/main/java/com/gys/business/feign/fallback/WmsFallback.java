package com.gys.business.feign.fallback;

import com.gys.business.feign.WmsService;
import com.gys.business.service.data.GetDcInitInData;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.JsonResultWms;
import org.springframework.stereotype.Component;

@Component
public class WmsFallback implements WmsService {
   @Override
   public JsonResult getApprovalResult(GetWfApproveInData inData) {
      return null;
   }

   @Override
   public JsonResultWms dcInit(GetDcInitInData inData) {
      return null;
   }
}
