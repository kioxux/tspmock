package com.gys.business.feign;

import com.gys.business.feign.fallback.WmsFallback;
import com.gys.business.service.data.GetDcInitInData;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.JsonResultWms;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
   value = "wms",
   fallback = WmsFallback.class
//        ,url = "172.19.1.120:10109"
)
public interface WmsService {
   @PostMapping({"/web/api/v1/thirdPartyManager/external/approval/callback"})
   JsonResult getApprovalResult(@RequestBody GetWfApproveInData inData);

   @PostMapping({"/web/api/v1/thirdPartyManager/external/init/baseData"})
   JsonResultWms dcInit(@RequestBody GetDcInitInData inData);
}

