package com.gys.business.feign.fallback;

import com.gys.business.feign.StoreAppService;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class StoreAppFallback implements StoreAppService {
    @Override
    public JsonResult getApprovalResult(GetWfApproveInData inData) {
        return null;
    }
}
