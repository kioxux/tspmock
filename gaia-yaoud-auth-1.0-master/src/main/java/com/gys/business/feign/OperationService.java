package com.gys.business.feign;

import com.gys.business.feign.fallback.OperationFallback;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
   value = "gys-operation",
   fallback = OperationFallback.class
//        ,url = "172.19.1.120:10104"
)
public interface OperationService {
   @PostMapping({"/getApprovalResult"})
   JsonResult getApprovalResult(@RequestBody GetWfApproveInData inData);

   @PostMapping({"/unqualified/checkTodoList"})
   JsonResult checkTodoList(@RequestBody GetWfApproveInData inData);

   @PostMapping({"/unqualified/sendLossDefecpro"})
   JsonResult sendLossDefecpro(@RequestBody GetWfApproveInData inData);

   @PostMapping({"/salesReceipts/orderCommit"})
   JsonResult orderCommit(@RequestBody GetWfApproveInData inData);
}
