package com.gys.business.feign;

import com.gys.business.feign.fallback.StoreWebFallback;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "gys-store-web",
        fallback = StoreWebFallback.class
//        ,url = "172.19.1.120:10114"
)
public interface StoreWebService {
    @PostMapping({"/external/approval/callback"})
    JsonResult getApprovalResult(@RequestBody GetWfApproveInData inData);
}
