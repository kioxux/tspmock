package com.gys.business.feign.fallback;

import com.gys.business.feign.OperateService;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class OperateFallback implements OperateService {
   @Override
   public JsonResult payCallBack(GetWfApproveInData inData) {
      return null;
   }

   @Override
   public JsonResult approvePayment(GetWfApproveInData inData) {
      return null;
   }
}
