package com.gys.business.feign;

import com.gys.business.feign.fallback.PurchaseFallback;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
   value = "gys-purchase",
   fallback = PurchaseFallback.class
//                ,url = "172.19.1.120:10108"
)
public interface PurchaseService {
   @PostMapping({"/api/getApprovalResult"})
   JsonResult getApprovalResult(@RequestBody GetWfApproveInData inData);
}
