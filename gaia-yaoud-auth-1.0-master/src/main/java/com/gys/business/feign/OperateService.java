package com.gys.business.feign;

import com.gys.business.feign.fallback.OperateFallback;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
   value = "gys-operate",
   fallback = OperateFallback.class
//        ,url = "172.19.1.120:10113"
)
public interface OperateService {
   @PostMapping({"/invoice/supplierPayment/payCallBack"})
   JsonResult payCallBack(@RequestBody GetWfApproveInData inData);

   @PostMapping({"/invoice/paymentApply/approvePayment"})
   JsonResult approvePayment(@RequestBody GetWfApproveInData inData);
}
