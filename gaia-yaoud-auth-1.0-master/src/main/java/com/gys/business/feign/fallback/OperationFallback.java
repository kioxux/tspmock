package com.gys.business.feign.fallback;

import com.gys.business.feign.OperationService;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.data.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class OperationFallback implements OperationService {
   @Override
   public JsonResult getApprovalResult(GetWfApproveInData inData) {
      return null;
   }

   @Override
   public JsonResult checkTodoList(GetWfApproveInData inData) {
      return null;
   }

   @Override
   public JsonResult sendLossDefecpro(GetWfApproveInData inData) {
      return null;
   }

   @Override
   public JsonResult orderCommit(GetWfApproveInData inData) {
      return null;
   }
}
