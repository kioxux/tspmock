package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaZzData;
import com.gys.business.service.data.ZzInData;
import com.gys.business.service.data.ZzOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaZzDataMapper extends BaseMapper<GaiaZzData> {
   List<ZzOutData> getZzList(ZzInData inData);
}
