package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.common.base.BaseMapper;

public interface GaiaSdStoreDataMapper extends BaseMapper<GaiaSdStoreData> {
}
