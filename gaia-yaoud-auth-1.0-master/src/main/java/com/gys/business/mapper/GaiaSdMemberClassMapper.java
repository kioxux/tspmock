package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberClass;
import com.gys.common.base.BaseMapper;

public interface GaiaSdMemberClassMapper extends BaseMapper<GaiaSdMemberClass> {
}
