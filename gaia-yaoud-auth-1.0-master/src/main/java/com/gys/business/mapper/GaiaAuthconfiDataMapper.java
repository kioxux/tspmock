package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaAuthconfiData;
import com.gys.business.service.data.AuthInData;
import com.gys.business.service.data.AuthOutData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface GaiaAuthconfiDataMapper extends BaseMapper<GaiaAuthconfiData> {
   List<String> selectAuthStoreList(@Param("clientId") String clientId, @Param("userId") String userId);

   List<AuthOutData> selectAuthList(AuthInData inData);

   void deleteByType(@Param("client") String client, @Param("userList") List<String> userList, @Param("type") String type);

   void deleteByTypeAndSite(@Param("client") String client, @Param("userList") List<String> userList, @Param("type") String type, @Param("site") String site);

   void insertBatch(List<GaiaAuthconfiData> list);

    int selectCountByPath(Map inData);

   List<GaiaAuthconfiData> getOutStockRoleList(GetLoginOutData loginUser);
}
