package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaAuthConfigData;
import com.gys.business.service.data.auth.StoreRoleRes;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wu mao yin
 * @Description: 自定义权限分配
 * @date 2022/1/7 13:19
 */
public interface GaiaAuthConfigDataMapper extends BaseMapper<GaiaAuthConfigData> {

    /**
     * 查询用户门店权限
     *
     * @param client client
     * @param userId userId
     * @return List<StoreRoleRes>
     */
    List<StoreRoleRes> selectPermStoreByUserId(@Param("client") String client, @Param("userId") String userId);

    /**
     * 删除加盟商下指定用户的角色权限配置
     *
     * @param client   client
     * @param stoCodes stoCodes
     * @param userId   userId
     */
    void deleteAuthConfByUserId(@Param("client") String client,
                                @Param("stoCodes") List<String> stoCodes,
                                @Param("userId") String userId);

    /**
     * 批量新增权限分配
     *
     * @param list list
     */
    void insertBatch(List<GaiaAuthConfigData> list);

}
