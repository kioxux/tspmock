package com.gys.business.mapper;

import com.gys.business.mapper.entity.DefultParamData;
import com.gys.business.service.data.DefultParamInData;
import com.gys.business.service.data.DefultParamOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaDefaultParamMapper extends BaseMapper<DefultParamData> {
   List<DefultParamOutData> getDefaultParamList(DefultParamInData inData);
}
