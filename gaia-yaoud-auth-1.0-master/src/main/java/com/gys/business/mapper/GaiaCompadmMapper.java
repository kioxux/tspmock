package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaCompadm;
import com.gys.business.service.data.CompadmInData;
import com.gys.business.service.data.CompadmOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

import com.gys.common.data.SelectModel;
import org.apache.ibatis.annotations.Param;

public interface GaiaCompadmMapper extends BaseMapper<GaiaCompadm> {
   int selectMaxId(@Param("client") String client);

   List<CompadmOutData> getCompadmList(CompadmInData inData);

    List<CompadmOutData> compadmInfoByClientId(@Param("client") String client, @Param("compadmId")String compadmId);

    List<SelectModel> getCompadmAndCompadmWmsAndStore(CompadmInData inData);
}
