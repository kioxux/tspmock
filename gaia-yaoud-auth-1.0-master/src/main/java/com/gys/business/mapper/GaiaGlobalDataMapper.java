package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaGlobalData;
import com.gys.common.base.BaseMapper;

public interface GaiaGlobalDataMapper extends BaseMapper<GaiaGlobalData> {
}
