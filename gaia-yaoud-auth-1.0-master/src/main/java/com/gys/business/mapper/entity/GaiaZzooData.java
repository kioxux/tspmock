package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_ZZOO_DATA"
)
public class GaiaZzooData implements Serializable {
   @Id
   @Column(
      name = "ZZ_ID"
   )
   private String zzId;
   @Column(
      name = "ZZ_NAME"
   )
   private String zzName;
   @Column(
      name = "ZZ_CRE_DATE"
   )
   private String zzCreDate;
   @Column(
      name = "ZZ_CRE_TIME"
   )
   private String zzCreTime;
   @Column(
      name = "ZZ_CRE_ID"
   )
   private String zzCreId;
   @Column(
      name = "ZZ_MODI_DATE"
   )
   private String zzModiDate;
   @Column(
      name = "ZZ_MODI_TIME"
   )
   private String zzModiTime;
   @Column(
      name = "ZZ_MODI_ID"
   )
   private String zzModiId;
   private static final long serialVersionUID = 1L;

   public String getZzId() {
      return this.zzId;
   }

   public void setZzId(String zzId) {
      this.zzId = zzId;
   }

   public String getZzName() {
      return this.zzName;
   }

   public void setZzName(String zzName) {
      this.zzName = zzName;
   }

   public String getZzCreDate() {
      return this.zzCreDate;
   }

   public void setZzCreDate(String zzCreDate) {
      this.zzCreDate = zzCreDate;
   }

   public String getZzCreTime() {
      return this.zzCreTime;
   }

   public void setZzCreTime(String zzCreTime) {
      this.zzCreTime = zzCreTime;
   }

   public String getZzCreId() {
      return this.zzCreId;
   }

   public void setZzCreId(String zzCreId) {
      this.zzCreId = zzCreId;
   }

   public String getZzModiDate() {
      return this.zzModiDate;
   }

   public void setZzModiDate(String zzModiDate) {
      this.zzModiDate = zzModiDate;
   }

   public String getZzModiTime() {
      return this.zzModiTime;
   }

   public void setZzModiTime(String zzModiTime) {
      this.zzModiTime = zzModiTime;
   }

   public String getZzModiId() {
      return this.zzModiId;
   }

   public void setZzModiId(String zzModiId) {
      this.zzModiId = zzModiId;
   }
}
