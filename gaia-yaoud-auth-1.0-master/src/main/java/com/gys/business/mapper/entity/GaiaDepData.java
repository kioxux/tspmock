package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_DEP_DATA"
)
@Data
public class GaiaDepData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "DEP_ID"
   )
   private String depId;
   @Column(
      name = "DEP_NAME"
   )
   private String depName;
   @Column(
      name = "DEP_HEAD_ID"
   )
   private String depHeadId;
   @Column(
      name = "DEP_HEAD_NAM"
   )
   private String depHeadNam;
   @Column(
      name = "DEP_CRE_DATE"
   )
   private String depCreDate;
   @Column(
      name = "DEP_CRE_TIME"
   )
   private String depCreTime;
   @Column(
      name = "DEP_CRE_ID"
   )
   private String depCreId;
   @Column(
      name = "DEP_MODI_DATE"
   )
   private String depModiDate;
   @Column(
      name = "DEP_MODI_TIME"
   )
   private String depModiTime;
   @Column(
      name = "DEP_MODI_ID"
   )
   private String depModiId;
   @Column(
      name = "DEP_DIS_DATE"
   )
   private String depDisDate;
   @Column(
      name = "STO_CHAIN_HEAD"
   )
   private String stoChainHead;
   @Column(
      name = "DEP_TYPE"
   )
   private String depType;
   @Column(
      name = "DEP_STATUS"
   )
   private String depStatus;
   @Column(
           name = "DEP_CLS"
   )
   private String depCls;
   private static final long serialVersionUID = 1L;


}
