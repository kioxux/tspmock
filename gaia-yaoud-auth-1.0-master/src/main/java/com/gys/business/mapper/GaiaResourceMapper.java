package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaResource;
import com.gys.business.service.data.auth.AuthTree;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface GaiaResourceMapper extends BaseMapper<GaiaResource> {
    List<GaiaResource> queryResByUser(GetLoginOutData outData);

    List<AuthTree> queryResByUserV2(GetLoginOutData loginOutData);
}
