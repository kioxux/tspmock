package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author flynn
 * @since 2021-12-27
 */
@Data
@Table( name = "GAIA_AREA")
public class Area implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "地区Id")
    @Id
    @Column(name = "ID")
    private String areaId;

    @ApiModelProperty(value = "地区名")
    @Column(name = "AREA_NAME")
    private String areaName;

    @ApiModelProperty(value = "地区级别（1:省份province,2:市city,3:区县district,4:街道street）")
    @Column(name = "LEVEL")
    private String level;

    @ApiModelProperty(value = "地区父节点")
    @Column(name = "PARENT_ID")
    private String parentId;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;


}
