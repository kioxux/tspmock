package com.gys.business.mapper;

import com.gys.business.mapper.entity.RoleAuth;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 自定义角色与权限关联表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-12-22
 */
@Mapper
public interface RoleAuthMapper extends BaseMapper<RoleAuth> {
    Integer selectBasicRefNum(@Param(value = "basicRoleId") String basicRoleId);

    /**
     * 根据角色获取对应的权限id集合
     *
     * @param roleId roleId
     * @return List<String>
     */
    List<String> selectAuthIdByRoleId(String roleId);

    void deleteByRoleId(@Param("roleId") String roleId);
}
