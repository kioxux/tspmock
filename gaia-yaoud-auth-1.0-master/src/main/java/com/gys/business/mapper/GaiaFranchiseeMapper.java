package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginFrancOutData;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaFranchiseeMapper extends BaseMapper<GaiaFranchisee> {
   Integer selectMaxId();

   List<GetLoginFrancOutData> selectUserFrancList(@Param("phone") String phone);
}
