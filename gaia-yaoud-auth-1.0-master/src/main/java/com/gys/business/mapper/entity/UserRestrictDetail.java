package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 用户数据范围权限明细表
 * </p>
 *
 * @author flynn
 * @since 2021-12-27
 */
@Data
@Table( name = "GAIA_USER_RESTRICT_DETAIL")
public class UserRestrictDetail implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户数据范围权限ID")
//    @Id
    @Column(name = "RESTRICT_ID")
    private Integer restrictId;

    @ApiModelProperty(value = "公司类型，如果是片区，设定为空字符串 1-4按门店属性保持一致5代表仓库 6代表部门 7代表区域")
    @Column(name = "COMP_TYPE")
    private String compType;

    @ApiModelProperty(value = "公司代码或区域代码")
    @Column(name = "COMP_CODE")
    private String compCode;

    @ApiModelProperty(value = "是否删除（0不删除 1删除）")
    @Column(name = "DELETE_FLAG")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人ID")
    @Column(name = "MOD_ID")
    private String modId;


}
