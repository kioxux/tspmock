package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author wu mao yin
 * @Description: 自定义权限分配
 * @date 2022/1/7 13:18
 */
@Data
@Table(name = "GAIA_AUTH_CONFIG_DATA")
public class GaiaAuthConfigData implements Serializable {
    @Id
    @Column(name = "CLIENT")
    private String client;

    @Id
    @Column(name = "AUTH_GROUP_ID")
    private String authGroupId;

    @Column(name = "AUTH_GROUP_NAME")
    private String authGroupName;

    @Id
    @Column(name = "AUTHOBJ_SITE")
    private String authobjSite;

    @Id
    @Column(name = "AUTHCONFI_USER")
    private String authconfiUser;

    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    @Column(name = "CRE_DATE")
    private String creDate;

    @Column(name = "CRE_TIME")
    private String creTime;

    @Column(name = "CRE_ID")
    private String creId;

    @Column(name = "MOD_DATE")
    private String modDate;

    @Column(name = "MOD_TIME")
    private String modTime;

    @Column(name = "MOD_ID")
    private String modId;

    private static final long serialVersionUID = 1L;

}
