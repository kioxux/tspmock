package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "GAIA_SD_STOCK_BATCH")
public class GaiaSdStockBatch implements Serializable {
    private static final long serialVersionUID = -2863707445983923515L;
    /**
     * 加盟号
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 店号
     */
    @Id
    @Column(name = "GSSB_BR_ID")
    private String gssbBrId;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSSB_PRO_ID")
    private String gssbProId;

    /**
     * 批号
     */
    @Id
    @Column(name = "GSSB_BATCH_NO")
    private String gssbBatchNo;

    /**
     * 批次
     */
    @Id
    @Column(name = "GSSB_BATCH")
    private String gssbBatch;

    /**
     * 数量
     */
    @Column(name = "GSSB_QTY")
    private String gssbQty;

    /**
     * 有效期
     */
    @Column(name = "GSSB_VAILD_DATE")
    private String gssbVaildDate;

    /**
     * 更新日期
     */
    @Column(name = "GSSB_UPDATE_DATE")
    private String gssbUpdateDate;

    /**
     * 更新人员编码
     */
    @Column(name = "GSSB_UPDATE_EMP")
    private String gssbUpdateEmp;
}
