package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSet;
import com.gys.business.service.data.ExchangeInData;
import com.gys.business.service.data.ExchangeOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdIntegralExchangeSetMapper extends BaseMapper<GaiaSdIntegralExchangeSet> {
   String selectMaxId(@Param("client") String client, @Param("codePre") String codePre);

   List<ExchangeOutData> exchangeSetList(ExchangeInData inData);
}
