package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaStoreDataMapper extends BaseMapper<GaiaStoreData> {
   int selectMaxId(@Param("client") String client);

   List<StoreOutData> getStoreList(StoreInData inData);

   List<GetStoListOutData> getStoListByClientId(StoreInData inData);

   List<GetUserStoreOutData> selectUserStoreList(@Param("userIds") List<GaiaUserData> userIds);

   List<GaiaStoreData> selectListForAuth(@Param("client") String client, @Param("chainHead") String chainHead);

   String getStoreName(@Param("client") String client, @Param("stoCode") String stoCode);

   GetUserStoreOutData getUserStoreByBrId(@Param("client") String client, @Param("stoCode") String stoCode);

   /**
    * 获取门店集合
    *
    * @param client  client
    * @param stoCode stoCode
    * @return List<GetUserStoreOutData>
    */
   List<GetUserStoreOutData> selectStoreByCondition(@Param("client") String client, @Param("stoCode") String stoCode);


   List<StoreOutData> getStoreListByUserId(@Param("client")String client,@Param("stoCode")String stoCode, @Param("userId")String userId,@Param("stoAttribute")String stoAttribute,@Param("stoName")String stoName,@Param("stoProvince")String stoProvince,@Param("stoCity")String stoCity,@Param("districts")List<String> districts);

   StoreRestrictBasicRes getStoreByStoCode(@Param("client")String client, @Param("stoCode") String stoCode);

   List<StoreRestrictBasicRes> getAllStoreList(@Param("client")String client);

   List<StoreRestrictBasicRes> getStoreInRegion(@Param("client")String client, @Param("restrictId")Integer restrictId);

   List<StoreRestrictBasicRes> getStoreAppoint(@Param("client")String client, @Param("restrictId")Integer restrictId);
}
