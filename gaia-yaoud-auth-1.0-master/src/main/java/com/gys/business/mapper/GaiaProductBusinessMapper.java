package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.common.base.BaseMapper;

public interface GaiaProductBusinessMapper extends BaseMapper<GaiaProductBusiness> {
}
