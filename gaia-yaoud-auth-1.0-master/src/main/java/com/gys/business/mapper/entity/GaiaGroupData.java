package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_GROUP_DATA"
)
public class GaiaGroupData implements Serializable {
   @Id
   @Column(
      name = "AUTH_GROUP_ID"
   )
   private String authGroupId;
   @Id
   @Column(
      name = "AUTHOBJ_ID"
   )
   private String authobjId;
   @Column(
      name = "AUTH_GROUP_NAME"
   )
   private String authGroupName;
   @Column(
      name = "AUTHOBJ_NAME"
   )
   private String authobjName;
   @Column(
      name = "AUTHOBJ_SITE"
   )
   private String authobjSite;
   private static final long serialVersionUID = 1L;

   public String getAuthGroupId() {
      return this.authGroupId;
   }

   public void setAuthGroupId(String authGroupId) {
      this.authGroupId = authGroupId;
   }

   public String getAuthobjId() {
      return this.authobjId;
   }

   public void setAuthobjId(String authobjId) {
      this.authobjId = authobjId;
   }

   public String getAuthGroupName() {
      return this.authGroupName;
   }

   public void setAuthGroupName(String authGroupName) {
      this.authGroupName = authGroupName;
   }

   public String getAuthobjName() {
      return this.authobjName;
   }

   public void setAuthobjName(String authobjName) {
      this.authobjName = authobjName;
   }

   public String getAuthobjSite() {
      return this.authobjSite;
   }

   public void setAuthobjSite(String authobjSite) {
      this.authobjSite = authobjSite;
   }
}
