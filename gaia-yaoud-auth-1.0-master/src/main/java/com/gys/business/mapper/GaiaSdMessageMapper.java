package com.gys.business.mapper;
import com.gys.business.mapper.entity.GaiaSdMessage;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdMessageMapper {

    int insertSelective(GaiaSdMessage record);

    String selectNextVoucherId(@Param("clientId") String clientId, @Param("type") String type);

    String selectNextVoucherIdFX(@Param("clientId") String clientId,@Param("type") String type);
}