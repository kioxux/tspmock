package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_SD_MEMBER_BASIC"
)
public class GaiaSdMemberBasic implements Serializable {
   @Id
   @Column(
      name = "GSMB_CARD_ID"
   )
   private String gsmbCardId;
   @Id
   @Column(
      name = "CLIENT"
   )
   private String clientId;
   @Column(
      name = "GSMB_BR_ID"
   )
   private String gsmbBrId;
   @Column(
      name = "GSMB_NAME"
   )
   private String gsmbName;
   @Column(
      name = "GSMB_ADDRESS"
   )
   private String gsmbAddress;
   @Column(
      name = "GSMB_MOBILE"
   )
   private String gsmbMobile;
   @Column(
      name = "GSMB_TEL"
   )
   private String gsmbTel;
   @Column(
      name = "GSMB_SEX"
   )
   private String gsmbSex;
   @Column(
      name = "GSMB_CREDENTIALS"
   )
   private String gsmbCredentials;
   @Column(
      name = "GSMB_AGE"
   )
   private String gsmbAge;
   @Column(
      name = "GSMB_BIRTH"
   )
   private String gsmbBirth;
   @Column(
      name = "GSMB_BB_NAME"
   )
   private String gsmbBbName;
   @Column(
      name = "GSMB_BB_SEX"
   )
   private String gsmbBbSex;
   @Column(
      name = "GSMB_BB_AGE"
   )
   private String gsmbBbAge;
   @Column(
      name = "GSMB_CHRONIC_DISEASE_TYPE"
   )
   private String gsmbChronicDiseaseType;
   @Column(
      name = "GSMB_CONTACT_ALLOWED"
   )
   private String gsmbContactAllowed;
   @Column(
      name = "GSMB_CHANNEL"
   )
   private String gsmbChannel;
   @Column(
      name = "GSMB_CLASS_ID"
   )
   private String gsmbClassId;
   @Column(
      name = "GSMB_INTEGRAL"
   )
   private String gsmbIntegral;
   @Column(
      name = "GSMB_INTEGRAL_LASTDATE"
   )
   private String gsmbIntegralLastdate;
   @Column(
      name = "GSMB_ZERO_DATE"
   )
   private String gsmbZeroDate;
   @Column(
      name = "GSMB_STATUS"
   )
   private String gsmbStatus;
   @Column(
      name = "GSMB_UPDATE_BR_ID"
   )
   private String gsmbUpdateBrId;
   @Column(
      name = "GSMB_UPDATE_SALER"
   )
   private String gsmbUpdateSaler;
   @Column(
      name = "GSMB_UPDATE_DATE"
   )
   private String gsmbUpdateDate;
   @Column(
      name = "GSMB_CREATE_DATA"
   )
   private String gsmbCreateData;
   @Column(
      name = "GSMB_CREATE_SALER"
   )
   private String gsmbCreateSaler;
   @Column(
      name = "GSMB_REMARK"
   )
   private String gsmbRemark;
   @Column(
      name = "GSMB_CREAT_DATE"
   )
   private String gsmbCreatDate;
   @Column(
      name = "GSMB_TYPE"
   )
   private String gsmbType;
   private static final long serialVersionUID = 1L;

   public String getGsmbCardId() {
      return this.gsmbCardId;
   }

   public void setGsmbCardId(String gsmbCardId) {
      this.gsmbCardId = gsmbCardId;
   }

   public String getClientId() {
      return this.clientId;
   }

   public void setClientId(String clientId) {
      this.clientId = clientId;
   }

   public String getGsmbBrId() {
      return this.gsmbBrId;
   }

   public void setGsmbBrId(String gsmbBrId) {
      this.gsmbBrId = gsmbBrId;
   }

   public String getGsmbName() {
      return this.gsmbName;
   }

   public void setGsmbName(String gsmbName) {
      this.gsmbName = gsmbName;
   }

   public String getGsmbAddress() {
      return this.gsmbAddress;
   }

   public void setGsmbAddress(String gsmbAddress) {
      this.gsmbAddress = gsmbAddress;
   }

   public String getGsmbMobile() {
      return this.gsmbMobile;
   }

   public void setGsmbMobile(String gsmbMobile) {
      this.gsmbMobile = gsmbMobile;
   }

   public String getGsmbTel() {
      return this.gsmbTel;
   }

   public void setGsmbTel(String gsmbTel) {
      this.gsmbTel = gsmbTel;
   }

   public String getGsmbSex() {
      return this.gsmbSex;
   }

   public void setGsmbSex(String gsmbSex) {
      this.gsmbSex = gsmbSex;
   }

   public String getGsmbCredentials() {
      return this.gsmbCredentials;
   }

   public void setGsmbCredentials(String gsmbCredentials) {
      this.gsmbCredentials = gsmbCredentials;
   }

   public String getGsmbAge() {
      return this.gsmbAge;
   }

   public void setGsmbAge(String gsmbAge) {
      this.gsmbAge = gsmbAge;
   }

   public String getGsmbBirth() {
      return this.gsmbBirth;
   }

   public void setGsmbBirth(String gsmbBirth) {
      this.gsmbBirth = gsmbBirth;
   }

   public String getGsmbBbName() {
      return this.gsmbBbName;
   }

   public void setGsmbBbName(String gsmbBbName) {
      this.gsmbBbName = gsmbBbName;
   }

   public String getGsmbBbSex() {
      return this.gsmbBbSex;
   }

   public void setGsmbBbSex(String gsmbBbSex) {
      this.gsmbBbSex = gsmbBbSex;
   }

   public String getGsmbBbAge() {
      return this.gsmbBbAge;
   }

   public void setGsmbBbAge(String gsmbBbAge) {
      this.gsmbBbAge = gsmbBbAge;
   }

   public String getGsmbChronicDiseaseType() {
      return this.gsmbChronicDiseaseType;
   }

   public void setGsmbChronicDiseaseType(String gsmbChronicDiseaseType) {
      this.gsmbChronicDiseaseType = gsmbChronicDiseaseType;
   }

   public String getGsmbContactAllowed() {
      return this.gsmbContactAllowed;
   }

   public void setGsmbContactAllowed(String gsmbContactAllowed) {
      this.gsmbContactAllowed = gsmbContactAllowed;
   }

   public String getGsmbChannel() {
      return this.gsmbChannel;
   }

   public void setGsmbChannel(String gsmbChannel) {
      this.gsmbChannel = gsmbChannel;
   }

   public String getGsmbClassId() {
      return this.gsmbClassId;
   }

   public void setGsmbClassId(String gsmbClassId) {
      this.gsmbClassId = gsmbClassId;
   }

   public String getGsmbIntegral() {
      return this.gsmbIntegral;
   }

   public void setGsmbIntegral(String gsmbIntegral) {
      this.gsmbIntegral = gsmbIntegral;
   }

   public String getGsmbIntegralLastdate() {
      return this.gsmbIntegralLastdate;
   }

   public void setGsmbIntegralLastdate(String gsmbIntegralLastdate) {
      this.gsmbIntegralLastdate = gsmbIntegralLastdate;
   }

   public String getGsmbZeroDate() {
      return this.gsmbZeroDate;
   }

   public void setGsmbZeroDate(String gsmbZeroDate) {
      this.gsmbZeroDate = gsmbZeroDate;
   }

   public String getGsmbStatus() {
      return this.gsmbStatus;
   }

   public void setGsmbStatus(String gsmbStatus) {
      this.gsmbStatus = gsmbStatus;
   }

   public String getGsmbUpdateBrId() {
      return this.gsmbUpdateBrId;
   }

   public void setGsmbUpdateBrId(String gsmbUpdateBrId) {
      this.gsmbUpdateBrId = gsmbUpdateBrId;
   }

   public String getGsmbUpdateSaler() {
      return this.gsmbUpdateSaler;
   }

   public void setGsmbUpdateSaler(String gsmbUpdateSaler) {
      this.gsmbUpdateSaler = gsmbUpdateSaler;
   }

   public String getGsmbUpdateDate() {
      return this.gsmbUpdateDate;
   }

   public void setGsmbUpdateDate(String gsmbUpdateDate) {
      this.gsmbUpdateDate = gsmbUpdateDate;
   }

   public String getGsmbCreateData() {
      return this.gsmbCreateData;
   }

   public void setGsmbCreateData(String gsmbCreateData) {
      this.gsmbCreateData = gsmbCreateData;
   }

   public String getGsmbCreateSaler() {
      return this.gsmbCreateSaler;
   }

   public void setGsmbCreateSaler(String gsmbCreateSaler) {
      this.gsmbCreateSaler = gsmbCreateSaler;
   }

   public String getGsmbRemark() {
      return this.gsmbRemark;
   }

   public void setGsmbRemark(String gsmbRemark) {
      this.gsmbRemark = gsmbRemark;
   }

   public String getGsmbCreatDate() {
      return this.gsmbCreatDate;
   }

   public void setGsmbCreatDate(String gsmbCreatDate) {
      this.gsmbCreatDate = gsmbCreatDate;
   }

   public String getGsmbType() {
      return this.gsmbType;
   }

   public void setGsmbType(String gsmbType) {
      this.gsmbType = gsmbType;
   }
}
