package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(
   name = "GAIA_USER_DATA"
)
public class GaiaUserData implements Serializable {
   @Id
   @Column(
      name = "USER_ID"
   )
   private String userId;
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Column(
      name = "USER_PASSWORD"
   )
   private String userPassword;
   @Column(
      name = "USER_NAM"
   )
   private String userNam;
   @Column(
      name = "USER_SEX"
   )
   private String userSex;
   @Column(
      name = "USER_TEL"
   )
   private String userTel;
   @Column(
      name = "USER_IDC"
   )
   private String userIdc;
   @Column(
      name = "USER_YS_ID"
   )
   private String userYsId;
   @Column(
      name = "USER_YS_ZYFW"
   )
   private String userYsZyfw;
   @Column(
      name = "USER_YS_ZYLB"
   )
   private String userYsZylb;
   @Column(
      name = "DEP_ID"
   )
   private String depId;
   @Column(
      name = "DEP_NAME"
   )
   private String depName;
   @Column(
      name = "USER_ADDR"
   )
   private String userAddr;
   @Column(
      name = "USER_STA"
   )
   private String userSta;
   @Column(
      name = "USER_CRE_DATE"
   )
   private String userCreDate;
   @Column(
      name = "USER_CRE_TIME"
   )
   private String userCreTime;
   @Column(
      name = "USER_CRE_ID"
   )
   private String userCreId;
   @Column(
      name = "USER_MODI_DATE"
   )
   private String userModiDate;
   @Column(
      name = "USER_MODI_TIME"
   )
   private String userModiTime;
   @Column(
      name = "USER_MODI_ID"
   )
   private String userModiId;
   @Column(
      name = "USER_JOIN_DATE"
   )
   private String userJoinDate;
   @Column(
      name = "USER_DIS_DATE"
   )
   private String userDisDate;
   @Column(
      name = "USER_EMAIL"
   )
   private String userEmail;
   @Column(
      name = "USER_LOGIN_STA"
   )
   private String userLoginSta;

   @Column(
           name = "USER_TYPE"
   )
   private Integer userType;

   @Column(
           name = "USER_YBLPA_ID"
   )
   private String userYblpaId;
   @Column(name = "DEFAULT_SITE")
   private String defaultSite;

   @Column(
           name = "USER_SOLE_PHARMACIST_NO"
   )
   private String userSolePharmacistNo;

   @Column(
           name = "USER_SOLE_SALE_NO"
   )
   private String userSoleSaleNo;

   @Column(
           name = "USER_SIGN_ADDS"
   )
   private String userSignAdds;

   public Integer getUserType() {
      return userType;
   }

   public void setUserType(Integer userType) {
      this.userType = userType;
   }

   private static final long serialVersionUID = 1L;

   public String getUserId() {
      return this.userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getUserPassword() {
      return this.userPassword;
   }

   public void setUserPassword(String userPassword) {
      this.userPassword = userPassword;
   }

   public String getUserNam() {
      return this.userNam;
   }

   public void setUserNam(String userNam) {
      this.userNam = userNam;
   }

   public String getUserSex() {
      return this.userSex;
   }

   public void setUserSex(String userSex) {
      this.userSex = userSex;
   }

   public String getUserTel() {
      return this.userTel;
   }

   public void setUserTel(String userTel) {
      this.userTel = userTel;
   }

   public String getUserIdc() {
      return this.userIdc;
   }

   public void setUserIdc(String userIdc) {
      this.userIdc = userIdc;
   }

   public String getUserYsId() {
      return this.userYsId;
   }

   public void setUserYsId(String userYsId) {
      this.userYsId = userYsId;
   }

   public String getUserYsZyfw() {
      return this.userYsZyfw;
   }

   public void setUserYsZyfw(String userYsZyfw) {
      this.userYsZyfw = userYsZyfw;
   }

   public String getUserYsZylb() {
      return this.userYsZylb;
   }

   public void setUserYsZylb(String userYsZylb) {
      this.userYsZylb = userYsZylb;
   }

   public String getDepId() {
      return this.depId;
   }

   public void setDepId(String depId) {
      this.depId = depId;
   }

   public String getDepName() {
      return this.depName;
   }

   public void setDepName(String depName) {
      this.depName = depName;
   }

   public String getUserAddr() {
      return this.userAddr;
   }

   public void setUserAddr(String userAddr) {
      this.userAddr = userAddr;
   }

   public String getUserSta() {
      return this.userSta;
   }

   public void setUserSta(String userSta) {
      this.userSta = userSta;
   }

   public String getUserCreDate() {
      return this.userCreDate;
   }

   public void setUserCreDate(String userCreDate) {
      this.userCreDate = userCreDate;
   }

   public String getUserCreTime() {
      return this.userCreTime;
   }

   public void setUserCreTime(String userCreTime) {
      this.userCreTime = userCreTime;
   }

   public String getUserCreId() {
      return this.userCreId;
   }

   public void setUserCreId(String userCreId) {
      this.userCreId = userCreId;
   }

   public String getUserModiDate() {
      return this.userModiDate;
   }

   public void setUserModiDate(String userModiDate) {
      this.userModiDate = userModiDate;
   }

   public String getUserModiTime() {
      return this.userModiTime;
   }

   public void setUserModiTime(String userModiTime) {
      this.userModiTime = userModiTime;
   }

   public String getUserModiId() {
      return this.userModiId;
   }

   public void setUserModiId(String userModiId) {
      this.userModiId = userModiId;
   }

   public String getUserJoinDate() {
      return this.userJoinDate;
   }

   public void setUserJoinDate(String userJoinDate) {
      this.userJoinDate = userJoinDate;
   }

   public String getUserDisDate() {
      return this.userDisDate;
   }

   public void setUserDisDate(String userDisDate) {
      this.userDisDate = userDisDate;
   }

   public String getUserEmail() {
      return this.userEmail;
   }

   public void setUserEmail(String userEmail) {
      this.userEmail = userEmail;
   }

   public String getUserLoginSta() {
      return this.userLoginSta;
   }

   public void setUserLoginSta(String userLoginSta) {
      this.userLoginSta = userLoginSta;
   }

   public String getUserYblpaId() {
      return userYblpaId;
   }

   public void setUserYblpaId(String userYblpaId) {
      this.userYblpaId = userYblpaId;
   }

   public String getDefaultSite() {
      return defaultSite;
   }

   public void setDefaultSite(String defaultSite) {
      this.defaultSite = defaultSite;
   }

   public String getUserSolePharmacistNo() {
      return userSolePharmacistNo;
   }

   public void setUserSolePharmacistNo(String userSolePharmacistNo) {
      this.userSolePharmacistNo = userSolePharmacistNo;
   }

   public String getUserSoleSaleNo() {
      return userSoleSaleNo;
   }

   public void setUserSoleSaleNo(String userSoleSaleNo) {
      this.userSoleSaleNo = userSoleSaleNo;
   }

   public String getUserSignAdds() {
      return userSignAdds;
   }

   public void setUserSignAdds(String userSignAdds) {
      this.userSignAdds = userSignAdds;
   }
}
