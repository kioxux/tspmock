package com.gys.business.mapper.entity;

import lombok.Data;

import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/16 13:50
 */
@Data
public class UserResourceRecord {
    private Long id;
    private String client;
    private String userId;
    private String userName;
    private String pageId;
    private String pagePath;
    private Integer deleteFlag;
    private Date createTime;
}
