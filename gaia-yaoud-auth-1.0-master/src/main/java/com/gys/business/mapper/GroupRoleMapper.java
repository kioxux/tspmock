package com.gys.business.mapper;

import com.gys.business.mapper.entity.GroupRole;
import com.gys.business.service.data.auth.StoreRoleRes;
import com.gys.business.service.data.auth.StroeRoleDelDetail;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 自定义角色表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-12-22
 */
@Mapper
public interface GroupRoleMapper extends BaseMapper<GroupRole> {
    List<StoreRoleRes> getStoreListRoleNum(String client);

    void deleteByStoCoceAndRoles(@Param("client") String client, @Param("stoCode") String stoCode, @Param("roles") List<StroeRoleDelDetail> roles);

    List<GroupRole> getRepeatNameCountInStores(@Param("client") String client, @Param("roleName") String roleName, @Param("stoCodes") List<String> stoCodes, @Param("selfId") String selfId);

    void updateRoleName(GroupRole groupRole);

    Integer selectAllGADRoleNumInClient(String client);
}
