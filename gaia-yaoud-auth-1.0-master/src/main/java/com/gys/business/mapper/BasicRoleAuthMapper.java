package com.gys.business.mapper;

import com.gys.business.mapper.entity.BasicRoleAuth;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 默认角色与权限关联表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-12-22
 */
@Mapper
public interface BasicRoleAuthMapper extends BaseMapper<BasicRoleAuth> {

    /**
     * 根据角色获取对应的权限id集合
     *
     * @param roleId roleId
     * @return List<String>
     */
    List<String> selectAuthIdByBasicRoleId(String roleId);

}
