package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_AUTHOBJ_DATA"
)
public class GaiaAuthobjData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "AUTHOBJ_ID"
   )
   private String authobjId;
   @Column(
      name = "AUTHOBJ_NAME"
   )
   private String authobjName;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getAuthobjId() {
      return this.authobjId;
   }

   public void setAuthobjId(String authobjId) {
      this.authobjId = authobjId;
   }

   public String getAuthobjName() {
      return this.authobjName;
   }

   public void setAuthobjName(String authobjName) {
      this.authobjName = authobjName;
   }
}
