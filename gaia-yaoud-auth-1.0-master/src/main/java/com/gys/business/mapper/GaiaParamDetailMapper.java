package com.gys.business.mapper;

import com.gys.business.mapper.entity.ParamDetailData;
import com.gys.business.service.data.ParamDetailInData;
import com.gys.business.service.data.ParamDetailOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaParamDetailMapper extends BaseMapper<ParamDetailData> {
   List<ParamDetailOutData> getParamDetailList(ParamDetailInData inData);

   List<ParamDetailOutData> getDefaultParam(ParamDetailInData inData);
}
