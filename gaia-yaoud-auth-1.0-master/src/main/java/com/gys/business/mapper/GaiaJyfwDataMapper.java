package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaJyfwData;
import com.gys.business.service.data.JyfwInData;
import com.gys.business.service.data.JyfwOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaJyfwDataMapper extends BaseMapper<GaiaJyfwData> {
   List<JyfwOutData> getJyfwList(JyfwInData inData);
}
