package com.gys.business.mapper;


import com.gys.business.mapper.entity.Area;
import com.gys.business.service.data.AreaDto;
import com.gys.business.service.data.auth.AreaStoreNumVo;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAreaMapper extends BaseMapper<Area> {

    List<AreaDto> getAreaList();

    List<AreaDto> getAreaListNew(@Param("client") String client);

    List<Area> selectByName(String areaName);

    List<AreaStoreNumVo> getAreaStoreNumVoList(@Param("client") String client);

}