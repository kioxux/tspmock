package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

import javax.persistence.Table;

/**
 * GAIA_SD_INCOME_STATEMENT_H
 * @author 
 */
@Data
@Table( name = "GAIA_SD_INCOME_STATEMENT_H")
public class GaiaSdIncomeStatementH extends GaiaSdIncomeStatementHKey implements Serializable {
    /**
     * 门店
     */
    private String gsishBrId;

    /**
     * 生成日期
     */
    private String gsishDate;

    /**
     * 损益类型 1盘点转入 2批号养护 3门店领用 4门店报损 5不合格品报损 6门店报溢 
     */
    private String gsishIsType;

    /**
     * 单据类型
     */
    private String gsishTableType;

    /**
     * 合计金额
     */
    private BigDecimal gsishTotalQty;

    /**
     * 合计数量
     */
    private BigDecimal gsishTotalAmt;

    /**
     * 备注
     */
    private String gsishRemark;

    /**
     * 审核日期
     */
    private String gsishExamineDate;

    /**
     * 审核人员
     */
    private String gsishExamineEmp;

    /**
     * 审核状态：0未审核，1审核未批准，2已审核
     */
    private String gsishStatus;

    /**
     * 盘点差异单号
     */
    private String gsishPcdVoucherId;

    /**
     * 盘点单号
     */
    private String gsishPcVoucherId;

    /**
     * 销毁状态(0 未申请 1 已申请 2 审批不同意)
     */
    private String gsishXhzt;

    /**
     * 附件信息
     */
    private String gsishFj;

    /**
     * 领用部门
     */
    private String gsishBranch;

    private static final long serialVersionUID = 1L;
}