package com.gys.business.mapper;

import com.gys.business.mapper.entity.UserResourceRecord;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/16 14:01
 */
public interface GaiaUserResourceRecordMapper {

    UserResourceRecord getById(Long id);

    int add(UserResourceRecord userResourceRecord);

    UserResourceRecord getUnique(UserResourceRecord cond);
}
