package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductBasic;
import com.gys.common.base.BaseMapper;

public interface GaiaProductBasicMapper extends BaseMapper<GaiaProductBasic> {
}
