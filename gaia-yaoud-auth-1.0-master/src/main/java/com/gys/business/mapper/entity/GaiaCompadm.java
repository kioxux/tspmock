package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_COMPADM"
)
public class GaiaCompadm implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "COMPADM_ID"
   )
   private String compadmId;
   @Column(
      name = "COMPADM_NAME"
   )
   private String compadmName;
   @Column(
      name = "COMPADM_NO"
   )
   private String compadmNo;
   @Column(
      name = "COMPADM_LEGAL_PERSON"
   )
   private String compadmLegalPerson;
   @Column(
      name = "COMPADM_QUA"
   )
   private String compadmQua;
   @Column(
      name = "COMPADM_ADDR"
   )
   private String compadmAddr;
   @Column(
      name = "COMPADM_CRE_DATE"
   )
   private String compadmCreDate;
   @Column(
      name = "COMPADM_CRE_TIME"
   )
   private String compadmCreTime;
   @Column(
      name = "COMPADM_CRE_ID"
   )
   private String compadmCreId;
   @Column(
      name = "COMPADM_MODI_DATE"
   )
   private String compadmModiDate;
   @Column(
      name = "COMPADM_MODI_TIME"
   )
   private String compadmModiTime;
   @Column(
      name = "COMPADM_MODI_ID"
   )
   private String compadmModiId;
   @Column(
      name = "COMPADM_LOGO"
   )
   private String compadmLogo;
   @Column(
      name = "COMPADM_STATUS"
   )
   private String compadmStatus;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getCompadmId() {
      return this.compadmId;
   }

   public void setCompadmId(String compadmId) {
      this.compadmId = compadmId;
   }

   public String getCompadmName() {
      return this.compadmName;
   }

   public void setCompadmName(String compadmName) {
      this.compadmName = compadmName;
   }

   public String getCompadmNo() {
      return this.compadmNo;
   }

   public void setCompadmNo(String compadmNo) {
      this.compadmNo = compadmNo;
   }

   public String getCompadmLegalPerson() {
      return this.compadmLegalPerson;
   }

   public void setCompadmLegalPerson(String compadmLegalPerson) {
      this.compadmLegalPerson = compadmLegalPerson;
   }

   public String getCompadmQua() {
      return this.compadmQua;
   }

   public void setCompadmQua(String compadmQua) {
      this.compadmQua = compadmQua;
   }

   public String getCompadmAddr() {
      return this.compadmAddr;
   }

   public void setCompadmAddr(String compadmAddr) {
      this.compadmAddr = compadmAddr;
   }

   public String getCompadmCreDate() {
      return this.compadmCreDate;
   }

   public void setCompadmCreDate(String compadmCreDate) {
      this.compadmCreDate = compadmCreDate;
   }

   public String getCompadmCreTime() {
      return this.compadmCreTime;
   }

   public void setCompadmCreTime(String compadmCreTime) {
      this.compadmCreTime = compadmCreTime;
   }

   public String getCompadmCreId() {
      return this.compadmCreId;
   }

   public void setCompadmCreId(String compadmCreId) {
      this.compadmCreId = compadmCreId;
   }

   public String getCompadmModiDate() {
      return this.compadmModiDate;
   }

   public void setCompadmModiDate(String compadmModiDate) {
      this.compadmModiDate = compadmModiDate;
   }

   public String getCompadmModiTime() {
      return this.compadmModiTime;
   }

   public void setCompadmModiTime(String compadmModiTime) {
      this.compadmModiTime = compadmModiTime;
   }

   public String getCompadmModiId() {
      return this.compadmModiId;
   }

   public void setCompadmModiId(String compadmModiId) {
      this.compadmModiId = compadmModiId;
   }

   public String getCompadmLogo() {
      return this.compadmLogo;
   }

   public void setCompadmLogo(String compadmLogo) {
      this.compadmLogo = compadmLogo;
   }

   public String getCompadmStatus() {
      return this.compadmStatus;
   }

   public void setCompadmStatus(String compadmStatus) {
      this.compadmStatus = compadmStatus;
   }
}
