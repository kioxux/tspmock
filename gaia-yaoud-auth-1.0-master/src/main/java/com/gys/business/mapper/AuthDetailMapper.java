package com.gys.business.mapper;

import com.gys.business.mapper.entity.AuthDetail;
import com.gys.business.service.data.auth.AuthTree;
import com.gys.business.service.data.auth.ModelClassify;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 权限明细表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-12-22
 */
@Mapper
public interface AuthDetailMapper extends BaseMapper<AuthDetail> {

    /**
     * 查询模块分类
     *
     * @param code       编码
     * @param containApp 是否包含app
     * @return List<ModelClassify>
     */
    List<ModelClassify> selectGlobalModel(@Param("code") String code, @Param("containApp") Boolean containApp);

    /**
     * 查询权限明细
     *
     * @return List<AuthTree>
     */
    List<AuthTree> selectAuthTree();

}
