package com.gys.business.mapper.entity;

import lombok.Data;

/**
 * @desc: 加盟商系统参数实体
 * @author: Ryan
 * @createTime: 2021/10/25 14:17
 */
@Data
public class SdParam {
    //
    private String client;
    //门店编码
    private String gsspBrId;
    //参数id
    private String  gsspId;
    //参数名字
    private String gsspName;
    // 参数值
    private String gsspPara;
}
