package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
@Table(
   name = "GAIA_ZZ_DATA"
)
public class GaiaZzData implements Serializable {
   @Id
   @Column(
           name = "ID"
   )
   private String id;
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Column(
      name = "ZZ_ORGID"
   )
   private String zzOrgid;
   @Column(
      name = "ZZ_ORGNAME"
   )
   private String zzOrgname;
   @Column(
      name = "ZZ_ID"
   )
   private String zzId;
   @Column(
      name = "ZZ_NAME"
   )
   private String zzName;
   @Column(
           name = "ZZ_USER_ID"
   )
   private String zzUserId;
   @Column(
           name = "ZZ_USER_NAME"
   )
   private String zzUserName;
   @Column(
      name = "ZZ_NO"
   )
   private String zzNo;
   @Column(
      name = "ZZ_SDATE"
   )
   private String zzSdate;
   @Column(
      name = "ZZ_EDATE"
   )
   private String zzEdate;
   @Column(
      name = "ZZ_CRE_DATE"
   )
   private String zzCreDate;
   @Column(
      name = "ZZ_CRE_TIME"
   )
   private String zzCreTime;
   @Column(
      name = "ZZ_CRE_ID"
   )
   private String zzCreId;
   @Column(
      name = "ZZ_MODI_DATE"
   )
   private String zzModiDate;
   @Column(
      name = "ZZ_MODI_TIME"
   )
   private String zzModiTime;
   @Column(
      name = "ZZ_MODI_ID"
   )
   private String zzModiId;

   private static final long serialVersionUID = 1L;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getZzOrgid() {
      return this.zzOrgid;
   }

   public void setZzOrgid(String zzOrgid) {
      this.zzOrgid = zzOrgid;
   }

   public String getZzOrgname() {
      return this.zzOrgname;
   }

   public void setZzOrgname(String zzOrgname) {
      this.zzOrgname = zzOrgname;
   }

   public String getZzId() {
      return this.zzId;
   }

   public void setZzId(String zzId) {
      this.zzId = zzId;
   }

   public String getZzName() {
      return this.zzName;
   }

   public void setZzName(String zzName) {
      this.zzName = zzName;
   }

   public String getZzNo() {
      return this.zzNo;
   }

   public void setZzNo(String zzNo) {
      this.zzNo = zzNo;
   }

   public String getZzSdate() {
      return this.zzSdate;
   }

   public void setZzSdate(String zzSdate) {
      this.zzSdate = zzSdate;
   }

   public String getZzEdate() {
      return this.zzEdate;
   }

   public void setZzEdate(String zzEdate) {
      this.zzEdate = zzEdate;
   }

   public String getZzCreDate() {
      return this.zzCreDate;
   }

   public void setZzCreDate(String zzCreDate) {
      this.zzCreDate = zzCreDate;
   }

   public String getZzCreTime() {
      return this.zzCreTime;
   }

   public void setZzCreTime(String zzCreTime) {
      this.zzCreTime = zzCreTime;
   }

   public String getZzCreId() {
      return this.zzCreId;
   }

   public void setZzCreId(String zzCreId) {
      this.zzCreId = zzCreId;
   }

   public String getZzModiDate() {
      return this.zzModiDate;
   }

   public void setZzModiDate(String zzModiDate) {
      this.zzModiDate = zzModiDate;
   }

   public String getZzModiTime() {
      return this.zzModiTime;
   }

   public void setZzModiTime(String zzModiTime) {
      this.zzModiTime = zzModiTime;
   }

   public String getZzModiId() {
      return this.zzModiId;
   }

   public void setZzModiId(String zzModiId) {
      this.zzModiId = zzModiId;
   }

   public String getZzUserId() {
      return zzUserId;
   }

   public void setZzUserId(String zzUserId) {
      this.zzUserId = zzUserId;
   }

   public String getZzUserName() {
      return zzUserName;
   }

   public void setZzUserName(String zzUserName) {
      this.zzUserName = zzUserName;
   }
}
