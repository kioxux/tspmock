package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaSdIncomeStatementH;
import com.gys.business.mapper.entity.GaiaWfRecord;
import com.gys.common.base.BaseMapper;

public interface GaiaSdIncomeStatementHMapper  extends BaseMapper<GaiaSdIncomeStatementH> {

}