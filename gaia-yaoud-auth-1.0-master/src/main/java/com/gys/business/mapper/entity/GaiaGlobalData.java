package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_GLOBAL_DATA"
)
public class GaiaGlobalData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "GLOBAL_ID"
   )
   private String globalId;
   @Id
   @Column(
      name = "GLOBAL_SITE"
   )
   private String globalSite;
   @Id
   @Column(
      name = "GLOBAL_TYPE"
   )
   private String globalType;
   @Column(
      name = "GLOBAL_NAME"
   )
   private String globalName;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getGlobalId() {
      return this.globalId;
   }

   public void setGlobalId(String globalId) {
      this.globalId = globalId;
   }

   public String getGlobalSite() {
      return this.globalSite;
   }

   public void setGlobalSite(String globalSite) {
      this.globalSite = globalSite;
   }

   public String getGlobalType() {
      return this.globalType;
   }

   public void setGlobalType(String globalType) {
      this.globalType = globalType;
   }

   public String getGlobalName() {
      return this.globalName;
   }

   public void setGlobalName(String globalName) {
      this.globalName = globalName;
   }
}
