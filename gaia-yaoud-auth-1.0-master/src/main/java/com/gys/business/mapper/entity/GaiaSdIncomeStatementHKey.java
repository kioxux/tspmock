package com.gys.business.mapper.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * GAIA_SD_INCOME_STATEMENT_H
 * @author 
 */
@Data
public class GaiaSdIncomeStatementHKey implements Serializable {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 损益单号
     */
    private String gsishVoucherId;

    private static final long serialVersionUID = 1L;
}