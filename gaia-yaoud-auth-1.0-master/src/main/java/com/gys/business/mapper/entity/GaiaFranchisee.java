package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_FRANCHISEE"
)
public class GaiaFranchisee implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Column(
      name = "FRANC_NAME"
   )
   private String francName;
   @Column(
      name = "FRANC_NO"
   )
   private String francNo;
   @Column(
      name = "FRANC_LEGAL_PERSON"
   )
   private String francLegalPerson;
   @Column(
      name = "FRANC_QUA"
   )
   private String francQua;
   @Column(
      name = "FRANC_ADDR"
   )
   private String francAddr;
   @Column(
      name = "FRANC_CRE_DATE"
   )
   private String francCreDate;
   @Column(
      name = "FRANC_CRE_TIME"
   )
   private String francCreTime;
   @Column(
      name = "FRANC_CRE_ID"
   )
   private String francCreId;
   @Column(
      name = "FRANC_MODI_DATE"
   )
   private String francModiDate;
   @Column(
      name = "FRANC_MODI_TIME"
   )
   private String francModiTime;
   @Column(
      name = "FRANC_MODI_ID"
   )
   private String francModiId;
   @Column(
      name = "FRANC_LOGO"
   )
   private String francLogo;
   @Column(
      name = "FRANC_TYPE1"
   )
   private String francType1;
   @Column(
      name = "FRANC_TYPE2"
   )
   private String francType2;
   @Column(
      name = "FRANC_TYPE3"
   )
   private String francType3;
   @Column(
      name = "FRANC_ASS"
   )
   private String francAss;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getFrancName() {
      return this.francName;
   }

   public void setFrancName(String francName) {
      this.francName = francName;
   }

   public String getFrancNo() {
      return this.francNo;
   }

   public void setFrancNo(String francNo) {
      this.francNo = francNo;
   }

   public String getFrancLegalPerson() {
      return this.francLegalPerson;
   }

   public void setFrancLegalPerson(String francLegalPerson) {
      this.francLegalPerson = francLegalPerson;
   }

   public String getFrancQua() {
      return this.francQua;
   }

   public void setFrancQua(String francQua) {
      this.francQua = francQua;
   }

   public String getFrancAddr() {
      return this.francAddr;
   }

   public void setFrancAddr(String francAddr) {
      this.francAddr = francAddr;
   }

   public String getFrancCreDate() {
      return this.francCreDate;
   }

   public void setFrancCreDate(String francCreDate) {
      this.francCreDate = francCreDate;
   }

   public String getFrancCreTime() {
      return this.francCreTime;
   }

   public void setFrancCreTime(String francCreTime) {
      this.francCreTime = francCreTime;
   }

   public String getFrancCreId() {
      return this.francCreId;
   }

   public void setFrancCreId(String francCreId) {
      this.francCreId = francCreId;
   }

   public String getFrancModiDate() {
      return this.francModiDate;
   }

   public void setFrancModiDate(String francModiDate) {
      this.francModiDate = francModiDate;
   }

   public String getFrancModiTime() {
      return this.francModiTime;
   }

   public void setFrancModiTime(String francModiTime) {
      this.francModiTime = francModiTime;
   }

   public String getFrancModiId() {
      return this.francModiId;
   }

   public void setFrancModiId(String francModiId) {
      this.francModiId = francModiId;
   }

   public String getFrancLogo() {
      return this.francLogo;
   }

   public void setFrancLogo(String francLogo) {
      this.francLogo = francLogo;
   }

   public String getFrancType1() {
      return this.francType1;
   }

   public void setFrancType1(String francType1) {
      this.francType1 = francType1;
   }

   public String getFrancType2() {
      return this.francType2;
   }

   public void setFrancType2(String francType2) {
      this.francType2 = francType2;
   }

   public String getFrancType3() {
      return this.francType3;
   }

   public void setFrancType3(String francType3) {
      this.francType3 = francType3;
   }

   public String getFrancAss() {
      return this.francAss;
   }

   public void setFrancAss(String francAss) {
      this.francAss = francAss;
   }
}
