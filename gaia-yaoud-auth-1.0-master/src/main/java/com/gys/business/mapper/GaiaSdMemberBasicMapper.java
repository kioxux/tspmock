package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberBasic;
import com.gys.business.service.data.GetMemberOutData;
import com.gys.business.service.data.GetMemberPreSetInData;
import com.gys.business.service.data.MemberInData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdMemberBasicMapper extends BaseMapper<GaiaSdMemberBasic> {
   void addCards(MemberInData inData);

   String getVirtualMaxCard(@Param("client") String client);

   List<GetMemberOutData> selectNewCardList(GetMemberPreSetInData inData);
}
