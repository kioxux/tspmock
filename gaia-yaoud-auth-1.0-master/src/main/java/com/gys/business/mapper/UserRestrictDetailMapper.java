package com.gys.business.mapper;

import com.gys.business.mapper.entity.UserRestrictDetail;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户数据范围权限明细表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-12-27
 */
@Mapper
public interface UserRestrictDetailMapper extends BaseMapper<UserRestrictDetail> {
}
