package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 自定义角色表
 * </p>
 *
 * @author flynn
 * @since 2021-12-22
 */
@Data
@Table( name = "GAIA_GROUP_ROLE")
public class GroupRole implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
//    @Id
    @Column(name = "ID")
    private String id;

    @ApiModelProperty(value = "角色名")
    @Column(name = "ROLE_NAME")
    private String roleName;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "公司类型")
    @Column(name = "COMP_TYPE")
    private String compType;

    @ApiModelProperty(value = "公司代码")
    @Column(name = "COMP_CODE")
    private String compCode;

    @ApiModelProperty(value = "角色类型")
    @Column(name = "BASIC_ROLE")
    private String basicRole;

    @ApiModelProperty(value = "是否后台角色(1.是  0.不是)")
    @Column(name = "IS_ADMIN")
    private Boolean isAdmin;

    @ApiModelProperty(value = "是否门店角色(1.是  0.不是)")
    @Column(name = "IS_STO")
    private Boolean isSto;

    @ApiModelProperty(value = "是否APP角色(1.是  0.不是)")
    @Column(name = "IS_APP")
    private Boolean isApp;

    @ApiModelProperty(value = "是否删除（0不删除 1删除）")
    @Column(name = "DELETE_FLAG")
    private Integer deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人ID")
    @Column(name = "MOD_ID")
    private String modId;


}
