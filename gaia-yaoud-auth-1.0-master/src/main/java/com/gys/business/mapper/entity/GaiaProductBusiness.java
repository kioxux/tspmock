package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_PRODUCT_BUSINESS"
)
public class GaiaProductBusiness implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "PRO_SITE"
   )
   private String proSite;
   @Id
   @Column(
      name = "PRO_SELF_CODE"
   )
   private String proSelfCode;
   @Column(
      name = "PRO_CODE"
   )
   private String proCode;
   @Column(
      name = "PRO_MATCH_STATUS"
   )
   private String proMatchStatus;
   @Column(
      name = "PRO_COMMONNAME"
   )
   private String proCommonname;
   @Column(
      name = "PRO_DEPICT"
   )
   private String proDepict;
   @Column(
      name = "PRO_PYM"
   )
   private String proPym;
   @Column(
      name = "PRO_NAME"
   )
   private String proName;
   @Column(
      name = "PRO_SPECS"
   )
   private String proSpecs;
   @Column(
      name = "PRO_UNIT"
   )
   private String proUnit;
   @Column(
      name = "PRO_FORM"
   )
   private String proForm;
   @Column(
      name = "PRO_PARTFORM"
   )
   private String proPartform;
   @Column(
      name = "PRO_MINDOSE"
   )
   private String proMindose;
   @Column(
      name = "PRO_TOTALDOSE"
   )
   private String proTotaldose;
   @Column(
      name = "PRO_BARCODE"
   )
   private String proBarcode;
   @Column(
      name = "PRO_BARCODE2"
   )
   private String proBarcode2;
   @Column(
      name = "PRO_REGISTER_CLASS"
   )
   private String proRegisterClass;
   @Column(
      name = "PRO_REGISTER_NO"
   )
   private String proRegisterNo;
   @Column(
      name = "PRO_REGISTER_DATE"
   )
   private String proRegisterDate;
   @Column(
      name = "PRO_REGISTER_EXDATE"
   )
   private String proRegisterExdate;
   @Column(
      name = "PRO_CLASS"
   )
   private String proClass;
   @Column(
      name = "PRO_CLASS_NAME"
   )
   private String proClassName;
   @Column(
      name = "PRO_COMPCLASS"
   )
   private String proCompclass;
   @Column(
      name = "PRO_COMPCLASS_NAME"
   )
   private String proCompclassName;
   @Column(
      name = "PRO_PRESCLASS"
   )
   private String proPresclass;
   @Column(
      name = "PRO_FACTORY_CODE"
   )
   private String proFactoryCode;
   @Column(
      name = "PRO_FACTORY_NAME"
   )
   private String proFactoryName;
   @Column(
      name = "PRO_MARK"
   )
   private String proMark;
   @Column(
      name = "PRO_BRAND"
   )
   private String proBrand;
   @Column(
      name = "PRO_BRAND_CLASS"
   )
   private String proBrandClass;
   @Column(
      name = "PRO_LIFE"
   )
   private String proLife;
   @Column(
      name = "PRO_LIFE_UNIT"
   )
   private String proLifeUnit;
   @Column(
      name = "PRO_HOLDER"
   )
   private String proHolder;
   @Column(
      name = "PRO_INPUT_TAX"
   )
   private String proInputTax;
   @Column(
      name = "PRO_OUTPUT_TAX"
   )
   private String proOutputTax;
   @Column(
      name = "PRO_BASIC_CODE"
   )
   private String proBasicCode;
   @Column(
      name = "PRO_TAX_CLASS"
   )
   private String proTaxClass;
   @Column(
      name = "PRO_CONTROL_CLASS"
   )
   private String proControlClass;
   @Column(
      name = "PRO_PRODUCE_CLASS"
   )
   private String proProduceClass;
   @Column(
      name = "PRO_STORAGE_CONDITION"
   )
   private String proStorageCondition;
   @Column(
      name = "PRO_STORAGE_AREA"
   )
   private String proStorageArea;
   @Column(
      name = "PRO_LONG"
   )
   private String proLong;
   @Column(
      name = "PRO_WIDE"
   )
   private String proWide;
   @Column(
      name = "PRO_HIGH"
   )
   private String proHigh;
   @Column(
      name = "PRO_MID_PACKAGE"
   )
   private String proMidPackage;
   @Column(
      name = "PRO_BIG_PACKAGE"
   )
   private String proBigPackage;
   @Column(
      name = "PRO_ELECTRONIC_CODE"
   )
   private String proElectronicCode;
   @Column(
      name = "PRO_QS_CODE"
   )
   private String proQsCode;
   @Column(
      name = "PRO_MAX_SALES"
   )
   private String proMaxSales;
   @Column(
      name = "PRO_INSTRUCTION_CODE"
   )
   private String proInstructionCode;
   @Column(
      name = "PRO_INSTRUCTION"
   )
   private String proInstruction;
   @Column(
      name = "PRO_MED_PRODCT"
   )
   private String proMedProdct;
   @Column(
      name = "PRO_STATUS"
   )
   private String proStatus;
   @Column(
      name = "PRO_MED_PRODCTCODE"
   )
   private String proMedProdctcode;
   @Column(
      name = "PRO_COUNTRY"
   )
   private String proCountry;
   @Column(
      name = "PRO_PLACE"
   )
   private String proPlace;
   @Column(
      name = "PRO_TAKE_DAYS"
   )
   private String proTakeDays;
   @Column(
      name = "PRO_USAGE"
   )
   private String proUsage;
   @Column(
      name = "PRO_CONTRAINDICATION"
   )
   private String proContraindication;
   @Column(
      name = "PRO_POSITION"
   )
   private String proPosition;
   @Column(
      name = "PRO_NO_RETAIL"
   )
   private String proNoRetail;
   @Column(
      name = "PRO_NO_PURCHASE"
   )
   private String proNoPurchase;
   @Column(
      name = "PRO_NO_DISTRIBUTED"
   )
   private String proNoDistributed;
   @Column(
      name = "PRO_NO_SUPPLIER"
   )
   private String proNoSupplier;
   @Column(
      name = "PRO_NO_DC"
   )
   private String proNoDc;
   @Column(
      name = "PRO_NO_ADJUST"
   )
   private String proNoAdjust;
   @Column(
      name = "PRO_NO_SALE"
   )
   private String proNoSale;
   @Column(
      name = "PRO_NO_APPLY"
   )
   private String proNoApply;
   @Column(
      name = "PRO_IFPART"
   )
   private String proIfpart;
   @Column(
      name = "PRO_PART_UINT"
   )
   private String proPartUint;
   @Column(
      name = "PRO_PART_RATE"
   )
   private String proPartRate;
   @Column(
      name = "PRO_PURCHASE_UNIT"
   )
   private String proPurchaseUnit;
   @Column(
      name = "PRO_PURCHASE_RATE"
   )
   private String proPurchaseRate;
   @Column(
      name = "PRO_SALE_UNIT"
   )
   private String proSaleUnit;
   @Column(
      name = "PRO_SALE_RATE"
   )
   private String proSaleRate;
   @Column(
      name = "PRO_MIN_QTY"
   )
   private BigDecimal proMinQty;
   @Column(
      name = "PRO_IF_MED"
   )
   private String proIfMed;
   @Column(
      name = "PRO_SLAE_CLASS"
   )
   private String proSlaeClass;
   @Column(
      name = "PRO_LIMIT_QTY"
   )
   private BigDecimal proLimitQty;
   @Column(
      name = "PRO_MAX_QTY"
   )
   private BigDecimal proMaxQty;
   @Column(
      name = "PRO_PACKAGE_FLAG"
   )
   private String proPackageFlag;
   @Column(
      name = "PRO_KEY_CARE"
   )
   private String proKeyCare;
   @Column(
      name = "PRO_TCM_SPECS"
   )
   private String proTcmSpecs;
   @Column(
      name = "PRO_TCM_REGISTER_NO"
   )
   private String proTcmRegisterNo;
   @Column(
      name = "PRO_TCM_FACTORY_CODE"
   )
   private String proTcmFactoryCode;
   @Column(
      name = "PRO_TCM_PLACE"
   )
   private String proTcmPlace;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getProSite() {
      return this.proSite;
   }

   public void setProSite(String proSite) {
      this.proSite = proSite;
   }

   public String getProSelfCode() {
      return this.proSelfCode;
   }

   public void setProSelfCode(String proSelfCode) {
      this.proSelfCode = proSelfCode;
   }

   public String getProCode() {
      return this.proCode;
   }

   public void setProCode(String proCode) {
      this.proCode = proCode;
   }

   public String getProMatchStatus() {
      return this.proMatchStatus;
   }

   public void setProMatchStatus(String proMatchStatus) {
      this.proMatchStatus = proMatchStatus;
   }

   public String getProCommonname() {
      return this.proCommonname;
   }

   public void setProCommonname(String proCommonname) {
      this.proCommonname = proCommonname;
   }

   public String getProDepict() {
      return this.proDepict;
   }

   public void setProDepict(String proDepict) {
      this.proDepict = proDepict;
   }

   public String getProPym() {
      return this.proPym;
   }

   public void setProPym(String proPym) {
      this.proPym = proPym;
   }

   public String getProName() {
      return this.proName;
   }

   public void setProName(String proName) {
      this.proName = proName;
   }

   public String getProSpecs() {
      return this.proSpecs;
   }

   public void setProSpecs(String proSpecs) {
      this.proSpecs = proSpecs;
   }

   public String getProUnit() {
      return this.proUnit;
   }

   public void setProUnit(String proUnit) {
      this.proUnit = proUnit;
   }

   public String getProForm() {
      return this.proForm;
   }

   public void setProForm(String proForm) {
      this.proForm = proForm;
   }

   public String getProPartform() {
      return this.proPartform;
   }

   public void setProPartform(String proPartform) {
      this.proPartform = proPartform;
   }

   public String getProMindose() {
      return this.proMindose;
   }

   public void setProMindose(String proMindose) {
      this.proMindose = proMindose;
   }

   public String getProTotaldose() {
      return this.proTotaldose;
   }

   public void setProTotaldose(String proTotaldose) {
      this.proTotaldose = proTotaldose;
   }

   public String getProBarcode() {
      return this.proBarcode;
   }

   public void setProBarcode(String proBarcode) {
      this.proBarcode = proBarcode;
   }

   public String getProBarcode2() {
      return this.proBarcode2;
   }

   public void setProBarcode2(String proBarcode2) {
      this.proBarcode2 = proBarcode2;
   }

   public String getProRegisterClass() {
      return this.proRegisterClass;
   }

   public void setProRegisterClass(String proRegisterClass) {
      this.proRegisterClass = proRegisterClass;
   }

   public String getProRegisterNo() {
      return this.proRegisterNo;
   }

   public void setProRegisterNo(String proRegisterNo) {
      this.proRegisterNo = proRegisterNo;
   }

   public String getProRegisterDate() {
      return this.proRegisterDate;
   }

   public void setProRegisterDate(String proRegisterDate) {
      this.proRegisterDate = proRegisterDate;
   }

   public String getProRegisterExdate() {
      return this.proRegisterExdate;
   }

   public void setProRegisterExdate(String proRegisterExdate) {
      this.proRegisterExdate = proRegisterExdate;
   }

   public String getProClass() {
      return this.proClass;
   }

   public void setProClass(String proClass) {
      this.proClass = proClass;
   }

   public String getProClassName() {
      return this.proClassName;
   }

   public void setProClassName(String proClassName) {
      this.proClassName = proClassName;
   }

   public String getProCompclass() {
      return this.proCompclass;
   }

   public void setProCompclass(String proCompclass) {
      this.proCompclass = proCompclass;
   }

   public String getProCompclassName() {
      return this.proCompclassName;
   }

   public void setProCompclassName(String proCompclassName) {
      this.proCompclassName = proCompclassName;
   }

   public String getProPresclass() {
      return this.proPresclass;
   }

   public void setProPresclass(String proPresclass) {
      this.proPresclass = proPresclass;
   }

   public String getProFactoryCode() {
      return this.proFactoryCode;
   }

   public void setProFactoryCode(String proFactoryCode) {
      this.proFactoryCode = proFactoryCode;
   }

   public String getProFactoryName() {
      return this.proFactoryName;
   }

   public void setProFactoryName(String proFactoryName) {
      this.proFactoryName = proFactoryName;
   }

   public String getProMark() {
      return this.proMark;
   }

   public void setProMark(String proMark) {
      this.proMark = proMark;
   }

   public String getProBrand() {
      return this.proBrand;
   }

   public void setProBrand(String proBrand) {
      this.proBrand = proBrand;
   }

   public String getProBrandClass() {
      return this.proBrandClass;
   }

   public void setProBrandClass(String proBrandClass) {
      this.proBrandClass = proBrandClass;
   }

   public String getProLife() {
      return this.proLife;
   }

   public void setProLife(String proLife) {
      this.proLife = proLife;
   }

   public String getProLifeUnit() {
      return this.proLifeUnit;
   }

   public void setProLifeUnit(String proLifeUnit) {
      this.proLifeUnit = proLifeUnit;
   }

   public String getProHolder() {
      return this.proHolder;
   }

   public void setProHolder(String proHolder) {
      this.proHolder = proHolder;
   }

   public String getProInputTax() {
      return this.proInputTax;
   }

   public void setProInputTax(String proInputTax) {
      this.proInputTax = proInputTax;
   }

   public String getProOutputTax() {
      return this.proOutputTax;
   }

   public void setProOutputTax(String proOutputTax) {
      this.proOutputTax = proOutputTax;
   }

   public String getProBasicCode() {
      return this.proBasicCode;
   }

   public void setProBasicCode(String proBasicCode) {
      this.proBasicCode = proBasicCode;
   }

   public String getProTaxClass() {
      return this.proTaxClass;
   }

   public void setProTaxClass(String proTaxClass) {
      this.proTaxClass = proTaxClass;
   }

   public String getProControlClass() {
      return this.proControlClass;
   }

   public void setProControlClass(String proControlClass) {
      this.proControlClass = proControlClass;
   }

   public String getProProduceClass() {
      return this.proProduceClass;
   }

   public void setProProduceClass(String proProduceClass) {
      this.proProduceClass = proProduceClass;
   }

   public String getProStorageCondition() {
      return this.proStorageCondition;
   }

   public void setProStorageCondition(String proStorageCondition) {
      this.proStorageCondition = proStorageCondition;
   }

   public String getProStorageArea() {
      return this.proStorageArea;
   }

   public void setProStorageArea(String proStorageArea) {
      this.proStorageArea = proStorageArea;
   }

   public String getProLong() {
      return this.proLong;
   }

   public void setProLong(String proLong) {
      this.proLong = proLong;
   }

   public String getProWide() {
      return this.proWide;
   }

   public void setProWide(String proWide) {
      this.proWide = proWide;
   }

   public String getProHigh() {
      return this.proHigh;
   }

   public void setProHigh(String proHigh) {
      this.proHigh = proHigh;
   }

   public String getProMidPackage() {
      return this.proMidPackage;
   }

   public void setProMidPackage(String proMidPackage) {
      this.proMidPackage = proMidPackage;
   }

   public String getProBigPackage() {
      return this.proBigPackage;
   }

   public void setProBigPackage(String proBigPackage) {
      this.proBigPackage = proBigPackage;
   }

   public String getProElectronicCode() {
      return this.proElectronicCode;
   }

   public void setProElectronicCode(String proElectronicCode) {
      this.proElectronicCode = proElectronicCode;
   }

   public String getProQsCode() {
      return this.proQsCode;
   }

   public void setProQsCode(String proQsCode) {
      this.proQsCode = proQsCode;
   }

   public String getProMaxSales() {
      return this.proMaxSales;
   }

   public void setProMaxSales(String proMaxSales) {
      this.proMaxSales = proMaxSales;
   }

   public String getProInstructionCode() {
      return this.proInstructionCode;
   }

   public void setProInstructionCode(String proInstructionCode) {
      this.proInstructionCode = proInstructionCode;
   }

   public String getProInstruction() {
      return this.proInstruction;
   }

   public void setProInstruction(String proInstruction) {
      this.proInstruction = proInstruction;
   }

   public String getProMedProdct() {
      return this.proMedProdct;
   }

   public void setProMedProdct(String proMedProdct) {
      this.proMedProdct = proMedProdct;
   }

   public String getProStatus() {
      return this.proStatus;
   }

   public void setProStatus(String proStatus) {
      this.proStatus = proStatus;
   }

   public String getProMedProdctcode() {
      return this.proMedProdctcode;
   }

   public void setProMedProdctcode(String proMedProdctcode) {
      this.proMedProdctcode = proMedProdctcode;
   }

   public String getProCountry() {
      return this.proCountry;
   }

   public void setProCountry(String proCountry) {
      this.proCountry = proCountry;
   }

   public String getProPlace() {
      return this.proPlace;
   }

   public void setProPlace(String proPlace) {
      this.proPlace = proPlace;
   }

   public String getProTakeDays() {
      return this.proTakeDays;
   }

   public void setProTakeDays(String proTakeDays) {
      this.proTakeDays = proTakeDays;
   }

   public String getProUsage() {
      return this.proUsage;
   }

   public void setProUsage(String proUsage) {
      this.proUsage = proUsage;
   }

   public String getProContraindication() {
      return this.proContraindication;
   }

   public void setProContraindication(String proContraindication) {
      this.proContraindication = proContraindication;
   }

   public String getProPosition() {
      return this.proPosition;
   }

   public void setProPosition(String proPosition) {
      this.proPosition = proPosition;
   }

   public String getProNoRetail() {
      return this.proNoRetail;
   }

   public void setProNoRetail(String proNoRetail) {
      this.proNoRetail = proNoRetail;
   }

   public String getProNoPurchase() {
      return this.proNoPurchase;
   }

   public void setProNoPurchase(String proNoPurchase) {
      this.proNoPurchase = proNoPurchase;
   }

   public String getProNoDistributed() {
      return this.proNoDistributed;
   }

   public void setProNoDistributed(String proNoDistributed) {
      this.proNoDistributed = proNoDistributed;
   }

   public String getProNoSupplier() {
      return this.proNoSupplier;
   }

   public void setProNoSupplier(String proNoSupplier) {
      this.proNoSupplier = proNoSupplier;
   }

   public String getProNoDc() {
      return this.proNoDc;
   }

   public void setProNoDc(String proNoDc) {
      this.proNoDc = proNoDc;
   }

   public String getProNoAdjust() {
      return this.proNoAdjust;
   }

   public void setProNoAdjust(String proNoAdjust) {
      this.proNoAdjust = proNoAdjust;
   }

   public String getProNoSale() {
      return this.proNoSale;
   }

   public void setProNoSale(String proNoSale) {
      this.proNoSale = proNoSale;
   }

   public String getProNoApply() {
      return this.proNoApply;
   }

   public void setProNoApply(String proNoApply) {
      this.proNoApply = proNoApply;
   }

   public String getProIfpart() {
      return this.proIfpart;
   }

   public void setProIfpart(String proIfpart) {
      this.proIfpart = proIfpart;
   }

   public String getProPartUint() {
      return this.proPartUint;
   }

   public void setProPartUint(String proPartUint) {
      this.proPartUint = proPartUint;
   }

   public String getProPartRate() {
      return this.proPartRate;
   }

   public void setProPartRate(String proPartRate) {
      this.proPartRate = proPartRate;
   }

   public String getProPurchaseUnit() {
      return this.proPurchaseUnit;
   }

   public void setProPurchaseUnit(String proPurchaseUnit) {
      this.proPurchaseUnit = proPurchaseUnit;
   }

   public String getProPurchaseRate() {
      return this.proPurchaseRate;
   }

   public void setProPurchaseRate(String proPurchaseRate) {
      this.proPurchaseRate = proPurchaseRate;
   }

   public String getProSaleUnit() {
      return this.proSaleUnit;
   }

   public void setProSaleUnit(String proSaleUnit) {
      this.proSaleUnit = proSaleUnit;
   }

   public String getProSaleRate() {
      return this.proSaleRate;
   }

   public void setProSaleRate(String proSaleRate) {
      this.proSaleRate = proSaleRate;
   }

   public BigDecimal getProMinQty() {
      return this.proMinQty;
   }

   public void setProMinQty(BigDecimal proMinQty) {
      this.proMinQty = proMinQty;
   }

   public String getProIfMed() {
      return this.proIfMed;
   }

   public void setProIfMed(String proIfMed) {
      this.proIfMed = proIfMed;
   }

   public String getProSlaeClass() {
      return this.proSlaeClass;
   }

   public void setProSlaeClass(String proSlaeClass) {
      this.proSlaeClass = proSlaeClass;
   }

   public BigDecimal getProLimitQty() {
      return this.proLimitQty;
   }

   public void setProLimitQty(BigDecimal proLimitQty) {
      this.proLimitQty = proLimitQty;
   }

   public BigDecimal getProMaxQty() {
      return this.proMaxQty;
   }

   public void setProMaxQty(BigDecimal proMaxQty) {
      this.proMaxQty = proMaxQty;
   }

   public String getProPackageFlag() {
      return this.proPackageFlag;
   }

   public void setProPackageFlag(String proPackageFlag) {
      this.proPackageFlag = proPackageFlag;
   }

   public String getProKeyCare() {
      return this.proKeyCare;
   }

   public void setProKeyCare(String proKeyCare) {
      this.proKeyCare = proKeyCare;
   }

   public String getProTcmSpecs() {
      return this.proTcmSpecs;
   }

   public void setProTcmSpecs(String proTcmSpecs) {
      this.proTcmSpecs = proTcmSpecs;
   }

   public String getProTcmRegisterNo() {
      return this.proTcmRegisterNo;
   }

   public void setProTcmRegisterNo(String proTcmRegisterNo) {
      this.proTcmRegisterNo = proTcmRegisterNo;
   }

   public String getProTcmFactoryCode() {
      return this.proTcmFactoryCode;
   }

   public void setProTcmFactoryCode(String proTcmFactoryCode) {
      this.proTcmFactoryCode = proTcmFactoryCode;
   }

   public String getProTcmPlace() {
      return this.proTcmPlace;
   }

   public void setProTcmPlace(String proTcmPlace) {
      this.proTcmPlace = proTcmPlace;
   }
}
