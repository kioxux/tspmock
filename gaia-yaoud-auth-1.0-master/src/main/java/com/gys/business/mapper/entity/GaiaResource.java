package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_RESOURCE"
)
public class GaiaResource implements Serializable {
   @Id
   @Column(
      name = "ID"
   )
   @GeneratedValue(
      strategy = GenerationType.IDENTITY
   )
   private String id;
   @Column(
      name = "NAME"
   )
   private String name;
   @Column(
      name = "TITLE"
   )
   private String title;
   @Column(
      name = "PATH"
   )
   private String path;
   @Column(
      name = "PAGE_PATH"
   )
   private String pagePath;
   @Column(
      name = "REQUEST_PATH"
   )
   private String requestPath;
   @Column(
      name = "ICON"
   )
   private String icon;
   @Column(
      name = "TYPE"
   )
   private String type;
   @Column(
      name = "BUTTON_TYPE"
   )
   private String buttonType;
   @Column(
      name = "PARENT_ID"
   )
   private String parentId;
   @Column(
      name = "STATUS"
   )
   private String status;
   @Column(
      name = "SEQ"
   )
   private Integer seq;
   @Column(
      name = "MODULE"
   )
   private String module;
   private static final long serialVersionUID = 1L;

   public String getId() {
      return this.id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getName() {
      return this.name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getTitle() {
      return this.title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getPath() {
      return this.path;
   }

   public void setPath(String path) {
      this.path = path;
   }

   public String getPagePath() {
      return this.pagePath;
   }

   public void setPagePath(String pagePath) {
      this.pagePath = pagePath;
   }

   public String getRequestPath() {
      return this.requestPath;
   }

   public void setRequestPath(String requestPath) {
      this.requestPath = requestPath;
   }

   public String getIcon() {
      return this.icon;
   }

   public void setIcon(String icon) {
      this.icon = icon;
   }

   public String getType() {
      return this.type;
   }

   public void setType(String type) {
      this.type = type;
   }

   public String getButtonType() {
      return this.buttonType;
   }

   public void setButtonType(String buttonType) {
      this.buttonType = buttonType;
   }

   public String getParentId() {
      return this.parentId;
   }

   public void setParentId(String parentId) {
      this.parentId = parentId;
   }

   public String getStatus() {
      return this.status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public Integer getSeq() {
      return this.seq;
   }

   public void setSeq(Integer seq) {
      this.seq = seq;
   }

   public String getModule() {
      return this.module;
   }

   public void setModule(String module) {
      this.module = module;
   }
}
