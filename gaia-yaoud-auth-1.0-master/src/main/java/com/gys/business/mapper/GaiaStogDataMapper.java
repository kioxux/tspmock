package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStogData;
import com.gys.business.service.data.StogOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaStogDataMapper extends BaseMapper<GaiaStogData> {
   int selectMaxId(@Param("clientId") String clientId);

   List<StogOutData> getStogList(String client);

   List<StogOutData> getStoGroupList(String client);
}
