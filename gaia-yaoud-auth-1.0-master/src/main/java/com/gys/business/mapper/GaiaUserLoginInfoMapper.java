package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaUserLoginInfo;
import com.gys.common.base.BaseMapper;

public interface GaiaUserLoginInfoMapper extends BaseMapper<GaiaUserLoginInfo> {
}