package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 用户数据范围权限表
 * </p>
 *
 * @author flynn
 * @since 2021-12-27
 */
@Data
@Table( name = "GAIA_USER_RESTRICT")
public class UserRestrict implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @Id
    @Column(name = "ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT_ID")
    private String clientId;

    @ApiModelProperty(value = "用户")
    @Column(name = "USER_ID")
    private String userId;

    @ApiModelProperty(value = "默认的数据可见范围（0不限制 1只能看到自己被授权了角色的公司 2精确到片区 3精确到公司）")
    @Column(name = "RESTRICT_TYPE")
    private String restrictType;

    @ApiModelProperty(value = "是否删除（0不删除 1删除）")
    @Column(name = "DELETE_FLAG")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人ID")
    @Column(name = "MOD_ID")
    private String modId;


}
