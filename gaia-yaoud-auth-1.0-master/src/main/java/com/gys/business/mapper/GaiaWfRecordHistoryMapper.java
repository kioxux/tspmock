package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWfRecordHistory;
import com.gys.business.service.data.GetWorkflowApproveOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaWfRecordHistoryMapper extends BaseMapper<GaiaWfRecordHistory> {
   List<GetWorkflowApproveOutData> selectApprovedList(@Param("client") String client, @Param("wfCode") String wfCode);
}
