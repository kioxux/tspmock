package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaThirdPara;
import com.gys.common.base.BaseMapper;

public interface GaiaThirdParaMapper extends BaseMapper<GaiaThirdPara> {
}
