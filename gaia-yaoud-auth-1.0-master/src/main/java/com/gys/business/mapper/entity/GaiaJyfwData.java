package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_JYFW_DATA"
)
public class GaiaJyfwData implements Serializable {
   @Id
   @Column(
           name = "ID"
   )
   private String id;

   @Column(
      name = "CLIENT"
   )
   private String client;
   @Column(
      name = "JYFW_ORGID"
   )
   private String jyfwOrgid;
   @Column(
      name = "JYFW_ORGNAME"
   )
   private String jyfwOrgname;
   @Column(
      name = "JYFW_ID"
   )
   private String jyfwId;
   @Column(
      name = "JYFW_NAME"
   )
   private String jyfwName;
   @Column(
      name = "JYFW_CRE_DATE"
   )
   private String jyfwCreDate;
   @Column(
      name = "JYFW_CRE_TIME"
   )
   private String jyfwCreTime;
   @Column(
      name = "JYFW_CRE_ID"
   )
   private String jyfwCreId;
   @Column(
      name = "JYFW_MODI_DATE"
   )
   private String jyfwModiDate;
   @Column(
      name = "JYFW_MODI_TIME"
   )
   private String jyfwModiTime;
   @Column(
      name = "JYFW_MODI_ID"
   )
   private String jyfwModiId;

   private static final long serialVersionUID = 1L;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getJyfwOrgid() {
      return this.jyfwOrgid;
   }

   public void setJyfwOrgid(String jyfwOrgid) {
      this.jyfwOrgid = jyfwOrgid;
   }

   public String getJyfwOrgname() {
      return this.jyfwOrgname;
   }

   public void setJyfwOrgname(String jyfwOrgname) {
      this.jyfwOrgname = jyfwOrgname;
   }

   public String getJyfwId() {
      return this.jyfwId;
   }

   public void setJyfwId(String jyfwId) {
      this.jyfwId = jyfwId;
   }

   public String getJyfwName() {
      return this.jyfwName;
   }

   public void setJyfwName(String jyfwName) {
      this.jyfwName = jyfwName;
   }

   public String getJyfwCreDate() {
      return this.jyfwCreDate;
   }

   public void setJyfwCreDate(String jyfwCreDate) {
      this.jyfwCreDate = jyfwCreDate;
   }

   public String getJyfwCreTime() {
      return this.jyfwCreTime;
   }

   public void setJyfwCreTime(String jyfwCreTime) {
      this.jyfwCreTime = jyfwCreTime;
   }

   public String getJyfwCreId() {
      return this.jyfwCreId;
   }

   public void setJyfwCreId(String jyfwCreId) {
      this.jyfwCreId = jyfwCreId;
   }

   public String getJyfwModiDate() {
      return this.jyfwModiDate;
   }

   public void setJyfwModiDate(String jyfwModiDate) {
      this.jyfwModiDate = jyfwModiDate;
   }

   public String getJyfwModiTime() {
      return this.jyfwModiTime;
   }

   public void setJyfwModiTime(String jyfwModiTime) {
      this.jyfwModiTime = jyfwModiTime;
   }

   public String getJyfwModiId() {
      return this.jyfwModiId;
   }

   public void setJyfwModiId(String jyfwModiId) {
      this.jyfwModiId = jyfwModiId;
   }

}
