package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaAuthobjData;
import com.gys.common.base.BaseMapper;

public interface GaiaAuthobjDataMapper extends BaseMapper<GaiaAuthobjData> {
}
