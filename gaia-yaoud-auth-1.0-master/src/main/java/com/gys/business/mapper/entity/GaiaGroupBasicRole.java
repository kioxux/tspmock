package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 默认角色表
 */
@Data
@Table( name = "GAIA_GROUP_BASIC_ROLE")
public class GaiaGroupBasicRole implements Serializable {

    @Id
    @ApiModelProperty(value = "主键")
    @Column(name = "ID")
    private Integer id;

    @ApiModelProperty(value = "角色编码")
    @Column(name = "CODE")
    private String code;

    @ApiModelProperty(value = "角色名")
    @Column(name = "NAME")
    private String name;

    @ApiModelProperty(value = "角色类型（门店、商采、物流等）")
    @Column(name = "ROLE_TYPE")
    private String roleType;

    @ApiModelProperty(value = "是否后台角色(1.是  0.不是)")
    @Column(name = "IS_ADMIN")
    private Boolean isAdmin;

    @ApiModelProperty(value = "是否门店角色(1.是  0.不是)")
    @Column(name = "IS_STO")
    private Boolean isSto;

    @ApiModelProperty(value = "是否APP角色(1.是  0.不是)")
    @Column(name = "IS_APP")
    private Boolean isApp;

    @ApiModelProperty(value = "是否删除（0不删除 1删除）")
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人ID")
    @Column(name = "MOD_ID")
    private String modId;

}