package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralAddSet;
import com.gys.common.base.BaseMapper;

public interface GaiaSdIntegralAddSetMapper extends BaseMapper<GaiaSdIntegralAddSet> {
}
