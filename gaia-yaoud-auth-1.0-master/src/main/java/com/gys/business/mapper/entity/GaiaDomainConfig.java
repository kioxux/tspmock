package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_DOMAIN_CONFIG")
public class GaiaDomainConfig implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    /**
     * 域名
     */
    @Column(name = "DOMAIN")
    private String domain;

    /**
     * 收银是否可以用(Y: 是 N:否)
     */
    @Column(name = "FLAG_CASHIER_SYSTEM")
    private String flagCashierSystem;

    /**
     * APP是否可以用(Y: 是 N:否)
     */
    @Column(name = "FLAG_APP")
    private String flagApp;

    /**
     * 启用(Y: 是 N:否)
     */
    @Column(name = "STATUS")
    private String status;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取域名
     *
     * @return DOMAIN - 域名
     */
    public String getDomain() {
        return domain;
    }

    /**
     * 设置域名
     *
     * @param domain 域名
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * 获取收银是否可以用(Y: 是 N:否)
     *
     * @return FLAG_CASHIER_SYSTEM - 收银是否可以用(Y: 是 N:否)
     */
    public String getFlagCashierSystem() {
        return flagCashierSystem;
    }

    /**
     * 设置收银是否可以用(Y: 是 N:否)
     *
     * @param flagCashierSystem 收银是否可以用(Y: 是 N:否)
     */
    public void setFlagCashierSystem(String flagCashierSystem) {
        this.flagCashierSystem = flagCashierSystem;
    }

    /**
     * 获取APP是否可以用(Y: 是 N:否)
     *
     * @return FLAG_APP - APP是否可以用(Y: 是 N:否)
     */
    public String getFlagApp() {
        return flagApp;
    }

    /**
     * 设置APP是否可以用(Y: 是 N:否)
     *
     * @param flagApp APP是否可以用(Y: 是 N:否)
     */
    public void setFlagApp(String flagApp) {
        this.flagApp = flagApp;
    }

    /**
     * 获取启用(Y: 是 N:否)
     *
     * @return STATUS - 启用(Y: 是 N:否)
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置启用(Y: 是 N:否)
     *
     * @param status 启用(Y: 是 N:否)
     */
    public void setStatus(String status) {
        this.status = status;
    }
}