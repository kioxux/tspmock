package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralCashSet;
import com.gys.business.service.data.CashInData;
import com.gys.business.service.data.CashOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdIntegralCashSetMapper extends BaseMapper<GaiaSdIntegralCashSet> {
   String selectMaxId(@Param("client")  String client);

   List<CashOutData> cashSetList(CashInData inData);
}
