package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaGroupBasicRole;
import com.gys.business.service.data.auth.GroupRoleExt;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaGroupBasicRoleMapper extends BaseMapper<GaiaGroupBasicRole> {

    /**
     * 获取门店下角色信息
     *
     * @param client  加盟商
     * @param stoCode 门店
     * @param flag    是否查询默认角色 1 是
     * @return List<GroupRoleExt>
     */
    List<GroupRoleExt> selectRoleByStore(@Param("client") String client, @Param("stoCode") String stoCode, @Param("flag") Integer flag);

    List<GroupRoleExt> selectRoleByStoreWithoutPerson(@Param("client") String client, @Param("stoCode") String stoCode, @Param("flag") Integer flag);

}
