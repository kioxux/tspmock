package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_SD_STORE_DATA"
)
public class GaiaSdStoreData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String clientId;
   @Id
   @Column(
      name = "GSST_BR_ID"
   )
   private String gsstBrId;
   @Column(
      name = "GSST_BR_NAME"
   )
   private String gsstBrName;
   @Column(
      name = "GSST_SALE_MAX_QTY"
   )
   private String gsstSaleMaxQty;
   @Column(
      name = "GSST_LAST_DAY"
   )
   private String gsstLastDay;
   @Column(
      name = "GSST_PD_START_DATE"
   )
   private String gsstPdStartDate;
   @Column(
      name = "GSST_VERSION"
   )
   private String gsstVersion;
   @Column(
      name = "GSST_MIN_DISCOUNT_RATE"
   )
   private String gsstMinDiscountRate;
   @Column(
      name = "GSST_DYQ_MAX_AMT"
   )
   private BigDecimal gsstDyqMaxAmt;
   @Column(
      name = "GSST_DAILY_OPEN_DATE"
   )
   private String gsstDailyOpenDate;
   @Column(
      name = "GSST_DAILY_CHANGE_DATE"
   )
   private String gsstDailyChangeDate;
   @Column(
      name = "GSST_DAILY_CLOSE_DATE"
   )
   private String gsstDailyCloseDate;
   @Column(
      name = "GSST_JF_QLDATE"
   )
   private String gsstJfQldate;
   @Column(
      name = "GSST_MEM_NEW_MIX"
   )
   private String gsstMemNewMix;
   @Column(
      name = "GSST_MEN_NEW_AUTO"
   )
   private String gsstMenNewAuto;
   private static final long serialVersionUID = 1L;

   public String getClientId() {
      return this.clientId;
   }

   public void setClientId(String clientId) {
      this.clientId = clientId;
   }

   public String getGsstBrId() {
      return this.gsstBrId;
   }

   public void setGsstBrId(String gsstBrId) {
      this.gsstBrId = gsstBrId;
   }

   public String getGsstBrName() {
      return this.gsstBrName;
   }

   public void setGsstBrName(String gsstBrName) {
      this.gsstBrName = gsstBrName;
   }

   public String getGsstSaleMaxQty() {
      return this.gsstSaleMaxQty;
   }

   public void setGsstSaleMaxQty(String gsstSaleMaxQty) {
      this.gsstSaleMaxQty = gsstSaleMaxQty;
   }

   public String getGsstLastDay() {
      return this.gsstLastDay;
   }

   public void setGsstLastDay(String gsstLastDay) {
      this.gsstLastDay = gsstLastDay;
   }

   public String getGsstPdStartDate() {
      return this.gsstPdStartDate;
   }

   public void setGsstPdStartDate(String gsstPdStartDate) {
      this.gsstPdStartDate = gsstPdStartDate;
   }

   public String getGsstVersion() {
      return this.gsstVersion;
   }

   public void setGsstVersion(String gsstVersion) {
      this.gsstVersion = gsstVersion;
   }

   public String getGsstMinDiscountRate() {
      return this.gsstMinDiscountRate;
   }

   public void setGsstMinDiscountRate(String gsstMinDiscountRate) {
      this.gsstMinDiscountRate = gsstMinDiscountRate;
   }

   public BigDecimal getGsstDyqMaxAmt() {
      return this.gsstDyqMaxAmt;
   }

   public void setGsstDyqMaxAmt(BigDecimal gsstDyqMaxAmt) {
      this.gsstDyqMaxAmt = gsstDyqMaxAmt;
   }

   public String getGsstDailyOpenDate() {
      return this.gsstDailyOpenDate;
   }

   public void setGsstDailyOpenDate(String gsstDailyOpenDate) {
      this.gsstDailyOpenDate = gsstDailyOpenDate;
   }

   public String getGsstDailyChangeDate() {
      return this.gsstDailyChangeDate;
   }

   public void setGsstDailyChangeDate(String gsstDailyChangeDate) {
      this.gsstDailyChangeDate = gsstDailyChangeDate;
   }

   public String getGsstDailyCloseDate() {
      return this.gsstDailyCloseDate;
   }

   public void setGsstDailyCloseDate(String gsstDailyCloseDate) {
      this.gsstDailyCloseDate = gsstDailyCloseDate;
   }

   public String getGsstJfQldate() {
      return this.gsstJfQldate;
   }

   public void setGsstJfQldate(String gsstJfQldate) {
      this.gsstJfQldate = gsstJfQldate;
   }

   public String getGsstMemNewMix() {
      return this.gsstMemNewMix;
   }

   public void setGsstMemNewMix(String gsstMemNewMix) {
      this.gsstMemNewMix = gsstMemNewMix;
   }

   public String getGsstMenNewAuto() {
      return this.gsstMenNewAuto;
   }

   public void setGsstMenNewAuto(String gsstMenNewAuto) {
      this.gsstMenNewAuto = gsstMenNewAuto;
   }
}
