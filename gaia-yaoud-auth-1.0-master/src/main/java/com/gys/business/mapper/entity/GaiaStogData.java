package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_STOG_DATA"
)
public class GaiaStogData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "STOG_CODE"
   )
   private String stogCode;
   @Column(
      name = "STOG_NAME"
   )
   private String stogName;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String clent) {
      this.client = clent;
   }

   public String getStogCode() {
      return this.stogCode;
   }

   public void setStogCode(String stogCode) {
      this.stogCode = stogCode;
   }

   public String getStogName() {
      return this.stogName;
   }

   public void setStogName(String stogName) {
      this.stogName = stogName;
   }
}
