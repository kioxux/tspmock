package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaJyfwooData;
import com.gys.business.service.data.JyfwooOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaJyfwooDataMapper extends BaseMapper<GaiaJyfwooData> {
   List<JyfwooOutData> getJyfwooList();
}
