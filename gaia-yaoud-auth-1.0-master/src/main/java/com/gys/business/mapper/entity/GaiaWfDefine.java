package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_WF_DEFINE"
)
public class GaiaWfDefine implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "PO_COMPANY_CODE"
   )
   private String poCompanyCode;
   @Id
   @Column(
      name = "WF_DEFINE_CODE"
   )
   private String wfDefineCode;
   @Column(
      name = "WF_KIND"
   )
   private String wfKind;
   @Column(
      name = "WF_MODULE"
   )
   private String wfModule;
   @Column(
      name = "WF_REF_PAGE"
   )
   private String wfRefPage;
   @Column(
      name = "WF_REF_RETURN"
   )
   private String wfRefReturn;
   @Column(
      name = "CREATE_USER"
   )
   private String createUser;
   @Column(
      name = "CREATE_TIME"
   )
   private String createTime;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getPoCompanyCode() {
      return this.poCompanyCode;
   }

   public void setPoCompanyCode(String poCompanyCode) {
      this.poCompanyCode = poCompanyCode;
   }

   public String getWfDefineCode() {
      return this.wfDefineCode;
   }

   public void setWfDefineCode(String wfDefineCode) {
      this.wfDefineCode = wfDefineCode;
   }

   public String getWfKind() {
      return this.wfKind;
   }

   public void setWfKind(String wfKind) {
      this.wfKind = wfKind;
   }

   public String getWfModule() {
      return this.wfModule;
   }

   public void setWfModule(String wfModule) {
      this.wfModule = wfModule;
   }

   public String getWfRefPage() {
      return this.wfRefPage;
   }

   public void setWfRefPage(String wfRefPage) {
      this.wfRefPage = wfRefPage;
   }

   public String getWfRefReturn() {
      return this.wfRefReturn;
   }

   public void setWfRefReturn(String wfRefReturn) {
      this.wfRefReturn = wfRefReturn;
   }

   public String getCreateUser() {
      return this.createUser;
   }

   public void setCreateUser(String createUser) {
      this.createUser = createUser;
   }

   public String getCreateTime() {
      return this.createTime;
   }

   public void setCreateTime(String createTime) {
      this.createTime = createTime;
   }
}
