package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_SD_SYSTEM_PARA"
)
public class ParamDetailData {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String clientId;
   @Id
   @Column(
      name = "GSSP_BR_ID"
   )
   private String gsspBrId;
   @Id
   @Column(
      name = "GSSP_ID"
   )
   private String gsspId;
   @Column(
      name = "GSSP_NAME"
   )
   private String gsspName;
   @Column(
      name = "GSSP_PARA_REMARK"
   )
   private String gsspParaRemark;
   @Column(
      name = "GSSP_PARA"
   )
   private String gsspPara;
   @Column(
      name = "GSSP_UPDATE_EMP"
   )
   private String gsspUpdateEmp;
   @Column(
      name = "GSSP_UPDATE_DATE"
   )
   private String gsspUpdateDate;

   public String getClientId() {
      return this.clientId;
   }

   public void setClientId(String clientId) {
      this.clientId = clientId;
   }

   public String getGsspBrId() {
      return this.gsspBrId;
   }

   public void setGsspBrId(String gsspBrId) {
      this.gsspBrId = gsspBrId;
   }

   public String getGsspId() {
      return this.gsspId;
   }

   public void setGsspId(String gsspId) {
      this.gsspId = gsspId;
   }

   public String getGsspName() {
      return this.gsspName;
   }

   public void setGsspName(String gsspName) {
      this.gsspName = gsspName;
   }

   public String getGsspParaRemark() {
      return this.gsspParaRemark;
   }

   public void setGsspParaRemark(String gsspParaRemark) {
      this.gsspParaRemark = gsspParaRemark;
   }

   public String getGsspPara() {
      return this.gsspPara;
   }

   public void setGsspPara(String gsspPara) {
      this.gsspPara = gsspPara;
   }

   public String getGsspUpdateEmp() {
      return this.gsspUpdateEmp;
   }

   public void setGsspUpdateEmp(String gsspUpdateEmp) {
      this.gsspUpdateEmp = gsspUpdateEmp;
   }

   public String getGsspUpdateDate() {
      return this.gsspUpdateDate;
   }

   public void setGsspUpdateDate(String gsspUpdateDate) {
      this.gsspUpdateDate = gsspUpdateDate;
   }
}
