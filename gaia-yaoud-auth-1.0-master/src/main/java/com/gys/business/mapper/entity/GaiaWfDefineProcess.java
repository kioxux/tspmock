package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_WF_DEFINE_PROCESS"
)
public class GaiaWfDefineProcess implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "PO_COMPANY_CODE"
   )
   private String poCompanyCode;
   @Id
   @Column(
      name = "WF_DEFINE_CODE"
   )
   private String wfDefineCode;
   @Id
   @Column(
      name = "WF_SEQ"
   )
   private BigDecimal wfSeq;
   @Id
   @Column(
      name = "AUTH_SEQ"
   )
   private BigDecimal authSeq;
   @Column(
      name = "WF_KIND"
   )
   private String wfKind;
   @Column(
      name = "AUTH_GROUP_ID"
   )
   private String authGroupId;
   @Column(
      name = "CREATE_USER"
   )
   private String createUser;
   @Column(
      name = "CREATE_TIME"
   )
   private String createTime;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getPoCompanyCode() {
      return this.poCompanyCode;
   }

   public void setPoCompanyCode(String poCompanyCode) {
      this.poCompanyCode = poCompanyCode;
   }

   public String getWfDefineCode() {
      return this.wfDefineCode;
   }

   public void setWfDefineCode(String wfDefineCode) {
      this.wfDefineCode = wfDefineCode;
   }

   public BigDecimal getWfSeq() {
      return this.wfSeq;
   }

   public void setWfSeq(BigDecimal wfSeq) {
      this.wfSeq = wfSeq;
   }

   public BigDecimal getAuthSeq() {
      return this.authSeq;
   }

   public void setAuthSeq(BigDecimal authSeq) {
      this.authSeq = authSeq;
   }

   public String getWfKind() {
      return this.wfKind;
   }

   public void setWfKind(String wfKind) {
      this.wfKind = wfKind;
   }

   public String getAuthGroupId() {
      return this.authGroupId;
   }

   public void setAuthGroupId(String authGroupId) {
      this.authGroupId = authGroupId;
   }

   public String getCreateUser() {
      return this.createUser;
   }

   public void setCreateUser(String createUser) {
      this.createUser = createUser;
   }

   public String getCreateTime() {
      return this.createTime;
   }

   public void setCreateTime(String createTime) {
      this.createTime = createTime;
   }
}
