package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_STORE_DATA"
)
@Data
public class GaiaStoreData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "STO_CODE"
   )
   private String stoCode;
   @Column(
      name = "STO_NAME"
   )
   private String stoName;
   @Column(
      name = "STO_PYM"
   )
   private String stoPym;
   @Column(
      name = "STO_SHORT_NAME"
   )
   private String stoShortName;
   @Column(
      name = "STO_ATTRIBUTE"
   )
   private String stoAttribute;
   @Column(
      name = "STO_DELIVERY_MODE"
   )
   private String stoDeliveryMode;
   @Column(
      name = "STO_RELATION_STORE"
   )
   private String stoRelationStore;
   @Column(
      name = "STO_STATUS"
   )
   private String stoStatus;
   @Column(
      name = "STO_AREA"
   )
   private String stoArea;
   @Column(
      name = "STO_OPEN_DATE"
   )
   private String stoOpenDate;
   @Column(
      name = "STO_CLOSE_DATE"
   )
   private String stoCloseDate;
   @Column(
      name = "STO_ADD"
   )
   private String stoAdd;
   @Column(
      name = "STO_PROVINCE"
   )
   private String stoProvince;
   @Column(
      name = "STO_CITY"
   )
   private String stoCity;
   @Column(
      name = "STO_DISTRICT"
   )
   private String stoDistrict;
   @Column(
      name = "STO_IF_MEDICALCARE"
   )
   private String stoIfMedicalcare;
   @Column(
      name = "STO_IF_DTP"
   )
   private String stoIfDtp;
   @Column(
      name = "STO_TAX_CLASS"
   )
   private String stoTaxClass;
   @Column(
      name = "STO_DELIVERY_COMPANY"
   )
   private String stoDeliveryCompany;
   @Column(
      name = "STO_CHAIN_HEAD"
   )
   private String stoChainHead;
   @Column(
      name = "STO_TAX_SUBJECT"
   )
   private String stoTaxSubject;
   @Column(
      name = "STO_TAX_RATE"
   )
   private String stoTaxRate;
   @Column(
      name = "STO_NO"
   )
   private String stoNo;
   @Column(
      name = "STO_LEGAL_PERSON"
   )
   private String stoLegalPerson;
   @Column(
      name = "STO_QUA"
   )
   private String stoQua;
   @Column(
      name = "STO_CRE_DATE"
   )
   private String stoCreDate;
   @Column(
      name = "STO_CRE_TIME"
   )
   private String stoCreTime;
   @Column(
      name = "STO_CRE_ID"
   )
   private String stoCreId;
   @Column(
      name = "STO_MODI_DATE"
   )
   private String stoModiDate;
   @Column(
      name = "STO_MODI_TIME"
   )
   private String stoModiTime;
   @Column(
      name = "STO_MODI_ID"
   )
   private String stoModiId;
   @Column(
      name = "STO_LOGO"
   )
   private String stoLogo;
   @Column(
      name = "STOG_CODE"
   )
   private String stogCode;
   @Column(
      name = "STO_LEADER"
   )
   private String stoLeader;
   private static final long serialVersionUID = 1L;

}
