package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_SD_MEMBER_CLASS"
)
public class GaiaSdMemberClass implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String clientId;
   @Id
   @Column(
      name = "GSMC_ID"
   )
   private String gsmcId;
   @Column(
      name = "GSMC_NAME"
   )
   private String gsmcName;
   @Column(
      name = "GSMC_DISCOUNT_RATE"
   )
   private String gsmcDiscountRate;

   @Column(
           name = "GSMC_DISCOUNT_RATE2"
   )
   private String gsmcDiscountRate2;
   @Column(
      name = "GSMC_AMT_SALE"
   )
   private BigDecimal gsmcAmtSale;
   @Column(
      name = "GSMC_INTEGRAL_SALE"
   )
   private String gsmcIntegralSale;
   @Column(
           name = "GSMC_UPGRADE_INTE"
   )
   private String gsmcUpgradeInte;
   @Column(
           name = "GSMC_UPGRADE_RECH"
   )
   private String gsmcUpgradeRech;
   @Column(
           name = "GSMC_UPGRADE_SET"
   )
   private String gsmcUpgradeSet;
   @Column(
           name = "GSMC_CRE_DATE"
   )
   private String gsmcCreDate;
   @Column(
           name = "GSMC_CRE_EMP"
   )
   private String gsmcCreEmp;
   @Column(
           name = "GSMC_UPDATE_DATE"
   )
   private String gsmcUpdateDate;
   @Column(
           name = "GSMC_UPDATE_EMP"
   )
   private String gsmcUpdateEmp;
   private static final long serialVersionUID = 1L;

   public String getGsmcUpgradeInte() {
      return gsmcUpgradeInte;
   }

   public void setGsmcUpgradeInte(String gsmcUpgradeInte) {
      this.gsmcUpgradeInte = gsmcUpgradeInte;
   }

   public String getGsmcUpgradeRech() {
      return gsmcUpgradeRech;
   }

   public void setGsmcUpgradeRech(String gsmcUpgradeRech) {
      this.gsmcUpgradeRech = gsmcUpgradeRech;
   }

   public String getGsmcUpgradeSet() {
      return gsmcUpgradeSet;
   }

   public void setGsmcUpgradeSet(String gsmcUpgradeSet) {
      this.gsmcUpgradeSet = gsmcUpgradeSet;
   }

   public String getGsmcCreDate() {
      return gsmcCreDate;
   }

   public void setGsmcCreDate(String gsmcCreDate) {
      this.gsmcCreDate = gsmcCreDate;
   }

   public String getGsmcCreEmp() {
      return gsmcCreEmp;
   }

   public void setGsmcCreEmp(String gsmcCreEmp) {
      this.gsmcCreEmp = gsmcCreEmp;
   }

   public String getGsmcUpdateDate() {
      return gsmcUpdateDate;
   }

   public void setGsmcUpdateDate(String gsmcUpdateDate) {
      this.gsmcUpdateDate = gsmcUpdateDate;
   }

   public String getGsmcUpdateEmp() {
      return gsmcUpdateEmp;
   }

   public void setGsmcUpdateEmp(String gsmcUpdateEmp) {
      this.gsmcUpdateEmp = gsmcUpdateEmp;
   }

   public String getClientId() {
      return this.clientId;
   }

   public void setClientId(String clientId) {
      this.clientId = clientId;
   }

   public String getGsmcId() {
      return this.gsmcId;
   }

   public void setGsmcId(String gsmcId) {
      this.gsmcId = gsmcId;
   }

   public String getGsmcName() {
      return this.gsmcName;
   }

   public void setGsmcName(String gsmcName) {
      this.gsmcName = gsmcName;
   }

   public String getGsmcDiscountRate() {
      return this.gsmcDiscountRate;
   }

   public void setGsmcDiscountRate(String gsmcDiscountRate) {
      this.gsmcDiscountRate = gsmcDiscountRate;
   }

   public BigDecimal getGsmcAmtSale() {
      return this.gsmcAmtSale;
   }

   public void setGsmcAmtSale(BigDecimal gsmcAmtSale) {
      this.gsmcAmtSale = gsmcAmtSale;
   }

   public String getGsmcIntegralSale() {
      return this.gsmcIntegralSale;
   }

   public void setGsmcIntegralSale(String gsmcIntegralSale) {
      this.gsmcIntegralSale = gsmcIntegralSale;
   }

   public String getGsmcDiscountRate2() {
      return gsmcDiscountRate2;
   }

   public void setGsmcDiscountRate2(String gsmcDiscountRate2) {
      this.gsmcDiscountRate2 = gsmcDiscountRate2;
   }
}
