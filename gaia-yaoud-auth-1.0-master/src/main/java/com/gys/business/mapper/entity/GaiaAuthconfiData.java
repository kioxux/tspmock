package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_AUTHCONFI_DATA"
)
public class GaiaAuthconfiData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "AUTH_GROUP_ID"
   )
   private String authGroupId;
   @Id
   @Column(
      name = "AUTHOBJ_SITE"
   )
   private String authobjSite;
   @Id
   @Column(
      name = "AUTHCONFI_USER"
   )
   private String authconfiUser;
   @Column(
      name = "AUTH_GROUP_NAME"
   )
   private String authGroupName;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getAuthGroupId() {
      return this.authGroupId;
   }

   public void setAuthGroupId(String authGroupId) {
      this.authGroupId = authGroupId;
   }

   public String getAuthobjSite() {
      return this.authobjSite;
   }

   public void setAuthobjSite(String authobjSite) {
      this.authobjSite = authobjSite;
   }

   public String getAuthconfiUser() {
      return this.authconfiUser;
   }

   public void setAuthconfiUser(String authconfiUser) {
      this.authconfiUser = authconfiUser;
   }

   public String getAuthGroupName() {
      return this.authGroupName;
   }

   public void setAuthGroupName(String authGroupName) {
      this.authGroupName = authGroupName;
   }
}
