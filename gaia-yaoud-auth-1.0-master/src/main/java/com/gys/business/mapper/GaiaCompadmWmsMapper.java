package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaCompadmWms;
import com.gys.common.base.BaseMapper;

public interface GaiaCompadmWmsMapper extends BaseMapper<GaiaCompadmWms> {
}