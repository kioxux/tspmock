package com.gys.business.mapper.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "GAIA_USER_LOGIN_INFO")
public class GaiaUserLoginInfo implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商ID
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店ID
     */
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 登录时间
     */
    @Column(name = "LOGIN_TIME")
    private Date loginTime;

    /**
     * 用户名

     */
    @Column(name = "USER_NAME")
    private String userName;

    /**
     * 1:web 2:fx 3:app 4:web(验证码) 5:fx(验证码) 6:app(验证码)
     */
    @Column(name = "LOGIN_MODEL")
    private Integer loginModel;

    private static final long serialVersionUID = 1L;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商ID
     *
     * @return CLIENT - 加盟商ID
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商ID
     *
     * @param client 加盟商ID
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店ID
     *
     * @return BR_ID - 门店ID
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店ID
     *
     * @param brId 门店ID
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取登录时间
     *
     * @return LOGIN_TIME - 登录时间
     */
    public Date getLoginTime() {
        return loginTime;
    }

    /**
     * 设置登录时间
     *
     * @param loginTime 登录时间
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * 获取用户名

     *
     * @return USER_NAME - 用户名

     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名

     *
     * @param userName 用户名

     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取1:web 2:fx 3:app 4:web(验证码) 5:fx(验证码) 6:app(验证码)
     *
     * @return LOGIN_MODEL - 1:web 2:fx 3:app 4:web(验证码) 5:fx(验证码) 6:app(验证码)
     */
    public Integer getLoginModel() {
        return loginModel;
    }

    /**
     * 设置1:web 2:fx 3:app 4:web(验证码) 5:fx(验证码) 6:app(验证码)
     *
     * @param loginModel 1:web 2:fx 3:app 4:web(验证码) 5:fx(验证码) 6:app(验证码)
     */
    public void setLoginModel(Integer loginModel) {
        this.loginModel = loginModel;
    }
}