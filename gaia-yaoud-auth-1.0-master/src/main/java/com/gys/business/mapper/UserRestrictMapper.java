package com.gys.business.mapper;

import com.gys.business.mapper.entity.UserRestrict;
import com.gys.business.service.data.StoreOutData;
import com.gys.business.service.data.auth.UserRestrictListReq;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 用户数据范围权限表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-12-27
 */
@Mapper
public interface UserRestrictMapper extends BaseMapper<UserRestrict> {
    List<StoreOutData> getStoreList(UserRestrictListReq inData);
}
