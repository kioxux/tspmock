package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDomainConfig;
import com.gys.common.base.BaseMapper;

public interface GaiaDomainConfigMapper extends BaseMapper<GaiaDomainConfig> {
}