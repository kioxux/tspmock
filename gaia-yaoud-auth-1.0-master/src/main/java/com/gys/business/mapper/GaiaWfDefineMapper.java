package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWfDefine;
import com.gys.business.service.data.GetDefineInData;
import com.gys.business.service.data.GetDefineOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaWfDefineMapper extends BaseMapper<GaiaWfDefine> {
   List<GetDefineOutData> selectDefaultList(GetDefineInData inData);
}
