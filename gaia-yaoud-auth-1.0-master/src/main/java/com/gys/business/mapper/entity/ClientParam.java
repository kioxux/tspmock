package com.gys.business.mapper.entity;

import lombok.Data;

/**
 * @desc: 加盟商系统参数实体
 * @author: Ryan
 * @createTime: 2021/10/25 14:17
 */
@Data
public class ClientParam {
    private String client;
    private String gcspId;
    private String gcspName;
    private String gcspParaRemark;
    private String gcspPara1;
    private String gcspPara2;
    private String gcspUpdateEmp;
    private String gcspUpdateDate;
    private String gcspUpdateTime;
}
