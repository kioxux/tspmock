package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWfDefineProcess;
import com.gys.business.service.data.GetDefineInData;
import com.gys.business.service.data.GetWfDefineProcessOutData;
import com.gys.business.service.data.GetWorkflowApproveOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWfDefineProcessMapper extends BaseMapper<GaiaWfDefineProcess> {
   List<GetWfDefineProcessOutData> selectApproveUser(@Param("client") String client, @Param("defineCode") String defineCode, @Param("site") String site);

   List<GetWorkflowApproveOutData> selectDefineApproveList(@Param("client") String client, @Param("defineCode") String defineCode);

   List<GetWfDefineProcessOutData> checkApproveUser(GetDefineInData defineInData);
}
