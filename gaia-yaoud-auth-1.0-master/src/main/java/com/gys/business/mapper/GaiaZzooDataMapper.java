package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaZzooData;
import com.gys.business.service.data.ZzooOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaZzooDataMapper extends BaseMapper<GaiaZzooData> {
   List<ZzooOutData> getZzooList();
}
