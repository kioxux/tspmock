package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 权限明细表
 * </p>
 *
 * @author flynn
 * @since 2021-12-22
 */
@Data
@Table( name = "GAIA_AUTH_DETAIL")
public class AuthDetail implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "权限ID")
    @Column(name = "ID")
    private String id;

    @ApiModelProperty(value = "描述")
    @Column(name = "TITLE")
    private String title;

    @ApiModelProperty(value = "权限名称")
    @Column(name = "NAME")
    private String name;

    @ApiModelProperty(value = "类型（1模块 2页面 3按钮 4列表项）")
    @Column(name = "AUTH_TYPE")
    private String authType;

    @ApiModelProperty(value = "按钮类型 1-新增 2-编辑 3-查看 4-审核 5-导入 6-导出")
    @Column(name = "BUTTON_TYPE")
    private String buttonType;

    @ApiModelProperty(value = "唯一code")
    @Column(name = "CODE")
    private String code;

    @ApiModelProperty(value = "是哪个端（产品）功能的权限")
    @Column(name = "PRODUCT")
    private String product;

    @ApiModelProperty(value = "所属模块")
    @Column(name = "MODEL")
    private String model;

    @ApiModelProperty(value = "父节点的ID")
    @Column(name = "PARENT_ID")
    private String parentId;

    @ApiModelProperty(value = "路由/跳转地址")
    @Column(name = "PATH")
    private String path;

    @ApiModelProperty(value = "页面地址")
    @Column(name = "PAGE_PATH")
    private String pagePath;

    @ApiModelProperty(value = "请求路径")
    @Column(name = "REQUEST_PATH")
    private String requestPath;

    @ApiModelProperty(value = "Y:是N：否")
    @Column(name = "MAIN_FORM")
    private String mainForm;

    @ApiModelProperty(value = "是否收费（0免费 1收费）")
    @Column(name = "PAY_FLAG")
    private Boolean payFlag;

    @ApiModelProperty(value = "是否是指定客户的专属功能(0开放给所有用户 1只开放给部分用户)")
    @Column(name = "PRIVATE_FLAG")
    private Boolean privateFlag;

    @ApiModelProperty(value = "排序")
    @Column(name = "SORT")
    private Integer sort;

    @ApiModelProperty(value = "图标")
    @Column(name = "ICON")
    private String icon;

    @ApiModelProperty(value = "是否删除（0不删除 1删除）")
    @Column(name = "DELETE_FLAG")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人ID")
    @Column(name = "MOD_ID")
    private String modId;


}
