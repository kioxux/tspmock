package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.service.data.DeptInData;
import com.gys.business.service.data.DeptOutData;
import com.gys.business.service.data.dep.dto.DepDto;
import com.gys.business.service.data.dep.vo.DepVo;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaDepDataMapper extends BaseMapper<GaiaDepData> {
   int selectMaxId(@Param("client") String client);

   List<DeptOutData> getDeptList(String client);

   List<DeptOutData> getDeptListByType(DeptInData inData);

   List<GaiaDepData> selectListForAuth(@Param("client") String client, @Param("chainHead") String chainHead);

   List<DepVo> getAllDeptList(DepDto dto);
}
