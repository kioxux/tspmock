package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_DC_DATA"
)
public class GaiaDcData implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "DC_CODE"
   )
   private String dcCode;
   @Column(
      name = "DC_NAME"
   )
   private String dcName;
   @Column(
      name = "DC_PYM"
   )
   private String dcPym;
   @Column(
      name = "DC_SHORT_NAME"
   )
   private String dcShortName;
   @Column(
      name = "DC_ADD"
   )
   private String dcAdd;
   @Column(
      name = "DC_TEL"
   )
   private String dcTel;
   @Column(
      name = "DC_STATUS"
   )
   private String dcStatus;
   @Column(
      name = "DC_INVENT"
   )
   private String dcInvent;
   @Column(
      name = "DC_WHOLESALE"
   )
   private String dcWholesale;
   @Column(
      name = "DC_CHAIN_HEAD"
   )
   private String dcChainHead;
   @Column(
      name = "DC_TYPE"
   )
   private String dcType;
   @Column(
      name = "DC_TAX_SUBJECT"
   )
   private String dcTaxSubject;
   @Column(
      name = "DC_HEAD_ID"
   )
   private String dcHeadId;
   @Column(
      name = "DC_HEAD_NAM"
   )
   private String dcHeadNam;
   @Column(
      name = "DC_CRE_DATE"
   )
   private String dcCreDate;
   @Column(
      name = "DC_CRE_TIME"
   )
   private String dcCreTime;
   @Column(
      name = "DC_CRE_ID"
   )
   private String dcCreId;
   @Column(
      name = "DC_MODI_DATE"
   )
   private String dcModiDate;
   @Column(
      name = "DC_MODI_TIME"
   )
   private String dcModiTime;
   @Column(
      name = "DC_MODI_ID"
   )
   private String dcModiId;
   private static final long serialVersionUID = 1L;

   public String getClient() {
      return this.client;
   }

   public void setClient(String client) {
      this.client = client;
   }

   public String getDcCode() {
      return this.dcCode;
   }

   public void setDcCode(String dcCode) {
      this.dcCode = dcCode;
   }

   public String getDcName() {
      return this.dcName;
   }

   public void setDcName(String dcName) {
      this.dcName = dcName;
   }

   public String getDcPym() {
      return this.dcPym;
   }

   public void setDcPym(String dcPym) {
      this.dcPym = dcPym;
   }

   public String getDcShortName() {
      return this.dcShortName;
   }

   public void setDcShortName(String dcShortName) {
      this.dcShortName = dcShortName;
   }

   public String getDcAdd() {
      return this.dcAdd;
   }

   public void setDcAdd(String dcAdd) {
      this.dcAdd = dcAdd;
   }

   public String getDcTel() {
      return this.dcTel;
   }

   public void setDcTel(String dcTel) {
      this.dcTel = dcTel;
   }

   public String getDcStatus() {
      return this.dcStatus;
   }

   public void setDcStatus(String dcStatus) {
      this.dcStatus = dcStatus;
   }

   public String getDcInvent() {
      return this.dcInvent;
   }

   public void setDcInvent(String dcInvent) {
      this.dcInvent = dcInvent;
   }

   public String getDcWholesale() {
      return this.dcWholesale;
   }

   public void setDcWholesale(String dcWholesale) {
      this.dcWholesale = dcWholesale;
   }

   public String getDcChainHead() {
      return this.dcChainHead;
   }

   public void setDcChainHead(String dcChainHead) {
      this.dcChainHead = dcChainHead;
   }

   public String getDcTaxSubject() {
      return this.dcTaxSubject;
   }

   public void setDcTaxSubject(String dcTaxSubject) {
      this.dcTaxSubject = dcTaxSubject;
   }

   public String getDcHeadId() {
      return this.dcHeadId;
   }

   public void setDcHeadId(String dcHeadId) {
      this.dcHeadId = dcHeadId;
   }

   public String getDcHeadNam() {
      return this.dcHeadNam;
   }

   public void setDcHeadNam(String dcHeadNam) {
      this.dcHeadNam = dcHeadNam;
   }

   public String getDcCreDate() {
      return this.dcCreDate;
   }

   public void setDcCreDate(String dcCreDate) {
      this.dcCreDate = dcCreDate;
   }

   public String getDcCreTime() {
      return this.dcCreTime;
   }

   public void setDcCreTime(String dcCreTime) {
      this.dcCreTime = dcCreTime;
   }

   public String getDcCreId() {
      return this.dcCreId;
   }

   public void setDcCreId(String dcCreId) {
      this.dcCreId = dcCreId;
   }

   public String getDcModiDate() {
      return this.dcModiDate;
   }

   public void setDcModiDate(String dcModiDate) {
      this.dcModiDate = dcModiDate;
   }

   public String getDcModiTime() {
      return this.dcModiTime;
   }

   public void setDcModiTime(String dcModiTime) {
      this.dcModiTime = dcModiTime;
   }

   public String getDcModiId() {
      return this.dcModiId;
   }

   public void setDcModiId(String dcModiId) {
      this.dcModiId = dcModiId;
   }

   public String getDcType() {
      return this.dcType;
   }

   public void setDcType(String dcType) {
      this.dcType = dcType;
   }
}
