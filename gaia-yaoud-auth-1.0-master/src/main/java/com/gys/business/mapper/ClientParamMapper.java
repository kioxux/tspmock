package com.gys.business.mapper;

import com.gys.business.mapper.entity.ClientParam;
import com.gys.business.mapper.entity.SdParam;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/25 14:18
 */
public interface ClientParamMapper {

    ClientParam getUnique(ClientParam cond);

    int add(ClientParam clientParam);

    SdParam getUniqueSdParam(SdParam param);
}
