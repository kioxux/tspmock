package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 陈浩
 */
@Table(
        name = "GAIA_WF_RECORD_HISTORY"
)
@Data
public class GaiaWfRecordHistory implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "PO_COMPANY_CODE"
    )
    private String poCompanyCode;
    @Id
    @Column(
            name = "WF_CODE"
    )
    private String wfCode;
    @Id
    @Column(
            name = "WF_HISTORY_SEQ"
    )
    private BigDecimal wfHistorySeq;
    @Column(
            name = "WF_DEFINE_CODE"
    )
    private String wfDefineCode;
    @Column(
            name = "WF_KIND"
    )
    private String wfKind;
    @Column(
            name = "WF_SEQ"
    )
    private BigDecimal wfSeq;
    @Column(
            name = "USER_ID"
    )
    private String userId;
    @Column(
            name = "RESULT"
    )
    private String result;
    @Column(
            name = "MEMO"
    )
    private String memo;
    @Column(
            name = "REJECT_SEQ"
    )
    private BigDecimal rejectSeq;
    @Column(
            name = "CREATE_USER"
    )
    private String createUser;
    @Column(
            name = "CREATE_TIME"
    )
    private String createTime;

    private static final long serialVersionUID = 1L;

}
