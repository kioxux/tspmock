package com.gys.business.mapper.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * GAIA_SD_MESSAGE
 * @author 
 */
@Data
public class GaiaSdMessageKey implements Serializable {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String gsmId;

    /**
     * 消息单号
     */
    private String gsmVoucherId;

    private static final long serialVersionUID = 1L;
}