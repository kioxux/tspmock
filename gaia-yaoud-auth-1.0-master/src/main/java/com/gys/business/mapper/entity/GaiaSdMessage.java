package com.gys.business.mapper.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * GAIA_SD_MESSAGE
 * @author tzh
 */
@Data
public class GaiaSdMessage extends GaiaSdMessageKey implements Serializable {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String gsmId;

    /**
     * 消息单号
     */
    private String gsmVoucherId;
    /**
     * 消息类型
     */
    private String gsmType;

    /**
     * 消息类型名称
     */
    private String gsmTypeName;

    /**
     * 消息值
     */
    private String gsmValue;

    /**
     * 消息内容
     */
    private String gsmRemark;

    /**
     * 是否查看
     */
    private String gsmFlag;

    /**
     * 跳转页面
     */
    private String gsmPage;

    /**
     * 效期天数
     */
    private String gsmWarningDay;

    /**
     * 业务单号
     */
    private String gsmBusinessVoucherId;

    /**
     * 消息送达日期
     */
    private String gsmArriveDate;

    /**
     * 消息送达时间
     */
    private String gsmArriveTime;

    /**
     * 消息查看日期
     */
    private String gsmCheckDate;

    /**
     * 消息查看时间
     */
    private String gsmCheckTime;

    /**
     * 消息查看人员
     */
    private String gsmCheckEmp;

    /**
     * 平台类型  APP  WEB  FX
     */
    private String gsmPlatForm;

    /**
     * 消息删除标记
     */
    private String gsmDeleteFlag;

    private static final long serialVersionUID = 1L;
}