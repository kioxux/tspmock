package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.common.base.BaseMapper;

public interface GaiaDcDataMapper extends BaseMapper<GaiaDcData> {
   int selectMaxId();
}
