package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.data.GetStoDepInData;
import com.gys.business.service.data.GetStoDepOutData;
import com.gys.business.service.data.UserInData;
import com.gys.business.service.data.UserOutData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaUserDataMapper extends BaseMapper<GaiaUserData> {
   int selectMaxId(String client);

   UserOutData selectByDeptId(@Param("client") String client,@Param("depHeadId") String depHeadId);

   List<UserOutData> getUserList(UserInData inData);

   List<UserOutData> getUsersByClientId(UserInData inData);

   List<UserOutData> getUserAllBySto(UserInData inData);

   List<UserOutData> selectPharmacistByBrId(UserInData inData);

   void updateUserInfo(UserInData inData);

   List<UserOutData> getUserAllByCompadm(UserInData inData);

    List<UserOutData> getUserAllByCompadmWms(UserInData inData);

   List<UserOutData> getUserAllByDep(UserInData inData);

   List<UserOutData> getUserAll(UserInData inData);

    List<GetStoDepOutData> getStoDepList(GetStoDepInData inData);

    int countAdminGroup(GaiaUserData user);

    int updateDefaultSite(GaiaUserData userData);

    List<GaiaUserData> getPharmacist(GetLoginOutData loginUser);

    void updateNbybInfo(GaiaUserData loginUser);

}
