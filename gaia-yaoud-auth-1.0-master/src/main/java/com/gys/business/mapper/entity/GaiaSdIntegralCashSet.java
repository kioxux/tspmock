package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
   name = "GAIA_SD_INTEGRAL_CASH_SET"
)
public class GaiaSdIntegralCashSet implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String clientId;
   @Id
   @Column(
      name = "GSICS_VOUCHER_ID"
   )
   private String gsicsVoucherId;
   @Column(
      name = "GSICS_BR_ID"
   )
   private String gsicsBrId;
   @Column(
      name = "GSICS_BR_NAME"
   )
   private String gsicsBrName;
   @Column(
      name = "GSICS_MEMBER_CLASS"
   )
   private String gsicsMemberClass;
   @Column(
      name = "GSICS_DATE_START"
   )
   private String gsicsDateStart;
   @Column(
      name = "GSICS_DATE_END"
   )
   private String gsicsDateEnd;
   @Column(
      name = "GSICS_INTEGRAL_DEDUCT"
   )
   private String gsicsIntegralDeduct;
   @Column(
      name = "GSICS_AMT_OFFSET"
   )
   private BigDecimal gsicsAmtOffset;
   @Column(
      name = "GSICS_AMT_OFFSET_MAX"
   )
   private BigDecimal gsicsAmtOffsetMax;
   @Column(
      name = "GSICS_DESCRIPTION"
   )
   private String gsicsDescription;
   @Column(
      name = "GSICS_INTEGRAL_ADD_SET"
   )
   private String gsicsIntegralAddSet;
   @Column(
      name = "GSICS_CHANGE_SET"
   )
   private String gsicsChangeSet;
   @Column(
      name = "GSICS_EMP"
   )
   private String gsicsEmp;
   private static final long serialVersionUID = 1L;

   public String getClientId() {
      return this.clientId;
   }

   public void setClientId(String clientId) {
      this.clientId = clientId;
   }

   public String getGsicsVoucherId() {
      return this.gsicsVoucherId;
   }

   public void setGsicsVoucherId(String gsicsVoucherId) {
      this.gsicsVoucherId = gsicsVoucherId;
   }

   public String getGsicsBrId() {
      return this.gsicsBrId;
   }

   public void setGsicsBrId(String gsicsBrId) {
      this.gsicsBrId = gsicsBrId;
   }

   public String getGsicsBrName() {
      return this.gsicsBrName;
   }

   public void setGsicsBrName(String gsicsBrName) {
      this.gsicsBrName = gsicsBrName;
   }

   public String getGsicsMemberClass() {
      return this.gsicsMemberClass;
   }

   public void setGsicsMemberClass(String gsicsMemberClass) {
      this.gsicsMemberClass = gsicsMemberClass;
   }

   public String getGsicsDateStart() {
      return this.gsicsDateStart;
   }

   public void setGsicsDateStart(String gsicsDateStart) {
      this.gsicsDateStart = gsicsDateStart;
   }

   public String getGsicsDateEnd() {
      return this.gsicsDateEnd;
   }

   public void setGsicsDateEnd(String gsicsDateEnd) {
      this.gsicsDateEnd = gsicsDateEnd;
   }

   public String getGsicsIntegralDeduct() {
      return this.gsicsIntegralDeduct;
   }

   public void setGsicsIntegralDeduct(String gsicsIntegralDeduct) {
      this.gsicsIntegralDeduct = gsicsIntegralDeduct;
   }

   public BigDecimal getGsicsAmtOffset() {
      return this.gsicsAmtOffset;
   }

   public void setGsicsAmtOffset(BigDecimal gsicsAmtOffset) {
      this.gsicsAmtOffset = gsicsAmtOffset;
   }

   public BigDecimal getGsicsAmtOffsetMax() {
      return this.gsicsAmtOffsetMax;
   }

   public void setGsicsAmtOffsetMax(BigDecimal gsicsAmtOffsetMax) {
      this.gsicsAmtOffsetMax = gsicsAmtOffsetMax;
   }

   public String getGsicsDescription() {
      return this.gsicsDescription;
   }

   public void setGsicsDescription(String gsicsDescription) {
      this.gsicsDescription = gsicsDescription;
   }

   public String getGsicsIntegralAddSet() {
      return this.gsicsIntegralAddSet;
   }

   public void setGsicsIntegralAddSet(String gsicsIntegralAddSet) {
      this.gsicsIntegralAddSet = gsicsIntegralAddSet;
   }

   public String getGsicsChangeSet() {
      return this.gsicsChangeSet;
   }

   public void setGsicsChangeSet(String gsicsChangeSet) {
      this.gsicsChangeSet = gsicsChangeSet;
   }

   public String getGsicsEmp() {
      return this.gsicsEmp;
   }

   public void setGsicsEmp(String gsicsEmp) {
      this.gsicsEmp = gsicsEmp;
   }
}
