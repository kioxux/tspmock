package com.gys.business.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @desc: 服务配置类
 * @author: ryan
 * @createTime: 2021/5/31 15:24
 */
@Configuration
public class WebAppConfig {

    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        //核心工作线程数
        threadPoolTaskExecutor.setCorePoolSize(1);
        //最大工作线程数
        threadPoolTaskExecutor.setMaxPoolSize(10);
        //允许线程空闲时间
        threadPoolTaskExecutor.setKeepAliveSeconds(10);
        //核心线程满之后，排队的队列大小
        threadPoolTaskExecutor.setQueueCapacity(20);
        //拒绝策略
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return threadPoolTaskExecutor;
    }
}
