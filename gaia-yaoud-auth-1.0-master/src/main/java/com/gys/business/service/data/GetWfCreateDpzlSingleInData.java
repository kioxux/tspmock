package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateDpzlSingleInData {
   @Field(
      name = "销售单号"
   )
   @JSONField(
      ordinal = 1
   )
   private String saleOrder = "";
   @Field(
      name = "销售日期"
   )
   @JSONField(
      ordinal = 2
   )
   private String saleDate = "";
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 4
   )
   private String proCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 5
   )
   private String proName = "";
   @Field(
      name = "规格/单位"
   )
   @JSONField(
      ordinal = 6
   )
   private String proSpecs = "";
   @Field(
      name = "数量"
   )
   @JSONField(
      ordinal = 7
   )
   private String qty = "";
   @Field(
      name = "零售单价"
   )
   @JSONField(
      ordinal = 8
   )
   private String salePrice = "";
   @Field(
      name = "应收总价"
   )
   @JSONField(
      ordinal = 9
   )
   private String totalPrice = "";
   @Field(
      name = "折率"
   )
   @JSONField(
      ordinal = 10
   )
   private String rate = "";
   @Field(
      name = "折后单价"
   )
   @JSONField(
      ordinal = 11
   )
   private String changeSalePrice = "";
   @Field(
      name = "折后总价"
   )
   @JSONField(
      ordinal = 12
   )
   private String changeTotalPrice = "";
   @Field(
      name = "批次成本"
   )
   @JSONField(
      ordinal = 13
   )
   private String batchCost = "";
   @Field(
      name = "加权移动平均成本"
   )
   @JSONField(
      ordinal = 14
   )
   private String wmaCost = "";
}
