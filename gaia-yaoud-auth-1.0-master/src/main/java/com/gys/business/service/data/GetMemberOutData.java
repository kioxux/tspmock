package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetMemberOutData {
   private String brId;
   private String brName;
   private String gsmbCardId;
   private String gsmbStatus;
   private String gsmbCreatDate;
   private String gsmbType;
   private String gsmbChannel;
}
