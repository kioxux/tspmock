package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetButtonAuthObjOutData {
   private String id;
   private String name;
   private String type;
}
