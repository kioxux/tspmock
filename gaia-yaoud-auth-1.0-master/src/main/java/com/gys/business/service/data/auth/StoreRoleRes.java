package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午3:43
 */
@Data
public class StoreRoleRes implements Serializable {

    private String stoCode;

    private String stoName;

    private Integer roleNums;

    private String stoType;

    private List<GroupRoleExt> roleList;
}

