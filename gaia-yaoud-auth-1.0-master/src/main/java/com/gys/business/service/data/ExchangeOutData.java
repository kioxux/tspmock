package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class ExchangeOutData {
   private String clientId;
   private String gsiesVoucherId;
   private String gsiesBrId;
   private String gsiesBrName;
   private String gsiesStoreGroup;
   private String gsiesMemberClass;
   private String gsiesMemberClassName;
   private String gsiesDateStart;
   private String gsiesDateEnd;
   private String gsiesExchangeProId;
   private String gsiesExchangeProName;
   private String gsiesExchangeProSpec;
   private String gsiesExchangeFactoryName;
   private String gsiesExchangeAddress;
   private String gsiesExchangePrice;
   private String gsieslExchangeIntegra;
   private BigDecimal gsiesExchangeAmt;
   private String gsiesExchangeQty;
   private String gsiesExchangeRepeat;
   private String gsiesIntegralAddSet;
   private String gsiesStatus;
}
