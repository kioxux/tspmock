package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 根据用户数据权限返回的最终门店类
 * 此基础类封装的初始目的是用于统一封装根据用户权限加载门店初始化的下拉框，以及用于sql中涉及到数据权限的入参
 * 后续不允许修改此类，如有修改，麻烦联系 《樊挺》
 * @author: flynn
 * @date: 2022年01月27日 上午11:23
 */
@Data
public class StoreRestrictBasicRes implements Serializable {

    private static final long serialVersionUID = 7019523613842868215L;

    //加盟商编号
    private String client;

    //门店编码
    private String stoCode;

    //门店名称
    private String stoName;

    //门店简称
    private String stoShortName;


}

