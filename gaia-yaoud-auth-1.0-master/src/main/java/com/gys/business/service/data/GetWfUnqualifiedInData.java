package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfUnqualifiedInData {
    @Field(
            name = "行号"
    )
    @JSONField(
            ordinal = 1
    )
    private String serial = "";

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 2
    )
    private String proId = "";

    @Field(
            name = "通用名"
    )
    @JSONField(
            ordinal = 3
    )
    private String commonName = "";

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 4
    )
    private String specs = "";

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 5
    )
    private String factoryName = "";

    @Field(
            name = "批号"
    )
    @JSONField(
            ordinal = 6
    )
    private String batchNo = "";

    @Field(
            name = "效期"
    )
    @JSONField(
            ordinal = 7
    )
    private String validDate = "";

    @Field(
            name = "损溢数量"
    )
    @JSONField(
            ordinal = 8
    )
    private String qty = "";

    @Field(
            name = "报损原因"
    )
    @JSONField(
            ordinal = 9
    )
    private String remark = "";

    private String billDate;

    private String stoName;
}
