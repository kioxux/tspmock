package com.gys.business.service.impl;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaAreaMapper;
import com.gys.business.mapper.UserRestrictDetailMapper;
import com.gys.business.mapper.UserRestrictMapper;
import com.gys.business.mapper.entity.UserRestrict;
import com.gys.business.mapper.entity.UserRestrictDetail;
import com.gys.business.service.AreaService;
import com.gys.business.service.data.AreaDto;
import com.gys.business.service.data.auth.AreaStoreNumVo;
import com.gys.common.data.JsonResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AreaServiceImpl implements AreaService {

    @Resource
    private GaiaAreaMapper gaiaAreaMapper;
    @Resource
    private UserRestrictMapper userRestrictMapper;
    @Resource
    private UserRestrictDetailMapper userRestrictDetailMapper;

    @Override
    public JsonResult getAreaList(String client, String userId) {

        List<AreaStoreNumVo> areaStoreNumVoList = gaiaAreaMapper.getAreaStoreNumVoList(client);
        Map<String, Integer> areaStoreNumMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(areaStoreNumVoList)) {
            areaStoreNumMap = areaStoreNumVoList.stream().collect(Collectors.toMap(AreaStoreNumVo::getId, AreaStoreNumVo::getStoNum));
        }

        Map<String,String> userRestrictDetailMap = new HashMap<>();

        UserRestrict queryUserRestrict = new UserRestrict();
        queryUserRestrict.setClientId(client);
        queryUserRestrict.setUserId(userId);
        queryUserRestrict.setDeleteFlag(false);
        UserRestrict userRestrictOld = userRestrictMapper.selectOne(queryUserRestrict);
        if (userRestrictOld != null && "2".equals(userRestrictOld.getRestrictType())) {
            UserRestrictDetail delUserRestrictDetail = new UserRestrictDetail();
            delUserRestrictDetail.setRestrictId(userRestrictOld.getId());
            delUserRestrictDetail.setDeleteFlag(false);
            List<UserRestrictDetail> details = userRestrictDetailMapper.select(delUserRestrictDetail);
            if(CollectionUtil.isNotEmpty(details)){
                userRestrictDetailMap = details.stream().collect(Collectors.toMap(UserRestrictDetail::getCompCode, UserRestrictDetail::getCompCode));
            }
        }


        List<AreaDto> list = gaiaAreaMapper.getAreaListNew(client);
        List<AreaDto> resultList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            resultList = list.stream().filter(t -> t.getLevel().equals("3")).collect(Collectors.toList());
        }


        for (AreaDto entity : resultList) {
            entity.setIfUserChoose(userRestrictDetailMap.get(entity.getAreaId()) != null);
            entity.setStoNum(areaStoreNumMap.get(entity.getId()) == null ? 0 : areaStoreNumMap.get(entity.getId()));
            entity.setChildren(getChildNodes(entity.getId(), list, areaStoreNumMap,userRestrictDetailMap));
        }

        return JsonResult.success(resultList);
    }

    /**
     * 获取子节点下
     *
     * @param districtCode
     * @param list
     * @return
     */
    private List<AreaDto> getChildNodes(String districtCode, List<AreaDto> list, Map<String, Integer> areaStoreNumMap,Map<String,String> userRestrictDetailMap) {
        List<AreaDto> childList = new ArrayList<>();
        for (AreaDto areaDto : list) {
            areaDto.setIfUserChoose(userRestrictDetailMap.get(areaDto.getAreaId()) != null);
            areaDto.setStoNum(areaStoreNumMap.get(areaDto.getId()) == null ? 0 : areaStoreNumMap.get(areaDto.getId()));
            if (StrUtil.isNotBlank(areaDto.getParentId()) && districtCode.equals(areaDto.getParentId())) {
                childList.add(areaDto);
            }
        }

        if (childList.size() == 0) {
            return childList;
        }

        for (AreaDto entity : childList) {
            entity.setIfUserChoose(userRestrictDetailMap.get(entity.getAreaId()) != null);
            entity.setStoNum(areaStoreNumMap.get(entity.getId()) == null ? 0 : areaStoreNumMap.get(entity.getId()));
            entity.setChildren(getChildNodes(entity.getId(), list, areaStoreNumMap,userRestrictDetailMap));
        }
        return childList;

    }
}
