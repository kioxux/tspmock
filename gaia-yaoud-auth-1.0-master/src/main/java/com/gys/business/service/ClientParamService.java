package com.gys.business.service;

import com.gys.business.mapper.entity.ClientParam;
import com.gys.business.mapper.entity.SdParam;

public interface ClientParamService {

   ClientParam getConfig(String client);

   SdParam getReceivePriceConfig(String client, String depId);
}
