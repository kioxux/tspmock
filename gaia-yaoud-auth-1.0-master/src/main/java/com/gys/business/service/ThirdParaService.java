package com.gys.business.service;

import com.gys.business.service.data.GetCosParaOutData;
import com.gys.business.service.data.GetSmsParaOutData;

public interface ThirdParaService {
   GetCosParaOutData getCosPara();

   GetSmsParaOutData getPhoneCode();

   GetSmsParaOutData getSmsFranchisee();

   GetSmsParaOutData getSmsFranchiseeNew(String type);

   GetSmsParaOutData addUser();
}
