package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaWfRecord;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;

public interface WorkflowService {
   GaiaWfRecord createWorkflow(GetWfCreateInData wfCreateInData, GetLoginOutData loginOutData);

   PageInfo<GetWorkflowOutData> selectApprovingList(GetWorkflowInData inData);

   PageInfo<GetWorkflowOutData> getMyApprovalList(GetWorkflowInData inData);

   PageInfo<GetWorkflowOutData> selectApprovedList(GetWorkflowInData inData);

   GetWorkflowOutData selectOne(GetWorkflowInData inData);

   List<GetWorkflowDetailListData> selectDetailList(GetWorkflowInData inData);

   void approve(GetWorkflowInData inData);

   void cc(GetWorkflowInData inData);

   void cancel(GetWorkflowInData inData);

   List<GetWorkflowApproveOutData> selectWorkflowApproveProcess(GetWfCreateInData inData);

   List<GetWorkflowOutData> getByClientAndTitle(GetWorkflowInData inData);

   List<GetStoDepOutData> getStoDepList(GetStoDepInData inData);
}
