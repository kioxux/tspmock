package com.gys.business.service.data;

import lombok.Data;

@Data
public class UserInData {
   private String userId;
   private String client;
   private String userNam;
   private String userSex;
   private String userTel;
   private String userIdc;
   private String userYsId;
   private String userYsZyfw;
   private String userYsZylb;
   private String depId;
   private String depName;
   private String userAddr;
   private String userEmail;
   private String userSta;
   private Boolean hideDis;
   private String francCreDate;
   private String francCreTime;
   private String francCreId;
   private String francModiDate;
   private String francModiTime;
   private String francModiId;
   private String userJoinDate;
   private String userDisDate;
   public int pageSize;
   private int pageNum;
   private String sourceType;
   private Integer userType;
   private String userYblpaId;

   /**
    * 操作平台
    */
   private String platform;
}
