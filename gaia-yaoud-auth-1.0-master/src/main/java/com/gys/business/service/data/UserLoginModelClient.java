package com.gys.business.service.data;


import lombok.Data;

@Data
public class UserLoginModelClient {
    private String client;
    private String francName;
    private String userId;
    private String userTel;
    private String userPswd;
    private String platform;
    private String deviceNo;

}
