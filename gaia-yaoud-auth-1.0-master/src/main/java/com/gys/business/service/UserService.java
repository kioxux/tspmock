package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.mapper.entity.UserResourceRecord;
import com.gys.business.service.data.UserExportInData;
import com.gys.business.service.data.UserInData;
import com.gys.business.service.data.UserLoginModelClient;
import com.gys.business.service.data.UserOutData;
import com.gys.common.data.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface UserService {
   PageInfo<UserOutData> getUserList(UserInData inData);

   PageInfo<UserOutData> getUserAllList(UserInData inData, GetLoginOutData userInfo);

   List<UserOutData> getUserListByClientId(GetLoginOutData inData);

   List<UserOutData> getUsersByClientId(GetLoginOutData inData, UserInData userInData);

   void exportUser(HttpServletResponse response, UserInData inData) throws Exception;

   JsonResult exportIn(List<UserExportInData> inData, GetLoginOutData client) throws Exception;

   UserOutData getUserById(UserInData inData);

   void insertUser(UserInData inData, GetLoginOutData userInfo);

   void deleteUser(UserInData inData);

   void updateUser(UserInData inData, GetLoginOutData userInfo);

   void addDeptUser(UserInData inData, GetLoginOutData userInfo);

   String checkResource(String userId, String path);

   List<UserOutData> selectPharmacistByBrId(UserInData inData);

   /**
    * 获取当前用户
    */
   UserLoginModelClient getCurrentUser();

   List<SelectModel> getDepAndStore(String type,GetLoginOutData userInfo);

    Boolean getCheckPath(Map inData);

    UserResourceRecord getResourceRecord(ResourceRecordForm resourceRecordForm);

   void updateResourceRecord(ResourceRecordForm resourceRecordForm);

   void autoOperate(List<String> pageIdList);

   void setDefaultStore(StoreForm storeForm);

   List<GaiaUserData> getPharmacist(GetLoginOutData loginUser);

   void updateNbybInfo(GaiaUserData loginUser);

}
