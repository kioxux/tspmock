package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.MemberService;
import com.gys.business.service.StoreService;
import com.gys.business.service.data.*;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.Util;
import com.gys.util.UtilConst;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

@Service
public class MemberServiceImpl implements MemberService {
   @Autowired
   private GaiaSdMemberBasicMapper memberBasicMapper;
   @Autowired
   private GaiaSdMemberClassMapper memberClassMapper;
   @Autowired
   private GaiaSdIntegralAddSetMapper integralAddSetMapper;
   @Autowired
   private GaiaSdIntegralExchangeSetMapper integralExchangeSetMapper;
   @Autowired
   private GaiaSdIntegralCashSetMapper integralCashSetMapper;
   @Autowired
   private GaiaStoreDataMapper storeDataMapper;
   @Autowired
   private GaiaProductBusinessMapper productBusinessMapper;
   @Autowired
   private GaiaSdStoreDataMapper sdStoreDataMapper;
   @Autowired
   private StoreService storeService;

   @Transactional
   public void typeSet(MemberInData inData) {
      new ArrayList();
      List<GaiaSdMemberBasic> memberBasicList;
      if (!"all".equals(inData.getGsiasBrId())) {
         GaiaSdMemberBasic memberBasic = new GaiaSdMemberBasic();
         memberBasic.setClientId(inData.getClientId());
         memberBasic.setGsmbBrId(inData.getGsiasBrId());
         memberBasicList = this.memberBasicMapper.select(memberBasic);
      } else {
         List<StoreOutData> list = this.storeService.selectAuthStoreList(inData.getClientId(), inData.getUserId());
         Example example = new Example(GaiaSdMemberBasic.class);
         example.createCriteria().andEqualTo("clientId", inData.getClientId()).andIn("gsmbBrId", list);
         memberBasicList = this.memberBasicMapper.selectByExample(example);
      }

      List<String> gsmbCardIds = new ArrayList();
      if (ObjectUtil.isNotEmpty(memberBasicList)) {
         memberBasicList.forEach((item) -> {
            gsmbCardIds.add(item.getGsmbCardId());
         });
      }

      inData.getMemberIds().forEach((item) -> {
         if (gsmbCardIds.indexOf(item) == -1) {
            throw new BusinessException(MessageFormat.format("提示：卡号{0}，不存在", item));
         }
      });
      this.memberBasicMapper.addCards(inData);
      Iterator var10 = inData.getMemberTypeList().iterator();

      while(var10.hasNext()) {
         MemberTypeInData memberTypeInData = (MemberTypeInData)var10.next();
         GaiaSdMemberClass memberClassIn = new GaiaSdMemberClass();
         memberClassIn.setClientId(inData.getClientId());
         memberClassIn.setGsmcId(memberTypeInData.getGsmcId());
         GaiaSdMemberClass memberClass = (GaiaSdMemberClass)this.memberClassMapper.selectOne(memberClassIn);
         if (ObjectUtil.isEmpty(memberClass)) {
            memberClassIn.setGsmcName(memberTypeInData.getGsmcName());
            memberClassIn.setGsmcDiscountRate(memberTypeInData.getGsmcDiscountRate());
            memberClassIn.setGsmcIntegralSale(memberTypeInData.getGsmcIntegralSale());
            memberClassIn.setGsmcAmtSale(memberTypeInData.getGsmcAmtSale());
            this.memberClassMapper.insert(memberClassIn);
         } else {
            memberClass.setGsmcAmtSale(memberTypeInData.getGsmcAmtSale());
            memberClass.setGsmcDiscountRate(memberTypeInData.getGsmcDiscountRate());
            memberClass.setGsmcIntegralSale(memberTypeInData.getGsmcIntegralSale());
            this.memberClassMapper.updateByPrimaryKeySelective(memberClass);
         }
      }

   }

   public List<MemberTypeOutData> getMemberClass(GetLoginOutData inData) {
      Example example = new Example(GaiaSdMemberClass.class);
      example.createCriteria().andEqualTo("clientId", inData.getClient());
      List<GaiaSdMemberClass> memberClasses = this.memberClassMapper.selectByExample(example);
      List<MemberTypeOutData> outData = new ArrayList();
      Iterator var5 = memberClasses.iterator();

      while(var5.hasNext()) {
         GaiaSdMemberClass memberClass = (GaiaSdMemberClass)var5.next();
         MemberTypeOutData memberTypeOutData = new MemberTypeOutData();
         memberTypeOutData.setClientId(memberClass.getClientId());
         memberTypeOutData.setGsmcAmtSale(memberClass.getGsmcAmtSale());
         memberTypeOutData.setGsmcDiscountRate(memberClass.getGsmcDiscountRate());
         memberTypeOutData.setGsmcDiscountRate2(memberClass.getGsmcDiscountRate2());
         memberTypeOutData.setGsmcId(memberClass.getGsmcId());
         memberTypeOutData.setGsmcIntegralSale(memberClass.getGsmcIntegralSale());
         memberTypeOutData.setGsmcName(memberClass.getGsmcName());
         memberTypeOutData.setGsmcUpgradeInte(memberClass.getGsmcUpgradeInte());
         memberTypeOutData.setGsmcUpgradeRech(memberClass.getGsmcUpgradeRech());
         memberTypeOutData.setGsmcUpgradeSet(memberClass.getGsmcUpgradeSet());
         memberTypeOutData.setGsmcCreDate(memberClass.getGsmcCreDate());
         memberTypeOutData.setGsmcCreEmp(memberClass.getGsmcCreEmp());
         memberTypeOutData.setGsmcUpdateDate(memberClass.getGsmcUpdateDate());
         memberTypeOutData.setGsmcUpdateEmp(memberClass.getGsmcUpdateEmp());
         outData.add(memberTypeOutData);
      }

      return outData;
   }

   @Transactional
   public boolean integralSet(IntegralInData inData) {
      if ("all".equals(inData.getGsiasBrId())) {
         StoreInData storeData = new StoreInData();
         storeData.setClient(inData.getClientId());
         List<StoreOutData> storeList = this.storeDataMapper.getStoreList(storeData);
         storeList.forEach((item) -> {
            inData.setGsiasBrId(item.getStoCode());
            this.integralSetStore(inData);
         });
      } else {
         this.integralSetStore(inData);
      }

      return true;
   }

   private void integralSetStore(IntegralInData inData) {
      Example example = new Example(GaiaSdIntegralAddSet.class);
      example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsiasBrId", inData.getGsiasBrId());
      GaiaSdIntegralAddSet integralAddSet = (GaiaSdIntegralAddSet)this.integralAddSetMapper.selectOneByExample(example);
      if (ObjectUtil.isNotEmpty(integralAddSet)) {
         integralAddSet.setGsiasAmtPm1(inData.getGsiasAmtPm1());
         integralAddSet.setGsiasAmtPm2(inData.getGsiasAmtPm2());
         integralAddSet.setGsiasAmtPm3(inData.getGsiasAmtPm3());
         integralAddSet.setGsiasAmtPm4(inData.getGsiasAmtPm4());
         integralAddSet.setGsiasAmtPm5(inData.getGsiasAmtPm5());
         integralAddSet.setGsiasAmtPm6(inData.getGsiasAmtPm6());
         integralAddSet.setGsiasIntegralPm1(inData.getGsiasIntegralPm1());
         integralAddSet.setGsiasIntegralPm2(inData.getGsiasIntegralPm2());
         integralAddSet.setGsiasIntegralPm3(inData.getGsiasIntegralPm3());
         integralAddSet.setGsiasIntegralPm4(inData.getGsiasIntegralPm4());
         integralAddSet.setGsiasIntegralPm5(inData.getGsiasIntegralPm5());
         integralAddSet.setGsiasIntegralPm6(inData.getGsiasIntegralPm6());
         integralAddSet.setGsiasChangeSet(inData.getGsiasChangeSet());
         integralAddSet.setGsiasIntegralMinAmt(inData.getGsiasIntegralMinAmt());
         integralAddSet.setGsiasIntegralMax(inData.getGsiasIntegralMax());
         this.integralAddSetMapper.updateByPrimaryKeySelective(integralAddSet);
      } else {
         GaiaStoreData storeData = new GaiaStoreData();
         storeData.setClient(inData.getClientId());
         storeData.setStoCode(inData.getGsiasBrId());
         storeData = (GaiaStoreData)this.storeDataMapper.selectByPrimaryKey(storeData);
         integralAddSet = new GaiaSdIntegralAddSet();
         BeanUtils.copyProperties(inData, integralAddSet);
         integralAddSet.setGsiasBrName(storeData.getStoName());
         this.integralAddSetMapper.insert(integralAddSet);
      }

   }

   public IntegralOutData getIntegralSet(IntegralInData inData) {
      Example example = new Example(GaiaSdIntegralAddSet.class);
      example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsiasBrId", inData.getGsiasBrId());
      GaiaSdIntegralAddSet integralAddSet = (GaiaSdIntegralAddSet)this.integralAddSetMapper.selectOneByExample(example);
      IntegralOutData outData = null;
      if (ObjectUtil.isNotEmpty(integralAddSet)) {
         outData = new IntegralOutData();
         BeanUtils.copyProperties(integralAddSet, outData);
      }

      return outData;
   }

   public PageInfo<ExchangeOutData> exchangeSetList(ExchangeInData inData) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<ExchangeOutData> outData = this.integralExchangeSetMapper.exchangeSetList(inData);
      Iterator var4 = outData.iterator();

      while(var4.hasNext()) {
         ExchangeOutData data = (ExchangeOutData)var4.next();
         if ("0".equals(data.getGsiesMemberClass())) {
            data.setGsiesMemberClassName("全部");
         }
      }

      PageInfo pageInfo;
      if (ObjectUtil.isNotEmpty(outData)) {
         pageInfo = new PageInfo(outData);
      } else {
         pageInfo = new PageInfo();
      }

      return pageInfo;
   }

   public ExchangeOutData getExchangeSet(ExchangeInData inData) {
      Example example = new Example(GaiaSdIntegralExchangeSet.class);
      example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsiesVoucherId", inData.getGsiesVoucherId());
      GaiaSdIntegralExchangeSet integralExchangeSet = (GaiaSdIntegralExchangeSet)this.integralExchangeSetMapper.selectOneByExample(example);
      ExchangeOutData exchangeOutData = new ExchangeOutData();
      exchangeOutData.setGsiesVoucherId(integralExchangeSet.getGsiesVoucherId());
      exchangeOutData.setGsiesBrId(integralExchangeSet.getGsiesBrId());
      exchangeOutData.setGsiesBrName(integralExchangeSet.getGsiesBrName());
      exchangeOutData.setGsiesDateEnd(DateUtil.format(DateUtil.parse(integralExchangeSet.getGsiesDateEnd(), "yyyyMMdd"), "yyyy-MM-dd"));
      exchangeOutData.setGsiesDateStart(DateUtil.format(DateUtil.parse(integralExchangeSet.getGsiesDateStart(), "yyyyMMdd"), "yyyy-MM-dd"));
      exchangeOutData.setGsiesExchangeAmt(integralExchangeSet.getGsiesExchangeAmt());
      exchangeOutData.setGsiesExchangeProId(integralExchangeSet.getGsiesExchangeProId());
      exchangeOutData.setGsiesExchangeQty(integralExchangeSet.getGsiesExchangeQty());
      exchangeOutData.setGsiesExchangeRepeat(integralExchangeSet.getGsiesExchangeRepeat());
      exchangeOutData.setGsiesIntegralAddSet(integralExchangeSet.getGsiesIntegralAddSet());
      exchangeOutData.setGsieslExchangeIntegra(integralExchangeSet.getGsieslExchangeIntegra());
      exchangeOutData.setGsiesMemberClass(integralExchangeSet.getGsiesMemberClass());
      exchangeOutData.setGsiesStoreGroup(integralExchangeSet.getGsiesStoreGroup());
      exchangeOutData.setGsiesStatus(integralExchangeSet.getGsiesStatus());
      return exchangeOutData;
   }

   @Transactional
   public boolean approveExchangeSet(ExchangeInData inData) {
      Example example = new Example(GaiaSdIntegralExchangeSet.class);
      Example.Criteria clientId = example.createCriteria().andEqualTo("clientId", inData.getClientId());
      if(CollUtil.isNotEmpty(inData.getIdList())){
         clientId.andIn("gsiesVoucherId", inData.getIdList());
      }
      List<GaiaSdIntegralExchangeSet> list = this.integralExchangeSetMapper.selectByExample(example);
      Iterator var4 = list.iterator();

      GaiaSdIntegralExchangeSet data;
      do {
         if (!var4.hasNext()) {
            GaiaSdIntegralExchangeSet integralExchangeSet = new GaiaSdIntegralExchangeSet();
            integralExchangeSet.setGsiesStatus(inData.getGsiesStatus());
            this.integralExchangeSetMapper.updateByExampleSelective(integralExchangeSet, example);
            return true;
         }

         data = (GaiaSdIntegralExchangeSet)var4.next();
      } while("0".equals(data.getGsiesStatus()));

      throw new BusinessException("存在已审核数据");
   }

   @Transactional
   public boolean updateExchangeSet(ExchangeInData inData) {
      Example example = new Example(GaiaSdIntegralExchangeSet.class);
      example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsiesVoucherId", inData.getGsiesVoucherId());
      GaiaSdIntegralExchangeSet integralExchangeSet = (GaiaSdIntegralExchangeSet)this.integralExchangeSetMapper.selectOneByExample(example);
      integralExchangeSet.setGsiesBrId(inData.getGsiesBrId());
      integralExchangeSet.setGsiesBrName(inData.getGsiesBrName());
      integralExchangeSet.setGsiesDateEnd(DateUtil.format(DateUtil.parseDate(inData.getGsiesDateEnd()), "yyyyMMdd"));
      integralExchangeSet.setGsiesDateStart(DateUtil.format(DateUtil.parseDate(inData.getGsiesDateStart()), "yyyyMMdd"));
      integralExchangeSet.setGsiesExchangeAmt(inData.getGsiesExchangeAmt());
      integralExchangeSet.setGsiesExchangeProId(inData.getGsiesExchangeProId());
      integralExchangeSet.setGsiesExchangeQty(inData.getGsiesExchangeQty());
      integralExchangeSet.setGsiesExchangeRepeat(inData.getGsiesExchangeRepeat());
      integralExchangeSet.setGsiesIntegralAddSet(inData.getGsiesIntegralAddSet());
      integralExchangeSet.setGsieslExchangeIntegra(inData.getGsieslExchangeIntegra());
      integralExchangeSet.setGsiesMemberClass(inData.getGsiesMemberClass());
      integralExchangeSet.setGsiesStoreGroup(inData.getGsiesStoreGroup());
      integralExchangeSet.setGsiesStatus(inData.getGsiesStatus());
      this.integralExchangeSetMapper.updateByPrimaryKeySelective(integralExchangeSet);
      return true;
   }

   @Transactional
   public boolean addExchangeSet(ExchangeInData inData) {
      Example example = new Example(GaiaProductBusiness.class);
      example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSite", inData.getGsiesBrId()).andEqualTo("proSelfCode", inData.getGsiesExchangeProId());
      List<GaiaProductBusiness> list = this.productBusinessMapper.selectByExample(example);
      if (ObjectUtil.isEmpty(list)) {
         throw new BusinessException("此兑换商品编码不存在！");
      } else {
         String codePre = DateUtil.format(new Date(), "yyMMdd");
         String code = this.integralExchangeSetMapper.selectMaxId(inData.getClientId(), codePre);
         GaiaSdIntegralExchangeSet integralExchangeSet = new GaiaSdIntegralExchangeSet();
         integralExchangeSet.setGsiesVoucherId(code);
         integralExchangeSet.setClientId(inData.getClientId());
         integralExchangeSet.setGsiesBrId(inData.getGsiesBrId());
         integralExchangeSet.setGsiesBrName(inData.getGsiesBrName());
         integralExchangeSet.setGsiesStatus("0");
         integralExchangeSet.setGsiesDateEnd(DateUtil.format(DateUtil.parseDate(inData.getGsiesDateEnd()), "yyyyMMdd"));
         integralExchangeSet.setGsiesDateStart(DateUtil.format(DateUtil.parseDate(inData.getGsiesDateStart()), "yyyyMMdd"));
         integralExchangeSet.setGsiesExchangeAmt(inData.getGsiesExchangeAmt());
         integralExchangeSet.setGsiesExchangeProId(inData.getGsiesExchangeProId());
         integralExchangeSet.setGsiesExchangeQty(inData.getGsiesExchangeQty());
         integralExchangeSet.setGsiesExchangeRepeat(inData.getGsiesExchangeRepeat());
         integralExchangeSet.setGsiesIntegralAddSet(inData.getGsiesIntegralAddSet());
         integralExchangeSet.setGsieslExchangeIntegra(inData.getGsieslExchangeIntegra());
         integralExchangeSet.setGsiesMemberClass(inData.getGsiesMemberClass());
         integralExchangeSet.setGsiesStoreGroup(inData.getGsiesStoreGroup());
         this.integralExchangeSetMapper.insert(integralExchangeSet);
         return true;
      }
   }

   public PageInfo<CashOutData> cashSetList(CashInData inData) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<CashOutData> outData = this.integralCashSetMapper.cashSetList(inData);
      Iterator var4 = outData.iterator();

      while(var4.hasNext()) {
         CashOutData data = (CashOutData)var4.next();
         if ("0".equals(data.getGsicsMemberClass())) {
            data.setGsicsMemberClassName("全部");
         }
      }

      PageInfo pageInfo;
      if (ObjectUtil.isNotEmpty(outData)) {
         pageInfo = new PageInfo(outData);
      } else {
         pageInfo = new PageInfo();
      }

      return pageInfo;
   }

   @Transactional
   public boolean updateCashSet(CashInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaSdIntegralCashSet.class);
      example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsicsVoucherId", inData.getGsicsVoucherId());
      GaiaSdIntegralCashSet integralCashSet = (GaiaSdIntegralCashSet)this.integralCashSetMapper.selectOneByExample(example);
      integralCashSet.setGsicsDateEnd(DateUtil.format(DateUtil.parseDate(inData.getGsicsDateEnd()), "yyyyMMdd"));
      integralCashSet.setGsicsDateStart(DateUtil.format(DateUtil.parseDate(inData.getGsicsDateStart()), "yyyyMMdd"));
      integralCashSet.setGsicsAmtOffset(inData.getGsicsAmtOffset());
      integralCashSet.setGsicsAmtOffsetMax(inData.getGsicsAmtOffsetMax());
      integralCashSet.setGsicsChangeSet(inData.getGsicsChangeSet());
      integralCashSet.setGsicsDescription(inData.getGsicsDescription());
      integralCashSet.setGsicsIntegralAddSet(inData.getGsicsIntegralAddSet());
      integralCashSet.setGsicsEmp(userInfo.getUserId());
      integralCashSet.setGsicsMemberClass(inData.getGsicsMemberClass());
      integralCashSet.setGsicsIntegralDeduct(inData.getGsicsIntegralDeduct());
      this.integralCashSetMapper.updateByPrimaryKeySelective(integralCashSet);
      return true;
   }

   @Transactional
   public boolean addCashSet(CashInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaSdIntegralCashSet.class);
      example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsicsBrId", inData.getGsicsBrId());
      List<GaiaSdIntegralCashSet> integralCashSetList = this.integralCashSetMapper.selectByExample(example);
      if (ObjectUtil.isNotEmpty(integralCashSetList)) {
         throw new BusinessException("该门店已有积分抵现设置！");
      } else {
         String maxId = this.integralCashSetMapper.selectMaxId(userInfo.getClient());
         GaiaSdIntegralCashSet integralCashSet = new GaiaSdIntegralCashSet();
         integralCashSet.setGsicsVoucherId(maxId);
         integralCashSet.setClientId(userInfo.getClient());
         integralCashSet.setGsicsBrId(inData.getGsicsBrId());
         integralCashSet.setGsicsBrName(inData.getGsicsBrName());
         integralCashSet.setGsicsDateEnd(DateUtil.format(DateUtil.parseDate(inData.getGsicsDateEnd()), "yyyyMMdd"));
         integralCashSet.setGsicsDateStart(DateUtil.format(DateUtil.parseDate(inData.getGsicsDateStart()), "yyyyMMdd"));
         integralCashSet.setGsicsAmtOffset(inData.getGsicsAmtOffset());
         integralCashSet.setGsicsAmtOffsetMax(inData.getGsicsAmtOffsetMax());
         integralCashSet.setGsicsChangeSet(inData.getGsicsChangeSet());
         integralCashSet.setGsicsDescription(inData.getGsicsDescription());
         integralCashSet.setGsicsIntegralAddSet(inData.getGsicsIntegralAddSet());
         integralCashSet.setGsicsEmp(userInfo.getUserId());
         integralCashSet.setGsicsMemberClass(inData.getGsicsMemberClass());
         integralCashSet.setGsicsIntegralDeduct(inData.getGsicsIntegralDeduct());
         this.integralCashSetMapper.insert(integralCashSet);
         return true;
      }
   }

   public void exportOut(HttpServletResponse response, ExchangeInData inData) throws Exception {
      List<ExchangeOutData> outData = this.integralExchangeSetMapper.exchangeSetList(inData);
      List<ExchangeExportOutData> list = new ArrayList();
      Iterator var5 = outData.iterator();

      while(var5.hasNext()) {
         ExchangeOutData out = (ExchangeOutData)var5.next();
         ExchangeExportOutData exportOutData = new ExchangeExportOutData();
         exportOutData.setGsiesBrId(out.getGsiesBrId());
         exportOutData.setGsiesBrName(out.getGsiesBrName());
         exportOutData.setGsiesDateEnd(DateUtil.format(DateUtil.parseDate(out.getGsiesDateEnd()), "yyyyMMdd"));
         exportOutData.setGsiesDateStart(DateUtil.format(DateUtil.parseDate(out.getGsiesDateStart()), "yyyyMMdd"));
         exportOutData.setGsiesExchangeAmt(out.getGsiesExchangeAmt());
         exportOutData.setGsiesExchangeProId(out.getGsiesExchangeProId());
         exportOutData.setGsiesExchangeQty(out.getGsiesExchangeQty());
         exportOutData.setGsiesExchangeRepeat(out.getGsiesExchangeRepeat());
         exportOutData.setGsiesIntegralAddSet(out.getGsiesIntegralAddSet());
         exportOutData.setGsieslExchangeIntegra(out.getGsieslExchangeIntegra());
         exportOutData.setGsiesMemberClass(out.getGsiesMemberClass());
         exportOutData.setGsiesStoreGroup(out.getGsiesStoreGroup());
         list.add(exportOutData);
      }

      String excelName = "积分兑换设置";
      String[] headers = new String[]{"店号", "店名", "门店类别", "会员卡类型", "开始日期", "结束日期", "兑换商品编码", "兑换积分", "换购金额", "兑换数量", "是否允许重复兑换", "当笔交易是否积分"};
      ExportExcel<ExchangeExportOutData> excelUtil = new ExportExcel();
      response.setContentType("application/vnd.ms-excel; charset=utf-8");
      response.setHeader("Content-disposition", "attachment;filename=" + new String(excelName.getBytes(), "iso-8859-1") + ".xls");
      response.setCharacterEncoding("UTF-8");
      OutputStream outputStream = response.getOutputStream();
      excelUtil.exportExcel(headers, list, outputStream, (String)null);
   }

   @Transactional
   public JsonResult exportIn(List<ExchangeExportInData> inData, String client) throws Exception {
      if (ObjectUtil.isEmpty(inData)) {
         return JsonResult.fail(UtilConst.CODE_1001, "需要导入的积分兑换为空");
      } else {
         Iterator var3 = inData.iterator();

         ExchangeExportInData exportInData;
         while(var3.hasNext()) {
            exportInData = (ExchangeExportInData)var3.next();
            if (ObjectUtil.isEmpty(exportInData.getGsiesBrId())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的店号不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesBrName())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的店名不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesDateEnd())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的结束日期不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesDateStart())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的开始日期不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesExchangeAmt())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的兑换金额不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesExchangeProId())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的兑换商品编码不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesExchangeQty())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的兑换数量不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesExchangeRepeat())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的是否允许重复兑换不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesIntegralAddSet())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的当笔交易是否积分不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsieslExchangeIntegra())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的兑换积分不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getGsiesMemberClass())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的会员卡类型不能为空");
            }
         }

         var3 = inData.iterator();

         while(var3.hasNext()) {
            exportInData = (ExchangeExportInData)var3.next();
            String codePre = DateUtil.format(new Date(), "yyMMdd");
            String code = this.integralExchangeSetMapper.selectMaxId(client, codePre);
            GaiaSdIntegralExchangeSet integralExchangeSet = new GaiaSdIntegralExchangeSet();
            integralExchangeSet.setGsiesVoucherId(code);
            integralExchangeSet.setClientId(client);
            integralExchangeSet.setGsiesBrId(exportInData.getGsiesBrId());
            integralExchangeSet.setGsiesBrName(exportInData.getGsiesBrName());
            integralExchangeSet.setGsiesStatus("0");
            integralExchangeSet.setGsiesDateEnd(exportInData.getGsiesDateEnd());
            integralExchangeSet.setGsiesDateStart(exportInData.getGsiesDateStart());
            integralExchangeSet.setGsiesExchangeAmt(new BigDecimal(exportInData.getGsiesExchangeAmt()));
            integralExchangeSet.setGsiesExchangeProId(exportInData.getGsiesExchangeProId());
            integralExchangeSet.setGsiesExchangeQty(exportInData.getGsiesExchangeQty());
            integralExchangeSet.setGsiesExchangeRepeat(exportInData.getGsiesExchangeRepeat());
            integralExchangeSet.setGsiesIntegralAddSet(exportInData.getGsiesIntegralAddSet());
            integralExchangeSet.setGsieslExchangeIntegra(exportInData.getGsieslExchangeIntegra());
            integralExchangeSet.setGsiesMemberClass(exportInData.getGsiesMemberClass());
            integralExchangeSet.setGsiesStoreGroup(exportInData.getGsiesStoreGroup());
            this.integralExchangeSetMapper.insert(integralExchangeSet);
         }

         return JsonResult.success((Object)null, "提示：上传成功！");
      }
   }

   public String getVirtualMaxCard(GetLoginOutData userInfo) {
      return this.memberBasicMapper.getVirtualMaxCard(userInfo.getClient());
   }

   public GetMemberPreSetOutData getVirtualStoreSet(GetMemberPreSetInData inData, GetLoginOutData userInfo) {
      GetMemberPreSetOutData outData = new GetMemberPreSetOutData();
      GaiaSdStoreData record = new GaiaSdStoreData();
      record.setClientId(userInfo.getClient());
      record.setGsstBrId(inData.getBrId());
      record = (GaiaSdStoreData)this.sdStoreDataMapper.selectByPrimaryKey(record);
      if (ObjectUtil.isNotNull(record)) {
         outData.setMemNewMix(record.getGsstMemNewMix());
         outData.setMenNewAuto(record.getGsstMenNewAuto());
      }

      return outData;
   }

   @Transactional
   public void saveMemberPre(GetMemberPreSetInData inData, GetLoginOutData userInfo) {
      String startCard;
      if ("1".equals(inData.getType())) {
         Iterator var3 = inData.getBrIds().iterator();

         while(var3.hasNext()) {
            startCard = (String)var3.next();
            GaiaSdStoreData record = new GaiaSdStoreData();
            record.setClientId(userInfo.getClient());
            record.setGsstBrId(startCard);
            record = (GaiaSdStoreData)this.sdStoreDataMapper.selectByPrimaryKey(record);
            if (ObjectUtil.isNotNull(record)) {
               record.setGsstMemNewMix(inData.getMemNewMix());
               record.setGsstMenNewAuto(inData.getMenNewAuto());
               this.sdStoreDataMapper.updateByPrimaryKey(record);
            } else {
               record = new GaiaSdStoreData();
               record.setClientId(userInfo.getClient());
               record.setGsstBrId(startCard);
               record.setGsstMemNewMix(inData.getMemNewMix());
               record.setGsstMenNewAuto(inData.getMenNewAuto());
               this.sdStoreDataMapper.insert(record);
            }
         }
      }

      if ("2".equals(inData.getType())) {
         String preCard = inData.getStartCardNo().substring(0, inData.getStartCardNo().length() - 4);
         startCard = inData.getStartCardNo().substring(inData.getStartCardNo().length() - 4);
         String endCard = inData.getEndCardNo().substring(inData.getEndCardNo().length() - 4);
         boolean flag = true;

         while(flag) {
            GaiaSdMemberBasic member = new GaiaSdMemberBasic();
            member.setClientId(userInfo.getClient());
            member.setGsmbCardId(preCard + startCard);
            member = (GaiaSdMemberBasic)this.memberBasicMapper.selectByPrimaryKey(member);
            if (ObjectUtil.isNotNull(member)) {
               if (startCard.equals(endCard)) {
                  flag = false;
               } else {
                  startCard = Util.strAddOne(startCard);
               }
            } else {
               member = new GaiaSdMemberBasic();
               member.setClientId(userInfo.getClient());
               member.setGsmbCardId(preCard + startCard);
               member.setGsmbBrId(inData.getBrId());
               member.setGsmbClassId("5");
               member.setGsmbStatus("1");
               member.setGsmbCreatDate(DateUtil.format(new Date(), "yyyyMMdd"));
               member.setGsmbType("2");
               member.setGsmbChannel("线下");
               this.memberBasicMapper.insert(member);
               if (startCard.equals(endCard)) {
                  flag = false;
               } else {
                  startCard = Util.strAddOne(startCard);
               }
            }
         }
      }

   }

   @Transactional
   public void importMember(List<GetMemberPreSetImportInData> inData, GetLoginOutData userInfo) {
      if (CollUtil.isEmpty(inData)) {
         throw new BusinessException("没有数据");
      } else {
         List<StoreOutData> storeList = this.storeService.selectAuthStoreList(userInfo.getClient(), userInfo.getUserId());
         List<String> storeCodeList = new ArrayList();
         Iterator var5 = storeList.iterator();

         while(var5.hasNext()) {
            StoreOutData store = (StoreOutData)var5.next();
            storeCodeList.add(store.getStoCode());
         }

         var5 = inData.iterator();

         GaiaSdMemberBasic member;
         GetMemberPreSetImportInData data;
         while(var5.hasNext()) {
            data = (GetMemberPreSetImportInData)var5.next();
            if (StrUtil.isBlank(data.getBrId())) {
               throw new BusinessException(StrUtil.format("第{}行门店编号不能为空", new Object[]{data.getIndex()}));
            }

            if (!storeCodeList.contains(data.getBrId())) {
               throw new BusinessException(StrUtil.format("第{}行无该门店权限", new Object[]{data.getIndex()}));
            }

            if (StrUtil.isBlank(data.getCardNo())) {
               throw new BusinessException(StrUtil.format("第{}行卡号不能为空", new Object[]{data.getIndex()}));
            }

            member = new GaiaSdMemberBasic();
            member.setClientId(userInfo.getClient());
            member.setGsmbCardId(data.getCardNo());
            member = (GaiaSdMemberBasic)this.memberBasicMapper.selectByPrimaryKey(member);
            if (ObjectUtil.isNotNull(member)) {
               throw new BusinessException(StrUtil.format("第{}行卡号已存在", new Object[]{data.getIndex()}));
            }
         }

         var5 = inData.iterator();

         while(var5.hasNext()) {
            data = (GetMemberPreSetImportInData)var5.next();
            member = new GaiaSdMemberBasic();
            member.setClientId(userInfo.getClient());
            member.setGsmbCardId(data.getCardNo());
            member.setGsmbClassId("5");
            member.setGsmbBrId(data.getBrId());
            member.setGsmbStatus("1");
            member.setGsmbChannel("线下");
            member.setGsmbCreatDate(DateUtil.format(new Date(), "yyyyMMdd"));
            member.setGsmbType("2");
            this.memberBasicMapper.insert(member);
         }

      }
   }

   @Transactional
   public void importMemberSet(List<GetMemberSetImportInData> inData, GetLoginOutData userInfo) {
      if (CollUtil.isEmpty(inData)) {
         throw new BusinessException("没有有效数据");
      } else {
         List<StoreOutData> storeList = this.storeService.selectAuthStoreList(userInfo.getClient(), userInfo.getUserId());
         List<String> storeCodeList = new ArrayList();
         Iterator var5 = storeList.iterator();

         while(var5.hasNext()) {
            StoreOutData store = (StoreOutData)var5.next();
            storeCodeList.add(store.getStoCode());
         }

         GaiaSdMemberBasic member;
         GetMemberSetImportInData data;
         for(int i = 0; i < inData.size(); ++i) {
            data = (GetMemberSetImportInData)inData.get(i);
            if (StrUtil.isBlank(data.getBrId())) {
               throw new BusinessException(StrUtil.format("第{}行门店编号不能为空", new Object[]{i + 1}));
            }

            if (!storeCodeList.contains(data.getBrId())) {
               throw new BusinessException(StrUtil.format("第{}行无该门店权限", new Object[]{i + 1}));
            }

            if (StrUtil.isBlank(data.getCardNo())) {
               throw new BusinessException(StrUtil.format("第{}行卡号不能为空", new Object[]{i + 1}));
            }

            member = new GaiaSdMemberBasic();
            member.setClientId(userInfo.getClient());
            member.setGsmbCardId(data.getCardNo());
            member = (GaiaSdMemberBasic)this.memberBasicMapper.selectByPrimaryKey(member);
            if (ObjectUtil.isEmpty(member)) {
               throw new BusinessException(StrUtil.format("第{}行卡号不存在", new Object[]{i + 1}));
            }

            if (StrUtil.isBlank(data.getType())) {
               throw new BusinessException(StrUtil.format("第{}行卡类型不能为空", new Object[]{i + 1}));
            }

            if (!Arrays.asList("1", "2", "3", "4", "5").contains(data.getType())) {
               throw new BusinessException(StrUtil.format("第{}行卡类型值非法", new Object[]{i + 1}));
            }
         }

         var5 = inData.iterator();

         while(var5.hasNext()) {
            data = (GetMemberSetImportInData)var5.next();
            member = new GaiaSdMemberBasic();
            member.setClientId(userInfo.getClient());
            member.setGsmbCardId(data.getCardNo());
            member = (GaiaSdMemberBasic)this.memberBasicMapper.selectByPrimaryKey(member);
            member.setGsmbClassId(data.getType());
            this.memberBasicMapper.updateByPrimaryKeySelective(member);
         }

      }
   }

   public PageInfo<GetMemberOutData> selectNewCardList(GetMemberPreSetInData inData, GetLoginOutData userInfo) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      inData.setClientId(userInfo.getClient());
      List<GetMemberOutData> outData = this.memberBasicMapper.selectNewCardList(inData);
      PageInfo pageInfo;
      if (ObjectUtil.isNotEmpty(outData)) {
         pageInfo = new PageInfo(outData);
      } else {
         pageInfo = new PageInfo();
      }

      return pageInfo;
   }

   public void autoCreateVirtualCard() {
      List<GaiaSdStoreData> sdStoreDataList = this.sdStoreDataMapper.selectAll();
      if (!CollUtil.isEmpty(sdStoreDataList)) {
         Iterator var2 = sdStoreDataList.iterator();

         while(var2.hasNext()) {
            GaiaSdStoreData sdStoreData = (GaiaSdStoreData)var2.next();

            try {
               if (!StrUtil.isBlank(sdStoreData.getGsstMemNewMix()) && !StrUtil.isBlank(sdStoreData.getGsstMenNewAuto())) {
                  Example example = new Example(GaiaSdMemberBasic.class);
                  example.createCriteria().andEqualTo("clientId", sdStoreData.getClientId()).andEqualTo("gsmbBrId", sdStoreData.getGsstBrId()).andEqualTo("gsmbType", "1").andEqualTo("gsmbStatus", "1");
                  int mun = this.memberBasicMapper.selectCountByExample(example);
                  if (mun < Integer.valueOf(sdStoreData.getGsstMemNewMix())) {
                     String currentCard = this.memberBasicMapper.getVirtualMaxCard(sdStoreData.getClientId());
                     int card = Integer.valueOf(currentCard);
                     int index = 1;

                     while(index <= Integer.valueOf(sdStoreData.getGsstMenNewAuto())) {
                        ++card;
                        GaiaSdMemberBasic member = new GaiaSdMemberBasic();
                        member.setClientId(sdStoreData.getClientId());
                        member.setGsmbCardId(card + "");
                        member = (GaiaSdMemberBasic)this.memberBasicMapper.selectByPrimaryKey(member);
                        if (!ObjectUtil.isNotNull(member)) {
                           member = new GaiaSdMemberBasic();
                           member.setClientId(sdStoreData.getClientId());
                           member.setGsmbCardId(card + "");
                           member.setGsmbBrId(sdStoreData.getGsstBrId());
                           member.setGsmbClassId("5");
                           member.setGsmbStatus("1");
                           member.setGsmbCreatDate(DateUtil.format(new Date(), "yyyyMMdd"));
                           member.setGsmbType("1");
                           member.setGsmbChannel("线下");
                           this.memberBasicMapper.insert(member);
                           ++index;
                        }
                     }
                  }
               }
            } catch (Exception var10) {
               ;
            }
         }

      }
   }

   @Override
   public void export(HttpServletResponse response, CashInData inData) throws Exception {
      List<CashOutData> outData = this.integralCashSetMapper.cashSetList(inData);
      List<ExportOutData> list = new ArrayList();
      Iterator var5 = outData.iterator();

      while(var5.hasNext()) {
         CashOutData out = (CashOutData)var5.next();
         ExportOutData exportOutData = new ExportOutData();
         exportOutData.setGsicsVoucherId(out.getGsicsVoucherId());
         exportOutData.setGsicsBrId(out.getGsicsBrId());
         exportOutData.setGsicsBrName(out.getGsicsBrName());
         exportOutData.setGsicsMemberClass(out.getGsicsMemberClass());
         exportOutData.setGsicsDateStart(DateUtil.format(DateUtil.parseDate(out.getGsicsDateStart()), "yyyyMMdd"));
         exportOutData.setGsicsDateEnd(DateUtil.format(DateUtil.parseDate(out.getGsicsDateEnd()), "yyyyMMdd"));
         //扣除积分
         exportOutData.setGsicsIntegralDeduct(out.getGsicsIntegralDeduct());
         //扣除金额
         exportOutData.setGsicsAmtOffset(out.getGsicsAmtOffset());
         exportOutData.setGsicsAmtOffsetMax(out.getGsicsAmtOffsetMax());
         exportOutData.setGsicsDescription(out.getGsicsDescription());
         //当前交易是否积分
         exportOutData.setGsicsIntegralAddSet("1".equals(out.getGsicsIntegralAddSet()) ?"是":"否");
         //是否允许支付小数
         exportOutData.setGsicsChangeSet("1".equals(out.getGsicsChangeSet()) ?"是":"否");
         exportOutData.setGsicsEmp(out.getGsicsEmp());
         list.add(exportOutData);
      }

      String excelName = "积分抵现设置";
      String[] headers = new String[]{"单号", "店号", "店名", "会员卡类型", "开始日期", "结束日期", "扣除积分", "扣除金额", "最大抵现金金额", "内容说明", "当前交易是否积分", "是否允许支付小数","修改人员"};
      ExportExcel<ExportOutData> excelUtil = new ExportExcel();
      response.setContentType("application/vnd.ms-excel; charset=utf-8");
      response.setHeader("Content-disposition", "attachment;filename=" + new String(excelName.getBytes(), "iso-8859-1") + ".xls");
      response.setCharacterEncoding("UTF-8");
      OutputStream outputStream = response.getOutputStream();
      excelUtil.exportExcel(headers, list, outputStream, (String)null);
   }
}
