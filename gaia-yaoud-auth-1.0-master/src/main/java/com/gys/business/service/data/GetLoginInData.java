package com.gys.business.service.data;

import lombok.Data;

import java.beans.ConstructorProperties;
import java.io.Serializable;
@Data
public class GetLoginInData implements Serializable {
   private static final long serialVersionUID = 1L;
   private String username;
   private String loginAccount;
   private String phone;
   private String password;
   private String passwordOld;
   private String passwordNew;
   private String code;
   private String client;
   private String userId;
   private String stoCode;
}
