package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.gys.business.controller.SupplierPaymentWfData;
import com.gys.business.feign.*;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.WorkflowService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.YunGeData.ReturnWorkflowDTO;
import com.gys.business.service.data.YunGeData.WfCkspbsby;
import com.gys.business.service.data.YunGeData.WfSpbssq;
import com.gys.business.service.data.YunGeData.WfZzysq;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.enums.WorkflowTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.Util;
import com.gys.util.UtilMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WorkflowServiceImpl implements WorkflowService {
   @Resource
   private GaiaWfDefineMapper wfDefineMapper;

   @Resource
   private GaiaWfDefineProcessMapper wfDefineProcessMapper;

   @Resource
   private GaiaWfRecordMapper wfRecordMapper;

   @Resource
   private GaiaWfRecordHistoryMapper wfRecordHistoryMapper;

   @Resource
   private GaiaUserDataMapper userDataMapper;

   @Resource
   private OperationService operationService;

   @Resource
   private PurchaseService purchaseService;

   @Resource
   private WmsService wmsService;

   @Resource
   private StoreWebService storeWebService;

   @Resource
   private StoreAppService storeAppService;

   @Resource
   private GaiaStoreDataMapper storeDataMapper;

   @Resource
   private OperateService operateService;
   @Resource
   private GaiaSdIncomeStatementHMapper gaiaSdIncomeStatementHMapper;

   @Resource
   private GaiaSdMessageMapper gaiaSdMessageMapper;

   @Value("${cos.url}")
   private String url;

   @Transactional(rollbackFor = Exception.class)
   @Override
   public GaiaWfRecord createWorkflow(GetWfCreateInData wfCreateInData, GetLoginOutData loginOutData) {
      log.info("创建工作流参数: {}",  JSONObject.toJSONString(wfCreateInData));
      GaiaWfRecord wfRecord = new GaiaWfRecord();
      if (ObjectUtil.isNull(loginOutData)) {
         throw new BusinessException("提示：账号登录已超时，请重新登录后再试");
      } else {
         String client = loginOutData.getClient();
         if (ObjectUtil.isNull(wfCreateInData)) {
            throw new BusinessException("提示：发起工作流参数不正确");
         } else {
            String defineCode = wfCreateInData.getWfDefineCode();
            if (StrUtil.isBlank(defineCode)) {
               throw new BusinessException("提示：工作流类型不能为空");
            } else if (StrUtil.isBlank(wfCreateInData.getWfTitle())) {
               throw new BusinessException("提示：工作流标题不能为空");
            } else if (StrUtil.isBlank(wfCreateInData.getWfDescription())) {
               throw new BusinessException("提示：工作流描述不能为空");
            } else if (StrUtil.isBlank(wfCreateInData.getWfOrder())) {
               throw new BusinessException("提示：工作流单据编号不能为空");
            } else if (StringUtils.equalsIgnoreCase("GAIA_WF_029", defineCode) && StrUtil.isBlank(wfCreateInData.getWfSite())) {
               throw new BusinessException("提示：工作流SITE不能为空");
            } else {
               Example example = new Example(GaiaWfDefine.class);
               example.createCriteria().andEqualTo("client", client).andEqualTo("wfDefineCode", defineCode);
               GaiaWfDefine define = this.wfDefineMapper.selectOneByExample(example);
               if (ObjectUtil.isNull(define)) {
                  throw new BusinessException(UtilMessage.WF_TYPE_NOT_EXITS);
               } else {
                  List<GetWfDefineProcessOutData> processList = this.wfDefineProcessMapper.selectApproveUser(client, defineCode, wfCreateInData.getWfSite());
                  Iterator var8 = processList.iterator();

                  while(var8.hasNext()) {
                     GetWfDefineProcessOutData process = (GetWfDefineProcessOutData)var8.next();
                     if (CollUtil.isEmpty(process.getApproveUserOutData())) {
                        throw new BusinessException("提示：节点未分配审批人！");
                     }
                  }

                  example = new Example(GaiaWfRecord.class);
                  example.createCriteria().andEqualTo("client", client).andEqualTo("wfDefineCode", defineCode).andEqualTo("wfOrder", wfCreateInData.getWfOrder()).andNotEqualTo("wfStatus", "0");
                  wfRecord = this.wfRecordMapper.selectOneByExample(example);
                  if (ObjectUtil.isNull(wfRecord)) {
                     wfRecord = new GaiaWfRecord();
                  } else {
                     if ("2".equals(wfRecord.getWfStatus())) {
                        throw new BusinessException("提示：当前单据对应的工作流正在审批中，不能重复发起工作流！");
                     }

                     if ("3".equals(wfRecord.getWfStatus())) {
                        throw new BusinessException("提示：当前单据对应的工作流已审批通过，不能重复发起工作流！");
                     }
                  }

                  byte wfCode = -1;
                  switch(defineCode.hashCode()) {
                  case 867125806:
                     if (defineCode.equals("GAIA_WF_001")) {
                        wfCode = 0;
                     }
                     break;
                  case 867125807:
                     if (defineCode.equals("GAIA_WF_002")) {
                        wfCode = 1;
                     }
                     break;
                  case 867125808:
                     if (defineCode.equals("GAIA_WF_003")) {
                        wfCode = 2;
                     }
                     break;
                  case 867125809:
                     if (defineCode.equals("GAIA_WF_004")) {
                        wfCode = 3;
                     }
                     break;
                  case 867125810:
                     if (defineCode.equals("GAIA_WF_005")) {
                        wfCode = 4;
                     }
                     break;
                  case 867125811:
                     if (defineCode.equals("GAIA_WF_006")) {
                        wfCode = 5;
                     }
                     break;
                  case 867125812:
                     if (defineCode.equals("GAIA_WF_007")) {
                        wfCode = 6;
                     }
                     break;
                  case 867125813:
                     if (defineCode.equals("GAIA_WF_008")) {
                        wfCode = 7;
                     }
                     break;
                  case 867125814:
                     if (defineCode.equals("GAIA_WF_009")) {
                        wfCode = 8;
                     }
                  case 867125815:
                  case 867125816:
                  case 867125817:
                  case 867125818:
                  case 867125819:
                  case 867125820:
                  case 867125821:
                  case 867125822:
                  case 867125823:
                  case 867125824:
                  case 867125825:
                  case 867125826:
                  case 867125827:
                  case 867125828:
                  case 867125829:
                  case 867125830:
                  case 867125831:
                  case 867125832:
                  case 867125833:
                  case 867125834:
                  case 867125835:
                  case 867125846:
                  case 867125847:
                  case 867125848:
                  case 867125849:
                  case 867125850:
                  case 867125851:
                  case 867125852:
                  case 867125853:
                  case 867125854:
                  case 867125855:
                  case 867125856:
                  case 867125857:
                  case 867125858:
                  case 867125859:
                  case 867125860:
                  case 867125861:
                  case 867125862:
                  case 867125863:
                  case 867125864:
                  case 867125865:
                  case 867125866:
                  default:
                     break;
                  case 867125836:
                     if (defineCode.equals("GAIA_WF_010")) {
                        wfCode = 9;
                     }
                     break;
                  case 867125837:
                     if (defineCode.equals("GAIA_WF_011")) {
                        wfCode = 10;
                     }
                     break;
                  case 867125838:
                     if (defineCode.equals("GAIA_WF_012")) {
                        wfCode = 11;
                     }
                     break;
                  case 867125839:
                     if (defineCode.equals("GAIA_WF_013")) {
                        wfCode = 12;
                     }
                     break;
                  case 867125840:
                     if (defineCode.equals("GAIA_WF_014")) {
                        wfCode = 13;
                     }
                     break;
                  case 867125841:
                     if (defineCode.equals("GAIA_WF_015")) {
                        wfCode = 14;
                     }
                     break;
                  case 867125842:
                     if (defineCode.equals("GAIA_WF_016")) {
                        wfCode = 15;
                     }
                     break;
                  case 867125843:
                     if (defineCode.equals("GAIA_WF_017")) {
                        wfCode = 16;
                     }
                     break;
                  case 867125844:
                     if (defineCode.equals("GAIA_WF_018")) {
                        wfCode = 17;
                     }
                     break;
                  case 867125845:
                     if (defineCode.equals("GAIA_WF_019")) {
                        wfCode = 18;
                     }
                     break;
                  case 867125867:
                     if (defineCode.equals("GAIA_WF_020")) {
                        wfCode = 19;
                     }
                     break;
                  case 867125868:
                     if (defineCode.equals("GAIA_WF_021")) {
                        wfCode = 20;
                     }
                     break;
                  case 867125869:
                     if (defineCode.equals("GAIA_WF_022")) {
                        wfCode = 21;
                     }
                     break;
                  case 867125870:
                     if (defineCode.equals("GAIA_WF_023")) {
                        wfCode = 22;
                     }
                     break;
                  case 867125871:
                     if (defineCode.equals("GAIA_WF_024")) {
                        wfCode = 23;
                     }
                     break;
                  case 867125872:
                     if (defineCode.equals("GAIA_WF_025")) {
                        wfCode = 24;
                     }
                     break;
                  case 867125873:
                     if (defineCode.equals("GAIA_WF_026")) {
                        wfCode = 25;
                     }
                  case -1992661116:
                     if (defineCode.equals("GAIA_WSD_1001")) {
                        wfCode = 26;
                     }
                  case -1992661115:
                     if (defineCode.equals("GAIA_WSD_1002")) {
                        wfCode = 27;
                     }
                  case -1992661114:
                     if (defineCode.equals("GAIA_WSD_1003")) {
                        wfCode = 28;
                     }
                  case -1992661113:
                     if (defineCode.equals("GAIA_WSD_1004")) {
                        wfCode = 29;
                     }
                  case 867125900:
                     if (defineCode.equals("GAIA_WF_032")) {
                        wfCode = 30;
                     }
                  case 867125901:
                     if (defineCode.equals("GAIA_WF_033")) {
                        wfCode = 31;
                     }
                  case 867125902:
                     if (defineCode.equals("GAIA_WF_034")) {
                        wfCode = 32;
                     }

                  case 867125903:
                     if (defineCode.equals("GAIA_WF_035")) {
                        wfCode = 33;
                     }

                  case 867125904:
                     if (defineCode.equals("GAIA_WF_036")) {
                        wfCode = 34;
                     }

                  case 867125905:
                     if (defineCode.equals("GAIA_WF_037")) {
                        wfCode = 35;
                     }

                  case 867125906:
                     if (defineCode.equals("GAIA_WF_038")) {
                        wfCode = 36;
                     }

                  case 867125907:
                     if (defineCode.equals("GAIA_WF_039")) {
                        wfCode = 37;
                     }
                     break;
                  case 867125929:
                     if (defineCode.equals("GAIA_WF_040")) {
                        wfCode = 40;
                     }
                     break;
                  case 867125932:
                     if (defineCode.equals("GAIA_WF_043")) {
                        wfCode = 39;
                     }
                     break;
                  case 867125933:
                     if (defineCode.equals("GAIA_WF_044")) {
                        wfCode = 38;
                     }
                     break;
                  case 867125934:
                     if (defineCode.equals("GAIA_WF_045")) {
                        wfCode = 40;
                     }
                     break;
                  case 867125935:
                     if (defineCode.equals("GAIA_WF_046")) {
                        wfCode = 41;
                     }
                     break;
                  case 867125936:
                     if (defineCode.equals("GAIA_WF_047")) {
                        wfCode = 42;
                     }
                     break;
                  case 867125937:
                     if (defineCode.equals("GAIA_WF_048")) {
                        wfCode = 43;
                     }
                     break;
                  case 867125938:
                     if (defineCode.equals("GAIA_WF_049")) {
                        wfCode = 44;
                     }
                     break;
                  case 867125960:
                     if (defineCode.equals("GAIA_WF_050")) {
                        wfCode = 45;
                     }
                     break;
                  case 867125961:
                     if (defineCode.equals("GAIA_WF_051")) {
                        wfCode = 46;
                     }
                     break;
                  case 867125930:
                     if (defineCode.equals("GAIA_WF_041")) {
                        wfCode = 47;
                     }
                     break;
                  case 867125962:
                     if (defineCode.equals("GAIA_WF_052")) {
                        wfCode = 48;
                     }
                     break;
                  }


                  switch(wfCode) {
                  case 0:
                     wfRecord.setWfSeq(new BigDecimal(processList.size()));
                     wfRecord.setWfStatus("3");
                     GetWfCreateAuthInData authTitle = Util.initFieldName(GetWfCreateAuthInData.class);
                     if (ObjectUtil.isNull(authTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateAuthInData> authDetail = (List)wfCreateInData.getWfDetail();
                     if (authDetail == null) {
                        authDetail = new ArrayList();
                     }

                     ((List)authDetail).add(0, authTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(authDetail));
                     break;
                  case 1:
                     wfRecord.setWfSeq(new BigDecimal(processList.size()));
                     wfRecord.setWfStatus("3");
                     GetWfCreateDelegateInData delegateTitle = (GetWfCreateDelegateInData)Util.initFieldName(GetWfCreateDelegateInData.class);
                     if (ObjectUtil.isNull(delegateTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateDelegateInData> delegateDetail = (List)wfCreateInData.getWfDetail();
                     if (delegateDetail == null) {
                        delegateDetail = new ArrayList();
                     }

                     ((List)delegateDetail).add(0, delegateTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(delegateDetail));
                     break;
                  case 2:
                     wfRecord.setWfSeq(new BigDecimal(processList.size()));
                     wfRecord.setWfStatus("3");
                     GetWfCreateOrgInData orgTitle = (GetWfCreateOrgInData)Util.initFieldName(GetWfCreateOrgInData.class);
                     if (ObjectUtil.isNull(orgTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateOrgInData> orgDetail = (List)wfCreateInData.getWfDetail();
                     if (orgDetail == null) {
                        orgDetail = new ArrayList();
                     }

                     ((List)orgDetail).add(0, orgTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(orgDetail));
                     break;
                  case 3:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateCategoryAnalysisInData categoryAnalysisTitle = (GetWfCreateCategoryAnalysisInData)Util.initFieldName(GetWfCreateCategoryAnalysisInData.class);
                     if (ObjectUtil.isNull(categoryAnalysisTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateCategoryAnalysisInData> categoryAnalysisDetail = (List)wfCreateInData.getWfDetail();
                     if (categoryAnalysisDetail == null) {
                        categoryAnalysisDetail = new ArrayList();
                     }

                     ((List)categoryAnalysisDetail).add(0, categoryAnalysisTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(categoryAnalysisDetail));
                     break;
                  case 4:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateReturnCompadmInData returnCompadmTitle = (GetWfCreateReturnCompadmInData)Util.initFieldName(GetWfCreateReturnCompadmInData.class);
                     if (ObjectUtil.isNull(returnCompadmTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateReturnCompadmInData> returnCompadmDetail = (List)wfCreateInData.getWfDetail();
                     if (returnCompadmDetail == null) {
                        returnCompadmDetail = new ArrayList();
                     }

                     ((List)returnCompadmDetail).add(0, returnCompadmTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(returnCompadmDetail));
                     break;
                  case 5:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateLossCompadmInData lossCompadmTitle = Util.initFieldName(GetWfCreateLossCompadmInData.class);
                     if (ObjectUtil.isNull(lossCompadmTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateLossCompadmInData> lossCompadmDetail = (List)wfCreateInData.getWfDetail();
                     if (lossCompadmDetail == null) {
                        lossCompadmDetail = new ArrayList();
                     }

                     ((List)lossCompadmDetail).add(0, lossCompadmTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(lossCompadmDetail));
                     wfRecord.setWfNewWorkflowDetail(JSONObject.toJSONString(wfCreateInData.getNewWorkflowInData()));
                     break;
                  case 6:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateLossSingleInData lossSingleTitle = Util.initFieldName(GetWfCreateLossSingleInData.class);
                     if (ObjectUtil.isNull(lossSingleTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateLossSingleInData> lossSingleDetail = (List)wfCreateInData.getWfDetail();
                     if (lossSingleDetail == null) {
                        lossSingleDetail = new ArrayList();
                     }

                     lossSingleDetail.add(0, lossSingleTitle);
                     wfRecord.setWfNewWorkflowDetail(JSONObject.toJSONString(wfCreateInData.getNewWorkflowInData()));
                     wfRecord.setWfJsonString(JSONObject.toJSONString(lossSingleDetail));
                     break;
                  case 7:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateDpgjCompadmInData dpgjCompadmTitle = Util.initFieldName(GetWfCreateDpgjCompadmInData.class);
                     if (ObjectUtil.isNull(dpgjCompadmTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateDpgjCompadmInData> dpgjCompadmDetail = (List)wfCreateInData.getWfDetail();
                     if (dpgjCompadmDetail == null) {
                        dpgjCompadmDetail = new ArrayList();
                     }

                     ((List)dpgjCompadmDetail).add(0, dpgjCompadmTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(dpgjCompadmDetail));
                     break;
                  case 8:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateDpgjSingleInData dpgjSingleTitle = (GetWfCreateDpgjSingleInData)Util.initFieldName(GetWfCreateDpgjSingleInData.class);
                     if (ObjectUtil.isNull(dpgjSingleTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateDpgjSingleInData> dpgjSingleDetail = (List)wfCreateInData.getWfDetail();
                     if (dpgjSingleDetail == null) {
                        dpgjSingleDetail = new ArrayList();
                     }

                     ((List)dpgjSingleDetail).add(0, dpgjSingleTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(dpgjSingleDetail));
                     break;
                  case 9:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateDpzlCompadmInData dpzlCompadmTitle = (GetWfCreateDpzlCompadmInData)Util.initFieldName(GetWfCreateDpzlCompadmInData.class);
                     if (ObjectUtil.isNull(dpzlCompadmTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateDpzlCompadmInData> dpzlCompadmDetail = (List)wfCreateInData.getWfDetail();
                     if (dpzlCompadmDetail == null) {
                        dpzlCompadmDetail = new ArrayList();
                     }

                     ((List)dpzlCompadmDetail).add(0, dpzlCompadmTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(dpzlCompadmDetail));
                     break;
                  case 10:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateDpzlSingleInData dpzlSingleTitle = (GetWfCreateDpzlSingleInData)Util.initFieldName(GetWfCreateDpzlSingleInData.class);
                     if (ObjectUtil.isNull(dpzlSingleTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateDpzlSingleInData> dpzlSingleDetail = (List)wfCreateInData.getWfDetail();
                     if (dpzlSingleDetail == null) {
                        dpzlSingleDetail = new ArrayList();
                     }

                     ((List)dpzlSingleDetail).add(0, dpzlSingleTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(dpzlSingleDetail));
                     break;
                  case 11:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateZdzlCompadmInData zdzlCompadmTitle = (GetWfCreateZdzlCompadmInData)Util.initFieldName(GetWfCreateZdzlCompadmInData.class);
                     if (ObjectUtil.isNull(zdzlCompadmTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateZdzlCompadmInData> zdzlCompadmDetail = (List)wfCreateInData.getWfDetail();
                     if (zdzlCompadmDetail == null) {
                        zdzlCompadmDetail = new ArrayList();
                     }

                     ((List)zdzlCompadmDetail).add(0, zdzlCompadmTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(zdzlCompadmDetail));
                     break;
                  case 12:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateZdzlSingleInData zdzlSingleTitle = (GetWfCreateZdzlSingleInData)Util.initFieldName(GetWfCreateZdzlSingleInData.class);
                     if (ObjectUtil.isNull(zdzlSingleTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateZdzlSingleInData> zdzlSingleDetail = (List)wfCreateInData.getWfDetail();
                     if (zdzlSingleDetail == null) {
                        zdzlSingleDetail = new ArrayList();
                     }

                     ((List)zdzlSingleDetail).add(0, zdzlSingleTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(zdzlSingleDetail));
                     break;
                  case 13: case 38:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateProductGspInData productGspTitle = Util.initFieldName(GetWfCreateProductGspInData.class);
                     if (ObjectUtil.isNull(productGspTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateProductGspInData> productGspDetail = (List)wfCreateInData.getWfDetail();
                     if (productGspDetail == null) {
                        productGspDetail = new ArrayList();
                     }

                     ((List)productGspDetail).add(0, productGspTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(productGspDetail));
                     break;
                  case 14:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateSupGspInData supGspTitle = Util.initFieldName(GetWfCreateSupGspInData.class);
                     if (ObjectUtil.isNull(supGspTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateSupGspInData> supGspDetail = (List)wfCreateInData.getWfDetail();
                     if (supGspDetail == null) {
                        supGspDetail = new ArrayList();
                     }

                     ((List)supGspDetail).add(0, supGspTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(supGspDetail));
                     break;
                  case 15:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateCusGspInData cusGspTitle = Util.initFieldName(GetWfCreateCusGspInData.class);
                     if (ObjectUtil.isNull(cusGspTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateCusGspInData> cusGspDetail = (List)wfCreateInData.getWfDetail();
                     if (cusGspDetail == null) {
                        cusGspDetail = new ArrayList();
                     }

                     ((List)cusGspDetail).add(0, cusGspTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(cusGspDetail));
                     break;
                  case 16:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreatePurOrderInData purOrderTitle = Util.initFieldName(GetWfCreatePurOrderInData.class);
                     if (ObjectUtil.isNull(purOrderTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreatePurOrderInData> purOrderDetail = (List)wfCreateInData.getWfDetail();
                     if (purOrderDetail == null) {
                        purOrderDetail = new ArrayList();
                     }

                     ((List)purOrderDetail).add(0, purOrderTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(purOrderDetail));
                     break;
                  case 17:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreatePurPayInData purPayTitle = (GetWfCreatePurPayInData)Util.initFieldName(GetWfCreatePurPayInData.class);
                     if (ObjectUtil.isNull(purPayTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreatePurPayInData> purPayDetail = (List)wfCreateInData.getWfDetail();
                     if (purPayDetail == null) {
                        purPayDetail = new ArrayList();
                     }

                     ((List)purPayDetail).add(0, purPayTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(purPayDetail));
                     break;
                  case 18:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     WfSpbssq spbsTitle = (WfSpbssq)Util.initFieldName(WfSpbssq.class);
                     if (ObjectUtil.isNull(spbsTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List wfSpbssqList = (List)wfCreateInData.getWfDetail();
                     List<WfSpbssq> spbsDetail = new ArrayList<>();
                     for (Object o : wfSpbssqList) {
                        WfSpbssq wfSpbssq = new WfSpbssq();
                        BeanUtil.copyProperties(o,wfSpbssq);
                        spbsDetail.add(wfSpbssq);
                     }
                     if (spbsDetail == null) {
                        spbsDetail = new ArrayList();
                     }
                     if (CollUtil.isNotEmpty(spbsDetail) && spbsDetail.size()>0) {
                        WfSpbssq wfSpbssq = spbsDetail.get(0);
                        wfSpbssq.setBillNo(wfCreateInData.getWfOrder());
                        wfSpbssq.setReason(wfCreateInData.getWfDescription());
                     }

                     ((List)spbsDetail).add(0, spbsTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(spbsDetail));
                     break;
                  case 19:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     WfZzysq zzyTitle = (WfZzysq)Util.initFieldName(WfZzysq.class);
                     if (ObjectUtil.isNull(zzyTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List zzyList = (List)wfCreateInData.getWfDetail();
                     List<WfZzysq> zzyDetail = new ArrayList<>();
                     for (Object o : zzyList) {
                        WfZzysq wfZzysq = new WfZzysq();
                        BeanUtil.copyProperties(o,wfZzysq);
                        zzyDetail.add(wfZzysq);
                     }
                     if (zzyDetail == null) {
                        zzyDetail = new ArrayList();
                     }
                     if (CollUtil.isNotEmpty(zzyDetail) && zzyDetail.size()>0) {
                        WfZzysq wfZzysq = zzyDetail.get(0);
                        wfZzysq.setBillNo(wfCreateInData.getWfOrder());
                        wfZzysq.setReason(wfCreateInData.getWfDescription());
                     }
                     ((List)zzyDetail).add(0, zzyTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(zzyDetail));
                     break;
                  case 20: case 31:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateSpyhInData spyhTitle = Util.initFieldName(GetWfCreateSpyhInData.class);
                     if (ObjectUtil.isNull(spyhTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateSpyhInData> spyhDetail = (List)wfCreateInData.getWfDetail();
                     if (spyhDetail == null) {
                        spyhDetail = new ArrayList();
                     }

                     ((List)spyhDetail).add(0, spyhTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(spyhDetail));
                     break;
                  case 21:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateNewCkbsbyInData ckbsbyTitle = Util.initFieldName(GetWfCreateNewCkbsbyInData.class);
                     if (ObjectUtil.isNull(ckbsbyTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateNewCkbsbyInData> ckbsbyDetail = (List)wfCreateInData.getWfDetail();
                     if (ckbsbyDetail == null) {
                        ckbsbyDetail = new ArrayList();
                     }

                     ckbsbyDetail.add(0, ckbsbyTitle);
//                     JSONArray.parseArray(JSONObject.toJSONString(ckbsbyDetail.get(1), GetWfCreateNewCkbsbyInData.class));
                     wfRecord.setWfNewWorkflowDetail(this.createNewWorkflowInData(JSON.parseObject(JSON.toJSONString(ckbsbyDetail.get(1)), GetWfCreateNewCkbsbyInData.class)));
                     break;
                  case 22:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreatePhtzInData phtzTitle = Util.initFieldName(GetWfCreatePhtzInData.class);
                     if (ObjectUtil.isNull(phtzTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreatePhtzInData> phtzDetail = (List)wfCreateInData.getWfDetail();
                     if (phtzDetail == null) {
                        phtzDetail = new ArrayList();
                     }

                     ((List)phtzDetail).add(0, phtzTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(phtzDetail));
                     break;
                  case 23: case 32:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateRkshInData rkshTitle = Util.initFieldName(GetWfCreateRkshInData.class);
                     if (ObjectUtil.isNull(rkshTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateRkshInData> rkshDetail = (List)wfCreateInData.getWfDetail();
                     if (rkshDetail == null) {
                        rkshDetail = new ArrayList();
                     }

                     ((List)rkshDetail).add(0, rkshTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(rkshDetail));
                     break;
                  case 24:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateRkysInData rkysTitle = (GetWfCreateRkysInData)Util.initFieldName(GetWfCreateRkysInData.class);
                     if (ObjectUtil.isNull(rkysTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateRkysInData> rkysDetail = (List)wfCreateInData.getWfDetail();
                     if (rkysDetail == null) {
                        rkysDetail = new ArrayList();
                     }

                     ((List)rkysDetail).add(0, rkysTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(rkysDetail));
                     break;
                  case 25:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateKctzInData kctzTitle = Util.initFieldName(GetWfCreateKctzInData.class);
                     if (ObjectUtil.isNull(kctzTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateKctzInData> kctzDetail = (List)wfCreateInData.getWfDetail();
                     if (kctzDetail == null) {
                        kctzDetail = new ArrayList();
                     }

                     ((List)kctzDetail).add(0, kctzTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(kctzDetail));
                     break;
                  case 26:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfNewBatchNoInData batchTitle = Util.initFieldName(GetWfNewBatchNoInData.class);
                     if (ObjectUtil.isNull(batchTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfNewBatchNoInData> batchDetail = (List)wfCreateInData.getWfDetail();
                     if (batchDetail == null) {
                        batchDetail = new ArrayList();
                     }

                     ((List)batchDetail).add(0, batchTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(batchDetail));
                     break;
                  case 27:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfConsignmentDistributionInData cdTitle = Util.initFieldName(GetWfConsignmentDistributionInData.class);
                     if (ObjectUtil.isNull(cdTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfConsignmentDistributionInData> cdDetail = (List)wfCreateInData.getWfDetail();
                     if (cdDetail == null) {
                        cdDetail = new ArrayList();
                     }

                     ((List)cdDetail).add(0, cdTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(cdDetail));
                     break;
                  case 28:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfDistributionReturnInData drTitle = Util.initFieldName(GetWfDistributionReturnInData.class);
                     if (ObjectUtil.isNull(drTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfDistributionReturnInData> drDetail = (List)wfCreateInData.getWfDetail();
                     if (drDetail == null) {
                        drDetail = new ArrayList();
                     }

                     ((List)drDetail).add(0, drTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(drDetail));
                     break;
                  case 29:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     ReturnWorkflowDTO srTitle = Util.initFieldName(ReturnWorkflowDTO.class);
                     if (ObjectUtil.isNull(srTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List returnWorkFlowList = (List)wfCreateInData.getWfDetail();
                     List<ReturnWorkflowDTO> srDetail = new ArrayList<>();
                     for (Object o : returnWorkFlowList) {
                        ReturnWorkflowDTO returnWorkflowDTO = new ReturnWorkflowDTO();
                        BeanUtil.copyProperties(o,returnWorkflowDTO);
                        srDetail.add(returnWorkflowDTO);
                     }
                     if (srDetail == null) {
                        srDetail = new ArrayList();
                     }
                     if (CollUtil.isNotEmpty(srDetail) && srDetail.size()>0) {
                        ReturnWorkflowDTO returnWorkflowDTO = srDetail.get(0);
                        returnWorkflowDTO.setBillNo(wfCreateInData.getWfOrder());
                        returnWorkflowDTO.setReason(wfCreateInData.getWfDescription());
                     }

                     ((List)srDetail).add(0, srTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(srDetail));
                     break;
                  case 30:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfSupplierApprovalInData saTitle = Util.initFieldName(GetWfSupplierApprovalInData.class);
                     if (ObjectUtil.isNull(saTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfSupplierApprovalInData> saDetail = (List)wfCreateInData.getWfDetail();
                     if (saDetail == null) {
                        saDetail = new ArrayList();
                     }

                     ((List)saDetail).add(0, saTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(saDetail));
                     break;
                  case 39:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfSupplierBeforeApprovalInData sbaTitle = Util.initFieldName(GetWfSupplierBeforeApprovalInData.class);
                     if (ObjectUtil.isNull(sbaTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfSupplierBeforeApprovalInData> sbaDetail = (List)wfCreateInData.getWfDetail();
                     if (sbaDetail == null) {
                        sbaDetail = new ArrayList();
                     }

                     ((List)sbaDetail).add(0, sbaTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(sbaDetail));
                     break;
                  case 33: case 34:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateStoreBsInData bsTitle = Util.initFieldName(GetWfCreateStoreBsInData.class);
                     if (ObjectUtil.isNull(bsTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateStoreBsInData> bsDetail = (List)wfCreateInData.getWfDetail();
                     if (bsDetail == null) {
                        bsDetail = new ArrayList();
                     }

                     ((List)bsDetail).add(0, bsTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(bsDetail));
                     break;
                  case 35:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateStoreLyInData lyTitle1 = Util.initFieldName(GetWfCreateStoreLyInData.class);
                     if (ObjectUtil.isNull(lyTitle1)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateStoreLyInData> lyDetail1 = (List)wfCreateInData.getWfDetail();
                     if (lyDetail1 == null) {
                        lyDetail1 = new ArrayList();
                     }

                     ((List)lyDetail1).add(0, lyTitle1);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(lyDetail1));
                     break;
                  case 36:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateStoreLyInData lyTitle = Util.initFieldName(GetWfCreateStoreLyInData.class);
                     if (ObjectUtil.isNull(lyTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateStoreLyInData> lyDetail = (List)wfCreateInData.getWfDetail();
                     if (lyDetail == null) {
                        lyDetail = new ArrayList();
                     }

                     ((List)lyDetail).add(0, lyTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(lyDetail));
                     break;
                  case 37:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     WfCkspbsby ciTitle = Util.initFieldName(WfCkspbsby.class);
                     if (ObjectUtil.isNull(ciTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List ciDetailList = (List)wfCreateInData.getWfDetail();
                     List<WfCkspbsby> ciDetail = new ArrayList<>();
                     for (Object o : ciDetailList) {
                        WfCkspbsby wfCkspbsby = new WfCkspbsby();
                        BeanUtil.copyProperties(o,wfCkspbsby);
                        ciDetail.add(wfCkspbsby);
                     }
                     if (ciDetail == null) {
                        ciDetail = new ArrayList();
                     }
                     if (CollUtil.isNotEmpty(ciDetail) && ciDetail.size()>0) {
                        WfCkspbsby wfCkspbsby = ciDetail.get(0);
                        wfCkspbsby.setBillNo(wfCreateInData.getWfOrder());
                        wfCkspbsby.setReason(wfCreateInData.getWfDescription());
                     }
                     ((List)ciDetail).add(0, ciTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(ciDetail));
                     break;
                  case 40:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfSupplierApprovalInData ckbTitle = Util.initFieldName(GetWfSupplierApprovalInData.class);
                     if (ObjectUtil.isNull(ckbTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfSupplierApprovalInData> ckbDetail = (List)wfCreateInData.getWfDetail();
                     if (ckbDetail == null) {
                        ckbDetail = new ArrayList();
                     }

                     ckbDetail.add(0, ckbTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(ckbDetail));
                     break;
                  case 41:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCreateLossDefecproInData ldTitle = Util.initFieldName(GetWfCreateLossDefecproInData.class);
                     if (ObjectUtil.isNull(ldTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCreateLossDefecproInData> ldDetail = (List)wfCreateInData.getWfDetail();
                     if (ldDetail == null) {
                        ldDetail = new ArrayList();
                     }

                     ldDetail.add(0, ldTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(ldDetail));
                     break;
                  case 42:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfUnqualifiedInData unqTitle = Util.initFieldName(GetWfUnqualifiedInData.class);
                     if (ObjectUtil.isNull(unqTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfUnqualifiedInData> unqDetail = (List)wfCreateInData.getWfDetail();
                     if (unqDetail == null) {
                        unqDetail = new ArrayList();
                     }

                     unqDetail.add(0, unqTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(unqDetail));
                     break;
                  case 43:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfClientEditInData ceiTitle = Util.initFieldName(GetWfClientEditInData.class);
                     if (ObjectUtil.isNull(ceiTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfClientEditInData> ceiDetail = (List)wfCreateInData.getWfDetail();
                     if (ceiDetail == null) {
                        ceiDetail = new ArrayList();
                     }

                     ceiDetail.add(0, ceiTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(ceiDetail));
                     break;
                  case 44:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfCustomerEditInData ceeTitle = Util.initFieldName(GetWfCustomerEditInData.class);
                     if (ObjectUtil.isNull(ceeTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfCustomerEditInData> ceeDetail = (List)wfCreateInData.getWfDetail();
                     if (ceeDetail == null) {
                        ceeDetail = new ArrayList();
                     }

                     ceeDetail.add(0, ceeTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(ceeDetail));
                     break;
                  case 47:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfProductEditInData peTitle = Util.initFieldName(GetWfProductEditInData.class);
                     if (ObjectUtil.isNull(peTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfProductEditInData> peDetail = (List)wfCreateInData.getWfDetail();
                     if (peDetail == null) {
                        peDetail = new ArrayList();
                     }

                     peDetail.add(0, peTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(peDetail));
                     break;
                  case 45: case 46:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetWfStoreStockInData unq2Title = Util.initFieldName(GetWfStoreStockInData.class);
                     if (ObjectUtil.isNull(unq2Title)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetWfStoreStockInData> unq2Detail = (List)wfCreateInData.getWfDetail();
                     if (unq2Detail == null) {
                        unq2Detail = new ArrayList();
                     }

                     unq2Detail.add(0, unq2Title);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(unq2Detail));
                     break;
                  case 48:
                     wfRecord.setWfSeq(new BigDecimal("1"));
                     wfRecord.setWfStatus("2");
                     GetSalesChangePriceInData scpTitle = Util.initFieldName(GetSalesChangePriceInData.class);
                     if (ObjectUtil.isNull(scpTitle)) {
                        throw new BusinessException("提示：发起工作流异常");
                     }

                     List<GetSalesChangePriceInData> scpDetail = (List)wfCreateInData.getWfDetail();
                     if (scpDetail == null) {
                        scpDetail = new ArrayList();
                     }

                     scpDetail.add(0, scpTitle);
                     wfRecord.setWfJsonString(JSONObject.toJSONString(scpDetail));
                     break;
                  default:
                     throw new BusinessException(UtilMessage.WF_TYPE_NOT_EXITS);
                  }

                  boolean insertFlag = false;
                  if (StrUtil.isBlank(wfRecord.getWfCode())) {
                     insertFlag = true;
                     String codePre = DateUtil.format(new Date(), "yyMMdd");
                     String code = this.wfRecordMapper.selectNextWfCode(client, codePre);
                     wfRecord.setWfCode(code);
                     wfRecord.setClient(client);
                     wfRecord.setPoCompanyCode(define.getPoCompanyCode());
                     wfRecord.setWfDefineCode(defineCode);
                     wfRecord.setWfKind(define.getWfKind());
                     wfRecord.setWfRefPage(define.getWfRefPage());
                     wfRecord.setWfRefReturn(define.getWfRefReturn());
                     wfRecord.setWfOrder(wfCreateInData.getWfOrder());
                  }

                  wfRecord.setWfSite(wfCreateInData.getWfSite());
                  wfRecord.setWfTitle(wfCreateInData.getWfTitle());
                  wfRecord.setWfDescription(wfCreateInData.getWfDescription());
                  wfRecord.setCreateUser(loginOutData.getUserId());
                  wfRecord.setCreateTime(DateUtil.format(new Date(), "yyyyMMdd"));
                  if (insertFlag) {
                     this.wfRecordMapper.insert(wfRecord);
                  } else {
                     this.wfRecordMapper.updateByPrimaryKey(wfRecord);
                  }

                  int index = 1;
                  byte var66 = -1;
                  switch(defineCode.hashCode()) {
                  case 867125806:
                     if (defineCode.equals("GAIA_WF_001")) {
                        var66 = 0;
                     }
                     break;
                  case 867125807:
                     if (defineCode.equals("GAIA_WF_002")) {
                        var66 = 1;
                     }
                     break;
                  case 867125808:
                     if (defineCode.equals("GAIA_WF_003")) {
                        var66 = 2;
                     }
                  }

                  switch(var66) {
                  case 0:
                  case 1:
                  case 2:

                     Iterator<GetWfDefineProcessOutData> processOutDataIterator = processList.iterator();

                     while(processOutDataIterator.hasNext()) {
                        GetWfDefineProcessOutData process = processOutDataIterator.next();

                        for(Iterator<GetWfDefineUserOutData> var69 = process.getApproveUserOutData().iterator(); var69.hasNext(); ++index) {
                           GetWfDefineUserOutData user = var69.next();
                           GaiaWfRecordHistory history = new GaiaWfRecordHistory();
                           history.setClient(define.getClient());
                           history.setPoCompanyCode(define.getPoCompanyCode());
                           history.setWfCode(wfRecord.getWfCode());
                           history.setWfHistorySeq(new BigDecimal(index));
                           history.setWfDefineCode(define.getWfDefineCode());
                           history.setWfKind(define.getWfKind());
                           history.setWfSeq(process.getWfSeq());
                           history.setUserId(user.getUserId());
                           history.setResult("PASS");
                           history.setMemo("同意");
                           history.setCreateUser(loginOutData.getUserId());
                           history.setCreateTime(DateUtil.format(new Date(), "yyyyMMdd"));
                           this.wfRecordHistoryMapper.insert(history);
                        }
                     }
                  default:
                  }

                  //自动审批
//                  GetWorkflowInData approveWorkflowInData = new GetWorkflowInData();
//                  approveWorkflowInData.setUserId(loginOutData.getUserId());
//                  approveWorkflowInData.setClient(client);
//                  approveWorkflowInData.setWfCode(wfRecord.getWfCode());
//                  approveWorkflowInData.setResult("PASS");
//                  approveWorkflowInData.setMemo("同意");
//                  approveWorkflowInData.setAutoApprove(1);
//                  this.approve(approveWorkflowInData);
//                  this.autoApproveProcess(wfRecord, wfDefineProcessMapper.selectDefineApproveList(client, wfRecord.getWfDefineCode()), new GaiaWfRecordHistory(), BigDecimal.ONE);
               }
            }
         }
      }

      return wfRecord;
   }

   private String createNewWorkflowInData(GetWfCreateNewCkbsbyInData newCkbsbyInData){
      GetWfPhysicalNewWorkflowInData workflowInData = new GetWfPhysicalNewWorkflowInData();
      workflowInData.setVoucherId(newCkbsbyInData.getWmsPddh());
      workflowInData.setInventoryDate(newCkbsbyInData.getWmsSqrq());
      workflowInData.setInventoryType(newCkbsbyInData.getWmsPdlx() + "、" + newCkbsbyInData.getWmsPdff());
      workflowInData.setItemInventoryTotal(new BigDecimal(newCkbsbyInData.getWmsPdPx()));
      workflowInData.setItemInventoryProfit(new BigDecimal(newCkbsbyInData.getWmsPyPx()));
      workflowInData.setItemInventoryLosses(new BigDecimal(newCkbsbyInData.getWmsPkPx()));
      workflowInData.setItemAbsoluteDiff(new BigDecimal(newCkbsbyInData.getWmsJdcyPx()));
      workflowInData.setItemAbsoluteDiffRatio(newCkbsbyInData.getWmsJdcylPx());
      workflowInData.setItemRelativeDiff(new BigDecimal(newCkbsbyInData.getWmsXdcyPx()));
      workflowInData.setItemRelativeDiffRatio(newCkbsbyInData.getWmsXdcylPx());
      workflowInData.setBatchInventoryTotal(newCkbsbyInData.getWmsPdPhhs());
      workflowInData.setBatchInventoryProfit(new BigDecimal(newCkbsbyInData.getWmsPyPhhs()));
      workflowInData.setBatchInventoryLosses(new BigDecimal(newCkbsbyInData.getWmsPkPhhs()));
      workflowInData.setBatchAbsoluteDiff(new BigDecimal(newCkbsbyInData.getWmsJdcyPhhs()));
      workflowInData.setBatchAbsoluteDiffRatio(newCkbsbyInData.getWmsJdcylPhhs());
      workflowInData.setBatchRelativeDiff(new BigDecimal(newCkbsbyInData.getWmsXdcyPhhs()));
      workflowInData.setBatchRelativeDiffRatio(newCkbsbyInData.getWmsXdcylPhhs());
      workflowInData.setNumInventoryTotal(newCkbsbyInData.getWmsPdSl());
      workflowInData.setNumInventoryProfit(new BigDecimal(newCkbsbyInData.getWmsPySl()));
      workflowInData.setNumInventoryLosses(new BigDecimal(newCkbsbyInData.getWmsPkSl()));
      workflowInData.setNumAbsoluteDiff(new BigDecimal(newCkbsbyInData.getWmsJdcySl()));
      workflowInData.setNumAbsoluteDiffRatio(newCkbsbyInData.getWmsJdcylSl());
      workflowInData.setNumRelativeDiff(new BigDecimal(newCkbsbyInData.getWmsXdcySl()));
      workflowInData.setNumRelativeDiffRatio(newCkbsbyInData.getWmsXdcylSl());
      workflowInData.setCostInventoryTotal(newCkbsbyInData.getWmsPdCbe());
      workflowInData.setCostInventoryProfit(newCkbsbyInData.getWmsPyCbe());
      workflowInData.setCostInventoryLosses(newCkbsbyInData.getWmsPkCbe());
      workflowInData.setCostAbsoluteDiff(new BigDecimal(newCkbsbyInData.getWmsJdcyCbe()));
      workflowInData.setCostAbsoluteDiffRatio(newCkbsbyInData.getWmsJdcylCbe());
      workflowInData.setCostRelativeDiff(new BigDecimal(newCkbsbyInData.getWmsXdcyCbe()));
      workflowInData.setCostRelativeDiffRatio(newCkbsbyInData.getWmsXdcylCbe());
      workflowInData.setRetailInventoryTotal(newCkbsbyInData.getWmsPdLse());
      workflowInData.setRetailInventoryProfit(newCkbsbyInData.getWmsPyLse());
      workflowInData.setRetailInventoryLosses(newCkbsbyInData.getWmsPkLse());
      workflowInData.setRetailAbsoluteDiff(new BigDecimal(newCkbsbyInData.getWmsJdcyLse()));
      workflowInData.setRetailAbsoluteDiffRatio(newCkbsbyInData.getWmsJdcylLse());
      workflowInData.setRetailRelativeDiff(new BigDecimal(newCkbsbyInData.getWmsXdcyLse()));
      workflowInData.setRetailRelativeDiffRatio(newCkbsbyInData.getWmsXdcylLse());
      return JSON.toJSONString(workflowInData);
   }

   @Override
   public PageInfo<GetWorkflowOutData> selectApprovingList(GetWorkflowInData inData) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      String startDate = inData.getStartDate();
      String endDate = inData.getEndDate();
      if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
         throw new BusinessException("请输入结束日期");
      }

      if(StringUtils.isBlank(startDate) && StringUtils.isNotBlank(endDate)){
         throw new BusinessException("请输入开始日期");
      }
      List<GetWorkflowOutData> outData = this.wfRecordMapper.selectApprovingList(inData);
//      this.transferWorkflowList(outData);
      return ObjectUtil.isNotEmpty(outData) ? new PageInfo(outData) : new PageInfo();
   }

   @Override
   public PageInfo<GetWorkflowOutData> selectApprovedList(GetWorkflowInData inData) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<GetWorkflowOutData> outData = this.wfRecordMapper.selectApprovedList(inData);
      String startDate = inData.getStartDate();
      String endDate = inData.getEndDate();
      if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
         throw new BusinessException("请输入结束日期");
      }

      if(StringUtils.isBlank(startDate) && StringUtils.isNotBlank(endDate)){
         throw new BusinessException("请输入开始日期");
      }
//      this.transferWorkflowList(outData);
      return ObjectUtil.isNotEmpty(outData) ? new PageInfo(outData) : new PageInfo();
   }

   @Override
   public PageInfo<GetWorkflowOutData> getMyApprovalList(GetWorkflowInData inData) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<GetWorkflowOutData> outData = this.wfRecordMapper.getMyApprovalList(inData);
      String startDate = inData.getStartDate();
      String endDate = inData.getEndDate();
      if(StringUtils.isNotBlank(startDate) && StringUtils.isBlank(endDate)){
         throw new BusinessException("请输入结束日期");
      }

      if(StringUtils.isBlank(startDate) && StringUtils.isNotBlank(endDate)){
         throw new BusinessException("请输入开始日期");
      }
//      this.transferWorkflowList(outData);
      return ObjectUtil.isNotEmpty(outData) ? new PageInfo(outData) : new PageInfo();
   }

   /**
    * 根据业务方需求工作流显示字段个性化
    * @param outDataList
    * @return
    */
   private List<GetWorkflowOutData> transferWorkflowList(List<GetWorkflowOutData> outDataList){
      for(GetWorkflowOutData workflowOutData : outDataList){
         String defineCode = workflowOutData.getDefineCode();
         //供应商预付款
         if(StringUtils.equalsIgnoreCase("GAIA_WF_043", defineCode)){
            List<GetWfSupplierBeforeApprovalInData> supplierBeforeApprovalInDataList = JSONArray.parseArray(workflowOutData.getWfJsonString(), GetWfSupplierBeforeApprovalInData.class);
            for(GetWfSupplierBeforeApprovalInData supplierBeforeApprovalInData : supplierBeforeApprovalInDataList){
               workflowOutData.setWfTitle(supplierBeforeApprovalInData.getSupCode() + "-" + supplierBeforeApprovalInData.getSupName() + "(" + supplierBeforeApprovalInData.getApplicationPaymentAmount() + "元" + ")");
               workflowOutData.setWfDescription(supplierBeforeApprovalInData.getApprovalRemark());
            }

            //供应商付款申请
         }else if(StringUtils.equalsIgnoreCase("GAIA_WF_032", defineCode)){
            List<GetWfSupplierApprovalInData> supplierApprovalInDataList = JSONArray.parseArray(workflowOutData.getWfJsonString(), GetWfSupplierApprovalInData.class);
            for(GetWfSupplierApprovalInData supplierApprovalInData : supplierApprovalInDataList){
               workflowOutData.setWfTitle(supplierApprovalInData.getSupCode() + "-" +supplierApprovalInData.getSupName() + "(" + supplierApprovalInData.getApplicationAmount() + "元" + ")");
               workflowOutData.setWfDescription(supplierApprovalInData.getGpaRemarks());
            }
         }
      }

      return outDataList;
   }

   @Override
   public GetWorkflowOutData selectOne(GetWorkflowInData inData) {
      GetWorkflowOutData outData = new GetWorkflowOutData();
      String defineCode = inData.getDefineCode();
      Example example = new Example(GaiaWfRecord.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wfCode", inData.getWfCode());
      GaiaWfRecord record = this.wfRecordMapper.selectOneByExample(example);
      if (ObjectUtil.isNull(record)) {
         throw new BusinessException("提示：工作流不存在");
      } else {
         int isNewReport = 1;
         outData.setWfTitle(record.getWfTitle());
         outData.setWfDescription(record.getWfDescription());
         outData.setWfKind(record.getWfKind());

         //新版报表
         if((StringUtils.isNotEmpty(defineCode) && StringUtils.equals("GAIA_WF_007", defineCode) || StringUtils.equals("GAIA_WF_006", defineCode) || StringUtils.equals("GAIA_WF_022", defineCode)) &&  isNewReport == 1){
            String storeName = storeDataMapper.getStoreName(inData.getClient(), record.getWfSite());
            outData.setStoreName(storeName);
            outData.setWfNewWorkflowDetail(record.getWfNewWorkflowDetail());
         }else{
            String storeName = storeDataMapper.getStoreName(inData.getClient(), record.getWfSite());
            outData.setStoreName(storeName);
            outData.setWfJsonString(record.getWfJsonString());
            outData.setIsNewReport(0);
         }

         outData.setWfSeq(record.getWfSeq().intValue());
         outData.setWfRefPage(record.getWfRefPage());
         outData.setWfOrder(record.getWfOrder());
         outData.setClientId(record.getClient());
         outData.setDefineCode(record.getWfDefineCode());
         outData.setWfSite(record.getWfSite());
         String wfStatus = record.getWfStatus();
         outData.setWfStatus(record.getWfStatus());
         outData.setCurrentOperation("3");
         if ("1".equals(wfStatus) && inData.getUserId().equals(record.getCreateUser())) {
            outData.setCurrentOperation("1");
         }
         Example income = new Example(GaiaSdIncomeStatementH.class);
         income.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("gsishVoucherId", record.getWfOrder());
         GaiaSdIncomeStatementH  incomeStatementH = gaiaSdIncomeStatementHMapper.selectOneByExample(income);
         if(incomeStatementH!=null){
            //要是其他字符串 要分割处理
            outData.setGsishBranch(incomeStatementH.getGsishBranch());
            outData.setGsishRemark(incomeStatementH.getGsishRemark());
            if(StringUtils.isNotBlank(incomeStatementH.getGsishFj())){
               String gsishFj = incomeStatementH.getGsishFj();
               List<String> strings = Arrays.asList(gsishFj.split(","));
               List<String> stringList = strings.stream().map(s -> url.concat(s)).collect(Collectors.toList());
               outData.setImgs(stringList);
            }
         }

         List defineApproveList;
         if ("2".equals(wfStatus)) {
            defineApproveList = this.wfDefineProcessMapper.selectApproveUser(record.getClient(), record.getWfDefineCode(), record.getWfSite());
            Iterator<GetWfDefineProcessOutData> defineApproveIt = defineApproveList.iterator();
            outData.setCurrentOperation("2");

//            while(defineApproveIt.hasNext()) {
//               GetWfDefineProcessOutData process = defineApproveIt.next();
//               if (process.getWfSeq().equals(record.getWfSeq())) {
//                  List<GetWfDefineUserOutData> userOutDataList =  process.getApproveUserOutData();
//                  for(GetWfDefineUserOutData userOutData : userOutDataList){
//                     String userId = userOutData.getUserId();
//                     if (process.getWfSeq().equals(record.getWfSeq()) && StringUtils.equals(userId, inData.getUserId())) {
//                        outData.setCurrentOperation("2");
//                        break;
//                     }
////                     if(StringUtils.equalsIgnoreCase(userId, inData.getUserId())){
////                        String userAuthType = userOutData.getUserAuthType();
////                        if(!StringUtils.equalsIgnoreCase("SD", userAuthType)){
////                           outData.setCurrentOperation("2");
////                           break;
////                        }else if(StringUtils.equalsIgnoreCase("SD", userAuthType) && StringUtils.equalsIgnoreCase(inData.getDepId(), userOutData.getUserAuthSite())){
////                           outData.setCurrentOperation("2");
////                           break;
////                        }
////                     }
//                  }
//               }
//            }

//            while(defineApproveIt.hasNext()) {
//               GetWfDefineProcessOutData process = defineApproveIt.next();
//               if (process.getWfSeq().equals(record.getWfSeq()) && process.getApproveUserOutData().contains(inData.getUserId())) {
//                  outData.setCurrentOperation("2");
//                  break;
//               }
//            }
         }

         defineApproveList = wfDefineProcessMapper.selectDefineApproveList(inData.getClient(), record.getWfDefineCode());
         List<GetWorkflowApproveOutData> approvedList = this.wfRecordHistoryMapper.selectApprovedList(inData.getClient(), record.getWfCode());
         Map<BigDecimal, GetWorkflowApproveOutData> approveMap = new HashMap();
         Iterator<GetWorkflowApproveOutData> approvedIterator = approvedList.iterator();

         GetWorkflowApproveOutData approve;
         while(approvedIterator.hasNext()) {
            approve = approvedIterator.next();
            if (!ObjectUtil.isNotNull(approveMap.get(approve.getWfSeq()))) {
               approveMap.put(approve.getWfSeq(), approve);
            }
         }

         approvedIterator = defineApproveList.iterator();

         while(approvedIterator.hasNext()) {
            approve = approvedIterator.next();
            if (ObjectUtil.isNotNull(approveMap.get(approve.getWfSeq()))) {
               GetWorkflowApproveOutData approveOutData = approveMap.get(approve.getWfSeq());
               approve.setResult(approveOutData.getResult());
               approve.setMemo(approveOutData.getMemo());
               approve.setDesc(approve.getGroupName() + "(" + approveOutData.getGroupName() + ")" + approveOutData.getCreateTime() + ":" + approve.getMemo());
               approve.setCreateDate(approveOutData.getCreateTime());
               approve.setApproverName(approveOutData.getGroupName());
            } else {
               approve.setDesc(approve.getGroupName() + ":尚未审批");
            }

            if(StringUtils.isEmpty(approve.getCreateDate())){
               approve.setApproverName(StringUtils.EMPTY);
            }
         }

         GetWorkflowApproveOutData apply = new GetWorkflowApproveOutData();
         example = new Example(GaiaUserData.class);
         example.createCriteria().andEqualTo("client", record.getClient()).andEqualTo("userId", record.getCreateUser());
         GaiaUserData userData = this.userDataMapper.selectOneByExample(example);
         apply.setWfSeq(BigDecimal.ZERO);
         apply.setGroupName("发起人");
         if (ObjectUtil.isNotNull(userData)) {
            apply.setDesc(userData.getUserNam() + record.getCreateTime() + ":申请");
            apply.setApproverName(userData.getUserNam());
            apply.setCreateDate(record.getCreateTime());
         }

         defineApproveList.add(0, apply);
         outData.setApproveList(defineApproveList);
         if ("3".equals(wfStatus)) {
            outData.setWfSeq(defineApproveList.size());
         }

         return outData;
      }
   }

   @Override
   public List<GetWorkflowDetailListData> selectDetailList(GetWorkflowInData inData) {
      List<String> wfCodeList = inData.getWfCodeList();
      List<GetWorkflowDetailListData> resultList = Lists.newArrayList();
      for(String wfCode : wfCodeList){
         inData.setWfCode(wfCode);
         GetWorkflowOutData outData = this.selectOne(inData);
         String wfDefineCode = outData.getDefineCode();
         //打印预览【目前仅支持天一医药连锁的三个工作流打印预览】
         if (WorkflowTypeEnum.GAIA_WF_032.code.equals(wfDefineCode) || WorkflowTypeEnum.GAIA_WF_045.code.equals(wfDefineCode)) {
            List<PrintSupplierApprovalOutData> listDataList = JSONArray.parseArray(outData.getWfJsonString(), PrintSupplierApprovalOutData.class);
            List<GetWorkflowApproveOutData> approveDataList = outData.getApproveList();
            List<GetSampleWorkflowApproveOutData> sampleWorkflowApproveOutDataList = Lists.newArrayList();
            for (GetWorkflowApproveOutData approveOutData : approveDataList) {
               GetSampleWorkflowApproveOutData sampleWorkflowApproveOutData = new GetSampleWorkflowApproveOutData(approveOutData.getWfSeq(), approveOutData.getApproverName(), approveOutData.getCreateDate());
               sampleWorkflowApproveOutDataList.add(sampleWorkflowApproveOutData);
            }
            GetWorkflowDetailListData detailListData = new GetWorkflowDetailListData(wfCode, outData.getWfTitle(), listDataList, sampleWorkflowApproveOutDataList);
            resultList.add(detailListData);
         } else if (WorkflowTypeEnum.GAIA_WF_043.code.equals(wfDefineCode)) {
            List<SupplierPaymentWfData> listDataList = JSONArray.parseArray(outData.getWfJsonString(), SupplierPaymentWfData.class);
            List<GetWorkflowApproveOutData> approveDataList = outData.getApproveList();
            List<GetSampleWorkflowApproveOutData> sampleWorkflowApproveOutDataList = Lists.newArrayList();
            for (GetWorkflowApproveOutData approveOutData : approveDataList) {
               GetSampleWorkflowApproveOutData sampleWorkflowApproveOutData = new GetSampleWorkflowApproveOutData(approveOutData.getWfSeq(), approveOutData.getApproverName(), approveOutData.getCreateDate());
               sampleWorkflowApproveOutDataList.add(sampleWorkflowApproveOutData);
            }
            GetWorkflowDetailListData detailListData = new GetWorkflowDetailListData(wfCode, outData.getWfTitle(), listDataList, sampleWorkflowApproveOutDataList);
            resultList.add(detailListData);
         }
      }

      return resultList;
   }


   private int autoApproveProcess(GaiaWfRecord wfRecord, List<GetWorkflowApproveOutData> processList, GaiaWfRecordHistory history, BigDecimal index){
      String createUserId = wfRecord.getCreateUser();
      int insertFlag = 0;

      BigDecimal maxIndex = CollectionUtil.getLast(processList).getWfSeq();
      Iterator<GetWorkflowApproveOutData> processIt = processList.iterator();
      while (processIt.hasNext()) {
         GetWorkflowApproveOutData approveOutData = processIt.next();
         String approverUserId = approveOutData.getApproverUserId();
         //节点是发起人，自动审批
         if(StringUtils.equals(createUserId, approverUserId)){
            history = new GaiaWfRecordHistory();
            history.setClient(wfRecord.getClient());
            history.setPoCompanyCode(wfRecord.getPoCompanyCode());
            history.setWfCode(wfRecord.getWfCode());
            history.setWfHistorySeq(index);
            history.setWfDefineCode(wfRecord.getWfDefineCode());
            history.setWfKind(wfRecord.getWfKind());
            history.setWfSeq(index);
            history.setUserId(createUserId);
            history.setResult("PASS");
            history.setMemo("同意");
            history.setRejectSeq(wfRecord.getWfSeq());
            history.setCreateUser(createUserId);
            history.setCreateTime(DateUtil.format(new Date(), "yyyyMMdd"));
            insertFlag = wfRecordHistoryMapper.insert(history);

            //已到最后一个节点
            if(index.compareTo(maxIndex) >= 0){
               wfRecord.setWfStatus("3");
            }

            if(insertFlag == 1){
               index = index.add(BigDecimal.ONE);
               history.setRejectSeq(index);
               wfRecord.setWfSeq(index);
            }

            wfRecordMapper.updateByPrimaryKey(wfRecord);
         }
      }

      return insertFlag;
   }

   @Transactional(rollbackFor = Exception.class)
   @Override
   public void approve(GetWorkflowInData inData) {
      Example example = new Example(GaiaWfRecord.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wfCode", inData.getWfCode());
      GaiaWfRecord wfRecord = this.wfRecordMapper.selectOneByExample(example);

      if (ObjectUtil.isNull(wfRecord)) {
         throw new BusinessException("提示：工作流不存在");
      } else if (!"2".equals(wfRecord.getWfStatus())) {
         throw new BusinessException("提示：当前工作流审批状态不是审批中，不可审批");
      } else if (!"PASS".equals(inData.getResult()) && !"FAIL".equals(inData.getResult())) {
         throw new BusinessException("提示：审批结果不正确或不能为空");
      } else {
         GetDefineInData defineInData = new GetDefineInData();
         defineInData.setClient(wfRecord.getClient());
         defineInData.setDefineCode(wfRecord.getWfDefineCode());
         defineInData.setSite(wfRecord.getWfSite());
         defineInData.setUserId(inData.getUserId());
         defineInData.setSeq(wfRecord.getWfSeq());

         List<GetWfDefineProcessOutData> defineApproveList = this.wfDefineProcessMapper.checkApproveUser(defineInData);
         if(CollectionUtils.isEmpty(defineApproveList)){
            throw new BusinessException("无当前审批节点权限");
         }

//            while(defineApproveIt.hasNext()) {
//               GetWfDefineProcessOutData process = defineApproveIt.next();
//               if (process.getWfSeq().equals(record.getWfSeq())) {
//                  List<GetWfDefineUserOutData> userOutDataList =  process.getApproveUserOutData();
//                  for(GetWfDefineUserOutData userOutData : userOutDataList){
//                     String userId = userOutData.getUserId();
//                     if (process.getWfSeq().equals(record.getWfSeq()) && StringUtils.equals(userId, inData.getUserId())) {
//                        outData.setCurrentOperation("2");
//                        break;
//                     }
////                     if(StringUtils.equalsIgnoreCase(userId, inData.getUserId())){
////                        String userAuthType = userOutData.getUserAuthType();
////                        if(!StringUtils.equalsIgnoreCase("SD", userAuthType)){
////                           outData.setCurrentOperation("2");
////                           break;
////                        }else if(StringUtils.equalsIgnoreCase("SD", userAuthType) && StringUtils.equalsIgnoreCase(inData.getDepId(), userOutData.getUserAuthSite())){
////                           outData.setCurrentOperation("2");
////                           break;
////                        }
////                     }
//                  }
//               }
//            }

         example = new Example(GaiaWfRecordHistory.class);
         example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wfCode", inData.getWfCode());
         example.orderBy("wfHistorySeq").desc();
         List<GaiaWfRecordHistory> historyList = this.wfRecordHistoryMapper.selectByExample(example);
         BigDecimal index = BigDecimal.ONE;
         if (CollUtil.isNotEmpty(historyList)) {
            index = CollectionUtil.getFirst(historyList).getWfHistorySeq().add(BigDecimal.ONE);
         }

         GaiaWfRecordHistory history = new GaiaWfRecordHistory();
         String approveDate = inData.getApproveDate();
         history.setClient(wfRecord.getClient());
         history.setPoCompanyCode(wfRecord.getPoCompanyCode());
         history.setWfCode(wfRecord.getWfCode());
         history.setWfHistorySeq(index);
         history.setWfDefineCode(wfRecord.getWfDefineCode());
         history.setWfKind(wfRecord.getWfKind());
         history.setWfSeq(wfRecord.getWfSeq());
         history.setUserId(inData.getUserId());
         history.setResult(inData.getResult());
         history.setMemo(inData.getMemo());
         history.setRejectSeq(inData.getRejectSeq());
         history.setCreateUser(inData.getUserId());
         history.setCreateTime(StringUtils.isEmpty(approveDate) ? DateUtil.format(new Date(), "yyyyMMdd") : approveDate);

         if(StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_006") || StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_007") ){

         }

         String orderNo = wfRecord.getWfOrder();

         if ("PASS".equals(inData.getResult())) {
            List<GetWorkflowApproveOutData> processList = wfDefineProcessMapper.selectDefineApproveList(inData.getClient(), wfRecord.getWfDefineCode());
            BigDecimal maxIndex = CollectionUtil.getLast(processList).getWfSeq();
            if (wfRecord.getWfSeq().intValue() >= maxIndex.intValue()) {
               wfRecord.setWfStatus("3");
            } else {
               wfRecord.setWfSeq(wfRecord.getWfSeq().add(BigDecimal.ONE));
            }

            //自动审批
//            insertFlag = this.autoApproveProcess(wfRecord, processList, history, index);

            if (StrUtil.isBlank(history.getMemo())) {
               history.setMemo("同意");
            }

            if(StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_006") || StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_007") ){
               orderNo = wfRecord.getWfOrder();
            }
         }

         if ("FAIL".equals(inData.getResult())) {
            if (inData.getRejectSeq().intValue() == 0) {
               wfRecord.setWfStatus("1");
            }

            wfRecord.setWfSeq(inData.getRejectSeq());
            if(StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_006") || StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_007") ){
               orderNo = wfRecord.getWfOrder();
               wfRecord.setWfOrder(IdUtil.randomUUID());
            }
            //插入驳回消息
            //盘点审批、门店报损、门店报益、门店领用、门店退库
            //
            List<String> stringList=new ArrayList<>();
            //门店报损
            stringList.add("GAIA_WF_035");
            stringList.add("GAIA_WF_036");
            //门店领用
            stringList.add("GAIA_WF_037");
            stringList.add("GAIA_WF_038");
            //门店报益
            stringList.add("GAIA_WF_050");
            stringList.add("GAIA_WF_051");
            //盘点审批
            stringList.add("GAIA_WF_006");
            stringList.add("GAIA_WF_007");
            //门店退库
            stringList.add("GAIA_WSD_1002");
            stringList.add("GAIA_WSD_1003");
            stringList.add("GAIA_WSD_1004");
            if(stringList.contains(wfRecord.getWfDefineCode())){
               try {
                  insertMessage(inData.getClient(),wfRecord.getWfSite(),wfRecord.getWfKind(),wfRecord.getWfCode());
               } catch (Exception e) {
                  log.info("驳回消息插入异常：{}",e.getMessage());
               }
            }

         }


//         if(insertFlag == 0 && inData.getAutoApprove() == 0){
            this.wfRecordHistoryMapper.insert(history);
            this.wfRecordMapper.updateByPrimaryKey(wfRecord);
//         }

         log.info("审批工作流实体:{}", JSON.toJSONString(wfRecord));
         if ("1".equals(wfRecord.getWfStatus()) || "3".equals(wfRecord.getWfStatus())) {
            GetWfApproveInData approveInData = new GetWfApproveInData();
            approveInData.setWfStatus(wfRecord.getWfStatus());
            approveInData.setWfDefineCode(wfRecord.getWfDefineCode());
            approveInData.setClientId(wfRecord.getClient());
            if(StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_006") || StringUtils.equalsIgnoreCase(wfRecord.getWfDefineCode(), "GAIA_WF_007") ){
               approveInData.setWfOrder(orderNo);
            }else{
               approveInData.setWfOrder(wfRecord.getWfOrder());
            }

            approveInData.setCreateUser(wfRecord.getCreateUser());
            approveInData.setApproverUserId(inData.getUserId());
            approveInData.setMemo(inData.getMemo());
            JsonResult result = null;
            String defineCode = approveInData.getWfDefineCode();
            log.info("审批工作流回调请求实体:{}", JSON.toJSONString(approveInData));
            if ("GAIA_WF_005".equals(defineCode) || "GAIA_WF_006".equals(defineCode) || "GAIA_WF_007".equals(defineCode) || "GAIA_WF_008".equals(defineCode) ||
                "GAIA_WF_009".equals(defineCode) || "GAIA_WF_010".equals(defineCode) || "GAIA_WF_011".equals(defineCode) || "GAIA_WF_012".equals(defineCode) || "GAIA_WF_013".equals(defineCode) ||
                "GAIA_WF_035".equals(defineCode) || "GAIA_WF_036".equals(defineCode) || "GAIA_WF_037".equals(defineCode) || "GAIA_WF_038".equals(defineCode) || "GAIA_WF_050".equals(defineCode) || "GAIA_WF_051".equals(defineCode)) {
               approveInData.setSite(wfRecord.getWfSite());
               result = this.operationService.getApprovalResult(approveInData);
            } else if ("GAIA_WF_014".equals(defineCode) || "GAIA_WF_044".equals(defineCode) || "GAIA_WF_015".equals(defineCode) || "GAIA_WF_016".equals(defineCode) || "GAIA_WF_017".equals(defineCode) ||
                       "GAIA_WF_018".equals(defineCode) || "GAIA_WF_032".equals(defineCode)) {
               result = this.purchaseService.getApprovalResult(approveInData);
            } else if ("GAIA_WF_019".equals(defineCode) || "GAIA_WF_020".equals(defineCode) || "GAIA_WF_021".equals(defineCode) || "GAIA_WF_022".equals(defineCode) ||
                       "GAIA_WF_023".equals(defineCode) || "GAIA_WF_024".equals(defineCode) || "GAIA_WF_025".equals(defineCode) || "GAIA_WF_039".equals(defineCode) ||
                       "GAIA_WF_040".equals(defineCode)) {
               result = this.wmsService.getApprovalResult(approveInData);
            } else if ("GAIA_WSD_1001".equals(defineCode) || "GAIA_WSD_1003".equals(defineCode)) {
               result = this.storeAppService.getApprovalResult(approveInData);
            } else if ("GAIA_WSD_1002".equals(defineCode) || "GAIA_WSD_1004".equals(defineCode) || "GAIA_WF_034".equals(defineCode) || "GAIA_WF_033".equals(defineCode)) {
               result = this.storeWebService.getApprovalResult(approveInData);
            } else if("GAIA_WF_043".equals(defineCode)){
               result = this.operateService.payCallBack(approveInData);
            } else if("GAIA_WF_047".equals(defineCode)){
               result = this.operationService.checkTodoList(approveInData);
            } else if("GAIA_WF_046".equals(defineCode)){
               result = this.operationService.sendLossDefecpro(approveInData);
            } else if("GAIA_WF_045".equals(defineCode)){
               result = this.operateService.approvePayment(approveInData);
            } else if("GAIA_WF_041".equals(defineCode) || "GAIA_WF_048".equals(defineCode) || "GAIA_WF_049".equals(defineCode) ){
               result = this.purchaseService.getApprovalResult(approveInData);
            } else if("GAIA_WF_052".equals(defineCode)){
               approveInData.setSite(wfRecord.getWfSite());
               result = this.operationService.orderCommit(approveInData);
            }

            log.info("审批工作流回调:{}", JSON.toJSONString(result));
            if (ObjectUtil.isNull(result)) {
               throw new BusinessException("服务异常");
            }

            if (result.getCode() != 0) {
               throw new BusinessException(result.getMessage());
            }
         }

      }
   }
   //插入消息表
   private void insertMessage(String client, String stoCode, String title,String code) {
      String voucherIdWeb = gaiaSdMessageMapper.selectNextVoucherId(client, "store");
      GaiaSdMessage sdMessageWeb = new GaiaSdMessage();
      sdMessageWeb.setClient(client);
      sdMessageWeb.setGsmId(stoCode);
      sdMessageWeb.setGsmType(SdMessageTypeEnum.REJECT_REMINDER.code);
      sdMessageWeb.setGsmPage(SdMessageTypeEnum.REJECT_REMINDER.page );
      sdMessageWeb.setGsmPlatForm("WEB");
      sdMessageWeb.setGsmDeleteFlag("0");
      sdMessageWeb.setGsmVoucherId(voucherIdWeb);
      sdMessageWeb.setGsmArriveDate(DateUtil.format(DateUtil.date(), "yyyyMMdd"));
      sdMessageWeb.setGsmArriveTime(DateUtil.format(DateUtil.date(), "HHmmss"));
      sdMessageWeb.setGsmRemark("您有" + "<font color='red'>" +title+",工作流单号:"+code+"</font>" + "被驳回,请及时查看并处理!");
      sdMessageWeb.setGsmFlag("N");
      sdMessageWeb.setGsmWarningDay(stoCode);
      gaiaSdMessageMapper.insertSelective(sdMessageWeb);

      String voucherIdFx = gaiaSdMessageMapper.selectNextVoucherIdFX(client, "store");
      GaiaSdMessage sdMessageFx = new GaiaSdMessage();
      sdMessageFx.setClient(client);
      sdMessageFx.setGsmId(stoCode);
      sdMessageFx.setGsmType(SdMessageTypeEnum.REJECT_REMINDER.code);
      sdMessageFx.setGsmPage(SdMessageTypeEnum.REJECT_REMINDER.page);
      sdMessageFx.setGsmPlatForm("FX");
      sdMessageFx.setGsmDeleteFlag("0");
      sdMessageFx.setGsmVoucherId(voucherIdFx);
      sdMessageFx.setGsmArriveDate(DateUtil.format(DateUtil.date(), "yyyyMMdd"));
      sdMessageFx.setGsmArriveTime(DateUtil.format(DateUtil.date(), "HHmmss"));
      sdMessageFx.setGsmRemark("您有" + title +",工作流单号:"+code+ "被驳回,请及时查看并处理!");
      sdMessageFx.setGsmFlag("N");
      //sdMessageFx.setGsmValue("storeId=" + stoCode + "&conserveMonth=" + DateUtil.format(DateUtil.date(), "yyyyMM"));
      sdMessageFx.setGsmWarningDay(stoCode);
      gaiaSdMessageMapper.insertSelective(sdMessageFx);
   }

   @Transactional(rollbackFor = Exception.class)
   @Override
   public void cc(GetWorkflowInData inData) {
      Example example = new Example(GaiaWfRecord.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wfCode", inData.getWfCode());
      GaiaWfRecord wfRecord = (GaiaWfRecord)this.wfRecordMapper.selectOneByExample(example);
      if (ObjectUtil.isNull(wfRecord)) {
         throw new BusinessException("提示：工作流不存在");
      } else {
         example = new Example(GaiaWfRecordHistory.class);
         example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wfCode", inData.getWfCode());
         example.orderBy("wfHistorySeq").desc();
         List<GaiaWfRecordHistory> historyList = this.wfRecordHistoryMapper.selectByExample(example);
         BigDecimal index = new BigDecimal("1");
         if (CollUtil.isNotEmpty(historyList)) {
            index = ((GaiaWfRecordHistory)historyList.get(0)).getWfHistorySeq().add(new BigDecimal("1"));
         }

         GaiaWfRecordHistory history = new GaiaWfRecordHistory();
         history.setClient(wfRecord.getClient());
         history.setPoCompanyCode(wfRecord.getPoCompanyCode());
         history.setWfCode(wfRecord.getWfCode());
         history.setWfHistorySeq(index);
         history.setWfDefineCode(wfRecord.getWfDefineCode());
         history.setWfKind(wfRecord.getWfKind());
         history.setWfSeq(new BigDecimal("-1"));
         history.setUserId(inData.getCc());
         history.setResult("PASS");
         history.setMemo("同意");
         history.setCreateUser(inData.getUserId());
         history.setCreateTime(DateUtil.format(new Date(), "yyyyMMdd"));
         this.wfRecordHistoryMapper.insert(history);
      }
   }

   @Transactional
   @Override
   public void cancel(GetWorkflowInData inData) {
      Example example = new Example(GaiaWfRecord.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wfCode", inData.getWfCode());
      GaiaWfRecord wfRecord = (GaiaWfRecord)this.wfRecordMapper.selectOneByExample(example);
      if (ObjectUtil.isNull(wfRecord)) {
         throw new BusinessException("提示：工作流不存在");
      } else if (!"1".equals(wfRecord.getWfStatus())) {
         throw new BusinessException("提示：只能取消待提交状态的工作流");
      } else if (!inData.getUserId().equals(wfRecord.getCreateUser())) {
         throw new BusinessException("提示：只能取消自己提交的工作流");
      } else {
         wfRecord.setWfStatus("0");
         this.wfRecordMapper.updateByPrimaryKey(wfRecord);
      }
   }

   @Override
   public List<GetWorkflowApproveOutData> selectWorkflowApproveProcess(GetWfCreateInData inData) {
      Example example = new Example(GaiaWfRecord.class);
      example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wfDefineCode", inData.getWfDefineCode()).andEqualTo("wfOrder", inData.getWfOrder());
      GaiaWfRecord record = this.wfRecordMapper.selectOneByExample(example);
      if (ObjectUtil.isNull(record)) {
         throw new BusinessException("提示：工作流不存在");
      } else {
         List<GetWorkflowApproveOutData> approvedList = this.wfRecordHistoryMapper.selectApprovedList(inData.getClientId(), record.getWfCode());
         GetWorkflowApproveOutData apply = new GetWorkflowApproveOutData();
         example = new Example(GaiaUserData.class);
         example.createCriteria().andEqualTo("client", record.getClient()).andEqualTo("userId", record.getCreateUser());
         GaiaUserData userData = this.userDataMapper.selectOneByExample(example);
         if (ObjectUtil.isNotNull(userData)) {
            apply.setGroupName(userData.getUserNam());
         }

         if(CollectionUtil.isNotEmpty(approvedList)){
            GetWorkflowApproveOutData approveOutData = CollectionUtil.getFirst(approvedList);
            apply.setResult(approveOutData.getResult());
            apply.setMemo(approveOutData.getMemo());
         }

         approvedList.add(apply);
         return approvedList;
      }
   }

   @Override
   public List<GetWorkflowOutData> getByClientAndTitle(GetWorkflowInData inData) {
      return wfRecordMapper.getByClientAndTitle(inData);
   }

   @Override
   public List<GetStoDepOutData> getStoDepList(GetStoDepInData inData) {
      return userDataMapper.getStoDepList(inData);
   }

}
