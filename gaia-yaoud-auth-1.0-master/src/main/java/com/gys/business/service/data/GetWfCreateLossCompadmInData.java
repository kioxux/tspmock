package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateLossCompadmInData {
   @Field(
      name = "损溢时间"
   )
   @JSONField(
      ordinal = 1
   )
   private String lossDate = "";
   @Field(
      name = "损溢单号"
   )
   @JSONField(
      ordinal = 2
   )
   private String lossOrder = "";
   @Field(
      name = "门店"
   )
   @JSONField(
      ordinal = 3
   )
   private String storeId = "";
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 4
   )
   private String proCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 5
   )
   private String proName = "";
   @Field(
      name = "规格/单位"
   )
   @JSONField(
      ordinal = 6
   )
   private String proSpecs = "";
   @Field(
      name = "批次"
   )
   @JSONField(
      ordinal = 7
   )
   private String batch = "";
   @Field(
      name = "批次成本"
   )
   @JSONField(
      ordinal = 8
   )
   private String batchCost = "";
   @Field(
      name = "税额"
   )
   @JSONField(
      ordinal = 9
   )
   private String tax = "";
   @Field(
      name = "加权移动平均成本"
   )
   @JSONField(
      ordinal = 10
   )
   private String wmaCost = "";
   @Field(
      name = "加权移动平均成本税额"
   )
   @JSONField(
      ordinal = 11
   )
   private String wmaCostTax = "";
   @Field(
      name = "报损/报溢数量"
   )
   @JSONField(
      ordinal = 12
   )
   private String qty = "";
   @Field(
      name = "原因"
   )
   @JSONField(
      ordinal = 13
   )
   private String reason = "";
   @Field(
      name = "生产厂家"
   )
   @JSONField(
      ordinal = 14
   )
   private String sccj = "";
}
