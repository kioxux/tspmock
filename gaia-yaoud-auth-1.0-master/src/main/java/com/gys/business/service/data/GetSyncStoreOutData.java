package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetSyncStoreOutData {
   private String client;
   private String stoCode;
   private String stoName;
   private String stoPym;
   private String stoShortName;
   private String stoAttribute;
   private String stoDeliveryMode;
   private String stoRelationStore;
   private String stoStatus;
   private String stoArea;
   private String stoOpenDate;
   private String stoCloseDate;
   private String stoAdd;
   private String stoProvince;
   private String stoCity;
   private String stoDistrict;
   private String stoIfMedicalcare;
   private String stoIfDtp;
   private String stoTaxClass;
   private String stoDeliveryCompany;
   private String stoChainHead;
   private String stoTaxSubject;
   private String stoTaxRate;
   private String stoNo;
   private String stoLegalPerson;
   private String stoQua;
   private String stoCreDate;
   private String stoCreTime;
   private String stoCreId;
   private String stoModiDate;
   private String stoModiTime;
   private String stoModiId;
   private String stoLogo;
   private String stogCode;
   private String stoLeader;
}
