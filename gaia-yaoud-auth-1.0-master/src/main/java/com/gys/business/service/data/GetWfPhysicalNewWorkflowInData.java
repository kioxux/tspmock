package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetWfPhysicalNewWorkflowInData {
    @ApiModelProperty(value = "盘点单号")
    private String voucherId;

    @ApiModelProperty(value = "盘点单创建日期")
    private String inventoryDate;

    @ApiModelProperty(value = "盘点单类型")
    private String inventoryType;

    @ApiModelProperty(value = "品项 盘点总数")
    private BigDecimal itemInventoryTotal;

    @ApiModelProperty(value = "品项 盘盈数")
    private BigDecimal itemInventoryProfit;

    @ApiModelProperty(value = "品项 盘亏数")
    private BigDecimal itemInventoryLosses;

    @ApiModelProperty(value = "品项 差异合计(绝对值)")
    private BigDecimal itemAbsoluteDiff;

    @ApiModelProperty(value = "品项 绝对差异率")
    private String itemAbsoluteDiffRatio;

    @ApiModelProperty(value = "品项 差异合计(相对值)")
    private BigDecimal itemRelativeDiff;

    @ApiModelProperty(value = "品项 相对差异率")
    private String itemRelativeDiffRatio;

    @ApiModelProperty(value = "批号行数 盘点总数")
    private String batchInventoryTotal;

    @ApiModelProperty(value = "批号行数 盘盈数")
    private BigDecimal batchInventoryProfit;

    @ApiModelProperty(value = "批号行数 盘亏数")
    private BigDecimal batchInventoryLosses;

    @ApiModelProperty(value = "批号行数 差异合计(绝对值)")
    private BigDecimal batchAbsoluteDiff;

    @ApiModelProperty(value = "批号行数 绝对差异率")
    private String batchAbsoluteDiffRatio;

    @ApiModelProperty(value = "批号行数 差异合计(相对值)")
    private BigDecimal batchRelativeDiff;

    @ApiModelProperty(value = "批号行数 相对差异率")
    private String batchRelativeDiffRatio;

    @ApiModelProperty(value = "数量 盘点总数")
    private String numInventoryTotal;

    @ApiModelProperty(value = "数量 盘盈数")
    private BigDecimal numInventoryProfit;

    @ApiModelProperty(value = "数量 盘亏数")
    private BigDecimal numInventoryLosses;

    @ApiModelProperty(value = "数量 差异合计(绝对值)")
    private BigDecimal numAbsoluteDiff;

    @ApiModelProperty(value = "数量 绝对差异率")
    private String numAbsoluteDiffRatio;

    @ApiModelProperty(value = "数量 差异合计(相对值)")
    private BigDecimal numRelativeDiff;

    @ApiModelProperty(value = "数量 相对差异率")
    private String numRelativeDiffRatio;

    @ApiModelProperty(value = "成本额 盘点总数")
    private String costInventoryTotal;

    @ApiModelProperty(value = "成本额 盘盈数")
    private String costInventoryProfit;

    @ApiModelProperty(value = "成本额 盘亏数")
    private String costInventoryLosses;

    @ApiModelProperty(value = "成本额 差异合计(绝对值)")
    private BigDecimal costAbsoluteDiff;

    @ApiModelProperty(value = "成本额 绝对差异率")
    private String costAbsoluteDiffRatio;

    @ApiModelProperty(value = "成本额 差异合计(相对值)")
    private BigDecimal costRelativeDiff;

    @ApiModelProperty(value = "成本额 相对差异率")
    private String costRelativeDiffRatio;

    @ApiModelProperty(value = "零售额 盘点总数")
    private String retailInventoryTotal;

    @ApiModelProperty(value = "零售额 盘盈数")
    private String retailInventoryProfit;

    @ApiModelProperty(value = "零售额 盘亏数")
    private String retailInventoryLosses;

    @ApiModelProperty(value = "零售额 差异合计(绝对值)")
    private BigDecimal retailAbsoluteDiff;

    @ApiModelProperty(value = "零售额 绝对差异率")
    private String retailAbsoluteDiffRatio;

    @ApiModelProperty(value = "零售额 差异合计(相对值)")
    private BigDecimal retailRelativeDiff;

    @ApiModelProperty(value = "零售额 相对差异率")
    private String retailRelativeDiffRatio;

    @ApiModelProperty(value = "是否批号盘点")
    private String isBatch;
}
