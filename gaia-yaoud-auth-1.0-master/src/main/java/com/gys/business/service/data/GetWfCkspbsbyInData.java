package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCkspbsbyInData {
    @Field(
            name = "序号"
    )
    @JSONField(
            ordinal = 1
    )
    private String wsdXh;

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 2
    )
    private String wsdSpBm;

    @Field(
            name = "名称"
    )
    @JSONField(
            ordinal = 3
    )
    private String wsdSpMc;

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 4
    )
    private String wsdSpGg;

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 5
    )
    private String wsdSpCj;

    @Field(
            name = "单位"
    )
    @JSONField(
            ordinal = 6
    )
    private String wsdDw;

    @Field(
            name = "批号"
    )
    @JSONField(
            ordinal = 7
    )
    private String wsdPh;

    @Field(
            name = "调整数量"
    )
    @JSONField(
            ordinal = 8
    )
    private String wsdTzsl;

    @Field(
            name = "参考成本价"
    )
    @JSONField(
            ordinal = 9
    )
    private String wsdCkcbj;

    @Field(
            name = "备注"
    )
    @JSONField(
            ordinal = 10
    )
    private String wsdBz;
}
