package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class AuthInData {
   private String userId;
   private List<String> userList;
   private String userName;
   private String site;
   private String client;
   private List<String> groupList;
   private String type;
}
