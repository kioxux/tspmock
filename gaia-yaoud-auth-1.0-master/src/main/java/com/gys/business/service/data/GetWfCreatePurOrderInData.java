package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreatePurOrderInData {
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 1
   )
   private String proCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String proName = "";
   @Field(
      name = "规格/单位"
   )
   @JSONField(
      ordinal = 3
   )
   private String proSpecs = "";
   @Field(
      name = "前五次采购价"
   )
   @JSONField(
      ordinal = 4
   )
   private String priceAverage = "";
   @Field(
      name = "实际采购价"
   )
   @JSONField(
      ordinal = 5
   )
   private String priceReal = "";
   @Field(
      name = "警戒项"
   )
   @JSONField(
      ordinal = 6
   )
   private String warnValue = "";
   @Field(
      name = "存销比"
   )
   @JSONField(
      ordinal = 7
   )
   private String stockUseRate = "";
}
