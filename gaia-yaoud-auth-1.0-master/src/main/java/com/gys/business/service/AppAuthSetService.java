package com.gys.business.service;

import com.gys.common.data.AppPermissionVO;
import com.gys.common.data.GetLoginOutData;

/**
 * @Description 权限设置
 * @Author huxinxin
 * @Date 2021/6/15 10:27
 * @Version 1.0.0
 **/
public interface AppAuthSetService {
    AppPermissionVO getBusinessPermission(GetLoginOutData loginUser);

}

