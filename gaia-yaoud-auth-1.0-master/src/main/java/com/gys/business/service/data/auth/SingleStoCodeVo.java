package com.gys.business.service.data.auth;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月23日 下午4:00
 */
@Data
public class SingleStoCodeVo implements Serializable {
    @NotBlank(message = "门店编码不能为空")
    private String stoCode;
}

