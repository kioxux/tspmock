package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateCategoryAnalysisInData {
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 1
   )
   private String proCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String proName = "";
   @Field(
      name = "规格单位"
   )
   @JSONField(
      ordinal = 3
   )
   private String proSpecs = "";
   @Field(
      name = "采购计划"
   )
   @JSONField(
      ordinal = 4
   )
   private String purchaseQty = "";
   @Field(
      name = "实际采购数量"
   )
   @JSONField(
      ordinal = 5
   )
   private String actualPurchaseQty = "";
   @Field(
      name = "完成度"
   )
   @JSONField(
      ordinal = 6
   )
   private String compDegree = "";
}
