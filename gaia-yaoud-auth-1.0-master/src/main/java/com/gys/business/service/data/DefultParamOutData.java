package com.gys.business.service.data;

import lombok.Data;

@Data
public class DefultParamOutData {
   private String gsspId;
   private String gsspName;
   private String gsspParaRemark;
   private String gsspPara;
}
