package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaZzDataMapper;
import com.gys.business.mapper.GaiaZzooDataMapper;
import com.gys.business.mapper.entity.GaiaZzData;
import com.gys.business.service.ZzService;
import com.gys.business.service.data.ZzInData;
import com.gys.business.service.data.ZzOutData;
import com.gys.business.service.data.ZzooOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gys.util.Util;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

@Service
public class ZzServiceImpl implements ZzService {
   @Autowired
   private GaiaZzDataMapper zzDataMapper;
   @Autowired
   private GaiaZzooDataMapper zzooDataMapper;

   public PageInfo<ZzOutData> getZzList(ZzInData inData) {
//      if (StringUtils.isEmpty(inData.getZzOrgid())|| StringUtils.isEmpty(inData.getZzType())){
//         throw new BusinessException("请选择组织");
//      }
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<ZzOutData> outData = this.zzDataMapper.getZzList(inData);
      PageInfo pageInfo;
      if (ObjectUtil.isNotEmpty(outData)) {
         pageInfo = new PageInfo(outData);
      } else {
         pageInfo = new PageInfo();
      }

      return pageInfo;
   }

   public ZzOutData getZzById(ZzInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaZzData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("zzOrgid", inData.getZzOrgid()).andEqualTo("zzId", inData.getZzId());
      GaiaZzData zzData = (GaiaZzData)this.zzDataMapper.selectOneByExample(example);
      ZzOutData outData = new ZzOutData();
      if (ObjectUtil.isNotEmpty(zzData)) {
         outData.setClient(zzData.getClient());
         outData.setZzId(zzData.getZzId());
         outData.setZzName(zzData.getZzName());
         outData.setZzNo(zzData.getZzNo());
         outData.setZzOrgid(zzData.getZzOrgid());
         outData.setZzOrgname(zzData.getZzOrgname());
         outData.setZzSdate(DateUtil.format(DateUtil.parse(zzData.getZzSdate(), "yyyyMMdd"), "yyyy-MM-dd"));
         outData.setZzEdate(DateUtil.format(DateUtil.parse(zzData.getZzEdate(), "yyyyMMdd"), "yyyy-MM-dd"));
      }

      return outData;
   }

   @Transactional
   public void insertZz(ZzInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaZzData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("zzOrgid", inData.getZzOrgid()).andEqualTo("zzId", inData.getZzId());
      GaiaZzData zzData = (GaiaZzData)this.zzDataMapper.selectOneByExample(example);
      if (ObjectUtil.isNotEmpty(zzData)) {
         throw new BusinessException("该证照已存在，不要重复添加！");
      } else {
         zzData = new GaiaZzData();
         zzData.setClient(userInfo.getClient());
         zzData.setZzId(inData.getZzId());
         zzData.setZzName(inData.getZzName());
         zzData.setZzNo(inData.getZzNo());
         zzData.setZzOrgid(inData.getZzOrgid());
         zzData.setZzOrgname(inData.getZzOrgname());
         zzData.setZzUserId(inData.getZzUserId());
         zzData.setZzUserName(inData.getZzUserName());
         zzData.setZzSdate(DateUtil.format(DateUtil.parse(inData.getZzSdate(), "yyyy-MM-dd"), "yyyyMMdd"));
         zzData.setZzEdate(DateUtil.format(DateUtil.parse(inData.getZzEdate(), "yyyy-MM-dd"), "yyyyMMdd"));
         zzData.setZzCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         zzData.setZzCreTime(DateUtil.format(new Date(), "HHmmss"));
         zzData.setZzCreId(userInfo.getUserId());
         zzData.setZzModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         zzData.setZzModiTime(DateUtil.format(new Date(), "HHmmss"));
         zzData.setZzModiId(userInfo.getUserId());
         this.zzDataMapper.insert(zzData);
      }
   }

   @Transactional
   public void updateZz(ZzInData inData, GetLoginOutData userInfo) {

      GaiaZzData zzData = new GaiaZzData();
      //如果这个id存在 是修改
      if(ObjectUtil.isEmpty(inData.getId())){
         //客户要求可以重复
//         Example example = new Example(GaiaZzData.class);
//         example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("zzOrgid", inData.getZzOrgid()).andEqualTo("zzId", inData.getZzId());
//         GaiaZzData zzOutData = (GaiaZzData)this.zzDataMapper.selectOneByExample(example);
//         if (ObjectUtil.isNotEmpty(zzOutData)){
//            throw new BusinessException(inData.getZzOrgname()+"已存在"+inData.getZzName()+"，不要重复添加！");
//         }
         zzData.setId(Util.generateUUID());
         zzData.setClient(userInfo.getClient());
         zzData.setZzId(inData.getZzId());
         zzData.setZzName(inData.getZzName());
         zzData.setZzNo(inData.getZzNo());
         zzData.setZzOrgid(inData.getZzOrgid());
         zzData.setZzOrgname(inData.getZzOrgname());
         zzData.setZzUserId(inData.getZzUserId());
         zzData.setZzUserName(inData.getZzUserName());
         zzData.setZzCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         zzData.setZzCreTime(DateUtil.format(new Date(), "HHmmss"));
         zzData.setZzCreId(userInfo.getUserId());
         zzData.setZzSdate(DateUtil.format(DateUtil.parse(inData.getZzSdate(), "yyyy-MM-dd"), "yyyyMMdd"));
         zzData.setZzEdate(DateUtil.format(DateUtil.parse(inData.getZzEdate(), "yyyy-MM-dd"), "yyyyMMdd"));
         zzData.setZzModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         zzData.setZzModiTime(DateUtil.format(new Date(), "HHmmss"));
         zzData.setZzModiId(userInfo.getUserId());
         this.zzDataMapper.insertSelective(zzData);
      }else {

         zzData.setId(inData.getId());
         zzData.setZzName(inData.getZzName());
         zzData.setZzNo(inData.getZzNo());
         zzData.setZzId(inData.getZzId());
         zzData.setZzUserId(inData.getZzUserId());
         zzData.setZzUserName(inData.getZzUserName());
         zzData.setZzSdate(DateUtil.format(DateUtil.parse(inData.getZzSdate(), "yyyy-MM-dd"), "yyyyMMdd"));
         zzData.setZzEdate(DateUtil.format(DateUtil.parse(inData.getZzEdate(), "yyyy-MM-dd"), "yyyyMMdd"));
         zzData.setZzModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         zzData.setZzModiTime(DateUtil.format(new Date(), "HHmmss"));
         zzData.setZzModiId(userInfo.getUserId());
         this.zzDataMapper.updateByPrimaryKeySelective(zzData);
      }


   }

   @Transactional
   public void deleteZz(ZzInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaZzData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andIn("zzOrgid", inData.getOrgIds()).andIn("zzId", inData.getIds());
      this.zzDataMapper.deleteByExample(example);
   }

   public List<ZzooOutData> getZzooList() {
      List<ZzooOutData> outData = this.zzooDataMapper.getZzooList();
      if (ObjectUtil.isEmpty(outData)) {
         outData = new ArrayList();
      }

      return (List)outData;
   }
}
