package com.gys.business.service;

import com.gys.business.service.data.JyfwInData;
import com.gys.business.service.data.JyfwOutData;
import com.gys.business.service.data.JyfwooOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface JyfwService {
   PageInfo<JyfwOutData> getJyfwList(JyfwInData inData);

   JyfwOutData getJyfwById(JyfwInData inData, GetLoginOutData userInfo);

   void insertJyfw(JyfwInData inData, GetLoginOutData userInfo);

   void updateJyfw(JyfwInData inData, GetLoginOutData userInfo);

   void deleteJyfw(JyfwInData inData, GetLoginOutData userInfo);

   List<JyfwooOutData> getJyfwooList();
}
