package com.gys.business.service.data;

import lombok.Data;

@Data
public class ZzOutData {
   private String id;
   private String client;
   private String zzOrgid;
   private String zzOrgname;
   private String zzId;
   private String zzName;
   private String zzUserId;
   private String zzUserName;
   private String zzNo;
   private String zzSdate;
   private String zzEdate;
   private String zzModiName;
}
