package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.LoginService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.auth.AuthTree;
import com.gys.common.data.*;
import com.gys.common.enums.ClientParmaEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {
    @Resource
    private GaiaUserDataMapper userDataMapper;

    @Resource
    private GaiaAuthconfiDataMapper authconfiDataMapper;

    @Resource
    private GaiaResourceMapper resourceMapper;

    @Resource
    private GaiaFranchiseeMapper franchiseeMapper;

    @Resource
    private GaiaStoreDataMapper storeDataMapper;

    @Resource
    private ThirdParaService thirdParaService;

    @Resource
    private RedisManager redisManager;

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    private GaiaUserLoginInfoMapper userLoginInfoMapper;
    @Resource
    private ClientParamMapper clientParamMapper;

    @Value("${yaodeAseKey:1234567890123456}")
    private String ydKey;

    private void insertUserLoginInfo(GetLoginOutData loginData, int loginModel){
        GaiaUserLoginInfo userLoginInfo = new GaiaUserLoginInfo();
        try {
            userLoginInfo.setClient(loginData.getClient());
            userLoginInfo.setBrId(loginData.getDepId());
            userLoginInfo.setUserName(loginData.getUserId());
            userLoginInfo.setLoginModel(loginModel);
            userLoginInfo.setLoginTime(new Date());
            userLoginInfoMapper.insert(userLoginInfo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("登录记录error:{}", JSON.toJSONString(userLoginInfo));
        }
    }

    @Override
    public GetLoginOutData login(GetLoginInData indata) {
        GetLoginOutData outData = new GetLoginOutData();
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("userTel", indata.getUsername());
        List<GaiaUserData> userList = this.userDataMapper.selectByExample(example);
        Iterator<GaiaUserData> userDataIterator = userList.iterator();

        GaiaUserData user;
        do {
            if (!userDataIterator.hasNext()) {
                throw new BusinessException("提示：密码不正确");
            }

            user = userDataIterator.next();
        } while (!user.getUserPassword().equals(indata.getPassword()));

        if ("1".equals(user.getUserSta())) {
            throw new BusinessException("提示：员工已离职");
        } else if ("2".equals(user.getUserSta())) {
            throw new BusinessException("提示：账号已停用");
        } else {
            outData.setLoginAccount(user.getUserTel());
            outData.setUserId(user.getUserId());
            outData.setLoginName(user.getUserNam());
            outData.setClient(user.getClient());
//         outData.setDepId(user.getDepId());
            outData.setDepName(user.getDepName());
            outData.setUserAddr(user.getUserAddr());
            outData.setUserIdc(user.getUserIdc());
            outData.setUserSex(user.getUserSex());
            outData.setUserSta(user.getUserSta());
            outData.setUserTel(user.getUserTel());
            outData.setUserYsId(user.getUserYsId());
            outData.setUserYsZyfw(user.getUserYsZyfw());
            outData.setUserYsZylb(user.getUserYsZylb());
            outData.setUserLoginSta(user.getUserLoginSta());
            outData.setDepId(user.getDepId());
            outData.setClientName(getClientName(outData.getClient()));
            outData.setShowName(outData.getClientName());
            ClientParam clientParam = getStoreSelectLimitParam(outData.getClient());
            int selectParam1 = clientParam != null && StringUtil.isNotEmpty(clientParam.getGcspPara1()) ? Integer.parseInt(clientParam.getGcspPara1()) : 0;
            int selectParam2 = clientParam != null && StringUtil.isNotEmpty(clientParam.getGcspPara2()) ? Integer.parseInt(clientParam.getGcspPara2()) : 0;
            outData.setStoreSelectLimit(selectParam1);
            Integer adminFlag = getUserAdminFlag(user);
            outData.setAdminFlag(adminFlag);
            if (adminFlag == 1 && selectParam1 == 1 && selectParam2 == 1) {
                outData.setShowName(outData.getDepName());
            }
            String token = Util.generateUUID();
            outData.setToken(token);
            this.redisManager.set(token, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);

            threadPoolTaskExecutor.execute(() -> this.insertUserLoginInfo(outData, 1));
            return outData;
        }
    }

    private String getClientName(String client) {
        try {
            GaiaFranchisee cond = new GaiaFranchisee();
            cond.setClient(client);
            GaiaFranchisee gaiaFranchisee = franchiseeMapper.selectOne(cond);
            return gaiaFranchisee == null ? "" : gaiaFranchisee.getFrancName();
        } catch (Exception e) {
            log.error("<用户模块><登录接口><获取加盟商信息异常:{}>", e.getMessage(), e);
            return "";
        }
    }

    private Integer getUserAdminFlag(GaiaUserData user) {
        int count = userDataMapper.countAdminGroup(user);
        return count > 0 ? 1 : 0;
    }

    private ClientParam getStoreSelectLimitParam(String client) {
        try {
            ClientParam cond = new ClientParam();
            cond.setClient(client);
            cond.setGcspId(ClientParmaEnum.WEB_STORE_SELECT_LIMIT.id);
            return clientParamMapper.getUnique(cond);
        } catch (Exception e) {
            log.error("<用户模块><登录接口><查询系统参数异常,>", e);
            return new ClientParam();
        }
    }

    @Override
    public GetLoginOutData loginByPhone(GetLoginInData inData) {
        GetLoginOutData outData = new GetLoginOutData();
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("userTel", inData.getPhone());
        List<GaiaUserData> userList = this.userDataMapper.selectByExample(example);
        if (CollUtil.isEmpty(userList)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            int leaveCount = 0;
            int stopCount = 0;
            List<String> clientList = new ArrayList();
            Map<String, String> urseMap = new HashMap();
            Iterator var9 = userList.iterator();

            while (var9.hasNext()) {
                GaiaUserData user = (GaiaUserData) var9.next();
                if ("1".equals(user.getUserSta())) {
                    ++leaveCount;
                }

                if ("2".equals(user.getUserSta())) {
                    ++stopCount;
                }

                if ("0".equals(user.getUserSta())) {
                    clientList.add(user.getClient());
                    if (!StrUtil.isNotBlank(outData.getUserId())) {
                        urseMap.put(user.getClient(), user.getUserId());
                        outData.setUserId(user.getUserId());
                        outData.setLoginName(user.getUserNam());
                        outData.setClient(user.getClient());
//                  outData.setDepId(user.getDepId());
                        outData.setDepName(user.getDepName());
                        outData.setUserAddr(user.getUserAddr());
                        outData.setUserIdc(user.getUserIdc());
                        outData.setUserSex(user.getUserSex());
                        outData.setUserSta(user.getUserSta());
                        outData.setUserTel(user.getUserTel());
                        outData.setUserYsId(user.getUserYsId());
                        outData.setUserYsZyfw(user.getUserYsZyfw());
                        outData.setUserYsZylb(user.getUserYsZylb());
                        outData.setUserLoginSta(user.getUserLoginSta());
                    }
                }
            }

            if (leaveCount == userList.size()) {
                throw new BusinessException("提示：员工已离职");
            } else if (stopCount == userList.size()) {
                throw new BusinessException("提示：账号已停用");
            } else {
                Object obj = this.redisManager.get(inData.getPhone());
                if (ObjectUtil.isEmpty(obj)) {
                    throw new BusinessException("提示：短信验证码已过期");
                } else if (!inData.getCode().equals(obj.toString())) {
                    throw new BusinessException("提示：短信验证码错误");
                } else {
                    example = new Example(GaiaFranchisee.class);
                    example.createCriteria().andIn("client", clientList);
                    List<GaiaFranchisee> franchisees = this.franchiseeMapper.selectByExample(example);
                    Map<String, String> francNameMap = new HashMap();
                    Iterator var12 = franchisees.iterator();

                    while (var12.hasNext()) {
                        GaiaFranchisee franchisee = (GaiaFranchisee) var12.next();
                        francNameMap.put(franchisee.getClient(), franchisee.getFrancName());
                    }

                    List<GetLoginFrancOutData> franchiseeList = new ArrayList();
                    Iterator var19 = clientList.iterator();

                    while (var19.hasNext()) {
                        String client = (String) var19.next();
                        GetLoginFrancOutData franchiseeOutData = new GetLoginFrancOutData();
                        franchiseeOutData.setClient(client);
                        franchiseeOutData.setFrancName((String) francNameMap.get(client));
                        franchiseeOutData.setUserId((String) urseMap.get(client));
                        franchiseeList.add(franchiseeOutData);
                    }

                    outData.setFranchiseeList(franchiseeList);
                    String token = Util.generateUUID();
                    outData.setToken(token);
                    this.redisManager.set(token, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);
                    this.redisManager.delete(inData.getPhone());
                    return outData;
                }
            }
        }
    }

    @Override
    public List<GetResourceOutData> getResByUser(GetLoginOutData loginOutData) {
        String userId = loginOutData.getUserId();
        List<GetResourceOutData> top = new ArrayList();
        Example example = new Example(GaiaResource.class);
        example.createCriteria().andEqualTo("status", "1").andEqualTo("module", "1");
        example.orderBy("seq");
        //List<GaiaResource> menus = this.resourceMapperselectByExample.(example);

        List<GaiaResource> menus = this.resourceMapper.queryResByUser(loginOutData);

        Map<String, GetResourceOutData> treeMap = new HashMap();
        Iterator var7 = menus.iterator();

        GaiaResource menu;
        String id;
        String pId;
        GetResourceOutData depResource;
        while (var7.hasNext()) {
            menu = (GaiaResource) var7.next();
            if (!"3".equals(menu.getType())) {
                id = menu.getId();
                pId = menu.getParentId();
                depResource = new GetResourceOutData();
                depResource.setId(id);
                depResource.setName(menu.getName());
                depResource.setIcon(menu.getIcon());
                depResource.setPath(menu.getPath());
                depResource.setPagePath(menu.getPagePath());
                depResource.setTitle(menu.getTitle());
                depResource.setType(menu.getType());
                depResource.setPid(pId);
                treeMap.put(id, depResource);
                if (StrUtil.isBlank(pId)) {
                    top.add(depResource);
                }
            }
        }

        var7 = menus.iterator();

        GetResourceOutData warehouseResource;
        while (var7.hasNext()) {
            menu = (GaiaResource) var7.next();
            id = menu.getId();
            pId = menu.getParentId();
            depResource = (GetResourceOutData) treeMap.get(id);
            warehouseResource = (GetResourceOutData) treeMap.get(pId);
            if (!ObjectUtil.isEmpty(warehouseResource) && !ObjectUtil.isEmpty(depResource)) {
                warehouseResource.addChild(depResource);
            }
        }

        var7 = top.iterator();

        while (var7.hasNext()) {
            GetResourceOutData resource = (GetResourceOutData) var7.next();
            if ("GAIA_AU_01".equals(resource.getId())) {
                GaiaFranchisee franchisee = (GaiaFranchisee) this.franchiseeMapper.selectByPrimaryKey(loginOutData.getClient());
                if (ObjectUtil.isNull(franchisee)) {
                    break;
                }
                if (ObjectUtil.isNull(resource.getNodes())) {
                    break;
                }
                for (GetResourceOutData resource2 : resource.getNodes()) {
                    if ("GAIA_AU_0102".equals(resource2.getId())) {

//            GetResourceOutData francResourceEdit = new GetResourceOutData();
//            francResourceEdit.setId("CLIENTEDIT" + franchisee.getClient());
//            francResourceEdit.setName("用户");
//            francResourceEdit.setIcon("");
//            francResourceEdit.setPath(StrUtil.format("/editFranchisee{}", new Object[]{franchisee.getClient()}));
//            francResourceEdit.setPagePath("auth/franchisee/edit.vue");
//            francResourceEdit.setQuery(franchisee.getClient());
//            francResourceEdit.setTitle("用户");
//            francResourceEdit.setType("2");
//            francResourceEdit.setPid(resource.getId());
//            resource.addChild(francResourceEdit);
                        if ("1".equals(franchisee.getFrancType1())) {
                            depResource = new GetResourceOutData();
                            depResource.setId("STORE");
                            depResource.setName("门店");
                            depResource.setIcon("");
                            depResource.setPath("/store");
                            depResource.setPagePath("auth/store/index.vue");
                            depResource.setTitle("门店");
                            depResource.setType("2");
                            depResource.setPid(resource.getId());
                            resource2.addChild(depResource);
                        }

                        if ("1".equals(franchisee.getFrancType2())) {
                            depResource = new GetResourceOutData();
                            depResource.setId("FRANC_TYPE2");
                            depResource.setName("连锁");
                            depResource.setIcon("");
                            depResource.setPath("/compadmList");
                            depResource.setPagePath("auth/compadm/list.vue");
                            depResource.setTitle("连锁");
                            depResource.setType("2");
                            depResource.setPid(resource.getId());
                            resource2.addChild(depResource);
                        }

                        if ("1".equals(franchisee.getFrancType3())) {
                            depResource = new GetResourceOutData();
                            depResource.setId("FRANC_TYPE3");
                            depResource.setName("批发");
                            depResource.setIcon("");
                            depResource.setPath("/wholesaleList");
                            depResource.setPagePath("wholesale/list");
                            depResource.setTitle("批发");
                            depResource.setType("2");
                            depResource.setPid(resource.getId());
                            resource2.addChild(depResource);
                        }

                        depResource = new GetResourceOutData();
                        depResource.setId("DEP");
                        depResource.setName("部门");
                        depResource.setIcon("");
                        depResource.setPath("/deptStore/dept");
                        depResource.setPagePath("auth/deptStore/deptIndex.vue");
                        depResource.setTitle("部门");
                        depResource.setType("2");
                        depResource.setPid(resource.getId());
                        resource2.addChild(depResource);

//            warehouseResource = new GetResourceOutData();
//            warehouseResource.setId("WAREHOUSE");
//            warehouseResource.setName("公司仓库");
//            warehouseResource.setIcon("");
//            warehouseResource.setPath("/deptStore/store");
//            warehouseResource.setPagePath("auth/deptStore/storeIndex.vue");
//            warehouseResource.setTitle("公司仓库");
//            warehouseResource.setType("2");
//            warehouseResource.setPid(resource.getId());
//            resource.addChild(warehouseResource);
                    }

                }
            }
        }

        return top;
    }

    @Override
    public String getSmsCode(GetLoginInData inData) {
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("userTel", inData.getPhone());
        List<GaiaUserData> userList = this.userDataMapper.selectByExample(example);
        if (CollUtil.isEmpty(userList)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            int leaveCount = 0;
            int stopCount = 0;
            Iterator var6 = userList.iterator();

            while (var6.hasNext()) {
                GaiaUserData user = (GaiaUserData) var6.next();
                if ("1".equals(user.getUserSta())) {
                    ++leaveCount;
                }

                if ("2".equals(user.getUserSta())) {
                    ++stopCount;
                }
            }

            if (leaveCount == userList.size()) {
                throw new BusinessException("提示：员工已离职");
            } else if (stopCount == userList.size()) {
                throw new BusinessException("提示：账号已停用");
            } else {
                String code = RandomUtil.randomNumbers(6);
                String seq = RandomUtil.randomNumbers(2);
                GetSmsParaOutData para = this.thirdParaService.getPhoneCode();
                SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
                smsUtil.sendCaptcha(inData.getPhone(), code, seq);
                this.redisManager.set(inData.getPhone(), code, UtilConst.PHONE_CODE_EXPIRE);
                return seq;
            }
        }
    }

    @Override
    public List<GetLoginFrancOutData> selectFrancList(GetLoginOutData userInfo) {
        String loginAccount = userInfo.getLoginAccount();
        if (ObjectUtil.isEmpty(loginAccount)) {
            loginAccount = userInfo.getUserTel();
        }
        List<GetLoginFrancOutData> list = this.franchiseeMapper.selectUserFrancList(loginAccount);
        return list;
    }

    @Override
    public void chooseFranc(GetLoginInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("userId", inData.getUserId());
        GaiaUserData user = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            userInfo.setUserId(user.getUserId());
            userInfo.setLoginName(user.getUserNam());
            userInfo.setClient(user.getClient());
            userInfo.setDepId(user.getDepId());
            userInfo.setDepName(user.getDepName());
            userInfo.setUserAddr(user.getUserAddr());
            userInfo.setUserIdc(user.getUserIdc());
            userInfo.setUserSex(user.getUserSex());
            userInfo.setUserSta(user.getUserSta());
            userInfo.setUserTel(user.getUserTel());
            userInfo.setUserYsId(user.getUserYsId());
            userInfo.setUserYsZyfw(user.getUserYsZyfw());
            userInfo.setUserYsZylb(user.getUserYsZylb());
            userInfo.setUserLoginSta(user.getUserLoginSta());
            this.redisManager.set(userInfo.getToken(), JSON.toJSONString(userInfo), UtilConst.TOKEN_EXPIRE);
        }
    }

    @Override
    public void updatePassword(GetLoginInData inData, GetLoginOutData userInfo) {
        String client = userInfo.getClient();
        String userId = userInfo.getUserId();
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("userId", userId);
        GaiaUserData user = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException("提示：账号不存在");
        } else if (ObjectUtil.isNotEmpty(inData.getPasswordOld()) && !inData.getPasswordOld().equals(user.getUserPassword())) {
            throw new BusinessException("提示：原密码不正确");
        } else {
            user.setUserPassword(inData.getPassword());
            user.setUserLoginSta("1");
            this.userDataMapper.updateByPrimaryKey(user);
        }
    }

    @Override
    public List<GetButtonAuthObjOutData> selectAuthObj(String parentId, GetLoginOutData loginOutData) {
        String userId = loginOutData.getUserId();
        List<GetButtonAuthObjOutData> result = new ArrayList();
        Example example = new Example(GaiaAuthconfiData.class);
        example.createCriteria().andEqualTo("authconfiUser", userId);
        List<GaiaAuthconfiData> userMenus = this.authconfiDataMapper.selectByExample(example);
        if (CollUtil.isEmpty(userMenus)) {
            return result;
        } else {
            List<String> resourceList = new ArrayList();

            GaiaAuthconfiData var9;
            for (Iterator var8 = userMenus.iterator(); var8.hasNext(); var9 = (GaiaAuthconfiData) var8.next()) {
                ;
            }

            example = new Example(GaiaResource.class);
            example.createCriteria().andIn("id", resourceList).andEqualTo("parentId", parentId).andEqualTo("status", "1").andEqualTo("type", "3");
            example.orderBy("seq");
            List<GaiaResource> menus = this.resourceMapper.selectByExample(example);
            Iterator var13 = menus.iterator();

            while (var13.hasNext()) {
                GaiaResource menu = (GaiaResource) var13.next();
                GetButtonAuthObjOutData outData = new GetButtonAuthObjOutData();
                outData.setId(menu.getId());
                outData.setName(menu.getName());
                outData.setType(menu.getButtonType());
                result.add(outData);
            }

            return result;
        }
    }

    @Override
    public GetLoginOutData operationLogin(GetLoginInData inData) {
        GetLoginOutData outData = new GetLoginOutData();
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("userTel", inData.getUsername()).andEqualTo("userPassword", inData.getPassword());
        List<GaiaUserData> userList = this.userDataMapper.selectByExample(example);
        if (CollUtil.isEmpty(userList)) {
            throw new BusinessException("提示：账号不存在或密码不正确");
        } else {
            List<GaiaUserData> userIds = new ArrayList();

            GaiaUserData user = userList.get(0);
            outData.setUserId(user.getUserId());
            outData.setLoginName(user.getUserNam());
            outData.setClient(user.getClient());
            outData.setUserAddr(user.getUserAddr());
            outData.setUserIdc(user.getUserIdc());
            outData.setUserSex(user.getUserSex());
            outData.setUserSta(user.getUserSta());
            outData.setUserTel(user.getUserTel());
            outData.setUserYsId(user.getUserYsId());
            outData.setUserYsZyfw(user.getUserYsZyfw());
            outData.setUserYsZylb(user.getUserYsZylb());
            outData.setUserLoginSta(user.getUserLoginSta());
            outData.setUserSoleSaleNo(user.getUserSoleSaleNo());
            for (GaiaUserData u : userList) {
                userIds.add(u);
            }

            List<GetUserStoreOutData> userStoreList = this.storeDataMapper.selectUserStoreList(userIds);
            if (CollUtil.isEmpty(userStoreList)) {
                throw new BusinessException("提示：当前员工不属于任一门店");
            } else {
                List<GetLoginStoreOutData> storeList = new ArrayList();
                for (GetUserStoreOutData userStoreOutData : userStoreList) {
                    GetLoginStoreOutData storeData = new GetLoginStoreOutData();
                    storeData.setClient(userStoreOutData.getClient());
                    storeData.setUserId(userStoreOutData.getUserId());
                    storeData.setStoCode(userStoreOutData.getStoCode());
                    storeData.setStoName(userStoreOutData.getStoName());
                    storeData.setStoYbcode(userStoreOutData.getStoYbcode());

                    storeList.add(storeData);
                }
                outData.setDepId((storeList.get(0)).getStoCode());
                outData.setDepName((storeList.get(0)).getStoCode());
                List<GaiaUserData> userSelect = userList.stream().filter(ur -> ur.getClient().equals(storeList.get(0).getClient()) && ur.getUserId().equals(storeList.get(0).getUserId())).collect(Collectors.toList());
                GaiaUserData userLast = userSelect.get(0);
                outData.setUserId(userLast.getUserId());
                outData.setLoginName(userLast.getUserNam());
                outData.setClient(userLast.getClient());
                outData.setUserAddr(userLast.getUserAddr());
                outData.setUserIdc(userLast.getUserIdc());
                outData.setUserSex(userLast.getUserSex());
                outData.setUserSta(userLast.getUserSta());
                outData.setUserTel(userLast.getUserTel());
                outData.setUserYsId(userLast.getUserYsId());
                outData.setUserYsZyfw(userLast.getUserYsZyfw());
                outData.setUserYsZylb(userLast.getUserYsZylb());
                outData.setUserLoginSta(userLast.getUserLoginSta());
                outData.setUserSoleSaleNo(user.getUserSoleSaleNo());
                outData.setStoreList(storeList);
                String token = Util.generateUUID();
                outData.setToken(token);
                this.redisManager.set(token, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);

                threadPoolTaskExecutor.execute(() -> this.insertUserLoginInfo(outData, 2));
                return outData;

            }
        }
    }

    /**
     * FX token 刷新
     * @param request
     * @return
     */
    @Override
    public JsonResult FXRefreshToken(HttpServletRequest request) {
        String tokenInfoStr = request.getHeader("X-Token");
        if (tokenInfoStr == null) {
            return JsonResult.fail(101, "未携带token");
        }
//        String decryptToken = AesUtil.decrypt(tokenInfoStr, ydKey);
//        GetLoginOutData getLoginOutData = JSON.parseObject(decryptToken, GetLoginOutData.class);
//        String token = getLoginOutData.getTokenUUID();
        String userInfo = (String) this.redisManager.get(tokenInfoStr);
        this.redisManager.set(tokenInfoStr, userInfo, UtilConst.TOKEN_EXPIRE);
        log.info("FX refresh token, userInfo:{}", userInfo);
        return JsonResult.success(null, "refresh token success");
    }

    private void handleSpNode(List<AuthTree> rootAuthTree, String client){
        Iterator<AuthTree> iteratorTop = rootAuthTree.iterator();
        TreeNode node;
        while (iteratorTop.hasNext()) {
            AuthTree auth = (AuthTree) iteratorTop.next();
            if ("GAIA_AU_01".equals(auth.getId())) {
                GaiaFranchisee franchisee = (GaiaFranchisee) this.franchiseeMapper.selectByPrimaryKey(client);
                if (ObjectUtil.isNull(franchisee)) {
                    break;
                }
                if (ObjectUtil.isNull(auth.getChild())) {
                    break;
                }
                for (TreeNode authTree : auth.getChild()) {
                    List<TreeNode> child = authTree.getChild();
                    if ("GAIA_AU_0102".equals(authTree.getId())) {
                        if ("1".equals(franchisee.getFrancType1())) {
                            node = new TreeNode();
                            node.setId("STORE");
                            node.setLabel("门店");
                            node.setIcon("");
                            node.setPath("/store");
                            node.setPagePath("auth/store/index.vue");
                            node.setTitle("门店");
                            node.setAuthType("2");
                            node.setPId(authTree.getId());
                            child.add(node);
                        }

                        if ("1".equals(franchisee.getFrancType2())) {
                            node = new TreeNode();
                            node.setId("FRANC_TYPE2");
                            node.setLabel("连锁");
                            node.setIcon("");
                            node.setPath("/compadmList");
                            node.setPagePath("auth/compadm/list.vue");
                            node.setTitle("连锁");
                            node.setAuthType("2");
                            node.setPId(authTree.getId());
                            child.add(node);
                        }

                        if ("1".equals(franchisee.getFrancType3())) {
                            node = new TreeNode();
                            node.setId("FRANC_TYPE3");
                            node.setLabel("批发");
                            node.setIcon("");
                            node.setPath("/wholesaleList");
                            node.setPagePath("wholesale/list");
                            node.setTitle("批发");
                            node.setAuthType("2");
                            node.setPId(authTree.getId());
                            child.add(node);
                        }

                        node = new TreeNode();
                        node.setId("DEP");
                        node.setLabel("部门");
                        node.setIcon("");
                        node.setPath("/deptStore/dept");
                        node.setPagePath("auth/deptStore/deptIndex.vue");
                        node.setTitle("部门");
                        node.setAuthType("2");
                        node.setPId(authTree.getId());
                        child.add(node);
                    }

                }
            }
        }
    }

    @Override
    public List<TreeNode> getResByUserV2(GetLoginOutData loginOutData) {
        List<AuthTree> authTrees = this.resourceMapper.queryResByUserV2(loginOutData);
        // 过滤根节点
        List<AuthTree> rootAuthTree = authTrees.stream()
                .filter(item -> StrUtil.isBlank(item.getPId()))
                .collect(Collectors.toList());
        handleSpNode(rootAuthTree, loginOutData.getClient());
        // 过滤子节点
        List<AuthTree> childAuthTree = authTrees.stream()
                .filter(item -> !Objects.isNull(item.getPId()))
                .collect(Collectors.toList());
        // 组装树结构
        TreeUtil treeUtil = new TreeUtil(rootAuthTree, childAuthTree);
        List<TreeNode> tree = treeUtil.getTree();
        return tree;
    }

    @Override
    public GetLoginOutData operationLoginByPhone(GetLoginInData inData) {
        GetLoginOutData outData = new GetLoginOutData();
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("userTel", inData.getPhone());
        List<GaiaUserData> userList = this.userDataMapper.selectByExample(example);
        if (CollUtil.isEmpty(userList)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            String smsCode = (String) this.redisManager.get("operation_" + inData.getPhone());
            if (StringUtil.isEmpty(smsCode)) {
                throw new BusinessException("提示：验证码已过期，请重新获取验证码！");
            } else {
                if (!smsCode.equals(inData.getCode())) {
                    this.redisManager.delete("operation_" + inData.getPhone());
                    throw new BusinessException("提示：验证码不正确！请重新获取验证码！");
                }

                List<GaiaUserData> userIds = new ArrayList();

                GaiaUserData user = userList.get(0);
                outData.setUserId(user.getUserId());
                outData.setLoginName(user.getUserNam());
                outData.setClient(user.getClient());
                outData.setUserAddr(user.getUserAddr());
                outData.setUserIdc(user.getUserIdc());
                outData.setUserSex(user.getUserSex());
                outData.setUserSta(user.getUserSta());
                outData.setUserTel(user.getUserTel());
                outData.setUserYsId(user.getUserYsId());
                outData.setUserYsZyfw(user.getUserYsZyfw());
                outData.setUserYsZylb(user.getUserYsZylb());
                outData.setUserLoginSta(user.getUserLoginSta());

                for (GaiaUserData u : userList) {
                    userIds.add(u);
                }

                List<GetUserStoreOutData> userStoreList = this.storeDataMapper.selectUserStoreList(userIds);
                if (CollUtil.isEmpty(userStoreList)) {
                    throw new BusinessException("提示：当前员工不属于任一门店");
                } else {
                    List<GetLoginStoreOutData> storeList = new ArrayList();
                    for (GetUserStoreOutData userStoreOutData : userStoreList) {
                        GetLoginStoreOutData storeData = new GetLoginStoreOutData();
                        storeData.setClient(userStoreOutData.getClient());
                        storeData.setUserId(userStoreOutData.getUserId());
                        storeData.setStoCode(userStoreOutData.getStoCode());
                        storeData.setStoName(userStoreOutData.getStoName());
                        storeList.add(storeData);
                    }
                    outData.setDepId((storeList.get(0)).getStoCode());
                    outData.setDepName((storeList.get(0)).getStoCode());
                    List<GaiaUserData> userSelect = userList.stream().filter(ur -> ur.getClient().equals(storeList.get(0).getClient()) && ur.getUserId().equals(storeList.get(0).getUserId())).collect(Collectors.toList());
                    GaiaUserData userLast = userSelect.get(0);
                    outData.setUserId(userLast.getUserId());
                    outData.setLoginName(userLast.getUserNam());
                    outData.setClient(userLast.getClient());
                    outData.setUserAddr(userLast.getUserAddr());
                    outData.setUserIdc(userLast.getUserIdc());
                    outData.setUserSex(userLast.getUserSex());
                    outData.setUserSta(userLast.getUserSta());
                    outData.setUserTel(userLast.getUserTel());
                    outData.setUserYsId(userLast.getUserYsId());
                    outData.setUserYsZyfw(userLast.getUserYsZyfw());
                    outData.setUserYsZylb(userLast.getUserYsZylb());
                    outData.setUserLoginSta(userLast.getUserLoginSta());
                    outData.setStoreList(storeList);
                    String token = Util.generateUUID();
                    outData.setToken(token);
                    this.redisManager.set(token, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);

                    this.redisManager.delete("operation_" + inData.getPhone());

                    threadPoolTaskExecutor.execute(() -> this.insertUserLoginInfo(outData, 5));
                    return outData;
                }
            }
        }
    }

    @Override
    public String operationSmsCode(GetLoginInData inData) {
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("userTel", inData.getPhone());
        List<GaiaUserData> userList = this.userDataMapper.selectByExample(example);
        if (CollUtil.isEmpty(userList)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            String code = RandomUtil.randomNumbers(6);
            String seq = RandomUtil.randomNumbers(2);
            GetSmsParaOutData para = this.thirdParaService.getPhoneCode();
            SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
            smsUtil.sendCaptcha(inData.getPhone(), code, seq);
            this.redisManager.set("operation_" + inData.getPhone(), code, UtilConst.PHONE_CODE_EXPIRE);
            return seq;
        }
    }

    @Override
    public List<GetResourceOutData> operationResByUser(GetLoginOutData loginOutData) {
        String userId = loginOutData.getUserId();
        List<GetResourceOutData> top = new ArrayList();
        Example example = new Example(GaiaResource.class);
        example.createCriteria().andEqualTo("status", "1").andEqualTo("module", "2");
        example.orderBy("seq");
        List<GaiaResource> menus = this.resourceMapper.selectByExample(example);
        Map<String, GetResourceOutData> treeMap = new HashMap();
        Iterator var7 = menus.iterator();

        GaiaResource menu;
        String id;
        String pId;
        GetResourceOutData idData;
        while (var7.hasNext()) {
            menu = (GaiaResource) var7.next();
            if (!"3".equals(menu.getType())) {
                id = menu.getId();
                pId = menu.getParentId();
                idData = new GetResourceOutData();
                idData.setId(id);
                idData.setName(menu.getName());
                idData.setIcon(menu.getIcon());
                idData.setPath(menu.getPath());
                idData.setPagePath(menu.getPagePath());
                idData.setTitle(menu.getTitle());
                idData.setType(menu.getType());
                idData.setPid(pId);
                treeMap.put(id, idData);
                if (StrUtil.isBlank(pId)) {
                    top.add(idData);
                }
            }
        }

        var7 = menus.iterator();

        while (var7.hasNext()) {
            menu = (GaiaResource) var7.next();
            id = menu.getId();
            pId = menu.getParentId();
            idData = (GetResourceOutData) treeMap.get(id);
            GetResourceOutData pIdData = (GetResourceOutData) treeMap.get(pId);
            if (!ObjectUtil.isEmpty(pIdData) && !ObjectUtil.isEmpty(idData)) {
                pIdData.addChild(idData);
            }
        }

        return top;
    }

    @Override
    public void operationChooseStore(GetLoginInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("userId", inData.getUserId());
        GaiaUserData user = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stoCode", inData.getStoCode());
            GaiaStoreData store = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNull(store)) {
                throw new BusinessException("提示：门店不存在");
            } else {
                userInfo.setUserId(user.getUserId());
                userInfo.setLoginName(user.getUserNam());
                userInfo.setClient(user.getClient());
                userInfo.setDepId(store.getStoCode());
                userInfo.setDepName(store.getStoName());
                userInfo.setUserAddr(user.getUserAddr());
                userInfo.setUserIdc(user.getUserIdc());
                userInfo.setUserSex(user.getUserSex());
                userInfo.setUserSta(user.getUserSta());
                userInfo.setUserTel(user.getUserTel());
                userInfo.setUserYsId(user.getUserYsId());
                userInfo.setUserYsZyfw(user.getUserYsZyfw());
                userInfo.setUserYsZylb(user.getUserYsZylb());
                userInfo.setUserLoginSta(user.getUserLoginSta());
                userInfo.setStoDeliveryMode(store.getStoDeliveryMode());
                this.redisManager.set(userInfo.getToken(), JSON.toJSONString(userInfo), UtilConst.TOKEN_EXPIRE);
            }
        }
    }

    @Override
    public GetLoginOutData operationChooseStoreWeb(GetLoginInData inData, GetLoginOutData userInfo) {
        //把门店属性传到前端  我也不知道为啥要这么写
//      GetLoginOutData userInfoOld=new  GetLoginOutData();
//      BeanUtil.copyProperties(userInfo,userInfoOld);
        Example example = new Example(GaiaUserData.class);
        if (StringUtil.isEmpty(inData.getClient())) {
            example.createCriteria().andEqualTo("userTel", inData.getPhone());
        } else {
            example.createCriteria().andEqualTo("userTel", inData.getPhone()).andEqualTo("client", inData.getClient());
        }
        GaiaUserData user = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException("提示：账号不存在");
        } else {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stoCode", inData.getStoCode());
            GaiaStoreData store = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNull(store)) {
                throw new BusinessException("提示：门店不存在");
            } else {
                userInfo.setUserId(user.getUserId());
                userInfo.setLoginName(user.getUserNam());
                userInfo.setClient(user.getClient());
                userInfo.setDepId(store.getStoCode());
                userInfo.setDepName(store.getStoName());
                userInfo.setStoAttribute(store.getStoAttribute());
                userInfo.setUserAddr(user.getUserAddr());
                userInfo.setUserIdc(user.getUserIdc());
                userInfo.setUserSex(user.getUserSex());
                userInfo.setUserSta(user.getUserSta());
                userInfo.setUserTel(user.getUserTel());
                userInfo.setUserYsId(user.getUserYsId());
                userInfo.setUserYsZyfw(user.getUserYsZyfw());
                userInfo.setUserYsZylb(user.getUserYsZylb());
                userInfo.setUserLoginSta(user.getUserLoginSta());
                userInfo.setStoDeliveryMode(store.getStoDeliveryMode());
                this.redisManager.set(userInfo.getToken(), JSON.toJSONString(userInfo), UtilConst.TOKEN_EXPIRE);
//            userInfoOld.setStoAttribute(store.getStoAttribute());

            }
        }
        return userInfo;
    }

    @Override
    public GetLoginOutData getLoginOutDataByUserID(String Client, String userid) {
        GetLoginOutData outData = new GetLoginOutData();
        Example example = new Example(GaiaUserData.class);
        example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("client", Client).andEqualTo("userId", userid);
        GaiaUserData userData = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
        if (ObjectUtil.isNull(userData)) {
            throw new BusinessException("提示：账号不存在");
        }
        outData.setLoginAccount(userData.getUserTel());
        outData.setUserId(userData.getUserId());
        outData.setLoginName(userData.getUserNam());
        outData.setClient(userData.getClient());
        outData.setDepName(userData.getDepName());
        outData.setUserAddr(userData.getUserAddr());
        outData.setUserIdc(userData.getUserIdc());
        outData.setUserSex(userData.getUserSex());
        outData.setUserSta(userData.getUserSta());
        outData.setUserTel(userData.getUserTel());
        outData.setUserYsId(userData.getUserYsId());
        outData.setUserYsZyfw(userData.getUserYsZyfw());
        outData.setUserYsZylb(userData.getUserYsZylb());
        outData.setUserLoginSta(userData.getUserLoginSta());
        return outData;
    }

    @Override
    public String copyToken(GetLoginOutData outData) {
        String token = Util.generateUUID();
        outData.setToken(token);
        this.redisManager.set(token, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);
        return token;
    }

    @Override
    public GetLoginOutData FXCacheLogin(GetLoginInData inData) {
        GetLoginOutData outData = new GetLoginOutData();
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("userTel", inData.getPhone()).andEqualTo("userPassword", inData.getPassword());
        GaiaUserData user = this.userDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException("提示：账号不存在或密码不正确");
        }

        GetUserStoreOutData userStore = this.storeDataMapper.getUserStoreByBrId(inData.getClient(),inData.getStoCode());
        if (ObjectUtil.isEmpty(userStore)) {
            throw new BusinessException("提示：当前员工不属于任一门店");
        }
        String token = Util.generateUUID();
        outData.setUserId(user.getUserId());
        outData.setLoginName(user.getUserNam());
        outData.setClient(user.getClient());
        outData.setUserAddr(user.getUserAddr());
        outData.setUserIdc(user.getUserIdc());
        outData.setUserSex(user.getUserSex());
        outData.setUserSta(user.getUserSta());
        outData.setUserTel(user.getUserTel());
        outData.setUserYsId(user.getUserYsId());
        outData.setUserYsZyfw(user.getUserYsZyfw());
        outData.setUserYsZylb(user.getUserYsZylb());
        outData.setUserLoginSta(user.getUserLoginSta());
        outData.setDepId(userStore.getStoCode());
        outData.setToken(token);
        this.redisManager.set(token, JSON.toJSONString(outData), UtilConst.TOKEN_EXPIRE);
        return outData;
    }
}
