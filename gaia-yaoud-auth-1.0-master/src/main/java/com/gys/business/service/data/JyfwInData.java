package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class JyfwInData {
   private String id;
   private String client;
   private String jyfwType;
   private String jyfwOrgid;
   private String jyfwOrgname;
   private String jyfwId;
   private String jyfwName;
   public int pageSize;
   private int pageNum;
   private List<String> ids;
   private List<String> orgIds;
}
