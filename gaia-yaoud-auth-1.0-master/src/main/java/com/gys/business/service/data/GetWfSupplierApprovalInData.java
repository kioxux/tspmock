package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

/**
 * 供应商付款审批
 * @author 陈浩
 */
@Data
public class GetWfSupplierApprovalInData {
    @Field(
            name = "序号"
    )
    @JSONField(
            ordinal = 1
    )
    private String index;

    @Field(
            name = "供应商编码"
    )
    @JSONField(
            ordinal = 2
    )
    private String supCode;

    @Field(
            name = "供应商名称"
    )
    @JSONField(
            ordinal = 3
    )
    private String supName;

    @Field(
            name = "申请付款金额"
    )
    @JSONField(
            ordinal = 4
    )
    private String applicationAmount;

    @Field(
            name = "开户行名称"
    )
    @JSONField(
            ordinal = 5
    )
    private String supBankName;

    @Field(
            name = "开户行帐号"
    )
    @JSONField(
            ordinal = 6
    )
    private String supBankAccount;

    @Field(
            name = "申请人帐号"
    )
    @JSONField(
            ordinal = 7
    )
    private String applicant;

    @Field(
            name = "申请人"
    )
    @JSONField(
            ordinal = 8
    )
    private String applicantUserName;

    @Field(
            name = "申请日期"
    )
    @JSONField(
            ordinal = 9
    )
    private String dateOfApplication;

    @Field(
            name = "备注"
    )
    @JSONField(
            ordinal = 10
    )
    private String gpaRemarks;


}
