package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreatePurPayInData {
   @Field(
      name = "加盟商"
   )
   @JSONField(
      ordinal = 1
   )
   private String client = "";
   @Field(
      name = "采购主体"
   )
   @JSONField(
      ordinal = 2
   )
   private String payCompanyCode = "";
   @Field(
      name = "收款企业"
   )
   @JSONField(
      ordinal = 3
   )
   private String payOrderPayer = "";
   @Field(
      name = "付款申请单"
   )
   @JSONField(
      ordinal = 4
   )
   private String payOrderId = "";
   @Field(
      name = "付款日期"
   )
   @JSONField(
      ordinal = 5
   )
   private String payOrderDate = "";
   @Field(
      name = "支付方式"
   )
   @JSONField(
      ordinal = 6
   )
   private String payOrderMode = "";
   @Field(
      name = "应付账款余额"
   )
   @JSONField(
      ordinal = 7
   )
   private String payOrderPayable = "";
   @Field(
      name = "本次付款金额"
   )
   @JSONField(
      ordinal = 8
   )
   private String payOrderAmt = "";
   @Field(
      name = "情况说明"
   )
   @JSONField(
      ordinal = 9
   )
   private String payOrderRemark = "";
}
