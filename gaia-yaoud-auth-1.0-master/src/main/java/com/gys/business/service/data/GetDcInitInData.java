package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetDcInitInData {
   private String client;
   private String site;
}
