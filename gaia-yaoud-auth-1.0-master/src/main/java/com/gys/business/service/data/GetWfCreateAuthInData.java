package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateAuthInData {
   @Field(
      name = "姓名"
   )
   @JSONField(
      ordinal = 1
   )
   private String userName = "";
   @Field(
      name = "部门名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String depName = "";
   @Field(
      name = "消息内容"
   )
   @JSONField(
      ordinal = 3
   )
   private String msg = "";

}
