package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaDefaultParamMapper;
import com.gys.business.mapper.GaiaParamDetailMapper;
import com.gys.business.mapper.entity.DefultParamData;
import com.gys.business.mapper.entity.ParamDetailData;
import com.gys.business.service.ParamService;
import com.gys.business.service.StoreService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ParamServiceImpl implements ParamService {
   @Autowired
   private GaiaDefaultParamMapper paramDao;
   @Autowired
   private GaiaParamDetailMapper paramDetailDao;
   @Autowired
   private StoreService storeService;

   @Transactional
   public void addDefaultParam(DefultParamInData inData, GetLoginOutData userInfo) {
      DefultParamData data = (DefultParamData)this.paramDao.selectByPrimaryKey(inData.getGsspId());
      if (ObjectUtil.isNotEmpty(data)) {
         throw new BusinessException("参数名已存在，不要重复添加！");
      } else {
         DefultParamData defultParamData = new DefultParamData();
         defultParamData.setGsspId(inData.getGsspId());
         defultParamData.setGsspName(inData.getGsspName());
         defultParamData.setGsspPara(inData.getGsspPara());
         defultParamData.setGsspParaRemark(inData.getGsspParaRemark());
         defultParamData.setGsspUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
         defultParamData.setGsspUpdateEmp(userInfo.getUserId());
         this.paramDao.insert(defultParamData);
      }
   }

   @Transactional
   public void updateDefaultParam(DefultParamInData inData, GetLoginOutData userInfo) {
      DefultParamData defultParamData = (DefultParamData)this.paramDao.selectByPrimaryKey(inData.getGsspId());
      defultParamData.setGsspName(inData.getGsspName());
      defultParamData.setGsspParaRemark(inData.getGsspParaRemark());
      defultParamData.setGsspPara(inData.getGsspPara());
      defultParamData.setGsspUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
      defultParamData.setGsspUpdateEmp(userInfo.getUserId());
      this.paramDao.updateByPrimaryKey(defultParamData);
   }

   public PageInfo<DefultParamOutData> getDefaultParamList(DefultParamInData inData) {
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<DefultParamOutData> outData = this.paramDao.getDefaultParamList(inData);
      PageInfo pageInfo;
      if (ObjectUtil.isNotEmpty(outData)) {
         pageInfo = new PageInfo(outData);
      } else {
         pageInfo = new PageInfo();
      }

      return pageInfo;
   }

   public List getParamDetailList(ParamDetailInData inData) {
      Integer queryType = inData.getQueryType();
      List paramDetailList = new ArrayList();
      if (queryType == 1) {
         DefultParamInData dprInData = DefultParamInData.builder().gsspId(inData.getGsspId()).gsspName(inData.getGsspName()).build();
         paramDetailList = this.paramDao.getDefaultParamList(dprInData);
      } else if (queryType == 2) {
         paramDetailList = this.paramDetailDao.getParamDetailList(inData);
      }

      return (List)paramDetailList;
   }

   @Transactional
   public void updateParamDetail(ParamDetailInData inData, GetLoginOutData userInfo) {
      Integer queryType = inData.getQueryType();
      DefultParamData var10000 = (DefultParamData)this.paramDao.selectByPrimaryKey(inData.getGsspId());
      if (queryType == 1) {
         StoreInData storeInData = new StoreInData();
         storeInData.setClient(userInfo.getClient());
         List<StoreOutData> storeList = this.storeService.getStoreList(storeInData);
         if (!CollUtil.isEmpty(storeList)) {
            Iterator var7 = storeList.iterator();

            while(var7.hasNext()) {
               StoreOutData storeOutData = (StoreOutData)var7.next();
               inData.setGsspBrId(storeOutData.getStoCode());
               this.doUpdateParamDetail(inData, userInfo);
            }
         }
      } else if (queryType == 2) {
         this.doUpdateParamDetail(inData, userInfo);
      }

   }

   private void doUpdateParamDetail(ParamDetailInData inData, GetLoginOutData userInfo) {
      DefultParamData defultParamData = (DefultParamData)this.paramDao.selectByPrimaryKey(inData.getGsspId());
      if (!ObjectUtil.isEmpty(defultParamData)) {
         ParamDetailData detailData = new ParamDetailData();
         detailData.setClientId(userInfo.getClient());
         detailData.setGsspId(inData.getGsspId());
         detailData.setGsspName(inData.getGsspName());
         detailData.setGsspPara(inData.getGsspPara());
         detailData.setGsspParaRemark(inData.getGsspParaRemark());
         detailData.setGsspUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
         detailData.setGsspUpdateEmp(userInfo.getUserId());
         detailData.setGsspBrId(inData.getGsspBrId());
         ParamDetailData paramDetailData = (ParamDetailData)this.paramDetailDao.selectByPrimaryKey(detailData);
         if (ObjectUtil.isEmpty(paramDetailData)) {
            this.paramDetailDao.insert(detailData);
         } else {
            this.paramDetailDao.updateByPrimaryKey(detailData);
         }
      }

   }

   @Override
   public List<ParamDetailOutData> getDefaultParam(ParamDetailInData inData) {
      return paramDetailDao.getDefaultParam(null);
   }
}
