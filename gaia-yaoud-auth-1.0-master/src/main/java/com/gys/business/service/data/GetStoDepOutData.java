package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetStoDepOutData {

    private String stoDepCode;

    private String stoDepName;
}
