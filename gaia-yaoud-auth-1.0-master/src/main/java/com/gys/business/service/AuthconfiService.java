package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface AuthconfiService {
   void authorization(AuthListInData inData);

   void deleteAuth(AuthInData inData);

   AuthOutData authorizationList(AuthInData inData);

   List<AuthSiteOutData> selectSiteList(AuthInData inData);

   List<AuthSiteOutData> selectStoreList(AuthInData inData);

   List<AuthSiteOutData> selectDepList(AuthInData inData);

   List<AuthGroupOutData> selectGroupList(GetLoginOutData userInfo);

   void saveAuth(String client, String userId, List<String> groupList, List<String> siteList, String delUserId);
}
