package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfClientEditInData {
    @Field(
            name = "供应商编码"
    )
    @JSONField(
            ordinal = 1
    )
    private String supSelfCode = "";

    @Field(
            name = "供应商名称"
    )
    @JSONField(
            ordinal = 2
    )
    private String supName = "";

    @Field(
            name = "统一社会信用码"
    )
    @JSONField(
            ordinal = 3
    )
    private String supCreditCode = "";

    @Field(
            name = "修改字段"
    )
    @JSONField(
            ordinal = 4
    )
    private String supChangeColumnDes = "";

    @Field(
            name = "修改前"
    )
    @JSONField(
            ordinal = 5
    )
    private String supChangeFrom = "";

    @Field(
            name = "修改后"
    )
    @JSONField(
            ordinal = 6
    )
    private String supChangeTo = "";

    @Field(
            name = "修改原因"
    )
    @JSONField(
            ordinal = 7
    )
    private String supRemarks = "";
}
