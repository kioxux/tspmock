package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class GetSyncOutData {
   private List<GetSyncUserOutData> userList;
   private List<GetSyncStoreOutData> storeList;
}
