package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetMemberPreSetOutData {
   private String brId;
   private String memNewMix;
   private String menNewAuto;
}
