package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
@Data
@ApiModel(value = "工作流进度详情")
public class GetWorkflowApproveOutData {
   @ApiModelProperty(value = "进度序号，从0开始")
   private BigDecimal wfSeq;

   @ApiModelProperty(value = "审核人岗位")
   private String groupName;

   @ApiModelProperty(value = "审核人姓名")
   private String approverName;

   @ApiModelProperty(value = "审核人id")
   private String approverUserId;

   @ApiModelProperty(value = "进度状态描述")
   private String desc;

   @ApiModelProperty(value = "")
   private String result;

   @ApiModelProperty(value = "")
   private String memo;

   @ApiModelProperty(value = "审核日期")
   private String createDate;

   @ApiModelProperty(value = "审核时间")
   private String createTime;

   /**
    * 部门名称
    */
   private String deptName;
}
