package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaDomainConfig;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface DomainConfigService {
    List<GaiaDomainConfig> getAll();
}