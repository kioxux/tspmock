package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.mapper.GaiaThirdParaMapper;
import com.gys.business.mapper.entity.GaiaThirdPara;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.GetCosParaOutData;
import com.gys.business.service.data.GetSmsParaOutData;
import com.gys.common.exception.BusinessException;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ThirdParaServiceImpl implements ThirdParaService {
   @Autowired
   private GaiaThirdParaMapper thirdParaMapper;

   public GetCosParaOutData getCosPara() {
      Example example = new Example(GaiaThirdPara.class);
      example.createCriteria().andEqualTo("gtpType", "1");
      List<GaiaThirdPara> thirdParaList = this.thirdParaMapper.selectByExample(example);
      if (CollUtil.isEmpty(thirdParaList)) {
         throw new BusinessException("提示：未配置对象存储参数");
      } else {
         GetCosParaOutData outData = new GetCosParaOutData();
         Iterator var4 = thirdParaList.iterator();

         while(var4.hasNext()) {
            GaiaThirdPara thirdPara = (GaiaThirdPara)var4.next();
            if ("TENCENT_COS_SECRET_ID".equals(thirdPara.getGtpId())) {
               outData.setSecretId(thirdPara.getGtpPara());
            } else if ("TENCENT_COS_SECRET_KEY".equals(thirdPara.getGtpId())) {
               outData.setSecretKey(thirdPara.getGtpPara());
            } else if ("TENCENT_COS_REGION_NAME".equals(thirdPara.getGtpId())) {
               outData.setRegionName(thirdPara.getGtpPara());
            } else if ("TENCENT_COS_BUCKET_NAME".equals(thirdPara.getGtpId())) {
               outData.setBucketName(thirdPara.getGtpPara());
            }
         }

         return outData;
      }
   }

   public GetSmsParaOutData getPhoneCode() {
      Example example = new Example(GaiaThirdPara.class);
      example.createCriteria().andEqualTo("gtpType", "2");
      List<GaiaThirdPara> thirdParaList = this.thirdParaMapper.selectByExample(example);
      if (CollUtil.isEmpty(thirdParaList)) {
         throw new BusinessException("提示：未配置短信参数");
      } else {
         GetSmsParaOutData outData = new GetSmsParaOutData();
         Iterator var4 = thirdParaList.iterator();

         while(var4.hasNext()) {
            GaiaThirdPara thirdPara = (GaiaThirdPara)var4.next();
            if ("SMS_SPCODE".equals(thirdPara.getGtpId())) {
               outData.setSpCode(thirdPara.getGtpPara());
            } else if ("SMS_LOGIN_NAME".equals(thirdPara.getGtpId())) {
               outData.setLoginName(thirdPara.getGtpPara());
            } else if ("SMS_PASSWORD".equals(thirdPara.getGtpId())) {
               outData.setPassword(thirdPara.getGtpPara());
            } else if ("SMS_CONTENT_CODE".equals(thirdPara.getGtpId())) {
               outData.setTemplateContent(thirdPara.getGtpPara());
            } else if ("SMS_TEMPLATE_CODE".equals(thirdPara.getGtpId())) {
               outData.setTemplateId(thirdPara.getGtpPara());
            }
         }

         return outData;
      }
   }

   public GetSmsParaOutData getSmsFranchisee() {
      Example example = new Example(GaiaThirdPara.class);
      example.createCriteria().andEqualTo("gtpType", "2");
      List<GaiaThirdPara> thirdParaList = this.thirdParaMapper.selectByExample(example);
      if (CollUtil.isEmpty(thirdParaList)) {
         throw new BusinessException("提示：未配置短信参数");
      } else {
         GetSmsParaOutData outData = new GetSmsParaOutData();
         Iterator var4 = thirdParaList.iterator();

         while(var4.hasNext()) {
            GaiaThirdPara thirdPara = (GaiaThirdPara)var4.next();
            if ("SMS_SPCODE".equals(thirdPara.getGtpId())) {
               outData.setSpCode(thirdPara.getGtpPara());
            } else if ("SMS_LOGIN_NAME".equals(thirdPara.getGtpId())) {
               outData.setLoginName(thirdPara.getGtpPara());
            } else if ("SMS_PASSWORD".equals(thirdPara.getGtpId())) {
               outData.setPassword(thirdPara.getGtpPara());
            } else if ("SMS_CONTENT_FRANCHISEE".equals(thirdPara.getGtpId())) {
               outData.setTemplateContent(thirdPara.getGtpPara());
            } else if ("SMS_TEMPLATE_FRANCHISEE".equals(thirdPara.getGtpId())) {
               outData.setTemplateId(thirdPara.getGtpPara());
            }
         }

         return outData;
      }
   }
   public GetSmsParaOutData getSmsFranchiseeNew(String type) {
      Example example = new Example(GaiaThirdPara.class);
      example.createCriteria().andEqualTo("gtpType", "2");
      List<GaiaThirdPara> thirdParaList = this.thirdParaMapper.selectByExample(example);
      if (CollUtil.isEmpty(thirdParaList)) {
         throw new BusinessException("提示：未配置短信参数");
      } else {
         GetSmsParaOutData outData = new GetSmsParaOutData();
         Iterator var4 = thirdParaList.iterator();

         while(var4.hasNext()) {
            GaiaThirdPara thirdPara = (GaiaThirdPara)var4.next();
            if ("SMS_SPCODE".equals(thirdPara.getGtpId())) {
               outData.setSpCode(thirdPara.getGtpPara());
            } else if ("SMS_LOGIN_NAME".equals(thirdPara.getGtpId())) {
               outData.setLoginName(thirdPara.getGtpPara());
            } else if ("SMS_PASSWORD".equals(thirdPara.getGtpId())) {
               outData.setPassword(thirdPara.getGtpPara());
            } else if(type.equals("1")){
               if("SMS_CONTENT_FRANCHISEE_LEGAL".equals(thirdPara.getGtpId())) {
                  outData.setTemplateContent(thirdPara.getGtpPara());
               } else if ("SMS_TEMPLATE_FRANCHISEE_LEGAL".equals(thirdPara.getGtpId())) {
                  outData.setTemplateId(thirdPara.getGtpPara());
               }
            }else if(type.equals("2")){
               if("SMS_CONTENT_FRANCHISEE_ASS".equals(thirdPara.getGtpId())) {
                  outData.setTemplateContent(thirdPara.getGtpPara());
               } else if ("SMS_TEMPLATE_FRANCHISEE_ASS".equals(thirdPara.getGtpId())) {
                  outData.setTemplateId(thirdPara.getGtpPara());
               }
            }
         }

         return outData;
      }
   }

   //注册账号
   public GetSmsParaOutData addUser() {
      Example example = new Example(GaiaThirdPara.class);
      example.createCriteria().andEqualTo("gtpType", "2");
      List<GaiaThirdPara> thirdParaList = this.thirdParaMapper.selectByExample(example);
      if (CollUtil.isEmpty(thirdParaList)) {
         throw new BusinessException("提示：未配置短信参数");
      } else {
         GetSmsParaOutData outData = new GetSmsParaOutData();
         Iterator var4 = thirdParaList.iterator();

         while(var4.hasNext()) {
            GaiaThirdPara thirdPara = (GaiaThirdPara)var4.next();
            if ("SMS_SPCODE".equals(thirdPara.getGtpId())) {
               outData.setSpCode(thirdPara.getGtpPara());
            } else if ("SMS_LOGIN_NAME".equals(thirdPara.getGtpId())) {
               outData.setLoginName(thirdPara.getGtpPara());
            } else if ("SMS_PASSWORD".equals(thirdPara.getGtpId())) {
               outData.setPassword(thirdPara.getGtpPara());
            } else if ("SMS_CONTENT_USER".equals(thirdPara.getGtpId())) {
               outData.setTemplateContent(thirdPara.getGtpPara());
            } else if ("SMS_TEMPLATE_USER".equals(thirdPara.getGtpId())) {
               outData.setTemplateId(thirdPara.getGtpPara());
            }
         }

         return outData;
      }
   }
}
