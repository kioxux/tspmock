package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateDelegateInData {
   @Field(
      name = "姓名"
   )
   @JSONField(
      ordinal = 1
   )
   private String userName = "";
   @Field(
      name = "消息内容"
   )
   @JSONField(
      ordinal = 3
   )
   private String msg = "";
}
