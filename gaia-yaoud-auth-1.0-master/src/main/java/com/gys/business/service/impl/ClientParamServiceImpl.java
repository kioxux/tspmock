package com.gys.business.service.impl;

import com.gys.business.mapper.ClientParamMapper;
import com.gys.business.mapper.entity.ClientParam;
import com.gys.business.mapper.entity.SdParam;
import com.gys.business.service.ClientParamService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.gys.util.UtilConst.MEMCARD_DISCOUNT2;
import static com.gys.util.UtilConst.RECEIVE_PRICE_SHOW;

/**
 * @Auther: tzh
 * @Date: 2021/12/2 13:14
 * @Description: ClientParamServiceImpl
 * @Version 1.0.0
 */
@Service
public class ClientParamServiceImpl implements ClientParamService {
    @Resource
    private ClientParamMapper clientParamMapper;

    @Override
    public ClientParam getConfig(String client) {
        ClientParam param=new ClientParam();
        param.setClient(client);
        param.setGcspId(MEMCARD_DISCOUNT2);
        ClientParam unique = clientParamMapper.getUnique(param);
        return unique;
    }

    @Override
    public SdParam getReceivePriceConfig(String client, String depId) {
        SdParam param=new SdParam();
        param.setClient(client);
        param.setGsspBrId(depId);
        param.setGsspId(RECEIVE_PRICE_SHOW);
        SdParam unique = clientParamMapper.getUniqueSdParam(param);
        return unique;
    }
}
