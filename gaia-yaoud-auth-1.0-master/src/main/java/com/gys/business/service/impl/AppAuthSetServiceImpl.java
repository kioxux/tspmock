package com.gys.business.service.impl;

import com.gys.business.mapper.ClientParamMapper;
import com.gys.business.mapper.GaiaAuthconfiDataMapper;
import com.gys.business.mapper.entity.ClientParam;
import com.gys.business.mapper.entity.GaiaAuthconfiData;
import com.gys.business.service.AppAuthSetService;
import com.gys.common.data.AppPermissionVO;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.AuthRoleEnum;
import com.gys.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class AppAuthSetServiceImpl implements AppAuthSetService {
    @Resource
    private ClientParamMapper clientParamMapper;

    @Autowired
    private GaiaAuthconfiDataMapper authconfiDataMapper;

    @Override
    public AppPermissionVO getBusinessPermission(GetLoginOutData loginUser) {
        AppPermissionVO permissionVO = new AppPermissionVO();
        try {
            //登录账号具有的角色：公司负责人（查看|推送|淘汰）、商采负责人（查看|淘汰）、门店店长（查看|淘汰）
            List<GaiaAuthconfiData> authConfigList = authconfiDataMapper.getOutStockRoleList(loginUser);
            List<AppPermissionVO.StockPermission> stockPermissions = new ArrayList<>();
            if (authConfigList != null && authConfigList.size() > 0) {
                //对应角色具有的缺断货权限
                for (GaiaAuthconfiData authConfigData : authConfigList) {
                    AppPermissionVO.StockPermission stockPermission = new AppPermissionVO.StockPermission();
                    if (AuthRoleEnum.COMPANY_MASTER.type.equals(authConfigData.getAuthGroupId())) {
                        stockPermission = getPermissionFromSystemParam(loginUser.getClient(), AuthRoleEnum.COMPANY_MASTER);
                    } else if (AuthRoleEnum.SCZG.type.equals(authConfigData.getAuthGroupId())) {
                        stockPermission = getPermissionFromSystemParam(loginUser.getClient(), AuthRoleEnum.SCZG);
                    } else if (AuthRoleEnum.STORE_MASTER.type.equals(authConfigData.getAuthGroupId())) {
                        stockPermission = getPermissionFromSystemParam(loginUser.getClient(), AuthRoleEnum.STORE_MASTER);
                        stockPermission.setAuthSite(authConfigData.getAuthobjSite());
                    }
                    stockPermissions.add(stockPermission);
                }
            }
            permissionVO.setStockPermissions(stockPermissions);
        } catch (Exception e) {
            log.error("<APP业务权限><缺断货权限><获取用户缺断货权限异常：{}>", e.getMessage(), e);
        }
        return permissionVO;
    }

    private AppPermissionVO.StockPermission getPermissionFromSystemParam(String client, AuthRoleEnum authRoleEnum) {
        AppPermissionVO.StockPermission stockPermission = new AppPermissionVO.StockPermission();
        ClientParam cond = new ClientParam();
        cond.setClient(client);
        cond.setGcspId(authRoleEnum.permissionCode);
        ClientParam unique = clientParamMapper.getUnique(cond);
        if (unique == null) {
            //初始化系统参数
            unique = new ClientParam();
            unique.setClient(client);
            unique.setGcspId(authRoleEnum.permissionCode);
            unique.setGcspName(authRoleEnum.name);
            unique.setGcspPara1(authRoleEnum.permissionValue);
            unique.setGcspPara2("");
            unique.setGcspUpdateEmp("");
            unique.setGcspUpdateDate(DateUtils.dateToString(new Date(), "yyyyMMdd"));
            unique.setGcspUpdateTime(DateUtils.dateToString(new Date(), "HHmmss"));
            clientParamMapper.add(unique);
        }
        stockPermission.setRoleCode(authRoleEnum.type);
        stockPermission.setRoleName(authRoleEnum.name);
        String[] values = unique.getGcspPara1().split("\\|");
        if (values.length == 4) {
            stockPermission.setViewFlag(Integer.parseInt(values[0]));
            stockPermission.setPushFlag(Integer.parseInt(values[1]));
            stockPermission.setDeleteFlag(Integer.parseInt(values[2]));
            stockPermission.setReplenishFlag(Integer.parseInt(values[3]));
        }
        return stockPermission;
    }
}