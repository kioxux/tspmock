package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaAuthConfigDataMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.UserRestrictDetailMapper;
import com.gys.business.mapper.UserRestrictMapper;
import com.gys.business.mapper.entity.UserRestrict;
import com.gys.business.mapper.entity.UserRestrictDetail;
import com.gys.business.service.UserRestrictService;
import com.gys.business.service.data.CommonVo;
import com.gys.business.service.data.StoreOutData;
import com.gys.business.service.data.auth.StoInfo;
import com.gys.business.service.data.auth.UserRestrictAddReq;
import com.gys.business.service.data.auth.UserRestrictListReq;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月27日 下午2:00
 */
@Transactional
@Service
public class UserRestrictServiceImpl implements UserRestrictService {

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private UserRestrictMapper userRestrictMapper;

    @Autowired
    private UserRestrictDetailMapper userRestrictDetailMapper;

    @Resource
    private GaiaAuthConfigDataMapper gaiaAuthConfigDataMapper;


    @Override
    public Map<String, Object> init(GetLoginOutData userInfo, String userId) {
        Map<String, Object> resMap = new HashMap<>();
        List<CommonVo> restrictTypes = new ArrayList<>();

        CommonVo allSto = new CommonVo("全部公司的数据","0");

        restrictTypes.add(allSto);

        CommonVo inSto = new CommonVo("所在公司的数据","1");
        restrictTypes.add(inSto);

        CommonVo inArea = new CommonVo("指定片区的数据","2");
        restrictTypes.add(inArea);

        CommonVo appointSto = new CommonVo("指定公司的数据","3");
        restrictTypes.add(appointSto);

        resMap.put("restrictTypes", restrictTypes);

        List<CommonVo> comTypes = new ArrayList<>();
        CommonVo comSingle = new CommonVo("单体","1");
        comTypes.add(comSingle);
        CommonVo comChain = new CommonVo("连锁","2");
        comTypes.add(comChain);
        CommonVo comJoin = new CommonVo("加盟","3");
        comTypes.add(comJoin);
        CommonVo comOutpatient = new CommonVo("门诊","4");
        comTypes.add(comOutpatient);
        //5代表仓库 6代表部门 7代表区域
        CommonVo comSd = new CommonVo("仓库","5");
        comTypes.add(comSd);
        CommonVo comDept = new CommonVo("部门","6");
        comTypes.add(comDept);
//        CommonVo comRegion = new CommonVo("区域","7");
//        comTypes.add(comRegion);
        resMap.put("comTypes", comTypes);

        if(StrUtil.isBlank(userId)){
            throw new BusinessException("请传入所选用户userId!");
        }
        Example example = new Example(UserRestrict.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("userId", userId).andEqualTo("deleteFlag", "0");
        List<UserRestrict> storeDataList = this.userRestrictMapper.selectByExample(example);
        if(CollectionUtil.isNotEmpty(storeDataList)){
            resMap.put("chooseType", storeDataList.get(0).getRestrictType());
        }else{
            resMap.put("chooseType", "1");
        }
        return resMap;
    }

    private void handleOldDataDelete(String client, String userId, Integer userRestrictId) {

        //先删除明细，再删除主数据
        UserRestrictDetail delUserRestrictDetail = new UserRestrictDetail();
        delUserRestrictDetail.setRestrictId(userRestrictId);
        delUserRestrictDetail.setDeleteFlag(false);
        userRestrictDetailMapper.delete(delUserRestrictDetail);

        UserRestrict delUserRestrict = new UserRestrict();
        delUserRestrict.setClientId(client);
        delUserRestrict.setUserId(userId);
        delUserRestrict.setDeleteFlag(false);
        userRestrictMapper.delete(delUserRestrict);


    }

    @Override
    public Object addUserRestrict(GetLoginOutData userInfo, UserRestrictAddReq inData) {
        //指定片区的数据校验
        if ("2".equals(inData.getRestrictType())) {
            if (CollectionUtil.isEmpty(inData.getDistricts())) {
                throw new BusinessException("请至少选中一家门店!");
            }
        }
        //指定公司的数据校验
        if ("3".equals(inData.getRestrictType())) {
            if (CollectionUtil.isEmpty(inData.getStos())) {
                throw new BusinessException("请至少选中一家公司!");
            }
        }
        //首先删除原来的设定，均为逻辑删除
        //首先查询原来此用户是否存在配置
        UserRestrict queryUserRestrict = new UserRestrict();
        queryUserRestrict.setClientId(userInfo.getClient());
        queryUserRestrict.setUserId(inData.getUserId());
        queryUserRestrict.setDeleteFlag(false);
        UserRestrict userRestrictOld = userRestrictMapper.selectOne(queryUserRestrict);
        if (userRestrictOld != null) {
            //需要删除原有数据
            handleOldDataDelete(userInfo.getClient(), inData.getUserId(), userRestrictOld.getId());
        }
        //进行新数据的插入
        UserRestrict insertUserRestrict = new UserRestrict();
        insertUserRestrict.setClientId(userInfo.getClient());
        insertUserRestrict.setUserId(inData.getUserId());
        insertUserRestrict.setRestrictType(inData.getRestrictType());
        insertUserRestrict.setDeleteFlag(false);
        insertUserRestrict.setCreId(userInfo.getUserId());
        insertUserRestrict.setCreDate(DateUtils.getCurrentDate());
        insertUserRestrict.setCreTime(DateUtils.getCurrentTime());
        userRestrictMapper.insertUseGeneratedKeys(insertUserRestrict);

        Integer mainId = insertUserRestrict.getId();
        if ("2".equals(inData.getRestrictType())) {
            List<UserRestrictDetail> insertUserRestrictDetailList = new ArrayList<>();
            //处理指定片区数据
            for (String district : inData.getDistricts()) {
                UserRestrictDetail insertUserRestrictDetail = new UserRestrictDetail();
                insertUserRestrictDetail.setRestrictId(mainId);
                insertUserRestrictDetail.setCompType("7");
                insertUserRestrictDetail.setCompCode(district);
                insertUserRestrictDetail.setDeleteFlag(false);
                insertUserRestrictDetail.setCreId(userInfo.getUserId());
                insertUserRestrictDetail.setCreDate(DateUtils.getCurrentDate());
                insertUserRestrictDetail.setCreTime(DateUtils.getCurrentTime());
                insertUserRestrictDetailList.add(insertUserRestrictDetail);
            }
            userRestrictDetailMapper.insertList(insertUserRestrictDetailList);
        }

        if ("3".equals(inData.getRestrictType())) {
            //查询门店信息，

//            Example example = new Example(GaiaStoreData.class);
//            example.createCriteria().andEqualTo("client", userInfo.getClient()).andIn("stoCode", inData.getStoCodes()).andEqualTo("stoStatus", "0");
//            List<GaiaStoreData> storeDataList = this.storeDataMapper.selectByExample(example);
//            if(CollectionUtil.isEmpty(storeDataList)){
//                throw new BusinessException("提示：门店编码不合法");
//            }
//            Map<String, String> storeDataMap = new HashMap<>();
//            if (CollectionUtil.isNotEmpty(storeDataList)) {
//                storeDataMap = storeDataList.stream().collect(Collectors.toMap(GaiaStoreData::getStoCode, GaiaStoreData::getStoAttribute));
//            }
            //处理指定公司数据
            List<UserRestrictDetail> insertUserRestrictDetailList = new ArrayList<>();
            //处理指定片区数据
            for (StoInfo stoInfo : inData.getStos()) {
                UserRestrictDetail insertUserRestrictDetail = new UserRestrictDetail();
                insertUserRestrictDetail.setRestrictId(mainId);
                insertUserRestrictDetail.setCompType(stoInfo.getStoAttribute());
                insertUserRestrictDetail.setCompCode(stoInfo.getStoCode());
                insertUserRestrictDetail.setDeleteFlag(false);
                insertUserRestrictDetail.setCreId(userInfo.getUserId());
                insertUserRestrictDetail.setCreDate(DateUtils.getCurrentDate());
                insertUserRestrictDetail.setCreTime(DateUtils.getCurrentTime());
                insertUserRestrictDetailList.add(insertUserRestrictDetail);
            }
            userRestrictDetailMapper.insertList(insertUserRestrictDetailList);
        }

        return null;
    }

    @Override
    public List<StoreOutData> getStoreList(UserRestrictListReq inData, GetLoginOutData userInfo) {
        List<StoreOutData> stores = new ArrayList();
        inData.setClient(userInfo.getClient());
        //此处情况需要进行区分，首先查询该用户是否存在GAD情况，如果存在GAD意味着 restrictType 在0 1 情况下数据是一致的
        //如果不存在GAD情况，那么0和1存在区别，需要进行实际情况的查询

        //根据入参数进行区分
        if(StrUtil.isNotBlank(inData.getRestrictType()) && "1".equals(inData.getRestrictType())){
            stores = storeDataMapper.getStoreListByUserId(userInfo.getClient(),userInfo.getDepId(), inData.getUserId(),inData.getStoAttribute(),inData.getStoName(),inData.getStoProvince(),inData.getStoCity(),inData.getDistricts());
        }else{
            if(StrUtil.isNotBlank(inData.getStoAttribute()) || CollectionUtil.isNotEmpty(inData.getDistricts())){
                inData.setSearchFlag("Y");
            }
            stores = this.userRestrictMapper.getStoreList(inData);
            if (CollectionUtil.isNotEmpty(stores)) {
                //查询用户已经选中的数据
                UserRestrict queryUserRestrict = new UserRestrict();
                queryUserRestrict.setClientId(userInfo.getClient());
                queryUserRestrict.setUserId(inData.getUserId());
                queryUserRestrict.setDeleteFlag(false);
                UserRestrict userRestrictOld = userRestrictMapper.selectOne(queryUserRestrict);
                if (userRestrictOld != null && "3".equals(userRestrictOld.getRestrictType())) {
                    //配置了指定公司数据
                    UserRestrictDetail queryUserRestrictDetail = new UserRestrictDetail();
                    queryUserRestrictDetail.setRestrictId(userRestrictOld.getId());
                    queryUserRestrictDetail.setDeleteFlag(false);
                    List<UserRestrictDetail> details = userRestrictDetailMapper.select(queryUserRestrictDetail);
                    Map<String, String> detailMap = new HashMap<>();
                    if (CollectionUtil.isNotEmpty(details)) {
                        detailMap = details.stream().collect(Collectors.toMap(UserRestrictDetail::getCompCode, UserRestrictDetail::getCompCode));
                    }
                    for(StoreOutData x : stores){
                        x.setIfUserChoose(detailMap.get(x.getStoCode())==null?false:true);
                    }
                }
            }
        }
        return (List) stores;
    }


}

