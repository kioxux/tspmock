package com.gys.business.service;

import com.gys.business.service.data.GetResourceOutData;
import java.util.List;

public interface ResourceService {
   List<GetResourceOutData> selectMenu();
}
