package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class GetDefineOutData {
   private String defineType;
   private String defineName;
   private List<String> approveGroup1;
   private List<String> approveGroup2;
   private List<String> approveGroup3;
   private List<String> approveGroup4;
   private Integer maxGroup = 0;
   private String wfJsonString;
}
