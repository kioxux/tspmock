package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class ExchangeExportOutData {
   private String gsiesBrId;
   private String gsiesBrName;
   private String gsiesStoreGroup;
   private String gsiesMemberClass;
   private String gsiesDateStart;
   private String gsiesDateEnd;
   private String gsiesExchangeProId;
   private String gsieslExchangeIntegra;
   private BigDecimal gsiesExchangeAmt;
   private String gsiesExchangeQty;
   private String gsiesExchangeRepeat;
   private String gsiesIntegralAddSet;
}
