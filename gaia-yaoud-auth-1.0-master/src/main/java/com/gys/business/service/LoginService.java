package com.gys.business.service;

import com.gys.business.service.data.GetButtonAuthObjOutData;
import com.gys.business.service.data.GetLoginInData;
import com.gys.business.service.data.GetResourceOutData;
import com.gys.common.data.GetLoginFrancOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.TreeNode;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LoginService {
   GetLoginOutData login(GetLoginInData indata);

   GetLoginOutData loginByPhone(GetLoginInData indata);

   List<GetResourceOutData> getResByUser(GetLoginOutData loginOutData);

   String getSmsCode(GetLoginInData indata);

   List<GetLoginFrancOutData> selectFrancList(GetLoginOutData userInfo);

   void chooseFranc(GetLoginInData inData, GetLoginOutData userInfo);

   void updatePassword(GetLoginInData inData, GetLoginOutData userInfo);

   List<GetButtonAuthObjOutData> selectAuthObj(String parentId, GetLoginOutData loginOutData);

   GetLoginOutData operationLogin(GetLoginInData inData);

   GetLoginOutData operationLoginByPhone(GetLoginInData inData);

   String operationSmsCode(GetLoginInData inData);

   List<GetResourceOutData> operationResByUser(GetLoginOutData loginOutData);

   void operationChooseStore(GetLoginInData inData, GetLoginOutData userInfo);

   GetLoginOutData operationChooseStoreWeb(GetLoginInData inData, GetLoginOutData userInfo);

    GetLoginOutData getLoginOutDataByUserID(String Client,String userid);

   String copyToken(GetLoginOutData loginOutData);

   /**
    * fx 离线缓存登陆
    * @param inData
    * @return
    */
   GetLoginOutData FXCacheLogin(GetLoginInData inData);

    JsonResult FXRefreshToken(HttpServletRequest request);

   List<TreeNode> getResByUserV2(GetLoginOutData loginOutData);

}
