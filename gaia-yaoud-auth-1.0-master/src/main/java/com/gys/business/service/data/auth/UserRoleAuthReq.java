package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 单个门店的针对个人的授权角色
 * @author: flynn
 * @date: 2021年12月22日 下午11:09
 */
@Data
public class UserRoleAuthReq implements Serializable {

    //门店编码
    private String stoCode;

    //选中的角色集合
    private List<StroeRoleDelDetail> roles;
}

