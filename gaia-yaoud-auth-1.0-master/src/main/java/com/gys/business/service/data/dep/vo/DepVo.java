package com.gys.business.service.data.dep.vo;

import lombok.Data;

/**
 * @Auther: tzh
 * @Date: 2021/12/20 10:52
 * @Description: DepVo
 * @Version 1.0.0
 */
@Data
public class DepVo {
    private String depId;
    private String  depName;
}
