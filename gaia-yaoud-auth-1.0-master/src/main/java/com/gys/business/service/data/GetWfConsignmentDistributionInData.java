package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

/**
 * 委托配送的退库审批
 * @author 陈浩
 */
@Data
public class GetWfConsignmentDistributionInData {
    @Field(
            name = "序号"
    )
    @JSONField(
            ordinal = 1
    )
    private String index;

    @Field(
            name = "退库申请单号"
    )
    @JSONField(
            ordinal = 2
    )
    private String wsdTksqdh;

    @Field(
            name = "门店编号"
    )
    @JSONField(
            ordinal = 3
    )
    private String proSite;

    @Field(
            name = "门店名称"
    )
    @JSONField(
            ordinal = 4
    )
    private String wsdMdMc;

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 5
    )
    private String wsdSpBm;

    @Field(
            name = "名称"
    )
    @JSONField(
            ordinal = 6
    )
    private String wsdSpMc;

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 7
    )
    private String wsdSpGg;

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 8
    )
    private String wsdSpCj;

    @Field(
            name = "批次号"
    )
    @JSONField(
            ordinal = 9
    )
    private String wsdPch;

    @Field(
            name = "批号"
    )
    @JSONField(
            ordinal = 10
    )
    private String wsdPh;

    @Field(
            name = "批次价"
    )
    @JSONField(
            ordinal = 11
    )
    private String wsdPcj;

    @Field(
            name = "退库价"
    )
    @JSONField(
            ordinal = 12
    )
    private String wsdTkj;

    @Field(
            name = "供应商编号"
    )
    @JSONField(
            ordinal = 13
    )
    private String wsdGysBm;

    @Field(
            name = "供应商名称"
    )
    @JSONField(
            ordinal = 14
    )
    private String wsdGysMc;

    @Field(
            name = "申请数量"
    )
    @JSONField(
            ordinal = 15
    )
    private String wsdSqsl;

    @Field(
            name = "申请金额"
    )
    @JSONField(
            ordinal = 16
    )
    private String wsdSqje;

    @Field(
            name = "退库原因"
    )
    @JSONField(
            ordinal = 17
    )
    private String wsdTkyy;
}
