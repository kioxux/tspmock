package com.gys.business.service.data;

import lombok.Data;

@Data
public class GlobalOutData {
   private String storeId;
   private String storeName;
   private String typeOne;
   private String typeTwo;
   private String typeThree;
   private String typeFour;
   private String typeFive;
}
