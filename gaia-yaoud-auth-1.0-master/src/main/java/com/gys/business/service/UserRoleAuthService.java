package com.gys.business.service;

import com.gys.business.service.data.auth.AuthConfWithUser;
import com.gys.common.data.JsonResult;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午11:20
 */
public interface UserRoleAuthService {

    /**
     * 根据加盟商和用户id查询权限内门店信息
     *
     * @param client client
     * @param userId userId
     * @return JsonResult
     */
    JsonResult selectPermStoreAndRoleByUser(String client, String userId);

    /**
     * 查询角色权限树
     *
     * @param client   加盟商
     * @param roleType 角色类型
     * @param roleId   角色id
     * @return JsonResult
     */
    JsonResult selectRolePermTree(String client, String roleType, String roleId);

    /**
     * 查询门店下分配角色信息
     *
     * @param client  client
     * @param stoCode stoCode
     * @param userId  userId
     * @return JsonResult
     */
    JsonResult selectRoleInfoByStore(String client, String stoCode, String userId);

    /**
     * 保存用户门店角色权限
     *
     * @param authConfWithUser authConfWithUser
     * @return JsonResult
     */
    JsonResult saveAuthConfWithUser(AuthConfWithUser authConfWithUser);

}

