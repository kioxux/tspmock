package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class GetUserStoreOutData {
   private String client;
   private String userId;
   private String stoCode;
   private String stoName;
   private String stoYbcode;
   private String stoAttribute;
   private List<GetUserStoreOutData> storeList;
}
