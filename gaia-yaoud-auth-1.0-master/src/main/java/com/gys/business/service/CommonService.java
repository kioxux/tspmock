package com.gys.business.service;

import com.gys.business.service.data.GetFileOutData;
import org.springframework.web.multipart.MultipartFile;

public interface CommonService {
   GetFileOutData fileUpload(MultipartFile file);
}
