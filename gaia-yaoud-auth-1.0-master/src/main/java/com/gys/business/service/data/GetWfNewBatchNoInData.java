package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

/**
 * 门店盘点新增批号审批
 * @author 陈浩
 */
@Data
public class GetWfNewBatchNoInData {
    @Field(
            name = "序号"
    )
    @JSONField(
            ordinal = 1
    )
    private String index;

    @Field(
            name = "盘点单号"
    )
    @JSONField(
            ordinal = 2
    )
    private String wsdPddh;

    @Field(
            name = "门店编号"
    )
    @JSONField(
            ordinal = 3
    )
    private String proSite;

    @Field(
            name = "门店名称"
    )
    @JSONField(
            ordinal = 4
    )
    private String wsdMdMc;

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 5
    )
    private String wsdSpBm;

    @Field(
            name = "名称"
    )
    @JSONField(
            ordinal = 6
    )
    private String wsdSpMc;

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 7
    )
    private String wsdSpGg;

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 8
    )
    private String wsdSpCj;

    @Field(
            name = "新增批号"
    )
    @JSONField(
            ordinal = 9
    )
    private String wsdPh;

    @Field(
            name = "生产日期"
    )
    @JSONField(
            ordinal = 10
    )
    private String wsdScrq;

    @Field(
            name = "有效期"
    )
    @JSONField(
            ordinal = 11
    )
    private String wsdYxq;

    @Field(
            name = "供应商编号"
    )
    @JSONField(
            ordinal = 12
    )
    private String wsdGysBh;

    @Field(
            name = "供应商名称"
    )
    @JSONField(
            ordinal = 13
    )
    private String wsdGysMc;

    @Field(
            name = "批次价"
    )
    @JSONField(
            ordinal = 14
    )
    private String wsdPcj;

    @Field(
            name = "申请原因"
    )
    @JSONField(
            ordinal = 15
    )
    private String wsdSqyy;
}
