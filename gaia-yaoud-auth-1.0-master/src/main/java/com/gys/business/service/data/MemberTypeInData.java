package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class MemberTypeInData {
   private String gsmcId;
   private String gsmcName;
   private String gsmcDiscountRate;
   private String gsmcDiscountRate2;
   private BigDecimal gsmcAmtSale;
   private String gsmcIntegralSale;
}
