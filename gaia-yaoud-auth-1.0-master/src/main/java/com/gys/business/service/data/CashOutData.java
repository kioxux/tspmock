package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class CashOutData {
   private String clientId;
   private String gsicsVoucherId;
   private String gsicsBrId;
   private String gsicsBrName;
   private String gsicsMemberClass;
   private String gsicsMemberClassName;
   private String gsicsDateStart;
   private String gsicsDateEnd;
   private String gsicsIntegralDeduct;
   private BigDecimal gsicsAmtOffset;
   private BigDecimal gsicsAmtOffsetMax;
   private String gsicsDescription;
   private String gsicsIntegralAddSet;
   private String gsicsChangeSet;
   private String gsicsEmp;
}
