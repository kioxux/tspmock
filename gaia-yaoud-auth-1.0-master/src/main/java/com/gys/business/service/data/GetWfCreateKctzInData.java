package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateKctzInData {
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 1
   )
   private String proCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String proName = "";
   @Field(
      name = "规格/单位"
   )
   @JSONField(
      ordinal = 3
   )
   private String proSpecs = "";
   @Field(
      name = "批号"
   )
   @JSONField(
      ordinal = 4
   )
   private String batchCode = "";
   @Field(
      name = "调整数量"
   )
   @JSONField(
      ordinal = 5
   )
   private String qty = "";
   @Field(
      name = "调整前成本"
   )
   @JSONField(
      ordinal = 6
   )
   private String movPrice = "";
   @Field(
      name = "调整后成本"
   )
   @JSONField(
      ordinal = 7
   )
   private String movPrice1 = "";
   @Field(
      name = "总去税成本差异"
   )
   @JSONField(
      ordinal = 8
   )
   private String movPrice2 = "";
   @Field(
      name = "总税金差异"
   )
   @JSONField(
      ordinal = 9
   )
   private String movTax1 = "";
   @Field(
      name = "调整类型"
   )
   @JSONField(
      ordinal = 10
   )
   private String adjustmentType = "";
   @Field(
      name = "生产厂家"
   )
   @JSONField(
      ordinal = 11
   )
   private String proFactoryName = "";
}
