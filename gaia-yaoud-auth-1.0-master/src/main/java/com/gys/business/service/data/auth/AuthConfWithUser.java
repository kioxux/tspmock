package com.gys.business.service.data.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 用户角色权限配置
 * @date 2021/12/2313:37
 */
@Data
public class AuthConfWithUser implements Serializable {

    private String creator;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("门店角色")
    private List<StoreWithRole> stoCodeRoles;

    @Data
    public static class StoreWithRole {

        @ApiModelProperty("门店")
        private String stoCode;

        @ApiModelProperty("角色")
        private List<String> roleIds;

    }

}
