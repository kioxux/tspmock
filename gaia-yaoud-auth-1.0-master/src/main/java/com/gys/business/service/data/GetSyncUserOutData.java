package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetSyncUserOutData {
   private String client;
   private String userId;
   private String userPassword;
   private String userNam;
   private String depId;
   private String depName;
}
