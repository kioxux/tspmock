package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
@Data
public class DeptOutData {

   @ApiModelProperty(value = "加盟商")
   private String client;

   @ApiModelProperty(value = "部门编码")
   private String depId;

   @ApiModelProperty(value = "部门名称")
   private String depName;

   @ApiModelProperty(value = "部门负责人ID")
   private String depHeadId;

   @ApiModelProperty(value = "部门负责人姓名")
   private String depHeadNam;

   @ApiModelProperty(value = "创建日期")
   private String depCreDate;

   @ApiModelProperty(value = "创建时间")
   private String depCreTime;

   @ApiModelProperty(value = "创建人账号")
   private String depCreId;

   @ApiModelProperty(value = "修改日期")
   private String depModiDate;

   @ApiModelProperty(value = "修改时间")
   private String depModiTime;

   @ApiModelProperty(value = "修改人账号")
   private String depModiId;

   @ApiModelProperty(value = "停用日期")
   private String depDisDate;

   @ApiModelProperty(value = "连锁总部")
   private String compadmId;
   @ApiModelProperty(value = "连锁总部名称")
   private String compadmName;
   @ApiModelProperty(value = "类型")
   private String depType;

   @ApiModelProperty(value = "部门类型(11.质管部 12.商采部 13.运营部 14.财务部 16.人事行政部 15.信息部 17.物料仓 10.配送中心)")
   private String depCls;

   @ApiModelProperty(value = "状态 '0停用 1启用'")
   private String depStatus;
   private String depHeadPhone;
   private List<UserOutData> userList;
}
