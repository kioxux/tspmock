package com.gys.business.service.data;

import lombok.Data;

@Data
public class StogOutData {
   private String stogCode;
   private String stogName;
}
