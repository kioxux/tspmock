package com.gys.business.service.data;

import lombok.Data;

@Data
public class ExchangeExportInData {
   private String gsiesBrId;
   private String gsiesBrName;
   private String gsiesStoreGroup;
   private String gsiesMemberClass;
   private String gsiesDateStart;
   private String gsiesDateEnd;
   private String gsiesExchangeProId;
   private String gsieslExchangeIntegra;
   private String gsiesExchangeAmt;
   private String gsiesExchangeQty;
   private String gsiesExchangeRepeat;
   private String gsiesIntegralAddSet;
   private String index;
}
