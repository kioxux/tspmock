package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

/**
 * 配送差异的退库审批
 * @author 陈浩
 */
@Data
public class GetWfDistributionReturnInData {
    @Field(
            name = "序号"
    )
    @JSONField(
            ordinal = 1
    )
    private String index;

    @Field(
            name = "退库申请单号"
    )
    @JSONField(
            ordinal = 2
    )
    private String wsdTksqdh;

    @Field(
            name = "门店编号"
    )
    @JSONField(
            ordinal = 3
    )
    private String proSite;

    @Field(
            name = "门店名称"
    )
    @JSONField(
            ordinal = 4
    )
    private String wsdMdMc;

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 5
    )
    private String wsdSpBm;

    @Field(
            name = "名称"
    )
    @JSONField(
            ordinal = 6
    )
    private String wsdSpMc;

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 7
    )
    private String wsdSpGg;

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 8
    )
    private String wsdSpCj;

    @Field(
            name = "批号"
    )
    @JSONField(
            ordinal = 9
    )
    private String wsdPh;

    @Field(
            name = "生产日期"
    )
    @JSONField(
            ordinal = 10
    )
    private String wsdScrq;

    @Field(
            name = "有效期"
    )
    @JSONField(
            ordinal = 11
    )
    private String wsdYxq;

    @Field(
            name = "申请数量"
    )
    @JSONField(
            ordinal = 12
    )
    private String wsdSqsl;

    @Field(
            name = "退库原因"
    )
    @JSONField(
            ordinal = 13
    )
    private String wsdTkyy;

    @Field(
            name = "原拣货单号"
    )
    @JSONField(
            ordinal = 14
    )
    private String wsdYjhdh;

    @Field(
            name = "原配送单号"
    )
    @JSONField(
            ordinal = 15
    )
    private String wsdYpsdh;

    @Field(
            name = "原拣货人"
    )
    @JSONField(
            ordinal = 16
    )
    private String wsdYjhr;

    @Field(
            name = "原复核人"
    )
    @JSONField(
            ordinal = 17
    )
    private String wsdYfhr;

    @Field(
            name = "原复核日期"
    )
    @JSONField(
            ordinal = 18
    )
    private String wsdYfhrq;

    @Field(
            name = "原复核时间"
    )
    @JSONField(
            ordinal = 19
    )
    private String wsdYfhsj;
}
