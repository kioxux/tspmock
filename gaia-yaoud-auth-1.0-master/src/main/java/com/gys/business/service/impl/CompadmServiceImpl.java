package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaAuthconfiDataMapper;
import com.gys.business.mapper.GaiaCompadmMapper;
import com.gys.business.mapper.GaiaGroupDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaCompadm;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.CompadmService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.CompadmInData;
import com.gys.business.service.data.CompadmOutData;
import com.gys.business.service.data.GetCosParaOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.SelectModel;
import com.gys.common.exception.BusinessException;
import com.gys.util.CosUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class CompadmServiceImpl implements CompadmService {
   @Autowired
   private GaiaCompadmMapper compadmMapper;
   @Autowired
   private GaiaUserDataMapper userDataMapper;
   @Autowired
   private GaiaGroupDataMapper groupDataMapper;
   @Autowired
   private GaiaAuthconfiDataMapper authconfiDataMapper;
   @Autowired
   private ThirdParaService thirdParaService;

   public CompadmOutData getCompadmById(CompadmInData inData) {
      Example example = new Example(GaiaCompadm.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("compadmId", inData.getCompadmId());
      GaiaCompadm compadm = (GaiaCompadm)this.compadmMapper.selectOneByExample(example);
      CompadmOutData outData = new CompadmOutData();
      if (ObjectUtil.isNotEmpty(compadm)) {
         outData.setClient(compadm.getClient());
         outData.setCompadmAddr(compadm.getCompadmAddr());
         outData.setCompadmName(compadm.getCompadmName());
         outData.setCompadmNo(compadm.getCompadmNo());
         outData.setCompadmLegalPersonName(compadm.getCompadmLegalPerson());
         if (StrUtil.isNotBlank(compadm.getCompadmLogo())) {
            GetCosParaOutData para = this.thirdParaService.getCosPara();
            String secretId = para.getSecretId();
            String secretKey = para.getSecretKey();
            String regionName = para.getRegionName();
            String bucketName = para.getBucketName();
            CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
            String url = cosUtil.getFileUrl(compadm.getCompadmLogo());
            outData.setCompadmLogoUrl(url);
         }

         outData.setCompadmLogo(compadm.getCompadmLogo());
         example = new Example(GaiaUserData.class);
         example.createCriteria().andEqualTo("client", compadm.getClient()).andEqualTo("userId", compadm.getCompadmQua());
         GaiaUserData userData = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
         if (ObjectUtil.isNotEmpty(userData)) {
            outData.setCompadmQuaName(userData.getUserNam());
         }

         outData.setCompadmId(compadm.getCompadmId());
         outData.setCompadmStatus(compadm.getCompadmStatus());
      }

      return outData;
   }

   public List<CompadmOutData> compadmInfoByClientId(CompadmInData compadmInData) {
      List<CompadmOutData> outData = new ArrayList();
      Example example = new Example(GaiaCompadm.class);
      Criteria criteria = example.createCriteria().andEqualTo("client", compadmInData.getClient());
      if (ObjectUtil.isNotEmpty(compadmInData.getCompadmId())) {
         criteria.andEqualTo("compadmId", compadmInData.getCompadmId());
      }

      List<CompadmOutData> compadm = this.compadmMapper.compadmInfoByClientId( compadmInData.getClient(),compadmInData.getCompadmId());

//      for(int i = 0; i < compadm.size(); ++i) {
//         GaiaCompadm gaiaCompadm = (GaiaCompadm)compadm.get(i);
//         CompadmOutData compadmOutData = new CompadmOutData();
//         BeanUtils.copyProperties(gaiaCompadm, compadmOutData);
//         example = new Example(GaiaUserData.class);
//         example.createCriteria().andEqualTo("client", gaiaCompadm.getClient()).andEqualTo("userId", gaiaCompadm.getCompadmQua());
//         GaiaUserData userData = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
//         if (ObjectUtil.isNotEmpty(userData)) {
//            compadmOutData.setCompadmQuaName(userData.getUserNam());
//         }
//         if ("0".equals(compadmOutData.getCompadmStatus())){
//            compadmOutData.setCompadmName("(停用)"+compadmOutData.getCompadmName());
//         }else {
//            compadmOutData.setCompadmName("(启用)"+compadmOutData.getCompadmName());
//         }
//         outData.add(compadmOutData);
//      }

      return compadm;
   }

   @Transactional
   public void insertCompadm(CompadmInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaCompadm.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("compadmName", inData.getCompadmName());
      List<GaiaCompadm> list = this.compadmMapper.selectByExample(example);
      if (ObjectUtil.isNotEmpty(list)) {
         throw new BusinessException("该加盟商下已存在此公司！");
      } else {
         int max = this.compadmMapper.selectMaxId(userInfo.getClient());
         GaiaCompadm compadm = new GaiaCompadm();
         compadm.setCompadmId(String.valueOf(max + 1));
         compadm.setClient(inData.getClient());
         compadm.setCompadmAddr(inData.getCompadmAddr());
         compadm.setCompadmLegalPerson(inData.getCompadmLegalPersonName());
         compadm.setCompadmLogo(inData.getCompadmLogo());
         compadm.setCompadmName(inData.getCompadmName());
         compadm.setCompadmNo(inData.getCompadmNo());
         compadm.setCompadmQua(inData.getCompadmQua());
         compadm.setCompadmStatus("1");
         compadm.setCompadmCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         compadm.setCompadmCreTime(DateUtil.format(new Date(), "HHmmss"));
         compadm.setCompadmCreId(userInfo.getUserId());
         compadm.setCompadmModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         compadm.setCompadmModiTime(DateUtil.format(new Date(), "HHmmss"));
         compadm.setCompadmModiId(userInfo.getUserId());
         this.compadmMapper.insert(compadm);
      }
   }

   @Transactional
   public void updateCompadm(CompadmInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaCompadm.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("compadmId", inData.getCompadmId());
      GaiaCompadm compadm = (GaiaCompadm)this.compadmMapper.selectOneByExample(example);
      compadm.setCompadmAddr(inData.getCompadmAddr());
      compadm.setCompadmLegalPerson(inData.getCompadmLegalPersonName());
      compadm.setCompadmLogo(inData.getCompadmLogo());
      compadm.setCompadmName(inData.getCompadmName());
      compadm.setCompadmNo(inData.getCompadmNo());
      compadm.setCompadmQua(inData.getCompadmQua());
      compadm.setCompadmModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      compadm.setCompadmModiTime(DateUtil.format(new Date(), "HHmmss"));
      compadm.setCompadmModiId(userInfo.getUserId());
      this.compadmMapper.updateByPrimaryKeySelective(compadm);
   }

   @Transactional
   public void updateCompadmStatus(CompadmInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaCompadm.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("compadmId", inData.getCompadmId());
      GaiaCompadm compadm = (GaiaCompadm)this.compadmMapper.selectOneByExample(example);
      compadm.setCompadmStatus(inData.getCompadmStatus());
      compadm.setCompadmModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      compadm.setCompadmModiTime(DateUtil.format(new Date(), "HHmmss"));
      compadm.setCompadmModiId(userInfo.getUserId());
      this.compadmMapper.updateByPrimaryKeySelective(compadm);
   }

   public List<CompadmOutData> getCompadmList(CompadmInData inData) {
      List<CompadmOutData> outData = this.compadmMapper.getCompadmList(inData);
      if (ObjectUtil.isEmpty(outData)) {
         outData = new ArrayList();
      }

      return (List)outData;
   }
   public List<SelectModel> getCompadmAndCompadmWmsAndStore(CompadmInData inData) {
      List<SelectModel> outData = this.compadmMapper.getCompadmAndCompadmWmsAndStore(inData);
      if (ObjectUtil.isEmpty(outData)) {
         outData = new ArrayList();
      }

      return (List)outData;
   }
}
