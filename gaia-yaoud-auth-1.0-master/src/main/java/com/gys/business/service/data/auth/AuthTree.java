package com.gys.business.service.data.auth;

import com.gys.common.data.TreeNode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wu mao yin
 * @Title:
 * @date 2021/12/2218:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AuthTree extends TreeNode {

    @ApiModelProperty("是哪个端（产品）功能的权限 1表示web")
    private String product;


}
