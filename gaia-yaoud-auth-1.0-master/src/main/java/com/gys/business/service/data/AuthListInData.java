package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class AuthListInData {
   private String client;
   private List<AuthInData> authList;
   private String type;
   private String site;
}
