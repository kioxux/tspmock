package com.gys.business.service.data;

import lombok.Data;

@Data
public class CompadmInData {
   private String client;
   private String compadmId;
   private String compadmName;
   private String compadmNo;
   private String compadmLegalPersonName;
   private String compadmLegalPerson;
   private String compadmQuaName;
   private String compadmQua;
   private String compadmAddr;
   private String compadmCreDate;
   private String compadmCreTime;
   private String compadmCreId;
   private String compadmModiDate;
   private String compadmModiTime;
   private String compadmModiId;
   private String compadmLogo;
   private String compadmStatus;
   private String type;
}
