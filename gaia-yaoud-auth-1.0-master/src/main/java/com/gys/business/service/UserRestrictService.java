package com.gys.business.service;

import com.gys.business.service.data.StoreOutData;
import com.gys.business.service.data.auth.UserRestrictAddReq;
import com.gys.business.service.data.auth.UserRestrictListReq;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月27日 下午2:00
 */
public interface UserRestrictService {
    Map<String,Object> init(GetLoginOutData userInfo, String userId);

    Object addUserRestrict(GetLoginOutData userInfo, UserRestrictAddReq inData);

    List<StoreOutData> getStoreList(UserRestrictListReq inData, GetLoginOutData userInfo);
}

