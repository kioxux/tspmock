package com.gys.business.service;

import com.gys.business.service.data.CompadmInData;
import com.gys.business.service.data.CompadmOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.SelectModel;

import java.util.List;

public interface CompadmService {
   CompadmOutData getCompadmById(CompadmInData inData);

   List<CompadmOutData> compadmInfoByClientId(CompadmInData userInfo);

   void insertCompadm(CompadmInData inData, GetLoginOutData userInfo);

   void updateCompadm(CompadmInData inData, GetLoginOutData userInfo);

   void updateCompadmStatus(CompadmInData inData, GetLoginOutData userInfo);

   List<CompadmOutData> getCompadmList(CompadmInData inData);

   List<SelectModel> getCompadmAndCompadmWmsAndStore(CompadmInData inData);
}
