package com.gys.business.service.data;

import lombok.Data;

@Data
public class StoreExportInData {
   private String stoCode;
   private String stoName;
   private String stoAdd;
   private String stoNo;
   private String stoLegalPerson;
   private String stoQua;
   private String index;
}
