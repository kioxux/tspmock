package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaStogData;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.mapper.entity.UserRestrict;
import com.gys.business.service.StoreService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.auth.UserRestrictInfo;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.CosUtil;
import com.gys.util.UtilConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaStogDataMapper stogDataMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;
    @Autowired
    private GaiaAuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private ThirdParaService thirdParaService;
    @Autowired
    private RedisManager redisManager;

    @Autowired
    private UserRestrictMapper userRestrictMapper;

    @Autowired
    private UserRestrictDetailMapper userRestrictDetailMapper;

    @Override
    public List<StoreOutData> getStoreList(StoreInData inData) {
        List<StoreOutData> stores = new ArrayList();
        if (ObjectUtil.isNotEmpty(inData.getClient())) {
            stores = this.storeDataMapper.getStoreList(inData);
        }

        return (List) stores;
    }

    @Override
    public PageInfo<GetStoListOutData> getStoListByClientId(StoreInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        String type = inData.getType();

        if (StrUtil.isNotBlank(inData.getStoChainHead())) {
            inData.setType("2");
        } else {
            if (StrUtil.isNotEmpty(type)) {
                inData.setType(type);
            } else {
                inData.setType("1");
            }
        }

        List<GetStoListOutData> outData = this.storeDataMapper.getStoListByClientId(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public StoreOutData getStoreById(StoreInData inData) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stoCode", inData.getStoCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
        StoreOutData outData = new StoreOutData();
        if (ObjectUtil.isNotEmpty(storeData)) {
            outData.setClient(storeData.getClient());
            outData.setStoAdd(storeData.getStoAdd());
            outData.setStoCode(storeData.getStoCode());
            outData.setStogCode(storeData.getStogCode());
            outData.setStoLegalPersonName(storeData.getStoLegalPerson());
            GaiaUserData userData;
            if (StrUtil.isNotBlank(storeData.getStoLeader())) {
                example = new Example(GaiaUserData.class);
                example.createCriteria().andEqualTo("client", storeData.getClient()).andEqualTo("userId", storeData.getStoLeader());
                userData = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
                if (ObjectUtil.isNotNull(userData)) {
                    outData.setStoLeaderName(userData.getUserNam());
                    outData.setStoLeader(storeData.getStoLeader());
                }
            }

            if (StrUtil.isNotBlank(storeData.getStoLogo())) {
                GetCosParaOutData para = this.thirdParaService.getCosPara();
                String secretId = para.getSecretId();
                String secretKey = para.getSecretKey();
                String regionName = para.getRegionName();
                String bucketName = para.getBucketName();
                CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
                String url = cosUtil.getFileUrl(storeData.getStoLogo());
                outData.setStoLogoUrl(url);
            }

            outData.setStoLogo(storeData.getStoLogo());
            outData.setStoName(storeData.getStoName());
            outData.setStoNo(storeData.getStoNo());
            if (StrUtil.isNotBlank(storeData.getStoQua())) {
                example = new Example(GaiaUserData.class);
                example.createCriteria().andEqualTo("client", storeData.getClient()).andEqualTo("userId", storeData.getStoQua());
                userData = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
                if (ObjectUtil.isNotNull(userData)) {
                    outData.setStoQuaName(userData.getUserNam());
                    outData.setStoQua(storeData.getStoQua());
                }
            }

            outData.setStoStatus(storeData.getStoStatus());
        }

        return outData;
    }

    @Override
    @Transactional
    public void updateStoreStatus(StoreInData inData, GetLoginOutData userInfo) {
        if (ObjectUtil.isNotEmpty(inData.getStoCode())) {
            Example example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stoCode", inData.getStoCode());
            GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
            storeData.setStoStatus(inData.getStoStatus());
            storeData.setStoModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            storeData.setStoModiTime(DateUtil.format(new Date(), "HHmmss"));
            storeData.setStoModiId(userInfo.getUserId());
            this.storeDataMapper.updateByPrimaryKeySelective(storeData);
        }

    }

    @Override
    @Transactional
    public void insertStore(StoreInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stoCode", inData.getStoCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
        if (ObjectUtil.isNotEmpty(storeData)) {
            throw new BusinessException("门店编号已存在");
        } else {
            storeData = new GaiaStoreData();
            storeData.setStoCode(inData.getStoCode());
            storeData.setClient(inData.getClient());
            storeData.setStoAdd(inData.getStoAdd());
            storeData.setStoChainHead(inData.getStoChainHead());
            storeData.setStogCode(inData.getStogCode());
            storeData.setStoLegalPerson(inData.getStoLegalPersonName());
            storeData.setStoLeader(inData.getStoLeader());
            storeData.setStoLogo(inData.getStoLogo());
            storeData.setStoName(inData.getStoName());
            storeData.setStoNo(inData.getStoNo());
            storeData.setStoQua(inData.getStoQua());
            storeData.setStoStatus("0");
            storeData.setStoCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            storeData.setStoCreTime(DateUtil.format(new Date(), "HHmmss"));
            storeData.setStoCreId(userInfo.getUserId());
            storeData.setStoModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            storeData.setStoModiTime(DateUtil.format(new Date(), "HHmmss"));
            storeData.setStoModiId(userInfo.getUserId());
            this.storeDataMapper.insert(storeData);
            if (StrUtil.isNotBlank(storeData.getStoLeader())) {
//            example = new Example(GaiaUserData.class);
//            example.createCriteria().andEqualTo("userId", storeData.getStoLeader()).andEqualTo("client", storeData.getClient());
//            GaiaUserData record = new GaiaUserData();
//            record.setDepId(storeData.getStoCode());
//            record.setDepName(storeData.getStoName());
//            this.userDataMapper.updateByExample(record, example);
                UserInData userInData = new UserInData();
                userInData.setClient(storeData.getClient());
                userInData.setDepId(storeData.getStoCode());
                userInData.setDepName(storeData.getStoName());
                userInData.setUserId(storeData.getStoLeader());
                this.userDataMapper.updateUserInfo(userInData);
            }

        }
    }

    @Override
    @Transactional
    public void updateStore(StoreInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("stoCode", inData.getStoCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
        storeData.setStoAdd(inData.getStoAdd());
        storeData.setStoChainHead(inData.getStoChainHead());
        storeData.setStogCode(inData.getStogCode());
        storeData.setStoLegalPerson(inData.getStoLegalPersonName());
        storeData.setStoLogo(inData.getStoLogo());
        storeData.setStoName(inData.getStoName());
        if (StrUtil.isNotBlank(storeData.getStoLeader())) {
            example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", storeData.getClient()).andEqualTo("userId", storeData.getStoLeader());
            GaiaUserData userData = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
            //如果换人修改部门
            if (!inData.getStoLeader().equals(userData.getUserId())) {
                storeData.setStoLeader(inData.getStoLeader());
                UserInData userInData = new UserInData();
                userInData.setClient(storeData.getClient());
                userInData.setDepId(storeData.getStoCode());
                userInData.setDepName(storeData.getStoName());
                userInData.setUserId(inData.getStoLeader());
                this.userDataMapper.updateUserInfo(userInData);
            }
        }
        storeData.setStoNo(inData.getStoNo());
        if (StrUtil.isNotBlank(storeData.getStoQua())) {
            example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", storeData.getClient()).andEqualTo("userId", storeData.getStoQua());
            GaiaUserData userDataQua = (GaiaUserData) this.userDataMapper.selectOneByExample(example);
            //如果换人修改部门
            if (!inData.getStoQua().equals(userDataQua.getUserId())) {
                storeData.setStoQua(inData.getStoQua());
                UserInData userInData = new UserInData();
                userInData.setClient(storeData.getClient());
                userInData.setDepId(storeData.getStoCode());
                userInData.setDepName(storeData.getStoName());
                userInData.setUserId(inData.getStoQua());
                this.userDataMapper.updateUserInfo(userInData);
            }
        }
        storeData.setStoModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
        storeData.setStoModiTime(DateUtil.format(new Date(), "HHmmss"));
        storeData.setStoModiId(userInfo.getUserId());
        this.storeDataMapper.updateByPrimaryKeySelective(storeData);


    }

    @Override
    @Transactional
    public void insertStoreGroup(StoreInData inData) {
        Example example = new Example(GaiaStogData.class);
        example.createCriteria().andEqualTo("stogName", inData.getStogName()).andEqualTo("client", inData.getClient());
        GaiaStogData stogData = (GaiaStogData) this.stogDataMapper.selectOneByExample(example);
        if (ObjectUtil.isNotEmpty(stogData)) {
            throw new BusinessException("该门店组名称已存在");
        } else {
            int max = this.stogDataMapper.selectMaxId(inData.getClient());
            stogData = new GaiaStogData();
            stogData.setStogCode(String.valueOf(max));
            stogData.setClient(inData.getClient());
            stogData.setStogName(inData.getStogName());
            this.stogDataMapper.insert(stogData);
        }
    }

    @Override
    @Transactional
    public void deleteStog(StoreInData inData) {
        Example example = new Example(GaiaStogData.class);
        example.createCriteria().andIn("stogCode", inData.getIds()).andEqualTo("client", inData.getClient());
        this.stogDataMapper.deleteByExample(example);
    }

    @Override
    public List<StogOutData> getStogList(StoreInData inData) {
        List<StogOutData> stores = new ArrayList();
        if (ObjectUtil.isNotEmpty(inData.getClient())) {
            stores = this.stogDataMapper.getStogList(inData.getClient());
        }

        return (List) stores;
    }

    @Override
    public List<StogOutData> getStoGroupList(StoreInData inData) {
        List<StogOutData> stores = new ArrayList();
        if (ObjectUtil.isNotEmpty(inData.getClient())) {
            stores = this.stogDataMapper.getStoGroupList(inData.getClient());
        }

        return (List) stores;
    }

    @Override
    public PageInfo<StogOutData> getStogPage(StoreInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<StogOutData> outData = this.stogDataMapper.getStogList(inData.getClient());
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<String> getStoList(StoreInData inData) {
        List<String> stores = new ArrayList();
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stogCode", inData.getStogCode());
        List<GaiaStoreData> storeDataList = this.storeDataMapper.selectByExample(example);
        Iterator var5 = storeDataList.iterator();

        while (var5.hasNext()) {
            GaiaStoreData storeData = (GaiaStoreData) var5.next();
            stores.add(storeData.getStoCode());
        }

        return stores;
    }

    @Override
    public String getStoreCode(StoreInData inData) {
        int max = this.storeDataMapper.selectMaxId(inData.getClient());
        return String.valueOf(max + 1);
    }

    @Override
    public List<StoreOutData> selectAuthStoreList(String clientId, String userId) {
        List<StoreOutData> storeList = new ArrayList();
        List<String> storeCodeList = this.authconfiDataMapper.selectAuthStoreList(clientId, userId);
        if (CollUtil.isEmpty(storeCodeList)) {
            return null;
        } else {
            Example example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", clientId).andEqualTo("stoStatus", "0");
            List<GaiaStoreData> storeDataList = this.storeDataMapper.selectByExample(example);
            List<StoreOutData> allStoreList = new ArrayList<>();
            if (ObjectUtil.isEmpty(storeDataList)) {
                return null;
            } else {
                for (GaiaStoreData storeData : storeDataList) {
                    StoreOutData store = new StoreOutData();
                    store.setStoCode(storeData.getStoCode());
                    store.setStoName(storeData.getStoName());
                    store.setStoAttribute(storeData.getStoAttribute());
                    store.setStoDeliveryMode(storeData.getStoDeliveryMode());
                    allStoreList.add(store);
                }

                storeList.clear();
                if (storeCodeList.contains("GAD")) {
                    storeList.addAll(allStoreList);
                } else {
                    for (String storeCode : storeCodeList) {
                        for (GaiaStoreData storeData : storeDataList) {
                            if (storeCode.equals(storeData.getStoCode())) {
                                StoreOutData store = new StoreOutData();
                                store.setStoCode(storeData.getStoCode());
                                store.setStoName(storeData.getStoName());
                                store.setStoAttribute(storeData.getStoAttribute());
                                store.setStoDeliveryMode(storeData.getStoDeliveryMode());
                                storeList.add(store);
                            }
                        }

                    }
                }
            }
            return storeList;
        }
    }

    @Override
    public StoreOutData selectDefaultAuthStore(String clientId, String userId, GetLoginOutData userInfo) {
        List<StoreOutData> storeList = new ArrayList();
        List<String> storeCodeList = this.authconfiDataMapper.selectAuthStoreList(clientId, userId);
        if (CollUtil.isEmpty(storeCodeList)) {
            return null;
        } else {
            Example example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", clientId).andEqualTo("stoStatus", "0");
            List<GaiaStoreData> storeDataList = this.storeDataMapper.selectByExample(example);
            List<StoreOutData> allStoreList = new ArrayList<>();
            if (ObjectUtil.isEmpty(storeDataList)) {
                return null;
            } else {
                for (GaiaStoreData storeData : storeDataList) {
                    StoreOutData store = new StoreOutData();
                    store.setStoCode(storeData.getStoCode());
                    store.setStoName(storeData.getStoName());
                    store.setStoAttribute(storeData.getStoAttribute());
                    store.setStoDeliveryMode(storeData.getStoDeliveryMode());
                    allStoreList.add(store);
                }

                storeList.clear();
                if (storeCodeList.contains("GAD")) {
                    storeList.addAll(allStoreList);
                } else {
                    for (String storeCode : storeCodeList) {
                        for (GaiaStoreData storeData : storeDataList) {
                            if (storeCode.equals(storeData.getStoCode())) {
                                StoreOutData store = new StoreOutData();
                                store.setStoCode(storeData.getStoCode());
                                store.setStoName(storeData.getStoName());
                                store.setStoAttribute(storeData.getStoAttribute());
                                store.setStoDeliveryMode(storeData.getStoDeliveryMode());
                                storeList.add(store);
                            }
                        }

                    }
                }
            }
            if (storeList.size() > 0) {
                example = new Example(GaiaUserData.class);
                example.createCriteria().andEqualTo("userId", userId).andEqualTo("client", clientId);
                GaiaUserData user = this.userDataMapper.selectOneByExample(example);

                example = new Example(GaiaUserData.class);
                if (StringUtil.isEmpty(user.getClient())) {
                    example.createCriteria().andEqualTo("userTel", user.getUserTel());
                } else {
                    example.createCriteria().andEqualTo("userTel", user.getUserTel()).andEqualTo("client", user.getClient());
                }
                user = this.userDataMapper.selectOneByExample(example);
                List<StoreOutData> defaultStoreList = null;
                if (ObjectUtil.isEmpty(user)) {
                    throw new BusinessException("提示：账号不存在");
                } else {
                    //获取默认登录门店
                    if (!StringUtil.isEmpty(user.getDefaultSite())) {
                        GaiaUserData finalUser = user;
                        defaultStoreList = storeList.stream().filter(store -> store.getStoCode().equals(finalUser.getDefaultSite())).collect(Collectors.toList());
                    }
                    defaultStoreList = defaultStoreList == null || defaultStoreList.size() == 0 ? storeList : defaultStoreList;
                    example = new Example(GaiaStoreData.class);
                    example.createCriteria().andEqualTo("client", user.getClient()).andEqualTo("stoCode", defaultStoreList.get(0).getStoCode());
                    GaiaStoreData store2 = this.storeDataMapper.selectOneByExample(example);
                    if (ObjectUtil.isNull(store2)) {
                        throw new BusinessException("提示：门店信息异常");
                    } else {
                        userInfo.setUserId(user.getUserId());
                        userInfo.setLoginName(user.getUserNam());
                        userInfo.setClient(user.getClient());
                        userInfo.setDepId(store2.getStoCode());
                        userInfo.setDepName(store2.getStoName());
                        userInfo.setUserAddr(user.getUserAddr());
                        userInfo.setUserIdc(user.getUserIdc());
                        userInfo.setUserSex(user.getUserSex());
                        userInfo.setUserSta(user.getUserSta());
                        userInfo.setUserTel(user.getUserTel());
                        userInfo.setUserYsId(user.getUserYsId());
                        userInfo.setUserYsZyfw(user.getUserYsZyfw());
                        userInfo.setUserYsZylb(user.getUserYsZylb());
                        userInfo.setUserLoginSta(user.getUserLoginSta());
                        this.redisManager.set(userInfo.getToken(), JSON.toJSONString(userInfo), UtilConst.TOKEN_EXPIRE);
                    }
                }
                return defaultStoreList.get(0);
            } else {
                return null;
            }
        }
    }

    @Override
    public StoreOutData selectDefaultAuthStoreByToken(String clientId, String userId, GetLoginOutData userInfo) {
        List<StoreOutData> storeList = new ArrayList();
        List<String> storeCodeList = this.authconfiDataMapper.selectAuthStoreList(clientId, userId);
        if (CollUtil.isEmpty(storeCodeList)) {
            return null;
        } else {
            Example example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", clientId).andEqualTo("stoStatus", "0");
            List<GaiaStoreData> storeDataList = this.storeDataMapper.selectByExample(example);
            List<StoreOutData> allStoreList = new ArrayList<>();
            if (ObjectUtil.isEmpty(storeDataList)) {
                return null;
            } else {
                for (GaiaStoreData storeData : storeDataList) {
                    StoreOutData store = new StoreOutData();
                    store.setStoCode(storeData.getStoCode());
                    store.setStoName(storeData.getStoName());
                    store.setStoAttribute(storeData.getStoAttribute());
                    store.setStoDeliveryMode(storeData.getStoDeliveryMode());
                    allStoreList.add(store);
                }

                storeList.clear();
                if (storeCodeList.contains("GAD")) {
                    storeList.addAll(allStoreList);
                } else {
                    for (String storeCode : storeCodeList) {
                        for (GaiaStoreData storeData : storeDataList) {
                            if (storeCode.equals(storeData.getStoCode())) {
                                StoreOutData store = new StoreOutData();
                                store.setStoCode(storeData.getStoCode());
                                store.setStoName(storeData.getStoName());
                                store.setStoAttribute(storeData.getStoAttribute());
                                store.setStoDeliveryMode(storeData.getStoDeliveryMode());
                                storeList.add(store);
                            }
                        }
                    }
                }
            }
            if (storeList.size() > 0) {
                example = new Example(GaiaUserData.class);
                example.createCriteria().andEqualTo("userId", userId).andEqualTo("client", clientId);
                GaiaUserData user = this.userDataMapper.selectOneByExample(example);

                example = new Example(GaiaUserData.class);
                if (StringUtil.isEmpty(user.getClient())) {
                    example.createCriteria().andEqualTo("userTel", user.getUserTel());
                } else {
                    example.createCriteria().andEqualTo("userTel", user.getUserTel()).andEqualTo("client", user.getClient());
                }
                user = this.userDataMapper.selectOneByExample(example);
                if (ObjectUtil.isEmpty(user)) {
                    throw new BusinessException("提示：账号不存在");
                } else {
                    example = new Example(GaiaStoreData.class);
                    example.createCriteria().andEqualTo("client", user.getClient()).andEqualTo("stoCode", storeList.stream().filter(item -> item.getStoCode().equals(userInfo.getDepId())).findFirst().orElse(storeList.get(0)).getStoCode());
                    GaiaStoreData store2 = this.storeDataMapper.selectOneByExample(example);
                    if (ObjectUtil.isNull(store2)) {
                        throw new BusinessException("提示：门店信息异常");
                    } else {
                        userInfo.setUserId(user.getUserId());
                        userInfo.setLoginName(user.getUserNam());
                        userInfo.setClient(user.getClient());
                        userInfo.setDepId(store2.getStoCode());
                        userInfo.setDepName(store2.getStoName());
                        userInfo.setUserAddr(user.getUserAddr());
                        userInfo.setUserIdc(user.getUserIdc());
                        userInfo.setUserSex(user.getUserSex());
                        userInfo.setUserSta(user.getUserSta());
                        userInfo.setUserTel(user.getUserTel());
                        userInfo.setUserYsId(user.getUserYsId());
                        userInfo.setUserYsZyfw(user.getUserYsZyfw());
                        userInfo.setUserYsZylb(user.getUserYsZylb());
                        userInfo.setUserLoginSta(user.getUserLoginSta());
                        this.redisManager.set(userInfo.getToken(), JSON.toJSONString(userInfo), UtilConst.TOKEN_EXPIRE);
                    }
                }
                return storeList.stream().filter(item -> item.getStoCode().equals(userInfo.getDepId())).findFirst().orElse(storeList.get(0));
            } else {
                return null;
            }
        }
    }

    private UserRestrict getUserRestrictInfoByLoginInfo(String client, String userId) {
        UserRestrict mainQuery = new UserRestrict();
        mainQuery.setClientId(client);
        mainQuery.setUserId(userId);
        mainQuery.setDeleteFlag(false);
        UserRestrict mainUserRestrict = userRestrictMapper.selectOne(mainQuery);
        return mainUserRestrict;
    }

    @Override
    public List<StoreRestrictBasicRes> getRestrictStoreList(GetLoginOutData userInfo) {
        List<StoreRestrictBasicRes> resList = new ArrayList<>();
        UserRestrict mainUserRestrict = getUserRestrictInfoByLoginInfo(userInfo.getClient(), userInfo.getUserId());
        if (mainUserRestrict == null) {
            //查询不到用户配置数据权限时，使用当前门店
            String loginStoreCode = userInfo.getDepId();
            StoreRestrictBasicRes loginStore = storeDataMapper.getStoreByStoCode(userInfo.getClient(), loginStoreCode);
            if (loginStore != null) {
                resList.add(loginStore);
            }
        } else {
            //根据实际配置的数据权限进行拉取
            String restrictType = mainUserRestrict.getRestrictType();
            switch (restrictType) {
                case "0":
                    List<StoreRestrictBasicRes> allStores = storeDataMapper.getAllStoreList(userInfo.getClient());
                    resList = allStores;
                    break;
                case "1":
                    String loginStoreCode = userInfo.getDepId();
                    StoreRestrictBasicRes loginStore = storeDataMapper.getStoreByStoCode(userInfo.getClient(), loginStoreCode);
                    if (loginStore != null) {
                        resList.add(loginStore);
                    }
                    break;
                case "2":
                    List<StoreRestrictBasicRes> regionStores = storeDataMapper.getStoreInRegion(userInfo.getClient(), mainUserRestrict.getId());
                    resList = regionStores;
                    break;
                case "3":
                    List<StoreRestrictBasicRes> appointStores = storeDataMapper.getStoreAppoint(userInfo.getClient(), mainUserRestrict.getId());
                    resList = appointStores;
                    break;
            }
        }
        return resList;
    }

    @Override
    public UserRestrictInfo getRestrictStoreListWithType(GetLoginOutData userInfo) {
        UserRestrictInfo res = new UserRestrictInfo();
        UserRestrict mainUserRestrict = getUserRestrictInfoByLoginInfo(userInfo.getClient(), userInfo.getUserId());
        if (mainUserRestrict == null) {
            //查询不到用户配置数据权限时，使用当前门店
            res.setRestrictType("4");
            List<String> stoCodeList = new ArrayList<>();
            String loginStoreCode = userInfo.getDepId();
            StoreRestrictBasicRes loginStore = storeDataMapper.getStoreByStoCode(userInfo.getClient(), loginStoreCode);
            if (loginStore != null) {
                stoCodeList.add(loginStore.getStoCode());
            }
            res.setRestrictStoCodes(stoCodeList);
        } else {
            //根据实际配置的数据权限进行拉取
            String restrictType = mainUserRestrict.getRestrictType();
            res.setRestrictType(restrictType);
            List<String> stoCodeList = new ArrayList<>();
            switch (restrictType) {
                case "0":
                    List<StoreRestrictBasicRes> allStores = storeDataMapper.getAllStoreList(userInfo.getClient());
                    if (CollectionUtil.isNotEmpty(allStores)) {
                        for (StoreRestrictBasicRes basicRes : allStores) {
                            stoCodeList.add(basicRes.getStoCode());
                        }
                    }
                    break;
                case "1":
                    String loginStoreCode = userInfo.getDepId();
                    StoreRestrictBasicRes loginStore = storeDataMapper.getStoreByStoCode(userInfo.getClient(), loginStoreCode);
                    if (loginStore != null) {
                        stoCodeList.add(loginStore.getStoCode());
                    }
                    break;
                case "2":
                    List<StoreRestrictBasicRes> regionStores = storeDataMapper.getStoreInRegion(userInfo.getClient(), mainUserRestrict.getId());
                    if (CollectionUtil.isNotEmpty(regionStores)) {
                        for (StoreRestrictBasicRes basicRes : regionStores) {
                            stoCodeList.add(basicRes.getStoCode());
                        }
                    }
                    break;
                case "3":
                    List<StoreRestrictBasicRes> appointStores = storeDataMapper.getStoreAppoint(userInfo.getClient(), mainUserRestrict.getId());
                    if (CollectionUtil.isNotEmpty(appointStores)) {
                        for (StoreRestrictBasicRes basicRes : appointStores) {
                            stoCodeList.add(basicRes.getStoCode());
                        }
                    }
                    break;
            }
            res.setRestrictStoCodes(stoCodeList);
        }
        return res;
    }

    @Override
    @Transactional
    public JsonResult exportIn(List<StoreExportInData> inData, GetLoginOutData outData) throws Exception {
        if (ObjectUtil.isEmpty(inData)) {
            return JsonResult.fail(UtilConst.CODE_1001, "需要导入的门店为空");
        } else {
            Iterator var3 = inData.iterator();

            StoreExportInData exportInData;
            while (var3.hasNext()) {
                exportInData = (StoreExportInData) var3.next();
                if (ObjectUtil.isEmpty(exportInData.getStoCode())) {
                    return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的门店编码不能为空");
                }

                if (ObjectUtil.isEmpty(exportInData.getStoName())) {
                    return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的门店名称不能为空");
                }

                if (ObjectUtil.isEmpty(exportInData.getStoAdd())) {
                    return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的详细地址不能为空");
                }

                if (ObjectUtil.isEmpty(exportInData.getStoNo())) {
                    return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的统一社会信用代码不能为空");
                }

                if (ObjectUtil.isEmpty(exportInData.getStoLegalPerson())) {
                    return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的法人不能为空");
                }

                if (ObjectUtil.isEmpty(exportInData.getStoQua())) {
                    return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的质量负责人不能为空");
                }
            }

            var3 = inData.iterator();

            while (var3.hasNext()) {
                exportInData = (StoreExportInData) var3.next();
                int max = this.storeDataMapper.selectMaxId(outData.getClient());
                GaiaStoreData storeData = new GaiaStoreData();
                storeData.setStoCode(String.valueOf(max + 1));
                storeData.setClient(outData.getClient());
                storeData.setStoAdd(exportInData.getStoAdd());
                storeData.setStoLegalPerson(exportInData.getStoLegalPerson());
                storeData.setStoName(exportInData.getStoName());
                storeData.setStoNo(exportInData.getStoNo());
                storeData.setStoQua(exportInData.getStoQua());
                storeData.setStoCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
                storeData.setStoCreTime(DateUtil.format(new Date(), "HHmmss"));
                storeData.setStoCreId(outData.getUserId());
                storeData.setStoModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
                storeData.setStoModiTime(DateUtil.format(new Date(), "HHmmss"));
                storeData.setStoModiId(outData.getUserId());
                this.storeDataMapper.insert(storeData);
            }

            return JsonResult.success((Object) null, "提示：上传成功！");
        }
    }
}
