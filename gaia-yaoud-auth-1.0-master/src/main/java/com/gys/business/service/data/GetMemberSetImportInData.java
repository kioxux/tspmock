package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetMemberSetImportInData {
   private String brId;
   private String CardNo;
   private String type;
}
