package com.gys.business.service.data;

import lombok.Data;

@Data
public class JyfwooOutData {
   private String jyfwId;
   private String jyfwName;
}
