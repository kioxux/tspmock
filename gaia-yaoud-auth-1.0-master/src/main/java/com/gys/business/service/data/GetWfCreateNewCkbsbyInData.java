package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateNewCkbsbyInData {
    @Field(
            name = "加盟商"
    )
    @JSONField(
            ordinal = 1
    )
    private String client = "";

    @Field(
            name = "地点"
    )
    @JSONField(
            ordinal = 2
    )
    private String proSite = "";

    @Field(
            name = "仓库地点"
    )
    @JSONField(
            ordinal = 3
    )
    private String wmsCkMc = "";

    @Field(
            name = "盘点单号"
    )
    @JSONField(
            ordinal = 4
    )
    private String wmsPddh = "";

    @Field(
            name = "申请原因"
    )
    @JSONField(
            ordinal = 5
    )
    private String wmsSqyy = "";

    @Field(
            name = "申请人"
    )
    @JSONField(
            ordinal = 6
    )
    private String wmsSqr = "";

    @Field(
            name = "申请日期"
    )
    @JSONField(
            ordinal = 7
    )
    private String wmsSqrq = "";

    @Field(
            name = "申请时间"
    )
    @JSONField(
            ordinal = 8
    )
    private String wmsSqsj = "";

    @Field(
            name = "盘点类型"
    )
    @JSONField(
            ordinal = 9
    )
    private String wmsPdlx = "";

    @Field(
            name = "盘点方法"
    )
    @JSONField(
            ordinal = 10
    )
    private String wmsPdff = "";

    @Field(
            name = "盘点日期"
    )
    @JSONField(
            ordinal = 11
    )
    private String wmsPdrq = "";

    @Field(
            name = "盘点品项"
    )
    @JSONField(
            ordinal = 12
    )
    private String wmsPdPx = "";

    @Field(
            name = "盘点批号行数"
    )
    @JSONField(
            ordinal = 13
    )
    private String wmsPdPhhs = "";

    @Field(
            name = "盘点数量"
    )
    @JSONField(
            ordinal = 14
    )
    private String wmsPdSl = "";

    @Field(
            name = "盘点成本额"
    )
    @JSONField(
            ordinal = 15
    )
    private String wmsPdCbe = "";

    @Field(
            name = "盘点零售额"
    )
    @JSONField(
            ordinal = 16
    )
    private String wmsPdLse = "";

    @Field(
            name = "盘盈品项"
    )
    @JSONField(
            ordinal = 17
    )
    private String wmsPyPx = "";

    @Field(
            name = "盘盈批号行数"
    )
    @JSONField(
            ordinal = 18
    )
    private String wmsPyPhhs = "";

    @Field(
            name = "盘盈数量"
    )
    @JSONField(
            ordinal = 19
    )
    private String wmsPySl = "";

    @Field(
            name = "盘盈成本额"
    )
    @JSONField(
            ordinal = 20
    )
    private String wmsPyCbe = "";

    @Field(
            name = "盘盈零售额"
    )
    @JSONField(
            ordinal = 21
    )
    private String wmsPyLse = "";

    @Field(
            name = "盘亏品项"
    )
    @JSONField(
            ordinal = 22
    )
    private String wmsPkPx = "";

    @Field(
            name = "盘亏批号行数"
    )
    @JSONField(
            ordinal = 22
    )
    private String wmsPkPhhs = "";

    @Field(
            name = "盘亏数量"
    )
    @JSONField(
            ordinal = 23
    )
    private String wmsPkSl = "";

    @Field(
            name = "盘亏成本额"
    )
    @JSONField(
            ordinal = 24
    )
    private String wmsPkCbe = "";

    @Field(
            name = "盘亏零售额"
    )
    @JSONField(
            ordinal = 25
    )
    private String wmsPkLse = "";

    @Field(
            name = "绝对差异品项"
    )
    @JSONField(
            ordinal = 26
    )
    private String wmsJdcyPx = "";

    @Field(
            name = "绝对差异批号行数"
    )
    @JSONField(
            ordinal = 27
    )
    private String wmsJdcyPhhs = "";

    @Field(
            name = "绝对差异数量"
    )
    @JSONField(
            ordinal = 28
    )
    private String wmsJdcySl = "";

    @Field(
            name = "绝对差异成本额"
    )
    @JSONField(
            ordinal = 29
    )
    private String wmsJdcyCbe = "";

    @Field(
            name = "绝对差异零售额"
    )
    @JSONField(
            ordinal = 30
    )
    private String wmsJdcyLse = "";

    @Field(
            name = "绝对差异率品项"
    )
    @JSONField(
            ordinal = 31
    )
    private String wmsJdcylPx = "";

    @Field(
            name = "绝对差异率批号行数"
    )
    @JSONField(
            ordinal = 32
    )
    private String wmsJdcylPhhs = "";

    @Field(
            name = "绝对差异率数量"
    )
    @JSONField(
            ordinal = 33
    )
    private String wmsJdcylSl = "";

    @Field(
            name = "绝对差异率成本额"
    )
    @JSONField(
            ordinal = 34
    )
    private String wmsJdcylCbe = "";

    @Field(
            name = "绝对差异率零售额"
    )
    @JSONField(
            ordinal = 35
    )
    private String wmsJdcylLse = "";

    @Field(
            name = "相对差异品项"
    )
    @JSONField(
            ordinal = 36
    )
    private String wmsXdcyPx = "";

    @Field(
            name = "相对差异批号行数"
    )
    @JSONField(
            ordinal = 37
    )
    private String wmsXdcyPhhs = "";

    @Field(
            name = "相对差异数量"
    )
    @JSONField(
            ordinal = 38
    )
    private String wmsXdcySl = "";

    @Field(
            name = "相对差异成本额"
    )
    @JSONField(
            ordinal = 39
    )
    private String wmsXdcyCbe = "";

    @Field(
            name = "相对差异零售额"
    )
    @JSONField(
            ordinal = 40
    )
    private String wmsXdcyLse = "";

    @Field(
            name = "相对差异率品项"
    )
    @JSONField(
            ordinal = 41
    )
    private String wmsXdcylPx = "";

    @Field(
            name = "相对差异率批号行数"
    )
    @JSONField(
            ordinal = 42
    )
    private String wmsXdcylPhhs = "";

    @Field(
            name = "相对差异率数量"
    )
    @JSONField(
            ordinal = 43
    )
    private String wmsXdcylSl = "";

    @Field(
            name = "相对差异率成本额"
    )
    @JSONField(
            ordinal = 44
    )
    private String wmsXdcylCbe = "";

    @Field(
            name = "相对差异率零售额"
    )
    @JSONField(
            ordinal = 45
    )
    private String wmsXdcylLse = "";

    @Field(
            name = "是否审批结束"
    )
    @JSONField(
            ordinal = 46
    )
    private String wmsSfsp = "";

    @Field(
            name = "是否审批同意"
    )
    @JSONField(
            ordinal = 47
    )
    private String wmsSfty = "";
}
