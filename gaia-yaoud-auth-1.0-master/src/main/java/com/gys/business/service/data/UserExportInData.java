package com.gys.business.service.data;

import lombok.Data;

@Data
public class UserExportInData {
   private String userNam;
   private String userSex;
   private String depId;
   private String depName;
   private String userTel;
   private String userIdc;
   private String userYsId;
   private String userYsZyfw;
   private String userYsZylb;
   private String userAddr;
   private String userEmail;
   private String userSta;
   private String userJoinDate;
   private String userDisDate;
   private String index;
}
