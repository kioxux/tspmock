package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetWorkflowDetailListData<E> {
    @ApiModelProperty(value = "工作流编号")
    private String wfCode;

    @ApiModelProperty(value = "工作流名称")
    private String wfTitle;

    @ApiModelProperty(value = "列表数据")
    private List<E> listDataList;

    @ApiModelProperty(value = "审批节点信息")
    private List<GetSampleWorkflowApproveOutData> approveDataList;
}
