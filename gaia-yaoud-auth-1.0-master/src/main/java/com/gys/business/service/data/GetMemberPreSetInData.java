package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class GetMemberPreSetInData {
   private String brId;
   private List<String> brIds;
   private String type;
   private String memNewMix;
   private String menNewAuto;
   private String startCardNo;
   private String endCardNo;
   private String clientId;
   private int pageNum;
   private int pageSize;
}
