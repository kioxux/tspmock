package com.gys.business.service.data.auth;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午5:13
 */
@Data
public class RoleAuthAddReq implements Serializable {

    //角色名称
    @NotBlank(message = "角色名称不能为空")
    private String roleName;

    //选中的公司
    private List<String> stoCodes;

    //参考角色id集合
    @NotBlank(message = "必须选择一个参考角色")
    private String refRoleId;

    // 1表示通用角色 2表示自定义角色
    private Integer roleType;

    //选中的权限id集合
    private List<String> authIds;



}

