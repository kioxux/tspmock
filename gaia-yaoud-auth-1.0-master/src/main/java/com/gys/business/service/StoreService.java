package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.business.service.data.auth.UserRestrictInfo;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;

import java.util.List;
import java.util.Map;

public interface StoreService {
    List<StoreOutData> getStoreList(StoreInData inData);

    PageInfo<GetStoListOutData> getStoListByClientId(StoreInData inData);

    StoreOutData getStoreById(StoreInData inData);

    void insertStore(StoreInData inData, GetLoginOutData userInfo);

    void updateStoreStatus(StoreInData inData, GetLoginOutData userInfo);

    void updateStore(StoreInData inData, GetLoginOutData userInfo);

    void insertStoreGroup(StoreInData inData);

    void deleteStog(StoreInData inData);

    JsonResult exportIn(List<StoreExportInData> inData, GetLoginOutData outData) throws Exception;

    List<StogOutData> getStogList(StoreInData inData);

    List<StogOutData> getStoGroupList(StoreInData inData);

    PageInfo<StogOutData> getStogPage(StoreInData inData);

    List<String> getStoList(StoreInData inData);

    String getStoreCode(StoreInData inData);

    List<StoreOutData> selectAuthStoreList(String clientId, String userId);

    StoreOutData selectDefaultAuthStore(String clientId, String userId, GetLoginOutData userInfo);

    StoreOutData selectDefaultAuthStoreByToken(String clientId, String userId, GetLoginOutData userInfo);

    /**
     * 根据用户的数据权限来拉取用户数据权限对应的门店
     *
     * @param userInfo
     * @return
     */
    List<StoreRestrictBasicRes> getRestrictStoreList(GetLoginOutData userInfo);

   UserRestrictInfo getRestrictStoreListWithType(GetLoginOutData userInfo);
}
