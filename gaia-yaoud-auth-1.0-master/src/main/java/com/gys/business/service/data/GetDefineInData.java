package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class GetDefineInData {
   private String defineName;
   private String defineType;
   private Integer defineIndex;
   private String wfModule;
   private List<String> groupId;
   private String client;
   private BigDecimal seq;
   private String userId;
   private String defineCode;
   private String site;
}
