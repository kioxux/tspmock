package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaAuthConfigData;
import com.gys.business.mapper.entity.GroupRole;
import com.gys.business.service.UserRoleAuthService;
import com.gys.business.service.data.GetUserStoreOutData;
import com.gys.business.service.data.auth.*;
import com.gys.common.Constants;
import com.gys.common.data.JsonResult;
import com.gys.common.data.TreeNode;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtils;
import com.gys.util.TreeUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午11:21
 */
@Transactional
@Service
public class UserRoleAuthServiceImpl implements UserRoleAuthService {

    @Resource
    private RoleAuthMapper roleAuthMapper;

    @Resource
    private GroupRoleMapper groupRoleMapper;

    @Resource
    private AuthDetailMapper authDetailMapper;

    @Resource
    private BasicRoleAuthMapper basicRoleAuthMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaGroupBasicRoleMapper basicRoleMapper;

    @Resource
    private GaiaAuthConfigDataMapper gaiaAuthConfigDataMapper;

    @Override
    public JsonResult selectPermStoreAndRoleByUser(String client, String userId) {
        if (StringUtils.isBlank(userId)) {
            throw new BusinessException("用户编码不能为空!");
        }

        // 获取用户有权限的门店
        List<StoreRoleRes> permStoreByUserId = gaiaAuthConfigDataMapper.selectPermStoreByUserId(client, userId);
        Map<String, Integer> permStoreByUserMap = permStoreByUserId.stream()
                .collect(Collectors.toMap(StoreRoleRes::getStoCode, StoreRoleRes::getRoleNums));

        List<StoreRoleRes> storeRoleRes = new ArrayList<>();
        // 展示所有的门店
        List<GetUserStoreOutData> storeDatas = gaiaStoreDataMapper.selectStoreByCondition(client, null);
        for (GetUserStoreOutData storeData : storeDatas) {
            StoreRoleRes storeRole = new StoreRoleRes();
            storeRole.setStoCode(storeData.getStoCode());
            storeRole.setStoName(storeData.getStoName());
            storeRole.setRoleNums(MapUtils.getInteger(permStoreByUserMap, storeRole.getStoCode(), 0));
            storeRoleRes.add(storeRole);
        }
        return JsonResult.success(storeRoleRes);
    }

    @Override
    public JsonResult selectRolePermTree(String client, String roleType, String roleId) {
        Map<String, Object> resMap = new HashMap<>();
        List<String> chooseIds = new ArrayList<>();
        if (StringUtils.isBlank(roleId)) {
            throw new BusinessException("角色编码不能为空!");
        }

        List<String> authIds;
        if (Constants.BASIC_ROLE.equals(roleType)) {
            // 获取默认角色已分配的权限
            authIds = basicRoleAuthMapper.selectAuthIdByBasicRoleId(roleId);
        } else if (Constants.CUSTOM_ROLE.equals(roleType)) {
            // 获取自定义角色已分配的权限
            authIds = roleAuthMapper.selectAuthIdByRoleId(roleId);
        } else {
            throw new BusinessException("不合法的角色类型!");
        }

        // 获取权限明细
        List<AuthTree> authTrees = authDetailMapper.selectAuthTree();
        for (AuthTree authTree : authTrees) {
            if (authIds.contains(authTree.getId().trim())) {
                authTree.setChoose(true);
                chooseIds.add(authTree.getId());
            }
            authTree.setRemark("网页端");
            String authType = authTree.getAuthType();
            if (StringUtils.isNotBlank(authType)) {
                authTree.setLabel(Constants.AUTH_TYPE_MAP.get(authType));
            }
        }

        // 过滤根节点
        List<AuthTree> rootAuthTree = authTrees.stream()
                .filter(item -> StrUtil.isBlank(item.getPId()))
                .collect(Collectors.toList());
        // 过滤子节点
        List<AuthTree> childAuthTree = authTrees.stream()
                .filter(item -> !Objects.isNull(item.getPId()))
                .collect(Collectors.toList());

        // 组装树结构
        TreeUtil treeUtil = new TreeUtil(rootAuthTree, childAuthTree);
        List<TreeNode> tree = treeUtil.getTree();
        resMap.put("tree", tree);
        resMap.put("ids", chooseIds);
        return JsonResult.success(resMap);
    }

    @Override
    public JsonResult selectRoleInfoByStore(String client, String stoCode, String userId) {
        List<GroupRoleExt> resList = new ArrayList<>();
        List<String> chooseIds = new ArrayList<>();
        // 获取模块数据
        List<ModelClassify> modelClassifies = authDetailMapper.selectGlobalModel(Constants.SYSTEM_PARAM_MODEL, false);

        // 获取门店角色
        List<GroupRoleExt> groupRoleExts = basicRoleMapper.selectRoleByStore(client, stoCode, null);

        if (CollectionUtils.isNotEmpty(groupRoleExts)) {

            Map<String, String> map = modelClassifies.stream()
                    .collect(Collectors.toMap(ModelClassify::getModel, ModelClassify::getName));
            if (CollectionUtil.isNotEmpty(modelClassifies)) {
                for (ModelClassify modelClassify : modelClassifies) {
                    String model = modelClassify.getModel();
                    GroupRoleExt titleVo = new GroupRoleExt();
                    titleVo.setRoleName(modelClassify.getName());
                    titleVo.setId(model);

                    List<GroupRoleExt> temp = new ArrayList<>();
                    int chooseSum = 0;
                    for (GroupRoleExt groupRoleExt : groupRoleExts) {
                        // 姓名展示格式化
                        if (StrUtil.isNotBlank(groupRoleExt.getModel()) && groupRoleExt.getModel().equals(model)) {
                            String userName = groupRoleExt.getUserName();
                            if (StringUtils.isNotBlank(userName)) {
                                List<String> userNameList = Splitter.on(CharUtil.COMMA).splitToList(userName);
                                StringJoiner stringJoiner = new StringJoiner("");
                                int i = 0;
                                for (String name : userNameList) {
                                    stringJoiner.add(name).add("\t");
                                    i++;
                                    if (i % 3 == 0) {
                                        stringJoiner.add("\n");
                                    }
                                }
                                List<String> userIdList = Splitter.on(CharUtil.COMMA).splitToList(groupRoleExt.getUserId());
                                groupRoleExt.setChoose(userIdList.contains(userId));
                                groupRoleExt.setUserName(stringJoiner.toString());
                            } else {
                                groupRoleExt.setChoose(false);
                            }
                            if (groupRoleExt.getChoose()) {
                                chooseIds.add(groupRoleExt.getId());
                                chooseSum++;
                            }
                            groupRoleExt.setModelName(map.get(groupRoleExt.getModel()));
                            groupRoleExt.setPId(model);
                            temp.add(groupRoleExt);
                        }
                    }
                    titleVo.setChildren(temp);
                    titleVo.setChoose(temp.size() > 0 && temp.size() == chooseSum);
                    resList.add(titleVo);
                }
            }
            Map<String, Object> resMap = new HashMap<>();
            resMap.put("tree", resList);
            resMap.put("ids", chooseIds);
            return JsonResult.success(resMap);
        } else {
            if (CollectionUtil.isNotEmpty(modelClassifies)) {
                for (ModelClassify modelClassify : modelClassifies) {
                    String model = modelClassify.getModel();
                    GroupRoleExt titleVo = new GroupRoleExt();
                    titleVo.setRoleName(modelClassify.getName());
                    titleVo.setId(model);
                    titleVo.setChildren(new ArrayList<>());
                    resList.add(titleVo);
                }
            }
            Map<String, Object> resMap = new HashMap<>();
            resMap.put("tree", resList);
            resMap.put("ids", Lists.newArrayList());
            return JsonResult.success(resMap);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult saveAuthConfWithUser(AuthConfWithUser authConfWithUser) {
        String userId = authConfWithUser.getUserId();
        String client = authConfWithUser.getClient();

        List<AuthConfWithUser.StoreWithRole> stoCodeRoles = authConfWithUser.getStoCodeRoles();
        if (CollectionUtil.isNotEmpty(stoCodeRoles)) {
            List<String> stoCodes = stoCodeRoles.stream()
                    .map(AuthConfWithUser.StoreWithRole::getStoCode)
                    .collect(Collectors.toList());
            // 删除用户老的门店角色权限配置
            gaiaAuthConfigDataMapper.deleteAuthConfByUserId(client, stoCodes, userId);

            // 获取自定义角色名称
            GroupRole groupRole = new GroupRole();
            groupRole.setDeleteFlag(0);
            List<GroupRole> customRole = groupRoleMapper.select(groupRole);
            Map<String, String> roleMap = customRole.stream().collect(Collectors.toMap(GroupRole::getId, GroupRole::getRoleName));

            List<GaiaAuthConfigData> authConfigDatas = new ArrayList<>();

            String creator = authConfWithUser.getCreator();
            String creTime = DateUtils.getCurrentDateTimeStr("HHmmss");
            String creDate = DateUtils.getCurrentDateTimeStr("yyyyMMdd");
            for (AuthConfWithUser.StoreWithRole stoCodeRole : stoCodeRoles) {
                List<String> roleIds = stoCodeRole.getRoleIds();
                String stoCode = stoCodeRole.getStoCode();
                for (String roleId : roleIds) {
                    GaiaAuthConfigData authConfigData = new GaiaAuthConfigData();
                    authConfigData.setClient(client);
                    authConfigData.setAuthGroupId(roleId);
                    authConfigData.setAuthGroupName(roleMap.get(roleId));
                    authConfigData.setAuthobjSite(stoCode);
                    authConfigData.setAuthconfiUser(userId);
                    authConfigData.setCreDate(creDate);
                    authConfigData.setCreTime(creTime);
                    authConfigData.setCreId(creator);
                    authConfigDatas.add(authConfigData);
                }
            }

            if (CollectionUtils.isNotEmpty(authConfigDatas)) {
                gaiaAuthConfigDataMapper.insertBatch(authConfigDatas);
            }
        }
        return JsonResult.success();
    }

}

