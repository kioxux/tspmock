package com.gys.business.service.data;

import lombok.Data;

@Data
public class LoginQRCodeInData {
    private String qrCode;
    private String status;
    private String token;
    private int action;
}
