package com.gys.business.service.data.auth;

import lombok.Data;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2022年01月20日 上午9:59
 */
@Data
public class StoInfo {

    private String stoCode;

    private String stoAttribute;
}

