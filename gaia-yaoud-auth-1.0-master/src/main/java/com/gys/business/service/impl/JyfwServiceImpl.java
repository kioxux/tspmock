package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaJyfwDataMapper;
import com.gys.business.mapper.GaiaJyfwooDataMapper;
import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.mapper.entity.GaiaJyfwData;
import com.gys.business.mapper.entity.GaiaZzData;
import com.gys.business.service.JyfwService;
import com.gys.business.service.data.JyfwInData;
import com.gys.business.service.data.JyfwOutData;
import com.gys.business.service.data.JyfwooOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gys.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

@Service
public class JyfwServiceImpl implements JyfwService {
   @Autowired
   private GaiaFranchiseeMapper franchiseeMapper;
   @Autowired
   private GaiaJyfwDataMapper jyfwDataMapper;
   @Autowired
   private GaiaJyfwooDataMapper jyfwooDataMapper;

   public PageInfo<JyfwOutData> getJyfwList(JyfwInData inData) {
//      if (StringUtils.isEmpty(inData.getJyfwOrgid())|| StringUtils.isEmpty(inData.getJyfwType())){
//         throw new BusinessException("请选择组织");
//      }
      PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
      List<JyfwOutData> outData = this.jyfwDataMapper.getJyfwList(inData);
      PageInfo pageInfo;
      if (ObjectUtil.isNotEmpty(outData)) {
         pageInfo = new PageInfo(outData);
      } else {
         pageInfo = new PageInfo();
      }

      return pageInfo;
   }

   public JyfwOutData getJyfwById(JyfwInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaJyfwData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("jyfwOrgid", inData.getJyfwOrgid()).andEqualTo("jyfwId", inData.getJyfwId());
      GaiaJyfwData jyfwData = (GaiaJyfwData)this.jyfwDataMapper.selectOneByExample(example);
      JyfwOutData outData = new JyfwOutData();
      if (ObjectUtil.isNotEmpty(jyfwData)) {
         GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(jyfwData.getClient());
         outData.setClient(jyfwData.getClient());
         outData.setClientName(franchisee.getFrancName());
         outData.setJyfwId(jyfwData.getJyfwId());
         outData.setJyfwName(jyfwData.getJyfwName());
         outData.setJyfwOrgid(jyfwData.getJyfwOrgid());
         outData.setJyfwOrgname(jyfwData.getJyfwOrgname());
      }

      return outData;
   }

   @Transactional
   public void insertJyfw(JyfwInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaJyfwData.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("jyfwOrgid", inData.getJyfwOrgid()).andEqualTo("jyfwId", inData.getJyfwId());
      List<GaiaJyfwData> list = this.jyfwDataMapper.selectByExample(example);
      if (ObjectUtil.isNotEmpty(list)) {
         throw new BusinessException("该机构经营范围编码已存在，不要重复添加！");
      } else {
         GaiaJyfwData jyfwData = new GaiaJyfwData();
         jyfwData.setClient(inData.getClient());
         jyfwData.setJyfwId(inData.getJyfwId());
         jyfwData.setJyfwName(inData.getJyfwName());
         jyfwData.setJyfwOrgid(inData.getJyfwOrgid());
         jyfwData.setJyfwOrgname(inData.getJyfwOrgname());
         jyfwData.setJyfwCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         jyfwData.setJyfwCreTime(DateUtil.format(new Date(), "HHmmss"));
         jyfwData.setJyfwCreId(userInfo.getUserId());
         jyfwData.setJyfwModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         jyfwData.setJyfwModiTime(DateUtil.format(new Date(), "HHmmss"));
         jyfwData.setJyfwModiId(userInfo.getUserId());
         this.jyfwDataMapper.insert(jyfwData);
      }
   }

   @Transactional
   public void updateJyfw(JyfwInData inData, GetLoginOutData userInfo) {

      GaiaJyfwData jyfwData = new GaiaJyfwData();
      if(ObjectUtil.isEmpty(inData.getId())){
         Example example = new Example(GaiaJyfwData.class);
         example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("jyfwOrgid", inData.getJyfwOrgid()).andEqualTo("jyfwId", inData.getJyfwId());
         GaiaJyfwData jyfwOutData = (GaiaJyfwData)this.jyfwDataMapper.selectOneByExample(example);
         if (ObjectUtil.isNotEmpty(jyfwOutData)){
            throw new BusinessException(inData.getJyfwOrgname()+"已存在"+inData.getJyfwName()+"，不要重复添加！");
         }
         jyfwData.setId(Util.generateUUID());
         jyfwData.setClient(inData.getClient());
         jyfwData.setJyfwId(inData.getJyfwId());
         jyfwData.setJyfwName(inData.getJyfwName());
         jyfwData.setJyfwOrgid(inData.getJyfwOrgid());
         jyfwData.setJyfwOrgname(inData.getJyfwOrgname());
         jyfwData.setJyfwCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         jyfwData.setJyfwCreTime(DateUtil.format(new Date(), "HHmmss"));
         jyfwData.setJyfwCreId(userInfo.getUserId());
         jyfwData.setJyfwModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         jyfwData.setJyfwModiTime(DateUtil.format(new Date(), "HHmmss"));
         jyfwData.setJyfwModiId(userInfo.getUserId());
         this.jyfwDataMapper.insertSelective(jyfwData);
      }else {
         jyfwData.setId(inData.getId());
         jyfwData.setJyfwId(inData.getJyfwId());
         jyfwData.setJyfwName(inData.getJyfwName());
         jyfwData.setJyfwModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         jyfwData.setJyfwModiTime(DateUtil.format(new Date(), "HHmmss"));
         jyfwData.setJyfwModiId(userInfo.getUserId());
         this.jyfwDataMapper.updateByPrimaryKeySelective(jyfwData);
      }

   }

   @Transactional
   public void deleteJyfw(JyfwInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaJyfwData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andIn("jyfwOrgid", inData.getOrgIds()).andIn("jyfwId", inData.getIds());
      this.jyfwDataMapper.deleteByExample(example);
   }

   public List<JyfwooOutData> getJyfwooList() {
      List<JyfwooOutData> outData = this.jyfwooDataMapper.getJyfwooList();
      if (ObjectUtil.isEmpty(outData)) {
         outData = new ArrayList();
      }

      return (List)outData;
   }
}
