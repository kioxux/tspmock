package com.gys.business.service.impl;

import com.gys.business.service.CommonService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.GetCosParaOutData;
import com.gys.business.service.data.GetFileOutData;
import com.gys.util.CosUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CommonServiceImpl implements CommonService {
   @Autowired
   private ThirdParaService thirdParaService;

   public GetFileOutData fileUpload(MultipartFile file) {
      GetFileOutData outData = new GetFileOutData();
      GetCosParaOutData para = this.thirdParaService.getCosPara();
      String secretId = para.getSecretId();
      String secretKey = para.getSecretKey();
      String regionName = para.getRegionName();
      String bucketName = para.getBucketName();
      CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
      String path = cosUtil.upload(file);
      String url = cosUtil.getFileUrl(path);
      outData.setPath(path);
      outData.setUrl(url);
      return outData;
   }

   public String abc() {
      GetCosParaOutData para = this.thirdParaService.getCosPara();
      String secretId = para.getSecretId();
      String secretKey = para.getSecretKey();
      String regionName = para.getRegionName();
      String bucketName = para.getBucketName();
      CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
      return cosUtil.getFileUrl("20200710134436868/创新港微信公众号二维码.jpg");
   }
}
