package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaCompadmMapper;
import com.gys.business.mapper.GaiaDcDataMapper;
import com.gys.business.mapper.GaiaDepDataMapper;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.AuthconfiService;
import com.gys.business.service.FranchiseeService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.http.HttpClientUtil;
import com.gys.common.http.HttpJson;
import com.gys.common.redis.RedisManager;
import com.gys.util.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

@Service
public class FranchiseeServiceImpl implements FranchiseeService {
   @Autowired
   private GaiaFranchiseeMapper franchiseeMapper;
   @Autowired
   private GaiaUserDataMapper userDataMapper;
   @Autowired
   private GaiaCompadmMapper compadmMapper;
   @Autowired
   private GaiaStoreDataMapper storeDataMapper;
   @Autowired
   private GaiaDepDataMapper depDataMapper;
   @Autowired
   private GaiaDcDataMapper dcDataMapper;
   @Autowired
   private ThirdParaService thirdParaService;
   @Autowired
   private AuthconfiService authconfiService;
   @Autowired
   private RedisManager redisManager;

   private static final Logger log = LoggerFactory.getLogger(FranchiseeServiceImpl.class);

   public FranchiseeOutData getFranchiseeById(FranchiseeInData inData) {
      GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(inData.getClient());
      FranchiseeOutData outData = new FranchiseeOutData();
      if (ObjectUtil.isNotEmpty(franchisee)) {
         outData.setClient(franchisee.getClient());
         outData.setFrancAddr(franchisee.getFrancAddr());
         outData.setFrancLegalPerson(franchisee.getFrancLegalPerson());
         Example example;
         GaiaUserData userData;
         if (ObjectUtil.isNotEmpty(franchisee.getFrancLegalPerson())) {
            example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", franchisee.getClient()).andEqualTo("userId", franchisee.getFrancLegalPerson());
            userData = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNotNull(userData)) {
               outData.setFrancLegalPersonName(userData.getUserNam());
            }
         }

         if (StrUtil.isNotBlank(franchisee.getFrancLogo())) {
            GetCosParaOutData para = this.thirdParaService.getCosPara();
            String secretId = para.getSecretId();
            String secretKey = para.getSecretKey();
            String regionName = para.getRegionName();
            String bucketName = para.getBucketName();
            CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
            String url = cosUtil.getFileUrl(franchisee.getFrancLogo());
            outData.setFrancLogoUrl(url);
         }

         outData.setFrancLogo(franchisee.getFrancLogo());
         outData.setFrancName(franchisee.getFrancName());
         outData.setFrancNo(franchisee.getFrancNo());
         outData.setFrancQua(franchisee.getFrancQua());
         outData.setFrancAss(franchisee.getFrancAss());
         if (ObjectUtil.isNotEmpty(franchisee.getFrancAss())) {
            example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", franchisee.getClient()).andEqualTo("userId", franchisee.getFrancAss());
            userData = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNotNull(userData)) {
               outData.setFrancAssName(userData.getUserNam());
            }
         }

         outData.setFrancType1(franchisee.getFrancType1());
         outData.setFrancType2(franchisee.getFrancType2());
         outData.setFrancType3(franchisee.getFrancType3());
      }

      return outData;
   }

   @Transactional
   public void deleteFranchisee(FranchiseeInData inData) {
      if (ObjectUtil.isNotEmpty(inData.getClient())) {
         this.franchiseeMapper.deleteByPrimaryKey(inData.getClient());
      }

   }

   @Transactional
   public void insertFranchisee(FranchiseeInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaFranchisee.class);
      example.createCriteria().andEqualTo("francName", inData.getFrancName());
      List<GaiaFranchisee> list = this.franchiseeMapper.selectByExample(example);
      if (ObjectUtil.isNotEmpty(list)) {
         throw new BusinessException("加盟商名称已存在！");
      } else {
         int max = this.franchiseeMapper.selectMaxId();
         GaiaFranchisee franchisee = new GaiaFranchisee();
         franchisee.setClient(String.valueOf(max + 1));
         franchisee.setFrancAddr(inData.getFrancAddr());
         franchisee.setFrancLogo(inData.getFrancLogo());
         franchisee.setFrancName(inData.getFrancName());
         franchisee.setFrancNo(inData.getFrancNo());
         franchisee.setFrancQua(inData.getFrancQua());
         franchisee.setFrancAss(inData.getFrancAss());
         franchisee.setFrancType1(inData.getFrancType1());
         franchisee.setFrancType2(inData.getFrancType2());
         franchisee.setFrancType3(inData.getFrancType3());
         franchisee.setFrancCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         franchisee.setFrancCreTime(DateUtil.format(new Date(), "HHmmss"));
         franchisee.setFrancModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         franchisee.setFrancModiTime(DateUtil.format(new Date(), "HHmmss"));
         String password = RandomUtil.randomNumbers(6);
         inData.setUserPassword(CodecUtil.encryptMD5(password));
         GaiaUserData userData = this.saveAuthConfi(franchisee.getClient(), inData, userInfo);
         franchisee.setFrancLegalPerson(userData.getUserId());
         this.franchiseeMapper.insert(franchisee);
         List<String> groupList = new ArrayList();
         groupList.add("GAIA_AL_ADMIN");
         groupList.add("GAIA_AL_MANAGER");
         groupList.add("GAIA_WM_FZR");
         groupList.add("GAIA_MM_ZLZG");
         groupList.add("GAIA_MM_SCZG");
         groupList.add("SD_01");
         groupList.add("GAIA_FI_ZG");
         this.authconfiService.saveAuth(franchisee.getClient(), userData.getUserId(), groupList, (List)null, (String)null);
         GetSmsParaOutData para = this.thirdParaService.getSmsFranchisee();
         SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
         smsUtil.sendFranchisee(inData.getFrancPhone(), userData.getUserNam(), password);
      }
   }

   private GaiaUserData saveAuthConfi(String client, FranchiseeInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaUserData.class);
      example.createCriteria().andEqualTo("client", client).andEqualTo("userTel", inData.getFrancPhone());
      List<GaiaUserData> list = this.userDataMapper.selectByExample(example);
      if (ObjectUtil.isNotEmpty(list)) {
         throw new BusinessException("该负责人手机号已存在，不要重复创建！");
      } else {
         int userMax = this.userDataMapper.selectMaxId(userInfo.getClient());
         GaiaUserData userData = new GaiaUserData();
         userData.setUserId(String.valueOf(userMax + 1));
         userData.setClient(client);
         userData.setUserNam(inData.getFrancLegalPersonName());
         userData.setUserPassword(inData.getUserPassword());
         userData.setUserTel(inData.getFrancPhone());
         userData.setUserSta("0");
         userData.setUserLoginSta("0");
         userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
         userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
         this.userDataMapper.insert(userData);
         return userData;
      }
   }

   @Transactional
   public void updateFranchisee(FranchiseeInData inData, GetLoginOutData userInfo) {
      GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(inData.getClient());
      String oldLegal = franchisee.getFrancLegalPerson();
      String oldAss = franchisee.getFrancAss();
      franchisee.setFrancAddr(inData.getFrancAddr());
      franchisee.setFrancLegalPerson(inData.getFrancLegalPerson());
      franchisee.setFrancLogo(inData.getFrancLogo());
      franchisee.setFrancName(inData.getFrancName());
      franchisee.setFrancNo(inData.getFrancNo());
      franchisee.setFrancQua(inData.getFrancQua());
      franchisee.setFrancAss(inData.getFrancAss());
      franchisee.setFrancType1(inData.getFrancType1());
      franchisee.setFrancType2(inData.getFrancType2());
      franchisee.setFrancType3(inData.getFrancType3());
      franchisee.setFrancModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      franchisee.setFrancModiTime(DateUtil.format(new Date(), "HHmmss"));
      franchisee.setFrancModiId(userInfo.getUserId());
      this.franchiseeMapper.updateByPrimaryKeySelective(franchisee);
      List<String> groupList = new ArrayList();
      groupList.add("GAIA_AL_ADMIN");
      groupList.add("GAIA_AL_MANAGER");
      groupList.add("GAIA_WM_FZR");
      groupList.add("GAIA_MM_ZLZG");
      groupList.add("GAIA_MM_SCZG");
      groupList.add("SD_01");
      groupList.add("GAIA_FI_ZG");
      List<String> siteList = new ArrayList();
      List<GaiaStoreData> storeDataList = this.storeDataMapper.selectListForAuth(franchisee.getClient(), (String)null);
      Iterator var9 = storeDataList.iterator();

      while(var9.hasNext()) {
         GaiaStoreData storeData = (GaiaStoreData)var9.next();
         siteList.add(storeData.getStoCode());
      }

      List<GaiaDepData> depDataList = this.depDataMapper.selectListForAuth(franchisee.getClient(), (String)null);
      Iterator var12 = depDataList.iterator();

      while(var12.hasNext()) {
         GaiaDepData depData = (GaiaDepData)var12.next();
         siteList.add(depData.getDepId());
      }

      if (!oldLegal.equals(inData.getFrancLegalPerson())) {
         this.authconfiService.saveAuth(franchisee.getClient(), franchisee.getFrancLegalPerson(), groupList, siteList, oldLegal);
      }

      String delAss = "";
      if (StrUtil.isNotBlank(oldAss) && !oldAss.equals(inData.getFrancAss())) {
         delAss = oldAss;
      }

      this.authconfiService.saveAuth(franchisee.getClient(), franchisee.getFrancAss(), groupList, siteList, delAss);
   }

   public List<FranchiseeTycOutData> getFranchiseeList(FranchiseeInData inData) {
      List<FranchiseeTycOutData> list = new ArrayList();
      if (ObjectUtil.isNotEmpty(inData.getFrancName())) {
         String url = "http://open.api.tianyancha.com/services/open/search/2.0?word=" + inData.getFrancName();
         HttpJson httpJson = HttpClientUtil.doGet(url);
         if (ObjectUtil.isNotEmpty(httpJson.getData())) {
            JSONArray array = httpJson.getData().getJSONArray("items");

            for(int i = 0; i < array.size(); ++i) {
               JSONObject object = array.getJSONObject(i);
               FranchiseeTycOutData outData = new FranchiseeTycOutData();
               outData.setName(object.getString("name"));
               outData.setFrancLegalPerson(object.getString("legalPersonName"));
               list.add(outData);
            }
         }
      }

      return list;
   }

   public FranchiseeTycOutData baseInfo(FranchiseeInData inData) {
      FranchiseeTycOutData outData = new FranchiseeTycOutData();
      if (ObjectUtil.isNotEmpty(inData.getFrancName())) {
         String url = "http://open.api.tianyancha.com/services/open/ic/baseinfoV2/2.0?name=" + inData.getFrancName();
         HttpJson httpJson = HttpClientUtil.doGet(url);
         if (ObjectUtil.isNotEmpty(httpJson.getData())) {
            outData.setFrancAddr(httpJson.getData().getString("regLocation"));
            outData.setFrancLegalPerson(httpJson.getData().getString("legalPersonName"));
            outData.setFrancNo(httpJson.getData().getString("creditCode"));
         }
      }

      return outData;
   }

   public FranchiseeOutData getHomeFranchiseeInfo(GetLoginOutData loginOutData) {
      GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(loginOutData.getClient());
      if (ObjectUtil.isNull(franchisee)) {
         throw new BusinessException("提示：加盟商不存在");
      } else {
         FranchiseeOutData outData = new FranchiseeOutData();
         outData.setClient(franchisee.getClient());
         outData.setFrancName(franchisee.getFrancName());
         outData.setFrancType1(franchisee.getFrancType1());
         outData.setFrancType2(franchisee.getFrancType2());
         outData.setFrancType3(franchisee.getFrancType3());
         Example example = new Example(GaiaCompadm.class);
         example.createCriteria().andEqualTo("client", franchisee.getClient());
         List<GaiaCompadm> compadms = this.compadmMapper.selectByExample(example);
         List<CompadmOutData> compadmList = new ArrayList();
         Iterator var7 = compadms.iterator();

         while(var7.hasNext()) {
            GaiaCompadm compadm = (GaiaCompadm)var7.next();
            CompadmOutData compadmOutData = new CompadmOutData();
            compadmOutData.setCompadmId(compadm.getCompadmId());
            compadmOutData.setCompadmName(compadm.getCompadmName());
            compadmList.add(compadmOutData);
         }

         outData.setCompadmList(compadmList);
         return outData;
      }
   }

   @Override
   public void getSmsAuthenticationCode(GetLoginInData inData) {
      String code = RandomUtil.randomNumbers(6);
      String seq = RandomUtil.randomNumbers(2);
      GetSmsParaOutData para = this.thirdParaService.getPhoneCode();
      SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
      smsUtil.sendCaptcha(inData.getPhone(), code, seq);
      this.redisManager.set("franchisee_" + inData.getPhone(), code, UtilConst.PHONE_CODE_EXPIRE);
   }


   /**
    * 加盟商移动端注册
    * @param inData
    */
   @Override
   @Transactional
   public  void insertFranchiseeByPhone(FranchiseeInData inData){
      Object obj = this.redisManager.get("franchiseeByPhone_" +inData.getFrancPhone());
      if(StringUtils.isEmpty(inData.getFrancName())){
         throw new BusinessException("提示：请输入品牌名称");
      }
      if(StringUtils.isEmpty(inData.getFrancLegalPersonName())){
         throw new BusinessException("提示：请输入负责人");
      }
      if(StringUtils.isEmpty(inData.getFrancPhone())){
         throw new BusinessException("提示：请输入负责人手机号");
      }
      if(StringUtils.isEmpty(inData.getCode())){
         throw new BusinessException("提示：请输入验证码");
      }
      if(inData.getFrancType1().equals('1') && inData.getFrancType1().equals('2') && inData.getFrancType1().equals('3') ){
         throw new BusinessException("提示：至少选择一总公司性质");
      }
      if(ObjectUtil.isEmpty(obj)){
         throw new BusinessException("提示：短信验证码已过期");
      }else if(!inData.getCode().equals(obj.toString())) {
         throw new BusinessException("提示：短信验证码错误");
      }else {
         Example example = new Example(GaiaFranchisee.class);
         example.createCriteria().andEqualTo("francName", inData.getFrancName());
         List<GaiaFranchisee> list = this.franchiseeMapper.selectByExample(example);
         if (ObjectUtil.isNotEmpty(list)) {
            throw new BusinessException("提示：加盟商名称已存在！");
         } else {
            int max = this.franchiseeMapper.selectMaxId();
            GaiaFranchisee franchisee = new GaiaFranchisee();
            franchisee.setClient(String.valueOf(max + 1));
            franchisee.setFrancAddr(inData.getFrancAddr());
            franchisee.setFrancLogo(inData.getFrancLogo());
            franchisee.setFrancName(inData.getFrancName());
            franchisee.setFrancNo(inData.getFrancNo());
            franchisee.setFrancQua(inData.getFrancQua());
            franchisee.setFrancType1(inData.getFrancType1());
            franchisee.setFrancType2(inData.getFrancType2());
            franchisee.setFrancType3(inData.getFrancType3());
            franchisee.setFrancCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            franchisee.setFrancCreTime(DateUtil.format(new Date(), "HHmmss"));
            franchisee.setFrancModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            franchisee.setFrancModiTime(DateUtil.format(new Date(), "HHmmss"));
            //是同一人
            GaiaUserData userLegal = null;
            GaiaUserData userAss = null;
            userLegal = this.saveAuthConfigByLegal(franchisee.getClient(), inData);
            franchisee.setFrancModiId(userLegal.getUserId());
            franchisee.setFrancLegalPerson(userLegal.getUserId());

            if(inData.getFrancPhone().equals(inData.getFrancLegalPerson())){
               franchisee.setFrancAss(userLegal.getUserId());
               franchisee.setFrancLegalPerson(userLegal.getUserId());
            }
            else {
                userLegal = this.saveAuthConfigByLegal(franchisee.getClient(), inData); //负责人账号
                userAss = this.saveAuthConfigByAss(franchisee.getClient(), inData);//委托人账号
               franchisee.setFrancModiId(userLegal.getUserId());
               franchisee.setFrancAss(userAss.getUserId());
            }


            this.franchiseeMapper.insertSelective(franchisee);
         }
      }
   }


   /**
    * 加盟商移动端修改
    * @param inData
    */
   @Override
   @Transactional
   public void updateFranchiseeByPhone( UserLoginModelClient currentUser,FranchiseeInData inData) {
      if(StringUtils.isEmpty(inData.getFrancName())){
         throw new BusinessException("提示：请输入品牌名称");
      }
      if(StringUtils.isEmpty(inData.getFrancLegalPerson())){
         throw new BusinessException("提示：请选择负责人");
      }
//      if(StringUtils.isEmpty(inData.getFrancAss())){
//         throw new BusinessException("提示：请选择委托人");
//      }else
      if(inData.getFrancType1().equals('0') && inData.getFrancType2().equals('0') && inData.getFrancType3().equals('0') ){
         throw new BusinessException("提示：至少选择一总公司性质");
      }else {
         GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(currentUser.getClient());
         String oldLegal = franchisee.getFrancLegalPerson();
         String oldAss = franchisee.getFrancAss();
         franchisee.setFrancAddr(inData.getFrancAddr());
         franchisee.setFrancLegalPerson(inData.getFrancLegalPerson());
         franchisee.setFrancLogo(inData.getFrancLogo());
         franchisee.setFrancName(inData.getFrancName());
         franchisee.setFrancNo(inData.getFrancNo());
         franchisee.setFrancQua(inData.getFrancQua());
         franchisee.setFrancAss(inData.getFrancAss());
         franchisee.setFrancType1(inData.getFrancType1());
         franchisee.setFrancType2(inData.getFrancType2());
         franchisee.setFrancType3(inData.getFrancType3());
         franchisee.setFrancModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         franchisee.setFrancModiTime(DateUtil.format(new Date(), "HHmmss"));
         franchisee.setFrancModiId(currentUser.getUserId());
         this.franchiseeMapper.updateByPrimaryKeySelective(franchisee);
         List<String> groupList = new ArrayList();
         groupList.add("GAIA_AL_ADMIN");
         groupList.add("GAIA_AL_MANAGER");
         groupList.add("GAIA_WM_FZR");
         groupList.add("GAIA_MM_ZLZG");
         groupList.add("GAIA_MM_SCZG");
         groupList.add("SD_01");
         groupList.add("GAIA_FI_ZG");
         List<String> siteList = new ArrayList();
         List<GaiaStoreData> storeDataList = this.storeDataMapper.selectListForAuth(franchisee.getClient(), (String)null);
         Iterator var9 = storeDataList.iterator();

         while(var9.hasNext()) {
            GaiaStoreData storeData = (GaiaStoreData)var9.next();
            siteList.add(storeData.getStoCode());
         }

         List<GaiaDepData> depDataList = this.depDataMapper.selectListForAuth(franchisee.getClient(), (String)null);
         Iterator var12 = depDataList.iterator();

         while(var12.hasNext()) {
            GaiaDepData depData = (GaiaDepData)var12.next();
            siteList.add(depData.getDepId());
         }

         if (!oldLegal.equals(inData.getFrancLegalPerson())) {
            this.authconfiService.saveAuth(franchisee.getClient(), franchisee.getFrancLegalPerson(), groupList, siteList, oldLegal);
         }

         String delAss = "";
         if (StrUtil.isNotBlank(oldAss) && !oldAss.equals(inData.getFrancAss())) {
            delAss = oldAss;
         }

         this.authconfiService.saveAuth(franchisee.getClient(), franchisee.getFrancAss(), groupList, siteList, delAss);
      }
   }

   @Override
   public FranchiseeOutData getFranchiseeByIdPhone(UserLoginModelClient currentUser,FranchiseeInData inData){
      GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(currentUser.getClient());
      FranchiseeOutData outData = new FranchiseeOutData();
      if (ObjectUtil.isNotEmpty(franchisee)) {
         outData.setClient(franchisee.getClient());
         outData.setFrancAddr(franchisee.getFrancAddr());
         outData.setFrancLegalPerson(franchisee.getFrancLegalPerson());
         Example example;
         GaiaUserData userDataQua;
         GaiaUserData userDataAss;

         if (ObjectUtil.isNotEmpty(franchisee.getFrancLegalPerson())) {
            example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", franchisee.getClient()).andEqualTo("userId", franchisee.getFrancLegalPerson());
            userDataQua = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNotNull(userDataQua)) {
               outData.setFrancLegalPerson(userDataQua.getUserId());
               outData.setFrancLegalPersonName(userDataQua.getUserNam());
               outData.setFrancLegalPersonPhone(userDataQua.getUserTel());
            }
         }

         if (StrUtil.isNotBlank(franchisee.getFrancLogo())) {
            GetCosParaOutData para = this.thirdParaService.getCosPara();
            String secretId = para.getSecretId();
            String secretKey = para.getSecretKey();
            String regionName = para.getRegionName();
            String bucketName = para.getBucketName();
            CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
            String url = cosUtil.getFileUrl(franchisee.getFrancLogo());
            outData.setFrancLogoUrl(url);
         }

         outData.setFrancLogo(franchisee.getFrancLogo());
         outData.setFrancName(franchisee.getFrancName());
         outData.setFrancNo(franchisee.getFrancNo());
         outData.setFrancQua(franchisee.getFrancQua());
         outData.setFrancAss(franchisee.getFrancAss());
         if (ObjectUtil.isNotEmpty(franchisee.getFrancAss())) {
            example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", franchisee.getClient()).andEqualTo("userId", franchisee.getFrancAss());
            userDataAss = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNotNull(userDataAss)) {
               outData.setFrancAss(userDataAss.getUserId());
               outData.setFrancAssName(userDataAss.getUserNam());
               outData.setFrancAssPhone(userDataAss.getUserTel());

            }
         }

         outData.setFrancType1(franchisee.getFrancType1());
         outData.setFrancType2(franchisee.getFrancType2());
         outData.setFrancType3(franchisee.getFrancType3());
      }

      return outData;
   }

   /**
    * 创建委托人
    * @param client
    * @param inData
    * @return
    */
   @Transactional
   public GaiaUserData saveAuthConfigByAss(String client, FranchiseeInData inData) {
      Example exampleAss = new Example(GaiaUserData.class);
      exampleAss.createCriteria().andEqualTo("client", client).andEqualTo("userTel", inData.getFrancAssPhone());
      List<GaiaUserData> listAss = this.userDataMapper.selectByExample(exampleAss);
      String password = RandomUtil.randomNumbers(6);
      GaiaUserData userData = new GaiaUserData();
      userData.setUserPassword(CodecUtil.encryptMD5(password));
      userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
      userData.setDepId(inData.getFrancNo());
      userData.setDepName(inData.getFrancName());
      if (ObjectUtil.isNotEmpty(listAss)) {
//         throw new BusinessException("该委托人手机号已存在，不要重复创建！");
         Example exampleUpd = new Example(GaiaUserData.class);
         exampleUpd.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("userId", inData.getFrancAssPhone());
         this.userDataMapper.updateByExampleSelective(userData,exampleUpd);
      }  else {
         int userMax = this.userDataMapper.selectMaxId(client);
         userData.setUserId(String.valueOf(userMax + 1));
         userData.setClient(client);
         userData.setUserNam(inData.getFrancAssName());
//         userData.setUserPassword(password);
         userData.setUserTel(inData.getFrancAssPhone());
         userData.setUserSta("0");
         userData.setUserLoginSta("0");
         userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
//         userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
//         userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
         this.userDataMapper.insert(userData);
         this.saveAuthConfig(userData);
         System.out.println("phone:"+userData.getUserTel());
         this.sendSms(userData,password,"2");
      }
      return userData;

   }
   @Transactional
   public GaiaUserData saveAuthConfigByLegal(String client, FranchiseeInData inData) {
      Example example = new Example(GaiaUserData.class);
      example.createCriteria().andEqualTo("client", client).andEqualTo("userTel", inData.getFrancPhone());
      List<GaiaUserData> list = this.userDataMapper.selectByExample(example);
      String password =RandomUtil.randomNumbers(6) ;
      GaiaUserData userData = new GaiaUserData();
      userData.setUserPassword(CodecUtil.encryptMD5(password));
      userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
      userData.setDepId(inData.getFrancNo());
      userData.setDepName(inData.getFrancName());
      if (ObjectUtil.isNotEmpty(list)) {
//         throw new BusinessException("该负责人手机号已存在，不要重复创建！");
         Example exampleUpd = new Example(GaiaUserData.class);
         exampleUpd.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("userId", inData.getFrancAssPhone());
         this.userDataMapper.updateByExampleSelective(userData,exampleUpd);
      } else {
         int userMax = this.userDataMapper.selectMaxId(client);
         userData.setUserId(String.valueOf(userMax + 1));
         userData.setClient(client);
         userData.setUserNam(inData.getFrancLegalPersonName());
//         userData.setUserPassword(password);
         userData.setUserTel(inData.getFrancPhone());
         userData.setUserSta("0");
         userData.setUserLoginSta("0");
         userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
         this.userDataMapper.insert(userData);
         this.saveAuthConfig(userData);
         System.out.println("phone:"+userData.getUserTel());
         this.sendSms(userData,password,"1");
      }
      return userData;

   }

   @Transactional
   public void saveAuthConfig(GaiaUserData userData){
      List<String> groupList = new ArrayList();
      groupList.add("GAIA_AL_ADMIN");
      groupList.add("GAIA_AL_MANAGER");
      groupList.add("GAIA_WM_FZR");
      groupList.add("GAIA_MM_ZLZG");
      groupList.add("GAIA_MM_SCZG");
      groupList.add("SD_01");
      groupList.add("GAIA_FI_ZG");
      this.authconfiService.saveAuth(userData.getClient(), userData.getUserId(), groupList, (List)null, (String)null);
   }

   @Transactional
   public void sendSms(GaiaUserData userData,String password,String type){
      //1是发负责人  2是发委托人
      GetSmsParaOutData para = this.thirdParaService.getSmsFranchiseeNew(type);
      SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
      smsUtil.sendFranchisee(userData.getUserTel(), userData.getUserNam(), password);
   }

   @Override
   public void getSmsAuthenticationCodeByPhone(FranchiseeInData inData) {
      log.info("加盟商注册发送验证码："+inData.getFrancPhone());
      String code = RandomUtil.randomNumbers(6);
      String seq = RandomUtil.randomNumbers(2);
      GetSmsParaOutData para = this.thirdParaService.getPhoneCode();
      SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
      smsUtil.sendCaptcha(inData.getFrancPhone(), code, seq);
      this.redisManager.set("franchiseeByPhone_" + inData.getFrancPhone(), code, UtilConst.PHONE_CODE_EXPIRE);
   }
}
