package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.AuthconfiService;
import com.gys.business.service.DepartmentService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.dep.dto.DepDto;
import com.gys.business.service.data.dep.vo.DepVo;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.UtilConst;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

@Service
public class DepartmentServiceImpl implements DepartmentService {
   @Autowired
   private GaiaDepDataMapper depDataMapper;
   @Autowired
   private GaiaUserDataMapper userDataMapper;
   @Autowired
   private GaiaUserDataMapper gaiaUserDataMapper;
   @Autowired
   private GaiaStoreDataMapper storeDataMapper;
   @Autowired
   private GaiaDcDataMapper dcDataMapper;
   @Autowired
   private AuthconfiService authconfiService;
   @Autowired
   private DcServiceImpl dcService;
   @Autowired
   private GaiaCompadmMapper compadmMapper;
   @Autowired
   private GaiaCompadmWmsMapper compadmWmsMapper;
   public List<DeptOutData> getDeptList(DeptInData inData) {
      List<DeptOutData> depts = new ArrayList();
      if (ObjectUtil.isNotEmpty(inData.getClient())) {
         depts = this.depDataMapper.getDeptList(inData.getClient());
      }

      return (List)depts;
   }


   public List<GaiaDepData> getDeptListByType(DeptInData inData) {
      Example example = new Example(GaiaDepData.class);
      example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("depStatus", "1");
      example.orderBy("depId").asc();
      List<GaiaDepData> depList = this.depDataMapper.selectByExample(example);
      return depList;
   }

   public DeptOutData getDeptById(String depId, GetLoginOutData userInfo) {
      GaiaDepData inData =new GaiaDepData();
      inData.setClient(userInfo.getClient());
      inData.setDepId(depId);
      GaiaDepData depData = (GaiaDepData)this.depDataMapper.selectByPrimaryKey(inData);
      DeptOutData outData = new DeptOutData();

//      DeptOutData outData = new DeptOutData();
      if (ObjectUtil.isNotEmpty(depData)) {
          outData = new DeptOutData();
         BeanUtil.copyProperties(depData,outData);
//         outData.setClient(depData.getClient());
//         outData.setDepHeadId(depData.getDepHeadId());
//         outData.setDepHeadNam(depData.getDepHeadNam());
//         outData.setDepId(depData.getDepId());
//         outData.setDepName(depData.getDepName());
//         outData.setDepDisDate(depData.getDepDisDate());
         UserOutData userList = this.userDataMapper.selectByDeptId(depData.getClient(),depData.getDepHeadId());
         if(depData.getDepType().equals("20")){
            Example example = new Example(GaiaCompadm.class);
            example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("compadmId", depData.getStoChainHead());
            GaiaCompadm compadm = (GaiaCompadm)this.compadmMapper.selectOneByExample(example);
            outData.setCompadmId(depData.getStoChainHead());
            outData.setCompadmName(compadm.getCompadmName());
         }if (depData.getDepType().equals("30")){
            Example example = new Example(GaiaCompadmWms.class);
            example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("compadmId", depData.getStoChainHead());
            GaiaCompadmWms compadmWms = (GaiaCompadmWms)this.compadmWmsMapper.selectOneByExample(example);
            outData.setCompadmId(depData.getStoChainHead());
            outData.setCompadmName(compadmWms.getCompadmName());
         }
         outData.setDepHeadPhone(userList.getUserTel());
      }

      return outData;
   }

   @Transactional
   public void disableDept(DeptInData inData, GetLoginOutData userInfo) {
      if (ObjectUtil.isNotEmpty(inData.getDepId())) {
         inData.setClient(userInfo.getClient());
         GaiaDepData depData = (GaiaDepData)this.depDataMapper.selectByPrimaryKey(inData);
         depData.setDepDisDate(DateUtil.format(new Date(), "yyyyMMdd"));
         depData.setDepStatus("0");
         this.depDataMapper.updateByPrimaryKeySelective(depData);
      }

   }

   public void enableDept(DeptInData inData, GetLoginOutData userInfo) {
      if (ObjectUtil.isNotEmpty(inData.getDepId())) {
         inData.setClient(userInfo.getClient());
         GaiaDepData depData = (GaiaDepData)this.depDataMapper.selectByPrimaryKey(inData);
         depData.setDepStatus("1");
         this.depDataMapper.updateByPrimaryKey(depData);
      }

   }

   @Transactional
   public GaiaDepData insertDept(DeptInData inData, GetLoginOutData userInfo) {
      GaiaDepData depData = new GaiaDepData();
      depData.setClient(userInfo.getClient());
      depData.setDepName(inData.getDepName());

      if(inData.getDepCls().equals("10")){
         Example example = new Example(GaiaDepData.class);
         example.createCriteria().andEqualTo("client", depData.getClient()).andEqualTo("stoChainHead", inData.getCompadmId());
         int count = this.depDataMapper.selectCountByExample(example);
         if(count>0){
            throw new BusinessException("提示："+depData.getDepName()+"已存在,请误重复创建");
         }
      }
      if (ObjectUtil.isNotEmpty(inData.getStoChainHead())) {
         depData.setStoChainHead(inData.getStoChainHead());
      }

      List<GaiaDepData> out = this.depDataMapper.select(depData);
      if (ObjectUtil.isNotEmpty(out)) {
         throw new BusinessException("提示：该部门已存在");
      } else {
         int max = this.depDataMapper.selectMaxId(userInfo.getClient());
         depData.setDepId(String.valueOf(max + 1));
         depData.setDepHeadId(inData.getDepHeadId());
         GaiaUserData gaiaUserData = new GaiaUserData();
         gaiaUserData.setClient(userInfo.getClient());
         gaiaUserData.setUserId(inData.getDepHeadId());
         gaiaUserData = (GaiaUserData)this.gaiaUserDataMapper.selectByPrimaryKey(gaiaUserData);
         depData.setDepHeadNam(gaiaUserData.getUserNam());

         depData.setDepCls(inData.getDepCls());
         depData.setDepCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
         depData.setDepCreTime(DateUtil.format(new Date(), "HHmmss"));
         depData.setDepCreId(userInfo.getUserId());
         depData.setDepModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
         depData.setDepModiTime(DateUtil.format(new Date(), "HHmmss"));
         depData.setDepModiId(userInfo.getUserId());
         depData.setDepStatus("1");
         depData.setDepType(inData.getDepType());

         List<String> groupList = new ArrayList();
         if ("11".equals(inData.getDepCls())) {
            groupList.add("GAIA_MM_ZLZG");
         }

         if ("12".equals(inData.getDepCls())) {
            groupList.add("GAIA_MM_SCZG");
         }

         if ("13".equals(inData.getDepCls())) {
            groupList.add("SD_01");
         }

         if ("14".equals(inData.getDepCls())) {
            groupList.add("GAIA_FI_ZG");
         }

         if ("16".equals(inData.getDepCls())) {
            groupList.add("GAIA_AL_ADMIN");
         }

         if ("15".equals(inData.getDepCls())) {
            groupList.add("GAIA_AL_ADMIN");
            groupList.add("GAIA_WM_FZR");
            groupList.add("GAIA_MM_ZLZG");
            groupList.add("GAIA_MM_SCZG");
            groupList.add("SD_01");
            groupList.add("GAIA_FI_ZG");
         }

         if ("17".equals(inData.getDepCls())) {
            groupList.add("GAIA_MM_SCZG");
         }

         if ("10".equals(inData.getDepCls())) {
            DcIndata dcData = new DcIndata();
            dcData.setClientId(userInfo.getClient());
            dcData.setDcName(inData.getDepName());
            dcData.setDcCode(String.valueOf(max + 1));
            dcData.setDcHeadId(inData.getDepHeadId());
            dcData.setDcHeadName(inData.getDepHeadNam());
            dcData.setStoChainHead(inData.getCompadmId());
            depData.setStoChainHead(inData.getCompadmId());
            dcService.insertDc(dcData,userInfo);
            if ("20".equals(inData.getDepType())) {
               groupList.add("GAIA_WM_FZR");

            }else if("30".equals(inData.getDepType())) {
               groupList.add("GAIA_AL_ADMIN");
               groupList.add("GAIA_AL_MANAGER");
               groupList.add("GAIA_WM_FZR");
               groupList.add("GAIA_MM_SCZG");
               groupList.add("GAIA_MM_ZLZG");
               groupList.add("SD_01");
               groupList.add("GAIA_FI_ZG");
            }
         }
         this.depDataMapper.insert(depData);
         gaiaUserData.setDepId(inData.getDepId());
         gaiaUserData.setDepName(inData.getDepName());
         this.gaiaUserDataMapper.updateByPrimaryKey(gaiaUserData);


         List<String> siteList = new ArrayList();
         siteList.add(depData.getDepId());
//         List<GaiaStoreData> storeDataList = this.storeDataMapper.selectListForAuth(depData.getClient(), depData.getStoChainHead());
//         Iterator var10 = storeDataList.iterator();

//         while(var10.hasNext()) {
//            GaiaStoreData storeData = (GaiaStoreData)var10.next();
//            siteList.add(storeData.getStoCode());
//         }

//         List<GaiaDepData> depDataList = this.depDataMapper.selectListForAuth(depData.getClient(), depData.getStoChainHead());
//         Iterator var14 = depDataList.iterator();

//         while(var14.hasNext()) {
//            GaiaDepData dep = (GaiaDepData)var14.next();
//            siteList.add(dep.getDepId());
//         }

         this.authconfiService.saveAuth(depData.getClient(), depData.getDepHeadId(), groupList, siteList, (String)null);
         return depData;
      }
   }

   @Transactional
   public void updateDept(DeptInData inData, GetLoginOutData userInfo) {
      inData.setClient(userInfo.getClient());
      GaiaDepData depData = (GaiaDepData)this.depDataMapper.selectByPrimaryKey(inData);
      String oldHeadId = depData.getDepHeadId();
      depData.setDepName(inData.getDepName());
      depData.setDepHeadId(inData.getDepHeadId());
      GaiaUserData gaiaUserData = new GaiaUserData();
      gaiaUserData.setClient(userInfo.getClient());
      gaiaUserData.setUserId(inData.getDepHeadId());
      gaiaUserData = (GaiaUserData)this.gaiaUserDataMapper.selectByPrimaryKey(gaiaUserData);
      depData.setDepHeadNam(gaiaUserData.getUserNam());
      depData.setDepCls(inData.getDepCls());
      depData.setDepStatus(inData.getDepStatus());
      depData.setDepModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      depData.setDepModiTime(DateUtil.format(new Date(), "HHmmss"));
      depData.setDepModiId(userInfo.getUserId());
      depData.setDepType(inData.getDepType());
      if ("20".equals(inData.getDepType()) || "30".equals(inData.getDepType())) {
         DcIndata dcData = new DcIndata();
         dcData.setClientId(userInfo.getClient());
         dcData.setDcName(inData.getDepName());
         dcData.setDcCode(inData.getDepId());
         dcData.setDcHeadId(inData.getDepHeadId());
         dcData.setDcHeadName(inData.getDepHeadNam());
         dcData.setStoChainHead(inData.getCompadmId());
         depData.setStoChainHead(inData.getCompadmId());
         dcService.upDateDc(dcData,userInfo);
      }
      this.depDataMapper.updateByPrimaryKeySelective(depData);



      if (!oldHeadId.equals(inData.getDepHeadId())) {
         List<String> groupList = new ArrayList();
         if ("11".equals(inData.getDepCls())) {
            groupList.add("GAIA_MM_ZLZG");
         }

         if ("12".equals(inData.getDepCls())) {
            groupList.add("GAIA_MM_SCZG");
         }

         if ("13".equals(inData.getDepCls())) {
            groupList.add("SD_01");
         }

         if ("14".equals(inData.getDepCls())) {
            groupList.add("GAIA_FI_ZG");
         }

         if ("16".equals(inData.getDepCls())) {
            groupList.add("GAIA_AL_ADMIN");
         }

         if ("15".equals(inData.getDepCls())) {
            groupList.add("GAIA_AL_ADMIN");
            groupList.add("GAIA_WM_FZR");
            groupList.add("GAIA_MM_ZLZG");
            groupList.add("GAIA_MM_SCZG");
            groupList.add("SD_01");
            groupList.add("GAIA_FI_ZG");
         }

         if ("17".equals(inData.getDepCls())) {
            groupList.add("GAIA_MM_SCZG");
         }

         if ("10".equals(inData.getDepCls())) {

            if ("20".equals(inData.getDepType())) {
               groupList.add("GAIA_WM_FZR");

            }else if("30".equals(inData.getDepType())) {
               groupList.add("GAIA_AL_ADMIN");
               groupList.add("GAIA_AL_MANAGER");
               groupList.add("GAIA_WM_FZR");
               groupList.add("GAIA_MM_SCZG");
               groupList.add("GAIA_MM_ZLZG");
               groupList.add("SD_01");
               groupList.add("GAIA_FI_ZG");
            }
         }

         gaiaUserData.setDepId(depData.getDepId());
         gaiaUserData.setDepName(depData.getDepName());
         this.gaiaUserDataMapper.updateByPrimaryKey(gaiaUserData);
         //添加账号权限
         List<String> siteList = new ArrayList();
//         List<GaiaStoreData> storeDataList = this.storeDataMapper.selectListForAuth(depData.getClient(),inData.getStoChainHead());
//         Iterator var9 = storeDataList.iterator();
//
//         while(var9.hasNext()) {
//            GaiaStoreData storeData = (GaiaStoreData)var9.next();
//            siteList.add(storeData.getStoCode());
//         }
//
//         List<GaiaDepData> depDataList = this.depDataMapper.selectListForAuth(depData.getClient(), inData.getStoChainHead());
//         Iterator var13 = depDataList.iterator();
////
//         while(var13.hasNext()) {
//            GaiaDepData dep = (GaiaDepData)var13.next();
//            siteList.add(dep.getDepId());
//         }

//         Example example;
//         List<GaiaDcData> dcDataList;
//         if ("10".equals(depData.getDepType())) {
//            example = new Example(GaiaDcData.class);
//            example.createCriteria().andEqualTo("client", depData.getClient());
//            dcDataList = this.dcDataMapper.selectByExample(example);
//            dcDataList.forEach((x) -> {
//               siteList.add(x.getDcCode());
//            });
//         }
//
//         if ("20".equals(depData.getDepType())) {
//            example = new Example(GaiaDcData.class);
//            example.createCriteria().andEqualTo("client", depData.getClient()).andEqualTo("dcChainHead", depData.getStoChainHead());
//            dcDataList = this.dcDataMapper.selectByExample(example);
//            dcDataList.forEach((x) -> {
//               siteList.add(x.getDcCode());
//            });
//         }
//
//         if ("30".equals(depData.getDepType())) {
//            siteList.add(depData.getStoChainHead());
//         }

         this.authconfiService.saveAuth(inData.getClient(), depData.getDepHeadId(), groupList, siteList, oldHeadId);
      }

   }

   @Transactional
   public JsonResult exportIn(List<DeptExportInData> inData, GetLoginOutData userInfo) throws Exception {
      if (ObjectUtil.isEmpty(inData)) {
         return JsonResult.fail(UtilConst.CODE_1001, "需要导入的门店为空");
      } else {
         Iterator var3 = inData.iterator();

         DeptExportInData exportInData;
         while(var3.hasNext()) {
            exportInData = (DeptExportInData)var3.next();
            if (ObjectUtil.isEmpty(exportInData.getDepName())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的部门名称不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getDepHeadNam())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的部门负责人姓名不能为空");
            }

            if (ObjectUtil.isEmpty(exportInData.getDepType())) {
               return JsonResult.fail(UtilConst.CODE_1001, "第" + exportInData.getIndex() + "行的部门类型不能为空");
            }
         }

         var3 = inData.iterator();

         while(var3.hasNext()) {
            exportInData = (DeptExportInData)var3.next();
            int max = this.depDataMapper.selectMaxId(userInfo.getClient());
            GaiaDepData depData = new GaiaDepData();
            depData.setDepId(String.valueOf(max + 1));
            depData.setClient(userInfo.getClient());
            depData.setDepName(exportInData.getDepName());
            depData.setDepHeadNam(exportInData.getDepHeadNam());
            depData.setDepType(exportInData.getDepType());
            depData.setDepCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            depData.setDepCreTime(DateUtil.format(new Date(), "HHmmss"));
            depData.setDepCreId(userInfo.getUserId());
            depData.setDepModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            depData.setDepModiTime(DateUtil.format(new Date(), "HHmmss"));
            depData.setDepModiId(userInfo.getUserId());
            this.depDataMapper.insert(depData);
         }

         return JsonResult.success((Object)null, "提示：上传成功！");
      }
   }

   @Override
   public List<DepVo> getAllDeptList(DepDto dto) {
      List<DepVo> vos = depDataMapper.getAllDeptList(dto);
      return vos;
   }
}
