package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateCusGspInData {
   @Field(
      name = "客户编码"
   )
   @JSONField(
      ordinal = 1
   )
   private String cusCode = "";
   @Field(
      name = "客户名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String cusName = "";
   @Field(
      name = "营业执照编号"
   )
   @JSONField(
      ordinal = 3
   )
   private String cusCreditCode = "";
   @Field(
      name = "营业期限"
   )
   @JSONField(
      ordinal = 4
   )
   private String cusCreditDate = "";
   @Field(
      name = "法人"
   )
   @JSONField(
      ordinal = 5
   )
   private String cusLegalPerson = "";
   @Field(
      name = "注册地址"
   )
   @JSONField(
      ordinal = 6
   )
   private String cusRegAdd = "";
   @Field(
      name = "许可证编号"
   )
   @JSONField(
      ordinal = 7
   )
   private String cusLicenceNo = "";
   @Field(
      name = "发证单位"
   )
   @JSONField(
      ordinal = 9
   )
   private String cusLicenceOrgan = "";
   @Field(
      name = "发证日期"
   )
   @JSONField(
      ordinal = 10
   )
   private String cusLicenceDate = "";
   @Field(
      name = "有效期至"
   )
   @JSONField(
      ordinal = 12
   )
   private String cusLicenceValid = "";
   @Field(
      name = "邮政编码"
   )
   @JSONField(
      ordinal = 13
   )
   private String cusPostalCode = "";
   @Field(
      name = "税务登记证"
   )
   @JSONField(
      ordinal = 14
   )
   private String cusTaxCard = "";
   @Field(
      name = "组织机构代码证"
   )
   @JSONField(
      ordinal = 15
   )
   private String cusOrgCard = "";
   @Field(
      name = "开户户名"
   )
   @JSONField(
      ordinal = 15
   )
   private String cusAccountName = "";
   @Field(
      name = "开户银行"
   )
   @JSONField(
      ordinal = 16
   )
   private String cusAccountBank = "";
   @Field(
      name = "银行账号"
   )
   @JSONField(
      ordinal = 17
   )
   private String cusBankAccount = "";
   @Field(
      name = "随货同行单（票）情况"
   )
   @JSONField(
      ordinal = 18
   )
   private String cusDocState = "";
   @Field(
      name = "企业印章情况"
   )
   @JSONField(
      ordinal = 19
   )
   private String cusSealState = "";
   @Field(
      name = "生产或经营范围"
   )
   @JSONField(
      ordinal = 20
   )
   private String cusScope = "";
   @Field(
      name = "经营方式"
   )
   @JSONField(
      ordinal = 21
   )
   private String cusOperationMode = "";
}
