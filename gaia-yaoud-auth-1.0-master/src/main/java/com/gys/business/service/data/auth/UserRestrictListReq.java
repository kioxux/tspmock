package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 用户添加数据权限新增入参实体类
 * @author: flynn
 * @date: 2021年12月27日 下午4:44
 */
@Data
public class UserRestrictListReq implements Serializable {

    private String userId;

    private String stoName;

    private String stoAttribute;

    private String stoProvince;

    private String stoCity;

    //当指定片区数据时，可以选中多个片区
    private List<String> districts;

    private String client;

    private String restrictType;

    private String searchFlag;

}

