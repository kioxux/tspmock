package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
@ApiModel(value = "工作流详情列表")
public class GetWorkflowOutData {
   @ApiModelProperty(value = "是否为新版报表 0:否 1:是")
   private int isNewReport = 1;

   @ApiModelProperty(value = "是否为批号盘点 0:否 1:是")
   private int isBatch;

   @ApiModelProperty(value = "门店名称")
   private String storeName;

   @ApiModelProperty(value = "门店编码")
   private String storeCode;

   private String wfNewWorkflowDetail;

   @ApiModelProperty(value = " ")
   private BigDecimal wfCode;

   @ApiModelProperty(value = "")
   private String wfKind;

   @ApiModelProperty(value = "")
   private String wfTitle;

   @ApiModelProperty(value = "")
   private String wfDescription;

   @ApiModelProperty(value = "")
   private String wfStatus;

   @ApiModelProperty(value = "")
   private String wfJsonString;

   @ApiModelProperty(value = "")
   private Integer wfSeq;

   @ApiModelProperty(value = "")
   private String currentOperation;

   @ApiModelProperty(value = "")
   private String wfRefPage;

   @ApiModelProperty(value = "")
   private String wfOrder;

   @ApiModelProperty(value = "加盟商")
   private String clientId;

   @ApiModelProperty(value = "发起人姓名")
   private String fullName;

   private String defineCode;

   private String createDate;

   @ApiModelProperty(value = "门店/部门/配送中心")
   private String wfSite;

   @ApiModelProperty(value = "工作流进度详情steps")
   private List<GetWorkflowApproveOutData> approveList;

   private List<String> imgs;
   private String gsishBranch;
   private String gsishRemark;
}
