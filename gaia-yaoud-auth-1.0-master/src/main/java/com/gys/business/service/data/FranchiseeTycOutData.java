package com.gys.business.service.data;

import lombok.Data;

@Data
public class FranchiseeTycOutData {
   private String name;
   private String francNo;
   private String francLegalPerson;
   private String francAddr;
}
