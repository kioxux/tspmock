package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateSupGspInData {
   @Field(
      name = "供应商自编码"
   )
   @JSONField(
      ordinal = 1
   )
   private String supSelfCode = "";
   @Field(
      name = "供应商名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String supName = "";
   @Field(
      name = "营业执照编号"
   )
   @JSONField(
      ordinal = 3
   )
   private String supCreditCode = "";
   @Field(
      name = "营业期限"
   )
   @JSONField(
      ordinal = 4
   )
   private String supCreditDate = "";
   @Field(
      name = "法人"
   )
   @JSONField(
      ordinal = 5
   )
   private String supLegalPerson = "";
   @Field(
      name = "注册地址"
   )
   @JSONField(
      ordinal = 6
   )
   private String supRegAdd = "";
   @Field(
      name = "许可证编号"
   )
   @JSONField(
      ordinal = 7
   )
   private String supLicenceNo = "";
   @Field(
      name = "发证单位"
   )
   @JSONField(
      ordinal = 9
   )
   private String supLicenceOrgan = "";
   @Field(
      name = "发证日期"
   )
   @JSONField(
      ordinal = 10
   )
   private String supLicenceDate = "";
   @Field(
      name = "有效期至"
   )
   @JSONField(
      ordinal = 12
   )
   private String supLicenceValid = "";
   @Field(
      name = "邮政编码"
   )
   @JSONField(
      ordinal = 13
   )
   private String supPostalCode = "";
   @Field(
      name = "税务登记证"
   )
   @JSONField(
      ordinal = 14
   )
   private String supTaxCard = "";
   @Field(
      name = "组织机构代码证"
   )
   @JSONField(
      ordinal = 15
   )
   private String supOrgCard = "";
   @Field(
      name = "开户户名"
   )
   @JSONField(
      ordinal = 15
   )
   private String supAccountName = "";
   @Field(
      name = "开户银行"
   )
   @JSONField(
      ordinal = 16
   )
   private String supAccountBank = "";
   @Field(
      name = "银行账号"
   )
   @JSONField(
      ordinal = 17
   )
   private String supBankAccount = "";
   @Field(
      name = "随货同行单（票）情况"
   )
   @JSONField(
      ordinal = 18
   )
   private String supDocState = "";
   @Field(
      name = "企业印章情况"
   )
   @JSONField(
      ordinal = 19
   )
   private String supSealState = "";
   @Field(
      name = "生产或经营范围"
   )
   @JSONField(
      ordinal = 20
   )
   private String supScope = "";
   @Field(
      name = "经营方式"
   )
   @JSONField(
      ordinal = 21
   )
   private String supOperationMode = "";
}
