package com.gys.business.service.data;

import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCkbhgsppdInData {
    @Field(
            name = "配送中心名称"
    )
    private String wmsCkMc;

    @Field(
            name = "类型"
    )
    private String wmsLx;

    @Field(
            name = "移位单号"
    )
    private String wsdYwdh;

    @Field(
            name = "申请日期"
    )
    private String wmsSqrq;

    @Field(
            name = "申请人"
    )
    private String wmsSqr;

    @Field(
            name = "商品编码"
    )
    private String wsdSpBm;

    @Field(
            name = "商品规格"
    )
    private String wsdSpGg;

    @Field(
            name = "商品名称"
    )
    private String wsdSpMc;

    @Field(
            name = "批号"
    )
    private String wsdPh;

    @Field(
            name = "生产厂家"
    )
    private String wsdSpCj;

    @Field(
            name = "单位"
    )
    private String wsdDw;

    @Field(
            name = "生产日期"
    )
    private String wsdScrq;

    @Field(
            name = "有效期"
    )
    private String wsdYxq;

    @Field(
            name = "申请数量"
    )
    private String wsdTzsl;

    @Field(
            name = "原因"
    )
    private String wmsYy;

    @Field(
            name = "原货位号"
    )
    private String wsdYhw;

    @Field(
            name = "目的货位号"
    )
    private String wsdMdhw;

    @Field(
            name = "参考成本价"
    )
    private String wsdCkcbj;

    @Field(
            name = "参考成本额"
    )
    private String wsdCkcbe;

    @Field(
            name = "供应商编号"
    )
    private String wmsGysBh;

    @Field(
            name = "供应商名称"
    )
    private String wmsGysMc;

    @Field(
            name = "质量部意见"
    )
    private String comment;
}
