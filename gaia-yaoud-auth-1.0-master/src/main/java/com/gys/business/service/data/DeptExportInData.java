package com.gys.business.service.data;

import lombok.Data;

@Data
public class DeptExportInData {
   private String depName;
   private String depHeadNam;
   private String depType;
   private String index;
}
