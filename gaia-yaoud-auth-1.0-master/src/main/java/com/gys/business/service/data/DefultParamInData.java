package com.gys.business.service.data;

import lombok.Data;

@Data
public class DefultParamInData {
   private Integer queryType;
   private String gsspId;
   private String gsspName;
   private String gsspParaRemark;
   private String gsspPara;
   public int pageSize;
   private int pageNum;

   public DefultParamInData() {
   }

   public DefultParamInData(Integer queryType, String gsspId, String gsspName, String gsspParaRemark, String gsspPara, int pageSize, int pageNum) {
      this.queryType = queryType;
      this.gsspId = gsspId;
      this.gsspName = gsspName;
      this.gsspParaRemark = gsspParaRemark;
      this.gsspPara = gsspPara;
      this.pageSize = pageSize;
      this.pageNum = pageNum;
   }

   public static DefultParamInData.DefultParamInDataBuilder builder() {
      return new DefultParamInData.DefultParamInDataBuilder();
   }

   public String toString() {
      return "DefultParamInData(queryType=" + this.getQueryType() + ", gsspId=" + this.getGsspId() + ", gsspName=" + this.getGsspName() + ", gsspParaRemark=" + this.getGsspParaRemark() + ", gsspPara=" + this.getGsspPara() + ", pageSize=" + this.getPageSize() + ", pageNum=" + this.getPageNum() + ")";
   }

   public static class DefultParamInDataBuilder {
      private Integer queryType;
      private String gsspId;
      private String gsspName;
      private String gsspParaRemark;
      private String gsspPara;
      private int pageSize;
      private int pageNum;

      public DefultParamInData.DefultParamInDataBuilder queryType(final Integer queryType) {
         this.queryType = queryType;
         return this;
      }

      public DefultParamInData.DefultParamInDataBuilder gsspId(final String gsspId) {
         this.gsspId = gsspId;
         return this;
      }

      public DefultParamInData.DefultParamInDataBuilder gsspName(final String gsspName) {
         this.gsspName = gsspName;
         return this;
      }

      public DefultParamInData.DefultParamInDataBuilder gsspParaRemark(final String gsspParaRemark) {
         this.gsspParaRemark = gsspParaRemark;
         return this;
      }

      public DefultParamInData.DefultParamInDataBuilder gsspPara(final String gsspPara) {
         this.gsspPara = gsspPara;
         return this;
      }

      public DefultParamInData.DefultParamInDataBuilder pageSize(final int pageSize) {
         this.pageSize = pageSize;
         return this;
      }

      public DefultParamInData.DefultParamInDataBuilder pageNum(final int pageNum) {
         this.pageNum = pageNum;
         return this;
      }

      public DefultParamInData build() {
         return new DefultParamInData(this.queryType, this.gsspId, this.gsspName, this.gsspParaRemark, this.gsspPara, this.pageSize, this.pageNum);
      }

   }
}
