package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 前端下拉需要list返回，封装通用类型
 */
@Data
public class CommonVo implements Serializable {
    private static final long serialVersionUID = -6311116521300067518L;

    private String label;

    private Object value;

    public CommonVo(String lable, String value) {
        this.label = lable;
        this.value = value;
    }
}
