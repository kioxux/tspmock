package com.gys.business.service;

import com.gys.business.service.data.DefultParamInData;
import com.gys.business.service.data.DefultParamOutData;
import com.gys.business.service.data.ParamDetailInData;
import com.gys.business.service.data.ParamDetailOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface ParamService {
   void addDefaultParam(DefultParamInData inData, GetLoginOutData userInfo);

   void updateDefaultParam(DefultParamInData inData, GetLoginOutData userInfo);

   PageInfo<DefultParamOutData> getDefaultParamList(DefultParamInData inData);

   List getParamDetailList(ParamDetailInData inData);

   void updateParamDetail(ParamDetailInData inData, GetLoginOutData userInfo);

   List<ParamDetailOutData> getDefaultParam(ParamDetailInData inData);
}
