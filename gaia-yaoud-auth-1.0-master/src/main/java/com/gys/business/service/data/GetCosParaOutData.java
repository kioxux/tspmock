package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetCosParaOutData {
   private String secretId;
   private String secretKey;
   private String regionName;
   private String bucketName;
}
