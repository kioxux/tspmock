package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class IntegralInData {
   private String clientId;
   private String gsiasBrId;
   private String gsiasBrName;
   private BigDecimal gsiasIntegralMinAmt;
   private String gsiasIntegralMax;
   private String gsiasChangeSet;
   private BigDecimal gsiasAmtPm1;
   private String gsiasIntegralPm1;
   private BigDecimal gsiasAmtPm2;
   private String gsiasIntegralPm2;
   private BigDecimal gsiasAmtPm3;
   private String gsiasIntegralPm3;
   private BigDecimal gsiasAmtPm4;
   private String gsiasIntegralPm4;
   private BigDecimal gsiasAmtPm5;
   private String gsiasIntegralPm5;
   private BigDecimal gsiasAmtPm6;
   private String gsiasIntegralPm6;
}
