package com.gys.business.service.data;

import lombok.Data;

@Data
public class DcIndata {
   private String clientId;
   private String dcName;
   private String dcCode;
   private String dcHeadId;
   private String dcHeadName;
   private String compadmId;
   private String dcType;
   private String stoChainHead;
}
