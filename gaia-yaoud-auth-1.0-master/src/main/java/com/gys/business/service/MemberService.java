package com.gys.business.service;

import com.gys.business.service.data.CashInData;
import com.gys.business.service.data.CashOutData;
import com.gys.business.service.data.ExchangeExportInData;
import com.gys.business.service.data.ExchangeInData;
import com.gys.business.service.data.ExchangeOutData;
import com.gys.business.service.data.GetMemberOutData;
import com.gys.business.service.data.GetMemberPreSetImportInData;
import com.gys.business.service.data.GetMemberPreSetInData;
import com.gys.business.service.data.GetMemberPreSetOutData;
import com.gys.business.service.data.GetMemberSetImportInData;
import com.gys.business.service.data.IntegralInData;
import com.gys.business.service.data.IntegralOutData;
import com.gys.business.service.data.MemberInData;
import com.gys.business.service.data.MemberTypeOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public interface MemberService {
   void typeSet(MemberInData inData);

   List<MemberTypeOutData> getMemberClass(GetLoginOutData inData);

   boolean integralSet(IntegralInData inData);

   IntegralOutData getIntegralSet(IntegralInData inData);

   PageInfo<ExchangeOutData> exchangeSetList(ExchangeInData inData);

   ExchangeOutData getExchangeSet(ExchangeInData inData);

   boolean approveExchangeSet(ExchangeInData inData);

   boolean updateExchangeSet(ExchangeInData inData);

   boolean addExchangeSet(ExchangeInData inData);

   PageInfo<CashOutData> cashSetList(CashInData inData);

   boolean updateCashSet(CashInData inData, GetLoginOutData userInfo);

   boolean addCashSet(CashInData inData, GetLoginOutData userInfo);

   void exportOut(HttpServletResponse response, ExchangeInData inData) throws Exception;

   JsonResult exportIn(List<ExchangeExportInData> inData, String client) throws Exception;

   String getVirtualMaxCard(GetLoginOutData userInfo);

   GetMemberPreSetOutData getVirtualStoreSet(GetMemberPreSetInData inData, GetLoginOutData userInfo);

   void saveMemberPre(GetMemberPreSetInData inData, GetLoginOutData userInfo);

   void importMember(List<GetMemberPreSetImportInData> inData, GetLoginOutData userInfo);

   void importMemberSet(List<GetMemberSetImportInData> inData, GetLoginOutData userInfo);

   PageInfo<GetMemberOutData> selectNewCardList(GetMemberPreSetInData inData, GetLoginOutData userInfo);

   void autoCreateVirtualCard();

    void export(HttpServletResponse response, CashInData inData) throws Exception;
}
