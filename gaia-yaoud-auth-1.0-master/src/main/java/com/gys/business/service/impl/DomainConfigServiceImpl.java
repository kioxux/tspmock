package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaDomainConfigMapper;
import com.gys.business.mapper.entity.GaiaDomainConfig;
import com.gys.business.service.DomainConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import java.awt.*;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/4 15:07
 * @Version 1.0.0
 **/
@Service
public class DomainConfigServiceImpl implements DomainConfigService {
    @Autowired
    private GaiaDomainConfigMapper domainConfigMapper;

    @Override
    public List<GaiaDomainConfig> getAll(){
       return domainConfigMapper.selectAll();
    }

}
