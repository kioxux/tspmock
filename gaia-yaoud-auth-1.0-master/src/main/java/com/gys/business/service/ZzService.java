package com.gys.business.service;

import com.gys.business.service.data.ZzInData;
import com.gys.business.service.data.ZzOutData;
import com.gys.business.service.data.ZzooOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface ZzService {
   PageInfo<ZzOutData> getZzList(ZzInData inData);

   ZzOutData getZzById(ZzInData inData, GetLoginOutData userInfo);

   void insertZz(ZzInData inData, GetLoginOutData userInfo);

   void updateZz(ZzInData inData, GetLoginOutData userInfo);

   void deleteZz(ZzInData inData, GetLoginOutData userInfo);

   List<ZzooOutData> getZzooList();
}
