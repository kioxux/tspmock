package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfDefineUserOutData {
    private String userId;

    private String userAuthType;

    private String userAuthSite;
}
