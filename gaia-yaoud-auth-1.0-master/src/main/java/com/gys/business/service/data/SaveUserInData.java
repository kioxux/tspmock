package com.gys.business.service.data;

import lombok.Data;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 11:06 2021/11/23
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class SaveUserInData extends UserInData{
    private String operateUser;
}
