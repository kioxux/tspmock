package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class MemberInData {
   private String clientId;
   private String userId;
   private List<String> memberIds;
   private String type;
   private List<MemberTypeInData> memberTypeList;
   private String gsiasBrId;
}
