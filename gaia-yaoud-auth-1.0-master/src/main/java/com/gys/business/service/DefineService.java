package com.gys.business.service;

import com.gys.business.service.data.GetDefineInData;
import com.gys.business.service.data.GetDefineOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface DefineService {
   List<GetDefineOutData> selectDefaultList(GetDefineInData inData, GetLoginOutData userInfo);

   void updateDefaultGroup(GetDefineInData inData, GetLoginOutData userInfo);
}
