package com.gys.business.service.data;

import com.gys.business.mapper.entity.Area;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AreaDto extends Area {

    private List<AreaDto> children;

    private Integer stoNum;

    private boolean ifUserChoose;

    private String id;

}
