package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface FranchiseeService {
   FranchiseeOutData getFranchiseeById(FranchiseeInData inData);

   void insertFranchisee(FranchiseeInData inData, GetLoginOutData userInfo);

   void deleteFranchisee(FranchiseeInData inData);

   void updateFranchisee(FranchiseeInData inData, GetLoginOutData userInfo);

   List<FranchiseeTycOutData> getFranchiseeList(FranchiseeInData inData);

   FranchiseeTycOutData baseInfo(FranchiseeInData inData);

   FranchiseeOutData getHomeFranchiseeInfo(GetLoginOutData loginOutData);

   void getSmsAuthenticationCode(GetLoginInData loginOutData);

    void insertFranchiseeByPhone(FranchiseeInData inData);

   void updateFranchiseeByPhone(UserLoginModelClient currentUser, FranchiseeInData inData);

   FranchiseeOutData getFranchiseeByIdPhone(UserLoginModelClient currentUser,FranchiseeInData inData);

   void getSmsAuthenticationCodeByPhone(FranchiseeInData loginOutData);

}
