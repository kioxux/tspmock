package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetSampleWorkflowApproveOutData {
    @ApiModelProperty(value = "进度序号，从0开始")
    private BigDecimal wfSeq;

    @ApiModelProperty(value = "审核人姓名")
    private String approverName;

    @ApiModelProperty(value = "审核日期")
    private String createDate;
}
