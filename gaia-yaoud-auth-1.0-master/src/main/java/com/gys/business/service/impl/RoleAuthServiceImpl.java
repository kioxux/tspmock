package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.base.Splitter;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaGroupBasicRole;
import com.gys.business.mapper.entity.GroupRole;
import com.gys.business.mapper.entity.RoleAuth;
import com.gys.business.service.RoleAuthService;
import com.gys.business.service.data.GetUserStoreOutData;
import com.gys.business.service.data.auth.*;
import com.gys.common.Constants;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午3:24
 */
@Transactional
@Service
public class RoleAuthServiceImpl implements RoleAuthService {

    @Resource
    private AuthDetailMapper authDetailMapper;

    @Resource
    private BasicRoleAuthMapper basicRoleAuthMapper;

    @Resource
    private GroupRoleMapper groupRoleMapper;

    @Resource
    private GaiaGroupBasicRoleMapper basicRoleMapper;

    @Resource
    private RoleAuthMapper roleAuthMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    private static final String split_separator = "-";


    private String generateRoleCode() {
        String res = "";
        res = UUID.randomUUID().toString().replaceAll("-","");
        return res;
    }

    @Override
    public List<StoreRoleRes> getStoreAndRols(GetLoginOutData userInfo) {
        List<StoreRoleRes> clientAllStoreInfo = this.getClientAllStoreInfo(userInfo.getClient());
        return clientAllStoreInfo;
    }


    private List<GroupRole> checkRepeatNameInStores(String client, String roleName, List<String> stoCodes,String selfId) {
        List<GroupRole> resList = new ArrayList<>();
        List<GroupRole> roles = groupRoleMapper.getRepeatNameCountInStores(client, roleName, stoCodes,selfId);
        if (CollectionUtil.isNotEmpty(roles)) {
            resList = roles;
        }
        return resList;
    }


    @Override
    public Object addRoleAuth(GetLoginOutData userInfo, List<RoleAuthAddReq> inData) {
        if (CollectionUtil.isEmpty(inData)) {
            throw new BusinessException("请传递合法信息!");
        }
        for (RoleAuthAddReq req : inData) {
            if (StrUtil.isBlank(req.getRefRoleId())) {
                throw new BusinessException("请至少选中一个参考角色!");
            }

            if (req.getRoleType() == null) {
                throw new BusinessException("请传递参考角色的类型!");
            }
            if(req.getRoleName().length()>30){
                throw new BusinessException("角色名称长度不能超过30!");
            }
            String basicRoleId = "";
            Boolean isAdmin = null;
            if (req.getRoleType() == 1) {
                //基础角色
                basicRoleId = req.getRefRoleId();
                GaiaGroupBasicRole query = new GaiaGroupBasicRole();
                query.setCode(req.getRefRoleId());
                GaiaGroupBasicRole groupRole = basicRoleMapper.selectOne(query);
                isAdmin = groupRole.getIsAdmin();
            } else if (req.getRoleType() == 2) {
                //自定义角色
                GroupRole query = new GroupRole();
                query.setId(req.getRefRoleId());
                GroupRole groupRole = groupRoleMapper.selectOne(query);
                if (groupRole == null) {
                    throw new BusinessException("查无此数据!");
                }
                basicRoleId = groupRole.getBasicRole();
                isAdmin = groupRole.getIsAdmin();
            } else {
                throw new BusinessException("请传递合法的角色的类型!");
            }

            //添加角色
            List<GroupRole> repeatRoles = checkRepeatNameInStores(userInfo.getClient(), req.getRoleName(), req.getStoCodes(),null);
            if (CollectionUtil.isEmpty(repeatRoles)) {
                //开始添加新角色
                if (CollectionUtil.isNotEmpty(req.getStoCodes())) {
                    List<GroupRole> dbRoleInsertList = new ArrayList<>();
                    List<RoleAuth> dbRoleAuthInsertList = new ArrayList<>();
                    for (String stoCode : req.getStoCodes()) {
                        List<GetUserStoreOutData> stores = storeDataMapper.selectStoreByCondition(userInfo.getClient(), stoCode);
                        if (CollectionUtil.isEmpty(stores)) {
                            throw new BusinessException("存在非法的公司编码!");
                        }
                        String roleId = generateRoleCode();
                        GroupRole dbRoleInsert = new GroupRole();
                        dbRoleInsert.setId(roleId);
                        dbRoleInsert.setRoleName(req.getRoleName());
                        dbRoleInsert.setClient(userInfo.getClient());
                        dbRoleInsert.setCompCode(stoCode);
                        dbRoleInsert.setCompType(stores.get(0).getStoAttribute());
                        dbRoleInsert.setBasicRole(basicRoleId);
                        dbRoleInsert.setDeleteFlag(0);
                        dbRoleInsert.setCreDate(DateUtils.getCurrentDate());
                        dbRoleInsert.setCreTime(DateUtils.getCurrentTime());
                        dbRoleInsert.setCreId(userInfo.getUserId());
                        dbRoleInsert.setIsAdmin(isAdmin);
                        dbRoleInsertList.add(dbRoleInsert);


                        //处理权限
                        if (CollectionUtil.isNotEmpty(req.getAuthIds())) {
                            for (String authId : req.getAuthIds()) {
                                RoleAuth dbRoleAuthInsert = new RoleAuth();
                                dbRoleAuthInsert.setRoleId(roleId);
                                dbRoleAuthInsert.setAuthId(authId);
                                dbRoleAuthInsert.setDeleteFlag(false);
                                dbRoleAuthInsert.setCreDate(DateUtils.getCurrentDate());
                                dbRoleAuthInsert.setCreTime(DateUtils.getCurrentTime());
                                dbRoleAuthInsert.setCreId(userInfo.getUserId());
                                dbRoleAuthInsertList.add(dbRoleAuthInsert);
                            }

                        }
                    }
                    groupRoleMapper.insertList(dbRoleInsertList);
                    if(CollectionUtil.isNotEmpty(dbRoleAuthInsertList)){
                        roleAuthMapper.insertList(dbRoleAuthInsertList);
                    }
                }
            } else {
                String compCode = repeatRoles.get(0).getCompCode();
                List<GetUserStoreOutData> stores = storeDataMapper.selectStoreByCondition(userInfo.getClient(), compCode);
                if (CollectionUtil.isEmpty(stores)) {
                    throw new BusinessException("存在非法的公司编码!");
                }
                throw new BusinessException("公司" + stores.get(0).getStoName() + "下已经存在名为" + req.getRoleName() + "的角色！");
            }
        }
        return null;
    }

    @Override
    public Object deleteStoreRole(GetLoginOutData userInfo, List<StoreRoleReq> inData) {
        if (CollectionUtil.isEmpty(inData)) {
            throw new BusinessException("请至少选中一家门店!");
        }

        for(StoreRoleReq req : inData){
            if(CollectionUtil.isEmpty(req.getRoles())){
                for(StroeRoleDelDetail role: req.getRoles()){
                    if(1==role.getRoleType()){
                        throw new BusinessException("存在基础角色，不能删除!");
                    }
                }
            }
        }
        for (StoreRoleReq roleReq : inData) {
            groupRoleMapper.deleteByStoCoceAndRoles(userInfo.getClient(), roleReq.getStoCode(), roleReq.getRoles());
        }
        return null;
    }

    @Override
    public Object resetStoreRole(GetLoginOutData userInfo, StoreRoleReq inData) {

        List<StroeRoleDelDetail> roles = inData.getRoles();
        if (CollectionUtil.isNotEmpty(roles)) {
            for (StroeRoleDelDetail delDetail : roles) {
                Integer roleType = delDetail.getRoleType();
                if (roleType == null) {
                    throw new BusinessException("请传递合法的角色类型");
                }
                String basicRoleId = "";
                if (roleType == 1) {
                    //基础角色情况下不需要重制
                    throw new BusinessException("不能重置基础角色!");
//                    continue;
                } else if (roleType == 2) {
                    //自定义角色
                    GroupRole query = new GroupRole();
                    query.setId(delDetail.getRoleId());
                    GroupRole groupRole = groupRoleMapper.selectOne(query);
                    if (groupRole == null) {
                        throw new BusinessException("查无此数据!");
                    }
                    basicRoleId = groupRole.getBasicRole();
                } else {
                    throw new BusinessException("请传递合法的角色类型");
                }
                List<String> basicAuthIds = basicRoleAuthMapper.selectAuthIdByBasicRoleId(basicRoleId);
                if (CollectionUtil.isNotEmpty(basicAuthIds)) {
                    List<RoleAuth> dbRoleAuthInsertList = new ArrayList<>();
                    //首先需要去删除原来自定义角色的关系,逻辑删除
                    roleAuthMapper.deleteByRoleId(delDetail.getRoleId());
                    for (String authId : basicAuthIds) {
                        //构建新的关系
                        RoleAuth dbRoleAuthInsert = new RoleAuth();
                        dbRoleAuthInsert.setRoleId(delDetail.getRoleId());
                        dbRoleAuthInsert.setAuthId(authId);
                        dbRoleAuthInsert.setDeleteFlag(false);
                        dbRoleAuthInsert.setCreDate(DateUtils.getCurrentDate());
                        dbRoleAuthInsert.setCreTime(DateUtils.getCurrentTime());
                        dbRoleAuthInsert.setCreId(userInfo.getUserId());
                        dbRoleAuthInsertList.add(dbRoleAuthInsert);
                    }
                    if (CollectionUtil.isNotEmpty(dbRoleAuthInsertList)) {
                        roleAuthMapper.insertList(dbRoleAuthInsertList);
                    }
                }
            }
        } else {
            throw new BusinessException("请至少选中一个角色");
        }

        return null;
    }

    @Override
    public Object updateStoreRole(GetLoginOutData userInfo, StoreRoleUpdateReq inData) {
        if (StrUtil.isNotBlank(inData.getRoleId())) {
            Integer roleType = inData.getRoleType();
            if (roleType == null) {
                throw new BusinessException("请传递合法的角色类型");
            }
            if (roleType == 1) {
                //基础角色情况下不需要重制，直接过滤循环
                throw new BusinessException("基础角色不能进行修改");
            } else if (roleType == 2) {
                //自定义角色
                GroupRole query = new GroupRole();
                query.setId(inData.getRoleId());
                GroupRole groupRole = groupRoleMapper.selectOne(query);
                if (groupRole == null) {
                    throw new BusinessException("查无此数据!");
                }
                List<GroupRole> repeatRoles = checkRepeatNameInStores(userInfo.getClient(), inData.getRoleName(), Arrays.asList(inData.getStoCode()),groupRole.getId());
                if(CollectionUtil.isNotEmpty(repeatRoles)){
                    String compCode = repeatRoles.get(0).getCompCode();
                    List<GetUserStoreOutData> stores = storeDataMapper.selectStoreByCondition(userInfo.getClient(), compCode);
                    if (CollectionUtil.isEmpty(stores)) {
                        throw new BusinessException("存在非法的公司编码!");
                    }
                    throw new BusinessException("公司" + stores.get(0).getStoName() + "下已经存在名为" + inData.getRoleName() + "的角色！");
                }
                groupRole.setRoleName(inData.getRoleName());
                groupRole.setModDate(DateUtils.getCurrentDate());
                groupRole.setModId(userInfo.getUserId());
                groupRole.setModTime(DateUtils.getCurrentTime());
                groupRoleMapper.updateRoleName(groupRole);
            } else {
                throw new BusinessException("请传递合法的角色类型");
            }
            //首先需要去删除原来自定义角色的关系,逻辑删除
            roleAuthMapper.deleteByRoleId(inData.getRoleId());
            if (CollectionUtil.isNotEmpty(inData.getAuthIds())) {
                List<RoleAuth> dbRoleAuthInsertList = new ArrayList<>();
                for (String authId : inData.getAuthIds()) {
                    //构建新的关系
                    RoleAuth dbRoleAuthInsert = new RoleAuth();
                    dbRoleAuthInsert.setRoleId(inData.getRoleId());
                    dbRoleAuthInsert.setAuthId(authId);
                    dbRoleAuthInsert.setDeleteFlag(false);
                    dbRoleAuthInsert.setCreDate(DateUtils.getCurrentDate());
                    dbRoleAuthInsert.setCreTime(DateUtils.getCurrentTime());
                    dbRoleAuthInsert.setCreId(userInfo.getUserId());
                    dbRoleAuthInsertList.add(dbRoleAuthInsert);
                }
                if (CollectionUtil.isNotEmpty(dbRoleAuthInsertList)) {
                    roleAuthMapper.insertList(dbRoleAuthInsertList);
                }
            }
        } else {
            throw new BusinessException("请传入roleId");
        }
        return null;
    }

    @Override
    public String getRoleAuthDefaultName(GetLoginOutData userInfo, StroeRoleDelDetail inData) {
        StringBuilder sb = new StringBuilder("");
        if (StrUtil.isBlank(inData.getRoleId()) || inData.getRoleType() == null) {
            throw new BusinessException("请传递合法参数!");
        }
//        Integer count = 1;
        String basicRoleId = inData.getRoleId();
        if (inData.getRoleType() == 1) {
            GaiaGroupBasicRole gaiaGroupBasicRole = basicRoleMapper.selectByPrimaryKey(basicRoleId);
            if (gaiaGroupBasicRole == null) {
                throw new BusinessException("查无基础角色数据!");
            }
            sb.append(gaiaGroupBasicRole.getName());
        } else if (inData.getRoleType() == 2) {
            //根据自定义roleId查询基础roleId
            GroupRole groupRole = groupRoleMapper.selectByPrimaryKey(inData.getRoleId());
            if (groupRole == null) {
                throw new BusinessException("查无此数据!");
            }
            sb.append(groupRole.getRoleName());
        } else {
            throw new BusinessException("请传递合法roleType参数!");
        }
        sb.append(split_separator).append("001");
        return sb.toString();
    }

    @Override
    public List<StoreRoleRes> getClientAllStoreInfo(String client) {
        List<StoreRoleRes> resList = new ArrayList<>();
        //处理特殊的GAD类型
        StoreRoleRes all = new StoreRoleRes();
        all.setStoCode("GAD");
        all.setStoName("全部");
        all.setStoType("");
        Integer allNum = groupRoleMapper.selectAllGADRoleNumInClient(client);
        all.setRoleNums(allNum==null?0:allNum);
        resList.add(all);
        List<StoreRoleRes> storeListRoleNum = groupRoleMapper.getStoreListRoleNum(client);
        if (CollectionUtil.isNotEmpty(storeListRoleNum)) {
            resList.addAll(storeListRoleNum);
        }
        return resList;
    }

    @Override
    public List<ModelClassify> addStoreRoleAuthInit(GetLoginOutData userInfo, String stoCode) {
        List<ModelClassify> resList = new ArrayList<>();
        // 获取模块数据
        List<ModelClassify> modelClassifies = authDetailMapper.selectGlobalModel(Constants.SYSTEM_PARAM_MODEL, false);

        // 获取门店角色
        List<GroupRoleExt> groupRoleExts = basicRoleMapper.selectRoleByStore(userInfo.getClient(), stoCode, 1);

        if (CollectionUtils.isNotEmpty(groupRoleExts)) {
            Map<String, List<GroupRoleExt>> groupRoleMap = groupRoleExts.stream()
                    .collect(Collectors.groupingBy(GroupRoleExt::getModel));
            for (ModelClassify modelClassify : modelClassifies) {
                modelClassify.setGroupRoleExts(groupRoleMap.get(modelClassify.getModel()));
            }
            if (CollectionUtil.isNotEmpty(modelClassifies)) {
                resList = modelClassifies;
            }
        }
        return resList;
    }

    @Override
    public JsonResult selectRoleInfoByStore(String client, String stoCode) {
        List<GroupRoleExt> resList = new ArrayList<>();
        // 获取模块数据
        List<ModelClassify> modelClassifies = authDetailMapper.selectGlobalModel(Constants.SYSTEM_PARAM_MODEL, false);

        // 获取门店角色
        List<GroupRoleExt> groupRoleExts = basicRoleMapper.selectRoleByStore(client, stoCode, 1);

        if (CollectionUtils.isNotEmpty(groupRoleExts)) {
            Map<String, String> map = modelClassifies.stream()
                    .collect(Collectors.toMap(ModelClassify::getModel, ModelClassify::getName));
            if(CollectionUtil.isNotEmpty(modelClassifies)){
                for(ModelClassify modelClassify : modelClassifies){
                    String model = modelClassify.getModel();
                    GroupRoleExt titleVo = new GroupRoleExt();
                    titleVo.setRoleName(modelClassify.getName());
                    titleVo.setId(model);

                    List<GroupRoleExt> temp = new ArrayList<>();
                    for (GroupRoleExt groupRoleExt : groupRoleExts) {
                        // 姓名展示格式化
                        if(groupRoleExt.getModel().equals(model)){
                            String userName = groupRoleExt.getUserName();
                            if (StringUtils.isNotBlank(userName)) {
                                List<String> userNameList = Splitter.on(CharUtil.COMMA).splitToList(userName);
                                StringJoiner stringJoiner = new StringJoiner("");
                                int i = 0;
                                for (String name : userNameList) {
                                    stringJoiner.add(name).add("\t");
                                    i++;
                                    if (i % 3 == 0) {
                                        stringJoiner.add("\n");
                                    }
                                }
                                groupRoleExt.setChoose(groupRoleExt.getUserNum() != null && groupRoleExt.getUserNum() > 0);
                                groupRoleExt.setUserName(stringJoiner.toString());
                            } else {
                                groupRoleExt.setChoose(false);
                            }
                            groupRoleExt.setModelName(map.get(groupRoleExt.getModel()));
                            groupRoleExt.setPId(model);
                            temp.add(groupRoleExt);
                        }
                    }
                    titleVo.setChildren(temp);
                    resList.add(titleVo);
                }
            } else {
                if (CollectionUtil.isNotEmpty(modelClassifies)) {
                    for (ModelClassify modelClassify : modelClassifies) {
                        String model = modelClassify.getModel();
                        GroupRoleExt titleVo = new GroupRoleExt();
                        titleVo.setRoleName(modelClassify.getName());
                        titleVo.setId(model);
                        titleVo.setChildren(new ArrayList<>());
                        resList.add(titleVo);
                    }
                }
                return JsonResult.success(resList);
            }
            return JsonResult.success(resList);
        }
        return JsonResult.success();
    }

    @Override
    public JsonResult selectRoleInfoByStoreWithoutPerson(String client, String stoCode) {
        List<GroupRoleExt> resList = new ArrayList<>();
        // 获取模块数据
        List<ModelClassify> modelClassifies = authDetailMapper.selectGlobalModel(Constants.SYSTEM_PARAM_MODEL, false);

        // 获取门店角色
        List<GroupRoleExt> groupRoleExts = basicRoleMapper.selectRoleByStoreWithoutPerson(client, stoCode, 1);
        List<String> chooseIds = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(groupRoleExts)) {
            Map<String, String> map = modelClassifies.stream()
                    .collect(Collectors.toMap(ModelClassify::getModel, ModelClassify::getName));
            if(CollectionUtil.isNotEmpty(modelClassifies)){
                for(ModelClassify modelClassify : modelClassifies){
                    String model = modelClassify.getModel();
                    GroupRoleExt titleVo = new GroupRoleExt();
                    titleVo.setRoleName(modelClassify.getName());
                    titleVo.setId(model);

                    List<GroupRoleExt> temp = new ArrayList<>();
                    for (GroupRoleExt groupRoleExt : groupRoleExts) {
                        // 姓名展示格式化
                        if(groupRoleExt.getModel().equals(model)){
                            String userName = groupRoleExt.getUserName();
                            if (StringUtils.isNotBlank(userName)) {
                                List<String> userNameList = Splitter.on(CharUtil.COMMA).splitToList(userName);
                                StringJoiner stringJoiner = new StringJoiner("");
                                int i = 0;
                                for (String name : userNameList) {
                                    stringJoiner.add(name).add("\t");
                                    i++;
                                    if (i % 3 == 0) {
                                        stringJoiner.add("\n");
                                    }
                                }
                                groupRoleExt.setChoose(groupRoleExt.getUserNum() != null && groupRoleExt.getUserNum() > 0);
                                groupRoleExt.setUserName(stringJoiner.toString());
                            } else {
                                groupRoleExt.setChoose(false);
                            }
                            if (groupRoleExt.getChoose()) {
                                chooseIds.add(groupRoleExt.getId());
                            }
                            groupRoleExt.setModelName(map.get(groupRoleExt.getModel()));
                            groupRoleExt.setPId(model);
                            temp.add(groupRoleExt);
                        }
                    }
                    titleVo.setChildren(temp);
                    resList.add(titleVo);
                }
            } else {
                if (CollectionUtil.isNotEmpty(modelClassifies)) {
                    for (ModelClassify modelClassify : modelClassifies) {
                        String model = modelClassify.getModel();
                        GroupRoleExt titleVo = new GroupRoleExt();
                        titleVo.setRoleName(modelClassify.getName());
                        titleVo.setId(model);
                        titleVo.setChildren(new ArrayList<>());
                        resList.add(titleVo);
                    }
                }
                return JsonResult.success(resList);
            }



//            Map<String, List<GroupRoleExt>> groupRoleMap = groupRoleExts.stream()
//                    .collect(Collectors.groupingBy(GroupRoleExt::getModel));
//            for (ModelClassify modelClassify : modelClassifies) {
//                modelClassify.setGroupRoleExts(groupRoleMap.get(modelClassify.getModel()) != null ? groupRoleMap.get(modelClassify.getModel()) : new ArrayList<GroupRoleExt>(0));
//            }
            Map<String,Object> resMap = new HashMap<>();
            resMap.put("tree", resList);
            resMap.put("ids", chooseIds);
            return JsonResult.success(resMap);
        }
        return JsonResult.success();
    }

    @Override
    public List<StoreRoleRes> getStoreAndRoleNums(GetLoginOutData userInfo) {
        return null;
    }

}

