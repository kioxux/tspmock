package com.gys.business.service.data;

import lombok.Data;

@Data
public class DeptInData {
   private String client;
   private String depId;
   private String depName;
   private String depHeadId;
   private String depHeadNam;
   private String depCreDate;
   private String depCreTime;
   private String depCreId;
   private String depModiDate;
   private String depModiTime;
   private String depModiId;
   private String depDisDate;
   private String depType;
   private String stoChainHead;
   private String depCls;
   private String compadmId;
   private String dcType;
   private String depStatus;
}
