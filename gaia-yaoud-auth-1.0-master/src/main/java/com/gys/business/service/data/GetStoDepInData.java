package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetStoDepInData {

    private String client;

    private String userId;

    private String queryString;
}
