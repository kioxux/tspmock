package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetStoListOutData {
   private String stoCode;
   private String stoName;
   private String stoPym;
   private String stoShortName;
   private String stoAttribute;
   private String stoDeliveryMode;
   private String stoRelationStore;
   private String stoStatus;
   private String stoArea;
   private String stoOpenDate;
   private String stoCloseDate;
   private String stoAdd;
   private String stoProvince;
   private String stoCity;
   private String stoDistrict;
   private String stoIfMedicalcare;
   private String stoIfDtp;
   private String stoTaxClass;
   private String stoDeliveryCompany;
   private String usstoChainHeaderTel;
   private String stoTaxSubject;
   private String stoTaxRate;
   private String stoNo;
   private String stoLegalPerson;
   private String stoQua;
   private String stoLogo;
   private String stogCode;
   private String stoLeaderName;
   private String depName;
   private String stoYbcode;
}
