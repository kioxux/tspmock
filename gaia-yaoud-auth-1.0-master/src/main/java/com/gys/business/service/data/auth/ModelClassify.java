package com.gys.business.service.data.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 模块分类
 * @date 2021/12/2217:48
 */
@Data
public class ModelClassify implements Serializable {

    @ApiModelProperty("模块名称")
    private String name;

    @ApiModelProperty("模块分类")
    private String classify;

    @ApiModelProperty("模块编码")
    private String model;

    private List<GroupRoleExt> groupRoleExts;

}
