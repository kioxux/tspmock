package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.service.data.DeptExportInData;
import com.gys.business.service.data.DeptInData;
import com.gys.business.service.data.DeptOutData;
import com.gys.business.service.data.dep.dto.DepDto;
import com.gys.business.service.data.dep.vo.DepVo;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import java.util.List;

public interface DepartmentService {
   List<DeptOutData> getDeptList(DeptInData inData);

   DeptOutData getDeptById(String depId, GetLoginOutData userInfo);

   GaiaDepData insertDept(DeptInData inData, GetLoginOutData userInfo);

   void disableDept(DeptInData inData, GetLoginOutData userInfo);

   void enableDept(DeptInData inData, GetLoginOutData userInfo);

   List<GaiaDepData> getDeptListByType(DeptInData inData);

   void updateDept(DeptInData inData, GetLoginOutData userInfo);

   JsonResult exportIn(List<DeptExportInData> inData, GetLoginOutData outData) throws Exception;

    List<DepVo> getAllDeptList(DepDto dto);
}
