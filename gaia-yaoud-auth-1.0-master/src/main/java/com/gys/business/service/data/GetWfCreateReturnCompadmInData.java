package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateReturnCompadmInData {
   @Field(
      name = "退库门店"
   )
   @JSONField(
      ordinal = 1
   )
   private String storeId = "";
   @Field(
      name = "退库日期"
   )
   @JSONField(
      ordinal = 2
   )
   private String returnDate = "";
   @Field(
      name = "退库单号"
   )
   @JSONField(
      ordinal = 3
   )
   private String returnOrder = "";
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 4
   )
   private String proCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 5
   )
   private String proName = "";
   @Field(
      name = "规格/单位"
   )
   @JSONField(
      ordinal = 6
   )
   private String proSpecs = "";
   @Field(
      name = "批次"
   )
   @JSONField(
      ordinal = 7
   )
   private String batch = "";
   @Field(
      name = "批次成本"
   )
   @JSONField(
      ordinal = 8
   )
   private String batchCost = "";
   @Field(
      name = "税额"
   )
   @JSONField(
      ordinal = 9
   )
   private String tax = "";
   @Field(
      name = "加权移动平均成本"
   )
   @JSONField(
      ordinal = 10
   )
   private String wmaCost = "";
   @Field(
      name = "加权移动平均成本税额"
   )
   @JSONField(
      ordinal = 11
   )
   private String wmaCostTax = "";
   @Field(
      name = "申请数量"
   )
   @JSONField(
      ordinal = 12
   )
   private String applyQty = "";
   @Field(
      name = "实退数量"
   )
   @JSONField(
      ordinal = 13
   )
   private String returnQty = "";
   @Field(
      name = "退库验收数量"
   )
   @JSONField(
      ordinal = 14
   )
   private String acceptQty = "";
   @Field(
      name = "退库拒收数量"
   )
   @JSONField(
      ordinal = 15
   )
   private String rejectQty = "";
   @Field(
      name = "生产厂家"
   )
   @JSONField(
      ordinal = 16
   )
   private String sccj = "";
}
