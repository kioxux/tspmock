package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2022年01月05日 下午5:15
 */
@Data
public class UserClientRes implements Serializable {

    private String client;

    private String clientName;

    private String redirectPath;

}

