package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 改价工作流
 */
@Data
public class GetSalesChangePriceInData {
    @Field(
            name = "行号"
    )
    @JSONField(
            ordinal = 1
    )
    private String index;

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 2
    )
    private String proSelfCode;

    @Field(
            name = "通用名"
    )
    @JSONField(
            ordinal = 3
    )
    private String proCommonname;

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 4
    )
    private String proSpecs;

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 5
    )
    private String proFactoryName;

    @Field(
            name = "批号"
    )
    @JSONField(
            ordinal = 6
    )
    private String batchNo;

    @Field(
            name = "效期"
    )
    @JSONField(
            ordinal = 7
    )
    private String validDay;

    @Field(
            name = "销售数量"
    )
    @JSONField(
            ordinal = 8
    )
    private String saleQty;

    @Field(
            name = "零售价"
    )
    @JSONField(
            ordinal = 9
    )
    private String price;

    @Field(
            name = "成本价"
    )
    @JSONField(
            ordinal = 10
    )
    private String cost;

    @Field(
            name = "零售额"
    )
    @JSONField(
            ordinal = 11
    )
    private String saleAmt;

    @Field(
            name = "原毛利"
    )
    @JSONField(
            ordinal = 12
    )
    private String oriGross;

    @Field(
            name = "原毛利率（%）"
    )
    @JSONField(
            ordinal = 13
    )
    private String oriGrossRate;

    @Field(
            name = "改后应收价"
    )
    @JSONField(
            ordinal = 14
    )
    private String changePrice;

    @Field(
            name = "改后应收额"
    )
    @JSONField(
            ordinal = 15
    )
    private String changeAmt;

    @Field(
            name = "改后毛利率（%）"
    )
    @JSONField(
            ordinal = 16
    )
    private String changGrossRate;

    @Field(
            name = "改后毛利额"
    )
    @JSONField(
            ordinal = 17
    )
    private String changeGross;
//    /**
//     *门店号
//     */
//    @Field(
//            name = "行号"
//    )
//    @JSONField(
//            ordinal = 1
//    )
//    private String stoCode;
//    /**
//     *提交日期
//     */
//    @Field(
//            name = "行号"
//    )
//    @JSONField(
//            ordinal = 1
//    )
//    private String createTime;
//    /**
//     *单号
//     */
//    @Field(
//            name = "行号"
//    )
//    @JSONField(
//            ordinal = 1
//    )
//    private String billCode;
//    /**
//     *整单金额
//     */
//    @Field(
//            name = "行号"
//    )
//    @JSONField(
//            ordinal = 1
//    )
//    private String billAmt;
//    /**
//     *改价备注
//     */
//    @Field(
//            name = "行号"
//    )
//    @JSONField(
//            ordinal = 1
//    )
//    private String remark;

//    /**
//     * 序号
//     */
//    private String index;
//    /**
//     * 商品编码
//     */
//    private String proSelfCode;
//    /**
//     * 通用名
//     */
//    private String proCommonname;
//    /**
//     * 规格
//     */
//    private String proSpecs;
//    /**
//     * 厂家
//     */
//    private String proFactoryName;
//    /**
//     * 批号
//     */
//    private String batchNo;
//    /**
//     * 效期
//     */
//    private String validDay;
//    /**
//     *销售数量
//     */
//    private BigDecimal saleQty;
//    /**
//     *零售价
//     */
//    private BigDecimal price;
//    /**
//     *零售额
//     */
//    private BigDecimal saleAmt;
//    /**
//     *原毛利
//     */
//    private BigDecimal oriGross;
//    /**
//     *原毛利率
//     */
//    private BigDecimal oriGrossRate;
//    /**
//     *改后应收价
//     */
//    private BigDecimal changePrice;
//    /**
//     *改后应收额
//     */
//    private BigDecimal changeAmt;
//    /**
//     *改后毛利率
//     */
//    private BigDecimal changGrossRate;
//    /**
//     *改后毛利额
//     */
//    private BigDecimal changeGross;
//    /**
//     *门店号
//     */
//    private String stoCode;
//    /**
//     *提交日期
//     */
//    private String createTime;
//    /**
//     *单号
//     */
//    private String billCode;
//    /**
//     *整单金额
//     */
//    private String billAmt;
//    /**
//     *改价备注
//     */
//    private String remark;
}
