package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.feign.WmsService;
import com.gys.business.mapper.GaiaCompadmMapper;
import com.gys.business.mapper.GaiaDcDataMapper;
import com.gys.business.mapper.GaiaDepDataMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.AuthconfiService;
import com.gys.business.service.DcService;
import com.gys.business.service.data.DcIndata;
import com.gys.business.service.data.GetDcInitInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResultWms;
import com.gys.common.exception.BusinessException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class DcServiceImpl implements DcService {
   @Autowired
   private GaiaDcDataMapper gaiaDcDataMapper;
   @Autowired
   private GaiaUserDataMapper userDataMapper;
   @Autowired
   private GaiaCompadmMapper gaiaCompadmMapper;
   @Autowired
   private GaiaDepDataMapper gaiaDepDataMapper;
   @Autowired
   private GaiaStoreDataMapper storeDataMapper;
   @Autowired
   private GaiaDepDataMapper depDataMapper;
   @Autowired
   private WmsService wmsService;
   @Autowired
   private AuthconfiService authconfiService;
   @Value("${thirdServer}")
   private String thirdServer;

   @Transactional
   public GaiaDcData insertDc(DcIndata inData, GetLoginOutData userInfo) {
      GaiaDcData gaiaDcData = new GaiaDcData();
      gaiaDcData.setClient(userInfo.getClient());
      gaiaDcData.setDcName(inData.getDcName());
//      int idMax = this.gaiaDcDataMapper.selectMaxId();
      gaiaDcData.setDcCode(inData.getDcCode());
      gaiaDcData.setDcStatus("0");
      if (ObjectUtil.isNotEmpty(inData.getStoChainHead())) {
         gaiaDcData.setDcChainHead(inData.getStoChainHead());
      }

      gaiaDcData.setDcHeadId(inData.getDcHeadId());
//      Example example = new Example(GaiaUserData.class);
//      example.createCriteria().andEqualTo("userId", inData.getDcHeadId()).andEqualTo("client", userInfo.getClient());
//      GaiaUserData user = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
//      gaiaDcData.setDcType(inData.getDcType());
      gaiaDcData.setDcHeadNam(inData.getDcHeadName());
      gaiaDcData.setDcCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
      gaiaDcData.setDcCreTime(DateUtil.format(new Date(), "HHmmss"));
      gaiaDcData.setDcCreId(userInfo.getUserId());
      this.gaiaDcDataMapper.insert(gaiaDcData);
//      user.setDepId(gaiaDcData.getDcCode());
//      user.setDepName(gaiaDcData.getDcName());
//      this.userDataMapper.updateByPrimaryKey(user);
//      List<String> groupList = new ArrayList();
//      if ("20".equals(inData.getDcType())) {
//         groupList.add("GAIA_WM_ZG");
//      }
//
//      if ("30".equals(inData.getDcType())) {
//         groupList.add("GAIA_AL_ADMIN");
//         groupList.add("GAIA_AL_MANAGER");
//         groupList.add("GAIA_WM_ZG");
//         groupList.add("GAIA_MM_SCZG");
//         groupList.add("GAIA_MM_ZLZG");
//         groupList.add("SD_01");
//         groupList.add("GAIA_FI_ZG");
//      }
//
//      List<String> siteList = new ArrayList();
//      List<GaiaStoreData> storeDataList = this.storeDataMapper.selectListForAuth(gaiaDcData.getClient(), gaiaDcData.getDcChainHead());
//      Iterator var10 = storeDataList.iterator();
//
//      while(var10.hasNext()) {
//         GaiaStoreData storeData = (GaiaStoreData)var10.next();
//         siteList.add(storeData.getStoCode());
//      }
//
//      List<GaiaDepData> depDataList = this.depDataMapper.selectListForAuth(gaiaDcData.getClient(), gaiaDcData.getDcChainHead());
//      Iterator var14 = depDataList.iterator();
//
//      while(var14.hasNext()) {
//         GaiaDepData dep = (GaiaDepData)var14.next();
//         siteList.add(dep.getDepId());
//      }
//
//      siteList.add(gaiaDcData.getDcCode());
//      this.authconfiService.saveAuth(gaiaDcData.getClient(), gaiaDcData.getDcHeadId(), groupList, siteList, (String)null);
      GetDcInitInData dcInitInData = new GetDcInitInData();
      dcInitInData.setClient(gaiaDcData.getClient());
      dcInitInData.setSite(gaiaDcData.getDcCode());
      if ("1".equals(this.thirdServer)) {
         JsonResultWms resultWms = this.wmsService.dcInit(dcInitInData);
         if (ObjectUtil.isNull(resultWms)) {
            throw new BusinessException("服务异常");
         }

         if (!resultWms.isSuccess()) {
            throw new BusinessException(resultWms.getMsg());
         }
      }

      return gaiaDcData;
   }

   @Transactional
   public void upDateDc(DcIndata inData, GetLoginOutData userInfo) {
      GaiaDcData gaiaDcData = new GaiaDcData();
      gaiaDcData.setClient(userInfo.getClient());
      gaiaDcData.setDcCode(inData.getDcCode());
      gaiaDcData = (GaiaDcData)this.gaiaDcDataMapper.selectOne(gaiaDcData);
//      String oldHeadId = gaiaDcData.getDcHeadId();
      gaiaDcData.setDcName(inData.getDcName());
      gaiaDcData.setDcHeadId(inData.getDcHeadId());
//      Example example = new Example(GaiaUserData.class);
//      example.createCriteria().andEqualTo("userId", inData.getDcHeadId()).andEqualTo("client", userInfo.getClient());
//      GaiaUserData user = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
      gaiaDcData.setDcHeadNam(inData.getDcHeadName());
      gaiaDcData.setDcChainHead(inData.getStoChainHead());
      gaiaDcData.setDcModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
      gaiaDcData.setDcModiTime(DateUtil.format(new Date(), "HHmmss"));
      gaiaDcData.setDcModiId(userInfo.getUserId());
      this.gaiaDcDataMapper.updateByPrimaryKeySelective(gaiaDcData);
//      user.setDepId(gaiaDcData.getDcCode());
//      user.setDepName(gaiaDcData.getDcName());
//      this.userDataMapper.updateByPrimaryKey(user);
//      if (!oldHeadId.equals(inData.getDcHeadId())) {
//         List<String> groupList = new ArrayList();
//         if ("20".equals(inData.getDcType())) {
//            groupList.add("GAIA_WM_ZG");
//         }
//
//         if ("30".equals(inData.getDcType())) {
//            groupList.add("GAIA_AL_ADMIN");
//            groupList.add("GAIA_AL_MANAGER");
//            groupList.add("GAIA_WM_ZG");
//            groupList.add("GAIA_MM_SCZG");
//            groupList.add("GAIA_MM_ZLZG");
//            groupList.add("SD_01");
//            groupList.add("GAIA_FI_ZG");
//         }
//
//         List<String> siteList = new ArrayList();
////         List<GaiaStoreData> storeDataList = this.storeDataMapper.selectListForAuth(gaiaDcData.getClient(), gaiaDcData.getDcChainHead());
////         Iterator var10 = storeDataList.iterator();
////
////         while(var10.hasNext()) {
////            GaiaStoreData storeData = (GaiaStoreData)var10.next();
////            siteList.add(storeData.getStoCode());
////         }
//
//         List<GaiaDepData> depDataList = this.depDataMapper.selectListForAuth(gaiaDcData.getClient(), gaiaDcData.getDcCode());
//         Iterator var14 = depDataList.iterator();
//
//         while(var14.hasNext()) {
//            GaiaDepData dep = (GaiaDepData)var14.next();
//            siteList.add(dep.getDepId());
//         }
//
//         siteList.add(gaiaDcData.getDcCode());
//         this.authconfiService.saveAuth(gaiaDcData.getClient(), gaiaDcData.getDcHeadId(), groupList, siteList, oldHeadId);
//      }
   }

   public List<GaiaDcData> dcInfoByClientId(DcIndata dcIndata) {
      GaiaDcData gaiaDcData = new GaiaDcData();
      gaiaDcData.setClient(dcIndata.getClientId());
      if (ObjectUtil.isNotEmpty(dcIndata.getCompadmId())) {
         gaiaDcData.setDcChainHead(dcIndata.getCompadmId());
      }

      gaiaDcData.setDcType(dcIndata.getDcType());
      List<GaiaDcData> out = this.gaiaDcDataMapper.select(gaiaDcData);
      return out;
   }

   public GaiaDcData getDcById(GaiaDcData gaiaDcData, GetLoginOutData userInfo) {
      gaiaDcData.setClient(userInfo.getClient());
      gaiaDcData = (GaiaDcData)this.gaiaDcDataMapper.selectByPrimaryKey(gaiaDcData);
      return gaiaDcData;
   }

   public List<GaiaDcData> wholesaleInfoByClientId(DcIndata indata) {
      Example example = new Example(GaiaDcData.class);
      Criteria criteria = example.createCriteria().andEqualTo("client", indata.getClientId()).andIsNull("dcChainHead").andEqualTo("dcType", "30");
      if (ObjectUtil.isNotEmpty(indata.getDcCode())) {
         criteria.andEqualTo("dcCode", indata.getDcCode());
      }

      List<GaiaDcData> out = this.gaiaDcDataMapper.selectByExample(example);
      return out;
   }

   public List<GaiaDepData> dcDeptInfo(GetLoginOutData userInfo, GaiaDepData inData) {
      GaiaDepData gaiaDepData = new GaiaDepData();
      gaiaDepData.setClient(userInfo.getClient());
      gaiaDepData.setDepType(inData.getDepType());
      gaiaDepData.setStoChainHead(inData.getStoChainHead());
      List<GaiaDepData> out = this.gaiaDepDataMapper.select(gaiaDepData);
      return out;
   }
}
