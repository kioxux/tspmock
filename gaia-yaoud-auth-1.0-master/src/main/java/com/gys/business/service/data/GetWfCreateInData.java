package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfCreateInData{
   private String wfDefineCode;

   private String wfTitle;

   private String wfDescription;

   private String wfOrder;

   private String wfSite;

   private String clientId;

   Object wfDetail;

   private GetWfPhysicalNewWorkflowInData newWorkflowInData;
}
