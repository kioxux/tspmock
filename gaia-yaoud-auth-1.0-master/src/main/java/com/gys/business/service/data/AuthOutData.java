package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class AuthOutData {
   private String userId;
   private String userName;
   private List<String> groupList;
   private List<AuthOutData> authList;
   private List<AuthGroupOutData> tableHeaderList;
}
