package com.gys.business.service;

import com.gys.business.service.data.GlobalInData;
import com.gys.business.service.data.GlobalOutData;
import java.util.List;

public interface GlobalService {
   boolean authorization(GlobalInData inData);

   List<GlobalOutData> authorList(GlobalInData inData);
}
