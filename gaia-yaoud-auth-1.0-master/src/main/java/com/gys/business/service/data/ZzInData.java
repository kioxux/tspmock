package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;
@Data
public class ZzInData {
   private String id;
   private String client;
   @NotBlank(message = "机构编码不能为空")
   private String zzOrgid;
   //1门店 2连锁 3批发
   @NotBlank(message = "门店类型能为空")
   private String zzType;
   @NotBlank(message = "机构名称不能为空")
   private String zzOrgname;
   private String zzId;
   private String zzName;
   private String zzUserId;
   private String zzUserName;
   private String zzNo;
   private String zzSdate;
   private String zzEdate;
   public int pageSize;
   private int pageNum;
   private List<String> ids;
   private List<String> orgIds;
}
