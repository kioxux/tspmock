package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.service.data.DcIndata;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface DcService {
   GaiaDcData insertDc(DcIndata inData, GetLoginOutData userInfo);

   void upDateDc(DcIndata inData, GetLoginOutData userInfo);

   List<GaiaDcData> dcInfoByClientId(DcIndata userInfo);

   List<GaiaDcData> wholesaleInfoByClientId(DcIndata userInfo);

   GaiaDcData getDcById(GaiaDcData gaiaDcData, GetLoginOutData userInfo);

   List<GaiaDepData> dcDeptInfo(GetLoginOutData userInfo, GaiaDepData gaiaDepData);
}
