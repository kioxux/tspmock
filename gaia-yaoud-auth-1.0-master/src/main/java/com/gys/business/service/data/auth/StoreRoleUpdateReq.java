package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午6:02
 */
@Data
public class StoreRoleUpdateReq implements Serializable {

    private String roleName;

    private String roleId;

    // 1表示基础角色 2表示自定义角色
    private Integer roleType;

    private List<String> authIds;

    private String stoCode;
}

