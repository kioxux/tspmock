package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class MemberTypeOutData {
   private String clientId;
   private String gsmcId;
   private String gsmcName;
   private String gsmcDiscountRate;
   private String gsmcDiscountRate2;
   private BigDecimal gsmcAmtSale;
   private String gsmcIntegralSale;
   private String gsmcUpgradeInte;
   private String gsmcUpgradeRech;
   private String gsmcUpgradeSet;
   private String gsmcCreDate;
   private String gsmcCreEmp;
   private String gsmcUpdateDate;
   private String gsmcUpdateEmp;
}
