package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateSpyhInData {
   @Field(
      name = "养护时间"
   )
   @JSONField(
      ordinal = 1
   )
   private String wmYhsj = "";
   @Field(
      name = "养护月份"
   )
   @JSONField(
      ordinal = 2
   )
   private String wmYhyf = "";
   @Field(
      name = "养护类型"
   )
   @JSONField(
      ordinal = 3
   )
   private String wmYhlx = "";
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 4
   )
   private String proCode = "";
   @Field(
      name = "名称"
   )
   @JSONField(
      ordinal = 5
   )
   private String proName = "";
   @Field(
      name = "规格"
   )
   @JSONField(
      ordinal = 6
   )
   private String proSpecs = "";
   @Field(
      name = "厂家"
   )
   @JSONField(
      ordinal = 7
   )
   private String sccj = "";
   @Field(
      name = "单位"
   )
   @JSONField(
      ordinal = 8
   )
   private String proUnit = "";
   @Field(
      name = "批号"
   )
   @JSONField(
      ordinal = 9
   )
   private String batchCode = "";
   @Field(
      name = "生产日期"
   )
   @JSONField(
      ordinal = 10
   )
   private String proDate = "";
   @Field(
      name = "有效期"
   )
   @JSONField(
      ordinal = 11
   )
   private String proValid = "";
   @Field(
      name = "库存数量"
   )
   @JSONField(
      ordinal = 12
   )
   private String stockQty = "";
   @Field(
      name = "养护数量"
   )
   @JSONField(
      ordinal = 13
   )
   private String wmYhsl = "";
   @Field(
      name = "养护结果"
   )
   @JSONField(
      ordinal = 14
   )
   private String wmYhjg = "";
   @Field(
      name = "备注"
   )
   @JSONField(
      ordinal = 15
   )
   private String wmYhbz = "";
}
