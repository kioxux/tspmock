package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class PrintSupplierApprovalOutData extends GetWfSupplierApprovalInData{
    @JSONField(serialize = false)
    @JsonIgnore
    private String applicant;
}
