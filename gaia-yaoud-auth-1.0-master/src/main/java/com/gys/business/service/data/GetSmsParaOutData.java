package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetSmsParaOutData {
   private String spCode;
   private String loginName;
   private String password;
   private String templateId;
   private String templateContent;
}
