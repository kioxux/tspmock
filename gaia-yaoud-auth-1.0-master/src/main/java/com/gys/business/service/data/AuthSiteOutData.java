package com.gys.business.service.data;

import lombok.Data;

/**
 * @author 陈浩
 */
@Data
public class AuthSiteOutData {
   private String name;
   private String value;
}
