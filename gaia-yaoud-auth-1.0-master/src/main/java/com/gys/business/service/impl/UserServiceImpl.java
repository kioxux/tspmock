package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.UserService;
import com.gys.business.service.data.*;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.*;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.CodecUtil;
import com.gys.util.SmsUtil;
import com.gys.util.UserUtils;
import com.gys.util.UtilConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.*;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private GaiaDepDataMapper depDataMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;
    @Autowired
    private GaiaResourceMapper resourceMapper;
    @Autowired
    private GaiaAuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private ThirdParaService thirdParaService;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;
    @Resource
    private GaiaUserResourceRecordMapper gaiaUserResourceRecordMapper;

    public PageInfo<UserOutData> getUserList(UserInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<UserOutData> outData = this.userDataMapper.getUserList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    public PageInfo<UserOutData> getUserAllList(UserInData inData, GetLoginOutData userInfo) {
        inData.setClient(userInfo.getClient());
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        //1是门店  2是连锁 3批发 4部门 应为人员信息里面部门id 存的可能是门店 连锁 批发 部门 所以必须知道是查询的什么
        List<UserOutData> outData = new ArrayList<>();
        if(inData.getSourceType().equals("1")) {
            outData = this.userDataMapper.getUserAllBySto(inData);
        }else if(inData.getSourceType().equals("2")) {
            outData = this.userDataMapper.getUserAllByCompadm(inData);
        }else if(inData.getSourceType().equals("3")){
            outData = this.userDataMapper.getUserAllByCompadmWms(inData);
        }else if(inData.getSourceType().equals("4")){
            outData = this.userDataMapper.getUserAllByDep(inData);
        }else {
            outData = this.userDataMapper.getUserAll(inData);
        }
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    public List<UserOutData> getUserListByClientId(GetLoginOutData inData) {
        UserInData data = new UserInData();
        data.setClient(inData.getClient());
        List<UserOutData> outData = this.userDataMapper.getUserList(data);
        return outData;
    }

    public List<UserOutData> getUsersByClientId(GetLoginOutData inData, UserInData userInData) {
        UserInData data = new UserInData();
        data.setClient(inData.getClient());
        data.setDepId(userInData.getDepId());
        List<UserOutData> outData = this.userDataMapper.getUsersByClientId(data);
        return outData;
    }

    public void exportUser(HttpServletResponse response, UserInData inData) throws Exception {
        List<UserOutData> outData = this.userDataMapper.getUserAllBySto(inData);
        List<UserExportOutData> list = new ArrayList();
        Iterator var5 = outData.iterator();

        while(var5.hasNext()) {
            UserOutData out = (UserOutData)var5.next();
            UserExportOutData exportOutData = new UserExportOutData();
            exportOutData.setUserNam(out.getUserNam());
            exportOutData.setUserSex(out.getUserSex());
            exportOutData.setDepName(out.getDepName());
            exportOutData.setUserTel(out.getUserTel());
            exportOutData.setUserIdc(out.getUserIdc());
            exportOutData.setUserYsId(out.getUserYsId());
            exportOutData.setUserYsZyfw(out.getUserYsZyfw());
            exportOutData.setUserYsZylb(out.getUserYsZylb());
            exportOutData.setUserAddr(out.getUserAddr());
            exportOutData.setUserEmail(out.getUserEmail());
            exportOutData.setUserSta(out.getUserSta());
            exportOutData.setUserJoinDate(out.getUserJoinDate());
            exportOutData.setUserDisDate(out.getUserDisDate());
            list.add(exportOutData);
        }

        String excelName = "部门人员";
        String[] headers = new String[]{"姓名", "性别", "部门", "手机号", "身份证号", "注册证号", "职业范围", "职业类别", "地址", "邮箱", "状态", "入职日期", "停用日期"};
        ExportExcel<UserExportOutData> excelUtil = new ExportExcel();
        response.setContentType("application/vnd.ms-excel; charset=utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(excelName.getBytes(), "iso-8859-1") + ".xls");
        response.setCharacterEncoding("UTF-8");
        OutputStream outputStream = response.getOutputStream();
        excelUtil.exportExcel(headers, list, outputStream, (String)null);
    }

    public JsonResult exportIn(List<UserExportInData> inData, GetLoginOutData user) throws Exception {
        if (ObjectUtil.isEmpty(inData)) {
            return JsonResult.fail(UtilConst.CODE_1001, "需要导入的资源项目明细为空");
        } else {
            Iterator var3 = inData.iterator();

            //判断姓名不能为空
            UserExportInData exportIn;
            String isNull = "";
            //去重：唯一约束冲突
            String str = "";
            while(var3.hasNext()) {
                exportIn = (UserExportInData)var3.next();
                if (ObjectUtil.isEmpty(exportIn.getUserNam())) {
                    isNull =""+ isNull+"，"+exportIn.getIndex();
                }
                GaiaUserData userData = new GaiaUserData();
                userData.setClient(user.getClient());
                userData.setUserTel(exportIn.getUserTel());
                GaiaUserData out = this.userDataMapper.selectOne(userData);
                if (ObjectUtil.isNotEmpty(out)){
                    str =""+ str+"，"+exportIn.getIndex();
                }
            }
            if (ObjectUtil.isNotEmpty(isNull)){
                isNull = isNull.substring(1);
                return JsonResult.fail(UtilConst.CODE_1001, "第" + isNull + "行的姓名不能为空！");
            }
            if (ObjectUtil.isNotEmpty(str)){
                str = str.substring(1);
                return JsonResult.fail(UtilConst.CODE_1001, "第" + str + "行的数据重复！");
            }


//         var3 = inData.iterator();
            UserExportInData exportInData;
            while(var3.hasNext()) {
                exportInData = (UserExportInData)var3.next();
                int max = this.userDataMapper.selectMaxId(user.getClient());
                GaiaUserData userData = new GaiaUserData();
                userData.setUserId(String.valueOf(max + 1));
                userData.setClient(user.getClient());
                userData.setUserPassword("e10adc3949ba59abbe56e057f20f883e");
                userData.setUserNam(exportInData.getUserNam());
                userData.setUserSex(exportInData.getUserSex());
                userData.setUserTel(exportInData.getUserTel());
                userData.setUserIdc(exportInData.getUserIdc());
                userData.setUserYsId(exportInData.getUserYsId());
                userData.setUserYsZyfw(exportInData.getUserYsZyfw());
                userData.setUserYsZylb(exportInData.getUserYsZylb());
                userData.setDepId(exportInData.getDepId());
                userData.setDepName(exportInData.getDepName());
                userData.setUserAddr(exportInData.getUserAddr());
                userData.setUserSta(exportInData.getUserSta());
                userData.setUserJoinDate(exportInData.getUserJoinDate());
                userData.setUserDisDate(exportInData.getUserDisDate());
                userData.setUserEmail(exportInData.getUserEmail());
                userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
                userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
                userData.setUserCreId(user.getUserId());
                userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
                userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
                userData.setUserModiId(user.getUserId());
                this.userDataMapper.insert(userData);
            }
            return JsonResult.success((Object)null, "提示：上传成功！");
        }
    }

    public UserOutData getUserById(UserInData inData) {
        GaiaUserData userData = (GaiaUserData)this.userDataMapper.selectByPrimaryKey(inData);
        UserOutData outData = new UserOutData();
        if (ObjectUtil.isNotEmpty(userData)) {
            outData.setClient(userData.getClient());
            outData.setDepId(userData.getDepId());
            outData.setDepName(userData.getDepName());
            outData.setUserAddr(userData.getUserAddr());
            outData.setUserDisDate(userData.getUserDisDate());
            outData.setUserId(userData.getUserId());
            outData.setUserIdc(userData.getUserIdc());
            outData.setUserJoinDate(userData.getUserJoinDate());
            outData.setUserNam(userData.getUserNam());
            outData.setUserSex(userData.getUserSex());
            outData.setUserSta(userData.getUserSta());
            outData.setUserTel(userData.getUserTel());
            outData.setUserYsId(userData.getUserYsId());
            outData.setUserYsZyfw(userData.getUserYsZyfw());
            outData.setUserYsZylb(userData.getUserYsZylb());
            outData.setUserEmail(userData.getUserEmail());
            outData.setUserType(userData.getUserType());
            outData.setUserYblpaId(userData.getUserYblpaId());
            outData.setUserSignAdds(userData.getUserSignAdds());
        }

        return outData;
    }

    @Transactional
    public void deleteUser(UserInData inData) {
        if (ObjectUtil.isNotEmpty(inData.getClient())) {
            this.userDataMapper.deleteByPrimaryKey(inData.getClient());
        }

    }

    @Transactional
    public void insertUser(UserInData inData, GetLoginOutData userInfo) {
        int max = this.userDataMapper.selectMaxId(userInfo.getClient());
        GaiaUserData userData = new GaiaUserData();
        userData.setUserId(String.valueOf(max + 1));
        userData.setClient(userData.getClient());
        userData.setDepId(userData.getDepId());
        userData.setDepName(userData.getDepName());
        userData.setUserAddr(userData.getUserAddr());
        userData.setUserDisDate(userData.getUserDisDate());
        userData.setUserIdc(userData.getUserIdc());
        userData.setUserJoinDate(userData.getUserJoinDate());
        userData.setUserNam(userData.getUserNam());
        userData.setUserSex(userData.getUserSex());
        userData.setUserSta(userData.getUserSta());
        userData.setUserTel(userData.getUserTel());
        userData.setUserYsId(userData.getUserYsId());
        userData.setUserYsZyfw(userData.getUserYsZyfw());
        userData.setUserYsZylb(userData.getUserYsZylb());
        userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
        userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
        userData.setUserCreId(userInfo.getUserId());
        userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
        userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
        userData.setUserModiId(userInfo.getUserId());
        this.userDataMapper.insert(userData);
    }

    @Transactional
    public void updateUser(UserInData inData, GetLoginOutData userInfo) {
        GaiaUserData userDataTemp = new GaiaUserData();
        userDataTemp.setClient(userInfo.getClient());
        userDataTemp.setDepId(String.valueOf(inData.getDepId()));
        userDataTemp.setUserTel(inData.getUserTel());
        GaiaUserData out = (GaiaUserData)this.userDataMapper.selectOne(userDataTemp);
        if (ObjectUtil.isNotEmpty(out) && !out.getUserId().equals(inData.getUserId())) {
            throw new BusinessException("提示：手机号已存在");
        } else {
            userDataTemp.setUserTel((String)null);
            userDataTemp.setUserIdc(inData.getUserIdc());
            Example example = new Example(GaiaUserData.class);
            example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("depId",String.valueOf(inData.getDepId())).andNotEqualTo("userIdc","").andEqualTo("userIdc",inData.getUserIdc());
            GaiaUserData outIdc = (GaiaUserData)this.userDataMapper.selectOneByExample(example);
            if (ObjectUtil.isNotEmpty(outIdc) && !outIdc.getUserId().equals(inData.getUserId())) {
                throw new BusinessException("提示：身份证号已存在");
            } else {
                GaiaUserData outEma;
                if (ObjectUtil.isNotEmpty(inData.getUserEmail())) {
                    userDataTemp.setUserTel((String)null);
                    userDataTemp.setUserIdc((String)null);
                    userDataTemp.setUserEmail(inData.getUserEmail());
                    outEma = (GaiaUserData)this.userDataMapper.selectOne(userDataTemp);
                    if (ObjectUtil.isNotEmpty(outEma) && !outEma.getUserId().equals(inData.getUserId())) {
                        throw new BusinessException("提示：邮箱已存在");
                    }
                }
                inData.setClient(userInfo.getClient());
                outEma = (GaiaUserData)this.userDataMapper.selectByPrimaryKey(inData);
                outEma.setClient(userInfo.getClient());
                outEma.setDepId(inData.getDepId().toString());
                outEma.setDepName(inData.getDepName());
                outEma.setUserAddr(inData.getUserAddr());
                outEma.setUserDisDate(inData.getUserDisDate());
                outEma.setUserIdc(inData.getUserIdc());
                outEma.setUserJoinDate(inData.getUserJoinDate());
                outEma.setUserNam(inData.getUserNam());
                outEma.setUserSex(inData.getUserSex());
                outEma.setUserSta(inData.getUserSta());
                outEma.setUserTel(inData.getUserTel());
                outEma.setUserYsId(inData.getUserYsId());
                outEma.setUserEmail(inData.getUserEmail());
                outEma.setUserYsZyfw(inData.getUserYsZyfw());
                outEma.setUserYsZylb(inData.getUserYsZylb());
                outEma.setUserYblpaId(inData.getUserYblpaId());
                if (inData.getUserType() != null) {
                    outEma.setUserType(inData.getUserType());
                } else {
                    outEma.setUserType(0);
                }
                outEma.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
                outEma.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
                outEma.setUserModiId(userInfo.getUserId());
                this.userDataMapper.updateByPrimaryKeySelective(outEma);
            }
        }
    }

    public void addDeptUser(UserInData inData, GetLoginOutData userInfo) {
        GaiaUserData userData = new GaiaUserData();
        userData.setClient(userInfo.getClient());
        userData.setUserTel(inData.getUserTel());
        GaiaUserData out = this.userDataMapper.selectOne(userData);
        if (ObjectUtil.isNotEmpty(out)) {
            //2021-4-19 陆老板要求 新增提示手机号重复
            throw new BusinessException("提示：手机号已存在");
//         GaiaUserData gaiaUserData = new GaiaUserData();
//         BeanUtil.copyProperties(inData,userData);
//         userData.setClient(userInfo.getClient());
//         userData.setUserId(out.getUserId());
//         userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
//         userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
//         userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
//         this.userDataMapper.updateByPrimaryKeySelective(userData);
        } else {
            userData.setUserTel((String)null);
            userData.setUserIdc(inData.getUserIdc());
            List<GaiaUserData> outIdc = this.userDataMapper.select(userData);
//         if (ObjectUtil.isNotEmpty(outIdc)) {
//            throw new BusinessException("提示：身份证号已存在");
//         } else {
            if (ObjectUtil.isNotEmpty(inData.getUserEmail())) {
                userData.setUserTel((String)null);
                userData.setUserIdc((String)null);
                userData.setUserEmail(inData.getUserEmail());
//               List<GaiaUserData> outEma = this.userDataMapper.select(userData);
//               if (ObjectUtil.isNotEmpty(outEma)) {
//                  throw new BusinessException("提示：邮箱已存在");
//               }
            }

            BeanUtils.copyProperties(inData, userData);
            int max = this.userDataMapper.selectMaxId(userInfo.getClient());
            userData.setClient(userInfo.getClient());
            if (ObjectUtil.isNotEmpty(inData.getDepId())) {
                userData.setDepId(inData.getDepId());
                userData.setDepName(inData.getDepName());
            }else {
                //如果没有选部门 默认是当前加盟商编码
                userData.setDepId(userInfo.getClient());
                GaiaFranchisee franchisee = this.franchiseeMapper.selectByPrimaryKey(userInfo.getClient());
                userData.setDepName(franchisee.getFrancName());
            }
            String password = RandomUtil.randomNumbers(6);
            userData.setUserTel(inData.getUserTel());
            userData.setUserType(inData.getUserType());
            userData.setUserId(String.valueOf(max + 1));
//            userData.setUserPassword("e10adc3949ba59abbe56e057f20f883e");
            userData.setUserJoinDate(DateUtil.format(new Date(), "yyyyMMdd"));
            userData.setUserPassword(CodecUtil.encryptMD5(password));
            userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            userData.setUserCreId(userInfo.getUserId());
            userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
            userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
            userData.setUserModiId(userInfo.getUserId());
            if (inData.getUserType() != null) {
                userData.setUserType(inData.getUserType());
            } else {
                userData.setUserType(0);
            }
            this.userDataMapper.insert(userData);
//         }
            GetSmsParaOutData para = this.thirdParaService.addUser();
            SmsUtil smsUtil = SmsUtil.getInstance(para.getSpCode(), para.getLoginName(), para.getPassword(), para.getTemplateId(), para.getTemplateContent());
            if (!"his".equals(inData.getPlatform())){
                smsUtil.sendUser(inData.getUserTel(), password);
            }
        }
    }

    public String checkResource(String userId, String path) {
        Example example = new Example(GaiaResource.class);
        example.createCriteria().andEqualTo("status", "1").andEqualTo("requestPath", path);
        List<GaiaResource> resourceList = this.resourceMapper.selectByExample(example);
        if (CollUtil.isEmpty(resourceList)) {
            return "0";
        } else {
            List<String> authIds = new ArrayList();
            Iterator var6 = resourceList.iterator();

            while(var6.hasNext()) {
                GaiaResource resource = (GaiaResource)var6.next();
                authIds.add(resource.getId());
            }

            example = new Example(GaiaAuthconfiData.class);
            example.createCriteria().andEqualTo("authconfiUser", userId).andIn("authobjId", authIds);
            int count = this.authconfiDataMapper.selectCountByExample(example);
            return count == 0 ? "0" : "1";
        }
    }

    public List<UserOutData> selectPharmacistByBrId(UserInData inData) {
        List<UserOutData> list = this.userDataMapper.selectPharmacistByBrId(inData);
        return list;
    }

    /**
     * 获取当前用户
     */
    @Override
    public UserLoginModelClient getCurrentUser() {
        String tokenCode = UserUtils.getTokenCode();
        String userStr = (String) this.redisManager.get(tokenCode);
        if (StringUtils.isBlank(userStr)) {
            throw new BusinessException("无效token");
        }
        try {
            UserLoginModelClient userLoginModelClient = JSONObject.parseObject(userStr, UserLoginModelClient.class);
            if (userLoginModelClient == null) {
                throw new BusinessException("无效token");
            }
            return userLoginModelClient;
        } catch (Exception e) {
            throw new BusinessException("无效token");
        }
    }

    /**
     * 根据传参查询部门（用户  门店  连锁 批发 部门）
     * @param type
     * @param userInfo
     * @return
     */
    @Override
    public List<SelectModel> getDepAndStore(String type,GetLoginOutData userInfo) {
        List<SelectModel>  selectModel = new ArrayList<>();
//      if (type.equals("2") || StringUtils.isEmpty(type)){
        StoreInData storeInData = new StoreInData();
        storeInData.setClient(userInfo.getClient());
        List<StoreOutData> storeData = this.storeDataMapper.getStoreList(storeInData);
        for (StoreOutData item :storeData){
            SelectModel sm = new SelectModel();
            sm.setKey(item.getStoCode());
            sm.setValue(item.getStoName());
            selectModel.add(sm);
        }
//      }
//      if (type.equals("4") || StringUtils.isEmpty(type)){
        List<DeptOutData> depData = this.depDataMapper.getDeptList(userInfo.getClient());
        for (DeptOutData item :depData){
            SelectModel sm = new SelectModel();
            sm.setKey(item.getDepId());
            sm.setValue(item.getDepName());
            selectModel.add(sm);
        }
//      }
        return selectModel;
    }

    @Override
    public Boolean getCheckPath(Map inData) {
        if("/store".equals(inData.get("path")) || "/compadmList".equals(inData.get("path"))
                || "/wholesaleList".equals(inData.get("path")) || "/store".equals(inData.get("/deptStore/dept"))){
            return true;
        }
        int count = this.authconfiDataMapper.selectCountByPath(inData);
        if(count >0){
            return true;
        }
        return false;
    }

    @Override
    public UserResourceRecord getResourceRecord(ResourceRecordForm resourceRecordForm) {
        UserResourceRecord cond = new UserResourceRecord();
        cond.setClient(resourceRecordForm.getClient());
        cond.setUserId(resourceRecordForm.getUserId());
        cond.setPageId(resourceRecordForm.getPageId());
        return gaiaUserResourceRecordMapper.getUnique(cond);
    }

    @Override
    public void updateResourceRecord(ResourceRecordForm resourceRecordForm) {
        UserResourceRecord cond = new UserResourceRecord();
        cond.setClient(resourceRecordForm.getClient());
        cond.setUserId(resourceRecordForm.getUserId());
        cond.setPageId(resourceRecordForm.getPageId());
        UserResourceRecord unique = gaiaUserResourceRecordMapper.getUnique(cond);
        if (unique == null) {
            unique = fillUserResourceRecord(resourceRecordForm);
            gaiaUserResourceRecordMapper.add(unique);
        }
    }

    @Override
    public void autoOperate(List<String> pageIdList) {
        //用户列表
        List<GaiaUserData> userList = userDataMapper.selectAll();
        for (GaiaUserData gaiaUserData : userList) {
            try {
                updateResourceByUser(gaiaUserData, pageIdList);
            } catch (Exception e) {
                log.error("<操作指引><自动操作><更新自动操作记录：{}>", e.getMessage(), e);
            }
        }
    }

    @Override
    public void setDefaultStore(StoreForm storeForm) {
        try {
            GaiaUserData cond = new GaiaUserData();
            cond.setClient(storeForm.getClient());
            cond.setUserId(storeForm.getUserId());
            GaiaUserData userData = userDataMapper.selectOne(cond);
            if (userData != null) {
                userData.setDefaultSite(storeForm.getStoreCode());
                userDataMapper.updateDefaultSite(userData);
            }
        } catch (Exception e) {
            log.error("<用户管理><设置默认门店><保存默认门店异常：{}>", e.getMessage(), e);
        }
    }

    @Override
    public List<GaiaUserData> getPharmacist(GetLoginOutData loginUser) {
        return userDataMapper.getPharmacist(loginUser);
    }

    @Override
    public void updateNbybInfo(GaiaUserData loginUser) {
        userDataMapper.updateNbybInfo(loginUser);
    }

    private void updateResourceByUser(GaiaUserData gaiaUserData, List<String> pageIdList) {
        for (String pageId : pageIdList) {
            ResourceRecordForm resourceRecordForm = new ResourceRecordForm();
            resourceRecordForm.setClient(gaiaUserData.getClient());
            resourceRecordForm.setUserId(gaiaUserData.getUserId());
            resourceRecordForm.setPageId(pageId);
            updateResourceRecord(resourceRecordForm);
        }
    }

    private UserResourceRecord fillUserResourceRecord(ResourceRecordForm resourceRecordForm) {
        UserResourceRecord record = new UserResourceRecord();
        record.setClient(resourceRecordForm.getClient());
        record.setUserId(resourceRecordForm.getUserId());
        record.setUserName(resourceRecordForm.getUserName());
        record.setPageId(resourceRecordForm.getPageId());
        record.setPagePath(resourceRecordForm.getPagePath());
        return record;
    }
}
