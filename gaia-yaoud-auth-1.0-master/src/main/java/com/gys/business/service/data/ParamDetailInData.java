package com.gys.business.service.data;

import lombok.Data;

@Data
public class ParamDetailInData {
   private Integer queryType;
   private String gsspBrId;
   private String gsspId;
   private String gsspName;
   private String gsspPara;
   private String gsspParaRemark;
   private String clientId;

   public ParamDetailInData() {
   }

   public ParamDetailInData(Integer queryType, String gsspBrId, String gsspId, String gsspName, String gsspPara, String gsspParaRemark) {
      this.queryType = queryType;
      this.gsspBrId = gsspBrId;
      this.gsspId = gsspId;
      this.gsspName = gsspName;
      this.gsspPara = gsspPara;
      this.gsspParaRemark = gsspParaRemark;
   }

   public static ParamDetailInData.ParamDetailInDataBuilder builder() {
      return new ParamDetailInData.ParamDetailInDataBuilder();
   }

   public static class ParamDetailInDataBuilder {
      private Integer queryType;
      private String gsspBrId;
      private String gsspId;
      private String gsspName;
      private String gsspPara;
      private String gsspParaRemark;

      public ParamDetailInData.ParamDetailInDataBuilder queryType(final Integer queryType) {
         this.queryType = queryType;
         return this;
      }

      public ParamDetailInData.ParamDetailInDataBuilder gsspBrId(final String gsspBrId) {
         this.gsspBrId = gsspBrId;
         return this;
      }

      public ParamDetailInData.ParamDetailInDataBuilder gsspId(final String gsspId) {
         this.gsspId = gsspId;
         return this;
      }

      public ParamDetailInData.ParamDetailInDataBuilder gsspName(final String gsspName) {
         this.gsspName = gsspName;
         return this;
      }

      public ParamDetailInData.ParamDetailInDataBuilder gsspPara(final String gsspPara) {
         this.gsspPara = gsspPara;
         return this;
      }

      public ParamDetailInData.ParamDetailInDataBuilder gsspParaRemark(final String gsspParaRemark) {
         this.gsspParaRemark = gsspParaRemark;
         return this;
      }

      public ParamDetailInData build() {
         return new ParamDetailInData(this.queryType, this.gsspBrId, this.gsspId, this.gsspName, this.gsspPara, this.gsspParaRemark);
      }

      public String toString() {
         return "ParamDetailInData.ParamDetailInDataBuilder(queryType=" + this.queryType + ", gsspBrId=" + this.gsspBrId + ", gsspId=" + this.gsspId + ", gsspName=" + this.gsspName + ", gsspPara=" + this.gsspPara + ", gsspParaRemark=" + this.gsspParaRemark + ")";
      }
   }
}
