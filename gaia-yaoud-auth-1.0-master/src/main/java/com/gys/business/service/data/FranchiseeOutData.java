package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class FranchiseeOutData {
   private String client;
   private String francName;
   private String francNo;
   private String francLegalPerson;//负责人编号
   private String francLegalPersonName;//负责人姓名
   private String francLegalPersonPhone;//负责人手机号
   private String francQua;
   private String francAddr;
   private String francCreDate;
   private String francCreTime;
   private String francCreId;
   private String francModiDate;
   private String francModiTime;
   private String francModiId;
   private String francLogo;
   private String francLogoUrl;
   private String francType1;//公司性质-单体店
   private String francType2;//公司性质-连锁公司
   private String francType3;//公司性质-批发公司
   private String francAss;//责任人编号
   private String francAssName;//责任人姓名
   private String francAssPhone;//责任人手机号
   private List<CompadmOutData> compadmList;
}
