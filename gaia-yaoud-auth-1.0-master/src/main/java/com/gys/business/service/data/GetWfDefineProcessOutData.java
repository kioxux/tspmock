package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class GetWfDefineProcessOutData {
   private String client;
   private String wfDefineCode;
   private BigDecimal wfSeq;
   private BigDecimal authSeq;
   private String wfKind;
   private String authGroupId;
   private List<GetWfDefineUserOutData> approveUserOutData;
}
