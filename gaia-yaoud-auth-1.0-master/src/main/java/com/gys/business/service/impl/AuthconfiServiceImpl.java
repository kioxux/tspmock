package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.AuthconfiService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.util.UtilConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.*;

@Service
@Slf4j
public class AuthconfiServiceImpl implements AuthconfiService {
   @Resource
   private GaiaAuthconfiDataMapper authconfiDataMapper;

   @Resource
   private GaiaGroupDataMapper groupDataMapper;

   @Resource
   private GaiaStoreDataMapper storeDataMapper;

   @Resource
   private GaiaDcDataMapper dcDataMapper;

   @Resource
   private GaiaDepDataMapper depDataMapper;

   @Transactional(rollbackFor = Exception.class)
   @Override
   public void authorization(AuthListInData inData) {
      if (!CollUtil.isEmpty(inData.getAuthList())) {
         List<String> userList = new ArrayList();
         List<GaiaAuthconfiData> list = new ArrayList<>();
         Iterator var3 = inData.getAuthList().iterator();

         while(var3.hasNext()) {
            AuthInData auth = (AuthInData)var3.next();
            userList.add(auth.getUserId());
         }

         String site = inData.getSite();
         if (!"AL".equals(inData.getType()) && !"AN".equals(inData.getType())) {
            this.authconfiDataMapper.deleteByTypeAndSite(inData.getClient(), userList, inData.getType(), inData.getSite());
         } else {
            this.authconfiDataMapper.deleteByType(inData.getClient(), userList, inData.getType());
            site = "GAD";
         }

         List<AuthGroupOutData> groupList = this.groupDataMapper.selectGroupByModule(inData.getType(),inData.getClient());
         Map<String, String> groupMap = new HashMap();
         Iterator var6 = groupList.iterator();

         while(var6.hasNext()) {
            AuthGroupOutData group = (AuthGroupOutData)var6.next();
            groupMap.put(group.getGroupId(), group.getGroupName());
         }

         var6 = inData.getAuthList().iterator();

         while(var6.hasNext()) {
            AuthInData auth = (AuthInData)var6.next();
            Iterator var8 = auth.getGroupList().stream().distinct().iterator();

            while(var8.hasNext()) {
               String group = (String)var8.next();
               GaiaAuthconfiData authconfiData = new GaiaAuthconfiData();
               authconfiData.setAuthconfiUser(auth.getUserId());
               authconfiData.setAuthGroupId(group);
               authconfiData.setAuthGroupName((String)groupMap.get(group));
               authconfiData.setAuthobjSite(site);
               authconfiData.setClient(inData.getClient());
               list.add(authconfiData);
               this.authconfiDataMapper.insert(authconfiData);
            }
         }
//         this.authconfiDataMapper.insertBatch(list);

      }
   }

   @Transactional(rollbackFor = Exception.class)
   @Override
   public void deleteAuth(AuthInData inData) {
      if (!"AL".equals(inData.getType()) && !"AN".equals(inData.getType())) {
         this.authconfiDataMapper.deleteByTypeAndSite(inData.getClient(), inData.getUserList(), inData.getType(), inData.getSite());
      } else {
         this.authconfiDataMapper.deleteByType(inData.getClient(), inData.getUserList(), inData.getType());
      }

   }

   @Override
   public AuthOutData authorizationList(AuthInData inData) {
      AuthOutData outData = new AuthOutData();
      List<AuthGroupOutData> groupList = this.groupDataMapper.selectGroupByModule(inData.getType(),inData.getClient());
      List<AuthOutData> authList = this.authconfiDataMapper.selectAuthList(inData);
      outData.setTableHeaderList(groupList);
      outData.setAuthList(authList);
      return outData;
   }

   @Override
   public List<AuthSiteOutData> selectSiteList(AuthInData inData) {
      List<AuthSiteOutData> authSiteOutDataList = Lists.newArrayList();
      List<GaiaDepData> depList = Lists.newArrayList();
      if(StringUtils.equalsIgnoreCase(inData.getType(), "WM")){
         Example example = new Example(GaiaDepData.class);
         example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("depCls", "10");
         example.orderBy("depType");
         depList = this.depDataMapper.selectByExample(example);
      }else{
         Example example = new Example(GaiaStoreData.class);
         example.createCriteria().andEqualTo("client", inData.getClient());
         List<GaiaStoreData> storeList = this.storeDataMapper.selectByExample(example);
         Iterator storeDataIterator = storeList.iterator();

         while(storeDataIterator.hasNext()) {
            GaiaStoreData storeData = (GaiaStoreData)storeDataIterator.next();
            AuthSiteOutData site = new AuthSiteOutData();
            site.setName(storeData.getStoName());
            site.setValue(storeData.getStoCode());
            authSiteOutDataList.add(site);
         }
         example = new Example(GaiaDcData.class);
         example.createCriteria().andEqualTo("client", inData.getClient());
         List<GaiaDcData> dcList = this.dcDataMapper.selectByExample(example);

         AuthSiteOutData site;
         for(Iterator<GaiaDcData> dcDataIterator = dcList.iterator(); dcDataIterator.hasNext(); authSiteOutDataList.add(site)) {
            GaiaDcData dc = dcDataIterator.next();
            site = new AuthSiteOutData();
            site.setValue(dc.getDcCode());
            if (StrUtil.isNotBlank(dc.getDcChainHead())) {
               site.setName("连锁总部-" + dc.getDcName());
            } else {
               site.setName("批发公司-" + dc.getDcName());
            }
         }

         example = new Example(GaiaDepData.class);
         example.createCriteria().andEqualTo("client", inData.getClient());
         example.orderBy("depType");
         depList = this.depDataMapper.selectByExample(example);
      }
      AuthSiteOutData site;
      for(Iterator<GaiaDepData> depDataIterator = depList.iterator(); depDataIterator.hasNext(); authSiteOutDataList.add(site)) {
         GaiaDepData dep = depDataIterator.next();
         site = new AuthSiteOutData();
         site.setValue(dep.getDepId());
         if ("10".equals(dep.getDepType())) {
            site.setName("加盟商-" + dep.getDepName());
         }

         if ("20".equals(dep.getDepType())) {
            site.setName("连锁总部-" + dep.getDepName());
         }

         if ("30".equals(dep.getDepType())) {
            site.setName("批发公司-" + dep.getDepName());
         }

         if ("40".equals(dep.getDepType())) {
            site.setName(dep.getDepName());
         }
      }

      return Lists.newArrayList(Sets.newHashSet(authSiteOutDataList));
   }

   @Override
   public List<AuthSiteOutData> selectStoreList(AuthInData inData) {
      List<AuthSiteOutData> authSiteOutDataList = Lists.newArrayList();
         Example example = new Example(GaiaStoreData.class);
         example.createCriteria().andEqualTo("client", inData.getClient());
         example.setOrderByClause("STO_CODE desc");
         List<GaiaStoreData> storeList = this.storeDataMapper.selectByExample(example);
         Iterator storeDataIterator = storeList.iterator();

         while(storeDataIterator.hasNext()) {
            GaiaStoreData storeData = (GaiaStoreData)storeDataIterator.next();
            AuthSiteOutData site = new AuthSiteOutData();
            site.setName(storeData.getStoName());
            site.setValue(storeData.getStoCode());
            authSiteOutDataList.add(site);
         }
      return authSiteOutDataList;
   }

   @Override
   public List<AuthSiteOutData> selectDepList(AuthInData inData) {
      List<AuthSiteOutData> authSiteOutDataList = Lists.newArrayList();
      List<GaiaDepData> depList = Lists.newArrayList();
      Example example = new Example(GaiaDepData.class);

      if(StringUtils.equalsIgnoreCase(inData.getType(), "MM")){
         //商采
         example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("depCls", "12");
         example.orderBy("depType");
         depList = this.depDataMapper.selectByExample(example);
      }else if(StringUtils.equalsIgnoreCase(inData.getType(), "FI")){
         //财务
         example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("depCls", "14");
         example.orderBy("depType");
         depList = this.depDataMapper.selectByExample(example);
      }
      AuthSiteOutData site;
      for(Iterator<GaiaDepData> depDataIterator = depList.iterator(); depDataIterator.hasNext(); authSiteOutDataList.add(site)) {
         GaiaDepData dep = depDataIterator.next();
         site = new AuthSiteOutData();
         site.setValue(dep.getDepId());
            site.setName(dep.getDepName());

      }
      return Lists.newArrayList(Sets.newHashSet(authSiteOutDataList));
   }

   @Override
   public List<AuthGroupOutData> selectGroupList(GetLoginOutData userInfo) {
      return this.groupDataMapper.selectGroupList(userInfo.getClient());
   }

   @Transactional(rollbackFor = Exception.class)
   @Override
   public void saveAuth(String client, String userId, List<String> groupList, List<String> siteList, String delUserId) {
      if (!StrUtil.isBlank(client) && !StrUtil.isBlank(userId) && !CollUtil.isEmpty(groupList)) {
         Iterator var6 = groupList.iterator();

         while(true) {
            while(true) {
               String group;
               Example example;
               List groupDataList;
               do {
                  if (!var6.hasNext()) {
                     return;
                  }

                  group = (String)var6.next();
                  example = new Example(GaiaGroupData.class);
                  example.createCriteria().andEqualTo("authGroupId", group);
                  groupDataList = this.groupDataMapper.selectByExample(example);
               } while(CollUtil.isEmpty(groupDataList));

               String groupName = ((GaiaGroupData)groupDataList.get(0)).getAuthGroupName();
               if (UtilConst.GAD_GROUP_LIST.contains(group)) {
                  if (StrUtil.isNotBlank(delUserId)) {
                     example = new Example(GaiaAuthconfiData.class);
                     example.createCriteria().andEqualTo("client", client).andEqualTo("authGroupId", group).andEqualTo("authconfiUser", delUserId);
                     this.authconfiDataMapper.deleteByExample(example);
                  }

                  GaiaAuthconfiData authconfiData = new GaiaAuthconfiData();
                  authconfiData.setAuthconfiUser(userId);
                  authconfiData.setAuthGroupId(group);
                  authconfiData.setAuthobjSite("GAD");
                  authconfiData.setClient(client);
                  GaiaAuthconfiData record = (GaiaAuthconfiData)this.authconfiDataMapper.selectByPrimaryKey(authconfiData);
                  if (!ObjectUtil.isNotNull(record)) {
                     authconfiData.setAuthGroupName(groupName);
                     this.authconfiDataMapper.insert(authconfiData);
                  }
               } else if (!CollUtil.isEmpty(siteList)) {
                  Iterator var11 = siteList.iterator();

                  while(var11.hasNext()) {
                     String site = (String)var11.next();
                     if (StrUtil.isNotBlank(delUserId)) {
                        example = new Example(GaiaAuthconfiData.class);
                        example.createCriteria().andEqualTo("client", client).andEqualTo("authGroupId", group).andEqualTo("authconfiUser", delUserId).andEqualTo("authobjSite", site);
                        this.authconfiDataMapper.deleteByExample(example);
                     }

                     GaiaAuthconfiData authconfiData = new GaiaAuthconfiData();
                     authconfiData.setAuthconfiUser(userId);
                     authconfiData.setAuthGroupId(group);
                     authconfiData.setAuthobjSite(site);
                     authconfiData.setClient(client);
                     GaiaAuthconfiData record = this.authconfiDataMapper.selectByPrimaryKey(authconfiData);
                     if (!ObjectUtil.isNotNull(record)) {
                        authconfiData.setAuthGroupName(groupName);
                        this.authconfiDataMapper.insert(authconfiData);
                     }
                  }
               }
            }
         }
      }
   }
}
