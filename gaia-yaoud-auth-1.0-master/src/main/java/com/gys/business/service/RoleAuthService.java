package com.gys.business.service;

import com.gys.business.service.data.auth.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import java.util.List;

/**
 * 权限管理
 *
 * @author wu mao yin
 * @version 2.0
 * @date 2021/12/22 17:14
 */
public interface RoleAuthService {
    List<StoreRoleRes> getStoreAndRols(GetLoginOutData userInfo);

    Object addRoleAuth(GetLoginOutData userInfo, List<RoleAuthAddReq> inData);

    List<StoreRoleRes> getStoreAndRoleNums(GetLoginOutData userInfo);

    Object deleteStoreRole(GetLoginOutData userInfo, List<StoreRoleReq> inData);

    Object resetStoreRole(GetLoginOutData userInfo, StoreRoleReq inData);

    Object updateStoreRole(GetLoginOutData userInfo, StoreRoleUpdateReq inData);

    /**
     * 根据选中的角色来进行初始化命名
     *
     * @param userInfo
     * @param inData
     * @return
     */
    String getRoleAuthDefaultName(GetLoginOutData userInfo, StroeRoleDelDetail inData);


    List<StoreRoleRes> getClientAllStoreInfo(String client);

    List<ModelClassify> addStoreRoleAuthInit(GetLoginOutData userInfo, String stoCode);

    JsonResult selectRoleInfoByStore(String client, String stoCode);

    JsonResult selectRoleInfoByStoreWithoutPerson(String client, String stoCode);
}
