package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
@Data
public class GlobalInData {
   private String client;
   private List<GlobalDetailInData> clientList;
}
