package com.gys.business.service.data.YunGeData;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

/**
 * 转自用申请
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WfZzysq {
	/**
	 * 申请部门编号
	 */
	@Field(
			name = "申请部门编号"
	)
	@JSONField(
			ordinal = 1
	)
	private String departmentCode;
	/**
	 * 申请部门描述
	 */
	@Field(
			name = "申请部门描述"
	)
	@JSONField(
			ordinal = 2
	)
	private String departmentName;
	/**
	 * 单号
	 */
	@Field(
			name = "转自用单号"
	)
	@JSONField(
			ordinal = 3
	)
	private String billNo;
	/**
	 * 原因
	 */
	@Field(
			name = "备注的申请原因"
	)
	@JSONField(
			ordinal = 4
	)
	private String reason;
	/**
	 * 总行数
	 */
	@Field(
			name = "该单总行数"
	)
	@JSONField(
			ordinal = 5
	)
	private String totalCount;
	/**
	 * 总数量
	 */
	@Field(
			name = "该单总数量"
	)
	@JSONField(
			ordinal = 6
	)
	private String totalQuantity;
	/**
	 * 批次成本额合计
	 */
	@Field(
			name = "批次成本额合计"
	)
	@JSONField(
			ordinal = 7
	)
	private String totalCostAmount;
}
