package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateOrgInData {
   @Field(
      name = "加盟商名称"
   )
   @JSONField(
      ordinal = 1
   )
   private String francName = "";
   @Field(
      name = "机构编码"
   )
   @JSONField(
      ordinal = 2
   )
   private String wfMechId = "";
   @Field(
      name = "机构名称"
   )
   @JSONField(
      ordinal = 3
   )
   private String wfMechName = "";
}
