package com.gys.business.service.data.dep.dto;

import lombok.Data;

/**
 * @Auther: tzh
 * @Date: 2021/12/20 10:50
 * @Description: DepDto
 * @Version 1.0.0
 */
@Data
public class DepDto {
    private String client;
}
