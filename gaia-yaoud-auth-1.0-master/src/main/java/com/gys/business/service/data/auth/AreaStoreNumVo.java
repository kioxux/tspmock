package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月27日 下午5:08
 */
@Data
public class AreaStoreNumVo implements Serializable {

    private String id;

    private String areaId;

    private String areaName;

    private Integer stoNum;

    private String level;

    private String parentId;
}

