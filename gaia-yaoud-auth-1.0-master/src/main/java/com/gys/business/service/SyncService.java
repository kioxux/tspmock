package com.gys.business.service;

import com.gys.business.service.data.GetSyncOutData;
import com.gys.common.data.GetLoginOutData;

public interface SyncService {
   GetSyncOutData selectBaseInfo(GetLoginOutData userInfo);
}
