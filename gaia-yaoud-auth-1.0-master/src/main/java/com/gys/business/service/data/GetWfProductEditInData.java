package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfProductEditInData {
    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 1
    )
    private String proSelfCode = "";

    @Field(
            name = "商品名称"
    )
    @JSONField(
            ordinal = 2
    )
    private String proCommonname = "";

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 3
    )
    private String proSpecs = "";

    @Field(
            name = "生产厂家"
    )
    @JSONField(
            ordinal = 4
    )
    private String proFactoryName = "";

    @Field(
            name = "批准文号"
    )
    @JSONField(
            ordinal = 5
    )
    private String proRegisterNo = "";

    @Field(
            name = "修改字段"
    )
    @JSONField(
            ordinal = 6
    )
    private String changeColumnDes = "";

    @Field(
            name = "修改前"
    )
    @JSONField(
            ordinal = 7
    )
    private String proChangeFrom = "";

    @Field(
            name = "修改后"
    )
    @JSONField(
            ordinal = 8
    )
    private String proChangeTo = "";

    @Field(
            name = "修改原因"
    )
    @JSONField(
            ordinal = 9
    )
    private String proRemarks = "";
}
