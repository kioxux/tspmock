package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaGlobalDataMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaGlobalData;
import com.gys.business.service.GlobalService;
import com.gys.business.service.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class GlobalServiceImpl implements GlobalService {
   @Autowired
   private GaiaGlobalDataMapper globalDataMapper;
   @Autowired
   private GaiaStoreDataMapper storeDataMapper;

   @Transactional
   public boolean authorization(GlobalInData inData) {
      Example example = new Example(GaiaGlobalData.class);
      example.createCriteria().andEqualTo("client", inData.getClient());
      this.globalDataMapper.deleteByExample(example);
      Iterator var3 = inData.getClientList().iterator();

      while(var3.hasNext()) {
         GlobalDetailInData detailInData = (GlobalDetailInData)var3.next();

         for(int i = 0; i < 5; ++i) {
            GaiaGlobalData globalData = new GaiaGlobalData();
            globalData.setClient(inData.getClient());
            globalData.setGlobalSite(detailInData.getStoreId());
            switch(i) {
            case 0:
               globalData.setGlobalType(detailInData.getTypeOne());
               globalData.setGlobalId("1");
               globalData.setGlobalName("是否显示成本");
               break;
            case 1:
               globalData.setGlobalType(detailInData.getTypeTwo());
               globalData.setGlobalId("2");
               globalData.setGlobalName("单体店/连锁店");
               break;
            case 2:
               globalData.setGlobalType(detailInData.getTypeThree());
               globalData.setGlobalId("3");
               globalData.setGlobalName("盘点方式");
               break;
            case 3:
               globalData.setGlobalType(detailInData.getTypeFour());
               globalData.setGlobalId("4");
               globalData.setGlobalName("中药单位");
               break;
            case 4:
               globalData.setGlobalType(detailInData.getTypeFive());
               globalData.setGlobalId("5");
               globalData.setGlobalName("是否允许导出");
            }

            this.globalDataMapper.insert(globalData);
         }
      }

      return true;
   }

   public List<GlobalOutData> authorList(GlobalInData inData) {
      List<StoreOutData> stores = new ArrayList();
      if (ObjectUtil.isNotEmpty(inData.getClient())) {
         StoreInData storeInData = new StoreInData();
         storeInData.setClient(inData.getClient());
         stores = this.storeDataMapper.getStoreList(storeInData);
      }

      List<GlobalOutData> list = new ArrayList();
      Iterator var4 = ((List)stores).iterator();

      while(var4.hasNext()) {
         StoreOutData storeOutData = (StoreOutData)var4.next();
         GlobalOutData outData = new GlobalOutData();
         outData.setStoreId(storeOutData.getStoCode());
         outData.setStoreName(storeOutData.getStoName());
         Example example = new Example(GaiaGlobalData.class);
         example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("globalSite", storeOutData.getStoCode());
         example.setOrderByClause("GLOBAL_ID ASC");
         List<GaiaGlobalData> globalDataList = this.globalDataMapper.selectByExample(example);

         for(int i = 0; i < globalDataList.size(); ++i) {
            GaiaGlobalData globalData = (GaiaGlobalData)globalDataList.get(i);
            String var11 = globalData.getGlobalId();
            byte var12 = -1;
            switch(var11.hashCode()) {
            case 49:
               if (var11.equals("1")) {
                  var12 = 0;
               }
               break;
            case 50:
               if (var11.equals("2")) {
                  var12 = 1;
               }
               break;
            case 51:
               if (var11.equals("3")) {
                  var12 = 2;
               }
               break;
            case 52:
               if (var11.equals("4")) {
                  var12 = 3;
               }
               break;
            case 53:
               if (var11.equals("5")) {
                  var12 = 4;
               }
               break;
            }

            switch(var12) {
            case 0:
               outData.setTypeOne(globalData.getGlobalType());
               break;
            case 1:
               outData.setTypeTwo(globalData.getGlobalType());
               break;
            case 2:
               outData.setTypeThree(globalData.getGlobalType());
               break;
            case 3:
               outData.setTypeFour(globalData.getGlobalType());
               break;
            case 4:
               outData.setTypeFive(globalData.getGlobalType());
               break;
            }
         }

         list.add(outData);
      }

      return list;
   }
}
