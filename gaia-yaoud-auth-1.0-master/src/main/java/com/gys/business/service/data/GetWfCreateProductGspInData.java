package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateProductGspInData {
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 1
   )
   private String proSelfCode = "";
   @Field(
      name = "商品名称"
   )
   @JSONField(
      ordinal = 2
   )
   private String proName = "";
   @Field(
      name = "通用名称"
   )
   @JSONField(
      ordinal = 3
   )
   private String proCommonname = "";
   @Field(
      name = "剂型"
   )
   @JSONField(
      ordinal = 4
   )
   private String proForm = "";
   @Field(
      name = "规格"
   )
   @JSONField(
      ordinal = 5
   )
   private String proSpecs = "";
   @Field(
      name = "包装规格"
   )
   @JSONField(
      ordinal = 6
   )
   private String proPackSpecs = "";
   @Field(
      name = "生产厂家"
   )
   @JSONField(
      ordinal = 7
   )
   private String sccj = "";
   @Field(
      name = "是否通过GMP认证"
   )
   @JSONField(
      ordinal = 9
   )
   private String proGmp = "";
   @Field(
      name = "批准文号"
   )
   @JSONField(
      ordinal = 10
   )
   private String proRegisterNo = "";
   @Field(
      name = "质量标准"
   )
   @JSONField(
      ordinal = 12
   )
   private String proQs = "";
   @Field(
      name = "功能主治"
   )
   @JSONField(
      ordinal = 13
   )
   private String proFunction = "";
   @Field(
      name = "有效期"
   )
   @JSONField(
      ordinal = 14
   )
   private String proLifeDate = "";
   @Field(
      name = "贮存条件"
   )
   @JSONField(
      ordinal = 15
   )
   private String proStorageConditionName = "";
   @Field(
      name = "供货单位名称"
   )
   @JSONField(
      ordinal = 15
   )
   private String proSupplyName = "";
   @Field(
      name = "供货单位地址"
   )
   @JSONField(
      ordinal = 16
   )
   private String proSupplyAdd = "";
   @Field(
      name = "供货单位联系人"
   )
   @JSONField(
      ordinal = 17
   )
   private String proSupplyContact = "";
   @Field(
      name = "供货单位联系电话"
   )
   @JSONField(
      ordinal = 18
   )
   private String proSupplyTel = "";
}
