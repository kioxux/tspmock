package com.gys.business.service;

import com.gys.common.data.JsonResult;

public interface AreaService {

    /**
     * 获取省市区集合
     * @return
     */
    JsonResult getAreaList(String client, String userId);
}
