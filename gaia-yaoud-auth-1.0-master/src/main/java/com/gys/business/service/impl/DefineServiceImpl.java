package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaWfDefineMapper;
import com.gys.business.mapper.GaiaWfDefineProcessMapper;
import com.gys.business.mapper.entity.GaiaWfDefine;
import com.gys.business.mapper.entity.GaiaWfDefineProcess;
import com.gys.business.service.DefineService;
import com.gys.business.service.data.GetDefineInData;
import com.gys.business.service.data.GetDefineOutData;
import com.gys.business.service.data.GetWfCreateAuthInData;
import com.gys.business.service.data.GetWfCreateCategoryAnalysisInData;
import com.gys.business.service.data.GetWfCreateCkbsbyInData;
import com.gys.business.service.data.GetWfCreateCusGspInData;
import com.gys.business.service.data.GetWfCreateDelegateInData;
import com.gys.business.service.data.GetWfCreateDpgjCompadmInData;
import com.gys.business.service.data.GetWfCreateDpgjSingleInData;
import com.gys.business.service.data.GetWfCreateDpzlCompadmInData;
import com.gys.business.service.data.GetWfCreateDpzlSingleInData;
import com.gys.business.service.data.GetWfCreateKctzInData;
import com.gys.business.service.data.GetWfCreateLossCompadmInData;
import com.gys.business.service.data.GetWfCreateLossSingleInData;
import com.gys.business.service.data.GetWfCreateOrgInData;
import com.gys.business.service.data.GetWfCreatePhtzInData;
import com.gys.business.service.data.GetWfCreateProductGspInData;
import com.gys.business.service.data.GetWfCreatePurOrderInData;
import com.gys.business.service.data.GetWfCreatePurPayInData;
import com.gys.business.service.data.GetWfCreateReturnCompadmInData;
import com.gys.business.service.data.GetWfCreateRkshInData;
import com.gys.business.service.data.GetWfCreateRkysInData;
import com.gys.business.service.data.GetWfCreateSpbsInData;
import com.gys.business.service.data.GetWfCreateSpyhInData;
import com.gys.business.service.data.GetWfCreateSupGspInData;
import com.gys.business.service.data.GetWfCreateZdzlCompadmInData;
import com.gys.business.service.data.GetWfCreateZdzlSingleInData;
import com.gys.business.service.data.GetWfCreateZzyInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.Util;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Service
@Slf4j
public class DefineServiceImpl implements DefineService {
   @Resource
   private GaiaWfDefineMapper wfDefineMapper;

   @Resource
   private GaiaWfDefineProcessMapper wfDefineProcessMapper;

   public List<GetDefineOutData> selectDefaultList(GetDefineInData inData, GetLoginOutData userInfo) {
      inData.setClient(userInfo.getClient());
      List<GetDefineOutData> defineList = this.wfDefineMapper.selectDefaultList(inData);
      Example example = new Example(GaiaWfDefineProcess.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient());
      List<GaiaWfDefineProcess> processList = this.wfDefineProcessMapper.selectByExample(example);
      Map<String, Set<BigDecimal>> wfSeqMap = new HashMap();
      Map<String, List<String>> groupMap = new HashMap();
      Iterator processIterator = processList.iterator();

      String defineCode;
      while(processIterator.hasNext()) {
         GaiaWfDefineProcess process = (GaiaWfDefineProcess)processIterator.next();
         defineCode = process.getWfDefineCode() + "-" + process.getWfSeq().intValue();
         List<String> group = groupMap.get(defineCode);
         if (group == null) {
            group = new ArrayList();
         }

         if (StrUtil.isNotBlank(process.getAuthGroupId())) {
            (group).add(process.getAuthGroupId());
         }

         groupMap.put(defineCode, group);
         String seqKey = process.getWfDefineCode();
         Set<BigDecimal> wfSeqSet = wfSeqMap.get(seqKey);
         if (wfSeqSet == null) {
            wfSeqSet = new HashSet();
         }

         (wfSeqSet).add(process.getWfSeq());
         wfSeqMap.put(seqKey, wfSeqSet);
      }

      processIterator = defineList.iterator();

      while(processIterator.hasNext()) {
         GetDefineOutData define = (GetDefineOutData)processIterator.next();
         define.setApproveGroup1(groupMap.get(define.getDefineType() + "-" + "1"));
         define.setApproveGroup2(groupMap.get(define.getDefineType() + "-" + "2"));
         define.setApproveGroup3(groupMap.get(define.getDefineType() + "-" + "3"));
         define.setApproveGroup4(groupMap.get(define.getDefineType() + "-" + "4"));
         if (wfSeqMap.get(define.getDefineType()) != null) {
            define.setMaxGroup((wfSeqMap.get(define.getDefineType())).size());
         }

         defineCode = define.getDefineType();
         byte var66 = -1;
         switch(defineCode.hashCode()) {
         case 867125806:
            if (defineCode.equals("GAIA_WF_001")) {
               var66 = 0;
            }
            break;
         case 867125807:
            if (defineCode.equals("GAIA_WF_002")) {
               var66 = 1;
            }
            break;
         case 867125808:
            if (defineCode.equals("GAIA_WF_003")) {
               var66 = 2;
            }
            break;
         case 867125809:
            if (defineCode.equals("GAIA_WF_004")) {
               var66 = 3;
            }
            break;
         case 867125810:
            if (defineCode.equals("GAIA_WF_005")) {
               var66 = 4;
            }
            break;
         case 867125811:
            if (defineCode.equals("GAIA_WF_006")) {
               var66 = 5;
            }
            break;
         case 867125812:
            if (defineCode.equals("GAIA_WF_007")) {
               var66 = 6;
            }
            break;
         case 867125813:
            if (defineCode.equals("GAIA_WF_008")) {
               var66 = 7;
            }
            break;
         case 867125814:
            if (defineCode.equals("GAIA_WF_009")) {
               var66 = 8;
            }
         case 867125815:
         case 867125816:
         case 867125817:
         case 867125818:
         case 867125819:
         case 867125820:
         case 867125821:
         case 867125822:
         case 867125823:
         case 867125824:
         case 867125825:
         case 867125826:
         case 867125827:
         case 867125828:
         case 867125829:
         case 867125830:
         case 867125831:
         case 867125832:
         case 867125833:
         case 867125834:
         case 867125835:
         case 867125846:
         case 867125847:
         case 867125848:
         case 867125849:
         case 867125850:
         case 867125851:
         case 867125852:
         case 867125853:
         case 867125854:
         case 867125855:
         case 867125856:
         case 867125857:
         case 867125858:
         case 867125859:
         case 867125860:
         case 867125861:
         case 867125862:
         case 867125863:
         case 867125864:
         case 867125865:
         case 867125866:
         default:
            break;
         case 867125836:
            if (defineCode.equals("GAIA_WF_010")) {
               var66 = 9;
            }
            break;
         case 867125837:
            if (defineCode.equals("GAIA_WF_011")) {
               var66 = 10;
            }
            break;
         case 867125838:
            if (defineCode.equals("GAIA_WF_012")) {
               var66 = 11;
            }
            break;
         case 867125839:
            if (defineCode.equals("GAIA_WF_013")) {
               var66 = 12;
            }
            break;
         case 867125840:
            if (defineCode.equals("GAIA_WF_014")) {
               var66 = 13;
            }
            break;
         case 867125841:
            if (defineCode.equals("GAIA_WF_015")) {
               var66 = 14;
            }
            break;
         case 867125842:
            if (defineCode.equals("GAIA_WF_016")) {
               var66 = 15;
            }
            break;
         case 867125843:
            if (defineCode.equals("GAIA_WF_017")) {
               var66 = 16;
            }
            break;
         case 867125844:
            if (defineCode.equals("GAIA_WF_018")) {
               var66 = 17;
            }
            break;
         case 867125845:
            if (defineCode.equals("GAIA_WF_019")) {
               var66 = 18;
            }
            break;
         case 867125867:
            if (defineCode.equals("GAIA_WF_020")) {
               var66 = 19;
            }
            break;
         case 867125868:
            if (defineCode.equals("GAIA_WF_021")) {
               var66 = 20;
            }
            break;
         case 867125869:
            if (defineCode.equals("GAIA_WF_022")) {
               var66 = 21;
            }
            break;
         case 867125870:
            if (defineCode.equals("GAIA_WF_023")) {
               var66 = 22;
            }
            break;
         case 867125871:
            if (defineCode.equals("GAIA_WF_024")) {
               var66 = 23;
            }
            break;
         case 867125872:
            if (defineCode.equals("GAIA_WF_025")) {
               var66 = 24;
            }
            break;
         case 867125873:
            if (defineCode.equals("GAIA_WF_026")) {
               var66 = 25;
            }
         }

         switch(var66) {
         case 0:
            GetWfCreateAuthInData authTitle = Util.initFieldName(GetWfCreateAuthInData.class);
            List<GetWfCreateAuthInData> authDetail = new ArrayList();
            authDetail.add(authTitle);
            define.setWfJsonString(JSONObject.toJSONString(authDetail));
            break;
         case 1:
            GetWfCreateDelegateInData delegateTitle = Util.initFieldName(GetWfCreateDelegateInData.class);
            List<GetWfCreateDelegateInData> delegateDetail = new ArrayList();
            delegateDetail.add(delegateTitle);
            define.setWfJsonString(JSONObject.toJSONString(delegateDetail));
            break;
         case 2:
            GetWfCreateOrgInData orgTitle = Util.initFieldName(GetWfCreateOrgInData.class);
            List<GetWfCreateOrgInData> orgDetail = new ArrayList();
            orgDetail.add(orgTitle);
            define.setWfJsonString(JSONObject.toJSONString(orgDetail));
            break;
         case 3:
            GetWfCreateCategoryAnalysisInData categoryAnalysisTitle = Util.initFieldName(GetWfCreateCategoryAnalysisInData.class);
            List<GetWfCreateCategoryAnalysisInData> categoryAnalysisDetail = new ArrayList();
            categoryAnalysisDetail.add(categoryAnalysisTitle);
            define.setWfJsonString(JSONObject.toJSONString(categoryAnalysisDetail));
            break;
         case 4:
            GetWfCreateReturnCompadmInData returnCompadmTitle = Util.initFieldName(GetWfCreateReturnCompadmInData.class);
            List<GetWfCreateReturnCompadmInData> returnCompadmDetail = new ArrayList();
            returnCompadmDetail.add(returnCompadmTitle);
            define.setWfJsonString(JSONObject.toJSONString(returnCompadmDetail));
            break;
         case 5:
            GetWfCreateLossCompadmInData lossCompadmTitle = Util.initFieldName(GetWfCreateLossCompadmInData.class);
            List<GetWfCreateLossCompadmInData> lossCompadmDetail = new ArrayList();
            lossCompadmDetail.add(lossCompadmTitle);
            define.setWfJsonString(JSONObject.toJSONString(lossCompadmDetail));
            break;
         case 6:
            GetWfCreateLossSingleInData lossSingleTitle = Util.initFieldName(GetWfCreateLossSingleInData.class);
            List<GetWfCreateLossSingleInData> lossSingleDetail = new ArrayList();
            lossSingleDetail.add(lossSingleTitle);
            define.setWfJsonString(JSONObject.toJSONString(lossSingleDetail));
            break;
         case 7:
            GetWfCreateDpgjCompadmInData dpgjCompadmTitle = Util.initFieldName(GetWfCreateDpgjCompadmInData.class);
            List<GetWfCreateDpgjCompadmInData> dpgjCompadmDetail = new ArrayList();
            dpgjCompadmDetail.add(dpgjCompadmTitle);
            define.setWfJsonString(JSONObject.toJSONString(dpgjCompadmDetail));
            break;
         case 8:
            GetWfCreateDpgjSingleInData dpgjSingleTitle = Util.initFieldName(GetWfCreateDpgjSingleInData.class);
            List<GetWfCreateDpgjSingleInData> dpgjSingleDetail = new ArrayList();
            dpgjSingleDetail.add(dpgjSingleTitle);
            define.setWfJsonString(JSONObject.toJSONString(dpgjSingleDetail));
            break;
         case 9:
            GetWfCreateDpzlCompadmInData dpzlCompadmTitle = Util.initFieldName(GetWfCreateDpzlCompadmInData.class);
            List<GetWfCreateDpzlCompadmInData> dpzlCompadmDetail = new ArrayList();
            dpzlCompadmDetail.add(dpzlCompadmTitle);
            define.setWfJsonString(JSONObject.toJSONString(dpzlCompadmDetail));
            break;
         case 10:
            GetWfCreateDpzlSingleInData dpzlSingleTitle = Util.initFieldName(GetWfCreateDpzlSingleInData.class);
            List<GetWfCreateDpzlSingleInData> dpzlSingleDetail = new ArrayList();
            dpzlSingleDetail.add(dpzlSingleTitle);
            define.setWfJsonString(JSONObject.toJSONString(dpzlSingleDetail));
            break;
         case 11:
            GetWfCreateZdzlCompadmInData zdzlCompadmTitle = Util.initFieldName(GetWfCreateZdzlCompadmInData.class);
            List<GetWfCreateZdzlCompadmInData> zdzlCompadmDetail = new ArrayList();
            zdzlCompadmDetail.add(zdzlCompadmTitle);
            define.setWfJsonString(JSONObject.toJSONString(zdzlCompadmDetail));
            break;
         case 12:
            GetWfCreateZdzlSingleInData zdzlSingleTitle = Util.initFieldName(GetWfCreateZdzlSingleInData.class);
            List<GetWfCreateZdzlSingleInData> zdzlSingleDetail = new ArrayList();
            zdzlSingleDetail.add(zdzlSingleTitle);
            define.setWfJsonString(JSONObject.toJSONString(zdzlSingleDetail));
            break;
         case 13:
            GetWfCreateProductGspInData productGspTitle = Util.initFieldName(GetWfCreateProductGspInData.class);
            List<GetWfCreateProductGspInData> productGspDetail = new ArrayList();
            productGspDetail.add(productGspTitle);
            define.setWfJsonString(JSONObject.toJSONString(productGspDetail));
            break;
         case 14:
            GetWfCreateSupGspInData supGspTitle = (GetWfCreateSupGspInData)Util.initFieldName(GetWfCreateSupGspInData.class);
            List<GetWfCreateSupGspInData> supGspDetail = new ArrayList();
            supGspDetail.add(supGspTitle);
            define.setWfJsonString(JSONObject.toJSONString(supGspDetail));
            break;
         case 15:
            GetWfCreateCusGspInData cusGspTitle = (GetWfCreateCusGspInData)Util.initFieldName(GetWfCreateCusGspInData.class);
            List<GetWfCreateCusGspInData> cusGspDetail = new ArrayList();
            cusGspDetail.add(cusGspTitle);
            define.setWfJsonString(JSONObject.toJSONString(cusGspDetail));
            break;
         case 16:
            GetWfCreatePurOrderInData purOrderTitle = (GetWfCreatePurOrderInData)Util.initFieldName(GetWfCreatePurOrderInData.class);
            List<GetWfCreatePurOrderInData> purOrderDetail = new ArrayList();
            purOrderDetail.add(purOrderTitle);
            define.setWfJsonString(JSONObject.toJSONString(purOrderDetail));
            break;
         case 17:
            GetWfCreatePurPayInData purPayTitle = (GetWfCreatePurPayInData)Util.initFieldName(GetWfCreatePurPayInData.class);
            List<GetWfCreatePurPayInData> purPayDetail = new ArrayList();
            purPayDetail.add(purPayTitle);
            define.setWfJsonString(JSONObject.toJSONString(purPayDetail));
            break;
         case 18:
            GetWfCreateSpbsInData spbsTitle = (GetWfCreateSpbsInData)Util.initFieldName(GetWfCreateSpbsInData.class);
            List<GetWfCreateSpbsInData> spbsDetail = new ArrayList();
            spbsDetail.add(spbsTitle);
            define.setWfJsonString(JSONObject.toJSONString(spbsDetail));
            break;
         case 19:
            GetWfCreateZzyInData zzyTitle = (GetWfCreateZzyInData)Util.initFieldName(GetWfCreateZzyInData.class);
            List<GetWfCreateZzyInData> zzyDetail = new ArrayList();
            zzyDetail.add(zzyTitle);
            define.setWfJsonString(JSONObject.toJSONString(zzyDetail));
            break;
         case 20:
            GetWfCreateSpyhInData spyhTitle = (GetWfCreateSpyhInData)Util.initFieldName(GetWfCreateSpyhInData.class);
            List<GetWfCreateSpyhInData> spyhDetail = new ArrayList();
            spyhDetail.add(spyhTitle);
            define.setWfJsonString(JSONObject.toJSONString(spyhDetail));
            break;
         case 21:
            GetWfCreateCkbsbyInData ckbsbyTitle = (GetWfCreateCkbsbyInData)Util.initFieldName(GetWfCreateCkbsbyInData.class);
            List<GetWfCreateCkbsbyInData> ckbsbyDetail = new ArrayList();
            ckbsbyDetail.add(ckbsbyTitle);
            define.setWfJsonString(JSONObject.toJSONString(ckbsbyDetail));
            break;
         case 22:
            GetWfCreatePhtzInData phtzTitle = (GetWfCreatePhtzInData)Util.initFieldName(GetWfCreatePhtzInData.class);
            List<GetWfCreatePhtzInData> phtzDetail = new ArrayList();
            phtzDetail.add(phtzTitle);
            define.setWfJsonString(JSONObject.toJSONString(phtzDetail));
            break;
         case 23:
            GetWfCreateRkshInData rkshTitle = (GetWfCreateRkshInData)Util.initFieldName(GetWfCreateRkshInData.class);
            List<GetWfCreateRkshInData> rkshDetail = new ArrayList();
            rkshDetail.add(rkshTitle);
            define.setWfJsonString(JSONObject.toJSONString(rkshDetail));
            break;
         case 24:
            GetWfCreateRkysInData rkysTitle = (GetWfCreateRkysInData)Util.initFieldName(GetWfCreateRkysInData.class);
            List<GetWfCreateRkysInData> rkysDetail = new ArrayList();
            rkysDetail.add(rkysTitle);
            define.setWfJsonString(JSONObject.toJSONString(rkysDetail));
            break;
         case 25:
            GetWfCreateKctzInData kctzTitle = (GetWfCreateKctzInData)Util.initFieldName(GetWfCreateKctzInData.class);
            List<GetWfCreateKctzInData> kctzDetail = new ArrayList();
            kctzDetail.add(kctzTitle);
            define.setWfJsonString(JSONObject.toJSONString(kctzDetail));
         }
      }

      return defineList;
   }

   @Transactional
   public void updateDefaultGroup(GetDefineInData inData, GetLoginOutData userInfo) {
      Example example = new Example(GaiaWfDefine.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("wfDefineCode", inData.getDefineType());
      GaiaWfDefine define = (GaiaWfDefine)this.wfDefineMapper.selectOneByExample(example);
      if (ObjectUtil.isNull(define)) {
         throw new BusinessException("提示：工作流类型不存在3");
      } else {
         example = new Example(GaiaWfDefineProcess.class);
         example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("wfDefineCode", inData.getDefineType()).andEqualTo("wfSeq", inData.getDefineIndex());
         this.wfDefineProcessMapper.deleteByExample(example);
         int i = 1;
         List<String> groups = inData.getGroupId();
         if (CollUtil.isEmpty((Collection)groups)) {
            groups = new ArrayList();
            ((List)groups).add("");
         }

         for(Iterator var7 = ((List)groups).iterator(); var7.hasNext(); ++i) {
            String group = (String)var7.next();
            GaiaWfDefineProcess process = new GaiaWfDefineProcess();
            process.setClient(userInfo.getClient());
            process.setPoCompanyCode(define.getPoCompanyCode());
            process.setWfDefineCode(inData.getDefineType());
            process.setWfSeq(new BigDecimal(inData.getDefineIndex()));
            process.setAuthSeq(new BigDecimal(i));
            process.setWfKind(define.getWfKind());
            process.setAuthGroupId(group);
            process.setCreateUser(userInfo.getUserId());
            process.setCreateTime(DateUtil.format(new Date(), "yyyyMMdd"));
            this.wfDefineProcessMapper.insert(process);
         }

      }
   }
}
