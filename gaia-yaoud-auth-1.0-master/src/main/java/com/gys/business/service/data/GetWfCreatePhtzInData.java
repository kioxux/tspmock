package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreatePhtzInData {
   @Field(
      name = "申请人"
   )
   @JSONField(
      ordinal = 1
   )
   private String wmSqr = "";
   @Field(
      name = "调整原因"
   )
   @JSONField(
      ordinal = 2
   )
   private String wmSqyy = "";
   @Field(
      name = "批次号"
   )
   @JSONField(
      ordinal = 3
   )
   private String batchCode = "";
   @Field(
      name = "供应商"
   )
   @JSONField(
      ordinal = 4
   )
   private String supName = "";
   @Field(
      name = "收货人"
   )
   @JSONField(
      ordinal = 5
   )
   private String receiver = "";
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 6
   )
   private String proCode = "";
   @Field(
      name = "名称"
   )
   @JSONField(
      ordinal = 7
   )
   private String proName = "";
   @Field(
      name = "规格"
   )
   @JSONField(
      ordinal = 8
   )
   private String proSpecs = "";
   @Field(
      name = "调整前厂家"
   )
   @JSONField(
      ordinal = 9
   )
   private String beforeFactory = "";
   @Field(
           name = "调整前产地"
   )
   @JSONField(
           ordinal = 10
   )
   private String beforePlace = "";
   @Field(
           name = "调整后厂家"
   )
   @JSONField(
           ordinal = 11
   )
   private String afterFactory = "";
   @Field(
           name = "调整后产地"
   )
   @JSONField(
           ordinal = 12
   )
   private String afterPlace = "";
   @Field(
      name = "单位"
   )
   @JSONField(
      ordinal = 13
   )
   private String proUnit = "";
   @Field(
      name = "调整前批号"
   )
   @JSONField(
      ordinal = 14
   )
   private String tzph = "";
   @Field(
      name = "调整前生产日期"
   )
   @JSONField(
      ordinal = 15
   )
   private String tzscrq = "";
   @Field(
      name = "调整前有效期"
   )
   @JSONField(
      ordinal = 16
   )
   private String tzyxq = "";
   @Field(
      name = "调整后批号"
   )
   @JSONField(
      ordinal = 17
   )
   private String tzhph = "";
   @Field(
      name = "调整后生产日期"
   )
   @JSONField(
      ordinal = 18
   )
   private String tzhscrq = "";
   @Field(
      name = "调整后有效期"
   )
   @JSONField(
      ordinal = 19
   )
   private String tzhyxq = "";
}
