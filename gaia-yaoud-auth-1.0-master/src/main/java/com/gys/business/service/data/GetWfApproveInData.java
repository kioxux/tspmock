package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfApproveInData {
   /**
    * 工作流类型
    */
   private String wfDefineCode;

   /**
    * 单据编号
    */
   private String wfOrder;

   /**
    * 工作流状态
    */
   private String wfStatus;

   /**
    * 加盟商编号
    */
   private String clientId;

   /**
    * 门店
    */
   private String site;

   /**
    * 该工作流创建人
    */
   private String createUser;

   /**
    * 目前审批人
    */
   private String approverUserId;

   /**
    * 审批意见
    */
   private String memo;
}
