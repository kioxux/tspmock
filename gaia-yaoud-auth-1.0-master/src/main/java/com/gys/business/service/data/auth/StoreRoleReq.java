package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午6:02
 */
@Data
public class StoreRoleReq implements Serializable {

    private String stoCode;

    private List<StroeRoleDelDetail> roles;
}

