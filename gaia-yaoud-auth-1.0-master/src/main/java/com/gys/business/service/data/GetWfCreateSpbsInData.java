package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateSpbsInData {
   @Field(
      name = "申请人"
   )
   @JSONField(
      ordinal = 1
   )
   private String wmSqr = "";
   @Field(
      name = "申请单号"
   )
   @JSONField(
      ordinal = 2
   )
   private String wmSqdh = "";
   @Field(
      name = "报损原因"
   )
   @JSONField(
      ordinal = 3
   )
   private String wmDsyy = "";
   @Field(
      name = "商品编码"
   )
   @JSONField(
      ordinal = 4
   )
   private String proCode = "";
   @Field(
      name = "名称"
   )
   @JSONField(
      ordinal = 5
   )
   private String proName = "";
   @Field(
      name = "规格"
   )
   @JSONField(
      ordinal = 6
   )
   private String proSpecs = "";
   @Field(
      name = "厂家"
   )
   @JSONField(
      ordinal = 7
   )
   private String sccj = "";
   @Field(
      name = "单位"
   )
   @JSONField(
      ordinal = 8
   )
   private String proUnit = "";
   @Field(
      name = "批号"
   )
   @JSONField(
      ordinal = 9
   )
   private String batchCode = "";
   @Field(
      name = "生产日期"
   )
   @JSONField(
      ordinal = 10
   )
   private String proDate = "";
   @Field(
      name = "有效期"
   )
   @JSONField(
      ordinal = 11
   )
   private String proValid = "";
   @Field(
      name = "入库日期"
   )
   @JSONField(
      ordinal = 12
   )
   private String proWhDate = "";
   @Field(
      name = "供应商"
   )
   @JSONField(
      ordinal = 13
   )
   private String supName = "";
   @Field(
      name = "成本价"
   )
   @JSONField(
      ordinal = 14
   )
   private String proCostPrice = "";
   @Field(
      name = "申请数量"
   )
   @JSONField(
      ordinal = 15
   )
   private String qty = "";
   @Field(
      name = "金额"
   )
   @JSONField(
      ordinal = 16
   )
   private String amount = "";
}
