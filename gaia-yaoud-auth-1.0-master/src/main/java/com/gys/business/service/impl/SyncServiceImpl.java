package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.SyncService;
import com.gys.business.service.data.GetSyncOutData;
import com.gys.business.service.data.GetSyncStoreOutData;
import com.gys.business.service.data.GetSyncUserOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SyncServiceImpl implements SyncService {
   @Autowired
   private GaiaUserDataMapper userDataMapper;
   @Autowired
   private GaiaStoreDataMapper storeDataMapper;

   public GetSyncOutData selectBaseInfo(GetLoginOutData userInfo) {
      GetSyncOutData outData = new GetSyncOutData();
      Example example = new Example(GaiaUserData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient());
      List<GaiaUserData> userDataList = this.userDataMapper.selectByExample(example);
      List<GetSyncUserOutData> userList = new ArrayList();
      Iterator var6 = userDataList.iterator();

      while(var6.hasNext()) {
         GaiaUserData userData = (GaiaUserData)var6.next();
         GetSyncUserOutData record = new GetSyncUserOutData();
         BeanUtil.copyProperties(userData, record);
         userList.add(record);
      }

      outData.setUserList(userList);
      example = new Example(GaiaStoreData.class);
      example.createCriteria().andEqualTo("client", userInfo.getClient());
      List<GaiaStoreData> storeDataList = this.storeDataMapper.selectByExample(example);
      List<GetSyncStoreOutData> storeList = new ArrayList();
      Iterator var13 = storeDataList.iterator();

      while(var13.hasNext()) {
         GaiaStoreData storeData = (GaiaStoreData)var13.next();
         GetSyncStoreOutData record = new GetSyncStoreOutData();
         BeanUtil.copyProperties(storeData, record);
         storeList.add(record);
      }

      outData.setStoreList(storeList);
      return outData;
   }
}
