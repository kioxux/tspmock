package com.gys.business.service.data;

import cn.hutool.core.util.ObjectUtil;
import lombok.Data;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@Data
public class GetResourceOutData implements Serializable {
   private static final long serialVersionUID = 1L;
   private String id;
   private String name;
   private String icon;
   private String path;
   private String pagePath;
   private String title;
   private String pid;
   private String type;
   private String query;
   private List<GetResourceOutData> nodes;

   public void addChild(GetResourceOutData child) {
      if (ObjectUtil.isEmpty(this.nodes)) {
         this.nodes = new ArrayList();
      }

      this.nodes.add(child);
   }

   @ConstructorProperties({"id", "name", "icon", "path", "pagePath", "title", "pid", "type", "query", "nodes"})
   public GetResourceOutData(final String id, final String name, final String icon, final String path, final String pagePath, final String title, final String pid, final String type, final String query, final List<GetResourceOutData> nodes) {
      this.id = id;
      this.name = name;
      this.icon = icon;
      this.path = path;
      this.pagePath = pagePath;
      this.title = title;
      this.pid = pid;
      this.type = type;
      this.query = query;
      this.nodes = nodes;
   }

   public GetResourceOutData() {
   }
}
