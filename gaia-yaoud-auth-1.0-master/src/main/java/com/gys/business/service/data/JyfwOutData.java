package com.gys.business.service.data;

import lombok.Data;

@Data
public class JyfwOutData {
   private String id;
   private String client;
   private String clientName;
   private String jyfwOrgid;
   private String jyfwOrgname;
   private String jyfwId;
   private String jyfwName;
   private String jyfwCreDate;
   private String jyfwCreTime;
   private String jyfwCreName;
}
