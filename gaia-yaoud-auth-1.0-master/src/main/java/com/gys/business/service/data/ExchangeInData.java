package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class ExchangeInData {
   private String clientId;
   private String gsiesVoucherId;
   private String gsiesBrId;
   private String gsiesBrName;
   private String gsiesStoreGroup;
   private String gsiesMemberClass;
   private String gsiesDateStart;
   private String gsiesDateEnd;
   private String gsiesExchangeProId;
   private String gsieslExchangeIntegra;
   private BigDecimal gsiesExchangeAmt;
   private String gsiesExchangeQty;
   private String gsiesExchangeRepeat;
   private String gsiesIntegralAddSet;
   private String gsiesStatus;
   public int pageSize;
   private int pageNum;
   private List<String> idList;
}
