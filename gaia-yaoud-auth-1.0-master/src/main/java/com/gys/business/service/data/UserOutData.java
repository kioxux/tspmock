package com.gys.business.service.data;

import lombok.Data;

@Data
public class UserOutData {
   private String userId;
   private String client;
   private String userNam;
   private String userSex;
   private String userTel;
   private String userIdc;
   private String userYsId;
   private String userYsZyfw;
   private String userYsZylb;
   private String depId;
   private String depName;
   private String userAddr;
   private String userSta;
   private String francCreDate;
   private String francCreTime;
   private String francCreId;
   private String francModiDate;
   private String francModiTime;
   private String francModiId;
   private String userJoinDate;
   private String userDisDate;
   private String userEmail;
   private String userPassword;
   private Integer userType;
   private String userYblpaId;
   private String userSoleNo;
   private String userSignAdds;
}
