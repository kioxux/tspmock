package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetMemberPreSetImportInData {
   private String brId;
   private String CardNo;
   private String index;
}
