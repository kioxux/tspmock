package com.gys.business.service.data.auth;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @Description: 用户添加数据权限新增入参实体类
 * @author: flynn
 * @date: 2021年12月27日 下午4:44
 */
@Data
public class UserRestrictAddReq implements Serializable {

    private String userId;

    @NotBlank(message = "用户可查看范围必填")
    private String restrictType;

    //当指定片区数据时，可以选中多个片区
    private List<String> districts;

    private List<StoInfo> stos;

}

