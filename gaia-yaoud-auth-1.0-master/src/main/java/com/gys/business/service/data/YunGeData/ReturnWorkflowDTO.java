package com.gys.business.service.data.YunGeData;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * 门店退库审批WfJsonString实体
 */
public class ReturnWorkflowDTO {
	//退库门店
	@Field(
			name = "门店号"
	)
	@JSONField(
			ordinal = 1
	)
	private String proSite;
	//门店名称
	@Field(
			name = "门店名称"
	)
	@JSONField(
			ordinal = 2
	)
	private String wsdMdMc;
	@Field(
			name = "单号"
	)
	@JSONField(
			ordinal = 3
	)
	private String billNo;
	@Field(
			name = "原因"
	)
	@JSONField(
			ordinal = 4
	)
	private String reason;
	/**
	 * 总行数
	 */
	@Field(
			name = "总行数"
	)
	@JSONField(
			ordinal = 5
	)
	private String totalCount;
	/**
	 * 总数量
	 */
	@Field(
			name = "总数量"
	)
	@JSONField(
			ordinal = 6
	)
	private String totalQuantity;
	//是否审批结束

	private String wsdSfsp;
	//是否审批同意

	private String wsdSfty;
}
