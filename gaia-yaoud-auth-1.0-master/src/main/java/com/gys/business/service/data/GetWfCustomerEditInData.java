package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCustomerEditInData {
    @Field(
            name = "供应商编码"
    )
    @JSONField(
            ordinal = 1
    )
    private String cusSelfCode = "";

    @Field(
            name = "供应商名称"
    )
    @JSONField(
            ordinal = 2
    )
    private String cusName = "";

    @Field(
            name = "统一社会信用码"
    )
    @JSONField(
            ordinal = 3
    )
    private String cusCreditCode = "";

    @Field(
            name = "修改字段"
    )
    @JSONField(
            ordinal = 4
    )
    private String cusChangeColumnDes = "";

    @Field(
            name = "修改前"
    )
    @JSONField(
            ordinal = 5
    )
    private String cusChangeFrom = "";

    @Field(
            name = "修改后"
    )
    @JSONField(
            ordinal = 6
    )
    private String cusChangeTo = "";

    @Field(
            name = "修改原因"
    )
    @JSONField(
            ordinal = 7
    )
    private String cusRemarks = "";
}
