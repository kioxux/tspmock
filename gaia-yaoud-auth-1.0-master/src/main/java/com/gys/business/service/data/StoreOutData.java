package com.gys.business.service.data;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

@Data
public class StoreOutData {
   private String client;
   private String stoCode;
   private String stoName;
   private String stoPym;
   private String stoShortName;
   private String stoAttribute;
   private String stoDeliveryMode;
   private String stoRelationStore;
   private String stoStatus;
   private String stoArea;
   private String stoOpenDate;
   private String stoCloseDate;
   private String stoAdd;
   private String stoProvince;
   private String stoCity;
   private String stoDistrict;
   private String stoIfMedicalcare;
   private String stoIfDtp;
   private String stoTaxClass;
   private String stoDeliveryCompany;
   private String stoChainHead;
   private String stoTaxSubject;
   private String stoTaxRate;
   private String stoNo;
   private String stoLegalPerson;
   private String stoLegalPersonName;
   private String stoLeader;
   private String stoLeaderName;
   private String stoQua;
   private String stoQuaName;
   private String stoCreDate;
   private String stoCreTime;
   private String stoCreId;
   private String stoModiDate;
   private String stoModiTime;
   private String stoModiId;
   private String stoLogo;
   private String stoLogoUrl;
   private String stogCode;
   private String stogName;

   private String areaName;

   private String areaId;

   public String getStoAttributeStr() {
      String res = "";
      if (StrUtil.isNotBlank(this.stoAttribute)) {
         if ("1".equals(this.stoAttribute)) {
            res = "单体";
         } else if ("2".equals(this.stoAttribute)) {
            res = "连锁";
         } else if ("3".equals(this.stoAttribute)) {
            res = "加盟";
         } else if ("4".equals(this.stoAttribute)) {
            res = "门诊";
         } else if ("5".equals(this.stoAttribute)) {
            res = "仓库";
         } else if ("6".equals(this.stoAttribute)) {
            res = "部门";
         }
      }
      return res;
   }

   private boolean ifUserChoose;

   private String stoType;
}
