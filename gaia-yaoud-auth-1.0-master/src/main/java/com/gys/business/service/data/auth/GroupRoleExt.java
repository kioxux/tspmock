package com.gys.business.service.data.auth;

import com.gys.business.mapper.entity.GroupRole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年12月22日 下午3:57
 */
@Data
public class GroupRoleExt extends GroupRole implements Serializable {

    //参考角色名称
    private String basicRoleName;

    //人数
    private Integer userNum;

    // 人名
    private String userName;

    // 人员id
    private String userId;

    // 1表示通用角色 2表示自定义角色
    private Integer roleType;

    @ApiModelProperty("模块")
    private String model;

    @ApiModelProperty("模块名称")
    private String modelName;

    @ApiModelProperty("是否选中 true选中")
    private Boolean choose;

    private String pId;

    private List<GroupRoleExt> children;

}

