package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

/**
 * 供应商付款审批
 * @author 陈浩
 */
@Data
public class GetWfSupplierBeforeApprovalInData {
//    @Field(
//            name = "序号"
//    )
//    @JSONField(
//            ordinal = 1
//    )
//    private String index;

    @Field(
            name = "供应商编码"
    )
    @JSONField(
            ordinal = 1
    )
    private String supCode;

    @Field(
            name = "供应商名称"
    )
    @JSONField(
            ordinal = 2
    )
    private String supName;

    @Field(
            name = "申请付款金额"
    )
    @JSONField(
            ordinal = 3
    )
    private String applicationPaymentAmount;

    @Field(
            name = "开户行名称"
    )
    @JSONField(
            ordinal = 4
    )
    private String gspSupBankName;

    @Field(
            name = "开户行帐号"
    )
    @JSONField(
            ordinal = 5
    )
    private String gspSupBankNum;

    @Field(
            name = "申请人帐号"
    )
    @JSONField(
            ordinal = 6
    )
    private String founder;

    @Field(
            name = "申请人"
    )
    @JSONField(
            ordinal = 7
    )
    private String userNam;

    @Field(
            name = "申请日期"
    )
    @JSONField(
            ordinal = 7
    )
    private String gspApplyDate;

    @Field(
            name = "备注"
    )
    @JSONField(
            ordinal = 8
    )
    private String approvalRemark;


}
