package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class GetWorkflowInData {
    private String wfTitle;
    private String userId;
    private String client;
    private String wfCode;
    private List<String> wfCodeList;
    private String cc;
    private String result;
    private String memo;
    private BigDecimal rejectSeq;
    private Integer pageNum;
    private Integer pageSize;
    private String depId;
    private String defineCode;
    private String orderNo;
    @ApiModelProperty(value = "审核日期")
    private String approveDate;
    private int autoApprove = 0;
    private String startDate;
    private String endDate;
    private List<String> stoDepList;
    private List<String> stateList;
}
