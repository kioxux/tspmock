package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.Data;

@Data
public class GetWfCreateLossDefecproInData {
    @Field(
            name = "行号"
    )
    @JSONField(
            ordinal = 1
    )
    private String index = "";

    @Field(
            name = "商品编码"
    )
    @JSONField(
            ordinal = 2
    )
    private String proCode = "";

    @Field(
            name = "通用名"
    )
    @JSONField(
            ordinal = 3
    )
    private String proCommonName = "";

    @Field(
            name = "规格"
    )
    @JSONField(
            ordinal = 4
    )
    private String proSpecs = "";

    @Field(
            name = "厂家"
    )
    @JSONField(
            ordinal = 5
    )
    private String factoryName = "";

    @Field(
            name = "批号"
    )
    @JSONField(
            ordinal = 6
    )
    private String batchNo = "";

    @Field(
            name = "效期"
    )
    @JSONField(
            ordinal = 7
    )
    private String validDate = "";

    @Field(
            name = "效期"
    )
    @JSONField(
            ordinal = 8
    )
    private String batch = "";

    @Field(
            name = "转入数量"
    )
    @JSONField(
            ordinal = 9
    )
    private String qty = "";
}
