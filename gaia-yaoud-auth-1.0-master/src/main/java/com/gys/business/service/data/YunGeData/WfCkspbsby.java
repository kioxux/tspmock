package com.gys.business.service.data.YunGeData;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.common.annotation.Field;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * 出库商品报损报溢
 */
public class WfCkspbsby {

	@Field(
			name = "报溢单号"
	)
	@JSONField(
			ordinal = 1
	)
	private String billNo;
	@Field(
			name = "备注的申请原因"
	)
	@JSONField(
			ordinal = 2
	)
	private String reason;

	/**
	 * 报损总行数
	 */
	@Field(
			name = "报损总行数"
	)
	@JSONField(
			ordinal = 3
	)
	private String lossTotalCount;
	/**
	 * 报损总数量
	 */
	@Field(
			name = "报损总数量"
	)
	@JSONField(
			ordinal = 4
	)
	private String lossTotalQuantity;
	/**
	 * 报损批次成本额合计
	 */
	@Field(
			name = "报损批次成本额合计"
	)
	@JSONField(
			ordinal = 5
	)
	private String lossTotalCostAmount;

	/**
	 * 报溢总行数
	 */
	@Field(
			name = "报溢总行数"
	)
	@JSONField(
			ordinal = 6
	)
	private String overflowTotalCount;
	/**
	 * 报溢总数量
	 */
	@Field(
			name = "报溢总数量"
	)
	@JSONField(
			ordinal = 7
	)
	private String overflowTotalQuantity;
	/**
	 * 报溢批次成本额合计
	 */
	@Field(
			name = "报溢批次成本额合计"
	)
	@JSONField(
			ordinal = 8
	)
	private String overflowTotalCostAmount;
	/**
	 * 批次成本额合计
	 * 报溢合计-报损合计
	 */
	@Field(
			name = "总合计成本"
	)
	@JSONField(
			ordinal = 9
	)
	private String totalCostAmount;
	/**
	 * 是否审批结束
	 */
	private String wmsSfsp;
	/**
	 * 是否审批同意
	 */
	private String wmsSfty;
}
