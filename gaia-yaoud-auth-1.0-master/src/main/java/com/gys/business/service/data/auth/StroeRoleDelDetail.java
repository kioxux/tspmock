package com.gys.business.service.data.auth;

import lombok.Data;

import java.io.Serializable;

@Data
public class StroeRoleDelDetail implements Serializable {
    //角色id
    private String roleId;

    // 1表示基础角色 2表示自定义角色
    private Integer roleType;
}