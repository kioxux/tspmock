package com.gys.util;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Util {
   private static final Logger log = LoggerFactory.getLogger(Util.class);

   public static String generateUUID() {
      return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 32);
   }

   public static String getExceptionInfo(Exception ex) {
      String sOut = "";
      sOut = sOut + ex.getMessage() + "\r\n";
      StackTraceElement[] trace = ex.getStackTrace();
      StackTraceElement[] var3 = trace;
      int var4 = trace.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         StackTraceElement s = var3[var5];
         sOut = sOut + "\tat " + s + "\r\n";
      }

      return sOut;
   }

   public static <T> T initFieldName(Class<T> clazz) {
      Field[] sourceFields = clazz.getDeclaredFields();

      try {
         T newClazz = clazz.newInstance();
         Field[] targetFields = newClazz.getClass().getDeclaredFields();
         Map<String, String> sourceMap = new HashMap();
         Field[] var5 = sourceFields;
         int var6 = sourceFields.length;

         int var7;
         Field targetField;
         for(var7 = 0; var7 < var6; ++var7) {
            targetField = var5[var7];
            if (targetField.isAnnotationPresent(com.gys.common.annotation.Field.class)) {
               com.gys.common.annotation.Field field = targetField.getAnnotation(com.gys.common.annotation.Field.class);
               String name = field.name();
               sourceMap.put(targetField.getName(), name);
            }
         }

         var5 = targetFields;
         var6 = targetFields.length;

         for(var7 = 0; var7 < var6; ++var7) {
            targetField = var5[var7];
            targetField.setAccessible(true);
            targetField.set(newClazz, sourceMap.get(targetField.getName()));
         }

         return newClazz;
      } catch (Exception var11) {
         log.error("反射操作异常---", var11);
         return null;
      }
   }

   public static String getUserPassword() {
      StringBuilder builder = new StringBuilder();
      int numberCount = 0;
      int lowerCount = 0;
      int upperCount = 0;

      for(int i = 0; i < 8; ++i) {
         if (i == 6 || i == 7) {
            if (numberCount == 0) {
               builder.append(RandomUtil.randomNumbers(1));
               ++numberCount;
               continue;
            }

            if (lowerCount == 0) {
               builder.append(RandomUtil.randomChar("abcdefghijklmnopqrstuvwxyz"));
               ++lowerCount;
               continue;
            }

            if (upperCount == 0) {
               builder.append(StrUtil.toString(RandomUtil.randomChar("abcdefghijklmnopqrstuvwxyz")).toUpperCase());
               ++upperCount;
               continue;
            }
         }

         int index = Integer.valueOf(RandomUtil.randomNumbers(1));
         if (index >= 7) {
            builder.append(StrUtil.toString(RandomUtil.randomChar("abcdefghijklmnopqrstuvwxyz")).toUpperCase());
            ++upperCount;
         } else if (index >= 3) {
            builder.append(RandomUtil.randomNumbers(1));
            ++numberCount;
         } else {
            builder.append(RandomUtil.randomChar("abcdefghijklmnopqrstuvwxyz"));
            ++lowerCount;
         }
      }

      return builder.toString();
   }

   public static String strAddOne(String str) {
      int length = str.length();
      int value = Integer.valueOf(str);
      ++value;
      return String.format("%04d", value);
   }
}
