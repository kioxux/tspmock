package com.gys.util;

public class UtilMessage {
   public static final String HTTP_CLIENT_ERROR = "接口请求出错！";
   public static final String HTTP_CLIENT_ERROR2 = "接口请求未响应！";
   public static final String GET_MESSAGE_SUCCESS = "提示：获取数据成功！";
   public static final String GET_MESSAGE_ERROR = "提示：获取数据失败！";
   public static final String GET_DATA_ERROR = "提示：暂无数据！";
   public static final String SAVE_MESSAGE_SUCCESS = "提示：保存成功！";
   public static final String SAVE_MESSAGE_ERROR = "提示：保存失败！";
   public static final String SAVE_MESSAGE_UPDATE_SUCCESS = "提示：更新成功！";
   public static final String SAVE_MESSAGE_UPDATE_ERROR = "提示：更新失败！";
   public static final String UPLOAD_MESSAGE_SUCCESS = "提示：上传成功！";
   public static final String UPLOAD_MESSAGE_ERROR = "提示：上传失败！";
   public static final String SEARCH_MESSAGE_SUCCESS = "提示：查询成功！";
   public static final String SEARCH_MESSAGE_ERROR = "提示：查询失败！";
   public static final String DEL_MESSAGE_SUCCESS = "提示：删除成功！";
   public static final String DEL_MESSAGE_ERROR = "提示：删除失败！";
   public static final String SEND_MESSAGE_SUCCESS = "提示：消息发送成功！";
   public static final String SEND_MESSAGE_ERROR = "提示：消息发送失败！";
   public static final String SEND_SMS_MESSAGE_SUCCESS = "提示：验证码发送成功！";
   public static final String SEND_SMS_MESSAGE_ERROR = "提示：验证码发送失败！";
   public static final String ERROR_MESSAGE = "提示：服务器出现了一些小状况，正在修复中！";
   public static final String LOGIN_SUCCESS = "提示：登录成功！";
   public static final String LOGIN_ERROR = "提示：登录失败！";
   public static final String LOGIN_OUT_SUCCESS = "提示：登出成功！";
   public static final String UPDATE_PASSWORD_SUCCESS = "提示：密码修改成功，请重新登录！";
   public static final String REGISTER_SUCCESS = "提示：注册成功！";
   public static final String USER_INFO_SUCCESS = "提示：资料更新成功！";
   public static final String TOKEN_EMPTY = "提示：token不能为空！";
   public static final String LOGIN_NO_ACCOUNT = "提示：账号不存在";
   public static final String LOGIN_OLD_PASSWORD_ERROR = "提示：原密码不正确";
   public static final String LOGIN_EXIST_ACCOUNT = "提示：账号已存在";
   public static final String LOGIN_ACCOUNT_INVALID = "提示：账号已停用";
   public static final String LOGIN_ACCOUNT_LEAVE = "提示：员工已离职";
   public static final String LOGIN_ACCOUNT_INVALID_OR_LEAVE = "提示：员工已停用或离职";
   public static final String LOGIN_PASSWORD_ERROR = "提示：密码不正确";
   public static final String LOGIN_MSG_ERROR = "提示：验证码不正确";
   public static final String UPDATE_PWD_LENGTH = "提示：请保持密码长度在6-20位";
   public static final String UPDATE_PWD_SAME_PASSWORD = "提示：新密码和旧密码不能相同";
   public static final String UPDATE_PWD_OLD_PASSWORD_ERROR = "提示：旧密码错误";
   public static final String LOGIN_CODE_ERROR = "提示：短信验证码错误";
   public static final String LOGIN_CODE_EXPIRE = "提示：短信验证码已过期";
   public static final String LOGIN_STORE_ERROR = "提示：当前员工不属于任一门店";
   public static final String FRANCHISEE_EMPTY = "提示：加盟商不存在";
   public static final String COS_PARA_EMPTY = "提示：未配置对象存储参数";
   public static final String SMS_PARA_EMPTY = "提示：未配置短信参数";
   public static final String WF_PARA_EMPTY = "提示：发起工作流参数不正确";
   public static final String WF_TYPE_EMPTY = "提示：工作流类型不能为空";
   public static final String WF_TYPE_NOT_EXITS = "提示：工作流类型不存在";
   public static final String WF_TITLE_EMPTY = "提示：工作流标题不能为空";
   public static final String WF_DESC_EMPTY = "提示：工作流描述不能为空";
   public static final String WF_ORDER_EMPTY = "提示：工作流单据编号不能为空";
   public static final String WF_SITE_EMPTY = "提示：工作流SITE不能为空";
   public static final String WF_ERROR = "提示：发起工作流异常";
   public static final String WF_APPROVING = "提示：当前单据对应的工作流正在审批中，不能重复发起工作流！";
   public static final String WF_APPROE_PASS = "提示：当前单据对应的工作流已审批通过，不能重复发起工作流！";
   public static final String WF_APPROE_USER_EMTPY = "提示：节点未分配审批人！";
   public static final String WF_RECORD_EMPTY = "提示：工作流不存在";
   public static final String WF_RECORD_APPROVE_STATUS_ERROR = "提示：当前工作流审批状态不是审批中，不可审批";
   public static final String WF_RECORD_APPROVE_RESULT_EMPTY = "提示：审批结果不正确或不能为空";
   public static final String WF_RECORD_CANCEL_ERROR = "提示：只能取消待提交状态的工作流";
   public static final String WF_RECORD_CANCEL_USER_ERROR = "提示：只能取消自己提交的工作流";
   public static final String CARD_EXIST = "提示：卡号{0}，已存在";
   public static final String CARD_NOT_EXIST = "提示：卡号{0}，不存在";
   public static final String DEPT_NAME_EXIST = "提示：该部门已存在";
   public static final String USER_TEL_EXIST = "提示：手机号已存在";
   public static final String USER_IDC_EXIST = "提示：身份证号已存在";
   public static final String USER_EMAIL_EXIST = "提示：邮箱已存在";
   public static final String STORE_EMPTY = "提示：门店不存在";
   public static final String USER_EMPTY = "提示：用户不存在";
   public static final String USER_DEP_EMPTY = "提示：用户({})没有所属门店或门";
}
