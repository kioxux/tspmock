package com.gys.util;


import com.gys.common.exception.BusinessException;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.jcajce.provider.asymmetric.rsa.RSAUtil;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.security.*;
import java.util.*;

public class UserUtils {


    private static final String TOKEN_HEADER = "X-Token";
    // APP
    public static final String PLATFORM_1 = "1";
    // 微信
    public static final String PLATFORM_2 = "2";




    /**
     * 获取Request
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return attrs.getRequest();
    }


    /**
     * 获取请求tokenCode
     */
    public static String getTokenCode() {
        String toekn = getRequest().getHeader(TOKEN_HEADER);
        if (StringUtils.isBlank(toekn)) {
            throw new BusinessException("无效token");
        }
        return toekn;
    }




}
