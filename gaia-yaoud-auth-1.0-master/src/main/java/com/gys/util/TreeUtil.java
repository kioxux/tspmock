package com.gys.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.gys.common.data.TreeNode;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wu mao yin
 * @Description: 树结构工具类
 * @date 2021/12/23 9:03
 */
public class TreeUtil {

    /**
     * 根节点对象存放到这里
     */
    private List<? extends TreeNode> rootList;

    /**
     * 其他节点存放到这里，可以包含根节点
     */
    private List<? extends TreeNode> bodyList;

    public TreeUtil(List<? extends TreeNode> rootList, List<? extends TreeNode> bodyList) {
        this.rootList = rootList;
        this.bodyList = bodyList;
    }

    public List<TreeNode> getTree() {
        if (bodyList != null && !bodyList.isEmpty()) {
            //声明一个map，用来过滤已操作过的数据
            Map<String, String> map = Maps.newHashMapWithExpectedSize(bodyList.size());
            rootList.forEach(beanTree -> getChild(beanTree, map));
            return (List<TreeNode>) rootList;
        }
        return null;
    }

    public void getChild(TreeNode treeNode, Map<String, String> map) {
        List<TreeNode> childList = Lists.newArrayList();
        AtomicInteger chooseIds = new AtomicInteger(0);
        bodyList.stream()
                .filter(c -> !map.containsKey(c.getId()))
                .filter(c -> c.getPId().equals(treeNode.getId()))
                .forEach(c -> {
                    map.put(c.getId(), c.getPId());
                    getChild(c, map);
                    if (c.getChoose()) {
                        chooseIds.incrementAndGet();
                    }
                    childList.add(c);
                });

        if (treeNode.getChoose() == null || !treeNode.getChoose()) {
            treeNode.setChoose(childList.size() > 0 && childList.size() == chooseIds.get());
        }
        treeNode.setChild(childList);
    }

}