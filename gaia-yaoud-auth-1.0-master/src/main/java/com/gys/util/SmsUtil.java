package com.gys.util;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SmsUtil {
   private static final Logger log = LoggerFactory.getLogger(SmsUtil.class);
   private static SmsUtil smsUtil = null;
   private static String SPCODE;
   private static String LOGIN_NAME;
   private static String PASSWORD;
   private static String TEMPLATE_ID;
   private static String TEMPLATE_CONTENT;

   public static SmsUtil getInstance(String spCode, String loginName, String password, String templateId, String templateContent) {
      if (smsUtil == null) {
         smsUtil = new SmsUtil();
      }

      SPCODE = spCode;
      LOGIN_NAME = loginName;
      PASSWORD = password;
      TEMPLATE_ID = templateId;
      TEMPLATE_CONTENT = templateContent;
      return smsUtil;
   }

   public void sendCaptcha(String phone, String code, String seq) {
      this.send(phone, "【药德】", StrUtil.format("：{}，", new Object[]{code}), StrUtil.format("：{}，", new Object[]{seq}), "！");
   }

   public void sendFranchisee(String phone, String name, String password) {
      this.send(phone, "【药德】", StrUtil.format("：{}，", new Object[]{phone}), StrUtil.format("{}，", new Object[]{password}));
   }

   public void sendUser(String phone, String password) {
      this.send(phone, "【药德】", StrUtil.format("：{}", new Object[]{phone}), StrUtil.format("{}", new Object[]{password}));
   }

   public void send(String phone, String... templateParam) {
      try {
         HttpClient httpclient = HttpClients.createDefault();
         HttpPost post = new HttpPost("https://api.ums86.com:9600/sms/Api/Send.do");
         post.addHeader("Content-type", "application/x-www-form-urlencoded; charset=gbk");
         List<NameValuePair> nameValuePairs = new ArrayList();
         nameValuePairs.add(new BasicNameValuePair("SpCode", SPCODE));
         nameValuePairs.add(new BasicNameValuePair("LoginName", LOGIN_NAME));
         nameValuePairs.add(new BasicNameValuePair("Password", PASSWORD));
         nameValuePairs.add(new BasicNameValuePair("MessageContent", StrUtil.format(TEMPLATE_CONTENT, templateParam)));
         nameValuePairs.add(new BasicNameValuePair("UserNumber", phone));
         nameValuePairs.add(new BasicNameValuePair("templateId", TEMPLATE_ID));
         nameValuePairs.add(new BasicNameValuePair("SerialNumber", RandomUtil.randomNumbers(2)));
         nameValuePairs.add(new BasicNameValuePair("ScheduleTime", ""));
         nameValuePairs.add(new BasicNameValuePair("f", "1"));
         log.info("短信参数：" + JSON.toJSONString(nameValuePairs));
         post.setEntity(new UrlEncodedFormEntity(nameValuePairs, "gbk"));
         HttpResponse response = httpclient.execute(post);
         int statusCode = response.getStatusLine().getStatusCode();
         if (statusCode == 200) {
            String conResult = EntityUtils.toString(response.getEntity(), "gbk");
            log.info(conResult);
         } else {
            log.error("错误码---" + statusCode);
         }
      } catch (Exception var9) {
         log.error("", var9);
         var9.printStackTrace();
      }

   }
}
