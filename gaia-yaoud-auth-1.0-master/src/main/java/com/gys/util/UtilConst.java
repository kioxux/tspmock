package com.gys.util;

import java.util.Arrays;
import java.util.List;

public class UtilConst {
   public static final String DEFULT_PASSWORD = "Gys123456";
   public static final String PASSWORD = "e10adc3949ba59abbe56e057f20f883e";
   public static final Integer CODE_0 = 0;
   public static final Integer CODE_401 = 401;
   public static final Integer CODE_500 = 500;
   public static final Integer CODE_1001 = 1001;
   public static final Integer CODE_1019 = 1019;
   public static final String LOGIN_TOKEN = "X-Token";
   public static final String STR_NUMBER_0 = "0";
   public static final String STR_NUMBER_1 = "1";
   public static final String STR_NUMBER_2 = "2";
   public static final String STR_NUMBER_3 = "3";
   public static final String STR_NUMBER_4 = "4";
   public static final String STR_NUMBER_5 = "5";
   public static final String STR_NUMBER_6 = "6";
   public static final String STR_NUMBER_10 = "10";
   public static final String STR_NUMBER_20 = "20";
   public static final String STR_NUMBER_30 = "30";
   public static final String STR_NUMBER_40 = "40";
   public static final Long TOKEN_EXPIRE = 86400L;
   public static final Long PHONE_CODE_EXPIRE = 300L;
   public static final String GAD = "GAD";
   public static final List<String> GAD_GROUP_LIST = Arrays.asList("GAIA_AL_ADMIN", "GAIA_AL_MANAGER");
   public static final String STORE_UPDATE_PATH = "/editStore{}";
   public static final String STORE_UPDATE_PAGE_PATH = "auth/store/edit.vue";
   public static final String COMPADM_UPDATE_PATH = "/editCompadm{}";
   public static final String COMPADM_LIST_PATH = "/compadmList";
   public static final String COMPADM_UPDATE_PAGE_PATH = "auth/compadm/edit.vue";
   public static final String COMPADM_LIST_PAGE_PATH = "auth/compadm/list.vue";
   public static final String FRANCHISEE_UPDATE_PAGE_PATH = "auth/franchisee/edit.vue";
   public static final String FRANCHISEE_UPDATE_PATH = "/editFranchisee{}";
   public static final String DC_UPDATE_PATH = "/editDc{}";
   public static final String DC_UPDATE_PAGE_PATH = "purchase/DClist/maintainDc.vue";
   public static final String DEP_UPDATE_PATH = "/deptStore{}";
   public static final String DEP_UPDATE_PAGE_PATH = "auth/deptStore/edit.vue";
   public static final String DEP_LIST_PAGE_PATH = "auth/store/index.vue";
   public static final String DEP_LIST_PATH = "/store";
   public static final String WHOLESALE_LIST_PATH = "/wholesaleList";
   public static final String WHOLESALE_LIST_PAGE_PATH = "wholesale/list";
   public static final String OPERATION_LOGIN_PHONE_KEY = "operation_";
   public static final String SELECT_ALL = "all";
   public static final String PARA_COS_SECRET_ID = "TENCENT_COS_SECRET_ID";
   public static final String PARA_COS_SECRET_KEY = "TENCENT_COS_SECRET_KEY";
   public static final String PARA_COS_REGION_NAME = "TENCENT_COS_REGION_NAME";
   public static final String PARA_COS_BUCKET_NAME = "TENCENT_COS_BUCKET_NAME";
   public static final String PARA_SMS_SPCODE = "SMS_SPCODE";
   public static final String PARA_SMS_LOGIN_NAME = "SMS_LOGIN_NAME";
   public static final String PARA_SMS_PASSWORD = "SMS_PASSWORD";
   public static final String PARA_SMS_CONTENT_CODE = "SMS_CONTENT_CODE";
   public static final String PARA_SMS_TEMPLATE_CODE = "SMS_TEMPLATE_CODE";
   public static final String PARA_SMS_CONTENT_FRANCHISEE = "SMS_CONTENT_FRANCHISEE";
   public static final String PARA_SMS_TEMPLATE_FRANCHISEE = "SMS_TEMPLATE_FRANCHISEE";
   public static final String SMS_SIGN = "【公约树】";
   public static final String ABB_PURE_DATE_PATTERN = "yyMMdd";
   public static final String WF_CODE_AUTH_NOTICE = "GAIA_WF_001";
   public static final String WF_CODE_DELEGATE_NOTICE = "GAIA_WF_002";
   public static final String WF_CODE_SAVE_ORG_NOTICE = "GAIA_WF_003";
   public static final String WF_CODE_CATEGORY_ANALYSIS = "GAIA_WF_004";
   public static final String WF_CODE_RETURN_COMPADM = "GAIA_WF_005";
   public static final String WF_CODE_LOSS_COMPADM = "GAIA_WF_006";
   public static final String WF_CODE_LOSS_SINGLE = "GAIA_WF_007";
   public static final String WF_CODE_DPGJ_COMPADM = "GAIA_WF_008";
   public static final String WF_CODE_DPGJ_SINGLE = "GAIA_WF_009";
   public static final String WF_CODE_DPZL_COMPADM = "GAIA_WF_010";
   public static final String WF_CODE_DPZL_SINGLE = "GAIA_WF_011";
   public static final String WF_CODE_ZDZL_COMPADM = "GAIA_WF_012";
   public static final String WF_CODE_ZDZL_SINGLE = "GAIA_WF_013";
   public static final String WF_CODE_PRODUCT_GSP = "GAIA_WF_014";
   public static final String WF_CODE_SUPPLIER_GSP = "GAIA_WF_015";
   public static final String WF_CODE_CUSTOMER_GSP = "GAIA_WF_016";
   public static final String WF_CODE_PURCHASE_ORDER = "GAIA_WF_017";
   public static final String WF_CODE_PURCHASE_PAY = "GAIA_WF_018";
   public static final String WF_CODE_SPBS = "GAIA_WF_019";
   public static final String WF_CODE_ZZY = "GAIA_WF_020";
   public static final String WF_CODE_SPYH = "GAIA_WF_021";
   public static final String WF_CODE_CKBSBY = "GAIA_WF_022";
   public static final String WF_CODE_PHTZ = "GAIA_WF_023";
   public static final String WF_CODE_RKSH = "GAIA_WF_024";
   public static final String WF_CODE_RKYS = "GAIA_WF_025";
   public static final String WF_CODE_KCTZ = "GAIA_WF_026";
   public static final String APPROVE_PASS = "PASS";
   public static final String APPROVE_FAIL = "FAIL";
   public static final String ON_LINE = "线上";
   public static final String DOWN_LINE = "线下";
   /*
     会员卡折扣2   0/NULL关闭，1开始，缺省为NULL；
    */
   public static final String MEMCARD_DISCOUNT2 = "MEMCARD_DISCOUNT2";
   /*
     RECEIVE_PRICE_SHOW  门店收货禁止显示收货单价格 【0-否（缺省）；1-是（不显示）】
    */

   public static final String RECEIVE_PRICE_SHOW = "RECEIVE_PRICE_SHOW";
}
