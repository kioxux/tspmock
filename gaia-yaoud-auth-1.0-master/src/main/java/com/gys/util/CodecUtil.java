package com.gys.util;

import cn.hutool.core.util.StrUtil;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CodecUtil {
   private static final Logger log = LoggerFactory.getLogger(CodecUtil.class);

   public static String encodeURL(String str) {
      if (StrUtil.isBlank(str)) {
         return "";
      } else {
         String target = "";

         try {
            target = URLEncoder.encode(str, "UTF-8");
         } catch (Exception var3) {
            log.error("编码出错！", var3);
         }

         return target;
      }
   }

   public static String decodeURL(String str) {
      if (StrUtil.isBlank(str)) {
         return "";
      } else {
         String target = "";

         try {
            target = URLDecoder.decode(str, "UTF-8");
         } catch (Exception var3) {
            log.error("解码出错！", var3);
         }

         return target;
      }
   }

   public static String encryptMD5(String str) {
      return DigestUtils.md5Hex(str);
   }

   public static String emojiConvert1(String str) {
      if (str == null) {
         return "";
      } else {
         String patternString = "([\\x{10000}-\\x{10ffff}\ud800-\udfff])";
         Pattern pattern = Pattern.compile(patternString);
         Matcher matcher = pattern.matcher(str);
         StringBuffer sb = new StringBuffer();

         while(matcher.find()) {
            try {
               matcher.appendReplacement(sb, "[[" + URLEncoder.encode(matcher.group(1), "UTF-8") + "]]");
            } catch (UnsupportedEncodingException var6) {
               return "";
            }
         }

         matcher.appendTail(sb);
         return sb.toString();
      }
   }

   public static String emojiRecovery2(String str) {
      if (str == null) {
         str = "";
      }

      String patternString = "\\[\\[(.*?)\\]\\]";
      Pattern pattern = Pattern.compile(patternString);
      Matcher matcher = pattern.matcher(str);
      StringBuffer sb = new StringBuffer();

      while(matcher.find()) {
         try {
            matcher.appendReplacement(sb, URLDecoder.decode(matcher.group(1), "UTF-8"));
         } catch (UnsupportedEncodingException var6) {
            return "";
         }
      }

      matcher.appendTail(sb);
      return sb.toString();
   }

   public static String emojiFilter(String str) throws UnsupportedEncodingException {
      if (str == null) {
         str = "";
      }

      String patternString = "\\[\\[(.*?)\\]\\]";
      Pattern pattern = Pattern.compile(patternString);
      Matcher matcher = pattern.matcher(str);
      StringBuffer sb = new StringBuffer();

      while(matcher.find()) {
         matcher.appendReplacement(sb, "");
      }

      matcher.appendTail(sb);
      return sb.toString();
   }

   public static boolean hasEmoji(String text) {
      String speChat = "[\\ud83c\\udc00-\\ud83c\\udfff]|[\\ud83d\\udc00-\\ud83d\\udfff]|[\\u2600-\\u27ff]";
      Pattern pattern = Pattern.compile(speChat);
      Matcher matcher = pattern.matcher(text);
      return matcher.find();
   }
}
