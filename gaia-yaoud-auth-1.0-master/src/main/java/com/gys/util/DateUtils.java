package com.gys.util;


import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Slf4j
public class DateUtils {

    /**
     * 默认日期格式
     */
    public static String DEFAULT_FORMAT = "yyyyMMdd";

    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


//    /**
//     * 测试主方法
//     * @param args
//     */
//    public static void main(String[] args) {
//        System.out.println(getYearFirst(2021));
//        System.out.println(getYearLast(2023));
//        System.out.println(getYearMonthFirst("202102"));
//
//        System.out.println(getYearMonthLast("202106"));
//
////        System.out.println(formatDate(getCurrYearFirst()));
////        System.out.println(formatDate(getCurrYearLast()));

//    }


    /**
     * 格式化日期
     *
     * @param date 日期对象
     * @return String 日期字符串
     */
    public static String formatDate(Date date) {
        SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
        String sDate = f.format(date);
        return sDate;
    }

    /**
     * 获取当年的第一天
     * @return
     */
//    public static Date getCurrYearFirst(){
//        Calendar currCal=Calendar.getInstance();
//        int currentYear = currCal.get(Calendar.YEAR);
//        return getYearFirst(currentYear);
//    }

    /**
     * 获取当年的最后一天
     *
     * @return
     */
//    public static Date getCurrYearLast(){
//        Calendar currCal=Calendar.getInstance();
//        int currentYear = currCal.get(Calendar.YEAR);
//        return getYearLast(currentYear);
//    }
    public static String addDay(Date date, String format, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String format1 = sdf.format(calendar.getTime());
        return format1;
    }

    /**
     * 获取某年第一天日期
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearFirst(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        Date currYearFirst = calendar.getTime();
        SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
        return f.format(currYearFirst);
    }

    /**
     * 获取某年最后一天日期
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearLast(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();
        SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
        return f.format(currYearLast);
    }

    /**
     * 获取某年第一天日期
     *
     * @param yearMonth 年月
     * @return Date
     */
    public static String getYearMonthFirst(String yearMonth) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        try {

            Date date = null;
            date = sdf.parse(yearMonth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);// 设为本月第一天
            return sdf2.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new BusinessException("起始日期异常！");

    }

    /**
     * 获取某年某月最后一天日期
     *
     * @param yearMonth 年月
     * @return Date
     */
    public static String getYearMonthLast(String yearMonth) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        try {

            Date date = null;
            date = sdf.parse(yearMonth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);// 设为本月第一天
            calendar.add(Calendar.MONTH, 1);// 月份加一
            calendar.add(Calendar.DATE, -1);// 天数加 -1 = 上一个月的最后一天
            return sdf2.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new BusinessException("结束日期异常！");
    }

    /**
     * 当前日期
     *
     * @return
     */
    public static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(new Date());
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        return sdf.format(new Date());
    }

    /**
     * 格式化时间   字符串转为Date类型
     *
     * @param dateStr
     * @param pattern
     * @return
     * @throws Exception
     */
    public static Date stringToDate(String dateStr, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new BusinessException("时间格式转换异常");
        }
        return date;
    }


    public static Date getMiddleTime(Date start, Date end) {
        Long startL = start.getTime();
        Long endL = end.getTime();
        long l = new Double(startL + (endL - startL) * Math.random()).longValue();
        return new Date(l);
    }


    /**
     * 格式化时间   Date类型转为字符串
     *
     * @param date
     * @param pattern
     * @return
     * @throws Exception
     */
    public static String dateToString(Date date, String pattern) {
        String dateStr = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            dateStr = sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("时间格式转换异常");
        }
        return dateStr;
    }


    public static String getTimeRange(String begin, String end) {
        Date startDate = stringToDate(begin, "HH:mm:ss");
        Date endDate = stringToDate(end, "HH:mm:ss");
        Date middleTime = getMiddleTime(startDate, endDate);

        String rangeTime = dateToString(middleTime, "HHmmss");
        return rangeTime;
    }

    public static String formatDateStr(String strDate, String pattern) {
        String format = "";
        try {
            Date date = (Date) new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);//设置日期格式
            format = dateFormat.format(date);
        } catch (ParseException e) {
            log.error("日期格式转换异常：", e);
        }
        return format;
    }

    /**
     * 当前时间字符串（yyyy-MM-dd）
     *
     * @return
     */
    public static String getCurrentDateStr() {
        return LocalDate.now().format(DATE_FORMATTER);
    }

    /**
     * 当前时间字符串（HH:mm:ss）
     *
     * @return
     */
    public static String getCurrentTimeStr() {
        return LocalTime.now().format(TIME_FORMATTER);
    }

    /**
     * 自定义格式获取当前时间(yyyy-MM-dd)
     *
     * @param pattern 格式
     * @return
     */
    public static String getCurrentDateStr(String pattern) {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param pattern
     * @return
     */
    public static String getCurrentDateTimeStr(String pattern) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param pattern
     * @return
     */
    public static String getCurrentTimeStr(String pattern) {
        return LocalTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取上月
     * 描述:<描述函数实现的功能>.
     * @param date
     * @return
     */
    public static String getLastMonth(String date) {
        String lastMonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("yyyyMM");
        int year = Integer.parseInt(date.substring(0, 4));
        String monthsString = date.substring(4, 6);
        int month;
        if ("0".equals(monthsString.substring(0, 1))) {
            month = Integer.parseInt(monthsString.substring(1, 2));
        } else {
            month = Integer.parseInt(monthsString.substring(0, 2));
        }
        cal.set(year,month-2,Calendar.DATE);
        lastMonth = dft.format(cal.getTime());
        return lastMonth;
    }

    /**
     * 获取下月
     * 描述:<描述函数实现的功能>.
     * @param date
     * @return
     */
    public static String getNextMonth(String date) {
        String lastMonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("yyyyMM");
        int year = Integer.parseInt(date.substring(0, 4));
        String monthsString = date.substring(4, 6);
        int month;
        if ("0".equals(monthsString.substring(0, 1))) {
            month = Integer.parseInt(monthsString.substring(1, 2));
        } else {
            month = Integer.parseInt(monthsString.substring(0, 2));
        }
        cal.set(year, month - 1, Calendar.DATE);
        cal.add(cal.MONTH, 1);
        lastMonth = dft.format(cal.getTime());
        return lastMonth;
    }

    public static void main(String[] args) {
        Date now = new Date();
        Date thisWeekMonday = getThisWeekMonday(now);
        System.out.println("thisWeekMonday = " + thisWeekMonday);
        Date thisMonthFirstDay = getThisMonthFirstDay(now);
        System.out.println("thisMonthFirstDay = " + thisMonthFirstDay);
        Date quarterlyFirstDay = getQuarterlyFirstDay(now);
        System.out.println("quarterlyFirstDay = " + quarterlyFirstDay);
        Date singleMonthFirstDay = getSingleMonthFirstDay(now);
        System.out.println("singleMonthFirstDay = " + singleMonthFirstDay);

        Date date = moveTime(now, 5);
        System.out.println("date = " + date);
    }

    /**
     * 获取季度的第一天
     *
     * @param now
     * @return
     */
    public static Date getQuarterlyFirstDay(Date now) {
        int month = now.getMonth();
        if (month == 0 || month == 1 || month == 2) {
            now.setMonth(0);
            now.setDate(1);
            return now;
        } else if (month == 3 || month == 4 || month == 5) {
            now.setMonth(3);
            now.setDate(1);
            return now;
        } else if (month == 6 || month == 7 || month == 8) {
            now.setMonth(6);
            now.setDate(1);
            return now;
        } else {
            now.setMonth(9);
            now.setDate(1);
            return now;
        }
    }

    /**
     * 获取单月第一天
     * @param now
     * @return
     */
    public static Date getSingleMonthFirstDay(Date now) {
        int month = now.getMonth();
        if (month == 0 || month == 1) {
            now.setMonth(0);
            now.setDate(1);
            return now;
        } else if (month == 3 || month == 2) {
            now.setMonth(3);
            now.setDate(1);
            return now;
        } else if (month == 4 || month == 5) {
            now.setMonth(4);
            now.setDate(1);
            return now;
        } else if (month == 6 || month == 7) {
            now.setMonth(6);
            now.setDate(1);
            return now;
        } else if (month == 8 || month == 9) {
            now.setMonth(8);
            now.setDate(1);
            return now;
        } else {
            now.setMonth(10);
            now.setDate(1);
            return now;
        }
    }

    /**
     * 本月第一天
     *
     * @param date
     * @return
     */
    public static Date getThisMonthFirstDay(Date date) {
        Calendar cal_1 = Calendar.getInstance();//获取当前日期

        cal_1.add(Calendar.MONTH, 0);

        cal_1.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天

        Date firstDay = cal_1.getTime();

        return firstDay;
    }

    /**
     * 本周一
     *
     * @param date
     * @return
     */
    public static Date getThisWeekMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 获得当前日期是一个星期的第几天
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        return cal.getTime();
    }

    public static Date moveTime(Date date, int day) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, day);//把日期往后增加一天.整数往后推,负数往前移动
        return calendar.getTime();  //这个时间就是日期往后推一天的结果
    }

}
