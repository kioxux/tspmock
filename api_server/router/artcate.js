const express = require('express')

// 创建路由对象
const router = express.Router()
const artcateHandle = require('../router_handler/artcate')

// 导入验证数据合法性的中间件
const expressJoi = require('@escook/express-joi')
// 导入需要验证规则对象
const { add_cate_schema, delete_cate_schema, get_cate_schema, update_cate_schema } = require('../schema/artcate')
const { route } = require('./user')

// 获取文章
router.get('/cates', artcateHandle.getArticleCates)

// 新增文章分类
router.post('/addcates', expressJoi(add_cate_schema), artcateHandle.addArticleCates)

// 删除文章分类
router.get('/deletecate/:id', expressJoi(delete_cate_schema), artcateHandle.deleteCateById)

// 根据id获取文章分类数据
router.get('/cates/:id', expressJoi(get_cate_schema), artcateHandle.getArticleById)

// 更新文章路由
router.post('/updatecate', expressJoi(update_cate_schema), artcateHandle.updateCateById)

module.exports = router