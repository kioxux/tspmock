const express = require('express')
// 创建路由对象
const router = express.Router()

const userinfoHandler = require('../router_handler/userinfo')
// 导入验证数据合法性的中间件
const expressJoi = require('@escook/express-joi')
// 导入需要验证规则对象
const { update_userinfo_schema, update_password_schema, update_avatar_schema } = require('../schema/user')



// 获取用户的基本信息
router.get('/userinfo', userinfoHandler.getUserInfo)
// 更新用户基本信息
router.post('/userinfo', expressJoi(update_userinfo_schema), userinfoHandler.updateUserInfo)

// 更新密码路由
router.post('/updatepwd', expressJoi(update_password_schema), userinfoHandler.updatePassword)

// 更新用户头像路由
router.post('/update/avatar', expressJoi(update_avatar_schema), userinfoHandler.updateAvatar)



module.exports = router