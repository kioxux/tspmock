/**
* 功能描述: 文章验证
* @author : 徐鑫
* 创建时间:2022/07/18 20:13:38
*/

const joi = require('joi')

// 定义 分类名称 和分类别名 的校验
const name = joi.string().required()
const alias = joi.string().alphanum().required()

exports.add_cate_schema = {
	body: {
		name,
		alias,
	}
}


// 定义 分类id的校验规则
const id = joi.number().integer().min(1).required()
exports.delete_cate_schema = {
	params: {
		id
	}
}

// 校验规则对象 - 根据 Id 获取分类
exports.get_cate_schema = {
	params: {
		id,
	},
}

// 校验规则对象 - 更新分类
exports.update_cate_schema = {
	body: {
		id,
		name,
		alias,
	},
}