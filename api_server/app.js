/**
* 功能描述: 入口文件
* @author : 徐鑫
* 创建时间:2022/07/17 18:21:17
*/
const express = require('express')
const app = express()
// 导入配置文件
const config = require('./config')

// 配置cors跨域
const cors = require('cors')
app.use(cors())
// 解析表单数据的中间件
// 配置解析 `application/x-www-form-urlencoded` 格式的表单数据的中间件：
// app.use(express.urlencoded({ extended: false }))
// 配置解析 `application/json` 格式的表单数据的中间件：
app.use(express.json())

// 解析 token 的中间件
const expressJWT = require('express-jwt')
// 使用 .unless({ path: [/^\/api\//] }) 指定哪些接口不需要进行 Token 的身份认证
app.use(expressJWT({ secret: config.jwtSecretKey }).unless({ path: [/^\/api\//] }))

const userRouter = require('./router/user')
app.use('/api', userRouter)

// 导入并使用用户信息的模块
const userinfoRouter = require('./router/userinfo')
app.use('/my', userinfoRouter)

// 导入文章模块
const artcateRouter = require('./router/artcate')
app.use('/my/article', artcateRouter)


const joi = require('joi')


// 错误中间件
app.use(function (err, req, res, next) {
	// 数据验证
	if (err instanceof joi.ValidationError) return res.send(err)
	// 省略其它代码...

	// 捕获身份认证失败的错误
	if (err.name === 'UnauthorizedError') return res.send({
		status: 1,
		message: '身份认证失败！'
	})

	// 未知错误...
})


app.listen(3007, () => {
	console.log('api server running at http://127.0.0.1:3007')
})