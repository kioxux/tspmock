module.exports = {
  apps : [{
    name   : "app",
    script : "./app.js",
    error_file : "./logs/err.log",
    out_file : "./logs/out.log"
  }]
}
