/**
* 功能描述: 用户信息的处理函数
* @author : 徐鑫
* 创建时间:2022/07/18 09:57:00
*/

// 导入数据库操作模块
const db = require('../db/index')

// 获取用户基本信息
exports.getUserInfo = (req, res) => {

	// 根据用户的id，查询用户的基本信息
	// 注意:为了防止用户的密码泄露，需要排除password字段
	const sql = `select id,username,nickname,email,user_pic from ev_users where id=?`
	db.query(sql, req.user.id, (err, results) => {
		if (err) return res.send(err)
		// 执行SQL语句成功，但是查询的条数不等于1
		if (results.length !== 1) return res.send('获取用户信息失败！')

		// 将用户信息响应给客户端
		res.send({
			status: 0,
			message: '获取用户基本信息成功！',
			data: results[0]
		})
	})
}

// !更新用户基本信息
exports.updateUserInfo = (req, res) => {
	const sql = `update ev_users set ? where id=?`
	db.query(sql, [req.body, req.body.id], (err, results) => {
		// 执行SQL语句失败
		if (err) return res.send(err)

		// 执行SQL语句成功，但影响行数不为1
		if (results.affectedRows !== 1) return res.send('修改用户基本信息失败！')

		// 修改用户信息成功
		return res.send('修改用户信息成功！')
	})
}

// ! 重置密码功能
exports.updatePassword = (req, res) => {
	// 根据id查询用户数据
	const sql = `select * from ev_users where id=?`
	// 执行SQL语句查询用户是否存在
	db.query(sql, req.user.id, (err, results) => {
		// 执行SQL语句失败
		if (err) return res.send(err)

		// TODO 检查指定的id用户是否存在
		if (results.length !== 1) return res.send('用户名不存在')
		// 判断提交的旧密码是否正确
		// 在头部区域导入 bcryptjs 后，
		// 即可使用 bcrypt.compareSync(提交的密码，数据库中的密码) 方法验证密码是否正确
		// compareSync() 函数的返回值为布尔值，true 表示密码正确，false 表示密码错误
		const bcrypt = require('bcryptjs')
		// 判断提交的旧密码是否正确
		const compareResult = bcrypt.compareSync(req.body.oldPwd, results[0].password)
		if (!compareResult) return res.send('原密码错误！')

		// TODO 对新密码进行加密添加到数据库中
		const sql = `update ev_users set password=? where id=?`
		// 对新密码进行加密
		const newPwd = bcrypt.hashSync(req.body.newPwd, 10)
		db.query(sql, [newPwd, req.user.id], (err, results) => {
			if (err) return res.send(err)
			if (results.affectedRows !== 1) return res.send('更新密码失败！')
			res.send('更新密码成功！')
		})
	})
}

exports.updateAvatar = (req, res) => {
	const sql = `update ev_users set user_pic=? where id =?`
	db.query(sql, [req.body.avatar, req.user.id], (err, results) => {
		if (err) return res.send(err)

		if (results.affectedRows !== 1) return res.send('更新头像失败！')

		return res.send('更新头像成功！')
	})
}
