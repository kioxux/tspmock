/**
* 功能描述: 文章功能
* @author : 徐鑫
* 创建时间:2022/07/18 20:01:00
*/
const db = require('../db/index')
exports.getArticleCates = (req, res) => {
	// 根据分类状态，获取所有未被删除的分类列表，并通过id进行升序排列
	const sql = `select * from ev_article_cate where is_delete=0 order by id asc`
	db.query(sql, (err, results) => {
		if (err) return res.send(err)
		res.send({
			status: 0,
			message: '获取文章分类列表成功！',
			data: results,
		})
	})
}

// 新增文章分类
exports.addArticleCates = (req, res) => {
	// 定义查询 分类名称 与 分类别名 是否被占用的 SQL 语句
	const sql = `select * from ev_article_cate where name=? or alias=?`
	db.query(sql, [req.body.name, req.body.alias], (err, results) => {
		if (err) return res.send(err)
		if (results.length === 2) return res.send('分类名称与别名被占用，请更换后重试！')
		if (results.length === 1 && results[0].name === req.body.name) return res.send('分类名称被占用，请更换后重试！')
		if (results.length === 1 && results[0].alias === req.body.alias) return res.send('分类别名被占用，更换后重试！')
		// TODO 新增文章分类
		const sql = `insert into ev_article_cate set ?`
		db.query(sql, req.body, (err, results) => {
			if (err) return res.send(err)
			if (results.affectedRows !== 1) return res.send('新增文章分类失败！')
			res.send('新增文章分类成功！')
		})
	})
}

exports.deleteCateById = (req, res) => {
	const sql = 'update ev_article_cate set is_delete=1 where id=?'
	db.query(sql, req.params.id, (err, results) => {
		if (err) return res.send(err)
		if (results.affectedRows !== 1) return res.send('删除文章分类失败！')
		res.send('删除文章分类成功')
	})
}

exports.getArticleById = (req, res) => {
	const sql = `select * from ev_article_cate where id=?`
	db.query(sql, req.params.id, (err, results) => {
		if (err) return res.send(err)
		if (results.length !== 1) return res.send('获取文章分类失败！')
		res.send({
			status: 0,
			message: '获取文章分类数据成功',
			data: results[0]
		})
	})
}

// 更新文章分类的处理函数
exports.updateCateById = (req, res) => {
	// 定义查询 分类名称 与 分类别名 是否被占用的 SQL 语句
	const sql = `select * from ev_article_cate where id<>? and (name=? or alias=?)`
	// 判断 分类名称 和 分类别名 是否被占用
	db.query(sql, [req.body.id, req.body.name, req.body.alias], (err, results) => {
		if (err) return res.send(err)
		if (results.length === 2) return res.send('分类名称与别名被占用，请更换后重试！')
		if (results.length === 1 && results[0].name === req.body.name) return res.send('分类名称被占用，请更换后重试！')
		if (results.length === 1 && results[0].alias === req.body.alias) return res.send('分类别名被占用，请更换后重试！')

		// TODO 更新文章分类
		const sql = `update ev_article_cate set ? where id=?`
		db.query(sql, [req.body, req.body.id], (err, results) => {
			// 执行 SQL 语句失败
			if (err) return res.send(err)

			// SQL 语句执行成功，但是影响行数不等于 1
			if (results.affectedRows !== 1) return res.send('更新文章分类失败！')

			// 更新文章分类成功
			res.send('更新文章分类成功！')
		})
	})
}