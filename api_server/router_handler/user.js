/**
* 功能描述: 路由处理函数
* @author : 徐鑫
* 创建时间:2022/07/17 18:17:52
*/
const db = require('../db/index')
// 加密组件
const bcrypt = require('bcryptjs')
// 用这个包来生成 Token 字符串
const jwt = require('jsonwebtoken')
const config = require('../config')
exports.reguser = (req, res) => {
	// 或崎岖客户端提交到服务器的用户信息
	const userinfo = req.body
	// 对表单的数据进行合理化校验
	if (!userinfo.username || !userinfo.password) {
		return res.send({
			status: 1,
			message: '用户名和密码不能为空！'
		})
	}

	// 定义sql语句，查询用户名是否被占用
	const sqlStr = 'select * from ev_users where username=?'
	db.query(sqlStr, [userinfo.username], (err, results) => {
		// 执行sql语句失败
		if (err) {
			return res.send({
				status: 1,
				message: err.message,
			})
		}
		// 判断用户名是否被占用
		if (results.length > 0) {
			return res.send({
				status: 1,
				message: ('用户名被占用，请更换其他用户名！'),
			})
		}
		//用户名可以使用
		// ! 调用bcrypt对密码进行加密
		userinfo.password = bcrypt.hashSync(userinfo.password, 10)

		// ? 插入用户
		const sqlInsert = 'insert into ev_users set ?'
		db.query(sqlInsert, { username: userinfo.username, password: userinfo.password }, (err, results) => {
			if (err) return res.send({
				status: 1,
				message: err.message
			})
			if (results.affectedRows !== 1) {
				return res.send({
					status: 1,
					message: '注册用户失败，请稍后再试！'
				})
			}
			// 注册成功
			res.send({
				status: 0,
				message: '注册成功！'
			})
		})
	})
}

// 登录
exports.login = (req, res) => {
	const userinfo = req.body
	const sql = `select * from ev_users where username=?`
	db.query(sql, [userinfo.username], (err, results) => {
		// 执行sql失败
		if (err) {
			return res.send(err)
		}
		// 没有查询到对应的数据
		if (results.length !== 1) {
			return res.send('查不到对应的数据，登录失败')
		}

		// TODO 拿着用户输入的密码,和数据库中存储的密码进行对比
		const compareResult = bcrypt.compareSync(userinfo.password, results[0].password)

		// 如果对比的结果等于 false, 则证明用户输入的密码错误
		if (!compareResult) {
			return res.send('用户密码输入错误，登录失败！')
		}

		// TODO 在服务器端生成Token字符串
		const user = { ...results[0], password: '', }
		// 对用户的信息进行加密
		const tokenStr = jwt.sign(user, config.jwtSecretKey, { expiresIn: config.expiresIn })
		// 调用res.send()将Token响应给客户端
		res.send({
			status: 0,
			message: '登录成功',
			token: 'Bearer ' + tokenStr
		})
	})
}