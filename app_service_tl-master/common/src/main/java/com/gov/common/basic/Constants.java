package com.gov.common.basic;

import java.math.BigDecimal;

public class Constants {

    /** 短信前缀 */
    public static final String SMS = "SMS";

    public static final String SMS_COMTENT_PD = "{}";

    public static final String SMS_COMTENT_PD_REGEX = "\\{}";

    /** 营销活动 会员姓名占位符 **/
    public static final String SMS_COMTENT_PD_NAME = "{KEY:MEMBERNAME}";

    /** 营销活动 会员积分占位符 **/
    public static final String SMS_COMTENT_PD_SCOPE = "{KEY:MEMBERSCOPE}";

    // 空字符串
    public static final String EMPTY_STRING = "";

    /** 短信发送线程锁 **/
    public static final String LOCK_SEND_MARKETING_SMS = "sendMarketingSms";

    /** 营销任务短信发送线程锁 **/
    public static final String LOCK_SEND_MARKET_TASK_SMS = "sendMarketTaskSms";

    /** 100 **/
    public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    /** -1/负一 **/
    public static final BigDecimal MINUS_ONE = new BigDecimal("-1");

    /** BigDecimal 除法精确位 **/
    public static final int SCALE = 4;

    /** 两位精度 **/
    public static final int SCALE_TWO = 2;

    public static final String WECHAT_ACCESSTOKEN = "wechat:accessToken:";

    public static final String GAIA_SD_ELECTRON_CHANGE_GSEC_ID = "GAIA_SD_ELECTRON_CHANGE_GSEC_ID";

    /**
     * 建议反馈类型
     */
    public interface FeedbackType {
        String TYPE_ONE = "1"; //
        String TYPE_TWO = "2"; //
    }

    /**
     * token获取返回类型
     */
    public interface TokenType {
        String TOKEN = "token";             //返回token字符串
        String CLIENT_LIST = "clientList";  //返回供应商列表
    }

    /**
     * 短信服务 业务类型
     */
    public interface SMSBusinessType {
        String BUSINESS_LOGIN = "1";     //登陆业务
        String BUSINESS_REGISTER = "2"; // 注册
        String BUSINESS_BIND = "3"; // 绑定
    }

    /**
     * 短信模版id
     */
    public interface SMSid {
        String SMS_ID_LOGIN = "SMS0000001"; //登陆业务
        String SMS_ID_REGISTER = "SMS0000002"; //注册业务
        String SMS_ID_BIND = "SMS0000003"; //绑定业务
        String SMS0000005 = "SMS0000005"; //店长注册业务
        String SMS0000006 = "SMS0000006"; //店长修改业务
        String SMS0000007 = "SMS0000007"; //加盟商注册验证码
        String SMS0000008 = "SMS0000008"; //加盟商注册责任人
        String SMS0000009 = "SMS0000009"; //加盟商注册委托人
        String SMS0000010 = "SMS0000010"; //发票下载
    }

    /**
     * 用户消息是否已读
     */
    public interface UserMessage {
        Integer READ = 1;       //已读
        Integer NOT_READ = 0;   //未读
    }

    /**
     * 部门是否停用
     */
    public interface DepStatus {
        String DEP_ABLE = "1";      // 启用
        String DEP_DISABLE = "0";   // 停用
    }

    /**
     * APP版本是否发布
     */
    public interface AppReleaseFlag {
        String RELEASE_NO = "1";    // 否
        String RELEASE_YES = "2";   // 是
    }


}
