package com.gov.common.basic;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApprovalEnum {
    CANCEL("1", "审批驳回"),
    SUCCESS("3", "审批通过");

    private String code;
    private String label;


    public void setCode(String code) {
        this.code = code;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static ApprovalEnum getEnumByCode(String code) {
        for (ApprovalEnum e : ApprovalEnum.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }
}
