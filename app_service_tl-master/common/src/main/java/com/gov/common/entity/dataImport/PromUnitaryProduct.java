package com.gov.common.entity.dataImport;

import com.gov.common.basic.CommonEnum;
import com.gov.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: xx
 * @date: 2021.02.26
 */
@Data
public class PromUnitaryProduct {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING)
    private String proSelfCode;

    /**
     * 效期天数
     */
    @ExcelValidate(index = 1, name = "效期天数", type = ExcelValidate.DataType.INTEGER, required = false)
    private String gspusVaild;

    /**
     * 达到数量1
     */
    @ExcelValidate(index = 2, name = "达到数量1", type = ExcelValidate.DataType.INTEGER, required = false)
    private String gspusQty1;

    /**
     * 促销价1
     */
    @ExcelValidate(index = 3, name = "促销价1", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspusPrc1;

    /**
     * 促销折扣1
     */
    @ExcelValidate(index = 4, name = "促销折扣1", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspusRebate1;

    /**
     * 达到数量2
     */
    @ExcelValidate(index = 5, name = "达到数量2", type = ExcelValidate.DataType.INTEGER, required = false)
    private String gspusQty2;

    /**
     * 促销价2
     */
    @ExcelValidate(index = 6, name = "促销价2", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspusPrc2;

    /**
     * 促销折扣2
     */
    @ExcelValidate(index = 7, name = "促销折扣2", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspusRebate2;

    /**
     * 达到数量3
     */
    @ExcelValidate(index = 8, name = "达到数量3", type = ExcelValidate.DataType.INTEGER, required = false)
    private String gspusQty3;

    /**
     * 促销价3
     */
    @ExcelValidate(index = 9, name = "促销价3", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspusPrc3;


    /**
     * 促销折扣3
     */
    @ExcelValidate(index = 10, name = "促销折扣3", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspusRebate3;

    /**
     * 是否会员
     */
    @ExcelValidate(index = 11, name = "是否会员", type = ExcelValidate.DataType.STRING, required = false, maxLength = 10,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.IS_OR_NO, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String gspusMemFlag;

    /**
     * 是否限价
     */
    @ExcelValidate(index = 12, name = "是否限价", type = ExcelValidate.DataType.STRING, required = false, maxLength = 1,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.IS_OR_NO, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String gspusLimitFlag;

    /**
     * 限价类型
     */
    @ExcelValidate(index = 13, name = "限价类型", type = ExcelValidate.DataType.STRING, required = false, maxLength = 2,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.GSPUS_LIMIT_TYPE, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String gspusLimitType;

}

