package com.gov.common.basic;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gov.common.entity.weChat.*;
import com.gov.common.response.CustomResultException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class WeChatUtils {

    /**
     * 凭证获取
     */
    private static final String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";

    /**
     * 草稿列表
     */
    private static final String DRAFT_BATCHGET_URL = "https://api.weixin.qq.com/cgi-bin/draft/batchget?access_token={0}";
    /**
     * 草稿发布
     */
    private static final String FREEPUBLISH_SUBMIT_URL = "https://api.weixin.qq.com/cgi-bin/freepublish/submit?access_token={0}";
    /**
     * 素材列表接口
     */
    private static final String BATCHGET_MATERIAL_URL = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token={0}";
    /**
     * 素材详情
     */
    private static final String GET_MATERIAL_URL = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token={0}";
    /**
     * 成功发布列表
     */
    private static final String FREEPUBLISH_BATCHGET_URL = "https://api.weixin.qq.com/cgi-bin/freepublish/batchget?access_token={0}";
    /**
     * 成功发布详情
     */
    private static final String GET_FREEPUBLISH_BATCHGET_URL = "https://api.weixin.qq.com/cgi-bin/freepublish/getarticle?access_token={0}";

    /**
     * 菜单
     */
    private static final String GET_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}";
    /**
     * 菜单删除
     */
    private static final String DELETE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token={0}";
    /**
     * 菜单创建
     */
    private static final String CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}";
    /**
     * 图片上传
     */
    private static final String UPLOAD_IMAGE_URL = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token={0}&type=image";
    /**
     * 上传图文
     */
    private static final String ADD_NEWS_URL = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token={0}";
    /**
     * 发布图文
     */
    private static final String SENDALL_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={0}";
    /**
     * 删除永久素材
     */
    private static final String DEL_MATERIAL_URL = "https://api.weixin.qq.com/cgi-bin/material/del_material?access_token={0}";

    /**
     * 发送消息
     */
    private static final String SEND_MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";

    /**
     * 更新会员卡
     */
    private static final String UPDATE_CARD_URL = "https://api.weixin.qq.com/card/update?access_token={0}";

    /**
     * 设置表单字段
     */
    private static final String ACTIVATEUSERFORM_SET_URL = "https://api.weixin.qq.com/card/membercard/activateuserform/set?access_token={0}";

    /**
     * 获取注册会员信息
     */
    private static final String GET_MEMBER_INFO = "https://api.weixin.qq.com/card/membercard/userinfo/get?access_token={0}";


    private static final String ACTIVE_MEMBER_INFO = "https://api.weixin.qq.com/card/membercard/activatetempinfo/get?access_token={0}";

    /**
     * 解码code
     */
    private static final String DECRYPT_CODE =  "https://api.weixin.qq.com/card/code/decrypt?access_token={0}";

    /**
     * 获取卡券激活url
     */
    private static final String GET_ACTIVE_URL = "https://api.weixin.qq.com/card/membercard/activate/geturl?access_token={0}";

    private static final String ACTIVE_USER_URL = "https://api.weixin.qq.com/card/membercard/activate?access_token={0}";


    /**
     * 获取公众号 token
     *
     * @param weChatAppid
     * @param weChatSecret
     * @return
     */
    public String getAccessToken(String weChatAppid, String weChatSecret) {

        Mono<String> response = WebClient.create()
                .get()
                .uri(MessageFormat.format(TOKEN_URL, weChatAppid, weChatSecret))
                .retrieve()
                .bodyToMono(String.class);
        String result = response.block();

        log.info("公众号token:APPID:{},结果{}", weChatAppid, result);

        JSONObject json = JSONObject.parseObject(result);
        return json.getString("access_token");
    }

    /**
     * 公众号草稿列表
     *
     * @param accessToken
     * @param offset
     * @param count
     * @return
     */
    public String getDraftBatchgetList(String accessToken, int offset, int count) {
        Map<String, Object> map = new HashMap<>();
        map.put("offset", offset);
        map.put("count", count);
        map.put("no_content", "0");
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(DRAFT_BATCHGET_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("草稿列表,参数:{}", map);
        return result;
    }

    /**
     * 草稿发布
     *
     * @param media_id
     */
    public String freepublishSubmit(String accessToken, String media_id) {
        Map<String, Object> map = new HashMap<>();
        map.put("media_id", media_id);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(FREEPUBLISH_SUBMIT_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("草稿发布,参数:{}", map);
        return result;
    }

    /**
     * 素材列表获取
     *
     * @param accessToken
     * @param type
     * @param offset
     * @param count
     * @return
     */
    public String getMaterialList(String accessToken, String type, int offset, int count) {
        Map<String, Object> map = new HashMap<>();
        map.put("type", type);
        map.put("offset", offset);
        map.put("count", count);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(BATCHGET_MATERIAL_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("素材列表,参数:{}", map);
        return result;
    }

    /**
     * 公众号素材详情
     *
     * @param accessToken
     * @param mediaId
     * @return
     */
    public String getMateria(String accessToken, String mediaId) {
        Map<String, Object> map = new HashMap<>();
        map.put("media_id", mediaId);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(GET_MATERIAL_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("公众号素材详情,参数:{},返回值:{}", map, result);
        return result;
    }

    /**
     * 素材列表获取
     *
     * @param accessToken
     * @param type
     * @param offset
     * @param count
     * @return
     */
    public String getFreepublishBatchgetList(String accessToken, String type, int offset, int count) {
        Map<String, Object> map = new HashMap<>();
        map.put("offset", offset);
        map.put("count", count);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(FREEPUBLISH_BATCHGET_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("素材列表,参数:{}", map);
        return result;
    }

    /**
     * 公众号素材详情
     *
     * @param accessToken
     * @param mediaId
     * @return
     */
    public String getFreepublishBatchget(String accessToken, String mediaId) {
        Map<String, Object> map = new HashMap<>();
        map.put("article_id", mediaId);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(GET_FREEPUBLISH_BATCHGET_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("公众号素材详情,参数:{},返回值:{}", map, result);
        return result;
    }

    /**
     * 菜单获取接口
     *
     * @param accessToken
     * @return
     */
    public String getWeChatMenu(String accessToken) {
        String url = MessageFormat.format(GET_MENU_URL, accessToken);
        Mono<String> response = WebClient.create().get().uri(url).retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("菜单列表,参数:{},返回值:{}", "", result);
        return result;
    }

    /**
     * 菜单删除
     *
     * @param accessToken
     * @return
     */
    public String deleteWeChatMenu(String accessToken) {
        String url = MessageFormat.format(DELETE_MENU_URL, accessToken);
        Mono<String> response = WebClient.create().get().uri(url).retrieve().bodyToMono(String.class);
        return response.block();
    }

    /**
     * 菜单新增
     *
     * @param accessToken
     * @param param
     * @return
     */
    public String createWeChatMenu(String accessToken, CreateWeChatMenuParam param) {
        String url = MessageFormat.format(CREATE_MENU_URL, accessToken);
        Mono<String> response = WebClient.create().post().uri(url)
                .body(BodyInserters.fromObject(param))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("菜单新增:参数:{},返回值:{}", param, result);
        return result;
    }

    /**
     * 微信通过Url上传图片
     *
     * @param accessToken
     * @param imageUrl
     * @return
     */
    public String uploadimgByUrl(String accessToken, String imageUrl) {
        // todo 代码需整理
        try {
            HttpClient httpclient = new HttpClient();
            GetMethod getMethod = new GetMethod(imageUrl);
            httpclient.executeMethod(getMethod);
            String info = new String(getMethod.getResponseBody());

            URL urlObj = new URL(MessageFormat.format(UPLOAD_IMAGE_URL, accessToken));
            HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false); // post方式不能使用缓存
            // 设置请求头信息
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Charset", "UTF-8");
            // 设置边界
            String BOUNDARY = "----------" + System.currentTimeMillis();
            con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
            // 请求正文信息
            // 第一部分：
            StringBuilder sb = new StringBuilder();
            sb.append("--"); // 必须多两道线
            sb.append(BOUNDARY);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"media\";filelength=\"" + info.length() + "\";filename=\"Url流读取方式.jpg" + "\"\r\n");
            sb.append("Content-Type:application/octet-stream\r\n\r\n");
            byte[] head = sb.toString().getBytes(StandardCharsets.UTF_8);

            // 获得输出流
            OutputStream out = new DataOutputStream(con.getOutputStream());
            // 输出表头
            out.write(head);

            // 文件正文部分
            // 把文件已流文件的方式 推入到url中
            DataInputStream in = new DataInputStream(getMethod.getResponseBodyAsStream());
            int bytes = 0;
            byte[] bufferOut = new byte[1024];
            while ((bytes = in.read(bufferOut)) != -1) {
                out.write(bufferOut, 0, bytes);
            }
            in.close();

            // 结尾部分
            // 定义最后数据分隔线
            byte[] foot = ("\r\n--" + BOUNDARY + "--\r\n").getBytes(StandardCharsets.UTF_8);
            out.write(foot);
            out.flush();
            out.close();
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = null;
            String result = null;
            try {
                // 定义BufferedReader输入流来读取URL的响应
                reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                result = buffer.toString();

            } catch (IOException e) {
                log.info("上传图片失败，{}", e.getMessage());
                throw new CustomResultException("上传图片失败");
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
            log.info("上传图片,参数:{},返回值:{}", imageUrl, result);
            if (result != null && !"".equals(result)) {
                JSONObject json = JSONObject.parseObject(result);
                return (String) json.get("media_id");
            }
            throw new CustomResultException("上传图片失败");
        } catch (Exception e) {
            log.info("上传图片失败，{}", e.getMessage());
            throw new CustomResultException("上传图片失败");
        }


    }

    /**
     * 上传图文
     *
     * @param accessToken
     * @param newsList
     * @return
     */
    public String addNews(String accessToken, List<News> newsList) {
        Map<String, Object> map = new HashMap<>();
        map.put("articles", newsList);
        Mono<String> response = WebClient.create().post().uri(MessageFormat.format(ADD_NEWS_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("上传图文,参数:{},返回值:{}", map, result);
        if (result != null && !"".equals(result)) {
            JSONObject json = JSONObject.parseObject(result);
            return json.getString("media_id");
        }
        return null;
    }

    /**
     * 发布图文
     *
     * @param accessToken
     * @param mediaId
     * @return
     */
    public Integer newsPulish(String accessToken, String mediaId) {
        Map<String, Object> filterMap = new HashMap<>();
        filterMap.put("is_to_all", true);
        Map<String, Object> mpnewsMap = new HashMap<>();
        mpnewsMap.put("media_id", mediaId);

        Map<String, Object> map = new HashMap<>();
        map.put("filter", filterMap);
        map.put("mpnews", mpnewsMap);
        map.put("msgtype", "mpnews");
        map.put("send_ignore_reprint", 1);
        log.info("body json: {}", map);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(SENDALL_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        JSONObject json = JSONObject.parseObject(result);
        log.info("图文发布,参数:{},返回值:{}", map, result);
        return (Integer) json.get("errcode");
    }

    /**
     * 删除图文
     */
    public String delMaterial(String accessToken, String mediaId) {
        Map<String, Object> map = new HashMap<>();
        map.put("media_id", mediaId);
        Mono<String> response = WebClient.create().post().uri(MessageFormat.format(DEL_MATERIAL_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("删除图文,参数:{},返回值:{}", map, result);
        return result;
    }

    /**
     * @param accessToken token
     * @param openId      用户openid
     * @param template_id 模板
     * @param data        参数
     * @return
     */
    public static boolean sendTempMessage(String accessToken, String openId, SendTempMessage.MessageId template_id, Map<SendTempMessage.ParamValueKey, SendTempMessageParams> data) {
        try {
            String url = MessageFormat.format(SEND_MESSAGE_URL, accessToken);
            SendTempMessage sendTempMessage = new SendTempMessage();
            sendTempMessage.setTouser(openId);
            sendTempMessage.setTemplate_id(template_id.getTemplate_id());
            sendTempMessage.setData(data);
            log.info("微信发送消息参数：{}", JsonUtils.beanToJson(sendTempMessage));
            Mono<String> response = WebClient.create()
                    .post()
                    .uri(url)
                    .body(BodyInserters.fromObject(sendTempMessage))
                    .retrieve()
                    .bodyToMono(String.class);
            // 返回值
            String result = response.block();
            log.info("微信发送消息返回值：{}", result);
            JSONObject jsonObject = JSON.parseObject(result);
            Object errcode = jsonObject.get("errcode");
            if (errcode != null || "0".equals(errcode.toString())) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("微信发送消息失败");
        }
        return false;
    }


    public String updateMemberCard(String accessToken, String cardId, String activateAfterSubmitUrl, String gzhUrl) {
        Map<String, Object> map = new HashMap<>();
        map.put("card_id", cardId);

        MemberCardDTO memberCardDTO = new MemberCardDTO();
        memberCardDTO.setWx_activate(true);
        memberCardDTO.setWx_activate_after_submit(true);
        memberCardDTO.setWx_activate_after_submit_url(activateAfterSubmitUrl);
        memberCardDTO.setSupply_balance(false);
        memberCardDTO.setSupply_bonus(false);
        memberCardDTO.setCustom_field1(new MemberCardCusFieldDTO().setName("积分").setUrl(gzhUrl));
        memberCardDTO.setCustom_field2(new MemberCardCusFieldDTO().setName("余额").setUrl(gzhUrl));
        memberCardDTO.setCustom_field3(new MemberCardCusFieldDTO().setName("优惠券").setUrl(gzhUrl));
        map.put("member_card", memberCardDTO);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(UPDATE_CARD_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("更新会员卡接口,参数:{}, 返回：{}", map, result);
        return result;
    }

    public String setActivateuserform(String accessToken, String cardId) {
        Map<String, Object> map = new HashMap<>();
        map.put("card_id", cardId);

        Activateuserform required_form = new Activateuserform();
        required_form.setCan_modify(false);
        List<String> fieldIdList = new ArrayList<>();
        fieldIdList.add("USER_FORM_INFO_FLAG_MOBILE");
        fieldIdList.add("USER_FORM_INFO_FLAG_NAME");
        fieldIdList.add("USER_FORM_INFO_FLAG_SEX");
        fieldIdList.add("USER_FORM_INFO_FLAG_BIRTHDAY");
        required_form.setCommon_field_id_list(fieldIdList);
        map.put("required_form", required_form);

        Activateuserform optional_form = new Activateuserform();
        optional_form.setCan_modify(false);
        List<String> fieldIdList2 = new ArrayList<>();
        fieldIdList2.add("USER_FORM_INFO_FLAG_IDCARD");
        fieldIdList2.add("USER_FORM_INFO_FLAG_LOCATION");
        optional_form.setCommon_field_id_list(fieldIdList2);
        List<String> customFieldList = new ArrayList<>();
        customFieldList.add("推荐人");
        optional_form.setCustom_field_list(customFieldList);
        map.put("optional_form", optional_form);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(ACTIVATEUSERFORM_SET_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("会员卡设置表单接口,参数:{},返回：{}", map, result);
        return result;
    }

    /**
     * 获取会员信息
     *
     * @param accessToken
     * @param cardId
     * @param code
     * @return
     */
    public String getMemberInfoByCard(String accessToken, String cardId, String code) {
        Map<String, Object> map = new HashMap<>();
        map.put("card_id", cardId);
        map.put("code", code);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(GET_MEMBER_INFO, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("拉取会员信息接口,参数:{}, 返回：{}", map, result);
        return result;
    }

    @SneakyThrows
    public String activatetempinfo(String accessToken, String activate_ticket) {
        Map<String, Object> map = new HashMap<>();
        map.put("activate_ticket", URLEncoder.encode(activate_ticket, "utf-8"));
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(ACTIVE_MEMBER_INFO, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("拉取会员信息接口,参数:{}, 返回：{}", map, result);
        return result;
    }

    /**
     * 解码code
     *
     * @param accessToken
     * @param encryptCode
     * @return
     */
    @SneakyThrows
    public String decryptCode(String accessToken, String encryptCode) {
        Map<String, Object> map = new HashMap<>();
        map.put("encrypt_code", URLEncoder.encode(encryptCode, "utf-8"));
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(DECRYPT_CODE, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("解码接口,参数:{}, 返回：{}", map, result);
        JSONObject json = JSONObject.parseObject(result);
        return json.getString("code");
    }

    /**
     * 获取卡券激活url
     *
     * @param accessToken
     * @param cardId      会员卡id
     * @param outer_str   额外参数，激活后事件会返回
     * @return
     */
    public String getActiveUrl(String accessToken, String cardId, String outer_str) {
        Map<String, Object> map = new HashMap<>();
        map.put("card_id", cardId);
        map.put("outer_str", outer_str);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(GET_ACTIVE_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("获取卡券激活url接口,参数:{}, 返回：{}", map, result);
        JSONObject json = JSONObject.parseObject(result);
        return json.getString("url");
    }

    /**
     * 会员激活
     *
     * @param accessToken
     * @param memberActiveParams
     * @return
     */
    public Integer membercardActive(String accessToken, MemberActiveParams memberActiveParams) {
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(ACTIVE_USER_URL, accessToken))
                .body(BodyInserters.fromObject(memberActiveParams))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("获取卡券激活url接口,参数:{}, 返回：{}", JsonUtils.beanToJson(memberActiveParams), result);
        JSONObject json = JSONObject.parseObject(result);
        return (Integer) json.get("errcode");
    }


//    public static void main(String[] ager) {
//        String token = "46_gT1r7B7kP1s9mOpeEFIg0xr_gCeDalDn7GO8ddsyBBtzWVDBksmP9BxoQstT-tcfPLAoH_qixgMICfNAZTmv08sAsAWiwRULeyeCKZVP13nWr20qfWnSpa9Sy58WSHgu5PYsFhldLbMtAHi2LIRaACAWTC";
//        String openId = "oGh8Pv6z0FOQnzVadKDIOGy3h588";
//        Map<SendTempMessage.ParamValueKey, SendTempMessageParams> params = new HashMap<>();
//        params.put(SendTempMessage.ParamValueKey.orderInfo, new SendTempMessageParams("bbbbb", null));
//        boolean result = sendTempMessage(token, openId, SendTempMessage.MessageId.PURCHASE_REMIND, params);
//        System.out.println(result);
//    }

}
