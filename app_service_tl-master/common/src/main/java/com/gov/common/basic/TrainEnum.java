package com.gov.common.basic;

public class TrainEnum {

    /**
     * 题目类型
     */
    public enum CourseType {
        NEW_EMPLOYEE("1", "新员工培训"),
        BASIC_KNOWLEDGE("2", "基础知识培训"),
        STORE_MANAGE("3", "门店管理培训"),
        QUALITY_TRAIN("4", "质量知识培训"),
        MARKET_TRAIN("5", "营销培训");

        private String code;
        private String message;

        CourseType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 难度级别
     */
    public enum CourseLevel {
        HARD("1", "难"),
        MIDDLE("2", "中"),
        EASILY("3", "易");

        private String code;
        private String message;

        CourseLevel(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    /**
     * 难度级别 1、单选，2、多选，3、不启用
     */
    public enum QuestionType {
        SINGLE("1", "单选"),
        MULTIPLE("2", "多选"),
        NOT_ENABLED("3", "不启用");

        private String code;
        private String message;

        QuestionType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 培训主旨 1、一般培训,2、考试
     */
    public enum GtSubject {
        GENERAL_TRAINING("1", "一般培训"),
        EXAM("2", "考试");

        private String code;
        private String message;

        GtSubject(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 培训期限 1、永久,2、培训保留期
     */
    public enum GtEndType {
        PERMANENT("1", "永久"),
        PERIOD("2", "期限保留");

        private String code;
        private String message;

        GtEndType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    /**
     * 课件推送类型 1、模块随机,2、手动选择
     */
    public enum GtCourseType {
        RANDOM(1, "模块随机"),
        MANUAL(2, "手动选择");

        private Integer code;
        private String message;

        GtCourseType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 培训状态 （0：停用，1：启用）
     */
    public enum GtStatus {
        DOWN(0, "停用"),
        UP(1, "启用");

        private Integer code;
        private String message;

        GtStatus(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


}
