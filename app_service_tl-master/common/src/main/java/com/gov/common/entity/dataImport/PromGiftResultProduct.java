package com.gov.common.entity.dataImport;

import com.gov.common.basic.CommonEnum;
import com.gov.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: xx
 * @date: 2021.02.26
 */
@Data
public class PromGiftResultProduct {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING)
    private String proSelfCode;


    /**
     * 单品价格
     */
    @ExcelValidate(index = 1, name = "单品价格", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspgrGiftPrc;


    /**
     * 单品折扣
     */
    @ExcelValidate(index = 2, name = "单品折扣", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspgrGiftRebate;

}

