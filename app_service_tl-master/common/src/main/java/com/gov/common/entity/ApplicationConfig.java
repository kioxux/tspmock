package com.gov.common.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.19
 */
@Data
@Component
@ConfigurationProperties(prefix = "applicationconfig")
public class ApplicationConfig {
    /**
     * 用户的企业编码
     */
    private String smsspcode;

    /**
     * 用户名
     */
    private String smsusername;

    /**
     * 密码
     */
    private String smspassword;

    /**
     * 营销短信参数
     */
    private String marketingSmsspcode;
    /**
     * 营销短信参数
     */
    private String marketingSmsusername;
    /**
     * 营销短信参数
     */
    private String marketingSmspassword;

    /**
     * 接口地址
     */
    private String smsurl;

    /**
     * 短信批量发送最大条数
     */
    private Integer smsBatchCnt;

    /**
     * COS cosSecretId
     */
    private String cosSecretId;
    /**
     * COS cosSecretKey
     */
    private String cosSecretKey;
    /**
     * COS cosRegion
     */
    private String cosRegion;
    /**
     * COS cosBucket
     */
    private String cosBucket;
    /**
     * cos cosUrl
     */
    private String cosUrl;
    /**
     * 导入目录
     */
    private String importPaht;
    /**
     * 导出目录
     */
    private String exportPath;
    /**
     * B扫C请求路径
     */
    private String bsaocpayurl;
    /**
     * 支付测试
     */
    private Integer bsaocpaytest;

    /**
     * 安卓推送APPid
     */
    private String tpnsAndroidAppId;
    /**
     * 安卓推送SecretKey;
     */
    private String tpnsAndroidSecretKey;
    /**
     * 推送服务
     */
    private String tpnsDoMain;
    /**
     * 苹果推送APPid
     */
    private String tpnsIosAppId;
    /**
     * 苹果推送SecretKey;
     */
    private String tpnsIosSecretKey;
    /**
     * 苹果用 0, "product"    1, "dev"
     */
    private int environment;
    /**
     * 微信公众号appid
     */
    private String weChatAppid;
    /**
     * 密钥
     */
    private String weChatSecret;

    /**
     * 推送内容
     */
    private String weChatPushContent;

    /**
     * 推送内容链接
     */
    private String weChatPushUrl;

    /*****************************发票*********************************/
    /**
     * 请求地址
     */
    private String invoiceRequestUrl;
    /**
     * 身份认证
     */
    private String invoiceAuth;

    /**
     * 查询请求接口
     */
    private String invoiceResUrl;

    /**
     * 发票目录
     */
    private String invoicePath;

    /**
     * 开票回调
     */
    private String invoiceCallBack;

    /******************************工单******************************/
    /**
     * 工单服务地址
     */
    private String orderServiceUrl;


    //csaobPay

    private String c2b_appId;
    private String c2b_appKey;

    /**
     * 商户号
     */
    private String c2b_mid;

    /**
     * 终端号
     */
    private String c2b_tid;

    /**
     * 业务类型
     */
    private String c2b_instMid;

    /**
     * 商户订单号前缀
     */
    private String c2b_billNoPre;

    /**
     * 支付结果回调地址
     */
    private String c2b_notifyUrl;

    /**
     * 请求地址
     */
    private String c2b_url;
    /**
     * 下单请求地址
     */
    private String c2b_order;

    /**
     * 支付状态查询地址
     */
    private String c2b_queryUrl;

    /**
     * 单钱包和多钱包,多钱包为:MULTIPLE
     */
    private String c2b_walletOption;

    /**
     * 二维码过期时间
     */
    private Integer c2b_expireTime;

    /**
     * 短信单价
     */
    private Integer c2b_smsPrice;

    //bsaobPay

    private String b2b_appId;
    private String b2b_appKey;

    /**
     * 商户号
     */
    private String b2b_mid;

    /**
     * 终端号
     */
    private String b2b_tid;

    /**
     * 业务类型
     */
    private String b2b_instMid;

    /**
     * 商户订单号前缀
     */
    private String b2b_billNoPre;

    /**
     * 支付结果回调地址
     */
    private String b2b_notifyUrl;

    /**
     * 请求地址
     */
    private String b2b_url;
    /**
     * 下单请求地址
     */
    private String b2b_order;

    /**
     * 支付状态查询地址
     */
    private String b2b_queryUrl;

    /**
     * 单钱包和多钱包,多钱包为:MULTIPLE
     */
    private String b2b_walletOption;

    /**
     * 二维码过期时间
     */
    private Integer b2b_expireTime;

    /**
     * 短信单价
     */
    private Integer b2b_smsPrice;

    //invoice

    /**
     * 下载前缀
     */
    private String downloadPath;

    /**
     * 短信签名
     */
    private String sgin;

    /**
     * 销方企业税号
     */
    private String saletaxnum;

    /**
     * 销方企业地址
     */
    private String saleaddress;

    /**
     * 销方企业银行开户行及账号
     */
    private String saleaccount;

    /**
     * 销方企业电话
     */
    private String salephone;

    /**
     * 开票员
     */
    private String clerk;

    /**
     * 推送手机号
     */
    private String phone;

    /*** 目前发票额度千元版最大9999  */
    private String invoiceMaxQuota;
    /*** 提前多少天提醒  */
    private Integer remindDay;

    /**
     * 微信会员卡激活后提交url
     */
    private String activateAfterSubmitUrl;

    /**
     * 公众号地址
     */
    private String gzhUrl;


}