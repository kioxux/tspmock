package com.gov.common.entity.weChat;

import lombok.Data;

@Data
public class MemberCardDTO {

    private boolean wx_activate;

    private boolean wx_activate_after_submit;

    private String wx_activate_after_submit_url;

    private boolean supply_bonus;

    private boolean supply_balance;

    private MemberCardCusFieldDTO custom_field1;

    private MemberCardCusFieldDTO custom_field2;

    private MemberCardCusFieldDTO custom_field3;

}
