package com.gov.common.entity.weChat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class WeChatMenuDTO {

    /**
     * menu : {"button":[{"type":"click","name":"今日歌曲","url":"http://xtk.turingtec.cn/?client=10000001&stoCode=1000000001&openStore=1000000001","key":"V1001_TODAY_MUSIC","sub_button":[]},{"type":"click","name":"歌手简介","key":"V1001_TODAY_SINGER","url":"http://xtk.turingtec.cn/?client=10000001&stoCode=1000000001&openStore=1000000001","sub_button":[]},{"name":"菜单","sub_button":[{"type":"view","name":"搜索","url":"http://www.soso.com/","sub_button":[]},{"type":"view","name":"视频","url":"http://v.qq.com/","sub_button":[]},{"type":"click","name":"赞一下我们","key":"V1001_GOOD","sub_button":[]}]}]}
     */

    private MenuBean menu;

    @NoArgsConstructor
    @Data
    public static class MenuBean {
        private List<ButtonBean> button;

        @NoArgsConstructor
        @Data
        public static class ButtonBean {
            /**
             * type : click
             * name : 今日歌曲
             * url : http://xtk.turingtec.cn/?client=10000001&stoCode=1000000001&openStore=1000000001
             * key : V1001_TODAY_MUSIC
             * sub_button : []
             */

            private String type;
            private String name;
            private String url;
            private String key;
            private List<?> sub_button;
        }
    }
}
