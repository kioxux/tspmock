package com.gov.common.basic;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.invoice.InvoiceEntity;
import com.gov.common.entity.invoice.InvoiceParams;
import com.gov.common.entity.invoice.InvoiceSendResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * @Author staxc
 * @Date 2020/10/19 14:10
 * @desc 发票处理
 */
@Slf4j
@Component
public class InvoiceUtils {

    /*public static void main(String[] args) throws Exception {
     *//*String str = "https://invtest.nntest.cn/fp/56tEksenDSuJ0V_WfBo5xnxe6ZZsAXSpv8WOk7ybTCHqW4mgYIDsf-_VOmCNryu3EkGfN3x4BQYCy-2OVYF36A.pdf";
        System.out.println("fileName= " + str.substring(str.lastIndexOf("/") + 1));*//*


        //开票请求
        String reqUrl = "https://nnfpbox.nuonuocs.cn/shop/buyer/allow/cxfKp/cxfServerKpOrderSync.action";
        String reqOrder = "{\"identity\":\"2329CC5F90EDAA8208F1F3C72A0CE72A713A9D425CD50CDE\",\"order\":\"buyername\":\"浙江爱信诺\",\"taxnum\":\"124511234993295177\",\"phone\":\"0\",\"address\":" +
                "\"浙江省杭州市万塘路\",\"account\":\"\",\"telephone\":\"0\",\"orderno\":\"staxcwerwer234\",\"invoicedate\":\"2018-10-31 19:16:51\",\"clerk\":\"黄芝\",\"saleaccount\":\"宇宙行442612010103507108\"," +
                "\"salephone\":\"0774-7893911\",\"saleaddress\":\"富川瑶族自治县新永路138号\",\"saletaxnum\":\"339901999999142\"," +
                "\"kptype\":\"1\",\"message\":\"\",\"payee\":\"林莉苏\",\"checker\":\"林莉苏\",\"tsfs\":\"-1\"," +
                "\"email\":\"502192347@qq.com\",\"qdbz\":\"0\",\"qdxmmc\":\"\",\"dkbz\":\"0\",\"deptid\":\"\",\"clerkid\"" +
                ":\"\",\"invoiceLine\":\"p\",\"cpybz\":\"\",\"detail\":[{\"goodsname\":\"苹果\",\"num\":\"1\",\"price\":" +
                "\"1\",\"hsbz\":\"0\",\"taxrate\":\"0.13\",\"spec\":\"\",\"unit\":\"吨\",\"spbm\":\"10101150101\",\"zxbm\":" +
                "\"\",\"fphxz\":\"0\",\"yhzcbs\":\"0\",\"zzstsgl\":\"\",\"lslbs\":\"\",\"kce\":\"\"}]}}";

        //返回的发票流水号  20102611290901586544 20102611395101586710 20102611425301586759
        String testOrder = "{\"identity\":\"2329CC5F90EDAA8208F1F3C72A0CE72A713A9D425CD50CDE\",\"order\":{\"buyername\":\"浙江爱信诺\",\"taxnum\":\"124511234993295177\",\"address\":\"江苏省无锡市\",\"account\":\"\",\"telephone\":\"18745639768\",\"orderno\":\"nuonuosfsfs7654321\",\"invoicedate\":\"2018-10-31 19:16:51\",\"saletaxnum\":\"339901999999142\",\"saleaccount\":\"测试行442612010103507108\",\"salephone\":\"0889-1234567\",\"saleaddress\":\"江苏省南京是124号\",\"kptype\":\"1\",\"clerk\":\"男人\",\"payee\":\"女人\",\"tsfs\":\"-1\",\"phone\":\"15885674353\",\"qdbz\":\"0\",\"dkbz\":\"0\",\"invoiceLine\":\"p\",\"cpybz\":\"\",\"detail\":[{\"goodsname\":\"手机\",\"num\":\"1\",\"price\":\"1\",\"hsbz\":\"1\",\"taxrate\":\"0.13\",\"spec\":\"\",\"unit\":\"升\",\"spbm\":\"10101150102\",\"fphxz\":\"0\",\"yhzcbs\":\"0\",}]}}";
        String resUrl = "https://nnfpbox.nuonuocs.cn/shop/buyer/allow/ecOd/queryElectricKp.action";

        //开票结果查询接口  fpqqlsh 发票请求流水号 20102218041201521585  自己的： 20102309255101532435  20102611290901586544 20102613371301588160
        // 20102613494401588337
        String resultOrder = "{\"identity\":\"93363DCC6064869708F1F3C72A0CE72A713A9D425CD50CDE\",\"fpqqlsh\":[\"20102309255101532435\"]}";
        String testResultOrder = "{\"identity\":\"93363DCC6064869708F1F3C72A0CE72A713A9D425CD50CDE\",\"fpqqlsh\":[\"20110218273901725603\"]}";

        //根据订单号查询发票请求流水号  orderno 订单编号  No.12016101304303
        String noOrder = "{\"identity\":\"93363DCC6064869708F1F3C72A0CE72A713A9D425CD50CDE\",\"orderno\":[\"nuonuo877654321\"]}";


//        System.out.println("reqSend result = " + reqSend(reqUrl, testOrder));

        System.out.println("resSend result by fpqqlsh = " + resSend(resUrl, testResultOrder));

//        System.out.println("resSend result by orderNo = " + resSend(resUrl, noOrder));


    }*/


    /**
     * 开票结果查询
     */
    public static InvoiceSendResponseDto resSend(ApplicationConfig applicationConfig, String order) throws Exception {
        log.info("发票结果请求参数reqJson={}", order);
        //order为密文 需要加密
        order = DESDZFP.encrypt(order);
        HttpClient httpclient = null;
        PostMethod post = null;
        InvoiceSendResponseDto dto = new InvoiceSendResponseDto();
        try {
            httpclient = new HttpClient();
            post = new PostMethod(applicationConfig.getInvoiceResUrl());
            //设置编码方式
            post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            //添加参数
            post.addParameter("order", order);
            //执行
            httpclient.executeMethod(post);
            //接口返回信息
            String res = new String(post.getResponseBody(), "UTF-8");
            log.info("请求发送结果：{}", res);
            JSONObject info = JSONObject.parseObject(res);
            if ("success".equals(info.getString("result"))) {
                JSONArray jan = (JSONArray) info.get("list");
                if (jan != null || jan.size() > 0) {
                    JSONObject jo = jan.getJSONObject(0);
                    dto.setFicoFpqqlsh(jo.getString("c_fpqqlsh"));
                    dto.setCUrl(jo.getString("c_url"));
                    dto.setFicoFphm(jo.getString("c_fphm"));
                    dto.setFicoFpdm(jo.getString("c_fpdm"));
                    dto.setFicoFprq(jo.getString("c_kprq"));
                    dto.setCMsg(
                            jo.getString("c_msg") +
                                    (jo.getString("c_resultmsg") == null || StringUtils.isBlank(jo.getString("c_resultmsg")) ? "" : jo.getString("c_resultmsg"))
                    );
                    return dto;
                }
            }
        } catch (Exception e) {
            log.error("发票结果查询失败");
        } finally {
            //关闭连接，释放资源
            post.releaseConnection();
            ((SimpleHttpConnectionManager) httpclient.getHttpConnectionManager()).shutdown();
        }
        return dto;
    }

    /**
     * 开票请求
     */
    public static String reqSend(ApplicationConfig applicationConfig, InvoiceEntity invoiceEntity, String identity) throws Exception {
        InvoiceParams invoiceParams = new InvoiceParams();
        invoiceParams.setIdentity(identity);
        invoiceParams.setOrder(invoiceEntity);
        String reqJson = JsonUtils.beanToJson(invoiceParams);
        log.info("发票生成请求参数reqJson={}", reqJson);
        //order为密文 需要加密
        reqJson = DESDZFP.encrypt(reqJson);
        HttpClient httpclient = null;
        PostMethod post = null;
        try {
            httpclient = new HttpClient();
            post = new PostMethod(applicationConfig.getInvoiceRequestUrl());
            //设置编码方式
            post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            //添加参数
            post.addParameter("order", reqJson);
            //执行
            httpclient.executeMethod(post);
            //接口返回信息
            String res = new String(post.getResponseBody(), "UTF-8");
            log.info("请求发送结果：{}", res);
            JSONObject info = JSONObject.parseObject(res);
            if ("0000".equals(info.getString("status"))) {
                //返回发票请求流水号
                return info.getString("fpqqlsh");
            } else {
                return "failure" + info.getString("message");
            }
        } catch (Exception e) {
            log.error("发票请求失败");
            e.printStackTrace();
            return "failure";
        } finally {
            //关闭连接，释放资源
            post.releaseConnection();
            ((SimpleHttpConnectionManager) httpclient.getHttpConnectionManager()).shutdown();
        }
    }

    /**
     * 开票请求
     */
    public static String reqSend(String url, String order) throws Exception {
        //order为密文 需要加密
        order = DESDZFP.encrypt(order);
        HttpClient httpclient = null;
        PostMethod post = null;
        try {
            httpclient = new HttpClient();
            post = new PostMethod(url);
            //设置编码方式
            post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            //添加参数
            post.addParameter("order", order);
            //执行
            httpclient.executeMethod(post);
            //接口返回信息
            String res = new String(post.getResponseBody(), "UTF-8");
            log.info("请求发送结果：{}", res);
            JSONObject info = JSONObject.parseObject(res);

            if ("0000".equals(info.getString("status"))) {
                //返回发票请求流水号
                return info.getString("fpqqlsh");
            }
        } catch (Exception e) {
            log.error("发票请求失败");
            e.printStackTrace();
        } finally {
            //关闭连接，释放资源
            post.releaseConnection();
            ((SimpleHttpConnectionManager) httpclient.getHttpConnectionManager()).shutdown();
        }
        return "failure";
    }

    /**
     * 获取发票库存数量
     */
    public static String countSend(String url, String order) throws Exception {
        //order为密文 需要加密
        order = DESDZFP.encrypt(order);
        HttpClient httpclient = null;
        PostMethod post = null;
        try {
            httpclient = new HttpClient();
            post = new PostMethod(url);
            //设置编码方式
            post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            //添加参数
            post.addParameter("order", order);
            //执行
            httpclient.executeMethod(post);
            //接口返回信息
            String res = new String(post.getResponseBody(), "UTF-8");
            log.info("请求发送结果：{}", res);
            JSONObject info = JSONObject.parseObject(res);
            if ("0000".equals(info.getString("status"))) {
                JSONArray jan = (JSONArray) info.get("list");
                if (jan != null || jan.size() > 0) {
                    for (int i = 0; i < jan.size(); i++) {
                        JSONObject jo = jan.getJSONObject(i);
                        if ("p".equals(jo.getString("invoiceLine"))) {
                            return jo.getString("remainNum");
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("发票请求失败");
            e.printStackTrace();
        } finally {
            //关闭连接，释放资源
            post.releaseConnection();
            ((SimpleHttpConnectionManager) httpclient.getHttpConnectionManager()).shutdown();
        }
        return "0";
    }

    public static ByteArrayOutputStream parse(final InputStream in) throws Exception {
        final ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        int ch;
        while ((ch = in.read()) != -1) {
            swapStream.write(ch);
        }
        return swapStream;
    }

}
