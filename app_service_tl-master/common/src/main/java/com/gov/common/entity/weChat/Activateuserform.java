package com.gov.common.entity.weChat;

import lombok.Data;

import java.util.List;

@Data
public class Activateuserform {

    private boolean can_modify;

    private List<String> common_field_id_list;

    private List<String> custom_field_list;
}
