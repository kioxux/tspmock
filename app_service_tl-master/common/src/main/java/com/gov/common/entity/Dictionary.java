package com.gov.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
public class Dictionary {

    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;

}
