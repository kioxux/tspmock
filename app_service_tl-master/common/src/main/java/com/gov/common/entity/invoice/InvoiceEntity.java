package com.gov.common.entity.invoice;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.27
 */
@Data
public class InvoiceEntity {

    private String orderId;

    /**
     * 购方企业名称
     */
    private String buyername;

    /**
     * 购方企业税号
     */
    private String taxnum;

    private String address;
    private String account;
    private String telephone;
    private String orderno;
    private String callbackurl;
    private String invoicedate;
    private String saletaxnum;
    private String saleaccount;
    private String salephone;
    private String saleaddress;
    private String kptype;
    private String message;
    private String clerk;
    private String payee;
    private String checker;
    private String fpdm;
    private String fphm;
    private String tsfs;
    private String email;
    private String phone;
    private String qdbz;
    private String qdxmmc;
    private String dkbz;
    private String deptid;
    private String clerkid;
    private String invoiceLine;
    private String cpybz;
    private String billInfoNo;
    private List<InvoiceDetail> detail;

    @Data
    public class InvoiceDetail {
        private String goodsname;
        private String num;
        private String price;
        private String hsbz;
        private String taxrate;
        private String spec;
        private String unit;
        private String spbm;
        private String zxbm;
        private String fphxz;
        private String yhzcbs;
        private String zzstsgl;
        private String lslbs;
        private String kce;
        private String taxfreeamt;
        private String tax;
        private String taxamt;
    }
}

