package com.gov.common.basic;

import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.SmsEntity;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.19
 */
@Slf4j
@Component
public class SmsUtils {

    public final static String SGIN = "【{}】";

    public final static String EXT = "回复TD退订。";

    public static final String SMS0000010 = "SMS0000010";


    @Resource
    ApplicationConfig applicationConfig;


    /**
     * 营销短信发送
     */
    public Result sendMarketingSms(SmsEntity smsEntity) {
        return this.sendSms(smsEntity, 0);
    }

    /**
     * 营销短信发送
     */
    public Result sendSms(SmsEntity smsEntity) {
        return this.sendSms(smsEntity, 1);
    }

    /**
     * 营销模板测试发送
     */
    public Result testMarketingTemplate(SmsEntity smsEntity) {
        return this.sendSms(smsEntity, 0);
    }

    /**
     * 获取登录短信验证码
     */
    public Result sendSmsToGetVerification(SmsEntity smsEntity) {
        return this.sendSms(smsEntity, 1);
    }

    /**
     * 新增/修改店长发生注册短信
     */
    public Result sendStoreRegisterSms(SmsEntity smsEntity) {
        return this.sendSms(smsEntity, 1);
    }

    /**
     * 发送短信
     * type 0:营销，
     */
    private Result sendSms(SmsEntity smsEntity, int type) {
        log.info(" 发送短信参数:{},type:{}", JsonUtils.beanToJson(smsEntity), type);
        HttpClient httpclient = new HttpClient();
        PostMethod post = new PostMethod(applicationConfig.getSmsurl());//
        post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "gbk");
        if (type == 0) {
            post.addParameter("SpCode", applicationConfig.getMarketingSmsspcode());
            post.addParameter("LoginName", applicationConfig.getMarketingSmsusername());
            post.addParameter("Password", applicationConfig.getMarketingSmspassword());
        } else {
            post.addParameter("SpCode", applicationConfig.getSmsspcode());
            post.addParameter("LoginName", applicationConfig.getSmsusername());
            post.addParameter("Password", applicationConfig.getSmspassword());
        }
        post.addParameter("SpCode", applicationConfig.getSmsspcode());
        post.addParameter("LoginName", applicationConfig.getSmsusername());
        post.addParameter("Password", applicationConfig.getSmspassword());
        post.addParameter("MessageContent", smsEntity.getMsg());
        post.addParameter("UserNumber", smsEntity.getPhone());
        post.addParameter("SerialNumber", UUIDUtil.getNumUUID());
        post.addParameter("ScheduleTime", "");
        String info = null;
        try {
//            log.info("短信发送参数：{}", JsonUtils.beanToJson(post));
            httpclient.executeMethod(post);
            info = new String(post.getResponseBody(), "gbk");
            log.info("短信发送结果：{}", info);
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0004);
        }
        if (null != info) {
            Map<String, String> infoMap = this.getResultParams(info);
            log.info(JsonUtils.beanToJson(infoMap));
            if ("0".equals(infoMap.get("result"))) {
                log.info("发送成功");
                return ResultUtil.success();
            } else if ("32".equals(infoMap.get("result"))) {
                log.info("超过次数");
                Result result = ResultUtil.error(ResultEnum.E0004);
                result.setMessage("您今日短信发送次数过多，请明日再试");
                return result;
            } else {
                log.info("失败");
                return ResultUtil.error(ResultEnum.E0004);
            }
        }
        return ResultUtil.error(ResultEnum.E0004);
    }

    /**
     * 解析返回参数
     */
    private Map getResultParams(String result) {
        Map<String, String> map = new HashMap<>();
        if (result.split("&").length > 0) {
            String[] arr = result.split("&");
            for (String s : arr) {
                String[] items = s.split("=");
                if (items.length > 1) {
                    String key = s.split("=")[0];
                    String value = s.split("=")[1];
                    map.put(key, value);
                } else if (items.length == 1) {
                    map.put(s.split("=")[0], "");
                }
            }
            return map;
        } else {
            return map;
        }
    }

}
