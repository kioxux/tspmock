package com.gov.common.entity.weChat;

import lombok.Data;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.07.07
 */
@Data
public class SendTempMessage {

    /**
     * 用户openId
     */
    private String touser;

    /**
     * 消息模板ID
     */
    private String template_id;

    /**
     * 消息参数
     */
    private Map<ParamValueKey, SendTempMessageParams> data;

    public enum MessageId {
        PURCHASE_REMIND("ncTKmoi5y4YIDsb0t12WswZq0eGwX_8QnzT9tiOxg_s");

        MessageId(String template_id) {
            this.template_id = template_id;
        }

        public String getTemplate_id() {
            return template_id;
        }

        public void setTemplate_id(String template_id) {
            this.template_id = template_id;
        }

        private String template_id;
    }

    /**
     * 消息key
     */
    public enum ParamValueKey {
        first,
        remark,
        keyword1,
        keyword2;
    }
}
