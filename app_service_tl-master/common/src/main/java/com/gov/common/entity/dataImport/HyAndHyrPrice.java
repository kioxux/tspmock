package com.gov.common.entity.dataImport;

import com.gov.common.basic.CommonEnum;
import com.gov.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Description:
 *
 * @author: zp
 * @date: 2021.04.06
 */
@Data
public class HyAndHyrPrice {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 20)
    private String proSelfCode;

    /**
     * 促销价
     */
    @ExcelValidate(index = 1, name = "促销价", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gsphsPrice;

    /**
     * 促销折扣
     */
    @ExcelValidate(index = 2, name = "促销折扣", type = ExcelValidate.DataType.STRING, required = false)
    private String gsphsRebate;

    /**
     * 是否积分
     */
    @ExcelValidate(index = 3, name = "是否积分", type = ExcelValidate.DataType.STRING, required = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.IS_OR_NO, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String gsphsInteFlag;

    /**
     * 积分倍率
     */
    @ExcelValidate(index = 4, name = "积分倍率", type = ExcelValidate.DataType.INTEGER, required = false)
    private String gsphsInteRate;

}
