package com.gov.common.entity.weChat;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class BatchgetMaterialRes {


    /**
     * item : [{"media_id":"DDrkRS_gXW9hobspX3fcmXmkBty8pqXaCbEkYpM2a8I","content":{"news_item":[{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>111<\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=1&sn=a0e34096a512dcbc5db031fc51de6b6c&chksm=1731a9632046207578313cfad5ffe6e0d9f4904d003e919873c6bf0c209eea436ab052d9b10d#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>222<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=2&sn=f3e3d4fc60b46513e4f27af48efead0a&chksm=1731a963204620750236983708f6f16229666100a047ece2e76a09d5d3ddecd70325a309450f#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>333<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=3&sn=3a9dbfaaeaa9fe07033acc659778cfb0&chksm=1731a96320462075757f13ac7cb275f103933e8580f3e1c525200208256ec15dc3eff3a50703#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"11111","author":"222","digest":"5523123","content":"<p>444444<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":0,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=4&sn=d79b2c2682868f1c5455774c6cc33aea&chksm=1731a9632046207547de8334c4f508768978b693c61cf6a6c18537c43c22a8143bf1acfd5ba4#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vPV7BkNwAksGP6nxZABZI6w6icktlA0a20xYNWm2jhpnuiaicW4T1ibic0hYYndwxicO7BTPickqMiccGypQ/0?wx_fmt=jpeg","need_open_comment":1,"only_fans_can_comment":0}],"create_time":1608281888,"update_time":1609218864},"update_time":1609218864}]
     * total_count : 1
     * item_count : 1
     */

    private Integer total_count;
    private Integer item_count;
    private List<ItemBean> item;

    @NoArgsConstructor
    @Data
    public static class ItemBean {

        /**
         * 总的客户数
         */
        private Integer totalCount;
        /**
         * 已经推送的客户数
         */
        private Integer pushCount;

        /**
         * media_id : DDrkRS_gXW9hobspX3fcmXmkBty8pqXaCbEkYpM2a8I
         * content : {"news_item":[{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>111<\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=1&sn=a0e34096a512dcbc5db031fc51de6b6c&chksm=1731a9632046207578313cfad5ffe6e0d9f4904d003e919873c6bf0c209eea436ab052d9b10d#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>222<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=2&sn=f3e3d4fc60b46513e4f27af48efead0a&chksm=1731a963204620750236983708f6f16229666100a047ece2e76a09d5d3ddecd70325a309450f#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>333<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=3&sn=3a9dbfaaeaa9fe07033acc659778cfb0&chksm=1731a96320462075757f13ac7cb275f103933e8580f3e1c525200208256ec15dc3eff3a50703#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"11111","author":"222","digest":"5523123","content":"<p>444444<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":0,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=4&sn=d79b2c2682868f1c5455774c6cc33aea&chksm=1731a9632046207547de8334c4f508768978b693c61cf6a6c18537c43c22a8143bf1acfd5ba4#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vPV7BkNwAksGP6nxZABZI6w6icktlA0a20xYNWm2jhpnuiaicW4T1ibic0hYYndwxicO7BTPickqMiccGypQ/0?wx_fmt=jpeg","need_open_comment":1,"only_fans_can_comment":0}],"create_time":1608281888,"update_time":1609218864}
         * update_time : 1609218864
         */
        private String article_id;
        private String media_id;
        public String getMedia_id() {
            return media_id == null ? getArticle_id() : media_id;
        }
        private ContentBean content;
        private Integer update_time;

        @NoArgsConstructor
        @Data
        public static class ContentBean {
            /**
             * news_item : [{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>111<\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=1&sn=a0e34096a512dcbc5db031fc51de6b6c&chksm=1731a9632046207578313cfad5ffe6e0d9f4904d003e919873c6bf0c209eea436ab052d9b10d#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>222<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=2&sn=f3e3d4fc60b46513e4f27af48efead0a&chksm=1731a963204620750236983708f6f16229666100a047ece2e76a09d5d3ddecd70325a309450f#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"测试新闻哈哈哈","author":"沈大侠","digest":"来看看我的新推文摘要","content":"<p>我们都有一个家，名字叫\u201c中国\u201d。<\/p><p>333<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":1,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=3&sn=3a9dbfaaeaa9fe07033acc659778cfb0&chksm=1731a96320462075757f13ac7cb275f103933e8580f3e1c525200208256ec15dc3eff3a50703#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg","need_open_comment":0,"only_fans_can_comment":0},{"title":"11111","author":"222","digest":"5523123","content":"<p>444444<br  /><\/p>","content_source_url":"","thumb_media_id":"DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A","show_cover_pic":0,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=4&sn=d79b2c2682868f1c5455774c6cc33aea&chksm=1731a9632046207547de8334c4f508768978b693c61cf6a6c18537c43c22a8143bf1acfd5ba4#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vPV7BkNwAksGP6nxZABZI6w6icktlA0a20xYNWm2jhpnuiaicW4T1ibic0hYYndwxicO7BTPickqMiccGypQ/0?wx_fmt=jpeg","need_open_comment":1,"only_fans_can_comment":0}]
             * create_time : 1608281888
             * update_time : 1609218864
             */

            private Integer create_time;
            private Integer update_time;
            private List<NewsItemBean> news_item;

            @NoArgsConstructor
            @Data
            public static class NewsItemBean {
                /**
                 * title : 测试新闻哈哈哈
                 * author : 沈大侠
                 * digest : 来看看我的新推文摘要
                 * content : <p>我们都有一个家，名字叫“中国”。</p><p>111</p>
                 * content_source_url :
                 * thumb_media_id : DDrkRS_gXW9hobspX3fcmTlzS506C6gNgFexaqMX11A
                 * show_cover_pic : 1
                 * url : http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000022&idx=1&sn=a0e34096a512dcbc5db031fc51de6b6c&chksm=1731a9632046207578313cfad5ffe6e0d9f4904d003e919873c6bf0c209eea436ab052d9b10d#rd
                 * thumb_url : http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4tnZC4IKVcvlhPnZ4H2Qvml3E0lM9ibEcibxbvtgVkaVlhsDEa9NCQg4riaLdcb34EscIO1ppt2iacaDg/0?wx_fmt=jpeg
                 * need_open_comment : 0
                 * only_fans_can_comment : 0
                 */

                private String title;
                private String author;
                private String digest;
                private String content;
                private String content_source_url;
                private String thumb_media_id;
                private Integer show_cover_pic;
                private String url;
                private String thumb_url;
                private Integer need_open_comment;
                private Integer only_fans_can_comment;
            }
        }
    }
}
