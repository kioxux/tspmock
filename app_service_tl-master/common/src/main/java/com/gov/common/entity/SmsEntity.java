package com.gov.common.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.19
 */
@Data
public class SmsEntity {
    /**
     * 模板ID
     */
    private String smsId;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 消息
     */
    private String msg;
}
