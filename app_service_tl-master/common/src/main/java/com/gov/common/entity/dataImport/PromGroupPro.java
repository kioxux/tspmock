package com.gov.common.entity.dataImport;

import com.gov.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.02
 */
@Data
public class PromGroupPro {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proSelfCode;
}

