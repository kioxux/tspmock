package com.gov.common.entity.weChat;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class MediaIdResDTO {


    /**
     * news_item : [{"title":"XXX新增一条","author":"","digest":"新增一条","content":"<p>新增一条<br  /><\/p><p style=\"text-align: center;\"><img class=\"rich_pages js_insertlocalimg\" data-ratio=\"1\" data-s=\"300,640\" data-src=\"https://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vDPCV3N1C4ID9mGM1uZibfFuJib7eKO9bQTJKd0Ju8rWx92fF0icIezqQk23LE3upmrhMEL5dTZTWzw/640?wx_fmt=jpeg\" data-type=\"jpeg\" data-w=\"500\" style=\"\"  /><\/p><p><br  /><\/p>","content_source_url":"","thumb_media_id":"","show_cover_pic":0,"url":"http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000034&idx=1&sn=ac32f8b411922653c9e1b3ecdeaa1ca7&chksm=1731a9572046204189a85b92e0557a64094d0cfde969b69b8a52b53537fac0a1159436abe84c#rd","thumb_url":"http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vDPCV3N1C4ID9mGM1uZibfFFsbpqyJ24peJibpUQCoQibyynsaxS7H7YKdICnHJwnSVcIBUbtRm3rWA/0?wx_fmt=jpeg","need_open_comment":1,"only_fans_can_comment":0}]
     * create_time : 1609921242
     * update_time : 1609922058
     */

    private int create_time;
    private int update_time;
    private List<NewsItemBean> news_item;

    @NoArgsConstructor
    @Data
    public static class NewsItemBean {
        /**
         * title : XXX新增一条
         * author :
         * digest : 新增一条
         * content : <p>新增一条<br  /></p><p style="text-align: center;"><img class="rich_pages js_insertlocalimg" data-ratio="1" data-s="300,640" data-src="https://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vDPCV3N1C4ID9mGM1uZibfFuJib7eKO9bQTJKd0Ju8rWx92fF0icIezqQk23LE3upmrhMEL5dTZTWzw/640?wx_fmt=jpeg" data-type="jpeg" data-w="500" style=""  /></p><p><br  /></p>
         * content_source_url :
         * thumb_media_id :
         * show_cover_pic : 0
         * url : http://mp.weixin.qq.com/s?__biz=MzIwNTQzOTI1OA==&mid=100000034&idx=1&sn=ac32f8b411922653c9e1b3ecdeaa1ca7&chksm=1731a9572046204189a85b92e0557a64094d0cfde969b69b8a52b53537fac0a1159436abe84c#rd
         * thumb_url : http://mmbiz.qpic.cn/mmbiz_jpg/pc5JMd79m4vDPCV3N1C4ID9mGM1uZibfFFsbpqyJ24peJibpUQCoQibyynsaxS7H7YKdICnHJwnSVcIBUbtRm3rWA/0?wx_fmt=jpeg
         * need_open_comment : 1
         * only_fans_can_comment : 0
         */

        private String title;
        private String author;
        private String digest;
        private String content;
        private String content_source_url;
        private String thumb_media_id;
        private int show_cover_pic;
        private String url;
        private String thumb_url;
        private int need_open_comment;
        private int only_fans_can_comment;
    }
}
