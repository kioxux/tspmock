package com.gov.common.entity.weChat;

import com.gov.common.entity.Dictionary;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.07.07
 */
@Data
public class SendTempMessageParams {

    public SendTempMessageParams(String value, String color) {
        this.value = value;
        this.color = color;
    }

    /**
     * 消息参数
     */
    private String value;

    /**
     * 颜色
     */
    private String color;

}
