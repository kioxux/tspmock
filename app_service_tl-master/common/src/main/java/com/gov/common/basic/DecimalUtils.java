package com.gov.common.basic;

import java.math.BigDecimal;

public class DecimalUtils {

    /**
     * 小数转化为百分数
     */
    public static BigDecimal toPercent(BigDecimal value) {
        BigDecimal multiply = value.multiply(Constants.ONE_HUNDRED);
        String decimalStr = multiply.stripTrailingZeros().toPlainString();
        return new BigDecimal(decimalStr);
    }

    /**
     * 小数转化为百分数
     */
    public static String toPercentStr(BigDecimal value) {
        BigDecimal multiply = value.multiply(Constants.ONE_HUNDRED);
        String decimalStr = multiply.stripTrailingZeros().toPlainString();
        return decimalStr + "%";
    }

    /**
     * 百分数转化为小数
     */
    public static BigDecimal toDecimal(BigDecimal value) {
        return value.divide(Constants.ONE_HUNDRED, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal stripTrailingZeros(BigDecimal num) {
        num = num == null ? BigDecimal.ZERO : num;
        return num.stripTrailingZeros();
    }

    /**
     * 数值相加
     *
     * @param decimals
     * @return
     */
    public static BigDecimal add(BigDecimal... decimals) {
        if (decimals == null || decimals.length == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal decimal : decimals) {
            if (decimal != null) {
                result = result.add(decimal);
            }
        }
        return result;
    }

    /**
     * 数值相减
     *
     * @param bigDecimal1
     * @param bigDecimal2
     * @return
     */
    public static BigDecimal subtract(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
        if (bigDecimal1 == null) {
            bigDecimal1 = BigDecimal.ZERO;
        }
        if (bigDecimal2 == null) {
            bigDecimal2 = BigDecimal.ZERO;
        }
        return bigDecimal1.subtract(bigDecimal2);
    }
}
