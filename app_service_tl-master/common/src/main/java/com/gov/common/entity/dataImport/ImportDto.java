package com.gov.common.entity.dataImport;

import lombok.Data;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class ImportDto {

    private String path;

    private String type;

    private Map<String, Object> dataMap;
}

