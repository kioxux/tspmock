package com.gov.common.entity.dataImport;

import com.gov.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class MarketingComponent {

    /**
     * 商品成分
     */
    @ExcelValidate(index = 0, name = "成分编码", type = ExcelValidate.DataType.STRING)
    private String component;

}

