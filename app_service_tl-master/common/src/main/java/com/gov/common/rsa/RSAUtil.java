package com.gov.common.rsa;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class RSAUtil {
    //私钥
    public static String privateKey  ="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALiz8gxsQQ1at9X7fSr93F+u7QezKxdtBWLzIm68+OdFc7yKFhCk3SJd7XeLiqs5Z49d9Moei7Oik4jE4CbjLm1+LfrSks90pzfNwQVmEcLCxKOC9+1dNwTpPhijdpjoVzBc+ewji0q4h+i16tWIzEMCqsam3UjChHWaOzT0l00rAgMBAAECgYBcZG8s9a3ozztsh9vJ+KkXF/qrwl1I35k1MShFOrRLg/tsEasN8lpHybJy/VOFaTa7iaVcwcDiP+4LGWiDK1taQo2axpeb9UU2yB9YScBB5U1NFRelPdy3OlbrsOH9kH2UwewRQQ9v8HcVDI1A1g9Fjy3bf8T9Ez+tt+ISBJUkAQJBAN46dJY8FGSh/P4el6iFdr311mbtP1iDvCBCWBY9vpL6DeUI4ZKElCZNwaJvz2+/NVYCxm9vAACXhYRgYHgomysCQQDUxZr1hEKgX/gSyE57BQCUh5CwVdiKs6qPajOMI53tewR4c1TyDfW5BZA15GL5ioe7c28FMTI6F1kual/cyxYBAkBrGr1KExa5EaF/UHtDi1+ZWjzFSp73KDapTZBq18NbzwyXUFp3ZVKJy3wBr+XIM92e4KOG3Xag0mZVUKXuA5QVAkEAs8EJzBfjzeemBgAxxV6RgeoRw9eSrINjOewoBmHkv0fpm/Ubs681hA1+Kvl64IU3bJb2GTXZ0Bf/oOX/fOIaAQJBAIuZsGOXiEZ1SONzEqkJh8NBIr1B64M9hNTMffPEnZ0HtG9/Rr4Axc/SNamiHV/seQj5p3Bw0to4tZFAjznNvOI=";

    private static final String RSA = "RSA/ECB/OAEPWithMD5AndMGF1Padding";

    private static final String PUBLIC_KEY = "publicKey";

    private static final String PRICE_KEY = "privateKey";

    /**
     * 加密算法RSA
     */
    public static final String KEY_ALGORITHM = "RSA";
    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;
    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    /**
     * 生成公钥和私钥的方法
     *
     * @param args
     */
    public static void main(String[] args) {
        Map<String, String> keyMap = genKeyPair();
        String content = "gysgov";
//        System.out.println("公钥为:" + keyMap.get(PUBLIC_KEY));
//        String enResult = rsaEncrypt(content, keyMap.get(PUBLIC_KEY));
//        System.out.println(enResult);
//        System.out.println("私钥为:" + keyMap.get(PRICE_KEY));
//        String deResult = rsaDecrypt(enResult, keyMap.get(PRICE_KEY));
//        System.out.println(deResult);


        System.out.println("公钥为:" + keyMap.get(PUBLIC_KEY));
        System.out.println("----------------");
        String enResult = encryptByPublicKey(content, keyMap.get(PUBLIC_KEY));
        System.out.println(enResult);
        System.out.println("----------------");
        System.out.println("私钥为:" + keyMap.get(PRICE_KEY));
        System.out.println("----------------");
        String deResult = decryptByPrivateKey(enResult, keyMap.get(PRICE_KEY));
        System.out.println(deResult);
    }


    public static Map<String, String> genKeyPair() {
        try {
            KeyPairGenerator genKeyPair = KeyPairGenerator.getInstance("RSA");
            genKeyPair.initialize(2048, new SecureRandom());
            KeyPair keyPair = genKeyPair.generateKeyPair();
            Map<String, String> keyMap = new HashMap<>();
            PrivateKey privateKey = keyPair.getPrivate();
            keyMap.put(PRICE_KEY, Base64.getEncoder().encodeToString(privateKey.getEncoded()));
            PublicKey publicKey = keyPair.getPublic();
            keyMap.put(PUBLIC_KEY, Base64.getEncoder().encodeToString(publicKey.getEncoded()));
            return keyMap;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 使用公钥加密
     */
    public static String rsaEncrypt(String content, String publicKey) {
        try {
            Cipher cipher = Cipher.getInstance(RSA);
            byte[] pubBytes = Base64.getDecoder().decode(publicKey);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pubBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey1 = keyFactory.generatePublic(keySpec);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey1);
            byte[] bytesResult = cipher.doFinal(content.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(bytesResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 私钥解密
     */
    public static String rsaDecrypt(String content, String privateKey) {
        try {
            byte[] priBytes = Base64.getDecoder().decode(privateKey);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(priBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey1 = keyFactory.generatePrivate(keySpec);
            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.DECRYPT_MODE, privateKey1);
            byte[] bytes = cipher.doFinal(Base64.getDecoder().decode(content));
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 使用公钥加密
     */
    public static String encryptByPublicKey(String data, String publicKey) {
        // 加密
        String str = "";
        try {
            byte[] pubByte = base64decode(publicKey.trim());
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pubByte);
            KeyFactory fac = KeyFactory.getInstance(KEY_ALGORITHM);
            RSAPublicKey rsaPubKey = (RSAPublicKey) fac.generatePublic(keySpec);

            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.ENCRYPT_MODE, rsaPubKey);
            int inputLen = data.getBytes().length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            while (inputLen - offSet > 0) {
                //MAX_ENCRYPT_BLOCK 117
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data.getBytes(), offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data.getBytes(), offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();

            str = base64encode(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * 使用私钥解密
     */
    public static String decryptByPrivateKey(String content, String privateKey) {
        // 加密
        String str = "";
        try {
            byte[] priByte = base64decode(privateKey.trim());
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(priByte);
            KeyFactory fac = KeyFactory.getInstance(KEY_ALGORITHM);
            RSAPrivateKey rsaPriKey = (RSAPrivateKey) fac.generatePrivate(keySpec);

            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.DECRYPT_MODE, rsaPriKey);

            byte[] dataBytes = content.getBytes(StandardCharsets.UTF_8);
            int inputLen = dataBytes.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offset = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offset > 0) {
                if (inputLen - offset > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(dataBytes, offset, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(dataBytes, offset, inputLen - offset);
                }
                out.write(cache, 0, cache.length);
                i++;
                offset = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            // 解密后的内容
            str = new String(decryptedData);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return str;
    }

    /**
     * base64加密
     */
    @SuppressWarnings("restriction")
    private static String base64encode(byte[] bytes) {
        String str = new sun.misc.BASE64Encoder().encode(bytes);
        str = str.replaceAll("rn", "").replaceAll("r", "").replaceAll("n", "");
        return str;
    }

    /**
     * base64解密
     */
    @SuppressWarnings("restriction")
    private static byte[] base64decode(String str) {
        byte[] bt = null;
        try {
            sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
            bt = decoder.decodeBuffer(str);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bt;
    }


}
