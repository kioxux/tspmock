package com.gov.common.entity.weChat;

import lombok.Data;

@Data
public class News {


    /**
     * title : 1
     * thumb_media_id : 2
     * author : 3
     * digest : 4
     * show_cover_pic : 0
     * content :
     * content_source_url :
     * need_open_comment : 1
     * only_fans_can_comment : 1
     */

    private String title;

    private String thumb_media_id;

    private String author;

    private String digest;

    private Integer show_cover_pic;

    private String content;

    private String content_source_url;

    private Integer need_open_comment;

    private Integer only_fans_can_comment;


}
