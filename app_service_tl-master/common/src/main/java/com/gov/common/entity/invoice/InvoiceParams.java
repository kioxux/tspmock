package com.gov.common.entity.invoice;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.27
 */
@Data
public class InvoiceParams {
    /**
     * 身份认证，在诺诺网备案后，由诺诺网提供，每个企业一个
     */
    private String identity;

    /**
     * 订单明细
     */
    private InvoiceEntity order;
}
