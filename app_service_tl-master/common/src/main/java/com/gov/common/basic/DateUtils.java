package com.gov.common.basic;

import com.gov.common.response.CustomResultException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.concurrent.CompletionException;

public class DateUtils {

    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
    public static final DateTimeFormatter TIME_FORMATTER_HHMMSS = DateTimeFormatter.ofPattern("HHmmss");
    public static final DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter DATETIME_FORMATTER_TWO = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter DATETIME_FORMATTER_FULL = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    public static final DateTimeFormatter DATE_FORMATTER_YYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static final DateTimeFormatter YEAR_FORMATTER = DateTimeFormatter.ofPattern("yyyy");
    public static final DateTimeFormatter YEAR_MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyyMM");
    public static final DateTimeFormatter HOUR_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHH");

    public static void main(String[] args) {
//        LocalDate currentYear =  LocalDate.now(); // 今年
//        LocalDate lastYear = LocalDate.now().plusYears(-1); // 去年同期
     /*   LocalDate currentYear =  LocalDate.of(2021,2,2); // 今年
        LocalDate lastYear =LocalDate.of(2021,2,2).plusYears(-1); // 去年同期
        String startDate ="";
        String endDate ="";
        String setLastWeekStartDate="";
        String setLastWeekEndDate="";

            startDate = DateUtils.getFirstDayOfWeek(currentYear);
            endDate = DateUtils.getLastDayOfWeek(currentYear);
            setLastWeekStartDate = DateUtils.getFirstDayOfWeek(lastYear);
            setLastWeekEndDate = DateUtils.getLastDayOfWeek(lastYear);

        System.out.println(startDate);
        System.out.println(endDate);
        System.out.println(setLastWeekStartDate);
        System.out.println(setLastWeekEndDate);

        startDate = DateUtils.getFirstDayOfYear(currentYear);
            endDate = DateUtils.getLastDayOfYear(currentYear);
            setLastWeekStartDate = DateUtils.getFirstDayOfYear(lastYear);
            setLastWeekEndDate = DateUtils.getLastDayOfYear(lastYear);
        System.out.println(startDate);
        System.out.println(endDate);
        System.out.println(setLastWeekStartDate);
        System.out.println(setLastWeekEndDate);*/

        System.out.println(getCurrentDateTimeStrTwo());
    }

    /**
     *
     * 返回当前的日期DATE_FORMATTER
     *
     * @return
     */
    public static LocalDate getCurrentLocalDate() {
        return LocalDate.now();
    }

    /**
     * 返回当前时间
     *
     * @return
     */
    public static LocalTime getCurrentLocalTime() {
        return LocalTime.now();
    }

    /**
     * 返回当前日期时间
     *
     * @return
     */
    public static LocalDateTime getCurrentLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 当前时间字符串（yyyy-MM-dd）
     *
     * @return
     */
    public static String getCurrentDateStr() {
        return LocalDate.now().format(DATE_FORMATTER);
    }

    /**
     * 当前日期字符串（yyyyMMdd）
     * @return
     */
    public static String getCurrentDateStrYYMMDD() {
        return LocalDate.now().format(DATE_FORMATTER_YYMMDD);
    }

    /**
     * 昨天日期字符串（yyyyMMdd）
     * @return
     */
    public static String getBeforeDateStrYYMMDD() {
        return LocalDate.now().plusDays(-1).format(DateUtils.DATE_FORMATTER_YYMMDD);
    }


    /**
     * 当前时间字符串（yyyy-MM）
     *
     * @return
     */
    public static String getCurrentMonthStr() {
        return LocalDate.now().format(MONTH_FORMATTER);
    }

    /**
     * 当前时间字符串（yyyyMM）
     *
     * @return
     */
    public static String getCurrentYearMonthStr() {
        return LocalDate.now().format(YEAR_MONTH_FORMATTER);
    }

    /**
     * 当前时间字符串（yyyy）
     *
     * @return
     */
    public static String getCurrentYearStr() {
        return LocalDate.now().format(YEAR_FORMATTER);
    }

    /**
     * 当前时间字符串（yyyy-MM-dd HH:mm:ss）
     *
     * @return
     */
    public static String getCurrentDateTimeStr() {
        return LocalDateTime.now().format(DATETIME_FORMATTER);
    }


    /**
     * 当前时间字符串（yyyyMMddHHmmss）
     *
     * @return
     */
    public static String getCurrentDateTimeStrTwo() {
        return LocalDateTime.now().format(DATETIME_FORMATTER_TWO);
    }

    /**
     * 当前时间字符串（yyyyMMddHHmmssSSS）
     *
     * @return
     */
    public static String getCurrentDateTimeStrFull() {
        return LocalDateTime.now().format(DATETIME_FORMATTER_FULL);
    }

    /**
     * 当前时间字符串（HH:mm:ss）
     *
     * @return
     */
    public static String getCurrentTimeStr() {
        return LocalTime.now().format(TIME_FORMATTER);
    }

    /**
     * 当前时间字符串（HHmmss）
     *
     * @return
     */
    public static String getCurrentTimeStrHHMMSS() {
        return LocalTime.now().format(TIME_FORMATTER_HHMMSS);
    }

    /**
     * 自定义格式获取当前时间(yyyy-MM-dd)
     *
     * @param pattern 格式
     * @return
     */
    public static String getCurrentDateStr(String pattern) {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param pattern
     * @return
     */
    public static String getCurrentDateTimeStr(String pattern) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param pattern
     * @return
     */
    public static String getCurrentTimeStr(String pattern) {
        return LocalTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param dateStr
     * @param pattern
     * @return
     */
    public static LocalDate parseLocalDate(String dateStr, String pattern) {
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param dateTimeStr
     * @param pattern
     * @return
     */
    public static LocalDateTime parseLocalDateTime(String dateTimeStr, String pattern) {
        return LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param timeStr
     * @param pattern
     * @return
     */
    public static LocalTime parseLocalTime(String timeStr, String pattern) {
        return LocalTime.parse(timeStr, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param date
     * @param pattern
     * @return
     */
    public static String formatLocalDate(LocalDate date, String pattern) {
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param datetime
     * @param pattern
     * @return
     */
    public static String formatLocalDateTime(LocalDateTime datetime, String pattern) {
        return datetime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param time
     * @param pattern
     * @return
     */
    public static String formatLocalTime(LocalTime time, String pattern) {
        return time.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param dateStr
     * @return
     */
    public static LocalDate parseLocalDate(String dateStr) {
        return LocalDate.parse(dateStr, DATE_FORMATTER);
    }

    /**
     * @param dateStr
     * @return yyyyMMdd
     */
    public static LocalDate parseDate(String dateStr) {
        return LocalDate.parse(dateStr, DATE_FORMATTER_YYMMDD);
    }

    /**
     * @param dateTimeStr
     * @return
     */
    public static LocalDateTime parseLocalDateTime(String dateTimeStr) {
        return LocalDateTime.parse(dateTimeStr, DATETIME_FORMATTER);
    }

    /**
     * @param dateTimeStr
     * @return
     */
    public static LocalDateTime getLocalDateTime(String dateTimeStr) {
        return LocalDateTime.parse(dateTimeStr, YEAR_MONTH_FORMATTER);
    }


    /**
     * @param timeStr
     * @return
     */
    public static LocalTime parseLocalTime(String timeStr) {
        return LocalTime.parse(timeStr, TIME_FORMATTER);
    }

    /**
     * @param date
     * @return
     */
    public static String formatLocalDate(LocalDate date) {
        return date.format(DATE_FORMATTER);
    }

    /**
     * @param datetime
     * @return
     */
    public static String formatLocalDateTime(LocalDateTime datetime) {
        return datetime.format(DATETIME_FORMATTER);
    }

    /**
     * @param time
     * @return
     */
    public static String formatLocalTime(LocalTime time) {
        return time.format(TIME_FORMATTER);
    }

    /**
     * 日期相隔天数
     *
     * @param startDateInclusive
     * @param endDateExclusive
     * @return
     */
    public static long periodDays(LocalDate startDateInclusive, LocalDate endDateExclusive) {
        return endDateExclusive.toEpochDay() - startDateInclusive.toEpochDay();
    }

    /**
     * 日期相隔小时
     *
     * @param startInclusive
     * @param endExclusive
     * @return
     */
    public static long durationHours(Temporal startInclusive, Temporal endExclusive) {
        return Duration.between(startInclusive, endExclusive).toHours();
    }

    /**
     * 日期相隔分钟
     *
     * @param startInclusive
     * @param endExclusive
     * @return
     */
    public static long durationMinutes(Temporal startInclusive, Temporal endExclusive) {
        return Duration.between(startInclusive, endExclusive).toMinutes();
    }

    /**
     * 日期相隔毫秒数
     *
     * @param startInclusive
     * @param endExclusive
     * @return
     */
    public static long durationMillis(Temporal startInclusive, Temporal endExclusive) {
        return Duration.between(startInclusive, endExclusive).toMillis();
    }

    /**
     * 是否当天
     *
     * @param date
     * @return
     */
    public static boolean isToday(LocalDate date) {
        return getCurrentLocalDate().equals(date);
    }

    /**
     * 获取此日期时间与默认时区<Asia/Shanghai>组合的时间毫秒数
     *
     * @param dateTime
     * @return
     */
    public static Long toEpochMilli(LocalDateTime dateTime) {
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 获取此日期时间与指定时区组合的时间毫秒数
     *
     * @param dateTime
     * @return
     */
    public static Long toSelectEpochMilli(LocalDateTime dateTime, ZoneId zoneId) {
        return dateTime.atZone(zoneId).toInstant().toEpochMilli();
    }

    /**
     * 两个时间比较
     *
     * @param dateBefore
     * @param dateAfter
     * @return
     */
    public static boolean compareTime(LocalDate dateBefore, LocalDate dateAfter) {
        return dateBefore.isAfter(dateAfter);
    }

    public static boolean isBefore(String before, LocalDate after) {
        LocalDate beforeDate = LocalDate.parse(before, DATE_FORMATTER_YYMMDD);
        return beforeDate.isBefore(after);
    }

    /**
     * 两个时间比较
     */
    public static boolean compareTime(LocalDate dateBefore, String dateAfter) {
        LocalDate dateAfterTemp = LocalDate.parse(dateAfter, DATE_FORMATTER_YYMMDD);
        return dateBefore.isBefore(dateAfterTemp);
    }

    /**
     * 指定日期加指定天数
     *
     * @return
     */
    public static Date addDate(Date date, long day) {
        long time = date.getTime(); // 得到指定日期的毫秒数
        day = day * 24 * 60 * 60 * 1000; // 要加上的天数转换成毫秒数
        time += day; // 相加得到新的毫秒数
        return new Date(time); // 将毫秒数转换成日期
    }

    /**
     * 当前日期加上指定天数后的日期
     */
    public static String currentAddDays(long day) {
        return LocalDate.now().plusDays(day).format(DATE_FORMATTER);
    }

    /**
     * 当前日期加上指定天数后的日期
     */
    public static String currentSubtractDays(long day) {
        return LocalDate.now().plusDays(day).format(DATE_FORMATTER_YYMMDD);
    }

    /**
     * String转换成Date
     *
     * @return
     */
    public static Date stringToDate(String stringTime) throws ParseException {
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        Date date = ft.parse(stringTime);
        return date;
    }

    /**
     * String转换成Date
     *
     * @return
     */
    public static Date stringToDateEnd(String stringTime) throws ParseException {
        stringTime = stringTime + "235959";
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddhhmmss");
        Date date = ft.parse(stringTime);
        return date;
    }

    /**
     * Date转换为LocalDateTime
     *
     * @param date
     */
    public static LocalDateTime date2LocalDateTime(Date date) {
        Instant instant = date.toInstant();//An instantaneous point on the time-line.(时间线上的一个瞬时点。)
        ZoneId zoneId = ZoneId.systemDefault();//A time-zone ID, such as {@code Europe/Paris}.(时区)
        LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();

        return localDateTime;
    }

    /**
     * 当前周index
     * @return
     */
    public static int[] getWeekIndex(Calendar calendar) {
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }
        int result[] = {calendar.get(Calendar.YEAR), calendar.get(Calendar.WEEK_OF_YEAR)};
        return result;
    }

    /**
     * 获取上月开始/结束的时间
     */
    public static List<String> getBeginEndDate(LocalDate date) {
        String firstday = date.with(TemporalAdjusters.firstDayOfMonth()).format(DATE_FORMATTER_YYMMDD);
        String lastDay = date.with(TemporalAdjusters.lastDayOfMonth()).format(DATE_FORMATTER_YYMMDD);
        return Arrays.asList(firstday, lastDay);
    }

    /**
     * 获得当月开始时间(yyyy-MM-dd)
     * @return
     */
    public static String getMonthStartData(){
        Calendar cale = null;
        cale = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstday, lastday;
        // 获取本月的第一天
        cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        return format.format(cale.getTime());
    }

    /**
     * 获得当月开始时间(yyyyMMdd)
     * @return
     */
    public static String getMonthStartDataStr(){
        Calendar cale = null;
        cale = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String firstday, lastday;
        // 获取本月的第一天
        cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        return format.format(cale.getTime());
    }

    /**
     * 获得当月结束时间
     * @return
     */
    public static String getMonthEndData(){
        Calendar cale = null;
        cale = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 获取本月的最后一天
        cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 1);
        cale.set(Calendar.DAY_OF_MONTH, 0);
        return format.format(cale.getTime());
    }

    // 获取当前时间所在周的开始日期
    public static String getFirstDayOfWeek(LocalDate date) {
        SimpleDateFormat format = new SimpleDateFormat();
        // 所在周开始时间
        LocalDate beginDayOfWeek = date.with(DayOfWeek.MONDAY);
        return beginDayOfWeek.format(DATE_FORMATTER_YYMMDD);
    }

    // 获取当前时间所在周的结束日期
    public static String getLastDayOfWeek(LocalDate date) {
        // 所在周结束时间
        LocalDate endDayOfWeek = date.with(DayOfWeek.SUNDAY);
        return endDayOfWeek.format(DATE_FORMATTER_YYMMDD);
    }


    // 获取当前时间所在年的开始日期
    public static String getFirstDayOfYear(LocalDate date) {
        return date.with(TemporalAdjusters.firstDayOfYear()).format(DATE_FORMATTER_YYMMDD);
    }

    // 获取当前时间所在年的结束日期
    public static String getLastDayOfYear(LocalDate date) {

        return date.with(TemporalAdjusters.lastDayOfYear()).format(DATE_FORMATTER_YYMMDD);
    }

    public static String getLocalHour() {
        return LocalDateTime.now().format(HOUR_FORMATTER);
    }


    public static int getTime(Date time) throws ParseException {


        Calendar   cal   =   new   GregorianCalendar();
        //或者用Calendar   cal   =   Calendar.getInstance();


        SimpleDateFormat oSdf = new SimpleDateFormat ("",Locale.ENGLISH);
        oSdf.applyPattern("yyyyMM");
        try {
            System.out.println(oSdf.parse(String.valueOf(time)));
            cal.setTime(oSdf.parse(String.valueOf(time)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /**或者设置月份，注意月是从0开始计数的，所以用实际的月份-1才是你要的月份**/
        //一月份: cal.set(   2009,   1-1,   1   );

        /**如果要获取上个月的**/
        //cal.set(Calendar.DAY_OF_MONTH, 1);
        //日期减一,取得上月最后一天时间对象
        //cal.add(Calendar.DAY_OF_MONTH, -1);
        //输出上月最后一天日期
        //System.out.println(cal.get(Calendar.DAY_OF_MONTH));
        /**开始用的这个方法获取月的最大天数，总是得到是31天**/
        //int num = cal.getMaximum(Calendar.DAY_OF_MONTH);
        /**开始用的这个方法获取实际月的最大天数**/
        int num2 = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return num2;
    }

    public  static Date addDay(Date date,Integer day){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH,day);
        return calendar.getTime();
    }


    /**
     * 获取某年第一天日期
     *
     * @param yearMonth 年月
     * @return Date
     */
    public static String getYearMonthFirst(String yearMonth) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        try {

            Date date = null;
            date = sdf.parse(yearMonth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);// 设为本月第一天
            return sdf2.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new CustomResultException("起始日期异常！");

    }

    /**
     * 获取某年某月最后一天日期
     *
     * @param yearMonth 年月
     * @return Date
     */
    public static String getYearMonthLast(String yearMonth) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        try {

            Date date = null;
            date = sdf.parse(yearMonth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);// 设为本月第一天
            calendar.add(Calendar.MONTH, 1);// 月份加一
            calendar.add(Calendar.DATE, -1);// 天数加 -1 = 上一个月的最后一天
            return sdf2.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new CustomResultException("结束日期异常！");
    }
}
