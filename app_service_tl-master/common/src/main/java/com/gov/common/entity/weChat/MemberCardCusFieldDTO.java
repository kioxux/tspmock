package com.gov.common.entity.weChat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MemberCardCusFieldDTO {

    private String name;

    private String url;

}
