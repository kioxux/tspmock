package com.gov.common.basic;

import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 字符串工具类
 *
 * @author wcy
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    /**
     *
     */
    public static final String EMPTY = "";


    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        Boolean a = !StringUtils.isEmpty(str) && !str.trim().equals("");

        return !StringUtils.isEmpty(str) && !str.trim().equals("");
    }

    /**
     * 判断字符串是否为空
     *
     * @param obj
     * @return
     */
    public static boolean isNotEmpty(Object obj) {
        if (obj instanceof String[]) {
            if (((String[]) obj).length == 0) {
                return false;
            }

        } else if (obj instanceof Collection) {
            if (((Collection) obj).size() == 0) {
                return false;
            }

        } else {
            if (obj == null || obj.equals("") || obj.toString().trim().equals("")) {
                return false;
            }

        }
        return true;
    }

    /**
     * 判断字符串是否为空
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {
        if (obj instanceof String[]) {
            if (((String[]) obj).length == 0) {
                return true;
            }

        } else if (obj instanceof Collection) {
            if (((Collection) obj).size() == 0) {
                return true;
            }

        } else {
            if (obj == null || obj.equals("") || obj.toString().trim().equals("")) {
                return true;
            }

        }
        return false;
    }

    /**
     * @param str
     * @return
     * @throws
     * @Title: getStrValue
     * @Description: 进行Null处理
     * @return: String
     */
    public static String getStrValue(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        } else {
            return str;
        }
    }

    /**
     * 判断字符串是否为空
     *
     * @param array
     * @return
     */
    public static boolean isEmpty(String... array) {
        boolean flag = false;
        if (array != null && array.length > 0) {
            for (String str : array) {
                flag = StringUtils.isEmpty(str);
                if (flag) {
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 判断List是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * 判断List是否为空
     *
     * @param list
     * @return
     */
    public static boolean isNotEmpty(List<?> list) {
        return !StringUtils.isEmpty(list);
    }

    /**
     * 判断Map是否为空
     *
     * @param map
     * @return
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.size() == 0;
    }

    /**
     * 判断object
     *
     * @param obj
     * @return
     */
    public static boolean isObjEmpty(Object obj) {
        return StringUtils.isEmpty(obj) || obj.toString().trim().equals("");
    }

    /**
     * 判断Map是否为空
     *
     * @param map
     * @return
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !StringUtils.isEmpty(map);
    }


    public static boolean isNotEmptyObjects(Object... objs) {
        if (objs instanceof String[]) {
            if (StringUtils.isEmpty(objs)) {
                return false;
            }

        } else {
            for (Object obj : objs) {
                if (StringUtils.isEmpty(obj)) {
                    return false;
                }


            }

        }
        return true;

    }

    public static boolean isAllNotEmptyObjects(Object... objs) {

        if (objs instanceof String[]) {
            if (StringUtils.isNotEmpty(objs)) {
                return true;
            }

        } else {
            for (Object obj : objs) {
                if (StringUtils.isNotEmpty(obj)) {
                    return true;
                }


            }

        }
        return false;

    }

    public static String upperFirst(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        } else {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        }
    }


    /**
     * md5转码
     *
     * @param str 字符串
     * @return 处理后字符串
     */
    public static String getMD5(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }

    /**
     * md5 16 位
     *
     * @param md5
     * @return
     */
    public static String getMD5TO16(String md5) {
        return md5.substring(8, 24);
    }

    /**
     * 字符串传参
     *
     * @param text
     * @param args
     * @return
     */
    public static String parse(String text, String... args) {
        return parseStr("{", "}", text, args);
    }

    /**
     * 字符串传参
     *
     * @param text
     * @param args
     * @return
     */
    public static String parseList(String text, List<String> args) {
        if (CollectionUtils.isEmpty(args)) {
            return parseStr("{", "}", text);

        } else {
            return parseStr("{", "}", text, args.toArray(new String[args.size()]));
        }
    }


    /**
     * 将字符串text中由openToken和closeToken组成的占位符依次替换为args数组中的值
     *
     * @param openToken
     * @param closeToken
     * @param text
     * @param args
     * @return
     */
    private static String parseStr(String openToken, String closeToken, String text, String... args) {
        if (args == null || args.length <= 0) {
            return text;
        }
        int argsIndex = 0;
        if (text == null || text.isEmpty()) {
            return "";
        }
        char[] src = text.toCharArray();
        int offset = 0;
        // search open token
        int start = text.indexOf(openToken, offset);
        if (start == -1) {
            return text;
        }
        final StringBuilder builder = new StringBuilder();
        StringBuilder expression = null;
        while (start > -1) {
            if (start > 0 && src[start - 1] == '\\') {
                // this open token is escaped. remove the backslash and continue.
                builder.append(src, offset, start - offset - 1).append(openToken);
                offset = start + openToken.length();
            } else {
                // found open token. let's search close token.
                if (expression == null) {
                    expression = new StringBuilder();
                } else {
                    expression.setLength(0);
                }
                builder.append(src, offset, start - offset);
                offset = start + openToken.length();
                int end = text.indexOf(closeToken, offset);
                while (end > -1) {
                    if (end > offset && src[end - 1] == '\\') {
                        // this close token is escaped. remove the backslash and continue.
                        expression.append(src, offset, end - offset - 1).append(closeToken);
                        offset = end + closeToken.length();
                        end = text.indexOf(closeToken, offset);
                    } else {
                        expression.append(src, offset, end - offset);
                        offset = end + closeToken.length();
                        break;
                    }
                }
                if (end == -1) {
                    // close token was not found.
                    builder.append(src, start, src.length - start);
                    offset = src.length;
                } else {
                    ///////////////////////////////////////仅仅修改了该else分支下的个别行代码////////////////////////
                    String value = (argsIndex <= args.length - 1) ?
                            (args[argsIndex] == null ? "" : args[argsIndex].toString()) : expression.toString();
                    builder.append(value);
                    offset = end + closeToken.length();
                    argsIndex++;
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                }
            }
            start = text.indexOf(openToken, offset);
        }
        if (offset < src.length) {
            builder.append(src, offset, src.length - offset);
        }
        return builder.toString();
    }
}
