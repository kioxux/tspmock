package com.gov.common.basic;

import com.gov.common.entity.Dictionary;
import com.gov.common.entity.DictionaryStatic;
import com.gov.common.entity.dataImport.*;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class CommonEnum {

    /**
     * 静态数据字典
     */
    public enum DictionaryStaticData {
        // 验证用
        DEFAULT("DEFAULT", new ArrayList<Dictionary>()),

        //是否
        IS_OR_NO("IS_OR_NO", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "是"));
            add(new Dictionary("0", "否"));
        }}),
        // 发票类型
        INVOICE_TYPE("INVOICE_TYPE", new ArrayList<Dictionary>() {
            {
                add(new Dictionary("1", "增值税普通发票"));
                add(new Dictionary("2", "增值税专用发票"));
                add(new Dictionary("3", "电子普票"));
                add(new Dictionary("4", "电子专票"));
                add(new Dictionary("5", "其他"));
            }
        }),
        // 发票地点类型
        INVOICE_SITE_TYPE("INVOICE_SITE_TYPE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "单体"));
            add(new Dictionary("2", "连锁"));
        }}),
        // 限价类型：0每日，1合计
        GSPUS_LIMIT_TYPE("GSPUS_LIMIT_TYPE", new ArrayList<Dictionary>() {
            {
                add(new Dictionary("0", "每日"));
                add(new Dictionary("1", "合计"));
            }
        });

        private String code;
        private List<Dictionary> list;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public List<Dictionary> getList() {
            return list;
        }

        public void setList(List<Dictionary> list) {
            this.list = list;
        }

        DictionaryStaticData(String code, List<Dictionary> list) {
            this.code = code;
            this.list = list;
        }

        //通过value获取对应的枚举对象
        public static List<DictionaryStatic> getDictionary(String value) {
            List<DictionaryStatic> list = new ArrayList<>();
            for (DictionaryStaticData dictionaryStaticData : DictionaryStaticData.values()) {
                DictionaryStatic dictionaryStatic = new DictionaryStatic();
                dictionaryStatic.setKey(dictionaryStaticData.getCode());
                dictionaryStatic.setList(dictionaryStaticData.getList());
                if (StringUtils.isNotBlank(value)) {
                    if (value.equals(dictionaryStaticData.getCode())) {
                        list.add(dictionaryStatic);
                        break;
                    }
                } else {
                    list.add(dictionaryStatic);
                }
            }
            return list;
        }

        /**
         * 返回当前字典的明细数据
         *
         * @param dictionaryStaticData 字典对象
         * @param value                编码
         * @return
         */
        public static Dictionary getDictionaryByValue(DictionaryStaticData dictionaryStaticData, String value) {
            // 	字典对象
            List<Dictionary> list = dictionaryStaticData.getList();
            // 数据不存在
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            // 遍历当前字典数据
            for (Dictionary dictionary : list) {
                if (dictionary.getValue().equals(value)) {
                    return dictionary;
                }
            }
            return null;
        }

        /**
         * 返回当前字典的明细数据
         *
         * @param dictionaryStaticData 字典对象
         * @param label                表示值
         * @return
         */
        public static Dictionary getDictionaryByLabel(DictionaryStaticData dictionaryStaticData, String label) {
            // 	字典对象
            List<Dictionary> list = dictionaryStaticData.getList();
            // 数据不存在
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            // 遍历当前字典数据
            for (Dictionary dictionary : list) {
                if (label.equals(dictionary.getLabel())) {
                    return dictionary;
                }
            }
            return null;
        }

        /**
         * 字典数据查询
         *
         * @param dictionaryStaticData
         * @param label
         * @return
         */
        public static List<String> getDictionaryValByLikeLabel(DictionaryStaticData dictionaryStaticData, String label) {
            List<String> list = new ArrayList<>();
            // 	字典对象
            List<Dictionary> dictionaryList = dictionaryStaticData.getList();
            // 数据不存在
            if (CollectionUtils.isEmpty(dictionaryList)) {
                return list;
            }
            // 遍历当前字典数据
            for (Dictionary dictionary : dictionaryList) {
                if (dictionary.getLabel().indexOf(label) >= 0) {
                    list.add(dictionary.getValue());
                }
            }
            return list;
        }
    }

    /**
     * 订单类型
     */
    public enum ficoOrderType {
        STORESERVICE(1, "门店服务费"),
        SMSSERVICE(2, "短信充值");

        private Integer code;
        private String message;

        ficoOrderType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 发票地点类型
     */
    public enum InvoiceSiteType {
        InvoiceSiteType1("1", "单体"),
        InvoiceSiteType2("2", "连锁");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        InvoiceSiteType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 开票状态
     */
    public enum InvoiceStatus {

        UN_INVOICE("0", "未开票"),
        INVOICING("1", "开票中"),
        SUCCESS("2", "已开票"),
        FAILE("3", "开票失败");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        InvoiceStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 发票类型
     */
    public enum InvioceType {
        ORDINARY("1", "增值税普通发票"),
        SPECIAL("2", "增值税专用发票");

        private String code;
        private String message;

        InvioceType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 支付方式
     */
    public enum PayType {
        SCAN(1, "扫码支付"),
        APP(2, "APP银联支付"),
        GATEWAY(3, "网关支付");

        private Integer code;
        private String message;

        PayType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 付款状态
     */
    public enum PaymentStatus {

        UNPAID("0", "未付款"),
        SUCCESS("1", "成功"),
        FAILE("2", "失败");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PaymentStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 支付回调结果
     */
    public enum PayResult {

        TRADE_SUCCESS("TRADE_SUCCESS", "调用成功"),

        PAID("PAID", "支付成功");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PayResult(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 业务导入类型
     */
    public enum BusinessImportTypet {
        MARKETINGCOMPONENT("1", "会员营销成分导入", 2, MarketingComponent.class),
        PROMUNITARYPRODUCT("2", "促销单品商品导入", 2, PromUnitaryProduct.class),
        PROMSERIESPRODUCT("3", "促销系列商品导入", 1, PromSeriesProduct.class),
        PROMGIFTCONDSPRODUCT("4", "促销赠品条件商品导入", 1, PromGiftCondsProduct.class),
        PROMGIFTRESULTPRODUCT("5", "促销赠品赠送商品导入", 1, PromGiftResultProduct.class),
        PROMASSOCONDSPRODUCT("6", "促销组合条件商品导入", 1, PromAssoCondsProduct.class),
        PROMASSORESULTPRODUCT("7", "促销组合赠送商品导入", 1, PromAssoResultProduct.class),
        MaterialDoc("8", "物料凭证导入", 1, MaterialDocData.class),
        HYANDHYRPRICE("9", "会员/会员日特价导入", 2, HyAndHyrPrice.class),
        SD_PROM_GROUP_PRO("10", "促销商品组导入", 1, PromGroupPro.class),
        INVOICE_INFORMATION_REGISTRATION("11", "发票导入", 2, InvoiceInformationRegistration.class);

        private String code;

        private String name;

        private Integer lineNo;

        private Class<?> cs;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getLineNo() {
            return lineNo;
        }

        public void setLineNo(Integer lineNo) {
            this.lineNo = lineNo;
        }

        public Class<?> getCs() {
            return cs;
        }

        public void setCs(Class<?> cs) {
            this.cs = cs;
        }

        BusinessImportTypet(String code, String name, Integer lineNo, Class<?> cs) {
            this.code = code;
            this.name = name;
            this.lineNo = lineNo;
            this.cs = cs;
        }

        public static BusinessImportTypet getBusinessImportTypet(String code) {
            for (BusinessImportTypet businessImportTypet : CommonEnum.BusinessImportTypet.values()) {
                if (businessImportTypet.getCode().equals(code)) {
                    return businessImportTypet;
                }
            }
            return null;
        }
    }

    public enum UserSta {
        ON_THE_JOB("0", "在职"),
        QUIT_THE_JOB("1", "离职"),
        STOP("2", "停用");

        private String code;
        private String message;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }

        UserSta(String code, String message) {
            this.code = code;
            this.message = message;
        }

    }

    /**
     * cos 目录路径
     */
    public enum CosPaht {
        FEEDBACK("feedback", "feedback/"),
        COMPANYICON("companyicon", "companyicon/"),
        TRAIN("train", "train/"),
        USERSIGN("usersignature/", "usersignature/");
        private String code;
        private String message;

        CosPaht(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 组织类型
     */
    public enum OrganizationType {
        STORE("1", "门店"),
        COMPADM("2", "连锁中心"),
        COMPADMWMS("6", "批发"),
        DC("3", "配送中心"),
        DEP("4", "部门"),
        WAREHOUSE("5", "仓库");

        private String code;
        private String message;

        OrganizationType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 门店类型
     */
    public enum StoreType {
        MONOMER_STORE("1", "单体"),
        CHAIN_STORE("2", "连锁"),
        JOIN_STORE("3", "加盟"),
        CLINIC_STORE("4", "门诊");

        private String code;
        private String message;

        StoreType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 部门类型
     */
    public enum DepType {
        FR_DEP("10", "加盟商"),
        CHAIN_DEP("20", "连锁"),
        WHOLESALE_DEP("30", "批发公司"),
        WAREHOUSE_DEP("40", "公司仓库");

        private String code;
        private String message;

        DepType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    /**
     * DC类型
     */
    public enum DCType {
        CHAIN_DC("20", "连锁"),
        WHOLESALE_DC("30", "批发公司");

        private String code;
        private String message;

        DCType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 部门类型/部门代码
     */
    public enum DepCls {
        WAREHOUSE_DLS("17", "公司仓库");

        private String code;
        private String message;

        DepCls(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 是否没有批发资质
     */
    public enum DC_WHOLESALE {
        DC_WHOLESALE_NO("0", "没有批发资质"),
        DC_WHOLESALE_YES("1", "有批发资质");

        private String code;
        private String message;

        DC_WHOLESALE(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 门店状态 0停用,1启用
     */
    public enum StoStatus {
        STO_STATUS_NO("0", "停用"),
        STO_STATUS_YES("1", "启用");

        private String code;
        private String message;

        StoStatus(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 岗位组
     */
    public enum AuthGroup {
        SD_01("SD_01", "主管"),
        GAIA_WM_FZR("GAIA_WM_FZR", "主管"),
        GAIA_AL_ADMIN("GAIA_AL_ADMIN", "系统管理员"),
        GAIA_MM_ZLZG("GAIA_MM_ZLZG", "质量主管"),
        GAIA_MM_SCZG("GAIA_MM_SCZG", "商采主管"),
        GAIA_FI_ZG("GAIA_FI_ZG", "主管"),
        GAIA_AL_MANAGER("GAIA_AL_MANAGER", "公司负责人");
//        GAIA_SD_ZLGL("GAIA_SD_ZLGL", "门店质量管理");


        private String code;
        private String message;

        AuthGroup(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static List<String> getEnumValueList() {
            List<String> list = new ArrayList<>();
            for (AuthGroup authGroup : AuthGroup.values()) {
                list.add(authGroup.getCode());
            }
            return list;
        }
    }


    /**
     * 公众号卡类型
     * 1：钻石 2：白金 3：金 4：银 5：普通
     */
    public enum CardType {
        CARD_TYPE_DIAMOND("1", "钻石"),
        CARD_TYPE_PLATINUM("2", "白金"),
        CARD_TYPE_GOLD("3", "金"),
        CARD_TYPE_SILVER("4", "银"),
        CARD_TYPE_ORDINARY("5", "普通");


        private String code;
        private String message;

        CardType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static List<String> getEnumValueList() {
            List<String> list = new ArrayList<>();
            for (CardType authGroup : CardType.values()) {
                list.add(authGroup.getCode());
            }
            return list;
        }
    }

    /**
     * 会员状态
     * 0：激活 1：新卡
     */
    public enum UserStatus {
        STATUS_ACTIVITY("0", "激活"),
        STATUS_NEW("1", "新卡");


        private String code;
        private String message;

        UserStatus(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static List<String> getEnumValueList() {
            List<String> list = new ArrayList<>();
            for (UserStatus authGroup : UserStatus.values()) {
                list.add(authGroup.getCode());
            }
            return list;
        }
    }


    /**
     * 会员卡类型
     * 1：虚拟卡 2：实体卡
     */
    public enum UserCardType {
        STATUS_ACTIVITY("1", "虚拟卡"),
        STATUS_NEW("2", "实体卡");


        private String code;
        private String message;

        UserCardType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static List<String> getEnumValueList() {
            List<String> list = new ArrayList<>();
            for (UserCardType authGroup : UserCardType.values()) {
                list.add(authGroup.getCode());
            }
            return list;
        }
    }


    /**
     * 门店管理员类型
     * 1：店长 2：质量负责人 3:公司负责人
     */
    public enum UserType {
        STATUS_LEADER("1", "店长"),
        STATUS_QUA("2", "质量负责人"),
        STATUS_LEADER_PERSON("3", "公司负责人");


        private String code;
        private String message;

        UserType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static List<String> getEnumValueList() {
            List<String> list = new ArrayList<>();
            for (UserType authGroup : UserType.values()) {
                list.add(authGroup.getCode());
            }
            return list;
        }
    }

    public enum paymentApplicationsType {

        BILL(1, "单据付款"),
        INVOICE(2, "发票付款"),
        PREPAIDBILL(3, "单据预付");

        private Integer code;
        private String message;

        paymentApplicationsType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static paymentApplicationsType getPaymentApplicationsType(Integer code) {
            for (paymentApplicationsType type : paymentApplicationsType.values()) {
                if (type.getCode().equals(code)) {
                    return type;
                }
            }
            return null;
        }
    }

    /**
     * 消息表示形式 1：消息，2：推送，3：消息+推送
     */
    public enum gmShowType {
        MESSAGE(1, "消息"),
        PUSH(2, "推送"),
        MSG_PUSH(3, "消息+推送");

        private Integer code;
        private String message;

        gmShowType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 是否已读
     * 0：未读，1：已读
     */
    public enum gmIsRead {
        UNREAD(0, "未读"),
        READ(1, "已读");
        private Integer code;
        private String message;

        gmIsRead(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public enum gmtId {
        MSG00001("MSG00001", "付费提醒"),
        MSG00002("MSG00002", "客户注册"),
        MSG00003("MSG00003", "门店、配送中心创建"),
        MSG00004("MSG00004", "部门负责人创建"),
        MSG00005("MSG00005", "门店、配送中心创建_付费"),
        MSG00006("MSG00006", "精准营销"),
        MSG00007("MSG00007", "员工培训");
        private String code;
        private String message;

        gmtId(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 消息模板启用停用
     * 0：停用，1：启用
     */
    public enum gmtFlag {
        FLAGT(1, "启用"),
        FLAGF(0, "停用");
        private Integer code;
        private String message;

        gmtFlag(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 角色功能
     * 角色功能：0-普通角色，1-公司负责人，2-门店店长，3-商采负责人，4-财务负责人，5-人资负责人，6-物流负责人
     */
    public enum garFunc {
        NOR("0", "普通角色"),
        CEO("1", "公司负责人"),
        SHOP_OWNER("2", "门店店长"),
        PURCHASE("3", "商采负责人"),
        FINANCE("4", "财务负责人"),
        HR("5", "人资负责人"),
        LOGISTICS("6", "物流负责人"),
        ADMIN("7", "系统管理员"),
        PURCHASE_USER("8", "商采用户"),
        FINANCE_USER("9", "财务用户"),
        HR_USER("10", "财务用户"),
        SHOP_USER("11", "门店员工"),
        LOGISTICS_USER("12", "物流用户"),
        QUALITY("13", "质量负责人"),
        QUALITY_USER("14", "质量用户");

        private String code;
        private String name;

        garFunc(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public static garFunc getByCode(String code) {
            for (garFunc value : values()) {
                if (value.getCode().equals(code)) {
                    return value;
                }
            }
            return null;
        }

    }

    /**
     * 角色功能
     * 角色功能：0-普通角色，1-公司负责人，2-门店店长，3-商采负责人，4-财务负责人，5-人资负责人，6-物流负责人
     */
    public enum PaymentMethod {
        XJ("1000", "现金", "#1E90FF"),
        WX("3001", "微信", "#65D979"),
        ZFB("3002", "支付宝", "#00BFFF"),
        YB("4000", "医保", "#F4A460"),
        QT("other", "其他", "#CCC");

        private String code;
        private String name;
        private String color;

        PaymentMethod(String code, String name, String color) {
            this.code = code;
            this.name = name;
            this.color = color;

        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public static PaymentMethod getByCode(String code) {
            for (PaymentMethod value : values()) {
                if (value.getCode().equals(code)) {
                    return value;
                }
            }
            return null;
        }

    }

    /**
     * 供应商发票类型
     */
    public enum Invoice_type {
        // 1:增值税普通发票/2:增值税专用发票/3:电子普票/4:电子专票/5:其他
        INVOICE1("1", "增值税普通发票"),
        INVOICE2("2", "增值税专用发票"),
        INVOICE3("3", "电子普票"),
        INVOICE4("4", "电子专票"),
        INVOICE5("5", "其他");
        private String code;
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        Invoice_type(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public static Invoice_type getByCode(String code) {
            for (Invoice_type value : values()) {
                if (value.getCode().equals(code)) {
                    return value;
                }
            }
            return null;
        }
    }

    /**
     * 供应商付款申请 状态
     */
    public enum SupplierPrepaymentApplyStatus {
        STATUS0("0", "未申请"),
        STATUS1("1", "审批中"),
        STATUS2("2", "已付款"),
        STATUS3("3", "已作废");

        private String code;
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        SupplierPrepaymentApplyStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 微信发布授权(0:未授权 1:已授权)
     * 储值卡充值是否可用(0.不可用   1.可用)
     * 储值卡充值方式 是否为接口(0为否，1为是)
     */
    public enum StrYesOrNo {
        YES("1", "是"),
        NO("0", "否");

        private String code;
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        StrYesOrNo(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     *
     */
    public enum IntYesOrNo {
        YES(1, "是"),
        NO(0, "否");

        private Integer code;
        private String name;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        IntYesOrNo(Integer code, String name) {
            this.code = code;
            this.name = name;
        }
    }
}
