package com.gov.common.entity.weChat;

import lombok.Data;

@Data
public class MemberActiveParams {

    private String code;

    private String membership_number;

    private String card_id;

}
