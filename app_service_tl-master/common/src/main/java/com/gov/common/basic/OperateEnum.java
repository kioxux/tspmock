package com.gov.common.basic;

/**
 * operate 项目枚举值
 */
public class OperateEnum {

    /**
     * 消费类型具体对应的天数（1-7天内、2-15天内、3-30天内、4-60天内、5-自定义）
     */
    public enum ConsumeType {
        ONE("1", "7"),
        TWO("2", "15"),
        THREE("3", "30"),
        FOUR("4", "60"),
        FIVE("5", "");

        private String code;
        private String message;

        ConsumeType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 剔除类型
     */
    public enum EliminateType {
        ELIMINATION("1", "剔除已发短信会员"),
        CUSTOM("2", "剔除自定义会员");

        private String code;
        private String message;

        EliminateType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    public enum YesOrNo {
        ZERO("0", "零"),
        ONE("1", "一");

        private String code;
        private String message;

        YesOrNo(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    public enum Sex {
        MAN("0", "女"),
        WOMAN("1", "男");

        private String code;
        private String message;

        Sex(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * APP是否发布(1:否，2:是)
     */
    public enum ReleaseFlag {
        NO("1", "否"),
        YES("2", "是");

        private String code;
        private String message;

        ReleaseFlag(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 模板状态 0启用，1未启用
     */
    public enum SmsStatus {
        YES("0", "启用"),
        NO("1", "未启用");

        private String code;
        private String message;

        SmsStatus(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 短信模版审批状态 0-审批中，1-已审批，2-已拒绝
     */
    public enum GspStatus {
        PROCESS("0", "审批中"),
        FINISH("1", "已审批"),
        REFUSE("2", "已拒绝");

        private String code;
        private String message;

        GspStatus(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 状态(0:未推送, 1:已推送, 2:立即执行, 3:待审批, 4:审批通过, 5:审批拒绝)
     */
    public enum GsmImpl {
        NOT_PUSHED("0", "未推送"),
        PUSHED("1", "已推送"),
        IMMEDIATE_EXECUTION("2", "立即执行"),
        PENDING_APPROVAL("3", "待审批"),
        APPROVED("4", "审批通过"),
        REFUSE("5", "审批拒绝");

        private String code;
        private String message;

        GsmImpl(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }


    /**
     * 官方账号配置/参数
     */
    public enum GoaObjectValue {
        WECHAT_OFFICIAL_ACCOUNTS_BIZ("WECHAT_OFFICIAL_ACCOUNTS_BIZ", "微信公众号BIZ"),
        WECHAT_OFFICIAL_ACCOUNTS_APPID("WECHAT_OFFICIAL_ACCOUNTS_APPID", "微信公众号APPID"),
        WECHAT_OFFICIAL_LOGO("WECHAT_OFFICIAL_LOGO", "微信公众号logo"),
        WECHAT_OFFICIAL_MEMBER_BENEFITS("WECHAT_OFFICIAL_MEMBER_BENEFITS", "微信公众号会员权益"),
        WECHAT_OFFICIAL_ACCOUNTS_APPSECRET("WECHAT_OFFICIAL_ACCOUNTS_APPSECRET", "微信公众号SECRET"),
        WECHAT_OFFICIAL_ACCOUNTS_VALID_PERIOD("WECHAT_OFFICIAL_ACCOUNTS_VALID_PERIOD", "微信公众号会员卡有效期"),
        WECHAT_OFFICIAL_ACCOUNTS_USAGE_NOTICE("WECHAT_OFFICIAL_ACCOUNTS_USAGE_NOTICE", "微信公众号会员卡使用说明"),
        WECHAT_OFFICIAL_ACCOUNTS_TEL("WECHAT_OFFICIAL_ACCOUNTS_TEL", "微信公众号电话号码"),
        WECHAT_OFFICIAL_WECHAT_TWEETS("WECHAT_OFFICIAL_WECHAT_TWEETS", "微信推文授权"),
        WECHAT_OFFICIAL_ORIGINAL_ID("WECHAT_OFFICIAL_ORIGINAL_ID", "微信原始ID"),
        WECHAT_OFFICIAL_WECHAT_MEMBERSHIP_CARD("WECHAT_OFFICIAL_WECHAT_MEMBERSHIP_CARD", "授权微信会员卡"),
        WECHAT_OFFICIAL_MEMBER_CARD_ID("WECHAT_OFFICIAL_MEMBER_CARD_ID", "会员卡ID"),
        ;

        private String code;
        private String message;

        GoaObjectValue(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 促销类型与促销类型描述
     */
    public enum GspvtType {
        PROM_SINGLE("PROM_SINGLE", "单品类促销"),
        PROM_SERIES("PROM_SERIES", "系列类促销"),
        PROM_GIFT("PROM_GIFT", "赠品类促销"),
        PROM_COUPON("PROM_COUPON", "电子券类促销"),
        PROM_HY("PROM_HY", "会员类促销"),
        PROM_COMBIN("PROM_COMBIN", "捆绑类促销");

        private String code;
        private String message;

        GspvtType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 营销审批状态
     */
    public enum MarketingImpl {
        NOTAPPROVED("-1", "待处理"),
        CONSENT_EXECUTION("0", "修改中"),
        APPROVAL("1", "待审批"),
        APPROVED("2", "已审批"),
        VOID("3", "作废"),
        EXECUTE("4", "同意执行"),
        NOENFORCEMENT("5", "不予执行");

        private String code;
        private String name;

        MarketingImpl(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public enum MarketingProm {
        SINGLE("SINGLE", "单品"),
        SERIES("SERIES", "系列"),
        GIFT("GIFT", "赠品"),
        COMBIN("COMBIN", "组合");

        private String code;
        private String name;

        MarketingProm(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 是否积分
     */
    public enum InteFlag {
        YES("1", "是"),
        NO("0", "否");

        private String code;
        private String name;

        InteFlag(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 是否发布 (0：未操作，1：已发布，2：不发布)
     */
    public enum GwthRelease {
        NO_OPERATE(0, "未操作"),
        RELEASE(1, "已发布"),
        NO_RELEASE(2, "不发布");

        private Integer code;
        private String name;

        GwthRelease(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 物料凭证导入状态 (1：未导入，2：执行中,3:导入失败，0：成功)
     */
    public enum GmdiStatus {
        NO_IMPORT(1, "未导入"),
        EXECUTING(2, "执行中"),
        FAIL(3, "导入失败"),
        SUCCESS(0, "成功");

        private Integer code;
        private String name;

        GmdiStatus(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 物料凭证导入类型 (1：正常导入，2：只生成凭证)
     */
    public enum GmdiType {
        NORMAL(1, "正常导入"),
        GENERATE_VOUCHER(2, "只生成凭证");

        private Integer code;
        private String name;

        GmdiType(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 储值卡 充值类型（0或空为正常充值，1为退款）
     */
    public enum GsrcpFlag {
        NORMAL("0", "正常充值"),
        REFUND("1", "退款");

        private String code;
        private String name;

        GsrcpFlag(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


    /**
     * 支付类型  1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     */
    public enum GsspmType {
        SALE("1", "销售支付"),
        CARD("2", "储值卡充值"),
        REGISTER("3", "挂号支付"),
        AGENCY_SALES("4", "代销售");

        private String code;
        private String name;

        GsspmType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
