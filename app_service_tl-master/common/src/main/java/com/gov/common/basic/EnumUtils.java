package com.gov.common.basic;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class EnumUtils {

    /**
     * 判断枚举是否包含当前值
     * @param value
     * @param enumT
     * @param <T>
     * @return
     */
    public static <T> Boolean contains(String value, Class<T> enumT) {
        T tobj = getEnumByCode(value, enumT);
        return tobj != null;
    }

    /**
     * 根据code取当前枚举
     *
     * @param enumT
     * @return enum mapcolloction
     */
    public static <T> T getEnumByCode(String value, Class<T> enumT) {
        // code 为空
        if (StringUtils.isBlank(value)) {
            return null;
        }
        if (!enumT.isEnum()) {
            return null;
        }
        T[] enums = enumT.getEnumConstants();
        if (enums == null || enums.length <= 0) {
            return null;
        }
        String valueMathod = "getCode"; //默认接口value方法
        for (int i = 0, len = enums.length; i < len; i++) {
            T tobj = enums[i];
            try {
                Object resultValue = getMethodValue(valueMathod, tobj); //获取value值
                if (value.equals(resultValue)) {
                    return tobj;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 根据反射，通过方法名称获取方法值，忽略大小写的
     *
     * @param methodName
     * @param obj
     * @param args
     * @return return value
     */
    private static <T> Object getMethodValue(String methodName, T obj, Object... args) {
        Object result = "";
        try {
            /********************************* start *****************************************/
            Method[] methods = obj.getClass().getMethods(); //获取方法数组，这里只要共有的方法
            if (methods.length <= 0) {
                return result;
            }
            Method method = null;
            for (int i = 0, len = methods.length; i < len; i++) {
                if (methods[i].getName().equalsIgnoreCase(methodName)) { //忽略大小写取方法
                    methodName = methods[i].getName(); //如果存在，则取出正确的方法名称
                    method = methods[i];
                    break;
                }
            }
            /*************************** end ***********************************************/
            if (method == null) {
                return result;
            }
            result = method.invoke(obj, args); //方法执行
            if (result == null) {
                result = "";
            }
            return result; //返回结果
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
