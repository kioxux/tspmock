package com.gov.common.entity.dataImport;

import com.gov.common.basic.CommonEnum;
import com.gov.common.validate.ExcelValidate;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 发票导入
 */
@Data
public class InvoiceInformationRegistration {

    /**
     * 发票号码
     */
    @ExcelValidate(index = 0, name = "发票号码", type = ExcelValidate.DataType.STRING, length = 8)
    private String invoiceNum;

    /**
     * 发票地点类型（1:单体 2.连锁）
     */
    @ExcelValidate(index = 1, name = "地点类型", type = ExcelValidate.DataType.STRING, dictionaryStaticField = "invoiceSiteTypeValue",
            dictionaryStaticData = CommonEnum.DictionaryStaticData.INVOICE_SITE_TYPE, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String invoiceSiteType;

    private String invoiceSiteTypeValue;

    /**
     * 采购
     */
    @ExcelValidate(index = 2, name = "地点", type = ExcelValidate.DataType.STRING)
    private String siteCode;

    /**
     * 供应商编码
     */
    @ExcelValidate(index = 3, name = "供应商", type = ExcelValidate.DataType.STRING)
    private String supCode;

    /**
     * 发票类型
     */
    @ExcelValidate(index = 4, name = "发票类型", type = ExcelValidate.DataType.STRING, dictionaryStaticField = "invoiceTypeValue",
            dictionaryStaticData = CommonEnum.DictionaryStaticData.INVOICE_TYPE, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String invoiceType;

    private String invoiceTypeValue;

    /**
     * 发票日期
     */
    @ExcelValidate(index = 5, name = "发票日期", type = ExcelValidate.DataType.DATE)
    private String invoiceDate;

    /**
     * 发票去税金额
     */
    @ExcelValidate(index = 6, name = "去税金额", type = ExcelValidate.DataType.DECIMAL)
    private String invoiceAmountExcludingTax;

    /**
     * 发票税额
     */
    @ExcelValidate(index = 7, name = "税额", type = ExcelValidate.DataType.DECIMAL)
    private String invoiceTax;

    /**
     * 折扣
     */
    @ExcelValidate(index = 8, name = "折扣", type = ExcelValidate.DataType.DECIMAL, required = false, defaultVal = "0")
    private String invoiceDiscount;

    /**
     * 业务员
     */
    @ExcelValidate(index = 9, name = "业务员", type = ExcelValidate.DataType.STRING, required = false)
    private String invoiceSalesman;

    /**
     * 备注
     */
    @ExcelValidate(index = 10, name = "备注", type = ExcelValidate.DataType.STRING, required = false, maxLength = 255)
    private String invoiceRemark;
}
