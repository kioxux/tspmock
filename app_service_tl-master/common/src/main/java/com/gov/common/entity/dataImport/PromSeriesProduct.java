package com.gov.common.entity.dataImport;

import com.gov.common.basic.CommonEnum;
import com.gov.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: xx
 * @date: 2021.02.26
 */
@Data
public class PromSeriesProduct {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING)
    private String proSelfCode;


    /**
     * 是否会员
     */
    @ExcelValidate(index = 1, name = "是否会员", type = ExcelValidate.DataType.STRING, required = false, maxLength = 10,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.IS_OR_NO, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String gspscMemFlag;

    /**
     * 是否积分
     */
    @ExcelValidate(index = 2, name = "是否积分", type = ExcelValidate.DataType.STRING, required = false, maxLength = 10,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.IS_OR_NO, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String gspscInteFlag;

    /**
     * 积分倍率
     */
    @ExcelValidate(index = 3, name = "积分倍率", type = ExcelValidate.DataType.DECIMAL, required = false)
    private String gspscInteRate;

}

