package com.gov.common.userutil;

import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.rsa.RSAUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.security.*;
import java.util.*;

public class UserUtils {


    private static final String TOKEN_HEADER = "X-Token";
    // APP
    public static final String PLATFORM_1 = "1";
    // 微信
    public static final String PLATFORM_2 = "2";

    public static Map<String, String> genKeyPair() {
        try {
            KeyPairGenerator genKeyPair = KeyPairGenerator.getInstance("RSA");
            genKeyPair.initialize(1024, new SecureRandom());
            KeyPair keyPair = genKeyPair.generateKeyPair();
            Map<String, String> keyMap = new HashMap<>();
            PrivateKey privateKey = keyPair.getPrivate();
            keyMap.put("privateKey", Base64.getEncoder().encodeToString(privateKey.getEncoded()));
            PublicKey publicKey = keyPair.getPublic();
            keyMap.put("publicKey", Base64.getEncoder().encodeToString(publicKey.getEncoded()));
            return keyMap;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取Request
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return attrs.getRequest();
    }


    /**
     * 获取请求tokenCode
     */
    public static String getTokenCode() {
        String toekn = getRequest().getHeader(TOKEN_HEADER);
        if (org.apache.commons.lang3.StringUtils.isBlank(toekn)) {
            throw new CustomResultException(ResultEnum.E0007);
        }
        return toekn;
    }


    /**
     * 获取token
     *
     * @return
     */
    public static Claims getToken() {
        String token = getTokenCode();
        if (StringUtils.isEmpty(token)) {
            return null;
        }

        try {
            return Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(RSAUtil.privateKey))
                    .parseClaimsJws(token).getBody();
        } catch (Exception ex) {
            return null;
        }
    }

    //获得client
    public static String getClient() {
        return (String) Objects.requireNonNull(getToken()).get("client");
    }

    //获得userId
    public static String getUserId() {
        return (String) Objects.requireNonNull(getToken()).get("userId");
    }

    //获得platform
    public static String getPlatform() {
        return (String) Objects.requireNonNull(getToken()).get("platform");
    }

    /**
     * @param client   加盟商
     * @param userId   用户id
     * @param platform 平台
     * @return
     */
    public static String createToken(String client, String userId,String depId, String userName, String platform) {

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        //生成签名密钥
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(RSAUtil.privateKey);

        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());


        Map<String, Object> map = new HashMap<>();
        map.put("client", client);
        map.put("platform", platform);
        map.put("userId", userId);
        map.put("depId", depId);
        map.put("loginName", userName);
        //添加构成JWT的参数
        JwtBuilder builder = Jwts.builder()
                .setClaims(map)
                .setIssuedAt(new Date())
                .signWith(signatureAlgorithm, signingKey);

        //生成JWT
        return builder.compact();

    }


    /**
     * @param client   加盟商
     * @param card   会员卡号
     * @param openId openId
     * @param Store 门店
     * @param userId 会员id
     * @return
     */
    public static String createMemberToken(String client, String card, String openId,String Store,String userId, String platform) {

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        //生成签名密钥
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(RSAUtil.privateKey);

        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());


        Map<String, Object> map = new HashMap<>();
        map.put("client", client);
        map.put("openId", openId);
        map.put("store", Store);
        map.put("userId", userId);
        map.put("platform", platform);

        //添加构成JWT的参数
        JwtBuilder builder = Jwts.builder()
                .setId(card)
                .setClaims(map)
                .setIssuedAt(new Date())
                .signWith(signatureAlgorithm, signingKey);

        //生成JWT
        return builder.compact();

    }

    public static String encryptMD5(String str) {
        return DigestUtils.md5Hex(str);
    }

}
