package com.gov.common.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.24
 */
@Data
public class FileResult {

    private String url;

    private String path;

    private String fileName;
}
