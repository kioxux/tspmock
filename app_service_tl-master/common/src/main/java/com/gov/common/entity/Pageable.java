package com.gov.common.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class Pageable {
    /**
     * 查询页面
     */
    @NotNull(message = "未发现查询参数页码")
    private Integer pageNum;

    /**
     * 分页信息
     */
    @NotNull(message = "未发现查询参数分页")
    private Integer pageSize;
}
