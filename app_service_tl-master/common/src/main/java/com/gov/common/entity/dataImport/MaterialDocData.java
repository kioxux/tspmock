package com.gov.common.entity.dataImport;

import com.gov.common.validate.ExcelValidate;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Description:
 *
 * @author: zp
 * @date: 2021.04.06
 */
@Data
public class MaterialDocData {

    /**
     * 凭证日期
     */
    @ExcelValidate(index = 0, name = "凭证日期", type = ExcelValidate.DataType.DATE)
    private String materialDate;

    /**
     * 加盟商
     */
    @ExcelValidate(index = 1, name = "加盟商", type = ExcelValidate.DataType.STRING)
    private String client;

    /**
     * 地点
     */
    @ExcelValidate(index = 2, name = "地点", type = ExcelValidate.DataType.STRING)
    private String siteCode;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 3, name = "商品编码", type = ExcelValidate.DataType.STRING)
    private String proCode;

    /**
     * 批次
     */
    @ExcelValidate(index = 4, name = "批次", type = ExcelValidate.DataType.STRING)
    private String batch;

    /**
     * 库存数量
     */
    @ExcelValidate(index = 5, name = "库存数量", type = ExcelValidate.DataType.DECIMAL)
    private String qty;

    /**
     * 库存金额
     */
    @ExcelValidate(index = 6, name = "库存金额", type = ExcelValidate.DataType.DECIMAL)
    private String batchAmt;

    /**
     * 库存税额
     */
    @ExcelValidate(index = 7, name = "库存税额", type = ExcelValidate.DataType.DECIMAL)
    private String rateAmt;


}
