package com.gov.common.response;

import lombok.Data;

/**
 * 接口返回实体
 */
@Data
public class Result {
    //返回码
    private String code;
    //提示信息
    private String message;
    //返回具体内容
    private Object data;

    public Result() {
    }

    public Result(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


}
