package com.gov.common.utils;

import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author xiaoyuan
 */
public class CommonUtil {

    private static DecimalFormat decimalFormat = new DecimalFormat("##########.##########");

    public static String getyyyyMMdd() {
        return DateUtil.format(new Date(), "yyyyMMdd");
    }

    public static String getHHmmss() {
        return DateUtil.format(new Date(), "HHmmss");
    }

    public static List<String> twoDimensionToOneDimensionArrar(String[][] arrtwo){
//            arrtwo= new String[][]{{"1", "101", "10101"}, {"1", "101", "10102"}};
        String[] int1d;
        int len = 0;
        for (String[] element : arrtwo) {
            len += element.length;
        }
        int1d = new String[len];
        int index = 0;
        for (String[] array : arrtwo) {
            for (String element : array) {
                int1d[index++] = element;
            }
        }

        List<String> list = Lists.newArrayList();
        for (int i = 0; i < int1d.length; i++) {
            if (!list.contains(int1d[i])) {
                list.add(int1d[i]);
            }
        }
        return list ;
    }

}
