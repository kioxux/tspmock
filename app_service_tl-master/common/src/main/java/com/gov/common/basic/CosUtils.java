package com.gov.common.basic;

import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.FileResult;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.09
 */
@Slf4j
@Component
public class CosUtils {

    @Resource
    private ApplicationConfig applicationConfig;

    /**
     * 上传文件
     *
     * @param file 文件
     * @param type 类型
     * @return
     */
    public Result uploadFile(MultipartFile file, String type) {
        try {
            // 上传对象为空不存在
            if (file == null || StringUtils.isBlank(type)) {
                return ResultUtil.error(ResultEnum.E0101);
            }
            // 上传目录不存在
            if (!EnumUtils.contains(type, CommonEnum.CosPaht.class)) {
                return ResultUtil.error(ResultEnum.E0101);
            }
            FileResult fileResult = new FileResult();
            // 枚举获取
            CommonEnum.CosPaht cosPaht = EnumUtils.getEnumByCode(type, CommonEnum.CosPaht.class);
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(this.applicationConfig.getCosSecretId(), this.applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(this.applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);
            // 长度
            int length = file.getBytes().length;
            // 获取文件流
            InputStream byteArrayInputStream = file.getInputStream();
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);
            // 文件后缀名
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            // 保存目录
            String fileName = System.currentTimeMillis() + suffix;
            String key = cosPaht.getMessage() + fileName;
            fileResult.setPath(key);
            fileResult.setFileName(file.getOriginalFilename());
            PutObjectRequest putObjectRequest = new PutObjectRequest(this.applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
            String eTag = putObjectResult.getETag();
            log.info(eTag);
            // 关闭客户端
            cosClient.shutdown();
            fileResult.setUrl(this.urlAuth(key));
            return ResultUtil.success(fileResult);
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0101);
        }
    }

    public Result uploadWorkbook(Workbook workbook, String fileName) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(applicationConfig.getCosSecretId(), applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            byte[] barray = bos.toByteArray();
            int length = barray.length;

            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);

            // 保存目录
            String key = applicationConfig.getImportPaht() + fileName;

            PutObjectRequest putObjectRequest = new PutObjectRequest(applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return ResultUtil.success();
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0101);
        }
    }

    public Result uploadFileInvoice(ByteArrayOutputStream bos, String fileName) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(applicationConfig.getCosSecretId(), applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);

            byte[] barray = bos.toByteArray();
            int length = barray.length;

            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);

            // 保存目录
            String key = applicationConfig.getInvoicePath() + fileName;

            PutObjectRequest putObjectRequest = new PutObjectRequest(applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return ResultUtil.success(this.urlAuth(key));
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0102);
        }
    }

    public Result uploadFile(ByteArrayOutputStream bos, String fileName) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(applicationConfig.getCosSecretId(), applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);

            byte[] barray = bos.toByteArray();
            int length = barray.length;

            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);

            // 保存目录
            String key = applicationConfig.getExportPath() + fileName;

            PutObjectRequest putObjectRequest = new PutObjectRequest(applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return ResultUtil.success(this.urlAuth(key));
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0102);
        }
    }


    public String uploadFileByKey(ByteArrayOutputStream bos, String key) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(applicationConfig.getCosSecretId(), applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);

            byte[] barray = bos.toByteArray();
            int length = barray.length;

            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);

            PutObjectRequest putObjectRequest = new PutObjectRequest(applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return key;
        } catch (Exception e) {
           e.printStackTrace();
        }
        return null;
    }

    /**
     * 文件签名
     *
     * @param path
     * @return
     */
    public String urlAuth(String path) {
        if (StringUtils.isBlank(path)) {
            return null;
        }
        String secretId = applicationConfig.getCosSecretId();
        String secretKey = applicationConfig.getCosSecretKey();
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(applicationConfig.getCosRegion());
        ClientConfig clientConfig = new ClientConfig(region);
        // 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // 存储桶的命名格式为 BucketName-APPID，此处填写的存储桶名称必须为此格式
        String bucketName = applicationConfig.getCosBucket();
        String key = path.startsWith("/") ? path : "/" + path;
        GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);
        // 设置签名过期时间(可选), 若未进行设置, 则默认使用 ClientConfig 中的签名过期时间(1小时)
        // 这里设置签名在半个小时后过期
        Date expirationDate = new Date(System.currentTimeMillis() + 60L * 60L * 1000L * 24L);
        req.setExpiration(expirationDate);
        URL url = cosClient.generatePresignedUrl(req);
        log.info(url.toString());
        if (url.toString().toLowerCase().startsWith("http:")) {
            return url.toString().replace("http:", "https:");
        }
        return url.toString();
    }

    public String getFileUrl(String key) {
        COSCredentials cred = new BasicCOSCredentials(applicationConfig.getCosSecretId(), applicationConfig.getCosSecretKey());
        Region region = new Region(applicationConfig.getCosRegion());
        ClientConfig clientConfig = new ClientConfig(region);
        COSClient cosClient = new COSClient(cred, clientConfig);
        GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(applicationConfig.getCosBucket(), key, HttpMethodName.GET);
        Date expirationDate = new Date(System.currentTimeMillis() + 1800000L);
        req.setExpiration(expirationDate);
        URL url = cosClient.generatePresignedUrl(req);
        cosClient.shutdown();
        return url.toString();
    }

    public Result uploadInvoiceFile(ApplicationConfig applicationConfig, ByteArrayOutputStream bos, String fileName) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(applicationConfig.getCosSecretId(), applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);
            byte[] barray = bos.toByteArray();
            int length = barray.length;
            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);
            // 保存目录
            String key = applicationConfig.getInvoicePath() + fileName;
            PutObjectRequest putObjectRequest = new PutObjectRequest(applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return ResultUtil.success(this.urlAuth(key));
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0101);
        }
    }


    /**
     * 上传文件
     *
     * @param file 文件
     * @param path 远端路径（带文件名）
     * @return
     */
    public Result uploadFileToTargetPath(MultipartFile file, String path) {
        try {
            // 上传对象为空不存在
            if (file == null || StringUtils.isBlank(path)) {
                return ResultUtil.error(ResultEnum.E0101);
            }
            FileResult fileResult = new FileResult();
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(this.applicationConfig.getCosSecretId(), this.applicationConfig.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(this.applicationConfig.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);
            // 长度
            int length = file.getBytes().length;
            // 获取文件流
            InputStream byteArrayInputStream = file.getInputStream();
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);
            // 文件后缀名
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            // 保存目录
            //String fileName = System.currentTimeMillis() + suffix;
            String key = path + suffix;
            fileResult.setPath(key);
            fileResult.setFileName(file.getOriginalFilename());
            PutObjectRequest putObjectRequest = new PutObjectRequest(this.applicationConfig.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
            String eTag = putObjectResult.getETag();
            log.info(eTag);
            // 关闭客户端
            cosClient.shutdown();
            //url修改为拼接形式，不设置时效。
            fileResult.setUrl("https://"+applicationConfig.getCosBucket()+".cos."+ applicationConfig.getCosRegion()+".myqcloud.com/"+key);
            return ResultUtil.success(fileResult);
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0101);
        }
    }
}
