package com.gov.common.entity;

import lombok.Data;

/**
 * @author 123
 */
@Data
public class PageInput<T> {

    private Integer pageNo;

    private Integer pageSize;

    private T data;
}
