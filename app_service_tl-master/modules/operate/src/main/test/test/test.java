package test;

import com.gov.operate.OperateApplication;
import com.gov.operate.entity.GaiaAlPlQjmd;
import com.gov.operate.kylin.TestData;
import com.gov.operate.service.impl.Al_Pl_AnalyseImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author xiaoyuan on 2020/9/16
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes={OperateApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class test {

    @Autowired
    private Al_Pl_AnalyseImpl alPlAnalyse;

    @Test
    public void execAlPro(){
        List<TestData> tdList= alPlAnalyse.getDistinctIds();
        log.info("test{}",tdList);

        List<GaiaAlPlQjmd> qjmdList= alPlAnalyse.getSaleSummaryByArea("JSSZ");
        log.info("test1{}",qjmdList);
        HashMap<String,Object> item=new HashMap<>();
        List<String> compList=new ArrayList<>();
        compList.add("A0111");
        compList.add("A0112");
        compList.add("A0113");
        item.put("aplArea","JSSZ");
        item.put("compList",compList);
        qjmdList= alPlAnalyse.getSaleInfoByComp(item);
        log.info("test2{}",qjmdList);
    }
}
