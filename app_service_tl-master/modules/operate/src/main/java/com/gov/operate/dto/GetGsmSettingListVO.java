package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * getGsmSettingList接口入参
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetGsmSettingListVO extends Pageable {

    /**
     * 活动类型(0-主题活动，1-周期活动)
     */
    private String gsmType;

    /**
     * 活动开始时间
     */
    private String gsmStartd;

    /**
     * 活动结束时间
     */
    private String gsmEndd;

    /**
     * 活动名称
     */
    private String gsmThename;

    /**
     *
     * 状态(0:未推送，1:已推送)
     */
    private String gsmImpl;

}
