package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/16 10:17
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateInDrawerVO extends AddInDrawerVO {
    @NotBlank(message = "装斗单号不能为空")
    private String gsihVoucherId;


}
