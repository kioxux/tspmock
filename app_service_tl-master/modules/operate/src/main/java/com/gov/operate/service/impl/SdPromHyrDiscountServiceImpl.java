package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromHyrDiscount;
import com.gov.operate.mapper.SdPromHyrDiscountMapper;
import com.gov.operate.service.ISdPromHyrDiscountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Service
public class SdPromHyrDiscountServiceImpl extends ServiceImpl<SdPromHyrDiscountMapper, SdPromHyrDiscount> implements ISdPromHyrDiscountService {

}
