package com.gov.operate.dto.dataImport;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * copy from QueryProListByStoreDto
 * @author zhoushuai
 * @date 2021/4/9 10:54
 */
@Data
@EqualsAndHashCode
public class GetProByProCodeAndStoDTO {

    private String stoCode;

    private String stoName;

    private String proSelfCode;

    private String proCommonname;

    private String proSpecs;

    private String proFactoryName;

    private String proName;

    private String proLsj;

    /**
     * 促销价
     */
    private String gsphsPrice;
    /**
     * 促销折扣
      */
    private String gsphsRebate;
    /**
     * 是否积分
      */
    private String gsphsInteFlag;
    /**
     * 积分倍率
      */
    private String gsphsInteRate;


    /**
     * 会员日特价传回字段
     */
    private String gsphpPrice;
    private String gsphpRebate;
    private String gsphpInteFlag;
    private String gsphpInteRate;

}
