package com.gov.operate.service;

import com.gov.operate.entity.SdPromGiftConds;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-11
 */
public interface ISdPromGiftCondsService extends SuperService<SdPromGiftConds> {

}
