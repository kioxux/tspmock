package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING_BASIC_ATTAC")
@ApiModel(value="SdMarketingBasicAttac对象", description="")
public class SdMarketingBasicAttac extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "主表ID")
    @TableField("BASIC_ID")
    private Integer basicId;

    @ApiModelProperty(value = "宣传名称")
    @TableField("GSMBA_NAME")
    private String gsmbaName;

    @ApiModelProperty(value = "路径PS")
    @TableField("GSMBA_PS")
    private String gsmbaPs;

    @ApiModelProperty(value = "路径图片")
    @TableField("GSMBA_JPG")
    private String gsmbaJpg;

    @ApiModelProperty(value = "路径场景")
    @TableField("GSMBA_SCENES")
    private String gsmbaScenes;


}
