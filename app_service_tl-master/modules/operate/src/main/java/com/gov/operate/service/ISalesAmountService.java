package com.gov.operate.service;

import com.gov.operate.entity.SalesAmount;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-02-08
 */
public interface ISalesAmountService extends SuperService<SalesAmount> {

}
