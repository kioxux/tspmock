package com.gov.operate.dto;

import com.gov.common.entity.dataImport.PromSeriesProduct;
import com.gov.common.entity.dataImport.PromUnitaryProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromSeriesProductImportDto extends PromSeriesProduct {
    private String proSelfCode;

    private String proCommonname;

    private String proName;

    private String proSpecs;

    private String proFactoryName;
}
