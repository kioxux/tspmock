package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("GAIA_AL_PL_QJMD")
public class GaiaAlPlQjmd implements Serializable {
    private static final long serialVersionUID = -7285809243680071175L;
    /**
     * 模型
     */
    @TableField("APL_MODEL")
    private String aplModel;

    /**
     * 区域
     */
    @TableField("APL_AREA")
    private String aplArea;

    /**
     * 成分
     */
    @TableField("APL_COMP")
    private String aplComp;

    /**
     * 区间段编号
     */
    @TableField("APL_QJD_CODE")
    private String aplQjdCode;

    /**
     * 价格上限
     */
    @TableField("APL_PRICE_UP")
    private BigDecimal aplPriceUp;

    /**
     * 价格下限
     */
    @TableField("APL_PRICE_LOW")
    private BigDecimal aplPriceLow;

    /**
     * 销售量
     */
    @TableField("APL_SALES_QTY")
    private BigDecimal aplSalesQty;

    /**
     * 销售额
     */
    @TableField("APL_SALES_AMT")
    private BigDecimal aplSalesAmt;

    /**
     * 毛利额
     */
    @TableField("APL_GROSS_AMT")
    private BigDecimal aplGrossAmt;

    /**
     * 动销品种数
     */
    @TableField("APL_MS_PRO")
    private BigDecimal aplMsPro;

    /**
     * 动销小票计数
     */
    @TableField("APL_MS_BILL")
    private BigDecimal aplMsBill;

    /**
     * 每个品种的月动销门店数之和
     */
    @TableField("APL_MS_BRS")
    private BigDecimal aplMsBrs;

    /**
     * 每个品种的月动销天数之和
     */
    @TableField("APL_MS_DAYS")
    private BigDecimal aplMsDays;

    /**
     * 有效期起
     */
    @TableField("APL_DATE_FROM")
    private String aplDateFrom;

    /**
     * 有效期止
     */
    @TableField("APL_DATE_END")
    private String aplDateEnd;

    /**
     * 更新日期
     */
    @TableField("APL_UPDATE_DATE")
    private String aplUpdateDate;

    /**
     * 更新时间
     */
    @TableField("APL_UPDATE_TIME")
    private String aplUpdateTime;
}