package com.gov.operate.dto.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 钱金华
 * @date 21-2-18 14:24
 */
@Data
public class WorkOrderVo extends WorkOrder {

    /**
     * 类型名称
     */
    private String gwotName;

    /**
     * 接单时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime gwoOrderTime;

    /**
     * 提交时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime gwoSendTime;

    /**
     * 日期开始
     */
    private String gwoSendTimeStart;

    /**
     * 日期结束
     */
    private String gwoSendTimeEnd;

    /**
     * 接单人
     */
    private String gwoTakerName;

    /**
     * 工单处理记录
     */
    List<WorkOrderReplyVo> workOrderReplies;

    /**
     * 工单分类 0:全部工单 1:我发起的
     */
    private Integer orderType;

    /**
     * 间隔
     */
    private Integer interval;
    /**
     * 间隔单位
     */
    private String intervalType;

    /**
     * 排序方式
     */
    private String sortOrder;

    /**
     * 排序属性
     */
    private String sortColumn;

    /**
     * 关闭时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime gwoCloseTime;

    /**
     * 耗时
     */
    private Long timeConsuming;

    @ApiModelProperty(value = "服务态度")
    private Integer gwoServiceAttitudeScore;

    @ApiModelProperty(value = "解决速度")
    private Integer gwoSpeedSolutionScore;

    @ApiModelProperty(value = "系统是否好用")
    private Integer gwoEasyToUseScore;
}
