package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.UserChargeMaintenanceFree;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-17
 */
public interface IUserChargeMaintenanceFreeService extends SuperService<UserChargeMaintenanceFree> {

}
