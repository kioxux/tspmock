package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.PromDTO;
import com.gov.operate.entity.SdPromVariableTran;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
public interface ISdPromVariableTranService extends SuperService<SdPromVariableTran> {

    /**
     * 促销方式列表
     */
    List<PromDTO> getPromList(String marketId);
}
