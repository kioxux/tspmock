package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.storePayment.SmsSendStatistic;
import com.gov.operate.entity.SmsRechargeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
public interface SmsRechargeRecordMapper extends BaseMapper<SmsRechargeRecord> {

    /**
     * 客户发送统计
     *
     * @param page
     * @param client
     * @return
     */
    IPage<SmsSendStatistic> getSmsSendStatistic(Page<SmsSendStatistic> page, @Param("client") String client);

    /**
     * 已发件数
     *
     * @param client
     * @return
     */
    Integer getSmsSendCount(@Param("client") String client);

    /**
     * 客户充值记录
     *
     * @param page
     * @param client
     * @param gsrrRechargeTimeStart
     * @param gsrrRechargeTimeEnd
     * @return
     */
    IPage<SmsSendStatistic> getRechargeRecord(Page<SmsSendStatistic> page, @Param("client") String client,
                                              @Param("gsrrRechargeTimeStart") String gsrrRechargeTimeStart, @Param("gsrrRechargeTimeEnd") String gsrrRechargeTimeEnd);

}
