package com.gov.operate.service;

import com.gov.operate.entity.SdMarketingBasicAttac;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-02
 */
public interface ISdMarketingBasicAttacService extends SuperService<SdMarketingBasicAttac> {

}
