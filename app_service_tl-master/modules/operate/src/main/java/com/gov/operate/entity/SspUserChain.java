package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName GAIA_SSP_USER_CHAIN
 */
@TableName(value ="GAIA_SSP_USER_CHAIN")
@Data
public class SspUserChain implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 连锁总部
     */
    private String chainHead;

    /**
     * 主体名称
     */
    private String chainName;

    /**
     * 主体类型
     */
    private Integer chainType;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 有效期起
     */
    private Date expiryStartDate;

    /**
     * 有效期至
     */
    private Date expiryEndDate;


    @TableField(exist = false)
    private String creditCode;



    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}