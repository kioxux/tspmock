package com.gov.operate.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductMatchUserCheckRes implements Serializable {


    private static final long serialVersionUID = 8393613282597576056L;

    private String endDate;

    private Integer checkProNum;

    private Boolean clientFlag;//true为加盟商 false为门店


    private String stoCode;
}
