package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.entity.AuthorityRolesAuths;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface IAuthorityRolesAuthsService extends SuperService<AuthorityRolesAuths> {

    Result saveOrUpdateAuth(List<AuthorityRolesAuths> rolesAuthsList);

}
