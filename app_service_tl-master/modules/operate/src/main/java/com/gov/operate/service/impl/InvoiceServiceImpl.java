package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.*;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.ExportSelectWarehousingDetailsVO;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Resource
    private CommonService commonService;
    @Resource
    private IDcDataService dcDataService;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private ISupplierBusinessService supplierBusinessService;
    @Resource
    private InvoiceMapper invoiceMapper;
    @Resource
    private IFicoInvoiceInformationRegistrationService ficoInvoiceInformationRegistrationService;
    @Resource
    private FicoInvoiceInformationRegistrationMapper ficoInvoiceInformationRegistrationMapper;
    @Resource
    private IFiInvoiceRegistrationService fiInvoiceRegistrationService;
    @Resource
    private IFiInvoiceRegistrationDetailedService fiInvoiceRegistrationDetailedService;
    @Resource
    private PaymentDocumentNoApplicationsMapper paymentDocumentNoApplicationsMapper;
    @Resource
    private FiSupplierPrepaymentMapper fiSupplierPrepaymentMapper;
    @Resource
    private PaymentApplicationsMapper paymentApplicationsMapper;
    @Resource
    private IFiSupplierPrepaymentService iFiSupplierPrepaymentService;
    @Resource
    private BillInvoiceEndMapper billInvoiceEndMapper;
    @Resource
    private ClSystemParaMapper clSystemParaMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private DtSupplierPrepaymentMapper dtSupplierPrepaymentMapper;
    @Resource
    private IDtSupplierPrepaymentService dtSupplierPrepaymentService;
    @Resource
    private ChajiaZMapper chajiaZMapper;
    @Resource
    private ExportTaskMapper exportTaskMapper;
    @Resource
    private IExportTaskService iExportTaskService;
    @Resource
    private ApplicationConfig applicationConfig;

    /**
     * 采购地点
     */
    @Override
    public List<DcData> getPurcSite() {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<DcData> dcDataQueryWrapper = new QueryWrapper<DcData>();
        dcDataQueryWrapper.eq("CLIENT", user.getClient());
        dcDataQueryWrapper.apply("(DC_CHAIN_HEAD IS NOT NULL AND LENGTH(DC_CHAIN_HEAD) > 0)");
        List<DcData> list = dcDataService.list(dcDataQueryWrapper);
        return list;
    }

    /**
     * 供应商
     */
    @Override
    public List<SupplierBusiness> getSupplier(String site) {
        if (StringUtils.isEmpty(site)) {
            throw new CustomResultException("地点编码不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        List<SupplierBusiness> list = supplierBusinessService.list(new QueryWrapper<SupplierBusiness>()
                .select("SUP_SELF_CODE", "SUP_NAME", "SUP_BANK_NAME", "SUP_BANK_ACCOUNT", "SUP_PYM", "SUP_BANK_CODE")
                .eq("CLIENT", user.getClient())
                .eq("SUP_SITE", site));
        return list;
    }

    /**
     * 出入库查询
     */
    @Override
    public IPage<SelectWarehousingDTO> selectWarehousing(SelectWarehousingVO vo) {
        // 地点和单体店若只有一个有值，那么另一个赋特殊值”-1“
        if (!((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))
                || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
            if (StringUtils.isBlank(vo.getSite())) {
                vo.setSite("-1");
            } else {
                vo.setStoCode("-1");
            }
        }

        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(vo.getPageNum(), vo.getPageSize());
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = invoiceMapper.selectWarehousing(page, vo);
        selectWarehousingDTOS.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 剩余可登记金额 (计算得到，type为GD为负数--总额减已登记金额)
                BigDecimal multiply = totalAmount.abs().subtract(item.getRegisteredAmount().abs()).multiply(Constants.MINUS_ONE);
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
            }
        });
        return selectWarehousingDTOS;
    }

    /**
     * 出入库明细
     */
    @Override
    public List<SelectWarehousingDetailsDTO> selectWarehousingDetails(SelectWarehousingDetailsVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        List<SelectWarehousingDetailsDTO> list = invoiceMapper.selectWarehousingDetails(vo);
        list.forEach(item -> {
            // 已经登记的金额 若为Null设置为零
            if (ObjectUtils.isEmpty(item.getRegisteredAmount())) {
                item.setRegisteredAmount(new BigDecimal(0));
            }
            // 登记剩余金额
            BigDecimal chargeAmount = item.getChargeAmount();
            if (ObjectUtils.isEmpty(chargeAmount)) {
                item.setChargeAmount(item.getTotalAmount().subtract(item.getRegisteredAmount()));
            }
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                BigDecimal totalAmount = item.getTotalAmount();
                BigDecimal registeredAmount = item.getRegisteredAmount();

                // 税额
                item.setRateBat(item.getRateBat().abs().multiply(Constants.MINUS_ONE));
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(totalAmount.multiply(Constants.MINUS_ONE));

                // 剩余登记金额
                BigDecimal multiply = totalAmount.subtract(registeredAmount.abs()).multiply(Constants.MINUS_ONE);
                item.setChargeAmount(multiply);
            }
        });
        return list;
    }

    /**
     * 订单发票确认
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderInvoice(OrderInvoiceVO vo) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
        billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 1);
        billInvoiceEndQueryWrapper.eq("GBIE_NUM", vo.getMatDnId());
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
        if (!CollectionUtils.isEmpty(billInvoiceEndList)) {
            throw new CustomResultException("该单据已核销");
        }
        // 1>单据信息
        // 入库数据
        WmsDTO wmsDTO = invoiceMapper.getWmsRkys(user.getClient(), vo.getMatDnId());
        if (ObjectUtils.isEmpty(wmsDTO)) {
            wmsDTO = invoiceMapper.wmsTuiGong(user.getClient(), vo.getMatDnId());
        }
        if (ObjectUtils.isEmpty(wmsDTO)) {
            wmsDTO = invoiceMapper.getChajia(user.getClient(), vo.getMatDnId());
        }
        if (ObjectUtils.isEmpty(wmsDTO)) {
            throw new CustomResultException("该单据没有找到");
        }
        // 已经登记的金额 若为Null设置为零
        if (ObjectUtils.isEmpty(wmsDTO.getRegisteredAmount())) {
            wmsDTO.setRegisteredAmount(new BigDecimal(0));
        }
        // 登记剩余金额
        BigDecimal chargeAmount = wmsDTO.getChargeAmount();
        if (ObjectUtils.isEmpty(chargeAmount)) {
            wmsDTO.setChargeAmount(wmsDTO.getTotalAmount().subtract(wmsDTO.getRegisteredAmount()));
        }
        // 要登记的金额
        chargeAmount = wmsDTO.getChargeAmount().abs();
        String type = wmsDTO.getType();
        if (ObjectUtils.isEmpty(chargeAmount)) {
            throw new CustomResultException("剩余金额获取失败");
        }

        // 2>发票信息 (已经正序排序)
        List<FicoInvoiceInformationRegistrationDTO> invoiceList = ficoInvoiceInformationRegistrationMapper.getInvoiceList(user.getClient(), vo.getInvoiceNumList());
        if (CollectionUtils.isEmpty(invoiceList) || invoiceList.size() != vo.getInvoiceNumList().size()) {
            throw new CustomResultException("发票数据已更新，请重新选择");
        }

        // 验证当前订单是否预付过，如果订单已预付，则不可以选择已付款的发票
        QueryWrapper<FiSupplierPrepayment> fiSupplierPrepaymentQueryWrapper = new QueryWrapper<>();
        fiSupplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
        fiSupplierPrepaymentQueryWrapper.eq("MAT_DN_ID", vo.getMatDnId());
        List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectList(fiSupplierPrepaymentQueryWrapper);
        // 单据已预付
        if (!CollectionUtils.isEmpty(fiSupplierPrepaymentList)) {
            // 发票
            invoiceList.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                if (item.getInvoicePaidAmount() != null && item.getInvoicePaidAmount().compareTo(BigDecimal.ZERO) > 0) {
                    throw new CustomResultException("单据已预付，不可以选择已付款发票[" + item.getInvoiceNum() + "]");
                }
            }));
        }

        // 金额全部转化为绝对值
        invoiceList.forEach(item -> {
            // 核销验证
            QueryWrapper<BillInvoiceEnd> invoiceEndQueryWrapper = new QueryWrapper<>();
            invoiceEndQueryWrapper.eq("CLIENT", user.getClient());
            invoiceEndQueryWrapper.eq("GBIE_TYPE", 2);
            invoiceEndQueryWrapper.eq("GBIE_NUM", item.getInvoiceNum());
            List<BillInvoiceEnd> invoiceEndList = billInvoiceEndMapper.selectList(invoiceEndQueryWrapper);
            if (!CollectionUtils.isEmpty(invoiceEndList)) {
                throw new CustomResultException("发票[" + item.getInvoiceNum() + "]已核销");
            }
            // 发票去税金额
            item.setInvoiceAmountExcludingTax(item.getInvoiceAmountExcludingTax().abs());
            // 发票税额
            item.setInvoiceTax(item.getInvoiceTax().abs());
            // 剩余发票金额
            item.setInvoiceBalance(item.getInvoiceBalance().abs());
            // 发票已经登记金额
            item.setInvoiceRegisteredAmount(item.getInvoiceRegisteredAmount().abs());
        });
        // 3>发票金额求和 并判断
        BigDecimal invoiceSurplusTotal = invoiceList.stream().map(FicoInvoiceInformationRegistrationDTO::getInvoiceBalance).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (!(!ObjectUtils.isEmpty(invoiceSurplusTotal) && invoiceSurplusTotal.compareTo(chargeAmount) >= 0)) {
            throw new CustomResultException("发票金额不够，请到明细中确认订单明细金额");
        }

        // 4>找出一定会使用到的发票 // 发票信息表"已登记金额"更新
        List<InvoiceBalanceDTO> invoiceBalanceList = new ArrayList<>();
        BigDecimal invoiceBalanceTotal = BigDecimal.ZERO;
        for (int invoiceIndex = 0; invoiceIndex < invoiceList.size(); invoiceIndex++) {
            // 累加发票剩余金额
            invoiceBalanceTotal = invoiceBalanceTotal.add(invoiceList.get(invoiceIndex).getInvoiceBalance());
            if (invoiceBalanceTotal.compareTo(chargeAmount) > 0) {
                // 最后一张会使用到的发票 超过部分
                BigDecimal outstrip = invoiceBalanceTotal.subtract(chargeAmount);
                // 使用金额
                BigDecimal invoiceCurrentToRegister = invoiceList.get(invoiceIndex).getInvoiceBalance().subtract(outstrip);
                if (invoiceCurrentToRegister.compareTo(BigDecimal.ZERO) > 0) {
                    // 发票号
                    String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
                    // 该发票已经登记的金额
                    BigDecimal invoiceHasRegistered = invoiceList.get(invoiceIndex).getInvoiceRegisteredAmount();
                    // 本次登记后 该发票的已登记金额(之前的+本次使用到的)
                    BigDecimal invoiceRegistered = invoiceHasRegistered.add(invoiceCurrentToRegister);
                    // 如果退厂 则金额为负数
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                    }
                    invoiceBalanceList.add(new InvoiceBalanceDTO(invoiceNum, invoiceCurrentToRegister));
                    // 保存发票登记数据
                    ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                            .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                            .set("INVOICE_REGISTRATION_STATUS", "1")
                            .eq("CLIENT", user.getClient())
                            .eq("INVOICE_NUM", invoiceNum));
                }
                break;
            }
            // 发票号
            String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
            // 本次登记操作后发票的已登记金额 (前面几张发票 剩余金额全部使用：登记金额等于发票总额)
            BigDecimal invoiceRegistered = invoiceList.get(invoiceIndex).getInvoiceTotalInvoiceAmount();
            BigDecimal invoiceBalance = invoiceList.get(invoiceIndex).getInvoiceBalance();
            if ("GD".equals(type) || "CJ".equals(type)) {
                invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
            }
            invoiceBalanceList.add(new InvoiceBalanceDTO(invoiceNum, invoiceBalance));
            // 保存发票登记金额数据
            ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                    // 发票已登记金额 直接更新为发票总额
                    .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                    .set("INVOICE_REGISTRATION_STATUS", "1")
                    .eq("CLIENT", user.getClient())
                    .eq("INVOICE_NUM", invoiceNum));
        }

        // 5>发票登记主表数据
        List<FiInvoiceRegistration> registrationList = new ArrayList<>();
        for (InvoiceBalanceDTO invoice : invoiceBalanceList) {
            // 判断该记录是否还在 若存在则更新 若不存在则新增 (主键: 加盟商+业务类型+业务单号+发票号码)
            QueryWrapper<FiInvoiceRegistration> query = new QueryWrapper<FiInvoiceRegistration>()
                    .eq("CLIENT", user.getClient())
                    .eq("BUSINESS_TYPE", type)
                    .eq("MAT_DN_ID", vo.getMatDnId())
                    .eq("INVOICE_NUM", invoice.getInvoiceNum());
            FiInvoiceRegistration invoiceRegistration = fiInvoiceRegistrationService.getOne(query);
            if (!ObjectUtils.isEmpty(invoiceRegistration)) {
                // 更新
                FiInvoiceRegistration updateDO = new FiInvoiceRegistration();
                // 已登记金额
                BigDecimal invoiceRegistered = invoice.getInvoiceRegistered().abs();
                updateDO.setAmountOfDocumentRegistration((invoiceRegistration.getAmountOfDocumentRegistration() == null ? BigDecimal.ZERO : invoiceRegistration.getAmountOfDocumentRegistration().abs()).add(invoiceRegistered));
                if ("GD".equals(type) || "CJ".equals(type)) {
                    updateDO.setAmountOfDocumentRegistration(updateDO.getAmountOfDocumentRegistration().multiply(Constants.MINUS_ONE));
                }
                fiInvoiceRegistrationService.update(updateDO, query);
            } else {
                // 新增
                FiInvoiceRegistration insertDO = new FiInvoiceRegistration();
                insertDO.setClient(user.getClient());
                insertDO.setBusinessType(type);
                // 订单单号
                insertDO.setMatDnId(vo.getMatDnId());
                // 发票号
                insertDO.setInvoiceNum(invoice.getInvoiceNum());
                // 供应商 订单的供应商
                insertDO.setPoSupplierId(wmsDTO.getWmGysBh());
                // 地点 订单的地点
                insertDO.setSiteCode(wmsDTO.getProSite());
                // 单据总金额
                BigDecimal totalAmount = wmsDTO.getTotalAmount();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    totalAmount = totalAmount.abs().multiply(Constants.MINUS_ONE);
                }
                insertDO.setDocumentAmount(totalAmount);
                // 已登记金额
                BigDecimal invoiceRegistered = invoice.getInvoiceRegistered();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                }
                insertDO.setAmountOfDocumentRegistration(invoiceRegistered);
                // 已付款金额
                insertDO.setPaymentAmountOfDocuments(BigDecimal.ZERO);
                insertDO.setCreationDate(DateUtils.getCurrentDateStrYYMMDD());
                insertDO.setFounder(user.getUserId());
                registrationList.add(insertDO);
            }
        }
        if (!CollectionUtils.isEmpty(registrationList)) {
            fiInvoiceRegistrationService.saveBatch(registrationList);
        }
        // 6>发票登记从表数据
        List<FiInvoiceRegistrationDetailed> registrationDetailedList = new ArrayList<>();
        // 单据明细
        SelectWarehousingDetailsVO detailsVO = new SelectWarehousingDetailsVO();
        detailsVO.setClient(user.getClient());
        detailsVO.setMatDnId(vo.getMatDnId());
        detailsVO.setType(wmsDTO.getType());
        List<SelectWarehousingDetailsDTO> detailsDTOS = invoiceMapper.selectWarehousingDetails(detailsVO);
        for (SelectWarehousingDetailsDTO details : detailsDTOS) {
            QueryWrapper detailsQuery = new QueryWrapper<FiInvoiceRegistrationDetailed>()
                    .eq("CLIENT", user.getClient())
                    .eq("BUSINESS_TYPE", details.getType())
                    .eq("MAT_DN_ID", details.getMatDnId())
                    .eq("MAT_ID", details.getMatId())
                    .eq("MAT_YEAR", details.getMatYear())
                    .eq("MAT_LINE_NO", details.getMatLineNo());
            FiInvoiceRegistrationDetailed invoiceRegistrationDetailed = fiInvoiceRegistrationDetailedService.getOne(detailsQuery);

            // 行金额
            BigDecimal totalAmount = details.getTotalAmount();
            if ("GD".equals(type) || "CJ".equals(type)) {
                totalAmount = totalAmount.multiply(Constants.MINUS_ONE);
            }
            if (ObjectUtils.isEmpty(invoiceRegistrationDetailed)) {
                // 新增
                FiInvoiceRegistrationDetailed registrationDetailed = new FiInvoiceRegistrationDetailed();
                registrationDetailed.setClient(user.getClient());
                registrationDetailed.setBusinessType(details.getType());
                registrationDetailed.setMatDnId(details.getMatDnId());
                registrationDetailed.setMatId(details.getMatId());
                registrationDetailed.setMatYear(details.getMatYear());
                registrationDetailed.setMatLineNo(details.getMatLineNo());
                registrationDetailed.setSoProCode(details.getProCode());
                // 行金额
                registrationDetailed.setLineAmount(totalAmount);
                // 行登记金额
                registrationDetailed.setLineRegistrationAmount(totalAmount);
                registrationDetailed.setPaymentAmountOfLine(BigDecimal.ZERO);
                registrationDetailedList.add(registrationDetailed);
            } else {
                // 更新
                fiInvoiceRegistrationDetailedService.update(new UpdateWrapper<FiInvoiceRegistrationDetailed>()
                        .set("LINE_REGISTRATION_AMOUNT", totalAmount)
                        .eq("CLIENT", user.getClient())
                        .eq("BUSINESS_TYPE", details.getType())
                        .eq("MAT_DN_ID", details.getMatDnId())
                        .eq("MAT_ID", details.getMatId())
                        .eq("MAT_YEAR", details.getMatYear())
                        .eq("MAT_LINE_NO", details.getMatLineNo()));
            }
        }
        if (!CollectionUtils.isEmpty(registrationDetailedList)) {
            fiInvoiceRegistrationDetailedService.saveBatch(registrationDetailedList);
        }
        // 差价单-已记账
        resChajia(user.getClient(), type, vo.getMatDnId());
    }

    /**
     * 订单明细发票确认
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderDetailInvoice(OrderDetailInvoiceVO vo) {
        TokenUser user = commonService.getLoginInfo();
        String type = vo.getOrderDetailList().get(0).getType();
        List<OrderDetailInvoiceVO.OrderDetail> orderDetailList = vo.getOrderDetailList();
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
        billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 1);
        billInvoiceEndQueryWrapper.eq("GBIE_NUM", vo.getMatDnId());
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
        if (!CollectionUtils.isEmpty(billInvoiceEndList)) {
            throw new CustomResultException("该单据已核销:" + vo.getMatDnId());
        }
        // 单据列表
        WmsDTO wmsDTO = invoiceMapper.getWmsRkys(user.getClient(), vo.getMatDnId());
        if (ObjectUtils.isEmpty(wmsDTO)) {
            wmsDTO = invoiceMapper.wmsTuiGong(user.getClient(), vo.getMatDnId());
        }
        if (ObjectUtils.isEmpty(wmsDTO)) {
            wmsDTO = invoiceMapper.getChajia(user.getClient(), vo.getMatDnId());
        }
        if (ObjectUtils.isEmpty(wmsDTO)) {
            throw new CustomResultException("该单据没有找到:" + vo.getMatDnId());
        }

        // 总金额 明细对账用
        BigDecimal totalAmt = vo.getTotalAmt();

        // 1>校验总额度是否满足
        // 本次需登记总额 /取绝对值
        BigDecimal amtTotal = orderDetailList.stream().map(OrderDetailInvoiceVO.OrderDetail::getAmt).reduce(BigDecimal.ZERO, BigDecimal::add).abs();
        // 发票集合
        List<FicoInvoiceInformationRegistrationDTO> invoiceList = ficoInvoiceInformationRegistrationMapper.getInvoiceList(user.getClient(), vo.getInvoiceNumList());
        if (CollectionUtils.isEmpty(invoiceList)) {
            throw new CustomResultException("发票编号有误，未找到发票");
        }
        // 金额全部转化为绝对值
        invoiceList.forEach(item -> {
            // 核销验证
            QueryWrapper<BillInvoiceEnd> invoiceEndQueryWrapper = new QueryWrapper<>();
            invoiceEndQueryWrapper.eq("CLIENT", user.getClient());
            invoiceEndQueryWrapper.eq("GBIE_TYPE", 2);
            invoiceEndQueryWrapper.eq("GBIE_NUM", item.getInvoiceNum());
            List<BillInvoiceEnd> invoiceEndList = billInvoiceEndMapper.selectList(invoiceEndQueryWrapper);
            if (!CollectionUtils.isEmpty(invoiceEndList)) {
                throw new CustomResultException("发票[" + item.getInvoiceNum() + "]已核销");
            }

            // 发票去税金额
            item.setInvoiceAmountExcludingTax(item.getInvoiceAmountExcludingTax().abs());
            // 发票税额
            item.setInvoiceTax(item.getInvoiceTax().abs());
            // 剩余发票金额
            item.setInvoiceBalance(item.getInvoiceBalance().abs());
            // 发票已经登记金额
            item.setInvoiceRegisteredAmount(item.getInvoiceRegisteredAmount().abs());
        });
        // 3>发票集合 剩余可使用金额求和 并判断
        BigDecimal invoiceSurplusTotal = invoiceList.stream().map(FicoInvoiceInformationRegistrationDTO::getInvoiceBalance).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (!(!ObjectUtils.isEmpty(invoiceSurplusTotal) && invoiceSurplusTotal.compareTo(amtTotal) >= 0) && totalAmt == null) {
            throw new CustomResultException("发票金额不够");
        }

        // 验证当前订单是否预付过，如果订单已预付，则不可以选择已付款的发票
        QueryWrapper<FiSupplierPrepayment> fiSupplierPrepaymentQueryWrapper = new QueryWrapper<>();
        fiSupplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
        fiSupplierPrepaymentQueryWrapper.eq("MAT_DN_ID", vo.getMatDnId());
        List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectList(fiSupplierPrepaymentQueryWrapper);
        // 单据已预付
        if (!CollectionUtils.isEmpty(fiSupplierPrepaymentList)) {
            // 发票
            invoiceList.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                if (item.getInvoicePaidAmount() != null && item.getInvoicePaidAmount().compareTo(BigDecimal.ZERO) > 0) {
                    throw new CustomResultException("单据已预付，不可以选择已付款发票[" + item.getInvoiceNum() + "]");
                }
            }));
        }

        // 4>找出一定会使用到的发票 // 发票信息表"已登记金额"更新
        List<InvoiceBalanceDTO> invoiceBalanceList = new ArrayList<>();
        BigDecimal invoiceBalanceTotal = BigDecimal.ZERO;
        for (int invoiceIndex = 0; invoiceIndex < invoiceList.size(); invoiceIndex++) {
            // 累加发票剩余金额
            invoiceBalanceTotal = invoiceBalanceTotal.add(invoiceList.get(invoiceIndex).getInvoiceBalance());
            if (invoiceBalanceTotal.compareTo(amtTotal) > 0) {
                // 最后一张会使用到的发票 超过部分
                BigDecimal outstrip = invoiceBalanceTotal.subtract(amtTotal);
                // 使用金额
                BigDecimal invoiceCurrentToRegister = invoiceList.get(invoiceIndex).getInvoiceBalance().subtract(outstrip);
                if (invoiceCurrentToRegister.compareTo(BigDecimal.ZERO) > 0) {
                    // 发票号
                    String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
                    // 该发票已经登记的金额
                    BigDecimal invoiceHasRegistered = invoiceList.get(invoiceIndex).getInvoiceRegisteredAmount();
                    // 本次登记后 该发票的已登记金额(之前的+本次使用到的)
                    BigDecimal invoiceRegistered = invoiceHasRegistered.add(invoiceCurrentToRegister);
                    // 如果退厂 则金额为负数
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                    }
                    invoiceBalanceList.add(new InvoiceBalanceDTO(invoiceNum, invoiceCurrentToRegister));
                    // 保存发票登记数据
                    ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                            .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                            .set("INVOICE_REGISTRATION_STATUS", "1")
                            .eq("CLIENT", user.getClient())
                            .eq("INVOICE_NUM", invoiceNum));
                }
                break;
            }
            // 发票号
            String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
            // 本次登记操作后发票的已登记金额 (前面几张发票 剩余金额全部使用：登记金额等于发票总额)
            BigDecimal invoiceRegistered = invoiceList.get(invoiceIndex).getInvoiceTotalInvoiceAmount();
            BigDecimal invoiceBalance = invoiceList.get(invoiceIndex).getInvoiceBalance();
            if ("GD".equals(type) || "CJ".equals(type)) {
                invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
            }
            invoiceBalanceList.add(new InvoiceBalanceDTO(invoiceNum, invoiceBalance));
            // 保存发票登记金额数据
            ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                    // 发票已登记金额 直接更新为发票总额
                    .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                    .set("INVOICE_REGISTRATION_STATUS", "1")
                    .eq("CLIENT", user.getClient())
                    .eq("INVOICE_NUM", invoiceNum));
        }

        // 5>发票登记主表数据
        List<FiInvoiceRegistration> registrationList = new ArrayList<>();
        for (InvoiceBalanceDTO invoice : invoiceBalanceList) {
            // 判断该记录是否还在 若存在则更新 若不存在则新增 (主键: 加盟商+业务类型+业务单号+发票号码)
            QueryWrapper<FiInvoiceRegistration> query = new QueryWrapper<FiInvoiceRegistration>()
                    .eq("CLIENT", user.getClient())
                    .eq("BUSINESS_TYPE", type)
                    .eq("MAT_DN_ID", vo.getMatDnId())
                    .eq("INVOICE_NUM", invoice.getInvoiceNum());
            FiInvoiceRegistration invoiceRegistration = fiInvoiceRegistrationService.getOne(query);
            if (!ObjectUtils.isEmpty(invoiceRegistration)) {
                // 更新
                FiInvoiceRegistration updateDO = new FiInvoiceRegistration();
                // 已登记金额
                BigDecimal invoiceRegistered = invoice.getInvoiceRegistered().abs();
                updateDO.setAmountOfDocumentRegistration((invoiceRegistration.getAmountOfDocumentRegistration() == null ? BigDecimal.ZERO : invoiceRegistration.getAmountOfDocumentRegistration().abs()).add(invoiceRegistered));
                if ("GD".equals(type) || "CJ".equals(type)) {
                    updateDO.setAmountOfDocumentRegistration(updateDO.getAmountOfDocumentRegistration().multiply(Constants.MINUS_ONE));
                }
                fiInvoiceRegistrationService.update(updateDO, query);
            } else {
                // 新增
                FiInvoiceRegistration insertDO = new FiInvoiceRegistration();
                insertDO.setClient(user.getClient());
                insertDO.setBusinessType(type);
                // 订单单号
                insertDO.setMatDnId(vo.getMatDnId());
                // 发票号
                insertDO.setInvoiceNum(invoice.getInvoiceNum());
                // 供应商 订单的供应商
                insertDO.setPoSupplierId(wmsDTO.getWmGysBh());
                // 地点 订单的地点
                insertDO.setSiteCode(wmsDTO.getProSite());

                // 单据总金额
                BigDecimal totalAmount = wmsDTO.getTotalAmount();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    totalAmount = totalAmount.abs().multiply(Constants.MINUS_ONE);
                }
                insertDO.setDocumentAmount(totalAmount);

                // 已登记金额
                BigDecimal invoiceRegistered = invoice.getInvoiceRegistered();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                }
                insertDO.setAmountOfDocumentRegistration(invoiceRegistered);
                // 已付款金额
                insertDO.setPaymentAmountOfDocuments(BigDecimal.ZERO);
                insertDO.setCreationDate(DateUtils.getCurrentDateStrYYMMDD());
                insertDO.setFounder(user.getUserId());
                registrationList.add(insertDO);
            }
        }
        if (!CollectionUtils.isEmpty(registrationList)) {
            fiInvoiceRegistrationService.saveBatch(registrationList);
        }

        // 6>发票登记从表数据
        SelectWarehousingDetailsVO detailsVO = new SelectWarehousingDetailsVO();
        detailsVO.setClient(user.getClient());
        detailsVO.setMatDnId(vo.getMatDnId());
        List<SelectWarehousingDetailsDTO> warehousingDetailsDTOList = invoiceMapper.selectWarehousingDetails(detailsVO);
        List<FiInvoiceRegistrationDetailed> registrationDetailedList = new ArrayList<>();
        for (OrderDetailInvoiceVO.OrderDetail details : orderDetailList) {
            // 从物料明细表查询出行金额 以及 商品名
            SelectWarehousingDetailsDTO detailsDTO = warehousingDetailsDTOList.stream().filter(item -> {
                boolean a = details.getMatId().equals(item.getMatId());
                boolean b = details.getType().equals(item.getType());
                boolean c = details.getMatLineNo().equals(item.getMatLineNo());
                boolean d = details.getMatYear().equals(item.getMatYear());
                boolean e = vo.getMatDnId().equals(item.getMatDnId());
                return a && b && c && d && e;
            }).findFirst().get();
            // 查询该明细是否存在,存在则更新，不存在则新增
            QueryWrapper detailsQuery = new QueryWrapper<FiInvoiceRegistrationDetailed>()
                    .eq("CLIENT", user.getClient())
                    .eq("BUSINESS_TYPE", details.getType())
                    .eq("MAT_DN_ID", vo.getMatDnId())
                    .eq("MAT_ID", details.getMatId())
                    .eq("MAT_YEAR", details.getMatYear())
                    .eq("MAT_LINE_NO", details.getMatLineNo());
            FiInvoiceRegistrationDetailed invoiceRegistrationDetailed = fiInvoiceRegistrationDetailedService.getOne(detailsQuery);
            if (ObjectUtils.isEmpty(invoiceRegistrationDetailed)) {
                // 新增
                FiInvoiceRegistrationDetailed registrationDetailed = new FiInvoiceRegistrationDetailed();
                registrationDetailed.setClient(user.getClient());
                registrationDetailed.setBusinessType(details.getType());
                registrationDetailed.setMatDnId(vo.getMatDnId());
                registrationDetailed.setMatId(details.getMatId());
                registrationDetailed.setMatYear(details.getMatYear());
                registrationDetailed.setMatLineNo(details.getMatLineNo());
                registrationDetailed.setSoProCode(details.getProCode());
                // 行金额 (来自物料凭证表)
                BigDecimal totalAmount = detailsDTO.getTotalAmount();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    totalAmount = totalAmount.abs().multiply(Constants.MINUS_ONE);
                }
                registrationDetailed.setLineAmount(totalAmount);
                // 行登记金额 (前端页面传值)
                BigDecimal amt = details.getAmt();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    amt = amt.abs().multiply(Constants.MINUS_ONE);
                }
                registrationDetailed.setLineRegistrationAmount(amt);
                // 行已付款金额
                registrationDetailed.setPaymentAmountOfLine(BigDecimal.ZERO);
                if (registrationDetailed.getLineRegistrationAmount().abs().compareTo(registrationDetailed.getLineAmount().abs()) > 0) {
                    throw new CustomResultException(MessageFormat.format("单据{0}：[{1}]行登记金额不能大于行金额",
                            vo.getMatDnId(), detailsDTO.getProName()));
                }
                registrationDetailedList.add(registrationDetailed);
            } else {
                // 更新
                // 本次登记之前 已经登记的金额
                BigDecimal lineRegistrationAmountPre = invoiceRegistrationDetailed.getLineRegistrationAmount().abs();
                // 本次登记之后 已经登记的金额
                BigDecimal lineRegistrationAmountCurrent = lineRegistrationAmountPre.add(details.getAmt().abs()).abs();
                // 登记金额 不能大于 行总金额
                if (lineRegistrationAmountCurrent.compareTo(invoiceRegistrationDetailed.getLineAmount().abs()) > 0) {
                    throw new CustomResultException(MessageFormat.format("单据{0}：[{1}]行登记金额不能大于行金额",
                            vo.getMatDnId(), detailsDTO.getProName()));
                }
                if ("GD".equals(type) || "CJ".equals(type)) {
                    lineRegistrationAmountCurrent = lineRegistrationAmountCurrent.abs().multiply(Constants.MINUS_ONE);
                }
                fiInvoiceRegistrationDetailedService.update(new UpdateWrapper<FiInvoiceRegistrationDetailed>()
                        .set("LINE_REGISTRATION_AMOUNT", lineRegistrationAmountCurrent)
                        .eq("CLIENT", user.getClient())
                        .eq("BUSINESS_TYPE", details.getType())
                        .eq("MAT_DN_ID", vo.getMatDnId())
                        .eq("MAT_ID", details.getMatId())
                        .eq("MAT_YEAR", details.getMatYear())
                        .eq("MAT_LINE_NO", details.getMatLineNo()));
            }
        }
        if (!CollectionUtils.isEmpty(registrationDetailedList)) {
            fiInvoiceRegistrationDetailedService.saveBatch(registrationDetailedList);
        }
        // 差价单-已记账
        resChajia(user.getClient(), type, vo.getMatDnId());
    }

    /**
     * 明细确认
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderDetailInvoice2(List<OrderDetailInvoiceVO> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("请选择登记数据");
        }
        for (OrderDetailInvoiceVO vo : list) {
            orderDetailInvoice(vo);
        }
    }

    /**
     * 订单发票批量确认
     *
     * @param vo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderBatchInvoice(OrderInvoiceVO vo) {
        // 发票未选择 or 业务单未选择
        if (CollectionUtils.isEmpty(vo.getInvoiceNumList())) {
            throw new CustomResultException("请选择登记的发票");
        }
        if (CollectionUtils.isEmpty(vo.getMatDnIdList())) {
            throw new CustomResultException("请选择登记的单据");
        }
        TokenUser user = commonService.getLoginInfo();
        // 单据信息
        List<WmsDTO> wmsDTOCDList = new ArrayList<>();
        List<WmsDTO> wmsDTOGDList = new ArrayList<>();
        BigDecimal chargeAmountCDSum = BigDecimal.ZERO;
        BigDecimal chargeAmountGDSum = BigDecimal.ZERO;
        for (String matDnId : vo.getMatDnIdList()) {
            // 1>单据信息
            // 入库数据
            WmsDTO wmsDTO = invoiceMapper.getWmsRkys(user.getClient(), matDnId);
            if (ObjectUtils.isEmpty(wmsDTO)) {
                wmsDTO = invoiceMapper.wmsTuiGong(user.getClient(), matDnId);
            }
            if (ObjectUtils.isEmpty(wmsDTO)) {
                throw new CustomResultException("单号" + matDnId + "数据没有找到");
            }
            wmsDTO.setMatDnId(matDnId);
            // 已经登记的金额 若为Null设置为零
            if (ObjectUtils.isEmpty(wmsDTO.getRegisteredAmount())) {
                wmsDTO.setRegisteredAmount(new BigDecimal(0));
            }
            // 登记剩余金额
            BigDecimal chargeAmount = wmsDTO.getChargeAmount();
            if (ObjectUtils.isEmpty(chargeAmount)) {
                wmsDTO.setChargeAmount(wmsDTO.getTotalAmount().subtract(wmsDTO.getRegisteredAmount()));
            }
            // 要登记的金额
            chargeAmount = wmsDTO.getChargeAmount().abs();
            String type = wmsDTO.getType();

            if (ObjectUtils.isEmpty(chargeAmount)) {
                throw new CustomResultException("单号" + matDnId + "剩余金额获取失败");
            }
            if ("CD".equals(type)) {
                wmsDTOCDList.add(wmsDTO);
                chargeAmountCDSum = chargeAmountCDSum.add(chargeAmount);
            }
            if ("GD".equals(type) || "CJ".equals(type)) {
                wmsDTOGDList.add(wmsDTO);
                chargeAmountGDSum = chargeAmountGDSum.add(chargeAmount);
            }
        }
        // 2>发票信息 (已经正序排序)
        List<FicoInvoiceInformationRegistrationDTO> invoiceList = ficoInvoiceInformationRegistrationMapper.getInvoiceList(user.getClient(), vo.getInvoiceNumList());
        if (CollectionUtils.isEmpty(invoiceList) || invoiceList.size() != vo.getInvoiceNumList().size()) {
            throw new CustomResultException("发票数据已更新，请重新选择");
        }
        // CD 发票
        List<FicoInvoiceInformationRegistrationDTO> invoiceCDList = invoiceList.stream().filter(item -> item.getInvoiceBalance().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
        // GC 发票
        List<FicoInvoiceInformationRegistrationDTO> invoiceGDList = invoiceList.stream().filter(item -> item.getInvoiceBalance().compareTo(BigDecimal.ZERO) < 0).collect(Collectors.toList());
        // CD GD 发票 单据 对应判断
        if ((!CollectionUtils.isEmpty(wmsDTOCDList) && CollectionUtils.isEmpty(invoiceCDList))) {
            throw new CustomResultException("采购单据和发票不匹配");
        }
        // CD GD 发票 单据 对应判断
        if ((!CollectionUtils.isEmpty(wmsDTOGDList) && CollectionUtils.isEmpty(invoiceGDList))) {
            throw new CustomResultException("退厂单据和发票不匹配");
        }
        invoice(invoiceCDList, wmsDTOCDList, chargeAmountCDSum, "CD");
        invoice(invoiceGDList, wmsDTOGDList, chargeAmountGDSum, "GD");
    }

    private void invoice(List<FicoInvoiceInformationRegistrationDTO> invoiceList, List<WmsDTO> wmsDTOList, BigDecimal chargeAmountSum, String type) {
        if (CollectionUtils.isEmpty(invoiceList)) {
            return;
        }
        if (CollectionUtils.isEmpty(wmsDTOList)) {
            return;
        }
        TokenUser user = commonService.getLoginInfo();
        // 金额全部转化为绝对值
        invoiceList.forEach(item -> {
            // 发票去税金额
            item.setInvoiceAmountExcludingTax(item.getInvoiceAmountExcludingTax().abs());
            // 发票税额
            item.setInvoiceTax(item.getInvoiceTax().abs());
            // 剩余发票金额
            item.setInvoiceBalance(item.getInvoiceBalance().abs());
            // 发票已经登记金额
            item.setInvoiceRegisteredAmount(item.getInvoiceRegisteredAmount().abs());
        });
        // 3>发票金额求和 并判断
        BigDecimal invoiceSurplusTotal = invoiceList.stream().map(FicoInvoiceInformationRegistrationDTO::getInvoiceBalance).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (!(!ObjectUtils.isEmpty(invoiceSurplusTotal) && invoiceSurplusTotal.compareTo(chargeAmountSum) >= 0)) {
            throw new CustomResultException(("CD".equals(type) ? "采购" : "退厂") + "发票金额不够，请到明细中登记发票");
        }
        // 4>找出一定会使用到的发票 // 发票信息表"已登记金额"更新
        List<InvoiceBalanceDTO> invoiceBalanceList = new ArrayList<>();
        BigDecimal invoiceBalanceTotal = BigDecimal.ZERO;
        for (int invoiceIndex = 0; invoiceIndex < invoiceList.size(); invoiceIndex++) {
            // 累加发票剩余金额
            invoiceBalanceTotal = invoiceBalanceTotal.add(invoiceList.get(invoiceIndex).getInvoiceBalance());
            if (invoiceBalanceTotal.compareTo(chargeAmountSum) > 0) {
                // 最后一张会使用到的发票 超过部分
                BigDecimal outstrip = invoiceBalanceTotal.subtract(chargeAmountSum);
                // 使用金额
                BigDecimal invoiceCurrentToRegister = invoiceList.get(invoiceIndex).getInvoiceBalance().subtract(outstrip);
                if (invoiceCurrentToRegister.compareTo(BigDecimal.ZERO) > 0) {
                    // 发票号
                    String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
                    // 该发票已经登记的金额
                    BigDecimal invoiceHasRegistered = invoiceList.get(invoiceIndex).getInvoiceRegisteredAmount();
                    // 本次登记后 该发票的已登记金额(之前的+本次使用到的)
                    BigDecimal invoiceRegistered = invoiceHasRegistered.add(invoiceCurrentToRegister);
                    // 如果退厂 则金额为负数
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                    }
                    invoiceBalanceList.add(new InvoiceBalanceDTO(invoiceNum, invoiceCurrentToRegister));
                    // 保存发票登记数据
                    ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                            .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                            .set("INVOICE_REGISTRATION_STATUS", "1")
                            .eq("CLIENT", user.getClient())
                            .eq("INVOICE_NUM", invoiceNum));
                }
                break;
            }
            // 发票号
            String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
            // 本次登记操作后发票的已登记金额 (前面几张发票 剩余金额全部使用：登记金额等于发票总额)
            BigDecimal invoiceRegistered = invoiceList.get(invoiceIndex).getInvoiceTotalInvoiceAmount();
            BigDecimal invoiceBalance = invoiceList.get(invoiceIndex).getInvoiceBalance();
            if ("GD".equals(type) || "CJ".equals(type)) {
                invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
            }
            invoiceBalanceList.add(new InvoiceBalanceDTO(invoiceNum, invoiceBalance));
            // 保存发票登记金额数据
            ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                    // 发票已登记金额 直接更新为发票总额
                    .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                    .set("INVOICE_REGISTRATION_STATUS", "1")
                    .eq("CLIENT", user.getClient())
                    .eq("INVOICE_NUM", invoiceNum));
        }

        // 5>发票登记主表数据
        List<FiInvoiceRegistration> registrationList = new ArrayList<>();
//        for (InvoiceBalanceDTO invoice : invoiceBalanceList) {
        int globalIndex = 0;
        for (WmsDTO wmsDTO : wmsDTOList) {// 单据总金额
            BigDecimal billAmount = wmsDTO.getTotalAmount();
            // 单据发票登记
            for (int i = globalIndex; i < invoiceList.size(); i++) {
                FicoInvoiceInformationRegistrationDTO ficoInvoiceInformationRegistrationDTO = invoiceList.get(i);
                if (ficoInvoiceInformationRegistrationDTO.getInvoiceBalance().compareTo(BigDecimal.ZERO) <= 0) {
                    continue;
                }
                // 单据总金额
                BigDecimal chargeAmount = wmsDTO.getChargeAmount();
                if (chargeAmount.compareTo(BigDecimal.ZERO) == 0) {
                    break;
                }
                // 发票剩余金额
                BigDecimal invoiceBalance = ficoInvoiceInformationRegistrationDTO.getInvoiceBalance();
                // 登记金额
                BigDecimal registrationAmt = BigDecimal.ZERO;
                if (chargeAmount.compareTo(invoiceBalance) >= 0) {
                    registrationAmt = invoiceBalance;
                    ficoInvoiceInformationRegistrationDTO.setInvoiceBalance(BigDecimal.ZERO);
                    wmsDTO.setChargeAmount(chargeAmount.subtract(invoiceBalance));
                } else {
                    registrationAmt = chargeAmount;
                    wmsDTO.setChargeAmount(BigDecimal.ZERO);
                    ficoInvoiceInformationRegistrationDTO.setInvoiceBalance(invoiceBalance.subtract(chargeAmount));
                }
                // 判断该记录是否还在 若存在则更新 若不存在则新增 (主键: 加盟商+业务类型+业务单号+发票号码)
                QueryWrapper<FiInvoiceRegistration> query = new QueryWrapper<FiInvoiceRegistration>()
                        .eq("CLIENT", user.getClient())
                        .eq("BUSINESS_TYPE", type)
                        .eq("MAT_DN_ID", wmsDTO.getMatDnId())
                        .eq("INVOICE_NUM", ficoInvoiceInformationRegistrationDTO.getInvoiceNum());
                FiInvoiceRegistration invoiceRegistration = fiInvoiceRegistrationService.getOne(query);
                if (!ObjectUtils.isEmpty(invoiceRegistration)) {
                    // 更新
                    FiInvoiceRegistration updateDO = new FiInvoiceRegistration();
                    // 已登记金额
                    updateDO.setAmountOfDocumentRegistration((invoiceRegistration.getAmountOfDocumentRegistration() == null ? BigDecimal.ZERO : invoiceRegistration.getAmountOfDocumentRegistration().abs()).add(registrationAmt));
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        updateDO.setAmountOfDocumentRegistration(updateDO.getAmountOfDocumentRegistration().multiply(Constants.MINUS_ONE));
                    }
                    fiInvoiceRegistrationService.update(updateDO, query);
                } else {
                    // 新增
                    FiInvoiceRegistration insertDO = new FiInvoiceRegistration();
                    insertDO.setClient(user.getClient());
                    insertDO.setBusinessType(type);
                    // 订单单号
                    insertDO.setMatDnId(wmsDTO.getMatDnId());
                    // 发票号
                    insertDO.setInvoiceNum(ficoInvoiceInformationRegistrationDTO.getInvoiceNum());
                    // 供应商 订单的供应商
                    insertDO.setPoSupplierId(wmsDTO.getWmGysBh());
                    // 地点 订单的地点
                    insertDO.setSiteCode(wmsDTO.getProSite());
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        billAmount = billAmount.abs().multiply(Constants.MINUS_ONE);
                    }
                    insertDO.setDocumentAmount(billAmount);
                    // 已登记金额
                    BigDecimal invoiceRegistered = registrationAmt;
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                    }
                    insertDO.setAmountOfDocumentRegistration(invoiceRegistered);
                    // 已付款金额
                    insertDO.setPaymentAmountOfDocuments(BigDecimal.ZERO);
                    insertDO.setCreationDate(DateUtils.getCurrentDateStrYYMMDD());
                    insertDO.setFounder(user.getUserId());
                    registrationList.add(insertDO);
                }
                globalIndex = i;
            }

        }
//        }
        if (!CollectionUtils.isEmpty(registrationList)) {
            fiInvoiceRegistrationService.saveBatch(registrationList);
        }
        for (WmsDTO wmsDTO : wmsDTOList) {
            // 6>发票登记从表数据
            List<FiInvoiceRegistrationDetailed> registrationDetailedList = new ArrayList<>();
            // 单据明细
            SelectWarehousingDetailsVO detailsVO = new SelectWarehousingDetailsVO();
            detailsVO.setClient(user.getClient());
            detailsVO.setMatDnId(wmsDTO.getMatDnId());
            detailsVO.setType(wmsDTO.getType());
            List<SelectWarehousingDetailsDTO> detailsDTOS = invoiceMapper.selectWarehousingDetails(detailsVO);
            for (SelectWarehousingDetailsDTO details : detailsDTOS) {
                QueryWrapper detailsQuery = new QueryWrapper<FiInvoiceRegistrationDetailed>()
                        .eq("CLIENT", user.getClient())
                        .eq("BUSINESS_TYPE", details.getType())
                        .eq("MAT_DN_ID", details.getMatDnId())
                        .eq("MAT_ID", details.getMatId())
                        .eq("MAT_YEAR", details.getMatYear())
                        .eq("MAT_LINE_NO", details.getMatLineNo());
                FiInvoiceRegistrationDetailed invoiceRegistrationDetailed = fiInvoiceRegistrationDetailedService.getOne(detailsQuery);
                // 行金额
                BigDecimal totalAmount = details.getTotalAmount();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    totalAmount = totalAmount.multiply(Constants.MINUS_ONE);
                }
                if (ObjectUtils.isEmpty(invoiceRegistrationDetailed)) {
                    // 新增
                    FiInvoiceRegistrationDetailed registrationDetailed = new FiInvoiceRegistrationDetailed();
                    registrationDetailed.setClient(user.getClient());
                    registrationDetailed.setBusinessType(details.getType());
                    registrationDetailed.setMatDnId(details.getMatDnId());
                    registrationDetailed.setMatId(details.getMatId());
                    registrationDetailed.setMatYear(details.getMatYear());
                    registrationDetailed.setMatLineNo(details.getMatLineNo());
                    registrationDetailed.setSoProCode(details.getProCode());
                    // 行金额
                    registrationDetailed.setLineAmount(totalAmount);
                    // 行登记金额
                    registrationDetailed.setLineRegistrationAmount(totalAmount);
                    registrationDetailed.setPaymentAmountOfLine(BigDecimal.ZERO);
                    registrationDetailedList.add(registrationDetailed);
                } else {
                    // 更新
                    fiInvoiceRegistrationDetailedService.update(new UpdateWrapper<FiInvoiceRegistrationDetailed>()
                            .set("LINE_REGISTRATION_AMOUNT", totalAmount)
                            .eq("CLIENT", user.getClient())
                            .eq("BUSINESS_TYPE", details.getType())
                            .eq("MAT_DN_ID", details.getMatDnId())
                            .eq("MAT_ID", details.getMatId())
                            .eq("MAT_YEAR", details.getMatYear())
                            .eq("MAT_LINE_NO", details.getMatLineNo()));
                }
            }
            if (!CollectionUtils.isEmpty(registrationDetailedList)) {
                fiInvoiceRegistrationDetailedService.saveBatch(registrationDetailedList);
            }
        }
    }

    /**
     * 订单发票批量确认2
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderBatchInvoice2(OrderBatchInvoiceVO vo) {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();

        /**
         * 1.验证发票剩余总金额 大于 订单剩余总金额（正负）
         */
        // 订单总额
        BigDecimal matDnSurplusAmount = vo.getMatDnList().stream().map(item -> {
            boolean isPositive = item.getTotalAmount().compareTo(BigDecimal.ZERO) > 0;
            if (isPositive) {
                return item.getTotalAmount().subtract(item.getRegisteredAmount());
            }
            BigDecimal totalAmount = item.getTotalAmount().abs();
            BigDecimal registeredAmount = item.getRegisteredAmount().abs();
            return totalAmount.subtract(registeredAmount).abs().multiply(Constants.MINUS_ONE);
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
        // 订单总额正负标识
        int matDnTotalAmountFlag = matDnSurplusAmount.compareTo(BigDecimal.ZERO);
        if (matDnTotalAmountFlag == 0) {
            throw new CustomResultException("单据总额不能为0");
        }
        // 发票总额
        BigDecimal invoiceSurplusAmount = vo.getInvoiceList().stream().map(item -> {
            if (matDnTotalAmountFlag > 0) {
                if (!(item.getInvoiceTotalInvoiceAmount().compareTo(BigDecimal.ZERO) > 0)) {
                    throw new CustomResultException("单据总额为正时，必须选择正值发票");
                }
            } else {
                if (!(item.getInvoiceTotalInvoiceAmount().compareTo(BigDecimal.ZERO) < 0)) {
                    throw new CustomResultException("单据总额为负时，必须选择负值发票");
                }
            }
            return item.getInvoiceTotalInvoiceAmount().subtract(item.getInvoiceRegisteredAmount());
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (invoiceSurplusAmount.abs().compareTo(matDnSurplusAmount.abs()) < 0) {
            throw new CustomResultException("发票金额不足");
        }

        // 已核销单据
        List<String> matDnList = vo.getMatDnList().stream().map(OrderBatchInvoiceVO.MatDn::getMatDnId).collect(Collectors.toList());
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper1 = new QueryWrapper<>();
        billInvoiceEndQueryWrapper1.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper1.eq("GBIE_TYPE", 1);
        billInvoiceEndQueryWrapper1.in("GBIE_NUM", matDnList);
        List<BillInvoiceEnd> billInvoiceEndList1 = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper1);

        QueryWrapper<FiSupplierPrepayment> fiSupplierPrepaymentQueryWrapper1 = new QueryWrapper<>();
        fiSupplierPrepaymentQueryWrapper1.eq("CLIENT", user.getClient());
        fiSupplierPrepaymentQueryWrapper1.in("MAT_DN_ID", matDnList);
        List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectList(fiSupplierPrepaymentQueryWrapper1);

        // 验证当前订单是否预付过，如果订单已预付，则不可以选择已付款的发票
        vo.getMatDnList().forEach(item -> {
            if (!CollectionUtils.isEmpty(billInvoiceEndList1)) {
                List<BillInvoiceEnd> gbieNumList = billInvoiceEndList1.stream().filter(s -> item.getMatDnId().equals(s.getGbieNum())).collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(gbieNumList)) {
                    throw new CustomResultException("单据[" + item.getMatDnId() + "]已核销");
                }
            }
            // 已预付单据
            List<FiSupplierPrepayment> matDnIdList = fiSupplierPrepaymentList.stream().filter(s -> item.getMatDnId().equals(s.getMatDnId())).collect(Collectors.toList());
            // 单据已预付
            if (!CollectionUtils.isEmpty(matDnIdList)) {
                // 发票号码集合
                List<String> invoiceNumList = vo.getInvoiceList().stream().map(p -> p.getInvoiceNum()).collect(Collectors.toList());
                // 2>发票信息 (已经正序排序)
                List<FicoInvoiceInformationRegistrationDTO> invoiceList = ficoInvoiceInformationRegistrationMapper.getInvoiceList(user.getClient(), invoiceNumList);
                // 发票
                invoiceList.forEach(LambdaUtils.consumerWithIndex((invoice, index) -> {
                    if (invoice.getInvoicePaidAmount() != null && invoice.getInvoicePaidAmount().compareTo(BigDecimal.ZERO) > 0) {
                        throw new CustomResultException("单据[" + item.getMatDnId() + "]已预付，不可以选择已付款发票[" + invoice.getInvoiceNum() + "]");
                    }
                }));
            }
        });

        /**
         * 2.页面值与DB里的值比对(不一样 抛异常)
         */
        // 订单列表
        List<String> matDnIdList = vo.getMatDnList().stream()
                .filter(item -> StringUtils.isNotEmpty(item.getMatDnId()))
                .map(OrderBatchInvoiceVO.MatDn::getMatDnId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(matDnIdList)) {
            throw new CustomResultException("单据列表不能为空");
        }
        List<WmsDTO> wmsList = invoiceMapper.getOrderList(client, matDnIdList);
        if (matDnIdList.size() != wmsList.size()) {
            throw new CustomResultException("选择的部分单据已经被登记,请重新选择");
        }
        // 发票列表
        List<String> invoiceNumList = vo.getInvoiceList().stream()
                .filter(item -> StringUtils.isNotEmpty(item.getInvoiceNum()))
                .map(OrderBatchInvoiceVO.Invoice::getInvoiceNum).collect(Collectors.toList());
        List<FicoInvoiceInformationRegistrationDTO> invoiceList = ficoInvoiceInformationRegistrationMapper.getInvoiceList(client, invoiceNumList);
        if (invoiceNumList.size() != invoiceList.size()) {
            throw new CustomResultException("勾选的部分发票已经被登记,请重新选择发票");
        }


        /**
         * 3.单据/发票剩余金额
         */
        // 订单剩余金额
        wmsList.forEach(item -> {
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                BigDecimal chargeAmount = item.getTotalAmount().abs().subtract(item.getRegisteredAmount().abs()).multiply(Constants.MINUS_ONE);
                item.setChargeAmount(chargeAmount);
            }
        });

        // 核销验证
        List<String> invoiceNums = invoiceList.stream().map(FicoInvoiceInformationRegistrationDTO::getInvoiceNum).collect(Collectors.toList());
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper2 = new QueryWrapper<>();
        billInvoiceEndQueryWrapper2.eq("CLIENT", client);
        billInvoiceEndQueryWrapper2.eq("GBIE_TYPE", 2);
        billInvoiceEndQueryWrapper2.in("GBIE_NUM", invoiceNums);
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper2);

        // 发票列表剩余金额
        invoiceList.forEach(item -> {
            if (!CollectionUtils.isEmpty(billInvoiceEndList)) {
                List<BillInvoiceEnd> gbieNumList = billInvoiceEndList.stream().filter(s -> item.getInvoiceNum().equals(s.getGbieNum())).collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(gbieNumList)) {
                    throw new CustomResultException("发票[" + item.getInvoiceNum() + "]已核销");
                }
            }

            boolean isPositive = item.getInvoiceTotalInvoiceAmount().compareTo(BigDecimal.ZERO) > 0;
            if (isPositive) {
                BigDecimal invoiceBalance = item.getInvoiceTotalInvoiceAmount().subtract(item.getInvoiceRegisteredAmount());
                item.setInvoiceBalance(invoiceBalance);
            } else {
                BigDecimal totalAmount = item.getInvoiceTotalInvoiceAmount().abs();
                BigDecimal registeredAmount = item.getInvoiceRegisteredAmount().abs();
                BigDecimal invoiceBalance = totalAmount.subtract(registeredAmount).multiply(Constants.MINUS_ONE);
                item.setInvoiceBalance(invoiceBalance);
            }
        });

        /**
         * 4.排序 （总额正，先负后正，负值从大到大，正值从小到大）（总额负，先正后负，正值从小到大，负值从大到小）
         */
        wmsList = this.sorMatDn(wmsList, matDnTotalAmountFlag);
        invoiceList = this.sortInvoice(invoiceList, matDnTotalAmountFlag);

        /**
         * 5.发票数据更新
         */
        BigDecimal invoiceBalanceTotal = BigDecimal.ZERO;
        for (int invoiceIndex = 0; invoiceIndex < invoiceList.size(); invoiceIndex++) {
            // 累加发票剩余金额
            invoiceBalanceTotal = invoiceBalanceTotal.abs().add(invoiceList.get(invoiceIndex).getInvoiceBalance().abs());
            if (invoiceBalanceTotal.abs().compareTo(matDnSurplusAmount.abs()) > 0) {
                // 最后一张会使用到的发票 超过部分
                BigDecimal outstrip = invoiceBalanceTotal.abs().subtract(matDnSurplusAmount.abs());
                // 使用金额
                BigDecimal invoiceCurrentToRegister = invoiceList.get(invoiceIndex).getInvoiceBalance().abs().subtract(outstrip.abs());
                if (invoiceCurrentToRegister.compareTo(BigDecimal.ZERO) > 0) {
                    // 发票号
                    String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
                    // 该发票已经登记的金额
                    BigDecimal invoiceHasRegistered = invoiceList.get(invoiceIndex).getInvoiceRegisteredAmount().abs();
                    // 本次登记后 该发票的已登记金额(之前的+本次使用到的)
                    BigDecimal invoiceRegistered = invoiceHasRegistered.add(invoiceCurrentToRegister).abs();
                    // 如果退厂 则金额为负数
                    if (matDnTotalAmountFlag < 0) {
                        invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                    }
                    // 保存发票登记数据
                    ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                            .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                            .set("INVOICE_REGISTRATION_STATUS", "1")
                            .eq("CLIENT", user.getClient())
                            .eq("INVOICE_NUM", invoiceNum));
                }
                break;
            }
            // 发票号
            String invoiceNum = invoiceList.get(invoiceIndex).getInvoiceNum();
            // 本次登记操作后发票的已登记金额 (前面几张发票 剩余金额全部使用：登记金额等于发票总额)
            BigDecimal invoiceRegistered = invoiceList.get(invoiceIndex).getInvoiceTotalInvoiceAmount().abs();
            BigDecimal invoiceBalance = invoiceList.get(invoiceIndex).getInvoiceBalance();
            if (matDnTotalAmountFlag < 0) {
                invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
            }
            // 保存发票登记金额数据
            ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                    // 发票已登记金额 直接更新为发票总额
                    .set("INVOICE_REGISTERED_AMOUNT", invoiceRegistered)
                    .set("INVOICE_REGISTRATION_STATUS", "1")
                    .eq("CLIENT", user.getClient())
                    .eq("INVOICE_NUM", invoiceNum));
        }

        /**
         * 6.发票登记主数据
         */
        List<FiInvoiceRegistration> registrationList = new ArrayList<>();
        int globalIndex = 0;
        for (WmsDTO wmsDTO : wmsList) {// 单据总金额
            BigDecimal billAmount = wmsDTO.getTotalAmount();
            String type = wmsDTO.getType();
            // 单据发票登记
            for (int i = globalIndex; i < invoiceList.size(); i++) {
                FicoInvoiceInformationRegistrationDTO ficoInvoiceInformationRegistrationDTO = invoiceList.get(i);
                if (ficoInvoiceInformationRegistrationDTO.getInvoiceBalance().compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                // 单据总金额
                BigDecimal chargeAmount = wmsDTO.getChargeAmount();
                if (chargeAmount.compareTo(BigDecimal.ZERO) == 0) {
                    break;
                }
                // 发票剩余金额
                BigDecimal invoiceBalance = ficoInvoiceInformationRegistrationDTO.getInvoiceBalance();
                // 登记金额
                BigDecimal registrationAmt = BigDecimal.ZERO;
                if (matDnTotalAmountFlag > 0) {
                    if (chargeAmount.compareTo(invoiceBalance) >= 0) {
                        registrationAmt = invoiceBalance;
                        ficoInvoiceInformationRegistrationDTO.setInvoiceBalance(BigDecimal.ZERO);
                        wmsDTO.setChargeAmount(chargeAmount.subtract(invoiceBalance));
                    } else {
                        registrationAmt = chargeAmount;
                        wmsDTO.setChargeAmount(BigDecimal.ZERO);
                        ficoInvoiceInformationRegistrationDTO.setInvoiceBalance(invoiceBalance.subtract(chargeAmount));
                    }
                } else {
                    if (chargeAmount.compareTo(invoiceBalance) <= 0) {
                        registrationAmt = invoiceBalance;
                        ficoInvoiceInformationRegistrationDTO.setInvoiceBalance(BigDecimal.ZERO);
                        wmsDTO.setChargeAmount(chargeAmount.subtract(invoiceBalance));
                    } else {
                        registrationAmt = chargeAmount;
                        wmsDTO.setChargeAmount(BigDecimal.ZERO);
                        ficoInvoiceInformationRegistrationDTO.setInvoiceBalance(invoiceBalance.subtract(chargeAmount));
                    }
                }

                // 判断该记录是否还在 若存在则更新 若不存在则新增 (主键: 加盟商+业务类型+业务单号+发票号码)
                QueryWrapper<FiInvoiceRegistration> query = new QueryWrapper<FiInvoiceRegistration>()
                        .eq("CLIENT", user.getClient())
                        .eq("BUSINESS_TYPE", type)
                        .eq("MAT_DN_ID", wmsDTO.getMatDnId())
                        .eq("INVOICE_NUM", ficoInvoiceInformationRegistrationDTO.getInvoiceNum());
                FiInvoiceRegistration invoiceRegistration = fiInvoiceRegistrationService.getOne(query);
                if (!ObjectUtils.isEmpty(invoiceRegistration)) {
                    // 更新
                    FiInvoiceRegistration updateDO = new FiInvoiceRegistration();
                    // 已登记金额
                    updateDO.setAmountOfDocumentRegistration((invoiceRegistration.getAmountOfDocumentRegistration() == null ? BigDecimal.ZERO : invoiceRegistration.getAmountOfDocumentRegistration()).add(registrationAmt));

                    fiInvoiceRegistrationService.update(updateDO, query);
                } else {
                    // 新增
                    FiInvoiceRegistration insertDO = new FiInvoiceRegistration();
                    insertDO.setClient(user.getClient());
                    insertDO.setBusinessType(type);
                    // 订单单号
                    insertDO.setMatDnId(wmsDTO.getMatDnId());
                    // 发票号
                    insertDO.setInvoiceNum(ficoInvoiceInformationRegistrationDTO.getInvoiceNum());
                    // 供应商 订单的供应商
                    insertDO.setPoSupplierId(wmsDTO.getWmGysBh());
                    // 地点 订单的地点
                    insertDO.setSiteCode(wmsDTO.getProSite());
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        billAmount = billAmount.abs().multiply(Constants.MINUS_ONE);
                    }
                    insertDO.setDocumentAmount(billAmount);
                    // 已登记金额
                    BigDecimal invoiceRegistered = registrationAmt;
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        invoiceRegistered = invoiceRegistered.abs().multiply(Constants.MINUS_ONE);
                    }
                    insertDO.setAmountOfDocumentRegistration(invoiceRegistered);
                    // 已付款金额
                    insertDO.setPaymentAmountOfDocuments(BigDecimal.ZERO);
                    insertDO.setCreationDate(DateUtils.getCurrentDateStrYYMMDD());
                    insertDO.setFounder(user.getUserId());
                    registrationList.add(insertDO);
                }
                globalIndex = i;
            }
        }
        if (!CollectionUtils.isEmpty(registrationList)) {
            fiInvoiceRegistrationService.saveBatch(registrationList);
        }
        /**
         * 7.发票登记明细
         */
        for (WmsDTO wmsDTO : wmsList) {
            String type = wmsDTO.getType();
            // 发票登记从表数据
            List<FiInvoiceRegistrationDetailed> registrationDetailedList = new ArrayList<>();
            // 单据明细
            SelectWarehousingDetailsVO detailsVO = new SelectWarehousingDetailsVO();
            detailsVO.setClient(user.getClient());
            detailsVO.setMatDnId(wmsDTO.getMatDnId());
            detailsVO.setType(wmsDTO.getType());
            List<SelectWarehousingDetailsDTO> detailsDTOS = invoiceMapper.selectWarehousingDetails(detailsVO);
            for (SelectWarehousingDetailsDTO details : detailsDTOS) {
                QueryWrapper detailsQuery = new QueryWrapper<FiInvoiceRegistrationDetailed>()
                        .eq("CLIENT", user.getClient())
                        .eq("BUSINESS_TYPE", details.getType())
                        .eq("MAT_DN_ID", details.getMatDnId())
                        .eq("MAT_ID", details.getMatId())
                        .eq("MAT_YEAR", details.getMatYear())
                        .eq("MAT_LINE_NO", details.getMatLineNo());
                FiInvoiceRegistrationDetailed invoiceRegistrationDetailed = fiInvoiceRegistrationDetailedService.getOne(detailsQuery);
                // 行金额
                BigDecimal totalAmount = details.getTotalAmount();
                if ("GD".equals(type) || "CJ".equals(type)) {
                    totalAmount = totalAmount.abs().multiply(Constants.MINUS_ONE);
                }
                if (ObjectUtils.isEmpty(invoiceRegistrationDetailed)) {
                    // 新增
                    FiInvoiceRegistrationDetailed registrationDetailed = new FiInvoiceRegistrationDetailed();
                    registrationDetailed.setClient(user.getClient());
                    registrationDetailed.setBusinessType(details.getType());
                    registrationDetailed.setMatDnId(details.getMatDnId());
                    registrationDetailed.setMatId(details.getMatId());
                    registrationDetailed.setMatYear(details.getMatYear());
                    registrationDetailed.setMatLineNo(details.getMatLineNo());
                    registrationDetailed.setSoProCode(details.getProCode());
                    // 行金额
                    registrationDetailed.setLineAmount(totalAmount);
                    // 行登记金额
                    registrationDetailed.setLineRegistrationAmount(totalAmount);
                    registrationDetailed.setPaymentAmountOfLine(BigDecimal.ZERO);
                    registrationDetailedList.add(registrationDetailed);
                } else {
                    // 更新
                    fiInvoiceRegistrationDetailedService.update(new UpdateWrapper<FiInvoiceRegistrationDetailed>()
                            .set("LINE_REGISTRATION_AMOUNT", totalAmount)
                            .eq("CLIENT", user.getClient())
                            .eq("BUSINESS_TYPE", details.getType())
                            .eq("MAT_DN_ID", details.getMatDnId())
                            .eq("MAT_ID", details.getMatId())
                            .eq("MAT_YEAR", details.getMatYear())
                            .eq("MAT_LINE_NO", details.getMatLineNo()));
                }
            }
            if (!CollectionUtils.isEmpty(registrationDetailedList)) {
                fiInvoiceRegistrationDetailedService.saveBatch(registrationDetailedList);
            }
            // 差价单-已记账
            resChajia(user.getClient(), type, wmsDTO.getMatDnId());
        }
    }

    /**
     * 单据预付查询
     *
     * @param vo
     * @return
     */
    @Override
    public Result selectPrepaidBill(SelectWarehousingVO vo) {
        // 地点和单体店只能选择一个（且必须选择一个）
//        if (((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode())) || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
//            throw new CustomResultException("[采购地点]和[单体店]必须选择且只能选择其中一个");
//        }
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(vo.getPageNum(), vo.getPageSize());

        if (!(StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode()) ||
                StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))) {
            vo.setSite(StringUtils.isBlank(vo.getSite()) ? "-1" : vo.getSite());
            vo.setStoCode(StringUtils.isBlank(vo.getStoCode()) ? "-1" : vo.getStoCode());
        }
        if (StringUtils.isNotBlank(vo.getPaymentOrderNo())) {
            vo.setPaymentOrderNoList(Arrays.asList(StringUtils.split(vo.getPaymentOrderNo(), ",")));
        }
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = invoiceMapper.selectPrepaidBill(page, vo);
        selectWarehousingDTOS.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
            }
            // 本次付款金额
            if (item.getTotalAmount() != null) {
                if (item.getInvoiceAmountOfThisPayment() == null) {
                    item.setInvoiceAmountOfThisPayment(BigDecimal.ZERO);
                }
                item.setPaymentAmt(item.getTotalAmount().subtract(item.getInvoiceAmountOfThisPayment()).setScale(2, BigDecimal.ROUND_HALF_UP));
            }

        });
        return ResultUtil.success(selectWarehousingDTOS);
    }

    /**
     * 单据预付
     *
     * @param list
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result savePrepaidBill(List<SelectWarehousingDTO> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("未选择付款单据");
        }
        TokenUser user = commonService.getLoginInfo();
        List<String> matDnIdList = list.stream().map(SelectWarehousingDTO::getMatDnId).collect(Collectors.toList());
        // 已在明细付款
        List<FiSupplierPrepayment> list1 = fiSupplierPrepaymentMapper.selectList(
                new QueryWrapper<FiSupplierPrepayment>()
                        .eq("CLIENT", user.getClient()).eq("PAYMENT_TYPE", 2).in("MAT_DN_ID", matDnIdList)
        );
        // 已核销
        List<BillInvoiceEnd> list2 = billInvoiceEndMapper.selectList(
                new QueryWrapper<BillInvoiceEnd>()
                        .eq("CLIENT", user.getClient()).eq("GBIE_TYPE", 1).in("GBIE_NUM", matDnIdList)
        );
        list.forEach(s -> {
            String matDnId = s.getMatDnId();
            FiSupplierPrepayment fiSupplierPrepayment = list1.stream().filter(t -> t.getMatDnId().equals(matDnId)).findFirst().orElse(null);
            if (fiSupplierPrepayment != null) {
                throw new CustomResultException(s.getMatDnId() + "已在明细付款");
            }
            BillInvoiceEnd billInvoiceEnd = list2.stream().filter(t -> t.getGbieNum().equals(matDnId)).findFirst().orElse(null);
            if (billInvoiceEnd != null) {
                throw new CustomResultException(s.getMatDnId() + "该单据已核销，请重新选择");
            }
        });

        Map<String, List<FiSupplierPrepayment>> map = new HashMap<>();
        list.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
            QueryWrapper<PaymentDocumentNoApplications> paymentDocumentNoApplicationsQueryWrapper = new QueryWrapper<>();
            paymentDocumentNoApplicationsQueryWrapper.eq("CLIENT", user.getClient());
            paymentDocumentNoApplicationsQueryWrapper.eq("BUSINESS_TYPE", item.getType());
            paymentDocumentNoApplicationsQueryWrapper.eq("MAT_DN_ID", item.getMatDnId());
            List<PaymentDocumentNoApplications> paymentDocumentNoApplicationsList = paymentDocumentNoApplicationsMapper.selectList(paymentDocumentNoApplicationsQueryWrapper);
            if (!CollectionUtils.isEmpty(paymentDocumentNoApplicationsList)) {
                throw new CustomResultException("单据" + item.getMatDnId() + "已付过款");
            }
            // 差价单-已记账
            resChajia(user.getClient(), item.getType(), item.getMatDnId());
//            QueryWrapper<FiSupplierPrepayment> fiSupplierPrepaymentQueryWrapper = new QueryWrapper<>();
//            fiSupplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
//            fiSupplierPrepaymentQueryWrapper.eq("BUSINESS_TYPE", item.getType());
//            fiSupplierPrepaymentQueryWrapper.eq("MAT_DN_ID", item.getMatDnId());
//            List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectList(fiSupplierPrepaymentQueryWrapper);
//            if (!CollectionUtils.isEmpty(fiSupplierPrepaymentList)) {
//                throw new CustomResultException("单据" + item.getMatDnId() + "已付过款");
//            }
            // 供应商
            String wmGysBh = item.getSupSelfCode();
            // 地点
            String proSite = item.getSite();
            // key
            String key = proSite + "_" + wmGysBh;
            // 分组明细
            List<FiSupplierPrepayment> details = map.get(key);
            if (details == null) {
                details = new ArrayList<>();
            }
            FiSupplierPrepayment fiSupplierPrepayment = new FiSupplierPrepayment();
            // 加盟商
            fiSupplierPrepayment.setClient(user.getClient());
            // 付款单号
//            fiSupplierPrepayment.setPaymentOrderNo(paymentOrderNo);
            // 付款单据日期
            fiSupplierPrepayment.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 业务单号
            fiSupplierPrepayment.setMatDnId(item.getMatDnId());
            // 供应商编码
            fiSupplierPrepayment.setSupCode(item.getSupSelfCode());
            // 本次付款金额
            fiSupplierPrepayment.setInvoiceAmountOfThisPayment(item.getPaymentAmt());
            // 总金额
            fiSupplierPrepayment.setInvoiceTotalAmount(item.getTotalAmount());
            // 地点
            fiSupplierPrepayment.setProSite(item.getSite());
            // 业务类型
            fiSupplierPrepayment.setBusinessType(item.getType());
            details.add(fiSupplierPrepayment);
            map.put(key, details);
        }));

        for (String key : map.keySet()) {
            // 付款单号
            String paymentOrderNo = "YF" + getPaymentOrderNoByDB(user.getClient());
            List<FiSupplierPrepayment> details = map.get(key);
            // 供应商
            String wmGysBh = details.get(0).getSupCode();
            // 地点
            String proSite = details.get(0).getProSite();
            // 本次付款总金额
            BigDecimal thisPaymentTotal = details.stream().map(FiSupplierPrepayment::getInvoiceAmountOfThisPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
            PaymentApplications applications = new PaymentApplications();
            {
                applications.setClient(user.getClient());
                // 供应商
                applications.setSupCode(wmGysBh);
                // 地点
                applications.setProSite(proSite);
                // 申请日期
                applications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
                // 1单据付款，2发票付款
                applications.setType(CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode());
                // 本次付款总金额
                applications.setInvoiceAmountOfThisPayment(thisPaymentTotal);
                // 0-已申请；1-未申请
                applications.setApplicationStatus(1);
                // '-1未申请，0-审批中，1-已审批，2-已废弃
                applications.setApprovalStatus(-1);
                // 工作流编码
                applications.setApprovalFlowNo("");
                // 付款单号
                applications.setPaymentOrderNo(paymentOrderNo);
                paymentApplicationsMapper.insert(applications);
            }
            // 主表id
            details.forEach(item -> {
                item.setApplicationsId(applications.getId());
                item.setPaymentOrderNo(paymentOrderNo);
                item.setPaymentType(1);
            });
            iFiSupplierPrepaymentService.saveBatch(details);
        }

        return ResultUtil.success();
    }

    /**
     * 单体店列表
     */
    @Override
    public List<StoreData> getStoList() {
        String client = commonService.getLoginInfo().getClient();
        return storeDataService.list(new QueryWrapper<StoreData>()
                        .eq("CLIENT", client)
                // 只要不是连锁的都是单体店
//                .ne("STO_ATTRIBUTE", "2")
        );
    }

    /**
     * @param vo 出入库查询金额合计
     * @return
     */
    @Override
    public Result selectWarehousingAmt(SelectWarehousingVO vo) {

        // 地点和单体店若只有一个有值，那么另一个赋特殊值”-1“
        if (!((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))
                || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
            if (StringUtils.isBlank(vo.getSite())) {
                vo.setSite("-1");
            } else {
                vo.setStoCode("-1");
            }
        }
        // 单据去税金额
        BigDecimal excludingTaxAmountTotal = BigDecimal.ZERO;
        // 单据税额
        BigDecimal taxAmountTotal = BigDecimal.ZERO;
        // 单据总金额
        BigDecimal totalAmountTotal = BigDecimal.ZERO;
        // 单据已登记金额
        BigDecimal registeredAmountTotal = BigDecimal.ZERO;
        // 单据剩余金额
        BigDecimal chargeAmountTotal = BigDecimal.ZERO;
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(-1, -1);
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = invoiceMapper.selectWarehousing(page, vo);
        for (SelectWarehousingDTO item : selectWarehousingDTOS.getRecords()) {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 剩余可登记金额 (计算得到，type为GD为负数--总额减已登记金额)
                BigDecimal multiply = totalAmount.abs().subtract(item.getRegisteredAmount().abs()).multiply(Constants.MINUS_ONE);

                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
                // 剩余可登记金额
                item.setChargeAmount(multiply);
            }
            // 单据去税金额
            excludingTaxAmountTotal = DecimalUtils.add(excludingTaxAmountTotal, item.getExcludingTaxAmount());
            // 单据税额
            taxAmountTotal = DecimalUtils.add(taxAmountTotal, item.getTaxAmount());
            // 单据总金额
            totalAmountTotal = DecimalUtils.add(totalAmountTotal, item.getTotalAmount());
            // 单据已登记金额
            registeredAmountTotal = DecimalUtils.add(registeredAmountTotal, item.getRegisteredAmount());
            // 单据剩余金额
            chargeAmountTotal = DecimalUtils.add(chargeAmountTotal, item.getChargeAmount());
        }
        SelectWarehousingDTO entity = new SelectWarehousingDTO();
        // 单据去税金额
        entity.setExcludingTaxAmount(excludingTaxAmountTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        // 单据税额
        entity.setTaxAmount(taxAmountTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        // 单据总金额
        entity.setTotalAmount(totalAmountTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        // 单据已登记金额
        entity.setRegisteredAmount(registeredAmountTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        // 单据剩余金额
        entity.setChargeAmount(chargeAmountTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        return ResultUtil.success(entity);
    }

    /**
     * 供应商单据预付导出
     */
    @SneakyThrows
    @Override
    public Result exportSelectPrepaidBill(SelectWarehousingVO vo) {
        if (CollectionUtils.isEmpty(vo.getMatDnIdList())) {
            throw new CustomResultException("单据列表集合不能为空");
        }
        vo.setPageNum(-1);
        vo.setPageSize(-1);
        IPage<SelectWarehousingDTO> page = (IPage<SelectWarehousingDTO>) (this.selectPrepaidBill(vo).getData());
        List<SelectWarehousingDTO> records = page.getRecords();
        List<List<Object>> dataList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(records)) {
            // 组建导出数据
            records.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 供应商编码
                data.add(item.getSupSelfCode() + item.getSupSelfName());
                // 业务类型
                data.add(item.getType() + item.getTypeName());
                // 门店
                data.add(item.getSite() + item.getStoName());
                // 单据编号
                data.add(item.getMatDnId());
                // 单据日期
                if (StringUtils.isBlank(item.getXgrq())) {
                    data.add("");
                } else {
                    StringBuffer sb = new StringBuffer(item.getXgrq());
                    sb.insert(6, "/");
                    sb.insert(4, "/");
                    data.add(sb.toString());
                }
                // 合计金额
                data.add(item.getTotalAmount());
                // 已付金额
                data.add(item.getInvoiceAmountOfThisPayment());
                // 付款单号
                data.add(item.getPaymentOrderNo());
                // 业务员
                data.add(item.getInvoiceSalesmanName());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headHList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("供应商单据预付");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 发票登记明细表头
     */
    private String[] invoiceRegHeadList = {
            "序号",
            "地点",
            "业务类型",
            "单据编号",
            "单据日期",
            "供应商编码",
            "商品编码",
            "商品通用名",
            "数量",
            "单价",
            "金额",
            "去税金额",
            "税金",
            "已登记金额",
            "剩余金额",
            "规格",
            "单位",
            "厂家"
    };

    /**
     * 导出数据表头
     */
    private String[] headHList = {
            "供应商编码",
            "业务类型",
            "门店",
            "单据编号",
            "单据日期",
            "合计金额",
            "已付金额",
            "付款单号",
            "业务员",
    };

    /**
     * 导出明细数据表头
     */
    private String[] detailHeadHList = {
            "地点",
            "业务类型",
            "单据编号",
            "单据日期",
            "供应商编码",
            "商品编码",
            "商品通用名",
            "数量",
            "单价",
            "金额",
            "已结算数量",
            "已结算金额",
            "规格",
            "单位",
            "厂家",
    };

    @SneakyThrows
    @Override
    public Result exportSelectWarehousingDetails(ExportSelectWarehousingDetailsVO vo) {
        SelectWarehousingDetailsVO vo1 = new SelectWarehousingDetailsVO();
        BeanUtils.copyProperties(vo, vo1);
        List<SelectWarehousingDetailsDTO> selectWarehousingDetailsDTOS = this.selectWarehousingDetails(vo1);

        List<List<Object>> dataList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(selectWarehousingDetailsDTOS)) {
            // 组建导出数据
            selectWarehousingDetailsDTOS.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 单据号
                data.add(item.getMatDnId());
                // 商品编码
                data.add(item.getProCode());
                // 商品名称
                data.add(item.getProName());
                // 规格
                data.add(item.getProSpecs());
                // 数量
                data.add(item.getMatQty());
                // 商品去税金额
                data.add(item.getExcludingTaxAmount());
                // 商品去税税额
                data.add(item.getRateBat());
                // 单价
                data.add(ObjectUtils.isEmpty(item.getTotalAmount()) || item.getTotalAmount().signum() == 0
                        || ObjectUtils.isEmpty(item.getMatQty()) || item.getMatQty().signum() == 0 ?
                        0 : item.getTotalAmount().divide(item.getMatQty(), 2, BigDecimal.ROUND_HALF_UP));
                // 含税金额
                data.add(item.getChargeAmount());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headDList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("供应商单据预付明细");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    @Override
    @Transactional
    public Result savePrepaidDetail(List<SelectPrepaidBillDetailVO> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("未选择付款单据");
        }
        TokenUser user = commonService.getLoginInfo();
        List<String> matDnIdList = list.stream().map(SelectPrepaidBillDetailVO::getBillNo).collect(Collectors.toList());
        // 单据付款
        List<FiSupplierPrepayment> list1 = fiSupplierPrepaymentMapper.selectList(
                new QueryWrapper<FiSupplierPrepayment>()
                        .eq("CLIENT", user.getClient()).eq("PAYMENT_TYPE", 1).in("MAT_DN_ID", matDnIdList)
        );
        // 已核销
        List<BillInvoiceEnd> list2 = billInvoiceEndMapper.selectList(
                new QueryWrapper<BillInvoiceEnd>()
                        .eq("CLIENT", user.getClient()).eq("GBIE_TYPE", 1).in("GBIE_NUM", matDnIdList)
        );
        list.forEach(s -> {
            String matDnId = s.getBillNo();
            FiSupplierPrepayment fiSupplierPrepayment = list1.stream().filter(t -> t.getMatDnId().equals(matDnId)).findFirst().orElse(null);
            if (fiSupplierPrepayment != null) {
                throw new CustomResultException(s.getBillNo() + "已在单据付款");
            }
            BillInvoiceEnd billInvoiceEnd = list2.stream().filter(t -> t.getGbieNum().equals(matDnId)).findFirst().orElse(null);
            if (billInvoiceEnd != null) {
                throw new CustomResultException(s.getMatDnId() + "该单据已核销，请重新选择");
            }
        });

        Map<String, List<FiSupplierPrepayment>> map = new HashMap<>();
        Map<String, List<DtSupplierPrepayment>> map2 = new HashMap<>();

        list.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
            QueryWrapper<PaymentDocumentNoApplications> paymentDocumentNoApplicationsQueryWrapper = new QueryWrapper<>();
            paymentDocumentNoApplicationsQueryWrapper.eq("CLIENT", user.getClient());
            paymentDocumentNoApplicationsQueryWrapper.eq("BUSINESS_TYPE", item.getType());
            paymentDocumentNoApplicationsQueryWrapper.eq("MAT_DN_ID", item.getMatDnId());
            List<PaymentDocumentNoApplications> paymentDocumentNoApplicationsList = paymentDocumentNoApplicationsMapper.selectList(paymentDocumentNoApplicationsQueryWrapper);
            if (!CollectionUtils.isEmpty(paymentDocumentNoApplicationsList)) {
                throw new CustomResultException("单据" + item.getMatDnId() + "已付过款");
            }
            // 差价单-已记账
            resChajia(user.getClient(), item.getType(), item.getMatDnId());
            // 供应商
            String wmGysBh = item.getSupSelfCode();
            // 地点
            String proSite = item.getSite();
            // key
            String key = proSite + "_" + wmGysBh;
            // 分组明细
            List<FiSupplierPrepayment> details = map.get(key);
            List<DtSupplierPrepayment> details2 = map2.get(key);
            if (details == null) {
                details = new ArrayList<>();
            }
            if (details2 == null) {
                details2 = new ArrayList<>();
            }
            FiSupplierPrepayment fiSupplierPrepayment = new FiSupplierPrepayment();
            // 加盟商
            fiSupplierPrepayment.setClient(user.getClient());
            // 付款单据日期
            fiSupplierPrepayment.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 业务单号
            fiSupplierPrepayment.setMatDnId(item.getMatDnId());
            // 供应商编码
            fiSupplierPrepayment.setSupCode(item.getSupSelfCode());
            // 本次付款金额
            fiSupplierPrepayment.setInvoiceAmountOfThisPayment(item.getPaymentAmt());
            // 总金额
            fiSupplierPrepayment.setInvoiceTotalAmount(item.getTotalAmount());
            // 地点
            fiSupplierPrepayment.setProSite(item.getSite());
            // 业务类型
            fiSupplierPrepayment.setBusinessType(item.getType());
            details.add(fiSupplierPrepayment);
            map.put(key, details);
            DtSupplierPrepayment dsp = new DtSupplierPrepayment();
            BeanUtils.copyProperties(item, dsp);
            dsp.setAmount(item.getPaymentAmt());
            dsp.setQty(item.getPaymentNum());
            dsp.setSupCode(item.getSupSelfCode());
            dsp.setProSite(item.getDeliveryTypeStore());
            details2.add(dsp);
            map2.put(key, details2);
        }));

        for (String key : map.keySet()) {
            // 付款单号
            String paymentOrderNo = "YF" + getPaymentOrderNoByDB(user.getClient());
            List<FiSupplierPrepayment> details = map.get(key);
            List<DtSupplierPrepayment> details2 = map2.get(key);
            // 供应商
            String wmGysBh = details.get(0).getSupCode();
            // 地点
            String proSite = details.get(0).getProSite();
            // 本次付款总金额
            BigDecimal thisPaymentTotal = details.stream().map(FiSupplierPrepayment::getInvoiceAmountOfThisPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
            PaymentApplications applications = new PaymentApplications();
            {
                applications.setClient(user.getClient());
                // 供应商
                applications.setSupCode(wmGysBh);
                // 地点
                applications.setProSite(proSite);
                // 申请日期
                applications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
                // 1单据付款，2发票付款
                applications.setType(CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode());
                // 本次付款总金额
                applications.setInvoiceAmountOfThisPayment(thisPaymentTotal);
                // 0-已申请；1-未申请
                applications.setApplicationStatus(1);
                // '-1未申请，0-审批中，1-已审批，2-已废弃
                applications.setApprovalStatus(-1);
                // 工作流编码
                applications.setApprovalFlowNo("");
                // 付款单号
                applications.setPaymentOrderNo(paymentOrderNo);
                paymentApplicationsMapper.insert(applications);
            }
            // 主表id
            details.forEach(item -> {
                item.setApplicationsId(applications.getId());
                item.setPaymentOrderNo(paymentOrderNo);
                item.setPaymentType(2);
            });
            details2.forEach(item -> {
                item.setApplicationsId(applications.getId());
                item.setPaymentOrderNo(paymentOrderNo);
            });
            Map<String, List<FiSupplierPrepayment>> mapList = details.stream().collect(Collectors.groupingBy(s -> s.getMatDnId()));
            List<FiSupplierPrepayment> fiDetails = new ArrayList<>();
            for (Map.Entry<String, List<FiSupplierPrepayment>> entry : mapList.entrySet()) {
                FiSupplierPrepayment fi = new FiSupplierPrepayment();
                FiSupplierPrepayment t = entry.getValue().get(0);
                BigDecimal invoiceAmt = BigDecimal.ZERO;
                BigDecimal totalAmt = BigDecimal.ZERO;
                BeanUtils.copyProperties(t, fi);
                for (FiSupplierPrepayment fi2 : entry.getValue()) {
                    invoiceAmt = invoiceAmt.add(fi2.getInvoiceAmountOfThisPayment());
                    totalAmt = totalAmt.add(fi2.getInvoiceTotalAmount());
                }
                fi.setInvoiceTotalAmount(totalAmt);
                fi.setInvoiceAmountOfThisPayment(invoiceAmt);
                fiDetails.add(fi);
            }
            iFiSupplierPrepaymentService.saveBatch(fiDetails);
            dtSupplierPrepaymentService.saveBatch(details2);
        }
       /* List<DtSupplierPrepayment> dspList = new ArrayList<>();
        for (SelectPrepaidBillDetailVO entry : list) {
            DtSupplierPrepayment dsp = new DtSupplierPrepayment();
            BeanUtils.copyProperties(entry, dsp);
            dsp.setAmount(entry.getPaymentAmt());
            dsp.setQty(entry.getMatQty());
            dspList.add(dsp);
        }
        dtSupplierPrepaymentService.saveBatch(dspList);*/
        return ResultUtil.success();
    }

    @Override
    public Result selectPrepaidBillDetail(SelectWarehousingVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(vo.getPageNum(), vo.getPageSize());

        if (!(StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode()) ||
                StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))) {
            vo.setSite(StringUtils.isBlank(vo.getSite()) ? "-1" : vo.getSite());
            vo.setStoCode(StringUtils.isBlank(vo.getStoCode()) ? "-1" : vo.getStoCode());
        }
        IPage<SelectPrepaidBillDetailVO> vos = dtSupplierPrepaymentMapper.selectPrepaidBillDetail(page, vo);
        vos.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
//                item.setPrice(item.getPrice().abs().multiply(Constants.MINUS_ONE));
            }
            item.setPrice(item.getTotalAmount().divide(item.getMatQty(), 2, RoundingMode.HALF_UP));
            item.setSettlementAmount(item.getInvoiceAmountOfThisPayment());
            item.setBillNo(item.getMatDnId());
            item.setTotalAmount(item.getPrice().multiply(item.getMatQty()).setScale(2));
            // 本次结算数量
            if (item.getMatQty() != null) {
                if (item.getSettlementQty() == null) {
                    item.setSettlementQty(BigDecimal.ZERO);
                }
                item.setPaymentNum(item.getMatQty().subtract(item.getSettlementQty()).setScale(2));
            }
            // 本次结算金额
            if (item.getPrice() != null && item.getPaymentNum() != null) {
                item.setPaymentAmt(item.getPrice().multiply(item.getPaymentNum()).setScale(2));
            }

        });
        return ResultUtil.success(vos);
    }

    @Override
    public Result exportSelectPrepaidBillDetail(SelectWarehousingVO vo) throws IOException {
        vo.setPageNum(-1);
        vo.setPageSize(-1);
        IPage<SelectPrepaidBillDetailVO> page = (IPage<SelectPrepaidBillDetailVO>) (this.selectPrepaidBillDetail(vo).getData());
        List<SelectPrepaidBillDetailVO> records = page.getRecords();
        List<List<Object>> dataList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(records)) {
            // 组建导出数据
            records.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 地点
                data.add(item.getSite() + "-" + item.getStoName());
                // 业务类型
                data.add(item.getType() + item.getTypeName());
                // 单据编号
                data.add(item.getMatDnId());
                // 单据日期
                data.add(item.getXgrq());
                // 供应商编码
                data.add(item.getSupSelfCode() + item.getSupSelfName() == null ? "" : item.getSupSelfName());
                // 商品编码
                data.add(item.getProCode());
                // 商品通用名
                data.add(item.getProName());
                // 数量
                data.add(item.getMatQty());
                // 单价
                data.add(item.getPrice());
                // 金额
                data.add(item.getTotalAmount());
                // 已结算数量
                data.add(item.getSettlementQty());
                // 已结算金额
                data.add(item.getSettlementAmount());
                // 规格
                data.add(item.getProSpecs());
                // 单位
                data.add(item.getProUnit());
                // 厂家
                data.add(item.getProFactoryName());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(detailHeadHList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("供应商单据预付");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 付款累计金额是否允许大于单据金额
     *
     * @return
     */
    @Override
    public Result gysdjyf() {
        TokenUser user = commonService.getLoginInfo();
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(
                new LambdaQueryWrapper<ClSystemPara>().eq(ClSystemPara::getClient, user.getClient())
                        .eq(ClSystemPara::getGcspId, "GYSDJYF")
        );
        int flg = 0;
        if (clSystemPara != null && "1".equals(clSystemPara.getGcspPara1())) {
            flg = 1;
        }
        return ResultUtil.success(flg);
    }

    @Override
    public IPage<SelectWarehousingDetailsDTO> selectWarehousingList(SelectWarehousingVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDetailsDTO>(vo.getPageNum(), vo.getPageSize());
        IPage<SelectWarehousingDetailsDTO> iPage = invoiceMapper.selectWarehousingList(page, vo);
        return iPage;
    }

    @Override
    public void selectWarehousingListExport(SelectWarehousingVO vo, ExportTask exportTask) throws IOException {
        try {
            TokenUser user = commonService.getLoginInfo();
            vo.setClient(user.getClient());
            Page page = new Page<SelectWarehousingDetailsDTO>(vo.getPageNum(), vo.getPageSize());
            IPage<SelectWarehousingDetailsDTO> iPage = invoiceMapper.selectWarehousingList(page, vo);
            List<List<Object>> dataList = new ArrayList<>();
            iPage.getRecords().forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 序号
                data.add(index + 1);
                // 地点
                data.add(StringUtils.parse("{}-{}", item.getSiteCode(), item.getSiteName()));
                // 业务类型
                data.add(item.getTypeName());
                // 单据编号
                data.add(item.getMatDnId());
                // 单据日期
                data.add(item.getMatDate());
                // 供应商编码
                data.add(StringUtils.parse("{}-{}", item.getSupSelfCode(), item.getSupName()));
                // 商品编码
                data.add(item.getProCode());
                // 商品通用名
                data.add(item.getProName());
                // 数量
                data.add(item.getMatQty());
                // 单价
                data.add(item.getPrice());
                // 金额
                data.add(item.getTotalAmount());
                // 去税金额
                data.add(item.getExcludingTaxAmount());
                // 税金
                data.add(item.getRateBat());
                // 已登记金额
                data.add(item.getRegisteredAmount());
                // 剩余金额
                data.add(item.getChargeAmount());
                // 规格
                data.add(item.getProSpecs());
                // 单位
                data.add(item.getProUnit());
                // 厂家
                data.add(item.getProFactoryName());
                dataList.add(data);
            }));
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(invoiceRegHeadList);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("发票登记明细");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            String fileName = String.valueOf(System.currentTimeMillis()).concat(RandomStringUtils.randomNumeric(3)).concat(".xls");
            Result result = cosUtils.uploadFile(bos, fileName);
            if (ResultUtil.hasError(result)) {
                exportTask.setGetStatus(3);
                exportTask.setGetReason("文件导出失败");
            } else {
                exportTask.setGetStatus(2);
                exportTask.setGetPath(applicationConfig.getExportPath() + fileName);
            }
            exportTaskMapper.updateById(exportTask);
        } catch (IOException e) {
            e.printStackTrace();
            exportTask.setGetStatus(3);
            exportTask.setGetReason(e.getMessage());
            exportTaskMapper.updateById(exportTask);
        }
    }

    @Override
    public ExportTask initExportTask() {
        ExportTask gaiaExportTask = iExportTaskService.initExportTask(1, 2);
        return gaiaExportTask;
    }

    /**
     * 已记账
     *
     * @param client
     * @param type
     * @param matDnId
     */
    private void resChajia(String client, String type, String matDnId) {
        if (!"CJ".equals(type)) {
            return;
        }
        ChajiaZ chajiaZ = new ChajiaZ();
        chajiaZ.setCjStatus("2");
        chajiaZMapper.update(chajiaZ, new LambdaQueryWrapper<ChajiaZ>().eq(ChajiaZ::getClient, client).eq(ChajiaZ::getCjId, matDnId));
    }

    /**
     * 导出数据表头
     */
    private String[] headDList = {
            "单据号",
            "商品编码",
            "商品名称",
            "规格",
            "数量",
            "商品去税金额",
            "商品去税税额",
            "单价",
            "含税金额",
    };


    /**
     * 付款单号获取DJ
     */
    private String getPaymentOrderNoByDB(String client) {
        String clientMaxOrderNo = fiSupplierPrepaymentMapper.getClientMaxOrderNo(client);
        if (ObjectUtils.isEmpty(clientMaxOrderNo)) {
            // 付款单号 = DJ+年月日（8位）+ 4位流水(/加盟商/天)
            clientMaxOrderNo = DateUtils.getCurrentDateStrYYMMDD() + "0001";
        } else {
            clientMaxOrderNo = new BigDecimal(clientMaxOrderNo).add(BigDecimal.ONE).toPlainString();
        }
        return clientMaxOrderNo;
    }

    /**
     * @param matDnList 订单列表
     * @param flag      订单的正负 1/-1
     * @return
     */
    private List<WmsDTO> sorMatDn(List<WmsDTO> matDnList, int flag) {
        if (flag > 0) {
            // 正序
            return matDnList.stream().sorted(Comparator.comparing(WmsDTO::getChargeAmount)).collect(Collectors.toList());
        } else {
            // 倒序
            return matDnList.stream().sorted(Comparator.comparing(WmsDTO::getChargeAmount).reversed()).collect(Collectors.toList());
        }
    }

    /**
     * @param invoiceList 发票列表
     * @param flag        发票的正负 1/-1
     * @return
     */
    private List<FicoInvoiceInformationRegistrationDTO> sortInvoice(List<FicoInvoiceInformationRegistrationDTO> invoiceList, int flag) {
        if (flag > 0) {
            // 正序
            return invoiceList.stream().sorted(Comparator.comparing(FicoInvoiceInformationRegistrationDTO::getInvoiceBalance)).collect(Collectors.toList());
        } else {
            // 倒序
            return invoiceList.stream().sorted(Comparator.comparing(FicoInvoiceInformationRegistrationDTO::getInvoiceBalance).reversed()).collect(Collectors.toList());
        }
    }

}
