package com.gov.operate.service;

import com.gov.operate.entity.SdPromGiftResult;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-11
 */
public interface ISdPromGiftResultService extends SuperService<SdPromGiftResult> {

}
