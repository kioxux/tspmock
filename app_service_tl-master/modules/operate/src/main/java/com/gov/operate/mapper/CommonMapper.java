package com.gov.operate.mapper;

import com.gov.operate.dto.StoreVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommonMapper {

    /**
     * 当前人门店编码集合
     *
     * @param client
     * @param userId
     * @return
     */
    List<StoreVo> getCurrentUserStores(@Param("client") String client, @Param("userId") String userId);
}
