package com.gov.operate.dto;

import com.gov.operate.entity.SdCleandrawerH;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.04.13
 */
@Data
public class CleanDrawerVO extends SdCleandrawerH {

    /**
     * 门店名
     */
    private String stoName;

    /**
     * 门店简称
     */
    private String stoShortName;

    /**
     * 门店名/门店简称
     */
    private String stoShowName;

    /**
     * 清斗人员
     */
    private String gschEmpName;

    /**
     * 复核人员
     */
    private String gschEmp1Name;
}

