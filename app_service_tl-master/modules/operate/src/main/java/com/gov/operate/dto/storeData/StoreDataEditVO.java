package com.gov.operate.dto.storeData;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StoreDataEditVO {

    @NotBlank(message = "门店名称不能为空")
    private String stoName;

    @NotBlank(message = "门店编码不能为空")
    private String stoCode;

    @NotBlank(message = "统一社会信用代码不能为空")
    private String stoNo;

//    @NotBlank(message = "法人不能为空")
    private String stoLegalPerson;

//    @NotBlank(message = "质量负责人不能为空")
    private String stoQua;

//    @NotBlank(message = "LOGO地址不能为空")
    private String stoLogo;

    @NotBlank(message = "负责人不能为空")
    private String stoLeader;

//    @NotBlank(message = "门店地址不能为空")
    private String stoAdd;

    @NotBlank(message = "门店状态不能为空")
    private String stoStatus;

//    @NotBlank(message = "门店组编码不能为空")
    private String stogCode;

    // @NotBlank(message = "纳税主体不能为空")
    private String stoTaxSubject;

    @NotBlank(message = "门店属性不能为空")
    private String stoAttribute;

    // @NotBlank(message = "连锁总部不能为空")
//    private String stoChainHead;
    private String client;
    private String stoYbcode;

    //门店简称
    @NotBlank(message = "门店简称不能为空")
    private String stoShortName;

    //省
    private String stoPrivince;

    //市
    private String stoCity;

    //区
    private String stoDistrict;

    //门店地址
    private String address;

    //一级区域
    private Long fristArea;

    //二级区域
    private Long secondArea;

    //三级区域
    private Long thirdArea;

    /**
     * 操作平台
     */
    private String platform;
}
