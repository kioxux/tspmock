package com.gov.operate.service.smsRecharge;

import com.gov.common.response.Result;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.17
 */
public interface ISmsRechargeManagService {

    /**
     * 客户短信统计
     *
     * @param pageNum
     * @param pageSize
     * @param client
     * @return
     */
    Result getSmsSendStatistic(Integer pageNum, Integer pageSize, String client);

    /**
     * 客户短信发送记录
     *
     * @param pageNum
     * @param pageSize
     * @param client
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    Result getSmsSendRecord(Integer pageNum, Integer pageSize, String client, String gssSendTimeStart, String gssSendTimeEnd);

    /**
     * 客户短信充值记录
     *
     * @param pageNum
     * @param pageSize
     * @param client
     * @param gsrrRechargeTimeStart
     * @param gsrrRechargeTimeEnd
     * @return
     */
    Result getRechargeRecord(Integer pageNum, Integer pageSize, String client, String gsrrRechargeTimeStart, String gsrrRechargeTimeEnd);

    /**
     * 客户集合
     * @return
     */
    Result getClientList();
}
