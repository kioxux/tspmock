package com.gov.operate.service.impl;

import com.gov.operate.entity.BillInvoiceEnd;
import com.gov.operate.mapper.BillInvoiceEndMapper;
import com.gov.operate.service.IBillInvoiceEndService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-25
 */
@Service
public class BillInvoiceEndServiceImpl extends ServiceImpl<BillInvoiceEndMapper, BillInvoiceEnd> implements IBillInvoiceEndService {

}
