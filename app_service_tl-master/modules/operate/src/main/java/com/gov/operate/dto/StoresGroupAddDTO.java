package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: jinwencheng
 * @Date: Created in 2021-12-24 13:41:30
 * @Description: 保存门店分类需要的参数
 * @Version: 1.0
 */
@Data
public class StoresGroupAddDTO {

    /**
     * 门店编码
     */
    @NotBlank(message = "门店编码不能为空")
    private String stoCode;

    /**
     * 门店名称
     */
    @NotBlank(message = "门店名称不能为空")
    private String stoName;

    /**
     * 分类类型编码
     */
    @NotBlank(message = "分类类型编码不能为空")
    private String gssgType;

    /**
     * 分类类型名称
     */
    @NotBlank(message = "分类类型名称不能为空")
    private String gssgTypeName;

    /**
     * 分类编码
     */
    @NotBlank(message = "分类编码不能为空")
    private String gssgId;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称不能为空")
    private String gssgIdName;

}
