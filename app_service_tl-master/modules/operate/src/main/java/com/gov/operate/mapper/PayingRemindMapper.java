package com.gov.operate.mapper;

import com.gov.operate.entity.PayingRemind;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加盟商余额 Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface PayingRemindMapper extends BaseMapper<PayingRemind> {

}
