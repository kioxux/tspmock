package com.gov.operate.service;

import com.gov.operate.entity.SdCleandrawerD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface ISdCleandrawerDService extends SuperService<SdCleandrawerD> {

}
