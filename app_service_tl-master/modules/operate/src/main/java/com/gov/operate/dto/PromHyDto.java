package com.gov.operate.dto;

import com.gov.operate.entity.SdPromHySet;
import com.gov.operate.entity.SdPromHyrDiscount;
import com.gov.operate.entity.SdPromHyrPrice;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromHyDto {

    private String gsphBeginDate;

    private String gsphEndDate;

    private String gsphDateFrequency;

    private String gsphWeekFrequency;

    private String gsphBeginTime;

    private String gsphEndTime;

    private String gsphNlFlag;

    private List<String> storeCodeList;

    private List<HySet> hySetList;

    private List<HyrDiscount> hyrDiscountList;

    private List<HyrPrice> hyrPriceList;


    @Data
    public static class HySet extends SdPromHySet{
//        private String stoCode;

//        private String stoName;

        private String proSelfCode;

        private String proCommonname;

        private String proName;

        private String proSpecs;

        private String proFactoryName;
    }

    @Data
    public static class HyrDiscount extends SdPromHyrDiscount {
        private String stoCode;

        private String stoName;

    }

    @Data
    public static class HyrPrice extends SdPromHyrPrice {
        private String stoCode;

        private String stoName;

        private String proSelfCode;

        private String proCommonname;

        private String proName;

        private String proSpecs;

        private String proFactoryName;
    }
}
