package com.gov.operate.dto.monthPushMoney;

import lombok.Data;

import java.util.List;

@Data
public class HomePushMoneyDTO {
    /**
     * 客户
     */
    private String client;

    /**
     * 查询类型： 1.今日，2：昨日，3：7日，4：7周，5：今年  6: 月
     */
    private String type;

    /**
     * 日期
     */
    private String gssdDate;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 营业员
     */
    private String salesclerk;

    /**
     * 提成月份
     */
    private String suitMonth;

    /**
     * 开始时间
     */
    private String startDate;

    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 同期开始时间
     */
    private String lastWeekStartDate;

    /**
     * 同期结束时间
     */
    private String lastWeekEndDate;

    /**
     * 门店code集合
     */
    private List<String> stoCodeList;
}
