package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_APP_VERSION")
@ApiModel(value="AppVersion对象", description="")
public class AppVersion extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "版本号")
    @TableField("VERSION_CODE")
    private String versionCode;

    @ApiModelProperty(value = "发布时间")
    @TableField("RELEASE_TIME")
    private String releaseTime;

    @ApiModelProperty(value = "是否强制更新  1:否,2:是")
    @TableField("FORCE_UPDATE_FLG")
    private Integer forceUpdateFlg;

    @ApiModelProperty(value = "是否发布 1:否,2:是")
    @TableField("RELEASE_FLAG")
    private Integer releaseFlag;

    @ApiModelProperty(value = "平台类型 1:android 2:ios")
    @TableField("TYPE")
    private Integer type;

    @ApiModelProperty(value = "更新内容")
    @TableField("CONTENT")
    private String content;

    @ApiModelProperty(value = "下载地址")
    @TableField("DOWNLOAD_PATH")
    private String downloadPath;


}
