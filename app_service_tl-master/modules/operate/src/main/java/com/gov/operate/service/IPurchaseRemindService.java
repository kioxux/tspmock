package com.gov.operate.service;

import com.gov.operate.entity.PurchaseRemind;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2021-07-08
 */
public interface IPurchaseRemindService extends SuperService<PurchaseRemind> {

    /**
     * 发送购药提醒
     */
    void sendPurchaseRemind();

}
