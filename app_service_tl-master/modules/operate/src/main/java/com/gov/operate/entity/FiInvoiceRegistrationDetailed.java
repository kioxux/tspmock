package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FI_INVOICE_REGISTRATION_DETAILED")
@ApiModel(value="FiInvoiceRegistrationDetailed对象", description="")
public class FiInvoiceRegistrationDetailed extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "业务类型")
    @TableField("BUSINESS_TYPE")
    private String businessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("MAT_DN_ID")
    private String matDnId;

    @ApiModelProperty(value = "物料凭证号")
    @TableField("MAT_ID")
    private String matId;

    @ApiModelProperty(value = "物料凭证年份")
    @TableField("MAT_YEAR")
    private String matYear;

    @ApiModelProperty(value = "物料凭证行号")
    @TableField("MAT_LINE_NO")
    private String matLineNo;

    @ApiModelProperty(value = "商品编码")
    @TableField("SO_PRO_CODE")
    private String soProCode;

    @ApiModelProperty(value = "行金额")
    @TableField("LINE_AMOUNT")
    private BigDecimal lineAmount;

    @ApiModelProperty(value = "行登记金额")
    @TableField("LINE_REGISTRATION_AMOUNT")
    private BigDecimal lineRegistrationAmount;

    @ApiModelProperty(value = "行已付款金额")
    @TableField("PAYMENT_AMOUNT_OF_LINE")
    private BigDecimal paymentAmountOfLine;


}
