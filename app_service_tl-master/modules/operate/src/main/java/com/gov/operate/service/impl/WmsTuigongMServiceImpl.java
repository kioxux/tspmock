package com.gov.operate.service.impl;

import com.gov.operate.entity.WmsTuigongM;
import com.gov.operate.mapper.WmsTuigongMMapper;
import com.gov.operate.service.IWmsTuigongMService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class WmsTuigongMServiceImpl extends ServiceImpl<WmsTuigongMMapper, WmsTuigongM> implements IWmsTuigongMService {

}
