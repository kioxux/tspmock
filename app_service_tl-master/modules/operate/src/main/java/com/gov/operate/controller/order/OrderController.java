package com.gov.operate.controller.order;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.order.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.06.21
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @Log("机构集合")
    @ApiOperation(value = "机构集合")
    @PostMapping("getFranchiseeList")
    public Result getFranchiseeList() {
        return orderService.getFranchiseeList();
    }

    @Log("人员集合")
    @ApiOperation(value = "人员集合")
    @PostMapping("getUserListByClient")
    public Result getUserListByClient(@RequestJson("client") String client) {
        return orderService.getUserListByClient(client);
    }
}
