package com.gov.operate.dto.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WorkOrder extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编号")
    private String gwoCode;

    @ApiModelProperty(value = "标题")
    private String gwoTitle;

    @ApiModelProperty(value = "类型")
    private Long gwoTypeId;

    @ApiModelProperty(value = "客户")
    private String gwoClient;

    @ApiModelProperty(value = "客户名")
    private String gwoClientName;

    @ApiModelProperty(value = "用户ID")
    private String gwoUserId;

    @ApiModelProperty(value = "用户名")
    private String gwoUserName;

    @ApiModelProperty(value = "联系手机号")
    private String gwoUserTel;

    @ApiModelProperty(value = "发起登录手机号")
    private String gwoSendTel;

    @ApiModelProperty(value = "提交时间")
    private LocalDateTime gwoSendTime;

    @ApiModelProperty(value = "级别")
    private Integer gwoLevel;

    @ApiModelProperty(value = "状态")
    private Integer gwoStatus;

    @ApiModelProperty(value = "接单人")
    private Long gwoTaker;

    @ApiModelProperty(value = "接单时间")
    private LocalDateTime gwoOrderTime;

    @ApiModelProperty(value = "当前处理人")
    private Long gwoCurrentTaker;

    @ApiModelProperty(value = "最后回复时间")
    private LocalDateTime gwoReplyTime;

    @ApiModelProperty(value = "备注")
    private String gwoRemarks;

    @ApiModelProperty(value = "关闭标识")
    private Integer gwoCloseFlag;

    @ApiModelProperty(value = "评分标识")
    private Integer gwoScoreFlag;

    @ApiModelProperty(value = "是否已解决问题")
    private Integer gwoUserScore;

    @ApiModelProperty(value = "问题系统")
    private Integer gwoSystem;

    @ApiModelProperty(value = "服务态度")
    private Integer gwoServiceAttitudeScore;

    @ApiModelProperty(value = "解决速度")
    private Integer gwoSpeedSolutionScore;

    @ApiModelProperty(value = "系统是否好用")
    private Integer gwoEasyToUseScore;

    @ApiModelProperty(value = "关闭时间")
    private LocalDateTime gwoCloseTime;


}
