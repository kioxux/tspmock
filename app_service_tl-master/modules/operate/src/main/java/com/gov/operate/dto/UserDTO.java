package com.gov.operate.dto;

import com.gov.operate.entity.UserData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserDTO {

    private String stoCode;

    private String stoName;

    private List<UserData> userDataList;
}
