package com.gov.operate.mapper;

import com.gov.operate.dto.zz.ZzInData;
import com.gov.operate.dto.zz.ZzOutData;
import com.gov.operate.dto.zz.ZzooOutData;
import com.gov.operate.entity.ZzData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-23
 */
public interface ZzDataMapper extends BaseMapper<ZzData> {
    List<ZzooOutData> getZzooList();

    List<ZzOutData> getZzList(ZzInData inData);
}
