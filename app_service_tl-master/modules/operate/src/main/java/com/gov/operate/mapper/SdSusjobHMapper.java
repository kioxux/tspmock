package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SdSusjobHDTO;
import com.gov.operate.dto.SusjobTaskListQuery;
import com.gov.operate.entity.SdSusjobD;
import com.gov.operate.entity.SdSusjobH;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.service.request.SdSusjobHRequest;
import com.gov.operate.service.response.SdSusjobHResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
public interface SdSusjobHMapper extends BaseMapper<SdSusjobH> {

    /**
     * 维系任务
     */
    IPage<SdSusjobH> querySusjobTaskList(Page<SusjobTaskListQuery> page, @Param("susjobTaskListQuery") SusjobTaskListQuery susjobTaskListQuery);

    /**
     * 维系任务列表
     */
    IPage<SdSusjobHDTO> getSusjobList(Page<SdSusjobHDTO> page,
                                      @Param("ew") QueryWrapper<SdSusjobHDTO> ew,
                                      @Param("client") String client,
                                      @Param("userId") String userId);

    /**
     * 我的维系任务
     *
     * @param page
     * @return
     */
    IPage<SdSusjobH> queryMySusjobTaskList(Page<SdSusjobH> page, @Param("client") String client, @Param("userId") String userId);


    List<SdSusjobH> selectByTimeAndStatus(@Param("date") String date,@Param("status") String status);

    void updateStatus(@Param("client") String client,@Param("susJobid") String susJobid,@Param("susJobbr") String susJobbr,@Param("status") String status);


    IPage<SdSusjobHResponse> querySusjobTaskAppList(Page<SdSusjobHResponse> iPage, @Param("client") String client, @Param("userId")String userId,@Param("request") SdSusjobHRequest request);

    IPage<SdSusjobHResponse> susjobTaskAppList(Page<SdSusjobHResponse> iPage, @Param("client") String client,@Param("request") SdSusjobHRequest request);

    /**
     * 获取维系任务类型数量
     * @param collect
     * @return
     */
    List<SdSusjobHResponse> getTaskCount(@Param("collect") List<String> collect,@Param("client")String client,@Param("userId")String userId,@Param("status")String status);
}
