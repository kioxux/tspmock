package com.gov.operate.mapper;

import com.gov.operate.entity.GaiaSdOutOfStockPushRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdOutOfStockPushRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdOutOfStockPushRecord record);

    int insertSelective(GaiaSdOutOfStockPushRecord record);

    GaiaSdOutOfStockPushRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdOutOfStockPushRecord record);

    int updateByPrimaryKey(GaiaSdOutOfStockPushRecord record);

    int updateBatch(List<GaiaSdOutOfStockPushRecord> list);

    int batchInsert(@Param("list") List<GaiaSdOutOfStockPushRecord> list);

    List<GaiaSdOutOfStockPushRecord> getListByCondition(GaiaSdOutOfStockPushRecord record);


}