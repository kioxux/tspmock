package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSON;
import com.gov.common.basic.*;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.SmsEntity;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.marketing.SmsRechargeRecordDTO;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdExasearchDMapper;
import com.gov.operate.mapper.SdMarketingSearchMapper;
import com.gov.operate.mapper.SmsTemplateMapper;
import com.gov.operate.service.CustomMultiThreadingService;
import com.gov.operate.service.ISdExasearchDService;
import com.gov.operate.service.ISdMarketingSearchService;
import com.gov.operate.service.ISdMemberBasicService;
import com.gov.operate.service.smsRecharge.ISmsRechargeRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomMultiThreadingServiceImpl implements CustomMultiThreadingService {

    @Resource
    private SmsUtils smsUtils;
    @Resource
    private SmsTemplateMapper smsTemplateMapper;
    @Resource
    private ISdExasearchDService sdExasearchDService;
    @Resource
    private SdExasearchDMapper sdExasearchDMapper;
    @Resource
    private ISdMemberBasicService sdMemberBasicService;
    @Resource
    private ISdMarketingSearchService marketingSearchService;
    @Resource
    private SdMarketingSearchMapper marketingSearchMapper;
    @Resource
    private ISmsRechargeRecordService smsRechargeRecordService;
    @Resource
    private ApplicationConfig applicationConfig;

    /**
     * 营销短信发送
     */
    @Async
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendMarketingSms(SendMarketingSmsVO vo, TokenUser user) {
        // 加盟商
        String client = user.getClient();
        // 短信模版
        SmsTemplate template = smsTemplateMapper.selectById(vo.getSmsId());
        // 短信批量发送最大条数
        Integer smsBatchCnt = applicationConfig.getSmsBatchCnt() == null ? 100 : applicationConfig.getSmsBatchCnt();
        synchronized (Constants.LOCK_SEND_MARKETING_SMS) {
            List<SdExasearchD> list = new ArrayList<>();
            // 短信剩余条数
            List<SmsRechargeRecordDTO> smsRechargeRecordList = marketingSearchMapper.smsRechargeRecord(client);
            AtomicInteger smsRemainCount = new AtomicInteger(smsRechargeRecordList.stream().filter(Objects::nonNull).mapToInt(SmsRechargeRecordDTO::getGsrrRechargeRemainCount).sum());
            int smsRemainCountTemp = smsRemainCount.get();
            LinkedList<SmsRechargeRecordDTO> smsRechargeRecordAllList = smsRechargeRecordList.stream()
                    .sorted(Comparator.comparing(SmsRechargeRecordDTO::getGsrrRechargeRemainCount))
                    .collect(Collectors.toCollection(LinkedList::new));
            LinkedList<SmsRechargeRecordDTO> smsRechargeRecordCurrentList = new LinkedList<>();
            List<SmsSend> smsSendList = new ArrayList<>();
            List<SmsEntity> smsEntityList = new ArrayList<>();
            for (MemberVO item : vo.getMemberList()) {
                try {
                    // 人员姓名,积分
                    GetMemberDO member = smsTemplateMapper.getMember(client, item.getMemberId(), item.getMemberCard(), item.getStoreCode());
                    // 判断是否发送成功，如成功则跳过，若失败则再次发送
                    SdExasearchD exaSearchD = sdExasearchDMapper.getExaSearchD(client, DateUtils.getCurrentDateTimeStrTwo(), item.getMemberCard());
                    if (!ObjectUtils.isEmpty(exaSearchD) && "0".equals(exaSearchD.getGseStatus())) {
                        continue;
                    }
                    // 判断 同-营销活动下,同一门店,同一短信 同一个人 只能发送一次
                    if (StringUtils.isNotEmpty(vo.getMarketId())) {
                        int count = sdExasearchDMapper.countSuccesMarket(client, item.getMemberCard(), item.getStoreCode(), vo.getMarketId(), vo.getSmsId());
                        if (count > 0) {
                            continue;
                        }
                    }
                    // 短信是否有剩余
                    if (smsRemainCount.get() == 0) {
                        log.info("加盟商{}可用短信数量不足, 本次发送了{}条", client, smsRemainCountTemp);
                        break;
                    }
                    // 签名
                    String sign = null;
                    if (StringUtils.isNotEmpty(item.getGmbBrShortName()) && item.getGmbBrShortName().length() < 20) {
                        sign = splicing(item.getGmbBrShortName());
                    } else {
                        int start = 0;
                        int end = Math.min(item.getSmsSgin().length(), 20);
                        sign = splicing(item.getSmsSgin().substring(start, end));
                    }
                    // 短息内容
                    String msgTemp = template.getSmsContent().replaceFirst(Constants.SMS_COMTENT_PD_REGEX, sign)
                            .replace(Constants.SMS_COMTENT_PD_NAME, splicing(member.getGsmbName()))
                            .replace(Constants.SMS_COMTENT_PD_SCOPE, splicing(member.getGsmbcIntegral()));
//                    String msgTemp ="【大喆堂药房】大药房11月11日-12日“嗨购双11，疯狂68折起，购物还享4重礼”，地址：姑苏路42号，回复TD退订。\n";
                    // 短信发送
                    String gseCretime = DateUtils.getCurrentDateTimeStrTwo();
                    SmsEntity smsEntity = new SmsEntity();
                    smsEntity.setSmsId(template.getSmsTemplateId());
                    smsEntity.setPhone(item.getMemberPhone());
                    smsEntity.setMsg(msgTemp + SmsUtils.EXT);
                    String gseStatus = null;
                    try {
//                        Result result = smsUtils.sendMarketingSms(smsEntity);
                        smsEntityList.add(smsEntity);
//                        log.info("短信发送结果{}", JsonUtils.beanToJson(result));
                        // 0-发送成功, 1-发送失败
//                        if (!ObjectUtils.isEmpty(result) && ResultEnum.SUCCESS.getCode().equals(result.getCode())) {
                        gseStatus = "0";
                        // 短信剩余条数减一
                        SmsRechargeRecordDTO currentRechargeRecord = CollectionUtils.isEmpty(smsRechargeRecordCurrentList) ? null : smsRechargeRecordCurrentList.getFirst();
                        if (ObjectUtils.isEmpty(currentRechargeRecord) || currentRechargeRecord.getGsrrRechargeRemainCount() == 0) {
                            currentRechargeRecord = smsRechargeRecordAllList.pop();
                            smsRechargeRecordCurrentList.addFirst(currentRechargeRecord);
                        }
                        // 剩余数量减一
                        currentRechargeRecord.setGsrrRechargeRemainCount(currentRechargeRecord.getGsrrRechargeRemainCount() - 1);
                        // 使用数量加一
                        currentRechargeRecord.setGsrrRechargeUseCount(currentRechargeRecord.getGsrrRechargeUseCount() + 1);
                        // 总剩余数量减一
                        smsRemainCount.getAndDecrement();

                        // 每条短信的消费记录
                        smsSendList.add(saveSmsSend(client, item.getMemberPhone(), msgTemp, item.getStoreCode(),
                                currentRechargeRecord.getGsrrRechargePrice(), currentRechargeRecord.getId()));
//                        } else {
//                            gseStatus = "1";
//                        }
                    } catch (Exception e) {
                        gseStatus = "1";
                        log.info(e.getMessage());
                    }
                    String gseSendtime = DateUtils.getCurrentDateTimeStrTwo();
                    list.add(saveSmsRecord(vo, client, template.getSmsId(), item, member, gseStatus, gseCretime, gseSendtime));

                } catch (Exception e) {
                    log.info(e.getMessage());
                }
            }
            if (!CollectionUtils.isEmpty(list)) {
                sdExasearchDService.saveBatch(list);
            }
            // 更新短信剩余条数
            if (!CollectionUtils.isEmpty(smsRechargeRecordCurrentList)) {
                smsRechargeRecordCurrentList.forEach(smsRechargeRecordDTO -> {
                    SmsRechargeRecord rechargeRecord = new SmsRechargeRecord();
                    rechargeRecord.setId(smsRechargeRecordDTO.getId());
                    rechargeRecord.setGsrrRechargeUseCount(smsRechargeRecordDTO.getGsrrRechargeUseCount());
                    smsRechargeRecordService.updateById(rechargeRecord);
                });
            }
            if (!CollectionUtils.isEmpty(smsSendList)) {
                marketingSearchMapper.insertSmsSendList(smsSendList);
            }
            try {
                // 明细数据
                if (!CollectionUtils.isEmpty(smsEntityList)) {
                    int cnt = 0;
                    // 500条数据
                    for (int i = 0; i < list.size(); i += smsBatchCnt.intValue()) {
                        List<SmsEntity> newList = null;
                        if (cnt * smsBatchCnt.intValue() + smsBatchCnt.intValue() > list.size()) {
                            newList = smsEntityList.subList(cnt * smsBatchCnt.intValue(), list.size());
                        } else {
                            newList = smsEntityList.subList(cnt * smsBatchCnt.intValue(), cnt * smsBatchCnt.intValue() + smsBatchCnt.intValue());
                        }
                        if (!CollectionUtils.isEmpty(newList)) {
                            SmsEntity smsEntity = newList.get(0);
                            List<String> phoneList = newList.stream().
                                    filter(Objects::nonNull).filter(t -> StringUtils.isNotBlank(t.getPhone())).map(SmsEntity::getPhone).collect(Collectors.toList());
                            if (!CollectionUtils.isEmpty(phoneList)) {
                                smsEntity.setPhone(StringUtils.join(phoneList, ","));
                                Result result = smsUtils.sendMarketingSms(smsEntity);
                                log.info("短信发送结果{}", JsonUtils.beanToJson(result));
                            }
                        }
                        cnt++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 每条短信的消费记录
     *
     * @param client              加盟商
     * @param phone               手机
     * @param msgTemp             内容
     * @param sto                 门店
     * @param rechargeRecordPrice 单价
     * @param rechargeRecordId    购买记录id
     * @return
     */
    private SmsSend saveSmsSend(String client, String phone, String msgTemp, String sto, Integer rechargeRecordPrice, Integer rechargeRecordId) {
        SmsSend smsSend = new SmsSend();
        // 加盟商
        smsSend.setClient(client);
        // 手机号
        smsSend.setGssTel(phone);
        // 发送时间
        smsSend.setGssSendTime(DateUtils.getCurrentDateTimeStrTwo());
        // 发送内容
        smsSend.setGssSendContent(msgTemp);
        // 是否已开票
        smsSend.setGssInvoice(0);
        // 所属门店
        smsSend.setGssStore(sto);
        // 发送单价
        smsSend.setGssPrice(rechargeRecordPrice);
        // 充值记录ID
        smsSend.setGsrrId(rechargeRecordId);
        return smsSend;
    }

    /**
     * 保存短信发送内容
     *
     * @param vo          常用查询 以及 营销活动
     * @param client      加盟商
     * @param smsId       短信id
     * @param item        会员卡信息
     * @param member      会员基础信息
     * @param gseStatus   短信发送状态
     * @param gseCretime  短信创建时间
     * @param gseSendtime 短信发送时间
     * @return 发送记录实体
     */
    private SdExasearchD saveSmsRecord(SendMarketingSmsVO vo, String client, String smsId, MemberVO item, GetMemberDO member, String gseStatus, String gseCretime, String gseSendtime) {
        SdExasearchD sdExasearchD = new SdExasearchD();
        // 状态
        sdExasearchD.setGseStatus(gseStatus);
        // 加盟商 /当前加盟商
        sdExasearchD.setClient(client);
        // 营销活动id
        sdExasearchD.setGseMarketid(vo.getMarketId());
        // 查询id /年月日时分秒
        sdExasearchD.setGseSearchid(UUIDUtil.getUUID());
        // 查询主题 /常用查询id
        sdExasearchD.setGseTheid(vo.getGsmsId());
        // 会员卡号
        sdExasearchD.setGseCardId(item.getMemberCard());
        // 会员姓名
        sdExasearchD.setGseMenname(member.getGsmbName());
        // 会员电话
        sdExasearchD.setGseMobile(member.getGsmbMobile());
        // 性别
        sdExasearchD.setGseSex(member.getGsmbSex());
        // 年龄
        sdExasearchD.setGseAge(member.getGsmbAge());
        // 生日
        sdExasearchD.setGseBirth(member.getGsmbBirth());
        // 积分
        sdExasearchD.setGseIntegral(new BigDecimal(member.getGsmbcIntegral()));
        // 所属门店编码
        sdExasearchD.setGseBrId(item.getStoreCode());
        // 所属门店名称
        sdExasearchD.setGseBrName(item.getSmsSgin());
        // 会员卡类型
        sdExasearchD.setGseClassId(member.getGsmbcClassId());
        // 最后积分日期
        sdExasearchD.setGseIntegralLastdate(member.getGsmbcIntegralLastdate());
        // 会员级别
        sdExasearchD.setGseGlid("");
        // 会员微信
        sdExasearchD.setGseWxid("");
        // 是否发送短信 /
        sdExasearchD.setGseSms("0");
        // 是否发送微信 /
        sdExasearchD.setGseWx("1");
        // 类型 /1-短信, 2-电话
        sdExasearchD.setGseNoticeType("1");
        // 信息创建时间
        sdExasearchD.setGseCretime(gseCretime);
        // 信息发送时间
        sdExasearchD.setGseSendtime(gseSendtime);
        // 短信模板ID
        sdExasearchD.setGseTempid(smsId);
        return sdExasearchD;
    }

    /**
     * 营销任务立即执行 发送短信到门店
     *
     * @param storeList 门店列表
     * @param vo        营销活动
     */
    @Async
    @Override
    public void sendMarketTaskSms(List<StoreData> storeList, GsmTaskSaveVo vo) {
        if (CollectionUtils.isEmpty(storeList)) {
            return;
        }
        if (ObjectUtils.isEmpty(vo)) {
            return;
        }
        // 短信模版
        SmsTemplate template = smsTemplateMapper.selectById(vo.getGsmTempid());
        if (ObjectUtils.isEmpty(template)) {
            return;
        }

        storeList.forEach(store -> {
            // 短信查询条件JSON
            String gsmSmscondi = vo.getGsmSmscondi();
            // 短信人数等于0或空，直接return,不发送短信
            if (ObjectUtils.isEmpty(vo.getGsmSmstimes()) || vo.getGsmSmstimes().compareTo(new BigDecimal("0")) == 0) {
                return;
            }

            GetMemberListVO getMemberListVO = null;
            if (StringUtils.isEmpty(gsmSmscondi)) {
                getMemberListVO = new GetMemberListVO();
            } else {
                try {
                    SdMarketingSearch marketingSearch = marketingSearchService.getById(gsmSmscondi);
                    getMemberListVO = JSON.parseObject(marketingSearch.getGsmsDetail(), GetMemberListVO.class);
                } catch (Exception e) {
                    log.info("获取会员列表失败", e.getMessage());
                }
            }
            getMemberListVO.setGsmSmstimes(vo.getGsmSmstimes());
            List<GetMemberListDTO> memberList = sdMemberBasicService.getMemberList(getMemberListVO);
            if (CollectionUtils.isEmpty(memberList)) {
                return;
            }
            synchronized (Constants.LOCK_SEND_MARKET_TASK_SMS) {
                List<SdExasearchD> list = new ArrayList<>();
                memberList.forEach(memberDTO -> {
                    try {
                        // 判断是否发送成功，如成功则跳过，若失败则再次发送
                        SdExasearchD exaSearchD = sdExasearchDMapper.getExaSearchD(vo.getClient(), DateUtils.getCurrentDateTimeStrTwo(), memberDTO.getGmbCardId());
                        if (!ObjectUtils.isEmpty(exaSearchD) && "0".equals(exaSearchD.getGseStatus())) {
                            return;
                        }
                        // 签名
                        String sign = null;
                        if (StringUtils.isNotEmpty(store.getStoName())) {
                            if (store.getStoName().length() < 20) {
                                sign = splicing(store.getStoName());
                            } else {
                                sign = splicing(store.getStoName().substring(0, 20));
                            }
                        } else {
                            sign = splicing(store.getStoShortName());
                        }

                        // 短息内容
                        String msgTemp = template.getSmsContent().replaceFirst(Constants.SMS_COMTENT_PD_REGEX, sign)
                                .replace(Constants.SMS_COMTENT_PD_NAME, splicing(memberDTO.getGsmbName()))
                                .replace(Constants.SMS_COMTENT_PD_SCOPE, splicing(memberDTO.getGmbIntegral()));

                        String gseCretime = DateUtils.getCurrentDateTimeStrTwo();
                        Result result = null;
                        try {
                            SmsEntity smsEntity = new SmsEntity();
                            smsEntity.setSmsId(template.getSmsTemplateId());
                            smsEntity.setPhone(memberDTO.getGsmbMobile());
                            smsEntity.setMsg(msgTemp);
                            result = smsUtils.sendMarketingSms(smsEntity);
                        } catch (Exception e) {
                            log.info(e.getMessage());
                        }
                        String gseSendtime = DateUtils.getCurrentDateTimeStrTwo();

                        SdExasearchD sdExasearchD = new SdExasearchD();
                        list.add(sdExasearchD);
                        // 0-发送成功, 1-发送失败
                        if (!ObjectUtils.isEmpty(result) && ResultEnum.SUCCESS.getCode().equals(result.getCode())) {
                            sdExasearchD.setGseStatus("0");
                        } else {
                            sdExasearchD.setGseStatus("1");
                        }
                        // 加盟商 /当前加盟商
                        sdExasearchD.setClient(vo.getClient());
                        // 营销活动id
                        sdExasearchD.setGseMarketid(vo.getGsmMarketid());
                        // 查询id /年月日时分秒
                        sdExasearchD.setGseSearchid(UUIDUtil.getUUID());
                        // 查询主题 /常用查询id

                        sdExasearchD.setGseTheid(vo.getGsmSmscondi());
                        // 会员卡号
                        sdExasearchD.setGseCardId(memberDTO.getGmbCardId());
                        // 会员姓名
                        sdExasearchD.setGseMenname(memberDTO.getGmbName());
                        // 会员电话
                        sdExasearchD.setGseMobile(memberDTO.getGmbMobile());
                        // 性别
                        sdExasearchD.setGseSex(memberDTO.getGmbSex());
                        // 年龄
                        sdExasearchD.setGseAge(memberDTO.getGmbAge());
                        // 生日
                        sdExasearchD.setGseBirth(memberDTO.getGmbBirth());
                        // 积分
                        sdExasearchD.setGseIntegral(new BigDecimal(memberDTO.getGmbIntegral()));
                        // 所属门店编码
                        sdExasearchD.setGseBrId(store.getStoCode());
                        // 所属门店名称
                        sdExasearchD.setGseBrName(store.getStoName());
                        // 会员卡类型
                        sdExasearchD.setGseClassId(memberDTO.getGsmbcClassId());
//                         最后积分日期
                        sdExasearchD.setGseIntegralLastdate(memberDTO.getGsmbcIntegralLastdate());
                        // 会员级别
                        sdExasearchD.setGseGlid("");
                        // 会员微信
                        sdExasearchD.setGseWxid("");
                        // 是否发送短信 /
                        sdExasearchD.setGseSms("0");
                        // 是否发送微信 /
                        sdExasearchD.setGseWx("1");
                        // 类型 /1-短信, 2-电话
                        sdExasearchD.setGseNoticeType("1");
                        // 信息创建时间
                        sdExasearchD.setGseCretime(gseCretime);
                        // 信息发送时间
                        sdExasearchD.setGseSendtime(gseSendtime);
                        // 短信模板ID
                        sdExasearchD.setGseTempid(template.getSmsId());
                    } catch (Exception e) {
                        log.info(e.getMessage());
                    }
                });
                sdExasearchDService.saveBatch(list);
            }
        });
    }

    private String splicing(String source) {
        return "【" + source + "】";
    }

}
