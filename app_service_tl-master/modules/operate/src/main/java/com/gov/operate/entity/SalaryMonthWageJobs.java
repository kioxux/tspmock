package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_MONTH_WAGE_JOBS")
@ApiModel(value="SalaryMonthWageJobs对象", description="")
public class SalaryMonthWageJobs extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSMWJ_ID", type = IdType.AUTO)
    private Integer gsmwjId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSMWJ_GSP_ID")
    private Integer gsmwjGspId;

    @ApiModelProperty(value = "日销售额最小值")
    @TableField("GSMWJ_SALES_MIN")
    private BigDecimal gsmwjSalesMin;

    @ApiModelProperty(value = "日销售额最大值")
    @TableField("GSMWJ_SALES_MAX")
    private BigDecimal gsmwjSalesMax;

    @ApiModelProperty(value = "岗位")
    @TableField("GSMWJ_JOB")
    private String gsmwjJob;

    @ApiModelProperty(value = "岗位名称")
    @TableField("GSMWJ_JOB_NAME")
    private String gsmwjJobName;

    @ApiModelProperty(value = "工资")
    @TableField("GSMWJ_WAGE")
    private BigDecimal gsmwjWage;

    @ApiModelProperty(value = "岗位顺序")
    @TableField("GSMWJ_JOB_SORT")
    private Integer gsmwjJobSort;

    @ApiModelProperty(value = "销售额顺序")
    @TableField("GSMWJ_SALES_SORT")
    private Integer gsmwjSalesSort;


}
