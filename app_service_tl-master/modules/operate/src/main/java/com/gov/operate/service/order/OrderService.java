package com.gov.operate.service.order;

import com.gov.common.response.Result;
import com.gov.operate.entity.Franchisee;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.06.21
 */
public interface OrderService {
    /**
     * 机构集合
     *
     * @return
     */
    Result getFranchiseeList();

    /**
     * 加盟商人员
     * @param client
     * @return
     */
    Result getUserListByClient(String client);
}

