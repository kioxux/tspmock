package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddMarketingTemplateVO {

    @NotBlank(message = "模板主题不能为空")
    private String smsTheme;

    @NotBlank(message = "模板内容不能为空")
    private String smsContent;

}
