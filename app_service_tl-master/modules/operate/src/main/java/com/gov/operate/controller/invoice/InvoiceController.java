package com.gov.operate.controller.invoice;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.ExportSelectWarehousingDetailsVO;
import com.gov.operate.entity.ExportTask;
import com.gov.operate.service.IFicoInvoiceInformationRegistrationService;
import com.gov.operate.service.InvoiceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Validated
@RestController
@RequestMapping("invoice/invoice")
public class InvoiceController {

    @Resource
    private InvoiceService invoiceService;
    @Resource
    private IFicoInvoiceInformationRegistrationService ficoInvoiceInformationRegistrationService;

    @Log("采购地点")
    @ApiOperation(value = "采购地点")
    @GetMapping("getPurcSite")
    public Result getPurcSite() {
        return ResultUtil.success(invoiceService.getPurcSite());
    }

    @Log("单体店")
    @ApiOperation(value = "单体店")
    @GetMapping("getStoList")
    public Result getStoList() {
        return ResultUtil.success(invoiceService.getStoList());
    }

    @Log("供应商")
    @ApiOperation(value = "供应商")
    @GetMapping("getSupplier")
    public Result getSupplier(@RequestParam("site") String site) {
        return ResultUtil.success(invoiceService.getSupplier(site));
    }

    @Log("发票登记")
    @ApiOperation(value = "发票登记")
    @PostMapping("addInvoice")
    public Result addInvoice(@Valid @RequestBody List<AddInvoiceVO> list) {
        ficoInvoiceInformationRegistrationService.addInvoice(list);
        return ResultUtil.success();
    }

    @Log("可用发票")
    @ApiOperation(value = "可用发票")
    @GetMapping("selectAvailableInvoice")
    public Result selectAvailableInvoice(@RequestParam(value = "type", required = false) String type,
                                         @RequestParam(value = "supCode", required = false) String supCode,
                                         @RequestParam(value = "site", required = false) String site) {
        return ResultUtil.success(ficoInvoiceInformationRegistrationService.selectAvailableInvoice(type, supCode, site));
    }

    @Log("出入库查询")
    @ApiOperation(value = "出入库查询")
    @PostMapping("selectWarehousing")
    public Result selectWarehousing(@RequestBody @Valid SelectWarehousingVO vo) {
        return ResultUtil.success(invoiceService.selectWarehousing(vo));
    }

    @Log("出入库明细对账查询")
    @ApiOperation(value = "出入库明细对账查询")
    @PostMapping("selectWarehousingList")
    public Result selectWarehousingList(@RequestBody @Valid SelectWarehousingVO vo) {
        return ResultUtil.success(invoiceService.selectWarehousingList(vo));
    }

    @Log("出入库明细对账导出")
    @ApiOperation(value = "出入库明细对账导出")
    @PostMapping("selectWarehousingListExport")
    public Result selectWarehousingListExport(@RequestBody @Valid SelectWarehousingVO vo) throws IOException {
        // 生成导出任务
        ExportTask exportTask = invoiceService.initExportTask();
        invoiceService.selectWarehousingListExport(vo, exportTask);
        return ResultUtil.success();
    }

    @Log("出入库查询金额合计")
    @ApiOperation(value = "出入库查询金额合计")
    @PostMapping("selectWarehousingAmt")
    public Result selectWarehousingAmt(@RequestBody SelectWarehousingVO vo) {
        return invoiceService.selectWarehousingAmt(vo);
    }

    @Log("出入库明细")
    @ApiOperation(value = "出入库明细")
    @PostMapping("selectWarehousingDetails")
    public Result selectWarehousingDetails(@RequestBody @Valid SelectWarehousingDetailsVO vo) {
        return ResultUtil.success(invoiceService.selectWarehousingDetails(vo));
    }

    @Log("订单发票确认")
    @ApiOperation(value = "订单发票确认")
    @PostMapping("orderInvoice")
    public Result orderInvoice(@RequestBody @Valid OrderInvoiceVO vo) {
        invoiceService.orderInvoice(vo);
        return ResultUtil.success();
    }

    @Log("订单发票批量确认")
    @ApiOperation(value = "订单发票批量确认")
    @PostMapping("orderBatchInvoice")
    public Result orderBatchInvoice(@RequestBody OrderInvoiceVO vo) {
        invoiceService.orderBatchInvoice(vo);
        return ResultUtil.success();
    }

    @Log("订单发票批量确认2")
    @ApiOperation(value = "订单发票批量确认2")
    @PostMapping("orderBatchInvoice2")
    public Result orderBatchInvoice2(@RequestBody @Valid OrderBatchInvoiceVO vo) {
        invoiceService.orderBatchInvoice2(vo);
        return ResultUtil.success();
    }

    @Log("订单明细发票确认")
    @ApiOperation(value = "订单明细发票确认")
    @PostMapping("orderDetailInvoice")
    public Result orderDetailInvoice(@RequestBody @Valid OrderDetailInvoiceVO vo) {
        invoiceService.orderDetailInvoice(vo);
        return ResultUtil.success();
    }

    @Log("订单明细发票确认2")
    @ApiOperation(value = "订单明细发票确认2")
    @PostMapping("orderDetailInvoice2")
    public Result orderDetailInvoice2(@RequestBody List<OrderDetailInvoiceVO> list) {
        invoiceService.orderDetailInvoice2(list);
        return ResultUtil.success();
    }

    @Log("根据发票号码来查询发票信息登记")
    @ApiOperation(value = "根据发票号码来查询发票信息登记")
    @GetMapping("getInvoice")
    public Result selectInvoice(@RequestParam("invoiceNum") String invoiceNum) {
        return ficoInvoiceInformationRegistrationService.selectInvoice(invoiceNum);
    }

    @Log("根据发票号码来更新发票信息登记")
    @ApiOperation(value = "根据发票号码来更新发票信息登记")
    @PostMapping("updateInvoice")
    public Result editInvoice(@RequestBody FicoInvoiceInformationRegistrationDTO registrationDTO) {
        return ficoInvoiceInformationRegistrationService.editInvoice(registrationDTO);
    }

    @Log("出入库查询_预付，未登记发票数据")
    @ApiOperation(value = "出入库查询_预付，未登记发票数据")
    @PostMapping("selectPrepaidBill")
    public Result selectPrepaidBill(@RequestBody @Valid SelectWarehousingVO vo) {
        return invoiceService.selectPrepaidBill(vo);
    }

    @Log("出入库查询_预付，未登记发票数据")
    @ApiOperation(value = "出入库查询_预付，未登记发票数据")
    @PostMapping("selectPrepaidBillDetail")
    public Result selectPrepaidBillDetail(@RequestBody @Valid SelectWarehousingVO vo) {
        return invoiceService.selectPrepaidBillDetail(vo);
    }

    @Log("供应商单据预付")
    @ApiOperation(value = "供应商单据预付")
    @PostMapping("savePrepaidBill")
    public Result savePrepaidBill(@RequestBody List<SelectWarehousingDTO> list) {
        return invoiceService.savePrepaidBill(list);
    }

    @Log("供应商明细预付")
    @ApiOperation(value = "供应商明细预付")
    @PostMapping("savePrepaidDetail")
    public Result savePrepaidDetail(@RequestBody List<SelectPrepaidBillDetailVO> list) {
        return invoiceService.savePrepaidDetail(list);
    }

    @Log("供应商单据预付导出")
    @ApiOperation(value = "供应商单据预付导出")
    @PostMapping("exportSelectPrepaidBill")
    public Result exportSelectPrepaidBill(@RequestBody @Valid SelectWarehousingVO vo) throws IOException {
        return invoiceService.exportSelectPrepaidBill(vo);
    }

    @Log("供应商明细预付导出")
    @ApiOperation(value = "供应商明细预付导出")
    @PostMapping("exportSelectPrepaidBillDetail")
    public Result exportSelectPrepaidBillDetail(@RequestBody @Valid SelectWarehousingVO vo) throws IOException {
        return invoiceService.exportSelectPrepaidBillDetail(vo);
    }

    @Log("供应商单据预付明细导出")
    @ApiOperation(value = "供应商单据预付明细导出")
    @PostMapping("exportSelectWarehousingDetails")
    public Result exportSelectWarehousingDetails(@RequestBody @Valid ExportSelectWarehousingDetailsVO vo) {
        return invoiceService.exportSelectWarehousingDetails(vo);
    }

    @Log("付款累计金额是否允许大于单据金额")
    @ApiOperation(value = "付款累计金额是否允许大于单据金额")
    @PostMapping("gysdjyf")
    public Result gysdjyf() {
        return invoiceService.gysdjyf();
    }

}
