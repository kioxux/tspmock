package com.gov.operate.dto;

import com.gov.operate.dto.marketing.GetMemberListVO3;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CreateSusjobVO extends GetMemberListVO3 {

    @NotBlank(message = "任务名称不能为空")
    private String susJobname;

    @NotBlank(message = "计划开始时间不能为空")
    private String susBegin;

    @NotBlank(message = "计划结束时间不能为空")
    private String susEnd;

    /**
     * 维系说明
     */
    private String susExplain;

    /**
     * 任务门店类型
     */
    @NotBlank(message = "任务门店类型不能为空")
    private String susType;

    /**
     * 开卡员工:1:开，0:关
     */
    private String susEmp;

    /**
     * 维系话术
     */
    private String susContent;

    /**
     * 推送日期
     */
    private String susPushDate;
    /**
     * 推送时间
     */
    private String susPushTime;

}
