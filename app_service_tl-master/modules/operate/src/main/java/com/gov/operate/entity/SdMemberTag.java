package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_TAG")
@ApiModel(value="SdMemberTag对象", description="")
public class SdMemberTag extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSMTG_CARD_ID")
    private String gsmtgCardId;

    @ApiModelProperty(value = "标签编号")
    @TableField("GSMTG_FLAG")
    private String gsmtgFlag;

    @ApiModelProperty(value = "生效状态")
    @TableField("GSMTG_STATUS")
    private String gsmtgStatus;

    @ApiModelProperty(value = "更新日期")
    @TableField("GSMTG_UPDATE_DATE")
    private String gsmtgUpdateDate;


}
