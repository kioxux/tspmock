package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GsmTaskListVO extends Pageable {

    /**
     *
     */
    private String client;

    /**
     * 活动状态 （0-未生效 1-已生效）
     */
    private String actStatus;

    /**
     * 门店组ID
     */
    private String stogCode;

    /**
     * 门店编号
     */
    private String gsmStore;

    /**
     * 商品编码
     */
    private String gsmCode;

    /**
     * 商品组ID
     */
    private String gsyGroup;

    /**
     * 活动类型(0-主题活动，1-周期活动)
     */
    private String gsmType;

    /**
     * 活动开始时间
     */
    private String gsmStartd;

    /**
     * 活动结束时间
     */
    private String gsmEndd;

    /**
     * 活动名称
     */
    private String gsmThename;

    /**
     * 状态(0:推荐营销活动, 1:历史营销活动, 2:当前营销活动)
     */
    private String status;

    /**
     * 年份
     */
    private String year;
}
