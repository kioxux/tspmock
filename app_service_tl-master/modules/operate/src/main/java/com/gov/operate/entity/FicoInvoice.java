package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FICO_INVOICE")
@ApiModel(value="FicoInvoice对象", description="")
public class FicoInvoice extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款订单号")
    @TableField("FICO_ID")
    private String ficoId;

    @ApiModelProperty(value = "发票订单号")
    @TableField("FICO_ORDER_NO")
    private String ficoOrderNo;

    @ApiModelProperty(value = "发票行号")
    @TableField("FICO_INVOCE_LINE_NUM")
    private String ficoInvoceLineNum;

    @ApiModelProperty(value = "纳税主体编码")
    @TableField("FICO_TAX_SUBJECT_CODE")
    private String ficoTaxSubjectCode;

    @ApiModelProperty(value = "纳税主体名称")
    @TableField("FICO_TAX_SUBJECT_NAME")
    private String ficoTaxSubjectName;

    @ApiModelProperty(value = "发票类型")
    @TableField("FICO_INVOICE_TYPE")
    private String ficoInvoiceType;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("FICO_SOCIAL_CREDIT_CODE")
    private String ficoSocialCreditCode;

    @ApiModelProperty(value = "开户行名称")
    @TableField("FICO_BANK_NAME")
    private String ficoBankName;

    @ApiModelProperty(value = "开户行账号")
    @TableField("FICO_BANK_NUMBER")
    private String ficoBankNumber;

    @ApiModelProperty(value = "地址")
    @TableField("FICO_COMPANY_ADDRESS")
    private String ficoCompanyAddress;

    @ApiModelProperty(value = "电话号码")
    @TableField("FICO_COMPANY_PHONE_NUMBER")
    private String ficoCompanyPhoneNumber;

    @ApiModelProperty(value = "付款金额")
    @TableField("FICO_INVOICE_AMOUNT")
    private BigDecimal ficoInvoiceAmount;

    @ApiModelProperty(value = "发票回调成功PDF地址")
    @TableField("FICO_PDF_URL")
    private String ficoPdfUrl;

    @ApiModelProperty(value = "发票请求流水号")
    @TableField("FICO_FPQQLSH")
    private String ficoFpqqlsh;

    @ApiModelProperty(value = "发票号码")
    @TableField("FICO_FPHM")
    private String ficoFphm;

    @ApiModelProperty(value = "发票代码")
    @TableField("FICO_FPDM")
    private String ficoFpdm;

    @ApiModelProperty(value = "发票日期")
    @TableField("FICO_KPRQ")
    private String ficoKprq;

    @ApiModelProperty(value = "发票状态 0未开票 1开票中 2已开票 3开票失败")
    @TableField("FICO_INVOICE_STATUS")
    private String ficoInvoiceStatus;

    @ApiModelProperty(value = "申请开票返回结果")
    @TableField("FICO_INVOICE_RESULT")
    private String ficoInvoiceResult;

    @ApiModelProperty(value = "发标唯一值")
    @TableField("FICO_INDEX")
    private Long ficoIndex;

    @ApiModelProperty(value = "发票区分 1:电子发票，2:纸质发票")
    @TableField("FICO_TYPE")
    private Integer ficoType;

    @ApiModelProperty(value = "发票业务类型，1：门店服务费发票，2：短信费发票")
    @TableField("FICO_CLASS")
    private Integer ficoClass;

    @ApiModelProperty(value = "开票人")
    @TableField("FICO_OPERATOR")
    private String ficoOperator;
}
