package com.gov.operate.mapper;

import com.gov.operate.entity.PoItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 采购订单明细表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
public interface PoItemMapper extends BaseMapper<PoItem> {

}
