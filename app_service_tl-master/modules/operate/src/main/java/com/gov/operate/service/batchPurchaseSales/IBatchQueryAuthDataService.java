package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchQueryAuthData;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 查询数据清单 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchQueryAuthDataService extends SuperService<BatchQueryAuthData> {

}
