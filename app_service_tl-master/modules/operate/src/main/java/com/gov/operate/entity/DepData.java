package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DEP_DATA")
@ApiModel(value="DepData对象", description="")
public class DepData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "部门编码")
    @TableField("DEP_ID")
    private String depId;

    @ApiModelProperty(value = "部门名称")
    @TableField("DEP_NAME")
    private String depName;

    @ApiModelProperty(value = "部门负责人ID")
    @TableField("DEP_HEAD_ID")
    private String depHeadId;

    @ApiModelProperty(value = "部门负责人姓名")
    @TableField("DEP_HEAD_NAM")
    private String depHeadNam;

    @ApiModelProperty(value = "创建日期")
    @TableField("DEP_CRE_DATE")
    private String depCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("DEP_CRE_TIME")
    private String depCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("DEP_CRE_ID")
    private String depCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("DEP_MODI_DATE")
    private String depModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("DEP_MODI_TIME")
    private String depModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("DEP_MODI_ID")
    private String depModiId;

    @ApiModelProperty(value = "停用日期")
    @TableField("DEP_DIS_DATE")
    private String depDisDate;

    @ApiModelProperty(value = "连锁总部")
    @TableField("STO_CHAIN_HEAD")
    private String stoChainHead;

    @ApiModelProperty(value = "类型")
    @TableField("DEP_TYPE")
    private String depType;

    @ApiModelProperty(value = "部门代码")
    @TableField("DEP_CLS")
    private String depCls;

    @ApiModelProperty(value = "状态 '0停用 1启用'")
    @TableField("DEP_STATUS")
    private String depStatus;

}
