package com.gov.operate.dto;

import com.gov.operate.entity.SdIndrawerH;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.04.13
 */
@Data
public class SdIndrawerHVO extends SdIndrawerH {
    /**
     * 门店名
     */
    private String stoName;

    /**
     * 门店简称
     */
    private String stoShortName;

    /**
     * 门店名/门店简称
     */
    private String stoShowName;

    /**
     * 清斗人员
     */
    private String gsihEmpName;

    /**
     * 复核人员
     */
    private String gsihEmp1Name;
}

