package com.gov.operate.controller.storePayment;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.smsRecharge.ISmsRechargeRecordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@RestController
@RequestMapping("/smsRecharge")
public class SmsRechargeRecordController {

    @Resource
    private ISmsRechargeRecordService iSmsRechargeRecordService;

    @Log("短信充值记录")
    @ApiOperation(value = "短信充值记录")
    @PostMapping("getRechargeRecord")
    public Result getRechargeRecord(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                    @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                    @RequestJson(value = "gsrrRechargeTimeStart", required = false) String gsrrRechargeTimeStart,
                                    @RequestJson(value = "gsrrRechargeTimeEnd", required = false) String gsrrRechargeTimeEnd) {
        return iSmsRechargeRecordService.getRechargeRecord(pageNum, pageSize, gsrrRechargeTimeStart, gsrrRechargeTimeEnd);
    }

    @Log("短信可用数量")
    @ApiOperation(value = "短信可用数量")
    @PostMapping("getAvailableQuantity")
    public Result getAvailableQuantity() {
        return iSmsRechargeRecordService.getAvailableQuantity();
    }

    @Log("短信发送记录")
    @ApiOperation(value = "短信发送记录")
    @PostMapping("getSmsSendRecord")
    public Result getSmsSendRecord(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                   @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                   @RequestJson(value = "gssTel", required = false) String gssTel,
                                   @RequestJson(value = "gssSendTimeStart", required = false) String gssSendTimeStart,
                                   @RequestJson(value = "gssSendTimeEnd", required = false) String gssSendTimeEnd) {
        return iSmsRechargeRecordService.getSmsSendRecord(pageNum, pageSize, gssTel, gssSendTimeStart, gssSendTimeEnd);
    }

    @Log("开票短信查询")
    @ApiOperation(value = "开票短信查询")
    @PostMapping("getInvoiceSmsList")
    public Result getInvoiceSmsList(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                    @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                    @RequestJson(value = "gssSendTimeStart", name = "发送开始日期", required = false) String gssSendTimeStart,
                                    @RequestJson(value = "gssSendTimeEnd", name = "发送结束日期", required = false) String gssSendTimeEnd) {
        return iSmsRechargeRecordService.getInvoiceSmsList(pageNum, pageSize, gssSendTimeStart, gssSendTimeEnd);
    }

    @Log("开票信息查询")
    @ApiOperation(value = "开票信息查询")
    @GetMapping("getInvoiceInfo")
    public Result getInvoiceInfo() {
        return iSmsRechargeRecordService.getInvoiceInfo();
    }

    @Log("开票信息保存")
    @ApiOperation(value = "开票信息保存")
    @PostMapping("saveInvoiceInfo")
    public Result saveInvoiceInfo(@RequestJson(value = "ficoSocialCreditCode", name = "统一社会信用代码") String ficoSocialCreditCode,
                                  @RequestJson(value = "ficoTaxSubjectName", name = "纳税主体名称") String ficoTaxSubjectName,
                                  @RequestJson(value = "ficoBankName", name = "开户行名称") String ficoBankName,
                                  @RequestJson(value = "ficoBankNumber", name = "开户行账号") String ficoBankNumber) {
        return iSmsRechargeRecordService.saveInvoiceInfo(ficoSocialCreditCode, ficoTaxSubjectName, ficoBankName, ficoBankNumber);
    }

    @Log("短信开票申请")
    @ApiOperation(value = "短信开票申请")
    @PostMapping("sendInvoice")
    public Result sendInvoice(@RequestJson(value = "gssSendTimeStart", name = "发送开始日期", required = false) String gssSendTimeStart,
                              @RequestJson(value = "gssSendTimeEnd", name = "发送结束日期", required = false) String gssSendTimeEnd) {
        return iSmsRechargeRecordService.sendInvoice(gssSendTimeStart, gssSendTimeEnd);
    }
}

