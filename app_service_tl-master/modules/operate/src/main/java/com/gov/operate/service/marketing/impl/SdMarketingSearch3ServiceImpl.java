package com.gov.operate.service.marketing.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.GetMemberListDTO;
import com.gov.operate.dto.ProductGroupVo;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.Compadm;
import com.gov.operate.entity.SdMarketingSearch;
import com.gov.operate.entity.StoreData;
import com.gov.operate.entity.UserData;
import com.gov.operate.enums.AgeLevelEnum;
import com.gov.operate.enums.ConstellationEnum;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdMarketingSearchMapper;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ICompadmService;
import com.gov.operate.service.IStoreDataService;
import com.gov.operate.service.marketing.ISdMarketingSearch3Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-23
 */
@Slf4j
@Service
public class SdMarketingSearch3ServiceImpl extends ServiceImpl<SdMarketingSearchMapper, SdMarketingSearch> implements ISdMarketingSearch3Service {


    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingSearchMapper sdMarketingSearchMapper;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private ICompadmService compadmService;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SdMarketingSearchMapper marketingSearchMapper;
    @Resource
    private CosUtils cosUtils;

    public static Map<String, ConstellationEnum> constellationEnumMap = new ConcurrentHashMap<>();
    public static Map<String, AgeLevelEnum> ageLevelEnumMap = new ConcurrentHashMap<>();

    static {
        ageLevelEnumMap.put(AgeLevelEnum.AfterTen.getAgeLevelCode(), AgeLevelEnum.AfterTen);
        ageLevelEnumMap.put(AgeLevelEnum.AfterTwenty.getAgeLevelCode(), AgeLevelEnum.AfterTwenty);
        ageLevelEnumMap.put(AgeLevelEnum.AfterThirty.getAgeLevelCode(), AgeLevelEnum.AfterThirty);
        ageLevelEnumMap.put(AgeLevelEnum.AfterForty.getAgeLevelCode(), AgeLevelEnum.AfterForty);
        ageLevelEnumMap.put(AgeLevelEnum.AfterFifty.getAgeLevelCode(), AgeLevelEnum.AfterFifty);
        ageLevelEnumMap.put(AgeLevelEnum.AfterSixty.getAgeLevelCode(), AgeLevelEnum.AfterSixty);
        ageLevelEnumMap.put(AgeLevelEnum.AfterSeventy.getAgeLevelCode(), AgeLevelEnum.AfterSeventy);
        ageLevelEnumMap.put(AgeLevelEnum.AfterEighty.getAgeLevelCode(), AgeLevelEnum.AfterEighty);
        ageLevelEnumMap.put(AgeLevelEnum.AfterNinety.getAgeLevelCode(), AgeLevelEnum.AfterNinety);
        ageLevelEnumMap.put(AgeLevelEnum.AfterZero.getAgeLevelCode(), AgeLevelEnum.AfterZero);

        constellationEnumMap.put(ConstellationEnum.Aquarius.getConstellationCode(), ConstellationEnum.Aquarius);
        constellationEnumMap.put(ConstellationEnum.Pisces.getConstellationCode(), ConstellationEnum.Pisces);
        constellationEnumMap.put(ConstellationEnum.Aries.getConstellationCode(), ConstellationEnum.Aries);
        constellationEnumMap.put(ConstellationEnum.Taurus.getConstellationCode(), ConstellationEnum.Taurus);
        constellationEnumMap.put(ConstellationEnum.Gemini.getConstellationCode(), ConstellationEnum.Gemini);
        constellationEnumMap.put(ConstellationEnum.Cancer.getConstellationCode(), ConstellationEnum.Cancer);
        constellationEnumMap.put(ConstellationEnum.Leo.getConstellationCode(), ConstellationEnum.Leo);
        constellationEnumMap.put(ConstellationEnum.Virgo.getConstellationCode(), ConstellationEnum.Virgo);
        constellationEnumMap.put(ConstellationEnum.Libra.getConstellationCode(), ConstellationEnum.Libra);
        constellationEnumMap.put(ConstellationEnum.Scorpio.getConstellationCode(), ConstellationEnum.Scorpio);
        constellationEnumMap.put(ConstellationEnum.Sagittarius.getConstellationCode(), ConstellationEnum.Sagittarius);
        constellationEnumMap.put(ConstellationEnum.Capricorn.getConstellationCode(), ConstellationEnum.Capricorn);
    }

    /**
     * 精准查询03版
     */
    @Override
    public IPage<GetMemberListDTO> getMemberList(GetMemberListVO3 vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());

        // 根据消费类型取天数
        if (OperateEnum.ConsumeType.FIVE.getCode().equals(vo.getConsumeType())) {
            vo.setConsumeTypeDay(0);
        } else {
            for (OperateEnum.ConsumeType type : OperateEnum.ConsumeType.values()) {
                if (!type.getCode().equals(vo.getConsumeType())) {
                    continue;
                }
                String consumeTypeDay = type.getMessage();
                vo.setConsumeTypeDay(Integer.valueOf(consumeTypeDay));
            }
        }
        String ageLevelType = vo.getAgeLevelType();
        String constellationType = vo.getConstellationType();
        if (ageLevelType != null && ageLevelEnumMap.containsKey(ageLevelType)) {
            vo.setMemberBirthStartYear(ageLevelEnumMap.get(ageLevelType).getAgeLevelStartYear());
            vo.setMemberBirthEndYear(ageLevelEnumMap.get(ageLevelType).getAgeLevelEndYear());
        }
        if (constellationType != null && constellationEnumMap.containsKey(constellationType)) {
            vo.setMemberBirthStartDate(constellationEnumMap.get(constellationType).getConstellationStartDate());
            vo.setMemberBirthEndDate(constellationEnumMap.get(constellationType).getConstellationEndDate());
        }
        if (vo.getProSelfCode() != null && vo.getProSelfCode().trim().length() > 0) {
            List<String> proSelfCodeList = Arrays.stream(vo.getProSelfCode().split(",")).collect(Collectors.toList());
            vo.setProSelfCodeList(proSelfCodeList);
        }
        if (vo.getProName() != null && vo.getProName().trim().length() > 0) {
            List<String> proNameList = Arrays.stream(vo.getProName().split(",")).collect(Collectors.toList());
            vo.setProNameList(proNameList);
        }
        if (vo.getGsyGroup() != null && vo.getGsyGroup().trim().length() > 0) {
            List<String> gsyGroupList = Arrays.stream(vo.getGsyGroup().split(",")).collect(Collectors.toList());
            vo.setGsyGroupList(gsyGroupList);
        }
        if (vo.getProFactoryName() != null && vo.getProFactoryName().trim().length() > 0) {

            List<String> proFactoryList = Arrays.stream(vo.getProFactoryName().split(",")).collect(Collectors.toList());
            List<String> facCodeList = new ArrayList<>();
            proFactoryList.forEach(facCodeAndName -> {
                facCodeList.add(Arrays.stream(facCodeAndName.split("-")).collect(Collectors.toList()).get(0));
            });
            vo.setProFactoryNameList(facCodeList);
        }
        if (vo.getProClass() != null && vo.getProClass().trim().length() > 0) {
            List<String> proClassList = Arrays.stream(vo.getProClass().split(",")).collect(Collectors.toList());
            vo.setProClassList(proClassList);
        }
        if (vo.getProCompclass() != null && vo.getProCompclass().trim().length() > 0) {
            List<String> procompclassList = Arrays.stream(vo.getProCompclass().split(",")).collect(Collectors.toList());
            vo.setProCompclassList(procompclassList);
        }
        if (vo.getProCompclassName() != null && vo.getProCompclassName().trim().length() > 0) {
            List<String> proCompclassNameList = Arrays.stream(vo.getProCompclassName().split(",")).collect(Collectors.toList());
            vo.setProCompclassNameList(proCompclassNameList);
        }
        if (vo.getProBrandCode() != null && vo.getProBrandCode().trim().length() > 0) {
            List<String> brandList = Arrays.stream(vo.getProBrandCode().split(",")).collect(Collectors.toList());
            vo.setProBrandCodeList(brandList);
        }
        //如果会员属性对应的查询条件不为空，那么办卡日期等于公共日期，另外如果办卡日期标签对应的日期条件不为空那么就取办卡日期标签的日期
        //会员属性对应的查询条件有:
        //办卡日期gsmbcCreateDateStart gsmbcCreateDateEnd、所属门店storeList、会员卡别classId、会员性别sex、
        // 会员年龄minAge maxAge、会员生日 memberBirthStartDate memberBirthEndDate、会员积分minIntegral maxIntegral、会员星座constellationType、
        // 年龄阶段ageLevelType
        if (
                (vo.getGsmbcCreateDateEnd() != null && vo.getGsmbcCreateDateEnd().trim().length() > 0)
                        || (vo.getGsmbcCreateDateStart() != null && vo.getGsmbcCreateDateStart().trim().length() > 0)
        ) {
            vo.setStartDate(vo.getGsmbcCreateDateStart());
            vo.setEndDate(vo.getGsmbcCreateDateEnd());
        }
        if (

                (vo.getStoreList() != null && vo.getStoreList().size() > 0)
                        || (vo.getClassId() != null && vo.getClassId().size() > 0)
                        || (vo.getSex() != null && vo.getSex().trim().length() > 0)
                        || (vo.getMinAge() != null && vo.getMinAge() >= 0)
                        || (vo.getMaxAge() != null && vo.getMaxAge() >= 0)
                        || (vo.getMemberBirthStartDate() != null && vo.getMemberBirthStartDate().trim().length() > 0)
                        || (vo.getMemberBirthEndDate() != null && vo.getMemberBirthEndDate().trim().length() > 0)
                        || (vo.getMinIntegral() != null && vo.getMinIntegral() >= 0)
                        || (vo.getMaxIntegral() != null && vo.getMaxIntegral() >= 0)
                        || (vo.getConstellationType() != null && vo.getConstellationType().trim().length() > 0)
                        || (vo.getAgeLevelType() != null && vo.getAgeLevelType().trim().length() > 0)
        ) {
            vo.setGsmbcCreateDateStart(vo.getStartDate());
            vo.setGsmbcCreateDateEnd(vo.getEndDate());
        }
        //如果消费属性对应的查询条件不为空,那么会员最近消费日期等于公共日期，，另外如果最近消费标签对应的日期条件不为空那么就取最近消费标签对应的日期
        //消费属性对应的查询条件有: 消费渠道 consumptionPaymentCodeList、
        // 消费频率consumptionFrequencyDateStart consumptionFrequencyDateEnd consumptionTimesMin consumptionTimesMax、
        // 最近消费consumeType consumeTypeDay、consumeStartDate、consumeEndDate、
        // 消费金额consumptionAmtMin 、consumptionAmtMax 活跃度gsmtgId
        if (
                (vo.getConsumeStartDate() != null && vo.getConsumeStartDate().trim().length() > 0)
                        || (vo.getConsumeEndDate() != null && vo.getConsumeEndDate().trim().length() > 0)
        ) {

            vo.setStartDate(vo.getConsumeStartDate());
            vo.setEndDate(vo.getConsumeEndDate());
        }
        if (

                (vo.getConsumptionPaymentCodeList() != null && vo.getConsumptionPaymentCodeList().size() > 0)
                        || (vo.getConsumptionTimesMin() != null && vo.getConsumptionTimesMin() >= 0)
                        || (vo.getConsumptionTimesMax() != null && vo.getConsumptionTimesMax() >= 0)

                        || (vo.getConsumeTypeDay() != null && vo.getConsumeTypeDay() > 0)

                        || (vo.getConsumptionAmtMin() != null && vo.getConsumptionAmtMin() >= 0)
                        || (vo.getConsumptionAmtMax() != null && vo.getConsumptionAmtMax() >= 0)
                        || (vo.getGsmtgId() != null && vo.getGsmtgId().size() > 0)
        ) {
            vo.setConsumeStartDate(vo.getStartDate());
            vo.setConsumeEndDate(vo.getEndDate());
        }
        //如果商品属性以及疾病属性对应的查询条件不为空,那么商品消费日期等于公共日期
        //商品属性对应的查询条件有: 商品编码、商品名称、成分编码、成分名称、商品分类、商品组、生产厂家、品牌、疾病标签
        if (
                (vo.getProSelfCode() != null && vo.getProSelfCode().trim().length() > 0)
                        || (vo.getProName() != null && vo.getProName().trim().length() > 0)
                        || (vo.getProCompclass() != null && vo.getProCompclass().trim().length() > 0)
                        || (vo.getProCompclassName() != null && vo.getProCompclassName().trim().length() > 0)
                        || (vo.getProClass() != null && vo.getProClass().trim().length() > 0)
                        || (vo.getGsyGroup() != null && vo.getGsyGroup().trim().length() > 0)
                        || (vo.getProFactoryName() != null && vo.getProFactoryName().trim().length() > 0)
                        || (vo.getProBrandCode() != null && vo.getProBrandCode().trim().length() > 0)
                        || (vo.getDiseaseCodeList() != null && vo.getDiseaseCodeList().size() > 0)
        ) {
            vo.setProSaleStartDate(vo.getStartDate());
            vo.setProSaleEndDate(vo.getEndDate());

        }
        //5.剔除会员属性对应的查询条件有 eliminateType 已发短信 自定义会员 勿扰会员
        if (vo.getEliminateType() != null && vo.getEliminateType().trim().length() > 0) {
            vo.setEliminateStartDate(vo.getStartDate());
            vo.setEliminateEndDate(vo.getEndDate());
        }

        //4.疾病编码不为空
        if (StrUtil.isNotEmpty(vo.getDiseaseCode())) {
            vo.setDiseaseCodeList(Arrays.stream(vo.getDiseaseCode().split(",")).collect(Collectors.toList()));
            List<String> elementList = new ArrayList<>();
            vo.getDiseaseCodeList().forEach(disCode -> {
                List<String> elementListByCode = marketingSearchMapper.getProCompByDisaseCode(disCode.split("#").length > 1 ? disCode.split("#")[1] : disCode);
                elementList.addAll(elementListByCode);
            });
            vo.setProCompclassList(elementList);
        }
        if (StrUtil.isNotEmpty(vo.getDiseaseName())) {
            vo.setDiseaseNameList(Arrays.stream(vo.getDiseaseName().split(",")).collect(Collectors.toList()));
            List<String> elementList = new ArrayList<>();
            vo.getDiseaseNameList().forEach(disName -> {
                List<String> elementListByCode = marketingSearchMapper.getProCompByDisaseCode(disName.split("#").length > 1 ? disName.split("#")[1] : disName);
                elementList.addAll(elementListByCode);
            });
            vo.setProCompclassList(elementList);
        }

        if(StrUtil.isNotBlank(vo.getSendStatus())){
            vo.setSendStartDate(vo.getStartDate());
            vo.setSendEndDate(vo.getEndDate());
        }

        Page page = new Page<GetMemberListDTO>(vo.getPageNum(), vo.getPageSize());
        log.info("getMemberPage3:{}", JsonUtils.beanToJson(vo));
        return marketingSearchMapper.getMemberPage3(page, vo);
    }

    /**
     * 精准查询导出
     */
    @Override
    public Result exportMemberList(GetMemberListVO3 vo) throws IOException {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        //最终查询的List结果
//        List<GetMemberListDTO> getMemberListDTOList = new ArrayList<>();
//        marketingSearchMapper.seachDataList(vo, new ResultHandler<GetMemberListDTO>() {
//            @Override
//            public void handleResult(ResultContext<? extends GetMemberListDTO> resultContext) {
//                /**回调处理逻辑 */
//                getMemberListDTOList.add(resultContext.getResultObject());
//            }
//        });

        vo.setPageNum(-1);
        vo.setPageSize(-1);
        vo.setLimit(5001);
        List<GetMemberListDTO> getMemberListDTOList = this.getMemberList(vo).getRecords();
        if (getMemberListDTOList.size() > 5000) {
            throw new CustomResultException("一次只能导出5000条记录");
        }
        List<List<Object>> dataList = new ArrayList<>();
        for (GetMemberListDTO dto : getMemberListDTOList) {
            // 打印行
            List<Object> item = new ArrayList<>();
            dataList.add(item);
            //	会员卡号
            item.add(dto.getGmbCardId());
            //	会员姓名
            item.add(dto.getGmbName());
            //	性别
            if (StringUtils.isNotBlank(dto.getGmbSex())) {
                OperateEnum.Sex sex = EnumUtils.getEnumByCode(dto.getGmbSex(), OperateEnum.Sex.class);
                if (sex != null) {
                    item.add(sex.getMessage());
                } else {
                    item.add("");
                }
            } else {
                item.add("");
            }
            //	出生日期
            item.add(formatDate(dto.getGmbBirth()));
            //	电话号码
            item.add(dto.getGmbMobile());
            //	会员积分
            item.add(dto.getGmbIntegral());
            //	开卡日期
            item.add(formatDate(dto.getGsmbcCreateDate()));
            //	开卡门店
            item.add(dto.getGsmbcOpenCardName());
            // 最后消费日期
            item.add(formatDate(dto.getGsshDate()));
        }
        String content = ExcelUtils.exportCSV(Arrays.asList(headList), dataList);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bos.write(content.getBytes("GBK"));
            bos.flush();
            return cosUtils.uploadFile(bos, "精准查询".concat(".csv"));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (bos != null) {
                bos.close();
            }
        }
//
    }

    /**
     * 日期格式化
     */
    private String formatDate(String date) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, "/");
        sb.insert(7, "/");
        return sb.toString();
    }

    /**
     * 精准查询导出数据表头
     */
    private String[] headList = {
            "会员卡号",
            "会员姓名",
            "性别",
            "出生日期",
            "电话号码",
            "会员积分",
            "开卡日期",
            "开卡门店",
            "最后消费日期",
    };

    /**
     * 常用查询列表_菜单入口
     */
    @Override
    public List<SdMarketingSearch> getCommonSearchListFromMenu(GetCommonSearchListFromMenu vo) {
        TokenUser user = commonService.getLoginInfo();
        // 用户部门ID
        String depId = null;
        QueryWrapper<SdMarketingSearch> query = new QueryWrapper<SdMarketingSearch>()
                // 加盟商
                .eq("CLIENT", user.getClient())
                // 查询类型 1:短信，2：电话
                .eq("GSMS_TYPE", 1)
                // 是否常用  0，常用查询，1,非常用查询
                .eq("GSMS_NORMAL", "0")
                // 删除标记 0-否，1-是
                .eq("GSMS_DELETE_FLAG", "0")
                // 排序
                .orderByAsc("GSMS_STORE", "MARKETING_ID", "GSMS_NAME");
        UserData userData = userDataMapper.selectOne(new QueryWrapper<UserData>().select("DEP_ID", "DEP_NAME").eq("CLIENT", user.getClient()).eq("USER_ID", user.getUserId()));
        if (!ObjectUtils.isEmpty(userData)) {
            depId = userData.getDepId();
        }
        // 单体门店
        StoreData storeData = storeDataService.getOne(new QueryWrapper<StoreData>().select("STO_CODE", "STO_NAME").eq("CLIENT", user.getClient()).eq("STO_CODE", depId));
        if (!ObjectUtils.isEmpty(storeData)) {
            query.eq("GSMS_STORE", storeData.getStoCode());
            return this.list(query);
        }
        // 连锁门店
        Compadm compadm = compadmService.getOne(new QueryWrapper<Compadm>().eq("CLIENT", user.getClient()).eq("COMPADM_ID", depId));
        if (!ObjectUtils.isEmpty(compadm)) {
            query.inSql("GSMS_STORE", "SELECT STO_CODE FROM GAIA_STORE_DATA WHERE STO_CHAIN_HEAD = '" + compadm.getCompadmId() + "'");
            return this.list(query);
        }
        return this.list(query);
    }

    /**
     * 常用查询列表_营销任务[当前营销,历史营销]详情跳转
     */
    @Override
    public List<SdMarketingSearch> getCommonSearchListFromMarketing(GetCommonSearchListFromMarketing vo) {
        TokenUser user = commonService.getLoginInfo();
        List<SdMarketingSearch> searchList = this.list(new QueryWrapper<SdMarketingSearch>()
                // 加盟商
                .eq("CLIENT", user.getClient())
                // 门店编码
                .eq("GSMS_STORE", vo.getStoCode())
                // 查询类型 1:短信，2：电话
                .eq("GSMS_TYPE", vo.getGsmsType())
                // 是否常用  0，常用查询，1,非常用查询
                .eq("GSMS_NORMAL", OperateEnum.YesOrNo.ZERO.getCode())
                // 删除标记 0-否，1-是
                .eq("GSMS_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode())
                // 排序
                .orderByAsc("GSMS_STORE", "MARKETING_ID", "GSMS_NAME"));
        addExtend(vo.getGsmsId(), searchList);
        return searchList;
    }

    /**
     * 针对具体id 添加到常用查询列表
     */
    private void addExtend(String gsmsId, List<SdMarketingSearch> list) {
        // 针对id查询
        if (StringUtils.isNotEmpty(gsmsId)) {
            SdMarketingSearch search = this.getById(gsmsId);
            // 如果集合里面不包括该元素->添加
            if (!ObjectUtils.isEmpty(search)) {
                boolean anyMatch = list.stream().anyMatch(item -> item.getGsmsId().equals(search.getGsmsId()));
                if (!anyMatch) {
                    list.add(search);
                }
            }
        }
    }

    /**
     * 常用查询另存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCommonSearch(SaveCommonSearchVO3 searchVO) {
        TokenUser user = commonService.getLoginInfo();
        SdMarketingSearch searchDO = new SdMarketingSearch();
        BeanUtils.copyProperties(searchVO, searchDO, "gsmsStore");
        // 加盟商
        searchDO.setClient(user.getClient());
        // 查询ID
        searchDO.setGsmsId(UUIDUtil.getUUID());
        // 非删除
        searchDO.setGsmsDeleteFlag(OperateEnum.YesOrNo.ZERO.getCode());
        // 门店编码，当前用户的部门ID
        if (StringUtils.isNotEmpty(searchVO.getGsmsStore())) {
            searchDO.setGsmsStore(searchVO.getGsmsStore());
        } else {
            UserData userData = userDataMapper.selectOne(new QueryWrapper<UserData>()
                    .select("DEP_ID", "DEP_NAME").eq("CLIENT", user.getClient()).eq("USER_ID", user.getUserId()));
            if (!ObjectUtils.isEmpty(userData)) {
                searchDO.setGsmsStore(userData.getDepId());
            }
        }

        this.save(searchDO);
    }

    /**
     * 常用查询覆盖
     *
     * @param vo
     */
    @Override
    public void editCommonSearch(EditCommonSearchVO3 vo) {
        // 更新
        SdMarketingSearch searchExit = this.getById(vo.getGsmsId());
        if (ObjectUtils.isEmpty(searchExit)) {
            throw new CustomResultException(ResultEnum.E0021);
        }
        SdMarketingSearch searchDO = new SdMarketingSearch();
        BeanUtils.copyProperties(vo, searchDO);
        this.updateById(searchDO);
    }

    /**
     * 常用查询删除
     */
    @Override
    public void deleteCommonSearch(String gsmsId) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        QueryWrapper<SdMarketingSearch> del = new QueryWrapper<SdMarketingSearch>()
                // 加盟商
                .eq(StringUtils.isNotEmpty(user.getClient()), "CLIENT", user.getClient())
                // 查询ID
                .eq("GSMS_ID", gsmsId);
        this.remove(del);
    }

    @Override
    public List<Map<String, String>> getPaymentList() {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        return sdMarketingSearchMapper.getPaymentList(user.getClient());
    }

    @Override
    public IPage<DiseaseDetailVo> getDiseasePage(GetDiseasePageDto dto) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        if (dto.getQueryString() != null && dto.getQueryString().trim().length() > 0) {
            List<String> queryItem = Arrays.stream(dto.getQueryString().split(",")).collect(Collectors.toList());
            List<String> queryStringList = new ArrayList<>();
            queryItem.forEach(item -> queryStringList.add(item.split("#").length > 1 ? item.split("#")[1] : item));
            dto.setQueryStringList(queryStringList);
        }
        Page page = new Page<GetDiseasePageDto>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getDiseasePage(page, dto);
    }

    @Override
    public IPage<ProductDetailVO> getProductPage(GetProductPageDto dto) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        if (dto.getQueryString() != null && dto.getQueryString().trim().length() > 0) {
            dto.setQueryStringList(Arrays.stream(dto.getQueryString().split(",")).collect(Collectors.toList()));
        }
        Page page = new Page<GetProductPageDto>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getProductPage(page, dto);
    }

    @Override
    public IPage<ProComDetailVO> getProComPage(GetProComPageDto dto) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        if (dto.getQueryString() != null && dto.getQueryString().trim().length() > 0) {
            dto.setQueryStringList(Arrays.stream(dto.getQueryString().split(",")).collect(Collectors.toList()));
        }
        Page page = new Page<ProComDetailVO>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getProComPage(page, dto);
    }

    @Override
    public IPage<FactoryDetailVo> getFactoryPage(GetFactoryPageDto dto) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        if (dto.getQueryString() != null && dto.getQueryString().trim().length() > 0) {
            dto.setQueryStringList(Arrays.stream(dto.getQueryString().split(",")).collect(Collectors.toList()));
        }
        Page page = new Page<FactoryDetailVo>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getFactoryPage(page, dto);
    }

    @Override
    public IPage<BrandDetailVo> getBrandPage(GetBrandPageDto dto) {

        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        if (dto.getQueryString() != null && dto.getQueryString().trim().length() > 0) {
            dto.setQueryStringList(Arrays.stream(dto.getQueryString().split(",")).collect(Collectors.toList()));
        }
        Page page = new Page<BrandDetailVo>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getBrandPage(page, dto);
    }

    @Override
    public IPage<ProductClassDetailVo> getProductClassPage(GetProductClassPageDto dto) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        if (dto.getQueryString() != null && dto.getQueryString().trim().length() > 0) {
            dto.setQueryStringList(Arrays.stream(dto.getQueryString().split(",")).collect(Collectors.toList()));
        }
        Page page = new Page<ProductClassDetailVo>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getProductClassPage(page, dto);
    }

    @Override
    public IPage<ProductGroupVo> getProGroupPage(GetProGroupPageDto dto) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        Page page = new Page<ProductGroupVo>(dto.getPageNum(), dto.getPageSize());
        return sdMarketingSearchMapper.getProGroupPage(page, dto);
    }

    /**
     * 会员活跃
     */
    @Override
    public void dailyInsertMemberActive() {
        List<String> clientList=sdMarketingSearchMapper.getClients();
        clientList.forEach(client->{
            List<Map<String,Object>> listData=sdMarketingSearchMapper.getMemberActiveList(client);
            List<Map<String,Object>> oldData=sdMarketingSearchMapper.getMemberActiveData(client);
            List<Map<String,Object>> insertMemberActiveList=makeMemberActiveInsertParams(listData,oldData);
            List<Map<String,Object>> updateMemberActiveList=makeMemberActiveUpdateParams(listData,oldData);
            if (CollectionUtil.isNotEmpty(insertMemberActiveList)) {
                sdMarketingSearchMapper.dailyInertMemberActive(insertMemberActiveList);
            }
            if (CollectionUtil.isNotEmpty(updateMemberActiveList)) {
                sdMarketingSearchMapper.dailyUpdateMemberActive(updateMemberActiveList);
            }
        });
    }

    private List<Map<String, Object>> makeMemberActiveUpdateParams(List<Map<String, Object>> listData, List<Map<String, Object>> oldData) {
        List<String> memberId = oldData.stream().map(o -> o.get("memberId").toString()).collect(Collectors.toList());
        listData.removeIf(o->
                !memberId.contains(o.get("memberId").toString())
        );
        return listData;
    }

    private List<Map<String, Object>> makeMemberActiveInsertParams(List<Map<String, Object>> listData,List<Map<String,Object>> oldData) {
        List<String> memberId = oldData.stream().map(o -> o.get("memberId").toString()).collect(Collectors.toList());
        listData.removeIf(o->
            memberId.contains(o.get("memberId").toString())
        );
        return listData;
    }
}
