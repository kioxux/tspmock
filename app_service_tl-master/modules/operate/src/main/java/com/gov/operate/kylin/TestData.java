package com.gov.operate.kylin;

import lombok.Data;

@Data
public class TestData{
    String client;
    String saleDate;
    String avgBill;
    String totalAmt;
    String avgBillAmt;
}
