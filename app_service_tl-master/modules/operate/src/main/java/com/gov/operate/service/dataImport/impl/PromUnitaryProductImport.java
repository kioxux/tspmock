package com.gov.operate.service.dataImport.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.Dictionary;
import com.gov.common.entity.dataImport.MarketingComponent;
import com.gov.common.entity.dataImport.PromUnitaryProduct;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.PromUnitaryProductImportDto;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.ProductComponent;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ProductComponentMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IProductBusinessService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: xx
 * @date: 2021.2.08
 */
@Service
public class PromUnitaryProductImport extends BusinessImport {


    @Resource
    private IProductBusinessService productBusinessService;
    @Resource
    private CommonService commonService;

    // 导入
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = commonService.getLoginInfo();
        // 已选商品
        List<String> proSelfCodeList = null;
        if (field.get("proSelfCodeList") != null) {
            proSelfCodeList = (List<String>) field.get("proSelfCodeList");
        }
        if (proSelfCodeList == null) {
            proSelfCodeList = new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        //查询产品信息
        List<ProductBusiness> productBusinessList = productBusinessService.list(new LambdaQueryWrapper<ProductBusiness>()
                .select(ProductBusiness::getProSelfCode, ProductBusiness::getProCommonname, ProductBusiness::getProFactoryName
                        , ProductBusiness::getProName, ProductBusiness::getProSpecs)
                .eq(ProductBusiness::getClient, user.getClient())
                .groupBy(ProductBusiness::getProSelfCode));
        List<String> dataProSelfCodeList = productBusinessList.stream().map(ProductBusiness::getProSelfCode).collect(Collectors.toList());
        Map<String, ProductBusiness> proDataMap = productBusinessList.stream().collect(Collectors.toMap(ProductBusiness::getProSelfCode, s -> s, (key1, key2) -> key2));
        List<String> errorList = new ArrayList<>();
        List<String> resultList = new ArrayList<>();
        List<PromUnitaryProductImportDto> returnDtoList = new ArrayList<>();
        // 行数据处理
        for (Integer key : map.keySet()) {
            // 行数据
            PromUnitaryProduct promUnitaryProduct = (PromUnitaryProduct) map.get(key);
            if (proSelfCodeList.contains(promUnitaryProduct.getProSelfCode())) {
                errorList.add(MessageFormat.format("第{0}行：商品已存在", key + 1));
                continue;
            }
            if (resultList.contains(promUnitaryProduct.getProSelfCode())) {
                errorList.add(MessageFormat.format("第{0}行：商品已存在导入表格中", key + 1));
                continue;
            }
            // 商品存在验证
            if (dataProSelfCodeList.contains(promUnitaryProduct.getProSelfCode())) {
                resultList.add(promUnitaryProduct.getProSelfCode());
                PromUnitaryProductImportDto dto = new PromUnitaryProductImportDto();
                BeanUtils.copyProperties(promUnitaryProduct, dto);
                ProductBusiness productBusiness = proDataMap.get(promUnitaryProduct.getProSelfCode());
                dto.setProCommonname(productBusiness.getProCommonname());
                dto.setProSelfCode(productBusiness.getProSelfCode());
                dto.setProName(productBusiness.getProName());
                dto.setProSpecs(productBusiness.getProSpecs());
                dto.setProFactoryName(productBusiness.getProFactoryName());
                // 是否会员
                if (StringUtils.isNotBlank(dto.getGspusMemFlag())) {
                    Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.IS_OR_NO, dto.getGspusMemFlag());
                    if (dictionary != null) {
                        dto.setGspusMemFlag(dictionary.getValue());
                    } else {
                        dto.setGspusMemFlag("0");
                    }
                } else {
                    dto.setGspusMemFlag("0");
                }
                // 是否限价
                if (StringUtils.isNotBlank(dto.getGspusLimitFlag())) {
                    Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.IS_OR_NO, dto.getGspusLimitFlag());
                    if (dictionary != null) {
                        dto.setGspusLimitFlag(dictionary.getValue());
                    } else {
                        dto.setGspusLimitFlag("");
                    }
                }
                // 限价类型
                if (StringUtils.isNotBlank(dto.getGspusLimitType())) {
                    Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.GSPUS_LIMIT_TYPE, dto.getGspusLimitType());
                    if (dictionary != null) {
                        dto.setGspusLimitType(dictionary.getValue());
                    } else {
                        dto.setGspusLimitType("");
                    }
                }
                // 当《是否会员》为否，则《是否限价》、《限价类型》显示内容为空且置灰
                if ("0".equals(dto.getGspusMemFlag())) {
                    dto.setGspusLimitFlag("");
                    dto.setGspusLimitType("");
                }
                // 当《是否限价》为否，则《限价类型》显示内容为空且置灰
                if (!"1".equals(dto.getGspusLimitFlag())) {
                    dto.setGspusLimitType("");
                }
                dto.setGspusInteFlag("1");
                dto.setGspusInteRate("1");
                returnDtoList.add(dto);
            } else {
                errorList.add(MessageFormat.format("第{0}行：商品编码库中不存在", key + 1));
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(returnDtoList);
    }
}
