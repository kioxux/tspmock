package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class StoreSaleD {
    /**
     * 营业员编号
     */
    private String gssdSalerId;

    /**
     * 商品编码
     */
    private String gssdProId;

    /**
     * 销售日期
     */
    private String gssdDate;

    /**
     * 销售量
     */
    private BigDecimal gssdQty;

    /**
     * 应收价
     */
    private BigDecimal gssdPrc1;

    /**
     * 销售额 = 实收金额
     */
    private BigDecimal gssdAmt;

    /**
     * 成本价
     */
    private BigDecimal gssdMovPrice;

    /**
     * 成本额 = 销售数量*成本价
     */
    private BigDecimal gssdCostAmt;

    /**
     * 毛利额 = 实收金额-成本额
     */
    private BigDecimal gssdGrossAmt;


}
