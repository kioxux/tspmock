package com.gov.operate.dto.weChat;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class AppidAndSecrectVO {

    @NotBlank(message = "对象编码不能为空")
    private String goaCode;
    @NotNull(message = "类型不能为空，1:门店，2：连锁")
    private Integer goaType;
    @NotBlank(message = "APPID不能为空")
    private String accountsAppid;
    @NotBlank(message = "密钥不能为空")
    private String accountsAppsecret;

    @NotBlank(message = "加盟商不能为空")
    private String client;
    // 微信推文授权
    private String wechatTweets;

    @NotBlank(message = "原始ID不能为空")
    private String originalId;
}
