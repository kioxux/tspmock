package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.EditAppVO;
import com.gov.operate.dto.GetAppListVO;
import com.gov.operate.dto.SaveAppVO;
import com.gov.operate.service.IAppVersionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
@RestController
@RequestMapping("app")
public class AppVersionController {

    @Resource
    private IAppVersionService appVersionService;


    @Log("APP版本列表")
    @ApiOperation(value = "APP版本列表")
    @PostMapping("getAppList")
    public Result getAppList(@Valid @RequestBody GetAppListVO vo) {
        return ResultUtil.success(appVersionService.getAppList(vo));
    }

    @Log("APP版本新增")
    @ApiOperation(value = "APP版本新增")
    @PostMapping("saveApp")
    public Result saveApp(@Valid @RequestBody SaveAppVO vo) {
        appVersionService.saveApp(vo);
        return ResultUtil.success();
    }

    @Log("APP版本编辑")
    @ApiOperation(value = "APP版本编辑")
    @PostMapping("editApp")
    public Result editApp(@Valid @RequestBody EditAppVO vo) {
        appVersionService.editApp(vo);
        return ResultUtil.success();
    }

    @Log("APP版本删除")
    @ApiOperation(value = "APP版本删除")
    @GetMapping("removeApp")
    public Result removeApp(@RequestParam("id") String id) {
        appVersionService.removeApp(id);
        return ResultUtil.success();
    }

    @Log("APP版本发布")
    @ApiOperation(value = "APP版本发布")
    @GetMapping("releaseApp")
    public Result releaseApp(@RequestParam("id") String id) {
        appVersionService.releaseApp(id);
        return ResultUtil.success();
    }

    @Log("APP版本详情")
    @ApiOperation(value = "APP版本详情")
    @GetMapping("getApp")
    public Result getApp(@RequestParam("id") String id) {
        return ResultUtil.success(appVersionService.getApp(id));
    }

}

