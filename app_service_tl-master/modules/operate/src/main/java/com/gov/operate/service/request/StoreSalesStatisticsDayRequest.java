package com.gov.operate.service.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StoreSalesStatisticsDayRequest extends StoreSalesStatisticsRequest implements Serializable {

    private static final long serialVersionUID = 15395082534111841L;

    private List<String> allDays;
}
