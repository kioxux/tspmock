package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMarketingProm;
import com.gov.operate.mapper.SdMarketingPromMapper;
import com.gov.operate.service.ISdMarketingPromService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Service
public class SdMarketingPromServiceImpl extends ServiceImpl<SdMarketingPromMapper, SdMarketingProm> implements ISdMarketingPromService {

}
