package com.gov.operate.dto.storeData;

import lombok.Data;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:23 2021/11/22
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class SaveStoreDataDto extends StoreDataAddVO{
    private String stoCreDate;

    /**
     * 创建时间 HHmmss
     */
    private String stoCreTime;

    /**
     * 创建人账号
     */
    private String stoCreId;

    /**
     * 关联门店
     */
    private String stoRelationStore;
}
