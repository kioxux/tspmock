package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_MESSAGE_TEMPLATE")
@ApiModel(value="UserMessageTemplate对象", description="")
public class UserMessageTemplate extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("GUMT_ID")
    private String gumtId;

    @ApiModelProperty(value = "消息名称")
    @TableField("GUMT_NAME")
    private String gumtName;

    @ApiModelProperty(value = "消息标题")
    @TableField("GUMT_TITLE")
    private String gumtTitle;

    @ApiModelProperty(value = "消息内容")
    @TableField("GUMT_CONTENT")
    private String gumtContent;

    @ApiModelProperty(value = "业务类型")
    @TableField("GUMT_BUSINESS_TYPE")
    private Integer gumtBusinessType;

    @ApiModelProperty(value = "是否跳转")
    @TableField("GUMT_GO_PAGE")
    private Integer gumtGoPage;

    @ApiModelProperty(value = "消息类型")
    @TableField("GUMT_TYPE")
    private Integer gumtType;

    @ApiModelProperty(value = "表示形式")
    @TableField("GUMT_SHOW_TYPE")
    private Integer gumtShowType;

    @ApiModelProperty(value = "是否启用")
    @TableField("GUMT_FLAG")
    private Integer gumtFlag;


}
