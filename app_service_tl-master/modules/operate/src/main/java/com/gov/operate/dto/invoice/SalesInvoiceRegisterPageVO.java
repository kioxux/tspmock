package com.gov.operate.dto.invoice;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SalesInvoiceRegisterPageVO {

    private String client;

    /**
     * 业务机构
     */
    private String gsiSite;

    /**
     * 机构名称
     */
    private String siteName;

    /**
     * 日期
     */
    private String gsiCreateDate;

    /**
     * 开票单号
     */
    private String gsiOrderId;

    /**
     * 客户
     */
    private String gsiCustomer;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 开票金额
     */
    private BigDecimal gsiAmount;

    /**
     * 回款金额
     */
    private BigDecimal collectionAmount;

    /**
     * excel地址
     */
    private String gsiExcelPath;

    private String userId;

    private String userNam;

    private String gsiBill;

    private Integer gsiBillStatus;

    private Integer gsiBillType;

    private String gsiInvoicePdf;

    private String gsiBillStatusReason;

    private String gsiDnId;
}
