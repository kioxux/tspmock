package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-09-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_CHECK_KUCUN_IMPORT")
@ApiModel(value="ProductCheckKucunImport对象", description="")
public class ProductCheckKucunImport extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店code")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "门店name")
    @TableField("STO_NAME")
    private String stoName;

    @ApiModelProperty(value = "商品code")
    @TableField("PRO_CODE")
    private String proCode;

    @ApiModelProperty(value = "国际条形码1")
    @TableField("PRO_BARCODE")
    private String proBarcode;

    @ApiModelProperty(value = "商品名称")
    @TableField("PRO_NAME")
    private String proName;

    @ApiModelProperty(value = "商品库存数量")
    @TableField("KU_CUN_NUM")
    private String kuCunNum;


}
