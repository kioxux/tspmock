package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@RestController
@RequestMapping("/operate/invoice-bill-document")
public class InvoiceBillDocumentController {

}

