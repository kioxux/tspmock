package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddProgramVO {

//    @NotNull(message = "系数类型不能为空")
    private Integer gspTypeSalary;

//    @NotNull(message = "基本工资不能为空")
    private BigDecimal gspBasicSalary;

//    @NotNull(message = "岗位工资不能为空")
    private BigDecimal gspPostSalary;

//    @NotNull(message = "绩效工资 不能为空")
    private BigDecimal gspPerformanceSalary;

//    @NotNull(message = "考评工资不能为空")
    private BigDecimal gspEvaluationSalary;



    @NotBlank(message = "方案名称不能为空")
    private String gspName;

    @NotBlank(message = "生效日期不能为空")
    private String gspEffectiveDate;

    @NotBlank(message = "目的不能为空")
    private String gspPurpose;

//    @NotBlank(message = "方案说明")
    private String gspDescription;

    @NotEmpty(message = "使用方式使用范围不能为空")
    private List<SalaryObjDTO> applyObjList;

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class SalaryObjDTO {
//        @NotBlank(message = "方案主键不能为空")
        private String gsoGspId;
//        @NotBlank(message = "适用范围不能为空")
        private String gsoApplyObj;
//        @NotBlank(message = "适用范围名称不能为空")
        private String gsoApplyObjName;
    }
}
