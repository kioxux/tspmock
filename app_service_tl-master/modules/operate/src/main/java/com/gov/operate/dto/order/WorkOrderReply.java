package com.gov.operate.dto.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WorkOrderReply extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "工单主键")
    private Long gwoId;

    @ApiModelProperty(value = "发布主体")
    private Integer gworOrg;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime gworSendTime;

    @ApiModelProperty(value = "客户ID")
    private String gworClient;

    @ApiModelProperty(value = "客户名")
    private String gworClientName;

    @ApiModelProperty(value = "用户ID")
    private String gworUserId;

    @ApiModelProperty(value = "用户名")
    private String gworUserName;

    @ApiModelProperty(value = "处理人ID")
    private Long gworReplyUserId;

    @ApiModelProperty(value = "发布内容")
    private String gworSendContent;


}
