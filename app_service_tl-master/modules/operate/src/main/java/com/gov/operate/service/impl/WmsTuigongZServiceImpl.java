package com.gov.operate.service.impl;

import com.gov.operate.entity.WmsTuigongZ;
import com.gov.operate.mapper.WmsTuigongZMapper;
import com.gov.operate.service.IWmsTuigongZService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class WmsTuigongZServiceImpl extends ServiceImpl<WmsTuigongZMapper, WmsTuigongZ> implements IWmsTuigongZService {

}
