package com.gov.operate.dto.invoice;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.operate.entity.SalesCreateInfo;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.27
 */
@Data
public class AutoInvoice {

    /**
     * 开票明细
     */
    private List<SalesInvoicePageVO> list;

    /**
     * 发票种类
     */
    private String gsiInvoiceClass;

    /**
     * 是否推送 1:是，0:否
     */
    private Integer gsiInvoicePush;

    /**
     * Y：是 N：否
     */
    private String gsiBillPush;

    /**
     * 备注
     */
    private String gsiInvoiceNote;

    /**
     * 开票人电话
     */
    private String gsiInvoicePhone;

    /**
     * 开票人邮箱
     */
    private String gsiInvoiceEmail;

    /**
     * 开票人ID
     */
    private String gsiInvoiceUserId;

    /**
     * 开票人姓名
     */
    private String gsiInvoiceUserName;

    /**
     * 收款人
     */
    private String gsiReceiverUser;

    /**
     * 收款人
     */
    private String gsiReceiverUserName;

    /**
     * 审核人
     */
    private String gsiCheckUser;

    /**
     * 审核人
     */
    private String gsiCheckUserName;
}
