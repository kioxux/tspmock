package com.gov.operate.service.invoiceOptimiz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.dto.invoiceOptimiz.PaymentBill;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.23
 */
public interface PaymentOptimizService {

    /**
     * 对账单集合
     * @param vo
     * @return
     */
    IPage<PaymentBill> selectBillList(SelectWarehousingVO vo);
}
