package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_CLASS")
@ApiModel(value="SdMemberClass对象", description="")
public class SdMemberClass extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "卡类型编号")
    @TableField("GSMC_ID")
    private String gsmcId;

    @ApiModelProperty(value = "卡类型名称")
    @TableField("GSMC_NAME")
    private String gsmcName;

    @ApiModelProperty(value = "卡类型折率")
    @TableField("GSMC_DISCOUNT_RATE")
    private String gsmcDiscountRate;

    @ApiModelProperty(value = "消费金额")
    @TableField("GSMC_AMT_SALE")
    private String gsmcAmtSale;

    @ApiModelProperty(value = "积分数值")
    @TableField("GSMC_INTEGRAL_SALE")
    private String gsmcIntegralSale;


}
