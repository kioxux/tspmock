package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.service.ISdMarketingSearchService;
import com.gov.operate.service.ISdMemberBasicService;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-22
 */
@RestController
@RequestMapping("gsm")
public class SdMemberBasicController {

    @Resource
    private ISdMemberBasicService sdMemberBasicService;
    @Resource
    private ISdMarketingSearchService sdMarketingSearchService;

    @Log("精准查询")
    @ApiOperation(value = "精准查询")
    @PostMapping("getMemberList")
    public Result getMemberList(@Valid @RequestBody GetMemberListVO vo) {
        return ResultUtil.success(sdMemberBasicService.getMemberList(vo));
    }

    @SneakyThrows
    @Log("精准查询导出")
    @ApiOperation(value = "精准查询导出")
    @PostMapping("exportMemberList")
    public Result exportMemberList(@Valid @RequestBody GetMemberListVO vo) {
        return sdMemberBasicService.exportMemberList(vo);
    }

    @Log("常用查询列表")
    @ApiOperation(value = "常用查询列表")
    @PostMapping("getCommonSearchList")
    public Result getCommonSearchList(@RequestBody @Valid GetCommonSearchListVO vo) {
        return ResultUtil.success(sdMarketingSearchService.getCommonSearchList(vo));
    }

    @Log("常用查询保存")
    @ApiOperation(value = "常用查询保存")
    @PostMapping("saveCommonSearch")
    public Result saveCommonSearch(@Valid @RequestBody SaveCommonSearchVO searchVO) {
        sdMarketingSearchService.saveCommonSearch(searchVO);
        return ResultUtil.success();
    }

    @Log("常用查询覆盖")
    @ApiOperation(value = "常用查询覆盖")
    @PostMapping("editCommonSearch")
    public Result editCommonSearch(@Valid @RequestBody EditCommonSearchVO vo) {
        sdMarketingSearchService.editCommonSearch(vo);
        return ResultUtil.success();
    }

    @Log("常用查询删除")
    @ApiOperation(value = "常用查询删除")
    @GetMapping("deleteCommonSearch")
    public Result deleteCommonSearch(@RequestParam("gsmsId") String gsmsId) {
        sdMarketingSearchService.deleteCommonSearch(gsmsId);
        return ResultUtil.success();
    }

    @Log("电子券主题列表")
    @ApiOperation(value = "电子券主题列表")
    @PostMapping("getElectronThemeList")
    public Result getElectronThemeList(@RequestBody ElectronThemeDTO dto) {
        return sdMarketingSearchService.getElectronThemeList(dto);
    }

    @Log("电子券异动保存")
    @ApiOperation(value = "电子券异动保存")
    @PostMapping("saveElectronChange")
    public Result saveElectronChange(@RequestBody SdElectronChangeDTO dto) {
        return sdMarketingSearchService.saveElectronChange(dto);
    }

}

