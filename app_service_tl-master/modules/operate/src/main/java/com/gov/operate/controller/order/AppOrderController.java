package com.gov.operate.controller.order;

import com.gov.common.basic.JsonUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.PageInput;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.rsa.AesEncodeUtils;
import com.gov.operate.dto.customer.order.RemoveCustomerOrderVO;
import com.gov.operate.dto.order.WorkOrder;
import com.gov.operate.dto.order.WorkOrderDto;
import com.gov.operate.dto.order.WorkOrderReplyDto;
import com.gov.operate.dto.order.WorkOrderVo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.07.05
 */
@Slf4j
@RestController
@RequestMapping("appOrder")
public class AppOrderController {

    @Resource
    private ApplicationConfig applicationConfig;

    private String getFullUrl(String relativePath) {
        return applicationConfig.getOrderServiceUrl() + relativePath;
    }

    private <T> Result postRequestOrderService(String url, T vo, String des) {
        Map<String, Object> param = new HashMap<>();
        if (vo != null) {
            param.put("text", AesEncodeUtils.encrypt(JsonUtils.beanToJson(vo)));
        }
        log.info("调用[{}]开始,请求地址[{}],参数:{}", des, url, param);
        Mono<String> response = WebClient.create()
                .post()
                .uri(url)
                .body(BodyInserters.fromObject(param))
                .retrieve()
                .bodyToMono(String.class);
        String result = response.block();
        log.info("调用[{}]结束,返回值:{}", des, result);
        return JsonUtils.jsonToBean(result, Result.class);
    }


    @Log("工单列表")
    @ApiOperation(value = "工单列表")
    @PostMapping("appPage")
    public Result appPage(@RequestBody PageInput<WorkOrderVo> pageInput) {
        String relativePath = "order/appOrder/appPage";
        return postRequestOrderService(getFullUrl(relativePath), pageInput, "工单列表");
    }

    @Log("工单回复附件")
    @ApiOperation(value = "工单回复附件")
    @GetMapping("getOrderReplyAtt")
    public Result getOrderReplyAtt(@RequestParam("gwoId") Long gwoId) {
        String relativePath = "order/appOrder/getOrderReplyAtt";
        return postRequestOrderService(getFullUrl(relativePath), gwoId, "工单回复附件");
    }

    @Log("工单保存")
    @ApiOperation(value = "工单保存")
    @PostMapping("save")
    public Result save(@RequestBody WorkOrderDto param) {
        String relativePath = "order/appOrder/save";
        return postRequestOrderService(getFullUrl(relativePath), param, "工单保存");
    }

    @Log("工单评分")
    @ApiOperation(value = "工单评分")
    @PostMapping("score")
    public Result score(@RequestBody WorkOrder param) {
        String relativePath = "order/appOrder/score";
        return postRequestOrderService(getFullUrl(relativePath), param, "工单评分");
    }

    @Log("工单详情")
    @ApiOperation(value = "工单详情")
    @GetMapping("info")
    public Result info(Long workOrderReplyId, @NotNull Long id, @NotNull Integer pageSize) {
        String relativePath = "order/appOrder/info";
        Map<String, Object> map = new HashMap<>();
        map.put("workOrderReplyId", workOrderReplyId);
        map.put("id", id);
        map.put("pageSize", pageSize);
        return postRequestOrderService(getFullUrl(relativePath), map, "工单详情");
    }

    @Log("工单回复")
    @ApiOperation(value = "工单回复")
    @PostMapping("send")
    public Result send(@RequestBody WorkOrderReplyDto param) {
        String relativePath = "order/appOrder/send";
        return postRequestOrderService(getFullUrl(relativePath), param, "工单回复");
    }

    @Log("工单关闭")
    @ApiOperation(value = "工单关闭")
    @GetMapping("close")
    public Result close(Long id) {
        String relativePath = "order/appOrder/close";
        return postRequestOrderService(getFullUrl(relativePath), id, "工单关闭");
    }

    @Log("工单类型")
    @ApiOperation(value = "工单类型")
    @PostMapping("getWorkOrderTypes")
    public Result getWorkOrderTypes() {
        String relativePath = "order/appOrder/getWorkOrderTypes";
        return postRequestOrderService(getFullUrl(relativePath), null, "工单类型");
    }

}
