package com.gov.operate.mapper;

import com.gov.operate.dto.member.MemberInfoDTO;
import com.gov.operate.dto.member.MemberInfoDetailsDTO;
import com.gov.operate.dto.member.OrderInfoDTO;
import com.gov.operate.dto.member.SaleInfoDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberManagementMapper {

    // 根据手机号模糊查询会员列表
    List<MemberInfoDTO> queryMemberListByMobile(@Param("client") String client, @Param("mobile") String mobile);

    // 根据会员id查询会员明细
    MemberInfoDetailsDTO queryMemberInfoById(@Param("client") String client, @Param("gsmbMemberId") String gsmbMemberId);

    // 获取最后消费的信息
    SaleInfoDTO querySaleInfoByHykNo(@Param("client") String client, @Param("gsmbcCardId") String gsmbcCardId);

    // 根据会员id获取对应卡号
    String queryIdByHykNo(@Param("client") String client, @Param("gsmbMemberId") String gsmbMemberId);

    // 根据会员卡号获取消费订单列表
    List<OrderInfoDTO> queryOrderInfoById(@Param("client") String client, @Param("hykNo") String hykNo, @Param("startDate") String startDate, @Param("endDate") String endDate);

}
