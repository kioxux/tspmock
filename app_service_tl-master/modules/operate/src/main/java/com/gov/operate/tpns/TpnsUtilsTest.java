//package com.gov.operate.tpns;
//
//import com.gov.common.entity.ApplicationConfig;
//import com.tencent.xinge.XingeApp;
//import com.tencent.xinge.bean.*;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created with IntelliJ IDEA.
// * Description:
// *
// * @author: libb
// * @date: 2020.11.06
// */
//@Slf4j
//@Component
//public class TpnsUtilsTest {
//
//    //將10進制轉換為16進制
//    public static String encodeHEX(long numb) {
//        System.currentTimeMillis();
//        String hex = Long.toHexString(numb);
//        return hex;
//
//    }
//
//    public static void main(String[] arge) {
////        String hex = encodeHEX(System.currentTimeMillis());
////        System.out.println("  1234567896342  的16进制为" + hex);
//
//        ApplicationConfig applicationConfig = new ApplicationConfig();
//        applicationConfig.setTpnsDoMain("https://api.tpns.sh.tencent.com/");
//
////        applicationConfig.setTpnsAndroidAppId("1580001964");
////        applicationConfig.setTpnsAndroidSecretKey("3b7a2039568fb6fe41dabc092073a023");
////        XingeApp xingeApp = XinGePushUtil.xingeApp(applicationConfig.getTpnsAndroidAppId(), applicationConfig.getTpnsAndroidSecretKey(), applicationConfig.getTpnsDoMain());
//
//        applicationConfig.setTpnsIosAppId("1680001965");
//        applicationConfig.setTpnsIosSecretKey("4d34da2f955c26f137a0fcb3aa53f740");
//        XingeApp xingeApp = TpnsUtils.xingeApp(applicationConfig.getTpnsIosAppId(), applicationConfig.getTpnsIosSecretKey(), applicationConfig.getTpnsDoMain());
//
//        PushContent pushContent = new PushContent();
//
//        pushContent.setTitle("1q1");
//        pushContent.setContent("2w2");
////         pushContent.setPlatform(Platform.android);
//        pushContent.setPlatform(Platform.ios);
//        Map<String, Object> map = new HashMap<>();
//        map.put("adfsafds", "adfadf");
//        pushContent.setAttachKeys(map);
//
//        pushContent.setEnvironment(Environment.dev);
//
//        pushContent.setAccountList(new ArrayList<String>() {{
////            add("10000001-13451540174");
//            add("21020007-13732641664");
//        }});
//
//        TpnsUtils.send(pushContent, xingeApp);
//
//    }
//
//}
