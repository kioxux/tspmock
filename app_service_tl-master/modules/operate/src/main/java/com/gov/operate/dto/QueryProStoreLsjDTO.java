package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class QueryProStoreLsjDTO {

    private String stoCode;

    private String stoName;

    private Double proLsj;
}
