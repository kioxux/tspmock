package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.bsaoc.BsaoCPay;
import com.gov.operate.dto.AddRechargeCardPayCheckVO;
import com.gov.operate.dto.AddRechargeSetVO;
import com.gov.operate.dto.RechargeCardLVO;
import com.gov.operate.dto.RechargeCardListVO;
import com.gov.operate.dto.recharge.RechargeRefundVO;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Service
@Slf4j
public class SdRechargeCardPaycheckServiceImpl extends ServiceImpl<SdRechargeCardPaycheckMapper, SdRechargeCardPaycheck> implements ISdRechargeCardPaycheckService {
    @Resource
    private CommonService commonService;
    @Resource
    private SdRechargeCardPaycheckMapper sdRechargeCardPaycheckMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private SdRechargeCardMapper sdRechargeCardMapper;
    @Resource
    private ISdRechargeCardSetService sdRechargeCardSetService;
    @Resource
    private SdPaymentMethodMapper sdPaymentMethodMapper;
    @Resource
    private SdSalePayMsgMapper sdSalePayMsgMapper;
    @Resource
    private ISdRechargeCardService sdRechargeCardService;
    @Resource
    private BsaoCPay bsaoCPay;
    @Resource
    private SdRechargeChangeMapper rechargeChangeMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private ISdRechargeCardChangeService sdRechargeCardChangeService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer addPayCheck(AddRechargeCardPayCheckVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }

        //验证当前用户的门店编码是连锁还是非连锁
        StoreData storeData = storeDataMapper.selectOne(new QueryWrapper<StoreData>()
                .eq("CLIENT", user.getClient())
                .eq("STO_CODE", user.getDepId()));
        if (null == storeData) {
            throw new CustomResultException("请选择门店");
        }

        //判断储值卡信息
        SdRechargeCard card = sdRechargeCardMapper.selectOne(new QueryWrapper<SdRechargeCard>()
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (null == card) {
            throw new CustomResultException("未查询到储值卡信息");
        }

        if ("3".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已作废");
        }

        //判断充值比例是否正确
        if (1 != vo.getType()) {
            List<AddRechargeSetVO> proportionList = sdRechargeCardSetService.getProportionData();
            if (proportionList.isEmpty()) {
                throw new CustomResultException("充值比例错误");
            } else {
                proportionList.forEach(item -> {
                    if (vo.getType() == item.getType()) {
                        if (!item.getProportion().equals(vo.getProportion())) {
                            throw new CustomResultException("充值比例错误");
                        }
                    }
                });
            }
        }

        //判断充值金额
//        if (vo.getGsrcpYsAmt().multiply(
//                new BigDecimal(vo.getProportion()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP
//        ).compareTo(vo.getGsrcpAfterAmt().setScale(2, BigDecimal.ROUND_HALF_UP)) != 0) {
//            throw new CustomResultException("充值金额错误");
//        }

        //判断支付方式
        SdPaymentMethod sdPaymentMethod = sdPaymentMethodMapper.selectOne(new QueryWrapper<SdPaymentMethod>()
                .eq("CLIENT", user.getClient())
                .eq("GSPM_BR_ID", vo.getStoCode())
                .eq("GSPM_RECHARGE", 1)
                .eq("GSPM_SERIAL", vo.getGspmSerial())
                .eq("GSPM_ID", vo.getGspmId()));
        if (null == sdPaymentMethod) {
            throw new CustomResultException("支付方式不正确");
        }

        if ("1".equals(sdPaymentMethod.getGspmFalg())) {
            //是接口的 必须要条形码
            if (StringUtils.isEmpty(vo.getPayCode())) {
                throw new CustomResultException("请先扫描付款码");
            }
        }

        //更新表储值卡信息表增加余额
        BigDecimal gsrcAmt = card.getGsrcAmt();//初始金额
        BigDecimal gsrcpAfterAmt = vo.getGsrcpAfterAmt();//充值金额
        card.setGsrcUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcUpdateEmp(user.getUserId());
        card.setGsrcUpdateBrId(vo.getStoCode());
        card.setGsrcAmt(card.getGsrcAmt().add(vo.getGsrcpAfterAmt()));
        sdRechargeCardMapper.update(card, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ID", card.getGsrcId())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId()));

        //新增储值卡充值表
        String payCode = sdRechargeCardPaycheckMapper.getUserCardNumber(user.getClient()).getGsrcpVoucherId();
        SdRechargeCardPaycheck paycheck = new SdRechargeCardPaycheck();
        paycheck.setClient(user.getClient());
        paycheck.setGsrcpBrId(vo.getStoCode());
        paycheck.setGsrcpVoucherId(payCode);
        paycheck.setGsrcpAccountId(vo.getGsrcAccountId());
        paycheck.setGsrcpCardId(vo.getGsrcId());
        paycheck.setGsrcpDate(DateUtils.getCurrentDateStrYYMMDD());
        paycheck.setGsrcpTime(DateUtils.getCurrentTimeStrHHMMSS());
        paycheck.setGsrcpEmp(user.getUserId());
        paycheck.setGsrcpAfterAmt(vo.getGsrcpAfterAmt());
        paycheck.setGsrcpYsAmt(vo.getGsrcpYsAmt());
        sdRechargeCardPaycheckMapper.insert(paycheck);

        //新增销售支付信息表
        SdSalePayMsg sdSalePayMsg = new SdSalePayMsg();
        sdSalePayMsg.setClient(user.getClient());
        sdSalePayMsg.setGsspmBrId(vo.getStoCode());
        sdSalePayMsg.setGsspmDate(DateUtils.getCurrentDateStrYYMMDD());
        sdSalePayMsg.setGsspmBillNo(payCode);
        sdSalePayMsg.setGsspmId(sdPaymentMethod.getGspmId());
        sdSalePayMsg.setGsspmType("2");
        sdSalePayMsg.setGsspmName(sdPaymentMethod.getGspmName());
        sdSalePayMsg.setGsspmCardNo(paycheck.getGsrcpCardId());
        sdSalePayMsg.setGsspmAmt(vo.getGsrcpYsAmt());
        if ("1001".equals(sdPaymentMethod.getGspmId())) {
            sdSalePayMsg.setGsspmRmbAmt(vo.getGsrcpYsAmt());
            sdSalePayMsg.setGsspmZlAmt(BigDecimal.ZERO);
        }
        sdSalePayMsgMapper.insert(sdSalePayMsg);

        //生成一条充值卡金额异动记录
        SdRechargeChange gaiaSdRechargeChange = new SdRechargeChange();
        //加盟商
        gaiaSdRechargeChange.setClient(paycheck.getClient());
        //储值卡号
        gaiaSdRechargeChange.setGsrcAccountId(paycheck.getGsrcpCardId());
        //异动门店
        gaiaSdRechargeChange.setGsrcBrId(paycheck.getGsrcpBrId());
        //异动单号
        gaiaSdRechargeChange.setGsrcVoucherId(paycheck.getGsrcpVoucherId());
        //异动日期
        gaiaSdRechargeChange.setGsrcDate(paycheck.getGsrcpDate());
        //异动时间
        gaiaSdRechargeChange.setGsrcTime(paycheck.getGsrcpTime());
        //异动类型
        gaiaSdRechargeChange.setGsrcType("1"); // 1:充值，2：退款 3：消费 4：退货
        //储值卡初始金额
        gaiaSdRechargeChange.setGsrcInitialAmt(gsrcAmt);
        //储值卡异动金额
        gaiaSdRechargeChange.setGsrcChangeAmt(gsrcpAfterAmt);
        //储值卡结果金额
        gaiaSdRechargeChange.setGsrcResultAmt(gsrcAmt.add(gsrcpAfterAmt));
        //异动人员
        gaiaSdRechargeChange.setGsrcEmp(paycheck.getGsrcpEmp());
        this.rechargeChangeMapper.insertOne(gaiaSdRechargeChange);

        if ("1".equals(sdPaymentMethod.getGspmFalg())) {
            //调支付接口
            String paymentContent = sdPaymentMethod.getGspmRemark();
            //分
            int amt = vo.getGsrcpAfterAmt().multiply(new BigDecimal(100)).intValue();
            String merchantRemark = "储值卡充值";
            String srcReserved = "";
            String orderDesc = "储值卡支付";
            Result result = bsaoCPay.payV2(paymentContent, amt, merchantRemark, vo.getPayCode(), srcReserved, orderDesc);
            if (ResultUtil.hasError(result)) {
                throw new CustomResultException(result.getMessage());
            }
        }

        return 1;
    }


    @Override
    public Result getRechargeCardList(RechargeCardListVO entity, Page<RechargeCardLVO> page) {
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = sdRechargeCardService.common();
        //如果不选门店，默认查询有权限查询到的所有门店
        if(CollectionUtils.isEmpty(entity.getStoList())){
            List<StoreData> storeList =  sdRechargeCardChangeService.getCurrentUserAuthStoList();
            List<String> storeNoList = storeList.stream().map(StoreData::getStoCode).collect(Collectors.toList());
            //如果没有任何查询门店的权限，默认门店编号为空字符串，否则sql错误
            if(storeNoList.isEmpty()){
                storeNoList.add("");
            }
            entity.setStoList(storeNoList);
        }
        //获取门店编码或连锁总部编码 与储值卡相关联
        IPage<RechargeCardLVO> pageList = sdRechargeCardPaycheckMapper.SelectCardList(page, new QueryWrapper<RechargeCardListVO>()
                .eq("recharge.CLIENT", user.getClient())
                .eq("card.GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq(StringUtils.isNotBlank(entity.getGsspmId()), "gsspg.GSSPM_ID", entity.getGsspmId())
                .ge(StringUtils.isNoneBlank(entity.getGsrcpKateStart()), " recharge.GSRCP_DATE", entity.getGsrcpKateStart())
                .le(StringUtils.isNoneBlank(entity.getGsrcpKateEnd()), " recharge.GSRCP_DATE", entity.getGsrcpKateEnd())
                .like(StringUtils.isNotEmpty(entity.getGsrcId()), "card.GSRC_ID", entity.getGsrcId())
                .like(StringUtils.isNotEmpty(entity.getGsrcName()), "card.GSRC_NAME", entity.getGsrcName())
                .like(StringUtils.isNotEmpty(entity.getGsrcMobile()), "card.GSRC_MOBILE", entity.getGsrcMobile())
                .in(CollectionUtils.isNotEmpty(entity.getStoList()), "recharge.GSRCP_BR_ID", entity.getStoList())
                .apply(" recharge.GSRCP_BR_ID IN (SELECT AUTHOBJ_SITE FROM GAIA_AUTHCONFI_DATA WHERE CLIENT = {0} AND AUTHCONFI_USER = {1})", user.getClient(), user.getUserId())
                .orderByDesc("GSRCP_DATE", "GSRCP_TIME"), user.getClient());
        return ResultUtil.success(pageList);
    }

    /**
     * 退款
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void rechargeRefund(RechargeRefundVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();

        // 已有的充值记录
        SdRechargeCardPaycheck paycheck = sdRechargeCardPaycheckMapper.getPayCheckByCompositekey(vo, client);
        if (ObjectUtils.isEmpty(paycheck)) {
            throw new CustomResultException("没有找到该付款记录或已经执行退款");
        }
        // 销售支付信息 GAIA_SD_SALE_PAY_MSG
        SdSalePayMsg sdSalePayMsg = sdSalePayMsgMapper.getSalePayMsgByCompositekey(vo, client, OperateEnum.GsspmType.CARD.getCode());
        if (ObjectUtils.isEmpty(sdSalePayMsg)) {
            throw new CustomResultException("没有找到该支付信息记录");
        }
        // 储值卡信息 GAIA_SD_RECHARGE_CARD
        SdRechargeCard rechargeCard = sdRechargeCardMapper.getRechargeCardByCompositekey(vo, client);
        if (ObjectUtils.isEmpty(rechargeCard)) {
            throw new CustomResultException("没有找到对应储值卡信息");
        }
        // 充值单号
        String payCode = sdRechargeCardPaycheckMapper.getUserCardNumber(client).getGsrcpVoucherId();
        //判断支付方式是否
        SdPaymentMethod paymentMethod = sdPaymentMethodMapper.selectOne(new QueryWrapper<SdPaymentMethod>()
                .eq("CLIENT", client)
                .eq("GSPM_BR_ID", user.getDepId())
                .eq("GSPM_RECHARGE", CommonEnum.StrYesOrNo.YES.getCode())
                .eq("GSPM_ID", vo.getRefundMethodId()));
        if (null == paymentMethod) {
            throw new CustomResultException("退款支付方式不正确");
        }
        // 校验金额参数
        // 付款金额 > 剩余金额 使用剩余金额进行比对
        if (paycheck.getGsrcpYsAmt().compareTo(rechargeCard.getGsrcAmt()) >= 0
                && vo.getRefundYsAmt().compareTo(rechargeCard.getGsrcAmt()) > 0) {
            throw new CustomResultException("退款金额不可大于充值金额");
        }
        // 付款金额 < 剩余金额  使用付款金额进行比对
        if (paycheck.getGsrcpYsAmt().compareTo(rechargeCard.getGsrcAmt()) <= 0
                && vo.getRefundYsAmt().compareTo(paycheck.getGsrcpYsAmt()) > 0) {
            throw new CustomResultException("退款金额不可大于当前余额");
        }

        // 新增退款记录 GAIA_SD_RECHARGE_CARD_PAYCHECK
        SdRechargeCardPaycheck paycheckDO = new SdRechargeCardPaycheck();
        BeanUtils.copyProperties(paycheck, paycheckDO);
        paycheckDO.setGsrcpDate(DateUtils.getCurrentDateStrYYMMDD());
        paycheckDO.setGsrcpTime(DateUtils.getCurrentTimeStrHHMMSS());
        paycheckDO.setGsrcpEmp(userId);
        paycheckDO.setClient(client);
        paycheckDO.setGsrcpVoucherId(payCode);
        paycheckDO.setGsrcpYsAmt(vo.getRefundYsAmt().multiply(Constants.MINUS_ONE));
        paycheckDO.setGsrcpAfterAmt(vo.getRefundYsAmt().multiply(Constants.MINUS_ONE));
        paycheckDO.setGsrcpReturn(vo.getGsrccVoucherId());
        paycheckDO.setGsrcpFlag(OperateEnum.GsrcpFlag.REFUND.getCode());
        this.save(paycheckDO);

        // 新增销售支付信息 GAIA_SD_SALE_PAY_MSG
        SdSalePayMsg salePayMsgDO = new SdSalePayMsg();
        BeanUtils.copyProperties(sdSalePayMsg, salePayMsgDO);
        salePayMsgDO.setClient(client);
        salePayMsgDO.setGsspmDate(DateUtils.getCurrentDateStrYYMMDD());
        salePayMsgDO.setGsspmBillNo(payCode);
        salePayMsgDO.setGsspmId(paymentMethod.getGspmId());
        salePayMsgDO.setGsspmType(OperateEnum.GsspmType.CARD.getCode());
        salePayMsgDO.setGsspmName(paymentMethod.getGspmName());
        salePayMsgDO.setGsspmAmt(vo.getRefundYsAmt().multiply(Constants.MINUS_ONE));
        if ("1001".equals(paymentMethod.getGspmId())) {
            salePayMsgDO.setGsspmRmbAmt(vo.getRefundYsAmt().multiply(Constants.MINUS_ONE));
            salePayMsgDO.setGsspmZlAmt(BigDecimal.ZERO);
        }
        sdSalePayMsgMapper.insert(salePayMsgDO);

        BigDecimal gsrcAmt1 = rechargeCard.getGsrcAmt();//初始金额
        BigDecimal refundYsAmt = vo.getRefundYsAmt();//退款金额
        // 更新余额
        BigDecimal gsrcAmt = rechargeCard.getGsrcAmt().subtract(vo.getRefundYsAmt());
        sdRechargeCardMapper.updateGsrcAmt(client, vo.getGsrcAccountId(), vo.getGsrcId(), gsrcAmt);

        //生成一条充值卡金额异动记录
        SdRechargeChange gaiaSdRechargeChange = new SdRechargeChange();
        //加盟商
        gaiaSdRechargeChange.setClient(paycheckDO.getClient());
        //储值卡号
        gaiaSdRechargeChange.setGsrcAccountId(paycheckDO.getGsrcpCardId());
        //异动门店
        gaiaSdRechargeChange.setGsrcBrId(paycheckDO.getGsrcpBrId());
        //异动单号
        gaiaSdRechargeChange.setGsrcVoucherId(paycheckDO.getGsrcpVoucherId());
        //异动日期
        gaiaSdRechargeChange.setGsrcDate(paycheckDO.getGsrcpDate());
        //异动时间
        gaiaSdRechargeChange.setGsrcTime(paycheckDO.getGsrcpTime());
        //异动类型
        gaiaSdRechargeChange.setGsrcType("2"); // 1:充值，2：退款 3：消费 4：退货
        //储值卡初始金额
        gaiaSdRechargeChange.setGsrcInitialAmt(gsrcAmt1);
        //储值卡异动金额
        gaiaSdRechargeChange.setGsrcChangeAmt(refundYsAmt.negate());
        //储值卡结果金额
        gaiaSdRechargeChange.setGsrcResultAmt(gsrcAmt1.subtract(refundYsAmt));
        //异动人员
        gaiaSdRechargeChange.setGsrcEmp(paycheckDO.getGsrcpEmp());
        rechargeChangeMapper.insertOne(gaiaSdRechargeChange);
    }

    @Override
    public Result rechargeRecordExport(RechargeCardListVO entity, Page<RechargeCardLVO> page) throws IOException {
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = sdRechargeCardService.common();
        //如果不选门店，默认查询有权限查询到的所有门店
        if(CollectionUtils.isEmpty(entity.getStoList())){
            List<StoreData> storeList =  sdRechargeCardChangeService.getCurrentUserAuthStoList();
            List<String> storeNoList = storeList.stream().map(StoreData::getStoCode).collect(Collectors.toList());
            //如果没有任何查询门店的权限，默认门店编号为空字符串，否则sql错误
            if(storeNoList.isEmpty()){
                storeNoList.add("");
            }
            entity.setStoList(storeNoList);
        }
        //获取门店编码或连锁总部编码 与储值卡相关联
        IPage<RechargeCardLVO> pageList = sdRechargeCardPaycheckMapper.SelectCardList(page, new QueryWrapper<RechargeCardListVO>()
                .eq("recharge.CLIENT", user.getClient())
                .eq("card.GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq(StringUtils.isNotBlank(entity.getGsspmId()), "gsspg.GSSPM_ID", entity.getGsspmId())
                .ge(StringUtils.isNoneBlank(entity.getGsrcpKateStart()), " recharge.GSRCP_DATE", entity.getGsrcpKateStart())
                .le(StringUtils.isNoneBlank(entity.getGsrcpKateEnd()), " recharge.GSRCP_DATE", entity.getGsrcpKateEnd())
                .like(StringUtils.isNotEmpty(entity.getGsrcId()), "card.GSRC_ID", entity.getGsrcId())
                .like(StringUtils.isNotEmpty(entity.getGsrcName()), "card.GSRC_NAME", entity.getGsrcName())
                .like(StringUtils.isNotEmpty(entity.getGsrcMobile()), "card.GSRC_MOBILE", entity.getGsrcMobile())
                .in("recharge.GSRCP_BR_ID", entity.getStoList())
                .orderByDesc("GSRCP_DATE", "GSRCP_TIME"), user.getClient());
        List<List<Object>> dataList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(pageList.getRecords())) {
            // 组建导出数据
            pageList.getRecords().forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 门店
                data.add(item.getGsrcpBrName());
                // 单号
                data.add(item.getGsrcpVoucherId());
                // 账号
                data.add(item.getGsrcAccountId());
                // 卡号
                data.add(item.getGsrcId());
                // 姓名
                data.add(item.getGsrcName());
                // 性别
                if (OperateEnum.Sex.MAN.getCode().equals(item.getGsrcSex())) {
                    data.add(OperateEnum.Sex.MAN.getMessage());
                } else if (OperateEnum.Sex.WOMAN.getCode().equals(item.getGsrcSex())) {
                    data.add(OperateEnum.Sex.WOMAN.getMessage());
                } else {
                    data.add("");
                }
                // 手机号码
                data.add(item.getGsrcMobile());
                // 充值日期
                data.add(item.getGsrcpDate());
                // 充值时间
                data.add(item.getGsrcpTime());
                // 充值金额
                data.add(item.getGsrcpAfterAmt());
                // 充值人员姓名
                data.add(item.getUserName());
                // 充值类型
                if (OperateEnum.GsrcpFlag.NORMAL.getCode().equals(item.getGsrcpFlag())) {
                    data.add(OperateEnum.GsrcpFlag.NORMAL.getName());
                } else if (OperateEnum.GsrcpFlag.REFUND.getCode().equals(item.getGsrcpFlag())) {
                    data.add(OperateEnum.GsrcpFlag.REFUND.getName());
                } else {
                    data.add(OperateEnum.GsrcpFlag.NORMAL.getName());
                }
                // 支付方式
                data.add(item.getGspmName());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("储值卡充值");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 导出表头
     */
    private String[] headList = {
            "门店",
            "单号",
            "账号",
            "卡号",
            "姓名",
            "性别",
            "手机号码",
            "充值日期",
            "充值时间",
            "充值金额",
            "充值人员姓名",
            "充值精型",
            "支付方式",
    };
}
