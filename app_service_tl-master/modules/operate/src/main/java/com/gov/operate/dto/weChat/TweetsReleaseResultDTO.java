package com.gov.operate.dto.weChat;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/14 13:50
 */
@Data
@AllArgsConstructor
public class TweetsReleaseResultDTO {
    /**
     * 图文id
     */
    private String mediaId;
    /**
     * 图文各个封面图片id
     */
    private List<String> thumbMediaIdList;
}
