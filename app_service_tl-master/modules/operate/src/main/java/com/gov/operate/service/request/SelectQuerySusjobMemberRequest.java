package com.gov.operate.service.request;

import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class SelectQuerySusjobMemberRequest extends Pageable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务编码")
    @NotBlank(message = "会员维系任务不能为空")
    private String jobId;

    @ApiModelProperty(value = "会员回访类型0待回访1已接听2无应答3已拦截")
    @NotBlank(message = "会员回访类型不能为空")
    private String susStatus = "0";

    @ApiModelProperty(value = "门店编码")
    @NotBlank(message = "门店编码不能为空")
    private String stoCode;

    @ApiModelProperty(value = "年龄段")
    private List<Integer> ageStage;

    @ApiModelProperty(value = "编号")
    private List<Integer> numberStage;

    @ApiModelProperty(value = "性别")
    private String susSex;

}
