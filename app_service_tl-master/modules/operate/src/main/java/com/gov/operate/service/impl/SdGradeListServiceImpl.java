package com.gov.operate.service.impl;

import com.gov.operate.entity.SdGradeList;
import com.gov.operate.mapper.SdGradeListMapper;
import com.gov.operate.service.ISdGradeListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
@Service
public class SdGradeListServiceImpl extends ServiceImpl<SdGradeListMapper, SdGradeList> implements ISdGradeListService {

    @Override
    public List<SdGradeList> getGradeList() {
        return this.list();
    }
}
