package com.gov.operate.service.impl;

import com.gov.operate.entity.SmsSend;
import com.gov.operate.mapper.SmsSendMapper;
import com.gov.operate.service.ISmsSendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Service
public class SmsSendServiceImpl extends ServiceImpl<SmsSendMapper, SmsSend> implements ISmsSendService {

}
