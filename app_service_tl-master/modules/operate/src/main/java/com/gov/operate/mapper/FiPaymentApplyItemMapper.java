package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.PaymentApplyItem;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zp
 * @since 2021-04-01
 */
public interface FiPaymentApplyItemMapper extends BaseMapper<PaymentApplyItem> {
}
