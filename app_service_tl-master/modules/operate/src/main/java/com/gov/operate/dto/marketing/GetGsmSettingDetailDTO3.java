package com.gov.operate.dto.marketing;

import com.gov.operate.dto.marketing.SdMarketingBasicAttacDTO;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SdMarketingBasic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetGsmSettingDetailDTO3 extends SdMarketingBasic {

    /**
     * 宣传物料
     */
    List<SdMarketingBasicAttacDTO> marketingBasicAttacList;
    /**
     * 加盟商详情
     */
    List<Franchisee> clientList;
}
