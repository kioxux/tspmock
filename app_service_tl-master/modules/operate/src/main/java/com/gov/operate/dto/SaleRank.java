package com.gov.operate.dto;

import lombok.Data;

@Data
public class SaleRank {
    //销售商品门店内部编码
    private String proId;
    //销售商品名称
    private String proName;
    //销售数量
    private String qty;
    //销售额
    private String amt;
    //最高价
    private String maxprc;
    //最低价
    private String minprc;
    //毛利额
    private String anMD;
    //毛利率
    private String anGM;
}
