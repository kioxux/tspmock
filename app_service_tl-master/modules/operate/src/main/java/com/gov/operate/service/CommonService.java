package com.gov.operate.service;

import com.gov.operate.dto.PromDTO;
import com.gov.operate.dto.StoreVo;
import com.gov.operate.feign.dto.TokenUser;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 内部接口调用接口类
 */
@Service
public interface CommonService {

//    /**
//     * 商品首营流程创建
//     * @param gspInfo
//     * @return
//     */
//    public FeignResult createProductWorkflow(GaiaProductGspinfo gspInfo);

    /**
     * 获取当前登录人
     *
     * @return
     */
    public TokenUser getLoginInfo();


    /**
     * 获取当前登录人门店编码集合
     *
     * @param client
     * @param userId
     * @return
     */
    public List<StoreVo> getCurrentUserStores(String client, String userId);


    /**
     * 促销保存
     *
     * @param gsph_marketid 营销ID
     * @param client        加盟商
     * @param voucherList   商品集合
     */
    void saveProm(String gsph_marketid, String client, List<PromDTO> voucherList, List<PromDTO> proList) throws InstantiationException, IllegalAccessException;

    /**
     * 促销类型判断
     *
     * @param types
     * @return
     */
    String getPromType(List<String> types);

    List<String> getStoCodeList();

    String queryWapperSto();

    String queryWapperSto(List<String> list);

    String queryWapperAuto(List<String> list);

    String queryWapper(List<String> stoCodeList);
}
