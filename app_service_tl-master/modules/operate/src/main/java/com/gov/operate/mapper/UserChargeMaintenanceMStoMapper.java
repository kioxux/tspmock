package com.gov.operate.mapper;

import com.gov.operate.entity.UserChargeMaintenanceMSto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户收费维护明细门店表 Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface UserChargeMaintenanceMStoMapper extends BaseMapper<UserChargeMaintenanceMSto> {

}
