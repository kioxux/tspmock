package com.gov.operate.dto.customer.order;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021-06-22 13:10
 */
@Data
public class WorkOrderReplyVO {

    @NotNull(message = "工单主键不能为空")
    private Long gwoId;

    @NotNull(message = "发布主体不能为空")
    private Integer gworOrg;

    @NotBlank(message = "客户ID不能为空")
    private String gworClient;

    @NotBlank(message = "用户ID不能为空")
    private String gworUserId;

//   NotBlankll(message = "客户名不能为空")
    private String gworClientName;

//   NotBlankll(message = "用户名不能为空")
    private String gworUserName;

//    @NotBlank(message = "发布内容不能为空")
    private String gworSendContent;

    /**
     * 附件
     */
    List<WorkOrderReplyAtt> workOrderReplyAtts;


}
