package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class SaveProGroupProductVO {

    @NotBlank(message = "所属门店不能为空")
    private String gsyStore;

    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;

    @NotBlank(message = "商品通用名不能为空")
    private String gsyCommonname;

//    @NotBlank(message = "商品描述不能为空")
    private String gsyDepict;

    @NotBlank(message = "助记码不能为空")
    private String gsyPym;

    /**
     * 规格
     */
    private String gsySpecs;

    /**
     * 单位
     */
    private String gsyUnit;


}
