package com.gov.operate.controller;

import com.gov.common.response.Result;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.service.ISdMemberGradeListService;
import com.gov.operate.service.ISdMemberTagListService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("memberReport")
public class MemberReportController {

    @Resource
    private ISdMemberTagListService sdMemberTagListService;

    @Resource
    private ISdMemberGradeListService sdMemberGradeListService;

    /**
     * 会员分布图表
     *
     * @param requestDto
     * @return
     */
    @PostMapping("tagDistributionList")
    public Result tagDistributionList(@RequestBody MemberReportRequestDto requestDto) {
        return sdMemberTagListService.getTagDistributionList(requestDto);
    }

    /**
     * 会员分布图表
     *
     * @param requestDto
     * @return
     */
    @PostMapping("tagDistributionListNew")
    public Result tagDistributionListNew(@RequestBody MemberReportRequestDto requestDto) {
        return sdMemberTagListService.tagDistributionListNew(requestDto);
    }

    /**
     * 会员分级统计图表
     * @param requestDto
     * @return
     */
    @PostMapping("gradeDistributionList")
    public Result gradeDistributionList(@RequestBody MemberReportRequestDto requestDto) {
        return sdMemberGradeListService.gradeDistributionList(requestDto);
    }

    /**
     * 会员分级统计图表
     * @param requestDto
     * @return
     */
    @PostMapping("gradeDistributionListNew")
    public Result gradeDistributionListNew(@RequestBody MemberReportRequestDto requestDto) {
        return sdMemberGradeListService.gradeDistributionListNew(requestDto);
    }

    /**
     * 会员分级趋势图表
     * @param requestDto
     * @return
     */
    @PostMapping("gradeTrendList")
    public Result gradeTrendList(@RequestBody MemberReportRequestDto requestDto) {
        return sdMemberGradeListService.gradeTrendList(requestDto);
    }

    /**
     * 会员分级趋势图表
     * @param requestDto
     * @return
     */
    @PostMapping("gradeTrendListNew")
    public Result gradeTrendListNew(@RequestBody MemberReportRequestDto requestDto) {
        return sdMemberGradeListService.gradeTrendListNew(requestDto);
    }


}
