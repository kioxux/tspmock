package com.gov.operate.mapper;

import com.gov.operate.entity.SalaryGrade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
public interface SalaryGradeMapper extends BaseMapper<SalaryGrade> {

}
