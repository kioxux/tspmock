package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class EnableMarketingTemplateVO {

    @NotBlank(message = "Id不能为空")
    private String smsId;

    @NotBlank(message = "0启用，1未启用")
    private String enable;
}
