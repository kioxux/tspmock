package com.gov.operate.service.dataImport.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.common.entity.dataImport.PromGiftCondsProduct;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.PromGiftCondsProductImportDto;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IProductBusinessService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: xx
 * @date: 2021.2.08
 */
@Service
public class PromGiftCondsProductImport extends BusinessImport {


    @Resource
    private IProductBusinessService productBusinessService;
    @Resource
    private CommonService commonService;

    // 导入
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = commonService.getLoginInfo();
        // 已选商品
        List<String> proSelfCodeList = null;
        if (field.get("proSelfCodeList") != null) {
            proSelfCodeList = (List<String>) field.get("proSelfCodeList");
        }
        if (proSelfCodeList == null) {
            proSelfCodeList = new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        //查询产品信息
        List<ProductBusiness> productBusinessList = productBusinessService.list(new LambdaQueryWrapper<ProductBusiness>()
                .select(ProductBusiness::getProSelfCode, ProductBusiness::getProCommonname, ProductBusiness::getProFactoryName
                        , ProductBusiness::getProName, ProductBusiness::getProSpecs)
                .eq(ProductBusiness::getClient, user.getClient())
                .groupBy(ProductBusiness::getProSelfCode));
        List<String> dataProSelfCodeList = productBusinessList.stream().map(ProductBusiness::getProSelfCode).collect(Collectors.toList());
        Map<String, ProductBusiness> proDataMap = productBusinessList.stream().collect(Collectors.toMap(ProductBusiness::getProSelfCode, s -> s, (key1, key2) -> key2));
        List<String> errorList = new ArrayList<>();
        List<String> resultList = new ArrayList<>();
        List<PromGiftCondsProductImportDto> returnDtoList = new ArrayList<>();
        // 行数据处理
        for (Integer key : map.keySet()) {
            // 行数据
            PromGiftCondsProduct promGiftCondsProduct = (PromGiftCondsProduct) map.get(key);
            if (proSelfCodeList.contains(promGiftCondsProduct.getProSelfCode())) {
                errorList.add(MessageFormat.format("第{0}行：商品已存在", key + 1));
                continue;
            }
            if (resultList.contains(promGiftCondsProduct.getProSelfCode())) {
                errorList.add(MessageFormat.format("第{0}行：商品已存在导入表格中", key + 1));
                continue;
            }
            // 商品存在验证
            if (dataProSelfCodeList.contains(promGiftCondsProduct.getProSelfCode())) {
                resultList.add(promGiftCondsProduct.getProSelfCode());
                PromGiftCondsProductImportDto dto = new PromGiftCondsProductImportDto();
                BeanUtils.copyProperties(promGiftCondsProduct, dto);
                ProductBusiness productBusiness = proDataMap.get(promGiftCondsProduct.getProSelfCode());
                dto.setProCommonname(productBusiness.getProCommonname());
                dto.setProSelfCode(productBusiness.getProSelfCode());
                dto.setProName(productBusiness.getProName());
                dto.setProSpecs(productBusiness.getProSpecs());
                dto.setProFactoryName(productBusiness.getProFactoryName());
                dto.setGspgcInteFlag("是".equals(dto.getGspgcInteFlag()) ? "1" : "0");
                dto.setGspgcMemFlag("是".equals(dto.getGspgcMemFlag()) ? "1" : "0");
                returnDtoList.add(dto);
            } else {
                errorList.add(MessageFormat.format("第{0}行：商品编码库中不存在", key + 1));
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(returnDtoList);
    }
}
