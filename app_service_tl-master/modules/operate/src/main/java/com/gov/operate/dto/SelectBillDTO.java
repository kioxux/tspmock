package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectBillDTO {

    /**
     * 业务类型
     */
    private String businessType;
    /**
     * 业务类型名
     */
    private String businessTypeName;
    /**
     * 单据号
     */
    private String matDnId;
    /**
     * 单据日期
     */
    private String matDate;

    /**
     * 应付金额
     */
    private BigDecimal totalAmount;
    /**
     * 已付金额
     */
    private BigDecimal paidAmount;
    /**
     * 本次登记金额
     */
    private BigDecimal settlementAmount;
    /**
     * 剩余金额
     */
    private BigDecimal chargeAmount;
    /**
     * 关联发票已付
     */
    private Integer invoicePayd;

    private String siteCode;

    private String siteName;

    /**
     * 关联发票
     */
    private String invoiceNums;

    /**
     * 商品编码
     */
    private String soProCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 行金额
     */
    private BigDecimal lineAmount;

    /**
     * 业务员姓名
     */
    private String invoiceSalesmanName;

    /**
     * 供应商编号
     */
    private String supSelfCode;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 备注
     */
    private String remark;
}
