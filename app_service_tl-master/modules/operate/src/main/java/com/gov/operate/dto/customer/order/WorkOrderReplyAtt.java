package com.gov.operate.dto.customer.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-02-22
 */
@Data
public class WorkOrderReplyAtt  {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "工单主键")
    @TableField("GWO_ID")
    private Long gwoId;

    @ApiModelProperty(value = "工单处理记录ID")
    @TableField("GWOR_ID")
    private Long gworId;

    @ApiModelProperty(value = "附件类型")
    @TableField("GWORA_TYPE")
    private Integer gworaType;

    @ApiModelProperty(value = "路径")
    @TableField("GWORA_PATH")
    private String gworaPath;

    @ApiModelProperty(value = "文件名")
    @TableField("GWORA_NAME")
    private String gworaName;

    @ApiModelProperty(value = "文件大小")
    @TableField("GWORA_SIZE")
    private Long gworaSize;

    @ApiModelProperty(value = "创建人名称")
    @TableField("CREATE_USER_NAME")
    private String createUserName;

    @ApiModelProperty(value = "创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime updateTime;

}
