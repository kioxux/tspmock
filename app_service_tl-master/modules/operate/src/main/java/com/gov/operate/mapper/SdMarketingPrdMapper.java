package com.gov.operate.mapper;

import com.gov.operate.entity.SdMarketingPrd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface SdMarketingPrdMapper extends BaseMapper<SdMarketingPrd> {

    int resultVoucherId(@Param("list") List<SdMarketingPrd> sdMarketingPrdUpdateList);
}
