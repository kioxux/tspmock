package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-11
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_APP_LOGIN_LOG")
@ApiModel(value="AppLoginLog对象", description="")
public class AppLoginLog extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工编号")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "登录凭证")
    @TableField("TOKEN")
    private String token;

    @ApiModelProperty(value = "登录平台")
    @TableField("PLATFORM")
    private String platform;

    @ApiModelProperty(value = "设备号")
    @TableField("DEVICE_NO")
    private String deviceNo;

    @ApiModelProperty(value = "登录时间")
    @TableField("LOGINTIME")
    private String logintime;

    @ApiModelProperty(value = "创建日期")
    @TableField("USER_CRE_DATE")
    private String userCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("USER_CRE_TIME")
    private String userCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("USER_CRE_ID")
    private String userCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("USER_MODI_DATE")
    private String userModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("USER_MODI_TIME")
    private String userModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("USER_MODI_ID")
    private String userModiId;


}
