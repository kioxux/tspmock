package com.gov.operate.dto.storePayment;

import com.gov.operate.entity.SmsSend;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.17
 */
@Data
public class SmsSendRecord extends SmsSend {
    /**
     * 加盟商名
     */
    private String francName;
}
