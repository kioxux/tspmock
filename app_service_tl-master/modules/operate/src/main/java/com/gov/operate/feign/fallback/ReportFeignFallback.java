package com.gov.operate.feign.fallback;

import com.gov.operate.feign.ReportFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.MatchProductInData;
import org.springframework.stereotype.Component;


@Component
public class ReportFeignFallback implements ReportFeign {

    @Override
    public JsonResult getProductMatch(MatchProductInData inData) {
        JsonResult result = new JsonResult();
        result.setCode(9999);
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }
}
