package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: jinwencheng
 * @Date: Created in 2021-12-23 16:21:32
 * @Description: 添加门店分类 - 门店选择 - 查询需要的参数
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
public class GetStoresForAddDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 分类类型编码
     */
    @NotBlank(message = "分类类型编码不能为空")
    private String gssgType;

    /**
     * 是否批量查询：1-是；0-否
     */
    @NotNull(message = "是否批量查询不能为空")
    private Integer isBatchSearch;

    /**
     * 查询条件：门店编码/门店名称/分类名称，如果为批量查询，则为门店编码，多个门店编码用"/r/n"拼接，如果为NULL，则查询全部
     */
    private String condition;

    /**
     * 门店编码数组
     */
    private String[] stoCode;

}
