package com.gov.operate.mapper;

import com.gov.operate.entity.SdRechargeCardSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface SdRechargeCardSetMapper extends BaseMapper<SdRechargeCardSet> {
    /**
     * 获取当前人的门店id 或连锁总部下的所有的门店id
     * @param UserId
     * @return
     */
    List<String> getStoreCode(@Param("userId")String UserId,@Param("client")String client );

    void BatchDelete(@Param("client")String client,@Param("list")List<SdRechargeCardSet> list );

}
