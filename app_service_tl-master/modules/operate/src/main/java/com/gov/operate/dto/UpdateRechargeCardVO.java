package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateRechargeCardVO {

    private String gsrccRemark;

    @NotBlank(message = "储值卡号不能为空")
    private String gsrcId;

    @NotBlank(message = "储值账户不能为空")
    private String gsrcAccountId;

    private String  gsrcIdNew;

}
