package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectTaskDetailResponse {

    @ApiModelProperty(value = "任务ID")
    private String susJobid;

    @ApiModelProperty(value = "任务名称")
    private String susJobname;

    @ApiModelProperty(value = "维系说明")
    private String susExplain;

    @ApiModelProperty(value = "维系话术")
    private String susContent;

    @ApiModelProperty(value = "计划开始时间")
    private String susBegin;

    @ApiModelProperty(value = "计划结束时间")
    private String susEnd;

    @ApiModelProperty(value = "时间格式化")
    private String date;
}
