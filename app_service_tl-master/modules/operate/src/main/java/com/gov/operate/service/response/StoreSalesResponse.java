package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreSalesResponse {


    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店编码")
    private String store;

    @ApiModelProperty(value = "会员卡")
    private int memberCard;


}
