package com.gov.operate.service.batchPurchaseSales.impl;

import com.gov.operate.entity.BatchQueryAuthData;
import com.gov.operate.mapper.BatchQueryAuthDataMapper;
import com.gov.operate.service.batchPurchaseSales.IBatchQueryAuthDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 查询数据清单 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Service
public class BatchQueryAuthDataServiceImpl extends ServiceImpl<BatchQueryAuthDataMapper, BatchQueryAuthData> implements IBatchQueryAuthDataService {

}
