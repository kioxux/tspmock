package com.gov.operate.service;

import com.gov.common.entity.Pageable;
import com.gov.common.response.Result;
import com.gov.operate.dto.SubmitSusjobDetail;
import com.gov.operate.dto.SusjobDetailsQuery;
import com.gov.operate.dto.SusjobTaskListQuery;
import com.gov.operate.service.request.SdSusjobHRequest;
import com.gov.operate.service.request.SelectQuerySusjobMemberRequest;
import com.gov.operate.service.request.SelectStatusCountRequset;
import com.gov.operate.service.request.UpdateStatusRequest;
import com.gov.operate.service.response.ConsumeInfoDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */
public interface ISusjobTaskService {

    /**
     * 会员维系任务
     *
     * @param susjobTaskListQuery
     * @return
     */
    Result querySusjobTaskList(SusjobTaskListQuery susjobTaskListQuery);

    /**
     * 会员维系任务明细
     *
     * @param susjobDetailQuery
     * @return
     */
    Result querySusjobDetails(SusjobDetailsQuery susjobDetailQuery);

    /**
     * 维护提交
     *
     * @param submitSusjobDetail
     * @return
     */
    Result submitSusjobDetail(SubmitSusjobDetail submitSusjobDetail);

    /**
     * 我的维系任务
     *
     * @return
     */
    Result queryMySusjobTaskList(Pageable pageable);

    /**
     * 标签列表
     * @return
     */
    Result getTagList();

    Result querySusjobTaskListNew(SusjobTaskListQuery susjobTaskListQuery);

    /**
     * 会员维系任务列表
     * @param type 0.我的维度任务 1.会员维度任务
     * @return
     */
    Result querySusjobTaskAppList(SdSusjobHRequest request);

    /**
     * 历史维系任务列表
     * @param request stoCode门店编码   taskName任务名称模糊查询
     * @return
     */
    Result querySusjobTaskAppHistoryList(SdSusjobHRequest request);

    /**
     * 修改会员维系状态
     * @param taskType 1进行中
     * @return
     */
    Result updateQuerySusjobTaskApp(String jobId,String taskType);

    /**
     * 会员维系任务会员列表
     * @param request
     * @return
     */
    Result selectQuerySusjobMemberApp(SelectQuerySusjobMemberRequest request);

    /**
     * 修改接听状态
     * @param request
     * @return
     */
    Result updateStatus(UpdateStatusRequest request);

    /**
     * 任务详情
     * @param jobId 任务ID
     * @return
     */
    Result selectTaskDetail(String jobId);

    Result selectStatusCount(SelectStatusCountRequset request);

    /**
     * 会员用户信息
     * @param cardId 会员卡号
     * @return
     */
    Result selectCardUserDetail(String cardId);

    ConsumeInfoDTO getOrderInfoById(String gsmbMemberId, String startDate, String endDate);
}
