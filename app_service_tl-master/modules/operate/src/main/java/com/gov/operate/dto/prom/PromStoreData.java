package com.gov.operate.dto.prom;

import com.gov.operate.entity.StoreData;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021-08-10 14:21
 */
@Data
public class PromStoreData extends StoreData {
    /**
     * 店型
     */
    private String gssgId;

    private String newgssgId;
    /**
     * 店型名称
     */
    private String gssgName;

    private String newGssgName;
    /**
     * 区域
     */
    private String areaId;
    /**
     * 区域名称
     */
    private String areaName;
}
