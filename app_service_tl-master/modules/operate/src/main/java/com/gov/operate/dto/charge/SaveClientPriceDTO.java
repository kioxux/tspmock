package com.gov.operate.dto.charge;

import com.gov.operate.entity.UserChargeMaintenanceFree;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class SaveClientPriceDTO {
    private static final long serialVersionUID=1L;

    private Long id;

    private Long gscClientId;

    private String client;

    private String gscCoName;

    private LocalDate gscSignDate;

    private LocalDate gscExcptStartDate;

    private LocalDate gscExcptEndDate;

    private Integer gscLimit;

    private Integer gscStoCount;

    private Integer gscPayFreq;

    private Integer gscDiscount;

    private String gscContractCode;

    private BigDecimal gscPayable;

    private BigDecimal gscActualPay;

    private BigDecimal gscDscntAmount;

    private BigDecimal gscAvgAmount;

    private BigDecimal gscGpPrice;

    private BigDecimal gscFreeMonths;

    private String spStartingDate;
    private String spEndingDate;

    private Integer spSend;

    private List<SaveClientPricePackDTO> signPackList;
    private List<UserChargeMaintenanceFree> signFreeList;

    public String getSpStartingDate() {
        return spStartingDate;
    }

    public Integer getSpSend() {
        return spSend;
    }

    public Long getSpClientId() {
        return gscClientId;
    }

    public String getSpCoName() {
        return gscCoName;
    }

    public LocalDate getSpSignDate() {
        return gscSignDate;
    }

    public LocalDate getSpExcptStartDate() {
        return gscExcptStartDate;
    }

    public LocalDate getSpExcptEndDate() {
        return gscExcptEndDate;
    }

    public Integer getSpLimit() {
        return gscLimit;
    }

    public Integer getSpStoCount() {
        return gscStoCount;
    }

    public Integer getSpPayFreq() {
        return gscPayFreq;
    }

    public Integer getSpDiscount() {
        return gscDiscount;
    }

    public String getSpContractCode() {
        return gscContractCode;
    }

    public BigDecimal getSpPayable() {
        return gscPayable;
    }

    public BigDecimal getSpActualPay() {
        return gscActualPay;
    }

    public BigDecimal getSpDscntAmount() {
        return gscDscntAmount;
    }

    public BigDecimal getSpAvgAmount() {
        return gscAvgAmount;
    }

    public BigDecimal getSpGpPrice() {
        return gscGpPrice;
    }

    public BigDecimal getSpFreeMonths() {
        return gscFreeMonths;
    }

}
