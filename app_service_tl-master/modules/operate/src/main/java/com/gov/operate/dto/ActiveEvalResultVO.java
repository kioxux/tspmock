package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

@Data
public class ActiveEvalResultVO {

    /**
     * 销售额
     */
    private List<BarChartVO> salesList;

    /**
     * 毛利额
     */
    private List<BarChartVO> grossProfitList;

    /**
     * 毛利率
     */
    private List<BarChartVO> profitList;

    /**
     * 日均交易额
     */
    private List<BarChartVO> avgList;

    /**
     * 交易次数
     */
    private List<BarChartVO> stincList;

    /**
     * 客单价
     */
    private List<BarChartVO> priceincist;

}
