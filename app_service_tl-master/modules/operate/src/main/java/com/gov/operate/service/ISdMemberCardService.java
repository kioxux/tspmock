package com.gov.operate.service;

import com.gov.operate.entity.SdMemberCard;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-17
 */
public interface ISdMemberCardService extends SuperService<SdMemberCard> {

}
