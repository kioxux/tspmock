package com.gov.operate.dto;

import lombok.Data;

@Data
public class SaveGsmSettingCountListVO {

//    @NotBlank(message = "折扣ID不能为空")
    private String gsmDiscount;

//    @NotBlank(message = "折扣名称不能为空")
    private String gsmDiscountName;

//    @NotBlank(message = "促销ID不能为空")
    private String gsmPromotId;

    /**
     * 参数1
     */
    private String parameter1;

    /**
     * 参数2
     */
    private String parameter2;

    /**
     * 参数3
     */
    private String parameter3;

    /**
     * 参数4
     */
    private String parameter4;
}
