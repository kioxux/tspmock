package com.gov.operate.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.DateUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.BindRecipelDto;
import com.gov.operate.dto.RecipelDto;
import com.gov.operate.dto.RecipelPageDto;
import com.gov.operate.entity.GaiaSdRecipelList;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.RecipelService;
import com.mysql.jdbc.util.ResultSetUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

import static com.gov.common.basic.DateUtils.addDay;
import static com.gov.common.response.ResultEnum.*;

/**
 * <p>
 * 门店主数据信息表 前端控制器
 * </p>
 *
 * @author tzh
 * @since 2021-09-07
 */
@RestController
@Api(tags = "处方列表")
@RequestMapping("/recipel")
public class RecipelController {

    @Autowired
    private RecipelService  recipelService;
    @Autowired
    private CommonService commonService;

    @PostMapping (value = "recipels")
    public Result getRecipels(@RequestBody RecipelPageDto dto){
        TokenUser user = commonService.getLoginInfo();
        if (dto.getPageNum() == null) {
            dto.setPageNum(1);
        }
        if (dto.getPageSize() == null || dto.getPageSize() > 100) {
            dto.setPageSize(20);
        }
        //显示最近两天内 已经上传 且并未绑定 时间倒序
       // Page<GaiaSdRecipelList> page=new Page<>(dto.getPageNum(), dto.getPageSize());
        QueryWrapper<GaiaSdRecipelList> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("FLAG","0");
        queryWrapper.eq("CLIENT",user.getClient());
        queryWrapper.eq("BR_ID",user.getDepId());
        Date beginDate = addDay(new Date(), -2);
        queryWrapper.ge("CREATE_DATE",beginDate);
        queryWrapper.le("CREATE_DATE",new Date());
        queryWrapper.orderByDesc("CREATE_DATE");
        //Page<GaiaSdRecipelList> listPage = recipelService.page(page, queryWrapper);
        List<GaiaSdRecipelList> list = recipelService.list(queryWrapper);
        return ResultUtil.success(list);
    }
    /*
      创建审方
     */
    @PostMapping(value = "recipel")
    public Result createRecipels(@RequestBody @Valid RecipelDto recipelDto){
        TokenUser user = commonService.getLoginInfo();
        GaiaSdRecipelList list =new GaiaSdRecipelList();
        list.setClient(user.getClient());
        list.setBrId(recipelDto.getDepId());
        list.setEmp(recipelDto.getLoginName());
        list.setFlag("0");
        list.setCreateDate(new Date());
        list.setPicAddress(recipelDto.getPicAddress());
        list.setPicName(recipelDto.getPicName()+ DateUtils.getCurrentDateTimeStrTwo());
        list.setPicId(UUIDUtil.getShortUUID());
        recipelService.save(list);
        return ResultUtil.success();
    }

    @PostMapping(value = "bindRecipel")
    public Result updateRecipel(@RequestBody @Valid BindRecipelDto dto){
       // TokenUser user = commonService.getLoginInfo();
        //数据是否存在
        GaiaSdRecipelList recipel= recipelService.getById(dto.getId());
        if(recipel==null){
        return  ResultUtil.error(R0001);
        }
        //是否已经绑定
        if(!recipel.getFlag().equals("0")){
         return   ResultUtil.error(R0002);
        }
        recipel.setFlag("1");
        recipelService.saveOrUpdate(recipel);
        return ResultUtil.success();
    }




}

