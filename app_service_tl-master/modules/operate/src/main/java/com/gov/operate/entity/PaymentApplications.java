package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYMENT_APPLICATIONS")
@ApiModel(value="PaymentApplications对象", description="")
public class PaymentApplications extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "供应商")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "申请日期")
    @TableField("PAYMENT_ORDER_DATE")
    private String paymentOrderDate;

    @ApiModelProperty(value = "类别")
    @TableField("TYPE")
    private Integer type;

    @ApiModelProperty(value = "本次付款金额")
    @TableField("INVOICE_AMOUNT_OF_THIS_PAYMENT")
    private BigDecimal invoiceAmountOfThisPayment;

    @ApiModelProperty(value = "申请状态")
    @TableField("APPLICATION_STATUS")
    private Integer applicationStatus;

    @ApiModelProperty(value = "审批状态")
    @TableField("APPROVAL_STATUS")
    private Integer approvalStatus;

    @ApiModelProperty(value = "工作流编码")
    @TableField("APPROVAL_FLOW_NO")
    private String approvalFlowNo;

    @ApiModelProperty(value = "付款单号")
    @TableField("PAYMENT_ORDER_NO")
    private String paymentOrderNo;

    @ApiModelProperty(value = "备注")
    @TableField("GPA_REMARKS")
    private String gpaRemarks;

    @ApiModelProperty(value = "审批完成日期")
    @TableField("GPA_APPROVAL_DATE")
    private String gpaApprovalDate;
}
