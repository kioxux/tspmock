package com.gov.operate.controller.Recharge;

import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.recharge.RechargeChangeInData;
import com.gov.operate.service.ISdRechargeChangeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;


@Api(tags = "储值卡金额异动")
@Controller
@RequestMapping({"/rechargeChange"})
public class RechargeChangeController {

    @Autowired
    private ISdRechargeChangeService rechargeChangeService;

    @PostMapping("/rechargeChangePage")
    @ApiOperation(value = "储值卡充值消费明细分页查询")
    @ResponseBody
    public Result rechargeChangePage(@Valid @RequestBody RechargeChangeInData inData) {
        return ResultUtil.success(rechargeChangeService.rechargeChangePage(inData));
    }

    @PostMapping("/export")
    @ApiOperation(value = "导出消费明细查询")
    public Result export(@Valid @RequestBody RechargeChangeInData inData, HttpServletResponse response) throws IOException {
        rechargeChangeService.export(inData,response);
        return ResultUtil.success("提示：获取数据成功！");
    }
}
