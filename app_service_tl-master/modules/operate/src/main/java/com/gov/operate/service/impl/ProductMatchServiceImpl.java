package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.SnowflakeIdUtil;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.utils.CommonUtil;
import com.gov.operate.dto.ProductMatchCheckManConfirmVO;
import com.gov.operate.dto.ProductMatchConfirmInData;
import com.gov.operate.dto.ProductMatchConfirmOtherInData;
import com.gov.operate.dto.ProductMatchUserCheckRes;
import com.gov.operate.entity.*;
import com.gov.operate.feign.ReportFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.MatchProductInData;
import com.gov.operate.feign.dto.MatchSchedule;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IProductMatchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.service.IWmsTupianzdrwcjService;
import com.gov.redis.jedis.RedisClient;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 导入商品匹配信息表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
@Service
public class ProductMatchServiceImpl extends ServiceImpl<ProductMatchMapper, ProductMatch> implements IProductMatchService {

    @Resource
    private CommonService commonService;
    @Autowired
    private ProductMatchZMapper productMatchZMapper;
    @Autowired
    private ProductMatchMapper productMatchMapper;
    @Autowired
    private ReportFeign reportFeign;
    @Autowired
    private ProductBasicMapper productBasicMapper;
    @Autowired
    private FranchiseeMapper franchiseeMapper;
    @Autowired
    private StoreDataMapper storeDataMapper;
    @Autowired
    private SystemParaMapper systemParaMapper;
    @Autowired
    private ProductCheckKucunImportMapper productCheckKucunImportMapper;

    @Autowired
    private IWmsTupianzdrwcjService iWmsTupianzdrwcjService;
    @Autowired
    private ProductMatchConfirmLogMapper productMatchConfirmLogMapper;

    @Autowired
    private ProductMatchZResultMapper productMatchZResultMapper;

    @Resource
    private RedisClient redisClient;

    @Autowired
    private UserDataMapper userDataMapper;

    private List<SystemPara> getSystemParaByCode(String code) {
        QueryWrapper<SystemPara> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CODE", code);
        return systemParaMapper.selectList(queryWrapper);
    }


    private List<String> getCountScOrZlPerson(String client, String stoId, String tel) {
        List<String> res = new ArrayList<>();
        res = productMatchMapper.getCountScOrZlPerson(client, stoId, tel);
        return res;
    }

    private ProductMatchConfirmLog getProductMatchConfirmLogByTelAndClient(String tel, String site, String client, String type, String date) {
        QueryWrapper<ProductMatchConfirmLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
//        if (StrUtil.isNotBlank(site)) {
        queryWrapper.eq("STO_CODE", site);
//        }
        queryWrapper.eq("TEL", tel);
        queryWrapper.eq("TYPE", type);
        if (StrUtil.isNotBlank(date)) {
            queryWrapper.eq("CRE_DATE", date);
        }
//        Map<String,Object> queryMap = new HashMap<>();
//        queryMap.put("CLIENT",client);
//        if(StrUtil.isNotBlank(site)){
//            queryMap.put("STO_CODE",site);
//        }
//        queryMap.put("TEL",tel);
        List<ProductMatchConfirmLog> productMatchConfirmLogs = productMatchConfirmLogMapper.selectList(queryWrapper);
        if(CollectionUtil.isNotEmpty(productMatchConfirmLogs)){
            return productMatchConfirmLogs.get(0);
        }else{
            return null;
        }
    }


    private List<ProductMatchConfirmLog> getProductMatchConfirmLogByTelAndClientStos(String tel, List<String> sites, String client, String type, String date) {
        QueryWrapper<ProductMatchConfirmLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        if (CollectionUtil.isNotEmpty(sites)) {
            queryWrapper.in("STO_CODE", sites);
        }
        queryWrapper.eq("TEL", tel);
        queryWrapper.eq("TYPE", type);
        if (StrUtil.isNotBlank(date)) {
            queryWrapper.eq("CRE_DATE", date);
        }
//        Map<String,Object> queryMap = new HashMap<>();
//        queryMap.put("CLIENT",client);
//        if(StrUtil.isNotBlank(site)){
//            queryMap.put("STO_CODE",site);
//        }
//        queryMap.put("TEL",tel);
        List<ProductMatchConfirmLog> productMatchConfirmLogs = productMatchConfirmLogMapper.selectList(queryWrapper);
        return productMatchConfirmLogs;
    }

    //
    private UserData getUserDataByTelNum(String tel) {
        UserData res = new UserData();
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("USER_TEL", tel);
        List<UserData> userDataList = userDataMapper.selectByMap(queryMap);
        if (CollectionUtil.isNotEmpty(userDataList)) {
            res = userDataList.get(0);
        }
        return res;
    }


    @Override
    public Map<String, Object> getDataStaticsByTelNum(String telNum) {
        Map<String, Object> resMap = new HashMap<>();
        List<SystemPara> productCheckTimes = getSystemParaByCode("PRODUCT_CHECK_TIME");
        String productCheckTime = "";
        if (CollectionUtil.isNotEmpty(productCheckTimes)) {
            productCheckTime = productCheckTimes.get(0).getModel();
        }
        //处理数据核验人员需要查看的消息-------开始
        List<ProductMatchCheckManConfirmVO> checkManMessages = new ArrayList<>();
//        TokenUser user = new TokenUser();
//        user.setClient("10000003");
        TokenUser user = commonService.getLoginInfo();
        if (user == null) {
            throw new CustomResultException("请重新登陆");
        }
        if (StrUtil.isBlank(telNum)) {
            throw new CustomResultException("缺失参数用户手机号");
        }
        Map<String, Object> queryCheckManMap = new HashMap<>();
        queryCheckManMap.put("MATCH_DATA_CHECK_TOUCH_TEL", telNum);
        queryCheckManMap.put("MATCH_DATA_CHECK_FLAG", "3");
//        queryCheckManMap.put("MATCH_SECOND_PUSH_FLAG", "1");
        queryCheckManMap.put("CLIENT", user.getClient());
        List<ProductMatchZ> productMatchZList = productMatchZMapper.selectByMap(queryCheckManMap);
        productMatchZList = productMatchZList.stream().filter(x -> (StrUtil.isBlank(x.getMatchSecondPushFlag()) || x.getMatchSecondPushFlag().equals("1"))).collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(productMatchZList)) {
            //数据库中存在两种情况，需要区分开进行处理,一种是供应商级别的处理，一种是门店级别的
            List<ProductMatchZ> clientTypeList = productMatchZList.stream().filter(x -> x.getMatchType().equals("0")).collect(Collectors.toList());
            List<ProductMatchZ> stoTypeList = productMatchZList.stream().filter(x -> x.getMatchType().equals("1")).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(clientTypeList)) {
                ProductMatchCheckManConfirmVO clientRes = handleClientImportMessage(clientTypeList);
                clientRes.setProductCheckTime(productCheckTime);
                checkManMessages.add(clientRes);
            }
            if (CollectionUtil.isNotEmpty(stoTypeList)) {
                List<ProductMatchCheckManConfirmVO> stoResList = handleStoImportMessage(stoTypeList);
                String finalProductCheckTime = productCheckTime;
                stoResList.forEach(x -> x.setProductCheckTime(finalProductCheckTime));
                checkManMessages.addAll(stoResList);
            }
        }
        resMap.put("checkManMessages", checkManMessages);

//        处理数据核验人员还需要看到数据处理返回结果
        List<ProductMatchZResult> checkResultMessage = new ArrayList<>();
        Map<String, Object> checkResultMap = new HashMap<>();
        checkResultMap.put("MATCH_DATA_CHECK_TOUCH_TEL", telNum);
        checkResultMap.put("MATCH_DATA_CHECK_FLAG", "1");
        checkResultMap.put("CLIENT", user.getClient());
        List<ProductMatchZ> productMatchResultList = productMatchZMapper.selectByMap(checkResultMap);
        if (CollectionUtil.isNotEmpty(productMatchResultList)) {
            //数据库中存在两种情况，需要区分开进行处理,一种是供应商级别的处理，一种是门店级别的
            List<ProductMatchZ> clientTypeList = productMatchResultList.stream().filter(x -> x.getMatchType().equals("0")).collect(Collectors.toList());
            List<ProductMatchZ> stoTypeList = productMatchResultList.stream().filter(x -> x.getMatchType().equals("1")).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(clientTypeList)) {
                Map<String, Object> resultMapQuery = new HashMap<>();
                resultMapQuery.put("CHECK_TOUCH_TEL", telNum);
                resultMapQuery.put("CLIENT", user.getClient());
                resultMapQuery.put("MATCH_TYPE", "0");
                resultMapQuery.put("CHECK_FLAG", "0");
                List<ProductMatchZResult> productMatchZResults = productMatchZResultMapper.selectByMap(resultMapQuery);
                if (CollectionUtil.isNotEmpty(productMatchZResults)) {
                    for (ProductMatchZResult result : productMatchZResults) {
                        result.setClientFlag(true);
                        checkResultMessage.add(result);
                    }
                }
            }
            if (CollectionUtil.isNotEmpty(stoTypeList)) {
                for (ProductMatchZ z : stoTypeList) {
                    Map<String, Object> resultMapQuery = new HashMap<>();
                    resultMapQuery.put("CHECK_TOUCH_TEL", telNum);
                    resultMapQuery.put("CLIENT", z.getClient());
                    resultMapQuery.put("STO_CODE", z.getStoCode());
                    resultMapQuery.put("MATCH_TYPE", "1");
                    resultMapQuery.put("CHECK_FLAG", "0");
                    List<ProductMatchZResult> productMatchZResults = productMatchZResultMapper.selectByMap(resultMapQuery);
                    if (CollectionUtil.isNotEmpty(productMatchZResults)) {
                        for (ProductMatchZResult result : productMatchZResults) {
                            result.setClientFlag(false);
                            checkResultMessage.add(result);
                        }
                    }
                }

            }
        }
        resMap.put("checkResultMessage", checkResultMessage);
        //处理数据核验人员需要查看的消息-------结束

        //处理负责人需要查看的消息-------开始
        List<ProductMatchCheckManConfirmVO> notifyManagerList = new ArrayList<>();
        List<String> userSites = getCountScOrZlPerson(user.getClient(), "", telNum);
        if (userSites.size() > 0) {

            //如果是负责人，那么需要去处理信息，获取逻辑跟填写通知电话号码的类似，只是不是根据电话号码，而是根据client 和 stocode
            Map<String, Object> queryManagerMap = new HashMap<>();
            queryManagerMap.put("CLIENT", user.getClient());
            queryManagerMap.put("MATCH_DATA_CHECK_FLAG", "1");
            List<ProductMatchZ> productMatchZManagerList = productMatchZMapper.selectByMap(queryManagerMap);
            //以上拉取是对应加盟商所有的商品核验记录，要进行赛选，
            if (CollectionUtil.isNotEmpty(productMatchZManagerList)) {
                //数据库中存在两种情况，需要区分开进行处理,一种是供应商级别的处理，一种是门店级别的
                List<ProductMatchZ> clientTypeList = productMatchZManagerList.stream().filter(x -> x.getMatchType().equals("0")).collect(Collectors.toList());
                if (CollectionUtil.isNotEmpty(clientTypeList)) {
                    ProductMatchConfirmLog productMatchConfirmLog = getProductMatchConfirmLogByTelAndClient(telNum, "", user.getClient(), "1", "");
                    if (productMatchConfirmLog == null) {
                        ProductMatchCheckManConfirmVO clientRes = handleClientImportMessage(clientTypeList);
                        clientRes.setProductCheckTime(productCheckTime);
                        UserData checkManData = getUserDataByTelNum(clientTypeList.get(0).getMatchDataCheckTouchTel());
                        if (checkManData != null) {
                            clientRes.setCheckManTel(checkManData.getUserTel());
                            clientRes.setCheckManName(checkManData.getUserNam());
                        }
                        notifyManagerList.add(clientRes);
                    }
                }
                List<ProductMatchZ> stoTypeList = productMatchZManagerList.stream().filter(x -> x.getMatchType().equals("1")).collect(Collectors.toList());
                if (CollectionUtil.isNotEmpty(stoTypeList)) {
                    //过滤已经点击过的
                    List<String> sites = stoTypeList.stream().map(x -> x.getStoCode()).collect(Collectors.toList());
                    List<ProductMatchConfirmLog> productMatchConfirmLogs = getProductMatchConfirmLogByTelAndClientStos(telNum, sites, user.getClient(), "1", "");
                    Map<String, String> productMatchConfirmLogMap = new HashMap<>();
                    if (CollectionUtil.isNotEmpty(productMatchConfirmLogs)) {
                        for(ProductMatchConfirmLog log: productMatchConfirmLogs){
                            productMatchConfirmLogMap.put(log.getStoCode(),log.getStoCode());
                        }
//                        productMatchConfirmLogMap = productMatchConfirmLogs.stream().collect(Collectors.toMap(ProductMatchConfirmLog::getStoCode, ProductMatchConfirmLog::getStoCode));
                    }
                    Map<String, String> finalProductMatchConfirmLogMap = productMatchConfirmLogMap;
                    stoTypeList = stoTypeList.stream().filter(x -> finalProductMatchConfirmLogMap.get(x.getStoCode()) == null).collect(Collectors.toList());
                    List<ProductMatchCheckManConfirmVO> stoResList = handleStoImportMessage(stoTypeList);
                    if (CollectionUtil.isNotEmpty(stoResList)) {
                        String finalProductCheckTime = productCheckTime;
                        stoResList.forEach(x -> x.setProductCheckTime(finalProductCheckTime));
                        notifyManagerList.addAll(stoResList);
                    }
                }
                resMap.put("notifyManagerMessages", notifyManagerList);

            }
        } else {
            //表示登陆用户不是商采负责人或者是质量负责人
            resMap.put("notifyManagerMessages", notifyManagerList);
        }
        //处理负责人需要查看的消息-------结束
        //处理店员进入时需要查看信息------开始
        List<ProductMatchUserCheckRes> notifyManMessage = new ArrayList<>();
        List<String> stoUserSites = productMatchMapper.getStoUserSites(user.getClient(), "", telNum);
        if (CollectionUtil.isNotEmpty(stoUserSites)) {
            //该用户具备多家店的权限
            for (String site : stoUserSites) {
                if (StrUtil.isBlank(redisClient.get("stoCheckOutDate:" + user.getClient() + "_" + site))) {
                    //表示没有权限或者已经失效，超过任务截止时间，无需再次查看
                    resMap.put("notifyManMessage", notifyManMessage);
                } else {
                    //需要过滤每天已经看过的用户
                    ProductMatchConfirmLog productMatchConfirmLog = getProductMatchConfirmLogByTelAndClient(telNum, site, user.getClient(), "2", DateUtils.getCurrentDateStrYYMMDD());
                    if (productMatchConfirmLog == null) {
                        String resStr = redisClient.get("stoCheckOutDate:" + user.getClient() + "_" + site);
                        Gson g = new Gson();
                        ProductMatchUserCheckRes productMatchUserCheckRes = g.fromJson(resStr, ProductMatchUserCheckRes.class);
                        productMatchUserCheckRes.setClientFlag(false);
                        productMatchUserCheckRes.setStoCode(site);
                        notifyManMessage.add(productMatchUserCheckRes);
                    }
                }
            }
        }
        resMap.put("notifyManMessage", notifyManMessage);
        //处理店员进入时需要查看信息------结束
        return resMap;
    }

    @Override
    public void confirm(ProductMatchConfirmInData inData) {
        //此处需要前端配合，根据前端回传的标记clientFlag 来确定处理逻辑
        TokenUser user = commonService.getLoginInfo();
        if (user == null) {
            throw new CustomResultException("请重新登陆");
        }
        if (StrUtil.isBlank(inData.getTelNum())) {
            throw new CustomResultException("缺失参数用户手机号");
        }
        if (StrUtil.isBlank(inData.getYesOrNo())) {
            throw new CustomResultException("用户必须确定或者取消");
        }
        if (inData.getClientFlag()) {
            //表示供应商
            Map<String, Object> queryMap = new HashMap<>();
            queryMap.put("MATCH_DATA_CHECK_TOUCH_TEL", inData.getTelNum());
            queryMap.put("CLIENT", inData.getClientId());
            List<ProductMatchZ> productMatchZList = productMatchZMapper.selectByMap(queryMap);
            if (CollectionUtil.isNotEmpty(productMatchZList)) {
                List<ProductMatchZ> productMatchZList1 = productMatchZList.stream().filter(x -> x.getMatchType().equals("0")).collect(Collectors.toList());
                if (CollectionUtil.isNotEmpty(productMatchZList1)) {
                    confirmAndMakeTasks(productMatchZList1, inData.getEndTime());
                    for (ProductMatchZ x : productMatchZList1) {
                        x.setMatchDataCheckFlag("Y".equals(inData.getYesOrNo()) ? "1" : "0");
                        x.setMatchSecondPushFlag("N".endsWith(inData.getYesOrNo()) ? "1" : "0");
                        if (x.getMatchDataCheckTime() == null) {
                            //初始化记录之后不再进行更新
                            x.setMatchDataCheckTime(LocalDateTime.now());
                        }
                        x.setMatchScFlag(inData.getScFlag() ? "1" : "0");
                        x.setMatchZlFlag(inData.getZlFlag() ? "1" : "0");
                        if (StrUtil.isBlank(x.getMatchDataCheckEndDate())) {
                            //初始化记录之后不再进行更新
                            x.setMatchDataCheckEndDate(inData.getEndTime());
                        }
                        if ("N".equals(inData.getYesOrNo())) {
                            x.setMatchCancelCount(x.getMatchCancelCount() == null ? 1 : x.getMatchCancelCount() + 1);
                        }
                        if (x.getMatchCancelCount() == null || x.getMatchCancelCount() == 1) {
                            x.setMatchSecondPushFlag("1");
                        } else if (x.getMatchCancelCount() == 2) {
                            x.setMatchSecondPushFlag("0");//满两次不能再次推送
                        }
                        LambdaQueryWrapper<ProductMatchZ> updateWrapper = new LambdaQueryWrapper();
                        updateWrapper.eq(ProductMatchZ::getClient, x.getClient());
                        updateWrapper.eq(ProductMatchZ::getMatchCode, x.getMatchCode());
//                        updateWrapper.set("CLIENT",x.getClient());
//                        updateWrapper.set("MATCH_CODE",x.getMatchCode());
//                        updateWrapper.set("STO_CODE",null);
                        productMatchZMapper.update(x, updateWrapper);
                    }
                }
            }

        } else if (!inData.getClientFlag()) {
            //表示单家门店,此处逻辑调整为分给对应店，并且业务方不进行多家门店导入
            Map<String, Object> queryMap = new HashMap<>();
            queryMap.put("MATCH_DATA_CHECK_TOUCH_TEL", inData.getTelNum());
            queryMap.put("CLIENT", inData.getClientId());
            queryMap.put("STO_CODE", inData.getStoCode());
            List<ProductMatchZ> productMatchZList = productMatchZMapper.selectByMap(queryMap);
            if (CollectionUtil.isNotEmpty(productMatchZList)) {
                List<ProductMatchZ> productMatchZList1 = productMatchZList.stream().filter(x -> x.getMatchType().equals("1")).collect(Collectors.toList());
                if (CollectionUtil.isNotEmpty(productMatchZList1)) {
                    productMatchZList1.forEach(x -> {
                        List<ProductMatch> dbList = productMatchMapper.selectIleaglCaseData(x.getClient(), x.getStoCode(), x.getMatchCreateDate(),x.getMatchCode());
//                        Map<String, ProCodeKuCunInfo> proCodeExtendsInfoMap = new HashMap<>();
//                        String resStr = redisClient.get("ProImport" + "_" + x.getClient());
//                        if (StrUtil.isNotBlank(resStr)) {
//                            Gson g = new Gson();
//                            proCodeExtendsInfoMap = g.fromJson(resStr, new TypeToken<Map<String, ProCodeKuCunInfo>>() {
//                            }.getType());
//                        }
                        handleStoTaskToDb(dbList, x.getStoCode(),null, inData.getEndTime());
                        x.setMatchDataCheckFlag("Y".equals(inData.getYesOrNo()) ? "1" : "0");
                        x.setMatchSecondPushFlag("N".endsWith(inData.getYesOrNo()) ? "1" : "0");
                        if (x.getMatchDataCheckTime() == null) {
                            x.setMatchDataCheckTime(LocalDateTime.now());
                        }
                        x.setMatchScFlag(inData.getScFlag() ? "1" : "0");
                        x.setMatchZlFlag(inData.getZlFlag() ? "1" : "0");
                        if (StrUtil.isBlank(x.getMatchDataCheckEndDate())) {
                            x.setMatchDataCheckEndDate(inData.getEndTime());
                        }
                        if ("N".equals(inData.getYesOrNo())) {
                            x.setMatchCancelCount(x.getMatchCancelCount() == null ? 1 : x.getMatchCancelCount() + 1);
                        }
                        if (x.getMatchCancelCount() == null || x.getMatchCancelCount() == 1) {
                            x.setMatchSecondPushFlag("1");
                        } else if (x.getMatchCancelCount() == 2) {
                            x.setMatchSecondPushFlag("0");//满两次不能再次推送
                        }
                        LambdaQueryWrapper<ProductMatchZ> updateWrapper = new LambdaQueryWrapper();
                        updateWrapper.eq(ProductMatchZ::getClient, x.getClient());
                        updateWrapper.eq(ProductMatchZ::getStoCode, x.getStoCode());
                        updateWrapper.eq(ProductMatchZ::getMatchCode, x.getMatchCode());
                        productMatchZMapper.update(x, updateWrapper);
                    });
                }
            }
        }

    }

    @Override
    public void confirmOthers(ProductMatchConfirmOtherInData inData) {
        if (StrUtil.isBlank(inData.getType())) {
            throw new CustomResultException("type必传");
        }

        TokenUser user = commonService.getLoginInfo();
        if (user == null) {
            throw new CustomResultException("请重新登陆");
        }
        if ("1".equals(inData.getType()) || "2".equals(inData.getType())) {
            ProductMatchConfirmLog productMatchConfirmLog = new ProductMatchConfirmLog();
            productMatchConfirmLog.setClient(user.getClient());
            productMatchConfirmLog.setStoCode(inData.getStoCode());
            productMatchConfirmLog.setTel(inData.getTelNum());
            productMatchConfirmLog.setType(inData.getType());
            productMatchConfirmLog.setCreDate(CommonUtil.getyyyyMMdd());
            productMatchConfirmLogMapper.insert(productMatchConfirmLog);
        } else if ("3".equals(inData.getType())) {
            QueryWrapper<ProductMatchZResult> queryWrapper = new QueryWrapper();
            queryWrapper.eq("CLIENT", user.getClient());
            queryWrapper.eq("STO_CODE", inData.getStoCode());
            queryWrapper.eq("CHECK_TOUCH_TEL", inData.getTelNum());
            ProductMatchZResult productMatchZResult = productMatchZResultMapper.selectOne(queryWrapper);
            productMatchZResult.setCheckFlag("1");
            productMatchZResultMapper.update(productMatchZResult, queryWrapper);
        }
    }

    //商品核验人点击确定之后，开始进行任务分配，经确定，将数据进行汇总之后插入外包对应的数据库中
    private void confirmAndMakeTasks(List<ProductMatchZ> productMatchZs, String endDate) {
        if (CollectionUtil.isNotEmpty(productMatchZs)) {
            for (ProductMatchZ productMatchZ : productMatchZs) {
                if (StrUtil.isBlank(productMatchZ.getStoCode())) {
                    //处理供应商级别的任务分配，此时需要将供应商下的所有门店拉取出来。然后进行统一的分配
                    List<StoreData> allStores = getAllStoreDataByClient(productMatchZ.getClient());
                    //首先需要根据导入的库存表计算出，几个计算指标，第一个指标，首先确认出一个商品存在哪几个门店有库存，第二确认商品在那家库存是最大的
//                    List<ProductCheckKucunImport> importKuCunInfo = getImportKuCunInfo(productMatchZ.getClient(), productMatchZ.getStoCode());
                    Map<String, ProCodeKuCunInfo> proCodeExtendsInfoMap = new HashMap<>();
                    String resStr = redisClient.get("ProImport" + "_" + productMatchZ.getClient());
                    if (StrUtil.isNotBlank(resStr)) {
                        Gson g = new Gson();
                        proCodeExtendsInfoMap = g.fromJson(resStr, new TypeToken<Map<String, ProCodeKuCunInfo>>() {
                        }.getType());
                        if (CollectionUtil.isNotEmpty(proCodeExtendsInfoMap)) {
                            ////查询出所有的明细信息
                            List<ProductMatch> dbList = productMatchMapper.selectIleaglCaseData(productMatchZ.getClient(), "", productMatchZ.getMatchCreateDate(),productMatchZ.getMatchCode());
                            handleClientTaskToDb(dbList, allStores.stream().map(x -> x.getStoCode()).collect(Collectors.toList()), proCodeExtendsInfoMap, endDate);
                        }
                    }
                } else {
                    String stoCode = productMatchZ.getStoCode();
                    //处理门店级别的任务分配，此时只需要将明细全部查询出来，然后塞入到对应的表中即可
                    ////查询出所有的明细信息
                    Map<String, ProCodeKuCunInfo> proCodeExtendsInfoMap = new HashMap<>();
                    String resStr = redisClient.get("ProImport" + "_" + productMatchZ.getClient());
                    if (StrUtil.isNotBlank(resStr)) {
                        Gson g = new Gson();
                        proCodeExtendsInfoMap = g.fromJson(resStr, new TypeToken<Map<String, ProCodeKuCunInfo>>() {
                        }.getType());
                        if (CollectionUtil.isNotEmpty(proCodeExtendsInfoMap)) {
                            List<ProductMatch> dbList = productMatchMapper.selectIleaglCaseData(productMatchZ.getClient(), stoCode, productMatchZ.getMatchCreateDate(),productMatchZ.getMatchCode());
                            handleStoTaskToDb(dbList, stoCode, proCodeExtendsInfoMap, endDate);
                        }
                    }

                }
            }
        }
    }
    //将所有的匹配明细分配到单家店
    private void handleStoTaskToDb(List<ProductMatch> dbList, String stoCode, Map<String, ProCodeKuCunInfo> proCodeExtendsInfoMap, String endDate) {
        List<SystemPara> productCheckTimes = getSystemParaByCode("PRODUCT_CHECK_TIME");
        String productCheckTime = "";
        if (CollectionUtil.isNotEmpty(productCheckTimes)) {
            productCheckTime = productCheckTimes.get(0).getModel();
        }
        TokenUser user = commonService.getLoginInfo();
        List<WmsTupianzdrwcj> insertDbList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(dbList)) {
            int i = 1;
            for (ProductMatch productMatch : dbList) {
                WmsTupianzdrwcj wmsTupianzdrwcj = new WmsTupianzdrwcj();
                wmsTupianzdrwcj.setClient(productMatch.getClient());
                wmsTupianzdrwcj.setProSite(stoCode);
                wmsTupianzdrwcj.setZdrwcjBh(productMatch.getMatchCode());
                wmsTupianzdrwcj.setWmXh(i);
                wmsTupianzdrwcj.setWmSpBm(productMatch.getProCode());
                wmsTupianzdrwcj.setWmSpZkbm(productMatch.getMatchProCode());
                wmsTupianzdrwcj.setWmZt("1");
                wmsTupianzdrwcj.setWmClient("0");
                wmsTupianzdrwcj.setWmCjr(user.getUserId());
                wmsTupianzdrwcj.setWmCjrq(CommonUtil.getyyyyMMdd());
                wmsTupianzdrwcj.setWmCjsj(CommonUtil.getHHmmss());
                wmsTupianzdrwcj.setId(SnowflakeIdUtil.getNextId());
                wmsTupianzdrwcj.setWmInvalidDate(endDate);
                wmsTupianzdrwcj.setWmInvalidFlag("1");
                i++;
                insertDbList.add(wmsTupianzdrwcj);
            }
            ProductMatchUserCheckRes productMatchUserCheckRes = new ProductMatchUserCheckRes();
            productMatchUserCheckRes.setCheckProNum(insertDbList.size());
            productMatchUserCheckRes.setEndDate(endDate);
            //将弹给单个用户的时间存储在redis中
            if (StrUtil.isBlank(redisClient.get("stoCheckOutDate:" + user.getClient() + "_" + stoCode))) {
                redisClient.set("stoCheckOutDate:" + user.getClient() + "_" + stoCode, new Gson().toJson(productMatchUserCheckRes), Integer.parseInt(productCheckTime) * 24 * 60 * 60);
            }
        }
        //由于存在多次导入的情况，所以任务需要进行判断，已经存在的商品不应该再进行二次的插入
        List<WmsTupianzdrwcj> wmsTupianzdrwcjsDbList = iWmsTupianzdrwcjService.selectCjResult(dbList.get(0).getClient(), stoCode, dbList.get(0).getMatchCode());
        Map<String, String> existProCodeMap = wmsTupianzdrwcjsDbList.stream().collect(Collectors.toMap(WmsTupianzdrwcj::getWmSpBm, WmsTupianzdrwcj::getWmSpBm));
        insertDbList = insertDbList.stream().filter(x -> existProCodeMap.get(x.getWmSpBm()) == null).collect(Collectors.toList());
        //处理序列号，查询之前已经存储的最大序列号，进行序列号的累加
        List<WmsTupianzdrwcj> orderList = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(insertDbList)){
            Integer count = 1;
            for(WmsTupianzdrwcj cj: insertDbList){
                cj.setWmXh(count);
                orderList.add(cj);
                count++;
            }
        }
        insertDbList = orderList;
        Integer maxWmXh = iWmsTupianzdrwcjService.getMaxWmXhByMatchCode(dbList.get(0).getMatchCode());
        if(maxWmXh!=null){
            insertDbList.forEach(x->{
                x.setWmXh(maxWmXh + x.getWmXh());
            });
        }
        if(CollectionUtil.isNotEmpty(insertDbList)){
            iWmsTupianzdrwcjService.saveBatch(insertDbList);
        }
    }

//    //将所有的匹配明细分配到单家店
//    private void handleStoTaskToDb(List<ProductMatch> dbList, String stoCode, Map<String, ProCodeKuCunInfo> proCodeExtendsInfoMap, String endDate) {
//        Map<String, List<ProductMatchCheckManConfirmVO>> resMap = new HashMap<>();
//        List<SystemPara> productCheckTimes = getSystemParaByCode("PRODUCT_CHECK_TIME");
//        String productCheckTime = "";
//        if (CollectionUtil.isNotEmpty(productCheckTimes)) {
//            productCheckTime = productCheckTimes.get(0).getModel();
//        }
//        //此处需要更改逻辑，更换为商品分配给库存最大的店
//        Map<String, StoProInfo> stoProInfoMap = new HashMap<>();
//        TokenUser user = commonService.getLoginInfo();
//        if (CollectionUtil.isNotEmpty(dbList)) {
//            for (ProductMatch productMatch : dbList) {
//                String proCode = productMatch.getProCode();//用户自编码
//                ProCodeKuCunInfo proCodeKuCunInfo = proCodeExtendsInfoMap.get(proCode);
//                if (proCodeKuCunInfo != null) {
//                    //只有商品库存不是空的情况下才需要处理
//                    if (stoProInfoMap.get(proCodeKuCunInfo.getMaxNumSite()) == null) {
//                        StoProInfo stoProInfo = new StoProInfo();
//                        stoProInfo.setSite(proCodeKuCunInfo.getMaxNumSite());
//                        List<ProductMatch> productMatchList = new ArrayList<>();
//                        productMatchList.add(productMatch);
//                        stoProInfo.setProductMatchList(productMatchList);
//
//                        //此处更改为匹配不到的商品直接过滤
//                        stoProInfoMap.put(proCodeKuCunInfo.getMaxNumSite(), stoProInfo);
//                    } else {
//                        StoProInfo stoProInfo = stoProInfoMap.get(proCodeKuCunInfo.getMaxNumSite());
//                        List<ProductMatch> productMatchList = stoProInfo.getProductMatchList();
//                        productMatchList.add(productMatch);
//                        stoProInfo.setProductMatchList(productMatchList);
//                        stoProInfoMap.put(proCodeKuCunInfo.getMaxNumSite(), stoProInfo);
//                    }
//                }
//
//            }
//        }
//        if (CollectionUtil.isNotEmpty(stoProInfoMap)) {
//            for (String siteCode : stoProInfoMap.keySet()) {
//                List<WmsTupianzdrwcj> insertDbList = new ArrayList<>();
//                StoProInfo stoProInfo = stoProInfoMap.get(stoCode);
//                List<ProductMatch> productMatchList = stoProInfo.getProductMatchList();
//                if (CollectionUtil.isNotEmpty(productMatchList)) {
//                    int i = 1;
//                    for (ProductMatch productMatch : dbList) {
//                        WmsTupianzdrwcj wmsTupianzdrwcj = new WmsTupianzdrwcj();
//                        wmsTupianzdrwcj.setClient(productMatch.getClient());
//                        wmsTupianzdrwcj.setProSite(siteCode);
//                        wmsTupianzdrwcj.setZdrwcjBh(productMatch.getMatchCode());
//                        wmsTupianzdrwcj.setWmXh(i);
//                        wmsTupianzdrwcj.setWmSpBm(productMatch.getProCode());
//                        wmsTupianzdrwcj.setWmSpZkbm(productMatch.getMatchProCode());
//                        wmsTupianzdrwcj.setWmZt("1");
//                        wmsTupianzdrwcj.setWmCjr(user.getUserId());
//                        wmsTupianzdrwcj.setWmCjrq(CommonUtil.getyyyyMMdd());
//                        wmsTupianzdrwcj.setWmCjsj(CommonUtil.getHHmmss());
//                        wmsTupianzdrwcj.setId(SnowflakeIdUtil.getNextId());
//                        i++;
//                        insertDbList.add(wmsTupianzdrwcj);
//                    }
//                    ProductMatchUserCheckRes productMatchUserCheckRes = new ProductMatchUserCheckRes();
//                    productMatchUserCheckRes.setCheckProNum(insertDbList.size());
//                    productMatchUserCheckRes.setEndDate(endDate);
//                    //将弹给单个用户的时间存储在redis中
//                    if (StrUtil.isBlank(redisClient.get("stoCheckOutDate:" + user.getClient() + "_" + stoCode))) {
//                        redisClient.set("stoCheckOutDate:" + user.getClient() + "_" + stoCode, new Gson().toJson(productMatchUserCheckRes), Integer.parseInt(productCheckTime) * 24 * 60 * 60);
//                    }
//                }
//                //由于存在多次导入的情况，所以任务需要进行判断，已经存在的商品不应该再进行二次的插入
//                List<WmsTupianzdrwcj> wmsTupianzdrwcjsDbList = iWmsTupianzdrwcjService.selectCjResult(dbList.get(0).getClient(), stoCode, dbList.get(0).getMatchCode());
//                Map<String, String> existProCodeMap = wmsTupianzdrwcjsDbList.stream().collect(Collectors.toMap(WmsTupianzdrwcj::getWmSpBm, WmsTupianzdrwcj::getWmSpBm));
//                insertDbList = insertDbList.stream().filter(x -> existProCodeMap.get(x.getWmSpBm()) == null).collect(Collectors.toList());
//                iWmsTupianzdrwcjService.saveBatch(insertDbList);
//            }
//        }
//    }


    //将所有的匹配明细分配到不同的店去
    private void handleClientTaskToDb(List<ProductMatch> dbList, List<String> sites, Map<String, ProCodeKuCunInfo> proCodeExtendsInfoMap, String endDate) {
        if (CollectionUtil.isEmpty(sites)) {
            throw new CustomResultException("无法进行任务分配，不存在门店");
        }
        Map<String, List<ProductMatchCheckManConfirmVO>> resMap = new HashMap<>();
        List<SystemPara> productCheckTimes = getSystemParaByCode("PRODUCT_CHECK_TIME");
        String productCheckTime = "";
        if (CollectionUtil.isNotEmpty(productCheckTimes)) {
            productCheckTime = productCheckTimes.get(0).getModel();
        }
        Map<String, StoProInfo> stoProInfoMap = new HashMap<>();
        TokenUser user = commonService.getLoginInfo();
        if (CollectionUtil.isNotEmpty(dbList)) {
            for (ProductMatch productMatch : dbList) {
                String proCode = productMatch.getProCode();//用户自编码
                String matchProCode = productMatch.getMatchProCode();//药德商品编码
                ProCodeKuCunInfo proCodeKuCunInfo = proCodeExtendsInfoMap.get(proCode);
                if (proCodeKuCunInfo != null) {
                    //只有商品库存不是空的情况下才需要处理
                    if (stoProInfoMap.get(proCodeKuCunInfo.getMaxNumSite()) == null) {
                        StoProInfo stoProInfo = new StoProInfo();
                        stoProInfo.setSite(proCodeKuCunInfo.getMaxNumSite());
                        List<ProductMatch> productMatchList = new ArrayList<>();
                        productMatchList.add(productMatch);
                        stoProInfo.setProductMatchList(productMatchList);
//                    if (proCodeKuCunInfo == null) {
//                        stoProInfoMap.put(sites.get(0), stoProInfo);
//                    } else {
//                        stoProInfoMap.put(proCodeKuCunInfo.getMaxNumSite(), stoProInfo);
//                    }
                        //此处更改为匹配不到的商品直接过滤
                        stoProInfoMap.put(proCodeKuCunInfo.getMaxNumSite(), stoProInfo);
                    } else {
                        StoProInfo stoProInfo = stoProInfoMap.get(proCodeKuCunInfo.getMaxNumSite());
                        List<ProductMatch> productMatchList = stoProInfo.getProductMatchList();
                        productMatchList.add(productMatch);
                        stoProInfo.setProductMatchList(productMatchList);
                        stoProInfoMap.put(proCodeKuCunInfo.getMaxNumSite(), stoProInfo);
                    }
                }

            }
        }

        if (CollectionUtil.isNotEmpty(stoProInfoMap)) {
//            List<WmsTupianzdrwcj> soutList = new ArrayList<>();
            for (String stoCode : stoProInfoMap.keySet()) {
                List<WmsTupianzdrwcj> insertDbList = new ArrayList<>();
                StoProInfo stoProInfo = stoProInfoMap.get(stoCode);
                List<ProductMatch> productMatchList = stoProInfo.getProductMatchList();
                if (CollectionUtil.isNotEmpty(productMatchList)) {
                    int i = 1;
                    for (ProductMatch productMatch : productMatchList) {
                        WmsTupianzdrwcj wmsTupianzdrwcj = new WmsTupianzdrwcj();
                        wmsTupianzdrwcj.setClient(productMatch.getClient());
                        wmsTupianzdrwcj.setProSite(stoProInfo.getSite());
                        wmsTupianzdrwcj.setZdrwcjBh(productMatch.getMatchCode());//存储该编号，用于后期拉取由不匹配到匹配的商品的数量
                        wmsTupianzdrwcj.setWmXh(i);
                        wmsTupianzdrwcj.setWmSpBm(productMatch.getProCode());
                        wmsTupianzdrwcj.setWmSpZkbm(productMatch.getMatchProCode());
                        wmsTupianzdrwcj.setWmZt("1");
                        wmsTupianzdrwcj.setWmClient("1");
                        wmsTupianzdrwcj.setWmCjr(user.getUserId());
                        wmsTupianzdrwcj.setWmCjrq(CommonUtil.getyyyyMMdd());
                        wmsTupianzdrwcj.setWmCjsj(CommonUtil.getHHmmss());
                        wmsTupianzdrwcj.setId(SnowflakeIdUtil.getNextId());
                        wmsTupianzdrwcj.setWmInvalidDate(endDate);//添加失效时间，用于后期让外包可以拉取未失效的数据
                        wmsTupianzdrwcj.setWmInvalidFlag("1");
                        i++;
                        insertDbList.add(wmsTupianzdrwcj);
//                        soutList.add(wmsTupianzdrwcj);
                    }
                    ProductMatchUserCheckRes productMatchUserCheckRes = new ProductMatchUserCheckRes();
                    productMatchUserCheckRes.setEndDate(endDate);
                    productMatchUserCheckRes.setCheckProNum(insertDbList.size());
                    if (StrUtil.isBlank(redisClient.get("stoCheckOutDate:" + user.getClient() + "_" + stoCode))) {
                        redisClient.set("stoCheckOutDate:" + user.getClient() + "_" + stoCode, new Gson().toJson(productMatchUserCheckRes), Integer.parseInt(productCheckTime) * 24 * 60 * 60);
                    }
                }
                //由于存在多次导入的情况，所以任务需要进行判断，已经存在的商品不应该再进行二次的插入
                List<WmsTupianzdrwcj> wmsTupianzdrwcjsDbList = iWmsTupianzdrwcjService.selectCjResult(dbList.get(0).getClient(), stoCode, dbList.get(0).getMatchCode());
                Map<String, String> existProCodeMap = wmsTupianzdrwcjsDbList.stream().collect(Collectors.toMap(WmsTupianzdrwcj::getWmSpBm, WmsTupianzdrwcj::getWmSpBm));
                insertDbList = insertDbList.stream().filter(x -> existProCodeMap.get(x.getWmSpBm()) == null).collect(Collectors.toList());
                List<WmsTupianzdrwcj> orderList = new ArrayList<>();
                if(CollectionUtil.isNotEmpty(insertDbList)){
                    Integer count = 1;
                    for(WmsTupianzdrwcj cj: insertDbList){
                        cj.setWmXh(count);
                        orderList.add(cj);
                        count++;
                    }
                }
                insertDbList = orderList;
                //处理序列号，查询之前已经存储的最大序列号，进行序列号的累加
                Integer maxWmXh = iWmsTupianzdrwcjService.getMaxWmXhByMatchCode(dbList.get(0).getMatchCode());
                if(maxWmXh!=null){
                    insertDbList.forEach(x->{
                        x.setWmXh(maxWmXh + x.getWmXh());
                    });
                }
                if(CollectionUtil.isNotEmpty(insertDbList)){
                    iWmsTupianzdrwcjService.saveBatch(insertDbList);
                }
            }
        }
    }


    private List<ProductCheckKucunImport> getImportKuCunInfo(String client, String stoCode) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("CLIENT", client);
        if (StrUtil.isNotBlank(stoCode)) {
            queryMap.put("STO_CODE", stoCode);
        }
        List<ProductCheckKucunImport> productCheckKucunImports = productCheckKucunImportMapper.selectByMap(queryMap);
        return productCheckKucunImports;
    }

    private Map<String, ProCodeKuCunInfo> makeProductCodeExtendsInfo(List<ProductCheckKucunImport> productCheckKucunImports) {
        Map<String, ProCodeKuCunInfo> resMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(productCheckKucunImports)) {
            for (ProductCheckKucunImport kucunImport : productCheckKucunImports) {
                String proCode = kucunImport.getProCode();
                if (resMap.get(proCode) != null) {
                    ProCodeKuCunInfo proCodeKuCunInfo = new ProCodeKuCunInfo();
                    List<String> sites = new ArrayList<>();
                    sites.add(kucunImport.getStoCode());
                    proCodeKuCunInfo.setProMaxNum(StrUtil.isBlank(kucunImport.getKuCunNum()) ? 0 : Integer.parseInt(kucunImport.getKuCunNum()));
                    proCodeKuCunInfo.setMaxNumSite(kucunImport.getStoCode());
                    proCodeKuCunInfo.setProCode(proCode);
                    proCodeKuCunInfo.setSites(sites);
                    resMap.put(kucunImport.getProCode(), proCodeKuCunInfo);
                } else {
                    ProCodeKuCunInfo proCodeKuCunInfo = resMap.get(proCode);
                    List<String> sites = proCodeKuCunInfo.getSites();
                    if (!sites.contains(kucunImport.getStoCode())) {
                        sites.add(kucunImport.getStoCode());
                        proCodeKuCunInfo.setSites(sites);
                    }
                    Integer importNum = StrUtil.isBlank(kucunImport.getKuCunNum()) ? 0 : Integer.parseInt(kucunImport.getKuCunNum());
                    if (importNum > proCodeKuCunInfo.getProMaxNum()) {
                        proCodeKuCunInfo.setProMaxNum(importNum);
                        proCodeKuCunInfo.setMaxNumSite(kucunImport.getStoCode());
                    }
                    proCodeKuCunInfo.setProCode(proCode);
                    resMap.put(kucunImport.getProCode(), proCodeKuCunInfo);
                }

            }
        }
        return resMap;
    }


    private Integer getStoreNum(String client) {
        Integer res = 0;
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STO_STATUS", "0");
        res = storeDataMapper.selectCount(queryWrapper);
        return res;
    }

    private StoreData getStoreData(String client, String stoCode) {
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STO_CODE", stoCode);
        queryWrapper.eq("STO_STATUS", "0");
        StoreData storeData = storeDataMapper.selectOne(queryWrapper);
        return storeData;
    }

    private List<StoreData> getAllStoreDataByClient(String client) {
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STO_STATUS", "0");
        queryWrapper.eq("STO_ATTRIBUTE", "2");
        List<StoreData> storeDataList = storeDataMapper.selectList(queryWrapper);
        return storeDataList;
    }

    private ProductMatchCheckManConfirmVO handleClientImportMessage(List<ProductMatchZ> clientTypeList) {
        ProductMatchCheckManConfirmVO res = new ProductMatchCheckManConfirmVO();
        res.setClientFlag(true);
        List<String> demos = new ArrayList<>();
        String client = "";
        BigDecimal averageCheckNum = BigDecimal.ZERO;
        if (CollectionUtil.isNotEmpty(clientTypeList)) {
            for (ProductMatchZ z : clientTypeList) {
                res.setMatchDataCheckEndDate(z.getMatchDataCheckEndDate());
                client = z.getClient();
                //首先需要查询原始数据
                MatchProductInData queryData = new MatchProductInData();
                queryData.setClientId(z.getClient());

                //此处需要更改,意思是需要汇总匹配中的实际情况
                Integer matchAllCount = productMatchMapper.selectAllMatchCount(client, "", z.getMatchCode());
                Integer unMatchCount = productMatchMapper.selectUnMatchCount(client, "", z.getMatchCode());
                Integer matchCount = productMatchMapper.selectMatchCount(client, "", z.getMatchCode());
                MatchSchedule matchSchedule = new MatchSchedule();
                matchSchedule.setTotalLine(matchAllCount);
                matchSchedule.setUnmatchLine(unMatchCount);
                matchSchedule.setMatchedLine(matchCount);
                if (matchAllCount != 0 && matchCount != null) {
                    matchSchedule.setMatchSchedule(new BigDecimal(matchCount).divide(new BigDecimal(matchAllCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).toPlainString() + "%");
                }
                res.setStatics(matchSchedule);
//                matchSchedule.setMatchedLine();
//                JsonResult result = reportFeign.getProductMatch(queryData);//获取推送的汇总数据
//                if (result != null && result.getCode() == 0) {
//                    LinkedHashMap data = ((LinkedHashMap) result.getData());
//                    MatchSchedule matchSchedule = JSON.parseObject(JSON.toJSONString(data), MatchSchedule.class);
//                    res.setStatics(matchSchedule);
//                }
                //需要处理例子信息，随机取对应的匹配信息
                List<ProductMatch> dbList = productMatchMapper.selectIleaglCaseData(z.getClient(), "", z.getMatchCreateDate(),z.getMatchCode());
                List<String> demoList = handleProIleaglData(dbList);
                demos.addAll(demoList);
                break;//运行一次即可，理论上不会出现多条
            }
        }
        if (StrUtil.isNotBlank(client)) {
            Franchisee franchisee = franchiseeMapper.selectById(client);
            res.setCompanyName(franchisee.getFrancName());
            Integer storeNum = getStoreNum(client);
            res.setStoNum(storeNum);
            if (storeNum == null || storeNum == 0) {
                averageCheckNum = new BigDecimal(res.getStatics().getUnmatchLine().toString());
            } else {
                averageCheckNum = new BigDecimal(res.getStatics().getUnmatchLine().toString()).divide(new BigDecimal(storeNum), 2, BigDecimal.ROUND_HALF_UP);
            }
            res.setAverageCheckNum(averageCheckNum.setScale(2, RoundingMode.HALF_UP).toPlainString());
        }

        res.setDemos(demos);
        return res;
    }

    private List<ProductMatchCheckManConfirmVO> handleStoImportMessage(List<ProductMatchZ> stoTypeList) {
        //多个店铺需要多个返回信息，此处需要处理
        List<ProductMatchCheckManConfirmVO> resList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(stoTypeList)) {
            for (ProductMatchZ z : stoTypeList) {
                ProductMatchCheckManConfirmVO res = new ProductMatchCheckManConfirmVO();
                UserData checkManData = getUserDataByTelNum(z.getMatchDataCheckTouchTel());
                if (checkManData != null) {
                    res.setCheckManTel(checkManData.getUserTel());
                    res.setCheckManName(checkManData.getUserNam());
                }
                res.setMatchDataCheckEndDate(z.getMatchDataCheckEndDate());
                List<String> demos = new ArrayList<>();
                //首先需要查询原始数据
                //此处需要更改,意思是需要汇总匹配中的实际情况
                Integer matchAllCount = productMatchMapper.selectAllMatchCount(z.getClient(), z.getStoCode(), z.getMatchCode());
                Integer unMatchCount = productMatchMapper.selectUnMatchCount(z.getClient(), z.getStoCode(), z.getMatchCode());
                Integer matchCount = productMatchMapper.selectMatchCount(z.getClient(), z.getStoCode(), z.getMatchCode());
                MatchSchedule matchSchedule = new MatchSchedule();
                matchSchedule.setTotalLine(matchAllCount);
                matchSchedule.setUnmatchLine(unMatchCount);
                matchSchedule.setMatchedLine(matchCount);
                if (matchAllCount != 0 && matchCount != null) {
                    matchSchedule.setMatchSchedule(new BigDecimal(matchCount).divide(new BigDecimal(matchAllCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).toPlainString() + "%");
                }
                res.setStatics(matchSchedule);
//                MatchProductInData queryData = new MatchProductInData();
//                queryData.setClientId(z.getClient());
//                queryData.setStoCodes(new String[]{z.getStoCode()});
//                JsonResult result = reportFeign.getProductMatch(queryData);
//                if (result != null && result.getCode() == 0) {
//                    LinkedHashMap data = ((LinkedHashMap) result.getData());
//                    MatchSchedule matchSchedule = JSON.parseObject(JSON.toJSONString(data), MatchSchedule.class);
//                    res.setStatics(matchSchedule);
////                    res.setStatics((MatchSchedule) result.getData());
//                }
                StoreData storeData = getStoreData(z.getClient(), z.getStoCode());
                if (storeData != null) {
                    res.setCompanyName(storeData.getStoName());
                    res.setStoCode(storeData.getStoCode());
                    res.setStoNum(1);
                    Integer unmatchLine = res.getStatics().getUnmatchLine();
                    res.setAverageCheckNum(new BigDecimal(unmatchLine).setScale(2, RoundingMode.HALF_UP).toPlainString());
                } else {
                    //查找不到对应的点，不需要发送消息，直接过滤
                    continue;
                }
                List<ProductMatch> dbList = productMatchMapper.selectIleaglCaseData(z.getClient(), z.getStoCode(), z.getMatchCreateDate(),z.getMatchCode());
                List<String> demoList = handleProIleaglData(dbList);
                demos.addAll(demoList);
                res.setDemos(demos);
                resList.add(res);
            }
        }
        return resList;
    }

    private List<String> handleProIleaglData(List<ProductMatch> details) {
        List<String> resList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(details)) {
            String res1 = handleProNoProBarcode(details);
            if (StrUtil.isNotBlank(res1)) {
                resList.add(res1);
            }
            String res2 = handleProNoProRegisterNo(details);
            if (StrUtil.isNotBlank(res2)) {
                resList.add(res2);
            }
            String res3 = handleProNoProSpecs(details);
            if (StrUtil.isNotBlank(res3)) {
                resList.add(res3);
            }
            if (CollectionUtil.isEmpty(resList)) {
                //如果以上数据均为得到结果，那么需要将系统数据与店铺数据进行对比，分析出任意一个原因即可
                String matchProCode = "";
                ProductMatch productMatch = new ProductMatch();
                for (ProductMatch de : details) {
                    if (StrUtil.isNotBlank(de.getMatchProCode()) && !"99999999".equals(de.getMatchProCode())) {
                        matchProCode = de.getMatchProCode();
                        productMatch = de;
                        break;
                    }
                }
                Map<String, Object> queryMap = new HashMap<>();
                queryMap.put("PRO_CODE", matchProCode);
                List<ProductBasic> productBasics = productBasicMapper.selectByMap(queryMap);
                if (CollectionUtil.isNotEmpty(productBasics)) {
                    ProductBasic productBasic = productBasics.get(0);
                    String res4 = productMatch.getProName() + "存在信息不匹配";
                    resList.add(res4);
//                    if(StrUtil.isNotBlank(productMatch.getProName()) && !productMatch.getProName().equals(productBasic.getProName())){
//
//                    }else if(StrUtil.isNotBlank(productMatch.getProSpecs()) && !productMatch.getProSpecs().equals(productBasic.getProSpecs())){
//                        String res4 = productMatch.getProName() + "," + "商品规格不匹配";
//                    }

                } else {
                    String res4 = "商品存在信息不匹配情况";
                    resList.add(res4);
                }
            }
        }

        return resList;
    }

    private String handleProNoProBarcode(List<ProductMatch> details) {
        String res = "";
        for (ProductMatch productMatch : details) {
            if (StrUtil.isBlank(productMatch.getProBarcode())) {
                String proName = productMatch.getProName();
                res = res + proName + "," + "无国际条码";
                break;
            }
        }
        return res;
    }

    private String handleProNoProRegisterNo(List<ProductMatch> details) {
        String res = "";
        for (ProductMatch productMatch : details) {
            if (StrUtil.isBlank(productMatch.getProRegisterNo())) {
                String proName = productMatch.getProName();
                res = res + proName + "," + "批准文号不全";
                break;
            }
        }
        return res;
    }

    private String handleProNoProSpecs(List<ProductMatch> details) {
        String res = "";
        for (ProductMatch productMatch : details) {
            if (StrUtil.isBlank(productMatch.getProSpecs())) {
                String proName = productMatch.getProName();
                res = res + proName + "," + "无规格";
                break;
            }
        }
        return res;
    }

    @Data
    class ProCodeKuCunInfo {
        private String proCode;

        private List<String> sites;

        private String maxNumSite;

        private Integer proMaxNum;
    }

    @Data
    class StoProInfo {
        private String site;

        private String siteName;

        private List<ProductMatch> productMatchList;
    }

}
