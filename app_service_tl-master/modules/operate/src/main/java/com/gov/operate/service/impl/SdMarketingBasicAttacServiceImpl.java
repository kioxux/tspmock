package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMarketingBasicAttac;
import com.gov.operate.mapper.SdMarketingBasicAttacMapper;
import com.gov.operate.service.ISdMarketingBasicAttacService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-02
 */
@Service
public class SdMarketingBasicAttacServiceImpl extends ServiceImpl<SdMarketingBasicAttacMapper, SdMarketingBasicAttac> implements ISdMarketingBasicAttacService {

}
