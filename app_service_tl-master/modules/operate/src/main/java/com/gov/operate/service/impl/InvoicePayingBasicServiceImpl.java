package com.gov.operate.service.impl;

import com.gov.operate.entity.InvoicePayingBasic;
import com.gov.operate.mapper.InvoicePayingBasicMapper;
import com.gov.operate.service.IInvoicePayingBasicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
@Service
public class InvoicePayingBasicServiceImpl extends ServiceImpl<InvoicePayingBasicMapper, InvoicePayingBasic> implements IInvoicePayingBasicService {

}
