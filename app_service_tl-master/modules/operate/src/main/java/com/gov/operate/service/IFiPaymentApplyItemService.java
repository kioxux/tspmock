package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.PaymentApplyItem;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zp
 * @since 2021-04-01
 */
public interface IFiPaymentApplyItemService extends SuperService<PaymentApplyItem> {
}
