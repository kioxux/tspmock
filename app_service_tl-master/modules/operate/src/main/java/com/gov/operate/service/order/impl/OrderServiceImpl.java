package com.gov.operate.service.order.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.common.basic.LambdaUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.UserData;
import com.gov.operate.mapper.FranchiseeMapper;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.order.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.06.21
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private FranchiseeMapper franchiseeMapper;

    @Resource
    private UserDataMapper userDataMapper;

    @Override
    public Result getFranchiseeList() {
        List<Franchisee> list = franchiseeMapper.selectList(null);
        return ResultUtil.success(list);
    }

    @Override
    public Result getUserListByClient(String client) {
        List<UserData> list = userDataMapper.selectList(new LambdaQueryWrapper<UserData>().eq(UserData::getClient, client));
        list.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
            item.setUserPassword("");
        }));
        return ResultUtil.success(list);
    }
}

