package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryMarketingTemplateVO extends Pageable {
    /**
     * 模板主题
     */
    private String smsTheme;

    /**
     * 模板状态
     */
    private String smsStatus;

    /**
     * 审批状态：0-审批中，1-已审批，2-已拒绝
     */
    private String smsGspStatus;

}
