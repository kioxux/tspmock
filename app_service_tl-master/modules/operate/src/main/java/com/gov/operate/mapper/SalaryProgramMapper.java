package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.QueryProgramListDTO;
import com.gov.operate.entity.SalaryProgram;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface SalaryProgramMapper extends BaseMapper<SalaryProgram> {

    /**
     * 方案列表
     */
    Page<QueryProgramListDTO> queryProgramList(Page<QueryProgramListDTO> page, @Param("client") String client);
}
