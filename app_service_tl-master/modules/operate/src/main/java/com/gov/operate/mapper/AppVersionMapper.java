package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.AppVersionDTO;
import com.gov.operate.entity.AppVersion;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
public interface AppVersionMapper extends BaseMapper<AppVersion> {

    /**
     * APP最新版本
     * @param type        平台类型 1:android 2:ios
     */
    AppVersion getAppLastVersion(@Param("type") Integer type);

    /**
     * 编辑事排除自身的最新版本号
     * @param type
     * @return
     */
    AppVersion getAppLastVersionExcludeSelf(@Param("type") Integer type);

    /**
     * APP版本列表
     */
    IPage<AppVersionDTO> getAppList(Page<AppVersionDTO> page, @Param("ew") QueryWrapper<AppVersionDTO> ew);


}
