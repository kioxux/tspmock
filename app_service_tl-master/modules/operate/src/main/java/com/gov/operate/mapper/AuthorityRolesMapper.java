package com.gov.operate.mapper;

import com.gov.operate.entity.AuthorityRoles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface AuthorityRolesMapper extends BaseMapper<AuthorityRoles> {

}
