package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class OrderInvoiceVO {

    @NotBlank(message = "业务单号不能为空")
    private String matDnId;

    @NotEmpty(message = "发票明细不能为空")
    private List<String> invoiceNumList;

    /**
     * 业务单集合
     */
    private List<String> matDnIdList;
}
