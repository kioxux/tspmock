package com.gov.operate.feign.fallback;

import com.gov.operate.feign.OperationFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.RegionDTO;
import org.springframework.stereotype.Component;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/7 01:06
 */
@Component
public class OperationFeignFallback implements OperationFeign {
    @Override
    public JsonResult addDefaultRegion(RegionDTO regionDTO) {
        return JsonResult.error();
    }
}
