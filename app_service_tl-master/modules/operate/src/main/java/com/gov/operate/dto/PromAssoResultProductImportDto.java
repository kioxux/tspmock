package com.gov.operate.dto;

import com.gov.common.entity.dataImport.PromAssoResultProduct;
import com.gov.common.entity.dataImport.PromGiftResultProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromAssoResultProductImportDto extends PromAssoResultProduct {
    private String proSelfCode;

    private String proCommonname;

    private String proName;

    private String proSpecs;

    private String proFactoryName;
}
