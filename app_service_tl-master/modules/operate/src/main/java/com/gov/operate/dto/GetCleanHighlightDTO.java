package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Author staxc
 * @Date 2020/10/15 15:04
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetCleanHighlightDTO {
    /**
     * 商品编码
     */
    private String gscdProId;

    /**
     * 批号
     */
    private String gscdBatchNo;

    /**
     * 有效期
     */
    private String gscdValidDate;

    /**
     * 数量
     */
    private BigDecimal gscdQty;

}
