package com.gov.operate.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;


/**
 * @Author staxc
 * @Date 2020/10/23 14:04
 * @desc 发票申请
 */
@Data
@ApiModel(value = "发票申请传入参数")
public class InvoiceApplyRequestDto {

    /**
     * 身份认证，在诺诺网备案后，由诺诺网提供，每个企业一个
     */
    private String identity;

    /**
     * 订单明细
     */
    private InvoiceApplyOrderRequestDto order;


}
