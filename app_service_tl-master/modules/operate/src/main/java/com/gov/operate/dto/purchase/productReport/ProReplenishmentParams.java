package com.gov.operate.dto.purchase.productReport;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.10.09
 */
@Data
public class ProReplenishmentParams {
    /**
     * 1：相等，2：模糊，3：in
     */
    private Integer type;

    private String field;

    private Object data;
}
