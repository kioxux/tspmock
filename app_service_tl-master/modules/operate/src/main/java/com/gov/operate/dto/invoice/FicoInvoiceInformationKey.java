package com.gov.operate.dto.invoice;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class FicoInvoiceInformationKey {

    /**
     * 采购地点
     */
    private String siteCode;
    /**
     * 供应商编码
     */
    private String supCode;

}
