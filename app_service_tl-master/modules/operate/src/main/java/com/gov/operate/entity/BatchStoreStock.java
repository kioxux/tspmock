package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 门店库存
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_STORE_STOCK")
@ApiModel(value="BatchStoreStock对象", description="门店库存")
public class BatchStoreStock extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableField("ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBSS_QUERY_FACTORY")
    private String gbssQueryFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBSS_DC")
    private String gbssDc;

    @ApiModelProperty(value = "日期")
    @TableField("GBSS_DATE")
    private String gbssDate;

    @ApiModelProperty(value = "公司代码")
    @TableField("GBSS_COMPANY_CODE")
    private String gbssCompanyCode;

    @ApiModelProperty(value = "公司名称")
    @TableField("GBSS_COMPANY_NAME")
    private String gbssCompanyName;

    @ApiModelProperty(value = "产品代码")
    @TableField("GBSS_PRO_CODE")
    private String gbssProCode;

    @ApiModelProperty(value = "产品名称")
    @TableField("GBSS_PRO_NAME")
    private String gbssProName;

    @ApiModelProperty(value = "产品规格")
    @TableField("GBSS_PRO_SPECS")
    private String gbssProSpecs;

    @ApiModelProperty(value = "批号")
    @TableField("GBSS_BATCH_NO")
    private String gbssBatchNo;

    @ApiModelProperty(value = "数量")
    @TableField("GBSS_QTY")
    private BigDecimal gbssQty;

    @ApiModelProperty(value = "单位")
    @TableField("GBSS_UNIT")
    private String gbssUnit;

    @ApiModelProperty(value = "生产厂家")
    @TableField("GBSS_FACTORY")
    private String gbssFactory;

    @ApiModelProperty(value = "有效期")
    @TableField("GBSS_VALID")
    private String gbssValid;

    @ApiModelProperty(value = "库存状态")
    @TableField("GBSS_KCZT_BH")
    private String gbssKcztBh;

    @ApiModelProperty(value = "批准文号")
    @TableField("GBSS_APPROVAL_NUMBER")
    private String gbssApprovalNumber;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBSS_CREATE_DATE")
    private String gbssCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBSS_CREATE_TIME")
    private String gbssCreateTime;


}
