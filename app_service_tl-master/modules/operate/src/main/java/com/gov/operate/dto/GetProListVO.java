package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetProListVO {

    @NotNull(message = "方案主键不能为空")
    private Integer gspId;
    /**
     * 商品编码/商品名
     */
    private String proSelfCode;

}
