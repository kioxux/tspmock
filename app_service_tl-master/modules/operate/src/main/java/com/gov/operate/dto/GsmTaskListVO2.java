package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = true)
public class GsmTaskListVO2 extends Pageable {


    /**
     * 门店组ID
     */
    private String stogCode;

    /**
     * 门店编号
     */
    private String gsmStore;

    /**
     * 活动类型(0-主题活动，1-周期活动)
     */
    private String gsmType;

    /**
     * 活动开始时间
     */
    private String gsmStartd;

    /**
     * 活动结束时间
     */
    private String gsmEndd;

    /**
     * 活动名称
     */
    private String gsmThename;

    /**
     * -1待处理，0：同意执行，1待审批，2已审批，3作废，4：立即执行，5不予执行
     */
    private String gsmImpl;

    /**
     * 状态(1:历史营销活动, 2:当前营销活动)
     */
    private String status;

    /**
     * 0:加盟商负责人/委托人 1:员工
     */
    private Integer legalPerson;
}
