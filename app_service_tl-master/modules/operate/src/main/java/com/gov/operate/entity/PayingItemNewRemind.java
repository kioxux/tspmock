package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 付款提醒表
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYING_ITEM_NEW_REMIND")
@ApiModel(value="PayingItemNewRemind对象", description="付款提醒表")
public class PayingItemNewRemind extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("FICO_SITE_CODE")
    private String ficoSiteCode;

    @ApiModelProperty(value = "本期开始日期")
    @TableField("FICO_STRING_TIME")
    private String ficoStringTime;

    @ApiModelProperty(value = "本期截止日期")
    @TableField("FICO_ENDING_TIME")
    private String ficoEndingTime;

    @ApiModelProperty(value = "本期赠送截止日期")
    @TableField("FICO_GIVE_TIME")
    private String ficoGiveTime;

    @ApiModelProperty(value = "服务包ID")
    @TableField("SP_ID")
    private String spId;

    @ApiModelProperty(value = "系列")
    @TableField("SP_SERIES")
    private String spSeries;

    @ApiModelProperty(value = "服务包名称")
    @TableField("SP_NAME")
    private String spName;

    @ApiModelProperty(value = "提醒日期")
    @TableField("FICO_MSG_DATE")
    private String ficoMsgDate;

    @ApiModelProperty(value = "是否已发")
    @TableField("SP_SEND")
    private Integer spSend;


}
