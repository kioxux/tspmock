package com.gov.operate.dto.marketing;

import com.gov.operate.entity.SdMarketingBasicAttac;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class EditGsmSettingVO3 {

    @NotNull(message = "id不能为空")
    private Integer id;

    /**
     * 1 基础信息设置
     */
    @NotBlank(message = "活动名称不能为空")
    private String gsmThename;

    @Length(min = 0, max = 8)
    @NotBlank(message = "活动日期_开始时间不能为空")
    private String gsmStartd;

    @Length(min = 0, max = 8)
    @NotBlank(message = "活动日期_结束时间不能为空")
    private String gsmEndd;

    // @NotBlank(message = "活动简介不能为空")
    private String gsmTheintro;


    /**
     * 2.1 促销报告设置_同比
     */
    @NotBlank(message = "同比日期开始不能为空")
    private String gsmYearStart;

    @NotBlank(message = "同比日期结束不能为空")
    private String gsmYearEnd;

    @NotNull(message = "同比营业额增长倍率不能为空")
    private BigDecimal gsmYearTurnover;

    @NotNull(message = "同比毛利额增长倍率不能为空")
    private BigDecimal gsmYearGrossprofit;

    @NotNull(message = "同比促销品增长倍率不能为空")
    private BigDecimal gsmYearPromotionalpro;


    /**
     * 2.2 促销报告设置_环比
     */
    @NotNull(message = "环比日期开始 ")
    private String gsmMonthStart;

    @NotNull(message = "环比日期结束 ")
    private String gsmMonthEnd;

    @NotNull(message = "环比营业额增长倍率")
    private BigDecimal gsmMonthTurnover;

    @NotNull(message = "环比毛利额增长倍率")
    private BigDecimal gsmMonthGrossprofit;

    @NotNull(message = "环比促销品增长倍率")
    private BigDecimal gsmMonthPromotionalpro;

    /**
     * 3 促销成分选择
     */
    // @NotBlank(message = "选品条件不能为空")
    private String gsmProcondi;


    /**
     * 4 会员宣传设置
     */
    // @NotBlank(message = "短信查询条件不能为空")
    private String gsmSmscondi;

    // @NotBlank(message = "电话通知查询条件不能为空")
    private String gsmTelcondi;

    // @NotBlank(message = "电话通知人数不能为空")
    private BigDecimal gsmTeltimes;

    /**
     * 5 宣传物料设置
     */
    List<SdMarketingBasicAttac> marketingBasicAttacList;

}
