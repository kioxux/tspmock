package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 查询数据权限
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_QUERY_AUTH")
@ApiModel(value="BatchQueryAuth对象", description="查询数据权限")
public class BatchQueryAuth extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBQA_FACTORY")
    private String gbqaFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBQA_DC")
    private String gbqaDc;

    @ApiModelProperty(value = "总部采购")
    @TableField("GBQA_HEAD_PURCHASE")
    private Integer gbqaHeadPurchase;

    @ApiModelProperty(value = "总部库存")
    @TableField("GBQA_HEAD_STOCK")
    private Integer gbqaHeadStock;

    @ApiModelProperty(value = "总部销售")
    @TableField("GBQA_HEAD_SALES")
    private Integer gbqaHeadSales;

    @ApiModelProperty(value = "门店购进")
    @TableField("GBQA_STORE_PURCHASE")
    private Integer gbqaStorePurchase;

    @ApiModelProperty(value = "门店销售")
    @TableField("GBQA_STORE_SALES")
    private Integer gbqaStoreSales;

    @ApiModelProperty(value = "门店库存")
    @TableField("GBQA_STORE_STOCK")
    private Integer gbqaStoreStock;

    @ApiModelProperty(value = "查询期限")
    @TableField("GBQA_END_DATE")
    private String gbqaEndDate;


}
