package com.gov.operate.service.impl;

import com.gov.operate.entity.ProductComponent;
import com.gov.operate.mapper.ProductComponentMapper;
import com.gov.operate.service.IProductComponentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
@Service
public class ProductComponentServiceImpl extends ServiceImpl<ProductComponentMapper, ProductComponent> implements IProductComponentService {

}
