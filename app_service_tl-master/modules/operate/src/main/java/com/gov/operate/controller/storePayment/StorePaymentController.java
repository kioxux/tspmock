package com.gov.operate.controller.storePayment;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.InvoicePayingBasicVO;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IStorePaymentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.28
 */
@RestController
@RequestMapping("storePayment/storePayment")
public class StorePaymentController {

    @Resource
    private IStorePaymentService storePaymentService;

    @Log("APP付款列表")
    @ApiOperation(value = "APP付款列表")
    @PostMapping("storePaymentList")
    public Result storePaymentList(@RequestJson(value = "type", required = false) String type, // 0 单体 1 连锁 2 批发
                                   @RequestJson(value = "ficoPayingstoreCode", required = false) String ficoPayingstoreCode,
                                   @RequestJson(value = "ficoPayingstoreName", required = false) String ficoPayingstoreName,
                                   @RequestJson(value = "validDate", required = false) Integer validDate) {
        return storePaymentService.storePaymentList(type, ficoPayingstoreCode, ficoPayingstoreName, validDate);
    }

    @Log("门店付款提醒")
    @ApiOperation(value = "门店付款提醒")
    @PostMapping("payReminder")
    public void payReminder() {
        storePaymentService.payReminder();
    }

    @Log("APP合并纳税列表")
    @ApiOperation(value = "APP合并纳税列表")
    @GetMapping("getPayGroup")
    public Result getPayGroup() {
        return storePaymentService.getPayGroup();
    }

    @Log("APP合并纳税明细列表")
    @ApiOperation(value = "APP合并纳税明细列表")
    @PostMapping("getPayGroupItem")
    public Result getPayGroupItem(@RequestJson("ficoCompanyCode") String ficoCompanyCode) {
        return storePaymentService.getPayGroupItem(ficoCompanyCode);
    }

    @Log("同一连锁下的付款主体")
    @ApiOperation(value = "同一连锁下的付款主体")
    @PostMapping("getPayList")
    public Result getPayList(@RequestJson("ficoCompanyCode") String ficoCompanyCode) {
        return storePaymentService.getPayList(ficoCompanyCode);
    }

    @Log("纳税主体合并保存")
    @ApiOperation(value = "同一连锁下的付款主体")
    @PostMapping("savePayItem")
    public Result savePayItem(@RequestBody InvoicePayingBasicVO invoicePayingBasicVO) {
        return storePaymentService.savePayItem(invoicePayingBasicVO);
    }
}

