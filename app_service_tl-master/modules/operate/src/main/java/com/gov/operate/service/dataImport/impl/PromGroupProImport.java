package com.gov.operate.service.dataImport.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.common.entity.dataImport.PromGroupPro;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SdPromGroupPro;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ProductBusinessMapper;
import com.gov.operate.mapper.SdPromGroupProMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdPromGroupProService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.02
 */
@Service
public class PromGroupProImport extends BusinessImport {

    @Resource
    private ProductBusinessMapper productBusinessMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private SdPromGroupProMapper sdPromGroupProMapper;
    @Resource
    private ISdPromGroupProService sdPromGroupProService;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 商品组编号验证
        if (field.get("groupId") == null || StringUtils.isBlank(field.get("groupId").toString())) {
            throw new CustomResultException("商品组编号不能为空");
        }
        // 商品组编号
        String groupId = field.get("groupId").toString();
        // 商品编号
        List<String> proCodeList = new ArrayList();
        // 商品验证
        for (Integer key : map.keySet()) {
            PromGroupPro data = (PromGroupPro) map.get(key);
            proCodeList.add(data.getProSelfCode());
        }
        if (CollectionUtils.isEmpty(proCodeList)) {
            return ResultUtil.success();
        }
        TokenUser user = commonService.getLoginInfo();
        List<ProductBusiness> productBusinesseList = productBusinessMapper.selectList(new LambdaQueryWrapper<ProductBusiness>()
                .eq(ProductBusiness::getClient, user.getClient())
                .in(ProductBusiness::getProSelfCode, proCodeList)
                .apply(" PRO_SITE = (SELECT MIN( STO_CODE ) FROM GAIA_STORE_DATA WHERE CLIENT = {0}) ", user.getClient()));
        if (CollectionUtils.isEmpty(productBusinesseList)) {
            throw new CustomResultException("商品编号不存在");
        }
        List<String> errorList = new ArrayList<>();
        // 商品验证
        for (Integer key : map.keySet()) {
            PromGroupPro data = (PromGroupPro) map.get(key);
            ProductBusiness productBusiness = productBusinesseList.stream().filter(t -> t.getProSelfCode().equals(data.getProSelfCode())).findFirst().orElse(null);
            if (productBusiness == null) {
                errorList.add(MessageFormat.format("第{0}行：商品编号不存在", key + 1));
            }
        }
        // 验证不通过
        if (!CollectionUtils.isEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }

        sdPromGroupProMapper.delete(new LambdaQueryWrapper<SdPromGroupPro>().eq(SdPromGroupPro::getClient, user.getClient())
                .eq(SdPromGroupPro::getGroupId, groupId)
                .in(SdPromGroupPro::getGroupProId, proCodeList));
        List<SdPromGroupPro> list = new ArrayList<>();
        for (String proCode : proCodeList) {
            SdPromGroupPro sdPromGroupPro = new SdPromGroupPro();
            sdPromGroupPro.setClient(user.getClient());
            sdPromGroupPro.setGroupId(groupId);
            sdPromGroupPro.setGroupProId(proCode);
            list.add(sdPromGroupPro);
        }
        sdPromGroupProService.saveBatch(list);
        return ResultUtil.success();
    }
}
