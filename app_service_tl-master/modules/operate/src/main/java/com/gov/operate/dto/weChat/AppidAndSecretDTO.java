package com.gov.operate.dto.weChat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class AppidAndSecretDTO {
    /**
     * APPID
     */
    private String accountsAppid;
    /**
     * 密钥
     */
    private String accountsAppsecret;
}
