package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.dto.SalaryProgramDTO;
import com.gov.operate.entity.SalaryMonthWageJobs;
import com.gov.operate.mapper.SalaryMonthWageJobsMapper;
import com.gov.operate.service.ISalaryMonthWageJobsService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryMonthWageJobsServiceImpl extends ServiceImpl<SalaryMonthWageJobsMapper, SalaryMonthWageJobs> implements ISalaryMonthWageJobsService {

    /**
     * 岗位工资等级
     */
    @Override
    public void saveWageJobs(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除岗位工资 原始数据
        this.remove(new QueryWrapper<SalaryMonthWageJobs>().eq("GSMWJ_GSP_ID", gspId));
        List<EditProgramVO.SalaryMonthWageJobsOuter> salaryMonthWageJobsOuterList = vo.getSalaryMonthWageJobsOuterList();
        if (CollectionUtils.isEmpty(salaryMonthWageJobsOuterList)) {
            throw new CustomResultException("薪资方案月岗位工资列表不能为空");
        }
        List<SalaryMonthWageJobs> list = new ArrayList<>();
        for (int job = 0; job < salaryMonthWageJobsOuterList.size(); job++) {
            EditProgramVO.SalaryMonthWageJobsOuter salaryMonthWageJobsOuter = salaryMonthWageJobsOuterList.get(job);
            String gsmwjJobName = salaryMonthWageJobsOuter.getGsmwjJobName();
            if (StringUtils.isEmpty(gsmwjJobName)) {
                throw new CustomResultException(MessageFormat.format("第[{0}]个岗位名称不能为空", job + 1));
            }
            if (StringUtils.isEmpty(salaryMonthWageJobsOuter.getGsmwjJob())) {
                throw new CustomResultException(MessageFormat.format("[{0}]岗位编码不能为空", gsmwjJobName));
            }
            List<SalaryMonthWageJobs> salaryMonthWageJobsInnerList = salaryMonthWageJobsOuter.getSalaryMonthWageJobsInnerList();
            if (CollectionUtils.isEmpty(salaryMonthWageJobsInnerList)) {
                throw new CustomResultException(MessageFormat.format("岗位:[{0}]工资等级列表不能为空", gsmwjJobName));
            }
            for (int level = 0; level < salaryMonthWageJobsInnerList.size(); level++) {
                SalaryMonthWageJobs salaryMonthWageJobs = salaryMonthWageJobsInnerList.get(level);
                if (ObjectUtils.isEmpty(salaryMonthWageJobs.getGsmwjSalesMin())) {
                    throw new CustomResultException(MessageFormat.format("岗位:[{0}]第[{1}]等级日销售额最小值不能为空", gsmwjJobName, level + 1));
                }
                if (ObjectUtils.isEmpty(salaryMonthWageJobs.getGsmwjSalesMax())) {
                    throw new CustomResultException(MessageFormat.format("岗位:[{0}]第[{1}]等级日销售额最大值不能为空", gsmwjJobName, level + 1));
                }
                if (ObjectUtils.isEmpty(salaryMonthWageJobs.getGsmwjWage())) {
                    throw new CustomResultException(MessageFormat.format("岗位:[{0}]第[{1}]等级岗位工资不能为空", gsmwjJobName, level + 1));
                }
                // 方案主键
                salaryMonthWageJobs.setGsmwjGspId(gspId);
                // 岗位编码
                salaryMonthWageJobs.setGsmwjJob(salaryMonthWageJobsOuter.getGsmwjJob());
                // 岗位名称
                salaryMonthWageJobs.setGsmwjJobName(gsmwjJobName);
                // 岗位顺序
                salaryMonthWageJobs.setGsmwjJobSort(job + 1);
                // 销售额顺序
                salaryMonthWageJobs.setGsmwjSalesSort(level + 1);
                list.add(salaryMonthWageJobs);
            }

            // 校验范围重叠的问题
            salaryMonthWageJobsInnerList.sort(Comparator.comparing(SalaryMonthWageJobs::getGsmwjSalesMin));
            for (int i = 0; i < salaryMonthWageJobsInnerList.size(); i++) {
                if (i == 0) {
                    continue;
                }
                SalaryMonthWageJobs current = salaryMonthWageJobsInnerList.get(i);
                SalaryMonthWageJobs previous = salaryMonthWageJobsInnerList.get(i - 1);
                if (current.getGsmwjSalesMin().compareTo(previous.getGsmwjSalesMax()) <= 0) {
                    throw new CustomResultException(MessageFormat.format("岗位:[{0}] 日销售额区间[{1}~{2}]与[{3}~{4}]存在重叠区,请重新输入",
                            previous.getGsmwjSalesMin(), previous.getGsmwjSalesMax(), gsmwjJobName, current.getGsmwjSalesMin(), current.getGsmwjSalesMax()));
                }
            }
        }
        this.saveBatch(list);
    }

    /**
     * 岗位薪资列表
     */
    @Override
    public List<SalaryProgramDTO.SalaryMonthWageJobsOuter> getWageJobsList(Integer gspId) {
        List<SalaryMonthWageJobs> list = this.list(new QueryWrapper<SalaryMonthWageJobs>()
                .eq("GSMWJ_GSP_ID", gspId)
                .orderByAsc("GSMWJ_JOB_SORT", "GSMWJ_SALES_SORT"));
        Map<Integer, List<SalaryMonthWageJobs>> jobsMap = list.stream().collect(Collectors.groupingBy(SalaryMonthWageJobs::getGsmwjJobSort));
        List<SalaryProgramDTO.SalaryMonthWageJobsOuter> outerList = new ArrayList<>();

        jobsMap.forEach((jobSort, salaryList) -> {
            // 岗位数据
            SalaryMonthWageJobs salaryMonthWageJobs = salaryList.get(0);
            SalaryProgramDTO.SalaryMonthWageJobsOuter salaryMonthWageJobsOuter = new SalaryProgramDTO.SalaryMonthWageJobsOuter();
            BeanUtils.copyProperties(salaryMonthWageJobs, salaryMonthWageJobsOuter);
            // 日销售额 正序排序
            salaryList.sort(Comparator.comparing(SalaryMonthWageJobs::getGsmwjSalesSort));
            salaryMonthWageJobsOuter.setSalaryMonthWageJobsInnerList(salaryList);
            //
            outerList.add(salaryMonthWageJobsOuter);
        });
        outerList.sort(Comparator.comparing(SalaryProgramDTO.SalaryMonthWageJobsOuter::getGsmwjJobSort));
        return outerList;
    }

    /**
     * 岗位薪资未分组列表
     */
    @Override
    public List<SalaryMonthWageJobs> getWageJobsNoGroupingList(Integer gspId) {
        List<SalaryMonthWageJobs> list = this.list(new QueryWrapper<SalaryMonthWageJobs>()
                .eq("GSMWJ_GSP_ID", gspId)
                .orderByAsc("GSMWJ_JOB_SORT", "GSMWJ_SALES_SORT"));
        return list;
    }
}
