package com.gov.operate.service;

import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryQuarterlyAwards;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryQuarterlyAwardsService extends SuperService<SalaryQuarterlyAwards> {

    /**
     * 薪资方案季度奖
     */
    void saveQuarterlyAwards(EditProgramVO vo);

    /**
     * 季度奖列表
     */
    List<SalaryQuarterlyAwards> getQuarterlyAwards(Integer gspId);
}
