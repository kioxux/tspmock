package com.gov.operate.mapper;

import com.gov.operate.entity.BatchQueryAuthData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 查询数据清单 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface BatchQueryAuthDataMapper extends BaseMapper<BatchQueryAuthData> {

}
