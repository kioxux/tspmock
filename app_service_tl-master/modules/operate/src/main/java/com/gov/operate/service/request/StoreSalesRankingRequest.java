package com.gov.operate.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StoreSalesRankingRequest {


    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店可多选")
    private String[] storeList;

    @ApiModelProperty(value = "年月份可多选")
    private String[] yearMonth;

    @ApiModelProperty(value = "排名0前 1后")
    private Integer beforeAndAfter;

    @ApiModelProperty(value = "名次")
    private int ranking;

    @ApiModelProperty(value = "门店/店经理")
    private String storeManager;
   //2：会员卡       3 :毛利率
    @ApiModelProperty(value = "0：营业额 1：毛利额 2：毛利率       3会员卡  4.交易次数 5客单价 6客品次7品单价8单店日均销售9单店日均毛利 10单店日均交易")
    private Integer type;
}
