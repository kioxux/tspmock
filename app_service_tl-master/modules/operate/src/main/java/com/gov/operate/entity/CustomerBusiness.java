package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 客户主数据业务信息表
 * </p>
 *
 * @author sy
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_CUSTOMER_BUSINESS")
@ApiModel(value="CustomerBusiness对象", description="客户主数据业务信息表")
public class CustomerBusiness extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("CUS_SITE")
    private String cusSite;

    @ApiModelProperty(value = "供应商自编码")
    @TableField("CUS_SELF_CODE")
    private String cusSelfCode;

    @ApiModelProperty(value = "客户编码")
    @TableField("CUS_CODE")
    private String cusCode;

    @ApiModelProperty(value = "匹配状态")
    @TableField("CUS_MATCH_STATUS")
    private String cusMatchStatus;

    @ApiModelProperty(value = "助记码")
    @TableField("CUS_PYM")
    private String cusPym;

    @ApiModelProperty(value = "客户名称")
    @TableField("CUS_NAME")
    private String cusName;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("CUS_CREDIT_CODE")
    private String cusCreditCode;

    @ApiModelProperty(value = "营业期限")
    @TableField("CUS_CREDIT_DATE")
    private String cusCreditDate;

    @ApiModelProperty(value = "客户分类")
    @TableField("CUS_CLASS")
    private String cusClass;

    @ApiModelProperty(value = "法人")
    @TableField("CUS_LEGAL_PERSON")
    private String cusLegalPerson;

    @ApiModelProperty(value = "注册地址")
    @TableField("CUS_REG_ADD")
    private String cusRegAdd;

    @ApiModelProperty(value = "客户状态")
    @TableField("CUS_STATUS")
    private String cusStatus;

    @ApiModelProperty(value = "许可证编号")
    @TableField("CUS_LICENCE_NO")
    private String cusLicenceNo;

    @ApiModelProperty(value = "发证日期")
    @TableField("CUS_LICENCE_DATE")
    private String cusLicenceDate;

    @ApiModelProperty(value = "有效期至")
    @TableField("CUS_LICENCE_VALID")
    private String cusLicenceValid;

    @ApiModelProperty(value = "生产或经营范围")
    @TableField("CUS_SCOPE")
    private String cusScope;

    @ApiModelProperty(value = "禁止销售")
    @TableField("CUS_NO_SALE")
    private String cusNoSale;

    @ApiModelProperty(value = "禁止退货")
    @TableField("CUS_NO_RETURN")
    private String cusNoReturn;

    @ApiModelProperty(value = "付款条件")
    @TableField("CUS_PAY_TERM")
    private String cusPayTerm;

    @ApiModelProperty(value = "业务联系人")
    @TableField("CUS_BUSSINESS_CONTACT")
    private String cusBussinessContact;

    @ApiModelProperty(value = "联系人电话")
    @TableField("CUS_CONTACT_TEL")
    private String cusContactTel;

    @ApiModelProperty(value = "收货地址")
    @TableField("CUS_DELIVERY_ADD")
    private String cusDeliveryAdd;

    @ApiModelProperty(value = "是否启用信用管理")
    @TableField("CUS_CREDIT_FLAG")
    private String cusCreditFlag;

    @ApiModelProperty(value = "信用额度")
    @TableField("CUS_CREDIT_QUOTA")
    private BigDecimal cusCreditQuota;

    @ApiModelProperty(value = "信用检查点")
    @TableField("CUS_CREDIT_CHECK")
    private String cusCreditCheck;

    @ApiModelProperty(value = "银行代码")
    @TableField("CUS_BANK_CODE")
    private String cusBankCode;

    @ApiModelProperty(value = "银行名称")
    @TableField("CUS_BANK_NAME")
    private String cusBankName;

    @ApiModelProperty(value = "账户持有人")
    @TableField("CUS_ACCOUNT_PERSON")
    private String cusAccountPerson;

    @ApiModelProperty(value = "银行账号")
    @TableField("CUS_BANK_ACCOUNT")
    private String cusBankAccount;

    @ApiModelProperty(value = "支付方式")
    @TableField("CUS_PAY_MODE")
    private String cusPayMode;

    @ApiModelProperty(value = "铺底授信额度")
    @TableField("CUS_CREDIT_AMT")
    private String cusCreditAmt;

    @ApiModelProperty(value = "质量负责人")
    @TableField("CUS_QUA")
    private String cusQua;

    @ApiModelProperty(value = "法人委托书图片")
    @TableField("CUS_PICTURE_FRWTS")
    private String cusPictureFrwts;

    @ApiModelProperty(value = "GSP证书图片")
    @TableField("CUS_PICTURE_GSP")
    private String cusPictureGsp;

    @ApiModelProperty(value = "GMP证书图片")
    @TableField("CUS_PICTURE_GMP")
    private String cusPictureGmp;

    @ApiModelProperty(value = "随货同行单图片")
    @TableField("CUS_PICTURE_SHTXD")
    private String cusPictureShtxd;

    @ApiModelProperty(value = "其他图片")
    @TableField("CUS_PICTURE_QT")
    private String cusPictureQt;

    @ApiModelProperty(value = "食品许可证书图片")
    @TableField("CUS_PICTURE_SPXKZ")
    private String cusPictureSpxkz;

    @ApiModelProperty(value = "食品许可证书")
    @TableField("CUS_SPXKZ")
    private String cusSpxkz;

    @ApiModelProperty(value = "食品许可证有效期")
    @TableField("CUS_SPXKZ_DATE")
    private String cusSpxkzDate;

    @ApiModelProperty(value = "医疗器械经营许可证图片")
    @TableField("CUS_PICTURE_YLQXXKZ")
    private String cusPictureYlqxxkz;

    @ApiModelProperty(value = "医疗器械经营许可证书")
    @TableField("CUS_YLQXXKZ")
    private String cusYlqxxkz;

    @ApiModelProperty(value = "医疗器械经营许可证有效期")
    @TableField("CUS_YLQXXKZ_DATE")
    private String cusYlqxxkzDate;

    @ApiModelProperty(value = "GMP证书")
    @TableField("CUS_GMP")
    private String cusGmp;

    @ApiModelProperty(value = "GMP有效期")
    @TableField("CUS_GMP_DATE")
    private String cusGmpDate;

    @ApiModelProperty(value = "医疗机构执业许可证图片")
    @TableField("CUS_PICTURE_YLJGXKZ")
    private String cusPictureYljgxkz;

    @ApiModelProperty(value = "医疗机构执业许可证书")
    @TableField("CUS_YLJGXKZ")
    private String cusYljgxkz;

    @ApiModelProperty(value = "医疗机构执业许可证书有效期")
    @TableField("CUS_YLJGXKZ_DATE")
    private String cusYljgxkzDate;

    @ApiModelProperty(value = "GSP证书")
    @TableField("CUS_GSP")
    private String cusGsp;

    @ApiModelProperty(value = "GSP有效期")
    @TableField("CUS_GSP_DATES")
    private String cusGspDates;

    @ApiModelProperty(value = "创建日期")
    @TableField("CUS_CREATE_DATE")
    private String cusCreateDate;


}
