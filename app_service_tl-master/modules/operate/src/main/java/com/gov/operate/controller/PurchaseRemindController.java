package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.service.IPurchaseRemindService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-07-08
 */
@RestController
@RequestMapping("purchaseRemind")
public class PurchaseRemindController {

    @Resource
    private IPurchaseRemindService iPurchaseRemindService;

    @Log("发送提醒购药微信模板消息")
    @ApiOperation(value = "发送提醒购药微信模板消息")
    @PostMapping("purchaseRemind")
    public Result purchaseRemind() {
        iPurchaseRemindService.sendPurchaseRemind();
        return ResultUtil.success();
    }
}

