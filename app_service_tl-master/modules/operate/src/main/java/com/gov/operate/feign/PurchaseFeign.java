package com.gov.operate.feign;

import com.gov.common.response.Result;
import com.gov.operate.feign.dto.SupplierPaymentAtt;
import com.gov.operate.feign.fallback.PurchaseFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(value = "gys-purchase", fallback = PurchaseFeignFallback.class
)
public interface PurchaseFeign {

    @PostMapping(value = "/purchase/supplierPaymentAtt")
    public Result supplierPaymentAtt(@RequestBody SupplierPaymentAtt supplierPaymentAtt);
}

