package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface ISdCleandrawerHService extends SuperService<SdCleandrawerH> {
    /**
     * 获取验收单信息
     *
     * @return
     */
    List<SdExamineH> getExamineInfo();

    /**
     * 清斗商品查询
     *
     * @param vo
     * @return
     */
    IPage<GetProductDTO> getCleanDrawderProductList(GetProductVO vo);

    /**
     * 清斗批号有效期选择
     *
     * @param vo
     * @return
     */
    List<BatchNoValidDto> getBatchNoValidList(GetBatchNoValidListVO vo);

    /**
     * 清斗人员
     *
     * @return
     */
    List<UserData> getCleanDrawerPersonnel(GetCleanDrawerPersonnelVO vo);

    /**
     * 清斗保存
     *
     * @param vo
     * @return
     */
    String addCleanDrawer(AddCleanDrawerVO vo);

    /**
     * 清斗审核
     *
     * @param vo
     * @return
     */
    Result checkCleanDrawer(CheckCleanDrawerVO vo);

    /**
     * 门店清斗记录查询
     *
     * @param vo
     * @return
     */
    IPage<CleanDrawerVO> listCleanDrawer(ListCleanDrawerVO vo);

    /**
     * 清斗明细
     *
     * @param gschVoucherId
     * @return
     */
    List<CleanDrawerDetailDTO> getCleanDrawer(String gschVoucherId);

    /**
     * 验收单高亮显示信息
     *
     * @param vo
     * @return
     */
    Result getHighlightInfo(GetHighlightVO vo);

    /**
     * 清斗修改
     *
     * @param vo
     * @return
     */
    Result updateCleanDrawer(UpdateCleanDrawerVO vo);

    /**
     * 清斗装斗控制维护开关查询
     * @return
     */
    ClSystemPara getCheckShop();
}
