package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.GradeDistributionVO;
import com.gov.operate.dto.MemberDistributionVO;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.entity.SdMemberGradeList;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdGradeListMapper;
import com.gov.operate.mapper.SdMemberGradeListMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdMemberGradeListService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
@Service
public class SdMemberGradeListServiceImpl extends ServiceImpl<SdMemberGradeListMapper, SdMemberGradeList> implements ISdMemberGradeListService {

    @Resource
    private CommonService commonService;

    @Override
    public Result gradeDistributionList(MemberReportRequestDto requestDto) {
        TokenUser user = commonService.getLoginInfo();
        requestDto.setClient(user.getClient());
//        requestDto.setClient("21020001");
        return ResultUtil.success(baseMapper.getGradeDistributionList(requestDto));
    }

    @Override
    public Result gradeTrendList(MemberReportRequestDto requestDto) {
        TokenUser user = commonService.getLoginInfo();
        requestDto.setClient(user.getClient());
//        requestDto.setClient("21020001");

        // 时间筛选条件 当前一年
        LocalDate current = LocalDate.now();
        requestDto.setStartMonth(current.getYear() + "01");
        requestDto.setEndMonth(current.getYear() + "12");

        List<GradeDistributionVO> list = baseMapper.getGradeTrendList(requestDto);

        //整理成嵌套式格式   外层是 分级参数  里层是 年月和个数
        List<GradeDistributionVO> resultList = new ArrayList<>();

        if (list != null && list.size() > 0) {
            Map<String, List<GradeDistributionVO>> mapList = list.stream().collect(Collectors.groupingBy(GradeDistributionVO::getGsglId));
            for (Map.Entry<String, List<GradeDistributionVO>> entry : mapList.entrySet()) {
                List<GradeDistributionVO> entryValueList = entry.getValue();
                GradeDistributionVO vo = new GradeDistributionVO();
                vo.setGsglId(entry.getKey());
                vo.setGsglName(entryValueList.get(0).getGsglName());
                vo.setGsglColor(entryValueList.get(0).getGsglColor());
                List<GradeDistributionVO> newList = new ArrayList<>();
                // 补一年
                for (int i = 1; i <= 12; i++) {
                    String yearMonthStr = current.getYear() + StringUtils.leftPad(String.valueOf(i), 2, "0");  // 新的日期格式
                    String yearMonthFormat = current.getYear() + "-" + StringUtils.leftPad(String.valueOf(i), 2, "0");  // 新的日期格式
                    boolean flag = false; // 是否找到
                    for (GradeDistributionVO gv : entryValueList) {
                        if (yearMonthStr.equals(gv.getGsmglCalcMonth())) {
                            flag = true;
                            gv.setGsmglCalcMonth(yearMonthFormat);
                            newList.add(gv);
                        }
                    }
                    if (!flag) {
                        GradeDistributionVO gv = new GradeDistributionVO();
                        gv.setGsmglCalcMonth(yearMonthFormat);
                        gv.setGradeTotal(0);
                        newList.add(gv);
                    }
                }
                vo.setDataList(newList);
                resultList.add(vo);
            }
        }
        return ResultUtil.success(resultList);
    }

    @Override
    public Result gradeDistributionListNew(MemberReportRequestDto requestDto) {
        if (requestDto.getType().intValue() == 1) {
            if (CollectionUtils.isEmpty(requestDto.getStoCodeList())) {
                requestDto.setStoCodeList(commonService.getStoCodeList());
            }
            if (CollectionUtils.isEmpty(requestDto.getStoCodeList())) {
                return ResultUtil.success(new ArrayList<MemberDistributionVO>());
            }
        }
        TokenUser user = commonService.getLoginInfo();
        requestDto.setClient(user.getClient());
        return ResultUtil.success(baseMapper.getGradeDistributionListNew(requestDto));
    }

    @Override
    public Result gradeTrendListNew(MemberReportRequestDto requestDto) {
        if (requestDto.getType().intValue() == 1) {
            if (CollectionUtils.isEmpty(requestDto.getStoCodeList())) {
                requestDto.setStoCodeList(commonService.getStoCodeList());
            }
            if (CollectionUtils.isEmpty(requestDto.getStoCodeList())) {
                return ResultUtil.success(new ArrayList<MemberDistributionVO>());
            }
        }
        TokenUser user = commonService.getLoginInfo();
        requestDto.setClient(user.getClient());
//        requestDto.setClient("21020001");

        // 时间筛选条件 当前一年
        LocalDate current = LocalDate.now();
        requestDto.setStartMonth(current.getYear() + "01");
        requestDto.setEndMonth(current.getYear() + "12");

        List<GradeDistributionVO> list = baseMapper.getGradeTrendListNew(requestDto);

        //整理成嵌套式格式   外层是 分级参数  里层是 年月和个数
        List<GradeDistributionVO> resultList = new ArrayList<>();

        if (list != null && list.size() > 0) {
            Map<String, List<GradeDistributionVO>> mapList = list.stream().collect(Collectors.groupingBy(GradeDistributionVO::getGsglId));
            for (Map.Entry<String, List<GradeDistributionVO>> entry : mapList.entrySet()) {
                List<GradeDistributionVO> entryValueList = entry.getValue();
                GradeDistributionVO vo = new GradeDistributionVO();
                vo.setGsglId(entry.getKey());
                vo.setGsglName(entryValueList.get(0).getGsglName());
                vo.setGsglColor(entryValueList.get(0).getGsglColor());
                List<GradeDistributionVO> newList = new ArrayList<>();
                // 补一年
                for (int i = 1; i <= 12; i++) {
                    String yearMonthStr = current.getYear() + StringUtils.leftPad(String.valueOf(i), 2, "0");  // 新的日期格式
                    String yearMonthFormat = current.getYear() + "-" + StringUtils.leftPad(String.valueOf(i), 2, "0");  // 新的日期格式
                    boolean flag = false; // 是否找到
                    for (GradeDistributionVO gv : entryValueList) {
                        if (yearMonthStr.equals(gv.getGsmglCalcMonth())) {
                            flag = true;
                            gv.setGsmglCalcMonth(yearMonthFormat);
                            newList.add(gv);
                        }
                    }
                    if (!flag) {
                        GradeDistributionVO gv = new GradeDistributionVO();
                        gv.setGsmglCalcMonth(yearMonthFormat);
                        gv.setGradeTotal(0);
                        newList.add(gv);
                    }
                }
                vo.setDataList(newList);
                resultList.add(vo);
            }
        }
        return ResultUtil.success(resultList);
    }
}
