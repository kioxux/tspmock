package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_STOG_DATA")
@ApiModel(value="StogDate对象", description="")
public class StogData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店组编码")
    @TableField("STOG_CODE")
    private String stogCode;

    @ApiModelProperty(value = "门店组名称")
    @TableField("STOG_NAME")
    private String stogName;


}
