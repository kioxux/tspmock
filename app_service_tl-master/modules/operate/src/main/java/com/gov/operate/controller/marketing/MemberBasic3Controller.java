package com.gov.operate.controller.marketing;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.service.marketing.ISdMarketingSearch3Service;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-22
 */
@RestController
@RequestMapping("gsm03/gsm")
public class MemberBasic3Controller {

    @Resource
    private ISdMarketingSearch3Service sdMarketingSearchService;

    @Log("精准查询")
    @ApiOperation(value = "精准查询")
    @PostMapping("getMemberList")
    public Result getMemberList(@Valid @RequestBody GetMemberListVO3 vo ) {
        return ResultUtil.success(sdMarketingSearchService.getMemberList(vo));
    }

    @Log("精准查询全部")
    @ApiOperation(value = "精准查询全部")
    @PostMapping("getMemberListAll")
    public Result getMemberListAll(@Valid @RequestBody GetMemberListVO3 vo) {
        vo.setPageSize(-1);
        vo.setPageNum(-1);
        return ResultUtil.success(sdMarketingSearchService.getMemberList(vo));
    }

    @Log("查询消费渠道")
    @ApiOperation(value = "查询消费渠道")
    @PostMapping("getPaymentList")
    public Result getPaymentList() {
        return ResultUtil.success(sdMarketingSearchService.getPaymentList());
    }

    @Log("查询疾病信息")
    @ApiOperation(value = "查询疾病信息")
    @PostMapping("getDiseasePage")
    public Result getDiseasePage(@Valid @RequestBody GetDiseasePageDto dto) {
        return ResultUtil.success(sdMarketingSearchService.getDiseasePage(dto));
    }

    @Log("查询商品信息")
    @ApiOperation(value = "查询商品信息")
    @PostMapping("getProductPage")
    public Result getProductPage(@Valid @RequestBody GetProductPageDto dto) {
        return ResultUtil.success(sdMarketingSearchService.getProductPage(dto));
    }

    @Log("查询商品分类信息")
    @ApiOperation(value = "查询商品分类信息")
    @PostMapping("getProductClassPage")
    public Result getProductClassPage(@Valid @RequestBody GetProductClassPageDto dto) {
        return ResultUtil.success(sdMarketingSearchService.getProductClassPage(dto));
    }

    @Log("查询生产厂家信息")
    @ApiOperation(value = "查询生产厂家信息")
    @PostMapping("getFactoryPage")
    public Result getFactoryPage(@Valid @RequestBody GetFactoryPageDto dto) {
        return ResultUtil.success(sdMarketingSearchService.getFactoryPage(dto));
    }

    @Log("查询品牌信息")
    @ApiOperation(value = "查询品牌信息")
    @PostMapping("getBrandPage")
    public Result getBrandPage(@Valid @RequestBody GetBrandPageDto dto) {
        return ResultUtil.success(sdMarketingSearchService.getBrandPage(dto));
    }

    @Log("查询成分信息")
    @ApiOperation(value = "查询成分信息")
    @PostMapping("getProComPage")
    public Result getProComPage(@Valid @RequestBody GetProComPageDto dto) {
        return ResultUtil.success(sdMarketingSearchService.getProComPage(dto));
    }



    @SneakyThrows
    @Log("精准查询导出")
    @ApiOperation(value = "精准查询导出")
    @PostMapping("exportMemberList")
    public Result exportMemberList(@Valid @RequestBody GetMemberListVO3 vo ) {
         return sdMarketingSearchService.exportMemberList(vo);
    }

    @Log("常用查询列表_菜单入口")
    @ApiOperation(value = "常用查询列表_菜单入口")
    @PostMapping("getCommonSearchListFromMenu")
    public Result getCommonSearchListFromMenu(@RequestBody @Valid GetCommonSearchListFromMenu vo) {
        return ResultUtil.success(sdMarketingSearchService.getCommonSearchListFromMenu(vo));
    }

    @Log("常用查询列表_营销任务详情跳转")
    @ApiOperation(value = "常用查询列表_营销任务详情跳转")
    @PostMapping("getCommonSearchListFromMarketing")
    public Result getCommonSearchListFromMarketing(@RequestBody @Valid GetCommonSearchListFromMarketing vo) {
        return ResultUtil.success(sdMarketingSearchService.getCommonSearchListFromMarketing(vo));
    }

    @Log("常用查询另存")
    @ApiOperation(value = "常用查询另存")
    @PostMapping("saveCommonSearch")
    public Result saveCommonSearch(@Valid @RequestBody SaveCommonSearchVO3 searchVO) {
        sdMarketingSearchService.saveCommonSearch(searchVO);
        return ResultUtil.success();
    }

    @Log("常用查询覆盖")
    @ApiOperation(value = "常用查询覆盖")
    @PostMapping("editCommonSearch")
    public Result editCommonSearch(@Valid @RequestBody EditCommonSearchVO3 vo) {
        sdMarketingSearchService.editCommonSearch(vo);
        return ResultUtil.success();
    }

    @Log("常用查询删除")
    @ApiOperation(value = "常用查询删除")
    @GetMapping("deleteCommonSearch")
    public Result deleteCommonSearch(@RequestParam("gsmsId") String gsmsId) {
        sdMarketingSearchService.deleteCommonSearch(gsmsId);
        return ResultUtil.success();
    }
}

