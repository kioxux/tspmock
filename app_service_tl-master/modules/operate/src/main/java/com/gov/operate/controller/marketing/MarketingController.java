package com.gov.operate.controller.marketing;


import com.alibaba.fastjson.JSONObject;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.GetGiveawayListToCheckVO;
import com.gov.operate.dto.GetProductListToCheckVO;
import com.gov.operate.dto.GsmTaskListVO2;
import com.gov.operate.dto.SaveGsmTaskVO;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IMarketingTaskService;
import com.gov.operate.service.ISdMarketingBasicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Slf4j
@Api(tags = "营销活动任务")
@RestController
@RequestMapping("marketing/marketing")
public class MarketingController {

    @Resource
    private IMarketingTaskService iMarketingTaskService;
    @Resource
    private ISdMarketingBasicService marketingBasicService;

    @Log("立即执行")
    @ApiOperation(value = "立即执行")
    @PostMapping("execute")
    public Result execute(@RequestBody Map<String, Object> map) {
        log.info("营销立即执行{}", JSONObject.toJSONString(map));
        Integer id = map.get("id") == null ? null : NumberUtils.toInt(map.get("id").toString());
        String clientId = map.get("clientId") == null ? null : map.get("clientId").toString();
        String flowNo = map.get("flowNo") == null ? null : map.get("flowNo").toString();
        String status = map.get("status") == null ? null : map.get("status").toString();
        iMarketingTaskService.execute(id, clientId, flowNo, status);
        return ResultUtil.success();
    }

    @Log("发送审批")
    @ApiOperation(value = "发送审批")
    @PostMapping("approval")
    public Result approval(@RequestJson(value = "id", name = "营销任务ID") Integer id) {
        iMarketingTaskService.approval(id);
        return ResultUtil.success();
    }

    @Log("营销任务列表")
    @ApiOperation(value = "营销任务列表")
    @PostMapping("getGsmTaskList")
    public Result getGsmTaskList(@RequestBody GsmTaskListVO2 vo) {
        return ResultUtil.success(marketingBasicService.getGsmTaskList(vo));
    }

    @Log("营销任务详情")
    @ApiOperation(value = "营销任务详情")
    @GetMapping("getGsmTaskDetail")
    public Result getGsmTaskDetail(@RequestParam("id") Integer id,
                                   @RequestParam("type") Integer type,
                                   @RequestParam(value = "stoCode", required = false) String stoCode) {
        return ResultUtil.success(marketingBasicService.getGsmTaskDetail(id, type, stoCode));
    }

    @Log("营销任务保存")
    @ApiOperation(value = "营销任务保存")
    @PostMapping("saveGsmTask")
    public Result saveGsmTask(@Valid @RequestBody SaveGsmTaskVO vo) {
        marketingBasicService.saveGsmTask(vo);
        return ResultUtil.success();
    }

    @Log("选品选择列表")
    @ApiOperation(value = "选品选择列表")
    @PostMapping("getProductListToCheck")
    public Result getProductListToCheck(@Valid @RequestBody GetProductListToCheckVO vo) {
        return ResultUtil.success(marketingBasicService.getProductListToCheck(vo));
    }

    @Log("赠品选择列表")
    @ApiOperation(value = "赠品选择列表")
    @PostMapping("getGiveawayListToCheck")
    public Result getGiveawayListToCheck(@Valid @RequestBody GetGiveawayListToCheckVO vo) {
        return ResultUtil.success(marketingBasicService.getGiveawayListToCheck(vo));
    }

}

