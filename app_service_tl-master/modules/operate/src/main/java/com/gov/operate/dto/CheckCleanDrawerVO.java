package com.gov.operate.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/14 15:20
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CheckCleanDrawerVO extends AddCleanDrawerVO {

    /**
     * 清斗单号
     */
    private String gschVoucherId;

    /**
     * 公司级参数值
     */
    @NotBlank(message = "checkShop不可为空")
    private String checkShop;


}
