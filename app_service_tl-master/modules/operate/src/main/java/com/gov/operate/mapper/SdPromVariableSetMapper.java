package com.gov.operate.mapper;

import com.gov.operate.entity.SdPromVariableSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
public interface SdPromVariableSetMapper extends BaseMapper<SdPromVariableSet> {

}
