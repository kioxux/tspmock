package com.gov.operate.dto.storeData;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "用户登录，选择加盟商后携带参数")
public class UserLoginModelClient {

    @NotBlank(message = "加盟商不能为空")
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "加盟商名称")
    private String francName;

    @NotBlank(message = "员工编号不能为空")
    @ApiModelProperty(value = "员工编号")
    private String userId;

    @ApiModelProperty(value = "手机号")
    private String userTel;

    @ApiModelProperty(value = "登陆密码")
    private String userPswd;


    @NotBlank(message = "设备类型不能为空")
    @ApiModelProperty(value = "设备类型（android/ios）")
    private String platform;

    @NotBlank(message = "设备号不能为空")
    @ApiModelProperty(value = "设备号")
    private String deviceNo;

}
