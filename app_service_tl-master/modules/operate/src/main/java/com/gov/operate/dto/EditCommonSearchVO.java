package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditCommonSearchVO {

    @NotBlank(message = "查询ID不能为空")
    private String gsmsId;

    @NotBlank(message = "查询明细不能为空")
    private String gsmsDetail;

    @NotBlank(message = "是否常用不能为空 0，常用查询，1,非常用查询")
    private String gsmsNormal;

}
