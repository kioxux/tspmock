package com.gov.operate.dto;

import lombok.Data;

@Data
public class HomeSalesVo {

    private String client;

    /**
     * 数据状态 D:本期 S:上周 T:去年同期
     */
    private String gssdType;


    /**
     * 门店
     */
    private String gssdBrId;


    /**
     * 销售日期
     */
    private String gssdDate;


    private String gcdWeek;

    /**
     * 门店数量
     */
    private String gssdStoreCount = "0";

    /**
     * 销售额
     */
    private String gssdAmt = "0";

    /**
     * 毛利额
     */
    private String gssdGrossAmt = "0";

    /**
     * 毛利率
     */
    private String gssdGrossMar = "0";

    /**
     * 来客数
     */
    private String gssdTrans = "0";

    /**
     * 客单价
     */
    private String gssdAprice = "0";

    /**
     * 本店日均销售额
     */
    private String gssdAmtAvg = "0";
}
