package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.StorePaymentDto;
import com.gov.operate.entity.PayingItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
public interface PayingItemMapper extends BaseMapper<PayingItem> {

    /**
     * 付款组织列表
     *
     * @param client
     * @return
     */
    List<StorePaymentDto> selectPaymentList(@Param("client") String client, @Param("type") String type,
                                            @Param("ficoPayingstoreCode") String ficoPayingstoreCode,
                                            @Param("ficoPayingstoreName") String ficoPayingstoreName,
                                            @Param("validDate") Integer validDate);

    /**
     * 通知付款加盟商
     *
     * @return
     */
    List<StorePaymentDto> selectPayReminder();

    /**
     * 初始化发票信息
     *
     * @param client
     */
    void initInvoice(@Param("client") String client);

    /**
     * 纳税主体名称同步
     *
     * @param client
     */
    void syncInvoice(String client);
}
