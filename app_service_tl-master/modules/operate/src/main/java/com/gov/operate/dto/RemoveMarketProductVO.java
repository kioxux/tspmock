package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class RemoveMarketProductVO {

    @NotBlank(message = "加盟商")
    private String client;

    @NotBlank(message = "营销活动ID")
    private String gsmMarketid;

    @NotBlank(message = "商品编码")
    private String gsmCode;

//    @NotBlank(message = "门店编码")
    private String gsmStore;

}
