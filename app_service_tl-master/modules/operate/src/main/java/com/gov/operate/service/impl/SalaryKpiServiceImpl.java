package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryKpi;
import com.gov.operate.mapper.SalaryKpiMapper;
import com.gov.operate.service.ISalaryKpiService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryKpiServiceImpl extends ServiceImpl<SalaryKpiMapper, SalaryKpi> implements ISalaryKpiService {

    /**
     * 薪资方案员工系数值
     */
    @Override
    public void saveKpi(EditProgramVO vo) {

        Integer gspId = vo.getGspId();
        // 删除薪资方案员工系数值 原始数据
        this.remove(new QueryWrapper<SalaryKpi>().eq("GSK_GSP_ID", gspId));
        List<SalaryKpi> salaryKpiList = vo.getSalaryKpiList();
        if (CollectionUtils.isEmpty(salaryKpiList)) {
            throw new CustomResultException("员工个人系数列表不能为空");
        }
        for (int i = 0; i < salaryKpiList.size(); i++) {
            SalaryKpi salaryKpi = salaryKpiList.get(i);
            if (StringUtils.isEmpty(salaryKpi.getGskKpiMin())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条KPI得分最小值不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryKpi.getGskKpiMax())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条KPI得分最大值不能为空", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryKpi.getGskCoeffi())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]员工个人系数不能为空", i + 1));
            }
            salaryKpi.setGskGspId(gspId);
            salaryKpi.setGskSort(i + 1);
        }
        // 校验范围重叠的问题
        salaryKpiList.sort(Comparator.comparing(SalaryKpi::getGskKpiMin));
        for (int i = 0; i < salaryKpiList.size(); i++) {
            if (i == 0) {
                continue;
            }
            SalaryKpi current = salaryKpiList.get(i);
            SalaryKpi previous = salaryKpiList.get(i - 1);
            if (current.getGskKpiMin().compareTo(previous.getGskKpiMax()) <= 0) {
                throw new CustomResultException(MessageFormat.format("KPI得分区间[{0}~{1}]与[{2}~{3}]存在重叠区,请重新输入",
                        previous.getGskKpiMin(), previous.getGskKpiMax(), current.getGskKpiMin(), current.getGskKpiMax()));
            }
        }
        this.saveBatch(salaryKpiList);
    }

    /**
     * 员工个人系数列表
     */
    @Override
    public List<SalaryKpi> getKpiList(Integer gspId) {
        List<SalaryKpi> list = this.list(new QueryWrapper<SalaryKpi>()
                .eq("GSK_GSP_ID", gspId)
                .orderByAsc("GSK_SORT"));
        return list;
    }
}
