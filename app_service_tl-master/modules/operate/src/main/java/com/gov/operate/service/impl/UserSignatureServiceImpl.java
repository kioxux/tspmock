package com.gov.operate.service.impl;

import com.gov.common.basic.CosUtils;
import com.gov.common.entity.FileResult;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.UserSignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.gov.common.basic.CommonEnum.CosPaht.USERSIGN;

@Service
public class UserSignatureServiceImpl implements UserSignatureService {

    @Autowired
    CosUtils cosUtils;
    @Autowired
    UserDataMapper userDataMapper;

    @Override
    public String uploadSignature(MultipartFile userSignature, String client, String userId) {
        //文件名修改为时间_userId格式
        String fileName = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS").format(LocalDateTime.now()) + "_" + userId;
        //上传签名
        Result result = cosUtils.uploadFileToTargetPath(userSignature, USERSIGN.getMessage() + client + "/" + fileName);
        //若出错，直接返回提交失败
        if (ResultUtil.hasError(result)) {
            return "";
        }
        return ((FileResult) result.getData()).getUrl();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUserElectronicSignatureData(String signatureUrl, String client, String userId) {
        return userDataMapper.updateUserElectronicSignatureData(signatureUrl, client, userId) == 1;
    }
}
