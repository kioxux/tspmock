package com.gov.operate.service;

import com.gov.operate.entity.SdIndrawerD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface ISdIndrawerDService extends SuperService<SdIndrawerD> {

}
