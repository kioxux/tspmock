package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetStoreListVO {

    private String keyLike;

    private String gssgId;

    private String stoDistrict;
}
