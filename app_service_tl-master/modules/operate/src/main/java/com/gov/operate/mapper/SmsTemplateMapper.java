package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.GetMemberDO;
import com.gov.operate.entity.SmsTemplate;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-13
 */
public interface SmsTemplateMapper extends BaseMapper<SmsTemplate> {

    /**
     * 获取对应用户对应门店的手机号
     * @param client    加盟商
     * @param memberId  会员id
     * @param memberCard 会员卡
     * @param storeCode 所属门店
     * @return
     */
    GetMemberDO getMember(@Param("client") String client,
                          @Param("memberId") String memberId,
                          @Param("memberCard") String memberCard,
                          @Param("storeCode") String storeCode);
}
