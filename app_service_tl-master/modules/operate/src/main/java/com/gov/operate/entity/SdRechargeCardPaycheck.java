package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_RECHARGE_CARD_PAYCHECK")
@ApiModel(value="SdRechargeCardPaycheck对象", description="")
public class SdRechargeCardPaycheck extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "储值卡账户")
    @TableField("GSRCP_ACCOUNT_ID")
    private String gsrcpAccountId;

    @ApiModelProperty(value = "充值单号")
    @TableField("GSRCP_VOUCHER_ID")
    private String gsrcpVoucherId;

    @ApiModelProperty(value = "充值卡号")
    @TableField("GSRCP_CARD_ID")
    private String gsrcpCardId;

    @ApiModelProperty(value = "充值门店")
    @TableField("GSRCP_BR_ID")
    private String gsrcpBrId;

    @ApiModelProperty(value = "充值日期")
    @TableField("GSRCP_DATE")
    private String gsrcpDate;

    @ApiModelProperty(value = "充值时间")
    @TableField("GSRCP_TIME")
    private String gsrcpTime;

    @ApiModelProperty(value = "充值人员")
    @TableField("GSRCP_EMP")
    private String gsrcpEmp;

    @ApiModelProperty(value = "应收金额")
    @TableField("GSRCP_YS_AMT")
    private BigDecimal gsrcpYsAmt;

    @ApiModelProperty(value = "充值金额")
    @TableField("GSRCP_AFTER_AMT")
    private BigDecimal gsrcpAfterAmt;

    @ApiModelProperty(value = "是否日结 0/空为否1为是")
    @TableField("GSRCP_DAYREPORT")
    private String gsrcpDayreport;

    @ApiModelProperty(value = "原充值单号")
    @TableField("GSRCP_RETURN")
    private String gsrcpReturn;

    @ApiModelProperty(value = "充值类型（0或空为正常充值，1为退款）")
    @TableField("GSRCP_FLAG")
    private String gsrcpFlag;
}
