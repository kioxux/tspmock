package com.gov.operate.service.response;

import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectQuerySusjobMemberResponse{

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "序号")
    private String susNumber;

    @ApiModelProperty(value = "会员名称")
    private String susMenname;

    @ApiModelProperty(value = "会员ID")
    private String gsmbMemberId;

    @ApiModelProperty(value = "性别")
    private String susSex;

    @ApiModelProperty(value = "年龄")
    private String age;

    @ApiModelProperty(value = "手机号")
    private String susMobile;


    @ApiModelProperty(value = "会员回访类型0待回访1已接听2无应答3已拦截")
    private String susStatus;

}
