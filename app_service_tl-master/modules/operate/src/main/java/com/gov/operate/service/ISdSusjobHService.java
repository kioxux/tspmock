package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.CreateSusjobVO;
import com.gov.operate.dto.GetSusjobListVO;
import com.gov.operate.dto.SdSusjobHDTO;
import com.gov.operate.entity.SdSusjobH;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
public interface ISdSusjobHService extends SuperService<SdSusjobH> {

    /**
     * 创建维系任务
     */
    void createSusjob(CreateSusjobVO vo);

    /**
     * 维系任务列表
     */
    IPage<SdSusjobHDTO> getSusjobList(GetSusjobListVO vo);

    void updatePushStatus();

    void updateTaskStatus();

    void pushMessage(String param);
}
