package com.gov.operate.service.ssp.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.ssp.SspLogPageDTO;
import com.gov.operate.entity.SspLog;
import com.gov.operate.mapper.SspLogMapper;
import com.gov.operate.service.ssp.ISspLogService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SspLogServiceImpl extends ServiceImpl<SspLogMapper, SspLog>
        implements ISspLogService {

    @Override
    public Result selectLogByPage(SspLogPageDTO sspLogPageDTO) {
        Page<SspLog> page = new Page<>(sspLogPageDTO.getPageNum(), sspLogPageDTO.getPageSize());
        return ResultUtil.success(baseMapper.selectUserByPage(page, sspLogPageDTO));
    }
}




