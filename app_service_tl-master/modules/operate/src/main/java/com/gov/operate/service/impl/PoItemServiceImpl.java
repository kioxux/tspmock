package com.gov.operate.service.impl;

import com.gov.operate.entity.PoItem;
import com.gov.operate.mapper.PoItemMapper;
import com.gov.operate.service.IPoItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 采购订单明细表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
@Service
public class PoItemServiceImpl extends ServiceImpl<PoItemMapper, PoItem> implements IPoItemService {

}
