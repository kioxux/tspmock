package com.gov.operate.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdRechargeCard;
import com.gov.operate.entity.StoreData;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface ISdRechargeCardService extends SuperService<SdRechargeCard> {

    MemberDTO getMemberCard(MemberNoVO vo);

    SdRechargeCard addRecharge(AddRechargeVO vo);

    Result getRechargeCardList(RechargeCardListVO entity, Page<SdRechargeCard> page);

    Result resetPassword(UpdateRechargeCardVO vo);

    Result changePassword(UpdateRechargeCardPasswordVO vo);

    Result reportLoss(UpdateRechargeCardVO vo);

    Result hangingSolutions(UpdateRechargeCardVO vo);

    Result changeCard(UpdateRechargeCardVO vo);

    StoreData common();

    /**
     * 储值卡编辑
     *
     * @param sdRechargeCard
     */
    void editRecharte(SdRechargeCard sdRechargeCard);

    /**
     * 充值门店
     *
     * @return
     */
    Result getRechargeStoreList();
}
