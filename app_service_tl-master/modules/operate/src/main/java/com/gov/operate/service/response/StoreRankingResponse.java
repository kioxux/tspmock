package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreRankingResponse {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "实收金额")
    private String actualAmount;

    @ApiModelProperty(value = "门店")
    private String store;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    @ApiModelProperty(value = "成本额")
    private String costAmount;

    @ApiModelProperty(value = "交易次数")
    private String tradeCount;

    @ApiModelProperty(value = "商品编码计数之和")
    private String productCount;

    @ApiModelProperty(value = "销售量")
    private String qtyCount;

    @ApiModelProperty(value = "销售天数")
    private String salesDays;
}
