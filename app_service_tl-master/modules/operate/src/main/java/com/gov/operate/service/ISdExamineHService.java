package com.gov.operate.service;

import com.gov.operate.entity.SdExamineH;
import com.gov.mybatis.SuperService;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
public interface ISdExamineHService extends SuperService<SdExamineH> {


}
