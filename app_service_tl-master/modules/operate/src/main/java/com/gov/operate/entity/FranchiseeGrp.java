package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FRANCHISEE_GRP")
@ApiModel(value="FranchiseeGrp对象", description="")
public class FranchiseeGrp extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "加盟商组ID")
    @TableField("FRANC_GRPID")
    private String francGrpid;

    @ApiModelProperty(value = "加盟商组名称")
    @TableField("FRANC_GRPNAME")
    private String francGrpname;

    @ApiModelProperty(value = "加盟商名称")
    @TableField("FRANC_NAME")
    private String francName;

    @ApiModelProperty(value = "详细地址")
    @TableField("FRANC_ADDR")
    private String francAddr;

    @ApiModelProperty(value = "创建日期")
    @TableField("FRANC_CRE_DATE")
    private String francCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("FRANC_CRE_TIME")
    private String francCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("FRANC_CRE_ID")
    private String francCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("FRANC_MODI_DATE")
    private String francModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("FRANC_MODI_TIME")
    private String francModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("FRANC_MODI_ID")
    private String francModiId;

    @ApiModelProperty(value = "省份")
    @TableField("FRANC_PROV")
    private String francProv;

    @ApiModelProperty(value = "地市")
    @TableField("FRANC_CITY")
    private String francCity;


}
