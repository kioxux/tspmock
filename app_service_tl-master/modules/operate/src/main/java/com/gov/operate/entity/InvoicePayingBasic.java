package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_INVOICE_PAYING_BASIC")
@ApiModel(value="InvoicePayingBasic对象", description="")
public class InvoicePayingBasic extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "组织")
    @TableField("FICO_COMPANY_CODE")
    private String ficoCompanyCode;

    @ApiModelProperty(value = "纳税主体编码")
    @TableField("FICO_TAX_SUBJECT_CODE")
    private String ficoTaxSubjectCode;

    @ApiModelProperty(value = "纳税主体名称")
    @TableField("FICO_TAX_SUBJECT_NAME")
    private String ficoTaxSubjectName;

    @ApiModelProperty(value = "发票类型")
    @TableField("FICO_INVOICE_TYPE")
    private String ficoInvoiceType;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("FICO_SOCIAL_CREDIT_CODE")
    private String ficoSocialCreditCode;

    @ApiModelProperty(value = "开户行名称")
    @TableField("FICO_BANK_NAME")
    private String ficoBankName;

    @ApiModelProperty(value = "开户行账号")
    @TableField("FICO_BANK_NUMBER")
    private String ficoBankNumber;

    @ApiModelProperty(value = "地址")
    @TableField("FICO_COMPANY_ADDRESS")
    private String ficoCompanyAddress;

    @ApiModelProperty(value = "电话号码")
    @TableField("FICO_COMPANY_PHONE_NUMBER")
    private String ficoCompanyPhoneNumber;

    @ApiModelProperty(value = "付款银行名称")
    @TableField("FICO_PAYINGBANK_NAME")
    private String ficoPayingbankName;

    @ApiModelProperty(value = "付款银行账号")
    @TableField("FICO_PAYINGBANK_NUMBER")
    private String ficoPayingbankNumber;


}
