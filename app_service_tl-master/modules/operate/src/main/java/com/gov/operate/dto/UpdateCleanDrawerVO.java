package com.gov.operate.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/14 15:20
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateCleanDrawerVO extends AddCleanDrawerVO {

    @NotBlank(message = "清斗单号不能为空")
    private String gschVoucherId;


}
