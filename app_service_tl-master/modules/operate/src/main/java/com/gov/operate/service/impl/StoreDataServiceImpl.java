package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.userutil.UserUtils;
import com.gov.operate.dto.*;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.dto.storeData.StoreDataAddVO;
import com.gov.operate.dto.storeData.StoreDataCodeDTO;
import com.gov.operate.dto.storeData.StoreDataDTO;
import com.gov.operate.dto.storeData.StoreDataEditVO;
import com.gov.operate.entity.*;
import com.gov.operate.enums.StoreEnum;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.kylin.RowMapper;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.operate.service.request.StoreSalesRankingRequest;
import com.gov.operate.service.request.StoreSalesStatisticsRequest;
import com.gov.operate.service.response.*;
import com.gov.operate.storegroup.CSVReadUtil;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@Slf4j
@Service
public class StoreDataServiceImpl extends ServiceImpl<StoreDataMapper, StoreData> implements IStoreDataService {

    private static final Map<String, List<StoreSalesStatisticsItemIndex>> STATISTICS_DIMENSION_MAP = new HashMap<>();

    static {
        STATISTICS_DIMENSION_MAP.put("销售额", new ArrayList<StoreSalesStatisticsItemIndex>());
        STATISTICS_DIMENSION_MAP.put("毛利额", new ArrayList<StoreSalesStatisticsItemIndex>());
        STATISTICS_DIMENSION_MAP.put("毛利率", new ArrayList<StoreSalesStatisticsItemIndex>());
        STATISTICS_DIMENSION_MAP.put("日均销售额（元）", new ArrayList<StoreSalesStatisticsItemIndex>());
        STATISTICS_DIMENSION_MAP.put("单店日均销售额（元）", new ArrayList<StoreSalesStatisticsItemIndex>());
        STATISTICS_DIMENSION_MAP.put("单店日均交易", new ArrayList<StoreSalesStatisticsItemIndex>());
        STATISTICS_DIMENSION_MAP.put("客单价", new ArrayList<StoreSalesStatisticsItemIndex>());
    }

    @Resource
    private CommonService commonService;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private IUserDataService userDataService;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Resource
    private ISmsTemplateService smsTemplateService;
    @Resource
    private IAuthconfiDataService authconfiService;
    @Resource
    private IMessageService messageService;
    @Resource
    private IUserRoleService userRoleService;
    @Resource
    private SdStoresGroupSetMapper sdStoresGroupSetMapper;
    @Resource
    private SdStoresGroupMapper sdStoresGroupMapper;
    @Resource
    private GaiaRegionalStoreMapper regionalStoreMapper;
    @Resource
    private GaiaRegionalPlanningModelMapper planningModelMapper;
    @Resource
    private IStoreDataService iStoreDataService;
    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;
    @Autowired
    private RedisClient redisClient;
    private final String AREA_KEY = "AREA";

    /**
     * 当前人门店列表
     */
    @Override
    public Result getCurrentStoreList() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = baseMapper.getCurrentStoreList(user.getClient(), user.getUserId());
        return ResultUtil.success(list);
    }

    /**
     * 当前加盟商下所有的门店
     */
    @Override
    public List<StoreData> getStoreList() {
        TokenUser user = commonService.getLoginInfo();
        return this.list(new QueryWrapper<StoreData>()
                .select("STO_CODE", "STO_NAME", "STO_SHORT_NAME")
                .eq("CLIENT", user.getClient()));
    }

    /**
     * 通过加盟商 + 门店编码 获取门店
     *
     * @param client    加盟商
     * @param storeCode 门店编码
     * @return
     */
    private StoreData getStoreByClientAndCode(String client, String storeCode) {
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STO_CODE", storeCode);
        StoreData storeData = this.getOne(queryWrapper);
        return storeData;
    }

    /**
     * 获取门店编码
     *
     * @return
     */
    @Override
    public StoreDataCodeDTO getStoreCode() {
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = storeDataMapper.getMaxNumStoreCode(user.getClient());
        if (StringUtils.isNotEmpty(storeData)) {
            Integer codeInteger = Integer.valueOf(storeData.getStoCode()) + 1;
            return new StoreDataCodeDTO(codeInteger.toString());
        }
        return new StoreDataCodeDTO("10000");
    }

    /**
     * 门店详情
     *
     * @param stoCode 门店编码
     */
    @Override
    public StoreDataDTO getStoreInfo(String stoCode) {
        TokenUser tokenUser = commonService.getLoginInfo();
        StoreData storeData = getStoreByClientAndCode(tokenUser.getClient(), stoCode);

        StoreDataDTO storeDataDTO = new StoreDataDTO();
        BeanUtils.copyProperties(storeData, storeDataDTO);
        storeDataDTO.setAddress(storeData.getStoAdd());
        //查询门店-区域关联连表和区域规划表：获取一二三级区域的ID
        //一级区域
        Long firstAreaId = this.regionalStoreMapper.findFirstAreaId(storeData.getStoCode(), tokenUser.getClient());
        storeDataDTO.setFristArea(firstAreaId);
        if (firstAreaId != null) {
            //二级区域
            Long secondAreaId = this.planningModelMapper.findRegionalId(firstAreaId);
            storeDataDTO.setSecondArea(secondAreaId);
            if (secondAreaId != null) {
                //三级区域
                Long thirdAreaId = this.planningModelMapper.findRegionalId(secondAreaId);
                storeDataDTO.setThirdArea(thirdAreaId);
            }
        }

        storeDataDTO.setLogoUrl(cosUtils.urlAuth(storeData.getStoLogo()));
        // 法人
        UserData user = userDataService.getUserByClientAndUserId(tokenUser.getClient(), storeData.getStoLegalPerson());
        if (StringUtils.isNotEmpty(user)) {
            storeDataDTO.setStoLegalPersonName(user.getUserNam());
        }

        // 质量负责人
        user = userDataService.getUserByClientAndUserId(tokenUser.getClient(), storeData.getStoQua());
        if (StringUtils.isNotEmpty(user)) {
            storeDataDTO.setStoQuaName(user.getUserNam());
            storeDataDTO.setStoQuaPhone(user.getUserTel());
        }

        // 店长
        user = userDataService.getUserByClientAndUserId(tokenUser.getClient(), storeData.getStoLeader());
        if (StringUtils.isNotEmpty(user)) {
            storeDataDTO.setStoLeaderName(user.getUserNam());
            storeDataDTO.setStoLeaderPhone(user.getUserTel());
        }

        // 门店组
//        StogData stogData = stogDataService.getNameByCode(tokenUser.getClient(), storeData.getStogCode());
//        if (StringUtils.isNotEmpty(stogData)) {
//            storeDataDTO.setStogName(stogData.getStogName());
//        }

        return storeDataDTO;
    }

    /**
     * 门店新增
     */
    @Override
    @Transactional
    public void addStore(StoreDataAddVO storeDataAddVO, TokenUser tokenUser) {
        if (tokenUser == null) {
            tokenUser = commonService.getLoginInfo();
        }
        // 校验门店编码是否被占用
        StoreData storeByClientAndCode = getStoreByClientAndCode(tokenUser.getClient(), storeDataAddVO.getStoCode());
        if (StringUtils.isNotEmpty(storeByClientAndCode)) {
            throw new CustomResultException(ResultEnum.E0015);
        }

        // 门店属性是连锁，则连锁总部以及纳税主体不能为空
//        if (CommonEnum.StoreType.CHAIN_STORE.getCode().equals(storeDataAddVO.getStoAttribute())) {
//            if (StringUtils.isEmpty(storeDataAddVO.getStoChainHead()) || StringUtils.isEmpty(storeDataAddVO.getStoTaxSubject())) {
//                throw new CustomResultException(ResultEnum.E0023);
//            }
//        }
        String userLeaderId = "";
        String userQuaId = "";
        StoreData storeData = new StoreData();
        BeanUtils.copyProperties(storeDataAddVO, storeData);
        if (StringUtils.isNotEmpty(storeDataAddVO.getStoQuaPhone())) {
            //如果店长和质量负责人是同一人
            if (storeDataAddVO.getStoLeaderPhone().equals(storeDataAddVO.getStoQuaPhone())) {
                userLeaderId = getUserId(tokenUser, storeDataAddVO, null, storeDataAddVO.getPlatform());
            } else {
                userLeaderId = getUserId(tokenUser, storeDataAddVO, CommonEnum.UserType.STATUS_LEADER.getCode(), storeDataAddVO.getPlatform());
                userQuaId = getUserId(tokenUser, storeDataAddVO, CommonEnum.UserType.STATUS_QUA.getCode(), storeDataAddVO.getPlatform());

            }
        } else {
            userLeaderId = getUserId(tokenUser, storeDataAddVO, null, storeDataAddVO.getPlatform());
        }

        /**
         * 插入店长角色权限关联(手机)
         */
        Result result = userRoleService.createUserRole(tokenUser.getClient(), CommonEnum.garFunc.SHOP_OWNER.getCode(), userLeaderId);
        if (!StringUtils.equals(result.getCode(), "0")) {
            throw new CustomResultException(result.getMessage());
        }
        //店长
        storeData.setStoLeader(userLeaderId);

        /**
         * 插入质量负责人角色权限关联(手机)
         */
        if (StringUtils.isNotEmpty(userQuaId)) {
            Result result1 = userRoleService.createUserRole(tokenUser.getClient(), CommonEnum.garFunc.QUALITY.getCode(), userQuaId);
            if (!StringUtils.equals(result1.getCode(), "0")) {
                throw new CustomResultException(result1.getMessage());
            }
        }


        //质量负责人
        storeData.setStoQua(userQuaId);
        // 门店状态
//        storeData.setStoStatus(CommonEnum.StoStatus.STO_STATUS_YES.getCode());
        storeData.setStoStatus(storeDataAddVO.getStoStatus());

        // 加盟商
        storeData.setClient(tokenUser.getClient());
        // 创建日期
        storeData.setStoCreDate(DateUtils.getCurrentDateStrYYMMDD());
        // 创建时间
        storeData.setStoCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        // 创建人id
        storeData.setStoCreId(tokenUser.getUserId());
        //门店简称
        storeData.setStoShortName(storeDataAddVO.getStoShortName());
        //行政区域：省、市、区
        storeData.setStoProvince(storeDataAddVO.getStoPrivince());
        storeData.setStoArea(storeDataAddVO.getStoCity());
        storeData.setStoDistrict(storeDataAddVO.getStoDistrict());
        storeData.setStoAdd(storeDataAddVO.getAddress());
        this.save(storeData);

        //保存到门店区域信息：GAIA_REGIONAL_STORE
        saveRegionStoreInfo(storeDataAddVO, tokenUser);

        //添加门店分类的默认配置
        setDefaultStoGroupSet(tokenUser.getClient(), storeDataAddVO.getStoCode(), storeDataAddVO.getStoAttribute());


        //判断操作平台 his不推送消息
        if (!"his".equals(storeDataAddVO.getPlatform())) {
            /**
             * 消息推送
             */
            try {
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(storeData.getClient());
                messageParams.setUserId(userLeaderId);
                messageParams.setId(CommonEnum.gmtId.MSG00003.getCode());
                messageParams.setContentParmas(new ArrayList<String>() {{
                    add(storeData.getStoName());
                }});
                messageService.sendMessage(messageParams);
                log.info("新增门店推送成功");
            } catch (Exception e) {
                log.error("新增门店推送时异常", e);
            }
        }
    }

    private void saveRegionStoreInfo(StoreDataAddVO storeDataAddVO, TokenUser tokenUser) {
        try {
            GaiaRegionalStore regionalStore = new GaiaRegionalStore();
            //加盟商
            regionalStore.setClient(tokenUser.getClient());
            //区域id
            regionalStore.setRegionalId(storeDataAddVO.getFristArea() == null ? planningModelMapper.getDefaultIdByClient(tokenUser.getClient()) : storeDataAddVO.getFristArea());
            //门店ID
            regionalStore.setStoCode(storeDataAddVO.getStoCode());
            //门店简称
            regionalStore.setStoName(storeDataAddVO.getStoShortName());
            //排序编码
            Integer sortNumber = this.regionalStoreMapper.getMaxSortNumber(storeDataAddVO.getFristArea() == null ? planningModelMapper.getDefaultIdByClient(tokenUser.getClient()) : storeDataAddVO.getFristArea(), tokenUser.getClient());
            sortNumber = sortNumber == null ? 0 : (sortNumber + 1);
            regionalStore.setSortNum(sortNumber);
            //创建人
            regionalStore.setCreateUser(tokenUser.getUserId());
            //创建时间
            regionalStore.setCreateTime(new Date());
            //删除状态
            regionalStore.setDeleteFlag("0");
            this.regionalStoreMapper.insert(regionalStore);
        } catch (Exception e) {
            log.error("<公司维护><门店维护><新增门店-保存门店区域关系异常：{}>", e.getMessage(), e);
        }
    }


    /**
     * 判断手机号是否已经注册 如果存在直接拿编码 不存则在创建角色
     *
     * @param tokenUser
     * @param storeDataAddVO
     * @param code
     * @return
     */
    @Transactional
    public String getUserId(TokenUser tokenUser, StoreDataAddVO storeDataAddVO, String code, String platform) {
        String userId = "";
        UserData userData;
        List<String> groupList = new ArrayList();
        if (CommonEnum.UserType.STATUS_QUA.getCode().equals(code)) {    //质量负责人
            userData = userDataService.getUserByClientAndPhone(tokenUser.getClient(), storeDataAddVO.getStoQuaPhone());
            groupList.add("GAIA_FI_ZG");
        } else {
            userData = userDataService.getUserByClientAndPhone(tokenUser.getClient(), storeDataAddVO.getStoLeaderPhone());
            groupList.add("SD_01");
        }

        String passWord = String.valueOf(new Random().nextInt(900000) + 100000);
        if (StringUtils.isEmpty(userData)) {
            userData = new UserData();
            int userMax = this.userDataService.selectMaxId(tokenUser.getClient());
            userData.setClient(tokenUser.getClient());
            userData.setDepId(storeDataAddVO.getStoCode()); //部门 改为门店
            userData.setDepName(storeDataAddVO.getStoName());
            userData.setUserId(String.valueOf(userMax + 1));
            if (CommonEnum.UserType.STATUS_QUA.getCode().equals(code)) {    //质量负责人
                userData.setUserNam(storeDataAddVO.getStoQuaName());
                userData.setUserTel(storeDataAddVO.getStoQuaPhone());
            } else {
                userData.setUserNam(storeDataAddVO.getStoLeaderName());
                userData.setUserTel(storeDataAddVO.getStoLeaderPhone());
            }
            userData.setUserPassword(UserUtils.encryptMD5(passWord));
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserJoinDate(DateUtils.getCurrentDateStrYYMMDD()); //入职时间
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(tokenUser.getUserId());
            userData.setUserCreId(tokenUser.getUserId());
            this.userDataService.insert(userData);
            userId = String.valueOf(userMax + 1);
            //给店长发短信
            if (!"his".equals(platform)) {
                this.sendSms(storeDataAddVO, passWord);
            }
        } else {
            userId = userData.getUserId();
            userData.setDepId(storeDataAddVO.getStoCode()); //部门 改为门店
            userData.setDepName(storeDataAddVO.getStoName());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(tokenUser.getUserId());
            UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("CLIENT", tokenUser.getClient());
            updateWrapper.eq("USER_ID", userId);
            this.userDataService.update(userData, updateWrapper);
            StoreDataEditVO storeDataEditVO = new StoreDataEditVO();
            storeDataEditVO.setClient(userData.getClient());
            storeDataEditVO.setStoLegalPerson(userData.getUserId());
            storeDataEditVO.setStoName(storeDataAddVO.getStoName());
            //给店长发短信
            if (!"his".equals(platform)) {
                this.sendUpdateSms(storeDataEditVO.getClient(), storeDataEditVO.getStoLegalPerson(), storeDataEditVO.getStoName());
            }
        }
        List<String> siteList = new ArrayList<>();
        siteList.add(storeDataAddVO.getStoCode());
        this.saveAuthConfig(tokenUser.getClient(), userData.getUserId(), siteList, userData.getUserId(), groupList);

        return userId;
    }

    @Transactional
    public void saveAuthConfig(String client, String userId, List<String> siteList, String delUserId, List groupList) {

        this.authconfiService.saveAuth(client, userId, groupList, siteList, delUserId);
    }

    public void sendSms(StoreDataAddVO storeDataAddVO, String passWord) {
        Verification verification = new Verification(storeDataAddVO.getStoLeaderPhone(), "2", storeDataAddVO.getStoName(), passWord);
        smsTemplateService.sendStoreRegisterSms(verification);
    }

    /**
     * 门店编辑
     *
     * @param storeDataEditVO 入参实体
     */
    @Override
    @Transactional
    public void editStore(StoreDataEditVO storeDataEditVO, TokenUser tokenUser) {
        if (tokenUser == null) {
            tokenUser = commonService.getLoginInfo();
        }
        List<String> groupList = new ArrayList();
        List<String> siteList = new ArrayList<>();
        StoreData exitStoreData = getStoreByClientAndCode(tokenUser.getClient(), storeDataEditVO.getStoCode());
        if (StringUtils.isEmpty(exitStoreData)) {
            throw new CustomResultException(ResultEnum.E0016);
        }
        // 门店属性是连锁，纳税主体不能为空
//        if (CommonEnum.StoreType.CHAIN_STORE.getCode().equals(storeDataEditVO.getStoAttribute())) {
//            if (StringUtils.isEmpty(storeDataEditVO.getStoTaxSubject())) {
//                throw new CustomResultException("纳税主体不能为空");
//            }
//        }
        storeDataEditVO.setClient(tokenUser.getClient());
        StoreData storeData = new StoreData();
        BeanUtils.copyProperties(storeDataEditVO, storeData);
        storeData.setStoModiDate(DateUtils.getCurrentDateStrYYMMDD());
        storeData.setStoModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        storeData.setStoModiId(tokenUser.getUserId());
        //省
        storeData.setStoProvince(storeDataEditVO.getStoPrivince());
        storeData.setStoAdd(storeDataEditVO.getAddress());
        UpdateWrapper<StoreData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", tokenUser.getClient());
        updateWrapper.eq("STO_CODE", storeDataEditVO.getStoCode());
        this.update(storeData, updateWrapper);
        //区域更改时，更新门店区域表：
        Long firstArea = this.regionalStoreMapper.findFirstAreaId(storeData.getStoCode(), tokenUser.getClient());
        if (firstArea != null) {
            if (!firstArea.equals(storeDataEditVO.getFristArea())) {
                updateRegionStoreInfo(storeDataEditVO, tokenUser);
            }
        }
//        storeDataEditVO.setStoLegalPerson(exitStoreData.getStoLeader());
        if (!exitStoreData.getStoLeader().equals(storeData.getStoLeader())) {
            //改用户部门
            this.userDataService.editDepById(storeDataEditVO.getStoCode(), storeDataEditVO.getStoName(), storeDataEditVO.getStoLeader());
            groupList.add("SD_01");
            siteList.add(storeDataEditVO.getStoCode());
            //改用户权限
            this.saveAuthConfig(tokenUser.getClient(), storeData.getStoLeader(), siteList, exitStoreData.getStoLeader(), groupList);
            this.sendUpdateSms(storeDataEditVO.getClient(), storeData.getStoLeader(), storeDataEditVO.getStoName());

            /**
             * 更新店长角色权限关联(手机)
             */
            Result result = userRoleService.createUserRole(tokenUser.getClient(), CommonEnum.garFunc.SHOP_OWNER.getCode(), storeData.getStoLeader(), exitStoreData.getStoLeader());
            if (!StringUtils.equals(result.getCode(), "0")) {
                throw new CustomResultException(result.getMessage());
            }

        }
        if (StringUtils.isNotEmpty(exitStoreData.getStoQua()) && !exitStoreData.getStoQua().equals(storeData.getStoQua())) {
            groupList.add("GAIA_FI_ZG");
            this.userDataService.editDepById(storeDataEditVO.getStoCode(), storeDataEditVO.getStoName(), storeDataEditVO.getStoQua());
            siteList.add(storeDataEditVO.getStoCode());
            this.saveAuthConfig(tokenUser.getClient(), storeData.getStoQua(), siteList, exitStoreData.getStoQua(), groupList);


            /**
             * 更新质量负责人权限关联(手机)
             */
            Result result = userRoleService.createUserRole(tokenUser.getClient(), CommonEnum.garFunc.QUALITY.getCode(), storeData.getStoQua(), exitStoreData.getStoQua());
            if (!StringUtils.equals(result.getCode(), "0")) {
                throw new CustomResultException(result.getMessage());
            }

        }

        //添加门店分类的默认配置
        setDefaultStoGroupSet(tokenUser.getClient(), storeDataEditVO.getStoCode(), storeDataEditVO.getStoAttribute());

        if (!"his".equals(storeDataEditVO.getPlatform())) {
            /**
             * 消息推送
             */
            if (!exitStoreData.getStoLeader().equals(storeData.getStoLeader())) {
                try {
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(storeData.getClient());
                    messageParams.setUserId(storeData.getStoLeader());
                    messageParams.setId(CommonEnum.gmtId.MSG00003.getCode());
                    messageParams.setContentParmas(new ArrayList<String>() {{
                        add(storeData.getStoName());
                    }});
                    messageService.sendMessage(messageParams);
                    log.info("编辑门店推送成功");
                } catch (Exception e) {
                    log.error("编辑门店推送时异常", e);
                }
            }
        }
    }

    /**
     * 门店编辑，门店-区域表更新
     *
     * @param storeDataEditVO
     * @param tokenUser
     */
    public void updateRegionStoreInfo(StoreDataEditVO storeDataEditVO, TokenUser tokenUser) {
        try {
            //判断排序编码：更新原区域修改门店之后的门店的排序编码
            int currentSortNumber = this.regionalStoreMapper.getCurrentSortNumber(tokenUser.getClient(), storeDataEditVO.getStoCode());
            //原区域门店的最大排序编号
            int lastMaxSortNumber = this.regionalStoreMapper.getLastMaxSortNumber(tokenUser.getClient(), storeDataEditVO.getStoCode());
            List<GaiaRegionalStore> regionalStores = this.regionalStoreMapper.findRegionalList(tokenUser.getClient(), storeDataEditVO.getStoCode());
            if (currentSortNumber < lastMaxSortNumber) {
                for (int i = currentSortNumber + 1; i <= lastMaxSortNumber; i++) {
                    for (GaiaRegionalStore store : regionalStores) {
                        if (i == store.getSortNum() && i > currentSortNumber) {
                            GaiaRegionalStore regionalStore = new GaiaRegionalStore();
                            regionalStore.setSortNum(i - 1);
                            UpdateWrapper<GaiaRegionalStore> wrapper = new UpdateWrapper<>();
                            wrapper.eq("CLIENT", tokenUser.getClient());
                            wrapper.eq("STO_CODE", store.getStoCode());
                            this.regionalStoreMapper.update(regionalStore, wrapper);
                        }
                    }
                }
            }
            GaiaRegionalStore regionalStore = new GaiaRegionalStore();
            //区域id
            regionalStore.setRegionalId(storeDataEditVO.getFristArea() == null ? planningModelMapper.getDefaultIdByClient(tokenUser.getClient()) : storeDataEditVO.getFristArea());
            //门店简称
            regionalStore.setStoName(storeDataEditVO.getStoShortName());
            //新的区域的最大排序编码
            Integer sortMaxNumber = this.regionalStoreMapper.getMaxSortNumber(storeDataEditVO.getFristArea() == null ? planningModelMapper.getDefaultIdByClient(tokenUser.getClient()) : storeDataEditVO.getFristArea(), tokenUser.getClient());
            sortMaxNumber = sortMaxNumber == null ? 0 : (sortMaxNumber + 1);
            regionalStore.setSortNum(sortMaxNumber);
            //更新人
            regionalStore.setUpdateUser(tokenUser.getUserId());
            //更新时间
            regionalStore.setUpdateTime(new Date());
            UpdateWrapper<GaiaRegionalStore> wrapper = new UpdateWrapper<>();
            wrapper.eq("ClIENT", tokenUser.getClient());
            wrapper.eq("STO_CODE", storeDataEditVO.getStoCode());
            this.regionalStoreMapper.update(regionalStore, wrapper);
        } catch (Exception e) {
            log.error("<公司维护><门店维护><门店编辑-更新门店区域关系异常：{}>", e.getMessage(), e);
        }
    }

    /**
     * 查询所有一级区域
     */
    public List<RegionalPlanningModel> getFristArea() {
        TokenUser tokenUser = commonService.getLoginInfo();
        String client = tokenUser.getClient();
        return this.planningModelMapper.getFristArea(client);
    }

    /**
     * 根据子级区域查询上级区域
     */
    @Override
    public RegionalPlanningModel getRegionalIdByRegional(RegionalPlanningModel regionalPlanningModel) {
        TokenUser tokenUser = commonService.getLoginInfo();
        String client = tokenUser.getClient();
        String regionalCode = regionalPlanningModel.getRegionalCode();
        return this.planningModelMapper.getRegionalIdByRegional(regionalCode, client);
    }

    /**
     * 行政区域
     */
    @Override
    public List<GaiaArea> findAreaByLevel() {
        String s = redisClient.get(AREA_KEY);
        if (StringUtils.isNotEmpty(s)) {
            return JSON.parseObject(s, List.class);
        }
        List<GaiaArea> areaList = planningModelMapper.findAreaByLevel("1");
        selectGaiaArea(areaList);
        redisClient.set(AREA_KEY, JSON.toJSONString(areaList), 3600 * 24 * 30);
        return areaList;
    }

    /**
     * 市
     */
//    @Override
//    public List<GaiaArea> findAreaByParentId(GaiaArea area){
//        List<GaiaArea> areaList = planningModelMapper.findAreaByParentId(area.getAreaId());
//        selectGaiaArea(area.getAreaId(),areaList);
//        return areaList;
//    }
    public void selectGaiaArea(List<GaiaArea> gaiaAreas) {
        for (GaiaArea area : gaiaAreas) {
            List<GaiaArea> areaList = planningModelMapper.findAreaByParentId(area.getAreaId());
            area.setAreaList(areaList);
            if (ObjectUtil.isNotEmpty(areaList)) {
                selectGaiaArea(areaList);
            }
        }
    }

    /**
     * 添加门店分类的默认配置
     *
     * @param client       加盟商
     * @param stoCode      门店id
     * @param stoAttribute 门店属性 1:单体 2:连锁
     */
    public void setDefaultStoGroupSet(String client, String stoCode, String stoAttribute) {
        //添加加盟商的默认配置，配置过的不重复配置
        saveDefaultStoreGroupSet(client);
        //保存门店默认分类
        saveDefaultStoreGroups(client, stoCode, stoAttribute);
    }

    /**
     * 保存门店默认分类
     *
     * @param client
     * @param stoCode
     * @param stoAttribute
     */
    private void saveDefaultStoreGroups(String client, String stoCode, String stoAttribute) {
        boolean isDX0004 = true;
        List<SdStoresGroup> defaultGroups = StoreEnum.defaultStoreGroup(client, stoCode, stoAttribute, isDX0004);
        List<SdStoresGroup> sdStoresGroups = sdStoresGroupMapper.selectGroupListByClientAndBrId(client, stoCode);

        //判断DX0004编号为1的名称是否为null
        SdStoresGroupSet dx0004 = sdStoresGroupSetMapper.selectOne(
                Wrappers.lambdaQuery(SdStoresGroupSet.class)
                        .eq(SdStoresGroupSet::getClient, client)
                        .eq(SdStoresGroupSet::getGssgType, "DX0004")
                        .eq(SdStoresGroupSet::getGssgId, "1")
        );
        for (SdStoresGroup defaultGroup : defaultGroups) {
            if (client.equals(defaultGroup.getClient()) && "DX0004".equals(defaultGroup.getGssgType()) && "1".equals(defaultGroup.getGssgId())) {
                defaultGroup.setGssgName(dx0004.getGssgIdName());
            }
        }

        if (ObjectUtils.isNotEmpty(sdStoresGroups)) {
            Iterator<SdStoresGroup> storesGroupIterator = defaultGroups.iterator();
            while (storesGroupIterator.hasNext()) {
                SdStoresGroup defaultStoresGroup = storesGroupIterator.next();
                sdStoresGroups.forEach(sdStoresGroup -> {
                    if (defaultStoresGroup.getGssgType().equals(sdStoresGroup.getGssgType())) {
                        if (StringUtils.isEmpty(sdStoresGroup.getGssgId())) {
                            sdStoresGroupMapper.update(null,
                                    Wrappers.lambdaUpdate(SdStoresGroup.class)
                                            .eq(SdStoresGroup::getClient, defaultStoresGroup.getClient())
                                            .eq(SdStoresGroup::getGssgType, defaultStoresGroup.getGssgType())
                                            .eq(SdStoresGroup::getGssgBrId, defaultStoresGroup.getGssgBrId())
                                            .set(SdStoresGroup::getGssgId, defaultStoresGroup.getGssgId())
                                            .set(SdStoresGroup::getGssgName, defaultStoresGroup.getGssgName())
                                            .set(SdStoresGroup::getGssgUpdateEmp, defaultStoresGroup.getGssgUpdateEmp())
                                            .set(SdStoresGroup::getGssgUpdateDate, defaultStoresGroup.getGssgUpdateDate())
                                            .set(SdStoresGroup::getGssgUpdateTime, defaultStoresGroup.getGssgUpdateTime())
                            );
                        }
                        storesGroupIterator.remove();
                        return;
                    }
                });
            }
        }
        if (ObjectUtils.isNotEmpty(defaultGroups)) {
            sdStoresGroupMapper.batchInsert(defaultGroups);
        }
    }

    /**
     * 保存加盟商默认门店分类设置
     *
     * @param client
     */
    @Transactional
    public void saveDefaultStoreGroupSet(String client) {
        //添加门店分类的默认配置
        LambdaQueryWrapper<SdStoresGroupSet> wrapper = Wrappers.lambdaQuery(SdStoresGroupSet.class).eq(SdStoresGroupSet::getClient, client);
        List<SdStoresGroupSet> sdStoresGroupSets = sdStoresGroupSetMapper.selectList(wrapper);
        //boolean isDX0004 = sdStoresGroupSets.stream().map(sdStoresGroupSet -> sdStoresGroupSet.getGssgType()).collect(Collectors.toList()).contains("DX0004");
        boolean isDX0004 = true;
        List<SdStoresGroupSet> defaultGroupSets = StoreEnum.defaultStoreGroupSet(client, isDX0004);
        if (ObjectUtils.isNotEmpty(sdStoresGroupSets)) {
            Iterator<SdStoresGroupSet> defaultGroupSetsIterator = defaultGroupSets.iterator();
            while (defaultGroupSetsIterator.hasNext()) {
                SdStoresGroupSet defaultGroupSet = defaultGroupSetsIterator.next();
                sdStoresGroupSets.forEach(sdStoresGroupSet -> {
                    if (defaultGroupSet.getGssgType().equals(sdStoresGroupSet.getGssgType()) && defaultGroupSet.getGssgId().equals(sdStoresGroupSet.getGssgId())) {
                        defaultGroupSetsIterator.remove();
                        return;
                    }
                });
            }
        }

        if (ObjectUtils.isNotEmpty(defaultGroupSets)) {
            sdStoresGroupSetMapper.batchInsert(defaultGroupSets);
        }
    }

    public void sendUpdateSms(String client, String legalPersonId, String stoName) {
        UserData userData = userDataService.getUserByClientAndUserId(client, legalPersonId);
        Verification verification = new Verification(userData.getUserTel(), "2", stoName, null);
        smsTemplateService.sendUpdateStoreRegisterSms(verification);
    }

    @Override
    public List<Map<String, Object>> getStoreGroupCode() {
        TokenUser user = commonService.getLoginInfo();
        List<SdStoresGroupSet> groupSetList = sdStoresGroupSetMapper.selectListByClient(user.getClient());
        List<Map<String, Object>> resultList = new ArrayList<>();
        Set<String> groupTypes = groupSetList.stream().map(groupSet -> groupSet.getGssgType()).collect(Collectors.toSet());
        groupTypes.forEach(type -> {
            HashMap<String, Object> map = new HashMap<>();
            ArrayList<HashMap<String, String>> list = new ArrayList<>();
            groupSetList.forEach(sdStoresGroupSet -> {
                if (type.equals(sdStoresGroupSet.getGssgType())) {
                    if (StringUtils.isEmpty(map.get("gssgType"))) {
                        map.put("gssgType", sdStoresGroupSet.getGssgType());
                        map.put("gssgTypeName", sdStoresGroupSet.getGssgTypeName());
                    }
                    HashMap<String, String> setMap = new HashMap<>();
                    setMap.put("gssgId", sdStoresGroupSet.getGssgId());
                    setMap.put("gssgName", sdStoresGroupSet.getGssgIdName());
                    setMap.put("ifConfig", sdStoresGroupSet.getGssgIfConfig().toString());
                    list.add(setMap);
                }
            });
            list.sort(Comparator.comparing(o -> Integer.parseInt(o.get("gssgId"))));
            map.put("groupSet", list);
            resultList.add(map);
        });
        return resultList;
    }

    @Override
    public List<StoreGroupVo> getStoreGroup(StoreGroupDto storeGroupDto) {
        TokenUser user = commonService.getLoginInfo();
        storeGroupDto.setClient(user.getClient());
        return sdStoresGroupMapper.getStoGroupDetails(storeGroupDto);
    }

    @Override
    @Transactional
    public void editStoreGroup(StoreGroupVo storeGroupVo) {
        TokenUser user = commonService.getLoginInfo();
        String dateStrYYMMDD = DateUtils.getCurrentDateStrYYMMDD();
        String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();
        SdStoresGroup sdStoresGroup = new SdStoresGroup(
                user.getClient(),
                storeGroupVo.getStoGroupId(),
                storeGroupVo.getStoGroupName(),
                storeGroupVo.getStoCode(),
                storeGroupVo.getStoName(),
                storeGroupVo.getStoGroupType(),
                user.getUserId(),
                dateStrYYMMDD,
                timeStrHHMMSS
        );
        sdStoresGroupMapper.updateStoGroupSetByPrimaryKey(sdStoresGroup);
    }

    @Override
    @Transactional
    public void setDefaultStoreGroup() {
        List<StoreData> storeDataList = storeDataMapper.getAllStore();
        storeDataList.forEach(storeData -> setDefaultStoGroupSet(storeData.getClient(), storeData.getStoCode(), storeData.getStoAttribute()));

        List<String> clients = storeDataMapper.getAllClient();
        clients.forEach(this::saveDefaultStoreGroupSet);
    }

    @Override
    public Result storeGroupExport(StoreGroupDto storeGroupDto) throws IOException {
        // edit by jinwencheng on 2021-12-28 11:17:32 添加csv头 和导入模板保持一致
        ArrayList<String> headers = Lists.newArrayList("序号", "门店编码", "门店名称", "分类类型编码", "分类类型名称", "分类名称编码", "分类名称", "更新时间", "更新人");
        // edit end
        List<StoreGroupVo> vos = getStoreGroup(storeGroupDto);
        List<List<Object>> body = new ArrayList<>();
        AtomicInteger i = new AtomicInteger(1);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
        vos.forEach(vo -> {
            ArrayList<Object> list = null;
            try {
                if (StringUtils.isNotEmpty(vo.getUpdateDate()) && StringUtils.isNotEmpty(vo.getUpdateTime())) {
                    vo.setUpdateDate(String.format("%tF", dateFormat.parse(vo.getUpdateDate())));
                    vo.setUpdateTime(String.format("%tT", timeFormat.parse(vo.getUpdateTime())));
                } else {
                    vo.setUpdateDate("");
                    vo.setUpdateTime("");
                }
                list = Lists.newArrayList(i.getAndIncrement() + "",
                        // edit by jinwencheng on 2021-12-28 11:17:32 添加csv头 和导入模板保持一致
                        vo.getStoCode(),
                        vo.getStoName(),
                        vo.getStoGroupType(),
                        vo.getStoGroupTypeName(),
                        vo.getStoGroupId(),
                        vo.getStoGroupName(),
                        // edit end
                        vo.getUpdateDate() + " " + vo.getUpdateTime(),
                        vo.getUpdateUser());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            body.add(list);
        });
        String content = ExcelUtils.exportCSV(headers, body);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(content.getBytes("GBK"));
        return cosUtils.uploadFile(bos, "门店分类数据".concat(".csv"));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveGroupSet(List<StoreGroupSetDto> dtoList) {
        if (ObjectUtil.isEmpty(dtoList)) {
            throw new CustomResultException("保存数据为空！");
        }

        String regexp = "[A-Za-z0-9\\u4e00-\\u9fa5]+";

        TokenUser user = commonService.getLoginInfo();
        String dateStrYYMMDD = DateUtils.getCurrentDateStrYYMMDD();
        String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();

        sdStoresGroupSetMapper.delete(
                Wrappers.<SdStoresGroupSet>lambdaQuery()
                        .eq(SdStoresGroupSet::getClient, user.getClient())
                        .eq(SdStoresGroupSet::getGssgType, dtoList.get(0).getGssgType())
        );

        for (StoreGroupSetDto dto : dtoList) {

            if (StrUtil.isBlank(dto.getGssgType())) {
                throw new CustomResultException("分类编号" + dto.getGssgId() + "对应的分类类型编码为空。");
            }
            if (StrUtil.isBlank(dto.getGssgTypeName())) {
                throw new CustomResultException("分类编号" + dto.getGssgId() + "对应的分类类型名称为空。");
            }
            if (StrUtil.isBlank(dto.getGssgId())) {
                throw new CustomResultException("第" + (dtoList.indexOf(dto) + 1) + "列分类编号为空");
            }

            if (StrUtil.isNotBlank(dto.getGssgName()) && !dto.getGssgName().matches(regexp)) {
                throw new CustomResultException("分类编号" + dto.getGssgId() + "对应的分类名称含特殊符号或空格，请修改。");
            }

            SdStoresGroupSet sdStoresGroupSet = new SdStoresGroupSet(user.getClient(), dto.getGssgType(), dto.getGssgId(), dto.getGssgTypeName(), dto.getGssgName(), user.getLoginName(), dateStrYYMMDD, timeStrHHMMSS, 1);

            sdStoresGroupSetMapper.insert(sdStoresGroupSet);

            //修改门店分类分类名称
            sdStoresGroupMapper.update(new SdStoresGroup(), Wrappers.<SdStoresGroup>lambdaUpdate()
                    .eq(SdStoresGroup::getClient, user.getClient())
                    .eq(SdStoresGroup::getGssgType, dto.getGssgType())
                    .eq(SdStoresGroup::getGssgId, dto.getGssgId())
                    .set(SdStoresGroup::getGssgName, dto.getGssgName()));
        }
        /*
        StoreGroupSetDto setDto = dtoList.stream().max(Comparator.comparing(o -> o.getGssgId())).orElseGet(() -> new StoreGroupSetDto());

        if (StrUtil.isNotBlank(setDto.getGssgName())){
            Integer i = Integer.parseInt(setDto.getGssgId());
            i++;
            SdStoresGroupSet sdStoresGroupSetNext = new SdStoresGroupSet(user.getClient(), setDto.getGssgType(), i.toString(), setDto.getGssgTypeName(), null, "system", dateStrYYMMDD, timeStrHHMMSS, 1);

            //新建下一个分类配置
            sdStoresGroupSetMapper.insert(sdStoresGroupSetNext);
        }*/
    }

    /**
     * add by jinwencheng on 2021-12-23 15:50:03 添加门店分类 - 门店选择 - 查询
     **/
    @Override
    public List<GetStoresForAddVO> getStoresForAdd(GetStoresForAddDTO getStoresForAddDTO) {
        String client = ObjectUtil.isNotEmpty(commonService.getLoginInfo()) ? commonService.getLoginInfo().getClient() : null;
        getStoresForAddDTO.setClient(client);
        List<GetStoresForAddVO> resultList = null;
        if (getStoresForAddDTO.getIsBatchSearch() == 1) {
            // 批量查询
            if (StrUtil.isNotEmpty(getStoresForAddDTO.getCondition())) {
                String[] stoCode = getStoresForAddDTO.getCondition().split(",");
                getStoresForAddDTO.setStoCode(stoCode);
                resultList = sdStoresGroupMapper.getStoresForAddByBatch(getStoresForAddDTO);
            } else {
                throw new CustomResultException("缺少批量查询条件");
            }
        } else if (getStoresForAddDTO.getIsBatchSearch() == 0) {
            // 非批量查询
            if (getStoresForAddDTO.getCondition().contains(",")) {
                String[] stoCode = getStoresForAddDTO.getCondition().split(",");
                getStoresForAddDTO.setStoCode(stoCode);
                resultList = sdStoresGroupMapper.getStoresForAddByBatch(getStoresForAddDTO);
            } else resultList = sdStoresGroupMapper.getStoresForAddNonBatch(getStoresForAddDTO);
        }
        if (CollUtil.isNotEmpty(resultList)) {
            // 未设置的门店默认没有分类类型
            List<GetStoresForAddVO> nosetList = resultList.stream().filter(group -> StrUtil.isEmpty(group.getGssgType())).collect(Collectors.toList());
            String gssgTypeName = sdStoresGroupMapper.getGssgTypeNameByClientAndGssgType(client, getStoresForAddDTO.getGssgType());
            nosetList.forEach(group -> {
                group.setGssgType(getStoresForAddDTO.getGssgType());
                group.setGssgTypeName(gssgTypeName);
            });
        }
        return resultList;
    }
    /** add end **/

    /**
     * add by jinwencheng on 2021-12-23 15:50:03 门店分类导入
     **/
    @Override
    public Result batchImport(HttpServletRequest request, MultipartFile file) {
        TokenUser tokenUser = commonService.getLoginInfo();
        if (Objects.isNull(tokenUser)) {
            throw new CustomResultException("请重新登陆");
        }
        if (file.isEmpty()) {
            throw new CustomResultException("上传文件为空");
        }
        List<BatchImportStoreDTO> list = CSVReadUtil.readCsv(tokenUser.getClient(), file);
        if (list.size() > 0) {
            list.forEach(store -> {
                if (StrUtil.isEmpty(store.getGssgBrId())) throw new CustomResultException("门店编码不可为空");
                if (StrUtil.isEmpty(store.getGssgType())) throw new CustomResultException("分类类型编码不可为空");
                if (StrUtil.isEmpty(store.getGssgId())) throw new CustomResultException("分类名称编码不可为空");
            });
            Map<String, List<BatchImportStoreDTO>> mapList = list.stream().collect(Collectors.groupingBy(BatchImportStoreDTO::getGssgType));
            if (mapList.keySet().size() > 1) {
                throw new CustomResultException("每次只可导入一种分类类型");
            }
            list.forEach(store -> {
                List<SdStoresGroupSet> groupSetList = sdStoresGroupMapper.getGroupSetByGssgType(tokenUser.getClient(), store.getGssgType());
                if (groupSetList.size() == 0) {
                    throw new CustomResultException(String.format("分类类型【%s】不存在，请确认后再操作", store.getGssgType()));
                }
                List<String> groupSetGssgIdList = groupSetList.stream().map(SdStoresGroupSet::getGssgId).collect(Collectors.toList());
                groupSetList.forEach(groupSet -> {
                    if (!groupSetGssgIdList.contains(store.getGssgId())) {
                        throw new CustomResultException(String.format("分类类型【%s】内，无【%s】分类编号，请确认后再操作", store.getGssgType(), store.getGssgId()));
                    }
                });
                List<StoreData> storeDataList = sdStoresGroupMapper.getStoreDatasByClient(tokenUser.getClient());
                if (CollUtil.isNotEmpty(storeDataList)) {
                    List<String> stoCodeList = storeDataList.stream().map(StoreData::getStoCode).collect(Collectors.toList());
                    if (!stoCodeList.contains(store.getGssgBrId())) {
                        throw new CustomResultException(String.format("门店编码【%s】不存在，请确认后再操作", store.getGssgBrId()));
                    }
                }
                BatchImportStoreDTO setedStore = sdStoresGroupMapper.getStoreGroupSetInfos(tokenUser.getClient(), store.getGssgType(), store.getGssgBrId());
                store.setGssgUpdateEmp(tokenUser.getUserId());
                String dateStrYYMMDD = DateUtils.getCurrentDateStrYYMMDD();
                store.setGssgUpdateDate(dateStrYYMMDD);
                List<String> params = sdStoresGroupMapper.getGroupSetParamsVal(tokenUser.getClient(), store.getGssgBrId(), store.getGssgType(), store.getGssgId());
                store.setStoName(params.get(0));
                store.setGssgTypeName(params.get(1));
                store.setGssgIdName(params.get(2));
                if (ObjectUtil.isEmpty(setedStore)) {
                    // 未设置
                    String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();
                    store.setGssgUpdateTime(timeStrHHMMSS);
                    sdStoresGroupMapper.saveOneStoreGroup(store);
                } else {
                    // 已设置
                    String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();
                    store.setGssgUpdateTime(timeStrHHMMSS);
                    sdStoresGroupMapper.updateStoreGroup(store);
                    }
            });
        } else {
            throw new CustomResultException("上传的csv没有数据");
        }
        return null;
    }
    /** add end **/

    /**
     * add by jinwencheng on 2021-12-23 15:50:03 保存门店分类
     **/
    @Override
    public void saveStoreGroup(List<StoresGroupAddDTO> storesGroupAddDTO) {
        TokenUser tokenUser = commonService.getLoginInfo();
        if (Objects.isNull(tokenUser)) {
            throw new CustomResultException("请重新登陆");
        }
        List<SdStoresGroup> list = Lists.newArrayList();
        String client = tokenUser.getClient();
        storesGroupAddDTO.forEach(store -> {
            if (!StringUtils.isEmpty(store.getGssgType()) || !StringUtils.isEmpty(store.getGssgTypeName())) {
                if (!StringUtils.isEmpty(store.getGssgIdName())) {
                    String dateStrYYMMDD = DateUtils.getCurrentDateStrYYMMDD();
                    String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();
                    SdStoresGroup sdStoresGroup = new SdStoresGroup();
                    sdStoresGroup.setClient(client);
                    sdStoresGroup.setGssgId(store.getGssgId());
                    sdStoresGroup.setGssgName(store.getGssgIdName());
                    sdStoresGroup.setGssgBrId(store.getStoCode());
                    sdStoresGroup.setGssgBrName(store.getStoName());
                    sdStoresGroup.setGssgType(store.getGssgType());
                    sdStoresGroup.setGssgUpdateEmp(tokenUser.getUserId());
                    sdStoresGroup.setGssgUpdateDate(dateStrYYMMDD);
                    sdStoresGroup.setGssgUpdateTime(timeStrHHMMSS);
                    list.add(sdStoresGroup);
                }
            } else {
                throw new CustomResultException("分类类型不可为空");
            }
        });
        if (list.size() > 0) {
            sdStoresGroupMapper.saveStoreGroup(list);
        }
    }

    /**
     * add end
     **/


    @Override
    public StoreSalesRankingResponse getStoreSalesRanking(StoreSalesRankingRequest request) throws ParseException {
        TokenUser user = commonService.getLoginInfo();
        if (Objects.isNull(request.getType())) {
            throw new CustomResultException("类型必传");
        }
        if (Objects.nonNull(request.getBeforeAndAfter())) {
            if (request.getRanking() <= 0) {
                throw new CustomResultException("排名不能小于0");
            }
        }

        //request.setClient("10000003");
        request.setClient(user.getClient());
        //如果不穿日期默认当月的时间
        if (ArrayUtil.isEmpty(request.getYearMonth())) {
            String format = DateUtil.format(new Date(), "yyyy-MM");
            String[] date = new String[1];
            date[0] = format;
            request.setYearMonth(date);
        }
        StoreSalesRankingResponse response = new StoreSalesRankingResponse();
        //获取基本的数据源
        List<StoreSalesRankingResponse.StoreSalesRanking> storeSalesRankings = this.storeSalesRanking(request);
        //计算客单价 客品次
        List<StoreSalesRankingResponse.StoreSalesRanking> storeNumber = getStoreNumber(storeSalesRankings);
        //获取销售额达成率 销售额达成额 毛利额达成率 毛利额达成额 会员卡达成率 会员卡达成额度
        List<Map<String, Object>> saleMember = getSaleMember(request.getClient(), request.getStoreList(), getDate(request.getYearMonth()));
        //数据的组装
        List<StoreSalesRankingResponse.StoreSalesRanking> rankings = getSum(storeNumber, saleMember);
        //通过麒麟获取会员卡
        List<StoreSalesResponse> getCardCount = getMemberCard(request.getClient(), request.getStoreList(), request.getYearMonth());
        //根据门店加盟商匹配门店的会员卡数量
        List<StoreSalesRankingResponse.StoreSalesRanking> getStoreCardList = getStoreCard(getCardCount, rankings);
        //筛选条件前多少名 后多少名
        List<StoreSalesRankingResponse.StoreSalesRanking> storeRanking = getStoreRanking(getStoreCardList, request);
        //获取门店经理名称 ID
        List<StoreSalesRankingResponse.StoreSalesRanking> storeManager = getStoreManager(storeRanking, request.getClient());
        //返回数据结构组装
        List<StoreSalesRankingResponse.StoreSalesRanking> dataAssembly = getDataAssembly(storeManager, request.getType());
        response.setStoreSalesRankings(dataAssembly);
        response.setStoreCount(dataAssembly.size());
        response.setType(request.getType());
        return response;
    }

    /**
     * 返回数据组装
     *
     * @param storeManager
     * @param type
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> getDataAssembly(List<StoreSalesRankingResponse.StoreSalesRanking> storeManager, Integer type) {
        //0：营业额 1：毛利额 2：会员卡  3 :毛利率 4.交易次数 5客单价 6客品次7品单价8单店日均销售9单店日均毛利 10单店日均交易
        if (CollUtil.isNotEmpty(storeManager)) {
            for (StoreSalesRankingResponse.StoreSalesRanking ranking : storeManager) {
                if (type == 0) {
                    ranking.setAmt(Convert.toStr(ranking.getActualAmount()));
                    ranking.setAchievementRate(ranking.getActualAchievementRate());
                } else if (type == 1) {
                    ranking.setAmt(Convert.toStr(ranking.getGrossProfitAmount()));
                    ranking.setAchievementRate(ranking.getGrossProfitAchievementRate());
                } else if (type == 2) {
                    //ranking.setAmt(Convert.toStr(ranking.getMemberCard()));
                    //ranking.setAchievementRate(ranking.getMemberCardRate());
                    ranking.setAmt(ranking.getGrossMarginRate());
                } else if (type == 3) {
                    //ranking.setAmt(ranking.getGrossMarginRate());
                    ranking.setAmt(Convert.toStr(ranking.getMemberCard()));
                    ranking.setAchievementRate(ranking.getMemberCardRate());
                } else if (type == 4) {
                    ranking.setAmt(Convert.toStr(ranking.getTradeCount()));
                } else if (type == 5) {
                    ranking.setAmt(Convert.toStr(ranking.getCustomerPrice()));
                } else if (type == 6) {
                    ranking.setAmt(Convert.toStr(ranking.getCustomerOrder()));
                } else if (type == 7) {
                    ranking.setAmt(Convert.toStr(ranking.getUnitPrices()));
                } else if (type == 8) {
                    ranking.setAmt(Convert.toStr(ranking.getAverageDailySales()));
                } else if (type == 9) {
                    ranking.setAmt(Convert.toStr(ranking.getAverageDailyGrossProfit()));
                } else if (type == 10) {
                    ranking.setAmt(Convert.toStr(ranking.getAverageDailyTransaction()));
                }
            }
        }
        return storeManager;
    }

    /**
     * 根据门店获取店经理 店经理ID
     *
     * @param storeRanking
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> getStoreManager(List<StoreSalesRankingResponse.StoreSalesRanking> storeRanking, String client) {
        if (CollUtil.isNotEmpty(storeRanking)) {
            List<Map<String, Object>> maps = storeDataMapper.getStoreManager(client, storeRanking.stream().map(store -> store.getStore()).collect(Collectors.toList()));
            if (CollUtil.isNotEmpty(maps)) {
                maps.forEach(map -> {
                    for (StoreSalesRankingResponse.StoreSalesRanking ranking : storeRanking) {
                        if (Objects.equals(map.get("client"), ranking.getClient()) && Objects.equals(map.get("store"), ranking.getStore())) {
                            ranking.setEmployeeId(Convert.toStr(map.get("userId")));
                            ranking.setStoreManager(Convert.toStr(map.get("userName")));
                        }
                    }
                });
            }
        }
        return storeRanking;
    }

    /**
     * 筛选条件前多少名 后多少名
     *
     * @param getStoreCardList
     * @param request
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> getStoreRanking(List<StoreSalesRankingResponse.StoreSalesRanking> getStoreCardList, StoreSalesRankingRequest request) {
        //说明有筛选排名
        if (request.getType() == 0) {
            //营业额度 达成率”从高到低排序。
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getActualAchievement().compareTo(o1.getActualAchievement());
                }
            });
        } else if (request.getType() == 1) {
            //毛利额达成率排序
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getGrossProfitAchievement().compareTo(o1.getGrossProfitAchievement());
                }
            });
        } else if (request.getType() == 2) {
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getGrossMargin().compareTo(o1.getGrossMargin());
                }
            });
        } else if (request.getType() == 3) {
            //会员卡达成率排序
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getCard().compareTo(o1.getCard());
                }
            });
        } else if (request.getType() == 4) {
            //交易次数排序
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getTradeCount() - (o1.getTradeCount());
                }
            });
        } else if (request.getType() == 5) {
            //5客单价次数排序
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getCustomerPrice().compareTo(o1.getCustomerPrice());
                }
            });
        } else if (request.getType() == 6) {
            //6客品次
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getCustomerOrder().compareTo(o1.getCustomerOrder());
                }
            });
        } else if (request.getType() == 7) {

            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getUnitPrices().compareTo(o1.getUnitPrices());
                }
            });
        } else if (request.getType() == 8) {
            //8单店日均销售
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getAverageDailySales().compareTo(o1.getAverageDailySales());
                }
            });
        } else if (request.getType() == 9) {
            //9单店日均毛利
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getAverageDailyGrossProfit().compareTo(o1.getAverageDailyGrossProfit());
                }
            });
        } else if (request.getType() == 10) {
            //9单店日均毛利
            Collections.sort(getStoreCardList, new Comparator<StoreSalesRankingResponse.StoreSalesRanking>() {
                @Override
                public int compare(StoreSalesRankingResponse.StoreSalesRanking o1, StoreSalesRankingResponse.StoreSalesRanking o2) {
                    return o2.getAverageDailyTransaction().compareTo(o1.getAverageDailyTransaction());
                }
            });
        }

        if (Objects.nonNull(request.getBeforeAndAfter())) {
            List<StoreSalesRankingResponse.StoreSalesRanking> salesRankings = new ArrayList<>();
            //需要排序
            if (request.getBeforeAndAfter() == 0) {
                if (request.getRanking() > getStoreCardList.size()) {
                    //取所有
                    salesRankings = getStoreCardList;
                } else {
                    //前
                    salesRankings = getStoreCardList.subList(0, request.getRanking());
                }
            } else if (request.getBeforeAndAfter() == 1) {
                for (int i = getStoreCardList.size() - 1; i >= 0; i--) {
                    salesRankings.add(getStoreCardList.get(i));
                }
                if (request.getRanking() < getStoreCardList.size()) {
                    salesRankings = getStoreCardList.subList(0, request.getRanking());
                }
            }
            return salesRankings;
        }
        return getStoreCardList;
    }

    /**
     * 根据门店加盟商匹配门店的会员卡数量
     *
     * @param getCardCount
     * @param rankings
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> getStoreCard(List<StoreSalesResponse> getCardCount, List<StoreSalesRankingResponse.StoreSalesRanking> rankings) {

        if (CollUtil.isNotEmpty(rankings)) {
            if (CollUtil.isNotEmpty(getCardCount)) {
                for (StoreSalesRankingResponse.StoreSalesRanking ranking : rankings) {
                    getCardCount.forEach(card -> {
                        if (Objects.equals(ranking.getClient(), card.getClient()) && Objects.equals(ranking.getStore(), card.getStore())) {
                            //会员卡数量
                            int memberCard = Convert.toInt(card.getMemberCard());
                            ranking.setMemberCard(Convert.toInt(memberCard));
                            //会员卡达成率
                            ranking.setMemberCardRate(new BigDecimal("0.00").compareTo(ranking.getMemberCardTarget()) == 0 ? "0.00%" : Convert.toStr(new BigDecimal(memberCard).divide(ranking.getMemberCardTarget(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP)).concat("%"));
                            ranking.setCard(new BigDecimal("0.00").compareTo(ranking.getMemberCardTarget()) == 0 ? BigDecimal.ZERO : new BigDecimal(memberCard).divide(ranking.getMemberCardTarget(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                    });
                }
            }
        }
        return rankings;
    }

    /**
     * 通过麒麟获取会员卡
     *
     * @param client    加盟商
     * @param storeList 多个门店
     * @param date      年月
     * @return
     */
    private List<StoreSalesResponse> getMemberCard(String client, String[] storeList, String[] date) {
        StringBuilder builder = new StringBuilder()
                .append("select ")
                .append("gsmc.CLIENT as client,")
                .append("gsmc.GSMBC_BR_ID store, ")
                .append("count(gsmc.GSMBC_CARD_ID) as memberCard  ")
                .append("from GAIA_SD_MEMBER_CARD as gsmc ")
                .append("where 1=1 ");
        if (StrUtil.isNotBlank(client)) {
            builder.append("and gsmc.CLIENT =" + client);
        }
        if (ArrayUtil.isNotEmpty(storeList)) {
            builder.append(" and gsmc.GSMBC_BR_ID in ");
            builder.append(" ( ");
            builder.append(org.apache.commons.lang3.StringUtils.join(storeList, ","));
            builder.append(" ) ");
        }
        if (ArrayUtil.isNotEmpty(date)) {
            List<Map<String, String>> dateTime = getDateTime(date);
            if (CollUtil.isNotEmpty(dateTime)) {
                builder.append(" and (");
                for (int i = 0; i < dateTime.size(); i++) {
                    builder.append(" (");
                    if (i + 2 <= dateTime.size()) {
                        builder.append(" gsmc.GSMBC_CREATE_DATE >=" + "'" + dateTime.get(i).get("minDate") + "'" + " and " + " gsmc.GSMBC_CREATE_DATE <=" + "'" + dateTime.get(i).get("maxDate") + "'" + ") or");
                    } else {
                        builder.append(" gsmc.GSMBC_CREATE_DATE >=" + "'" + dateTime.get(i).get("minDate") + "'" + " and " + " gsmc.GSMBC_CREATE_DATE <=" + "'" + dateTime.get(i).get("maxDate") + "'" + ")");
                    }
                }
                builder.append(" )");
            }
        }
        builder.append(" group by gsmc.CLIENT,gsmc.GSMBC_BR_ID");
        System.err.println(builder);
        log.info("门店销售数据：{}", Convert.toStr(builder));
        List<StoreSalesResponse> query = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(StoreSalesResponse.class));
        log.info("返回结果:{}", JSONObject.toJSONString(query));
        return query;
    }

    /**
     * 日期组装 2020-09 -》 202009
     *
     * @param date
     * @return
     */
    private List<String> getDate(String[] date) {
        List<String> dates = new ArrayList<>(date.length);
        if (ArrayUtil.isNotEmpty(date)) {
            //SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
            for (String s : date) {
               /* dates.add(DateUtils.formatLocalDateTime(LocalDateTime.parse(s,DateUtils.YEAR_MONTH_FORMATTER),"yyyyMM"));
                DateUtil.format(new Date(), "yyyyMM");*/
                String[] split = s.split("-");
                dates.add(split[0] + split[1]);
            }
        }
        return dates;
    }

    /**
     * 计算客单价 客品次
     *
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> getStoreNumber(List<StoreSalesRankingResponse.StoreSalesRanking> storeSalesRankings) {
        if (CollUtil.isNotEmpty(storeSalesRankings)) {
            storeSalesRankings.forEach(store -> {
                //实收金额
                BigDecimal amt = store.getActualAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
                store.setActualAmount(amt);
                //毛利额
                BigDecimal profitAmount = amt.subtract(store.getCostAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
                store.setGrossProfitAmount(profitAmount);
                //毛利率
                store.setGrossMarginRate(new BigDecimal("0.00").compareTo(amt) == 0 ? "0.00%" : Convert.toStr(profitAmount.divide(amt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP)).concat("%"));
                store.setGrossMargin(new BigDecimal("0.00").compareTo(amt) == 0 ? BigDecimal.ZERO : profitAmount.divide(amt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP));
                //交易次数
                BigDecimal tradeCount = new BigDecimal(store.getTradeCount());
                //客单价
                store.setCustomerPrice(new BigDecimal("0.00").compareTo(tradeCount) == 0 ? BigDecimal.ZERO : amt.divide(tradeCount, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                //客品次
                //store.setCustomerOrder(new BigDecimal("0.00").compareTo(tradeCount) == 0 ? BigDecimal.ZERO : new BigDecimal(store.getProductCount()).divide(tradeCount, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                //品单价
                //store.setUnitPrices(new BigDecimal("0.00").compareTo(store.getQtyCount()) == 0 ? BigDecimal.ZERO : amt.divide(store.getQtyCount(), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                //单店日均销售额
                store.setAverageDailySales(new BigDecimal("0.00").compareTo(new BigDecimal(store.getSalesDays())) == 0 ? BigDecimal.ZERO : amt.divide(new BigDecimal(store.getSalesDays()), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                //单店日均毛利额
                store.setAverageDailyGrossProfit(new BigDecimal("0.00").compareTo(new BigDecimal(store.getSalesDays())) == 0 ? BigDecimal.ZERO : profitAmount.divide(new BigDecimal(store.getSalesDays()), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                //单店日均交易
                store.setAverageDailyTransaction(new BigDecimal("0.00").compareTo(new BigDecimal(store.getSalesDays())) == 0 ? BigDecimal.ZERO : tradeCount.divide(new BigDecimal(store.getSalesDays()), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
            });
        }
        return storeSalesRankings;
    }


    /**
     * 计算获取销售额达成率 销售额达成额 毛利额达成率 毛利额达成额
     *
     * @param storeSalesRankings
     * @param saleMember
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> getSum(List<StoreSalesRankingResponse.StoreSalesRanking> storeSalesRankings, List<Map<String, Object>> saleMember) {
        if (CollUtil.isNotEmpty(saleMember)) {
            if (CollUtil.isNotEmpty(storeSalesRankings)) {
                for (Map<String, Object> map : saleMember) {
                    storeSalesRankings.forEach(rank -> {
                        if (Objects.equals(map.get("client"), rank.getClient()) && Objects.equals(map.get("store"), rank.getStore())) {
                            rank.setActualAchievementRate(map.get("saleAmt") != null && new BigDecimal("0.00").compareTo(new BigDecimal(Convert.toStr(map.get("saleAmt")))) == 0 ? "0.00%" : Convert.toStr(rank.getActualAmount().divide(new BigDecimal(Convert.toStr(map.get("saleAmt"))), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP)).concat("%"));
                            rank.setActualAchievement(map.get("saleAmt") != null && new BigDecimal("0.00").compareTo(new BigDecimal(Convert.toStr(map.get("saleAmt")))) == 0 ? BigDecimal.ZERO : rank.getActualAmount().divide(new BigDecimal(Convert.toStr(map.get("saleAmt"))), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP));
                            //销售额达成率
                            rank.setGrossProfitAchievementRate(map.get("saleGross") != null && Convert.toLong(map.get("saleGross")).equals(0L) ? "0.00%" : Convert.toStr(rank.getGrossProfitAmount().divide(new BigDecimal(Convert.toStr(map.get("saleGross"))), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP)).concat("%"));
                            rank.setGrossProfitAchievement(map.get("saleGross") != null && Convert.toLong(map.get("saleGross")).equals(0L) ? BigDecimal.ZERO : rank.getGrossProfitAmount().divide(new BigDecimal(Convert.toStr(map.get("saleGross"))), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP));
                            //会员达成目标
                            rank.setMemberCardTarget(new BigDecimal(Convert.toStr(map.get("mcard"))));
                        }
                    });
                }
            }
        }
        return storeSalesRankings;
    }


    /**
     * 获取门店销售数据
     *
     * @param request
     * @return
     */
    private List<StoreSalesRankingResponse.StoreSalesRanking> storeSalesRanking(StoreSalesRankingRequest request) {
        //会员卡 销售额达成率 销售额达成额 毛利额达成率 毛利额达成额 会员卡达成率 会员卡达成额度
        StringBuilder builder = new StringBuilder()
                .append("SELECT ")
                .append("dd.CLIENT as client,") //加盟商
                .append("dd.GSSD_BR_ID as store,") //门店编码
                .append("sd.STO_SHORT_NAME as storeName,")  //门店名称
                //.append("sd.STO_LEADER as employeeId,")  //门店经理工号
                //.append("gud.USER_ID as employeeId,")//用户id
                //.append("gud.USER_NAM as storeManager,")   //用户姓名
                //.append("dd.GSSD_DATE as dates,")//日期
                //.append("dd.GSSD_SALER_ID,")//人员
                .append("round(SUM( dd.GSSD_AMT ),2) AS actualAmount,") //实收金额
                .append("round(SUM( dd.GSSD_MOV_PRICE ),2) AS costAmount,") //成本额
                //.append("ROUND( SUM( dd.GSSD_AMT )- SUM( GSSD_MOV_PRICE )/ NULLIF( SUM( dd.GSSD_AMT ), 0 ), 2 ) AS grossMarginRate,") //毛利率
                .append("count(DISTINCT ( dd.GSSD_BILL_NO )) AS tradeCount,") //交易次数
                //.append("ROUND( SUM( dd.GSSD_AMT )/ NULLIF( count( DISTINCT dd.GSSD_BILL_NO ), 0 ), 2 ) AS customerPrice,") //客单价
                .append("count( DISTINCT(dd.GSSD_PRO_ID) ) as productCount, ") //商品编码之和
                .append("round(sum( dd.GSSD_QTY ),2) as qtyCount, ") //销售量
                //.append("sum( dd.GSSD_AMT )/ NULLIF( sum( dd.GSSD_QTY ), 0 ) AS unitPrices,")  //品单价
                //.append("(SUM(dd.GSSD_AMT)) / (count(DISTINCT ( dd.GSSD_DATE ))) as averageDailySales,")  //单店日均销售额
                //.append("(SUM( dd.GSSD_AMT )- SUM( GSSD_MOV_PRICE )) /  (count(DISTINCT ( dd.GSSD_DATE ))) as averageDailyGrossProfit ,")  //单店日均毛利额
                //.append("(count(DISTINCT ( dd.GSSD_BILL_NO ))) / SUM( dd.GSSD_AMT ) as averageDailyTransaction ,")  //单店日均交易
                .append(" count(DISTINCT(dd.GSSD_DATE))  salesDays ") //销售天数
                //.append("count( dd.GSSD_PRO_ID ) AS proNum ")
                .append(" FROM ")
                .append(" GAIA_SD_SALE_D dd ")
                .append(" INNER JOIN GAIA_STORE_DATA sd ON dd.CLIENT = sd.CLIENT AND dd.GSSD_BR_ID = sd.STO_CODE ")
                .append(" INNER JOIN GAIA_PRODUCT_BUSINESS pb ON dd.CLIENT = pb.CLIENT AND dd.GSSD_BR_ID = pb.PRO_SITE AND dd.GSSD_PRO_ID = pb.PRO_SELF_CODE ")
                .append(" LEFT JOIN GAIA_USER_DATA gud ON dd.CLIENT = gud.CLIENT AND dd.GSSD_SALER_ID = gud.USER_ID ")
                .append(" where 1=1 ");
        if (StrUtil.isNotBlank(request.getClient())) {
            builder.append(" and dd.CLIENT =" + request.getClient());
        }
        if (request.getYearMonth() != null && request.getYearMonth().length > 0) {
            List<Map<String, String>> dateTime = getDateTime(request.getYearMonth());
            if (CollUtil.isNotEmpty(dateTime)) {
                builder.append(" and (");
                for (int i = 0; i < dateTime.size(); i++) {
                    builder.append(" (");
                    if (i + 2 <= dateTime.size()) {
                        builder.append(" dd.GSSD_DATE >=" + "'" + dateTime.get(i).get("minDate") + "'" + " and " + " dd.GSSD_DATE <=" + "'" + dateTime.get(i).get("maxDate") + "'" + ") or");
                    } else {
                        //builder.append(" dd.GSSD_DATE >="+dateTime.get(i).get("minDate") +" and "+" dd.GSSD_DATE <="+dateTime.get(i).get("maxDate")+" ) ");
                        builder.append(" dd.GSSD_DATE >=" + "'" + dateTime.get(i).get("minDate") + "'" + " and " + " dd.GSSD_DATE <=" + "'" + dateTime.get(i).get("maxDate") + "'" + ")");

                    }
                }
                builder.append(" )");
            }
            if (request.getStoreList() != null && request.getYearMonth().length > 0) {
                builder.append("and dd.GSSD_BR_ID in (");
                builder.append(org.apache.commons.lang3.StringUtils.join(request.getStoreList(), ",") + ")");
            }
        }
        if (StrUtil.isNotBlank(request.getClient())) {
            builder.append(" and dd.CLIENT=" + request.getClient());
        }
        builder.append(" GROUP by  dd.CLIENT,dd.GSSD_BR_ID , sd.STO_SHORT_NAME");
        System.err.println(builder);
        log.info("门店销售数据：{}", Convert.toStr(builder));
        List<StoreRankingResponse> query = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(StoreRankingResponse.class));
        log.info("返回结果:{}", JSONObject.toJSONString(query));
        List<StoreSalesRankingResponse.StoreSalesRanking> salesRankings = JSONObject.parseArray(JSONObject.toJSONString(query), StoreSalesRankingResponse.StoreSalesRanking.class);


        List<StoreSalesRankingResponse.StoreSalesRanking> sales = salesRanking(salesRankings, request);

        // List<StoreSalesRankingResponse.StoreSalesRanking> resp = new ArrayList<>();
        List<StoreSalesRankingResponse.StoreSalesRanking> stores = new ArrayList<>();
        if (StrUtil.isNotBlank(request.getStoreManager())) {
            if (CollUtil.isNotEmpty(sales)) {
                List<String> storeIds = sales.stream().map(sa -> sa.getStore()).collect(Collectors.toList());
                List<Map<String, String>> user = this.storeDataMapper.selectUser(request.getClient(), storeIds, request.getStoreManager());
                if (CollUtil.isNotEmpty(user)) {
                    for (StoreSalesRankingResponse.StoreSalesRanking salesRanking : sales) {
                        user.forEach(u -> {
                            if (Objects.equals(salesRanking.getClient(), u.get("client")) && Objects.equals(salesRanking.getStore(), u.get("storce"))) {
                                stores.add(salesRanking);
                            }
                        });
                    }
                }
            }
            return stores;
        }
        return salesRankings;
    }

    private List<StoreSalesRankingResponse.StoreSalesRanking> salesRanking(List<StoreSalesRankingResponse.StoreSalesRanking> salesRankings, StoreSalesRankingRequest request) {
        //List<StoreRankingResponse> query = new ArrayList<>();
        if (CollUtil.isNotEmpty(salesRankings)) {
            StringBuilder builder = new StringBuilder()
                    .append("SELECT ")
                    .append("dd.CLIENT as client,") //加盟商
                    .append("dd.GSSD_BR_ID as store,") //门店编码
                    .append("count( dd.GSSD_PRO_ID ) as productCount, ") //商品编码之和
                    .append("round(sum( dd.GSSD_QTY ),2) as qtyCount ") //销售量
                    .append(" FROM ")
                    .append(" GAIA_SD_SALE_D dd ")
                    .append(" INNER JOIN GAIA_PRODUCT_BUSINESS pb ON dd.CLIENT = pb.CLIENT AND dd.GSSD_BR_ID = pb.PRO_SITE AND dd.GSSD_PRO_ID = pb.PRO_SELF_CODE ")
                    .append(" where 1=1 ");
            if (StrUtil.isNotBlank(request.getClient())) {
                builder.append(" and dd.CLIENT =" + request.getClient());
            }
            if (request.getYearMonth() != null && request.getYearMonth().length > 0) {
                List<Map<String, String>> dateTime = getDateTime(request.getYearMonth());
                if (CollUtil.isNotEmpty(dateTime)) {
                    builder.append(" and (");
                    for (int i = 0; i < dateTime.size(); i++) {
                        builder.append(" (");
                        if (i + 2 <= dateTime.size()) {
                            builder.append(" dd.GSSD_DATE >=" + "'" + dateTime.get(i).get("minDate") + "'" + " and " + " dd.GSSD_DATE <=" + "'" + dateTime.get(i).get("maxDate") + "'" + ") or");
                        } else {
                            //builder.append(" dd.GSSD_DATE >="+dateTime.get(i).get("minDate") +" and "+" dd.GSSD_DATE <="+dateTime.get(i).get("maxDate")+" ) ");
                            builder.append(" dd.GSSD_DATE >=" + "'" + dateTime.get(i).get("minDate") + "'" + " and " + " dd.GSSD_DATE <=" + "'" + dateTime.get(i).get("maxDate") + "'" + ")");

                        }
                    }
                    builder.append(" )");
                }
                if (request.getStoreList() != null && request.getYearMonth().length > 0) {
                    builder.append("and dd.GSSD_BR_ID in (");
                    builder.append(org.apache.commons.lang3.StringUtils.join(request.getStoreList(), ",") + ")");
                }
            }
            if (StrUtil.isNotBlank(request.getClient())) {
                builder.append(" and dd.CLIENT=" + request.getClient());
            }
            builder.append(" AND ( pb.PRO_CLASS NOT LIKE '301%' and  pb.PRO_CLASS NOT LIKE '302%' and pb.PRO_CLASS NOT LIKE '8%' ) ");
            builder.append(" GROUP by  dd.CLIENT,dd.GSSD_BR_ID");

            System.err.println(builder);
            log.info("门店销售数据：{}", Convert.toStr(builder));
            List<StoreRankingResponse> query = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(StoreRankingResponse.class));
            log.info("返回结果:{}", JSONObject.toJSONString(query));

            if (CollUtil.isNotEmpty(query)) {
                query.forEach(qu -> {
                    for (StoreSalesRankingResponse.StoreSalesRanking salesRanking : salesRankings) {
                        if (Objects.equals(qu.getClient(), salesRanking.getClient()) && Objects.equals(qu.getStore(), salesRanking.getStore())) {
                            salesRanking.setCustomerOrder(salesRanking.getTradeCount() == 0 ? BigDecimal.ZERO : new BigDecimal(qu.getProductCount()).divide(new BigDecimal(salesRanking.getTradeCount()), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                            salesRanking.setUnitPrices(new BigDecimal(qu.getQtyCount()).compareTo(new BigDecimal("0.00")) == 0 ? BigDecimal.ZERO : salesRanking.getActualAmount().divide(new BigDecimal(qu.getQtyCount()), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                    }
                });
            }
        }
        return salesRankings;
    }

    /**
     * 获取销售额达成率 销售额达成额 毛利额达成率 毛利额达成额 会员卡达成率 会员卡达成额度
     */
    private List<Map<String, Object>> getSaleMember(String client, String[] storeList, List<String> date) throws ParseException {

        List<Map<String, Object>> maps = storeDataMapper.getSaleMember(client, storeList, date);

        return maps;
    }


    private List<Map<String, String>> getDateTime(String[] dates) {

        List<Map<String, String>> list = new ArrayList<>();
        if (ArrayUtil.isNotEmpty(dates)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            for (String date : dates) {
                Map<String, String> map = new HashMap();
                //分割日期  2021-9 -》2021   9
                String[] split = date.split("-");
                if (ArrayUtil.isNotEmpty(split)) {
                    //设置年份
                    cal.set(Calendar.YEAR, Convert.toInt(split[0]));
                    //设置月份
                    cal.set(Calendar.MONTH, Convert.toInt(split[1]) - 1);
                    //获取某月最小天数
                    int firstDay = cal.getMinimum(Calendar.DATE);
                    //设置日历中月份的最小天数
                    cal.set(Calendar.DAY_OF_MONTH, firstDay);
                    map.put("minDate", sdf.format(cal.getTime()));
                    //获取某月最大天数
                    int lastDay = cal.getActualMaximum(Calendar.DATE);
                    //设置日历中月份的最大天数
                    cal.set(Calendar.DAY_OF_MONTH, lastDay);
                    map.put("maxDate", sdf.format(cal.getTime()));
                    list.add(map);
                }
            }

        }
        return list;
    }


    /**
     * 根据门店统计数据
     *
     * @param request
     * @return
     */
    @Override
    public StoreSalesStatisticsRes getStoreStatistics(StoreSalesStatisticsRequest request) throws ParseException {
        if (Objects.isNull(request.getDateType())) {
            throw new CustomResultException("时间类型必传");
        }
        TokenUser user = commonService.getLoginInfo();
        request.setClient(user.getClient());

        StoreSalesStatisticsQueryByDataTypeStrategy strategy = null;
        StoreSalesStatisticsRequest inData = new StoreSalesStatisticsRequest();
        BeanUtils.copyProperties(request, inData);
        inData.setClient(user.getClient());
        //日
        if (Objects.equals(request.getDateType(), 0)) {
            strategy = new StoreSalesStatisticsDayImpl(STATISTICS_DIMENSION_MAP, storeDataMapper);
        }
        StoreSalesStatisticsRes storeSalesStatisticsRes = strategy.handleData(inData);

        return storeSalesStatisticsRes;
    }

    private Map<String, String> time() {
        Map<String, String> time = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        //今日
        String today = new SimpleDateFormat("yyyyMMdd ").format(cal.getTime());
        time.put("today", today);
        cal.add(Calendar.DATE, -1);
        //昨日
        String yesterday = new SimpleDateFormat("yyyyMMdd ").format(cal.getTime());
        time.put("yesterday", yesterday);
        return time;
    }


    /**
     * 获取本月的每一天
     *
     * @return
     */
    private List<String> currentMonth() {
        List<String> list = new ArrayList();
        Calendar aCalendar = Calendar.getInstance(Locale.CHINA);
        int year = aCalendar.get(Calendar.YEAR);//年份
        int month = aCalendar.get(Calendar.MONTH) + 1;//月份
        int day = aCalendar.getActualMaximum(Calendar.DATE);
        for (int i = 1; i <= day; i++) {
            String aDate = String.valueOf(year) + month + i;
            list.add(aDate);
        }
        return list;
    }

    /**
     * 获取去年这个月每一天
     *
     * @return
     */
    private List<String> lastYearMonth() {
        List<String> list = new ArrayList();
        Calendar aCalendar = Calendar.getInstance(Locale.CHINA);
        aCalendar.setTime(new Date());
        aCalendar.add(Calendar.YEAR, -1);
        int year = aCalendar.get(Calendar.YEAR);//年份
        int month = aCalendar.get(Calendar.MONTH) + 1;//月份
        int day = aCalendar.getActualMaximum(Calendar.DATE);
        for (int i = 1; i <= day; i++) {
            String aDate = String.valueOf(year) + month + i;
            list.add(aDate);
        }
        return list;
    }
}
