package com.gov.operate.controller.salary;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("salary/salary")
public class SalaryController {

    @Resource
    private ICommonDataService commonDataService;
    @Resource
    private ISalarySelftestCoeffiService salarySelftestCoeffiService;
    @Resource
    private ISalaryProgramService salaryProgramService;
    @Resource
    private ISalaryTrialUserService salaryTrialUserService;
    @Resource
    private ISalaryObjService salaryObjService;
    @Resource
    private ISalaryGradeService salaryGradeService;
    @Resource
    private IPostService postService;

    @Log("功能说明")
    @ApiOperation(value = "功能说明")
    @GetMapping("funDesc")
    public Result funDesc() {
        return ResultUtil.success(commonDataService.funDesc());
    }

    @Log("自测数据获取")
    @ApiOperation(value = "自测数据获取")
    @GetMapping("getSelftestCoeffi")
    public Result getSelftestCoeffi() {
        return ResultUtil.success(salarySelftestCoeffiService.getSelftestCoeffi());
    }

    @Log("自测数据修改")
    @ApiOperation(value = "自测数据修改")
    @PostMapping("editSelftestCoeffi")
    public Result editSelftestCoeffi(@Valid @RequestBody EditSelftestCoeffiVO vo) {
        salarySelftestCoeffiService.editSelftestCoeffi(vo);
        return ResultUtil.success();
    }

    @Log("方案创建")
    @ApiOperation(value = "方案创建")
    @PostMapping("addProgram")
    public Result addProgram(@Valid @RequestBody AddProgramVO vo) {
        return ResultUtil.success(salaryProgramService.addProgram(vo));
    }

    @Log("方案修改")
    @ApiOperation(value = "方案修改")
    @PostMapping("editProgram")
    public Result editProgram(@Valid @RequestBody EditProgramVO vo) {
        salaryProgramService.editProgram(vo);
        return ResultUtil.success();
    }

    @Log("方案获取")
    @ApiOperation(value = "方案获取")
    @GetMapping("getProgram")
    public Result getProgram(@RequestParam("gspId") Integer gspId) {
        return ResultUtil.success(salaryProgramService.getProgram(gspId));
    }

    @Log("方案列表")
    @ApiOperation(value = "方案列表")
    @PostMapping("queryProgramList")
    public Result queryProgramList(@Valid @RequestBody QueryProgramListVO vo) {
        return ResultUtil.success(salaryProgramService.queryProgramList(vo));
    }

    @Log("方案试算")
    @ApiOperation(value = "方案试算")
    @PostMapping("querySalaryTrial")
    public Result querySalaryTrial(@RequestBody @Valid QuerySalaryTrialVO vo) {
        salaryTrialUserService.querySalaryTrial(vo);
        return ResultUtil.success();
    }

    @Log("方案试算结果")
    @ApiOperation(value = "方案试算结果")
    @GetMapping("getSalaryTrial")
    public Result getSalaryTrial(@RequestParam("gspId") Integer gspId) {
        return ResultUtil.success(salaryTrialUserService.getSalaryTrial(gspId));
    }

    @Log("方案提交")
    @ApiOperation(value = "方案提交")
    @PostMapping("subTrial")
    public Result subTrial(@RequestBody @Valid SubTrialVO vo) {
        salaryProgramService.subTrial(vo);
        return ResultUtil.success();
    }

    @Log("适用范围")
    @ApiOperation(value = "适用范围")
    @GetMapping("salaryObjList")
    public Result salaryObjList() {
        return ResultUtil.success(salaryObjService.salaryObjList());
    }

    @Log("商品列表")
    @ApiOperation(value = "商品列表")
    @PostMapping("getProList")
    public Result getProList(@Valid @RequestBody GetProListVO vo) {
        return ResultUtil.success(salaryObjService.getProList(vo));
    }

    @Log("工资级别列表")
    @ApiOperation(value = "工资级别列表")
    @GetMapping("getSalaryGradeList")
    public Result getSalaryGradeList() {
        return ResultUtil.success(salaryGradeService.getSalaryGradeList());
    }

    @Log("岗位列表")
    @ApiOperation(value = "岗位列表")
    @GetMapping("getPostList")
    public Result getPostList() {
        return ResultUtil.success(postService.getPostList());
    }

}
