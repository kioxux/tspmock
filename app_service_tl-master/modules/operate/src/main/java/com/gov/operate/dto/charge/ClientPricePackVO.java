package com.gov.operate.dto.charge;

import com.gov.operate.entity.StoreData;
import com.gov.operate.entity.UserChargeMaintenanceM;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ClientPricePackVO extends UserChargeMaintenanceM {
    private List<StoreData> storeDataList;
    private BigDecimal spOfficialPrice;
    private BigDecimal spContractPrice;
}
