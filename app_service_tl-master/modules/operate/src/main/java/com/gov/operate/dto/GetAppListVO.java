package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetAppListVO extends Pageable {

    //    @NotBlank(message = "版本号不能为空")
    private String versionCode;

    //    @NotBlank(message = "发布时间开始时间不能为空")
    private String releaseTimeStart;

    //    @NotBlank(message = "发布时间开始结束不能为空")
    private String releaseTimeEnd;

    //    @NotNull(message = "是否强制更新不能为空  1:否,2:是")
    private Integer forceUpdateFlg;

    //    @NotNull(message = "是否发布不能为空 1:否,2:是")
    private Integer releaseFlag;

    //    @NotNull(message = "平台类型不能为空 1:android 2:ios")
    private Integer type;

    //    @NotBlank(message = "更新内容不能为空")
    private String content;

}
