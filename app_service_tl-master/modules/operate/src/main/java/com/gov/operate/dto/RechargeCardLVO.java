package com.gov.operate.dto;

import com.gov.operate.entity.SdRechargeCardPaycheck;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RechargeCardLVO extends SdRechargeCardPaycheck {

    private String gsrcAccountId;

    private String gsrcId;

    private String gsrcName;

    private String gsrcSex;

    private String gsrcMobile;

    private String gsrcAmt;

    private String gsrcpBrName;

    private String gsrccVoucherId;

    private String gsrccBrId;

    private String gsrccDate;

    private String gsrcpTime;

    private String gsrccType;

    private String gsrccOldCardId;

    private String gsrccNewCardId;

    private String gspmName;
    /**
     * 充值人员姓名
     */
    private String userName;
    /**
     * 支付编码
     */
    private String gsspmId;
    /**
     * 支付类型
     */
    private String gsspmType;
    /**
     * 已经退款flag (0:未退款 1:存在退款记录)
     */
    private Integer hasRefundFlag;

}
