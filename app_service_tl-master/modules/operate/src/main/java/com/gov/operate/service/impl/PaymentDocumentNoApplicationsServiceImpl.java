package com.gov.operate.service.impl;

import com.gov.operate.entity.PaymentDocumentNoApplications;
import com.gov.operate.mapper.PaymentDocumentNoApplicationsMapper;
import com.gov.operate.service.IPaymentDocumentNoApplicationsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-16
 */
@Service
public class PaymentDocumentNoApplicationsServiceImpl extends ServiceImpl<PaymentDocumentNoApplicationsMapper, PaymentDocumentNoApplications> implements IPaymentDocumentNoApplicationsService {

}
