package com.gov.operate.service;

import com.gov.operate.entity.ClientInvoiceInfo;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
public interface IClientInvoiceInfoService extends SuperService<ClientInvoiceInfo> {

}
