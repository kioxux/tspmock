package com.gov.operate.dto.invoice;

import com.gov.operate.entity.SalesInvoiceDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SalesInvoiceDetailItemDTO extends SalesInvoiceDetail {

    private String siteName;

    private String customerName;

    private String proName;

    private String gsiBill;

}
