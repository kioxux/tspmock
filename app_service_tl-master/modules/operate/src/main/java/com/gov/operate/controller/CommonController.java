package com.gov.operate.controller;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api("共通")
@RestController
@RequestMapping("common")
public class CommonController {

    @Resource
    private ISdYxmclassService sdYxmclassService;
    @Resource
    private IStogDataService stogDataService;
    @Resource
    private ISdMemberClassService memberClassService;
    @Resource
    private ISdTagListService sdTagListService;
    @Resource
    private ISdGradeListService sdGradeListService;

    @Resource
    private IStoreDataService storeDataService;

    @Resource
    private ISdStoresGroupService sdStoresGroupService;

    @Log("商品组及商品集合")
    @ApiOperation(value = "商品组及商品集合")
    @GetMapping("getGroupAndProductList")
    public Result getGroupAndProductList(@RequestParam("gsmStore") String gsmStore) {
        return sdYxmclassService.selectGroupAndProductList(gsmStore);
    }

    @Log("门店组集合")
    @ApiOperation(value = "门店组集合")
    @GetMapping("getStogDateList")
    public Result getStogDateList() {
        return stogDataService.selectStogDateList();
    }

    @Log("会员卡类型集合")
    @ApiOperation(value = "会员卡类型集合")
    @GetMapping("getMemberClassList")
    public Result getMemberClassList() {
        return ResultUtil.success(memberClassService.selectMemberClassList());
    }

    @Log("会员标签集合")
    @ApiOperation(value = "会员标签集合")
    @GetMapping("getTagList")
    public Result getMemberTagList() {
        return ResultUtil.success(sdTagListService.getTagList());
    }

    @Log("会员分级集合")
    @ApiOperation(value = "会员分级集合")
    @GetMapping("getGradeList")
    public Result getMemberGradeList() {
        return ResultUtil.success(sdGradeListService.getGradeList());
    }

    @Log("当前人门店列表")
    @ApiOperation(value = "当前人门店列表")
    @GetMapping("getCurrentStoreList")
    public Result getCurrentStoreList() {
        return storeDataService.getCurrentStoreList();
    }

    @Log("当前加盟商下所有的门店")
    @ApiOperation(value = "当前加盟商下所有的门店")
    @GetMapping("getStoreList")
    public Result getStoreList() {
        return ResultUtil.success(storeDataService.getStoreList());
    }

    @Log("门店分类集合")
    @ApiOperation(value = "门店分类集合")
    @GetMapping("getStoresGroupList")
    public Result getStoresGroupList() {
        return sdStoresGroupService.getStoresGroupList();
    }

    @Log("门店集合")
    @ApiOperation(value = "门店集合")
    @PostMapping("getStoresList")
    public Result getStoresList() {
        return sdStoresGroupService.getStoresList();
    }

}
