package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SdStoresGroup;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
public interface ISdStoresGroupService extends SuperService<SdStoresGroup> {


    Result getStoresGroupList();

    Result getStoresList();
}
