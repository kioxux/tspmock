package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.invoice.SupplierPrepaymentDto;
import com.gov.operate.entity.SupplierPrepayment;
import com.gov.operate.feign.dto.ApprovalInfo;
import com.gov.operate.feign.dto.FeignResult;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2021-02-01
 */
public interface ISupplierPrepaymentService extends SuperService<SupplierPrepayment> {

    Result addSupplierPayment(SupplierPrepaymentDto supplierPrepayment);

    Result applySupplierPayment(SupplierPrepaymentDto supplierPrepayment);

    Result getSupplierPaymentList(Integer pageSize, Integer pageNum, String gspApplyDateStart, String gspApplyDateEnd, String payCompanyCode,
                                  String supCode, String payOrderId, BigDecimal applicationPaymentAmountStart, BigDecimal applicationPaymentAmountEnd,
                                  String founder, String applicationStatus, String gspApprovalDateStart, String gspApprovalDateEnd, String supSalesman,
                                  String approvalRemark, String remark);

    Result getCompanyList();

    FeignResult payCallBack(ApprovalInfo info);

    Result getCustomerList(String matSiteCode);

    Result getDcList();

    Result supplierPaymentWriteOff(SupplierPrepaymentDto supplierPrepaymentDto);

    Result deleteWriteOff(List<Integer> idList);

    Result getWriteOffList(Integer pageSize, Integer pageNum, String gspApplyDateStart, String gspApplyDateEnd, String orderCode,
                           String supSelfCode, String payCompanyCode, String type, String invoiceSalesman);

    Result viewWriteOffList(Integer pageSize, Integer pageNum, String payOrderId);

    Result getSupplierPaymentExport(String gspApplyDateStart, String gspApplyDateEnd, String payCompanyCode, String supCode, String payOrderId,
                                    BigDecimal applicationPaymentAmountStart, BigDecimal applicationPaymentAmountEnd, String founder, String applicationStatus,
                                    String gspApprovalDateStart, String gspApprovalDateEnd, String supSalesman, String approvalRemark, String wfDescription) throws IOException;

    /**
     * 备注下拉框
     *
     * @return
     */
    Result getApprovalRemarkList();
}
