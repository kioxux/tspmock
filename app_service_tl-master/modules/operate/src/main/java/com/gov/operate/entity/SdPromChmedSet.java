package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 中药类促销设置表
 * </p>
 *
 * @author sy
 * @since 2021-07-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_CHMED_SET")
@ApiModel(value="SdPromChmedSet对象", description="中药类促销设置表")
public class SdPromChmedSet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPMS_VOUCHER_ID")
    private String gspmsVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPMS_SERIAL")
    private String gspmsSerial;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPMS_QTY1")
    private String gspmsQty1;

    @ApiModelProperty(value = "生成促销折扣1")
    @TableField("GSPMS_REBATE1")
    private String gspmsRebate1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPMS_QTY2")
    private String gspmsQty2;

    @ApiModelProperty(value = "生成促销折扣2")
    @TableField("GSPMS_REBATE2")
    private String gspmsRebate2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPMS_QTY3")
    private String gspmsQty3;

    @ApiModelProperty(value = "生成促销折扣3")
    @TableField("GSPMS_REBATE3")
    private String gspmsRebate3;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPMS_MEM_FLAG")
    private String gspmsMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPMS_INTE_FLAG")
    private String gspmsInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPMS_INTE_RATE")
    private String gspmsInteRate;


}
