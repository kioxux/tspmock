package com.gov.operate.tpns;

import com.tencent.xinge.bean.Environment;
import com.tencent.xinge.bean.Platform;

import java.util.List;
import java.util.Map;


public class PushContent implements java.io.Serializable {
    private static final long serialVersionUID = 4517702940149420382L;

    public final static String TYPE = "TYPE";

    public final static String MESSAGEID = "MESSAGEID";

    public final static String PARAMS = "PARAMS";

    //平台
    private Platform platform;

    //标题
    private String title;

    //内容
    private String content;

    // 苹果用
    private Environment environment;

    /**
     * 附加数据
     */
    private Map<String, Object> attachKeys;

    /**
     * 发送的目标帐号
     */
    private List<String> accountList;

    public Platform getPlatform() {
        return platform;
    }


    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public Map<String, Object> getAttachKeys() {
        return attachKeys;
    }


    public void setAttachKeys(Map<String, Object> attachKeys) {
        this.attachKeys = attachKeys;
    }


    public List<String> getAccountList() {
        return accountList;
    }


    public void setAccountList(List<String> accountList) {
        this.accountList = accountList;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    /**
     * 推送类型
     */
    public enum Type {
        PAYREMINDER(1, "门店付款");

        Type(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        private Integer code;

        private String message;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}


