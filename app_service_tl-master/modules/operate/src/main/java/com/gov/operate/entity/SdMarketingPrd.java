package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING_PRD")
@ApiModel(value = "SdMarketingPrd对象", description = "")
public class SdMarketingPrd extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "营销活动加盟商分配表主键")
    @TableField("MARKETING_ID")
    private Integer marketingId;

    @ApiModelProperty(value = "促销关联表")
    @TableField("PROM_ID")
    private Integer promId;

    @ApiModelProperty(value = "商品编码")
    @TableField("PRO_CODE")
    private String proCode;

    @ApiModelProperty(value = "促销单价")
    @TableField("GSMP_PRICE")
    private BigDecimal gsmpPrice;

    @ApiModelProperty(value = "促销毛利率")
    @TableField("GSMP_FITS")
    private BigDecimal gsmpFits;

    @ApiModelProperty(value = "促销单价")
    @TableField("GSMP_PRICE1")
    private BigDecimal gsmpPrice1;

    @ApiModelProperty(value = "促销毛利率")
    @TableField("GSMP_FITS1")
    private BigDecimal gsmpFits1;

    @ApiModelProperty(value = "促销单价")
    @TableField("GSMP_PRICE2")
    private BigDecimal gsmpPrice2;

    @ApiModelProperty(value = "促销毛利率")
    @TableField("GSMP_FITS2")
    private BigDecimal gsmpFits2;

    @ApiModelProperty(value = "促销单价")
    @TableField("GSMP_PRICE3")
    private BigDecimal gsmpPrice3;

    @ApiModelProperty(value = "促销毛利率")
    @TableField("GSMP_FITS3")
    private BigDecimal gsmpFits3;

    @ApiModelProperty(value = "促销单号")
    @TableField("GSPAC_VOUCHER_ID")
    private String gspacVoucherId;

    @ApiModelProperty(value = "促销行号")
    @TableField("GSPAC_SERIAL")
    private String gspacSerial;
}
