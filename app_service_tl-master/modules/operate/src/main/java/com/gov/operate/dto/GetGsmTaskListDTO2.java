package com.gov.operate.dto;

import com.gov.operate.entity.SdMarketingClient;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetGsmTaskListDTO2 extends SdMarketingClient {

    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 推送人
     */
    private String userNam;

    /**
     * 推送时间
     */
    private String gsmPushTime;
}
