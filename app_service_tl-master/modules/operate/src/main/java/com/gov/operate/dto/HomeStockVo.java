package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

@Data
public class HomeStockVo {

    private String client;

    /**
     * 门店数量
     */
    private String storeCount = "0";

    /**
     * 库存品项数
     */
    private String stkCount = "0";

    /**
     * 库存成本额
     */
    private String stkAmt = "0";

    /**
     * 周转天数
     */
    private String turnoverDays = "0";

    /**
     * 效期数量
     */
    private String expiryCount;

    /**
     * 是否效期预警 0 否 1 是
     */
    private String isExpiry;

    /**
     * 缺货数量
     */
    private String outStockCount;

    /**
     * 是否缺断货预警 0 否 1 是
     */
    private String isOutStock;

    /**
     * 是否连锁公司 0 否 1 是
     */
    private String isChain;

    /**
     * 门店是否显示缺断货标志 0-不显示 1-显示
     */
    private String stoShowFlag;

    /**
     * 是否展示品类模块 0-不显示 1-显示
     */
    private String isShowModel;
}
