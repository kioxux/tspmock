package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchStoreStock;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 门店库存 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchStoreStockService extends SuperService<BatchStoreStock> {

}
