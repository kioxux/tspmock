package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.electron.ElectronReserved;
import com.gov.operate.entity.SdElectronChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 电子券异动表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-07-09
 */
public interface SdElectronChangeMapper extends BaseMapper<SdElectronChange> {

    /**
     * 预留优惠券
     *
     * @param client
     * @return
     */
    List<ElectronReserved> selectElectronReserved(@Param("client") String client);

    Integer selectGsecId(@Param("client") String client, @Param("gsecId") String gsecId);

    /**
     * 批量插入
     *
     * @param change
     */
    void insertBatch(@Param("list") List<SdElectronChange> change);

    /**
     * 老会员预留优惠券
     *
     * @param client
     * @param gseasId
     * @return
     */
    List<ElectronReserved> selectElectronReservedList(@Param("client") String client, @Param("gseasId") List<String> gseasId);
}
