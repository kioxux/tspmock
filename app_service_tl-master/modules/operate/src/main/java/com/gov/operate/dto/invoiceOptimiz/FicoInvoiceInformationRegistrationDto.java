package com.gov.operate.dto.invoiceOptimiz;

import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.15
 */
@Data
public class FicoInvoiceInformationRegistrationDto extends FicoInvoiceInformationRegistration {

    /**
     * 原发票号，修改用
     */
    private String oldInvoiceNum;

    /**
     * 地点名
     */
    private String siteName;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 状态（0:未对账 1:已对账）
     */
    private Integer status;

    /**
     * 对账日期
     */
    private String gibCreateDate;

    /**
     * 业务员
     */
    private String invoiceSalesmanName;
}

