package com.gov.operate.dto;

import com.gov.operate.entity.SdStockBatch;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.29
 */
@Data
public class BatchNoValidDto extends SdStockBatch {

    /**
     * 效期
     */
    private String gscdValidDate;
}

