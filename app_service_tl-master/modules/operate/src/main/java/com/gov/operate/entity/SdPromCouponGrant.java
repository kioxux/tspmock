package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_COUPON_GRANT")
@ApiModel(value="SdPromCouponGrant对象", description="")
public class SdPromCouponGrant extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPCG_VOUCHER_ID")
    private String gspcgVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPCG_SERIAL")
    private String gspcgSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPCG_PRO_ID")
    private String gspcgProId;

    @ApiModelProperty(value = "电子券活动号")
    @TableField("GSPCG_ACT_NO")
    private String gspcgActNo;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPCG_REACH_QTY1")
    private String gspcgReachQty1;

    @ApiModelProperty(value = "达到金额1")
    @TableField("GSPCG_REACH_AMT1")
    private BigDecimal gspcgReachAmt1;

    @ApiModelProperty(value = "送券数量1")
    @TableField("GSPCG_RESULT_QTY1")
    private String gspcgResultQty1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPCG_REACH_QTY2")
    private String gspcgReachQty2;

    @ApiModelProperty(value = "达到金额2")
    @TableField("GSPCG_REACH_AMT2")
    private BigDecimal gspcgReachAmt2;

    @ApiModelProperty(value = "送券数量2")
    @TableField("GSPCG_RESULT_QTY2")
    private String gspcgResultQty2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPCG_REACH_QTY3")
    private String gspcgReachQty3;

    @ApiModelProperty(value = "达到金额3")
    @TableField("GSPCG_REACH_AMT3")
    private BigDecimal gspcgReachAmt3;

    @ApiModelProperty(value = "送券数量3")
    @TableField("GSPCG_RESULT_QTY3")
    private String gspcgResultQty3;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPCG_MEM_FLAG")
    private String gspcgMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPCG_INTE_FLAG")
    private String gspcgInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPCG_INTE_RATE")
    private String gspcgInteRate;


}
