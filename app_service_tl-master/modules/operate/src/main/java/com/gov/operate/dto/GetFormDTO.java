package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/15 16:44
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetFormDTO {

    /**
     * 商品自编号
     */
    private String proSelfCode;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 批准文号
     */
    private String proRegisterNo;
}
