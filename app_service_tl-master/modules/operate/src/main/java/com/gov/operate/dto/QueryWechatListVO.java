package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryWechatListVO extends Pageable {

    /**
     * 公众号名
     */
    private String wechatName;

    /**
     * 类型 1:门店，2：连锁
     */
    private Integer goaType;

    /**
     * 加盟商
     */
    private String client;

}
