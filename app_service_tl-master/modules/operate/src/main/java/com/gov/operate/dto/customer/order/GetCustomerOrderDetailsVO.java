package com.gov.operate.dto.customer.order;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021-06-22 9:47
 */
@Data
public class GetCustomerOrderDetailsVO {

    @NotNull(message = "id不能为空")
    private Long id;
}
