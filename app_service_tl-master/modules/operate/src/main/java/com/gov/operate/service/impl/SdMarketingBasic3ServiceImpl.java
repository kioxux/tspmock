package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.entity.SmsEntity;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.operate.service.marketing.ISdMarketingSearch3Service;
import com.gov.operate.service.smsRecharge.ISmsRechargeRecordService;
import com.gov.operate.tpns.TpnsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.CaseUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Slf4j
@Service
public class SdMarketingBasic3ServiceImpl extends ServiceImpl<SdMarketingBasicMapper, SdMarketingBasic> implements ISdMarketingBasic3Service {

    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingBasicMapper marketingBasicMapper;
    @Resource
    private SdMarketingClientMapper sdMarketingClientMapper;
    @Resource
    private ISdMarketingClientService marketingClientService;
    @Resource
    private ISdMarketingPromService marketingPromService;
    @Resource
    private ISdMarketingGiveawayService marketingGiveawayService;
    @Resource
    private ISdPromVariableSetService promVariableSetService;
    @Resource
    private ISdMarketingStoService marketingStoService;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private ISdMarketingSearchService sdMarketingSearchService;
    @Resource
    private ISdMarketingBasicAttacService sdMarketingBasicAttacService;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private SdMarketingPrdMapper sdMarketingPrdMapper;
    @Resource
    private SdMarketingPromMapper sdMarketingPromMapper;
    @Resource
    private IFranchiseeService franchiseeService;
    @Resource
    private SdMarketingGiveawayMapper sdMarketingGiveawayMapper;
    @Resource
    private SdPromVariableSetMapper sdPromVaribaleSetMapper;
    @Resource
    private SdMarketingSearchMapper marketingSearchMapper;
    @Resource
    private SmsTemplateMapper smsTemplateMapper;
    @Resource
    private ISmsTemplateService smsTemplateService;
    @Resource
    private SmsUtils smsUtils;
    @Resource
    private IMessageService messageService;
    @Resource
    private ISmsRechargeRecordService smsRechargeRecordService;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;

    /**
     * 营销设置列表
     */
    @Override
    public IPage<SdMarketingBasicDTO3> getGsmSettingList(GetGsmSettingListVO3 vo) {
        // 分页对象
        Page<SdMarketingBasicDTO3> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        return marketingBasicMapper.getGsmSettingList(page, vo);
    }

    /**
     * 营销设置保存
     */
    @Override
    public void saveGsmSetting(SaveGsmSettingVO3 vo) {
        TokenUser user = commonService.getLoginInfo();

        SdMarketingBasic basic = new SdMarketingBasic();
        BeanUtils.copyProperties(vo, basic);
        // 营销活动id
        String maxNumMarketId = getMaxNumMarketId();
        // 营销主题活动id
        basic.setGsmMarketid(maxNumMarketId);
        // 营销主题ID
        basic.setGsmThenid(maxNumMarketId);
        // 创建人加盟商
        basic.setGsmCreateClient(user.getClient());
        // 创建人
        basic.setGsmCreateUserId(user.getUserId());
        // 创建日期
        basic.setGsmCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 是否下发(0已下发，1未下发)
        basic.setGsmRele("1");
        // 删除标记(0-否，1-是)
        basic.setGsmDeleteFlag("0");
        this.save(basic);

        // 宣传物料设置
        List<SdMarketingBasicAttac> attacList = vo.getMarketingBasicAttacList();
        if (CollectionUtils.isEmpty(attacList)) {
            return;
        }
        attacList.forEach(item -> item.setBasicId(basic.getId()));
        sdMarketingBasicAttacService.saveBatch(attacList);
    }

    /**
     * 营销设置编辑
     */
    @Override
    public void editGsmSetting(EditGsmSettingVO3 vo) {
        if (StringUtils.isEmpty(vo.getId())) {
            throw new CustomResultException("Id不能为空");
        }
        SdMarketingBasic basic = new SdMarketingBasic();
        BeanUtils.copyProperties(vo, basic);
        this.updateById(basic);

        // 宣传物料设置 全删全插
        List<SdMarketingBasicAttac> attacList = vo.getMarketingBasicAttacList();
        if (CollectionUtils.isEmpty(attacList)) {
            return;
        }
        sdMarketingBasicAttacService.remove(new QueryWrapper<SdMarketingBasicAttac>().eq("BASIC_ID", basic.getId()));
        attacList.forEach(item -> item.setBasicId(basic.getId()));
        sdMarketingBasicAttacService.saveBatch(attacList);
    }

    /**
     * 营销设置详情
     */
    @Override
    public GetGsmSettingDetailDTO3 getGsmSettingDetail(Integer id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        SdMarketingBasic marketingBasic = this.getById(id);
        if (ObjectUtils.isEmpty(marketingBasic)) {
            throw new CustomResultException("未查询到该营销设置");
        }
        GetGsmSettingDetailDTO3 dto = new GetGsmSettingDetailDTO3();
        BeanUtils.copyProperties(marketingBasic, dto);
        // 宣传物料设置
        List<SdMarketingBasicAttac> attacOriginalList = sdMarketingBasicAttacService.list(new QueryWrapper<SdMarketingBasicAttac>().eq("BASIC_ID", id));
        List<SdMarketingBasicAttacDTO> attacList = attacOriginalList.stream().filter(Objects::nonNull).map(attac -> {
            SdMarketingBasicAttacDTO attacDTO = new SdMarketingBasicAttacDTO();
            BeanUtils.copyProperties(attac, attacDTO);
            attacDTO.setGsmbaJpgUrl(cosUtils.urlAuth(attac.getGsmbaJpg()));
            attacDTO.setGsmbaPsUrl(cosUtils.urlAuth(attac.getGsmbaPs()));
            attacDTO.setGsmbaScenesUrl(cosUtils.urlAuth(attac.getGsmbaScenes()));
            return attacDTO;
        }).collect(toList());
        dto.setMarketingBasicAttacList(attacList);
        // 推送加盟商
        List<Franchisee> clientIdList = marketingBasicMapper.getClientList(id);
        dto.setClientList(clientIdList);
        return dto;
    }

    /**
     * 营销设置删除
     */
    @Override
    public void deleteMarketSetting(Integer id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        SdMarketingBasic marketingBasic = this.getById(id);
        if (ObjectUtils.isEmpty(marketingBasic)) {
            throw new CustomResultException("该数据已经被删除");
        }
        if (!ObjectUtils.isEmpty(marketingBasic.getGsmRele()) && marketingBasic.getGsmRele().equals("0")) {
            throw new CustomResultException("该营销设置已经下发,不可删除");
        }
        this.removeById(id);
    }

    /**
     * 活动方案推送
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pushGsm(PushGsmVO3 vo) {

        SdMarketingBasic marketingBasic = this.getById(vo.getId());
        if (ObjectUtils.isEmpty(marketingBasic)) {
            throw new CustomResultException("该营销活动已经被删除");
        }
        if (StringUtils.isNotEmpty(marketingBasic.getGsmRele()) && marketingBasic.getGsmRele().equals("0")) {
            throw new CustomResultException("该营销活动已经推送,不可重复推送");
        }

        if (vo.getReleType().equals(1)) {
            // 定时推送
            if (StringUtils.isEmpty(vo.getGsmPlanPushTime()) || vo.getGsmPlanPushTime().length() != 10) {
                throw new CustomResultException("推送时间格式不正确");
            }
            if (StringUtils.isEmpty(vo.getGsmPlanPushTime())) {
                throw new CustomResultException("[定时推送]方式[计划推送时间]不能空");
            }
            if (!StringUtils.isEmpty(marketingBasic.getGsmPushClient())) {
                throw new CustomResultException("该营销活动已经被定时推送,不可重复推送");
            }
            SdMarketingBasic marketingBasicDO = new SdMarketingBasic();
            marketingBasicDO.setId(vo.getId());
            marketingBasicDO.setGsmPlanPushTime(vo.getGsmPlanPushTime());
            marketingBasicDO.setGsmPushClient(String.join(",", vo.getClientList()));
            this.updateById(marketingBasicDO);
        } else {
            // 立即推送
            // [计划推送时间,推送加盟商]字段置空 (原因:可能之前已经设置定时推送)
            if (!StringUtils.isEmpty(marketingBasic.getGsmPushClient())) {
                SdMarketingBasic marketingBasicDO = new SdMarketingBasic();
                marketingBasicDO.setId(vo.getId());
                marketingBasicDO.setGsmPlanPushTime("");
                marketingBasicDO.setGsmPushClient("");
                this.updateById(marketingBasicDO);
            }

            TokenUser user = commonService.getLoginInfo();
            String client = user.getClient();
            String userId = user.getUserId();
            pushGsmCom(marketingBasic, vo.getId(), vo.getClientList(), client, userId);

            // 消息推送
            tpnsMsg(vo.getClientList(), marketingBasic.getGsmThename());
        }
    }


    /**
     * 营销设置推送定时器
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void marketingPushReminder() {
        // 当前时间 yyyyMMddHH
        String localDateTime = DateUtils.formatLocalDateTime(LocalDateTime.now(), "yyyyMMddHH");
        // 查询
        List<SdMarketingBasic> basicList = this.list(new QueryWrapper<SdMarketingBasic>()
                // 未下发
                .eq("GSM_RELE", 1)
                // 计划推送时间
                .eq("GSM_PLAN_PUSH_TIME", localDateTime));
        if (CollectionUtils.isEmpty(basicList)) {
            return;
        }
        basicList.stream().filter(Objects::nonNull).forEach(basic -> {
            if (StringUtils.isEmpty(basic.getGsmPushClient())) {
                return;
            }

            Integer id = basic.getId();
            List<String> clientList = Arrays.stream(basic.getGsmPushClient().split(",")).collect(toList());
            pushGsmCom(basic, id, clientList, null, null);
        });

        // 消息推送
        basicList.stream().filter(Objects::nonNull).forEach(basic -> {
            List<String> clientList = Arrays.stream(basic.getGsmPushClient().split(",")).collect(toList());
            tpnsMsg(clientList, basic.getGsmThename());
        });
    }

    /**
     * 推送具体执行
     */
    private void pushGsmCom(SdMarketingBasic marketingBasic, Integer basicId, List<String> clientList, String pushUserClient, String pushUserId) {
        /**
         * 1. basic 表数据更新
         */
        SdMarketingBasic basic = new SdMarketingBasic();
        // id
        basic.setId(basicId);
        // 是否下发(0已下发，1未下发)
        basic.setGsmRele("0");
        // 下发时间
        basic.setGsmReletime(DateUtils.getCurrentDateTimeStrTwo());
        // 推送时间 (下发就是推送)
        basic.setGsmPushTime(DateUtils.getCurrentDateTimeStrTwo());
        // 推送人加盟商
        basic.setGsmPushUserClinet(pushUserClient);
        // 推送人
        basic.setGsmPushUser(pushUserId);
        this.updateById(basic);

        /**
         * 2. GAIA_SD_MARKETING_CLIENT 营销活动加盟商分配表 数据插入
         */
        List<SmsTemplate> smsTemplates = new ArrayList<>();
        List<SdMarketingClient> marketingClientList = clientList.stream().map(item -> {
            SdMarketingClient marketingClient = new SdMarketingClient();
            BeanUtils.copyProperties(marketingBasic, marketingClient);
            // id
            marketingClient.setId(null);
            // basic表 Id
            marketingClient.setBasicId(marketingBasic.getId());
            // 加盟商
            marketingClient.setClient(item);
            // 审批状态(-1,未审批，0，审批中，1已审批，2，作废，3，立即执行)
            marketingClient.setGsmImpl("-1");
            // 短信模板处理
            initSmsTemp(marketingClient, smsTemplates);
            return marketingClient;
        }).collect(toList());
        marketingClientService.saveBatch(marketingClientList);
        if (!CollectionUtils.isEmpty(smsTemplates)) {
            smsTemplateService.saveBatch(smsTemplates);
        }

        /**
         * 4.GAIA_SD_MARKETING_PROM 营销活动促销关联表 数据插入
         */
        // 促销json
        String gsmProcondi = marketingBasic.getGsmProcondi();
        if (StringUtils.isEmpty(gsmProcondi)) {
            return;
        }
        // 促销实体
        GsmProcondiDTO gsmProcondiDTO = null;
        try {
            gsmProcondiDTO = JSON.parseObject(gsmProcondi, GsmProcondiDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("促销json转实体异常");
            throw e;
        }
        if (ObjectUtils.isEmpty(gsmProcondiDTO)) {
            throw new CustomResultException("促销方式不能为空");
        }
        List<SdMarketingProm> marketingPromList = new ArrayList<>();
        for (SdMarketingClient marketingClient : marketingClientList) {
            List<GsmProcondiDTO.ProComp> proCompList = gsmProcondiDTO.getProCompclass();
            for (GsmProcondiDTO.ProComp proComp : proCompList) {
                SdMarketingProm marketingProm = new SdMarketingProm();
                // 加盟商
                marketingProm.setClient(marketingClient.getClient());
                // 营销活动加盟商分配表主键
                marketingProm.setMarketingId(marketingClient.getId());
                // 成分
                marketingProm.setComponentId(proComp.getComponent());
                // 拷贝 (成分,类型,促销方式,促销方式描述,促销类型,促销类型描述,备注1,备注2)
                BeanUtils.copyProperties(proComp, marketingProm);
                // 拷贝 具体的促销方式
                BeanUtils.copyProperties(proComp.getProm(), marketingProm);
                // 系列
                // 达到金额1
                marketingProm.setGspssReachAmt1(StringUtils.isNotBlank(proComp.getProm().getGspssReachAmt1()) ? new BigDecimal(proComp.getProm().getGspssReachAmt1()) : null);
                // 结果减额1
                marketingProm.setGspssResultAmt1(StringUtils.isNotBlank(proComp.getProm().getGspssResultAmt1()) ? new BigDecimal(proComp.getProm().getGspssResultAmt1()) : null);
                // 赠品
                // 达到金额1
                marketingProm.setGspgsReachAmt1(StringUtils.isNotBlank(proComp.getProm().getGspgsReachAmt1()) ? new BigDecimal(proComp.getProm().getGspgsReachAmt1()) : null);
                // 赠送数量1
                marketingProm.setGspgsResultQty1(proComp.getProm().getGspgsResultQty1());
                // 组合
                // 赠品单品价格
                marketingProm.setGsparGiftPrc(StringUtils.isNotBlank(proComp.getProm().getGsparGiftPrc()) ? new BigDecimal(proComp.getProm().getGsparGiftPrc()) : null);
                // 组合金额
                marketingProm.setGspasAmt(StringUtils.isNotBlank(proComp.getProm().getGspasAmt()) ? new BigDecimal(proComp.getProm().getGspasAmt()) : null);

                // 系列编码
                marketingProm.setGspgsSeriesProId(marketingProm.getGspgcSeriesId());
                marketingProm.setGspgrSeriesId(marketingProm.getGspgcSeriesId());
                marketingProm.setGsparSeriesId(marketingProm.getGspacSeriesId());
                marketingProm.setGspasSeriesId(marketingProm.getGspacSeriesId());

                marketingPromList.add(marketingProm);
            }
        }
        marketingPromService.saveBatch(marketingPromList);

        /**
         * 5. 数据准备（product_business商品表中 对应成分下的所有商品 都 保存一份到 marketing_prd活动商品表）
         */
        List<Integer> promIdList = marketingPromList.stream().map(SdMarketingProm::getId).collect(toList());

        // 商品查询
        List<SdMarketingPrd> marketingPrdDOList = new ArrayList<>();
        List<SelectProBatchFromBusinessDTO> marketingProDTOList = marketingBasicMapper.selectProBatchFromProductBusiness(promIdList);
        Map<Integer, List<SelectProBatchFromBusinessDTO>> marketingProDTOMap = marketingProDTOList.stream().collect(groupingBy(SelectProBatchFromBusinessDTO::getPromId));
        marketingProDTOMap.forEach((promId, proDTOList) -> {
            // 促销方式
            SdMarketingProm marketingProm = marketingPromList.stream().filter(prom -> prom.getId().equals(promId)).findFirst().orElse(null);
            proDTOList.forEach(prdDTO -> {
                SdMarketingPrd prdDO = new SdMarketingPrd();
                BeanUtils.copyProperties(prdDTO, prdDO);
                marketingPrdDOList.add(prdDO);
                if (ObjectUtils.isEmpty(prdDTO.getPriceCost()) || ObjectUtils.isEmpty(prdDTO.getPriceNormal()) || ObjectUtils.isEmpty(marketingProm)) {
                    return;
                }
                // 单品类
                if (marketingProm.getGspvsType().equals("PROM_SINGLE")) {
                    // 单品打折
                    if (marketingProm.getGspvsMode().equals("SINGLE-1")) {
                        // 1 [促销单价1=零售价×生成促销折扣1/100], [促销毛利率1=（促销单价1-成本价）/促销单价1]
                        if (!ObjectUtils.isEmpty(marketingProm.getGspusRebate1()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty1())) {
                            BigDecimal gsmpPrice1 = prdDTO.getPriceNormal().multiply(new BigDecimal(NumberUtils.toDouble(marketingProm.getGspusRebate1(), 1)))
                                    .divide(BigDecimal.valueOf(100), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            BigDecimal gsmpFits1 = gsmpPrice1.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice1.subtract(prdDTO.getPriceCost()).divide(gsmpPrice1, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            prdDO.setGsmpPrice1(gsmpPrice1);
                            prdDO.setGsmpFits1(gsmpFits1);
                        }

                        // 2 [促销单价2=零售价×生成促销折扣2/100], [促销毛利率2=（促销单价2-成本价）/促销单价2]
                        if (!ObjectUtils.isEmpty(marketingProm.getGspusRebate2()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty2())) {
                            BigDecimal gsmpPrice2 = prdDTO.getPriceNormal().multiply(new BigDecimal(NumberUtils.toDouble(marketingProm.getGspusRebate2(), 1)))
                                    .divide(BigDecimal.valueOf(100), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            BigDecimal gsmpFits2 = gsmpPrice2.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice2.subtract(prdDTO.getPriceCost()).divide(gsmpPrice2, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            prdDO.setGsmpPrice2(gsmpPrice2);
                            prdDO.setGsmpFits2(gsmpFits2);
                        }

                        // 3 [促销单价3=零售价×生成促销折扣3/100], [促销毛利率3=（促销单价3-成本价）/促销单价3]
                        if (!ObjectUtils.isEmpty(marketingProm.getGspusRebate3()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty3())) {
                            BigDecimal gsmpPrice3 = prdDTO.getPriceNormal().multiply(new BigDecimal(NumberUtils.toDouble(marketingProm.getGspusRebate3(), 1)))
                                    .divide(BigDecimal.valueOf(100), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            BigDecimal gsmpFits3 = gsmpPrice3.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice3.subtract(prdDTO.getPriceCost()).divide(gsmpPrice3, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            prdDO.setGsmpPrice3(gsmpPrice3);
                            prdDO.setGsmpFits3(gsmpFits3);
                        }
                    }
                    // 单品特价
                    if (marketingProm.getGspvsMode().equals("SINGLE-2")) {
                        // 1 [促销单价1=生成促销价1/达到数量1],[促销毛利率1=（促销单价1-成本价）/促销单价1]
                        if (!ObjectUtils.isEmpty(marketingProm.getGspusPrc1()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty1())) {
                            BigDecimal gsmpPrice1 = marketingProm.getGspusPrc1().divide(new BigDecimal(NumberUtils.toDouble(marketingProm.getGspusQty1(), 1)), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            BigDecimal gsmpFits1 = gsmpPrice1.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice1.subtract(prdDTO.getPriceCost()).divide(gsmpPrice1, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            prdDO.setGsmpPrice1(gsmpPrice1);
                            prdDO.setGsmpFits1(gsmpFits1);
                        }

                        // 2 [促销单价2=生成促销价2/达到数量2],[促销毛利率2=（促销单价2-成本价）/促销单价2]
                        if (!ObjectUtils.isEmpty(marketingProm.getGspusPrc2()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty2())) {
                            BigDecimal gsmpPrice2 = marketingProm.getGspusPrc2().divide(new BigDecimal(NumberUtils.toDouble(marketingProm.getGspusQty2(), 1)), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            BigDecimal gsmpFits2 = gsmpPrice2.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice2.subtract(prdDTO.getPriceCost()).divide(gsmpPrice2, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            prdDO.setGsmpPrice2(gsmpPrice2);
                            prdDO.setGsmpFits2(gsmpFits2);
                        }

                        // 3 [促销单价3=生成促销价3/达到数量3],[促销毛利率3=（促销单价3-成本价）/促销单价3]
                        if (!ObjectUtils.isEmpty(marketingProm.getGspusPrc3()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty3())) {
                            BigDecimal gsmpPrice3 = marketingProm.getGspusPrc3().divide(new BigDecimal(NumberUtils.toDouble(marketingProm.getGspusQty3(), 1)), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            BigDecimal gsmpFits3 = gsmpPrice3.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice3.subtract(prdDTO.getPriceCost()).divide(gsmpPrice3, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                            prdDO.setGsmpPrice3(gsmpPrice3);
                            prdDO.setGsmpFits3(gsmpFits3);
                        }
                    }
                }
            });
        });
        if (!CollectionUtils.isEmpty(marketingPrdDOList)) {
            marketingBasicMapper.insertProBatchFromProductBusiness2(marketingPrdDOList);
        }
    }

    /**
     * 短信模板
     *
     * @param marketingClient
     * @param smsTemplates
     */
    private void initSmsTemp(SdMarketingClient marketingClient, List<SmsTemplate> smsTemplates) {
        // 短信模板处理
        String gsmSmscondi = marketingClient.getGsmSmscondi();
        if (StringUtils.isNotBlank(gsmSmscondi)) {
            List<SdMarketingSearch> sdMarketingSearcheList = JsonUtils.jsonToBeanList(marketingClient.getGsmSmscondi(), SdMarketingSearch.class);
            if (!CollectionUtils.isEmpty(sdMarketingSearcheList)) {
                for (SdMarketingSearch sdMarketingSearch : sdMarketingSearcheList) {
                    if (StringUtils.isNotBlank(sdMarketingSearch.getGsmsSmsTemp())) {
                        SmsTemplate smsTemplate = smsTemplateMapper.selectOne(new QueryWrapper<SmsTemplate>().eq("SMS_ID", sdMarketingSearch.getGsmsSmsTemp()));
                        if (StringUtils.isNotBlank(smsTemplate.getClient()) && smsTemplate.getClient().equals(marketingClient.getClient())) {
                            return;
                        }
                        if (smsTemplate != null) {
                            List<SmsTemplate> smsTemplateList = smsTemplateMapper.selectList(
                                    new QueryWrapper<SmsTemplate>().eq("CLIENT", marketingClient.getClient())
                                            .eq("SMS_SOURCE", sdMarketingSearch.getGsmsSmsTemp())
                                            .eq("SMS_CONTENT", smsTemplate.getSmsContent())
                            );
                            String smsId = UUIDUtil.getUUID();
                            if (CollectionUtils.isEmpty(smsTemplateList)) {
                                SmsTemplate entity = new SmsTemplate();
                                BeanUtils.copyProperties(smsTemplate, entity);
                                entity.setClient(marketingClient.getClient());
                                entity.setSmsSource(smsTemplate.getSmsId());
                                entity.setSmsId(smsId);
                                entity.setSmsTheme(smsTemplate.getSmsTheme() + "_" + marketingClient.getGsmThename());
                                smsTemplates.add(entity);
                            } else {
                                smsId = smsTemplateList.get(0).getSmsId();
                            }
                            // 修改模板ID
                            sdMarketingSearch.setGsmsSmsTemp(smsId);
                            marketingClient.setGsmSmscondi(JsonUtils.beanToJson(sdMarketingSearcheList));
                        }
                    }
                }
            }
        }
    }

    /**
     * 营销消息推送
     */
    private void tpnsMsg(List<String> clientList, String title) {
        try {
            for (String client : clientList) {
                List<String> userIdList = authconfiDataMapper.selectUserByGourp(client);
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(client);
                messageParams.setUserIdList(userIdList);
                messageParams.setContentParmas(new ArrayList<String>() {{
                    add(title);
                }});
                messageParams.setId(CommonEnum.gmtId.MSG00006.getCode());
                messageService.sendMessageList(messageParams);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("营销推送异常");
        }
    }


    /**
     * 促销方式列表
     */
    @Override
    public List<GetPromotionListDTO> getPromotionList() {
        return marketingBasicMapper.getPromotionList();
    }

    /**
     * 活动ID最大值
     */
    private String getMaxNumMarketId() {
        String maxNumMarketid = marketingBasicMapper.getMaxNumMarketid();
        if (StringUtils.isEmpty(maxNumMarketid)) {
            return "1000000000";
        }
        return new BigDecimal(maxNumMarketid).add(BigDecimal.ONE).toString();
    }


    /*********************************************营销任务 03版 *********************************************/

    /**
     * 推荐营销任务列表
     */
    @Override
    public IPage<GetGsmTaskListDTO2> getGsmTaskList(GsmTaskListVO2 vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<GetGsmTaskListDTO2> pageInfo = new Page<>(vo.getPageNum(), vo.getPageSize());
        // 0:加盟商负责人/委托人 1:员工
        Integer legalPerson = getLegalPerson(user.getClient(), user.getUserId());
        vo.setLegalPerson(legalPerson);
        return marketingBasicMapper.getGsmTaskList(pageInfo, user.getClient(), vo);
    }

    /**
     * 营销任务详情
     */
    @Override
    public GetGsmTaskDetailDTO3 getGsmTaskDetail(Integer id, Integer type, String stoCode) {
        if (ObjectUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        if (ObjectUtils.isEmpty(type)) {
            throw new CustomResultException("type不能为空,当前营销Tal页type传2,历史Tal页type传1");
        }
        if (type == 2 && StringUtils.isEmpty(stoCode)) {
            throw new CustomResultException("门店code不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();

        //1.营销活动主数据
        SdMarketingClient marketingClient = marketingClientService.getById(id);
        if (ObjectUtils.isEmpty(marketingClient)) {
            throw new CustomResultException("id错误,该数据不存在或被已被删除");
        }
        GetGsmTaskDetailDTO3 dto = new GetGsmTaskDetailDTO3();
        BeanUtils.copyProperties(marketingClient, dto);
        // 门店列表
        List<SdMarketingSto> marketingStoList = marketingStoService.list(new QueryWrapper<SdMarketingSto>()
                .eq("CLIENT", client).eq("MARKETING_ID", id));
        if (!CollectionUtils.isEmpty(marketingStoList)) {
            List<String> proCodeList = marketingStoList.stream().map(SdMarketingSto::getStoCode).collect(Collectors.toList());
            List<StoreData> stoList = storeDataService.list(new QueryWrapper<StoreData>().select("STO_CODE", "STO_NAME").eq("CLIENT", client).in("STO_CODE", proCodeList));
            dto.setStoList(stoList);
        }

        //2.短信/电话详情 （若当前营销/推荐营销 则设置短信电话查询id，以及门店编码）
        if (type == 2 || type == 1) {
            // 门店编码
            dto.setStoCode(stoCode);
            List<SdMarketingSearch> searchList = sdMarketingSearchService.list(new QueryWrapper<SdMarketingSearch>()
                    .eq("MARKETING_ID", id).eq("GSMS_STORE", stoCode).eq("GSMS_DELETE_FLAG", "0"));
            // 短信查询条件
            List<SdMarketingSearch> smsSearchList = searchList.stream().filter(search -> search.getGsmsType().equals(1)).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(smsSearchList)) {
                String gsmSmscondi = JsonUtils.beanToJson(smsSearchList);
                dto.setGsmSmscondi(gsmSmscondi);
            }
            // 电话查询条件
//            List<SdMarketingSearch> telSearchList = searchList.stream().filter(search -> search.getGsmsType().equals(2)).collect(Collectors.toList());
//            if (!CollectionUtils.isEmpty(telSearchList)) {
//                dto.setGsmTelcondiList(telSearchList);
//            }
        }

        /**
         * 3.宣传物料
         */
        List<SdMarketingBasicAttac> attacOriginalList = sdMarketingBasicAttacService.list(new QueryWrapper<SdMarketingBasicAttac>().eq("BASIC_ID", marketingClient.getBasicId()));
        List<SdMarketingBasicAttacDTO> attacList = attacOriginalList.stream().filter(Objects::nonNull).map(attac -> {
            SdMarketingBasicAttacDTO attacDTO = new SdMarketingBasicAttacDTO();
            BeanUtils.copyProperties(attac, attacDTO);
            attacDTO.setGsmbaJpgUrl(cosUtils.urlAuth(attac.getGsmbaJpg()));
            attacDTO.setGsmbaPsUrl(cosUtils.urlAuth(attac.getGsmbaPs()));
            attacDTO.setGsmbaScenesUrl(cosUtils.urlAuth(attac.getGsmbaScenes()));
            return attacDTO;
        }).collect(toList());
        dto.setMarketingBasicAttacList(attacList);

        /**
         * 4.当前用户是否为加盟商负责人/委托人
         */
        dto.setLegalPerson(getLegalPerson(client, userId));
        return dto;
    }

    /**
     * 判断当前用户是否为  加盟商负责人/委托人
     *
     * @return Integer 0:加盟商负责人/委托人 1:员工
     */
    private Integer getLegalPerson(String client, String userId) {
        Franchisee franchisee = franchiseeService.getById(client);
        if (!ObjectUtils.isEmpty(franchisee)) {
            String francLegalPerson = franchisee.getFrancLegalPerson();
            String francAss = franchisee.getFrancAss();
            if (!StringUtils.isEmpty(userId) && (userId.equals(francLegalPerson) || userId.equals(francAss))) {
                return 0;
            }
            return 1;
        }
        return 1;
    }

    /**
     * 营销任务保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveGsmTask(SaveGsmTaskVO3 vo) {
        List<String> stoList = vo.getStoList();
        // 转交修改flag 1,[立即执行,提交审批,不予执行]:0
        if (vo.getFlagTransfer() == 0 && CollectionUtils.isEmpty(stoList)) {
            throw new CustomResultException("门店列表不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        /**
         * 1.保存主数据
         */
        // 营销活动加盟商分配表主键
        Integer id = vo.getId();
        SdMarketingClient marketingClientExit = marketingClientService.getById(id);
        if (ObjectUtils.isEmpty(marketingClientExit)) {
            throw new CustomResultException("id错误,该数据不存在");
        }
//        if (!marketingClientExit.getGsmImpl().equals("-1")) {
//            throw new CustomResultException("当前营销已经执行/提交审批,不可编辑");
//        }
        SdMarketingClient marketingClient = new SdMarketingClient();
        BeanUtils.copyProperties(vo, marketingClient);
        marketingClientService.updateById(marketingClient);

        /**
         * 2.营销活动门店分配表
         */
        if (!CollectionUtils.isEmpty(stoList)) {
            // 删除旧数据
            marketingStoService.remove(new QueryWrapper<SdMarketingSto>().eq("MARKETING_ID", id));
            // 新数据
            List<SdMarketingSto> marketingStoList = stoList.stream().map(stoCode -> {
                SdMarketingSto marketingSto = new SdMarketingSto();
                //
                marketingSto.setClient(client);
                //
                marketingSto.setMarketingId(id);
                //
                marketingSto.setStoCode(stoCode);
                return marketingSto;
            }).collect(toList());
            if (!CollectionUtils.isEmpty(marketingStoList)) {
                marketingStoService.saveBatch(marketingStoList);
            }
        }

        /**
         * 3.短信/电话查询条件
         */
        // 删除旧数据
        sdMarketingSearchService.remove(new QueryWrapper<SdMarketingSearch>().eq("MARKETING_ID", id));
        // 添加新数据
        List<SdMarketingSearch> searchList = new ArrayList<>();
        stoList.forEach(item -> {
            // 短信
            List<SdMarketingSearch> sdMarketingSearcheList = JsonUtils.jsonToBeanList(marketingClient.getGsmSmscondi(), SdMarketingSearch.class);
            sdMarketingSearcheList.forEach(smsSearch -> {
                smsSearch.setClient(client);
                smsSearch.setGsmsId(UUIDUtil.getUUID());
                smsSearch.setGsmsName(marketingClient.getGsmThename());
                smsSearch.setMarketingId(id);
                smsSearch.setGsmsStore(item);
                smsSearch.setGsmsNormal("0");
                smsSearch.setGsmsDeleteFlag("0");
                smsSearch.setGsmsType(1);
                searchList.add(smsSearch);
            });

            // 电话
            SdMarketingSearch telSearch = new SdMarketingSearch();
            telSearch.setClient(client);
            telSearch.setGsmsId(UUIDUtil.getUUID());
            telSearch.setGsmsName(marketingClient.getGsmThename());
            telSearch.setGsmsDetail(marketingClient.getGsmTelcondi());
            telSearch.setMarketingId(id);
            telSearch.setGsmsStore(item);
            telSearch.setGsmsNormal("0");
            telSearch.setGsmsDeleteFlag("0");
            telSearch.setGsmsType(2);
            searchList.add(telSearch);
        });
        if (!CollectionUtils.isEmpty(searchList)) {
            sdMarketingSearchService.saveBatch(searchList);
        }

        /**
         * 4.物料列表
         */
//        List<SdMarketingBasicAttac> attacList = vo.getMarketingBasicAttacList();
//        sdMarketingBasicAttacService.remove(new QueryWrapper<SdMarketingBasicAttac>().eq("BASIC_ID", vo.getId()));
//        attacList.forEach(item -> item.setBasicId(vo.getId()));
//        sdMarketingBasicAttacService.saveBatch(attacList);

    }

    /**
     * 转交修改
     */
    @Override
    public void transferToEdit(Integer id, String gsmOpinion) {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        SdMarketingClient marketingClient = marketingClientService.getById(id);
        if (ObjectUtils.isEmpty(marketingClient)) {
            throw new CustomResultException("该记录不存在");
        }
        // 0:加盟商负责人/委托人 1:员工
        Integer legalPerson = getLegalPerson(client, userId);
        if (1 == legalPerson) {
            throw new CustomResultException("当前用户非加盟商[负责人/委托人],不可执行[转交修改]操作");
        }
        if (!OperateEnum.MarketingImpl.NOTAPPROVED.getCode().equals(marketingClient.getGsmImpl())) {
            throw new CustomResultException("该营销状态非[待处理]不可[转交修改]");
        }
        SdMarketingClient marketingClientDO = new SdMarketingClient();
        marketingClientDO.setGsmImpl(OperateEnum.MarketingImpl.CONSENT_EXECUTION.getCode());
        marketingClientDO.setGsmOpinion(gsmOpinion);
        marketingClientDO.setId(id);
        marketingClientService.updateById(marketingClientDO);
    }

    /**
     * 不予执行
     */
    @Override
    public void noEnforcement(Integer id) {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        SdMarketingClient marketingClient = marketingClientService.getById(id);
        if (ObjectUtils.isEmpty(marketingClient)) {
            throw new CustomResultException("该记录不存在");
        }
        // 0:加盟商负责人/委托人 1:员工
        Integer legalPerson = getLegalPerson(client, userId);
        if (1 == legalPerson) {
            throw new CustomResultException("当前用户非加盟商[负责人/委托人],不可执行该操作");
        }
        if (!OperateEnum.MarketingImpl.NOTAPPROVED.getCode().equals(marketingClient.getGsmImpl())) {
            throw new CustomResultException("该营销状态非[待处理]不可执行该操作");
        }
        SdMarketingClient marketingClientDO = new SdMarketingClient();
        marketingClientDO.setGsmImpl(OperateEnum.MarketingImpl.NOENFORCEMENT.getCode());
        marketingClientDO.setId(id);
        marketingClientService.updateById(marketingClientDO);
    }

    /**
     * 选品选择列表
     */
    @Override
    public IPage<GetProductListToCheckDTO> getProductListToCheck(GetProductListToCheckVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page<GetProductListToCheckDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<GetProductListToCheckDTO> result = marketingBasicMapper.getProductListToCheck(page, vo);
        return result;
    }

    /**
     * 赠品选择列表
     */
    @Override
    public IPage<GetProductListToCheckDTO> getGiveawayListToCheck(GetGiveawayListToCheckVO vo) {
        if (vo.getPromId() == null || vo.getMarketingId() == null) {
            throw new CustomResultException("营销促销ID不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page<GetProductListToCheckDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<GetProductListToCheckDTO> result = marketingBasicMapper.getGiveawayList(page, vo);
        return result;
    }

    /**
     * 目标试算
     */
    @Override
    public MarketingTargetTrialDTO marketingAimsTrial(MarketingAimsTrialVO vo) {

        MarketingTargetTrialDTO dto = new MarketingTargetTrialDTO();
        TokenUser user = commonService.getLoginInfo();

        // 同比日期营业额
        BigDecimal yearTurnover = marketingBasicMapper.getTurnover(user.getClient(), vo.getStoList(), vo.getGsmYearStart(), vo.getGsmYearEnd());
        // 同比日期毛利额
        BigDecimal yearGrossprofit = marketingBasicMapper.getGrossprofit(user.getClient(), vo.getStoList(), vo.getGsmYearStart(), vo.getGsmYearEnd());
        // 同比日期促销品销售额
        BigDecimal yearPromotionalpro = marketingBasicMapper.getPromotionalpro(user.getClient(), vo.getStoList(), vo.getGsmYearStart(), vo.getGsmYearEnd());

        // 同比营业额目标=同比日期营业额×同比营业额增长倍率，保留小数点后 2位
        BigDecimal gsmyearTurnoverAims = yearTurnover == null ? null : yearTurnover.multiply(vo.getGsmYearTurnover()).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        dto.setGsmYearTurnoverAims(gsmyearTurnoverAims);
        // 同比毛利额目标=同比日期毛利额×同比毛利额增长倍率，保留小数点后 2位
        BigDecimal gsmYearGrossprofitAims = yearGrossprofit == null ? null : yearGrossprofit.multiply(vo.getGsmYearGrossprofit()).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        dto.setGsmYearGrossprofitAims(gsmYearGrossprofitAims);
        // 同比促销品目标=同比日期促销品销售额×同比促销品增长倍率，保留小数点后2位。
        BigDecimal gsmYearPromotionalproAims = yearPromotionalpro == null ? null : yearPromotionalpro.multiply(vo.getGsmYearPromotionalpro()).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        dto.setGsmYearPromotionalproAims(gsmYearPromotionalproAims);
        // 同比毛利率=同比毛利额目标÷同比营业额目标，保留小数点后 4位
        BigDecimal gsmYearGrossmarginAims = (gsmyearTurnoverAims == null || gsmYearGrossprofitAims == null) ? null :
                gsmyearTurnoverAims.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmYearGrossprofitAims.divide(gsmyearTurnoverAims, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
        dto.setGsmYearGrossmarginAims(gsmYearGrossmarginAims);


        // 同比日期营业额
        BigDecimal monthTurnover = marketingBasicMapper.getTurnover(user.getClient(), vo.getStoList(), vo.getGsmMonthStart(), vo.getGsmMonthEnd());
        // 同比日期毛利额
        BigDecimal monthGrossprofit = marketingBasicMapper.getGrossprofit(user.getClient(), vo.getStoList(), vo.getGsmMonthStart(), vo.getGsmMonthEnd());
        // 同比日期促销品销售额
        BigDecimal monthPromotionalpro = marketingBasicMapper.getPromotionalpro(user.getClient(), vo.getStoList(), vo.getGsmMonthStart(), vo.getGsmMonthEnd());

        // 环比营业额目标=环比日期营业额×环比营业额增长倍率，保留小数点后 2位
        BigDecimal gsmMonthTurnoverAims = monthTurnover == null ? null : monthTurnover.multiply(vo.getGsmMonthTurnover()).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        dto.setGsmMonthTurnoverAims(gsmMonthTurnoverAims);
        // 环比毛利额目标=环比日期毛利额×环比毛利额增长倍率，保留小数点后 2位
        BigDecimal gsmMonthGrossprofitAims = monthGrossprofit == null ? null : monthGrossprofit.multiply(vo.getGsmMonthGrossprofit()).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        dto.setGsmMonthGrossprofitAims(gsmMonthGrossprofitAims);
        // 环比促销品目标=环比日期促销品销售额×环比促销品增长倍率，保留小数点后2位。
        BigDecimal gsmMonthPromotionalproAims = monthPromotionalpro == null ? null : monthPromotionalpro.multiply(vo.getGsmMonthPromotionalpro()).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        dto.setGsmMonthPromotionalproAims(gsmMonthPromotionalproAims);
        // 环比毛利率=环比毛利额目标÷环比营业额目标，保留小数点后 4位
        BigDecimal gsmMonthGrossmarginAims = (gsmMonthTurnoverAims == null || gsmMonthGrossprofitAims == null) ? null :
                gsmMonthTurnoverAims.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmMonthGrossprofitAims.divide(gsmMonthTurnoverAims, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
        dto.setGsmMonthGrossmarginAims(gsmMonthGrossmarginAims);

        return dto;
    }

    /**
     * 当前营销 、 历史营销
     *
     * @param vo
     * @return
     */
    @Override
    public IPage<GetGsmTaskListDTO2> getTaskExecutedList(GsmTaskListVO2 vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<GetGsmTaskListDTO2> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<GetGsmTaskListDTO2> iPage = sdMarketingClientMapper.selectMarketing(page, user.getClient(), vo);
        return iPage;
    }

    /**
     * 营销任务促销详情
     */
    @Override
    public IPage<GetPromDetailsDTO> getPromDetails(GetPromDetailsVO vo) {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        /**
         * 商品列表 分页
         */
        Page<GetPromDetailsDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<GetPromDetailsDTO> promPrdList = marketingBasicMapper.getPromDetails(page, vo, client);
        List<GetPromDetailsDTO> records = promPrdList.getRecords();
        if (CollectionUtils.isEmpty(records)) {
            return promPrdList;
        }
        // 营销活动促销关联表ID列表
        List<Integer> promIdList = records.stream().map(GetPromDetailsDTO::getPromId).distinct().collect(toList());

        /**
         * 促销
         */
        // 促销方式
        List<SdMarketingProm> marketingPromList = marketingPromService.list(new QueryWrapper<SdMarketingProm>()
                .eq("CLIENT", client).in("ID", promIdList));
        // 促销参数列表
        Map<String, List<SdPromVariableSet>> promVariableMap = promVariableSetService.list(new QueryWrapper<SdPromVariableSet>()
                .ne("GSPVS_TYPE", "PROM_HEADER").eq("GSPVS_FALG", "1"))
                .stream()
                .collect(groupingBy(SdPromVariableSet::getGspvsMode));

        // 促销参数列表
        records.stream().filter(Objects::nonNull).forEach(prdProm -> {
            // 1.该商品对应的促销
            SdMarketingProm sdMarketingProm = marketingPromList.stream().filter(Objects::nonNull).filter(prom -> prom.getId().equals(prdProm.getPromId())).findFirst().orElse(null);
            if (ObjectUtils.isEmpty(sdMarketingProm)) {
                return;
            }
            // 2.营销设置中 配置的促销 转 Map
            Map map = JSON.parseObject(JSON.toJSONString(sdMarketingProm), Map.class);
            // 3.筛选指定促销类型包含的参数
            List<GetPromDetailsDTO.Param> paramList = promVariableMap.get(sdMarketingProm.getGspvsMode()).stream().map(variable -> {
                GetPromDetailsDTO.Param param = new GetPromDetailsDTO.Param();
                BeanUtils.copyProperties(variable, param);
                return param;
            }).collect(Collectors.toList());
            // 4.参数赋值
            paramList.forEach(param -> {
                // 列名转成驼峰
                String gspvsColumns = CaseUtils.toCamelCase(param.getGspvsColumns(), false, '_');
                param.setGspvsColumns(gspvsColumns);
                // 赋值(参数具体的值)
                String gspvsColumnsValue = null;
                if (map.get(gspvsColumns) instanceof String || map.get(gspvsColumns) instanceof Integer) {
                    gspvsColumnsValue = String.valueOf(map.get(gspvsColumns));
                } else if (map.get(gspvsColumns) instanceof BigDecimal) {
                    gspvsColumnsValue = ((BigDecimal) map.get(gspvsColumns)).stripTrailingZeros().toPlainString();
                }
                param.setGspvsColumnsValue(gspvsColumnsValue);
            });
            prdProm.setParamList(paramList);
        });

        /**
         * 赠品
         */
        // 赠品列表
        Map<Integer, List<GetPromDetailsDTO.SdMarketingGiveawayDTO>> marketingGiveawayMap = marketingGiveawayService.list(new QueryWrapper<SdMarketingGiveaway>()
                .eq("CLIENT", client).eq("MARKETING_ID", vo.getId()).in("PROM_ID", promIdList))
                .stream()
                .map(giveaway -> {
                    // proCode 字段置空
                    GetPromDetailsDTO.SdMarketingGiveawayDTO giveawayDTO = new GetPromDetailsDTO.SdMarketingGiveawayDTO();
                    BeanUtils.copyProperties(giveaway, giveawayDTO);
                    giveawayDTO.setProCode(null);
                    giveawayDTO.setProSelfCode(giveaway.getProCode());
                    return giveawayDTO;
                })
                .collect(groupingBy(GetPromDetailsDTO.SdMarketingGiveawayDTO::getPromId));
        records.forEach(prdProm -> {
            List<GetPromDetailsDTO.SdMarketingGiveawayDTO> giveawayDTOList = marketingGiveawayMap.get(prdProm.getPromId());
            prdProm.setGiveawayList(giveawayDTOList);
        });

        return promPrdList;
    }

    /**
     * 活动商品
     *
     * @param pageNum
     * @param pageSize
     * @param id
     * @param proKey
     * @param promFitsType
     * @param promFitsStart
     * @param promFitsEnd
     * @return
     */
    @Override
    public IPage<GetProductListToCheckDTO> getProductList(Integer pageNum, Integer pageSize, Integer id, String proKey, BigDecimal mllStart, BigDecimal mllEnd, Integer promFitsType, BigDecimal promFitsStart, BigDecimal promFitsEnd) {
        QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper();
        sdMarketingClientQueryWrapper.eq("ID", id);
        SdMarketingClient sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
        if (sdMarketingClient == null) {
            throw new CustomResultException("营销数据不存在");
        }
        Page<GetProductListToCheckDTO> page = new Page<>(pageNum, pageSize);
        IPage<GetProductListToCheckDTO> iPage = sdMarketingClientMapper.getProductList(page, sdMarketingClient.getId(), sdMarketingClient.getClient(), proKey, mllStart, mllEnd, promFitsType, promFitsStart, promFitsEnd);
        return iPage;
    }

    /**
     * 活动商品删除
     *
     * @param list
     */
    @Override
    public void deleteProduct(List<Integer> list) {
        sdMarketingPrdMapper.deleteBatchIds(list);
    }

    /**
     * 活动商品保存
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveProduct(Integer marketingId, List<GetProductListToCheckDTO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper();
        sdMarketingClientQueryWrapper.eq("ID", marketingId);
        SdMarketingClient sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
        if (sdMarketingClient == null) {
            return;
        }

        List<Integer> promIdList = list.stream().filter(Objects::nonNull).map(GetProductListToCheckDTO::getPromId).collect(toList());
        List<SdMarketingProm> promList = marketingPromService.list(new QueryWrapper<SdMarketingProm>().in("ID", promIdList));

        for (GetProductListToCheckDTO item : list) {
            Integer id = item.getId();
            // 新增
            if (id == null || id.intValue() < 1) {
                QueryWrapper<SdMarketingPrd> sdMarketingPrdQueryWrapper = new QueryWrapper<>();
                sdMarketingPrdQueryWrapper.eq("MARKETING_ID", marketingId);
                sdMarketingPrdQueryWrapper.eq("PROM_ID", item.getPromId());
                sdMarketingPrdQueryWrapper.eq("PRO_CODE", item.getProSelfCode());
                List<SdMarketingPrd> prdList = sdMarketingPrdMapper.selectList(sdMarketingPrdQueryWrapper);
                if (!CollectionUtils.isEmpty(prdList)) {
                    throw new CustomResultException("商品" + item.getProSelfCode() + "已存在");
                }
                SdMarketingPrd sdMarketingPrd = new SdMarketingPrd();
                // 加盟商
                sdMarketingPrd.setClient(sdMarketingClient.getClient());
                // 营销活动加盟商分配表主键
                sdMarketingPrd.setMarketingId(marketingId);
                // 促销关联表
                sdMarketingPrd.setPromId(item.getPromId());
                // 商品编码
                sdMarketingPrd.setProCode(item.getProSelfCode());

                // 获取指定促销
                SdMarketingProm marketingProm = promList.stream().filter(Objects::nonNull).filter(prom -> prom.getId().equals(item.getPromId())).findFirst().orElse(null);
                if (!ObjectUtils.isEmpty(marketingProm)) {
                    setMarketingPrdPriceAndFits(sdMarketingPrd, marketingProm, item.getGsppPriceNormal(), item.getProCgj());
                }

//                // 促销单价
//                sdMarketingPrd.setGsmpPrice1(item.getGsmpPrice1());
//                // 促销毛利率
//                sdMarketingPrd.setGsmpFits1(item.getGsmpFits1());
//                // 促销单价
//                sdMarketingPrd.setGsmpPrice2(item.getGsmpPrice2());
//                // 促销毛利率
//                sdMarketingPrd.setGsmpFits2(item.getGsmpFits2());
//                // 促销单价
//                sdMarketingPrd.setGsmpPrice3(item.getGsmpPrice3());
//                // 促销毛利率
//                sdMarketingPrd.setGsmpFits3(item.getGsmpFits3());
                // TODO
                sdMarketingPrdMapper.insert(sdMarketingPrd);
            } else { // 修改
//                QueryWrapper<SdMarketingPrd> sdMarketingPrdQueryWrapper = new QueryWrapper<>();
//                sdMarketingPrdQueryWrapper.eq("ID", id);
//                SdMarketingPrd sdMarketingPrd = sdMarketingPrdMapper.selectOne(sdMarketingPrdQueryWrapper);
//                if (sdMarketingPrd == null) {
//                    continue;
//                }
//                // 促销单价
//                sdMarketingPrd.setGsmpPrice1(item.getGsmpPrice1());
//                // 促销毛利率
//                sdMarketingPrd.setGsmpFits1(item.getGsmpFits1());
//                // 促销单价
//                sdMarketingPrd.setGsmpPrice2(item.getGsmpPrice2());
//                // 促销毛利率
//                sdMarketingPrd.setGsmpFits2(item.getGsmpFits2());
//                // 促销单价
//                sdMarketingPrd.setGsmpPrice3(item.getGsmpPrice3());
//                // 促销毛利率
//                sdMarketingPrd.setGsmpFits3(item.getGsmpFits3());
//                sdMarketingPrdMapper.update(sdMarketingPrd, sdMarketingPrdQueryWrapper);
            }
        }
    }

    private boolean setMarketingPrdPriceAndFits(SdMarketingPrd prdDO, SdMarketingProm marketingProm, BigDecimal priceNormal, BigDecimal proCgj) {
        if (ObjectUtils.isEmpty(prdDO) || ObjectUtils.isEmpty(marketingProm) || ObjectUtils.isEmpty(priceNormal) || ObjectUtils.isEmpty(proCgj)) {
            return false;
        }
        boolean flg = false;
        // 单品类
        if (marketingProm.getGspvsType().equals("PROM_SINGLE")) {
            // 单品打折
            if (marketingProm.getGspvsMode().equals("SINGLE-1")) {
                // 1 [促销单价1=零售价×生成促销折扣1/100], [促销毛利率1=（促销单价1-成本价）/促销单价1]
                if (!ObjectUtils.isEmpty(marketingProm.getGspusRebate1()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty1())) {
                    BigDecimal gsmpPrice1 = priceNormal.multiply(new BigDecimal(marketingProm.getGspusRebate1())).divide(BigDecimal.valueOf(100), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    BigDecimal gsmpFits1 = gsmpPrice1.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice1.subtract(proCgj).divide(gsmpPrice1, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    prdDO.setGsmpPrice1(gsmpPrice1);
                    prdDO.setGsmpFits1(gsmpFits1);
                    flg = true;
                }

                // 2 [促销单价2=零售价×生成促销折扣2/100], [促销毛利率2=（促销单价2-成本价）/促销单价2]
                if (!ObjectUtils.isEmpty(marketingProm.getGspusRebate2()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty2())) {
                    BigDecimal gsmpPrice2 = priceNormal.multiply(new BigDecimal(marketingProm.getGspusRebate2())).divide(BigDecimal.valueOf(100), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    BigDecimal gsmpFits2 = gsmpPrice2.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice2.subtract(proCgj).divide(gsmpPrice2, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    prdDO.setGsmpPrice2(gsmpPrice2);
                    prdDO.setGsmpFits2(gsmpFits2);
                    flg = true;
                }

                // 3 [促销单价3=零售价×生成促销折扣3/100], [促销毛利率3=（促销单价3-成本价）/促销单价3]
                if (!ObjectUtils.isEmpty(marketingProm.getGspusRebate3()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty3())) {
                    BigDecimal gsmpPrice3 = priceNormal.multiply(new BigDecimal(marketingProm.getGspusRebate3())).divide(BigDecimal.valueOf(100), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    BigDecimal gsmpFits3 = gsmpPrice3.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice3.subtract(proCgj).divide(gsmpPrice3, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    prdDO.setGsmpPrice3(gsmpPrice3);
                    prdDO.setGsmpFits3(gsmpFits3);
                    flg = true;
                }

            }
            // 单品特价
            if (marketingProm.getGspvsMode().equals("SINGLE-2")) {
                // 1 [促销单价1=生成促销价1/达到数量1],[促销毛利率1=（促销单价1-成本价）/促销单价1]
                if (!ObjectUtils.isEmpty(marketingProm.getGspusPrc1()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty1())) {
                    BigDecimal gsmpPrice1 = marketingProm.getGspusPrc1().divide(new BigDecimal(marketingProm.getGspusQty1()), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    BigDecimal gsmpFits1 = gsmpPrice1.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice1.subtract(proCgj).divide(gsmpPrice1, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    prdDO.setGsmpPrice1(gsmpPrice1);
                    prdDO.setGsmpFits1(gsmpFits1);
                    flg = true;
                }

                // 2 [促销单价2=生成促销价2/达到数量2],[促销毛利率2=（促销单价2-成本价）/促销单价2]
                if (!ObjectUtils.isEmpty(marketingProm.getGspusPrc2()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty2())) {
                    BigDecimal gsmpPrice2 = marketingProm.getGspusPrc2().divide(new BigDecimal(marketingProm.getGspusQty2()), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    BigDecimal gsmpFits2 = gsmpPrice2.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice2.subtract(proCgj).divide(gsmpPrice2, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    prdDO.setGsmpPrice2(gsmpPrice2);
                    prdDO.setGsmpFits2(gsmpFits2);
                    flg = true;
                }

                // 3 [促销单价3=生成促销价3/达到数量3],[促销毛利率3=（促销单价3-成本价）/促销单价3]
                if (!ObjectUtils.isEmpty(marketingProm.getGspusPrc3()) && !ObjectUtils.isEmpty(marketingProm.getGspusQty3())) {
                    BigDecimal gsmpPrice3 = marketingProm.getGspusPrc3().divide(new BigDecimal(marketingProm.getGspusQty3()), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    BigDecimal gsmpFits3 = gsmpPrice3.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : gsmpPrice3.subtract(proCgj).divide(gsmpPrice3, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
                    prdDO.setGsmpPrice3(gsmpPrice3);
                    prdDO.setGsmpFits3(gsmpFits3);
                    flg = true;
                }
            }
        }
        return flg;
    }

    /**
     * 促销列表
     *
     * @param id
     * @return
     */
    @Override
    public List<SdMarketingProm> getPromList(Integer id) {
        QueryWrapper<SdMarketingProm> sdMarketingPromQueryWrapper = new QueryWrapper();
        sdMarketingPromQueryWrapper.eq("MARKETING_ID", id);
        List<SdMarketingProm> list = sdMarketingPromMapper.selectList(sdMarketingPromQueryWrapper);
        return list;
    }

    /**
     * @param proSelfCode
     * @param mllStart
     * @param mllEnd
     * @return
     */
    @Override
    public IPage<GetProductListToCheckDTO> getProductBySelfCode(Integer id, Integer promId, String proKey, String proSelfCode, BigDecimal mllStart, BigDecimal mllEnd, Integer pageNum, Integer pageSize) {
        QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper();
        sdMarketingClientQueryWrapper.eq("ID", id);
        SdMarketingClient sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
        if (sdMarketingClient == null) {
            throw new CustomResultException("营销数据不存在");
        }
        Page<GetProductListToCheckDTO> page = null;
        if (pageNum == null || pageSize == null) {
            page = new Page<>(-1, -1);
        } else {
            page = new Page<>(pageNum, pageSize);
        }
        IPage<GetProductListToCheckDTO> iPage = sdMarketingClientMapper.getProductBySelfCode(page, id, promId, sdMarketingClient.getClient(), proKey, proSelfCode, mllStart, mllEnd);
        return iPage;
    }

    /**
     * 赠品保存
     *
     * @param list
     */
    @Override
    public void saveGiveaway(List<GetProductListToCheckDTO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        for (GetProductListToCheckDTO getProductListToCheckDTO : list) {
            QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper();
            sdMarketingClientQueryWrapper.eq("ID", getProductListToCheckDTO.getMarketingId());
            SdMarketingClient sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
            if (sdMarketingClient == null) {
                throw new CustomResultException("营销数据不存在");
            }
            QueryWrapper<SdMarketingGiveaway> sdMarketingGiveawayQueryWrapper = new QueryWrapper();
            // MARKETING_ID
            sdMarketingGiveawayQueryWrapper.eq("MARKETING_ID", getProductListToCheckDTO.getMarketingId());
            // PROM_ID
            sdMarketingGiveawayQueryWrapper.eq("PROM_ID", getProductListToCheckDTO.getPromId());
            // PRO_CODE
            sdMarketingGiveawayQueryWrapper.eq("PRO_CODE", getProductListToCheckDTO.getProSelfCode());
            List<SdMarketingGiveaway> sdMarketingGiveawayList = sdMarketingGiveawayMapper.selectList(sdMarketingGiveawayQueryWrapper);
            // 删除
            if (getProductListToCheckDTO.getSeleted().intValue() == 0) {
                if (!CollectionUtils.isEmpty(sdMarketingGiveawayList)) {
                    sdMarketingGiveawayMapper.delete(sdMarketingGiveawayQueryWrapper);
                }
            }
            // 保存
            if (getProductListToCheckDTO.getSeleted().intValue() == 1) {
                if (CollectionUtils.isEmpty(sdMarketingGiveawayList)) {
                    SdMarketingGiveaway sdMarketingGiveaway = new SdMarketingGiveaway();
                    // 加盟商
                    sdMarketingGiveaway.setClient(sdMarketingClient.getClient());
                    // 营销活动加盟商分配表主键
                    sdMarketingGiveaway.setMarketingId(getProductListToCheckDTO.getMarketingId());
                    // 促销关联表
                    sdMarketingGiveaway.setPromId(getProductListToCheckDTO.getPromId());
                    // 商品编码
                    sdMarketingGiveaway.setProCode(getProductListToCheckDTO.getProSelfCode());
                    sdMarketingGiveawayMapper.insert(sdMarketingGiveaway);
                }
            }
        }

    }

    /**
     * 自定义参数获取
     *
     * @param promId
     * @return
     */
    @Override
    public List<CustomParam> getCustomParams(Integer promId) {
        QueryWrapper<SdMarketingProm> sdMarketingPromQueryWrapper = new QueryWrapper();
        sdMarketingPromQueryWrapper.eq("ID", promId);
        SdMarketingProm sdMarketingProm = sdMarketingPromMapper.selectOne(sdMarketingPromQueryWrapper);
        if (sdMarketingProm == null) {
            throw new CustomResultException("当前促销不存在");
        }
        List<CustomParam> list = new ArrayList<>();
        QueryWrapper<SdPromVariableSet> sdPromVariableSetQueryWrapper = new QueryWrapper<>();
        sdPromVariableSetQueryWrapper.eq("GSPVS_MODE", sdMarketingProm.getGspvsMode());
        sdPromVariableSetQueryWrapper.eq("GSPVS_TYPE", sdMarketingProm.getGspvsType());
        sdPromVariableSetQueryWrapper.eq("GSPVS_FALG", "1");
        sdPromVariableSetQueryWrapper.ne("GSPVS_TABLE", "GAIA_SD_PROM_HEAD");
        List<SdPromVariableSet> sdPromVariableSetList = sdPromVaribaleSetMapper.selectList(sdPromVariableSetQueryWrapper);
        if (sdPromVariableSetList == null) {
            return null;
        } else {
            for (SdPromVariableSet sdPromVariableSet : sdPromVariableSetList) {
                CustomParam customParam = new CustomParam();
                customParam.setPromId(promId);
                // 标题
                customParam.setLabel(sdPromVariableSet.getGspvsColumnsName());
                // 字段类型
                customParam.setGspvsColumnsType(sdPromVariableSet.getGspvsColumnsType());
                // 字段长度
                customParam.setGspvsColumnsLength(sdPromVariableSet.getGspvsColumnsLength());
                // 列名
                customParam.setColumn(sdPromVariableSet.getGspvsColumns());
                // 值
                for (Field field : sdMarketingProm.getClass().getDeclaredFields()) {
                    if (field.isAnnotationPresent(TableField.class)) {
                        TableField annotation = field.getAnnotation(TableField.class);
                        String gspvsColumns = annotation.value();
                        if (StringUtils.isNotBlank(gspvsColumns) && gspvsColumns.equals(sdPromVariableSet.getGspvsColumns())) {
                            // 值
                            Object object = getFieldValueByName(field.getName(), sdMarketingProm);
                            if (object != null) {
                                String value = object.toString();
                                if ("DECIMAL".equals(sdPromVariableSet.getGspvsColumnsType().toUpperCase())) {
                                    if (value.indexOf(".") > 0) {
                                        // 去掉多余的0
                                        value = value.replaceAll("0+?$", "");
                                        // 如最后一位是.则去掉
                                        value = value.replaceAll("[.]$", "");
                                        customParam.setValue(value);
                                    }
                                } else {
                                    customParam.setValue(value);
                                }
                            } else {
                                customParam.setValue("");
                            }
                            break;
                        }
                    }
                }
                list.add(customParam);
            }
        }
        // 促销名称
        CustomParam customParam = new CustomParam();
        // 促销ID
        customParam.setPromId(promId);
        // 标题
        customParam.setLabel("促销名称");
        // 值
        customParam.setValue(sdMarketingProm.getName());
        // 字段类型
        customParam.setGspvsColumnsType("VARCHAR");
        // 字段长度
        customParam.setGspvsColumnsLength("20");
        // 列名
        customParam.setColumn("NAME");
        list.add(0, customParam);
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCoustomParams(List<CustomParam> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        // 该接口 只能针对唯一的一条促销进行修改
        CustomParam param = list.stream().filter(Objects::nonNull).filter(item -> !ObjectUtils.isEmpty(item.getPromId())).findFirst().orElse(null);
        if (ObjectUtils.isEmpty(param)) {
            throw new CustomResultException("当前促销不存在");
        }
        SdMarketingProm prom = sdMarketingPromMapper.selectOne(new QueryWrapper<SdMarketingProm>().eq("ID", param.getPromId()));

        for (CustomParam customParam : list) {
            QueryWrapper<SdMarketingProm> sdMarketingPromQueryWrapper = new QueryWrapper();
            sdMarketingPromQueryWrapper.eq("ID", customParam.getPromId());
            SdMarketingProm sdMarketingProm = sdMarketingPromMapper.selectOne(sdMarketingPromQueryWrapper);
            if (sdMarketingProm == null) {
                throw new CustomResultException("当前促销不存在");
            }
            SdMarketingProm entity = new SdMarketingProm();
            boolean flg = false;
            // 值
            for (Field field : sdMarketingProm.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(TableField.class)) {
                    TableField annotation = field.getAnnotation(TableField.class);
                    String gspvsColumns = annotation.value();
                    if (StringUtils.isNotBlank(gspvsColumns) && gspvsColumns.equals(customParam.getColumn())) {
                        setFieldValueByFieldName(field.getName(), entity, StringUtils.isBlank(customParam.getValue()) ? "" : customParam.getValue());
                        setFieldValueByFieldName(field.getName(), prom, StringUtils.isBlank(customParam.getValue()) ? "" : customParam.getValue());
                        flg = true;
                        break;
                    }
                }
            }
            if (flg) {
                sdMarketingPromMapper.update(entity, sdMarketingPromQueryWrapper);
            }
        }

        // 当前促销下 已有商品的 促销价 + 成本价
        List<PrdPriceAndCost> marketingPrdPriceAndCost = marketingBasicMapper.getMarketingPrdPriceAndCost(param.getPromId());
        marketingPrdPriceAndCost.forEach(prdPriceAndCost -> {
            SdMarketingPrd prd = new SdMarketingPrd();
            prd.setId(prdPriceAndCost.getId());
            boolean flg = setMarketingPrdPriceAndFits(prd, prom, prdPriceAndCost.getPriceNormal(), prdPriceAndCost.getPriceCost());
            if (flg) {
                sdMarketingPrdMapper.updateById(prd);
            }
        });


    }

    /**
     * 对象属性值
     *
     * @param fieldName 属性名
     * @param o         对象
     * @return 返回值
     */
    private Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(o, new Object[]{});
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据属性名设置属性值
     *
     * @param fieldName
     * @param object
     * @return
     */
    private void setFieldValueByFieldName(String fieldName, Object object, String value) {
        try {
            // 获取obj类的字节文件对象
            Class c = object.getClass();
            // 获取该类的成员变量
            Field f = c.getDeclaredField(fieldName);
            // 取消语言访问检查
            f.setAccessible(true);
            if (f.getType() == int.class || f.getType() == Integer.class) {
                // 给变量赋值
                if (StringUtils.isNotBlank(value)) {
                    f.set(object, NumberUtils.toInt(value));
                }
            }
            if (f.getType() == BigDecimal.class) {
                f.set(object, new BigDecimal(value));
            }
            if (f.getType() == String.class) {
                // 给变量赋值
                f.set(object, StringUtils.isBlank(value) ? "" : value);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.UNKNOWN_ERROR);
        }
    }


    /**
     * 营销任务会员宣传短信发送 定时任务
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void marketingSmsSendReminder() {
        // 1.1.确定本次短信会员查询条件
        List<SdMarketingSearch> searchListToSendSms = marketingSearchMapper.getCurrentSendSearch();
        if (CollectionUtils.isEmpty(searchListToSendSms)) {
            return;
        }
        // 两级分组 营销一级，加盟商二级
        Map<Integer, Map<String, List<SdMarketingSearch>>> searchMarketingClientMap = searchListToSendSms.stream().filter(Objects::nonNull).filter(item -> !ObjectUtils.isEmpty(item.getMarketingId()) && StringUtils.isNotBlank(item.getGsmsStore()))
                .collect(groupingBy(SdMarketingSearch::getMarketingId, groupingBy(SdMarketingSearch::getClient)));
        // 1.2.会使用到的全部短信
        List<SmsTemplate> smsList = smsTemplateMapper.selectList(new QueryWrapper<SmsTemplate>()
                .in(!CollectionUtils.isEmpty(searchListToSendSms), "SMS_ID", searchListToSendSms.stream().map(SdMarketingSearch::getGsmsSmsTemp).distinct().collect(toList()))
                // 类型为营销活动
                .eq("SMS_TYPE", "2")
                // 启用
                .eq("SMS_STATUS", "0")
                // 已经审批
                .eq("SMS_GSP_STATUS", "1"));
        if (CollectionUtils.isEmpty(smsList)) {
            return;
        }
        // 1.3.加盟商列表
        List<Franchisee> franchiseeList = franchiseeService.list();

        List<SdExasearchD> exasearchDList = new ArrayList<>();
        List<SmsSend> smsSendList = new ArrayList<>();
        searchMarketingClientMap.forEach((marketing, searchClientMap) -> {
            searchClientMap.forEach((client, searchList) -> {
                // 当前加盟商可以发送的短信条数
                List<SmsRechargeRecordDTO> smsRechargeRecordList = marketingSearchMapper.smsRechargeRecord(client);
                if (CollectionUtils.isEmpty(smsRechargeRecordList)) {
                    return;
                }
                int sumCanUseCount = smsRechargeRecordList.stream().mapToInt(SmsRechargeRecordDTO::getGsrrRechargeRemainCount).sum();
                LinkedList<SmsRechargeRecordDTO> linkedListOrigin = new LinkedList<>(smsRechargeRecordList);
                LinkedList<SmsRechargeRecordDTO> linkedListCurrent = new LinkedList<>();
                linkedListCurrent.addFirst(linkedListOrigin.removeFirst());

                // 当前需要发送短信的人数
                List<SmsSearchMemberDTO> collect = searchList.stream().filter(Objects::nonNull).map(search -> {
                    // 会员查询条件构造
                    GetMemberListVO3 getMemberListVO3 = JsonUtils.jsonToBean(search.getGsmsDetail(), GetMemberListVO3.class);
                    // 加盟商
                    getMemberListVO3.setClient(search.getClient());
                    // 所属门店
                    getMemberListVO3.setStoreList(Collections.singletonList(search.getGsmsStore()));
                    // 精准查询id
                    getMemberListVO3.setGsmsId(search.getGsmsId());
                    // 发送人数
                    getMemberListVO3.setGsmSmstimes(ObjectUtils.isEmpty(search.getGsmsSendNum()) ? null : Integer.valueOf(search.getGsmsSendNum()));
                    // 查询会员
                    List<GetMemberListDTO> memberList = getMemberListToSendSms(getMemberListVO3).getRecords();
                    return new SmsSearchMemberDTO(search, memberList);
                }).collect(toList());
                int sumNeedSendCount = collect.stream().filter(Objects::nonNull).filter(item -> !ObjectUtils.isEmpty(item.getMemberList())).mapToInt(item -> item.getMemberList().size()).sum();
                // 如果短信可用数量不足，遍历下一条
                if (sumCanUseCount < sumNeedSendCount) {
                    log.info("营销活动[{}],加盟商[{}]可用短信数目不足,需要条数[{}],可用条数[{}],未发送短信", marketing, client, sumNeedSendCount, sumCanUseCount);
                    return;
                }
                // 发送短信
                collect.stream().filter(Objects::nonNull)
                        .filter(item -> !ObjectUtils.isEmpty(item.getMemberList()) && item.getMemberList().size() > 0)
                        .forEach(smsSearchMemberDTO -> {
                            SdMarketingSearch search = smsSearchMemberDTO.getSearch();
                            List<GetMemberListDTO> memberList = smsSearchMemberDTO.getMemberList();
                            // 短信内容
                            SmsTemplate smsTemplate = smsList.stream().filter(Objects::nonNull).filter(item -> item.getSmsId().equals(search.getGsmsSmsTemp())).findFirst().orElse(null);
                            if (ObjectUtils.isEmpty(smsTemplate)) {
                                return;
                            }
                            memberList.forEach(member -> {
                                String sign = getSmsSign(franchiseeList, search, member);
                                if (StringUtils.isBlank(sign)) {
                                    return;
                                }
                                // 3.2短息内容生成，例:【签名名称】XXXX回复TD退订
                                String msgTemp = MessageFormat.format("【{0}】{1}回复TD退订", sign, smsTemplate.getSmsContent());
                                String sendTime = null;
                                SdExasearchD sdExasearchD = new SdExasearchD();
                                // 3.3 发送短信
                                Result result = null;
                                try {
                                    SmsEntity smsEntity = new SmsEntity();
                                    smsEntity.setPhone(member.getGmbMobile());
                                    smsEntity.setMsg(msgTemp);
                                    // 记录发送时间
                                    sendTime = DateUtils.getCurrentDateTimeStrTwo();
                                    result = smsUtils.sendMarketingSms(smsEntity);
//                                    result = ResultUtil.success();
                                } catch (Exception e) {
                                    log.info(e.getMessage());
                                } finally {
                                    // 0-发送成功, 1-发送失败
                                    if (!ObjectUtils.isEmpty(result) && ResultEnum.SUCCESS.getCode().equals(result.getCode())) {
                                        sdExasearchD.setGseStatus("0");
                                    } else {
                                        sdExasearchD.setGseStatus("1");
                                    }
                                }
                                // 加盟商 /当前加盟商
                                sdExasearchD.setClient(search.getClient());
                                // 查询主题 /常用查询id
                                sdExasearchD.setGseTheid(search.getGsmsId());
                                // 营销活动id
                                sdExasearchD.setGseMarketid(String.valueOf(search.getMarketingId()));
                                // 查询id /年月日时分秒
                                sdExasearchD.setGseSearchid(UUIDUtil.getUUID());
                                // 会员卡号
                                sdExasearchD.setGseCardId(member.getGmbCardId());
                                // 会员姓名
                                sdExasearchD.setGseMenname(member.getGmbName());
                                // 会员电话
                                sdExasearchD.setGseMobile(member.getGmbMobile());
                                // 性别
                                sdExasearchD.setGseSex(member.getGmbSex());
                                // 年龄
                                sdExasearchD.setGseAge(member.getGmbAge());
                                // 生日
                                sdExasearchD.setGseBirth(member.getGmbBirth());
                                // 积分
                                sdExasearchD.setGseIntegral(new BigDecimal(member.getGmbIntegral()));
                                // 会员卡类型
                                sdExasearchD.setGseClassId(member.getGsmbcClassId());
                                // 最后积分日期
                                sdExasearchD.setGseIntegralLastdate(member.getGsmbcIntegralLastdate());
                                // 所属门店编码
                                sdExasearchD.setGseBrId(member.getGmbBrId());
                                // 所属门店名称
                                sdExasearchD.setGseBrName(member.getGmbBrName());
                                // 会员级别
                                sdExasearchD.setGseGlid("");
                                // 会员微信
                                sdExasearchD.setGseWxid("");
                                // 是否发送短信
                                sdExasearchD.setGseSms("0");
                                // 是否发送微信
                                sdExasearchD.setGseWx("1");
                                // 类型 /1-短信, 2-电话
                                sdExasearchD.setGseNoticeType("1");
                                // 信息创建时间
                                sdExasearchD.setGseCretime(sendTime);
                                // 信息发送时间
                                sdExasearchD.setGseSendtime(sendTime);
                                // 短信模板ID
                                sdExasearchD.setGseTempid(smsTemplate.getSmsId());
                                exasearchDList.add(sdExasearchD);

                                // 如果发送成功 记录表:GAIA_SMS_SEND
                                if (!ObjectUtils.isEmpty(result) && ResultEnum.SUCCESS.getCode().equals(result.getCode())) {
                                    // 计算剩余可发短信条数
                                    SmsRechargeRecordDTO rechargeRecord = linkedListCurrent.getFirst();
                                    if (rechargeRecord.getGsrrRechargeRemainCount() == 0) {
                                        linkedListCurrent.addFirst(linkedListOrigin.removeFirst());
                                        rechargeRecord = linkedListCurrent.getFirst();
                                    }
                                    // 剩余数目减一
                                    rechargeRecord.setGsrrRechargeRemainCount(rechargeRecord.getGsrrRechargeRemainCount() - 1);
                                    // 使用数目加一
                                    rechargeRecord.setGsrrRechargeUseCount(rechargeRecord.getGsrrRechargeUseCount() + 1);

                                    SmsSend smsSend = new SmsSend();
                                    // 加盟商
                                    smsSend.setClient(search.getClient());
                                    // 手机号
                                    smsSend.setGssTel(member.getGmbMobile());
                                    // 发送时间
                                    smsSend.setGssSendTime(sendTime);
                                    // 发送内容
                                    smsSend.setGssSendContent(msgTemp);
                                    // 是否已开票
                                    smsSend.setGssInvoice(0);
                                    // 所属门店
                                    smsSend.setGssStore(member.getGmbBrId());
                                    // 发送单价
                                    smsSend.setGssPrice(rechargeRecord.getGsrrRechargePrice());
                                    // 充值记录ID
                                    smsSend.setGsrrId(rechargeRecord.getId());
                                    smsSendList.add(smsSend);
                                }
                            });
                        });

                // 更新短信购买记录表GAIA_SMS_RECHARGE_RECORD 已使用 ‘已用条数’
                linkedListCurrent.forEach(smsRechargeRecordDTO -> {
                    SmsRechargeRecord rechargeRecord = new SmsRechargeRecord();
                    rechargeRecord.setId(smsRechargeRecordDTO.getId());
                    rechargeRecord.setGsrrRechargeUseCount(smsRechargeRecordDTO.getGsrrRechargeUseCount());
                    smsRechargeRecordService.updateById(rechargeRecord);
                });
            });

        });

        if (!CollectionUtils.isEmpty(exasearchDList)) {
            marketingSearchMapper.insertExasearchDList(exasearchDList);
        }
        if (!CollectionUtils.isEmpty(smsSendList)) {
            marketingSearchMapper.insertSmsSendList(smsSendList);
        }

    }

    /**
     * 签名生成
     * 公司: 参数为加盟商
     * 品牌：参数为门店的名称（单体门店取，自己的门店名称，连锁门店取 连锁总部的名称）
     */
    private String getSmsSign(List<Franchisee> franchiseeList, SdMarketingSearch search, GetMemberListDTO member) {
        String sign = null;
        // 1品牌，2公司名
        if (search.getGsmsSmsSgin() == 1) {
            // 单体1
            String stoAttribute = member.getStoAttribute();
            if (StringUtils.isNotBlank(stoAttribute) && stoAttribute.equals("1")) {
                sign = StringUtils.isNotBlank(member.getGmbBrShortName()) ? member.getGmbBrShortName() : member.getGmbBrName();
            } else {
                sign = member.getGmbBrName();
            }
        } else {
            Franchisee franchisee = franchiseeList.stream().filter(item -> item.getClient().equals(member.getClient())).findFirst().orElse(null);
            if (!ObjectUtils.isEmpty(franchisee)) {
                sign = franchisee.getFrancName();
            }
        }
        return sign;
    }

    /**
     * 客户端新增促销
     *
     * @param sdMarketingProm
     */
    @Override
    public void addProm(SdMarketingProm sdMarketingProm) {
        TokenUser user = commonService.getLoginInfo();
        sdMarketingProm.setClient(user.getClient());
        sdMarketingProm.setType(sdMarketingProm.getGspvsMode().split("-")[0]);
        // 成分
        sdMarketingProm.setComponentId("");
        sdMarketingPromMapper.insert(sdMarketingProm);
    }

    /**
     * 促销详情
     *
     * @param promId
     * @return
     */
    @Override
    public SdMarketingProm getParams(Integer promId) {
        QueryWrapper<SdMarketingProm> sdMarketingPromQueryWrapper = new QueryWrapper();
        sdMarketingPromQueryWrapper.eq("ID", promId);
        SdMarketingProm sdMarketingProm = sdMarketingPromMapper.selectOne(sdMarketingPromQueryWrapper);
        return sdMarketingProm;
    }

    /**
     * 促销参数保存
     *
     * @param sdMarketingPromPrd
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveParams(SdMarketingPromPrd sdMarketingPromPrd) {
        QueryWrapper<SdMarketingPrd> sdMarketingPrdListQueryWrapper = new QueryWrapper<>();
        sdMarketingPrdListQueryWrapper.eq("PROM_ID", sdMarketingPromPrd.getId());
        List<SdMarketingPrd> sdMarketingPrdList = sdMarketingPrdMapper.selectList(sdMarketingPrdListQueryWrapper);
        if (CollectionUtils.isEmpty(sdMarketingPrdList) || sdMarketingPrdList.size() == 1) {
            // 修改促销
            sdMarketingPromMapper.updateById(sdMarketingPromPrd);
        } else {
            QueryWrapper<SdMarketingPrd> sdMarketingPrdQueryWrapper = new QueryWrapper<>();
            sdMarketingPrdQueryWrapper.eq("ID", sdMarketingPromPrd.getPrdId());
            sdMarketingPrdMapper.delete(sdMarketingPrdQueryWrapper);
            // 新增促销
            sdMarketingPromMapper.insert(sdMarketingPromPrd);
            SdMarketingPrd sdMarketingPrd = new SdMarketingPrd();
            // ID
            // 加盟商
            sdMarketingPrd.setClient(sdMarketingPromPrd.getClient());
            // 营销活动加盟商分配表主键
            sdMarketingPrd.setMarketingId(sdMarketingPromPrd.getMarketingId());
            // 促销关联表
            sdMarketingPrd.setPromId(sdMarketingPromPrd.getId());
            // 商品编码
            sdMarketingPrd.setProCode(sdMarketingPromPrd.getProCode());
            // 促销单价
            // 促销毛利率
            // 促销单价
            // 促销毛利率
            // 促销单价
            // 促销毛利率
            setMarketingPrdPriceAndFits(sdMarketingPrd, sdMarketingPromPrd, sdMarketingPromPrd.getPriceNormal(), sdMarketingPromPrd.getProCgj());
            sdMarketingPrdMapper.insert(sdMarketingPrd);
        }
    }

    @Override
    public Result exportProductList(Integer pageNum, Integer pageSize, Integer id, String proKey, BigDecimal mllStart, BigDecimal mllEnd,
                                    Integer promFitsType, BigDecimal promFitsStart, BigDecimal promFitsEnd) throws IOException {


        // 导出数据
        List<GetProductListToCheckDTO> records = getProductList(pageNum, pageSize, id, proKey, mllStart, mllEnd, promFitsType, promFitsStart, promFitsEnd).getRecords();
        // 导出数据表头
        String[] headList = {"商品编码", "商品名称", "规格", "生成厂家", "库存", "零售价", "成本价", "毛利率%",
                "促销单价1", "促销毛利率1", "促销单价2", "促销毛利率2", "促销单价3", "促销毛利率3", "促销方式"};
        List<List<Object>> dataList = records.stream().map(product -> {
            List<Object> row = new ArrayList<>();
            // 商品编码
            row.add(product.getProSelfCode());
            // 商品名称
            row.add(product.getProName());
            // 规格
            row.add(product.getProSpecs());
            // 生成厂家
            row.add(product.getProFactoryName());
            // 库存
            row.add(product.getWmKcsl());
            // 零售价
            row.add(product.getGsppPriceNormal());
            // 成本价
            row.add(product.getProCgj());
            // 毛利率%
            row.add(product.getMll());
            // 促销单价1
            row.add(product.getGsmpPrice1());
            // 促销毛利率1
            row.add(product.getGsmpFits1());
            // 促销单价2
            row.add(product.getGsmpPrice2());
            // 促销毛利率2
            row.add(product.getGsmpFits2());
            // 促销单价3
            row.add(product.getGsmpPrice3());
            // 促销毛利率3
            row.add(product.getGsmpFits3());
            // 促销方式
            row.add(product.getName());
            return row;
        }).collect(toList());

        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("委托煎药数据");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        return cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));

    }

    private IPage<GetMemberListDTO> getMemberListToSendSms(GetMemberListVO3 vo) {
        // 根据消费类型取天数
        if (OperateEnum.ConsumeType.FIVE.getCode().equals(vo.getConsumeType())) {
            vo.setConsumeTypeDay(0);
        } else {
            for (OperateEnum.ConsumeType type : OperateEnum.ConsumeType.values()) {
                if (!type.getCode().equals(vo.getConsumeType())) {
                    continue;
                }
                String consumeTypeDay = type.getMessage();
                vo.setConsumeTypeDay(Integer.valueOf(consumeTypeDay));
            }
        }
        Page page = new Page(-1, -1);
        log.info("getMemberPage3:{}", JsonUtils.beanToJson(vo));
        return marketingSearchMapper.getMemberPage3(page, vo);
    }
}
