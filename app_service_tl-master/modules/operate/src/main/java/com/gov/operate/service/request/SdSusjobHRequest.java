package com.gov.operate.service.request;

import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Data
public class SdSusjobHRequest extends Pageable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "0会员维系/1历史维系")
    @NotBlank(message = "会员维系类型必填")
    private String taskType;

    @ApiModelProperty(value = "0我的维系任务1会员维系任务")
    @NotBlank(message = "会员维系类型必填")
    private String status;


    @ApiModelProperty(value = "门店编码")
    private String stoCode;

    @ApiModelProperty(value = "维系任务名称")
    private String taskName;

}
