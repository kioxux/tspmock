package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Zhangchi
 * @since 2021/09/22/9:42
 */
@Data
@TableName("GAIA_GROSS_MARGIN_INTERVAL")
public class GaiaGrossMarginInterval implements Serializable {
    /**
     * 主键
     */
    @TableField("ID")
    private Long id;

    /**
     * 加盟商
     */
    @TableField("CLIENT")
    private String client;

    /**
     * 毛利等级：ABCDE
     */
    @TableField("INTERVAL_TYPE")
    private String intervalType;

    /**
     * 起始毛利
     */
    @TableField("START_VALUE")
    private BigDecimal startValue;

    /**
     * 截止毛利
     */
    @TableField("END_VALUE")
    private BigDecimal endValue;

    /**
     * 是否删除：0-正常 1-删除
     */
    @TableField("DELETE_FLAG")
    private Integer deleteFlag;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 创建者
     */
    @TableField("CREATE_USER")
    private String createUser;

    /**
     * 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;

    /**
     * 更新者
     */
    @TableField("UPDATE_USER")
    private String updateUser;

    /**
     * 版本
     */
    @TableField("VERSION")
    private Integer version;

}
