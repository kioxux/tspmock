package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromCouponGrant;
import com.gov.operate.entity.SdPromCouponUse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface SdPromCouponGrantMapper extends BaseMapper<SdPromCouponGrant> {

    IPage<ProductResponseVo> selectProductList(Page<SdPromCouponUse> page, @Param("product") ProductRequestVo productRequestVo);

    List<ProductResponseVo> selectGiftProductList(@Param("product")ProductRequestVo productRequestVo);
}
