package com.gov.operate.dto.recharge;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RechargeChangeInData {

    /**
     * 储值卡卡号
     */
    @ApiModelProperty(name = "储值卡卡号")
    private String gsrcId;

    /**
     * 会员卡号
     */
    @ApiModelProperty(name = "会员卡号")
    private String gsrcMemberCardId;

    /**
     * 姓名
     */
    @ApiModelProperty(name = "姓名")
    private String gsrcName;

    /**
     * 手机
     */
    @ApiModelProperty(name = "手机")
    private String gsrcMobile;

    /**
     * 开始日期
     */
    @NotBlank(message = "开始日期不能为空")
    @ApiModelProperty(name = "开始日期")
    private String startDate;

    /**
     * 结束日期
     */
    @NotBlank(message = "结束日期不能为空")
    @ApiModelProperty(name = "结束日期")
    private String endDate;

    /**
     * 异动门店
     */
    @ApiModelProperty(name = "门店")
    private String gsrcBrId;

    /**
     * 异动单号
     */
    @ApiModelProperty(name = "单号")
    private String gsrcVoucherId;

    /**
     * 异动类型 1：充值 2：退款 3：消费 4：退货
     */
    @ApiModelProperty(name ="异动类型 1：充值 2：退款 3：消费 4：退货")
    private Integer gsrcType;

    @ApiModelProperty("起始页")
    private Integer pageNum;

    @ApiModelProperty("页面数量")
    private Integer pageSize;
}
