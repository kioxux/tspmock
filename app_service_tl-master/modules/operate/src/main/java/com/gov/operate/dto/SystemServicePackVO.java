package com.gov.operate.dto;

import com.gov.operate.entity.SystemServicePack;
import lombok.Data;

@Data
public class SystemServicePackVO extends SystemServicePack {
    private String arId2;

    private String resourceId;
}
