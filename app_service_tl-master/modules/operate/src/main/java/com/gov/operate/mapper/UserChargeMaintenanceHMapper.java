package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.charge.ClientStoreDataVO;
import com.gov.operate.dto.charge.PayableItemVO;
import com.gov.operate.entity.PayingItemNew;
import com.gov.operate.entity.StoreData;
import com.gov.operate.entity.UserChargeMaintenanceH;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户收费维护主表 Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface UserChargeMaintenanceHMapper extends BaseMapper<UserChargeMaintenanceH> {
    List<PayableItemVO> getPayableList(@Param("spId") String spId,
                                       @Param("days") Integer days,
                                       @Param("client") String client,
                                       @Param("payFreq") Integer payFreq);

    List<StoreData> getStoreList(@Param("sphId") Integer sphId,
                                 @Param("clientId") String clientId,
                                 @Param("spmId") Integer spmId);

    List<ClientStoreDataVO> getStoreVOList(@Param("sphId") Integer sphId,
                                           @Param("clientId") String clientId,
                                           @Param("spmId") Integer spmId);

    List<PayingItemNew> selectStoreStartDate(@Param("client") String client, @Param("stoList") List<String> stoList, @Param("rId") Integer rId);

    PayingItemNew selectFicoEndingTime(@Param("client") String client);
}
