package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.AppVersionDTO;
import com.gov.operate.dto.EditAppVO;
import com.gov.operate.dto.GetAppListVO;
import com.gov.operate.dto.SaveAppVO;
import com.gov.operate.entity.AppVersion;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
public interface IAppVersionService extends SuperService<AppVersion> {

    /**
     * APP版本列表
     */
    IPage<AppVersionDTO> getAppList(GetAppListVO vo);

    /**
     * APP版本新增
     */
    void saveApp(SaveAppVO vo);

    /**
     * APP版本编辑
     */
    void editApp(EditAppVO vo);

    /**
     * APP版本删除
     */
    void removeApp(String id);

    /**
     * APP版本发布
     */
    void releaseApp(String id);

    /**
     * APP版本详情
     */
    AppVersionDTO getApp(String id);
}
