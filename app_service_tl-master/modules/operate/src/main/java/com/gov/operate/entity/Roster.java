package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_ROSTER")
@ApiModel(value="Roster对象", description="")
public class Roster extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLENT")
    private String clent;

    @ApiModelProperty(value = "门店编码")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "员工编号")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "基本工资级别")
    @TableField("SLA_CL_NO")
    private String slaClNo;

    @ApiModelProperty(value = "基本工资")
    @TableField("SLA_AMT")
    private BigDecimal slaAmt;

    @ApiModelProperty(value = "岗位")
    @TableField("POS_NO")
    private String posNo;

    @ApiModelProperty(value = "岗位工资")
    @TableField("POS_SLA")
    private BigDecimal posSla;

    @ApiModelProperty(value = "考评工资基数")
    @TableField("EVA_BASE")
    private BigDecimal evaBase;

    @ApiModelProperty(value = "KPI评分")
    @TableField("KPI_TAR")
    private BigDecimal kpiTar;


}
