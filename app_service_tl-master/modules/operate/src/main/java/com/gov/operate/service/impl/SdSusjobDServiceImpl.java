package com.gov.operate.service.impl;

import com.gov.operate.entity.SdSusjobD;
import com.gov.operate.mapper.SdSusjobDMapper;
import com.gov.operate.service.ISdSusjobDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Service
public class SdSusjobDServiceImpl extends ServiceImpl<SdSusjobDMapper, SdSusjobD> implements ISdSusjobDService {

}
