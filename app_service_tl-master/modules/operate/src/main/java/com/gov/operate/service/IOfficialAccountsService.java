package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.entity.weChat.BatchgetMaterialRes;
import com.gov.common.entity.weChat.MediaIdResDTO;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.dto.weChat.*;
import com.gov.operate.entity.OfficialAccounts;
import com.gov.operate.entity.WechatTweetsH;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-28
 */
public interface IOfficialAccountsService extends SuperService<OfficialAccounts> {

    /**
     * 公众号列表
     */
    IPage<OfficialAccountsDTO> queryWechatList(QueryWechatListVO vo);

    /**
     * 公众号编辑
     */
    void editWechat(EditWechatVO vo);

    /**
     * 公众号数据获取
     */
    GetWechatDTO getWechat(GetWechatVO vo);

    /**
     * 公众号APPID和SECRECT设置
     *
     * @param vo
     */
    void editAppidAndSecrect(AppidAndSecrectVO vo);

    /**
     * 公众号素材列表
     *
     * @param vo
     * @return
     */
    BatchgetMaterialRes getMaterialList(GetMaterialListVO vo);

    /**
     * 公众号素材详情
     *
     * @param mediaId
     * @return
     */
    MediaIdResDTO getMaterial(String mediaId, String gwthMaterialType);

    /**
     * 公众号菜单列表
     *
     * @param vo
     * @return
     */
    Object getWeChatMenu(GetWeChatMenuVO vo);

    /**
     * 公众号菜单编辑
     *
     * @param vo
     */
    void editWeChatMenu(EditWeChatMenuVO vo);

    /**
     * 公众号文章推送
     */
    void tweetsPush(TweetsPushVO vo);

    /**
     * 公众号文章发布
     *
     * @param vo
     */
    void tweetsRelease(TweetsReleaseVO vo);

    /**
     * 客户列表
     *
     * @param vo
     * @return
     */
    List<OfficialAccountsDTO> getCostomerList(GetCostomerListVO vo);

    /**
     * 客户公众号素材列表
     *
     * @param vo
     * @return
     */
    IPage<GetCostomerMaterialListDTO> getCostomerMaterialList(GetCostomerMaterialListVO vo);


    /**
     * 客户下拉框
     *
     * @return
     */
    List<GetCostomerDropDownList> getCostomerDropDownList();


    /**
     * 公众号列表(平台用户)
     *
     * @param vo
     * @return
     */
    IPage<OfficialAccountsDTO> queryWechatListByPlatform(QueryWechatListVO vo);

    /**
     * 推文预览
     *
     * @param url
     * @return
     */
    Result getTweetsContent(String url);

    /**
     * 微信公众号定时推送
     */
    void tweetsPushJob();

    /**
     * 一个公众号 一个推文 进行发布 (一个公众号 一个推文 一个事务)
     */
    void tweetItemPushAndRelease(WechatTweetsH wechatTweetsH);

    /**
     * 推文授权
     *
     * @param goaCode
     * @param goaType
     * @param wechatTweets
     */
    void wechatTweets(String goaCode, Integer goaType, String wechatTweets);

    BatchgetMaterialRes getDraftBatchgetList(GetMaterialListVO vo);

    void freepublishSubmit(String media_id);

    /**
     * 微信会员卡授权
     * @param goaCode
     * @param goaType
     * @param wechatMembershipCard
     */
    void wechatMembershipCard(String goaCode, Integer goaType, String wechatMembershipCard);
}
