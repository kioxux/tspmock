package com.gov.operate.dto.charge;

import com.gov.operate.entity.UserChargeMaintenanceM;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PayableItemVO extends UserChargeMaintenanceM {
    private BigDecimal receivableAmount;
    private Integer stoCount;
    private Integer payFreq;
    private BigDecimal discountAmount;
    private BigDecimal contractAmount;
    private BigDecimal payAmount;
    private String ficoEndingTime;
    private String ficoLastDeadLine;
    private String clientName;
    private String ficoStartingTime;
    private List<ClientStoreDataVO> storeList;
}
