package com.gov.operate.service.ssp;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.ssp.SspUserProductPageDTO;
import com.gov.operate.entity.SspUserProduct;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

/**
 *
 */
public interface ISspUserProductService extends SuperService<SspUserProduct> {

    Result getSupplierProductList(SspUserProductPageDTO sspUserProductPageDTO);

    /**
     * 绑定用户产品
     *
     * @param productList
     * @param userId
     * @return
     */
    Result addUserProduct(List<SspUserProduct> productList, Long userId);

    /**
     * 移除用户绑定产品
     *
     * @param ids
     * @return
     */
    Result delUserProduct(List<SspUserProduct> productList);


    Result getUserProductList(SspUserProductPageDTO sspUserProductPageDTO);

    int deleteChainHead(Long userId, String client, Set<String> keySet);

    /**
     * 导入品种
     *
     * @param file
     * @param userId
     * @return
     */
    Result importSupplierProductList(MultipartFile file, Long userId);
}
