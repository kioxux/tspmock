package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryProCps;
import com.gov.operate.mapper.SalaryProCpsMapper;
import com.gov.operate.service.ISalaryProCpsService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryProCpsServiceImpl extends ServiceImpl<SalaryProCpsMapper, SalaryProCps> implements ISalaryProCpsService {

    /**
     * 薪资方案单品提成
     */
    @Override
    public void saveProCps(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除原始数据
        this.remove(new QueryWrapper<SalaryProCps>().eq("GSPC_GSP_ID", gspId));
        List<SalaryProCps> salaryProCpsList = vo.getSalaryProCpsList();
        if (CollectionUtils.isEmpty(salaryProCpsList)) {
            return;
        }
        for (int i = 0; i < salaryProCpsList.size(); i++) {
            SalaryProCps salaryProCps = salaryProCpsList.get(i);
            if (StringUtils.isEmpty(salaryProCps.getGspcProCode())) {
                throw new CustomResultException(MessageFormat.format("单品提成商品第[{0}]条商品编码不能为空", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryProCps.getGspcCommissionAmt())) {
                throw new CustomResultException(MessageFormat.format("单品提成商品第[{0}]条提成金额不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryProCps.getProName())) {
                throw new CustomResultException(MessageFormat.format("单品提成商品第[{0}]条商品名称不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryProCps.getProSpecs())) {
                throw new CustomResultException(MessageFormat.format("单品提成商品第[{0}]条商品规格不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryProCps.getProFactoryName())) {
                throw new CustomResultException(MessageFormat.format("单品提成商品第[{0}]条商品生产厂家不能为空", i + 1));
            }
            salaryProCps.setGspcGspId(gspId);
            salaryProCps.setGspcSort(i + 1);
        }
        this.saveBatch(salaryProCpsList);
    }

    /**
     * 单品提成列表
     */
    @Override
    public List<SalaryProCps> getProCpsList(Integer gspId) {
        List<SalaryProCps> list = this.list(new QueryWrapper<SalaryProCps>()
                .eq("GSPC_GSP_ID", gspId)
                .orderByAsc("GSPC_SORT"));
        return list;
    }
}
