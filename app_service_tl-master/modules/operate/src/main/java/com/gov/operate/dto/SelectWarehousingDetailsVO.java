package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectWarehousingDetailsVO {

    @NotBlank(message = "单据号不能为空")
    private String matDnId;

    @NotBlank(message = "业务类型不能为空（CD-采购、GD-退厂）")
    private String type;

    private String client;

    private String matId;

    private String matYear;

    private String matLineNo;

    /**
     * 导出 参数
     */
    private List<String> matDnIdList;
}
