package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-07-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PURCHASE_REMIND")
@ApiModel(value="PurchaseRemind对象", description="")
public class PurchaseRemind extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("ID")
    private Long id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("BR_ID")
    private String brId;

    @ApiModelProperty(value = "单号")
    @TableField("BILL_NO")
    private String billNo;

    @ApiModelProperty(value = "会员卡号")
    @TableField("MEMBER_CARD")
    private String memberCard;

    @ApiModelProperty(value = "手机号码")
    @TableField("TEL")
    private String tel;

    @ApiModelProperty(value = "商品编码")
    @TableField("PRO_ID")
    private String proId;

    @ApiModelProperty(value = "药品名称")
    @TableField("CH_TRADE_NAME")
    private String chTradeName;

    @ApiModelProperty(value = "购药时间")
    @TableField("PURCHASE_TIME")
    private LocalDateTime purchaseTime;

    @ApiModelProperty(value = "购药提醒日期")
    @TableField("REMIND_DATE")
    private LocalDateTime remindDate;

    @ApiModelProperty(value = "是否已发送 默认N")
    @TableField("IS_SEND")
    private String isSend;

    @ApiModelProperty(value = "创建时间")
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;


}
