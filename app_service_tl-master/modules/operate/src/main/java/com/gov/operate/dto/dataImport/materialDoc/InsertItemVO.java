package com.gov.operate.dto.dataImport.materialDoc;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author zhoushuai
 * @date 2021/4/26 15:10
 */
@Data
public class InsertItemVO {

    @NotBlank(message = "文件名不能为空")
    private String gmdiName;

    @NotBlank(message = "路径不能为空")
    private String gmdiPath;

    @NotNull(message = "类型不能为空")
    private Integer gmdiType;

//    @NotNull(message = "件数不能为空")
    private Integer gmdiCount;

//    @NotNull(message = "总数量不能为空")
    private BigDecimal gmdiQty;

//    @NotNull(message = "总金额不能为空")
    private BigDecimal gmdiAmt;

}
