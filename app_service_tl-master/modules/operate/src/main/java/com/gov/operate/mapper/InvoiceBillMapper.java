package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.dto.invoiceOptimiz.InvoiceBillDetailsDTO;
import com.gov.operate.entity.InvoiceBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
public interface InvoiceBillMapper extends BaseMapper<InvoiceBill> {

    /**
     * 单号
     * @param client
     * @return
     */
    Long selectBillNum(String client);

    /**
     * 可登记发票
     * @param page
     * @param client
     * @param siteCode
     * @param supCode
     * @param invoiceNum
     * @param invoiceDateStart
     * @param invoiceDateEnd
     * @return
     */
    IPage<FicoInvoiceInformationRegistrationDTO> selectInvoiceListForBill(Page page, @Param("client") String client, @Param("siteCode")String siteCode,
                                                                          @Param("supCode") String supCode, @Param("invoiceNum") String invoiceNum,
                                                                          @Param("invoiceDateStart") String invoiceDateStart, @Param("invoiceDateEnd") String invoiceDateEnd);

    /**
     * 发票对账信息
     *
     * @param client 加盟商
     * @param id 对账单号
     * @return
     */
    InvoiceBillDetailsDTO getInvoiceBill(@Param("client") String client, @Param("id") String id);

    /**
     * 发票对账_发票信息
     *
     * @param client 加盟商
     * @param id 对账单号
     * @return
     */
    List<InvoiceBillDetailsDTO.InvoiceBillInvoiceDTO> getInvoiceBillInvoiceList(@Param("client") String client, @Param("id") String id);

    /**
     * 发票对账_单据信息
     *
     * @param client 加盟商
     * @param id 对账单号
     * @return
     */
    List<InvoiceBillDetailsDTO.InvoiceBillDocDTO> getInvoiceBillDocList(@Param("client") String client, @Param("id") String id);

    /**
     * 发票对账_单据明细
     *
     * @param client 加盟商
     * @param id 对账单号
     * @param gibdiMatDnId 单据号
     * @param gibdiBusinessType 单据业务类型
     * @return
     */
    List<InvoiceBillDetailsDTO.InvoiceBillDocDTO.InvoiceBillDocDetailsDTO> getInvoiceBillDocDetailsList(@Param("client") String client, @Param("id") String id, @Param("gibdiMatDnId") String gibdiMatDnId, @Param("gibdiBusinessType") String gibdiBusinessType);
}
