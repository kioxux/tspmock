package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.BillEndBatchVO;
import com.gov.operate.dto.invoice.GetPaymentApplyDetails;
import com.gov.operate.dto.storePayment.PaymentApproval;

import java.io.IOException;
import java.util.List;

public interface PaymentService {

    /**
     * 可付款单据
     */
    IPage<SelectBillDTO> selectBill(SelectBillVO vo);

    /**
     * 可付款单据明细
     */
    List<SelectBillDetailsDTO> selectBillDetails(SelectBillDetailsVO vo);

    /**
     * 单据付款
     */
    void billPayment(List<BillPaymentVO> list);

    /**
     * 明细付款
     */
    void billDetailPayment(BillDetailPaymentVO vo);

    /**
     * 发票付款
     */
    void invoicePayment(List<InvoicePaymentVO> list);

    /**
     * 付款记录
     *
     * @param paymentRecordDto
     * @return
     */
    Result paymentRecord(PaymentRecordDto paymentRecordDto);

    /**
     * 发票查询
     */
    List<SelectInvoiceDTO> selectInvoice(SelectInvoiceVO vo);

    /**
     * 付款申请
     *
     * @param paymentApproval
     */
    String paymentApproval(PaymentApproval paymentApproval);

    /**
     * 付款单明细
     *
     * @param id
     * @return
     */
    Result paymentRecordDetails(Integer id);

    /**
     * 供应商发票登记付款查询
     *
     * @param selectInvoiceVO
     * @return
     */
    Result selectInvoicePayRecord(SelectInvoiceVO selectInvoiceVO);

    /**
     * 供应商发票登记付款查询导出
     *
     * @param selectInvoiceVO
     * @return
     */
    Result selectInvoicePayRecordExport(SelectInvoiceVO selectInvoiceVO) throws IOException;

    /**
     * 供应商单据登记付款查询
     *
     * @param selectWarehousingVO
     * @return
     */
    Result selectBillPayRecord(SelectWarehousingVO selectWarehousingVO);

    /**
     * 供应商单据登记付款查询导出
     *
     * @param selectWarehousingVO
     * @return
     */
    Result selectBillPayRecordExport(SelectWarehousingVO selectWarehousingVO) throws IOException;

    /**
     * 单据核销
     *
     * @param gbieNum
     * @param gbieBillType
     */
    void billEnd(String gbieNum, String gbieBillType);

    /**
     * 发票核销
     *
     * @param gbieNum
     */
    void invoiceEnd(String gbieNum);

    /**
     * 审批页面_通过工作流编号查询付款明细
     *
     * @param approvalFlowNo
     * @return
     */
    List<GetPaymentApplyDetails> getPaymentApplyDetails(String approvalFlowNo);

    /**
     * 发票批量核销
     */
    void invoiceEndBatch(List<String> invoiceNumList);

    /**
     * 单据批量核销
     */
    void billEndBatch(List<BillEndBatchVO> list);

    /**
     * 付款记录明细导出
     *
     * @param id
     * @return
     */
    Result paymentRecordDetailsExport(Integer id) throws IOException;

    /**
     * 付款记录明细导出
     *
     * @param id
     * @return
     */
    Result paymentRecordItemsExport(Integer id) throws IOException;

    /**
     * 付款记录删除
     *
     * @param id
     * @return
     */
    Result delPaymentRecord(Integer id);

    /**
     * 导出
     *
     * @param paymentRecordDto
     * @return
     */
    Result paymentRecordExport(PaymentRecordDto paymentRecordDto) throws IOException;
}
