package com.gov.operate.dto.dataImport.materialDoc;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhoushuai
 * @date 2021-06-09 16:10
 */
@Data
public class  GaiaMaterialAssessDO {

    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品编码
     */
    private String matProCode;
    /**
     * 估价地点(门店)
     */
    private String matAssessSite;
    /**
     * 总库存
     */
    private BigDecimal matTotalQty;
    /**
     * 总金额
     */
    private BigDecimal matTotalAmt;
    /**
     * 移动平均价
     */
    private BigDecimal matMovPrice;
    /**
     * 税金
     */
    private BigDecimal matRateAmt;
    /**
     * 创建日期
     */
    private String batCreateDate;
    /**
     * 创建时间
     */
    private String batCreateTime;
    /**
     * 创建人
     */
    private String batCreateUser;
    /**
     * 更新日期
     */
    private String batChangeDate;
    /**
     * 更新时间
     */
    private String batChangeTime;
    /**
     * 更新人
     */
    private String batChangeUser;
    /**
     * 加点后金额
     */
    private BigDecimal matAddAmt;
    /**
     * 加点后税金
     */
    private BigDecimal matAddTax;

}
