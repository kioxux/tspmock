package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @author: jiangbl
 * @date: 2022/1/17 16:51
 * @describe:
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class GetCleanDrawerPersonnelVO {
    @NotBlank(message = "checkShop不可为空")
    private String checkShop;
}
