package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.StogData;
import com.gov.operate.mapper.StogDataMapper;
import com.gov.operate.service.IStogDataService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Service
public class StogDataServiceImpl extends ServiceImpl<StogDataMapper, StogData> implements IStogDataService {

    @Override
    public Result selectStogDateList() {
        return ResultUtil.success(baseMapper.selectList(null));
    }
}
