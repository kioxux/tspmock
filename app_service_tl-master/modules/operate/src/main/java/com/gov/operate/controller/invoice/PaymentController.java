package com.gov.operate.controller.invoice;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.BillEndBatchVO;
import com.gov.operate.dto.storePayment.PaymentApproval;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.PaymentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("payment/payment")
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Log("可付款单据")
    @ApiOperation(value = "可付款单据")
    @PostMapping("selectBill")
    public Result selectBill(@RequestBody @Valid SelectBillVO vo) {
        return ResultUtil.success(paymentService.selectBill(vo));
    }

    @Log("可付款单据明细")
    @ApiOperation(value = "可付款单据明细")
    @PostMapping("selectBillDetails")
    public Result selectBillDetails(@RequestBody @Valid SelectBillDetailsVO vo) {
        return ResultUtil.success(paymentService.selectBillDetails(vo));
    }

    @Log("单据付款")
    @ApiOperation(value = "单据付款")
    @PostMapping("billPayment")
    public Result billPayment(@RequestBody @Valid List<BillPaymentVO> list) {
        paymentService.billPayment(list);
        return ResultUtil.success();
    }

    @Log("明细付款")
    @ApiOperation(value = "明细付款")
    @PostMapping("billDetailPayment")
    public Result billDetailPayment(@RequestBody @Valid BillDetailPaymentVO vo) {
        paymentService.billDetailPayment(vo);
        return ResultUtil.success();
    }

    @Log("发票付款")
    @ApiOperation(value = "发票付款")
    @PostMapping("invoicePayment")
    public Result invoicePayment(@RequestBody @Valid List<InvoicePaymentVO> list) {
        paymentService.invoicePayment(list);
        return ResultUtil.success();
    }

    @Log("付款记录")
    @ApiOperation(value = "付款记录")
    @PostMapping("paymentRecord")
    public Result paymentRecord(@RequestBody @Valid PaymentRecordDto paymentRecordDto) {
        return paymentService.paymentRecord(paymentRecordDto);
    }

    @Log("付款记录导出")
    @ApiOperation(value = "付款记录导出")
    @PostMapping("paymentRecordExport")
    public Result paymentRecordExport(@RequestBody @Valid PaymentRecordDto paymentRecordDto) throws IOException {
        return paymentService.paymentRecordExport(paymentRecordDto);
    }

    @Log("付款记录删除")
    @ApiOperation(value = "付款记录删除")
    @PostMapping("delPaymentRecord")
    public Result delPaymentRecord(@RequestJson("id") Integer id) {
        return paymentService.delPaymentRecord(id);
    }

    @Log("付款记录明细")
    @ApiOperation(value = "付款记录明细")
    @GetMapping("paymentRecordDetails")
    public Result paymentRecordDetails(@RequestParam("id") Integer id) {
        return paymentService.paymentRecordDetails(id);
    }

    @Log("付款记录明细导出")
    @ApiOperation(value = "付款记录明细导出")
    @GetMapping("paymentRecordDetailsExport")
    public Result paymentRecordDetailsExport(@RequestParam("id") Integer id) throws IOException {
        return paymentService.paymentRecordDetailsExport(id);
    }

    @Log("付款记录明细导出")
    @ApiOperation(value = "付款记录明细导出")
    @GetMapping("paymentRecordItemsExport")
    public Result paymentRecordItemsExport(@RequestParam("id") Integer id) throws IOException {
        return paymentService.paymentRecordItemsExport(id);
    }

    @Log("付款申请")
    @ApiOperation(value = "付款申请")
    @PostMapping("paymentApproval")
    public Result paymentApproval(@RequestBody PaymentApproval paymentApproval) {
        String flowNo = paymentService.paymentApproval(paymentApproval);
        return ResultUtil.success(flowNo);
    }

    @Log("发票查询")
    @ApiOperation(value = "发票查询")
    @PostMapping("selectInvoice")
    public Result selectInvoice(@RequestBody @Valid SelectInvoiceVO vo) {
        return ResultUtil.success(paymentService.selectInvoice(vo));
    }

    @Log("供应商发票登记付款查询")
    @ApiOperation(value = "供应商发票登记付款查询")
    @PostMapping("selectInvoicePayRecord")
    public Result selectInvoicePayRecord(@RequestBody @Valid SelectInvoiceVO selectInvoiceVO) {
        return paymentService.selectInvoicePayRecord(selectInvoiceVO);
    }

    @Log("供应商发票登记付款查询导出")
    @ApiOperation(value = "供应商发票登记付款查询导出")
    @PostMapping("selectInvoicePayRecordExport")
    public Result selectInvoicePayRecordExport(@RequestBody @Valid SelectInvoiceVO selectInvoiceVO) throws IOException {
        return paymentService.selectInvoicePayRecordExport(selectInvoiceVO);
    }

    @Log("供应商单据登记付款查询")
    @ApiOperation(value = "供应商单据登记付款查询")
    @PostMapping("selectBillPayRecord")
    public Result selectBillPayRecord(@RequestBody @Valid SelectWarehousingVO selectWarehousingVO) {
        return paymentService.selectBillPayRecord(selectWarehousingVO);
    }

    @Log("供应商单据登记付款查询导出")
    @ApiOperation(value = "供应商单据登记付款查询导出")
    @PostMapping("selectBillPayRecordExport")
    public Result selectBillPayRecordExport(@RequestBody @Valid SelectWarehousingVO selectWarehousingVO) throws IOException {
        return paymentService.selectBillPayRecordExport(selectWarehousingVO);
    }

    @Log("单据核销")
    @ApiOperation(value = "单据核销")
    @PostMapping("billEnd")
    public Result billEnd(@RequestJson(value = "gbieNum", name = "单据号") String gbieNum,
                          @RequestJson(value = "gbieBillType", name = "单据类型") String gbieBillType) {
        paymentService.billEnd(gbieNum, gbieBillType);
        return ResultUtil.success();
    }

    @Log("单据批量核销")
    @ApiOperation(value = "单据批量核销")
    @PostMapping("billEndBatch")
    public Result billEndBatch(@Valid @RequestBody List<BillEndBatchVO> list) {
        paymentService.billEndBatch(list);
        return ResultUtil.success();
    }

    @Log("发票核销")
    @ApiOperation(value = "发票核销")
    @PostMapping("invoiceEnd")
    public Result invoiceEnd(@RequestJson(value = "gbieNum", name = "发票号") String gbieNum) {
        paymentService.invoiceEnd(gbieNum);
        return ResultUtil.success();
    }

    @Log("发票批量核销")
    @ApiOperation(value = "发票批量核销")
    @PostMapping("invoiceEndBatch")
    public Result invoiceEndBatch(@RequestBody List<String> invoiceNumList) {
        paymentService.invoiceEndBatch(invoiceNumList);
        return ResultUtil.success();
    }

    @Log("审批页面_通过工作流编号查询付款明细")
    @ApiOperation(value = "审批页面_通过工作流编号查询付款明细")
    @PostMapping("getPaymentApplyDetails")
    public Result getPaymentApplyDetails(@RequestJson(value = "approvalFlowNo", name = "工作流编号") String approvalFlowNo) {
        return ResultUtil.success(paymentService.getPaymentApplyDetails(approvalFlowNo));
    }


}
