package com.gov.operate.mapper;

import com.gov.operate.entity.BatchHeadPurchase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.BatchQueryAuth;
import com.gov.operate.entity.BatchQueryAuthData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 总部采购 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface BatchHeadPurchaseMapper extends BaseMapper<BatchHeadPurchase> {

    /**
     * 总部采购
     *
     * @param batchQueryAuth
     * @param itemList
     * @return
     */
    Integer saveDdiList(@Param("batchQueryAuth") BatchQueryAuth batchQueryAuth, @Param("itemList") List<BatchQueryAuthData> itemList, @Param("day") String day);
}
