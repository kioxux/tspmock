package com.gov.operate.mapper;

import com.gov.operate.entity.CustomerBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户主数据业务信息表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-06-10
 */
public interface CustomerBusinessMapper extends BaseMapper<CustomerBusiness> {

}
