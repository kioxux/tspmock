package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_GRADE_LIST")
@ApiModel(value="SdGradeList对象", description="")
public class SdGradeList extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "分级编号")
    @TableId("GSGL_ID")
    private String gsglId;

    @ApiModelProperty(value = "分级名称")
    @TableField("GSGL_NAME")
    private String gsglName;

    @ApiModelProperty(value = "分级颜色")
    @TableField("GSGL_COLOR")
    private String gsglColor;


}
