package com.gov.operate.service.impl;

import com.gov.operate.entity.PayingItem;
import com.gov.operate.mapper.PayingItemMapper;
import com.gov.operate.service.IPayingItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
@Service
public class PayingItemServiceImpl extends ServiceImpl<PayingItemMapper, PayingItem> implements IPayingItemService {

}
