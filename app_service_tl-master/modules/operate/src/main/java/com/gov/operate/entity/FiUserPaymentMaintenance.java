package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FI_USER_PAYMENT_MAINTENANCE")
@ApiModel(value="FiUserPaymentMaintenance对象", description="")
public class FiUserPaymentMaintenance extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款主体编码")
    @TableField("FICO_PAYINGSTORE_CODE")
    private String ficoPayingstoreCode;

    @ApiModelProperty(value = "付款主体名称")
    @TableField("FICO_PAYINGSTORE_NAME")
    private String ficoPayingstoreName;

    @ApiModelProperty(value = "生效日期")
    @TableField("FICO_EFFECTIVE_DATE")
    private String ficoEffectiveDate;

    @ApiModelProperty(value = "收费标准")
    @TableField("FICO_CURRENT_CHARGING_STANDARD")
    private BigDecimal ficoCurrentChargingStandard;

}
