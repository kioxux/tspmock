package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 采购订单主表
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PO_HEADER")
@ApiModel(value="PoHeader对象", description="采购订单主表")
public class PoHeader extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "采购凭证号")
    @TableField("PO_ID")
    private String poId;

    @ApiModelProperty(value = "采购订单类型")
    @TableField("PO_TYPE")
    private String poType;

    @ApiModelProperty(value = "供应商编码")
    @TableField("PO_SUPPLIER_ID")
    private String poSupplierId;

    @ApiModelProperty(value = "采购主体")
    @TableField("PO_COMPANY_CODE")
    private String poCompanyCode;

    @ApiModelProperty(value = "主体类型")
    @TableField("PO_SUBJECT_TYPE")
    private String poSubjectType;

    @ApiModelProperty(value = "凭证日期")
    @TableField("PO_DATE")
    private String poDate;

    @ApiModelProperty(value = "付款条款")
    @TableField("PO_PAYMENT_ID")
    private String poPaymentId;

    @ApiModelProperty(value = "抬头备注")
    @TableField("PO_HEAD_REMARK")
    private String poHeadRemark;

    @ApiModelProperty(value = "物流模式")
    @TableField("PO_DELIVERY_TYPE")
    private String poDeliveryType;

    @ApiModelProperty(value = "物流模式门店")
    @TableField("PO_DELIVERY_TYPE_STORE")
    private String poDeliveryTypeStore;

    @ApiModelProperty(value = "前序订单单号")
    @TableField("PO_PRE_POID")
    private String poPrePoid;

    @ApiModelProperty(value = "门店请货单号")
    @TableField("PO_REQ_ID")
    private String poReqId;

    @ApiModelProperty(value = "创建人")
    @TableField("PO_CREATE_BY")
    private String poCreateBy;

    @ApiModelProperty(value = "创建日期")
    @TableField("PO_CREATE_DATE")
    private String poCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("PO_CREATE_TIME")
    private String poCreateTime;

    @ApiModelProperty(value = "审批状态")
    @TableField("PO_APPROVE_STATUS")
    private String poApproveStatus;

    @ApiModelProperty(value = "更新人")
    @TableField("PO_UPDATE_BY")
    private String poUpdateBy;

    @ApiModelProperty(value = "更新日期")
    @TableField("PO_UPDATE_DATE")
    private String poUpdateDate;

    @ApiModelProperty(value = "更新时间")
    @TableField("PO_UPDATE_TIME")
    private String poUpdateTime;

    @ApiModelProperty(value = "审批流程编号")
    @TableField("PO_FLOW_NO")
    private String poFlowNo;

    @TableField("PO_REQ_TYPE")
    private String poReqType;

    @ApiModelProperty(value = "开单标记")
    @TableField("PO_DN_FLAG")
    private String poDnFlag;

    @ApiModelProperty(value = "开单人")
    @TableField("PO_DN_BY")
    private String poDnBy;

    @ApiModelProperty(value = "开单日期")
    @TableField("PO_DN_DATE")
    private String poDnDate;

    @ApiModelProperty(value = "开单时间")
    @TableField("PO_DN_TIME")
    private String poDnTime;

    @ApiModelProperty(value = "订单来源 0 正常  1 补单  2 委托补单")
    @TableField("PO_SOURCE")
    private String poSource;

    @ApiModelProperty(value = "冷藏_车辆类型")
    @TableField("WM_LC_CLLX")
    private String wmLcCllx;

    @ApiModelProperty(value = "冷藏_保温方式")
    @TableField("WM_LC_BWFS")
    private String wmLcBwfs;

    @ApiModelProperty(value = "冷藏_起运时间")
    @TableField("WM_LC_QYSJ")
    private String wmLcQysj;

    @ApiModelProperty(value = "冷藏_到货时间")
    @TableField("WM_LC_DHSJ")
    private String wmLcDhsj;

    @ApiModelProperty(value = "运输人")
    @TableField("WM_LC_YSR")
    private String wmLcYsr;

    @ApiModelProperty(value = "冷藏_来货件数")
    @TableField("WM_LC_LHJS")
    private String wmLcLhjs;

    @ApiModelProperty(value = "冷藏_随货同行")
    @TableField("WM_LC_SHTX")
    private String wmLcShtx;

    @ApiModelProperty(value = "途中温度达标")
    @TableField("WM_LC_TZWD")
    private String wmLcTzwd;

    @ApiModelProperty(value = "冷藏_汽运温度")
    @TableField("WM_LC_QYWD")
    private String wmLcQywd;

    @ApiModelProperty(value = "冷藏_到货温度")
    @TableField("WM_LC_DHWD")
    private String wmLcDhwd;

    @ApiModelProperty(value = "送货前置期")
    @TableField("PO_LEAD_TIME")
    private String poLeadTime;

    @ApiModelProperty(value = "配送方式")
    @TableField("PO_DELIVERY_MODE")
    private String poDeliveryMode;

    @ApiModelProperty(value = "承运单位")
    @TableField("PO_CARRIER")
    private String poCarrier;

    @ApiModelProperty(value = "运输时间")
    @TableField("PO_DELIVERY_TIMES")
    private String poDeliveryTimes;

    @ApiModelProperty(value = "审核日期")
    @TableField("PO_AUDIT_DATE")
    private String poAuditDate;

    @ApiModelProperty(value = "供应商业务员")
    @TableField("PO_SUPPLIER_SALESMAN")
    private String poSupplierSalesman;

    @ApiModelProperty(value = "转单标记")
    @TableField("PO_SO_FLAG")
    private String poSoFlag;

    @ApiModelProperty(value = "货主")
    @TableField("PO_OWNER")
    private String poOwner;

    @ApiModelProperty(value = "审批人")
    @TableField("PO_APPROVE_BY")
    private String poApproveBy;

    @ApiModelProperty(value = "审批日期")
    @TableField("PO_APPROVE_DATE")
    private String poApproveDate;


}
