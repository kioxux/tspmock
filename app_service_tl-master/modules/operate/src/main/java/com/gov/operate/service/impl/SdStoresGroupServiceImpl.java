package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.SdStoresGroup;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdStoresGroupMapper;
import com.gov.operate.mapper.StoreDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdStoresGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class SdStoresGroupServiceImpl extends ServiceImpl<SdStoresGroupMapper, SdStoresGroup> implements ISdStoresGroupService {

    @Resource
    private CommonService commonService;
    @Resource
    private StoreDataMapper storeDataMapper;

    @Override
    public Result getStoresGroupList() {
        TokenUser user = commonService.getLoginInfo();
        List<SdStoresGroup> list = baseMapper.selectGroupList(user.getClient());
        return ResultUtil.success(list);
    }

    @Override
    public Result getStoresList() {
        TokenUser user = commonService.getLoginInfo();
        List<Map<String, String>> list = storeDataMapper.getStoresList(user.getClient());
        return ResultUtil.success(list);
    }
}
