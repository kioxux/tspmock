package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 采购入库差价单主表
 * </p>
 *
 * @author sy
 * @since 2021-12-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_CHAJIA_Z")
@ApiModel(value="ChajiaZ对象", description="采购入库差价单主表")
public class ChajiaZ extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "差价单号")
    @TableField("CJ_ID")
    private String cjId;

    @ApiModelProperty(value = "地点")
    @TableField("CJ_SITE")
    private String cjSite;

    @ApiModelProperty(value = "供应商编码")
    @TableField("CJ_SUPPLIER_ID")
    private String cjSupplierId;

    @ApiModelProperty(value = "供应商业务员")
    @TableField("CJ_SUPPLIER_SALESMAN")
    private String cjSupplierSalesman;

    @ApiModelProperty(value = "凭证日期")
    @TableField("CJ_DATE")
    private String cjDate;

    @ApiModelProperty(value = "抬头备注")
    @TableField("CJ_HEAD_REMARK")
    private String cjHeadRemark;

    @ApiModelProperty(value = "单据状态  0-待审批，1-已审批，2-已记账")
    @TableField("CJ_STATUS")
    private String cjStatus;

    @ApiModelProperty(value = "创建人")
    @TableField("CJ_CREATE_BY")
    private String cjCreateBy;

    @ApiModelProperty(value = "创建日期")
    @TableField("CJ_CREATE_DATE")
    private String cjCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("CJ_CREATE_TIME")
    private String cjCreateTime;

    @ApiModelProperty(value = "更新人")
    @TableField("CJ_UPDATE_BY")
    private String cjUpdateBy;

    @ApiModelProperty(value = "更新日期")
    @TableField("CJ_UPDATE_DATE")
    private String cjUpdateDate;

    @ApiModelProperty(value = "更新时间")
    @TableField("CJ_UPDATE_TIME")
    private String cjUpdateTime;

    @ApiModelProperty(value = "审核人")
    @TableField("CJ_SH_BY")
    private String cjShBy;

    @ApiModelProperty(value = "审核日期")
    @TableField("CJ_SH_DATE")
    private String cjShDate;

    @ApiModelProperty(value = "审核时间")
    @TableField("CJ_SH_TIME")
    private String cjShTime;

    @ApiModelProperty(value = "类型，明细表正CJ，明细负CJTZ")
    @TableField("CJ_TYPE")
    private String cjType;
}
