package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetPromListVO extends Pageable {

    /**
     * 加盟商
     */
    private String client;
//    /**
//     * 门店
//     */
//    private String gsphBrId;

    /**
     * 活动名称
     */
    private String gsphName;

    /**
     * 状态
     */
    private String gsphStatus;
}
