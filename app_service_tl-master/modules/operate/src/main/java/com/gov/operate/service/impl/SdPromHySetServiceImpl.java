package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromHySet;
import com.gov.operate.mapper.SdPromHySetMapper;
import com.gov.operate.service.ISdPromHySetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Service
public class SdPromHySetServiceImpl extends ServiceImpl<SdPromHySetMapper, SdPromHySet> implements ISdPromHySetService {

}
