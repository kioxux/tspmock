package com.gov.operate.dto.weChat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class TweetsReleaseVO {

    @NotNull(message = "id不能为空")
    private Long id;

    @NotNull(message = "是否发布标识不能为空(0：未操作，1：已发布，2：不发布)")
    private Integer gwthRelease;

    /**
     * 原因
     */
    private String gwthReason;

}
