package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 公司参数表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-06-02
 */
@RestController
@RequestMapping("/operate/cl-system-para")
public class ClSystemParaController {

}

