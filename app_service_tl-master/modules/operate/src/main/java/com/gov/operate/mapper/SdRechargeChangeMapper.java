package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.recharge.RechargeChangeInData;
import com.gov.operate.dto.recharge.RechargeChangeOutData;
import com.gov.operate.entity.SdRechargeChange;
import org.apache.ibatis.annotations.Param;


public interface SdRechargeChangeMapper extends BaseMapper<SdRechargeChange> {
    IPage<RechargeChangeOutData> rechargeChangePage(Page page, @Param("inData") RechargeChangeInData inData);

    void insertOne(SdRechargeChange sdRechargeChange);

}
