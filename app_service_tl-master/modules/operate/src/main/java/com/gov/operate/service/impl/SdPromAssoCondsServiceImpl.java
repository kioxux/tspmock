package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromAssoConds;
import com.gov.operate.mapper.SdPromAssoCondsMapper;
import com.gov.operate.service.ISdPromAssoCondsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Service
public class SdPromAssoCondsServiceImpl extends ServiceImpl<SdPromAssoCondsMapper, SdPromAssoConds> implements ISdPromAssoCondsService {

}
