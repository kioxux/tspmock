package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class GsmProcondiDTO {

    /**
     * proCompclass : [{"component":"成分id","type":"SINGLE","gspvsMode":"促销方式","gspvsModeName":"促销方式描述","gspvsType":"促销类型","gspvsTypeName":"促销类型描述","remarks1":"备注1","remarks2":"备注1","prom":{"gspusVaild":"","gspusQty1":"","gspusPrc1":0,"gspusRebate1":"","gspusQty2":"","gspusPrc2":0,"gspusRebate2":"","gspusQty3":"","gspusPrc3":0,"gspusRebate3":"","gspusMemFlag":"","gspusInteFlag":"","gspusInteRate":"","gspscSeriesId":"","gspscMemFlag":"","gspscInteFlag":"","gspscInteRate":"","gspssSerial":"","gspssSeriesId":"","gspssReachQty1":"","gspssReachAmt1":"","gspssResultAmt1":"","gspssResultRebate1":"","gspgcSeriesId":"","gspgcMemFlag":"","gspgcInteFlag":"","gspgcInteRate":"","gspgsSerial":"","gspgsSeriesProId":"","gspgsReachQty1":"","gspgsReachAmt1":"","gspgsResultQty1":"","gspgrSeriesId":"","gspgrGiftPrc":"","gspgrGiftRebate":"","gspacQty":"","gspacSeriesId":"","gspacMemFlag":"","gspacInteFlag":"","gspacInteRate":"","gsparSeriesId":"","gsparGiftPrc":"","gsparGiftRebate":"","gspasSerial":"","gspasSeriesId":"","gspasAmt":"","gspasRebate":"","gspasQty":""}}]
     * retailPriceBegin :
     * retailPriceEnd :
     * profitsBegin :
     * profitsEnd :
     */

    private String retailPriceBegin;
    private String retailPriceEnd;
    private String profitsBegin;
    private String profitsEnd;

    private List<ProComp> proCompclass;

    @NoArgsConstructor
    @Data
    public static class ProComp {
        /**
         * component : 成分id
         * type : SINGLE
         * gspvsMode : 促销方式
         * gspvsModeName : 促销方式描述
         * gspvsType : 促销类型
         * gspvsTypeName : 促销类型描述
         * remarks1 : 备注1
         * remarks2 : 备注1
         * prom : {"gspusVaild":"","gspusQty1":"","gspusPrc1":0,"gspusRebate1":"","gspusQty2":"","gspusPrc2":0,"gspusRebate2":"","gspusQty3":"","gspusPrc3":0,"gspusRebate3":"","gspusMemFlag":"","gspusInteFlag":"","gspusInteRate":"","gspscSeriesId":"","gspscMemFlag":"","gspscInteFlag":"","gspscInteRate":"","gspssSerial":"","gspssSeriesId":"","gspssReachQty1":"","gspssReachAmt1":"","gspssResultAmt1":"","gspssResultRebate1":"","gspgcSeriesId":"","gspgcMemFlag":"","gspgcInteFlag":"","gspgcInteRate":"","gspgsSerial":"","gspgsSeriesProId":"","gspgsReachQty1":"","gspgsReachAmt1":"","gspgsResultQty1":"","gspgrSeriesId":"","gspgrGiftPrc":"","gspgrGiftRebate":"","gspacQty":"","gspacSeriesId":"","gspacMemFlag":"","gspacInteFlag":"","gspacInteRate":"","gsparSeriesId":"","gsparGiftPrc":"","gsparGiftRebate":"","gspasSerial":"","gspasSeriesId":"","gspasAmt":"","gspasRebate":"","gspasQty":""}
         */
        /**
         * 促销名称
         */
        private String name;
        private String component;
        private String type;
        private String gspvsMode;
        private String gspvsModeName;
        private String gspvsType;
        private String gspvsTypeName;
        private String remarks1;
        private String remarks2;
        private PromBean prom;

        @NoArgsConstructor
        @Data
        public static class PromBean {
            /**
             * gspusVaild :
             * gspusQty1 :
             * gspusPrc1 : 0
             * gspusRebate1 :
             * gspusQty2 :
             * gspusPrc2 : 0
             * gspusRebate2 :
             * gspusQty3 :
             * gspusPrc3 : 0
             * gspusRebate3 :
             * gspusMemFlag :
             * gspusInteFlag :
             * gspusInteRate :
             * gspscSeriesId :
             * gspscMemFlag :
             * gspscInteFlag :
             * gspscInteRate :
             * gspssSerial :
             * gspssSeriesId :
             * gspssReachQty1 :
             * gspssReachAmt1 :
             * gspssResultAmt1 :
             * gspssResultRebate1 :
             * gspgcSeriesId :
             * gspgcMemFlag :
             * gspgcInteFlag :
             * gspgcInteRate :
             * gspgsSerial :
             * gspgsSeriesProId :
             * gspgsReachQty1 :
             * gspgsReachAmt1 :
             * gspgsResultQty1 :
             * gspgrSeriesId :
             * gspgrGiftPrc :
             * gspgrGiftRebate :
             * gspacQty :
             * gspacSeriesId :
             * gspacMemFlag :
             * gspacInteFlag :
             * gspacInteRate :
             * gsparSeriesId :
             * gsparGiftPrc :
             * gsparGiftRebate :
             * gspasSerial :
             * gspasSeriesId :
             * gspasAmt :
             * gspasRebate :
             * gspasQty :
             */

            private String gspusVaild;
            private String gspusQty1;
            private BigDecimal gspusPrc1;
            private String gspusRebate1;
            private String gspusQty2;
            private BigDecimal gspusPrc2;
            private String gspusRebate2;
            private String gspusQty3;
            private BigDecimal gspusPrc3;
            private String gspusRebate3;
            private String gspusMemFlag;
            private String gspusInteFlag;
            private String gspusInteRate;


            private String gspscSeriesId;
            private String gspscMemFlag;
            private String gspscInteFlag;
            private String gspscInteRate;
            private String gspssSerial;
            private String gspssSeriesId;
            private String gspssReachQty1;
            private String gspssReachAmt1;
            private String gspssResultAmt1;
            private String gspssResultRebate1;
            private String gspgcSeriesId;
            private String gspgcMemFlag;
            private String gspgcInteFlag;
            private String gspgcInteRate;
            private String gspgsSerial;
            private String gspgsSeriesProId;
            private String gspgsReachQty1;
            private String gspgsReachAmt1;
            private String gspgsResultQty1;
            private String gspgrSeriesId;
            private String gspgrGiftPrc;
            private String gspgrGiftRebate;


            private String gspacQty;
            private String gspacSeriesId;
            private String gspacMemFlag;
            private String gspacInteFlag;
            private String gspacInteRate;
            private String gsparSeriesId;
            private String gsparGiftPrc;
            private String gsparGiftRebate;
            private String gspasSerial;
            private String gspasSeriesId;
            private String gspasAmt;
            private String gspasRebate;
            private String gspasQty;
            /**
             * 促销类型名
             */
            private String typeName;
            /**
             * 阶梯
             */
            private String gsphPart;
        }
    }
}
