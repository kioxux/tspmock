package com.gov.operate.dto.marketing;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class MarketingAimsTrialVO {

    @Length(min = 8, max = 8, message = "同比日期开始日期格式不正确")
    @NotBlank(message = "同比日期开始不能为空 ")
    private String gsmYearStart;

    @Length(min = 8, max = 8, message = "同比日期结束日期格式不正确")
    @NotBlank(message = "同比日期结束不能为空 ")
    private String gsmYearEnd;

    @NotNull(message = "同比营业额增长倍率不能为空")
    private BigDecimal gsmYearTurnover;

    @NotNull(message = "同比毛利额增长倍率不能为空")
    private BigDecimal gsmYearGrossprofit;

    @NotNull(message = "同比促销品增长倍率不能为空")
    private BigDecimal gsmYearPromotionalpro;

    @Length(min = 8, max = 8, message = "环比日期结束日期格式不正确")
    @NotBlank(message = "环比日期开始不能为空 ")
    private String gsmMonthStart;

    @Length(min = 8, max = 8, message = "环比日期结束日期格式不正确")
    @NotBlank(message = "环比日期结束不能为空 ")
    private String gsmMonthEnd;

    @NotNull(message = "环比营业额增长倍率不能为空")
    private BigDecimal gsmMonthTurnover;

    @NotNull(message = "环比毛利额增长倍率不能为空")
    private BigDecimal gsmMonthGrossprofit;

    @NotNull(message = "环比促销品增长倍率不能为空")
    private BigDecimal gsmMonthPromotionalpro;

    /**
     * 门店列表不能为空
     */
    private List<String> stoList;


}
