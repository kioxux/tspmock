package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SalaryUserKpi;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryUserKpiService extends SuperService<SalaryUserKpi> {

    /**
     * 上月所有员工KPI列表
     */
    List<SalaryUserKpi> getUserKpiList();
}
