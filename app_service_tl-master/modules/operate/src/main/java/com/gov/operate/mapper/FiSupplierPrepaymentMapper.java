package com.gov.operate.mapper;

import com.gov.operate.entity.FiSupplierPrepayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
public interface FiSupplierPrepaymentMapper extends BaseMapper<FiSupplierPrepayment> {

    /**
     * 付款单号
     *
     * @param client
     * @return
     */
    String getClientMaxOrderNo(String client);

    /**
     * 发票关联的预付款单据
     *
     * @param invoiceNumStr
     * @param client
     * @return
     */
    List<FiSupplierPrepayment> selectFiPrePayment(@Param("invoiceNumStr") String invoiceNumStr, @Param("client") String client);
}
