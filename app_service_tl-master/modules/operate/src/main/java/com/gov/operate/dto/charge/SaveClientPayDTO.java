package com.gov.operate.dto.charge;

import com.gov.operate.entity.PayingHeaderNew;
import com.gov.operate.entity.UserChargeMaintenanceM;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SaveClientPayDTO extends PayingHeaderNew {
    private String payType;
    private String invoiceType;
    //优惠金额
    private BigDecimal discountAmount;
    //实收
    private BigDecimal payAmount;
    //应收
    private BigDecimal ReceivableAmount;
    //余额付款
    private BigDecimal chargeAmount;
    private List<SaveClientPricePackDTO> signPackList;
}
