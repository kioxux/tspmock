package com.gov.operate.dto.marketing;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * getGsmSettingList接口入参
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetGsmSettingListVO3 extends Pageable {

    /**
     * 活动开始时间
     */
    private String gsmStartd;

    /**
     * 活动结束时间
     */
    private String gsmEndd;

    /**
     * 活动名称
     */
    private String gsmThename;

    /**
     * 状态(0已下发，1未下发)
     */
    private String gsmRele;

}
