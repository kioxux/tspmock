package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.GaiaBillOfApVO;
import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.dto.SupplierPaymentDetailVO;
import com.gov.operate.dto.invoice.SupplierPrepaymentDto;
import com.gov.operate.entity.*;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.ApprovalInfo;
import com.gov.operate.feign.dto.FeignResult;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISupplierPrepaymentDetailService;
import com.gov.operate.service.ISupplierPrepaymentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-02-01
 */
@Slf4j
@Service
public class SupplierPrepaymentServiceImpl extends ServiceImpl<SupplierPrepaymentMapper, SupplierPrepayment> implements ISupplierPrepaymentService {

    @Resource
    private CommonService commonService;
    @Resource
    private AuthFeign authFeign;
    @Resource
    private SupplierBusinessMapper supplierBusinessMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private DcDataMapper dcDataMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SupplierPrepaymentDetailMapper supplierPrepaymentDetailMapper;
    @Resource
    private ISupplierPrepaymentDetailService supplierPrepaymentDetailService;
    @Resource
    private SupplierPrepaymentMapper supplierPrepaymentMapper;
    @Resource
    private PaymentMapper paymentMapper;
    @Resource
    private BillInvoiceEndMapper billInvoiceEndMapper;
    @Resource
    private FiSupplierPrepaymentMapper fiSupplierPrepaymentMapper;
    @Resource
    private CosUtils cosUtils;

    /**
     * 供应商付款保存
     *
     * @param supplierPrepayment
     * @return
     */
    @Override
    public Result addSupplierPayment(SupplierPrepaymentDto supplierPrepayment) {
        TokenUser user = commonService.getLoginInfo();
        String payOrderId = supplierPrepayment.getPayOrderId();
        if (StringUtils.isBlank(payOrderId)) {
            payOrderId = super.baseMapper.selectPayOrderId(user.getClient());
            // 加盟商
            supplierPrepayment.setClient(user.getClient());
            // 付款申请单号
            supplierPrepayment.setPayOrderId(payOrderId);
            // 创建人
            supplierPrepayment.setFounder(user.getUserId());
            // 状态
            supplierPrepayment.setApplicationStatus(CommonEnum.SupplierPrepaymentApplyStatus.STATUS0.getCode());
            super.baseMapper.insert(supplierPrepayment);
            return ResultUtil.success(supplierPrepayment);
        } else {
            QueryWrapper<SupplierPrepayment> supplierPrepaymentQueryWrapper = new QueryWrapper<>();
            supplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
            supplierPrepaymentQueryWrapper.eq("PAY_ORDER_ID", payOrderId);
            SupplierPrepayment entity = super.baseMapper.selectOne(supplierPrepaymentQueryWrapper);
            if (entity == null) {
                throw new CustomResultException("申请单号不存在！");
            }
            if (!CommonEnum.SupplierPrepaymentApplyStatus.STATUS0.getCode().equals(entity.getApplicationStatus())) {
                throw new CustomResultException("当前单号已申请！");
            }
            // 更新申请
            super.baseMapper.update(supplierPrepayment, supplierPrepaymentQueryWrapper);
            supplierPrepayment.setClient(user.getClient());
            return ResultUtil.success(supplierPrepayment);
        }
    }

    /**
     * 供应商付款申请
     *
     * @param supplierPrepayment
     * @return
     */
    @Override
    public Result applySupplierPayment(SupplierPrepaymentDto supplierPrepayment) {
        TokenUser user = commonService.getLoginInfo();
        String payOrderId = supplierPrepayment.getPayOrderId();
        // 已保存
        if (StringUtils.isNotBlank(payOrderId)) {
            QueryWrapper<SupplierPrepayment> supplierPrepaymentQueryWrapper = new QueryWrapper<>();
            supplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
            supplierPrepaymentQueryWrapper.eq("PAY_ORDER_ID", payOrderId);
            SupplierPrepayment entity = super.baseMapper.selectOne(supplierPrepaymentQueryWrapper);
            if (entity == null) {
                throw new CustomResultException("申请单号不存在！");
            }
            if (!CommonEnum.SupplierPrepaymentApplyStatus.STATUS0.getCode().equals(entity.getApplicationStatus())) {
                throw new CustomResultException("当前单号已申请！");
            }
            // 状态
            supplierPrepayment.setApplicationStatus(CommonEnum.SupplierPrepaymentApplyStatus.STATUS1.getCode());
            // 工作流编码
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
            supplierPrepayment.setApprovalFlowNo(flowNo);
            // 申请日期
            supplierPrepayment.setGspDate(DateUtils.getCurrentDateTimeStrFull());
            // 更新申请
            super.baseMapper.update(supplierPrepayment, supplierPrepaymentQueryWrapper);
        } else { // 未保存
            payOrderId = super.baseMapper.selectPayOrderId(user.getClient());
            // 加盟商
            supplierPrepayment.setClient(user.getClient());
            // 付款申请单号
            supplierPrepayment.setPayOrderId(payOrderId);
            // 创建人
            supplierPrepayment.setFounder(user.getUserId());
            // 状态
            supplierPrepayment.setApplicationStatus(CommonEnum.SupplierPrepaymentApplyStatus.STATUS1.getCode());
            // 工作流编码
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
            supplierPrepayment.setApprovalFlowNo(flowNo);
            // 申请日期
            supplierPrepayment.setGspDate(DateUtils.getCurrentDateTimeStrFull());
            super.baseMapper.insert(supplierPrepayment);
        }
        // 工作流
        createWorkflow(supplierPrepayment);
        supplierPrepayment.setClient(user.getClient());
        return ResultUtil.success(supplierPrepayment);
    }

    /**
     * 供应商付款列表查询
     *
     * @param pageSize
     * @param pageNum
     * @param gspApplyDateStart
     * @param gspApplyDateEnd
     * @param payCompanyCode
     * @param supCode
     * @param payOrderId
     * @param applicationPaymentAmountStart
     * @param applicationPaymentAmountEnd
     * @param founder
     * @return
     */
    @Override
    public Result getSupplierPaymentList(Integer pageSize, Integer pageNum, String gspApplyDateStart, String gspApplyDateEnd, String payCompanyCode,
                                         String supCode, String payOrderId, BigDecimal applicationPaymentAmountStart, BigDecimal applicationPaymentAmountEnd,
                                         String founder, String applicationStatus, String gspApprovalDateStart, String gspApprovalDateEnd, String supSalesman,
                                         String approvalRemark, String wfDescription) {
        TokenUser user = commonService.getLoginInfo();
        Page<SupplierPrepaymentDto> page = new Page<>(pageNum, pageSize);
        IPage<SupplierPrepaymentDto> iPage = super.baseMapper.getSupplierPaymentList(page, user.getClient(), gspApplyDateStart, gspApplyDateEnd,
                payCompanyCode, supCode, payOrderId, applicationPaymentAmountStart, applicationPaymentAmountEnd, founder,
                applicationStatus, gspApprovalDateStart, gspApprovalDateEnd, supSalesman, approvalRemark, wfDescription);
        iPage.getRecords().forEach(s -> s.setDiffAmount(s.getApplicationPaymentAmount().subtract(s.getWriteOffAmount())));
        return ResultUtil.success(iPage);
    }

    /**
     * 机构列表
     *
     * @return
     */
    @Override
    public Result getCompanyList() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = super.baseMapper.getCompanyList(user.getClient());
        return ResultUtil.success(list);
    }

    @Override
    public Result getDcList() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = super.baseMapper.getDcList(user.getClient());
        return ResultUtil.success(list);
    }

    @Override
    @Transactional
    public Result supplierPaymentWriteOff(SupplierPrepaymentDto supplierPrepaymentDto) {
        if (CollectionUtils.isNotEmpty(supplierPrepaymentDto.getPoList())) {
            List<SelectWarehousingDTO> poList = supplierPrepaymentDto.getPoList();
            List<String> poIds = new ArrayList<>();
            List<SupplierPrepaymentDetail> detailList = new ArrayList<>();
            for (SelectWarehousingDTO getSupplierPaymentDetailVO : poList) {
                SupplierPrepaymentDetail detail = new SupplierPrepaymentDetail();
                BeanUtils.copyProperties(getSupplierPaymentDetailVO, detail);
                BeanUtils.copyProperties(supplierPrepaymentDto, detail);
                detail.setPoAmt(getSupplierPaymentDetailVO.getTotalAmount());
                poIds.add(getSupplierPaymentDetailVO.getMatDnId());
                detail.setGspdCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                detailList.add(detail);
            }
            supplierPrepaymentDetailMapper.delete(new QueryWrapper<SupplierPrepaymentDetail>()
                    .eq("PAY_ORDER_ID", supplierPrepaymentDto.getPayOrderId())
                    .in("MAT_DN_ID", poIds));
            supplierPrepaymentDetailService.saveBatch(detailList);
        } else {
            throw new CustomResultException("请选择单据");
        }
        return ResultUtil.success("核销成功");
    }

    @Override
    @Transactional
    public Result deleteWriteOff(List<Integer> idList) {
        supplierPrepaymentDetailMapper.delete(new QueryWrapper<SupplierPrepaymentDetail>()
                .in("PAY_ORDER_DETAIL_ID", idList));
        return ResultUtil.success();
    }

    @Override
    public Result getWriteOffList(Integer pageSize, Integer pageNum, String gspApplyDateStart, String gspApplyDateEnd, String orderCode, String supSelfCode,
                                  String payCompanyCode, String type, String invoiceSalesman) {
        Page<SelectWarehousingDTO> page = new Page<>(pageNum, pageSize);
        TokenUser user = commonService.getLoginInfo();
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = supplierPrepaymentMapper.getSupplierPaymentDetail(page,
                user.getClient(), gspApplyDateStart, gspApplyDateEnd, payCompanyCode, orderCode, supSelfCode, type, invoiceSalesman);
        for (SelectWarehousingDTO item : selectWarehousingDTOS.getRecords()) {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
            }
            item.setDisabled("0");
            // 发票付款判断
            String invoice = paymentMapper.getinvoiceNumListByBill(item.getMatDnId(), user.getClient());
            if (StringUtils.isNotBlank(invoice)) {
                // 发票付款记录
                List<PaymentVatApplications> paymentVatApplicationsList = paymentMapper.selectInvoicePayList(invoice, user.getClient());
                if (CollectionUtils.isNotEmpty(paymentVatApplicationsList)) {
                    item.setDisabled("1");
                    continue;
                }
            }
            QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
            billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
            billInvoiceEndQueryWrapper.eq("GBIE_NUM", item.getMatDnId());
            billInvoiceEndQueryWrapper.eq("GBIE_BILL_TYPE", item.getType());
            List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
            if (CollectionUtils.isNotEmpty(billInvoiceEndList)) {
                item.setDisabled("1");
                continue;
            }
            if (CollectionUtils.isNotEmpty(fiSupplierPrepaymentMapper.selectList(
                    new QueryWrapper<FiSupplierPrepayment>().eq("CLIENT", user.getClient()).eq("MAT_DN_ID", item.getMatDnId())))) {
                item.setDisabled("1");
                continue;
            }
        }
        return ResultUtil.success(selectWarehousingDTOS);
    }

    @Override
    public Result viewWriteOffList(Integer pageSize, Integer pageNum, String payOrderId) {
        Page<SupplierPaymentDetailVO> page = new Page<>(pageNum, pageSize);
        TokenUser user = commonService.getLoginInfo();
        IPage<SupplierPaymentDetailVO> vos = supplierPrepaymentMapper.viewWriteOffList(page, user.getClient(), payOrderId);
        vos.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
            }
        });
        return ResultUtil.success(vos);
    }

    /**
     * 导出
     *
     * @param gspApplyDateStart
     * @param gspApplyDateEnd
     * @param payCompanyCode
     * @param supCode
     * @param payOrderId
     * @param applicationPaymentAmountStart
     * @param applicationPaymentAmountEnd
     * @param founder
     * @param applicationStatus
     * @param gspApprovalDateStart
     * @param gspApprovalDateEnd
     * @return
     */
    @Override
    public Result getSupplierPaymentExport(String gspApplyDateStart, String gspApplyDateEnd, String payCompanyCode, String supCode, String payOrderId,
                                           BigDecimal applicationPaymentAmountStart, BigDecimal applicationPaymentAmountEnd, String founder,
                                           String applicationStatus, String gspApprovalDateStart, String gspApprovalDateEnd, String supSalesman,
                                           String approvalRemark, String wfDescription) throws IOException {


        TokenUser user = commonService.getLoginInfo();
        Page<SupplierPrepaymentDto> page = new Page<>(-1, -1);
        IPage<SupplierPrepaymentDto> iPage = super.baseMapper.getSupplierPaymentList(page, user.getClient(), gspApplyDateStart, gspApplyDateEnd,
                payCompanyCode, supCode, payOrderId, applicationPaymentAmountStart, applicationPaymentAmountEnd, founder,
                applicationStatus, gspApprovalDateStart, gspApprovalDateEnd, supSalesman, approvalRemark, wfDescription);
        List<List<Object>> dataList = new ArrayList<>();
        iPage.getRecords().forEach(s -> {
            s.setDiffAmount(s.getApplicationPaymentAmount().subtract(s.getWriteOffAmount()));
            List<Object> data = new ArrayList<>();
            //  申请日期
            data.add(s.getGspApplyDate());
            //  业务机构
            data.add(StringUtils.parse("{}-{}",
                    StringUtils.isBlank(s.getPayCompanyCode()) ? "" : s.getPayCompanyCode(), StringUtils.isBlank(s.getCompanyName()) ? "" : s.getCompanyName()));
            //  供应商
            data.add(StringUtils.parse("{}-{}",
                    StringUtils.isBlank(s.getSupCode()) ? "" : s.getSupCode(), StringUtils.isBlank(s.getSupName()) ? "" : s.getSupName()));
            // 业务员
            data.add(s.getGssName());
            //  预付单号
            data.add(s.getPayOrderId());
            //  预付金额
            data.add(s.getApplicationPaymentAmount());
            //  核销金额
            data.add(s.getWriteOffAmount());
            //  差异
            data.add(s.getDiffAmount());
            //  采购员
            data.add(s.getUserNam());
            //  审批完成日期
            data.add(s.getGspApprovalDate());
            // 备注
            data.add(s.getApprovalRemark());
            // 工作流描述
            data.add(s.getWfDescription());
            dataList.add(data);
        });
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("供应商预付");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;

    }

    /**
     * 备注下拉框
     *
     * @return
     */
    @Override
    public Result getApprovalRemarkList() {
        TokenUser user = commonService.getLoginInfo();
        List<String> list = super.baseMapper.getApprovalRemarkList(user.getClient());
        return ResultUtil.success(list);
    }

    @Override
    public Result getCustomerList(String matSiteCode) {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = super.baseMapper.getCustomerList(user.getClient(), matSiteCode);
        return ResultUtil.success(list);
    }

    /**
     * 供应商付款审批回调
     *
     * @param info
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public FeignResult payCallBack(ApprovalInfo info) {
        try {
            FeignResult result = new FeignResult();
            QueryWrapper<SupplierPrepayment> supplierPrepaymentQueryWrapper = new QueryWrapper<>();
            supplierPrepaymentQueryWrapper.eq("CLIENT", info.getClientId());
            supplierPrepaymentQueryWrapper.eq("APPROVAL_FLOW_NO", info.getWfOrder());
            List<SupplierPrepayment> list = super.baseMapper.selectList(supplierPrepaymentQueryWrapper);
            for (SupplierPrepayment supplierPrepayment : list) {
                if (!CommonEnum.SupplierPrepaymentApplyStatus.STATUS1.getCode().equals(supplierPrepayment.getApplicationStatus())) {
                    continue;
                }
                // 审批状态 = 审批通过
                if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
                    supplierPrepayment.setApplicationStatus(CommonEnum.SupplierPrepaymentApplyStatus.STATUS2.getCode());
                } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
                    supplierPrepayment.setApplicationStatus(CommonEnum.SupplierPrepaymentApplyStatus.STATUS3.getCode());
                }
                QueryWrapper<SupplierPrepayment> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("CLIENT", supplierPrepayment.getClient());
                queryWrapper.eq("PAY_COMPANY_CODE", supplierPrepayment.getPayCompanyCode());
                queryWrapper.eq("PAY_ORDER_ID", supplierPrepayment.getPayOrderId());
                queryWrapper.eq("SUP_CODE", supplierPrepayment.getSupCode());
                supplierPrepayment.setGspApprovalDate(DateUtils.getCurrentDateStrYYMMDD());
                super.baseMapper.update(supplierPrepayment, queryWrapper);
                //已付款 供应商应付表插入数据 GAIA_BILL_OF_AP
               /* if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
                    insertBillOfAp(supplierPrepayment);
                }*/
            }
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            FeignResult result = new FeignResult();
            result.setCode(9999);
            result.setMessage("系统异常");
            return result;
        }
    }

    /**
     * 供应商预付
     *
     * @param supplierPrepayment
     */
    private void insertBillOfAp(SupplierPrepayment supplierPrepayment) {
        try {
            //判断供应商是否存在期初
            Integer count = this.supplierBusinessMapper.getBeginningOfPeriod(supplierPrepayment.getClient(), supplierPrepayment.getSupCode(), supplierPrepayment.getPayCompanyCode());
            if (Objects.nonNull(count) && count > 0) {
                GaiaBillOfApVO billOfApVO = new GaiaBillOfApVO();
                billOfApVO.setClient(supplierPrepayment.getClient());
                billOfApVO.setDcCode(supplierPrepayment.getPayCompanyCode());
                billOfApVO.setPaymentId("6666");
                billOfApVO.setAmountOfPayment(BigDecimal.ZERO.subtract(supplierPrepayment.getApplicationPaymentAmount()));
                String nowDate = DateUtil.format(DateUtil.parse(DateUtil.today(), "yyyy-MM-dd"), "yyyyMMdd");
                billOfApVO.setPaymentDate(nowDate);
                billOfApVO.setAmountOfTax(BigDecimal.ZERO);
                billOfApVO.setAmountExcludingTax(BigDecimal.ZERO);
                billOfApVO.setSupSelfCode(supplierPrepayment.getSupCode());

                //获取供应商名称
                SupplierBusiness business = this.supplierBusinessMapper.selectSupplier(supplierPrepayment.getClient(), supplierPrepayment.getPayCompanyCode(), supplierPrepayment.getSupCode());
                billOfApVO.setSupName(business.getSupName());
                this.supplierBusinessMapper.insertBillOfAp(billOfApVO);
                //修改供应商主业务数据金额
                updateSupplierBusinessAmt(supplierPrepayment.getClient(), supplierPrepayment.getSupCode(), supplierPrepayment.getPayCompanyCode(), nowDate);
            }
        } catch (Exception e) {
            // e.printStackTrace();
            log.info(String.format("<供应商预付><新增供应商应付清单><报错：%s>", e.getMessage()));
        }
    }

    /**
     * 修改供应商主业务数据金额
     *
     * @param client   加盟商
     * @param supCode  供应商
     * @param siteCode 地点
     * @param date     时间
     */
    private void updateSupplierBusinessAmt(String client, String supCode, String siteCode, String date) {
        List<GaiaBillOfApVO> billOfApVOList = this.supplierBusinessMapper.getAmt(client, supCode, siteCode, date);
        //期初是否存在
        if (CollUtil.isNotEmpty(billOfApVOList)) {
            GaiaBillOfApVO ofApVO = billOfApVOList.get(0);
            List<GaiaBillOfApVO> billOfList = this.supplierBusinessMapper.getBillOfAp(client, supCode, siteCode, date);
            //为空说明期初在时间后面
            if (CollUtil.isEmpty(billOfList)) {
                //修改
                this.supplierBusinessMapper.updateSupplierBusiness(client, supCode, siteCode, ofApVO.getAmountOfPayment());
            } else {
                //获取本月第一天
                //String firstDay = DateUtils.getTheFirstDayOfTheMonth();
                //昨天
                String yestDay = DateUtil.format(DateUtil.parse(DateUtil.yesterday().toString(), "yyyy-MM-dd"), "yyyyMMdd");
                GaiaBillOfApVO billOf = this.supplierBusinessMapper.getBillOf(client, supCode, siteCode, yestDay);
                BigDecimal amountOfPayment = BigDecimal.ZERO;
                if (Objects.nonNull(billOf)) {
                    amountOfPayment = billOfList.stream().map(GaiaBillOfApVO::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add).add(billOf.getAmountOfPayment());
                } else {
                    amountOfPayment = billOfList.stream().map(GaiaBillOfApVO::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
                }
                this.supplierBusinessMapper.updateSupplierBusiness(client, supCode, siteCode, amountOfPayment);
            }
        }
    }

    /**
     * 创建工作流
     */
    public void createWorkflow(SupplierPrepayment supplierPrepayment) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        // 人员
        TokenUser user = commonService.getLoginInfo();
        // 供应商
        QueryWrapper<SupplierBusiness> supplierBusinessQueryWrapper = new QueryWrapper<>();
        supplierBusinessQueryWrapper.eq("CLIENT", user.getClient());
        supplierBusinessQueryWrapper.eq("SUP_SITE", supplierPrepayment.getPayCompanyCode());
        supplierBusinessQueryWrapper.eq("SUP_SELF_CODE", supplierPrepayment.getSupCode());
        SupplierBusiness supplierBusiness = supplierBusinessMapper.selectOne(supplierBusinessQueryWrapper);
        param.put("token", token);  // token
        param.put("wfDefineCode", "GAIA_WF_043");  // 类型
        // SupCode + '-' + SupName + '(' + ApplicationPaymentAmount + '元)'
        if (supplierBusiness != null) {
            param.put("wfTitle",
                    supplierBusiness.getSupSelfCode() + "-" + supplierBusiness.getSupName()
                            + "(" + supplierPrepayment.getApplicationPaymentAmount().setScale(2, BigDecimal.ROUND_HALF_UP) + "元)"
            );   // 标题
        } else {
            param.put("wfTitle", "供应商预付");   // 标题
        }
        // wfDescription用gpaRemarks字段覆盖。
        param.put("wfDescription", supplierPrepayment.getApprovalRemark());  // 描述
        param.put("wfOrder", supplierPrepayment.getApprovalFlowNo());   // 业务单号

        List<SupplierPrepaymentDto> list = new ArrayList<>();
        // 工作流行
        SupplierPrepaymentDto supplierPrepaymentDto = new SupplierPrepaymentDto();
        BeanUtils.copyProperties(supplierPrepayment, supplierPrepaymentDto);
        // 供应商
        if (supplierBusiness != null) {
            supplierPrepaymentDto.setSupName(supplierBusiness.getSupName());
        }
        // 地点
        QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<>();
        storeDataQueryWrapper.eq("CLIENT", user.getClient());
        storeDataQueryWrapper.eq("STO_CODE", supplierPrepayment.getPayCompanyCode());
        StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
        if (storeData != null) {
            supplierPrepaymentDto.setCompanyName(storeData.getStoName());
        } else {
            QueryWrapper<DcData> dcDataQueryWrapper = new QueryWrapper();
            dcDataQueryWrapper.eq("CLIENT", user.getClient());
            dcDataQueryWrapper.eq("DC_CODE", supplierPrepayment.getPayCompanyCode());
            DcData dcData = dcDataMapper.selectOne(dcDataQueryWrapper);
            if (dcData != null) {
                supplierPrepaymentDto.setCompanyName(dcData.getDcName());
            }
        }
        // 序号
        supplierPrepaymentDto.setIndex(1);
        QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
        userDataQueryWrapper.eq("CLIENT", user.getClient());
        userDataQueryWrapper.eq("USER_ID", supplierPrepayment.getFounder());
        UserData userData = userDataMapper.selectOne(userDataQueryWrapper);
        if (userData != null) {
            supplierPrepaymentDto.setUserNam(userData.getUserNam());
        }
        // 银行代码
        supplierPrepaymentDto.setGspSupBankCode(supplierPrepayment.getGspSupBankCode());

        // 添加到集合中
        list.add(supplierPrepaymentDto);
        param.put("wfDetail", list); // 明细
        param.put("wfSite", supplierPrepayment.getPayCompanyCode()); // 门店
        log.info("供应商付款！ 提交参数：{}", JsonUtils.beanToJson(param));
        JsonResult result = authFeign.createWorkflow(param);
        log.info("供应商付款！ 返回结果：{}", JsonUtils.beanToJson(result));
        if (result.getCode().intValue() != 0) {
            throw new CustomResultException(result.getMessage());
        }
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "申请日期",
            "业务机构",
            "供应商",
            "业务员",
            "预付单号",
            "预付金额",
            "核销金额",
            "差异",
            "采购员",
            "审批完成日期",
            "备注",
            "工作流描述",
    };
}
