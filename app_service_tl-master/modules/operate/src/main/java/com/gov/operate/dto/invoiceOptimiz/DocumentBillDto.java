package com.gov.operate.dto.invoiceOptimiz;

import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.dto.SelectWarehousingDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.17
 */
@Data
public class DocumentBillDto {

    /**
     * 收货金额
     */
    private BigDecimal gdbTotalAmt;

    /**
     * 折扣
     */
    private BigDecimal gdbRebate;

    /**
     * 应付金额
     */
    private BigDecimal gdbPayAmt;

    /**
     * 差异
     */
    private BigDecimal gdbDiff;

    /**
     * 原因
     */
    private String gdbReason;

    /**
     * 单据列表
     */
    private List<SelectWarehousingDTO> selectWarehousingDTOList;

    /**
     * 登记发票集合
     */
    private List<FicoInvoiceInformationRegistrationDTO> ficoInvoiceInformationRegistrationDTOList;

    /**
     * 收货金额
     */
    private BigDecimal gibDocTotalAmt;
    /**
     * 发票金额
     */
    private BigDecimal gibInvoiceTotalAmt;
    /**
     * 折扣
     */
    private BigDecimal gibRebate;
    /**
     * 应付金额
     */
    private BigDecimal gibPayAmt;
    /**
     * 付款差异
     */
    private BigDecimal gibDocDiff;
    /**
     * 发票差异
     */
    private BigDecimal gibInvoiceDiff;
    /**
     * 原因
     */
    private String gibReason;


}

