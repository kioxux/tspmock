package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.UserPaymentMaintenance;
import com.gov.operate.entity.FiUserPaymentMaintenance;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-01
 */
public interface IFiUserPaymentMaintenanceService extends SuperService<FiUserPaymentMaintenance> {

    /**
     * 获取付款维护数据
     *
     * @param client
     * @param ficoPayingstoreCode
     * @param ficoPayingstoreName
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getPaymentMaintenance(String client, String ficoPayingstoreCode, String ficoPayingstoreName, Integer pageNum, Integer pageSize);

    /**
     * 保存付款维护数据
     *
     * @param list
     */
    void savePaymentMaintenance(List<UserPaymentMaintenance> list);
}
