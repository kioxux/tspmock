package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 加盟商权限表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
@RestController
@RequestMapping("/operate/authority-auths-client")
public class AuthorityAuthsClientController {

}

