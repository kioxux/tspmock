package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.UserDateVO;
import com.gov.operate.entity.AuthorityUserRoles;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface AuthorityUserRolesMapper extends BaseMapper<AuthorityUserRoles> {
    List<UserDateVO> selectUserList(Map<Object, Object> map);

    List<UserDateVO> selectNoUserList(Map<Object, Object> map);
}
