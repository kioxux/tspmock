package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SelectPrepaidBillDetailVO;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.entity.DtSupplierPrepayment;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-09
 */
public interface DtSupplierPrepaymentMapper extends BaseMapper<DtSupplierPrepayment> {

    IPage<SelectPrepaidBillDetailVO> selectPrepaidBillDetail(Page page, @Param("vo") SelectWarehousingVO vo);
}
