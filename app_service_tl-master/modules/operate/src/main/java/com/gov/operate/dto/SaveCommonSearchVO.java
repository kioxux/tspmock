package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class SaveCommonSearchVO {


    @NotBlank(message = "查询显示名不能为空")
    private String gsmsName;

    @NotBlank(message = "查询明细不能为空")
    private String gsmsDetail;

//    @NotBlank(message = "门店不能为空")
    private String gsmsStore;

    @NotBlank(message = "是否常用不能为空 0，常用查询，1,非常用查询")
    private String gsmsNormal;

    @NotNull(message = "查询类型不能为空 1:短信，2：电话")
    private Integer gsmsType;

}
