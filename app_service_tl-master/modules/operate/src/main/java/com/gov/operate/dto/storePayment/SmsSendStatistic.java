package com.gov.operate.dto.storePayment;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.entity.SmsRechargeRecord;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.17
 */
@Data
public class SmsSendStatistic extends SmsRechargeRecord {

    /**
     * 加盟商名
     */
    private String francName;

    /**
     * 剩余件数
     */
    private Integer surplus;

    private IPage<SmsSendStatistic> iPage;
}
