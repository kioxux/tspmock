package com.gov.operate.service.dataImport.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.entity.dataImport.ImportDto;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.operate.dto.MaterialImportVO;
import com.gov.operate.dto.dataImport.materialDoc.InsertItemVO;
import com.gov.operate.dto.dataImport.materialDoc.SelectImportDTO;
import com.gov.operate.entity.MaterialDocImp;
import com.gov.operate.mapper.MaterialImportMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.dataImport.BusinessImportService;
import com.gov.operate.service.dataImport.IMaterialImportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class MaterialImportServiceImpl extends ServiceImpl<MaterialImportMapper, MaterialDocImp> implements IMaterialImportService {

    @Resource
    private MaterialImportMapper materialImportMapper;
    @Resource
    private CommonService commonService;
    @Resource
    BusinessImportService businessImportService;
    @Resource
    IMaterialImportService materialImportService;

    /**
     * 履历列表查询
     */
    @Override
    public void insertItem(InsertItemVO vo) {
        MaterialDocImp materialDocImp = new MaterialDocImp();
        BeanUtils.copyProperties(vo, materialDocImp);
        materialDocImp.setGmdiStatus(OperateEnum.GmdiStatus.NO_IMPORT.getCode());
        materialDocImp.setGmdiCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        materialDocImp.setGmdiCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        materialDocImp.setGmdiCreateUser(commonService.getLoginInfo().getUserId());
        materialImportMapper.insert(materialDocImp);
    }

    /**
     * 新增履历
     */
    @Override
    public IPage<SelectImportDTO> selectImport(MaterialImportVO materialImportVO) {
        Page<SelectImportDTO> page = new Page<>(materialImportVO.getPageNum(), materialImportVO.getPageSize());
        return materialImportMapper.selectItem(page, materialImportVO);
    }

    /**
     * 删除
     */
    @Override
    public void deleteMaterialItem(Integer id) {
        if (ObjectUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        MaterialDocImp docImp = this.getById(id);
        if (ObjectUtils.isEmpty(docImp)) {
            throw new CustomResultException("该数据不存在或已经被删除");
        }
        materialImportMapper.deleteById(id);
    }

    /**
     * 导入操作
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void executeImport(Integer id) {
        String userId = commonService.getLoginInfo().getUserId();
        MaterialDocImp materialDocImpExit = materialImportMapper.selectById(id);
        if (ObjectUtils.isEmpty(materialDocImpExit)) {
            throw new CustomResultException("该数据不存在或已经被删除");
        }
        if (!OperateEnum.GmdiStatus.NO_IMPORT.getCode().equals(materialDocImpExit.getGmdiStatus())) {
            throw new CustomResultException("这条数据已经被操作");
        }
        // 更新状态为‘执行中’
        materialImportService.updateStatusToExecuting(id, userId);
        // 导入数据
        MaterialDocImp materialDocImpDO = new MaterialDocImp();
        try {
            materialDocImpDO = materialImportService.importDataByNormalType(materialDocImpExit, CommonEnum.BusinessImportTypet.MaterialDoc.getCode());
        } catch (CustomResultException e) {
            materialDocImpDO.setId(id);
            materialDocImpDO.setGmdiStatus(OperateEnum.GmdiStatus.FAIL.getCode());
            materialDocImpDO.setGmdiReason(e.getMessage());
            log.info("{}", e.getMessage());
        } catch (Exception e) {
            materialDocImpDO.setId(id);
            materialDocImpDO.setGmdiStatus(OperateEnum.GmdiStatus.FAIL.getCode());
            materialDocImpDO.setGmdiReason("系统异常");
            log.info("系统异常: {}", e.getMessage());
        }
        // 更新状态为 ‘成功/失败’
        materialImportMapper.updateById(materialDocImpDO);
    }

    /**
     * 更新状态为‘执行中’
     *
     * @param id     履历
     * @param userId 当前用户id
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void updateStatusToExecuting(Integer id, String userId) {
        MaterialDocImp materialDocImpDO = new MaterialDocImp();
        materialDocImpDO.setId(id);
        materialDocImpDO.setGmdiExeUser(userId);
        materialDocImpDO.setGmdiExeDate(DateUtils.getCurrentDateStrYYMMDD());
        materialDocImpDO.setGmdiExeTime(DateUtils.getCurrentTimeStrHHMMSS());
        materialDocImpDO.setGmdiStatus(OperateEnum.GmdiStatus.EXECUTING.getCode());
        materialImportMapper.updateById(materialDocImpDO);
    }

    /**
     * 导入
     *
     * @param materialDocImpExit 上传的文件路径
     * @param gmdiType           导入类型
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public MaterialDocImp importDataByNormalType(MaterialDocImp materialDocImpExit, String gmdiType) {
        MaterialDocImp materialDocImpDO = new MaterialDocImp();
        materialDocImpDO.setId(materialDocImpExit.getId());
        // 导入参数
        ImportDto importParam = new ImportDto();
        importParam.setPath(materialDocImpExit.getGmdiPath());
        importParam.setType(gmdiType);
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("gmdiType", materialDocImpExit.getGmdiType());
        importParam.setDataMap(dataMap);
        Result result = businessImportService.businessImport(importParam);
        if (ResultEnum.SUCCESS.getCode().equals(result.getCode())) {
            Map<String, Object> successData = (Map<String, Object>) (result.getData());
            materialDocImpDO.setGmdiStatus(OperateEnum.GmdiStatus.SUCCESS.getCode());
            materialDocImpDO.setGmdiCount((Integer) successData.get("gmdiCount"));
            materialDocImpDO.setGmdiAmt((BigDecimal) successData.get("gmdiQty"));
            materialDocImpDO.setGmdiQty((BigDecimal) successData.get("gmdiAmt"));
        } else {
            List<String> errorMessageList = (List<String>) (result.getData());
            materialDocImpDO.setGmdiStatus(OperateEnum.GmdiStatus.FAIL.getCode());
            materialDocImpDO.setGmdiReason(String.join("\r\n", errorMessageList));
        }
        return materialDocImpDO;
    }

}
