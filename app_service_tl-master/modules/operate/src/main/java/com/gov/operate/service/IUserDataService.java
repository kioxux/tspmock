package com.gov.operate.service;

import com.gov.operate.entity.UserData;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-02
 */
public interface IUserDataService extends SuperService<UserData> {
    /**
     * 根据加盟商和员工编号获取用户
     */
    UserData getUserByClientAndUserId(String client, String userId);
    /**
     * 根据加盟商和员工手机号获取用户
     */
    UserData getUserByClientAndPhone(String client, String phone);

    /**
     * 获取userID
     * @return
     */
    int selectMaxId(String client);

    void insert(UserData userData);
    //修改店员部门
    void editDepById(String depId, String depName, String userId);
}
