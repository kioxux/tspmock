package com.gov.operate.dto.storeData;

import lombok.Data;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:40 2021/11/22
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class EditStoreDataDto extends StoreDataEditVO{
    /**
     * 修改日期 yyyyMMdd
     */
    private String stoModiDate;
    /**
     * 修改时间 HHmmss
     */
    private String stoModiTime;

    /**
     * 修改人账号
     */
    private String stoModiId;
    /**
     * 关联门店
     */
    private String stoRelationStore;
}
