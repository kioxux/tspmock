package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SspMenuChain;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * @Entity generator.domain.SspMenuCompadm
 */
public interface SspMenuChainMapper extends BaseMapper<SspMenuChain> {

    int deleteChainHead(@Param("userId") Long userId, @Param("client") String client, @Param("keySet") Set<String> keySet);
}




