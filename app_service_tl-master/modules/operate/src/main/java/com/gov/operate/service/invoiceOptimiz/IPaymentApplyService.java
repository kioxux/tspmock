package com.gov.operate.service.invoiceOptimiz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.PaymentApplyVO;
import com.gov.operate.dto.SelectPaymentApplyDTO;
import com.gov.operate.dto.SelectPaymentApplyVO;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDetailsDTO;
import com.gov.operate.dto.invoiceOptimiz.InvoiceBillDetailsDTO;
import com.gov.operate.entity.PaymentApply;
import com.gov.mybatis.SuperService;
import com.gov.operate.feign.dto.ApprovalInfo;
import com.gov.operate.feign.dto.FeignResult;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhangp
 * @since 2021-03-30
 */

public interface IPaymentApplyService extends SuperService<PaymentApply> {

    Result addApply(List<PaymentApplyVO> list);

    public IPage<SelectPaymentApplyDTO> selectPaymentApply(SelectPaymentApplyVO vo);

    /**
     * 单据对账详情
     *
     * @param id 对账单号
     * @return
     */
    DocumentBillDetailsDTO getDocumentBill(String id);

    /**
     *
     * @param id 对账单号
     * @return
     */
    InvoiceBillDetailsDTO getInvoiceBill(String id);

    /**
     * 付款审批
     * @param info
     * @return
     */
    FeignResult approvePayment(ApprovalInfo info);

}
