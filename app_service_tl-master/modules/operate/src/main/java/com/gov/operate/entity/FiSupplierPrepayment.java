package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FI_SUPPLIER_PREPAYMENT")
@ApiModel(value="FiSupplierPrepayment对象", description="")
public class FiSupplierPrepayment extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款单号")
    @TableField("PAYMENT_ORDER_NO")
    private String paymentOrderNo;

    @ApiModelProperty(value = "付款单据日期")
    @TableField("PAYMENT_ORDER_DATE")
    private String paymentOrderDate;

    @ApiModelProperty(value = "业务单号")
    @TableField("MAT_DN_ID")
    private String matDnId;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "本次付款金额")
    @TableField("INVOICE_AMOUNT_OF_THIS_PAYMENT")
    private BigDecimal invoiceAmountOfThisPayment;

    @ApiModelProperty(value = "总金额")
    @TableField("INVOICE_TOTAL_AMOUNT")
    private BigDecimal invoiceTotalAmount;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "业务类型")
    @TableField("BUSINESS_TYPE")
    private String businessType;

    @ApiModelProperty(value = "申请主键")
    @TableField("APPLICATIONS_ID")
    private Integer applicationsId;

    @ApiModelProperty(value = "1单据2明细")
    @TableField("PAYMENT_TYPE")
    private Integer paymentType;
}
