package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-02-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALES_INVOICE_DETAIL")
@ApiModel(value = "SalesInvoiceDetail对象", description = "")
public class SalesInvoiceDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId("CLIENT")
    private String client;

    @TableField("GSI_ORDER_ID")
    private String gsiOrderId;

    @TableField("GSI_DN_ID")
    private String gsiDnId;

    @TableField("GSI_LINE_NO")
    private String gsiLineNo;

    @TableField("GSI_SITE")
    private String gsiSite;

    @TableField("GSI_CUSTOMER")
    private String gsiCustomer;

    @TableField("GSI_PRO_ID")
    private String gsiProId;

    @TableField("GSI_TAX")
    private String gsiTax;

    @TableField("GSI_SPECS")
    private String gsiSpecs;

    @TableField("GSI_UNIT")
    private String gsiUnit;

    @TableField("GSI_QTY")
    private BigDecimal gsiQty;

    @TableField("GSI_PRICE")
    private BigDecimal gsiPrice;

    @TableField("GSI_AMOUNT")
    private BigDecimal gsiAmount;

    @TableField("GSI_TAX_CLASS")
    private String gsiTaxClass;

    @TableField("GSI_ADD_TAX")
    private BigDecimal gsiAddTax;

    /**
     * 商品名称
     */
    @TableField("GSI_PRO_NAME")
    private String gsiProName;

    /**
     * 单据类型
     */
    @TableField("GSI_MAT_TYPE")
    private String gsiMatType;

    /**
     * 行号
     */
    @TableField("GSI_INDEX")
    private Integer gsiIndex;
}
