package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.HomeChartRequestDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;

public interface IMobileHomeService {

    /**
     * 首页销售数据
     * @param homeChartRequestDTO
     * @return
     */
    Result getHomeSalesList(HomeChartRequestDTO homeChartRequestDTO);

    Result getStoList();

    /**
     * 首页库存数据
     * @param homeChartRequestDTO
     * @return
     */
    Result getHomeStockList(HomeChartRequestDTO homeChartRequestDTO);

    Result getPaymentMethodList(HomeChartRequestDTO homeChartRequestDTO);

    Result getMonthPushMoneyList(HomePushMoneyDTO homePushMoneyDTO);

    Result getMonthPushMoneyChart(HomePushMoneyDTO homePushMoneyDTO);

    Result loginAppSaleTotal(HomeChartRequestDTO homeChartRequestDTO);

    Result getStoListByUserAuth();

    Result getHomeSalesListNew(HomeChartRequestDTO homeChartRequestDTO);
}
