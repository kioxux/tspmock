package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.gov.common.basic.DateUtils;
import com.gov.operate.entity.UserData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IUserDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-02
 */
@Service
public class UserDataServiceImpl extends ServiceImpl<UserDataMapper, UserData> implements IUserDataService {
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private CommonService commonService;

    /**
     * 根据加盟商和员工编号获取用户
     *
     * @param client
     * @param userId
     * @return
     */
    @Override
    public UserData getUserByClientAndUserId(String client, String userId) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", client);
        wrapper.eq("USER_ID", userId);
        return this.getOne(wrapper);
    }
    /**
     * 根据加盟商和员工手机号获取用户
     *
     * @param client
     * @param phone
     * @return
     */
    @Override
    public UserData getUserByClientAndPhone(String client, String phone) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", client);
        wrapper.eq("USER_TEL", phone);
        return this.getOne(wrapper);
    }

    /**
     * 获取userID
     * @return
     */
    @Override
    public int selectMaxId(String client){
        return userDataMapper.selectMaxId(client);
    }

    @Override
    public void insert(UserData userData) {
        userDataMapper.insert(userData);
    }

    @Override
    @Transactional
    public void editDepById(String depId, String depName, String userId){
        TokenUser tokenUser = commonService.getLoginInfo();

        //修改店员的部门
        UserData userData = new UserData();
        userData.setDepId(depId);
        userData.setDepName(depName);
        userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
        userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        userData.setUserModiId(userId);
        UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", tokenUser.getClient());
        updateWrapper.eq("USER_ID", userId);
        this.userDataMapper.update(userData,updateWrapper);
    }

}
