package com.gov.operate.dto.marketing;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class GetPromDetailsVO extends Pageable {

    @NotNull(message = "营销活动ID不能为空")
    private Integer id;

    // @NotBlank(message = "商品编码或名称")
    private String proCode;

}
