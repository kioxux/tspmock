package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_MONTH_APPRAISAL_SALARY")
@ApiModel(value="SalaryMonthAppraisalSalary对象", description="")
public class SalaryMonthAppraisalSalary extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSMAS_ID", type = IdType.AUTO)
    private Integer gsmasId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSMAS_GSP_ID")
    private Integer gsmasGspId;

    @ApiModelProperty(value = "岗位")
    @TableField("GSMAS_JOB")
    private String gsmasJob;

    @ApiModelProperty(value = "岗位名称")
    @TableField("GSMAS_JOB_NAME")
    private String gsmasJobName;

    @ApiModelProperty(value = "工资")
    @TableField("GSMAS_WAGE")
    private BigDecimal gsmasWage;

    @ApiModelProperty(value = "顺序")
    @TableField("GSMSG_SORT")
    private Integer gsmsgSort;


}
