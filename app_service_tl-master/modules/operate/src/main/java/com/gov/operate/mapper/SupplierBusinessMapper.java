package com.gov.operate.mapper;

import com.gov.operate.dto.GaiaBillOfApVO;
import com.gov.operate.entity.SupplierBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface SupplierBusinessMapper extends BaseMapper<SupplierBusiness> {

    SupplierBusiness selectSupplier(@Param("client") String client,@Param("payCompanyCode") String payCompanyCode,@Param("supCode") String supCode);

    Integer getBeginningOfPeriod(@Param("client")String client,@Param("supCode") String supCode,@Param("siteCode") String siteCode);

    List<GaiaBillOfApVO> getAmt(@Param("client")String client,@Param("supCode") String supCode, @Param("siteCode")String siteCode, @Param("date")String date);

    List<GaiaBillOfApVO> getBillOfAp(@Param("client")String client,@Param("supCode") String supCode, @Param("siteCode")String siteCode, @Param("date")String date);

    void updateSupplierBusiness(@Param("client")String client, @Param("supCode") String supCode, @Param("siteCode")String siteCode,@Param("amountOfPayment") BigDecimal amountOfPayment);

    GaiaBillOfApVO getBillOf(@Param("client")String client, @Param("supCode")String supCode, @Param("siteCode") String siteCode, @Param("yestDay") String yestDay);

    void insertBillOfAp(@Param("ofApVO")GaiaBillOfApVO ofApVO);

}
