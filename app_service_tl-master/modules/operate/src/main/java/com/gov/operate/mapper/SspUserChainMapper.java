package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SspUserChain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @Entity generator.domain.SspUserChain
 */
public interface SspUserChainMapper extends BaseMapper<SspUserChain> {

    /**
     * 获取当前用户可获取的连锁
     * @param userId
     * @param client
     * @return
     */
    List<SspUserChain> getChainList(@Param("userId") Long userId, @Param("client") String client);

    /**
     * 获取供应商主体
     * @param id
     * @param client
     * @return
     */
    List<SspUserChain> getSupplierChainList(@Param("userId") Long userId, @Param("client") String client);


    int deleteChainHead(@Param("userId") Long userId,@Param("client") String client,@Param("keySet") Set<String> keySet);


    int updateUserStatus(@Param("chain") SspUserChain chain);
}




