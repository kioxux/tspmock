package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_TRIAL_USER")
@ApiModel(value="SalaryTrialUser对象", description="")
public class SalaryTrialUser extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSTU_ID", type = IdType.AUTO)
    private Integer gstuId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSTU_GSP_ID")
    private Integer gstuGspId;

    @ApiModelProperty(value = "组织")
    @TableField("GSTU_GROUP")
    private String gstuGroup;

    @ApiModelProperty(value = "组织名")
    @TableField("GSTU_GROUP_NAME")
    private String gstuGroupName;

    @ApiModelProperty(value = "工号")
    @TableField("GSTU_USER_ID")
    private String gstuUserId;

    @ApiModelProperty(value = "姓名")
    @TableField("GSTU_USER_NAME")
    private String gstuUserName;

    @ApiModelProperty(value = "试算金额")
    @TableField("GSTU_AMOUNT")
    private BigDecimal gstuAmount;

    @ApiModelProperty(value = "上月实发")
    @TableField("GSTU_REAL")
    private BigDecimal gstuReal;

    @ApiModelProperty(value = "差异金额")
    @TableField("GSTU_DIFFERENCE")
    private BigDecimal gstuDifference;

    @ApiModelProperty(value = "顺序")
    @TableField("GSTU_SORT")
    private Integer gstuSort;


}
