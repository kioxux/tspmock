package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyVo;
import com.gov.operate.dto.monthPushMoney.MonthPushMoneyBySalespersonOutData;
import com.gov.operate.entity.StoreData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.kylin.RowMapper;
import com.gov.operate.mapper.SdMemberCardMapper;
import com.gov.operate.mapper.SdSaleDMapper;
import com.gov.operate.mapper.StoreDataMapper;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IMobileHomeService;
import com.gov.operate.service.ISdSaleDService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MobileHomeServiceImpl implements IMobileHomeService {

    @Resource
    private CommonService commonService;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;

    @Resource
    private UserDataMapper userDataMapper;

    @Resource
    private SdSaleDMapper saleDMapper;

    @Resource
    private ISdSaleDService saleDService;

    @Resource
    private SdMemberCardMapper sdMemberCardMapper;

    @Override
    public Result getHomeSalesList(HomeChartRequestDTO homeChartRequestDTO) {
        log.info("APP看板接口调用");
        TokenUser user = commonService.getLoginInfo();

//        TokenUser user = new TokenUser();
//        user.setClient("10000013");
//        homeChartRequestDTO = new HomeChartRequestDTO();
//        homeChartRequestDTO.setStoreId("1000000001");
//        homeChartRequestDTO.setType(5);
        List<HomeSalesVo> list = new ArrayList<>();
        log.info("报表参数：{}", JSONObject.toJSONString(homeChartRequestDTO));
        switch (homeChartRequestDTO.getType()) {
            case 1:
                list = getHomeSalesListByCurrent(user);
                break;
            case 2:
                list = getHomeSalesListByYesterday(user);
                break;
            case 3:
                list = getHomeSalesListByDay(user, homeChartRequestDTO);
                break;
            case 4:
                list = getHomeSalesListByWeek(user, homeChartRequestDTO);
                break;
            case 5:
                list = getHomeSalesListByMonth(user, homeChartRequestDTO);
                break;
            default:
                break;

        }


        return ResultUtil.success(list);
    }

    @Override
    public Result getStoList() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = storeDataMapper.getStoListByUserAuth(user.getClient(), user.getUserId());
//        StoreData storeData = storeDataMapper.getUserStore(user.getClient(), user.getUserId());
//        List<StoreData> storeList = new ArrayList<>();
//        if (storeData != null) {
//            storeList.add(storeData);
//        } else {
//            storeList = storeDataMapper.getStoreList(user.getClient());
//        }
        return ResultUtil.success(list);
    }

    /**
     * 首页库存数据
     *
     * @param homeChartRequestDTO
     * @return
     */
    @Override
    public Result getHomeStockList(HomeChartRequestDTO homeChartRequestDTO) {
        log.info("APP首页库存看板");
        TokenUser user = commonService.getLoginInfo();
        UserDataDTO userDataDTO = userDataMapper.hasStoreUser(user.getClient(), user.getUserId());
        if (userDataDTO == null) {
            return ResultUtil.error(ResultEnum.E0605);
        }
        HomeStockVo homeStockVo = new HomeStockVo();
//        UserDataDTO userDataDTO = new UserDataDTO();
//        userDataDTO.setClient("10000001");
//        userDataDTO.setDepId("1000000001");
//        userDataDTO.setIsStore(1);
//        homeChartRequestDTO.setType(2);
//        StringBuilder builder = new StringBuilder()
//                .append(" SELECT ")
//                .append(" COUNT(DISTINCT MAT_PRO_CODE) as STK_COUNT ")
//                .append(" ,round(sum(V_GAIA_MATERIAL_ASSESS.MAT_TOTAL_AMT)+sum(V_GAIA_MATERIAL_ASSESS.MAT_RATE_AMT),2) as STK_AMT ")
//                .append(" FROM YAOUD.V_GAIA_MATERIAL_ASSESS as V_GAIA_MATERIAL_ASSESS ")
//                .append(" INNER JOIN YAOUD.GAIA_STORE_DATA as GAIA_STORE_DATA ")
//                .append(" ON V_GAIA_MATERIAL_ASSESS.CLIENT = GAIA_STORE_DATA.CLIENT AND V_GAIA_MATERIAL_ASSESS.MAT_ASSESS_SITE = GAIA_STORE_DATA.STO_CODE ")
//                .append(" INNER JOIN YAOUD.GAIA_PRODUCT_BUSINESS as GAIA_PRODUCT_BUSINESS ")
//                .append(" ON V_GAIA_MATERIAL_ASSESS.CLIENT = GAIA_PRODUCT_BUSINESS.CLIENT AND V_GAIA_MATERIAL_ASSESS.MAT_PRO_CODE = GAIA_PRODUCT_BUSINESS.PRO_SELF_CODE AND V_GAIA_MATERIAL_ASSESS.MAT_ASSESS_SITE = GAIA_PRODUCT_BUSINESS.PRO_SITE ")
//                .append(" WHERE V_GAIA_MATERIAL_ASSESS.CLIENT = '")
//                .append(userDataDTO.getClient())
//                .append("'");
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList()) && homeChartRequestDTO.getStoCodeList().size() > 0) {
            //门店库存查询
            homeStockVo = saleDMapper.selectStoreStock(user.getClient(), homeChartRequestDTO.getStoCodeList());
        } else {
            //公司库存查询
            homeStockVo = saleDMapper.selectClientStock(user.getClient());
        }
        log.info("库存报表回结果:{}", JSONObject.toJSONString(homeStockVo));

        if (homeStockVo != null) {
            List<String> stoCodeList = homeChartRequestDTO.getStoCodeList();
            //门店级
            if (ObjectUtils.isNotEmpty(stoCodeList) && homeChartRequestDTO.getStoCodeList().size() > 0) {
                int stoCount = stoCodeList.size();
                homeStockVo.setStoreCount(stoCount + "");
                int count = saleDMapper.selectExpiryStoCount(user.getClient(), homeChartRequestDTO.getStoCodeList());
                //int outStockCount = saleDMapper.selectOutStockCountBySto(user.getClient(),  homeChartRequestDTO.getStoCodeList());
                int outStockCount = 0;
                //如果返回的List中有元素 则表示有权限显示，否则没权限显示
                List<String> showFlagList = getShowFlagList(stoCodeList, user);
                if(ObjectUtils.isEmpty(showFlagList)) {
                    homeStockVo.setStoShowFlag("0");
                } else {
                    homeStockVo.setStoShowFlag("1");
                    outStockCount = saleDMapper.selectOutStockCountBySto(user.getClient(), stoCodeList);
                }
                if (new BigDecimal(homeStockVo.getStkCount()).compareTo(BigDecimal.ZERO) == 1) {
                    BigDecimal expiryRate = new BigDecimal(count).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                    if (expiryRate.compareTo(new BigDecimal(10)) == 1) {
                        homeStockVo.setIsExpiry("1");
                    } else {
                        homeStockVo.setIsExpiry("0");
                    }
                    BigDecimal outStockRate = new BigDecimal(outStockCount).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                    if (outStockRate.compareTo(new BigDecimal(5)) == 1) {
                        homeStockVo.setIsOutStock("1");
                    } else {
                        homeStockVo.setIsOutStock("0");
                    }
                }
                homeStockVo.setExpiryCount(String.valueOf(count));
                homeStockVo.setOutStockCount(String.valueOf(outStockCount));
            } else {
                //公司级 门店数量
                List<StoreData> storeList = storeDataMapper.getStoreList(userDataDTO.getClient());
                if (storeList.size() > 0) {
                    homeStockVo.setStoreCount(String.valueOf(storeList.size()));
                    int count = saleDMapper.selectExpiryProCount(user.getClient());
                    homeStockVo.setExpiryCount(String.valueOf(count));
                    int outStockCount = saleDMapper.selectOutStockCountByClient(user.getClient(), null);
                    //根据加盟商判断公司是否为连锁公司
                    List<Map<String,String>> storeMapList = saleDMapper.findStoChainByClient(user.getClient());
                    homeStockVo.setIsChain("0");
                    for (Map<String,String> storeMap : storeMapList) {
                        if(storeMap.containsKey("dcCode")){
                            if (ObjectUtils.isNotEmpty(storeMap.get("dcCode"))){
                                homeStockVo.setIsChain("1");
                                break;
                            }
                        }
                    }
                    if (new BigDecimal(homeStockVo.getStkCount()).compareTo(BigDecimal.ZERO) == 1) {
                        BigDecimal expiryRate = new BigDecimal(count).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                        if (expiryRate.compareTo(new BigDecimal(10)) == 1) {
                            homeStockVo.setIsExpiry("1");
                        } else {
                            homeStockVo.setIsExpiry("0");
                        }
                        BigDecimal outStockRate = new BigDecimal(outStockCount).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                        if (outStockRate.compareTo(new BigDecimal(5)) == 1) {
                            homeStockVo.setIsOutStock("1");
                        } else {
                            homeStockVo.setIsOutStock("0");
                        }
                    }
                    homeStockVo.setOutStockCount(String.valueOf(outStockCount));
                }
            }

            LocalDate current = LocalDate.now(); // 当前日
            int monthDay = current.getDayOfMonth();

            LocalDate startDate = current.plusDays(-30);//30天前
            LocalDate endDate = current;
            if (homeChartRequestDTO.getType() == 2) {
                startDate = current.plusDays(-31);//30天前
                endDate = current.plusDays(-1);
            }
//            int day = endDate.getDayOfMonth();
//            int day = 0;
//            if (monthDay > 1) {
//                day = startDate.getDayOfMonth();
//            } else {
//                startDate =  current.plusDays(-1).with(TemporalAdjusters.firstDayOfMonth());
//            }

            if (StringUtils.isNotBlank(homeStockVo.getStkAmt())) {
                List<String> brIdList = new ArrayList<>();
                if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
                    brIdList.add(homeChartRequestDTO.getStoreId());
                }
                if(ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList()) && homeChartRequestDTO.getStoCodeList().size() > 0){
                    brIdList.addAll(homeChartRequestDTO.getStoCodeList());
                }
                String resultStartDate = startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                String resultEndDate = endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                BigDecimal saleCostAmt = saleDMapper.getSaleAmtByDateRangeExt(userDataDTO.getClient(), resultStartDate, resultEndDate, brIdList);
                if(ObjectUtils.isEmpty(saleCostAmt)) {
                    return ResultUtil.error("0001", "销售额错误");
                }
                //周转天数
                homeStockVo.setTurnoverDays(new BigDecimal(30).multiply(new BigDecimal(homeStockVo.getStkAmt())).divide(saleCostAmt, 0, RoundingMode.HALF_UP).toString());

                /*Map<String, String> map = new HashMap<>();
                map.put("client", userDataDTO.getClient());
                if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
                    map.put("depId", homeChartRequestDTO.getStoreId());
                }
                if(ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList()) && homeChartRequestDTO.getStoCodeList().size() > 0){
                    String stoCodeStr = "";
                    for(int i = 0;i< homeChartRequestDTO.getStoCodeList().size(); i++){
                        if (i + 1 == homeChartRequestDTO.getStoCodeList().size()) {
                            stoCodeStr += homeChartRequestDTO.getStoCodeList().get(i);
                        }else {
                            stoCodeStr += homeChartRequestDTO.getStoCodeList().get(i) + ",";
                        }
                    }
                    map.put("depIds", stoCodeStr);
                }
                map.put("startDate", startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                map.put("endDate", endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                String saleCost = getSaleCost(map);
                if (StringUtils.isBlank(saleCost)) {
                    return ResultUtil.error("0001", "销售额错误");
                }
                // 周转天数
                homeStockVo.setTurnoverDays(new BigDecimal(30).multiply(new BigDecimal(homeStockVo.getStkAmt())).divide(new BigDecimal(saleCost), 0, RoundingMode.HALF_UP).toString());*/
            }

        }
        return ResultUtil.success(homeStockVo);
    }


    /**
     * 获取有显示权限的门店
     * @param stoCodeList
     * @param user
     * @return
     */
    private List<String> getShowFlagList(List<String> stoCodeList, TokenUser user) {
        //获取门店数量
        int stoCount = stoCodeList.size();
        //有显示权限的门店List
        List<String> stoList = new ArrayList<>();
        //门店数量是否为1家
        if(stoCount > 1) {
            for(String stoCode : stoCodeList) {
                //遍历查看每家门店是否有显示权限
                String showFlag =
                        saleDMapper.selectAuthByClientAndStoCode(user.getClient(), stoCode, "APP_STORE_SHOW_OUT_STOCK_FLAG");
                if("1".equals(showFlag)) {
                    stoList.add(stoCode);
                }
            }
        } else if(stoCount == 1) {
            String showFlag =
                    saleDMapper.selectAuthByClientAndStoCode(user.getClient(), stoCodeList.get(0), "APP_STORE_SHOW_OUT_STOCK_FLAG");
            if("1".equals(showFlag)) {
                stoList.add(stoCodeList.get(0));
            }
        }
        return stoList;
    }

    @Override
    public Result getPaymentMethodList(HomeChartRequestDTO homeChartRequestDTO) {
        TokenUser user = commonService.getLoginInfo();
        LocalDate current = LocalDate.now(); // 当前日
        int monthDay = current.getDayOfMonth();
        LocalDate startDate = null;
        LocalDate endDate = null;
        List<PaymentMethodVo> outData = new ArrayList<>();
//        TokenUser user = new TokenUser();
//        user.setClient("10000001");
//        homeChartRequestDTO.setStoreId("1000000001");


        if (homeChartRequestDTO.getType() == 2) {//	进入页面默认公司下所有门店销售汇总；查询期间默认“日”，为登录日期前一天日期
            startDate = current.plusDays(-1);
        } else if (homeChartRequestDTO.getType() == 6) {//	，为当前月1号到登录日期前一天日期
            if (monthDay > 1) { //如果今天是一号 就去上个月一整个月
                startDate = current.with(TemporalAdjusters.firstDayOfMonth());
            } else {
                startDate = current.plusDays(-1).with(TemporalAdjusters.firstDayOfMonth());
            }
        } else if (homeChartRequestDTO.getType() == 5) {//	进入页面默认公司下所有门店销售汇总；查询期间默认“日”，为登录日期前一天日期
            if (monthDay > 1) {//如果今天是一号 就去上个月一整年
                startDate = current.with(TemporalAdjusters.firstDayOfYear());

            } else {
                startDate = current.plusYears(-1).with(TemporalAdjusters.firstDayOfYear());
            }
        }
        endDate = current.plusDays(-1);
        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" PAY_MSG.CLIENT as CLIENT, ")
                .append(" PAY_MSG.GSSPM_BR_ID as BR_ID, ")
                .append(" GAIA_STORE_DATA.STO_NAME as STO_NAME, ")//  --  1000 现金 2000 微信 3000 支付宝  4000医保  其余ID为”其它“
                .append(" PAY_MSG.GSSPM_ID as ID, ") //	默认显示5种支付方式“现金”、“医保”、“支付宝”、“微信”、“其他”  ,	“其他”支付方式金额=销售额-(现金+医保+支付宝+微信)
                .append(" PAY_MSG.GSSPM_NAME as NAME, ");
//                .append(" -- ,GCD_MONTH ,GCD_WEEK    -- 月 ，周  按此字段GROUP BY\n ")
        if (homeChartRequestDTO.getType() == 6) { //	，为当前月1号到登录日期前一天日期
            builder.append(" GCD_MONTH, ");
        }
        if (homeChartRequestDTO.getType() == 5) { //	//如果今天是一号 就去上个月一整年
            builder.append(" GCD_YEAR, ");
        }
        builder.append(" SUM(GSSPM_AMT) as AMT ")
                .append(" FROM YAOUD.V_GAIA_SD_SALE_PAY_MSG as PAY_MSG ")
                .append(" INNER JOIN YAOUD.GAIA_CAL_DT  ")
                .append(" ON PAY_MSG.GSSPM_DATE = GAIA_CAL_DT.GCD_DATE ")
                .append(" INNER JOIN YAOUD.GAIA_STORE_DATA ")
                .append(" ON PAY_MSG.CLIENT = GAIA_STORE_DATA.CLIENT AND PAY_MSG.GSSPM_BR_ID = GAIA_STORE_DATA.STO_CODE  ")
                .append(" WHERE 1=1 ")
                .append(" AND GSSPM_DATE >= '" + startDate + "' ")
                .append(" AND GSSPM_DATE <= '" + endDate + "' ")
                .append(" AND PAY_MSG.CLIENT='" + user.getClient() + "' ");

        //门店维度的查询
        if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" AND PAY_MSG.GSSPM_BR_ID='" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" GROUP BY PAY_MSG.CLIENT,PAY_MSG.GSSPM_BR_ID,STO_NAME,GSSPM_ID ,GSSPM_NAME  ");
        if (homeChartRequestDTO.getType() == 6) { //	，为当前月1号到登录日期前一天日期
            builder.append(" ,GCD_MONTH ");
        }
        if (homeChartRequestDTO.getType() == 5) { //	//如果今天是一号 就去上个月一整年
            builder.append(" ,GCD_YEAR ");
        }

        log.info("付款方式数据：{}", builder.toString());
        List<PaymentMethodVo> paymentMethodOut = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(PaymentMethodVo.class));
        log.info("付款方式数据返回结果:{}", JSONObject.toJSONString(paymentMethodOut));
        if (ObjectUtils.isNotEmpty(paymentMethodOut)) {
            Map<String, String> map = new HashMap<>();
            map.put("client", user.getClient());
            if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
                map.put("depId", homeChartRequestDTO.getStoreId());
            }
            map.put("startDate", startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            map.put("endDate", endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            String saleDAmt = getSales(map);
            BigDecimal bd = BigDecimal.ZERO;
            List<String> ids = Arrays.asList("1000", "3001", "3002", "4000");
            for (String id : ids) {
                PaymentMethodVo other = new PaymentMethodVo();
                other.setId(id);
                other.setName(CommonEnum.PaymentMethod.getByCode(id).getName());

                for (PaymentMethodVo item : paymentMethodOut) {

                    if (id.equals(item.getId())) {
                        bd = new BigDecimal(item.getAmt()).add(bd);
                        other.setAmt(item.getAmt());
                        break;
                    } else {
                        other.setAmt("0");
                    }

                }
                outData.add(other);
            }
            PaymentMethodVo other = new PaymentMethodVo();
            other.setId("other");
            other.setName("其他");
            other.setAmt(String.valueOf(new BigDecimal(saleDAmt).subtract(bd)));

            outData.add(other);
        } else {
            for (CommonEnum.PaymentMethod paytype : CommonEnum.PaymentMethod.values()) {
                PaymentMethodVo other = new PaymentMethodVo();
                other.setId(paytype.getCode());
                other.setName(paytype.getName());
                other.setAmt("0");

                outData.add(other);
            }
        }

        return ResultUtil.success(outData);
    }


    /**
     * 查询GAIA_SD_SALE_D销售额
     *
     * @param map
     * @return
     */
    private String getSales(Map<String, String> map) {
        StringBuilder builder = new StringBuilder()
                .append(" SELECT sum(GSSD_AMT) as GSSD_AMT ")
                .append(" FROM GAIA_SD_SALE_D  ")
                .append(" where client= '")
                .append(map.get("client"))
                .append("' ")
                .append("  and gssd_date>= '")
                .append(map.get("startDate"))
                .append("' ")
                .append("  and gssd_date<= '")
                .append(map.get("endDate"))
                .append("' ");
        if (map.containsKey("depId")) {
            builder.append("  and GSSD_BR_ID= '")
                    .append(map.get("depId"))
                    .append("' ");
        }

        log.info("用户营业实收金额数据：{}", builder.toString());
        Map<String, Object> objectMap = kylinJdbcTemplate.queryForMap(builder.toString());
        log.info("用户营业实收金额返回结果:{}", JSONObject.toJSONString(objectMap));
        if (!objectMap.isEmpty()) {
            return String.valueOf(objectMap.get("GSSD_AMT"));
        }
        return null;
    }

    private String getSaleCost(Map<String, String> map) {
        StringBuilder builder = new StringBuilder()
                .append(" SELECT sum(GSSD_MOV_PRICE) as SALE_COST ")
                .append(" FROM GAIA_SD_SALE_D  ")
                .append(" where client= '")
                .append(map.get("client"))
                .append("' ")
                .append("  and gssd_date>= '")
                .append(map.get("startDate"))
                .append("' ")
                .append("  and gssd_date<= '")
                .append(map.get("endDate"))
                .append("' ");
        if (map.containsKey("depId")) {
            builder.append("  and GSSD_BR_ID= '")
                    .append(map.get("depId"))
                    .append("' ");
        }
        if (map.containsKey("depIds")) {
            builder.append("  and GSSD_BR_ID IN (")
                    .append(map.get("depIds"))
                    .append(" )");
        }
        log.info("库存报表门店销售值数据：{}", builder.toString());
        Map<String, Object> objectMap = kylinJdbcTemplate.queryForMap(builder.toString());
        log.info("库存报表门店销售值返回结果:{}", JSONObject.toJSONString(objectMap));
        if (!objectMap.isEmpty()) {
            if (objectMap.get("SALE_COST") != null) {
                return objectMap.get("SALE_COST").toString();
            }
        }
        return null;
    }


    /**
     * 12月数据集
     *
     * @param user
     * @param homeChartRequestDTO
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByMonth(TokenUser user, HomeChartRequestDTO homeChartRequestDTO) {
        LocalDate current = LocalDate.now(); // 当前日
        String firstDay = current.getYear() + "-" + "01-01";

        String lastYearFirstDay = (current.getYear() - 1) + "-" + "01-01";

        String lastYearLastDay = (current.getYear() - 1) + "-" + "12-31";

        StringBuilder builder = new StringBuilder()
                .append(" select client , gssd_type,gssd_date,gssd_amt,gssd_gross_amt,gssd_gross_mar,gssd_trans,gssd_aprice,gssd_amt_avg ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,gssd_br_id ");
        }
        builder.append(" ,GCD_WEEK ");

        builder.append(" from ( ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_MONTH as GSSD_DATE, ")
                .append(" D.GCD_MONTH as GCD_WEEK, ")
                .append(" 'D' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + firstDay + "' ")
                .append(" and gssd_date<='" + current.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_MONTH ")
                .append(" ,D.GCD_MONTH ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_MONTH as GSSD_DATE, ")
                .append(" D.GCD_MONTH as GCD_WEEK, ")
                .append("  'T' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + lastYearFirstDay + "' ")
                .append(" and gssd_date<='" + lastYearLastDay + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_MONTH ")
                .append(" ,D.GCD_MONTH ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" ) order by gssd_type,client,gssd_date asc ");
        log.info("报表月数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));
        List<HomeSalesVo> resultList = new ArrayList<>();
        Map<String, List<HomeSalesVo>> mapList = new HashMap<>();
        if (list.size() > 0) {
            mapList = list.stream().collect(Collectors.groupingBy(HomeSalesVo::getGssdType));
            for (Map.Entry<String, List<HomeSalesVo>> entry : mapList.entrySet()) {
                List<HomeSalesVo> entryList = entry.getValue();
                for (int i = 1; i <= 12; i++) {
                    boolean flag = false;
                    for (HomeSalesVo vo : entryList) {
                        if (StringUtils.isNotBlank(vo.getGcdWeek()) && Integer.parseInt(vo.getGcdWeek()) == i) {
                            flag = true;
                            vo.setGssdDate(String.valueOf(i));
                            resultList.add(vo);
                            break;
                        }
                    }
                    if (!flag) {
                        HomeSalesVo homeSalesVo = new HomeSalesVo();
                        homeSalesVo.setGssdType(entry.getKey());
                        homeSalesVo.setGssdDate(String.valueOf(i));
                        resultList.add(homeSalesVo);
                    }

                }
            }
        }


        if (!mapList.containsKey("D")) {
            for (int i = 1; i <= 12; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("D");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("S")) {
            for (int i = 1; i <= 12; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("S");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("T")) {
            for (int i = 1; i <= 12; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("T");
                resultList.add(homeSalesVo);
            }
        }

        return resultList;

    }

    /**
     * 12月数据集
     *
     * @param user
     * @param homeChartRequestDTO
     * @return
     */
    private List<HomeSalesVo> getNewHomeSalesListByMonth(TokenUser user, HomeChartRequestDTO homeChartRequestDTO) {
        String stoQueryWapper = stoQueryWapper = commonService.queryWapperAuto(homeChartRequestDTO.getStoCodeList());
        if (StringUtils.isBlank(stoQueryWapper)) {
            return new ArrayList<HomeSalesVo>();
        }
        LocalDate current = LocalDate.now(); // 当前日
        String firstDay = current.getYear() + "-" + "01-01";

        String lastYearFirstDay = (current.getYear() - 1) + "-" + "01-01";

        String lastYearLastDay = (current.getYear() - 1) + "-" + "12-31";

        StringBuilder builder = new StringBuilder()
                .append(" select client , gssd_type,gssd_date,gssd_amt,gssd_gross_amt,gssd_gross_mar,gssd_trans,gssd_aprice,gssd_amt_avg ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,gssd_br_id ");
        }
        builder.append(" ,GCD_WEEK ");

        builder.append(" from ( ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_MONTH as GSSD_DATE, ")
                .append(" D.GCD_MONTH as GCD_WEEK, ")
                .append(" 'D' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + firstDay + "' ")
                .append(" and gssd_date<='" + current.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_MONTH ")
                .append(" ,D.GCD_MONTH ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_MONTH as GSSD_DATE, ")
                .append(" D.GCD_MONTH as GCD_WEEK, ")
                .append("  'T' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + lastYearFirstDay + "' ")
                .append(" and gssd_date<='" + lastYearLastDay + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_MONTH ")
                .append(" ,D.GCD_MONTH ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" ) order by gssd_type,client,gssd_date asc ");
        log.info("报表月数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));
        List<HomeSalesVo> resultList = new ArrayList<>();
        Map<String, List<HomeSalesVo>> mapList = new HashMap<>();
        if (list.size() > 0) {
            mapList = list.stream().collect(Collectors.groupingBy(HomeSalesVo::getGssdType));
            for (Map.Entry<String, List<HomeSalesVo>> entry : mapList.entrySet()) {
                List<HomeSalesVo> entryList = entry.getValue();
                for (int i = 1; i <= 12; i++) {
                    boolean flag = false;
                    for (HomeSalesVo vo : entryList) {
                        if (StringUtils.isNotBlank(vo.getGcdWeek()) && Integer.parseInt(vo.getGcdWeek()) == i) {
                            flag = true;
                            vo.setGssdDate(String.valueOf(i));
                            resultList.add(vo);
                            break;
                        }
                    }
                    if (!flag) {
                        HomeSalesVo homeSalesVo = new HomeSalesVo();
                        homeSalesVo.setGssdType(entry.getKey());
                        homeSalesVo.setGssdDate(String.valueOf(i));
                        resultList.add(homeSalesVo);
                    }

                }
            }
        }


        if (!mapList.containsKey("D")) {
            for (int i = 1; i <= 12; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("D");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("S")) {
            for (int i = 1; i <= 12; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("S");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("T")) {
            for (int i = 1; i <= 12; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("T");
                resultList.add(homeSalesVo);
            }
        }

        return resultList;
    }

    /**
     * 7周数据集
     *
     * @param user
     * @param homeChartRequestDTO
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByWeek(TokenUser user, HomeChartRequestDTO homeChartRequestDTO) {
        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 1);

        LocalDate current = LocalDate.now(); // 当前日
        int currentWeek = current.get(weekFields.weekOfYear()); // 今年第几周
        int lastWeek = 1;
        if (currentWeek == 7) {
            lastWeek = currentWeek - 6;
        } else if (currentWeek - 6 > 1) {
            lastWeek = currentWeek - 6 - 1;
        }

        LocalDate toDay = LocalDate.parse(current.getYear() + "-W" + StringUtils.leftPad(String.valueOf(currentWeek - 1), 2, "0") + "-7", DateTimeFormatter.ISO_WEEK_DATE);
        LocalDate fromDay = LocalDate.parse(current.getYear() + "-W" + StringUtils.leftPad(String.valueOf(lastWeek), 2, "0") + "-1", DateTimeFormatter.ISO_WEEK_DATE);
        // 本期同周
        LocalDate lastYearFirstDay = LocalDate.parse(current.plusYears(-1).getYear() + "-W" + StringUtils.leftPad(String.valueOf(lastWeek), 2, "0") + "-1", DateTimeFormatter.ISO_WEEK_DATE);
        LocalDate lastYearLastDay = LocalDate.parse(current.plusYears(-1).getYear() + "-W" + StringUtils.leftPad(String.valueOf(currentWeek - 1), 2, "0") + "-7", DateTimeFormatter.ISO_WEEK_DATE);

        StringBuilder builder = new StringBuilder()
                .append(" select client , gssd_type,gssd_date,gssd_amt,gssd_gross_amt,gssd_gross_mar,gssd_trans,gssd_aprice,gssd_amt_avg ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,gssd_br_id ");
        }
        builder.append(" ,GCD_WEEK ");

        builder.append(" from ( ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_WEEK as GSSD_DATE, ")
                .append(" D.GCD_WEEK, ")
                .append(" 'D' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + fromDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + toDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_WEEK ")
                .append(" ,D.GCD_WEEK ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_WEEK as GSSD_DATE, ")
                .append(" D.GCD_WEEK, ")
                .append("  'T' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + lastYearFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + lastYearLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_WEEK ")
                .append(" ,D.GCD_WEEK ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" ) order by gssd_type,client,gssd_date asc ");
        log.info("报表周数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));
        List<HomeSalesVo> resultList = new ArrayList<>();
        Map<String, List<HomeSalesVo>> mapList = new HashMap<>();
        if (list.size() > 0) {
            mapList = list.stream().collect(Collectors.groupingBy(HomeSalesVo::getGssdType));
            for (Map.Entry<String, List<HomeSalesVo>> entry : mapList.entrySet()) {
                List<HomeSalesVo> entryList = entry.getValue();
                for (int i = lastWeek; i <= currentWeek; i++) {
                    boolean flag = false;
                    for (HomeSalesVo vo : entryList) {
                        if (StringUtils.isNotBlank(vo.getGcdWeek()) && Integer.parseInt(vo.getGcdWeek()) == i) {
                            flag = true;
                            vo.setGssdDate(String.valueOf(i));
                            resultList.add(vo);
                            break;
                        }
                    }
                    if (!flag) {
                        HomeSalesVo homeSalesVo = new HomeSalesVo();
                        homeSalesVo.setGssdType(entry.getKey());
                        homeSalesVo.setGssdDate(String.valueOf(i));
                        resultList.add(homeSalesVo);
                    }

                }
            }
        }

        if (!mapList.containsKey("D")) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("D");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("S")) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("S");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("T")) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("T");
                resultList.add(homeSalesVo);
            }
        }

        return resultList;

    }

    /**
     * 7周数据集
     *
     * @param user
     * @param homeChartRequestDTO
     * @return
     */
    private List<HomeSalesVo> getNewHomeSalesListByWeek(TokenUser user, HomeChartRequestDTO homeChartRequestDTO) {
        String stoQueryWapper = stoQueryWapper = commonService.queryWapperAuto(homeChartRequestDTO.getStoCodeList());
        if (StringUtils.isBlank(stoQueryWapper)) {
            return new ArrayList<HomeSalesVo>();
        }

        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 1);

        LocalDate current = LocalDate.now(); // 当前日
        int currentWeek = current.get(weekFields.weekOfYear()); // 今年第几周
        int lastWeek = 1;
        if (currentWeek == 7) {
            lastWeek = currentWeek - 6;
        } else if (currentWeek - 6 > 1) {
            lastWeek = currentWeek - 6 - 1;
        }

        LocalDate toDay = LocalDate.parse(current.getYear() + "-W" + StringUtils.leftPad(String.valueOf(currentWeek - 1), 2, "0") + "-7", DateTimeFormatter.ISO_WEEK_DATE);
        LocalDate fromDay = LocalDate.parse(current.getYear() + "-W" + StringUtils.leftPad(String.valueOf(lastWeek), 2, "0") + "-1", DateTimeFormatter.ISO_WEEK_DATE);
        // 本期同周
        LocalDate lastYearFirstDay = LocalDate.parse(current.plusYears(-1).getYear() + "-W" + StringUtils.leftPad(String.valueOf(lastWeek), 2, "0") + "-1", DateTimeFormatter.ISO_WEEK_DATE);
        LocalDate lastYearLastDay = LocalDate.parse(current.plusYears(-1).getYear() + "-W" + StringUtils.leftPad(String.valueOf(currentWeek - 1), 2, "0") + "-7", DateTimeFormatter.ISO_WEEK_DATE);

        StringBuilder builder = new StringBuilder()
                .append(" select client , gssd_type,gssd_date,gssd_amt,gssd_gross_amt,gssd_gross_mar,gssd_trans,gssd_aprice,gssd_amt_avg ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,gssd_br_id ");
        }
        builder.append(" ,GCD_WEEK ");

        builder.append(" from ( ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_WEEK as GSSD_DATE, ")
                .append(" D.GCD_WEEK, ")
                .append(" 'D' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + fromDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + toDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_WEEK ,D.GCD_WEEK");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }
        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" D.GCD_YEAR||'-'||D.GCD_WEEK as GSSD_DATE, ")
                .append(" D.GCD_WEEK, ")
                .append("  'T' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + lastYearFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + lastYearLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,D.GCD_YEAR||'-'||D.GCD_WEEK, D.GCD_WEEK ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" ) order by gssd_type,client,gssd_date asc ");
        log.info("报表周数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));
        List<HomeSalesVo> resultList = new ArrayList<>();
        Map<String, List<HomeSalesVo>> mapList = new HashMap<>();
        if (list.size() > 0) {
            mapList = list.stream().collect(Collectors.groupingBy(HomeSalesVo::getGssdType));
            for (Map.Entry<String, List<HomeSalesVo>> entry : mapList.entrySet()) {
                List<HomeSalesVo> entryList = entry.getValue();
                for (int i = lastWeek; i <= currentWeek; i++) {
                    boolean flag = false;
                    for (HomeSalesVo vo : entryList) {
                        if (StringUtils.isNotBlank(vo.getGcdWeek()) && Integer.parseInt(vo.getGcdWeek()) == i) {
                            flag = true;
                            vo.setGssdDate(String.valueOf(i));
                            resultList.add(vo);
                            break;
                        }
                    }
                    if (!flag) {
                        HomeSalesVo homeSalesVo = new HomeSalesVo();
                        homeSalesVo.setGssdType(entry.getKey());
                        homeSalesVo.setGssdDate(String.valueOf(i));
                        resultList.add(homeSalesVo);
                    }

                }
            }
        }

        if (!mapList.containsKey("D")) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("D");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("S")) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("S");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("T")) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("T");
                resultList.add(homeSalesVo);
            }
        }

        return resultList;
    }

    /**
     * 7日数据集
     *
     * @param user
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByDay(TokenUser user, HomeChartRequestDTO homeChartRequestDTO) {
        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 1);

        LocalDate current = LocalDate.now(); // 当前日

        int currentWeek = current.get(weekFields.dayOfWeek()); // 当前周
        LocalDate currentFirstDay = current.plusDays((currentWeek - 1) * -1);
        LocalDate currentLastDay = current.plusDays(7 - currentWeek);

        LocalDate lastWeek = LocalDate.now().plusWeeks(-1); // 上周
        LocalDate lastWeekFirstDay = lastWeek.plusDays((currentWeek - 1) * -1);
        LocalDate lastWeekLastDay = lastWeek.plusDays(7 - currentWeek);

        LocalDate lastYear = LocalDate.now().plusYears(-1); // 去年同期
        LocalDate lastYearFirstDay = lastYear.plusDays((currentWeek - 1) * -1);
        LocalDate lastYearLastDay = lastYear.plusDays(7 - currentWeek);

        StringBuilder builder = new StringBuilder()
                .append(" select client , gssd_type,gssd_date,gssd_amt,gssd_gross_amt,gssd_gross_mar,gssd_trans,gssd_aprice,gssd_amt_avg ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,gssd_br_id ");
        }

        builder.append(" from ( ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" GSSD_DATE, ")
                .append("  'D' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + currentFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + currentLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,gssd_date ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }
        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" GSSD_DATE, ")
                .append("  'S' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + lastWeekFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + lastWeekLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,gssd_date ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" GSSD_DATE, ")
                .append("  'T' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        builder.append(" and gssd_date>='" + lastYearFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + lastYearLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,gssd_date ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" ) order by gssd_type,client,gssd_date asc ");
        log.info("报表日数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));
        List<HomeSalesVo> resultList = new ArrayList<>();
        Map<String, List<HomeSalesVo>> mapList = new HashMap<>();
        if (list.size() > 0) {
            mapList = list.stream().collect(Collectors.groupingBy(HomeSalesVo::getGssdType));
            for (Map.Entry<String, List<HomeSalesVo>> entry : mapList.entrySet()) {
                List<HomeSalesVo> entryList = entry.getValue();
                for (int i = 1; i <= 7; i++) {
                    boolean flag = false;
                    for (HomeSalesVo vo : entryList) {
                        if (DateUtils.parseLocalDate(vo.getGssdDate()).get(weekFields.dayOfWeek()) == i) {
                            flag = true;
                            resultList.add(vo);
                            break;
                        }
                    }
                    if (!flag) {
                        HomeSalesVo homeSalesVo = new HomeSalesVo();
                        homeSalesVo.setGssdType(entry.getKey());
                        resultList.add(homeSalesVo);
                    }

                }
            }
        }

        if (!mapList.containsKey("D")) {
            for (int i = 1; i <= 7; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("D");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("S")) {
            for (int i = 1; i <= 7; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("S");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("T")) {
            for (int i = 1; i <= 7; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("T");
                resultList.add(homeSalesVo);
            }
        }

        return resultList;
    }

    /**
     * 7日数据集
     *
     * @param user
     * @return
     */
    private List<HomeSalesVo> getNewHomeSalesListByDay(TokenUser user, HomeChartRequestDTO homeChartRequestDTO) {
        String stoQueryWapper = stoQueryWapper = commonService.queryWapperAuto(homeChartRequestDTO.getStoCodeList());
        if (StringUtils.isBlank(stoQueryWapper)) {
            return new ArrayList<HomeSalesVo>();
        }
        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 1);
        LocalDate current = LocalDate.now(); // 当前日

        int currentWeek = current.get(weekFields.dayOfWeek()); // 当前周
        LocalDate currentFirstDay = current.plusDays((currentWeek - 1) * -1);
        LocalDate currentLastDay = current.plusDays(7 - currentWeek);

        LocalDate lastWeek = LocalDate.now().plusWeeks(-1); // 上周
        LocalDate lastWeekFirstDay = lastWeek.plusDays((currentWeek - 1) * -1);
        LocalDate lastWeekLastDay = lastWeek.plusDays(7 - currentWeek);

        LocalDate lastYear = LocalDate.now().plusYears(-1); // 去年同期
        LocalDate lastYearFirstDay = lastYear.plusDays((currentWeek - 1) * -1);
        LocalDate lastYearLastDay = lastYear.plusDays(7 - currentWeek);

        StringBuilder builder = new StringBuilder()
                .append(" select client , gssd_type,gssd_date,gssd_amt,gssd_gross_amt,gssd_gross_mar,gssd_trans,gssd_aprice,gssd_amt_avg ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,gssd_br_id ");
        }

        builder.append(" from ( ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" GSSD_DATE, ")
                .append("  'D' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + currentFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + currentLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,gssd_date ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }
        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" GSSD_DATE, ")
                .append("  'S' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + lastWeekFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + lastWeekLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,gssd_date ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" union all ")
                .append(" SELECT ")
                .append(" client, ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" GSSD_DATE, ")
                .append("  'T' as GSSD_TYPE, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ");
        builder.append(" and client = '" + user.getClient() + "' ");
//        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
//            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date>='" + lastYearFirstDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" and gssd_date<='" + lastYearLastDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "' ")
                .append(" group by client,gssd_date ");
        if (StringUtils.isNotBlank(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GSSD_BR_ID ");
        }

        builder.append(" ) order by gssd_type,client,gssd_date asc ");
        log.info("报表日数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));
        List<HomeSalesVo> resultList = new ArrayList<>();
        Map<String, List<HomeSalesVo>> mapList = new HashMap<>();
        if (list.size() > 0) {
            mapList = list.stream().collect(Collectors.groupingBy(HomeSalesVo::getGssdType));
            for (Map.Entry<String, List<HomeSalesVo>> entry : mapList.entrySet()) {
                List<HomeSalesVo> entryList = entry.getValue();
                for (int i = 1; i <= 7; i++) {
                    boolean flag = false;
                    for (HomeSalesVo vo : entryList) {
                        if (DateUtils.parseLocalDate(vo.getGssdDate()).get(weekFields.dayOfWeek()) == i) {
                            flag = true;
                            resultList.add(vo);
                            break;
                        }
                    }
                    if (!flag) {
                        HomeSalesVo homeSalesVo = new HomeSalesVo();
                        homeSalesVo.setGssdType(entry.getKey());
                        resultList.add(homeSalesVo);
                    }

                }
            }
        }

        if (!mapList.containsKey("D")) {
            for (int i = 1; i <= 7; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("D");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("S")) {
            for (int i = 1; i <= 7; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("S");
                resultList.add(homeSalesVo);
            }
        }

        if (!mapList.containsKey("T")) {
            for (int i = 1; i <= 7; i++) {
                HomeSalesVo homeSalesVo = new HomeSalesVo();
                homeSalesVo.setGssdType("T");
                resultList.add(homeSalesVo);
            }
        }

        return resultList;
    }

    /**
     * 今日数据集
     *
     * @param user
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByCurrent(TokenUser user) {
        StoreData storeData = null;
        if (StringUtils.isNotBlank(user.getDepId())) {
            storeData = storeDataMapper.getUserStore(user.getClient(), user.getUserId());
        }

        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                // .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + user.getClient() + "' ");
        if (storeData != null) {
            builder.append(" and GSSD_BR_ID = '" + user.getDepId() + "'");
        }
        builder.append(" and gssd_date = '" + DateUtils.getCurrentDateStr() + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表今日数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));

        return list;
    }

    /**
     * 今日数据集
     *
     * @param user
     * @return
     */
    private List<HomeSalesVo> getNewHomeSalesListByCurrent(TokenUser user) {
        String stoQueryWapper = commonService.queryWapperSto();
        if (StringUtils.isBlank(stoQueryWapper)) {
            return new ArrayList<HomeSalesVo>();
        }
        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                // .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + user.getClient() + "' ");
//        if (storeData != null) {
//            builder.append(" and GSSD_BR_ID = '" + user.getDepId() + "'");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date = '" + DateUtils.getCurrentDateStr() + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表今日数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));

        return list;
    }

    private String queryWapperSto() {
        List<String> stoCodeList = this.getStoCodeList();
        StringBuilder sb = new StringBuilder();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            return null;
        }
        // ('10019','1000000001')
        sb.append("(");
        for (int i = 0; i < stoCodeList.size(); i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append("'");
            sb.append("stoCode");
            sb.append("'");
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * 今日数据集
     *
     * @param homeChartRequestDTO
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByCurrentNew(HomeChartRequestDTO homeChartRequestDTO) {

        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + homeChartRequestDTO.getClientId() + "' ");
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "'");
        }
        builder.append(" and gssd_date = '" + DateUtils.getCurrentDateStr() + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表今日数据：{}", builder.toString());
        List<HomeSalesVo> homeSalesVo = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(homeSalesVo));

        return homeSalesVo;
    }

    /**
     * 昨日数据集
     *
     * @param user
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByYesterday(TokenUser user) {
        StoreData storeData = null;
        if (StringUtils.isNotBlank(user.getDepId())) {
            storeData = storeDataMapper.getUserStore(user.getClient(), user.getUserId());
        }

        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                //    .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + user.getClient() + "' ");
        if (storeData != null) {
            builder.append(" and GSSD_BR_ID = '" + user.getDepId() + "'");
        }
        builder.append(" and gssd_date = '" + LocalDate.now().plusDays(-1).format(DateUtils.DATE_FORMATTER) + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表昨日数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));

        return list;
    }

    /**
     * 昨日数据集
     *
     * @param user
     * @return
     */
    private List<HomeSalesVo> getNewHomeSalesListByYesterday(TokenUser user) {
        String stoQueryWapper = commonService.queryWapperSto();
        if (StringUtils.isBlank(stoQueryWapper)) {
            return new ArrayList<HomeSalesVo>();
        }

        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                //    .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + user.getClient() + "' ");
//        if (storeData != null) {
//            builder.append(" and GSSD_BR_ID = '" + user.getDepId() + "'");
//        }
        builder.append(" and GSSD_BR_ID").append(stoQueryWapper);
        builder.append(" and gssd_date = '" + LocalDate.now().plusDays(-1).format(DateUtils.DATE_FORMATTER) + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表昨日数据：{}", builder.toString());
        List<HomeSalesVo> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));

        return list;
    }

    /**
     * 昨日数据集
     *
     * @param homeChartRequestDTO
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByYesterdayNew(HomeChartRequestDTO homeChartRequestDTO) {
        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + homeChartRequestDTO.getClientId() + "' ");
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "'");
        }
        builder.append(" and gssd_date = '" + LocalDate.now().plusDays(-1).format(DateUtils.DATE_FORMATTER) + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表昨日数据：{}", builder.toString());
        List<HomeSalesVo> homeSalesVo = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(homeSalesVo));

        return homeSalesVo;
    }

    @Override
    public Result getMonthPushMoneyList(HomePushMoneyDTO homePushMoneyDTO) {
        TokenUser user = commonService.getLoginInfo();
        homePushMoneyDTO.setClient(user.getClient());
        homePushMoneyDTO.setSalesclerk(user.getUserId());
        /**
         * 测试数据
         */
//        homePushMoneyDTO.setClient("10000001");
//        homePushMoneyDTO.setSalesclerk("10000001");
//        homePushMoneyDTO.setType("1");
//        homePushMoneyDTO.setStartDate("20210101");
//        homePushMoneyDTO.setEndDate("20210108");

//        homePushMoneyDTO.setSuitMonth("202101");


        String startDate = "";
        String endDate = "";
        if ("1".equals(homePushMoneyDTO.getType())) { //1 是今日
            endDate = DateUtils.getCurrentDateStrYYMMDD();
        } else if ("2".equals(homePushMoneyDTO.getType())) {
            endDate = DateUtils.getBeforeDateStrYYMMDD();
        }
        startDate = DateUtils.getMonthStartDataStr();

        homePushMoneyDTO.setStartDate(startDate);
        homePushMoneyDTO.setEndDate(endDate);
        homePushMoneyDTO.setSuitMonth(DateUtils.getCurrentYearMonthStr());

        HomePushMoneyVo pushMoneySale = this.saleDMapper.selectMonthSalesBySales(homePushMoneyDTO);
        HomePushMoneyVo pushMoneyPro = this.saleDMapper.selectMonthSalesByPro(homePushMoneyDTO);
        BigDecimal deductionWagePro = BigDecimal.ZERO;
        if (ObjectUtils.isNotEmpty(pushMoneyPro)) {
            deductionWagePro = DecimalUtils.stripTrailingZeros(pushMoneyPro.getDeductionWagePro());
        }

        if (ObjectUtils.isNotEmpty(pushMoneySale)) {

            pushMoneySale.setDeductionWagePro(deductionWagePro);
            pushMoneySale.setDeductionWage(DecimalUtils.stripTrailingZeros(pushMoneySale.getDeductionWageSales()).
                    add(deductionWagePro));
        } else {
            ResultUtil.success(null);
        }

        return ResultUtil.success(pushMoneySale);
    }

    @Override
    public Result getMonthPushMoneyChart(HomePushMoneyDTO homePushMoneyDTO) {
        TokenUser user = commonService.getLoginInfo();
        homePushMoneyDTO.setClient(user.getClient());
        homePushMoneyDTO.setSalesclerk(user.getUserId());
        LocalDate currentYear = LocalDate.now(); // 今年
        LocalDate lastYear = LocalDate.now().plusYears(-1); // 去年同期

        /**
         * 测试数据
         */
//        LocalDate currentYear =  LocalDate.of(2021,2,2); // 今年
//        LocalDate lastYear =LocalDate.of(2021,2,2).plusYears(-1); // 去年同期
////        homePushMoneyDTO.setType("5");
//        homePushMoneyDTO.setClient("10000001");
//        homePushMoneyDTO.setStoCode("1000000001");
//        homePushMoneyDTO.setSalesclerk("10000001");


        String startDate = "";
        String endDate = "";
        String setLastWeekStartDate = "";
        String setLastWeekEndDate = "";

        if ("3".equals(homePushMoneyDTO.getType())) { //3  日
            startDate = DateUtils.getFirstDayOfWeek(currentYear);
            endDate = DateUtils.getLastDayOfWeek(currentYear);
            setLastWeekStartDate = DateUtils.getFirstDayOfWeek(lastYear);
            setLastWeekEndDate = DateUtils.getLastDayOfWeek(lastYear);
        } else if ("5".equals(homePushMoneyDTO.getType())) {//5 今年
            startDate = DateUtils.getFirstDayOfYear(currentYear);
            endDate = DateUtils.getLastDayOfYear(currentYear);
            setLastWeekStartDate = DateUtils.getFirstDayOfYear(lastYear);
            setLastWeekEndDate = DateUtils.getLastDayOfYear(lastYear);
        }

        homePushMoneyDTO.setStartDate(startDate);
        homePushMoneyDTO.setEndDate(endDate);
        homePushMoneyDTO.setLastWeekStartDate(setLastWeekStartDate);
        homePushMoneyDTO.setLastWeekEndDate(setLastWeekEndDate);

        List<HomePushMoneyVo> resultData = new ArrayList<>();

        List<HomePushMoneyVo> pushMoneySales = this.saleDMapper.selectMonthSalesChartBySales(homePushMoneyDTO);

        List<HomePushMoneyVo> pushMoneyPros = this.saleDMapper.selectMonthSalesChartByPros(homePushMoneyDTO);

        /**
         * 1.先查销售的提成
         * 2.再查单品的提成
         * 3.拼接数据
         */
        if (ObjectUtils.isNotEmpty(pushMoneySales)) {
            for (HomePushMoneyVo pushMoneySale : pushMoneySales) {
                HomePushMoneyVo outData = new HomePushMoneyVo();
                BigDecimal deductionWagePro = BigDecimal.ZERO;
                outData = pushMoneySale;
                if (ObjectUtils.isNotEmpty(pushMoneyPros)) {
                    for (HomePushMoneyVo pushMoneyPro : pushMoneyPros) {
                        if (
//                                pushMoneySale.getStoCode().equals(pushMoneyPro.getStoCode())&&
                                pushMoneySale.getGssdType().equals(pushMoneyPro.getGssdType())
                                        && pushMoneySale.getGssdDate().equals(pushMoneyPro.getGssdDate())) {
                            deductionWagePro = DecimalUtils.stripTrailingZeros(pushMoneyPro.getDeductionWagePro());
                        }
                    }
                }
                outData.setDeductionWagePro(deductionWagePro);
                outData.setDeductionWage(DecimalUtils.stripTrailingZeros(pushMoneySale.getDeductionWageSales()).
                        add(deductionWagePro));
                resultData.add(outData);
            }
        }

        List<HomePushMoneyVo> resultList = new ArrayList<>();
        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 1);

        if (resultData.size() > 0) {
            Map<String, List<HomePushMoneyVo>> mapList = resultData.stream().collect(Collectors.groupingBy(HomePushMoneyVo::getGssdType));
            if (homePushMoneyDTO.getType().equals("3")) {
                for (Map.Entry<String, List<HomePushMoneyVo>> entry : mapList.entrySet()) {
                    List<HomePushMoneyVo> entryList = entry.getValue();
                    for (int i = 1; i <= 7; i++) {
                        boolean flag = false;
                        for (HomePushMoneyVo vo : entryList) {
                            if (DateUtils.parseDate(vo.getGssdDate()).get(weekFields.dayOfWeek()) == i) {
                                flag = true;
                                resultList.add(vo);
                                break;
                            }
                        }
                        if (!flag) {
                            HomePushMoneyVo homePushMoneyVo = new HomePushMoneyVo();
                            homePushMoneyVo.setGssdType(entry.getKey());
                            resultList.add(homePushMoneyVo);
                        }

                    }
                }

            } else if (homePushMoneyDTO.getType().equals("5")) {
                for (Map.Entry<String, List<HomePushMoneyVo>> entry : mapList.entrySet()) {
                    List<HomePushMoneyVo> entryList = entry.getValue();
                    for (int i = 1; i <= 12; i++) {
                        boolean flag = false;
                        for (HomePushMoneyVo vo : entryList) {
                            if (Integer.parseInt(vo.getGssdDate()) == i) {
                                flag = true;
                                vo.setGssdDate(String.valueOf(i));
                                resultList.add(vo);
                                break;
                            }
                        }
                        if (!flag) {
                            HomePushMoneyVo homePushMoneyVo = new HomePushMoneyVo();
                            homePushMoneyVo.setGssdType(entry.getKey());
                            homePushMoneyVo.setGssdDate(String.valueOf(i));
                            resultList.add(homePushMoneyVo);
                        }

                    }
                }
            }
        }


//        System.out.println(resultData.toString());
//        System.out.println(resultList.toString());

        return ResultUtil.success(resultList);
    }

    @Override
    public Result loginAppSaleTotal(HomeChartRequestDTO homeChartRequestDTO) {
        log.info("APP看板接口调用");
        TokenUser user = commonService.getLoginInfo();
        homeChartRequestDTO.setClientId(user.getClient());

        HomePushMoneyDTO homePushMoneyDTO = new HomePushMoneyDTO();
//        TokenUser user = new TokenUser();
//        user.setClient("10000013");
//        homeChartRequestDTO = new HomeChartRequestDTO();
//        homeChartRequestDTO.setStoreId("1000000001");
//        homeChartRequestDTO.setType(5);
        HomeSalesNewVo homeSalesNewVo = new HomeSalesNewVo();
        homePushMoneyDTO.setClient(user.getClient());
        homePushMoneyDTO.setType(homeChartRequestDTO.getType().toString());
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            homePushMoneyDTO.setStoCode(homeChartRequestDTO.getStoreId());
        }
        String dateTime = "";
        log.info("报表参数：{}", JSONObject.toJSONString(homeChartRequestDTO));
        List<HomeSalesVo> homeSalesVo = new ArrayList<>();
        switch (homeChartRequestDTO.getType()) {
            case 1://今日
                dateTime = DateUtils.getCurrentDateStrYYMMDD();
                //销售
                homeSalesVo = getHomeSalesListByCurrentNew(homeChartRequestDTO);
                break;
            case 2:
                dateTime = DateUtils.getBeforeDateStrYYMMDD();
                //销售
                homeSalesVo = getHomeSalesListByYesterdayNew(homeChartRequestDTO);
                break;
        }
        if (ObjectUtils.isNotEmpty(homeSalesVo)) {
            homeSalesNewVo.setGssdAmt(homeSalesVo.get(0).getGssdAmt());
            homeSalesNewVo.setGssdGrossAmt(homeSalesVo.get(0).getGssdGrossAmt());
            homeSalesNewVo.setGssdGrossMar(homeSalesVo.get(0).getGssdGrossMar());
            homeSalesNewVo.setGssdTrans(homeSalesVo.get(0).getGssdTrans());
            homeSalesNewVo.setGssdAprice(homeSalesVo.get(0).getGssdAprice());
            homeSalesNewVo.setGssdStoreCount(homeSalesVo.get(0).getGssdStoreCount());
        }
        //会员卡数
        int cartCount = 0;
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            cartCount = sdMemberCardMapper.selectCardCountByDate(homeChartRequestDTO.getStoreId(), homeChartRequestDTO.getStoreId(), dateTime);
        } else {
            cartCount = sdMemberCardMapper.selectCardCountByDate("", homeChartRequestDTO.getStoreId(), dateTime);
        }
        homeSalesNewVo.setMcardQty(new BigDecimal(cartCount));
        //销售提成
        HomePushMoneyVo homePushMoneyVo = getMonthPushMoney(homePushMoneyDTO);
        if (ObjectUtils.isNotEmpty(homePushMoneyVo)) {
            homeSalesNewVo.setDeductionWage(homePushMoneyVo.getDeductionWage());
            homeSalesNewVo.setDeductionWageSales(homePushMoneyVo.getDeductionWageSales());
            homeSalesNewVo.setDeductionWagePro(homePushMoneyVo.getDeductionWagePro());
        }
        //月计划
        LoginSalePlan loginSalePlan = getLoginSalePlanByMonth(homeChartRequestDTO);
        homeSalesNewVo.setTAmt(loginSalePlan.gettAmt());
        homeSalesNewVo.setTGross(loginSalePlan.gettGross());
        homeSalesNewVo.setTMcardQty(loginSalePlan.gettMcardQty());
        homeSalesNewVo.setAAmt(loginSalePlan.getaAmt());
        homeSalesNewVo.setAGross(loginSalePlan.getaGross());
        homeSalesNewVo.setAMcardQty(loginSalePlan.getaMcardQty());
        homeSalesNewVo.setRAmt(loginSalePlan.getrAmt());
        homeSalesNewVo.setRGross(loginSalePlan.getrGross());
        homeSalesNewVo.setRMcardQty(loginSalePlan.getrMcardQty());
        homeSalesNewVo.setDay(loginSalePlan.getDay());
        return ResultUtil.success(homeSalesNewVo);
    }

    @Override
    public Result getStoListByUserAuth() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = storeDataMapper.getStoListByUserAuth(user.getClient(), user.getUserId());
        return ResultUtil.success(list);
    }

    @Override
    public Result getHomeSalesListNew(HomeChartRequestDTO homeChartRequestDTO) {
        log.info("APP看板接口调用");
        TokenUser user = commonService.getLoginInfo();

//        TokenUser user = new TokenUser();
//        user.setClient("10000013");
//        homeChartRequestDTO = new HomeChartRequestDTO();
//        homeChartRequestDTO.setStoreId("1000000001");
//        homeChartRequestDTO.setType(5);
        List<HomeSalesVo> list = new ArrayList<>();
        log.info("报表参数：{}", JSONObject.toJSONString(homeChartRequestDTO));
        switch (homeChartRequestDTO.getType()) {
            case 1:
                list = getNewHomeSalesListByCurrent(user);
                break;
            case 2:
                list = getNewHomeSalesListByYesterday(user);
                break;
            case 3:
                list = getNewHomeSalesListByDay(user, homeChartRequestDTO);
                break;
            case 4:
                list = getNewHomeSalesListByWeek(user, homeChartRequestDTO);
                break;
            case 5:
                list = getNewHomeSalesListByMonth(user, homeChartRequestDTO);
                break;
            default:
                break;
        }
        return ResultUtil.success(list);
    }

    /**
     * 当前用户的权限下的门店编码集合
     *
     * @return
     */
    public List<String> getStoCodeList() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = storeDataMapper.getStoListByUserAuth(user.getClient(), user.getUserId());
        List<String> codeList = list.stream().map(StoreData::getStoCode).collect(Collectors.toList());
        return codeList;
    }

    /**
     * 查销售提成的 营业额 交易次数 客单价
     * 暂时不用
     *
     * @param user
     * @return
     */
    private HomeSalesVo getHomeSalesListByCurrentAndSalerId(TokenUser user, String saleDate) {
        StoreData storeData = null;
        if (StringUtils.isNotBlank(user.getDepId())) {
            storeData = storeDataMapper.getUserStore(user.getClient(), user.getUserId());
        }

        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + user.getClient() + "' ")
                .append(" and GSSD_SALER_ID = '" + user.getUserId() + "'");

        if (storeData != null) {
            builder.append(" and GSSD_BR_ID = '" + user.getDepId() + "'");
        }

        builder.append(" and gssd_date = '" + saleDate + "' ")
                .append(" group by client,GSSD_SALER_ID ");
        log.info("报表今日数据：{}", builder.toString());
        HomeSalesVo list = kylinJdbcTemplate.queryForObject(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(list));

        return list;
    }

    public HomePushMoneyVo getMonthPushMoney(HomePushMoneyDTO homePushMoneyDTO) {
        /**
         * 测试数据
         */
//        homePushMoneyDTO.setClient("10000001");
//        homePushMoneyDTO.setSalesclerk("10000001");
//        homePushMoneyDTO.setType("1");
//        homePushMoneyDTO.setStartDate("20210101");
//        homePushMoneyDTO.setEndDate("20210108");

//        homePushMoneyDTO.setSuitMonth("202101");


        String startDate = "";
        String endDate = "";
        if ("1".equals(homePushMoneyDTO.getType())) { //1 是今日
            endDate = DateUtils.getCurrentDateStrYYMMDD();
        } else if ("2".equals(homePushMoneyDTO.getType())) {
            endDate = DateUtils.getBeforeDateStrYYMMDD();
        }
        startDate = DateUtils.getMonthStartDataStr();

        homePushMoneyDTO.setStartDate(startDate);
        homePushMoneyDTO.setEndDate(endDate);
        homePushMoneyDTO.setSuitMonth(DateUtils.getCurrentYearMonthStr());

        HomePushMoneyVo pushMoneySale = this.saleDMapper.selectMonthSalesBySales(homePushMoneyDTO);
        HomePushMoneyVo pushMoneyPro = this.saleDMapper.selectMonthSalesByPro(homePushMoneyDTO);
        BigDecimal deductionWagePro = BigDecimal.ZERO;
        if (ObjectUtils.isNotEmpty(pushMoneyPro)) {
            deductionWagePro = DecimalUtils.stripTrailingZeros(pushMoneyPro.getDeductionWagePro());
        }

        if (ObjectUtils.isNotEmpty(pushMoneySale)) {
            pushMoneySale.setDeductionWagePro(deductionWagePro);
            pushMoneySale.setDeductionWage(DecimalUtils.stripTrailingZeros(pushMoneySale.getDeductionWageSales()).
                    add(deductionWagePro));
        }

        return pushMoneySale;
    }

    public LoginSalePlan getLoginSalePlanByMonth(HomeChartRequestDTO homeChartRequestDTO) {
        LoginSalePlan data = new LoginSalePlan();

//        String client = "10000001";
//        String startData = "2020-01-01";
//        String endData = "2020-12-31";
        int day = 0;//获取当前月的自然天数
        int day1 = 0;//获取当前日期实际天数
        String startData = DateUtils.getMonthStartData();
        String endData = DateUtils.getMonthEndData();
        if (homeChartRequestDTO.getType() == 2) {
            Date today = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 24);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            endData = simpleDateFormat.format(today);//获取昨天日期
            day1 = Integer.valueOf(endData.substring(8));
            try {
                day = getDaysOfMonth(simpleDateFormat.parse(endData));
            } catch (ParseException e) {
                e.getMessage();
            }
            String dateStr = endData.substring(0, 8);
            startData = dateStr + "01";
        } else if (homeChartRequestDTO.getType() == 1) {
            Date today = new Date(System.currentTimeMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            endData = simpleDateFormat.format(today);//获取今天日期
            day1 = Integer.valueOf(endData.substring(9));
            try {
                day = getDaysOfMonth(simpleDateFormat.parse(endData));
            } catch (ParseException e) {
                e.getMessage();
            }
        }

//        TokenUser user = commonService.getLoginInfo();
        SalePlan plan = new SalePlan();//计划数据
        EffectiveSale effectiveSale = new EffectiveSale();
        StringBuilder builder = new StringBuilder()
                .append(" select a.client, ");
        if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" a.gssd_br_id as \"brId\", ");
        }
        builder.append(" a.gcd_month as \"month\",")
                .append(" a.gssd_days as \"day\",sum( a.GSSD_AMT ) AS \"aAmt\",sum( a.GSSD_GROSS_AMT ) AS \"aGross\", ")
                .append(" b.t_amt as \"tAmt\" , ")
                .append(" b.t_gross as \"tGross\", ")
                .append(" b.t_mcard_qty as \"tMcardQty\" ")
                .append(" from ")
                /**  START
                 *第一张表开始 a,
                 */
                .append(" ( select ")
                .append(" GAIA_SD_SALE_D.client as client, ")//客户编号/加盟商编号
                .append(" D.GCD_YEAR||D.GCD_MONTH as gcd_month,GSSD_BR_ID, ");// 销售日期(年周)
        builder.append(" count(distinct GSSD_DATE) as GSSD_DAYS,")//销售天数
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")// 销售额
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT ")//毛利额
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_STORE_DATA as S ")
                .append(" ON GAIA_SD_SALE_D.CLIENT = S.CLIENT AND GAIA_SD_SALE_D.GSSD_BR_ID = S.STO_CODE ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ")
                .append(" and gssd_date BETWEEN '" + startData + "' AND '" + endData + "' ")
                .append(" and GAIA_SD_SALE_D.client='" + homeChartRequestDTO.getClientId() + "' ");
        if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID='" + homeChartRequestDTO.getStoreId() + "' ");
        }
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList())){
            builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID in (");
            for (int i = 0; i < homeChartRequestDTO.getStoCodeList().size(); i++){
                if (i == homeChartRequestDTO.getStoCodeList().size()-1){
                    builder.append("'" +homeChartRequestDTO.getStoCodeList().get(i)+ "'");
                }else {
                    builder.append("'" +homeChartRequestDTO.getStoCodeList().get(i)+ "',");
                }
            }
            builder.append(" ) ");
        }
        builder.append(" group by GAIA_SD_SALE_D.client, GSSD_BR_ID ");
        if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GAIA_SD_SALE_D.GSSD_BR_ID ");
        }
        builder.append(" ,D.GCD_YEAR||D.GCD_MONTH ")
                .append(" order by D.GCD_YEAR||D.GCD_MONTH asc ) as a ")
                /**END
                 *第一张表结束 a
                 */
                /**  START
                 *第二张表开始 b
                 */
                .append(" LEFT JOIN (select ")
                .append(" client,")
                .append(" month_plan, sto_code,");
        builder.append(" sum(T_SALE_AMT) as t_amt, ")
                .append(" sum(T_SALE_GROSS) as t_gross, ")
                .append(" sum(T_MCARD_QTY) as t_mcard_qty ")
                .append("  from GAIA_SALETASK_DPLAN ")
                .append(" group by client,sto_code,month_plan ");
        builder.append(" ) as b")

                /**END
                 *第二张表结束 a
                 */
                .append(" on a.client = b.client AND a.GSSD_BR_ID = b.sto_code")
                .append(" and a.gcd_month = b.month_plan GROUP BY a.client, a.gcd_month,a.gssd_days,b.t_amt,b.t_gross,b.t_mcard_qty");
        System.out.println(builder.toString());
        List<SalePlan> salePlans = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalePlan.class));
        if (ObjectUtils.isNotEmpty(salePlans)) {
            System.out.println(salePlans.get(0).toString());
            plan = salePlans.get(0);
            data.setMonth(plan.getMonth());
            data.setDay(plan.getDay());
            if (StringUtils.isNotEmpty(plan.gettAmt())) {
                data.settAmt(new BigDecimal(plan.gettAmt()));
            }
            if (StringUtils.isNotEmpty(plan.gettGross())) {
                data.settGross(new BigDecimal(plan.gettGross()));
            }
            if (StringUtils.isNotEmpty(plan.gettMcardQty())) {
                data.settMcardQty(new BigDecimal(plan.gettMcardQty()));
            }
            data.setaAmt(new BigDecimal(plan.getaAmt()));
            data.setaGross(new BigDecimal(plan.getaGross()));
        }
        Map map = new HashMap();
        map.put("client", homeChartRequestDTO.getClientId());
        map.put("depId", homeChartRequestDTO.getStoreId());
        map.put("startData", startData);
        map.put("endData", endData);
        effectiveSale = saleDService.selectSalePlan(map);
//        if (ObjectUtils.isNotEmpty(effectiveSale)) {
            //实际数据

            data.setaMcardQty(new BigDecimal(effectiveSale.getCardQty()));
            //百分比
//            data.setrAmt((data.getaAmt().divide(data.gettAmt()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
//            data.setrGross((data.getaGross().divide(data.gettGross()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
//            data.setrMcardQty((data.getaMcardQty().divide(data.getaMcardQty()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");

//            data.setrAmt( DecimalUtils.toPercentStr(data.getaAmt().divide(data.gettAmt(),2,BigDecimal.ROUND_DOWN)));
//            data.setrGross(DecimalUtils.toPercentStr(data.getaGross().divide(data.gettGross(),2,BigDecimal.ROUND_DOWN)));
//            data.setrMcardQty(DecimalUtils.toPercentStr(data.getaMcardQty().divide(data.getaMcardQty(),2,BigDecimal.ROUND_DOWN)));
            //日均达成率
            if (ObjectUtils.isNotEmpty(data.gettAmt())) {
                data.setrAmt(DecimalUtils.toPercentStr((data.getaAmt().divide(new BigDecimal(day1), 4, BigDecimal.ROUND_DOWN)).divide(data.gettAmt().divide(new BigDecimal(day), 4, BigDecimal.ROUND_DOWN), 2, BigDecimal.ROUND_DOWN)));
            } else {
                data.setrAmt("0.00");
            }
            if (ObjectUtils.isNotEmpty(data.gettGross()) && data.gettGross().compareTo(BigDecimal.ZERO) != 0) {
                data.setrGross(DecimalUtils.toPercentStr(data.getaGross().divide(new BigDecimal(day1), 4, BigDecimal.ROUND_DOWN).divide(data.gettGross().divide(new BigDecimal(day), 4, BigDecimal.ROUND_DOWN), 2, BigDecimal.ROUND_DOWN)));
            } else {
                data.setrGross("0.00");
            }
            if (ObjectUtils.isNotEmpty(data.gettMcardQty()) && data.gettMcardQty().compareTo(BigDecimal.ZERO) != 0) {
                data.setrMcardQty(DecimalUtils.toPercentStr(data.getaMcardQty().divide(new BigDecimal(day1), 4, BigDecimal.ROUND_DOWN).divide(data.gettMcardQty().divide(new BigDecimal(day), 4, BigDecimal.ROUND_DOWN), 2, BigDecimal.ROUND_DOWN)));
            } else {
                data.setrMcardQty("0.00");
            }


            //订单数量
            data.setNoQty(effectiveSale.getNoQty());
            if (ObjectUtils.isEmpty(plan.getDay())) {
                plan.setDay("0");
            }
            //日均交易
            data.setDayQty(String.valueOf(new BigDecimal(effectiveSale.getNoQty()).multiply(new BigDecimal(plan.getDay())).setScale(2, BigDecimal.ROUND_HALF_UP)));
            //客单价
            data.setPct(String.valueOf(new BigDecimal(effectiveSale.getAmt()).multiply(new BigDecimal(effectiveSale.getNoQty())).setScale(4, BigDecimal.ROUND_HALF_UP)));
//        }
//        if(StringUtils.isNotEmpty(depId)){
//            List<ShopAssistantOutData> shopAssistants = this.getShopAssistantSalePlan(map);
//            data.setShopAssistants(shopAssistants);
//        }
        return data;
    }

    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
}
