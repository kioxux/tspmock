package com.gov.operate.service.impl;

import com.gov.operate.entity.SdStockBatch;
import com.gov.operate.mapper.SdStockBatchMapper;
import com.gov.operate.service.ISdStockBatchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
@Service
public class SdStockBatchServiceImpl extends ServiceImpl<SdStockBatchMapper, SdStockBatch> implements ISdStockBatchService {

}
