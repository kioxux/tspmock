package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.DtSupplierPrepayment;
import com.gov.operate.mapper.DtSupplierPrepaymentMapper;
import com.gov.operate.service.IDtSupplierPrepaymentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-09-09
 */
@Service
public class DtSupplierPrepaymentServiceImpl extends ServiceImpl<DtSupplierPrepaymentMapper, DtSupplierPrepayment> implements IDtSupplierPrepaymentService {

}
