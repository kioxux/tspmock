package com.gov.operate.controller;

import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.ISalesAnalysisService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 分析报表
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@RestController
@RequestMapping("salesAnalysis")
public class SalesAnalysisController {

    @Resource
    private ISalesAnalysisService iSalesAnalysisService;

    @PostMapping("getStoreReportByMonth")
    @ApiOperation(value = "门店报表，当前月份明细")
    public Result getStoreReportByMonth() {
        return iSalesAnalysisService.getStoreReportByMonth();
    }

    @PostMapping("getStoreReportByWeek")
    @ApiOperation(value = "门店报表，当前周明细")
    public Result getStoreReportByWeek() {
        return iSalesAnalysisService.getStoreReportByWeek();
    }

    @PostMapping("getSummaryReportByMonth")
    @ApiOperation(value = "汇总报表，三个月数据")
    public Result getSummaryReportByMonth() {
        return iSalesAnalysisService.getSummaryReportByMonth();
    }

    @PostMapping("getSummaryReportByWeek")
    @ApiOperation(value = "汇总报表，十二周数据")
    public Result getSummaryReportByWeek() {
        return iSalesAnalysisService.getSummaryReportByWeek();
    }

    @PostMapping("getSaleRankTopTenByStore")
    @ApiOperation(value = "汇总报表，销售前sum商品")
    public Result getSaleRankTopTenByStore(@RequestBody Map<String,Object> map) {
        return ResultUtil.success(iSalesAnalysisService.getSaleRankTopTenByStore(map));
    }

    @PostMapping("getLoginSalePlanByMonth")
    @ApiOperation(value = "汇总报表,销售计划")
    public Result getSalePlan(@RequestJson(value = "depId", name = "门店",required = false) String depId) {
        return ResultUtil.success(iSalesAnalysisService.getLoginSalePlanByMonth(depId));
    }

    @PostMapping("getLoginSalePlanByDay")
    @ApiOperation(value = "汇总报表,销售计划")
    public Result getLoginSalePlanByDay(@RequestJson(value = "depId", name = "门店",required = false) String depId) {
        return ResultUtil.success(iSalesAnalysisService.getLoginSalePlanByDay(depId));
    }
}

