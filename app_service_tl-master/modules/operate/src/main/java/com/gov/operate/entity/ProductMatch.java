package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 导入商品匹配信息表
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_MATCH")
@ApiModel(value="ProductMatch对象", description="导入商品匹配信息表")
public class ProductMatch extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "匹配编码")
    @TableField("MATCH_CODE")
    private String matchCode;

    @ApiModelProperty(value = "匹配药德商品编码")
    @TableField("MATCH_PRO_CODE")
    private String matchProCode;

    @ApiModelProperty(value = "匹配类型 0-未匹配，1-部分匹配，2-完全匹配")
    @TableField("MATCH_TYPE")
    private String matchType;

    @ApiModelProperty(value = "匹配度")
    @TableField("MATCH_DEGREE")
    private String matchDegree;

    @ApiModelProperty(value = "药德商品编码")
    @TableField("PRO_CODE")
    private String proCode;

    @ApiModelProperty(value = "商品处理状态 0-未处理，1-自动处理，2-手动处理")
    @TableField("PRO_MATCH_STATUS")
    private String proMatchStatus;

    @ApiModelProperty(value = "商品名")
    @TableField("PRO_NAME")
    private String proName;

    @ApiModelProperty(value = "规格")
    @TableField("PRO_SPECS")
    private String proSpecs;

    @ApiModelProperty(value = "计量单位")
    @TableField("PRO_UNIT")
    private String proUnit;

    @ApiModelProperty(value = "国际条形码1")
    @TableField("PRO_BARCODE")
    private String proBarcode;

    @ApiModelProperty(value = "批准文号")
    @TableField("PRO_REGISTER_NO")
    private String proRegisterNo;

    @ApiModelProperty(value = "生产企业")
    @TableField("PRO_FACTORY_NAME")
    private String proFactoryName;

    @ApiModelProperty(value = "创建人")
    @TableField("MATCH_CREATER")
    private String matchCreater;

    @ApiModelProperty(value = "创建日期")
    @TableField("MATCH_CREATE_DATE")
    private String matchCreateDate;


}
