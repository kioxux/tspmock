package com.gov.operate.mapper;

import com.gov.operate.entity.PaymentVatApplications;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
public interface PaymentVatApplicationsMapper extends BaseMapper<PaymentVatApplications> {

}
