package com.gov.operate.dto.storePayment;

import com.gov.operate.dto.PaymentRecordDetail;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.18
 */
@Data
public class PaymentApproval {

    private List<PaymentRecordDetail> list;

    /**
     * 配送中心
     */
    private String site;

    /**
     * 门店
     */
    private String stoCode;
}
