package com.gov.operate.mapper;

import com.gov.operate.entity.SystemPara;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统模块参数表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-24
 */
public interface SystemParaMapper extends BaseMapper<SystemPara> {

}
