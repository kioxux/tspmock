package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.AppVersionDTO;
import com.gov.operate.dto.EditAppVO;
import com.gov.operate.dto.GetAppListVO;
import com.gov.operate.dto.SaveAppVO;
import com.gov.operate.entity.AppVersion;
import com.gov.operate.mapper.AppVersionMapper;
import com.gov.operate.service.IAppVersionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
@Service
public class AppVersionServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements IAppVersionService {

    @Resource
    private AppVersionMapper appVersionMapper;
    @Resource
    private CosUtils cosUtils;
    /**
     * APP列表
     */
    @Override
    public IPage<AppVersionDTO> getAppList(GetAppListVO vo) {
        Page<AppVersionDTO> page = new Page<AppVersionDTO>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<AppVersionDTO> query = new QueryWrapper<AppVersionDTO>()
                // 版本号
                .eq(StringUtils.isNotEmpty(vo.getVersionCode()), "app.VERSION_CODE", vo.getVersionCode())
                // 发布时间开始时间
                .apply(StringUtils.isNotEmpty(vo.getReleaseTimeStart()), "left(app.RELEASE_TIME, 8) >= {0}", vo.getReleaseTimeStart())
                // 发布时间结束时间
                .apply(StringUtils.isNotEmpty(vo.getReleaseTimeEnd()), "left(app.RELEASE_TIME, 8) <= {0}", vo.getReleaseTimeEnd())
                // 是否强制更新
                .eq(!ObjectUtils.isEmpty(vo.getForceUpdateFlg()), "app.FORCE_UPDATE_FLG", vo.getForceUpdateFlg())
                // 是否发布
                .eq(!ObjectUtils.isEmpty(vo.getReleaseFlag()), "app.RELEASE_FLAG", vo.getReleaseFlag())
                // 平台类型
                .eq(!ObjectUtils.isEmpty(vo.getType()), "app.TYPE", vo.getType())
                // 更新内容模糊查询
                .apply(StringUtils.isNotEmpty(vo.getContent()), "instr(app.CONTENT, {0})", vo.getContent());

        IPage<AppVersionDTO> iPage = appVersionMapper.getAppList(page, query);
        iPage.getRecords().forEach(item->{
            String url = cosUtils.urlAuth(item.getDownloadPath());
            item.setDownloadPathUrl(url);
        });
        return iPage;
    }

    /**
     * 新增
     */
    @Override
    public void saveApp(SaveAppVO vo) {
        AppVersion appLastVersion = appVersionMapper.getAppLastVersionExcludeSelf(vo.getType());
        if (!ObjectUtils.isEmpty(appLastVersion)) {
            boolean isVersion = this.checkVersion(appLastVersion.getVersionCode(), vo.getVersionCode());
            if (!isVersion) {
                throw new CustomResultException("版本号必须高于当前发布版本，请重新填写版本号");
            }
        }
        List<AppVersion> list = this.list(new QueryWrapper<AppVersion>().eq("TYPE", vo.getType()).eq("VERSION_CODE", vo.getVersionCode()));
        if (!CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("版本号不能重复");
        }
        AppVersion appVersion = new AppVersion();
        BeanUtils.copyProperties(vo,appVersion);
        // id
        appVersion.setId(UUIDUtil.getUUID());
        // 是否发布，默认1:否
        appVersion.setReleaseFlag(Integer.valueOf(OperateEnum.ReleaseFlag.NO.getCode()));
        this.save(appVersion);
    }

    /**
     * APP版本编辑
     */
    @Override
    public void editApp(EditAppVO vo) {

        AppVersion version = this.getById(vo.getId());
        if (ObjectUtils.isEmpty(version)) {
            throw new CustomResultException("Id参数有误,没有该APP版本");
        }
        if (Integer.valueOf(OperateEnum.ReleaseFlag.YES.getCode()).equals(version.getReleaseFlag())) {
            throw new CustomResultException("该版本已经发布，不可编辑");
        }
        AppVersion appVersionES = appVersionMapper.getAppLastVersionExcludeSelf(vo.getType());
        if (!ObjectUtils.isEmpty(appVersionES)) {
            boolean isVersion = this.checkVersion(appVersionES.getVersionCode(), vo.getVersionCode());
            if (!isVersion) {
                throw new CustomResultException("版本号必须高于当前发布版本，请重新填写版本号");
            }
        }
        QueryWrapper<AppVersion> query = new QueryWrapper<AppVersion>()
                .eq("TYPE", vo.getType())
                .eq("VERSION_CODE", vo.getVersionCode())
                .ne("ID", vo.getId());
        List<AppVersion> list = this.list(query);
        if (!CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("版本号不能重复");
        }
        AppVersion appVersion = new AppVersion();
        BeanUtils.copyProperties(vo,appVersion);
        this.updateById(appVersion);
    }

    /**
     * APP版本删除
     */
    @Override
    public void removeApp(String id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        AppVersion version = this.getById(id);
        if (Integer.valueOf(OperateEnum.ReleaseFlag.YES.getCode()).equals(version.getReleaseFlag())) {
            throw new CustomResultException("该版本已经发布，不可删除");
        }
        this.removeById(id);
    }

    /**
     * APP版本发布
     */
    @Override
    public void releaseApp(String id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        AppVersion version = this.getById(id);
        if (ObjectUtils.isEmpty(version)) {
            throw new CustomResultException("Id参数有误,未查询到该APP版本");
        }
        if (Integer.valueOf(OperateEnum.ReleaseFlag.YES.getCode()).equals(version.getReleaseFlag())) {
            throw new CustomResultException("该版本已经发布，不可重复发布");
        }

        // 版本要高于 当前发布版本
        AppVersion appLastVersion = appVersionMapper.getAppLastVersionExcludeSelf(version.getType());
        if (!ObjectUtils.isEmpty(appLastVersion)) {
            boolean isVersion = this.checkVersion(appLastVersion.getVersionCode(), version.getVersionCode());
            if (!isVersion) {
                throw new CustomResultException("该版本低于现在发布版，不可发布");
            }
        }
        UpdateWrapper<AppVersion> update = new UpdateWrapper<AppVersion>()
                // 是否发布，2:是
                .set("RELEASE_FLAG", Integer.valueOf(OperateEnum.ReleaseFlag.YES.getCode()))
                // 发布时间
                .set("RELEASE_TIME", DateUtils.getCurrentDateTimeStrTwo())

                .eq("ID", id);
        this.update(update);
    }

    /**
     * APP版本详情
     */
    @Override
    public AppVersionDTO getApp(String id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        AppVersion appVersion = this.getById(id);
        AppVersionDTO dto = new AppVersionDTO();
        BeanUtils.copyProperties(appVersion, dto);
        dto.setDownloadPathUrl(cosUtils.urlAuth(dto.getDownloadPath()));
        return dto;
    }

    /**
     * 版本比较
     */
    private boolean checkVersion(String oldVersion, String newVersion) {
        // 现在没有旧版本情况
        if (StringUtils.isEmpty(oldVersion)) {
            return true;
        }
        // 和目前的版本相同情况
        if (newVersion.equals(oldVersion)) {
            return false;
        }
        // 不相同情况
        List<Integer> oldList = Arrays.asList(oldVersion.split("\\.")).stream().map(Integer::valueOf).collect(Collectors.toList());
        List<Integer> newList = Arrays.asList(newVersion.split("\\.")).stream().map(Integer::valueOf).collect(Collectors.toList());
        // 大版本
        if (newList.get(0) > oldList.get(0)) {
            return true;
        }
        // 中间
        if (newList.get(1) > oldList.get(1)) {
            return true;
        }
        // 小版本
        if (newList.get(2) > oldList.get(2)) {
            return true;
        }
        return false;
    }

}
