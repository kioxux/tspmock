package com.gov.operate.mapper;

import com.gov.operate.dto.CleanDrawerDetailDTO;
import com.gov.operate.entity.SdExamineH;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
public interface SdExamineHMapper extends BaseMapper<SdExamineH> {
    List<CleanDrawerDetailDTO> getDrawerList(@Param("client") String client, @Param("store") String store);

    List<CleanDrawerDetailDTO> getList(@Param("client") String client, @Param("store") String store);
}
