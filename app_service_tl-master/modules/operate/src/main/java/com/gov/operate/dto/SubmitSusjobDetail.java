package com.gov.operate.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */
@Data
public class SubmitSusjobDetail {
    @ApiModelProperty(value = "任务ID")
    @NotBlank(message = "任务ID不能为空")
    public String susJobid;
    @ApiModelProperty(value = "任务门店")
    @NotBlank(message = "任务门店不能为空")
    public String susJobbr;
    @ApiModelProperty(value = "加盟商")
    @NotBlank(message = "加盟商不能为空")
    public String client;
    @ApiModelProperty(value = "会员卡号")
    @NotBlank(message = "会员卡号不能为空")
    public String susCardId;

    @ApiModelProperty(value = "维系标签")
    public String susFlgadd;
    @ApiModelProperty(value = "维系结果")
    public String susJobflg;
    @ApiModelProperty(value = "文字维护")
    public String susJobtxt;
    @ApiModelProperty(value = "不再维系")
    public String susFreeze;

}
