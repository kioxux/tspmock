package com.gov.operate.dto.charge;

import com.gov.operate.entity.StoreData;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SaveClientPricePackDTO {

    private String client;

    private Integer sphId;

    private String spSeries;

    private String spId;

    private String spName;

    private String spRemark;

    private String spType;

    private String spFree;

    private String spSend;

    private String payingstoreCode;

    private BigDecimal spOfficialPrice;

    private BigDecimal spContractPrice;

    private Integer spMode;

    private Integer rId;

    private String spModuleDes;

    private List<StoreData> storeDataList;

    public BigDecimal getSpPrice() {
        return spOfficialPrice;
    }

    public BigDecimal getSpPriceRealistic() {
        return spContractPrice;
    }
}
