package com.gov.operate.service.purchase.productReport.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.LambdaUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.purchase.productReport.ProReplenishmentParams;
import com.gov.operate.dto.purchase.productReport.ReplenishmentListResponseDto;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.kylin.RowMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.purchase.productReport.ProductReportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.10.09
 */
@Slf4j
@Service
public class ProductReportServiceImpl implements ProductReportService {

    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;

    @Resource
    private CommonService commonService;

    @Override
    public Result getProReplenishmentList(Integer pageSize, Integer pageNum, String dcCode, String stoCode, String proSelfCode, String proClass,
                                          String lastSupp, String recSupp, String proZdy1, String proZdy2, String proZdy3, String proZdy4, String proZdy5,
                                          String proSlaeClass, String proSclass, Integer proNoPurchase,
                                          Integer proPosition, List<ProReplenishmentParams> proReplenishmentParamsList,
                                          String salesVolumeStartDate, String salesVolumeEndDate, String proPurchaseRate) {
        IPage<ReplenishmentListResponseDto> page = new Page<>();
        page.setSize(pageSize);
        page.setCurrent(pageNum);
        String pageSql = this.exeSql(true, pageSize, pageNum, stoCode, dcCode, proNoPurchase, proPosition, proSelfCode, proClass,
                proZdy1, proZdy2, proZdy3, proZdy4, proZdy5, proSlaeClass, proSclass, proPurchaseRate, recSupp, proReplenishmentParamsList,
                salesVolumeStartDate, salesVolumeEndDate, lastSupp);
        // 总件数
        log.info("商品数据报表分页sql：{}", pageSql);
        Map<String, Object> objectMap = kylinJdbcTemplate.queryForMap(pageSql);
        log.info("商品数据报表分页结果：{}", JSONObject.toJSONString(objectMap));
        page.setTotal(NumberUtils.toLong(objectMap.get("CNT").toString(), 0));
        if ("0".equals(objectMap.get("CNT").toString())) {
            return ResultUtil.success(page);
        }
        String dataSql = this.exeSql(false, pageSize, pageNum, stoCode, dcCode, proNoPurchase, proPosition, proSelfCode, proClass,
                proZdy1, proZdy2, proZdy3, proZdy4, proZdy5, proSlaeClass, proSclass, proPurchaseRate, recSupp, proReplenishmentParamsList,
                salesVolumeStartDate, salesVolumeEndDate, lastSupp);
        log.info("商品数据报表查询sql：{}", dataSql);
        List<ReplenishmentListResponseDto> replenishmentListResponseDtoList = kylinJdbcTemplate.query(dataSql, RowMapper.getDefault(ReplenishmentListResponseDto.class));
        page.setRecords(replenishmentListResponseDtoList);
        return ResultUtil.success(replenishmentListResponseDtoList);
    }

    private String exeSql(boolean isCount, Integer pageSize, Integer pageNum, String stoCode, String dcCode, Integer proNoPurchase, Integer proPosition,
                          String proSelfCode, String proClass, String proZdy1, String proZdy2, String proZdy3, String proZdy4, String proZdy5,
                          String proSlaeClass, String proSclass, String proPurchaseRate, String recSupp, List<ProReplenishmentParams> proReplenishmentParamsList,
                          String salesVolumeStartDate, String salesVolumeEndDate, String lastSupp) {
        TokenUser user = commonService.getLoginInfo();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT ");
        if (isCount) {
            builder.append(" COUNT(1) CNT ");
        } else {
            builder.append(" PRO_SELF_CODE,PRO_NAME,PRO_CLASS,PRO_CLASS_NAME,PROBIGCLASS,PRO_SPECS,PRO_FACTORY_NAME,PRO_INPUT_TAX,PRO_UNIT,PRO_MID_PACKAGE, " +
                    " PRO_ZDY1,PRO_ZDY2,PRO_ZDY3,PRO_ZDY4,PRO_ZDY5,PRO_SLAE_CLASS,PRO_POSITION,PRO_SCLASS,PRO_COMMONNAME,PRO_REGISTER_NO,PRO_COMPCLASS,PRO_COMPCLASS_NAME,PRO_FORM, " +
                    " PRO_BARCODE,PRO_BARCODE2,PRO_PLACE,PRO_STATUS,PRO_BIG_PACKAGE,PRO_NO_PURCHASE,PRO_KEY_CARE,PRO_PRESCLASS,PRO_STORAGE_CONDITION,PRO_CONTROL_CLASS,PRO_LIFE, " +
                    " PRO_LIFE_UNIT,PRO_IF_MED,PRO_ZLBZ,PRO_PURCHASE_RATE,PRO_BRAND,PRO_BEIZHU,PRO_JYGLLB,PRO_DAH,GSPP_PRICE_NORMAL,PRO_BIG_CLASS_NAME, " +
                    " TAX_CODE_NAME,PRO_SCLASS_NAME,DC_CHAIN_HEAD, " +
                    " COALESCE(KUCEN.WM_KCSL, 0) AS DC_STOCK_TOTAL, " +
                    " COALESCE(STOCKSTORE.GSS_QTY, 0) AS STORESTOCKTOTAL, " +
                    " COALESCE(KUCEN.WM_KCSL, 0) + COALESCE(STOCKSTORE.GSS_QTY, 0) AS STOCKTOTAL, " +
                    " COALESCE(MONTHINFO.STORE_SALE_QTY, 0) AS DAYSSALESCOUNT, " +
                    " COALESCE(SEVENDAY.STORE_SALE_QTY, 0) AS DAYSSALESCOUNTSEVEN, " +
                    " COALESCE(QUARTER.STORE_SALE_QTY, 0) AS DAYSSALESCOUNTNINETY, " +
                    " COALESCE(SO_ITEM_HEADER.DAYS_WHOLESALE_COUNT, 0) DAYS_WHOLESALE_COUNT, " +
                    " COALESCE(PO_ITEM_HEADER.TRAFFIC_COUNT, 0) TRAFFIC_COUNT, " +
                    " BATCH_INFO.PO_DATE AS LAST_PURCHASE_DATE, " +
                    " BATCH_INFO.PO_SUPPLIER_ID AS LAST_SUPPLIER_ID, " +
                    " BATCH_INFO.SUP_NAME AS LAST_SUPPLIER, " +
                    " BATCH_INFO.SUP_LEAD_TIME, " +
                    " BATCH_INFO.SUP_PAY_TERM, " +
                    " BATCH_INFO.PO_PRICE AS LAST_PURCHASE_PRICE, " +
                    " COALESCE(PO_ITEM_HEADER_WAIT.PO_WAIT_COUNT, 0) + COALESCE(SO_ITEM_HEADER_WAIT.SO_WAIT_COUNT, 0) AS WAIT_COUNT, " +
                    " HEAD_HY_SET.GSPHS_PRICE, " +
                    " HEAD_HYR_PRICE.GSPHP_PRICE, " +
                    " CASE WHEN MAININFO.GSPP_PRICE_NORMAL IS NOT NULL AND MAININFO.GSPP_PRICE_NORMAL != 0 AND BATCH_INFO.PO_PRICE IS NOT NULL " +
                    " THEN ROUND((MAININFO.GSPP_PRICE_NORMAL - BATCH_INFO.PO_PRICE) / MAININFO.GSPP_PRICE_NORMAL * 100, 2) " +
                    " ELSE NULL END AS GROSSPROFIT, " +
                    " COALESCE(DATEDATA.STORE_SALE_QTY, 0) AS XXSTORESALEQTY, " +
                    " COALESCE(DATEDATA.STO_COUNT, 0) AS XXSTOCOUNT, " +
                    " ROUND(DATEDATA.STO_COUNT / STOREDATA.STO_CNT, 4) AS XXSTOCOUNTPERCENTAGE, " +
                    " COALESCE(STOCKSTOREINFO.GSS_BR_ID_COUNT, 0) AS STOCOUNTHASSTOCK, " +
                    " ROUND(STOCKSTOREINFO.GSS_BR_ID_COUNT / STOREDATA.STO_CNT, 4) AS STOCOUNTHASSTOCKPERCENTAG ");
        }
        builder.append(" FROM ( ");
        builder.append(" SELECT ");
        builder.append(" PROBUSINESS.PRO_SELF_CODE,PROBUSINESS.PRO_NAME,PROBUSINESS.PRO_CLASS,PROBUSINESS.PRO_CLASS_NAME,SUBSTRING(PROBUSINESS.PRO_CLASS,1) AS PROBIGCLASS, " +
                " PROBUSINESS.PRO_SPECS,PROBUSINESS.PRO_FACTORY_NAME,PROBUSINESS.PRO_INPUT_TAX,PROBUSINESS.PRO_UNIT,PROBUSINESS.PRO_MID_PACKAGE, " +
                " PROBUSINESS.PRO_ZDY1,PROBUSINESS.PRO_ZDY2,PROBUSINESS.PRO_ZDY3,PROBUSINESS.PRO_ZDY4,PROBUSINESS.PRO_ZDY5,PROBUSINESS.PRO_SLAE_CLASS, " +
                " PROBUSINESS.PRO_POSITION,PROBUSINESS.PRO_SCLASS,PROBUSINESS.PRO_COMMONNAME,PROBUSINESS.PRO_REGISTER_NO,PROBUSINESS.PRO_COMPCLASS,PROBUSINESS.PRO_COMPCLASS_NAME, " +
                " PROBUSINESS.PRO_FORM,PROBUSINESS.PRO_BARCODE,PROBUSINESS.PRO_BARCODE2,PROBUSINESS.PRO_PLACE,PROBUSINESS.PRO_STATUS, " +
                " PROBUSINESS.PRO_BIG_PACKAGE,PROBUSINESS.PRO_NO_PURCHASE,PROBUSINESS.PRO_KEY_CARE,PROBUSINESS.PRO_PRESCLASS,PROBUSINESS.PRO_STORAGE_CONDITION, " +
                " PROBUSINESS.PRO_CONTROL_CLASS,PROBUSINESS.PRO_LIFE,PROBUSINESS.PRO_LIFE_UNIT,PROBUSINESS.PRO_IF_MED,PROBUSINESS.PRO_ZLBZ, " +
                " PROBUSINESS.PRO_PURCHASE_RATE,PROBUSINESS.PRO_BRAND,PROBUSINESS.PRO_BEIZHU,PROBUSINESS.PRO_JYGLLB,PROBUSINESS.PRO_DAH,PROBUSINESS.PRO_BDZ,PROBUSINESS.CLIENT, " +
                " PROBUSINESS.PRO_SITE,PROPRICE.GSPP_PRICE_NORMAL,PROCLASS.PRO_BIG_CLASS_NAME,TAXCODE.TAX_CODE_NAME,PROSCLASS.PRO_SCLASS_NAME,DCDATA.DC_CHAIN_HEAD ");
        builder.append(" FROM ");
        builder.append(" GAIA_PRODUCT_BUSINESS PROBUSINESS " +
                " INNER JOIN GAIA_DC_DATA DCDATA ON PROBUSINESS.CLIENT = DCDATA.CLIENT AND PROBUSINESS.PRO_SITE = DCDATA.DC_CODE " +
                " LEFT JOIN GAIA_BATCH_INFO BATCHINFO ON PROBUSINESS.CLIENT = BATCHINFO.CLIENT AND PROBUSINESS.PRO_SELF_CODE=BATCHINFO.BAT_PRO_CODE " +
                " LEFT JOIN GAIA_PRODUCT_CLASS PROCLASS ON PROCLASS.PRO_CLASS_CODE = PROBUSINESS.PRO_CLASS " +
                " LEFT JOIN GAIA_TAX_CODE TAXCODE ON PROBUSINESS.PRO_INPUT_TAX = TAXCODE.TAX_CODE " +
                " LEFT JOIN GAIA_SD_PRODUCT_PRICE PROPRICE ON PROBUSINESS.CLIENT = PROPRICE.CLIENT AND PROBUSINESS.PRO_SELF_CODE = PROPRICE.GSPP_PRO_ID " +
                " LEFT JOIN GAIA_PRODUCT_SCLASS PROSCLASS ON PROBUSINESS.CLIENT = PROSCLASS.CLIENT AND PROBUSINESS.PRO_SCLASS=PROSCLASS.PRO_SCLASS_CODE ");
        builder.append(" WHERE ");

        builder.append(" PROBUSINESS.CLIENT='" + user.getClient() + "' ");
        builder.append(" AND PROBUSINESS.PRO_SITE='" + dcCode + "' ");
        builder.append(" AND PROPRICE.GSPP_BR_ID='" + stoCode + "' ");

        if (proNoPurchase != null && proNoPurchase.intValue() == 1) {
            builder.append(" AND PROBUSINESS.PRO_NO_PURCHASE!='1' ");
        }
        if (proPosition != null && proPosition.intValue() == 1) {
            builder.append(" AND PROBUSINESS.PRO_POSITION!='T' ");
            builder.append(" AND PROBUSINESS.PRO_POSITION!='Q' ");
        }
        if (StringUtils.isNotBlank(proSelfCode)) {
            String codes = Arrays.asList(proSelfCode.split(",")).stream().map(s -> "\'" + s + "\'").collect(Collectors.joining(", "));
            builder.append(" AND PROBUSINESS.PRO_SELF_CODE IN (" + codes + ") ");
        }
        if (StringUtils.isNotBlank(proClass)) {
            String codes = Arrays.asList(proClass.split(",")).stream().map(s -> "\'" + s + "\'").collect(Collectors.joining(", "));
            builder.append(" AND PROBUSINESS.PRO_CLASS IN (" + codes + ") ");
        }
        builder.append(StringUtils.isBlank(proZdy1) ? "" : " AND PROBUSINESS.PRO_ZDY1 like '%" + proZdy1 + "%' ");
        builder.append(StringUtils.isBlank(proZdy2) ? "" : " AND PROBUSINESS.PRO_ZDY2 like '%" + proZdy2 + "%' ");
        builder.append(StringUtils.isBlank(proZdy3) ? "" : " AND PROBUSINESS.PRO_ZDY3 like '%" + proZdy3 + "%' ");
        builder.append(StringUtils.isBlank(proZdy4) ? "" : " AND PROBUSINESS.PRO_ZDY4 like '%" + proZdy4 + "%' ");
        builder.append(StringUtils.isBlank(proZdy5) ? "" : " AND PROBUSINESS.PRO_ZDY5 like '%" + proZdy5 + "%' ");
        builder.append(StringUtils.isBlank(proSlaeClass) ? "" : " AND PROBUSINESS.PRO_SLAE_CLASS like '%" + proSlaeClass + "%' ");
        builder.append(StringUtils.isBlank(proSclass) ? "" : " AND PROBUSINESS.PRO_SCLASS='" + proSclass + "' ");
        builder.append(StringUtils.isBlank(proPurchaseRate) ? "" : " AND PROBUSINESS.PRO_PURCHASE_RATE='" + proPurchaseRate + "' ");
        builder.append(StringUtils.isBlank(recSupp) ? "" : " AND BATCHINFO.BAT_SUPPLIER_CODE = '" + recSupp + "' ");
        if (!CollectionUtils.isEmpty(proReplenishmentParamsList)) {
            proReplenishmentParamsList.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                if (item.getData() != null && StringUtils.isNotBlank(item.getData().toString())) {
                    if (item.getType().intValue() == 1) {
                        builder.append(" AND " + (item.getField().replace("A.", "PROBUSINESS.")) + " = '" + item.getData().toString() + "' ");
                    }
                    if (item.getType().intValue() == 2) {
                        builder.append(" AND " + (item.getField().replace("A.", "PROBUSINESS.")) + " like '%" + item.getData().toString() + "%' ");
                    }
                }
            }));
        }
        builder.append(" GROUP BY PROBUSINESS.PRO_SELF_CODE,PROBUSINESS.PRO_NAME,PROBUSINESS.PRO_CLASS,PROBUSINESS.PRO_CLASS_NAME, " +
                " PROBUSINESS.PRO_SPECS,PROBUSINESS.PRO_FACTORY_NAME,PROBUSINESS.PRO_INPUT_TAX,PROBUSINESS.PRO_UNIT,PROBUSINESS.PRO_MID_PACKAGE, " +
                " PROBUSINESS.PRO_ZDY1,PROBUSINESS.PRO_ZDY2,PROBUSINESS.PRO_ZDY3,PROBUSINESS.PRO_ZDY4,PROBUSINESS.PRO_ZDY5,PROBUSINESS.PRO_SLAE_CLASS, " +
                " PROBUSINESS.PRO_POSITION,PROBUSINESS.PRO_SCLASS,PROBUSINESS.PRO_COMMONNAME,PROBUSINESS.PRO_REGISTER_NO,PROBUSINESS.PRO_COMPCLASS,PROBUSINESS.PRO_COMPCLASS_NAME, " +
                " PROBUSINESS.PRO_FORM,PROBUSINESS.PRO_BARCODE,PROBUSINESS.PRO_BARCODE2,PROBUSINESS.PRO_PLACE,PROBUSINESS.PRO_STATUS, " +
                " PROBUSINESS.PRO_BIG_PACKAGE,PROBUSINESS.PRO_NO_PURCHASE,PROBUSINESS.PRO_KEY_CARE,PROBUSINESS.PRO_PRESCLASS,PROBUSINESS.PRO_STORAGE_CONDITION, " +
                " PROBUSINESS.PRO_CONTROL_CLASS,PROBUSINESS.PRO_LIFE,PROBUSINESS.PRO_LIFE_UNIT,PROBUSINESS.PRO_IF_MED,PROBUSINESS.PRO_ZLBZ, " +
                " PROBUSINESS.PRO_PURCHASE_RATE,PROBUSINESS.PRO_BRAND,PROBUSINESS.PRO_BEIZHU,PROBUSINESS.PRO_JYGLLB,PROBUSINESS.PRO_DAH,PROBUSINESS.PRO_BDZ,PROBUSINESS.CLIENT, " +
                " PROBUSINESS.PRO_SITE,PROPRICE.GSPP_PRICE_NORMAL,PROCLASS.PRO_BIG_CLASS_NAME,TAXCODE.TAX_CODE_NAME,PROSCLASS.PRO_SCLASS_NAME,DCDATA.DC_CHAIN_HEAD ");
        builder.append(" ) MAININFO ");

        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT STO_CHAIN_HEAD,COUNT(1) AS STO_CNT FROM GAIA_STORE_DATA ");
        builder.append("   WHERE CLIENT = '" + user.getClient() + "' AND STO_CHAIN_HEAD IS NOT NULL AND STO_CHAIN_HEAD!='' ");
        builder.append("   GROUP BY STO_CHAIN_HEAD ");
        builder.append(" ) STOREDATA  ");
        builder.append(" ON MAININFO.DC_CHAIN_HEAD=STOREDATA.STO_CHAIN_HEAD ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT CLIENT, PRO_SITE, WM_SP_BM, SUM(WM_KCSL) AS WM_KCSL ");
        builder.append("   FROM GAIA_WMS_KUCEN ");
        builder.append("   WHERE CLIENT =  '" + user.getClient() + "' GROUP BY CLIENT, PRO_SITE, WM_SP_BM ");
        builder.append(" ) KUCEN ");
        builder.append(" ON MAININFO.CLIENT = KUCEN.CLIENT AND MAININFO.PRO_SITE = KUCEN.PRO_SITE AND MAININFO.PRO_SELF_CODE = KUCEN.WM_SP_BM ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT SD_STOCK.CLIENT, STORE_DATA.STO_CHAIN_HEAD, SD_STOCK.GSS_PRO_ID, SUM( SD_STOCK.GSS_QTY ) AS GSS_QTY ");
        builder.append("   FROM GAIA_SD_STOCK SD_STOCK ");
        builder.append("   INNER JOIN GAIA_STORE_DATA AS STORE_DATA ON SD_STOCK.CLIENT = STORE_DATA.CLIENT AND SD_STOCK.GSS_BR_ID = ");
        builder.append("   STORE_DATA.STO_CODE ");
        builder.append("   WHERE SD_STOCK.CLIENT =  '" + user.getClient() + "' ");
        builder.append("   GROUP BY SD_STOCK.CLIENT, STORE_DATA.STO_CHAIN_HEAD, SD_STOCK.GSS_PRO_ID ");
        builder.append(" ) STOCKSTORE ");
        builder.append(" ON MAININFO.CLIENT = STOCKSTORE.CLIENT AND MAININFO.PRO_SELF_CODE = STOCKSTORE.GSS_PRO_ID AND MAININFO.DC_CHAIN_HEAD = STOCKSTORE.STO_CHAIN_HEAD ");
//        builder.append(" --七天数据 ");
        String sevenday = DateUtils.currentAddDays(-7L);
        String monthinfo = DateUtils.currentAddDays(-30L);
        String quarter = DateUtils.currentAddDays(-90L);
        builder.append(" LEFT JOIN ( ");
        builder.append("  SELECT ");
        builder.append("  B.STO_CHAIN_HEAD, A.GSSD_PRO_ID, SUM( GSSD_QTY ) AS STORE_SALE_QTY ");
        builder.append("  FROM ");
        builder.append("  GAIA_SD_SALE_D A ");
        builder.append("  INNER JOIN GAIA_STORE_DATA B ON A.CLIENT = B.CLIENT AND A.GSSD_BR_ID = B.STO_CODE ");
        builder.append("  WHERE ");
        builder.append("  A.CLIENT =  '" + user.getClient() + "' AND A.GSSD_DATE>='" + sevenday + "' ");
        builder.append("  GROUP BY B.STO_CHAIN_HEAD, A.GSSD_PRO_ID ");
        builder.append(" ) SEVENDAY ");
        builder.append(" ON MAININFO.PRO_SELF_CODE = SEVENDAY.GSSD_PRO_ID AND MAININFO.DC_CHAIN_HEAD = SEVENDAY.STO_CHAIN_HEAD ");
//        builder.append(" --三十天数据 ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   B.STO_CHAIN_HEAD, A.GSSD_PRO_ID, SUM( GSSD_QTY ) AS STORE_SALE_QTY ");
        builder.append("   FROM ");
        builder.append("   GAIA_SD_SALE_D A ");
        builder.append("   INNER JOIN GAIA_STORE_DATA B ON A.CLIENT = B.CLIENT AND A.GSSD_BR_ID = B.STO_CODE ");
        builder.append("   WHERE ");
        builder.append("   A.CLIENT =  '" + user.getClient() + "' AND A.GSSD_DATE>='" + monthinfo + "' ");
        builder.append("   GROUP BY ");
        builder.append("   B.STO_CHAIN_HEAD, A.GSSD_PRO_ID ");
        builder.append(" ) MONTHINFO ");
        builder.append(" ON MAININFO.PRO_SELF_CODE = MONTHINFO.GSSD_PRO_ID AND MAININFO.DC_CHAIN_HEAD = MONTHINFO.STO_CHAIN_HEAD ");
//        builder.append(" --九十天数据 ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   B.STO_CHAIN_HEAD, A.GSSD_PRO_ID, SUM( GSSD_QTY ) AS STORE_SALE_QTY ");
        builder.append("   FROM ");
        builder.append("   GAIA_SD_SALE_D A ");
        builder.append("   INNER JOIN GAIA_STORE_DATA B ON A.CLIENT = B.CLIENT AND A.GSSD_BR_ID = B.STO_CODE ");
        builder.append("   WHERE ");
        builder.append("   A.CLIENT =  '" + user.getClient() + "' AND A.GSSD_DATE>='" + quarter + "' ");
        builder.append("   GROUP BY ");
        builder.append("   B.STO_CHAIN_HEAD, ");
        builder.append("   A.GSSD_PRO_ID ");
        builder.append(" ) QUARTER ");
        builder.append(" ON MAININFO.PRO_SELF_CODE = QUARTER.GSSD_PRO_ID AND MAININFO.DC_CHAIN_HEAD = QUARTER.STO_CHAIN_HEAD ");
//        builder.append(" --阶段时间数据 ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   B.STO_CHAIN_HEAD, A.GSSD_PRO_ID, SUM( GSSD_QTY ) AS STORE_SALE_QTY, COUNT(DISTINCT GSSD_BR_ID) STO_COUNT ");
        builder.append("   FROM ");
        builder.append("   GAIA_SD_SALE_D A ");
        builder.append("   INNER JOIN GAIA_STORE_DATA B ON A.CLIENT = B.CLIENT AND A.GSSD_BR_ID = B.STO_CODE ");
        builder.append("   WHERE ");
        builder.append("   A.CLIENT =  '" + user.getClient() + "' AND A.GSSD_DATE>='" + salesVolumeStartDate + "' AND A.GSSD_DATE<='" + salesVolumeEndDate + "' ");
        builder.append("   GROUP BY ");
        builder.append("   B.STO_CHAIN_HEAD, A.GSSD_PRO_ID ");
        builder.append(" ) DATEDATA ");
        builder.append(" ON MAININFO.PRO_SELF_CODE = DATEDATA.GSSD_PRO_ID AND MAININFO.DC_CHAIN_HEAD = DATEDATA.STO_CHAIN_HEAD ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   CLIENT,STO_CHAIN_HEAD,GSS_PRO_ID,COUNT(GSS_BR_ID) AS GSS_BR_ID_COUNT ");
        builder.append("   FROM ( ");
        builder.append("     SELECT ");
        builder.append("     SD_STOCK.CLIENT,STORE_DATA.STO_CHAIN_HEAD,SD_STOCK.GSS_PRO_ID,SD_STOCK.GSS_BR_ID,SUM(GSS_QTY) AS GSS_QTY ");
        builder.append("     FROM ");
        builder.append("     GAIA_SD_STOCK SD_STOCK ");
        builder.append("     INNER JOIN GAIA_STORE_DATA AS STORE_DATA ON SD_STOCK.CLIENT = STORE_DATA.CLIENT AND SD_STOCK.GSS_BR_ID = STORE_DATA.STO_CODE ");
        builder.append("     WHERE SD_STOCK.CLIENT =  '" + user.getClient() + "' ");
        builder.append("     GROUP BY SD_STOCK.CLIENT, STORE_DATA.STO_CHAIN_HEAD, SD_STOCK.GSS_PRO_ID,SD_STOCK.GSS_BR_ID ");
        builder.append("   ) RESULTINFO ");
        builder.append("   WHERE GSS_QTY > 0 AND GSS_QTY IS NOT NULL ");
        builder.append("   GROUP BY CLIENT, STO_CHAIN_HEAD, GSS_PRO_ID ");
        builder.append(" )  STOCKSTOREINFO ");
        builder.append(" ON MAININFO.CLIENT = STOCKSTOREINFO.CLIENT AND MAININFO.PRO_SELF_CODE = STOCKSTOREINFO.GSS_PRO_ID AND MAININFO.DC_CHAIN_HEAD = STOCKSTOREINFO.STO_CHAIN_HEAD ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT CLIENT,BAT_SITE_CODE,BAT_PRO_CODE,PO_DATE,PO_SUPPLIER_ID,SUP_NAME,SUP_PAY_TERM,SUP_LEAD_TIME,PO_PRICE FROM ( ");
        builder.append("     SELECT ");
        builder.append("     ROW_NUMBER() OVER(PARTITION BY GBI.CLIENT,GBI.BAT_SITE_CODE,GBI.BAT_PRO_CODE ORDER BY GBI.BAT_CREATE_DATE,GBI.BAT_CREATE_TIME DESC) AS ROWNUMBER, ");
        builder.append("     GBI.CLIENT, ");
        builder.append("     GBI.BAT_SITE_CODE, ");
        builder.append("     GBI.BAT_PRO_CODE, ");
        builder.append("     GBI.BAT_CREATE_DATE AS PO_DATE, ");
        builder.append("     GBI.BAT_SUPPLIER_CODE AS PO_SUPPLIER_ID, ");
        builder.append("     SBS.SUP_NAME AS SUP_NAME, ");
        builder.append("     SBS.SUP_PAY_TERM AS SUP_PAY_TERM, ");
        builder.append("     SBS.SUP_LEAD_TIME AS SUP_LEAD_TIME, ");
        builder.append("     GBI.BAT_PO_PRICE AS PO_PRICE ");
        builder.append("     FROM ");
        builder.append("     GAIA_BATCH_INFO AS GBI ");
        builder.append("     INNER JOIN GAIA_SUPPLIER_BUSINESS SBS ON SBS.CLIENT = GBI.CLIENT ");
        builder.append("     AND SBS.SUP_SELF_CODE = GBI.BAT_SUPPLIER_CODE ");
        builder.append("     AND GBI.BAT_SITE_CODE = SBS.SUP_SITE ");
        builder.append("     WHERE GBI.CLIENT =  '" + user.getClient() + "'" + (StringUtils.isBlank(lastSupp) ? "" : "AND GBI.BAT_SUPPLIER_CODE='" + lastSupp + "' "));
        builder.append("     GROUP BY GBI.CLIENT,GBI.BAT_SITE_CODE,GBI.BAT_PRO_CODE,GBI.BAT_CREATE_DATE,BAT_CREATE_TIME, ");
        builder.append("     GBI.BAT_SUPPLIER_CODE,SBS.SUP_NAME,SBS.SUP_PAY_TERM,SBS.SUP_LEAD_TIME,GBI.BAT_PO_PRICE ");
        builder.append("   ) INFO ");
        builder.append("   WHERE ROWNUMBER=1 ");
        builder.append(" ) BATCH_INFO ");
        builder.append(" ON MAININFO.CLIENT = BATCH_INFO.CLIENT AND MAININFO.PRO_SELF_CODE = BATCH_INFO.BAT_PRO_CODE ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   SI.CLIENT, SI.SO_SITE_CODE, SI.SO_PRO_CODE, SUM( SI.SO_QTY ) AS DAYS_WHOLESALE_COUNT ");
        builder.append("   FROM ");
        builder.append("   GAIA_SO_ITEM SI ");
        builder.append("   INNER JOIN GAIA_SO_HEADER SH ON SI.CLIENT = SH.CLIENT AND SI.SO_ID = SH.SO_ID ");
        builder.append("   WHERE ");
        builder.append("   SI.CLIENT =  '" + user.getClient() + "' AND SH.SO_DATE>='' AND SI.SO_SITE_CODE = '" + dcCode + "' ");
        builder.append("   AND SH.SO_TYPE = 'SOR' AND SI.SO_LINE_DELETE = '0' ");
        builder.append("   GROUP BY ");
        builder.append("   SI.CLIENT, SI.SO_SITE_CODE, SI.SO_PRO_CODE ");
        builder.append(" ) SO_ITEM_HEADER ");
        builder.append(" ON SO_ITEM_HEADER.CLIENT = MAININFO.CLIENT AND SO_ITEM_HEADER.SO_SITE_CODE = MAININFO.PRO_SITE AND SO_ITEM_HEADER.SO_PRO_CODE = MAININFO.PRO_SELF_CODE ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT CLIENT,SO_SITE_CODE,SO_PRO_CODE, SO_QTY-SO_DELIVERED_QTY AS SO_WAIT_COUNT ");
        builder.append("   FROM ");
        builder.append("   ( ");
        builder.append("     SELECT ");
        builder.append("     SI2.CLIENT, SI2.SO_PRO_CODE, SI2.SO_SITE_CODE,SUM(SI2.SO_QTY) as SO_QTY,SUM(SI2.SO_DELIVERED_QTY) as SO_DELIVERED_QTY ");
        builder.append("     FROM ");
        builder.append("     GAIA_SO_ITEM SI2 ");
        builder.append("     INNER JOIN GAIA_SO_HEADER SH2 ON SI2.CLIENT=SH2.CLIENT AND SI2.SO_ID = SH2.SO_ID ");
        builder.append("     WHERE SH2.SO_TYPE = 'SOR' ");
        builder.append("     AND SI2.SO_LINE_DELETE = '0' ");
        builder.append("     AND SI2.SO_COMPLETE_FLAG = '0' ");
        builder.append("     AND SH2.SO_APPROVE_STATUS = '1' ");
        builder.append("     GROUP BY SI2.CLIENT, SI2.SO_PRO_CODE, SI2.SO_SITE_CODE ");
        builder.append("    ) INFO ");
        builder.append(" ) SO_ITEM_HEADER_WAIT ");
        builder.append(" ON SO_ITEM_HEADER_WAIT.CLIENT = MAININFO.CLIENT AND SO_ITEM_HEADER_WAIT.SO_SITE_CODE = MAININFO.PRO_SITE AND SO_ITEM_HEADER_WAIT.SO_PRO_CODE = MAININFO.PRO_SELF_CODE ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT CLIENT,PO_SITE_CODE,PO_PRO_CODE,PO_QTY-PO_DELIVERED_QTY AS TRAFFIC_COUNT ");
        builder.append("   FROM ");
        builder.append("   ( ");
        builder.append("     SELECT ");
        builder.append("     PI3.CLIENT, PI3.PO_SITE_CODE, PI3.PO_PRO_CODE,SUM(PI3.PO_QTY) AS PO_QTY,SUM(PI3.PO_DELIVERED_QTY) AS PO_DELIVERED_QTY ");
        builder.append("     FROM ");
        builder.append("     GAIA_PO_ITEM PI3 ");
        builder.append("     INNER JOIN GAIA_PO_HEADER PH3 ON PH3.CLIENT = PI3.CLIENT AND PH3.PO_ID = PI3.PO_ID ");
        builder.append("     WHERE ");
        builder.append("     PI3.CLIENT =  '" + user.getClient() + "' ");
        builder.append("     AND PH3.PO_TYPE = 'Z001' ");
        builder.append("     AND PI3.PO_SITE_CODE = '" + dcCode + "' ");
        builder.append("     AND PI3.PO_COMPLETE_FLAG = '0' ");
        builder.append("     AND PI3.PO_LINE_DELETE = '0' ");
        builder.append("     AND PH3.PO_APPROVE_STATUS != '-10' AND PH3.PO_APPROVE_STATUS != '1' ");
        builder.append("     GROUP BY PI3.CLIENT,PI3.PO_SITE_CODE,PI3.PO_PRO_CODE ");
        builder.append("   ) PP3 ");
        builder.append(" ) PO_ITEM_HEADER ");
        builder.append(" ON PO_ITEM_HEADER.CLIENT = MAININFO.CLIENT AND PO_ITEM_HEADER.PO_SITE_CODE = MAININFO.PRO_SITE AND PO_ITEM_HEADER.PO_PRO_CODE = MAININFO.PRO_SELF_CODE ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT CLIENT,PO_SITE_CODE,PO_PRO_CODE,PO_QTY-PO_DELIVERED_QTY AS PO_WAIT_COUNT ");
        builder.append("   FROM ");
        builder.append("   ( ");
        builder.append("     SELECT ");
        builder.append("     PI3.CLIENT, PI3.PO_SITE_CODE, PI3.PO_PRO_CODE,SUM(PI3.PO_QTY) AS PO_QTY,SUM(PI3.PO_DELIVERED_QTY) AS PO_DELIVERED_QTY ");
        builder.append("     FROM ");
        builder.append("     GAIA_PO_ITEM PI3 ");
        builder.append("     INNER JOIN GAIA_PO_HEADER PH3 ON PH3.CLIENT = PI3.CLIENT AND PH3.PO_ID = PI3.PO_ID ");
        builder.append("     WHERE ");
        builder.append("     PI3.CLIENT =  '" + user.getClient() + "' ");
        builder.append("     AND PI3.PO_SITE_CODE = '" + dcCode + "' ");
        builder.append("     AND PH3.PO_TYPE IN ( 'Z002', 'Z005', 'Z007' ) ");
        builder.append("     AND PI3.PO_COMPLETE_FLAG = '0' ");
        builder.append("     AND PI3.PO_LINE_DELETE = '0' ");
        builder.append("     AND PH3.PO_APPROVE_STATUS != '-10' ");
        builder.append("     AND PH3.PO_APPROVE_STATUS != '1' ");
        builder.append("     GROUP BY PI3.CLIENT, PI3.PO_SITE_CODE, PI3.PO_PRO_CODE ");
        builder.append("   ) PP3 ");
        builder.append(" ) PO_ITEM_HEADER_WAIT ");
        builder.append(" ON PO_ITEM_HEADER_WAIT.CLIENT = MAININFO.CLIENT AND PO_ITEM_HEADER_WAIT.PO_SITE_CODE = MAININFO.PRO_SITE AND PO_ITEM_HEADER_WAIT.PO_PRO_CODE = MAININFO.PRO_SELF_CODE ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   PH.CLIENT,PH.GSPH_BR_ID, ");
        builder.append("   HYJ.GSPHS_PRO_ID, ");
        builder.append("   HYJ.GSPHS_PRICE ");
        builder.append("   FROM ");
        builder.append("   GAIA_SD_PROM_HEAD PH ");
        builder.append("   INNER JOIN GAIA_SD_PROM_HY_SET HYJ ON PH.CLIENT = HYJ.CLIENT AND PH.GSPH_VOUCHER_ID = HYJ.GSPHS_VOUCHER_ID ");
        builder.append("   WHERE PH.CLIENT =  '" + user.getClient() + "' ");
        builder.append("   AND PH.GSPH_BR_ID = '" + stoCode + "' ");
        builder.append("   AND PH.GSPH_TYPE = 'PROM_HYJ' ");
        builder.append(" ) HEAD_HY_SET ");
        builder.append(" ON MAININFO.CLIENT = HEAD_HY_SET.CLIENT AND MAININFO.PRO_SELF_CODE = HEAD_HY_SET.GSPHS_PRO_ID AND MAININFO.PRO_SITE= HEAD_HY_SET.GSPH_BR_ID ");
        builder.append(" AND MAININFO.PRO_BDZ != '1' ");
        builder.append(" LEFT JOIN ( ");
        builder.append("   SELECT ");
        builder.append("   PH.CLIENT, ");
        builder.append("   PH.GSPH_BR_ID, ");
        builder.append("   HYRJ.GSPHP_PRO_ID, ");
        builder.append("   HYRJ.GSPHP_PRICE ");
        builder.append("   FROM ");
        builder.append("   GAIA_SD_PROM_HEAD PH ");
        builder.append("   INNER JOIN GAIA_SD_PROM_HYR_PRICE HYRJ ON PH.CLIENT = HYRJ.CLIENT AND PH.GSPH_VOUCHER_ID = HYRJ.GSPHP_VOUCHER_ID ");
        builder.append("   WHERE PH.CLIENT =  '" + user.getClient() + "' ");
        builder.append("   AND PH.GSPH_BR_ID = '" + stoCode + "' ");
        builder.append("   AND PH.GSPH_TYPE = 'PROM_HYRJ' ");
        builder.append(" ) HEAD_HYR_PRICE ON MAININFO.CLIENT = HEAD_HYR_PRICE.CLIENT AND MAININFO.PRO_SELF_CODE = HEAD_HYR_PRICE.GSPHP_PRO_ID AND MAININFO.PRO_SITE= HEAD_HY_SET.GSPH_BR_ID ");
        builder.append(" AND MAININFO.PRO_BDZ != '1' ");


        if (!isCount) {
            builder.append(" LIMIT " + (pageNum - 1) * pageSize + " " + pageSize);
        }
        return builder.toString();
    }
}
