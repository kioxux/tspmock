package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class TrialDTO {

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class TrialReport{
        private BigDecimal total;
        private List<TrialReportItem> list;
    }

    /**
     * 饼图
     */
    @Data
    @EqualsAndHashCode(callSuper = false)
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TrialReportItem {
        private String name;
        private BigDecimal amount;
        private BigDecimal amountPercent;
    }

    /**
     * 表格
     */
    @Data
    @EqualsAndHashCode(callSuper = false)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TrialSalary {
        private Integer num;
        private String type;
        private BigDecimal preMonth;
        private BigDecimal amount;
        private BigDecimal amountPercent;
    }

}
