package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.dto.SalaryProgramDTO;
import com.gov.operate.entity.SalaryMonthMeritPay;
import com.gov.operate.mapper.SalaryMonthMeritPayMapper;
import com.gov.operate.service.ISalaryMonthMeritPayService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryMonthMeritPayServiceImpl extends ServiceImpl<SalaryMonthMeritPayMapper, SalaryMonthMeritPay> implements ISalaryMonthMeritPayService {

    /**
     * 薪资方案月绩效工资
     */
    @Override
    public void saveMeritPay(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除绩效原始数据
        this.remove(new QueryWrapper<SalaryMonthMeritPay>().eq("GSMMP_GSP_ID", gspId));
        List<EditProgramVO.SalaryMonthMeritPayOuter> salaryMonthMeritPayOuterList = vo.getSalaryMonthMeritPayOuterList();
        if (CollectionUtils.isEmpty(salaryMonthMeritPayOuterList)) {
            throw new CustomResultException("绩效方式列表不能为空");
        }
        List<SalaryMonthMeritPay> list = new ArrayList<>();
        for (int job = 0; job < salaryMonthMeritPayOuterList.size(); job++) {
            EditProgramVO.SalaryMonthMeritPayOuter salaryMonthMeritPayOuter = salaryMonthMeritPayOuterList.get(job);
            if (StringUtils.isEmpty(salaryMonthMeritPayOuter.getGsmmpSalesMax())) {
                throw new CustomResultException(MessageFormat.format("销售额等级[{0}]最大值不能为空", job + 1));
            }
            if (StringUtils.isEmpty(salaryMonthMeritPayOuter.getGsmmpSalesMin())) {
                throw new CustomResultException(MessageFormat.format("销售额等级[{0}]最小值不能为空", job + 1));
            }
            List<SalaryMonthMeritPay> salaryMonthMeritPayInnerList = salaryMonthMeritPayOuter.getSalaryMonthMeritPayInnerList();
            if (CollectionUtils.isEmpty(salaryMonthMeritPayInnerList)) {
                throw new CustomResultException(MessageFormat.format("销售额等级[{0}]毛利率等级列表不能为空", job + 1));
            }
            for (int level = 0; level < salaryMonthMeritPayInnerList.size(); level++) {
                SalaryMonthMeritPay salaryMonthMeritPay = salaryMonthMeritPayInnerList.get(level);
                if (ObjectUtils.isEmpty(salaryMonthMeritPay.getGsmmpGrossProfitMin())) {
                    throw new CustomResultException(MessageFormat.format("销售额[{0}~{1}]中 毛利率等级[{2}]最小值不能为空", salaryMonthMeritPayOuter.getGsmmpSalesMin(), salaryMonthMeritPayOuter.getGsmmpSalesMax(), job + 1));
                }
                if (ObjectUtils.isEmpty(salaryMonthMeritPay.getGsmmpGrossProfitMax())) {
                    throw new CustomResultException(MessageFormat.format("销售额[{0}~{1}]中 毛利率等级[{2}]最大值不能为空", salaryMonthMeritPayOuter.getGsmmpSalesMin(), salaryMonthMeritPayOuter.getGsmmpSalesMax(), job + 1));
                }
                if (ObjectUtils.isEmpty(salaryMonthMeritPay.getGsmmpCoeffi())) {
                    throw new CustomResultException(MessageFormat.format("销售额[{0}~{1}]中 毛利率等级[{2}]提成不能为空", salaryMonthMeritPayOuter.getGsmmpSalesMin(), salaryMonthMeritPayOuter.getGsmmpSalesMax(), job + 1));
                }
                // 方案主键
                salaryMonthMeritPay.setGsmmpGspId(gspId);
                // 销售额最大值
                salaryMonthMeritPay.setGsmmpSalesMax(salaryMonthMeritPayOuter.getGsmmpSalesMax());
                // 销售额最小值
                salaryMonthMeritPay.setGsmmpSalesMin(salaryMonthMeritPayOuter.getGsmmpSalesMin());
                // 销售额顺序
                salaryMonthMeritPay.setGsmmpJobSort(job + 1);

                // 毛利率最大值
                salaryMonthMeritPay.setGsmmpGrossProfitMax(DecimalUtils.toDecimal(salaryMonthMeritPay.getGsmmpGrossProfitMax()));
                // 毛利率最小值
                salaryMonthMeritPay.setGsmmpGrossProfitMin(DecimalUtils.toDecimal(salaryMonthMeritPay.getGsmmpGrossProfitMin()));
                // 毛利率顺序
                salaryMonthMeritPay.setGsmmpSalesSort(level + 1);

                // 提成百分数转小数
                salaryMonthMeritPay.setGsmmpCoeffi(DecimalUtils.toDecimal(salaryMonthMeritPay.getGsmmpCoeffi()));
                list.add(salaryMonthMeritPay);
            }
            // 校验范围重叠的问题(内层)
            salaryMonthMeritPayInnerList.sort(Comparator.comparing(SalaryMonthMeritPay::getGsmmpGrossProfitMin));
            for (int i = 0; i < salaryMonthMeritPayInnerList.size(); i++) {
                if (i == 0) {
                    continue;
                }
                SalaryMonthMeritPay current = salaryMonthMeritPayInnerList.get(i);
                SalaryMonthMeritPay previous = salaryMonthMeritPayInnerList.get(i - 1);
                if (current.getGsmmpGrossProfitMin().compareTo(previous.getGsmmpGrossProfitMax()) <= 0) {
                    throw new CustomResultException(MessageFormat.format("销售额[{0}~{1}] 毛利率区间[{2}~{3}]与[{4}~{5}]存在重叠区,请重新输入",
                            salaryMonthMeritPayOuter.getGsmmpSalesMin(), salaryMonthMeritPayOuter.getGsmmpSalesMax(),
                            DecimalUtils.toPercentStr(previous.getGsmmpGrossProfitMin()), DecimalUtils.toPercentStr(previous.getGsmmpGrossProfitMax()),
                            DecimalUtils.toPercentStr(current.getGsmmpGrossProfitMin()), DecimalUtils.toPercentStr(current.getGsmmpGrossProfitMax())));
                }
            }
        }
        // 校验范围重叠的问题(外层)
        salaryMonthMeritPayOuterList.sort(Comparator.comparing(EditProgramVO.SalaryMonthMeritPayOuter::getGsmmpSalesMin));
        for (int i = 0; i < salaryMonthMeritPayOuterList.size(); i++) {
            if (i == 0) {
                continue;
            }
            EditProgramVO.SalaryMonthMeritPayOuter current = salaryMonthMeritPayOuterList.get(i);
            EditProgramVO.SalaryMonthMeritPayOuter previous = salaryMonthMeritPayOuterList.get(i - 1);
            if (current.getGsmmpSalesMin().compareTo(previous.getGsmmpSalesMax()) <= 0) {
                throw new CustomResultException(MessageFormat.format("销售额区间[{0}~{1}]与[{2}~{3}]存在重叠区,请重新输入",
                        previous.getGsmmpSalesMin(), previous.getGsmmpSalesMax(), current.getGsmmpSalesMin(), current.getGsmmpSalesMax()));
            }
        }


        this.saveBatch(list);
    }

    /**
     * 绩效列表
     */
    @Override
    public List<SalaryProgramDTO.SalaryMonthMeritPayOuter> getMeritPayList(Integer gspId) {
        List<SalaryMonthMeritPay> list = this.list(new QueryWrapper<SalaryMonthMeritPay>()
                .eq("GSMMP_GSP_ID", gspId)
                .orderByAsc("GSMMP_JOB_SORT", "GSMMP_SALES_SORT"));
        Map<Integer, List<SalaryMonthMeritPay>> jobsMap = list.stream().collect(Collectors.groupingBy(SalaryMonthMeritPay::getGsmmpJobSort));
        List<SalaryProgramDTO.SalaryMonthMeritPayOuter> outerList = new ArrayList<>();

        jobsMap.forEach((jobSort, grossProfitList) -> {
            // 日销售额数据(外层数据)
            SalaryMonthMeritPay salaryMonthMeritPay = grossProfitList.get(0);
            SalaryProgramDTO.SalaryMonthMeritPayOuter salaryMonthMeritPayOuter = new SalaryProgramDTO.SalaryMonthMeritPayOuter();
            BeanUtils.copyProperties(salaryMonthMeritPay, salaryMonthMeritPayOuter);
            // 毛利率 正序排序
            grossProfitList.sort(Comparator.comparing(SalaryMonthMeritPay::getGsmmpSalesSort));
            // 小数转百分数
            grossProfitList.forEach(profit -> {
                profit.setGsmmpGrossProfitMin(profit.getGsmmpGrossProfitMin().multiply(Constants.ONE_HUNDRED));
                profit.setGsmmpGrossProfitMax(profit.getGsmmpGrossProfitMax().multiply(Constants.ONE_HUNDRED));
                profit.setGsmmpCoeffi(profit.getGsmmpCoeffi().multiply(Constants.ONE_HUNDRED));
            });
            salaryMonthMeritPayOuter.setSalaryMonthMeritPayInnerList(grossProfitList);
            //
            outerList.add(salaryMonthMeritPayOuter);
        });
        outerList.sort(Comparator.comparing(SalaryProgramDTO.SalaryMonthMeritPayOuter::getGsmmpJobSort));
        return outerList;
    }

    /**
     * 不分组绩效列表
     */
    @Override
    public List<SalaryMonthMeritPay> getMeritPayNoGroupingList(Integer gspId) {
        List<SalaryMonthMeritPay> list = this.list(new QueryWrapper<SalaryMonthMeritPay>()
                .eq("GSMMP_GSP_ID", gspId)
                .orderByAsc("GSMMP_JOB_SORT", "GSMMP_SALES_SORT"));
        return list;
    }
}
