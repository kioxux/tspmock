package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_RECHARGE_CARD_SET")
@ApiModel(value="SdRechargeCardSet对象", description="")
public class SdRechargeCardSet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("GSRCS_BR_ID")
    private String gsrcsBrId;

    @ApiModelProperty(value = "前缀")
    @TableField("GSRCS_CARD_PREFIX")
    private String gsrcsCardPrefix;

    @ApiModelProperty(value = "初始密码")
    @TableField("GSRCS_PASSWORD")
    private String gsrcsPassword;

    @ApiModelProperty(value = "支付必须输入密码")
    @TableField("GSRCS_PW_NEED_OPT")
    private String gsrcsPwNeedOpt;

    @ApiModelProperty(value = "恢复密码需店长权限")
    @TableField("GSRCS_RECOVERPW_OPT")
    private String gsrcsRecoverpwOpt;

    @ApiModelProperty(value = "充值比例1")
    @TableField("GSRCS_PROPORTION1")
    private String gsrcsProportion1;

    @ApiModelProperty(value = "起充金额1")
    @TableField("GSRCS_MIN_AMT1")
    private BigDecimal gsrcsMinAmt1;

    @ApiModelProperty(value = "充值比例2")
    @TableField("GSRCS_PROPORTION2")
    private String gsrcsProportion2;

    @ApiModelProperty(value = "起充金额2")
    @TableField("GSRCS_MIN_AMT2")
    private BigDecimal gsrcsMinAmt2;

    @ApiModelProperty(value = "是否活动充值2")
    @TableField("GSRCS_PROMOTION2")
    private String gsrcsPromotion2;

    @ApiModelProperty(value = "是否重复2")
    @TableField("GSRCS_REPEAT2")
    private String gsrcsRepeat2;

    @ApiModelProperty(value = "充值比例3")
    @TableField("GSRCS_PROPORTION3")
    private String gsrcsProportion3;

    @ApiModelProperty(value = "起充金额3")
    @TableField("GSRCS_MIN_AMT3")
    private BigDecimal gsrcsMinAmt3;

    @ApiModelProperty(value = "是否活动充值3")
    @TableField("GSRCS_PROMOTION3")
    private String gsrcsPromotion3;

    @ApiModelProperty(value = "是否重复3")
    @TableField("GSRCS_REPEAT3")
    private String gsrcsRepeat3;

    @ApiModelProperty(value = "更新日期")
    @TableField("GSRCS_UPDATE_DATE")
    private String gsrcsUpdateDate;

    @ApiModelProperty(value = "更新工号")
    @TableField("GSRCS_UPDATE_EMP")
    private String gsrcsUpdateEmp;


}
