package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.operate.dto.SaveGsmSettingCountListVO;
import com.gov.operate.entity.SdDiscount;
import com.gov.operate.mapper.SdDiscountMapper;
import com.gov.operate.service.ISdDiscountService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
@Service
public class SdDiscountServiceImpl extends ServiceImpl<SdDiscountMapper, SdDiscount> implements ISdDiscountService {

    @Resource
    private SdDiscountMapper sdDiscountMapper;

    /**
     * 保存对应活动设置主表下的折扣数据
     */
    @Override
    public void saveBatchOfMarketing(List<SaveGsmSettingCountListVO> list, String client, String marketId) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        List<SdDiscount> listDiscount = new ArrayList<>();

        String maxDiscountId = getMaxNumCountId(client, marketId, "");
        BigDecimal maxNumDiscountId = new BigDecimal(maxDiscountId);

        for (SaveGsmSettingCountListVO discountVO : list) {
            SdDiscount discount = new SdDiscount();
            BeanUtils.copyProperties(discountVO, discount);
            // 加盟商
            discount.setClient(client);
            // 活动id
            discount.setGsmMarketid(marketId);
            // 折扣Id
            discount.setGsmDiscount(maxNumDiscountId.add(BigDecimal.ONE).toString());
            listDiscount.add(discount);
        }
        this.saveBatch(listDiscount);
    }

    /**
     * 最大数字编码 活动id+门店进行分组
     */
    @Override
    public String getMaxNumCountId(String client, String marketId, String storeCode) {
        String maxNumCountId = sdDiscountMapper.getMaxNumDisCountId(client, marketId, storeCode);
        if (StringUtils.isEmpty(maxNumCountId)) {
            return "10001";
        } else {
            BigDecimal discountId = new BigDecimal(maxNumCountId);
            return discountId.add(BigDecimal.ONE).toString();
        }
    }

    /**
     * 对应营销活动折扣数据另存
     */
    @Override
    public void saveAsDiscount(String client, String marketIdOld, String marketIdNew) {
        QueryWrapper<SdDiscount> queryDiscount = new QueryWrapper<SdDiscount>()
                .eq("CLIENT", client)
                .eq("GSM_MARKETID", marketIdOld)
                .eq("GSM_STORE", "");
        List<SdDiscount> discountList = this.list(queryDiscount);
        // 修改营销活动id
        for (SdDiscount sdDiscount : discountList) {
            sdDiscount.setGsmMarketid(marketIdNew);
        }
        this.saveBatch(discountList);
    }
}
