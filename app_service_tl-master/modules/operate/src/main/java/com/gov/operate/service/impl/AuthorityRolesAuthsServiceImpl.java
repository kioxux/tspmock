package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.AuthorityRolesAuths;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.AuthorityRolesAuthsMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IAuthorityRolesAuthsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Service
public class AuthorityRolesAuthsServiceImpl extends ServiceImpl<AuthorityRolesAuthsMapper, AuthorityRolesAuths> implements IAuthorityRolesAuthsService {

    @Resource
    private CommonService commonService;

    @Resource
    private AuthorityRolesAuthsMapper rolesAuthsMapper;


    /**
     * 新增或者编辑权限
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveOrUpdateAuth(List<AuthorityRolesAuths> rolesAuthsList) {
        TokenUser user = commonService.getLoginInfo();
        if (null == rolesAuthsList || rolesAuthsList.isEmpty()) {
            throw new CustomResultException("参数不正确");
        }
        //根据角色id和加盟商来查询权限
        List<AuthorityRolesAuths> auths = rolesAuthsMapper.selectList(new QueryWrapper<AuthorityRolesAuths>()
                .eq("CLIENT", user.getClient())
                .eq("GARA_SFSQ", "1")
                .eq("GARA_ROLE_ID", rolesAuthsList.get(0).getGaraRoleId())
        );

        if (!auths.isEmpty()) {
            auths.forEach(item -> {
                //编辑
                item.setGaraSfsq("0");
                item.setGaraXgr(user.getUserId());
                item.setGaraXgrxm(user.getLoginName());
                item.setGaraXgrq(DateUtils.getCurrentDateStrYYMMDD());
                item.setGaraXgsj(DateUtils.getCurrentTimeStrHHMMSS());
            });
            this.updateBatchById(auths);
        }

        rolesAuthsList.forEach(item -> {
            //新增
            item.setClient(user.getClient());
            item.setGaraCjr(user.getUserId());
            item.setGaraCjrxm(user.getLoginName());
            item.setGaraCjrq(DateUtils.getCurrentDateStrYYMMDD());
            item.setGaraCjsj(DateUtils.getCurrentTimeStrHHMMSS());
            item.setGaraSfsq("1");
        });
        this.saveBatch(rolesAuthsList);
        return ResultUtil.success();
    }


}
