package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.UserChargeMaintenanceMSto;
import com.gov.operate.mapper.UserChargeMaintenanceMStoMapper;
import com.gov.operate.service.IUserChargeMaintenanceMStoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Service
public class UserChargeMaintenanceMStoServiceImpl extends ServiceImpl<UserChargeMaintenanceMStoMapper, UserChargeMaintenanceMSto> implements IUserChargeMaintenanceMStoService {

}
