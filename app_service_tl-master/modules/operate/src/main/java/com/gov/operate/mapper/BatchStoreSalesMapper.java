package com.gov.operate.mapper;

import com.gov.operate.entity.BatchQueryAuth;
import com.gov.operate.entity.BatchQueryAuthData;
import com.gov.operate.entity.BatchStoreSales;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 门店销售 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface BatchStoreSalesMapper extends BaseMapper<BatchStoreSales> {

    /**
     * 门店销售
     *
     * @param batchQueryAuth
     * @param itemList
     * @param day
     * @return
     */
    Integer saveDdiList(@Param("batchQueryAuth") BatchQueryAuth batchQueryAuth, @Param("itemList") List<BatchQueryAuthData> itemList, @Param("day") String day);
}
