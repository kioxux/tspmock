package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryQuarterlyAwards;
import com.gov.operate.mapper.SalaryQuarterlyAwardsMapper;
import com.gov.operate.service.ISalaryQuarterlyAwardsService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryQuarterlyAwardsServiceImpl extends ServiceImpl<SalaryQuarterlyAwardsMapper, SalaryQuarterlyAwards> implements ISalaryQuarterlyAwardsService {

    /**
     * 薪资方案季度奖
     */
    @Override
    public void saveQuarterlyAwards(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除基本工资级别 原始数据
        this.remove(new QueryWrapper<SalaryQuarterlyAwards>().eq("GSQA_GSP_ID", gspId));
        List<SalaryQuarterlyAwards> salaryQuarterlyAwardsList = vo.getSalaryQuarterlyAwardsList();
        if (CollectionUtils.isEmpty(salaryQuarterlyAwardsList)) {
            throw new CustomResultException("季度奖 数据等级不能为空");
        }
        for (int i = 0; i < salaryQuarterlyAwardsList.size(); i++) {
            SalaryQuarterlyAwards salaryQuarterlyAwards = salaryQuarterlyAwardsList.get(i);
            if (StringUtils.isEmpty(salaryQuarterlyAwards.getGsqaSalesMin())) {
                throw new CustomResultException(MessageFormat.format("季度奖等级[{0}] 日销售额最小值不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryQuarterlyAwards.getGsqaSalesMax())) {
                throw new CustomResultException(MessageFormat.format("季度奖等级[{0}] 日销售额最大值不能为空", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryQuarterlyAwards.getGsqaCommision())) {
                throw new CustomResultException(MessageFormat.format("季度奖等级[{0}] 提成比例不能为空", i + 1));
            }
            salaryQuarterlyAwards.setGsqaGspId(gspId);
            salaryQuarterlyAwards.setGsqaSort(i + 1);
            // 提成比例
            salaryQuarterlyAwards.setGsqaCommision(DecimalUtils.toDecimal(salaryQuarterlyAwards.getGsqaCommision()));
        }
        // 校验范围重叠的问题
        salaryQuarterlyAwardsList.sort(Comparator.comparing(SalaryQuarterlyAwards::getGsqaSalesMin));
        for (int i = 0; i < salaryQuarterlyAwardsList.size(); i++) {
            if (i == 0) {
                continue;
            }
            SalaryQuarterlyAwards current = salaryQuarterlyAwardsList.get(i);
            SalaryQuarterlyAwards previous = salaryQuarterlyAwardsList.get(i - 1);
            if (current.getGsqaSalesMin().compareTo(previous.getGsqaSalesMax()) <= 0) {
                throw new CustomResultException(MessageFormat.format("季度奖等级区间[{0}~{1}]与[{2}~{3}]存在重叠区,请重新输入",
                        previous.getGsqaSalesMin(), previous.getGsqaSalesMax(), current.getGsqaSalesMin(), current.getGsqaSalesMax()));
            }
        }
        this.saveBatch(salaryQuarterlyAwardsList);
    }

    /**
     * 季度奖列表
     */
    @Override
    public List<SalaryQuarterlyAwards> getQuarterlyAwards(Integer gspId) {
        List<SalaryQuarterlyAwards> list = this.list(new QueryWrapper<SalaryQuarterlyAwards>()
                .eq("GSQA_GSP_ID", gspId)
                .orderByAsc("GSQA_SORT"));
        list.forEach(awards -> {
            awards.setGsqaCommision(awards.getGsqaCommision().multiply(Constants.ONE_HUNDRED));
        });
        return list;
    }
}
