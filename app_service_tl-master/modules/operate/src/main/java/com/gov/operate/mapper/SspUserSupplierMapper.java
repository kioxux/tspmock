package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SspUserSupplier;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity generator.domain.SspUserSupplier
 */
public interface SspUserSupplierMapper extends BaseMapper<SspUserSupplier> {

    /**
     * 更新用户状态
     * @param userSupplier
     * @return
     */
    int updateUserStatus(@Param("userSupplier") SspUserSupplier userSupplier);

    /**
     * 获取供应商有效期状态
     * @param client
     * @return
     */
    SspUserSupplier selectListByClient(@Param("userId") Long userId, @Param("client") String client);
}




