package com.gov.operate.dto.order;

import lombok.Data;

/**
 * @author 钱金华
 * @date 21-2-19 14:32
 */
@Data
public class Attachment {


    private String path;

    private Integer type;
}
