package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_CLIENT_INVOICE_INFO")
@ApiModel(value="ClientInvoiceInfo对象", description="")
public class ClientInvoiceInfo extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("FICO_SOCIAL_CREDIT_CODE")
    private String ficoSocialCreditCode;

    @ApiModelProperty(value = "纳税主体名称")
    @TableField("FICO_TAX_SUBJECT_NAME")
    private String ficoTaxSubjectName;

    @ApiModelProperty(value = "开户行名称")
    @TableField("FICO_BANK_NAME")
    private String ficoBankName;

    @ApiModelProperty(value = "开户行账号")
    @TableField("FICO_BANK_NUMBER")
    private String ficoBankNumber;


}
