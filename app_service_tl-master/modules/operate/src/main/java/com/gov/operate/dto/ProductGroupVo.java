package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.operate.entity.SdYxmclass;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductGroupVo {

    private String gsyGroup;

    private String gsyGroupname;

    private List<SdYxmclass> productList;
}
