package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_MATERIAL_DOC_IMPORT")
@ApiModel(value = "MaterialDoc对象", description = "")
public class MaterialDocImp extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private Integer id;

    @ApiModelProperty(value = "文件名")
    @TableField("GMDI_NAME")
    private String gmdiName;

    @ApiModelProperty(value = "路径")
    @TableField("GMDI_PATH")
    private String gmdiPath;

    @ApiModelProperty(value = "类型")
    @TableField("GMDI_TYPE")
    private Integer gmdiType;

    @ApiModelProperty(value = "状态")
    @TableField("GMDI_STATUS")
    private Integer gmdiStatus;

    @ApiModelProperty(value = "失败原因")
    @TableField("GMDI_REASON")
    private String gmdiReason;

    @ApiModelProperty(value = "件数")
    @TableField("GMDI_COUNT")
    private Integer gmdiCount;

    @ApiModelProperty(value = "总数量")
    @TableField("GMDI_QTY")
    private BigDecimal gmdiQty;

    @ApiModelProperty(value = "总金额")
    @TableField("GMDI_AMT")
    private BigDecimal gmdiAmt;

    @ApiModelProperty(value = "执行人")
    @TableField("GMDI_EXE_USER")
    private String gmdiExeUser;

    @ApiModelProperty(value = "执行日期")
    @TableField("GMDI_EXE_DATE")
    private String gmdiExeDate;

    @ApiModelProperty(value = "执行时间")
    @TableField("GMDI_EXE_TIME")
    private String gmdiExeTime;

    @ApiModelProperty(value = "创建人")
    @TableField("GMDI_CREATE_USER")
    private String gmdiCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GMDI_CREATE_DATE")
    private String gmdiCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GMDI_CREATE_TIME")
    private String gmdiCreateTime;
}
