package com.gov.operate.service;

import com.gov.common.entity.Pageable;
import com.gov.common.response.Result;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.entity.Message;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
public interface IMessageService extends SuperService<Message> {

    /**
     * 用户列表
     *
     * @return
     */
    Result getMessageList(Integer pageNum, Integer pageSize, Integer gmBusinessType, Integer isRead, Integer source);

    /**
     * 消息读取
     *
     * @param gmId
     * @return
     */
    Result readMessage(String gmId);

    /**
     * 消息推送
     * @param messageParams
     * @return
     */
    Result sendMessageList(MessageParams messageParams);

    /**
     * 消息发送
     *
     * @param message
     * @return
     */
    Result sendMessage(MessageParams message);

    /**
     * 消息删除
     *
     * @param list
     * @return
     */
    Result delMessage(List<String> list);

    /**
     * 批量已读
     *
     * @param list
     * @return
     */
    Result readBatchMessage(List<String> list);

    /**
     * 消息推送
     * @param messageParams messageParams
     * @return Result
     */
    Result pushBusinessMonthlyReportMsg(MessageParams messageParams );

    /**
     * 促销报告
     * @param messageParams messageParams
     * @return Result
     */
    Result pushPromotionReportMsg(MessageParams messageParams );

}
