package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.GaiaPriceRange;

public interface GaiaPriceRangeMapper extends BaseMapper<GaiaPriceRange> {

}
