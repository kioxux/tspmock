package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class PayMentInfo {

    /**
     * 单据登记总金额
     */
    private BigDecimal registration;

    /**
     * 单据付款总金额
     */
    private BigDecimal payment;
}
