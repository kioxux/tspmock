package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SelectPaymentApplyDTO;
import com.gov.operate.dto.SelectPaymentApplyVO;
import com.gov.operate.entity.PaymentApply;
import com.gov.operate.entity.PaymentApplyItem;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PaymentApplyMapper extends BaseMapper<PaymentApply> {

    /**
     * 付款申请列表
     */
    IPage<SelectPaymentApplyDTO> selectPaymentApply(Page page, @Param("vo") SelectPaymentApplyVO vo);

    /**
     * 付款单号
     *
     * @param client
     * @return
     */
    Long selectBillNum(@Param("client") String client);

    /**
     * 查询已付金额
     *
     * @param client,num
     * @return
     */
    BigDecimal getPaidAmt(@Param("client") String client, @Param("num") String num);

}
