package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.SupplierPrepaymentDetail;
import com.gov.operate.mapper.SupplierPrepaymentDetailMapper;
import com.gov.operate.service.ISupplierPrepaymentDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-09-07
 */
@Service
public class SupplierPrepaymentDetailServiceImpl extends ServiceImpl<SupplierPrepaymentDetailMapper, SupplierPrepaymentDetail> implements ISupplierPrepaymentDetailService {

}
