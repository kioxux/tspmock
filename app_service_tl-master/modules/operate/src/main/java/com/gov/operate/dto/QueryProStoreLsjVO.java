package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class QueryProStoreLsjVO {

    private String proSelfCode;
}
