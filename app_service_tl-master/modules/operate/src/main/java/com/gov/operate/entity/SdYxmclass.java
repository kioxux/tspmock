package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_YXMCLASS")
@ApiModel(value="SdYxmclass对象", description="")
public class SdYxmclass extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "商品组")
    @TableField("GSY_GROUP")
    private String gsyGroup;

    @ApiModelProperty(value = "商品组名")
    @TableField("GSY_GROUPNAME")
    private String gsyGroupname;

    @ApiModelProperty(value = "门店编码")
    @TableField("GSY_STORE")
    private String gsyStore;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSY_CODE")
    private String gsyCode;

    @ApiModelProperty(value = "通用名称")
    @TableField("GSY_COMMONNAME")
    private String gsyCommonname;

    @ApiModelProperty(value = "商品描述")
    @TableField("GSY_DEPICT")
    private String gsyDepict;

    @ApiModelProperty(value = "助记码")
    @TableField("GSY_PYM")
    private String gsyPym;

    @ApiModelProperty(value = "规格")
    @TableField("GSY_SPECS")
    private String gsySpecs;

    @ApiModelProperty(value = "计量单位")
    @TableField("GSY_UNIT")
    private String gsyUnit;


}
