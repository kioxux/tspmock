package com.gov.operate.mapper;

import com.gov.operate.entity.ProductMatchConfirmLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-27
 */
public interface ProductMatchConfirmLogMapper extends BaseMapper<ProductMatchConfirmLog> {

}
