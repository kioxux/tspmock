package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.GetPayingItemDetailDTO;
import com.gov.operate.entity.PayingItemNew;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 付款明细表（新） Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface PayingItemNewMapper extends BaseMapper<PayingItemNew> {

    List<String> listNotPaying(@Param("client") String client,
                               @Param("codeList") List<String> codeList,
                               @Param("status") String status,
                               @Param("expireTime") int expireTime);

    List<GetPayingItemDetailDTO> selectByClientAndPicoId(@Param("client") String client, @Param("ficoId") String ficoId);

    List<PayingItemNew> selectDeadline(@Param("pId") Integer pId);

    BigDecimal selectPayForCloseStore(@Param("client") String client,
                                      @Param("stoCode") String stoCode,
                                      @Param("closeDate") String closeDate);

    List<String> selectPayReminder();
}
