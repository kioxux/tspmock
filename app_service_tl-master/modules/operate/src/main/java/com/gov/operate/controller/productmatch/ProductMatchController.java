package com.gov.operate.controller.productmatch;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.ProductMatchConfirmInData;
import com.gov.operate.dto.ProductMatchConfirmOtherInData;
import com.gov.operate.service.IProductMatchService;
import com.gov.operate.service.IProductMatchZService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 导入商品匹配信息表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
@RestController
@RequestMapping("/productMatch")
public class ProductMatchController {

    @Autowired
    private IProductMatchService productMatchService;

    @Resource
    private IProductMatchZService iProductMatchZService;

    @ApiOperation(value = "图片采集列表--给责任人拉取确认列表")
    @GetMapping("/getDataStaticsByTelNum/{telNum}")
    public Result getList(@PathVariable(value = "telNum") String telNum) {
        return ResultUtil.success(productMatchService.getDataStaticsByTelNum(telNum));
    }


    @ApiOperation(value = "图片采集列表-责任人点击确认")
    @PostMapping("/confirm")
    public Result confirm(@RequestBody ProductMatchConfirmInData inData) {
        productMatchService.confirm(inData);
        return ResultUtil.success();
    }

    @ApiOperation(value = "图片采集其他方式去二年")
    @PostMapping("/confirm/others")
    public Result confirmOthers(@RequestBody ProductMatchConfirmOtherInData inData) {
        productMatchService.confirmOthers(inData);
        return ResultUtil.success();
    }

    @ApiOperation(value = "测试商品校验如果校验负责人没有确认，二次推送触发")
    @GetMapping("/confirmNoNotify")
    public Result confirmNoNotify() {
        iProductMatchZService.confirmNoNotify();
        return ResultUtil.success("");
    }

    @ApiOperation(value = "测试商品校验结果推送开始")
    @GetMapping("/resultNotifyJobHandler")
    public Result resultNotifyJobHandler() {
        iProductMatchZService.resultNotifyJobHandler();
        return ResultUtil.success("");
    }


}

