package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户收费维护赠送表
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_CHARGE_MAINTENANCE_FREE")
@ApiModel(value="UserChargeMaintenanceFree对象", description="用户收费维护赠送表")
public class UserChargeMaintenanceFree extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "主表主键")
    @TableField("SPH_ID")
    private Integer sphId;

    @ApiModelProperty(value = "优惠天数/月数")
    @TableField("FREE_NUM")
    private String freeNum;

    @ApiModelProperty(value = "支付月数")
    @TableField("PAY_NUM")
    private String payNum;


}
