package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_COUPON_SET")
@ApiModel(value="SdPromCouponSet对象", description="")
public class SdPromCouponSet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPCS_VOUCHER_ID")
    private String gspcsVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPCS_SERIAL")
    private String gspcsSerial;

    @ApiModelProperty(value = "电子券活动号")
    @TableField("GSPCS_ACT_NO")
    private String gspcsActNo;

    @ApiModelProperty(value = "电子券类型")
    @TableField("GSPCS_COUPON_TYPE")
    private String gspcsCouponType;

    @ApiModelProperty(value = "电子券描述")
    @TableField("GSPCS_COUPON_REMARKS")
    private String gspcsCouponRemarks;

    @ApiModelProperty(value = "使用起始日期")
    @TableField("GSPCS_BEGIN_DATE")
    private String gspcsBeginDate;

    @ApiModelProperty(value = "使用结束日期")
    @TableField("GSPCS_END_DATE")
    private String gspcsEndDate;

    @ApiModelProperty(value = "使用起始时间")
    @TableField("GSPCS_BEGIN_TIME")
    private String gspcsBeginTime;

    @ApiModelProperty(value = "使用结束时间")
    @TableField("GSPCS_END_TIME")
    private String gspcsEndTime;

    @ApiModelProperty(value = "整体赠送余额上限")
    @TableField("GSPCS_TOTAL_PAYCHECK_AMT")
    private BigDecimal gspcsTotalPaycheckAmt;

    @ApiModelProperty(value = "单次充值金额")
    @TableField("GSPCS_SINGLE_PAYCHECK_AMT")
    private BigDecimal gspcsSinglePaycheckAmt;

    @ApiModelProperty(value = "单次使用最小金额")
    @TableField("GSPCS_SINGLE_USE_AMT")
    private BigDecimal gspcsSingleUseAmt;


}
