package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditSelftestCoeffiVO;
import com.gov.operate.entity.SalarySelftestCoeffi;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
public interface ISalarySelftestCoeffiService extends SuperService<SalarySelftestCoeffi> {

    /**
     * 自测数据获取
     */
    List<SalarySelftestCoeffi> getSelftestCoeffi();

    /**
     * 自测数据修改
     */
    void editSelftestCoeffi(EditSelftestCoeffiVO vo);

}
