package com.gov.operate.dto.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="WorkOrderReplyAtt对象", description="")
public class WorkOrderReplyAtt extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "工单主键")
    private Long gwoId;

    @ApiModelProperty(value = "工单处理记录ID")
    private Long gworId;

    @ApiModelProperty(value = "附件类型")
    private Integer gworaType;

    @ApiModelProperty(value = "路径")
    private String gworaPath;

    @ApiModelProperty(value = "文件名")
    private String gworaName;

    @ApiModelProperty(value = "文件大小")
    private Long gworaSize;

    @ApiModelProperty(value = "创建人名称")
    private String createUserName;

    @ApiModelProperty(value = "创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime updateTime;

}
