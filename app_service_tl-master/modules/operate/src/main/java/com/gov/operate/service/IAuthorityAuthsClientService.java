package com.gov.operate.service;

import com.gov.operate.entity.AuthorityAuthsClient;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 加盟商权限表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
public interface IAuthorityAuthsClientService extends SuperService<AuthorityAuthsClient> {

}
