package com.gov.operate.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.AddRechargeCardPayCheckVO;
import com.gov.operate.dto.RechargeCardLVO;
import com.gov.operate.dto.RechargeCardListVO;
import com.gov.operate.dto.recharge.RechargeRefundVO;
import com.gov.operate.entity.SdRechargeCardPaycheck;

import java.io.IOException;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface ISdRechargeCardPaycheckService extends SuperService<SdRechargeCardPaycheck> {
    Integer addPayCheck(AddRechargeCardPayCheckVO vo);

    Result getRechargeCardList(RechargeCardListVO entity, Page<RechargeCardLVO> page);

    /**
     * 退款
     */
    void rechargeRefund(RechargeRefundVO vo);

    /**
     * 储值卡充值查询导出
     * @param vo
     * @param page
     * @return
     */
    Result rechargeRecordExport(RechargeCardListVO vo, Page<RechargeCardLVO> page) throws IOException;
}
