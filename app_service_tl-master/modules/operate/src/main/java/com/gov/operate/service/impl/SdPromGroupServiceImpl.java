package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.prom.SdPromGroupDto;
import com.gov.operate.entity.SdPromGroup;
import com.gov.operate.entity.SdPromGroupPro;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdPromGroupMapper;
import com.gov.operate.mapper.SdPromGroupProMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdPromGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.redis.jedis.RedisClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 促销商品分组表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
@Service
public class SdPromGroupServiceImpl extends ServiceImpl<SdPromGroupMapper, SdPromGroup> implements ISdPromGroupService {

    @Resource
    private SdPromGroupProMapper sdPromGroupProMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private RedisClient redisClient;
    private final static String GROUP_ID = "GAIA_SD_PROM_GROUP_GROUP_ID";

    /**
     * 商品分组列表
     *
     * @param pageNum
     * @param pageSize
     * @param groupName
     * @param groupProId
     * @return
     */
    @Override
    public Result getSdPromGroupList(Integer pageNum, Integer pageSize, String groupName, String groupProId) {
        TokenUser user = commonService.getLoginInfo();
        Page<SdPromGroupDto> page = new Page<>(pageNum, pageSize);
        IPage<SdPromGroupDto> iPage = baseMapper.getSdPromGroupList(page, user.getClient(), groupName, groupProId);
        return ResultUtil.success(iPage);
    }

    /**
     * 新增
     *
     * @param groupName
     * @return
     */
    @Override
    public Result addSdPromGroup(String groupName) {
        TokenUser user = commonService.getLoginInfo();
        SdPromGroup sdPromGroup = new SdPromGroup();
        sdPromGroup.setClient(user.getClient());
        Integer groupId = baseMapper.selectGroupId(user.getClient());
        String key = GROUP_ID + "_" + user.getClient() + "_" + groupId;
        while (redisClient.exists(key)) {
            groupId++;
            key = GROUP_ID + "_" + user.getClient() + "_" + groupId;
        }
        redisClient.set(key, groupId.toString(), 120);
        sdPromGroup.setGroupId(StringUtils.leftPad(groupId.toString(), 8, "0"));
        sdPromGroup.setGroupName(groupName);
        sdPromGroup.setGroupFlag("0");
        sdPromGroup.setCreateUser(user.getUserId());
        sdPromGroup.setCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        sdPromGroup.setCreateTiem(DateUtils.getCurrentTimeStrHHMMSS());
        baseMapper.insert(sdPromGroup);
        return ResultUtil.success();
    }

    /**
     * 编辑
     *
     * @param groupId
     * @param groupName
     * @return
     */
    @Override
    public Result editSdPromGroup(String groupId, String groupName) {
        TokenUser user = commonService.getLoginInfo();
        SdPromGroup sdPromGroup = new SdPromGroup();
        sdPromGroup.setGroupName(groupName);
        baseMapper.update(sdPromGroup, new LambdaQueryWrapper<SdPromGroup>().eq(SdPromGroup::getClient, user.getClient()).eq(SdPromGroup::getGroupId, groupId));
        return ResultUtil.success();
    }

    /**
     * 删除
     *
     * @param groupId
     * @return
     */
    @Override
    public Result delSdPromGroup(String groupId) {
        TokenUser user = commonService.getLoginInfo();
        SdPromGroup sdPromGroup = new SdPromGroup();
        sdPromGroup.setGroupFlag("1");
        baseMapper.update(sdPromGroup, new LambdaQueryWrapper<SdPromGroup>().eq(SdPromGroup::getClient, user.getClient()).eq(SdPromGroup::getGroupId, groupId));
        return ResultUtil.success();
    }

    /**
     * 促销商品组下拉框
     *
     * @return
     */
    @Override
    public Result getGroupProList() {
        TokenUser user = commonService.getLoginInfo();
        List<SdPromGroup> sdPromGroupList = baseMapper.selectList(new LambdaQueryWrapper<SdPromGroup>().eq(SdPromGroup::getClient, user.getClient())
                .eq(SdPromGroup::getGroupFlag, "0").orderByAsc(SdPromGroup::getGroupId));
        List<SdPromGroupDto> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(sdPromGroupList)) {
            return ResultUtil.success(result);
        }
        List<SdPromGroupPro> sdPromGroupProList = sdPromGroupProMapper.selectList(new LambdaQueryWrapper<SdPromGroupPro>().eq(SdPromGroupPro::getClient, user.getClient()));
        for (SdPromGroup sdPromGroup : sdPromGroupList) {
            SdPromGroupDto sdPromGroupDto = new SdPromGroupDto();
            BeanUtils.copyProperties(sdPromGroup, sdPromGroupDto);
            if (CollectionUtils.isEmpty(sdPromGroupProList)) {
                continue;
            }
            List<SdPromGroupPro> childList = sdPromGroupProList.stream().filter(t -> t.getGroupId().equals(sdPromGroupDto.getGroupId())).collect(Collectors.toList());
            sdPromGroupDto.setSdPromGroupProList(childList);
            result.add(sdPromGroupDto);
        }
        return ResultUtil.success(result);
    }
}
