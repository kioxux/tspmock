package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.recharge.RechargeRefundVO;
import com.gov.operate.entity.SdRechargeCard;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface SdRechargeCardMapper extends BaseMapper<SdRechargeCard> {
    /**
     * 获取会员卡号
     */
    SdRechargeCard getUserCardNumber(Map<Object, Object> map);

    void updateGsrcAmt(@Param("client") String client,
                       @Param("gsrcAccountId") String gsrcAccountId,
                       @Param("gsrcId") String gsrcId,
                       @Param("gsrcAmt") BigDecimal gsrcAmt);

    /**
     * 复合主键查询唯一值
     */
    SdRechargeCard getRechargeCardByCompositekey(@Param("vo") RechargeRefundVO vo, @Param("client") String client);
}
