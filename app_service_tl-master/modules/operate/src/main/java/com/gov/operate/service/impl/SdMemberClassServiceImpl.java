package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.operate.entity.SdMemberClass;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdMemberClassMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdMemberClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Service
public class SdMemberClassServiceImpl extends ServiceImpl<SdMemberClassMapper, SdMemberClass> implements ISdMemberClassService {

    @Resource
    private CommonService commonService;

    @Override
    public List<SdMemberClass> selectMemberClassList() {
        // 获取当前人加盟商
        TokenUser user = commonService.getLoginInfo();

        QueryWrapper<SdMemberClass> queryWrapper = new QueryWrapper<SdMemberClass>().eq("CLIENT", user.getClient());

        return baseMapper.selectList(queryWrapper);
    }
}
