package com.gov.operate.service;

import com.gov.operate.entity.SdGradeList;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
public interface ISdGradeListService extends SuperService<SdGradeList> {

    /**
     * 会员分级标签 (对应精准查询活跃度)
     * @return
     */
    List<SdGradeList> getGradeList();
}
