package com.gov.operate.service.impl;

import com.gov.operate.entity.ExportTask;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ExportTaskMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IExportTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 导出报表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2022-01-20
 */
@Service
public class ExportTaskServiceImpl extends ServiceImpl<ExportTaskMapper, ExportTask> implements IExportTaskService {

    @Resource
    private CommonService commonService;

    /**
     * 生成导出记录
     *
     * @param getStatus
     * @param getType
     * @return
     */
    @Override
    public ExportTask initExportTask(Integer getStatus, Integer getType) {
        TokenUser user = commonService.getLoginInfo();
        ExportTask exportTask = new ExportTask();
        exportTask.setClient(user.getClient());
        exportTask.setGetUser(user.getUserId());
        exportTask.setGetStatus(getStatus);
        exportTask.setGetType(getType);
        exportTask.setGetDate(LocalDateTime.now());
        baseMapper.insert(exportTask);
        return exportTask;
    }
}
