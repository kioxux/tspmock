package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.ContextHolderUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.UserLoginModelClient;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.userutil.UserUtils;
import com.gov.operate.dto.PromDTO;
import com.gov.operate.dto.StoreVo;
import com.gov.operate.entity.*;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CommonServiceImpl implements CommonService {

    @Resource
    private AuthFeign authFeign;
    @Resource
    private CommonMapper commonMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private SdPromHeadMapper sdPromHeadMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private SdPromVariableTranMapper sdPromVariableTranMapper;
    @Resource
    private SdPromUnitarySetMapper sdPromUnitarySetMapper;
    @Resource
    private ServiceImpl<SdPromHeadMapper, SdPromHead> sdPromHeadService;
    @Resource
    private ServiceImpl<SdPromUnitarySetMapper, SdPromUnitarySet> sdPromUnitarySetService;
    @Resource
    private SdPromSeriesSetMapper sdPromSeriesSetMapper;
    @Resource
    private ServiceImpl<SdPromSeriesSetMapper, SdPromSeriesSet> sdPromSeriesSetService;
    @Resource
    private SdPromSeriesCondsMapper sdPromSeriesCondsMapper;
    @Resource
    private ServiceImpl<SdPromSeriesCondsMapper, SdPromSeriesConds> sdPromSeriesCondsService;
    @Resource
    private ServiceImpl<SdPromAssoCondsMapper, SdPromAssoConds> sdPromAssoCondsService;
    @Resource
    private SdPromAssoSetMapper sdPromAssoSetMapper;
    @Resource
    private ServiceImpl<SdPromAssoResultMapper, SdPromAssoResult> sdPromAssoResultService;
    @Resource
    private ISdPromGiftCondsService sdPromGiftCondsService;
    @Resource
    private ISdPromGiftSetService sdPromGiftSetService;
    @Resource
    private ISdPromGiftResultService sdPromGiftResultService;
    @Resource
    private ISdPromCouponSetService sdPromCouponSetService;
    @Resource
    private ISdPromCouponGrantService sdPromCouponGrantService;
    @Resource
    private ISdPromCouponUseService sdPromCouponUseService;
    @Resource
    private ISdPromHyrDiscountService sdPromHyrDiscountService;
    @Resource
    private ISdPromHyrPriceService sdPromHyrPriceService;
    @Resource
    private ISdPromHySetService sdPromHySetService;
    @Resource
    private SdMarketingMapper sdMarketingMapper;

    /**
     * 获取登录人信息
     */
    @Override
    public TokenUser getLoginInfo() {
//        String profile = env.getProperty("spring.profiles.active");
//        if (StringUtils.isEmpty(profile)) {
//            TokenUser user = new TokenUser();
//            user.setClient("21020001");
//            user.setUserId("0000000001");
//            return user;
//        }
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isEmpty(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        // 解析失败 PC登录
        if (UserUtils.getToken() == null) {
            Map<String, Object> param = new HashMap<>();
            param.put("token", token);
            log.info("登录人信息！ 提交参数：{}", JsonUtils.beanToJson(param));
            JSONObject object = authFeign.getLoginInfo(param);
            log.info("登录人信息！ 返回结果：{}", object.toJSONString());
            if (object.isEmpty()) {
                throw new CustomResultException("返回结果异常");
            }
            if (object.containsKey("code")) {
                throw new CustomResultException(object.getString("message"));
            }
            return object.toJavaObject(TokenUser.class);
        } else {
            // APP、微信登录
            String tokenCode = UserUtils.getTokenCode();
            String userStr = redisClient.get(tokenCode);
            if (org.apache.commons.lang3.StringUtils.isBlank(userStr)) {
                throw new CustomResultException(ResultEnum.E0007);
            }
            try {
                UserLoginModelClient userLoginModelClient = JsonUtils.jsonToBean(userStr, UserLoginModelClient.class);
                if (userLoginModelClient == null) {
                    throw new CustomResultException(ResultEnum.E0007);
                }
                TokenUser tokenUser = new TokenUser();
                tokenUser.setClient(userLoginModelClient.getClient());
                tokenUser.setUserId(userLoginModelClient.getUserId());
                return tokenUser;
            } catch (Exception e) {
                throw new CustomResultException(ResultEnum.E0007);
            }
        }
    }

    /**
     * 获取当前登录人门店编码
     *
     * @param client
     * @param userId
     * @return
     */
    @Override
    public List<StoreVo> getCurrentUserStores(String client, String userId) {
        return commonMapper.getCurrentUserStores(client, userId);
    }

    /**
     * 促销保存
     *
     * @param gsph_marketid 营销ID
     * @param client        加盟商
     * @param voucherList   商品集合
     */
    @Override
    public void saveProm(String gsph_marketid, String client, List<PromDTO> voucherList, List<PromDTO> proList) throws InstantiationException, IllegalAccessException {
        // 促销数据为空
        if (CollectionUtils.isEmpty(voucherList)) {
            return;
        }
        // 门店集合
        List<String> storeCodeList = storeDataMapper.getStoreListByClient(client);
        // 门店为空
        if (CollectionUtils.isEmpty(storeCodeList)) {
            return;
        }
        // 促销主表插入数据
        List<SdPromHead> sdPromHeadInsertList = new ArrayList<>();
        // 促销变量传输表
        QueryWrapper<SdPromVariableTran> sdPromVariableTranQueryWrapper = new QueryWrapper<SdPromVariableTran>();
        // 加盟商 促销单号 促销类型
        sdPromVariableTranQueryWrapper.eq("CLIENT", client)
                .in("GSPVT_VOUCHER_ID", voucherList.stream().map(PromDTO::getId).collect(Collectors.toList()));
        List<SdPromVariableTran> sdPromVariableTranList = sdPromVariableTranMapper.selectList(sdPromVariableTranQueryWrapper);
        Map<String, List<SdPromVariableTran>> variableTranMap = sdPromVariableTranList.stream().collect(Collectors.groupingBy(SdPromVariableTran::getGspvtType));
        // 营销任务时间数据
        TokenUser loginInfo = getLoginInfo();
        SdMarketing marketing = sdMarketingMapper.selectOne(new QueryWrapper<SdMarketing>()
                .eq("CLIENT", loginInfo.getClient())
                .eq("GSM_MARKETID", gsph_marketid)
                .eq("GSM_STORE", Constants.EMPTY_STRING));
        // 插入门店促销数据
        for (String storeCode : storeCodeList) {
            // 门店促销数据
            for (PromDTO promDTO : voucherList) {
                // 促销单号
                String voucher_id = promDTO.getId();
                List<SdPromVariableTran> list = variableTranMap.get(PromDTO.PROM_HEADER).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                // 促销主表插入
                if (!CollectionUtils.isEmpty(list)) {
                    HashMap<String, String> promHeadMap = this.initPromHead(list);
                    // 加盟商
                    promHeadMap.put(PromDTO.SD_PROM_HEAD.CLIENT, client);
                    // 门店
                    promHeadMap.put(PromDTO.SD_PROM_HEAD.GSPH_BR_ID, storeCode);
                    // 促销单号
                    promHeadMap.put(PromDTO.SD_PROM_HEAD.GSPH_VOUCHER_ID, voucher_id);
                    // 营销活动ID
                    promHeadMap.put(PromDTO.SD_PROM_HEAD.GSPH_MARKETID, gsph_marketid);
                    SdPromHead sdPromHead = this.initEntity(promHeadMap, SdPromHead.class);
                    // 起始日期
                    sdPromHead.setGsphBeginDate(marketing.getGsmStartd());
                    // 结束日期
                    sdPromHead.setGsphEndDate(marketing.getGsmEndd());
                    // 起始时间
                    sdPromHead.setGsphBeginTime("000000");
                    // 结束时间
                    sdPromHead.setGsphEndTime("235959");

                    sdPromHeadInsertList.add(sdPromHead);
                }
            }
        }

        // 促销主表插入
        if (!CollectionUtils.isEmpty(sdPromHeadInsertList)) {
            sdPromHeadMapper.insertList(sdPromHeadInsertList);
        }


        // 单品促销
        List<SdPromUnitarySet> sdPromUnitarySetInsertList = new ArrayList<>();
        // 系列类促销
        Map<String, PromDTO.PromSeries> seriesMap = new HashMap<>();
        // 赠品类数据
        Map<String, PromDTO.PromGift> giftMap = new HashMap<>();
        // 电子卷类数据
        Map<String, PromDTO.PromCoupon> couponMap = new HashMap<>();
        // 会员类
        Map<String, PromDTO.PromHy> hyMap = new HashMap<>();
        // 组合数据
        Map<String, PromDTO.PromCombin> combinMap = new HashMap<>();

        int unitarySetSerial = 0;
        // 商品插入
        for (PromDTO promDTO : proList) {
            // 促销单号
            String voucher_id = promDTO.getId();
            // 促销类型
//            List<String> types = sdPromVariableTranMapper.selectTypes(client, voucher_id, PromDTO.PROM_HEADER);
            List<String> types = sdPromVariableTranList.stream()
                    .filter(entry -> voucher_id.equals(entry.getGspvtVoucherId()) && !PromDTO.PROM_HEADER.equals(entry.getGspvtType()))
                    .map(SdPromVariableTran::getGspvtType)
                    .distinct()
                    .collect(Collectors.toList());
            // 促销类型为空
            if (CollectionUtils.isEmpty(types)) {
                continue;
            }
            // 单品促销
            String type = getPromType(types);
            if (PromDTO.PROM_SINGLE.equals(type)) {
                List<SdPromVariableTran> tranList = variableTranMap.get(PromDTO.PROM_SINGLE).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                this.promUnitarySet(client, voucher_id, promDTO, voucherList, sdPromUnitarySetInsertList, unitarySetSerial, tranList);
                unitarySetSerial++;
            }
            // 系列类促销
            if (PromDTO.PROM_SERIES.equals(type)) {
                List<SdPromVariableTran> tranList = variableTranMap.get(PromDTO.PROM_SERIES).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                promSeries(seriesMap, client, voucher_id, promDTO, voucherList, tranList);
            }
            // 赠品类促销
            if (PromDTO.PROM_GIFT.equals(type)) {
                List<SdPromVariableTran> tranList = variableTranMap.get(PromDTO.PROM_GIFT).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                promGift(giftMap, client, voucher_id, promDTO, voucherList, tranList);
            }
            // 电子券类促销
            if (PromDTO.PROM_COUPON.equals(type)) {
                List<SdPromVariableTran> tranList = variableTranMap.get(PromDTO.PROM_COUPON).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                promCoupon(couponMap, client, voucher_id, promDTO, voucherList, tranList);
            }
            // 会员类促销
            if (PromDTO.PROM_HY.equals(type)) {
                List<SdPromVariableTran> tranList = variableTranMap.get(PromDTO.PROM_HY).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                promHy(hyMap, client, voucher_id, promDTO, voucherList, tranList);
            }
            // 组合类促销
            if (PromDTO.PROM_COMBIN.equals(type)) {
                List<SdPromVariableTran> tranList = variableTranMap.get(PromDTO.PROM_COMBIN).stream().filter(entity -> voucher_id.equals(entity.getGspvtVoucherId())).collect(Collectors.toList());
                promCombin(combinMap, client, voucher_id, promDTO, voucherList, tranList);
            }
        }

        // 单品促销
        if (!CollectionUtils.isEmpty(sdPromUnitarySetInsertList)) {
            sdPromUnitarySetService.saveBatch(sdPromUnitarySetInsertList);
        }

        // 系列类促销
        if (!seriesMap.isEmpty()) {
            seriesMap.forEach((voucher_id, promSeries) -> {
                if (ObjectUtils.isEmpty(promSeries)) {
                    return;
                }
                // 赠品了类促销 设置表
                SdPromSeriesSet sdPromSeriesSet = promSeries.getSdPromSeriesSet();
                if (!ObjectUtils.isEmpty(sdPromSeriesSet)) {
                    sdPromSeriesSetService.save(sdPromSeriesSet);
                }
                // 赠品了促销 商品表
                List<SdPromSeriesConds> sdPromSeriesCondsList = promSeries.getSdPromSeriesCondsList();
                if (!CollectionUtils.isEmpty(sdPromSeriesCondsList)) {
                    sdPromSeriesCondsService.saveBatch(sdPromSeriesCondsList);
                }
            });
        }

        // 赠品类促销
        if (!giftMap.isEmpty()) {
            giftMap.forEach((voucher_id, promGift) -> {
                if (ObjectUtils.isEmpty(promGift)) {
                    return;
                }
                // 赠品类类促销 设置表
                SdPromGiftSet sdPromGiftSet = promGift.getSdPromGiftSet();
                if (!ObjectUtils.isEmpty(sdPromGiftSet)) {
                    sdPromGiftSetService.save(sdPromGiftSet);
                }
                // 赠品类促销 商品表
                List<SdPromGiftConds> sdPromGiftCondsList = promGift.getSdPromGiftCondsList();
                if (!CollectionUtils.isEmpty(sdPromGiftCondsList)) {
                    sdPromGiftCondsService.saveBatch(sdPromGiftCondsList);
                }
                // 赠品类促销 赠品表
                List<SdPromGiftResult> sdPromGiftResultList = promGift.getSdPromGiftResultList();
                if (!CollectionUtils.isEmpty(sdPromGiftResultList)) {
                    sdPromGiftResultService.saveBatch(sdPromGiftResultList);
                }
            });
        }
        // 电子卷促销
        if (!couponMap.isEmpty()) {
            couponMap.forEach((voucher_id, promCoupon) -> {
                if (ObjectUtils.isEmpty(promCoupon)) {
                    return;
                }
                // 电子类促销 设置表
                SdPromCouponSet sdPromGiftSet = promCoupon.getSdPromCouponSet();
                if (!ObjectUtils.isEmpty(sdPromGiftSet)) {
                    sdPromCouponSetService.save(sdPromGiftSet);
                }
                // 电子类促销 用券表
                List<SdPromCouponUse> sdPromGiftCondsList = promCoupon.getSdPromCouponUseList();
                if (!CollectionUtils.isEmpty(sdPromGiftCondsList)) {
                    sdPromCouponUseService.saveBatch(sdPromGiftCondsList);
                }
                // 电子类促销 送券表
                List<SdPromCouponGrant> sdPromGiftResultList = promCoupon.getSdPromCouponGrantList();
                if (!CollectionUtils.isEmpty(sdPromGiftResultList)) {
                    sdPromCouponGrantService.saveBatch(sdPromGiftResultList);
                }
            });
        }

        // 会员类促销
        if (!hyMap.isEmpty()) {
            // 会员类促销会员日折扣设置表
            String key = hyMap.keySet().iterator().next();
            SdPromHyrDiscount hy = hyMap.get(key).getSdPromHyrDiscount();
            if (!ObjectUtils.isEmpty(hy)) {
                sdPromHyrDiscountService.save(hy);
            }
            hyMap.forEach((voucher_id, promHy) -> {
                // 会员类促销会员日价设置表
                if (!CollectionUtils.isEmpty(promHy.getSdPromHyrPriceList())) {
                    sdPromHyrPriceService.saveBatch(promHy.getSdPromHyrPriceList());
                }
                // 会员类促销会员价设置表
                if (!CollectionUtils.isEmpty(promHy.getSdPromHySetList())) {
                    sdPromHySetService.saveBatch(promHy.getSdPromHySetList());
                }
            });
        }

        // 组合类促销
        {
            if (!combinMap.isEmpty()) {
                for (String voucher_id : combinMap.keySet()) {
                    // 促销对象
                    PromDTO.PromCombin promCombin = combinMap.get(voucher_id);
                    if (promCombin == null) {
                        continue;
                    }
                    // 组合类促销条件商品设置表
                    List<SdPromAssoConds> sdPromAssoCondsList = promCombin.getSdPromAssoCondsList();
                    if (!CollectionUtils.isEmpty(sdPromAssoCondsList)) {
                        sdPromAssoCondsService.saveBatch(sdPromAssoCondsList);
                    }
                    // 组合类促销系列编码设置表
                    SdPromAssoSet sdPromAssoSet = promCombin.getSdPromAssoSet();
                    if (sdPromAssoSet != null) {
                        sdPromAssoSetMapper.insert(sdPromAssoSet);
                    }
                    // 组合类促销赠送商品设置表
                    List<SdPromAssoResult> sdPromAssoResultList = promCombin.getSdPromAssoResultList();
                    if (!CollectionUtils.isEmpty(sdPromAssoResultList)) {
                        sdPromAssoResultService.saveBatch(sdPromAssoResultList);
                    }
                }
            }
        }

        // GAIA_SD_PROM_VARIABLE_TRAN  促销变量传输表 数据更新
        this.updatePromVariableTran(client, voucherList);
    }

    /**
     * 促销类型
     *
     * @param types
     * @return
     */
    @Override
    public String getPromType(List<String> types) {
        // 单品促销
        if (types.size() == 1 && types.get(0).equals(PromDTO.PROM_SINGLE)) {
            return PromDTO.PROM_SINGLE;
        }
        // 系列促销
        if (types.size() == 1 && types.get(0).equals(PromDTO.PROM_SERIES)) {
            return PromDTO.PROM_SERIES;
        }
        // 赠品促销
        if (types.size() == 1 && types.get(0).equals(PromDTO.PROM_GIFT)) {
            return PromDTO.PROM_GIFT;
        }
        // 电子券类促销
        if (types.size() == 1 && types.get(0).equals(PromDTO.PROM_COUPON)) {
            return PromDTO.PROM_COUPON;
        }
        // 会员类类促销
        if (types.size() == 1 && types.get(0).equals(PromDTO.PROM_HY)) {
            return PromDTO.PROM_HY;
        }
        // 组合促销
        if (types.size() == 1 && types.get(0).equals(PromDTO.PROM_COMBIN)) {
            return PromDTO.PROM_COMBIN;
        }
        return null;
    }

    /**
     * 组建插入实体对象
     *
     * @param hashMap
     * @param tClass
     * @param <T>
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private <T> T initEntity(HashMap<String, String> hashMap, Class<T> tClass) throws IllegalAccessException, InstantiationException {
        T t = tClass.newInstance();
        Field[] declaredFields = t.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(TableField.class)) {
                // 标签
                TableField annotation = field.getAnnotation(TableField.class);
                // 字段名
                String column = annotation.value();
                field.setAccessible(true);
                if ("java.lang.String".equals(field.getType().getName())) {
                    field.set(t, hashMap.get(column));
                } else if ("java.math.BigDecimal".equals(field.getType().getName())) {
                    if (StringUtils.isNotBlank(hashMap.get(column))) {
                        field.set(t, new BigDecimal(hashMap.get(column)));
                    }
                } else {
                    field.set(t, hashMap.get(column));
                }
            }
            if (field.isAnnotationPresent(TableId.class)) {
                // 标签
                TableId annotation = field.getAnnotation(TableId.class);
                // 字段名
                String column = annotation.value();
                field.setAccessible(true);
                if ("java.lang.String".equals(field.getType().getName())) {
                    field.set(t, hashMap.get(column));
                } else if ("java.math.BigDecimal".equals(field.getType().getName())) {
                    if (StringUtils.isNotBlank(hashMap.get(column))) {
                        field.set(t, new BigDecimal(hashMap.get(column)));
                    }
                } else {
                    field.set(t, hashMap.get(column));
                }
            }
        }
        return t;
    }

    /**
     * 单品促销保存
     */
    private void promUnitarySet(String client, String voucher_id, PromDTO promDTO, List<PromDTO> voucherList, List<SdPromUnitarySet> sdPromUnitarySetInsertList, int unitarySetSerial, List<SdPromVariableTran> sdPromVariableTranList) throws InstantiationException, IllegalAccessException {

        HashMap<String, String> promMap = this.initPromHead(sdPromVariableTranList);
        // 加盟商
        promMap.put(PromDTO.GAIA_SD_PROM_UNITARY_SET.CLIENT, client);
        // 单号
        promMap.put(PromDTO.GAIA_SD_PROM_UNITARY_SET.GSPUS_VOUCHER_ID, voucher_id);
        // 行号
        promMap.put(PromDTO.GAIA_SD_PROM_UNITARY_SET.GSPUS_SERIAL, String.valueOf(unitarySetSerial + 1));
        // 商品编码
        promMap.put(PromDTO.GAIA_SD_PROM_UNITARY_SET.GSPUS_PRO_ID, promDTO.getProCode());
        // 页面参数
        List<PromDTO.ParamListBean> paramList = null;
        for (PromDTO entity : voucherList) {
            if (entity.getId().equals(promDTO.getId())) {
                paramList = entity.getParamList();
                break;
            }
        }
        if (!CollectionUtils.isEmpty(paramList)) {
            for (PromDTO.ParamListBean paramListBean : paramList) {
                // 字段名
                String key = paramListBean.getKey();
                // 值
                String value = paramListBean.getValue();
                if (StringUtils.isNotBlank(value)) {
                    promMap.put(key, value);
                }
            }
        }
        SdPromUnitarySet sdPromUnitarySetInsert = this.initEntity(promMap, SdPromUnitarySet.class);
        sdPromUnitarySetInsertList.add(sdPromUnitarySetInsert);
    }

    /**
     * 系列促销
     */
    private void promSeries(Map<String, PromDTO.PromSeries> seriesMap, String client, String voucher_id, PromDTO pro, List<PromDTO> voucherList, List<SdPromVariableTran> sdPromVariableTranList) throws InstantiationException, IllegalAccessException {

        HashMap<String, String> seriesMapDefault = this.initPromHead(sdPromVariableTranList);

        PromDTO.PromSeries promSeries = seriesMap.get(voucher_id);

        if (ObjectUtils.isEmpty(promSeries)) {
            promSeries = new PromDTO.PromSeries();
        }

        // 系列类商品设置表
        SdPromSeriesSet sdPromSeriesSet = promSeries.getSdPromSeriesSet();
        if (ObjectUtils.isEmpty(sdPromSeriesSet)) {
            sdPromSeriesSet = this.initEntity(seriesMapDefault, SdPromSeriesSet.class);
            // 加盟商
            sdPromSeriesSet.setClient(client);
            // 单号
            sdPromSeriesSet.setGspssVoucherId(voucher_id);
            // 行号
            sdPromSeriesSet.setGspssSerial("1");
            promSeries.setSdPromSeriesSet(sdPromSeriesSet);
            seriesMap.put(voucher_id, promSeries);
        }
        // 系列编码
        String serialId = sdPromSeriesSet.getGspssSeriesId();

        // 系列促销 商品表
        List<SdPromSeriesConds> sdPromSeriesCondsList = promSeries.getSdPromSeriesCondsList();
        if (ObjectUtils.isEmpty(sdPromSeriesCondsList)) {
            sdPromSeriesCondsList = new ArrayList<>();
            promSeries.setSdPromSeriesCondsList(sdPromSeriesCondsList);
        }
        initPageData(voucherList, voucher_id, seriesMapDefault);
        SdPromSeriesConds sdPromSeriesConds = this.initEntity(seriesMapDefault, SdPromSeriesConds.class);
        // 加盟商
        sdPromSeriesConds.setClient(client);
        // 单号
        sdPromSeriesConds.setGspscVoucherId(voucher_id);
        // 行号
        sdPromSeriesConds.setGspscSerial(String.valueOf(sdPromSeriesCondsList.size() + 1));
        // 系列编码
        sdPromSeriesConds.setGspscSeriesId(serialId);
        // 商品编码
        sdPromSeriesConds.setGspscProId(pro.getProCode());
        sdPromSeriesCondsList.add(sdPromSeriesConds);

    }

    /**
     * 赠品类促销
     */
    private void promGift(Map<String, PromDTO.PromGift> giftMap, String client, String voucher_id, PromDTO pro, List<PromDTO> voucherList, List<SdPromVariableTran> sdPromVariableTranList) throws InstantiationException, IllegalAccessException {

        HashMap<String, String> giftMapDefault = this.initPromHead(sdPromVariableTranList);

        PromDTO.PromGift promGift = giftMap.get(voucher_id);

        if (ObjectUtils.isEmpty(promGift)) {
            promGift = new PromDTO.PromGift();
        }

        // 赠品类商品设置表
        SdPromGiftSet sdPromGiftSet = promGift.getSdPromGiftSet();
        if (ObjectUtils.isEmpty(sdPromGiftSet)) {
            sdPromGiftSet = this.initEntity(giftMapDefault, SdPromGiftSet.class);
            // 加盟商
            sdPromGiftSet.setClient(client);
            // 单号
            sdPromGiftSet.setGspgsVoucherId(voucher_id);
            // 行号
            sdPromGiftSet.setGspgsSerial("1");
            promGift.setSdPromGiftSet(sdPromGiftSet);
            giftMap.put(voucher_id, promGift);
        }
        // 系列编码
        String serialId = sdPromGiftSet.getGspgsSeriesProId();

        // 赠品类 赠送商品表
        List<SdPromGiftResult> sdPromGiftResultList = promGift.getSdPromGiftResultList();
        if (ObjectUtils.isEmpty(sdPromGiftResultList)) {
            sdPromGiftResultList = new ArrayList<>();
            promGift.setSdPromGiftResultList(sdPromGiftResultList);
            PromDTO promHead = voucherList.stream().filter(entity -> voucher_id.equals(entity.getId())).findFirst().get();
            List<String> proCodeList = promHead.getProCodeList();
            if (!CollectionUtils.isEmpty(proCodeList)) {
                for (int i = 0; i < proCodeList.size(); i++) {
                    // 默认数据构造
                    HashMap<String, String> promMap = this.initPromHead(sdPromVariableTranList);
                    // 页面参数
                    initPageData(voucherList, voucher_id, promMap);
                    // 默认数据组建插入实体对象
                    SdPromGiftResult sdPromGiftResult = this.initEntity(promMap, SdPromGiftResult.class);
                    // 加盟商
                    sdPromGiftResult.setClient(client);
                    // 单号
                    sdPromGiftResult.setGspgrVoucherId(voucher_id);
                    // 行号
                    sdPromGiftResult.setGspgrSerial(String.valueOf(i + 1));
                    // 商品编码
                    sdPromGiftResult.setGspgrProId(proCodeList.get(i));
                    // 系列编码
                    sdPromGiftResult.setGspgrSeriesId(serialId);
                    sdPromGiftResultList.add(sdPromGiftResult);
                }
            }
        }


        // 赠品类商品表
        List<SdPromGiftConds> sdPromGiftCondsList = promGift.getSdPromGiftCondsList();
        if (ObjectUtils.isEmpty(sdPromGiftCondsList)) {
            sdPromGiftCondsList = new ArrayList<>();
            promGift.setSdPromGiftCondsList(sdPromGiftCondsList);
        }
        initPageData(voucherList, voucher_id, giftMapDefault);
        SdPromGiftConds sdPromGiftConds = this.initEntity(giftMapDefault, SdPromGiftConds.class);
        // 加盟商
        sdPromGiftConds.setClient(client);
        // 单号
        sdPromGiftConds.setGspgcVoucherId(voucher_id);
        // 行号
        sdPromGiftConds.setGspgcSerial(String.valueOf(sdPromGiftCondsList.size() + 1));
        // 系列编码
        sdPromGiftConds.setGspgcSeriesId(serialId);
        // 商品编码
        sdPromGiftConds.setGspgcProId(pro.getProCode());
        sdPromGiftCondsList.add(sdPromGiftConds);

    }

    /**
     * 电子卷类促销
     */
    private void promCoupon(Map<String, PromDTO.PromCoupon> couponMap, String client, String voucher_id, PromDTO pro, List<PromDTO> voucherList, List<SdPromVariableTran> sdPromVariableTranList) throws InstantiationException, IllegalAccessException {

        HashMap<String, String> couponMapDefault = this.initPromHead(sdPromVariableTranList);

        PromDTO.PromCoupon promCoupon = couponMap.get(voucher_id);

        if (ObjectUtils.isEmpty(promCoupon)) {
            promCoupon = new PromDTO.PromCoupon();
        }

        // 电子券类促销设置表
        SdPromCouponSet sdPromCouponSet = promCoupon.getSdPromCouponSet();
        if (ObjectUtils.isEmpty(sdPromCouponSet)) {
            sdPromCouponSet = this.initEntity(couponMapDefault, SdPromCouponSet.class);
            // 加盟商
            sdPromCouponSet.setClient(client);
            // 单号
            sdPromCouponSet.setGspcsVoucherId(voucher_id);
            // 行号
            sdPromCouponSet.setGspcsSerial("1");
            promCoupon.setSdPromCouponSet(sdPromCouponSet);
            couponMap.put(voucher_id, promCoupon);
        }
        // 电子券活动号
        String actNo = sdPromCouponSet.getGspcsActNo();

        // 电子券类促销送券表
        List<SdPromCouponGrant> sdPromCouponGrantList = promCoupon.getSdPromCouponGrantList();
        if (ObjectUtils.isEmpty(sdPromCouponGrantList)) {
            sdPromCouponGrantList = new ArrayList<>();
            promCoupon.setSdPromCouponGrantList(sdPromCouponGrantList);
            PromDTO promHead = voucherList.stream().filter(entity -> voucher_id.equals(entity.getId())).findFirst().get();
            List<String> proCodeList = promHead.getProCodeList();
            if (!CollectionUtils.isEmpty(proCodeList)) {
                for (int i = 0; i < proCodeList.size(); i++) {
                    // 默认数据构造
                    HashMap<String, String> promMap = this.initPromHead(sdPromVariableTranList);
                    // 页面参数
                    initPageData(voucherList, voucher_id, promMap);
                    // 默认数据组建插入实体对象
                    SdPromCouponGrant sdPromCouponGrant = this.initEntity(promMap, SdPromCouponGrant.class);
                    // 加盟商
                    sdPromCouponGrant.setClient(client);
                    // 单号
                    sdPromCouponGrant.setGspcgVoucherId(voucher_id);
                    // 行号
                    sdPromCouponGrant.setGspcgSerial(String.valueOf(i + 1));
                    // 商品编码
                    sdPromCouponGrant.setGspcgProId(proCodeList.get(i));
                    // 电子券活动号
                    sdPromCouponGrant.setGspcgActNo(actNo);
                    sdPromCouponGrantList.add(sdPromCouponGrant);
                }
            }
        }

        // 电子券类促销用券表
        List<SdPromCouponUse> sdPromCouponUseList = promCoupon.getSdPromCouponUseList();
        if (ObjectUtils.isEmpty(sdPromCouponUseList)) {
            sdPromCouponUseList = new ArrayList<>();
            promCoupon.setSdPromCouponUseList(sdPromCouponUseList);
        }
        initPageData(voucherList, voucher_id, couponMapDefault);
        SdPromCouponUse sdPromCouponUse = this.initEntity(couponMapDefault, SdPromCouponUse.class);
        // 加盟商
        sdPromCouponUse.setClient(client);
        // 单号
        sdPromCouponUse.setGspcuVoucherId(voucher_id);
        // 行号
        sdPromCouponUse.setGspcuSerial(String.valueOf(sdPromCouponUseList.size() + 1));
        // 系列编码
        sdPromCouponUse.setGspcuActNo(actNo);
        // 商品编码
        sdPromCouponUse.setGspcuProId(pro.getProCode());
        sdPromCouponUseList.add(sdPromCouponUse);
    }

    /**
     * 会员类促销
     */
    private void promHy(Map<String, PromDTO.PromHy> hyMap, String client, String voucher_id, PromDTO pro, List<PromDTO> voucherList, List<SdPromVariableTran> sdPromVariableTranList) throws InstantiationException, IllegalAccessException {

        HashMap<String, String> hyMapDefault = this.initPromHead(sdPromVariableTranList);
        PromDTO.PromHy promHy = hyMap.get(voucher_id);
        if (ObjectUtils.isEmpty(promHy)) {
            promHy = new PromDTO.PromHy();
            hyMap.put(voucher_id, promHy);
        }

        PromDTO promDTO1 = voucherList.stream().filter(entry -> entry.getId().equals(voucher_id)).findFirst().get();
        String promTableName = promDTO1.getParamList().get(0).getPromTableName();
        if ("GAIA_SD_PROM_HYR_PRICE".equals(promTableName)) {
            // 会员类促销会员日价设置表
            List<SdPromHyrPrice> sdPromHyrPriceList = promHy.getSdPromHyrPriceList();
            if (ObjectUtils.isEmpty(sdPromHyrPriceList)) {
                sdPromHyrPriceList = new ArrayList<>();
                promHy.setSdPromHyrPriceList(sdPromHyrPriceList);
            }
            initPageData(voucherList, voucher_id, hyMapDefault);
            SdPromHyrPrice sdPromHyrPrice = this.initEntity(hyMapDefault, SdPromHyrPrice.class);
            // 加盟商
            sdPromHyrPrice.setClient(client);
            // 单号
            sdPromHyrPrice.setGsphpVoucherId(voucher_id);
            // 行号
            sdPromHyrPrice.setGsphpSerial(String.valueOf(sdPromHyrPriceList.size() + 1));
            // 商品编码
            sdPromHyrPrice.setGsphpProId(pro.getProCode());
            sdPromHyrPriceList.add(sdPromHyrPrice);

        } else if ("GAIA_SD_PROM_HY_SET".equals(promTableName)) {
            // 会员类促销会员价设置表
            List<SdPromHySet> sdPromHySetList = promHy.getSdPromHySetList();
            if (ObjectUtils.isEmpty(sdPromHySetList)) {
                sdPromHySetList = new ArrayList<>();
                promHy.setSdPromHySetList(sdPromHySetList);
            }
            initPageData(voucherList, voucher_id, hyMapDefault);
            SdPromHySet sdPromHySet = this.initEntity(hyMapDefault, SdPromHySet.class);
            // 加盟商
            sdPromHySet.setClient(client);
            // 单号
            sdPromHySet.setGsphsVoucherId(voucher_id);
            // 行号
            sdPromHySet.setGsphsSerial(String.valueOf(sdPromHySetList.size() + 1));
            // 商品编码
            sdPromHySet.setGsphsProId(pro.getProCode());
            sdPromHySetList.add(sdPromHySet);

        } else {
            // 会员类促销会员日折扣设置表(全部商品均打折)
            SdPromHyrDiscount sdPromCouponSet = promHy.getSdPromHyrDiscount();
            if (ObjectUtils.isEmpty(sdPromCouponSet)) {
                sdPromCouponSet = this.initEntity(hyMapDefault, SdPromHyrDiscount.class);
                // 加盟商
                sdPromCouponSet.setClient(client);
                // 单号
                sdPromCouponSet.setGsppVoucherId(voucher_id);
                // 行号
                sdPromCouponSet.setGsppSerial("1");
                promHy.setSdPromHyrDiscount(sdPromCouponSet);
            }
        }

    }

    /**
     * 组合促销数据
     *
     * @param combinMap
     */
    private void promCombin(Map<String, PromDTO.PromCombin> combinMap, String client, String voucher_id, PromDTO promDTO, List<PromDTO> voucherList, List<SdPromVariableTran> sdPromVariableTranList) throws InstantiationException, IllegalAccessException {

        // 当前促销的商品数据
        PromDTO.PromCombin promCombin = combinMap.get(voucher_id);
        if (promCombin == null) {
            promCombin = (new PromDTO()).new PromCombin();
        }
        // 组合类促销系列编码设置表
        SdPromAssoSet sdPromAssoSet = promCombin.getSdPromAssoSet();
        if (sdPromAssoSet == null) {
            // 默认数据构造
            HashMap<String, String> promMap = this.initPromHead(sdPromVariableTranList);
            // 页面参数
            initPageData(voucherList, voucher_id, promMap);
            // 默认数据组建插入实体对象
            sdPromAssoSet = this.initEntity(promMap, SdPromAssoSet.class);
            // 加盟商
            sdPromAssoSet.setClient(client);
            // 单号
            sdPromAssoSet.setGspasVoucherId(voucher_id);
            // 行号
            sdPromAssoSet.setGspasSerial("1");
            promCombin.setSdPromAssoSet(sdPromAssoSet);
        }
        //  组合类促销赠送商品设置表
        List<SdPromAssoResult> sdPromAssoResultList = promCombin.getSdPromAssoResultList();
        if (sdPromAssoResultList == null) {
            sdPromAssoResultList = new ArrayList<>();
            // 对应促销
            PromDTO promHead = voucherList.stream().filter(entity -> voucher_id.equals(entity.getId())).findFirst().get();
            List<String> proCodeList = promHead.getProCodeList();
            // 已选赠品
            if (!CollectionUtils.isEmpty(proCodeList)) {
                for (int i = 0; i < proCodeList.size(); i++) {
                    String proCode = proCodeList.get(i);
                    // 默认数据构造
                    HashMap<String, String> promMap = this.initPromHead(sdPromVariableTranList);
                    // 页面参数
                    initPageData(voucherList, voucher_id, promMap);
                    // 默认数据组建插入实体对象
                    SdPromAssoResult sdPromAssoResult = this.initEntity(promMap, SdPromAssoResult.class);
                    // 加盟商
                    sdPromAssoResult.setClient(client);
                    // 单号
                    sdPromAssoResult.setGsparVoucherId(voucher_id);
                    // 行号
                    sdPromAssoResult.setGsparSerial(String.valueOf(i + 1));
                    // 商品编码
                    sdPromAssoResult.setGsparProId(proCode);
                    // 系列编码
                    sdPromAssoResult.setGsparSeriesId(sdPromAssoSet.getGspasSeriesId());
                    sdPromAssoResultList.add(sdPromAssoResult);
                }
            }
            promCombin.setSdPromAssoResultList(sdPromAssoResultList);
        }
        // 组合类促销条件商品设置表
        List<SdPromAssoConds> sdPromAssoCondsList = promCombin.getSdPromAssoCondsList();
        if (sdPromAssoCondsList == null) {
            sdPromAssoCondsList = new ArrayList<>();
        }
        // 默认数据构造
        HashMap<String, String> promMap = this.initPromHead(sdPromVariableTranList);
        // 页面参数
        initPageData(voucherList, voucher_id, promMap);
        // 默认数据组建插入实体对象
        SdPromAssoConds sdPromAssoConds = this.initEntity(promMap, SdPromAssoConds.class);
        // 加盟商
        sdPromAssoConds.setClient(client);
        // 单号
        sdPromAssoConds.setGspacVoucherId(voucher_id);
        // 行号
        sdPromAssoConds.setGspacSerial(String.valueOf(sdPromAssoCondsList.size() + 1));
        // 商品编码
        sdPromAssoConds.setGspacProId(promDTO.getProCode());
        // 系列编码
        sdPromAssoConds.setGspacSeriesId(sdPromAssoSet.getGspasSeriesId());
        sdPromAssoCondsList.add(sdPromAssoConds);
        promCombin.setSdPromAssoCondsList(sdPromAssoCondsList);
        // 数据插入
        combinMap.put(voucher_id, promCombin);
    }

    /**
     * 页面数据获取
     *
     * @param voucherList
     * @param voucher_id
     * @param promMap
     */
    private void initPageData(List<PromDTO> voucherList, String voucher_id, HashMap<String, String> promMap) {
        List<PromDTO.ParamListBean> paramList = voucherList.stream().filter(entity -> voucher_id.equals(entity.getId())).findFirst().get().getParamList();
        if (!CollectionUtils.isEmpty(paramList)) {
            for (PromDTO.ParamListBean paramListBean : paramList) {
                // 字段名
                String key = paramListBean.getKey();
                // 值
                String value = paramListBean.getValue();
                if (StringUtils.isNotBlank(value)) {
                    promMap.put(key, value);
                }
            }
        }
    }

    /**
     * 促销初始化
     *
     * @param sdPromVariableTranList
     * @return
     */
    private HashMap<String, String> initPromHead(List<SdPromVariableTran> sdPromVariableTranList) {
        HashMap<String, String> map = new HashMap<String, String>();
        // 促销变量传输表 促销数据初始化
        for (SdPromVariableTran sdPromVariableTran : sdPromVariableTranList) {
            map.put(sdPromVariableTran.getGspvtColumns(), sdPromVariableTran.getGspvtColumnsValue());
        }
        return map;
    }

    /**
     * GAIA_SD_PROM_VARIABLE_TRAN  促销变量传输表 数据更新
     *
     * @param client      加盟商
     * @param voucherList 促销方式列表
     */
    private void updatePromVariableTran(String client, List<PromDTO> voucherList) {
        if (ObjectUtils.isEmpty(client)) {
            return;
        }
        if (CollectionUtils.isEmpty(voucherList)) {
            return;
        }
        voucherList.forEach(promDTO -> {
            promDTO.getParamList().forEach(paramBean -> {
                SdPromVariableTran tran = new SdPromVariableTran();
                tran.setClient(client);
                tran.setGspvtVoucherId(promDTO.getId());
                tran.setGspvtType(paramBean.getPromType());
                tran.setGspvtColumns(paramBean.getKey());
                tran.setGspvtColumnsValue(paramBean.getValue());
                sdPromVariableTranMapper.updateValue(tran);
            });
        });
    }

    /**
     * 当前用户的权限下的门店编码集合
     *
     * @return
     */
    @Override
    public List<String> getStoCodeList() {
        TokenUser user = this.getLoginInfo();
        List<StoreData> list = storeDataMapper.getStoListByUserAuth(user.getClient(), user.getUserId());
        List<String> codeList = list.stream().map(StoreData::getStoCode).collect(Collectors.toList());
        return codeList;
    }

    @Override
    public String queryWapperSto() {
        List<String> stoCodeList = this.getStoCodeList();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            return null;
        }
        return queryWapper(stoCodeList);
    }

    @Override
    public String queryWapperSto(List<String> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return queryWapper(list);
    }

    @Override
    public String queryWapperAuto(List<String> list) {
        if (CollectionUtils.isEmpty(list)) {
            list = this.getStoCodeList();
        }
        return queryWapper(list);
    }
    @Override
    public String queryWapper(List<String> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(" in (");
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append("'");
            sb.append(list.get(i));
            sb.append("'");
        }
        sb.append(")");
        return sb.toString();
    }
}
