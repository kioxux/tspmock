package com.gov.operate.service;

import com.gov.operate.entity.Train;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-26
 */
public interface ITrainService extends SuperService<Train> {

    /**
     * 培训推送
     */
    void trainPush();

}
