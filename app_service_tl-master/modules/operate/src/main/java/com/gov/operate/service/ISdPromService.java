package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.response.Result;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdPromAssoConds;
import com.gov.operate.entity.SdPromGiftResult;
import com.gov.operate.entity.SdPromHead;
import com.gov.operate.entity.StoreData;

import java.util.List;

public interface ISdPromService {

    List<StoreData> getStoreList(GetStoreListVO getStoreListVO);

    IPage<QueryProListByStoreDto> getStoreProductList(GetStoreProductVO getStoreProductVO);

    Integer savePromList(SavePromListVO savePromListVO);

    Page<SdPromHead> queryPromList(QueryPromListVO queryPromListVO);

    SavePromListVO queryPromDetail(QueryPromDetailVO queryPromDetailVO);

    void updatePromList(SavePromListVO savePromListVO);

    List<StoreData> queryPromStoreList(QueryPromStoreListVO queryPromStoreListVO);

    SavePromListVO queryPromVoucherList(QueryPromStoreListVO queryPromStoreListVO);

    void updatePromVoucherStatus(UpdatePromVoucherStatusVO updatePromVoucherStatusVO);

    IPage<QueryProListByStoreDto> queryProListByStore(QueryProListByStoreVO queryProListByStoreVO);

    PromHyDto queryHySet(String storeCode);

    PromHyDto queryHyrDiscount();

    PromHyDto queryHyrPrice(String storeCode);

    void saveHySet(PromHyDto promHyDto);

    void saveHyrDiscount(PromHyDto promHyDto);

    List<QueryProStoreLsjDTO> queryProStoreLsj(QueryProStoreLsjVO queryProStoreLsjVO);

    void saveHyrPrice(PromHyDto promHyDto);

    /**
     * 修改促销活动状态
     *
     * @param gsphMarketid 活动id
     * @param gsphStatus
     */
    void updatePromStatus(Integer gsphMarketid, String gsphStatus);

    /**
     * 批量设置折扣
     *
     * @param queryProListByStoreVO
     * @return
     */
    List<QueryProListByStoreDto> setDiscountBatch(QueryProListByStoreVO queryProListByStoreVO);

    /**
     * 销售级别
     *
     * @return
     */
    List<String> getProSlaeClassList();

    /**
     * 是否发送电子券设置获取
     *
     * @return
     */
    Result getTicketFlag();

    /**
     * 是否发送电子券设置设置
     *
     * @param gcspPara1
     */
    void setTicketFlag(String gcspPara1);

    /**
     * 查询促销商品列表
     *
     * @param queryPromListVO
     * @return
     */
    Page<PromGoodsListVO> queryPromGoodsList(QueryPromListVO queryPromListVO);

    /**
     * 赠品类促销商品列表
     *
     * @param queryPromListVO
     * @return
     */
    Page<SdPromGiftResult> queryPromGiftProList(QueryPromListVO queryPromListVO);

    /**
     * 组合类促销商品列表
     *
     * @param queryPromListVO
     * @return
     */
    Page<SdPromAssoConds> queryPromAssoProList(QueryPromListVO queryPromListVO);

    /**
     * 单品类促销查询
     *
     * @param queryPromListVO
     * @return
     */
    List<PromUnitaryProductVO> queryPromUnitaryGood(QueryPromListVO queryPromListVO);

    /**
     * 赠品类促销查询
     *
     * @param queryPromListVO
     * @return
     */
    List<PromGiftProductVO> queryPromGiftGood(QueryPromListVO queryPromListVO);

    /**
     * 系列类促销查询
     *
     * @param queryPromListVO
     * @return
     */
    List<PromSeriesVO> queryPromSeriesGood(QueryPromListVO queryPromListVO);

    /**
     * 组合类促销查询
     *
     * @param queryPromListVO
     * @return
     */
    List<PromAssoVO> queryPromAssoGood(QueryPromListVO queryPromListVO);

    /**
     * 促销活动
     *
     * @param queryPromListVO
     * @return
     */
    List<SdPromHead> queryPromListForVoucherId(QueryPromListVO queryPromListVO);
}
