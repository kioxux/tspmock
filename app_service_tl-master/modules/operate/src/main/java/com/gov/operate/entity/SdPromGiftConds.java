package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_GIFT_CONDS")
@ApiModel(value="SdPromGiftConds对象", description="")
public class SdPromGiftConds extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPGC_VOUCHER_ID")
    private String gspgcVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPGC_SERIAL")
    private String gspgcSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPGC_PRO_ID")
    private String gspgcProId;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPGC_SERIES_ID")
    private String gspgcSeriesId;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPGC_MEM_FLAG")
    private String gspgcMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPGC_INTE_FLAG")
    private String gspgcInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPGC_INTE_RATE")
    private String gspgcInteRate;


}
