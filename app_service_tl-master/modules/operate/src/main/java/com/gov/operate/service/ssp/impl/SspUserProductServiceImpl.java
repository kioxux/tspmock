package com.gov.operate.service.ssp.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.JsonUtils;
import com.gov.common.excel.ImportExcel;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.validate.ValidateUtil;
import com.gov.common.validate.ValidationResult;
import com.gov.operate.dto.ssp.SspProductImportDTO;
import com.gov.operate.dto.ssp.SspUserProductPageDTO;
import com.gov.operate.entity.SspLog;
import com.gov.operate.entity.SspUserChain;
import com.gov.operate.entity.SspUserProduct;
import com.gov.operate.entity.SspUserSupplier;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SspLogMapper;
import com.gov.operate.mapper.SspUserChainMapper;
import com.gov.operate.mapper.SspUserProductMapper;
import com.gov.operate.mapper.SspUserSupplierMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ssp.ISspUserProductService;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SspUserProductServiceImpl extends ServiceImpl<SspUserProductMapper, SspUserProduct> implements ISspUserProductService {

    @Resource
    private CommonService commonService;

    @Resource
    private SspUserSupplierMapper sspUserSupplierMapper;

    @Resource
    private SspUserChainMapper sspUserChainMapper;

    @Resource
    private SspLogMapper sspLogMapper;

    @Override
    public Result getSupplierProductList(SspUserProductPageDTO sspUserProductPageDTO) {
        Page<SspUserProduct> page = new Page<>(sspUserProductPageDTO.getPageNum(), sspUserProductPageDTO.getPageSize());
        TokenUser currentUser = commonService.getLoginInfo();
//        TokenUser currentUser = new TokenUser();
//        currentUser.setClient("10000013");
        sspUserProductPageDTO.setClient(currentUser.getClient());
        // 统一社会信用码
        if (CollUtil.isEmpty(sspUserProductPageDTO.getCreditCodeList())) {
            List<SspUserSupplier> supList = sspUserSupplierMapper.selectList(
                    new LambdaQueryWrapper<SspUserSupplier>().eq(SspUserSupplier::getUserId, sspUserProductPageDTO.getUserId())
                            .eq(SspUserSupplier::getClient, currentUser.getClient()));
            if (supList.size() > 0) {
                sspUserProductPageDTO.setCreditCodeList(supList.stream().map(SspUserSupplier::getCreditCode).collect(Collectors.toList()));
            }
        }
        // 主体
        if (CollUtil.isEmpty(sspUserProductPageDTO.getChainHeadList())) {
            List<SspUserChain> chainList = sspUserChainMapper.selectList(
                    new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, sspUserProductPageDTO.getUserId())
                            .eq(SspUserChain::getClient, currentUser.getClient()));
            if (chainList.size() > 0) {
                sspUserProductPageDTO.setChainHeadList(chainList.stream().map(SspUserChain::getChainHead).collect(Collectors.toList()));
            }
        }

        IPage<SspUserProduct> userPageVOIPage = baseMapper.selectSupplierProductByPage(page, sspUserProductPageDTO);
        return ResultUtil.success(userPageVOIPage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addUserProduct(List<SspUserProduct> productList, Long userId) {
        if (CollUtil.isEmpty(productList)) {
            throw new CustomResultException("授权品种不能为空");
        }
        TokenUser currentUser = commonService.getLoginInfo();
//        TokenUser currentUser = new TokenUser();
//        currentUser.setClient("10000013");
//        currentUser.setUserId("1");

        List<SspUserProduct> existList = baseMapper.selectList(
                new LambdaQueryWrapper<SspUserProduct>().eq(SspUserProduct::getUserId, userId)
                        .eq(SspUserProduct::getClient, currentUser.getClient()));

        productList.forEach(t -> {
            SspUserProduct existData = existList.stream().filter(a ->
                            a.getClient().equals(t.getClient())
                                    && a.getCreditCode().equals(t.getCreditCode())
                                    && a.getChainHead().equals(t.getChainHead())
                                    && a.getProSelfCode().equals(t.getProSelfCode()))
                    .findFirst().orElse(null);
            if (existData != null) {
                throw new CustomResultException(StrUtil.format("供应商：{}，主体：{}，商品：{}已存在",
                        existData.getSupName(), existData.getChainName(), existData.getProName()));
            }
            t.setUserId(userId);
            t.setIsDelete(0);
            t.setCreateBy(currentUser.getUserId());
            t.setCreateTime(new Date());
        });
        // 批量保存
        saveBatch(productList);

        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        log.setContent(StrUtil.format("添加授权品种,{}", productList.stream().map(SspUserProduct::getProName).collect(Collectors.joining())));
        log.setJson(JsonUtils.beanToJson(productList));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);

        return ResultUtil.success();
    }

    /**
     * 移除
     *
     * @param productList
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result delUserProduct(List<SspUserProduct> productList) {
        baseMapper.deleteBatchIds(productList.stream().map(SspUserProduct::getId).collect(Collectors.toList()));

        TokenUser currentUser = commonService.getLoginInfo();
        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        log.setContent(StrUtil.format("移除授权品种-{}", productList.stream().map(SspUserProduct::getProName).collect(Collectors.joining())));
        log.setJson(JsonUtils.beanToJson(productList));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);
        return ResultUtil.success();
    }

    @Override
    public Result getUserProductList(SspUserProductPageDTO sspUserProductPageDTO) {
        Page<SspUserProduct> page = new Page<>(sspUserProductPageDTO.getPageNum(), sspUserProductPageDTO.getPageSize());
        TokenUser currentUser = commonService.getLoginInfo();
        sspUserProductPageDTO.setClient(currentUser.getClient());
        // 统一社会信用码
        if (CollUtil.isEmpty(sspUserProductPageDTO.getCreditCodeList())) {
            List<SspUserSupplier> supList = sspUserSupplierMapper.selectList(
                    new LambdaQueryWrapper<SspUserSupplier>().eq(SspUserSupplier::getUserId, sspUserProductPageDTO.getUserId())
                            .eq(SspUserSupplier::getClient, currentUser.getClient()));
            if (supList.size() > 0) {
                sspUserProductPageDTO.setCreditCodeList(supList.stream().map(SspUserSupplier::getCreditCode).collect(Collectors.toList()));
            }
        }
        // 主体
        if (CollUtil.isEmpty(sspUserProductPageDTO.getChainHeadList())) {
            List<SspUserChain> chainList = sspUserChainMapper.selectList(
                    new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, sspUserProductPageDTO.getUserId())
                            .eq(SspUserChain::getClient, currentUser.getClient()));
            if (chainList.size() > 0) {
                sspUserProductPageDTO.setChainHeadList(chainList.stream().map(SspUserChain::getChainHead).collect(Collectors.toList()));
            }
        }

        IPage<SspUserProduct> userPageVOIPage = baseMapper.selectUserProductByPage(page, sspUserProductPageDTO);
        return ResultUtil.success(userPageVOIPage);
    }

    @Override
    public int deleteChainHead(Long userId, String client, Set<String> keySet) {
        return baseMapper.deleteChainHead(userId, client, keySet);
    }

    @SneakyThrows
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result importSupplierProductList(MultipartFile file, Long userId) {
        ImportExcel importExcel = new ImportExcel(file, 0, 0);
        List<SspProductImportDTO> importList = importExcel.getDataList(SspProductImportDTO.class);
        TokenUser currentUser = commonService.getLoginInfo();
        List<String> errorList = new ArrayList<>();

        List<SspUserSupplier> sspUserSupplierList = sspUserSupplierMapper.selectList(new LambdaQueryWrapper<SspUserSupplier>()
                .eq(SspUserSupplier::getClient, currentUser.getClient())
                .eq(SspUserSupplier::getUserId, userId));
        if (sspUserSupplierList.size() == 0) {
            errorList.add("当前用户未绑定供应商");
            return ResultUtil.success(errorList);
        }

        List<SspUserChain> chainList = sspUserChainMapper.selectList(
                new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, userId)
                        .eq(SspUserChain::getClient, currentUser.getClient()));

        if (chainList.size() == 0) {
            errorList.add("当前用户未绑定主体");
            return ResultUtil.success(errorList);
        }

        List<String> chainHeadList = chainList.stream().map(SspUserChain::getChainHead).collect(Collectors.toList());

        List<SspUserProduct> productList = baseMapper.selectProductByChain(currentUser.getClient(), chainHeadList);


        List<SspUserProduct> existProList = baseMapper.selectList(new LambdaQueryWrapper<SspUserProduct>()
                .eq(SspUserProduct::getClient, currentUser.getClient())
                .eq(SspUserProduct::getUserId, userId));

        List<SspUserProduct> addProList = new ArrayList<>();
        int lineNo = 1;
        for (SspProductImportDTO importDTO : importList) {
            ValidationResult result = ValidateUtil.validateEntity(importDTO);
            Integer finalLineNo = lineNo;
            if (result.isHasErrors()) {
                result.getErrorMsg().forEach((k, v) -> {
                    errorList.add(StrUtil.format("第[{}]行{}", finalLineNo, v));
                });
            }
            SspUserSupplier sspUserSupplier = sspUserSupplierList.stream().filter(t -> t.getCreditCode().equals(importDTO.getCreditCode())).findFirst().orElse(null);
            if (sspUserSupplier == null) {
                errorList.add(StrUtil.format("第[{}]行统一社会信用码[{}]供应商未绑定", finalLineNo, importDTO.getCreditCode()));
            }

            SspUserChain sspUserChain = chainList.stream().filter(t -> t.getChainHead().equals(importDTO.getChainHead()))
                    .findFirst().orElse(null);
            if (sspUserChain == null || StrUtil.isBlank(sspUserChain.getChainHead())) {
                errorList.add(StrUtil.format("第[{}]行主体编码[{}]连锁未绑定", finalLineNo, importDTO.getCreditCode()));
            }
            SspUserProduct product = null;
            if (sspUserChain != null) {
                product = productList.stream()
                        .filter(t -> t.getChainHead().equals(sspUserChain.getChainHead())
                                && t.getProSelfCode().equals(importDTO.getProSelfCode())).findFirst().orElse(null);
                if (product == null) {
                    errorList.add(StrUtil.format("第[{}]行商品自编码[{}]商品未绑定", finalLineNo, importDTO.getProSelfCode()));
                }

                if (existProList.stream().anyMatch(t -> t.getChainHead().equals(sspUserChain.getChainHead())
                        && t.getProSelfCode().equals(importDTO.getProSelfCode()))) {
                    errorList.add(StrUtil.format("第[{}]行商品自编码[{}]商品已存在", finalLineNo, importDTO.getProSelfCode()));
                }

                if (addProList.stream().anyMatch(t -> t.getChainHead().equals(sspUserChain.getChainHead())
                        && t.getProSelfCode().equals(importDTO.getProSelfCode()))) {
                    errorList.add(StrUtil.format("第[{}]行商品自编码[{}]商品已存在", finalLineNo, importDTO.getProSelfCode()));
                }
            }

            if (errorList.size() == 0) {
                SspUserProduct sspUserProduct = new SspUserProduct();
                if (product != null) {
                    BeanUtils.copyProperties(product, sspUserProduct);
                }
                sspUserProduct.setUserId(userId);
                sspUserProduct.setClient(currentUser.getClient());
                if (sspUserSupplier != null) {
                    sspUserProduct.setSupName(sspUserSupplier.getSupName());
                    sspUserProduct.setCreditCode(sspUserSupplier.getCreditCode());
                }
                sspUserProduct.setChainHead(sspUserChain.getChainHead());
                sspUserProduct.setChainName(sspUserChain.getChainName());
                sspUserProduct.setIsDelete(0);
                sspUserProduct.setCreateBy(currentUser.getUserId());
                sspUserProduct.setCreateTime(new Date());
                addProList.add(sspUserProduct);
            }
            lineNo++;
        }

        if (addProList.size() > 0) {
            saveBatch(addProList);
        }

        return ResultUtil.success(errorList);
    }
}




