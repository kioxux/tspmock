package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.CommonData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
public interface ICommonDataService extends SuperService<CommonData> {

    /**
     * 功能说明
     */
    CommonData funDesc();
}
