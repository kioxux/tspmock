package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromGiftConds;
import com.gov.operate.mapper.SdPromGiftCondsMapper;
import com.gov.operate.service.ISdPromGiftCondsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-11
 */
@Service
public class SdPromGiftCondsServiceImpl extends ServiceImpl<SdPromGiftCondsMapper, SdPromGiftConds> implements ISdPromGiftCondsService {

}
