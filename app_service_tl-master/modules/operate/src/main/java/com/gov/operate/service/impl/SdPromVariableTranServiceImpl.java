package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.dto.PromDTO;
import com.gov.operate.entity.SdPromVariableTran;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdMarketingMapper;
import com.gov.operate.mapper.SdMarketingPromMapper;
import com.gov.operate.mapper.SdPromVariableSetMapper;
import com.gov.operate.mapper.SdPromVariableTranMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdPromVariableTranService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
@Slf4j
@Service
public class SdPromVariableTranServiceImpl extends ServiceImpl<SdPromVariableTranMapper, SdPromVariableTran> implements ISdPromVariableTranService {

    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingMapper sdMarketingMapper;
    @Resource
    private SdPromVariableTranMapper sdPromVariableTranMapper;
    @Resource
    private SdPromVariableSetMapper sdPromVariableSetMapper;
    @Resource
    private SdMarketingPromMapper sdMarketingPromMapper;


    /**
     * 促销方式列表
     */
    @Override
    public List<PromDTO> getPromList(String marketId) {
        TokenUser user = commonService.getLoginInfo();
        List<PromDTO> list = new ArrayList<>();

        /**

        // 指定营销活动下得促销方式id列表
        List<String> promIdList = sdMarketingPromMapper.selectList(new QueryWrapper<SdMarketingProm>()
                .eq("CLIENT", user.getClient())
                .eq("GSMP_MARKETID", marketId))
                .stream()
                .map(SdMarketingProm::getGsmpPromo)
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(promIdList)) {
            return list;
        }
        // 查询默认值
        List<PromVariableTranDTO> promDefaultValue = sdPromVariableTranMapper.getPromDefaultValue(user.getClient(), promIdList);
        if (CollectionUtils.isEmpty(promDefaultValue)) {
            return list;
        }
        Map<String, Map<String, List<PromVariableTranDTO>>> collect = promDefaultValue.stream()
                .collect(Collectors.groupingBy(PromVariableTranDTO::getGspvtVoucherId, Collectors.groupingBy(PromVariableTranDTO::getGspvtType)));

        List<String> typeList = Arrays.asList(OperateEnum.GspvtType.values()).stream().map(OperateEnum.GspvtType::getCode).collect(Collectors.toList());
        collect.forEach((id, promMap) -> {
            // 行
            if (!CollectionUtils.isEmpty(promMap)) {
                // 行实体
                PromDTO promDTO = new PromDTO();
                list.add(promDTO);
                // id
                promDTO.setId(id);
                // name
                String columnsValue = promMap.get(PromDTO.PROM_HEADER).get(0).getGspvtColumnsValue();
                promDTO.setName(columnsValue);
                // 是否需要反选商品
                promMap.forEach((gspvtType, paramlist)->{
                    if (!gspvtType.equals(PromDTO.PROM_HEADER)) {
                        promDTO.setPromType(gspvtType);
                    }
                });

                // 行 单元格
                List<PromDTO.ParamListBean> paramList = new ArrayList<>();
                promDTO.setParamList(paramList);
                typeList.forEach(type -> {
                    List<PromVariableTranDTO> tranDTOS = promMap.get(type);
                    if (CollectionUtils.isEmpty(tranDTOS)) {
                        return;
                    }
                    tranDTOS.stream().forEach(tran -> {
                        PromDTO.ParamListBean paramListBean = new PromDTO.ParamListBean();
                        paramListBean.setKey(tran.getGspvtColumns());
                        paramListBean.setValue(tran.getGspvtColumnsValue());
                        paramListBean.setName(tran.getGspvtColumnsName());
                        paramListBean.setType(tran.getGspvsColumnsType());
                        paramListBean.setLength(tran.getGspvsColumnsLength());
                        paramListBean.setPromType(tran.getGspvtType());
                        paramListBean.setPromTableName(tran.getGspvsTable());
                        paramList.add(paramListBean);
                    });
                });
            }
        });

         */

        return list;
    }
}
