package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.entity.DocumentInvoiceBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
public interface DocumentInvoiceBillMapper extends BaseMapper<DocumentInvoiceBill> {

    /**
     * 单据已登记发票
     *
     * @param client
     * @param matDnId
     * @param type
     * @return
     */
    List<FicoInvoiceInformationRegistrationDTO> selectInvoiceListByDoc(@Param("client") String client, @Param("matDnId") String matDnId, @Param("type") String type);

    /**
     * 可登记发票
     * @param page
     * @param client
     * @param siteCode
     * @param supCode
     * @param invoiceNum
     * @param invoiceDateStart
     * @param invoiceDateEnd
     * @return
     */
    IPage<FicoInvoiceInformationRegistrationDTO> selectInvoiceListForBill(Page page, @Param("client") String client, @Param("siteCode")String siteCode,
                                                                          @Param("supCode") String supCode, @Param("invoiceNum") String invoiceNum,
                                                                          @Param("invoiceDateStart") String invoiceDateStart, @Param("invoiceDateEnd") String invoiceDateEnd);
}
