package com.gov.operate.mapper;

import com.gov.operate.dto.invoiceOptimiz.DocumentBillDetailsDTO;
import com.gov.operate.entity.DocumentBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
public interface DocumentBillMapper extends BaseMapper<DocumentBill> {

    /**
     * 对账单号
     * @param client
     * @return
     */
    Long selectBillNum(@Param("client") String client);

    /**
     * 单据对账详情
     *
     * @param id
     * @param client
     * @return
     */
    DocumentBillDetailsDTO getDocumentBill(@Param("id") String id, @Param("client") String client);

}
