package com.gov.operate.service.member;

import com.gov.operate.dto.member.ConsumeInfoDTO;
import com.gov.operate.dto.member.MemberInfoDTO;
import com.gov.operate.dto.member.MemberInfoDetailsDTO;

import java.util.List;

public interface MemberManagementService {

    // 根据手机号模糊查询会员列表
    List<MemberInfoDTO> getMemberListByNum(String mobile);

    // 根据会员id查询会员明细
    MemberInfoDetailsDTO getMemberInfoById(String gsmbMemberId,String jobId);

    // 根据会员id查询会员消费记录
    ConsumeInfoDTO getOrderInfoById(String gsmbMemberId, String startDate, String endDate);
}
