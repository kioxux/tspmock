package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetPromotionListDTO {

    /**
     * 促销方式
     */
    private String gspvsMode;
    /**
     * 促销方式描述
     */
    private String gspvsModeName;
    /**
     * 促销类型
     */
    private String gspvsType;
    /**
     * 促销类型描述
     */
    private String gspvsTypeName;


}
