package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class StoreSalesStatisticsResponse {

    @ApiModelProperty(value = "本期")
    private List<Map<String, StoreSalesThisWeek>> thisPeriod;

    @ApiModelProperty(value = "上期")
    private List<Map<String, StoreSalesLastWeek>> previousPeriod;

    @ApiModelProperty(value = "同期")
    private List<Map<String, StoreSalesTheSamePeriod>> theSamePeriod;

    @Data
    public static class StoreSalesThisWeek {

        @ApiModelProperty(value = "日期")
        private String date;

        @ApiModelProperty(value = "加盟商")
        private String client;

        @ApiModelProperty(value = "实收金额")
        private BigDecimal actualAmount;

        @ApiModelProperty(value = "毛利额")
        private BigDecimal grossProfit;

        @ApiModelProperty(value = "毛利率")
        private String grossMargin;

        @ApiModelProperty(value = "会员卡")
        private int membershipCard;

        @ApiModelProperty(value = "交易次数")
        private int numberOfTransactions;

        @ApiModelProperty(value = "客单价")
        private BigDecimal customerPrice;

        @ApiModelProperty(value = "客品次")
        private BigDecimal customerOrder;

        @ApiModelProperty(value = "品单价")
        private BigDecimal unitPrices;

        @ApiModelProperty(value = "单店日均销售额")
        private BigDecimal averageDailySales;

        @ApiModelProperty(value = "单店日均毛利额")
        private BigDecimal averageDailyGrossProfit;

        @ApiModelProperty(value = "单店日均交易")
        private BigDecimal averageDailyTransaction;

        @ApiModelProperty(value = "达成率")
        private String achievementRate;

        @ApiModelProperty(value = "达成额度")
        private BigDecimal achieveTheQuota;

    }

    @Data
    public static class StoreSalesLastWeek {

        @ApiModelProperty(value = "加盟商")
        private String client;

        @ApiModelProperty(value = "实收金额")
        private BigDecimal actualAmount;

        @ApiModelProperty(value = "毛利额")
        private BigDecimal grossProfit;

        @ApiModelProperty(value = "毛利率")
        private String grossMargin;

        @ApiModelProperty(value = "会员卡")
        private int membershipCard;

        @ApiModelProperty(value = "交易次数")
        private int numberOfTransactions;

        @ApiModelProperty(value = "客单价")
        private BigDecimal customerPrice;

        @ApiModelProperty(value = "客品次")
        private BigDecimal customerOrder;

        @ApiModelProperty(value = "品单价")
        private BigDecimal unitPrices;

        @ApiModelProperty(value = "单店日均销售额")
        private BigDecimal averageDailySales;

        @ApiModelProperty(value = "单店日均毛利额")
        private BigDecimal averageDailyGrossProfit;

        @ApiModelProperty(value = "单店日均交易")
        private BigDecimal averageDailyTransaction;

        @ApiModelProperty(value = "达成率")
        private String achievementRate;

        @ApiModelProperty(value = "达成额度")
        private BigDecimal achieveTheQuota;

    }

    @Data
    public static class StoreSalesTheSamePeriod {

        @ApiModelProperty(value = "加盟商")
        private String client;

        @ApiModelProperty(value = "实收金额")
        private BigDecimal actualAmount;

        @ApiModelProperty(value = "毛利额")
        private BigDecimal grossProfit;

        @ApiModelProperty(value = "毛利率")
        private String grossMargin;

        @ApiModelProperty(value = "会员卡")
        private int membershipCard;

        @ApiModelProperty(value = "交易次数")
        private int numberOfTransactions;

        @ApiModelProperty(value = "客单价")
        private BigDecimal customerPrice;

        @ApiModelProperty(value = "客品次")
        private BigDecimal customerOrder;

        @ApiModelProperty(value = "品单价")
        private BigDecimal unitPrices;

        @ApiModelProperty(value = "单店日均销售额")
        private BigDecimal averageDailySales;

        @ApiModelProperty(value = "单店日均毛利额")
        private BigDecimal averageDailyGrossProfit;

        @ApiModelProperty(value = "单店日均交易")
        private BigDecimal averageDailyTransaction;

        @ApiModelProperty(value = "达成率")
        private String achievementRate;

        @ApiModelProperty(value = "达成额度")
        private BigDecimal achieveTheQuota;

    }
}
