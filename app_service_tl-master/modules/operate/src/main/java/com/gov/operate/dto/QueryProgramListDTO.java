package com.gov.operate.dto;

import com.gov.operate.entity.SalaryProgram;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryProgramListDTO extends SalaryProgram {

    /**
     * 创建人
     */
    private String gspCreateUserName;
}
