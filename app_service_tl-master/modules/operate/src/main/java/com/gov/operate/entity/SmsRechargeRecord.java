package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SMS_RECHARGE_RECORD")
@ApiModel(value="SmsRechargeRecord对象", description="")
public class SmsRechargeRecord extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "购买时间")
    @TableField("GSRR_RECHARGE_TIME")
    private String gsrrRechargeTime;

    @ApiModelProperty(value = "单价")
    @TableField("GSRR_RECHARGE_PRICE")
    private Integer gsrrRechargePrice;

    @ApiModelProperty(value = "条数")
    @TableField("GSRR_RECHARGE_COUNT")
    private Integer gsrrRechargeCount;

    @ApiModelProperty(value = "金额")
    @TableField("GSRR_RECHARGE_AMT")
    private Integer gsrrRechargeAmt;

    @ApiModelProperty(value = "已用条数")
    @TableField("GSRR_RECHARGE_USE_COUNT")
    private Integer gsrrRechargeUseCount;

    @ApiModelProperty(value = "付款订单号")
    @TableField("FICO_ID")
    private String ficoId;

    @ApiModelProperty(value = "付款状态")
    @TableField("GSRR_PAY_STATUS")
    private Integer gsrrPayStatus;


}
