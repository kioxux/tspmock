package com.gov.operate.dto;

import com.gov.operate.feign.dto.MatchSchedule;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductMatchConfirmInData implements Serializable {


    private static final long serialVersionUID = 1426562230711955302L;

    /**
     * 加盟商
     */
    private String clientId;
    /**
     * 门店编码字符串（多选）
     */
    private String stoCode;

    private String telNum;

    private Boolean scFlag;

    private Boolean zlFlag;

    private Boolean clientFlag;//true为加盟商 false为门店

    private String endTime;

    private String yesOrNo;//"Y表示确定 N表示取消"
}
