package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户收费维护明细表
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_CHARGE_MAINTENANCE_M")
@ApiModel(value="UserChargeMaintenanceM对象", description="用户收费维护明细表")
public class UserChargeMaintenanceM extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "主表主键")
    @TableField("SPH_ID")
    private Integer sphId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "服务包ID")
    @TableField("SP_ID")
    private String spId;

    @ApiModelProperty(value = "服务包名称")
    @TableField("SP_NAME")
    private String spName;

    @ApiModelProperty(value = "服务包类型")
    @TableField("SP_TYPE")
    private String spType;

    @ApiModelProperty(value = "收费类型")
    @TableField("SP_FREE")
    private String spFree;

    @ApiModelProperty(value = "收费价格")
    @TableField("SP_PRICE")
    private BigDecimal spPrice;

    @ApiModelProperty(value = "实际收费价格")
    @TableField("SP_PRICE_REALISTIC")
    private BigDecimal spPriceRealistic;

    @ApiModelProperty(value = "创建人账号")
    @TableField("SP_CRE_ID")
    private String spCreId;

    @ApiModelProperty(value = "是否已发")
    @TableField("SP_SEND")
    private Integer spSend;

    @ApiModelProperty(value = "创建时间")
    @TableField("SP_CRE_TIME")
    private String spCreTime;

    @ApiModelProperty(value = "修改日期")
    @TableField("SP_MODI_DATE")
    private String spModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("SP_MODI_TIME")
    private String spModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("SP_MODI_ID")
    private String spModiId;

    @ApiModelProperty(value = "付款主体编码")
    @TableField("PAYINGSTORE_CODE")
    private String payingstoreCode;

    @ApiModelProperty(value = "付款主体名称")
    @TableField("PAYINGSTORE_NAME")
    private String payingstoreName;

    @ApiModelProperty(value = "免费开始日期")
    @TableField("FREE_STARTING_DATE")
    private String freeStartingDate;

    @ApiModelProperty(value = "免费结束日期")
    @TableField("FREE_ENDING_DATE")
    private String freeEndingDate;

    @ApiModelProperty(value = "开始日期")
    @TableField("STARTING_DATE")
    private String startingDate;

    @ApiModelProperty(value = "结束日期")
    @TableField("ENDING_DATE")
    private String endingDate;

    @ApiModelProperty(value = "系列")
    @TableField("SP_SERIES")
    private String spSeries;

    @ApiModelProperty(value = "服务包说明")
    @TableField("SP_REMARK")
    private String spRemark;

    @ApiModelProperty(value = "计费类型")
    @TableField("SP_MODE")
    private Integer spMode;

    @ApiModelProperty(value = "服务包ID")
    @TableField("RID")
    private Integer rId;
}
