package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class StoreSalesRankingResponse {

    @ApiModelProperty(value = "门店数量")
    private int storeCount;

    @ApiModelProperty(value = "门店数量")
    private List<StoreSalesRanking> storeSalesRankings;

    @ApiModelProperty(value = "0：营业额 1：毛利额 2：会员卡       3 :毛利率 4.交易次数 5客单价 6客品次7品单价8单店日均销售9单店日均毛利 10单店日均交易")
    private Integer type;
    @Data
    public static class StoreSalesRanking {


        @ApiModelProperty(value = "加盟商")
        private String client;

        @ApiModelProperty(value = "实收金额")
        private BigDecimal actualAmount = BigDecimal.ZERO;

        @ApiModelProperty(value = "实收金额达成率排序")
        private BigDecimal actualAchievement = BigDecimal.ZERO;

        @ApiModelProperty(value = "实收金额达成率")
        private String actualAchievementRate = "0.00%";

        @ApiModelProperty(value = "毛利额")
        private BigDecimal grossProfitAmount = BigDecimal.ZERO;

        @ApiModelProperty(value = "毛利额达成率排序")
        private BigDecimal grossProfitAchievement = BigDecimal.ZERO;

        @ApiModelProperty(value = "毛利额达成率中文")
        private String grossProfitAchievementRate = "0.00%";

        @ApiModelProperty(value = "毛利率")
        private String grossMarginRate = "0.00%";

        @ApiModelProperty(value = "毛利率")
        private BigDecimal grossMargin = BigDecimal.ZERO;

        @ApiModelProperty(value = "会员卡")
        private int memberCard = 0;

        @ApiModelProperty(value = "会员卡达成率")
        private String memberCardRate = "0.00%";

        @ApiModelProperty(value = "会员卡达成率排序")
        private BigDecimal card = BigDecimal.ZERO;

        @ApiModelProperty(value = "会员卡达成目标")
        private BigDecimal memberCardTarget = BigDecimal.ZERO;

        @ApiModelProperty(value = "交易次数")
        private int tradeCount = 0;

        @ApiModelProperty(value = "客单价")
        private BigDecimal customerPrice = BigDecimal.ZERO;

        @ApiModelProperty(value = "客品次")
        private BigDecimal customerOrder = BigDecimal.ZERO;

        @ApiModelProperty(value = "品单价")
        private BigDecimal unitPrices = BigDecimal.ZERO;

        @ApiModelProperty(value = "单店日均销售额")
        private BigDecimal averageDailySales = BigDecimal.ZERO;

        @ApiModelProperty(value = "单店日均毛利额")
        private BigDecimal averageDailyGrossProfit = BigDecimal.ZERO;

        @ApiModelProperty(value = "单店日均交易")
        private BigDecimal averageDailyTransaction = BigDecimal.ZERO;

        @ApiModelProperty(value = "门店")
        private String store;

        @ApiModelProperty(value = "门店经理")
        private String storeManager;

        @ApiModelProperty(value = "工号")
        private String employeeId;

        @ApiModelProperty(value = "门店名称")
        private String storeName;

        @ApiModelProperty(value = "成本额")
        private BigDecimal costAmount = BigDecimal.ZERO;

        @ApiModelProperty(value = "商品编码计数之和")
        private int productCount = 0;

        @ApiModelProperty(value = "销售量")
        private int salesVolume = 0;

        @ApiModelProperty(value = "销售天数")
        private int salesDays = 0;

        @ApiModelProperty(value = "销售量")
        private BigDecimal qtyCount = BigDecimal.ZERO;

        //----------------------------------------------------------------


        @ApiModelProperty(value = "金额")
        private String amt;

        @ApiModelProperty(value = "达成率")
        private String achievementRate;
    }
}
