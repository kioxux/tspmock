package com.gov.operate.dto;

import com.gov.operate.entity.SdSusjobH;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SdSusjobHDTO extends SdSusjobH {

    /**
     * 任务门店名称
     */
    private String susJobbrName;
}
