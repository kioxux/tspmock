package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.DateUtils;
import com.gov.common.utils.CommonUtil;
import com.gov.operate.entity.ProductMatchZ;
import com.gov.operate.entity.ProductMatchZResult;
import com.gov.operate.entity.SystemPara;
import com.gov.operate.entity.WmsTupianzdrwcj;
import com.gov.operate.feign.ReportFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.MatchProductInData;
import com.gov.operate.feign.dto.MatchSchedule;
import com.gov.operate.mapper.ProductMatchMapper;
import com.gov.operate.mapper.ProductMatchZMapper;
import com.gov.operate.mapper.ProductMatchZResultMapper;
import com.gov.operate.mapper.SystemParaMapper;
import com.gov.operate.service.IProductMatchZService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.service.IWmsTupianzdrwcjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 导入商品匹配信息主表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-09-27
 */
@Service
public class ProductMatchZServiceImpl extends ServiceImpl<ProductMatchZMapper, ProductMatchZ> implements IProductMatchZService {

    @Autowired
    private ProductMatchZMapper productMatchZMapper;

    @Autowired
    private SystemParaMapper systemParaMapper;

    @Autowired
    private ReportFeign reportFeign;
    @Autowired
    private IWmsTupianzdrwcjService wmsTupianzdrwcjService;
    @Autowired
    private ProductMatchZResultMapper productMatchZResultMapper;

    @Autowired
    private ProductMatchMapper productMatchMapper;


    private List<SystemPara> getSystemParaByCode(String code) {
        Map<String, Object> query = new HashMap<>();
        query.put("CODE", code);
        return systemParaMapper.selectByMap(query);
    }

    @Override
    public void confirmNoNotify() {
        List<SystemPara> productCheckTimes = getSystemParaByCode("PRODUCT_CHECK_DOUBLE_SEND_TIME");
        Date nowDate = new Date();
        String productCheckDoubleSendTime = productCheckTimes.get(0).getModel();
        //查询出所有的商品匹配记录，并进行循环验证
        QueryWrapper<ProductMatchZ> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("MATCH_DATA_CHECK_FLAG", "0");
        List<ProductMatchZ> productMatchZS = productMatchZMapper.selectList(queryWrapper);
        if (CollectionUtil.isNotEmpty(productMatchZS)) {
            //先赛选出用户选择取消的列表
            List<ProductMatchZ> collect = productMatchZS.stream().filter(x -> "0".equals(x.getMatchDataCheckFlag())).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(collect)) {
                for (ProductMatchZ z : collect) {
                    if (z.getMatchCancelCount() == null || z.getMatchCancelCount() < 2) {
                        //筛选需要二次推送的数据
                        LocalDateTime matchDataCheckTime = z.getMatchDataCheckTime();
                        long dif = DateUtil.between(Date.from(matchDataCheckTime.atZone(ZoneId.systemDefault()).toInstant()), nowDate, DateUnit.DAY, false);
                        if (Long.parseLong(productCheckDoubleSendTime) == dif) {
                            //表示需要进行二次推送，这个时候将MATCH_DATA_CHECK_FLAG改成3，这也就可以拉取
                            z.setMatchDataCheckFlag("3");
                            z.setMatchSecondPushFlag("1");
                            LambdaQueryWrapper<ProductMatchZ> updateWrapper = new LambdaQueryWrapper();
                            updateWrapper.eq(ProductMatchZ::getClient, z.getClient());
                            updateWrapper.eq(ProductMatchZ::getMatchCode, z.getMatchCode());
                            productMatchZMapper.update(z, updateWrapper);
                        }
                    }
                }
            }
        }
    }


    private boolean ifTesultExsit(ProductMatchZ z) {
        LambdaQueryWrapper<ProductMatchZResult> updateWrapper = new LambdaQueryWrapper();
        updateWrapper.eq(ProductMatchZResult::getClient, z.getClient());
        updateWrapper.eq(ProductMatchZResult::getMatchCode, z.getMatchCode());
        updateWrapper.eq(ProductMatchZResult::getCreDate, CommonUtil.getyyyyMMdd());
        updateWrapper.eq(ProductMatchZResult::getMatchCode, z.getMatchCode());
        updateWrapper.eq(ProductMatchZResult::getCheckTouchTel, z.getMatchDataCheckTouchTel());
        updateWrapper.eq(ProductMatchZResult::getCheckFlag,"0");
        Integer integer = productMatchZResultMapper.selectCount(updateWrapper);
        if (integer > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void resultNotifyJobHandler() {
        List<SystemPara> productCheckTimes = getSystemParaByCode("PRODUCT_CHECK_TIME");
        String productCheckTime = "";
        if (CollectionUtil.isNotEmpty(productCheckTimes)) {
            productCheckTime = productCheckTimes.get(0).getModel();
        }
        List<ProductMatchZ> caculateList = new ArrayList<>();

        //查询出所有的商品匹配记录，并进行循环验证
        QueryWrapper<ProductMatchZ> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("MATCH_DATA_CHECK_FLAG", "1");
        List<ProductMatchZ> productMatchZS = productMatchZMapper.selectList(queryWrapper);
        if (CollectionUtil.isNotEmpty(productMatchZS)) {
            //先赛选出用户选择开始任务的列表
            List<ProductMatchZ> collect = productMatchZS;
            if (CollectionUtil.isNotEmpty(collect)) {
                for (ProductMatchZ z : collect) {
                    //筛选需要二次推送的数据
                    LocalDateTime matchDataCheckTime = z.getMatchDataCheckTime();
                    String matchDataCheckEndDate = z.getMatchDataCheckEndDate();
                    try {
                        Date nowDate = new Date();
                        Date endDate = DateUtils.stringToDateEnd(matchDataCheckEndDate);//截止时间为选择时间的最终时间
                        long dif = DateUtil.between(endDate, nowDate, DateUnit.DAY, false);
                        if ((Long.parseLong(productCheckTime) + 2) == dif) {
                            caculateList.add(z);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

            }

            if (CollectionUtil.isNotEmpty(caculateList)) {
                List<ProductMatchZ> clientTypeList = caculateList.stream().filter(x -> x.getMatchType().equals("0")).collect(Collectors.toList());
                List<ProductMatchZ> stoTypeList = caculateList.stream().filter(x -> x.getMatchType().equals("1")).collect(Collectors.toList());
                if (CollectionUtil.isNotEmpty(clientTypeList)) {
                    for (ProductMatchZ z : clientTypeList) {
//                        MatchSchedule matchSchedule = new MatchSchedule();
                        //查询校验前的完整度
//                        MatchProductInData queryData = new MatchProductInData();
//                        queryData.setClientId(z.getClient());
//                        JsonResult result = reportFeign.getProductMatch(queryData);//获取推送的汇总数据
//                        if (result != null && result.getCode() == 0) {
//                            LinkedHashMap data = ((LinkedHashMap) result.getData());
//                            matchSchedule = JSON.parseObject(JSON.toJSONString(data), MatchSchedule.class);
//                        }
                        //首先需要查询原始数据
                        //此处需要更改,意思是需要汇总匹配中的实际情况
                        Integer matchAllCount = productMatchMapper.selectAllMatchCount(z.getClient(), "", z.getMatchCode());
                        Integer unMatchCount = productMatchMapper.selectUnMatchCount(z.getClient(), "", z.getMatchCode());
                        Integer matchCount = productMatchMapper.selectMatchCount(z.getClient(), "", z.getMatchCode());
                        MatchSchedule matchSchedule = new MatchSchedule();
                        matchSchedule.setTotalLine(matchAllCount);
                        matchSchedule.setUnmatchLine(unMatchCount);
                        matchSchedule.setMatchedLine(matchCount);
                        if (matchAllCount != 0 && matchCount != null) {
                            matchSchedule.setMatchSchedule(new BigDecimal(matchCount).divide(new BigDecimal(matchAllCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).toPlainString() + "%");
                        }
//                        res.setStatics(matchSchedule);
                        String matchCode = z.getMatchCode();//该批次拉取
                        List<WmsTupianzdrwcj> dbList = wmsTupianzdrwcjService.selectCjResult(z.getClient(), z.getStoCode(), matchCode);
                        if (CollectionUtil.isNotEmpty(dbList)) {
                            List<WmsTupianzdrwcj> collect1 = dbList.stream().filter(x -> x.getWmZt().equals("2") || x.getWmZt().equals("3")).collect(Collectors.toList());
                            if (!ObjectUtil.isEmpty(matchSchedule)) {
                                matchSchedule.setMatchedLine(matchSchedule.getMatchedLine() + collect1.size());
                            }
                        }
                        if (!ifTesultExsit(z)) {
                            ProductMatchZResult productMatchZResult = new ProductMatchZResult();
                            productMatchZResult.setClient(z.getClient());
                            productMatchZResult.setMatchCode(z.getMatchCode());
                            productMatchZResult.setStoCode(z.getStoCode());
                            productMatchZResult.setCheckTouchTel(z.getMatchDataCheckTouchTel());
                            productMatchZResult.setTotalLine(matchSchedule.getTotalLine());
                            productMatchZResult.setMatchLine(matchSchedule.getMatchedLine());
                            productMatchZResult.setOldMatchSchedule(matchSchedule.getMatchSchedule());
                            productMatchZResult.setCreDate(CommonUtil.getyyyyMMdd());
                            productMatchZResult.setCheckFlag("0");
                            productMatchZResult.setMatchType("0");
                            productMatchZResultMapper.insert(productMatchZResult);
                        }
                    }
                }

                if (CollectionUtil.isNotEmpty(stoTypeList)) {
                    for (ProductMatchZ z : stoTypeList) {
//                        MatchSchedule matchSchedule = new MatchSchedule();
                        //查询校验前的完整度
//                        MatchProductInData queryData = new MatchProductInData();
//                        queryData.setClientId(z.getClient());
//                        queryData.setStoCodes(new String[]{z.getStoCode()});
//                        JsonResult result = reportFeign.getProductMatch(queryData);//获取推送的汇总数据
//                        if (result != null && result.getCode() == 0) {
//                            LinkedHashMap data = ((LinkedHashMap) result.getData());
//                            matchSchedule = JSON.parseObject(JSON.toJSONString(data), MatchSchedule.class);
//                        }
                        Integer matchAllCount = productMatchMapper.selectAllMatchCount(z.getClient(), "", z.getMatchCode());
                        Integer unMatchCount = productMatchMapper.selectUnMatchCount(z.getClient(), "", z.getMatchCode());
                        Integer matchCount = productMatchMapper.selectMatchCount(z.getClient(), "", z.getMatchCode());
                        MatchSchedule matchSchedule = new MatchSchedule();
                        matchSchedule.setTotalLine(matchAllCount);
                        matchSchedule.setUnmatchLine(unMatchCount);
                        matchSchedule.setMatchedLine(matchCount);
                        if (matchAllCount != 0 && matchCount != null) {
                            matchSchedule.setMatchSchedule(new BigDecimal(matchCount).divide(new BigDecimal(matchAllCount), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).toPlainString() + "%");
                        }
                        String matchCode = z.getMatchCode();//该批次拉取
                        List<WmsTupianzdrwcj> dbList = wmsTupianzdrwcjService.selectCjResult(z.getClient(), z.getStoCode(), matchCode);
                        if (CollectionUtil.isNotEmpty(dbList)) {
                            List<WmsTupianzdrwcj> collect1 = dbList.stream().filter(x -> x.getWmZt().equals("2") || x.getWmZt().equals("3")).collect(Collectors.toList());
                            if (!ObjectUtil.isEmpty(matchSchedule)) {
                                matchSchedule.setMatchedLine(matchSchedule.getMatchedLine() + collect1.size());
                            }
                        }
                        if (!ifTesultExsit(z)) {
                            ProductMatchZResult productMatchZResult = new ProductMatchZResult();
                            productMatchZResult.setClient(z.getClient());
                            productMatchZResult.setMatchCode(z.getMatchCode());
                            productMatchZResult.setStoCode(z.getStoCode());
                            productMatchZResult.setCheckTouchTel(z.getMatchDataCheckTouchTel());
                            productMatchZResult.setTotalLine(matchSchedule.getTotalLine());
                            productMatchZResult.setMatchLine(matchSchedule.getMatchedLine());
                            productMatchZResult.setOldMatchSchedule(matchSchedule.getMatchSchedule());
                            productMatchZResult.setCreDate(CommonUtil.getyyyyMMdd());
                            productMatchZResult.setCheckFlag("0");
                            productMatchZResult.setMatchType("1");
                            productMatchZResultMapper.insert(productMatchZResult);
                        }
                    }
                }
            }

        }
    }
}
