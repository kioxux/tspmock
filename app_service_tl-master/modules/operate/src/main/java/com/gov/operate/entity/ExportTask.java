package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 导出报表
 * </p>
 *
 * @author sy
 * @since 2022-01-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_EXPORT_TASK")
@ApiModel(value="ExportTask对象", description="导出报表")
public class ExportTask extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "导出用户")
    @TableField("GET_USER")
    private String getUser;

    @ApiModelProperty(value = "状态")
    @TableField("GET_STATUS")
    private Integer getStatus;

    @ApiModelProperty(value = "文件类型")
    @TableField("GET_TYPE")
    private Integer getType;

    @ApiModelProperty(value = "导出时间")
    @TableField("GET_DATE")
    private LocalDateTime getDate;

    @ApiModelProperty(value = "文件路径")
    @TableField("GET_PATH")
    private String getPath;

    @ApiModelProperty(value = "失败原因")
    @TableField("GET_REASON")
    private String getReason;


}
