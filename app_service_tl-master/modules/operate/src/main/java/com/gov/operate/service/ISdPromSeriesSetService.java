package com.gov.operate.service;

import com.gov.operate.entity.SdPromSeriesSet;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
public interface ISdPromSeriesSetService extends SuperService<SdPromSeriesSet> {

}
