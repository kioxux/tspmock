package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_MESSAGE")
@ApiModel(value="Message对象", description="")
public class Message extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("GM_ID")
    private String gmId;

    @ApiModelProperty(value = "接收人加盟商ID")
    @TableField("GM_CLIENT")
    private String gmClient;

    @ApiModelProperty(value = "接收人员工编号")
    @TableField("GM_USER_ID")
    private String gmUserId;

    @ApiModelProperty(value = "发送时间")
    @TableField("GM_SEND_TIME")
    private String gmSendTime;

    @ApiModelProperty(value = "标题")
    @TableField("GM_TITLE")
    private String gmTitle;

    @ApiModelProperty(value = "内容")
    @TableField("GM_CONTENT")
    private String gmContent;

    @ApiModelProperty(value = "是否已读")
    @TableField("GM_IS_READ")
    private Integer gmIsRead;

    @ApiModelProperty(value = "阅读时间")
    @TableField("GM_READ_TIME")
    private String gmReadTime;

    @ApiModelProperty(value = "业务类型")
    @TableField("GM_BUSINESS_TYPE")
    private Integer gmBusinessType;

    @ApiModelProperty(value = "是否跳转")
    @TableField("GM_GO_PAGE")
    private Integer gmGoPage;

    @ApiModelProperty(value = "消息类型")
    @TableField("GM_TYPE")
    private Integer gmType;

    @ApiModelProperty(value = "表示形式")
    @TableField("GM_SHOW_TYPE")
    private Integer gmShowType;

    @ApiModelProperty(value = "参数")
    @TableField("GM_PARAMS")
    private String gmParams;


}
