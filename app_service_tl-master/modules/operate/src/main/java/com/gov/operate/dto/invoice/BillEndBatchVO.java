package com.gov.operate.dto.invoice;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zhoushuai
 * @date 2021-05-17 17:41
 */
@Data
public class BillEndBatchVO {
    @NotBlank(message = "单据号不能为空")
    private String gbieNum;

    @NotBlank(message = "单据类型不能为空")
    private String gbieBillType;
}
