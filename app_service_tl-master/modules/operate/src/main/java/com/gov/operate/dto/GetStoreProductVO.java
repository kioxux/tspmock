package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetStoreProductVO {

    private List<String> stoCodeList;

    private String proKey;

    private Integer pageNum;

    private Integer pageSize;
}
