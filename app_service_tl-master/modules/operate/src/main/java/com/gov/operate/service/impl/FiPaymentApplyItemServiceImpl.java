package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.PaymentApplyItem;
import com.gov.operate.mapper.FiPaymentApplyItemMapper;
import com.gov.operate.service.IFiPaymentApplyItemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zp
 * @since 2021-04-01
 */
@Service
public class FiPaymentApplyItemServiceImpl extends ServiceImpl<FiPaymentApplyItemMapper, PaymentApplyItem> implements IFiPaymentApplyItemService {
}
