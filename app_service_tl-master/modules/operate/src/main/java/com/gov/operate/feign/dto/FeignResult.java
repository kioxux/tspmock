package com.gov.operate.feign.dto;

import lombok.Data;

@Data
public class FeignResult {

    private Integer code;

    private String message;

    private boolean success;

    private Object data;

    private String msg;
}
