package com.gov.operate.controller.productRelate;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IProductRelateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
@RestController
@RequestMapping("/productRelate/productRelate")
public class ProductRelateController {

    @Resource
    private IProductRelateService productRelateService;

    @Log("机构下拉框")
    @ApiOperation(value = "机构下拉框")
    @PostMapping("getSiteList")
    public Result getSiteList() {
        return productRelateService.getSiteList();
    }

    @Log("供应商下拉框")
    @ApiOperation(value = "供应商下拉框")
    @PostMapping("getSupList")
    public Result getSupList(@RequestJson("site") String site) {
        return productRelateService.getSupList(site);
    }

    @Log("商品下拉框")
    @ApiOperation(value = "商品下拉框")
    @PostMapping("getProList")
    public Result getProList(@RequestJson("site") String site, @RequestJson(value = "key", required = false) String key, @RequestJson(value = "proSelfCode", required = false) String proSelfCode) {
        return productRelateService.getProList(site, key, proSelfCode);
    }

    @Log("煎药列表查询")
    @ApiOperation(value = "煎药列表查询")
    @PostMapping("getProductRelateList")
    public Result getProductRelateList(@RequestJson(value = "site", name = "机构") String site, @RequestJson(value = "supCode", name = "供应商") String supCode,
                                       @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                       @RequestJson(value = "pageNum", name = "分页") Integer pageNum, @RequestJson(value = "pageSize", name = "分页") Integer pageSize) {
        return productRelateService.getProductRelateList(site, supCode, proSelfCode, pageNum, pageSize);
    }

    @Log("新增煎药")
    @ApiOperation(value = "新增煎药")
    @PostMapping("addProductRelate")
    public Result addProductRelate(@RequestJson(value = "site", name = "机构") String site,
                                   @RequestJson(value = "supCode", name = "供应商") String supCode,
                                   @RequestJson(value = "proSelfCode", name = "商品") String proSelfCode,
                                   @RequestJson(value = "proRelateCode", name = "关联商品") String proRelateCode,
                                   @RequestJson(value = "proPrice", name = "采购价格") BigDecimal proPrice) {
        return productRelateService.addProductRelate(site, supCode, proSelfCode, proRelateCode, proPrice);
    }

    @Log("删除煎药")
    @ApiOperation(value = "删除煎药")
    @PostMapping("delProductRelate")
    public Result delProductRelate(@RequestJson(value = "site", name = "机构") String site,
                                   @RequestJson(value = "supCode", name = "供应商") String supCode,
                                   @RequestJson(value = "proSelfCode", name = "商品") List<String> proSelfCodeList) {
        return productRelateService.delProductRelate(site, supCode, proSelfCodeList);
    }

    @Log("煎药列表查询导出")
    @ApiOperation(value = "煎药列表查询导出")
    @PostMapping("getProductRelateListExport")
    public Result getProductRelateListExport(@RequestJson(value = "site", name = "机构") String site,
                                             @RequestJson(value = "supCode", name = "供应商") String supCode,
                                             @RequestJson(value = "proSelfCode", required = false) String proSelfCode) throws IOException {
        return productRelateService.getProductRelateListExport(site, supCode, proSelfCode);
    }
}

