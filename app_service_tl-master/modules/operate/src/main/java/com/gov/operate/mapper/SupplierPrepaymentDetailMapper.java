package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SupplierPrepaymentDetail;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-07
 */
public interface SupplierPrepaymentDetailMapper extends BaseMapper<SupplierPrepaymentDetail> {

}
