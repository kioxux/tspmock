package com.gov.operate.service;

import com.gov.operate.entity.SdSusjobD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
public interface ISdSusjobDService extends SuperService<SdSusjobD> {

}
