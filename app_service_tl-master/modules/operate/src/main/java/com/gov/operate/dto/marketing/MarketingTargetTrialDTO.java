package com.gov.operate.dto.marketing;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class MarketingTargetTrialDTO {

    /**
     * 同比营业额目标
     */
    private BigDecimal gsmYearTurnoverAims;
    /**
     * 同比毛利额目标
     */
    private BigDecimal gsmYearGrossprofitAims;
    /**
     * 同比毛利率
     */
    private BigDecimal gsmYearGrossmarginAims;
    /**
     * 同比促销品目标
     */
    private BigDecimal gsmYearPromotionalproAims;


    /**
     * 环比营业额目标
     */
    private BigDecimal gsmMonthTurnoverAims;
    /**
     * 环比毛利额目标
     */
    private BigDecimal gsmMonthGrossprofitAims;
    /**
     * 环比毛利率
     */
    private BigDecimal gsmMonthGrossmarginAims;
    /**
     * 环比促销品目标
     */
    private BigDecimal gsmMonthPromotionalproAims;
}
