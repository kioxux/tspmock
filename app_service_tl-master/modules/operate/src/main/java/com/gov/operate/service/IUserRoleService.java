package com.gov.operate.service;

import com.gov.common.response.Result;

public interface IUserRoleService {

    /**
     * 更新用户角色关联
     *
     * @param client    加盟商
     * @param funcCode  功能Code
     * @param userId    新公司负责人
     * @param oldUserId 旧公司负责人
     */
    Result createUserRole(String client, String funcCode, String userId, String oldUserId);

    /**
     * 新增用户角色关联
     *
     * @param client   加盟商
     * @param funcCode 功能Code
     * @param userId   新公司负责人
     * @return
     */
    Result createUserRole(String client, String funcCode, String userId);

}
