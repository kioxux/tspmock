package com.gov.operate.service;

import com.gov.operate.entity.BillInvoiceEnd;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-25
 */
public interface IBillInvoiceEndService extends SuperService<BillInvoiceEnd> {

}
