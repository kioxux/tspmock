package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.AddInvoiceVO;
import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.entity.FicoInvoiceInformationRegistration;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface IFicoInvoiceInformationRegistrationService extends SuperService<FicoInvoiceInformationRegistration> {

    /**
     * 发票登记
     */
    void addInvoice(List<AddInvoiceVO> list);

    /**
     * 可用发票
     */
    List<FicoInvoiceInformationRegistrationDTO> selectAvailableInvoice(String type, String supCode, String site);


    /**
     * 根据发票号码俩查询发票登记信息
     *
     * @param invoiceNum
     * @return
     */
    Result selectInvoice(String invoiceNum);

    /**
     * 根据发票号码来更新发票登记信息
     *
     * @param registrationDTO
     * @return
     */
    Result editInvoice(FicoInvoiceInformationRegistrationDTO registrationDTO);
}
