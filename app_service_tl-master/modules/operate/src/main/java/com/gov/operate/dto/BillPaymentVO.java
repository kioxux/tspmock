package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class BillPaymentVO {

    @NotBlank(message = "业务类型不能为空")
    private String type;
    @NotBlank(message = "单据号不能为空")
    private String matDnId;

}
