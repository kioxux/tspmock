package com.gov.operate.dto.invoice;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SalesInvoiceRegisterPageDTO extends Pageable {

    private String client;

    /**
     * 业务机构(dc/门店)
     */
    private String matSiteCode;

    /**
     * 客户编码 （门店/客户）
     */
    private String cusCode;

    /**
     * 发生开始时间
     */
    private String startDate;

    /**
     * 发生结束时间
     */
    private String endDate;

    /**
     * 开票单号
     */
    private String orderId;

    /**
     * 发票号码
     */
    private String invoiceNo;

    /**
     * 采购员
     */
    private String userNam;

    /**
     * 0:开票中，1：开票成功 2:开票失败
     */
    private Integer gsiInvoiceStatus;

    private String gsiDnId;
}
