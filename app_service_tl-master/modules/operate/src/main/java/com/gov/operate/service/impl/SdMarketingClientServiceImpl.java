package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMarketingClient;
import com.gov.operate.mapper.SdMarketingClientMapper;
import com.gov.operate.service.ISdMarketingClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Service
public class SdMarketingClientServiceImpl extends ServiceImpl<SdMarketingClientMapper, SdMarketingClient> implements ISdMarketingClientService {

}
