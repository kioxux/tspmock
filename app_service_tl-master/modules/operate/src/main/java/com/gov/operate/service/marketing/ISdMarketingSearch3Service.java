package com.gov.operate.service.marketing;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.GetMemberListDTO;
import com.gov.operate.dto.ProductGroupVo;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.SdMarketingSearch;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-23
 */
public interface ISdMarketingSearch3Service extends SuperService<SdMarketingSearch> {

    /**
     * 精准查询
     */
    IPage<GetMemberListDTO> getMemberList(GetMemberListVO3 vo);

    /**
     * 精准查询导出
     */
    Result exportMemberList(GetMemberListVO3 vo) throws IOException;

    /**
     * 常用查询列表_菜单入口
     */
    List<SdMarketingSearch> getCommonSearchListFromMenu(GetCommonSearchListFromMenu vo);

    /**
     * 常用查询列表_营销任务详情跳转
     */
    List<SdMarketingSearch> getCommonSearchListFromMarketing(GetCommonSearchListFromMarketing vo);

    /**
     * 常用查询另存
     */
    void saveCommonSearch(SaveCommonSearchVO3 searchVO);

    /**
     * 常用查询覆盖
     */
    void editCommonSearch(EditCommonSearchVO3 vo);

    /**
     * 常用查询删除
     */
    void deleteCommonSearch(String gsmsId);


    List<Map<String,String>> getPaymentList();

    IPage<DiseaseDetailVo> getDiseasePage(GetDiseasePageDto dto);

    IPage<ProductDetailVO> getProductPage(GetProductPageDto dto);

    IPage<ProComDetailVO> getProComPage(GetProComPageDto dto);

    IPage<FactoryDetailVo> getFactoryPage(GetFactoryPageDto dto);

    IPage<BrandDetailVo> getBrandPage(GetBrandPageDto dto);

    IPage<ProductClassDetailVo> getProductClassPage(GetProductClassPageDto dto);

    IPage<ProductGroupVo> getProGroupPage(GetProGroupPageDto dto);

    void dailyInsertMemberActive();
}
