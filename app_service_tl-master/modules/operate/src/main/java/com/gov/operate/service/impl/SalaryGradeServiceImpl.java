package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.operate.entity.SalaryGrade;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SalaryGradeMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISalaryGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
@Service
public class SalaryGradeServiceImpl extends ServiceImpl<SalaryGradeMapper, SalaryGrade> implements ISalaryGradeService {

    @Resource
    private CommonService commonService;
    /**
     * 工资级别列表
     */
    @Override
    public List<SalaryGrade> getSalaryGradeList() {
        TokenUser user = commonService.getLoginInfo();
        return this.list(new QueryWrapper<SalaryGrade>().eq("CLENT", user.getClient()));
    }
}
