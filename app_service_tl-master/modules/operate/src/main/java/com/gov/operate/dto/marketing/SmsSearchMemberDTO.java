package com.gov.operate.dto.marketing;

import com.gov.operate.dto.GetMemberListDTO;
import com.gov.operate.entity.SdMarketingSearch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class SmsSearchMemberDTO {

    /**
     * 查询条件
     */
    private SdMarketingSearch search;

    /**
     * 查询到的会员
     */
    private List<GetMemberListDTO> memberList;
}
