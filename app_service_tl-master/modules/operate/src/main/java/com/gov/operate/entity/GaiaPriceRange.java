package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("GAIA_PRICE_RANGE")
public class GaiaPriceRange implements Serializable {
    /**
     * 主键
     */
    @TableId("ID")
    private Long id;

    /**
     * 加盟商
     */
    @TableField("CLIENT")
    private String client;

    /**
     * 城市
     */
    @TableField("CITY")
    private String city;

    /**
     * 价格区间等级：ABCD
     */
    @TableField("RANGE_LEVEL")
    private String rangeLevel;

    /**
     * 起始价格
     */
    @TableField("START_PRICE")
    private Integer startPrice;

    /**
     * 结束价格
     */
    @TableField("END_PRICE")
    private Integer endPrice;

    /**
     * 价格间隔
     */
    @TableField("PRICE_INTERVAL")
    private Integer priceInterval;

    /**
     * 是否删除：0未删除 1删除
     */
    @TableField("DELETE_FLAG")
    private Integer deleteFlag;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 创建者
     */
    @TableField("CREATE_USER")
    private String createUser;

    /**
     * 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;

    /**
     * 更新者
     */
    @TableField("UPDATE_USER")
    private String updateUser;

    /**
     * 版本
     */
    @TableField("VERSION")
    private String version;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取城市
     *
     * @return CITY - 城市
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置城市
     *
     * @param city 城市
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 获取价格区间等级：ABCD
     *
     * @return RANGE_LEVEL - 价格区间等级：ABCD
     */
    public String getRangeLevel() {
        return rangeLevel;
    }

    /**
     * 设置价格区间等级：ABCD
     *
     * @param rangeLevel 价格区间等级：ABCD
     */
    public void setRangeLevel(String rangeLevel) {
        this.rangeLevel = rangeLevel;
    }

    /**
     * 获取起始价格
     *
     * @return START_PRICE - 起始价格
     */
    public Integer getStartPrice() {
        return startPrice;
    }

    /**
     * 设置起始价格
     *
     * @param startPrice 起始价格
     */
    public void setStartPrice(Integer startPrice) {
        this.startPrice = startPrice;
    }

    /**
     * 获取结束价格
     *
     * @return END_PRICE - 结束价格
     */
    public Integer getEndPrice() {
        return endPrice;
    }

    /**
     * 设置结束价格
     *
     * @param endPrice 结束价格
     */
    public void setEndPrice(Integer endPrice) {
        this.endPrice = endPrice;
    }

    /**
     * 获取价格间隔
     *
     * @return PRICE_INTERVAL - 价格间隔
     */
    public Integer getPriceInterval() {
        return priceInterval;
    }

    /**
     * 设置价格间隔
     *
     * @param priceInterval 价格间隔
     */
    public void setPriceInterval(Integer priceInterval) {
        this.priceInterval = priceInterval;
    }

    /**
     * 获取是否删除：0未删除 1删除
     *
     * @return DELETE_FLAG - 是否删除：0未删除 1删除
     */
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * 设置是否删除：0未删除 1删除
     *
     * @param deleteFlag 是否删除：0未删除 1删除
     */
    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_TIME - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return CREATE_USER - 创建者
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建者
     *
     * @param createUser 创建者
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取更新时间
     *
     * @return UPDATE_TIME - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新者
     *
     * @return UPDATE_USER - 更新者
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置更新者
     *
     * @param updateUser 更新者
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * 获取版本
     *
     * @return VERSION - 版本
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置版本
     *
     * @param version 版本
     */
    public void setVersion(String version) {
        this.version = version;
    }
}