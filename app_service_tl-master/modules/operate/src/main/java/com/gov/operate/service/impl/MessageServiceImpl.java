package com.gov.operate.service.impl;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.tpns.PushContent;
import com.gov.operate.tpns.TpnsUtils;
import com.tencent.xinge.XingeApp;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
@Slf4j
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {

    @Resource
    private CommonService commonService;
    @Resource
    private MessageMapper messageMapper;
    @Resource
    private MessageTemplateMapper messageTemplateMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private AppLoginLogMapper appLoginLogMapper;

    @Resource
    private SdPromHeadMapper sdPromHeadMapper;

    /**
     * 用户消息
     *
     * @param source 1 : 首页， 2 : 个人中心
     * @return
     */
    @Override
    public Result getMessageList(Integer pageNum, Integer pageSize, Integer gmBusinessType, Integer isRead, Integer source) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<Message> messageQueryWrapper = new QueryWrapper<>();
        // 接收人加盟商ID
        messageQueryWrapper.eq("GM_CLIENT", user.getClient());
        // 接收人员工编号
        messageQueryWrapper.eq("GM_USER_ID", user.getUserId());
        // 业务类型
        if (gmBusinessType != null) {
            messageQueryWrapper.eq("GM_BUSINESS_TYPE", gmBusinessType);
        }
        // 是否已读
        if (isRead != null) {
            messageQueryWrapper.eq("GM_IS_READ", isRead);
        }
        // 消息表示类型
        messageQueryWrapper.in("GM_SHOW_TYPE", CommonEnum.gmShowType.MESSAGE.getCode(), CommonEnum.gmShowType.MSG_PUSH.getCode());
        // 首页 未读靠前
//        if (source != null && source.intValue() == 1) {
//            messageQueryWrapper.orderByAsc("GM_IS_READ");
//        }
        // 排序 发送时间倒序
        messageQueryWrapper.orderByDesc("GM_SEND_TIME");
        Page<Message> page = new Page<>(pageNum, pageSize);
        IPage<Message> iPage = messageMapper.selectPage(page, messageQueryWrapper);
        return ResultUtil.success(iPage);
    }

    /**
     * 消息读取
     *
     * @param gmId
     * @return
     */
    @Override
    public Result readMessage(String gmId) {
        // 消息获取
        QueryWrapper<Message> messageQueryWrapper = new QueryWrapper<>();
        // 主键
        messageQueryWrapper.eq("GM_ID", gmId);
        Message message = messageMapper.selectOne(messageQueryWrapper);
        if (message == null) {
            return ResultUtil.success();
        }
        // 消息已读
        message.setGmIsRead(CommonEnum.gmIsRead.READ.getCode());
        // 读取时间
        message.setGmReadTime(DateUtils.getCurrentDateTimeStrFull());
        messageMapper.update(message, messageQueryWrapper);
        return ResultUtil.success(message);
    }

    /**
     * 消息推送
     *
     * @param messageParams
     * @return
     */
    @Override
    public Result sendMessageList(MessageParams messageParams) {
        try {
            // 没有推送用户
            if (CollectionUtils.isEmpty(messageParams.getUserIdList())) {
                return ResultUtil.error(ResultEnum.E0605);
            }
            QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
            userDataQueryWrapper.eq("CLIENT", messageParams.getClient());
            userDataQueryWrapper.in("USER_ID", messageParams.getUserIdList());
            List<UserData> userDataList = userDataMapper.selectList(userDataQueryWrapper);
            // 推送用户未查询到
            if (CollectionUtils.isEmpty(userDataList)) {
                return ResultUtil.error(ResultEnum.E0605);
            }
            QueryWrapper<MessageTemplate> messageTemplateQueryWrapper = new QueryWrapper<>();
            messageTemplateQueryWrapper.eq("GMT_ID", messageParams.getId());
            MessageTemplate messageTemplate = messageTemplateMapper.selectOne(messageTemplateQueryWrapper);
            // 模板不存在
            if (messageTemplate == null) {
                return ResultUtil.error(ResultEnum.E0603);
            }
            List<Message> messageList = new ArrayList<>();
            for (UserData userData : userDataList) {
                // 用户消息
                Message message = new Message();
                // ID
                message.setGmId(UUID.randomUUID().toString());
                // 接收人加盟商ID
                message.setGmClient(userData.getClient());
                // 接收人员工编号
                message.setGmUserId(userData.getUserId());
                // 发送时间
                message.setGmSendTime(DateUtils.getCurrentDateTimeStrFull());
                // 标题
                message.setGmTitle(StringUtils.parseList(messageTemplate.getGmtTitle(), messageParams.getTitleParams()));
                // 内容
                message.setGmContent(StringUtils.parseList(messageTemplate.getGmtContent(), messageParams.getContentParmas()));
                // 是否已读
                message.setGmIsRead(CommonEnum.gmIsRead.UNREAD.getCode());
                // 阅读时间 初始化为空
                // 业务类型
                message.setGmBusinessType(messageTemplate.getGmtBusinessType());
                // 是否跳转
                message.setGmGoPage(messageTemplate.getGmtGoPage());
                // 消息类型
                message.setGmType(messageTemplate.getGmtType());
                // 表示形式
                message.setGmShowType(messageTemplate.getGmtShowType());
                // 参数
                if (messageParams.getParams() != null && !messageParams.getParams().isEmpty()) {
                    message.setGmParams(JSONObject.toJSONString(messageParams.getParams()));
                }
                messageList.add(message);
            }
            // 消息
            if (CommonEnum.gmShowType.MESSAGE.getCode().equals(messageTemplate.getGmtShowType()) ||
                    CommonEnum.gmShowType.MSG_PUSH.getCode().equals(messageTemplate.getGmtShowType())) {
                // 消息插入
                this.saveBatch(messageList);
            }
            // 推送
            if (CommonEnum.gmShowType.PUSH.getCode().equals(messageTemplate.getGmtShowType()) ||
                    CommonEnum.gmShowType.MSG_PUSH.getCode().equals(messageTemplate.getGmtShowType())) {
                // 发送参数
                PushContent pushContent = new PushContent();
                // 标题
                pushContent.setTitle(messageList.get(0).getGmTitle());
                // 内容
                pushContent.setContent(messageList.get(0).getGmContent());
                // 收信人
                pushContent.setAccountList(new ArrayList<String>() {{
                    for (UserData userData : userDataList) {
                        add(messageParams.getClient() + "-" + userData.getUserTel());
                    }
                }});
                XingeApp xingeAppAndroid = TpnsUtils.getXingeApp(pushContent, this.applicationConfig, "android");
                // 自定义参数
                Map<String, Object> map = new HashMap<>();
                map.put(PushContent.TYPE, messageTemplate.getGmtType());
                map.put(PushContent.MESSAGEID, messageList.get(0).getGmId());
                if (messageParams.getParams() != null && !messageParams.getParams().isEmpty()) {
                    map.put(PushContent.PARAMS, messageParams.getParams());
                }
                pushContent.setAttachKeys(map);
                TpnsUtils.send(pushContent, xingeAppAndroid);

                XingeApp xingeAppIos = TpnsUtils.getXingeApp(pushContent, this.applicationConfig, "ios");
                TpnsUtils.send(pushContent, xingeAppIos);
            }
            return ResultUtil.success();
        } catch (Exception e) {
            e.printStackTrace();
            log.info("消息发送失败", JSONObject.toJSONString(messageParams));
            return ResultUtil.error(ResultEnum.E0606);
        }
    }

    /**
     * 消息发送
     *
     * @param messageParams
     * @return
     */
    @Override
    public Result sendMessage(MessageParams messageParams) {
        try {
            QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
            userDataQueryWrapper.eq("CLIENT", messageParams.getClient());
            userDataQueryWrapper.eq("USER_ID", messageParams.getUserId());
            UserData userData = userDataMapper.selectOne(userDataQueryWrapper);
            if (userData == null) {
                return ResultUtil.error(ResultEnum.E0605);
            }
            QueryWrapper<MessageTemplate> messageTemplateQueryWrapper = new QueryWrapper<>();
            messageTemplateQueryWrapper.eq("GMT_ID", messageParams.getId());
            MessageTemplate messageTemplate = messageTemplateMapper.selectOne(messageTemplateQueryWrapper);
            // 模板不存在
            if (messageTemplate == null) {
                return ResultUtil.error(ResultEnum.E0603);
            }
            // 消息停用
            if (CommonEnum.gmtFlag.FLAGF.getCode().equals(messageTemplate.getGmtFlag())) {
                return ResultUtil.error(ResultEnum.E0604);
            }
            // 用户消息
            Message message = new Message();
            // ID
            message.setGmId(UUID.randomUUID().toString());
            // 接收人加盟商ID
            message.setGmClient(messageParams.getClient());
            // 接收人员工编号
            message.setGmUserId(messageParams.getUserId());
            // 发送时间
            message.setGmSendTime(DateUtils.getCurrentDateTimeStrFull());
            // 标题
            message.setGmTitle(StringUtils.parseList(messageTemplate.getGmtTitle(), messageParams.getTitleParams()));
            // 内容
            message.setGmContent(StringUtils.parseList(messageTemplate.getGmtContent(), messageParams.getContentParmas()));
            // 是否已读
            message.setGmIsRead(CommonEnum.gmIsRead.UNREAD.getCode());
            // 阅读时间 初始化为空
            // 业务类型
            message.setGmBusinessType(messageTemplate.getGmtBusinessType());
            // 是否跳转
            message.setGmGoPage(messageTemplate.getGmtGoPage());
            // 消息类型
            message.setGmType(messageTemplate.getGmtType());
            // 表示形式
            message.setGmShowType(messageTemplate.getGmtShowType());
            // 参数
            if (messageParams.getParams() != null && !messageParams.getParams().isEmpty()) {
                message.setGmParams(JSONObject.toJSONString(messageParams.getParams()));
            }
            // 消息
            if (CommonEnum.gmShowType.MESSAGE.getCode().equals(messageTemplate.getGmtShowType())) {
                // 消息插入
                messageMapper.insert(message);
            }
            // 消息 + 推送
            if (CommonEnum.gmShowType.PUSH.getCode().equals(messageTemplate.getGmtShowType()) ||
                    CommonEnum.gmShowType.MSG_PUSH.getCode().equals(messageTemplate.getGmtShowType())) {
                // 消息插入
                messageMapper.insert(message);
                // 推送
                // 用户登录数据
                AppLoginLog appLoginLog = appLoginLogMapper.selectLastLogByUser(messageParams.getClient(), messageParams.getUserId());
                if (appLoginLog != null) {
                    // 发送参数
                    PushContent pushContent = new PushContent();
                    // 标题
                    pushContent.setTitle(message.getGmTitle());
                    // 内容
                    pushContent.setContent(message.getGmContent());
                    // 收信人
                    pushContent.setAccountList(new ArrayList<String>() {{
                        add(messageParams.getClient() + "-" + userData.getUserTel());
                    }});
                    XingeApp xingeApp = TpnsUtils.getXingeApp(pushContent, this.applicationConfig, appLoginLog.getPlatform());
                    // 自定义参数
                    Map<String, Object> map = new HashMap<>();
                    map.put(PushContent.TYPE, messageTemplate.getGmtType());
                    map.put(PushContent.MESSAGEID, message.getGmId());
                    if (messageParams.getParams() != null && !messageParams.getParams().isEmpty()) {
                        map.put(PushContent.PARAMS, messageParams.getParams());
                    }
                    pushContent.setAttachKeys(map);
                    TpnsUtils.send(pushContent, xingeApp);
                }
            }
            return ResultUtil.success();
        } catch (Exception e) {
            e.printStackTrace();
            log.info("消息发送失败", JSONObject.toJSONString(messageParams));
            return ResultUtil.error(ResultEnum.E0606);
        }
    }

    /**
     * 消息删除
     *
     * @param list
     * @return
     */
    @Override
    public Result delMessage(List<String> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            QueryWrapper<Message> messageQueryWrapper = new QueryWrapper<>();
            messageQueryWrapper.in("GM_ID", list);
            messageMapper.delete(messageQueryWrapper);
        }
        return ResultUtil.success();
    }

    /**
     * 消息批量已读
     *
     * @param list
     * @return
     */
    @Override
    public Result readBatchMessage(List<String> list) {
        QueryWrapper<Message> messageQueryWrapper = new QueryWrapper<>();
        if (CollectionUtils.isNotEmpty(list)) {
            messageQueryWrapper.in("GM_ID", list);
        }
        TokenUser user = commonService.getLoginInfo();
        // 当前人加盟商
        messageQueryWrapper.eq("GM_CLIENT", user.getClient());
        // 当前人
        messageQueryWrapper.eq("GM_USER_ID", user.getUserId());
        // 未读消息
        messageQueryWrapper.eq("GM_IS_READ", CommonEnum.gmIsRead.UNREAD.getCode());
        Message message = new Message();
        // 消息已读
        message.setGmIsRead(CommonEnum.gmIsRead.READ.getCode());
        // 读取时间
        message.setGmReadTime(DateUtils.getCurrentDateTimeStrFull());
        messageMapper.update(message, messageQueryWrapper);

        return ResultUtil.success();
    }

    @Override
    public Result pushBusinessMonthlyReportMsg(MessageParams messageParams) {
        String gmtId = messageParams.getId();
        // 获取生效中的加盟商
        List<String> clients = messageTemplateMapper.selectClientsWithEffect(gmtId);

        for (String client : clients) {
            // 获取当前时间需要推送消息的用户
            List<PushMsgUserWithRule> pushMsgUserWithRules = messageTemplateMapper.selectPushMsgUserWithRule(gmtId, client);
            if (CollectionUtils.isEmpty(pushMsgUserWithRules)) {
                // 获取模板默认配置需要推送的人员
                List<PushMsgUserWithRule> pushMsgUserWithDefault = messageTemplateMapper.selectPushMsgUserWithDefault(gmtId, client);
                pushMsgUserWithRules.addAll(pushMsgUserWithDefault);
            }

            // 获取所有需要推送的用户
            if (CollectionUtils.isNotEmpty(pushMsgUserWithRules)) {
                List<AppLoginLog> loginUserWithClient = pushMsgUserWithRules.stream()
                        .map(item -> new AppLoginLog().setClient(client).setUserId(item.getReceiveUserId()))
                        .collect(Collectors.toList());
                List<AppLoginLog> appLoginLogs = appLoginLogMapper.selectLastLoginLogByUserIds(loginUserWithClient);
                for (PushMsgUserWithRule pushMsgUserWithRule : pushMsgUserWithRules) {
                    String userId = pushMsgUserWithRule.getReceiveUserId();
                    pushMsg(appLoginLogs, pushMsgUserWithRule, messageParams, client, userId, messageParams.getContentParmas(), null);
                }
            }
        }
        return ResultUtil.success();
    }

    @Override
    public Result pushPromotionReportMsg(MessageParams messageParams) {
        String gmtId = messageParams.getId();

        // 获取生效中的加盟商
        List<String> clients = messageTemplateMapper.selectClientsWithEffect(gmtId);
        for (String client : clients) {
            // 获取当前时间需要推送消息的用户
            List<PushMsgUserWithRule> pushMsgUserWithRules = messageTemplateMapper.selectPushMsgUserWithRule(gmtId, client);
            if (CollectionUtils.isEmpty(pushMsgUserWithRules)) {
                // 获取模板默认配置需要推送的人员
                List<PushMsgUserWithRule> pushMsgUserWithDefault = messageTemplateMapper.selectPushMsgUserWithDefault(gmtId, client);
                pushMsgUserWithRules.addAll(pushMsgUserWithDefault);
            }
            // 获取所有需要推送的用户
            if (CollectionUtils.isNotEmpty(pushMsgUserWithRules)) {
                List<AppLoginLog> loginUserWithClient = pushMsgUserWithRules.stream()
                        .map(item -> new AppLoginLog().setClient(client).setUserId(item.getReceiveUserId()))
                        .collect(Collectors.toList());
                    // 根据加盟商获取促销报告
                    List<SdPromHead> sdPromHeads = sdPromHeadMapper.selectPromotions(Collections.singletonList(client));
                    if (CollectionUtils.isNotEmpty(sdPromHeads)) {
                        // 获取用户登录日志，推送消息给用户最近的登录设备
                        List<AppLoginLog> appLoginLogs = appLoginLogMapper.selectLastLoginLogByUserIds(loginUserWithClient);
                        for (PushMsgUserWithRule pushMsgUserWithRule : pushMsgUserWithRules) {
                            String userId = pushMsgUserWithRule.getReceiveUserId();
                            pushMsgUserWithRule.setGmtType(7);
                            // 获取每个加盟商下的促销报告
                            List<SdPromHead> collect = sdPromHeads.stream()
                                    .filter(item -> item.getClient().equals(client)).collect(Collectors.toList());
                            for (SdPromHead sdPromHead : collect) {
                                pushMsg(appLoginLogs, pushMsgUserWithRule, messageParams, client, userId, Collections.singletonList(sdPromHead.getGsphName()), sdPromHead.getGsphMarketid());
                            }
                        }
                    }
            }
        }
        return ResultUtil.success();
    }

    /**
     * 具体推送操作
     *
     * @param appLoginLogs        app登录日志
     * @param pushMsgUserWithRule 需要推送的用户
     * @param messageParams       参数
     * @param client              加盟商
     * @param userId              用户
     * @param content             推送内容
     * @param gsphMarketId        gsphMarketId
     */
    private void pushMsg(List<AppLoginLog> appLoginLogs, PushMsgUserWithRule pushMsgUserWithRule,
                         MessageParams messageParams, String client, String userId, List<String> content, Integer gsphMarketId) {
        // 用户消息
        Message message = new Message();
        // ID
        message.setGmId(UUID.randomUUID().toString());
        // 接收人加盟商ID
        message.setGmClient(client);
        // 接收人员工编号
        message.setGmUserId(userId);
        // 发送时间
        message.setGmSendTime(DateUtils.getCurrentDateTimeStrFull());
        // 标题
        message.setGmTitle(StringUtils.parseList(pushMsgUserWithRule.getGmtTitle(), messageParams.getTitleParams()));
        // 内容
        message.setGmContent(StringUtils.parseList(pushMsgUserWithRule.getGmtContent(), content));
        // 是否已读
        message.setGmIsRead(CommonEnum.gmIsRead.UNREAD.getCode());
        // 业务类型
        message.setGmBusinessType(pushMsgUserWithRule.getGmtBusinessType());
        // 是否跳转
        message.setGmGoPage(pushMsgUserWithRule.getGmtGoPage());
        // 消息类型
        message.setGmType(pushMsgUserWithRule.getGmtType());
        // 表示形式
        Integer gmtShowType = pushMsgUserWithRule.getGmtShowType();
        message.setGmShowType(gmtShowType);
        // 参数
        Map<String, String> params = messageParams.getParams();
        if (MapUtil.isNotEmpty(params)) {
            // 经营月报
            message.setGmParams(JSONObject.toJSONString(params));
        } else {
            if (StringUtils.isNotEmpty(gsphMarketId)) {
                // 促销报告
                params = new HashMap<>();
                params.put("gsphMarketid", String.valueOf(gsphMarketId));
                message.setGmParams(JSONObject.toJSONString(params));
            }
        }
        try {

            // 消息
            if (CommonEnum.gmShowType.MESSAGE.getCode().equals(gmtShowType)) {
                // 消息插入
                messageMapper.insert(message);
            } else if (CommonEnum.gmShowType.PUSH.getCode().equals(gmtShowType) ||
                    CommonEnum.gmShowType.MSG_PUSH.getCode().equals(gmtShowType)) {
                // 消息插入
                messageMapper.insert(message);

                // 推送
                // 用户登录数据
                Optional<AppLoginLog> first = appLoginLogs.stream()
                        .filter(item -> item.getClient().equals(client) && item.getUserId().equals(userId))
                        .findFirst();
                if (first.isPresent()) {
                    AppLoginLog appLoginLog = first.get();
                    // 发送参数
                    PushContent pushContent = new PushContent();
                    // 标题
                    pushContent.setTitle(message.getGmTitle());
                    // 内容
                    pushContent.setContent(message.getGmContent());
                    // 收信人
                    pushContent.setAccountList(new ArrayList<String>() {{
                        add(client + "-" + pushMsgUserWithRule.getReceiveUserTel());
                    }});
                    XingeApp xingeApp = TpnsUtils.getXingeApp(pushContent, this.applicationConfig, appLoginLog.getPlatform());
                    // 自定义参数
                    Map<String, Object> map = new HashMap<>();
                    map.put(PushContent.TYPE, pushMsgUserWithRule.getGmtType());
                    map.put(PushContent.MESSAGEID, message.getGmId());
                    if (MapUtil.isNotEmpty(params)) {
                        map.put(PushContent.PARAMS, params);
                    }
                    pushContent.setAttachKeys(map);
                    TpnsUtils.send(pushContent, xingeApp);
                    log.error("推送成功 client:{}, userId:{}, message: {}", client, userId, JSON.toJSON(pushContent));
                }
            }
        } catch (Exception e) {
            log.error("推送失败  client:{}, userId:{}, message: {}, error: {}", client, userId, JSON.toJSON(message), e.getMessage());
        }
    }

}
