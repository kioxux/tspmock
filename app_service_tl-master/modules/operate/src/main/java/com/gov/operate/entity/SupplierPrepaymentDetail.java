package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-09-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SUPPLIER_PREPAYMENT_DETAIL")
@ApiModel(value = "SupplierPrepaymentDetail对象", description = "")
public class SupplierPrepaymentDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "采购主体")
    @TableField("PAY_COMPANY_CODE")
    private String payCompanyCode;

    @ApiModelProperty(value = "付款申请单号")
    @TableField("PAY_ORDER_ID")
    private String payOrderId;

    @ApiModelProperty(value = "付款申请详情号")
    @TableField("PAY_ORDER_DETAIL_ID")
    private Long payOrderDetailId;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "采购凭证号")
    @TableField("PO_ID")
    private String poId;

    @ApiModelProperty(value = "供应商编码")
    @TableField("PO_SUPPLIER_ID")
    private String poSupplierId;

    @ApiModelProperty(value = "订单金额")
    @TableField("PO_AMT")
    private BigDecimal poAmt;

    @ApiModelProperty(value = "过账日期")
    @TableField("MAT_POST_DATE")
    private String matPostDate;

    @ApiModelProperty(value = "单据去税金额")
    @TableField("EXCLUDING_TAX_AMOUNT")
    private BigDecimal excludingTaxAmount;

    @ApiModelProperty(value = "单据号")
    @TableField("MAT_DN_ID")
    private String matDnId;

    @ApiModelProperty(value = "单据日期")
    @TableField("XGRQ")
    private String xgrq;

    @ApiModelProperty(value = "单据税额")
    @TableField("TAX_AMOUNT")
    private BigDecimal taxAmount;

    @ApiModelProperty(value = "单据总金额")
    @TableField("TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "业务类型")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "业务类型名")
    @TableField("TYPE_NAME")
    private String typeName;

    @ApiModelProperty(value = "地点")
    @TableField("SITE")
    private String site;

    @ApiModelProperty(value = "门店名")
    @TableField("STO_NAME")
    private String stoName;

    @ApiModelProperty(value = "供应商名")
    @TableField("SUP_SELF_NAME")
    private String supSelfName;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_SELF_CODE")
    private String supSelfCode;

    @ApiModelProperty(value = "核销日期")
    @TableField("GSPD_CREATE_DATE")
    private String gspdCreateDate;
}
