package com.gov.operate.controller.category;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.category.CategoryInData;
import com.gov.operate.service.category.ICategoryAnalyseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/category")
public class CategoryAnalyseController {
    @Resource
    private ICategoryAnalyseService categoryAnalyseService;

    @ApiOperation(value = "品类建议列表查询")
    @PostMapping("getCategoryList")
    public Result getCategoryList(@Valid @RequestBody CategoryInData inData) {
        return ResultUtil.success(categoryAnalyseService.selectAplProductList(inData));
    }

    @ApiOperation(value = "品类建议明细商品查询")
    @PostMapping("getCategoryProductList")
    public Result getCategoryProductList(@Valid @RequestBody CategoryInData inData) {
        return ResultUtil.success(categoryAnalyseService.selectAplProduct(inData));
    }

    @ApiOperation(value = "商品调价")
    @PostMapping("updateAdjustPrice")
    public Result updateAdjustPrice(@Valid @RequestBody List<CategoryInData> inData) {
        categoryAnalyseService.updateAdjustPrice(inData);
        return ResultUtil.success("调价成功");
    }

    @ApiOperation(value = "商品淘汰")
    @PostMapping("outProduct")
    public Result outProduct(@Valid @RequestBody List<CategoryInData> inData) {
        categoryAnalyseService.outProduct(inData);
        return ResultUtil.success("淘汰成功");
    }

    @ApiOperation(value = "引进商品改变状态")
    @PostMapping("updateProduct")
    public Result updateProduct(@Valid @RequestBody List<CategoryInData> inData) {
        categoryAnalyseService.updateProduct(inData);
        return ResultUtil.success("操作成功");
    }

    @ApiOperation(value = "日期展示列表")
    @PostMapping("selectDateList")
    public Result selectDateList(@Valid @RequestBody Map<String,String> inData) {
        return ResultUtil.success(categoryAnalyseService.selectDateList(inData));
    }

    @ApiOperation(value = "综合展示表格")
    @PostMapping("selectTableData")
    public Result selectTableData(@Valid @RequestBody CategoryInData inData) {
        return ResultUtil.success(categoryAnalyseService.selectTableData(inData));
    }


}
