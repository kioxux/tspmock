package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.RechargeCardLVO;
import com.gov.operate.dto.RechargeCardListVO;
import com.gov.operate.entity.SdRechargeCardChange;
import com.gov.operate.entity.StoreData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
public interface SdRechargeCardChangeMapper extends BaseMapper<SdRechargeCardChange> {
    /**
     * 获取会员卡号
     */
    SdRechargeCardChange getUserCardNumber(Map<Object, Object> map);

    IPage<RechargeCardLVO> SelectCardList(Page<RechargeCardLVO> page, @Param("ew") QueryWrapper<RechargeCardListVO> wrapper);

    /**
     * 当前登录人有权限的门店列表_储值卡充值查询
     *
     * @param client 加盟商
     * @param userId 当前用户
     * @return 门店列表
     */
    List<StoreData> getCurrentUserAuthStoList(@Param("client") String client, @Param("userId") String userId);
}
