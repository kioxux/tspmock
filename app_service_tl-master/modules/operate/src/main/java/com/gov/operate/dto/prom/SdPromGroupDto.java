package com.gov.operate.dto.prom;

import com.gov.operate.entity.SdPromGroup;
import com.gov.operate.entity.SdPromGroupPro;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.02
 */
@Data
public class SdPromGroupDto extends SdPromGroup {

    /**
     * 创建人名
     */
    private String createUserName;

    /**
     * 商品集合
     */
    private List<SdPromGroupPro> sdPromGroupProList;

}
