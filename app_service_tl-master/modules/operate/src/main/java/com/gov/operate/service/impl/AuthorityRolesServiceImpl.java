package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.RoleVO;
import com.gov.operate.dto.UpdateRoleVO;
import com.gov.operate.entity.AuthorityRoles;
import com.gov.operate.entity.AuthorityUserRoles;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.AuthorityRolesMapper;
import com.gov.operate.mapper.AuthorityUserRolesMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IAuthorityRolesService;
import com.gov.operate.service.IAuthorityUserRolesService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Service
public class AuthorityRolesServiceImpl extends ServiceImpl<AuthorityRolesMapper, AuthorityRoles> implements IAuthorityRolesService {
    @Resource
    private CommonService commonService;
    @Resource
    private AuthorityRolesMapper rolesMapper;
    @Resource
    private AuthorityUserRolesMapper userRolesMapper;

    @Resource
    private IAuthorityUserRolesService userRolesService;

    /**
     * 创建角色
     */
    @Override
    public Result createRole(RoleVO vo) {
        TokenUser user = commonService.getLoginInfo();

        //判断roleName是否已经存在
        List<AuthorityRoles> rolesList = rolesMapper.selectList(new QueryWrapper<AuthorityRoles>()
                .eq("CLIENT", user.getClient())
                .eq("GAR_SFSQ", "1")
                .eq("GAR_ROLE_NAME", vo.getGarRoleName()));
        if (!rolesList.isEmpty()) {
            throw new CustomResultException("角色名称已存在");
        }
        AuthorityRoles roles = new AuthorityRoles();
        BeanUtils.copyProperties(vo, roles);
        roles.setClient(user.getClient());
        roles.setGarCjrq(DateUtils.getCurrentDateStrYYMMDD());
        roles.setGarCjsj(DateUtils.getCurrentTimeStrHHMMSS());
        roles.setGarCjr(user.getUserId());
        roles.setGarCjrxm(user.getLoginName());
        roles.setGarSfsq("1");
        rolesMapper.insert(roles);
        return ResultUtil.success();
    }


    /**
     * 删除角色
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result deleteRole(Integer roleId) {
        if (null == roleId) {
            throw new CustomResultException("角色id不可以为空");
        }
        TokenUser user = commonService.getLoginInfo();
        AuthorityRoles roles = rolesMapper.selectById(roleId);
        if (null == roles) {
            throw new CustomResultException("角色不存在，请刷新页面");
        }

        if ("0".equals(roles.getGarEnable())) {
            throw new CustomResultException("角色启用中不可删除");
        }

        List<AuthorityUserRoles> userRolesList = userRolesMapper.selectList(new QueryWrapper<AuthorityUserRoles>()
                .eq("CLIENT", user.getClient())
                .eq("GAUR_SFSQ", "1")
                .eq("GAUR_ROLE_ID", roleId));

        if (!userRolesList.isEmpty()) {
            //删除相关这角色相关的用户
            userRolesList.forEach(item -> {
                item.setGaurSfsq("0");
                item.setGaurXgr(user.getUserId());
                item.setGaurXgsj(DateUtils.getCurrentTimeStrHHMMSS());
                item.setGaurXgrxm(user.getLoginName());
                item.setGaurXgrq(DateUtils.getCurrentDateStrYYMMDD());
            });
            userRolesService.updateBatchById(userRolesList);
        }

        roles.setGarSfsq("0");
        roles.setGarXgr(user.getUserId());
        roles.setGarXgsj(DateUtils.getCurrentTimeStrHHMMSS());
        roles.setGarXgrxm(user.getLoginName());
        roles.setGarXgrq(DateUtils.getCurrentDateStrYYMMDD());
        rolesMapper.updateById(roles);
        return ResultUtil.success();
    }


    /**
     * 编辑角色
     */
    @Override
    public Result updateRole(UpdateRoleVO vo) {
        TokenUser user = commonService.getLoginInfo();
        AuthorityRoles roles = rolesMapper.selectById(vo.getId());
        if (null == roles) {
            throw new CustomResultException("角色不存在，请刷新页面");
        }

        AuthorityRoles role = new AuthorityRoles();
        if (StringUtils.isNotBlank(vo.getGarRoleName())) {
            //判断roleName是否已经存在
            List<AuthorityRoles> rolesList = rolesMapper.selectList(new QueryWrapper<AuthorityRoles>()
                    .eq("CLIENT", user.getClient())
                    .ne("ID", vo.getId())
                    .eq("GAR_ROLE_NAME", vo.getGarRoleName()));
            if (!rolesList.isEmpty()) {
                throw new CustomResultException("角色名称已存在");
            }
            role.setGarRoleName(vo.getGarRoleName());
        }
        if (StringUtils.isNotBlank(vo.getGarBz())) {
            role.setGarBz(vo.getGarBz());
        }
        role.setId(vo.getId());
        role.setGarEnable(vo.getGarEnable());
        role.setGarXgr(user.getUserId());
        role.setGarXgsj(DateUtils.getCurrentTimeStrHHMMSS());
        role.setGarXgrxm(user.getLoginName());
        role.setGarXgrq(DateUtils.getCurrentDateStrYYMMDD());
        //更新指定字段
        rolesMapper.updateById(role);
        return ResultUtil.success();
    }

    /**
     * 角色查询
     */
    @Override
    public Result roleList() {
        TokenUser user = commonService.getLoginInfo();
        List<AuthorityRoles> rolesList = rolesMapper.selectList(new QueryWrapper<AuthorityRoles>()
                .eq("CLIENT", user.getClient())
                .eq("GAR_SFSQ", "1"));
        return ResultUtil.success(rolesList);
    }


}
