package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromUnitarySet;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
public interface SdPromUnitarySetMapper extends BaseMapper<SdPromUnitarySet> {

    int deleteList(@Param("list") List<SdPromUnitarySet> sdPromUnitarySetDeleteList);


    IPage<ProductResponseVo> selectProductList(Page<SdPromUnitarySet> page, @Param("product") ProductRequestVo productRequestVo);
}
