package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.entity.ProductRelate;
import com.gov.mybatis.SuperService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
public interface IProductRelateService extends SuperService<ProductRelate> {

    /**
     * 机构下拉框
     *
     * @return
     */
    Result getSiteList();

    /**
     * 供应商下拉框
     *
     * @param site
     * @return
     */
    Result getSupList(String site);

    /**
     * 商品下拉框
     *
     * @param key
     * @param proSelfCode
     * @return
     */
    Result getProList(String site, String key, String proSelfCode);

    /**
     * 煎药列表
     *
     * @param site
     * @param supCode
     * @param proSelfCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getProductRelateList(String site, String supCode, String proSelfCode, Integer pageNum, Integer pageSize);

    /**
     * 新增煎药
     *
     * @param site
     * @param supCode
     * @param proSelfCode
     * @param proRelateCode
     * @param proPrice
     * @return
     */
    Result addProductRelate(String site, String supCode, String proSelfCode, String proRelateCode, BigDecimal proPrice);

    /**
     * 删除煎药
     *
     * @param site
     * @param supCode
     * @param proSelfCodeList
     * @return
     */
    Result delProductRelate(String site, String supCode, List<String> proSelfCodeList);

    /**
     * 煎药查询导出
     *
     * @param site
     * @param supCode
     * @param proSelfCode
     * @return
     */
    Result getProductRelateListExport(String site, String supCode, String proSelfCode) throws IOException;
}
