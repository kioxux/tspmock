package com.gov.operate.service.impl;

import com.gov.operate.entity.SdSalePayMsg;
import com.gov.operate.mapper.SdSalePayMsgMapper;
import com.gov.operate.service.ISdSalePayMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Service
public class SdSalePayMsgServiceImpl extends ServiceImpl<SdSalePayMsgMapper, SdSalePayMsg> implements ISdSalePayMsgService {

}
