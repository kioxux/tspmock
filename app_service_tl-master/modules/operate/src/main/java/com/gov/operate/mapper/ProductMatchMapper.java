package com.gov.operate.mapper;

import com.gov.operate.entity.ProductMatch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 导入商品匹配信息表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
public interface ProductMatchMapper extends BaseMapper<ProductMatch> {

    List<ProductMatch> selectIleaglCaseData(@Param("client") String client, @Param("stoCode") String stoCode, @Param("createDate") String createDate,@Param("matchCode") String matchCode);

    List<String> getCountScOrZlPerson(@Param("client") String client, @Param("stoCode") String stoCode, @Param("tel") String tel);

    List<String> getStoUserSites(@Param("client") String client, @Param("stoCode") String s, @Param("tel") String telNum);

    Integer selectAllMatchCount(@Param("client") String client, @Param("stoCode") String stoCode, @Param("matchCode") String matchCode);

    Integer selectUnMatchCount(@Param("client") String client, @Param("stoCode") String stoCode, @Param("matchCode") String matchCode);

    Integer selectMatchCount(@Param("client")String client, @Param("stoCode")String stoCode, @Param("matchCode")String matchCode);
}
