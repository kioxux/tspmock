package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/20 11:18
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AddInDrawerDetailVO {

    @NotBlank(message = "批号不能为空")
    private String gsidBatchNo;

    @NotBlank(message = "有效期不能为空")
    private String gsidValidDate;

    @NotBlank(message = "商品编号不能为空")
    private String gsidProId;

    @NotBlank(message = "装斗数量不能为空")
    private String gsidQty;
}
