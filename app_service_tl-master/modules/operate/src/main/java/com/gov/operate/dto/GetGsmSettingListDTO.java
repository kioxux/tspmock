package com.gov.operate.dto;

import com.gov.operate.entity.SdMarketing;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetGsmSettingListDTO extends SdMarketing {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 促销方式个数
     */
    private Integer promCount;
}
