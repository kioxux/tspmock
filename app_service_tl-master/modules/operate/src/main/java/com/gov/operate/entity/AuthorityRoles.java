package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHORITY_ROLES")
@ApiModel(value="AuthorityRoles对象", description="")
public class AuthorityRoles extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "角色名称")
    @TableField("GAR_ROLE_NAME")
    private String garRoleName;

    @ApiModelProperty(value = "备注")
    @TableField("GAR_BZ")
    private String garBz;

    @ApiModelProperty(value = "是否启用(0是：1否)")
    @TableField("GAR_ENABLE")
    private String garEnable;

    @ApiModelProperty(value = "创建日期")
    @TableField("GAR_CJRQ")
    private String garCjrq;

    @ApiModelProperty(value = "创建时间")
    @TableField("GAR_CJSJ")
    private String garCjsj;

    @ApiModelProperty(value = "创建人")
    @TableField("GAR_CJR")
    private String garCjr;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("GAR_CJRXM")
    private String garCjrxm;

    @ApiModelProperty(value = "是否删除0是/1否（默认否）")
    @TableField("GAR_SFSQ")
    private String garSfsq;

    @ApiModelProperty(value = "修改日期")
    @TableField("GAR_XGRQ")
    private String garXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("GAR_XGSJ")
    private String garXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("GAR_XGR")
    private String garXgr;

    @ApiModelProperty(value = "修改人姓名")
    @TableField("GAR_XGRXM")
    private String garXgrxm;

    @ApiModelProperty(value = "角色功能：0-普通角色，1-公司负责人，2-门店店长，3-商采负责人，4-财务负责人，5-人资负责人，6-物流负责人")
    @TableField("GAR_FUNC")
    private String garFunc;


}
