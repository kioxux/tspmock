package com.gov.operate.service.impl;

import com.gov.operate.entity.InvoiceBillInvoice;
import com.gov.operate.mapper.InvoiceBillInvoiceMapper;
import com.gov.operate.service.IInvoiceBillInvoiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Service
public class InvoiceBillInvoiceServiceImpl extends ServiceImpl<InvoiceBillInvoiceMapper, InvoiceBillInvoice> implements IInvoiceBillInvoiceService {

}
