package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN")
@ApiModel(value="Train对象", description="")
public class Train extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "培训ID")
    @TableId(value = "GT_ID", type = IdType.AUTO)
    private Long gtId;

    @ApiModelProperty(value = "培训类型(1、培训,2、考试)")
    @TableField("GT_SUBJECT")
    private Integer gtSubject;

    @ApiModelProperty(value = "培训描述")
    @TableField("GT_NAME")
    private String gtName;

    @ApiModelProperty(value = "时间属性(1、永久,2、期限保留)")
    @TableField("GT_END_TYPE")
    private Integer gtEndType;

    @ApiModelProperty(value = "开始日期")
    @TableField("GT_START_DATE")
    private String gtStartDate;

    @ApiModelProperty(value = "结束日期")
    @TableField("GT_DEADLINE")
    private String gtDeadline;

    @ApiModelProperty(value = "培训保留期")
    @TableField("GT_DURATION")
    private Integer gtDuration;

    @ApiModelProperty(value = "营销培训")
    @TableField("GT_TYPE5")
    private Integer gtType5;

    @ApiModelProperty(value = "加盟商")
    @TableField("GT_CLIENT")
    private String gtClient;

    @ApiModelProperty(value = "创建人加盟商")
    @TableField("GT_CREATE_CLIENT")
    private String gtCreateClient;

    @ApiModelProperty(value = "创建人")
    @TableField("GT_CREATE_USER")
    private String gtCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GT_CREATE_DATE")
    private String gtCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GT_CREATE_TIME")
    private String gtCreateTime;

    @ApiModelProperty(value = "培训状态 （0：停用，1：启用）")
    @TableField("GT_STATUS")
    private Integer gtStatus;

    @ApiModelProperty(value = "停用原因")
    @TableField("GT_REASON")
    private String gtReason;

    @ApiModelProperty(value = "停用提示")
    @TableField("GT_PROMPT")
    private String gtPrompt;

    @ApiModelProperty(value = "停用时间")
    @TableField("GT_DOWN_TIME")
    private String gtDownTime;

    @ApiModelProperty(value = "是否推送(0:未推送 1：已推送)")
    @TableField("GT_PUSH")
    private Integer gtPush;

    @ApiModelProperty(value = "计划推送时间")
    @TableField("GT_PLAN_PUSH_TIME")
    private String gtPlanPushTime;

    @ApiModelProperty(value = "推送时间")
    @TableField("GT_PUSH_TIME")
    private String gtPushTime;

    @ApiModelProperty(value = "课件推送类型(1:随机，2：手动选择)")
    @TableField("GT_COURSE_TYPE")
    private Integer gtCourseType;

    @ApiModelProperty(value = "停用操作人")
    @TableField("GT_DOWN_USER")
    private String gtDownUser;

    @ApiModelProperty(value = "停用操作人加盟商")
    @TableField("GT_DOWN_CLIENT")
    private String gtDownClient;


}
