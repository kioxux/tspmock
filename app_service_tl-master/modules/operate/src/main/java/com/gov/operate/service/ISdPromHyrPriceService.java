package com.gov.operate.service;

import com.gov.operate.entity.SdPromHyrPrice;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface ISdPromHyrPriceService extends SuperService<SdPromHyrPrice> {

}
