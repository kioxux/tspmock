package com.gov.operate.dto;

import com.gov.common.entity.dataImport.PromAssoCondsProduct;
import com.gov.common.entity.dataImport.PromGiftCondsProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromAssoCondsProductImportDto extends PromAssoCondsProduct {
    private String proSelfCode;

    private String proCommonname;

    private String proName;

    private String proSpecs;

    private String proFactoryName;
}
