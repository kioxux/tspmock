package com.gov.operate.mapper;

import com.gov.operate.entity.SdPromCouponUse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface SdPromCouponUseMapper extends BaseMapper<SdPromCouponUse> {

}
