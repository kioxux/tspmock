package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.GetMemberListDTO;
import com.gov.operate.dto.GetMemberListVO;
import com.gov.operate.entity.SdMemberBasic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-22
 */
public interface SdMemberBasicMapper extends BaseMapper<SdMemberBasic> {

    /**
     * 精准查询
     */
    List<GetMemberListDTO> getMemberList(Page<GetMemberListDTO> page, @Param("vo") GetMemberListVO vo);
}
