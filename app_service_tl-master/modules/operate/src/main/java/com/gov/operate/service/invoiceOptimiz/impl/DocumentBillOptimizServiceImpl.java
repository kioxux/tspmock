package com.gov.operate.service.invoiceOptimiz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDto;
import com.gov.operate.entity.DocumentBill;
import com.gov.operate.entity.DocumentBillD;
import com.gov.operate.entity.DocumentBillH;
import com.gov.operate.entity.DocumentInvoiceBill;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.DocumentBillDMapper;
import com.gov.operate.mapper.DocumentBillMapper;
import com.gov.operate.mapper.DocumentInvoiceBillMapper;
import com.gov.operate.mapper.InvoiceMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IDocumentBillDService;
import com.gov.operate.service.IDocumentBillHService;
import com.gov.operate.service.IDocumentInvoiceBillService;
import com.gov.operate.service.invoiceOptimiz.DocumentBillOptimizService;
import com.gov.redis.jedis.RedisClient;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.16
 */
@Service
public class DocumentBillOptimizServiceImpl implements DocumentBillOptimizService {
    /**
     * 对账单号key
     */
    private final static String DOCUMENTBILLNUM_KEY = "DOCUMENTBILLNUM";
    @Resource
    private CommonService commonService;
    @Resource
    private InvoiceMapper invoiceMapper;
    @Resource
    private DocumentBillMapper documentBillMapper;
    @Resource
    private DocumentBillDMapper documentBillDMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private IDocumentBillHService iDocumentBillHService;
    @Resource
    private IDocumentBillDService iDocumentBillDService;
    @Resource
    private DocumentInvoiceBillMapper documentInvoiceBillMapper;
    @Resource
    private IDocumentInvoiceBillService iDocumentInvoiceBillService;
    @Resource
    private CosUtils cosUtils;

    /**
     * 单据列表
     *
     * @param vo
     * @return
     */
    @Override
    public IPage<SelectWarehousingDTO> selectDocumentList(SelectWarehousingVO vo) {

        // 地点和单体店若只有一个有值，那么另一个赋特殊值”-1“
        if (!((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))
                || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
            if (StringUtils.isBlank(vo.getSite())) {
                vo.setSite("-1");
            } else {
                vo.setStoCode("-1");
            }
        }
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(vo.getPageNum(), vo.getPageSize());
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = invoiceMapper.selectDocumentList(page, vo);
        selectWarehousingDTOS.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
            }

            // 明细
            SelectWarehousingDetailsVO selectWarehousingDetailsVO = new SelectWarehousingDetailsVO();
            selectWarehousingDetailsVO.setClient(user.getClient());
            selectWarehousingDetailsVO.setType(item.getType());
            selectWarehousingDetailsVO.setMatDnId(item.getMatDnId());
            List<SelectWarehousingDetailsDTO> list = invoiceMapper.selectDocumentDetails(selectWarehousingDetailsVO);
            list.forEach(detail -> {
                // 如果为出库 则 金额均为 负数
                if ("GD".equals(detail.getType()) || "CJ".equals(detail.getType())) {
                    BigDecimal totalAmount = detail.getTotalAmount();
//                    BigDecimal registeredAmount = detail.getRegisteredAmount();
                    // 税额
                    detail.setRateBat(detail.getRateBat().abs().multiply(Constants.MINUS_ONE));
                    // 去税金额
                    detail.setExcludingTaxAmount(detail.getExcludingTaxAmount().multiply(Constants.MINUS_ONE));
                    // 总额
                    detail.setTotalAmount(totalAmount.multiply(Constants.MINUS_ONE));
                }
            });
            item.setDetailList(list);
        });
        return selectWarehousingDTOS;
    }

    /**
     * 单据合计
     *
     * @param vo
     * @return
     */
    @Override
    public Result selectDocumentTotal(SelectWarehousingVO vo) {
        // 地点和单体店若只有一个有值，那么另一个赋特殊值”-1“
        if (!((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))
                || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
            if (StringUtils.isBlank(vo.getSite())) {
                vo.setSite("-1");
            } else {
                vo.setStoCode("-1");
            }
        }
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(-1, -1);
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = invoiceMapper.selectDocumentList(page, vo);
        selectWarehousingDTOS.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
            }
        });
        SelectWarehousingDTO selectWarehousingDTO = new SelectWarehousingDTO();
        List<SelectWarehousingDTO> list = selectWarehousingDTOS.getRecords();
        if (!CollectionUtils.isEmpty(list)) {
            // 去税金额 excludingTaxAmount
            selectWarehousingDTO.setExcludingTaxAmount(list.stream().map(e -> e.getExcludingTaxAmount()).reduce(BigDecimal.ZERO, BigDecimal::add));
            // 税额 taxAmount
            selectWarehousingDTO.setTaxAmount(list.stream().map(e -> e.getTaxAmount()).reduce(BigDecimal.ZERO, BigDecimal::add));
            // 含税税金额 totalAmount
            selectWarehousingDTO.setTotalAmount(list.stream().map(e -> e.getTotalAmount()).reduce(BigDecimal.ZERO, BigDecimal::add));
            // 已对账金额 gdbhBillAmount
            selectWarehousingDTO.setGdbhBillAmount(list.stream().map(e -> e.getGdbhBillAmount()).reduce(BigDecimal.ZERO, BigDecimal::add));
        } else {
            selectWarehousingDTO.setExcludingTaxAmount(BigDecimal.ZERO);
            selectWarehousingDTO.setTaxAmount(BigDecimal.ZERO);
            selectWarehousingDTO.setTotalAmount(BigDecimal.ZERO);
            selectWarehousingDTO.setGdbhBillAmount(BigDecimal.ZERO);
        }
        return ResultUtil.success(selectWarehousingDTO);
    }

    /**
     * 单据对账
     *
     * @param documentBillDto
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void documentBill(DocumentBillDto documentBillDto) {
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        // 验证
        // 收货金额 = 折扣 + 应付 + 差异
        if (documentBillDto.getGdbTotalAmt() == null) {
            throw new CustomResultException("收货金额不能为空");
        }
        if (documentBillDto.getGdbPayAmt() == null) {
            throw new CustomResultException("应付金额不能为空");
        }
        if (documentBillDto.getGdbDiff() != null &&
                documentBillDto.getGdbDiff().compareTo(BigDecimal.ZERO) != 0 &&
                StringUtils.isBlank(documentBillDto.getGdbReason())) {
            throw new CustomResultException("请填入差异原因");
        }
        if (documentBillDto.getGdbTotalAmt().compareTo(DecimalUtils.add(documentBillDto.getGdbRebate(), documentBillDto.getGdbPayAmt(), documentBillDto.getGdbDiff())) != 0) {
            throw new CustomResultException("应付金额不正确，请核实");
        }
        // 选择数据验证
        if (CollectionUtils.isEmpty(documentBillDto.getSelectWarehousingDTOList())) {
            throw new CustomResultException("请选择对应表单");
        }
        Long billNum = documentBillMapper.selectBillNum(user.getClient());
        // 单号
        while (redisClient.exists(DOCUMENTBILLNUM_KEY + billNum)) {
            billNum++;
        }
        redisClient.set(DOCUMENTBILLNUM_KEY + billNum, billNum.toString(), 120);
        // 供应商
        String supCode = "";
        // 单据验证
        List<DocumentBillH> dcoumentBillHList = new ArrayList<>();
        List<DocumentBillD> documentBillDList = new ArrayList<>();
        for (SelectWarehousingDTO selectWarehousingDTO : documentBillDto.getSelectWarehousingDTOList()) {
            if (StringUtils.isBlank(supCode)) {
                supCode = selectWarehousingDTO.getSupSelfCode();
            } else {
                if (!supCode.equals(selectWarehousingDTO.getSupSelfCode())) {
                    throw new CustomResultException("请选择同一供应商单据");
                }
            }
            SelectWarehousingVO vo = new SelectWarehousingVO();
            vo.setClient(user.getClient());
            vo.setBillNo(selectWarehousingDTO.getMatDnId());
            vo.setType(selectWarehousingDTO.getType());
            SelectWarehousingDTO entity = invoiceMapper.selectDocumentItem(vo);
            if (entity == null) {
                throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "不存在");
            }
            // 单据明细验证
            if (CollectionUtils.isEmpty(selectWarehousingDTO.getDetailList())) {
                throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "没有选择明细数据");
            }
            // 单据对账金额
            BigDecimal gdbhBillAmount = BigDecimal.ZERO;
            for (SelectWarehousingDetailsDTO selectDocumentDetail : selectWarehousingDTO.getDetailList()) {
                SelectWarehousingDetailsVO selectWarehousingDetailsVO = new SelectWarehousingDetailsVO();
                selectWarehousingDetailsVO.setClient(user.getClient());
                selectWarehousingDetailsVO.setMatId(selectDocumentDetail.getMatId());
                selectWarehousingDetailsVO.setMatYear(selectDocumentDetail.getMatYear());
                selectWarehousingDetailsVO.setMatLineNo(selectDocumentDetail.getMatLineNo());
                SelectWarehousingDetailsDTO selectWarehousingDetailsDTO = invoiceMapper.selectDocumentDetail(selectWarehousingDetailsVO);
                if (selectWarehousingDetailsDTO == null) {
                    throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "明细商品" + selectDocumentDetail.getProCode() + "不存在");
                }
                List<DocumentBillD> documentBillDEntityList = documentBillDMapper.selectList(
                        new QueryWrapper<DocumentBillD>()
                                .eq("CLIENT", user.getClient())
                                .eq("GDBD_BUSINESS_TYPE", selectDocumentDetail.getType())
                                .eq("GDBD_MAT_DN_ID", selectDocumentDetail.getMatDnId())
                                .eq("MAT_ID", selectDocumentDetail.getMatId())
                                .eq("MAT_YEAR", selectDocumentDetail.getMatYear())
                                .eq("MAT_LINE_NO", selectDocumentDetail.getMatLineNo())
                );
                if (!CollectionUtils.isEmpty(documentBillDEntityList)) {
                    throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "明细商品" + selectDocumentDetail.getProCode() + "已对账");
                }
                gdbhBillAmount = DecimalUtils.add(gdbhBillAmount, selectDocumentDetail.getTotalAmount());
                DocumentBillD documentBillD = new DocumentBillD();
                // 加盟商
                documentBillD.setClient(user.getClient());
                // 对账单号
                documentBillD.setGdbdNum("DJDZ" + billNum);
                // 业务类型
                documentBillD.setGdbdBusinessType(selectDocumentDetail.getType());
                // 业务单号
                documentBillD.setGdbdMatDnId(selectDocumentDetail.getMatDnId());
                // 物料凭证号
                documentBillD.setMatId(selectDocumentDetail.getMatId());
                // 物料凭证年份
                documentBillD.setMatYear(selectDocumentDetail.getMatYear());
                // 物料凭证行号
                documentBillD.setMatLineNo(selectDocumentDetail.getMatLineNo());
                // 商品编码
                documentBillD.setMatProCode(selectDocumentDetail.getProCode());
                // 行金额
                documentBillD.setGdbdLineAmount(selectDocumentDetail.getTotalAmount());
                // 数量
                documentBillD.setMatQty(selectDocumentDetail.getMatQty());
                // 总金额（批次）
                documentBillD.setMatBatAmt(selectDocumentDetail.getExcludingTaxAmount());
                // 税金（批次）
                documentBillD.setMatRateBat(selectDocumentDetail.getRateBat());
                // 创建人
                documentBillD.setGdbdCreateUser(user.getUserId());
                // 创建日期
                documentBillD.setGdbdCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                // 创建时间
                documentBillD.setGdbdCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                documentBillDList.add(documentBillD);
            }
            DocumentBillH documentBillH = new DocumentBillH();
            // 加盟商
            documentBillH.setClient(user.getClient());
            // 对账单号
            documentBillH.setGdbhNum("DJDZ" + billNum);
            // 业务类型
            documentBillH.setGdbhBusinessType(selectWarehousingDTO.getType());
            // 业务单号
            documentBillH.setGdbhMatDnId(selectWarehousingDTO.getMatDnId());
            // 地点
            documentBillH.setGdbhSite(selectWarehousingDTO.getSite());
            // 供应商
            documentBillH.setGdbhSupCode(selectWarehousingDTO.getSupSelfCode());
            // 单据总金额
            documentBillH.setGdbhDocumentAmount(selectWarehousingDTO.getTotalAmount());
            // 对账金额
            documentBillH.setGdbhBillAmount(gdbhBillAmount);
            // 创建人
            documentBillH.setGdbhCreateUser(user.getUserId());
            // 创建日期
            documentBillH.setGdbhCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            documentBillH.setGdbhCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            dcoumentBillHList.add(documentBillH);
        }
        DocumentBill documentBill = new DocumentBill();
        // 加盟商
        documentBill.setClient(user.getClient());
        // 对账单号
        documentBill.setGdbNum("DJDZ" + billNum);
        // 供应商
        documentBill.setGdbSupCode(documentBillDto.getSelectWarehousingDTOList().get(0).getSupSelfCode());
        // 收货金额
        documentBill.setGdbTotalAmt(documentBillDto.getGdbTotalAmt());
        // 折扣
        documentBill.setGdbRebate(documentBillDto.getGdbRebate() == null ? BigDecimal.ZERO : documentBillDto.getGdbRebate());
        // 应付金额
        documentBill.setGdbPayAmt(documentBillDto.getGdbPayAmt());
        // 差异
        documentBill.setGdbDiff(documentBillDto.getGdbDiff() == null ? BigDecimal.ZERO : documentBillDto.getGdbDiff());
        // 原因
        documentBill.setGdbReason(documentBillDto.getGdbReason());
        // 创建人
        documentBill.setGdbCreateUser(user.getUserId());
        // 创建日期
        documentBill.setGdbCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 创建时间
        documentBill.setGdbCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        documentBillMapper.insert(documentBill);
        // 单据集合
        iDocumentBillHService.saveBatch(dcoumentBillHList);
        // 明细集合
        iDocumentBillDService.saveBatch(documentBillDList);
    }

    /**
     * 单据已登记发票
     *
     * @param type
     * @param matDnId
     * @return
     */
    @Override
    public Result selectInvoiceListByDoc(String type, String matDnId) {
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        List<FicoInvoiceInformationRegistrationDTO> list = documentInvoiceBillMapper.selectInvoiceListByDoc(user.getClient(), matDnId, type);
        return ResultUtil.success(list);
    }

    /**
     * 可登记 发票
     *
     * @param pageNum
     * @param pageSize
     * @param siteCode
     * @param supCode
     * @param invoiceNum
     * @param invoiceDateStart
     * @param invoiceDateEnd
     * @return
     */
    @Override
    public Result selectInvoiceList(Integer pageNum, Integer pageSize, String siteCode, String supCode,
                                    String invoiceNum, String invoiceDateStart, String invoiceDateEnd) {
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        Page page = new Page<SelectWarehousingDTO>(pageNum, pageSize);
        IPage<FicoInvoiceInformationRegistrationDTO> iPage = documentInvoiceBillMapper.selectInvoiceListForBill(page, user.getClient(), siteCode, supCode,
                invoiceNum, invoiceDateStart, invoiceDateEnd);
        return ResultUtil.success(iPage);
    }

    /**
     * 绑定发票
     *
     * @param selectWarehousingDTO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bindInvoice(SelectWarehousingDTO selectWarehousingDTO) {
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        // 发票删除
        documentInvoiceBillMapper.delete(
                new QueryWrapper<DocumentInvoiceBill>().eq("CLIENT", user.getClient())
                        .eq("GDIB_BUSINESS_TYPE", selectWarehousingDTO.getType())
                        .eq("GDIB_MAT_DN_ID", selectWarehousingDTO.getMatDnId())
        );
        if (CollectionUtils.isEmpty(selectWarehousingDTO.getInvoiceNumList())) {
            return;
        }
        List<DocumentInvoiceBill> documentInvoiceBillList = new ArrayList<>();
        for (String invoiceNum : selectWarehousingDTO.getInvoiceNumList()) {
            DocumentInvoiceBill documentInvoiceBill = new DocumentInvoiceBill();
            // 加盟商
            documentInvoiceBill.setClient(user.getClient());
            // 业务类型
            documentInvoiceBill.setGdibBusinessType(selectWarehousingDTO.getType());
            // 业务单号
            documentInvoiceBill.setGdibMatDnId(selectWarehousingDTO.getMatDnId());
            // 发票号
            documentInvoiceBill.setGdibInvoiceNum(invoiceNum);
            documentInvoiceBillList.add(documentInvoiceBill);
        }
        iDocumentInvoiceBillService.saveBatch(documentInvoiceBillList);
    }

    /**
     * 单据明细导出
     *
     * @param detailList
     * @return
     */
    @Override
    public Result exportDetailList(List<SelectWarehousingDetailsDTO> detailList) throws IOException {
        List<List<Object>> dataList = new ArrayList<>();
        for (int i = 0; !CollectionUtils.isEmpty(detailList) && i < detailList.size(); i++) {
            SelectWarehousingDetailsDTO item = detailList.get(i);
            List<Object> data = new ArrayList<>();
            // 商品
            data.add(StringUtils.parse("{}-{}", item.getProCode(), item.getProName()));
            // 规格
            data.add(item.getProSpecs());
            // 厂家
            data.add(item.getProFactoryName());
            // 业务类型
            data.add(StringUtils.parse("{}-{}", item.getType(), item.getTypeName()));
            // 数量
            data.add(item.getMatQty());
            // 单价
            data.add(item.getPrice());
            // 去税金额
            data.add(item.getExcludingTaxAmount());
            // 税额
            data.add(item.getRateBat());
            // 含税金额
            data.add(item.getTotalAmount());
            dataList.add(data);
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("单据明细");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "商品",
            "规格",
            "厂家",
            "业务类型",
            "数量",
            "单价",
            "去税金额",
            "税额",
            "含税金额",
    };
}
