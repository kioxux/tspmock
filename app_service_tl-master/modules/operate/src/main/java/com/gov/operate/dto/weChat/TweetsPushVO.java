package com.gov.operate.dto.weChat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.common.entity.weChat.BatchgetMaterialRes;
import com.gov.operate.dto.OfficialAccountsDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode
public class TweetsPushVO extends BatchgetMaterialRes.ItemBean {

    private List<OfficialAccountsDTO> customList;

    /**
     * null:历史素材，1：发布列表
     */
    private String materialType;
}
