package com.gov.operate.dto.invoice;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SalesInvoicePageDTO extends Pageable {

    private String client;

    /**
     * 业务机构(dc/门店)
     */
    private String matSiteCode;

    /**
     * 客户编码 （门店/客户）
     */
    private String cusCode;

    /**
     * 发生开始时间
     */
    private String docDateStart;

    /**
     * 发生结束时间
     */
    private String docDateEnd;

    /**
     * 单据状态
     */
    private String status;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 厂家
     */
    private String proFactoryName;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 单据号
     */
    private String matDnId;

    /**
     * 1正向发票，2负向发票
     */
    private Integer pnType;

    private String rateName;

}
