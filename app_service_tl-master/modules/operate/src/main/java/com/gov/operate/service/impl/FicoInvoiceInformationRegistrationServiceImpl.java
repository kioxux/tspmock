package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.AddInvoiceVO;
import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import com.gov.operate.entity.InvoiceBillInvoice;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.FicoInvoiceInformationRegistrationMapper;
import com.gov.operate.mapper.InvoiceBillInvoiceMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IFicoInvoiceInformationRegistrationService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class FicoInvoiceInformationRegistrationServiceImpl extends ServiceImpl<FicoInvoiceInformationRegistrationMapper, FicoInvoiceInformationRegistration> implements IFicoInvoiceInformationRegistrationService {

    @Resource
    private CommonService commonService;
    @Resource
    private FicoInvoiceInformationRegistrationMapper invoiceInformationRegistrationMapper;
    @Resource
    private InvoiceBillInvoiceMapper invoiceBillInvoiceMapper;

    /**
     * 发票登记
     */
    @Override
    public void addInvoice(List<AddInvoiceVO> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("发票登记列表不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        HashSet<String> invoiceNumSet = new HashSet<>();
        list.forEach(item -> {
            invoiceNumSet.add(item.getInvoiceNum());
        });
        if (list.size() != invoiceNumSet.size()) {
            throw new CustomResultException("发票号码不能重复");
        }
        list.forEach(item -> {
            LocalDate invoiceDate = LocalDate.parse(item.getInvoiceDate(), DateUtils.DATE_FORMATTER_YYMMDD);
            LocalDate now = LocalDate.now();
            if (invoiceDate.isAfter(now)) {
                throw new CustomResultException(MessageFormat.format("发票[{0}],发票日期不能大于今天", item.getInvoiceNum()));
            }
        });
        List<FicoInvoiceInformationRegistration> registrationList = this.list(new QueryWrapper<FicoInvoiceInformationRegistration>()
                .select("INVOICE_NUM")
                .eq("CLIENT", user.getClient())
                .in("INVOICE_NUM", invoiceNumSet.toArray()));
        if (!CollectionUtils.isEmpty(registrationList)) {
            String numExitList = registrationList.stream().map(FicoInvoiceInformationRegistration::getInvoiceNum).collect(Collectors.joining(","));
            throw new CustomResultException(MessageFormat.format("发票号码[{0}]库中已经存在，不可重复", numExitList));
        }

        List<FicoInvoiceInformationRegistration> regList = list.stream().map(item -> {
            FicoInvoiceInformationRegistration informationRegistration = new FicoInvoiceInformationRegistration();
            BeanUtils.copyProperties(item, informationRegistration);
            // 采购地点
            informationRegistration.setSiteCode(item.getSite());
            // 供应商编码
            informationRegistration.setSupCode(item.getSupSelfCode());
            // 加盟商
            informationRegistration.setClient(user.getClient());
            // 登记状态
            informationRegistration.setInvoiceRegistrationStatus("0");
            // 付款状态
            informationRegistration.setInvoicePaymentStatus("0");
            // 创建日期
            informationRegistration.setInvoiceCreationDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建人
            informationRegistration.setInvoiceFounder(user.getUserId());
            // 发票确认金额
            informationRegistration.setInvoiceConfirmationAmount(item.getInvoiceTotalInvoiceAmount());
            // 发票地点类型（1:单体 2.连锁）
            informationRegistration.setInvoiceSiteType(item.getInvoiceSiteType());
            return informationRegistration;
        }).collect(Collectors.toList());
        this.saveBatch(regList);
    }

    /**
     * 可用发票
     */
    @Override
    public List<FicoInvoiceInformationRegistrationDTO> selectAvailableInvoice(String type, String supCode, String site) {
        if (StringUtils.isNotBlank(type) && !"CD".equals(type) && !"GD".equals(type) && !"CX".equals(type) && !"CJ".equals(type) && !"CJTZ".equals(type)) {
            throw new CustomResultException("业务类型不正确");
        }
        TokenUser user = commonService.getLoginInfo();
        List<FicoInvoiceInformationRegistrationDTO> list = invoiceInformationRegistrationMapper.selectAvailableInvoice(user.getClient(), supCode, site, type);
        return list;
    }

    /**
     * 根据发票号码来查询发票
     */
    @Override
    public Result selectInvoice(String invoiceNum) {
        TokenUser user = commonService.getLoginInfo();
        return ResultUtil.success(invoiceInformationRegistrationMapper.selectOne(new QueryWrapper<FicoInvoiceInformationRegistration>()
                .eq("INVOICE_NUM", invoiceNum)
                .eq("CLIENT", user.getClient())));
    }

    /**
     * 根据发票号码来更新发票
     */
    @Override
    public Result editInvoice(FicoInvoiceInformationRegistrationDTO registrationDTO) {
        if (null == registrationDTO || StringUtils.isBlank(registrationDTO.getInvoiceNum())) {
            throw new CustomResultException("参数错误");
        }
        TokenUser user = commonService.getLoginInfo();
        FicoInvoiceInformationRegistration invoiceInformationRegistration = invoiceInformationRegistrationMapper.selectOne(new QueryWrapper<FicoInvoiceInformationRegistration>()
                .eq("INVOICE_NUM", registrationDTO.getInvoiceNum())
                .eq("CLIENT", user.getClient()));
        if (null == invoiceInformationRegistration) {
            throw new CustomResultException("数据不存在");
        }

        if (invoiceInformationRegistration.getInvoicePaidAmount().compareTo(BigDecimal.ZERO) > 0 || invoiceInformationRegistration.getInvoiceRegisteredAmount().compareTo(BigDecimal.ZERO) > 0) {
            throw new CustomResultException("此发票不可以修改");
        }
        // 已对账发票不可以修改
        List<InvoiceBillInvoice> invoiceBillInvoiceList = invoiceBillInvoiceMapper.selectList(
                new QueryWrapper<InvoiceBillInvoice>()
                        .eq("CLIENT", user.getClient())
                        .eq("GIBI_INVOICE_NUM", registrationDTO.getInvoiceNum())
        );
        if (!CollectionUtils.isEmpty(invoiceBillInvoiceList)) {
            throw new CustomResultException("已对账发票不可以修改");
        }
        registrationDTO.setInvoiceRegistrationStatus(null);
        registrationDTO.setInvoicePaymentStatus(null);
        registrationDTO.setInvoiceCreationDate(null);
        registrationDTO.setInvoiceFounder(null);
        BeanUtils.copyProperties(registrationDTO, invoiceInformationRegistration);
        invoiceInformationRegistration.setInvoiceConfirmationAmount(invoiceInformationRegistration.getInvoiceTotalInvoiceAmount());
        invoiceInformationRegistrationMapper.update(invoiceInformationRegistration, new QueryWrapper<FicoInvoiceInformationRegistration>()
                .eq("INVOICE_NUM", registrationDTO.getInvoiceNum())
                .eq("CLIENT", user.getClient()));
        return ResultUtil.success();
    }
}
