package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.RoleVO;
import com.gov.operate.dto.UpdateRoleVO;
import com.gov.operate.entity.AuthorityRoles;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface IAuthorityRolesService extends SuperService<AuthorityRoles> {

    Result createRole(RoleVO vo);

    Result deleteRole(Integer id);

    Result updateRole(UpdateRoleVO vo);

    Result roleList();

}
