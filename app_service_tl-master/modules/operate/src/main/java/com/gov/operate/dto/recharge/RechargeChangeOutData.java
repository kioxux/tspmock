package com.gov.operate.dto.recharge;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class RechargeChangeOutData implements Serializable {

    /**
     * 序号
     */
    @ApiModelProperty(name = "序号")
    private Integer index;

    /**
     * 储值卡卡号
     */
    @ApiModelProperty(name = "储值卡卡号")
    private String gsrcId;

    /**
     * 会员卡号
     */
    @ApiModelProperty(name = "会员卡号")
    private String gsrcMemberCardId;

    /**
     * 姓名
     */
    @ApiModelProperty(name = "姓名")
    private String gsrcName;

    /**
     * 性别
     */
    @ApiModelProperty(name = "性别")
    private String gsrcSex;

    /**
     * 手机
     */
    @ApiModelProperty(name = "手机")
    private String gsrcMobile;

    /**
     * 日期
     */
    @ApiModelProperty(name = "日期")
    private String gsrcDate;

    /**
     * 时间
     */
    @ApiModelProperty(name = "时间")
    private String gsrcTime;

    /**
     * 门店编码
     */
    @ApiModelProperty(name = "门店编码")
    private String gsrcBrId;

    /**
     * 门店名称
     */
    @ApiModelProperty(name = "门店名称")
    private String stoShortName;

    /**
     * 操作人员
     */
    @ApiModelProperty(name = "操作人员")
    private String gsrcEmp;

    /**
     * 异动单号
     */
    @ApiModelProperty(name = "单号")
    private String gsrcVoucherId;

    /**
     * 储值卡初始金额
     */
    @ApiModelProperty(name = "储值卡初始金额")
    private BigDecimal gsrcInitialAmt;
    /**
     * 储值卡异动金额
     */
    @ApiModelProperty(name = "储值卡异动金额")
    private BigDecimal gsrcChangeAmt;
    /**
     * 储值卡结果金额
     */
    @ApiModelProperty(name = "储值卡结果金额")
    private BigDecimal gsrcResultAmt;

    /**
     * 异动类型 1：充值 2：退款 3：消费 4：退货
     */
    @ApiModelProperty(name ="异动类型 1：充值 2：退款 3：消费 4：退货")
    private String gsrcType;
}
