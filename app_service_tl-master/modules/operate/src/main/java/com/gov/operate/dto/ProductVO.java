package com.gov.operate.dto;

import lombok.Data;

@Data
public class ProductVO {

    /**
     * 商品编码
     */
    private String gsmCode;

    /**
     * 促销ID
     */
    private String gsmPromotId;
}
