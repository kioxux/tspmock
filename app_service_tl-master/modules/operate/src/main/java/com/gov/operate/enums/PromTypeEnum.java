package com.gov.operate.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PromTypeEnum {

    PROM_SINGLE("PROM_SINGLE", "PROM_SINGLE"),
    PROM_SERIES("PROM_SERIES", "PROM_SERIES"),
    PROM_GIFT("PROM_GIFT", "PROM_GIFT"),
    PROM_COMBIN("PROM_COMBIN", "PROM_COMBIN"),
    PROM_CHMED("PROM_CHMED", "PROM_CHMED");

    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getValue(String code) {
        PromTypeEnum[] folderEnums = values();
        for (PromTypeEnum folderEnum : folderEnums) {
            if (folderEnum.getCode().equals(code)) {
                return folderEnum.getValue();
            }
        }
        return null;
    }

    public static PromTypeEnum[] getValues() {
        return values();
    }
}
