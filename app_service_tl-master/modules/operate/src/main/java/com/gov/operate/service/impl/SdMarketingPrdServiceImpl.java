package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMarketingPrd;
import com.gov.operate.mapper.SdMarketingPrdMapper;
import com.gov.operate.service.ISdMarketingPrdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Service
public class SdMarketingPrdServiceImpl extends ServiceImpl<SdMarketingPrdMapper, SdMarketingPrd> implements ISdMarketingPrdService {

}
