package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromAssoConds;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
public interface SdPromAssoCondsMapper extends BaseMapper<SdPromAssoConds> {

    /**
     * 已选活动商品
     * @param page
     * @param productRequestVo
     * @return
     */
    IPage<ProductResponseVo> selectProductList(Page<SdPromAssoConds> page, @Param("product") ProductRequestVo productRequestVo);

    /**
     * 已选赠送商品
     * @param productRequestVo
     * @return
     */
    List<ProductResponseVo> selectGiftProductList(@Param("product") ProductRequestVo productRequestVo);

}
