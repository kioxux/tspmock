package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_BASIC")
@ApiModel(value="SdMemberBasic对象", description="")
public class SdMemberBasic extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员ID")
    @TableField("GSMB_MEMBER_ID")
    private String gsmbMemberId;

    @ApiModelProperty(value = "会员姓名")
    @TableField("GSMB_NAME")
    private String gsmbName;

    @ApiModelProperty(value = "地址")
    @TableField("GSMB_ADDRESS")
    private String gsmbAddress;

    @ApiModelProperty(value = "手机")
    @TableField("GSMB_MOBILE")
    private String gsmbMobile;

    @ApiModelProperty(value = "电话")
    @TableField("GSMB_TEL")
    private String gsmbTel;

    @ApiModelProperty(value = "性别")
    @TableField("GSMB_SEX")
    private String gsmbSex;

    @ApiModelProperty(value = "身份证")
    @TableField("GSMB_CREDENTIALS")
    private String gsmbCredentials;

    @ApiModelProperty(value = "年龄")
    @TableField("GSMB_AGE")
    private String gsmbAge;

    @ApiModelProperty(value = "生日")
    @TableField("GSMB_BIRTH")
    private String gsmbBirth;

    @ApiModelProperty(value = "BB姓名")
    @TableField("GSMB_BB_NAME")
    private String gsmbBbName;

    @ApiModelProperty(value = "BB性别")
    @TableField("GSMB_BB_SEX")
    private String gsmbBbSex;

    @ApiModelProperty(value = "BB年龄")
    @TableField("GSMB_BB_AGE")
    private String gsmbBbAge;

    @ApiModelProperty(value = "慢病类型")
    @TableField("GSMB_CHRONIC_DISEASE_TYPE")
    private String gsmbChronicDiseaseType;

    @ApiModelProperty(value = "是否接受本司信息")
    @TableField("GSMB_CONTACT_ALLOWED")
    private String gsmbContactAllowed;

    @ApiModelProperty(value = "会员状态")
    @TableField("GSMB_STATUS")
    private String gsmbStatus;

    @ApiModelProperty(value = "修改资料门店")
    @TableField("GSMB_UPDATE_BR_ID")
    private String gsmbUpdateBrId;

    @ApiModelProperty(value = "修改资料人员")
    @TableField("GSMB_UPDATE_SALER")
    private String gsmbUpdateSaler;

    @ApiModelProperty(value = "修改资料日期")
    @TableField("GSMB_UPDATE_DATE")
    private String gsmbUpdateDate;


}
