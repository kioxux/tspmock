package com.gov.operate.controller.dataImport;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.MaterialImportVO;
import com.gov.operate.dto.dataImport.materialDoc.InsertItemVO;
import com.gov.operate.entity.MaterialDocImp;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.dataImport.BusinessImportService;
import com.gov.operate.service.dataImport.IMaterialImportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "物料凭证导入功能")
@RestController
@RequestMapping("MaterialDocImport")
public class MaterialDocImportController {

    @Resource
    BusinessImportService businessImportService;

    @Resource
    IMaterialImportService materialImportService;

    @Log("查询")
    @ApiOperation("查询")
    @PostMapping("selectItem")
    public Result selectItem(@RequestBody @Valid MaterialImportVO materialImportVO) {
        return ResultUtil.success(materialImportService.selectImport(materialImportVO));
    }

    @Log("删除")
    @ApiOperation("删除")
    @PostMapping("deleteItem")
    public Result deleteItem(@RequestJson(value = "id", defaultValue = "主键") Integer id) {
        materialImportService.deleteMaterialItem(id);
        return ResultUtil.success();
    }

    @Log("导入")
    @ApiOperation("导入")
    @PostMapping("executeImport")
    public Result executeImport(@RequestJson(value = "id", defaultValue = "主键") Integer id) {
        materialImportService.executeImport(id);
        return ResultUtil.success();
    }

    @Log("新增")
    @ApiOperation("新增")
    @PostMapping("insertItem")
    public Result insertItem(@RequestBody @Valid InsertItemVO vo) {
        materialImportService.insertItem(vo);
        return ResultUtil.success();
    }
}
