package com.gov.operate.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author staxc
 * @Date 2020/10/14 15:20
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AddCleanDrawerVO {

    /**
     * 验收单号
     */
    private String gschExaVoucherId;

    /**
     * 清斗人员
     */
    @NotBlank(message = "清斗人员不能为空")
    private String gschEmp;

    /**
     * 备注
     */
    private String gschRemaks;

    /**
     * 清斗详细
     */
    List<AddCleanDrawerDetailVO> details;

    /**
     * 复核人员
     */
    private String gschEmp1;
}
