package com.gov.operate.service.dataImport.impl;

import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.dataImport.HyAndHyrPrice;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.dataImport.GetProByProCodeAndStoDTO;
import com.gov.operate.mapper.ProductBusinessMapper;
import com.gov.operate.service.CommonService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Description:
 *
 * @author: zp
 * @date: 2021.04.06
 */
@Service
public class HyAndHyrPriceImport extends BusinessImport {

    @Resource
    private CommonService commonService;
    @Resource
    private ProductBusinessMapper productBusinessMapper;


    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        String client = commonService.getLoginInfo().getClient();
        List<String> list = new ArrayList<>();
        List<String> errorList = new ArrayList<>();

        // 查询条件_门店编码集合
        List<String> stoCodeList = new ArrayList<>();
        if (ObjectUtils.isEmpty(field.get("stoCodeList"))) {
            errorList.add("门店列表查询条件不能为空");
        } else {
            stoCodeList = (List<String>) field.get("stoCodeList");
        }

        // 门店+商品 列表
        List<HyAndHyrPrice> dataList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            HyAndHyrPrice data = (HyAndHyrPrice) map.get(key);
            dataList.add(data);
        }

        List<GetProByProCodeAndStoDTO> proByProCodeAndStoList = productBusinessMapper.getProByProCodeAndStoList(client, dataList);
        for (Integer key : map.keySet()) {
            HyAndHyrPrice data = (HyAndHyrPrice) map.get(key);
            // 促销折扣 促销价 必填校验
            if ((StringUtils.isBlank(data.getGsphsPrice()) && StringUtils.isBlank(data.getGsphsRebate()))
                    || (StringUtils.isNotBlank(data.getGsphsPrice()) && StringUtils.isNotBlank(data.getGsphsRebate()))) {
                errorList.add(MessageFormat.format("第[{0}]行[促销折扣]和[促销价]必填一个且只能填写一个", key));
            }
            if (OperateEnum.InteFlag.NO.getName().equals(data.getGsphsInteFlag()) && StringUtils.isNotBlank(data.getGsphsInteRate())) {
                errorList.add(MessageFormat.format("第[{0}]行[是否积分]为“否”时[积分倍率]必须为空", key));
            }
            GetProByProCodeAndStoDTO dto = proByProCodeAndStoList.stream().filter(Objects::nonNull).filter(item -> {
                return item.getProSelfCode().equals(data.getProSelfCode());
            }).findFirst().orElse(null);

            if (ObjectUtils.isEmpty(dto)) {
                errorList.add(MessageFormat.format("第[{0}]行，不存在商品[{1}]", key, data.getProSelfCode()));
            } else {
                // (是否积分：是，积分倍率：空) --->积分倍率设置默认值：1
                String inteRate = "";
                if (OperateEnum.InteFlag.YES.getName().equals(data.getGsphsInteFlag()) && StringUtils.isBlank(data.getGsphsInteRate())) {
                    inteRate = "1";
                } else {
                    inteRate = data.getGsphsInteRate();
                }
                // 会员特价
                dto.setGsphsPrice(data.getGsphsPrice());
                dto.setGsphsRebate(data.getGsphsRebate());
                dto.setGsphsInteFlag(OperateEnum.InteFlag.NO.getName().equals(data.getGsphsInteFlag()) ? OperateEnum.InteFlag.NO.getCode() : OperateEnum.InteFlag.YES.getCode());
                dto.setGsphsInteRate(inteRate);

                // 会员日特价
                dto.setGsphpPrice(data.getGsphsPrice());
                dto.setGsphpRebate(data.getGsphsRebate());
                dto.setGsphpInteFlag(OperateEnum.InteFlag.NO.getName().equals(data.getGsphsInteFlag()) ? OperateEnum.InteFlag.NO.getCode() : OperateEnum.InteFlag.YES.getCode());
                dto.setGsphpInteRate(inteRate);
            }
        }

        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(proByProCodeAndStoList);
    }
}
