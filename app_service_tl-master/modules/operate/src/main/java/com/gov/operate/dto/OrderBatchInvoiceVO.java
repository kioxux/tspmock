package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class OrderBatchInvoiceVO {

    @Valid
    @NotEmpty(message = "业务单集合不能为空")
    private List<MatDn> matDnList;

    @Valid
    @NotEmpty(message = "发票集合不能为空")
    private List<Invoice> invoiceList;

    @Data
    @EqualsAndHashCode
    public static class MatDn{
        @NotBlank(message = "单据号不能为空")
        private String matDnId;
        @NotNull(message = "单据总金额不能为空")
        private BigDecimal totalAmount;
        @NotNull(message = "单据已登记金额不能为空")
        private BigDecimal registeredAmount;
    }

    @Data
    @EqualsAndHashCode
    public static class Invoice{
        @NotBlank(message = "发票号不能为空")
        private String invoiceNum;
        @NotNull(message = "发票总金额不能为空")
        private BigDecimal invoiceTotalInvoiceAmount;
        @NotNull(message = "发票已登记金额不能为空")
        private BigDecimal invoiceRegisteredAmount;
    }



}
