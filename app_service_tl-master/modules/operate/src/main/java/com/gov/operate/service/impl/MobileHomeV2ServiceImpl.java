package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyVo;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.GaiaSdOutOfStockPushRecord;
import com.gov.operate.entity.SdStoresGroup;
import com.gov.operate.entity.SaleTaskPlan;
import com.gov.operate.entity.StoreData;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.kylin.RowMapper;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IMobileHomeV2Service;
import com.gov.operate.service.ISdSaleDService;
import io.netty.util.internal.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

@Service
@Slf4j
public class MobileHomeV2ServiceImpl implements IMobileHomeV2Service {

    @Resource
    private CommonService commonService;

    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;

    @Resource
    private SdMemberCardMapper sdMemberCardMapper;

    @Resource
    private SdSaleDMapper saleDMapper;

    @Resource
    private ISdSaleDService saleDService;

    @Resource
    private UserDataMapper userDataMapper;

    @Resource
    private StoreDataMapper storeDataMapper;

    @Resource
    private PaymentMapper paymentMapper;

    @Resource
    private FranchiseeMapper franchiseeMapper;

    @Resource
    private SdStoresGroupMapper storesGroupMapper;

    @Resource
    private GaiaSdOutOfStockPushRecordMapper outOfStockPushRecordMapper;

    @Override
    public Result loginAppSaleTotal(HomeChartRequestDTO homeChartRequestDTO) {
        log.info("APP看板接口调用");
        TokenUser user = commonService.getLoginInfo();
        homeChartRequestDTO.setClientId(user.getClient());

        HomePushMoneyDTO homePushMoneyDTO = new HomePushMoneyDTO();
        HomeSalesNewVo homeSalesNewVo = new HomeSalesNewVo();
        homePushMoneyDTO.setClient(user.getClient());
        homePushMoneyDTO.setType(homeChartRequestDTO.getType().toString());

        // 当前人有权限门店集合
        List<String> stoCodeList = homeChartRequestDTO.getStoCodeList();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            stoCodeList = commonService.getStoCodeList();
        }
        String stoQueryWrapper = commonService.queryWapper(stoCodeList);
        log.info("报表门店查询: " + stoCodeList.toString());

        if (!CollectionUtils.isEmpty(stoCodeList)) {
            homePushMoneyDTO.setStoCodeList(stoCodeList);
        }


        String dateTime = "";
        log.info("报表参数：{}", JSONObject.toJSONString(homeChartRequestDTO));
        List<HomeSalesVo> homeSalesVo = new ArrayList<>();

        if (StringUtils.isNotBlank(stoQueryWrapper)) {
            switch (homeChartRequestDTO.getType()) {
                case 1://今日
                    dateTime = DateUtils.getCurrentDateStrYYMMDD();
                    //销售
                    homeSalesVo = getHomeSalesListByCurrentNew(homeChartRequestDTO, stoQueryWrapper);
                    break;
                case 2:
                    dateTime = DateUtils.getBeforeDateStrYYMMDD();
                    //销售
                    homeSalesVo = getHomeSalesListByYesterdayNew(homeChartRequestDTO, stoQueryWrapper);
                    break;
            }
        }
        if (ObjectUtils.isNotEmpty(homeSalesVo)) {
            homeSalesNewVo.setGssdAmt(homeSalesVo.get(0).getGssdAmt());
            homeSalesNewVo.setGssdGrossAmt(homeSalesVo.get(0).getGssdGrossAmt());
            homeSalesNewVo.setGssdGrossMar(homeSalesVo.get(0).getGssdGrossMar());
            homeSalesNewVo.setGssdTrans(homeSalesVo.get(0).getGssdTrans());
            homeSalesNewVo.setGssdAprice(homeSalesVo.get(0).getGssdAprice());
            homeSalesNewVo.setGssdStoreCount(homeSalesVo.get(0).getGssdStoreCount());
        }
        //会员卡数
        int cartCount = 0;
        if (homeChartRequestDTO.getStoCodeList() != null && homeChartRequestDTO.getStoCodeList().size() > 0) {
            if (stoCodeList.size() > 0) {
//                cartCount = sdMemberCardMapper.selectCardCountByParam(stoCodeList, user.getClient(), dateTime);
                cartCount = selectCardCountByKylin(stoCodeList, user.getClient(), dateTime);
            }
        } else {
//            cartCount = sdMemberCardMapper.selectCardCountByParam(null, user.getClient(), dateTime);
            cartCount = selectCardCountByKylin(null, user.getClient(), dateTime);
        }
        homeSalesNewVo.setMcardQty(new BigDecimal(cartCount));
        //销售提成
        HomePushMoneyVo homePushMoneyVo = getMonthPushMoney(homePushMoneyDTO);
        if (ObjectUtils.isNotEmpty(homePushMoneyVo)) {
            homeSalesNewVo.setDeductionWage(homePushMoneyVo.getDeductionWage());
            homeSalesNewVo.setDeductionWageSales(homePushMoneyVo.getDeductionWageSales());
            homeSalesNewVo.setDeductionWagePro(homePushMoneyVo.getDeductionWagePro());
        }
        //月计划
        LoginSalePlan loginSalePlan = getLoginSalePlanByMonth(homeChartRequestDTO);
        //累计销售相关
        homeSalesNewVo.setAAmt(loginSalePlan.getaAmt());
        homeSalesNewVo.setAGross(loginSalePlan.getaGross());
        homeSalesNewVo.setAMcardQty(loginSalePlan.getaMcardQty());
        //月计划相关金额
        SaleTaskPlan monthPlan = saleDMapper.selectSaleTaskPlan(user.getClient(),dateTime,stoCodeList);
        if (ObjectUtils.isNotEmpty(monthPlan)) {
            if (ObjectUtils.isNotEmpty(monthPlan.getSaleAmt()) && monthPlan.getSaleAmt().compareTo(BigDecimal.ZERO) != 0) {
                homeSalesNewVo.setTAmt(monthPlan.getSaleAmt());
            } else {
                homeSalesNewVo.setTAmt(BigDecimal.ZERO);
            }
            if (ObjectUtils.isNotEmpty(monthPlan.getSaleGross()) && monthPlan.getSaleGross().compareTo(BigDecimal.ZERO) != 0) {
                homeSalesNewVo.setTGross(monthPlan.getSaleGross());
            } else {
                homeSalesNewVo.setTGross(BigDecimal.ZERO);
            }
            if (ObjectUtils.isNotEmpty(monthPlan.getMcardQty()) && monthPlan.getMcardQty().compareTo(BigDecimal.ZERO) != 0) {
                homeSalesNewVo.setTMcardQty(monthPlan.getMcardQty());
            } else {
                homeSalesNewVo.setTMcardQty(BigDecimal.ZERO);
            }
            //达成率预警值预设
            homeSalesNewVo.setAmtWarning("0");
            homeSalesNewVo.setGrossWarning("0");
            homeSalesNewVo.setMcardQtyWarning("0");
            int day = Integer.valueOf(dateTime.substring(6));
            //达成率相关
            if (ObjectUtils.isNotEmpty(monthPlan.getDailySaleAmt()) && monthPlan.getDailySaleAmt().compareTo(BigDecimal.ZERO) != 0) {
                if (ObjectUtils.isNotEmpty(loginSalePlan.getaAmt()) && loginSalePlan.getaAmt().compareTo(BigDecimal.ZERO) != 0) {
                    BigDecimal ramt = loginSalePlan.getaAmt().divide(new BigDecimal(day), 4, BigDecimal.ROUND_HALF_UP).divide(monthPlan.getDailySaleAmt(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    if (ramt.compareTo(new BigDecimal(95)) == -1) {
                        homeSalesNewVo.setAmtWarning("1");
                    }
                    homeSalesNewVo.setRAmt(ramt.toString());
                } else {
                    homeSalesNewVo.setRAmt("0.00");
                }
            } else {
                homeSalesNewVo.setRAmt("0.00");
            }
            if (ObjectUtils.isNotEmpty(monthPlan.getDailySaleGross()) && monthPlan.getDailySaleGross().compareTo(BigDecimal.ZERO) != 0) {
                if (ObjectUtils.isNotEmpty(loginSalePlan.getaGross()) && loginSalePlan.getaGross().compareTo(BigDecimal.ZERO) != 0) {
                    BigDecimal rgross = loginSalePlan.getaGross().divide(new BigDecimal(day), 4, BigDecimal.ROUND_HALF_UP).divide(monthPlan.getDailySaleGross(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    if (rgross.compareTo(new BigDecimal(95)) == -1) {
                        homeSalesNewVo.setGrossWarning("1");
                    }
                    homeSalesNewVo.setRGross(rgross.toString());
                } else {
                    homeSalesNewVo.setRGross("0.00");
                }
            } else {
                homeSalesNewVo.setRGross("0.00");
            }
            if (ObjectUtils.isNotEmpty(monthPlan.getDailyMcardQty()) && monthPlan.getDailyMcardQty().compareTo(BigDecimal.ZERO) != 0) {
                if (ObjectUtils.isNotEmpty(loginSalePlan.getaMcardQty()) && loginSalePlan.getaMcardQty().compareTo(BigDecimal.ZERO) != 0) {
                    BigDecimal rmcardQty = loginSalePlan.getaMcardQty().divide(new BigDecimal(day), 4, BigDecimal.ROUND_HALF_UP).divide(monthPlan.getDailyMcardQty(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    homeSalesNewVo.setRMcardQty(rmcardQty.toString());
                    if (rmcardQty.compareTo(new BigDecimal(95)) == -1) {
                        homeSalesNewVo.setMcardQtyWarning("1");
                    }
                } else {
                    homeSalesNewVo.setRMcardQty("0.00");
                }
            } else {
                homeSalesNewVo.setRMcardQty("0.00");
            }
        }else {
            homeSalesNewVo.setTAmt(BigDecimal.ZERO);
            homeSalesNewVo.setTGross(BigDecimal.ZERO);
            homeSalesNewVo.setTMcardQty(BigDecimal.ZERO);
            homeSalesNewVo.setAmtWarning("0");
            homeSalesNewVo.setGrossWarning("0");
            homeSalesNewVo.setMcardQtyWarning("0");
            homeSalesNewVo.setRAmt("0.00");
            homeSalesNewVo.setRGross("0.00");
            homeSalesNewVo.setRMcardQty("0.00");
        }
        //销售天数
        homeSalesNewVo.setDay(loginSalePlan.getDay());
        return ResultUtil.success(homeSalesNewVo);
    }

    /**
     * 首页库存数据
     *
     * @param homeChartRequestDTO
     * @return
     */
    public Result getHomeStockList(HomeChartRequestDTO homeChartRequestDTO) {
        log.info("APP首页库存看板");
        TokenUser user = commonService.getLoginInfo();
        UserDataDTO userDataDTO = userDataMapper.hasStoreUser(user.getClient(), user.getUserId());
        if (userDataDTO == null) {
            return ResultUtil.error(ResultEnum.E0605);
        }
        //返回参数
        HomeStockVo homeStockVo = new HomeStockVo();
        //获取门店
        List<String> stoCodeList = homeChartRequestDTO.getStoCodeList();
        List<String> directStoreList = null;
        //根据加盟商判断公司是否为连锁公司(是否有配送中心) 返回 0-没有 1-有
        String isChain = getStoChainByClient(user.getClient());

        //门店级
        if (ObjectUtils.isNotEmpty(stoCodeList)) {
            //门店库存查询
            homeStockVo = saleDMapper.selectStoreStock(user.getClient(), homeChartRequestDTO.getStoCodeList());
            log.info("库存报表回结果:{}", JSONObject.toJSONString(homeStockVo));

            int stoCount = stoCodeList.size();
            homeStockVo.setStoreCount(stoCount + "");
            int count = saleDMapper.selectExpiryStoCount(user.getClient(), homeChartRequestDTO.getStoCodeList());
            //int outStockCount = saleDMapper.selectOutStockCountBySto(user.getClient(),  homeChartRequestDTO.getStoCodeList());

            //如果存在配送中心 不论是直营店还是非直营店 都直接显示缺断货
            if("1".equals(isChain)) {
                homeStockVo.setStoShowFlag("1");
            } else {    //如果不存在配送中心  继续判断每家门店是否被推送过  如果有被推送过则显示 否则不显示
                //如果返回的List中有元素 则表示有权限显示，否则没权限显示
                List<String> showFlagList = getShowFlagList(stoCodeList, user);
                if(ObjectUtils.isEmpty(showFlagList)) {
                    homeStockVo.setStoShowFlag("0");
                } else {
                    homeStockVo.setStoShowFlag("1");
                }
            }

            int outStockCount = saleDMapper.selectOutStockCountBySto(user.getClient(), stoCodeList);
            if (new BigDecimal(homeStockVo.getStkCount()).compareTo(BigDecimal.ZERO) == 1) {
                BigDecimal expiryRate = new BigDecimal(count).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                if (expiryRate.compareTo(new BigDecimal(10)) == 1) {
                    homeStockVo.setIsExpiry("1");
                } else {
                    homeStockVo.setIsExpiry("0");
                }
                BigDecimal outStockRate = new BigDecimal(outStockCount).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                if (outStockRate.compareTo(new BigDecimal(5)) == 1) {
                    homeStockVo.setIsOutStock("1");
                } else {
                    homeStockVo.setIsOutStock("0");
                }
            }
            homeStockVo.setExpiryCount(String.valueOf(count));
            homeStockVo.setOutStockCount(String.valueOf(outStockCount));
        } else {
            //公司级 获取所有门店数量
            List<StoreData> storeList = storeDataMapper.getStoreList(userDataDTO.getClient());
            //获取所有直营门店  如果list不为空 则表示有直营店  否则表示没有直营店
            directStoreList = getDirectStoreList(userDataDTO.getClient(), storeList);

            //判断是否展示品类模块（判断加盟商是否包含连锁属性  如果有 展示品类模块  如果没有 不展示品类模块）
            boolean showModel = isShowModel(userDataDTO.getClient());
            homeStockVo.setIsShowModel(!showModel ? "0" : "1");

            homeStockVo.setStoreCount(String.valueOf(directStoreList.size()));   //门店数量
            //如果加盟商没有连锁性质 或者 没有直营店 则不展示品类模块  直接返回
            if(!showModel || ObjectUtils.isEmpty(directStoreList)) {
                homeStockVo.setIsShowModel("0");
                //return ResultUtil.success(homeStockVo);
            }
            //是否存在配送中心  0-没有 1-有
            homeStockVo.setIsChain(isChain);

            //公司库存查询
            HomeStockVo tempHomeStock = saleDMapper.selectClientStock(user.getClient());
            log.info("库存报表回结果:{}", JSONObject.toJSONString(tempHomeStock));
            if(ObjectUtils.isNotEmpty(tempHomeStock)) {
                homeStockVo.setStkCount(tempHomeStock.getStkCount());
                homeStockVo.setStkAmt(tempHomeStock.getStkAmt());
            }

            //效期商品数量
            int count = saleDMapper.selectExpiryProCount(user.getClient());
            homeStockVo.setExpiryCount(String.valueOf(count));

            int outStockCount = 0;    //缺断货数量
            if("0".equals(homeStockVo.getIsChain())) {
                //无配送中心缺断货--商品总数/门店数量
                outStockCount = saleDMapper.selectOutStockCountByClient(user.getClient(), directStoreList);
                homeStockVo.setStoShowFlag("1");
            } else {   //有配送中心缺断货--目前只获取仓库商品数量，后期完善
                //有配送中心时是否显示缺断货按钮  0-不显示 1-显示
                List<GaiaSdOutOfStockPushRecord> clientShowFlagList = getClientShowFlag(user.getClient(), user.getUserId());
                homeStockVo.setStoShowFlag(ObjectUtils.isNotEmpty(clientShowFlagList) ? "1" : "0");

                //有配送中心缺断货数量
                outStockCount = saleDMapper.selectDcOutStockCountByClient(user.getClient());
            }
            if (new BigDecimal(homeStockVo.getStkCount()).compareTo(BigDecimal.ZERO) == 1) {
                BigDecimal expiryRate = new BigDecimal(count).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                if (expiryRate.compareTo(new BigDecimal(10)) == 1) {
                    homeStockVo.setIsExpiry("1");
                } else {
                    homeStockVo.setIsExpiry("0");
                }
                BigDecimal outStockRate = new BigDecimal(outStockCount).divide(new BigDecimal(homeStockVo.getStkCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
                if (outStockRate.compareTo(new BigDecimal(5)) == 1) {
                    homeStockVo.setIsOutStock("1");
                } else {
                    homeStockVo.setIsOutStock("0");
                }
            }
            homeStockVo.setOutStockCount(String.valueOf(outStockCount));
        }

        //计算周转天数
        LocalDate current = LocalDate.now(); // 当前日
        LocalDate startDate = current.plusDays(-30);//30天前
        LocalDate endDate = current;
        if (homeChartRequestDTO.getType() == 2) {
            startDate = current.plusDays(-31);//30天前
            endDate = current.plusDays(-1);
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(homeStockVo.getStkAmt())) {
            List<String> brIdList = new ArrayList<>();
            //门店级
            if(ObjectUtils.isNotEmpty(stoCodeList)) {
                brIdList.addAll(stoCodeList);
            }
            String resultStartDate = startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String resultEndDate = endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            BigDecimal saleAmt = saleDMapper.getSaleAmtByDateRangeExt(userDataDTO.getClient(), resultStartDate, resultEndDate, brIdList);
            //周转天数
            if(ObjectUtils.isEmpty(saleAmt) || BigDecimal.ZERO.compareTo(saleAmt) == 0) {
                homeStockVo.setTurnoverDays("0");
            } else {
                homeStockVo.setTurnoverDays(new BigDecimal(30).multiply(new BigDecimal(homeStockVo.getStkAmt())).divide(saleAmt, 0, RoundingMode.HALF_UP).toString());
            }
        }
        return ResultUtil.success(homeStockVo);
    }


    /**
     * 判断是否展示品类模块
     * @param client
     */
    private boolean isShowModel(String client) {
        Franchisee clientInfo = franchiseeMapper.getClientInfo(client);
        if(ObjectUtils.isNotEmpty(clientInfo)) {
            //如果公司性质为连锁公司  则按连锁处理  否则不展示app首页品类模块
            String francType2 = clientInfo.getFrancType2();
            if("1".equals(francType2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取所有直营门店
     * @param storeList
     * @return
     */
    private List<String> getDirectStoreList(String client, List<StoreData> storeList) {
        //直营门店
        return storesGroupMapper.getByClientAndStoreAndType(client, "DX0003", storeList);
    }


    /**
     * 判断是否有配送中心 返回 0-没有 1-有
     * @param client
     * @return
     */
    private String getStoChainByClient(String client) {
        //查询不含虚拟仓库的配送中心
        List<Map<String, String>> storeMapList = saleDMapper.findStoChainByClient(client);

        Map<String, String> dcMap = null;
        if(ObjectUtils.isNotEmpty(storeMapList)) {
            for (Map<String,String> storeMap : storeMapList) {
                if(storeMap.containsKey("dcCode")) {
                    dcMap = storeMap; //如果dcCode为空 则表示无配送中心
                    break;
                }
            }
        }
        //如果配送中心map为空 则表示无配送中心
        if(ObjectUtils.isEmpty(dcMap)) {
            return "0";
        }
        //如果配送中心map不为空 则继续判断配送范围是否包含A范围
        String dcSppsfw = dcMap.get("dcSppsfw");   //商品配送范围
        if(ObjectUtils.isEmpty(dcSppsfw)) {        //如果为空 则表示无配送中心
            return "0";
        }
        //如果商品配送范围包含A 则表示有配送中心
        if(dcSppsfw.contains("A")) {
            return "1";
        }
        return "0";
    }


    /**
     * 获取该登陆用户是否有显示公司级缺断货按钮权限
     * @param client
     * @param receiveEmp
     * @return
     */
    private List<GaiaSdOutOfStockPushRecord> getClientShowFlag(String client, String receiveEmp) {
        GaiaSdOutOfStockPushRecord outOfStockPushRecord = new GaiaSdOutOfStockPushRecord();
        outOfStockPushRecord.setClient(client);
        outOfStockPushRecord.setReceiveEmp(receiveEmp);
        return outOfStockPushRecordMapper.getListByCondition(outOfStockPushRecord);
    }

    @Override
    public Result getPaymentMethodList(HomeChartRequestDTO homeChartRequestDTO) {
        TokenUser user = commonService.getLoginInfo();

        LocalDate current = LocalDate.now(); // 当前日
        int monthDay = current.getDayOfMonth();
        LocalDate startDate = null;
        LocalDate endDate = null;
        List<PaymentMethodVo> outData = new ArrayList<>();

        String stoQueryWrapper = commonService.queryWapperAuto(homeChartRequestDTO.getStoCodeList());
        log.info("报表门店查询: " + stoQueryWrapper);

        if (StringUtils.isEmpty(stoQueryWrapper)&&homeChartRequestDTO.getStoCodeList().isEmpty()) {
            return ResultUtil.error("200","暂无权限");
        }

        if (homeChartRequestDTO.getType() == 2) {//	进入页面默认公司下所有门店销售汇总；查询期间默认“日”，为登录日期前一天日期
            startDate = current.plusDays(-1);
        } else if (homeChartRequestDTO.getType() == 6) {//	，为当前月1号到登录日期前一天日期
            if (monthDay > 1) { //如果今天是一号 就去上个月一整个月
                startDate = current.with(TemporalAdjusters.firstDayOfMonth());
            } else {
                startDate = current.plusDays(-1).with(TemporalAdjusters.firstDayOfMonth());
            }
        } else if (homeChartRequestDTO.getType() == 5) {//	进入页面默认公司下所有门店销售汇总；查询期间默认“日”，为登录日期前一天日期
            if (monthDay > 1) {//如果今天是一号 就去上个月一整年
                startDate = current.with(TemporalAdjusters.firstDayOfYear());

            } else {
                startDate = current.plusYears(-1).with(TemporalAdjusters.firstDayOfYear());
            }
        }
        endDate = current.plusDays(-1);
        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" PAY_MSG.CLIENT as CLIENT, ")
//                .append(" PAY_MSG.GSSPM_BR_ID as BR_ID, ")
                .append(" GAIA_STORE_DATA.STO_NAME as STO_NAME, ")//  --  1000 现金 2000 微信 3000 支付宝  4000医保  其余ID为”其它“
                .append(" PAY_MSG.GSSPM_ID as ID, ") //	默认显示5种支付方式“现金”、“医保”、“支付宝”、“微信”、“其他”  ,	“其他”支付方式金额=销售额-(现金+医保+支付宝+微信)
                .append(" PAY_MSG.GSSPM_NAME as NAME, ");
//                .append(" -- ,GCD_MONTH ,GCD_WEEK    -- 月 ，周  按此字段GROUP BY\n ")
        if (homeChartRequestDTO.getType() == 6) { //	，为当前月1号到登录日期前一天日期
            builder.append(" GCD_MONTH, ");
        }
        if (homeChartRequestDTO.getType() == 5) { //	//如果今天是一号 就去上个月一整年
            builder.append(" GCD_YEAR, ");
        }
        builder.append(" SUM(GSSPM_AMT) as AMT ")
                .append(" FROM YAOUD.V_GAIA_SD_SALE_PAY_MSG as PAY_MSG ")
                .append(" INNER JOIN YAOUD.GAIA_CAL_DT  ")
                .append(" ON PAY_MSG.GSSPM_DATE = GAIA_CAL_DT.GCD_DATE ")
                .append(" INNER JOIN YAOUD.GAIA_STORE_DATA ")
                .append(" ON PAY_MSG.CLIENT = GAIA_STORE_DATA.CLIENT AND PAY_MSG.GSSPM_BR_ID = GAIA_STORE_DATA.STO_CODE  ")
                .append(" WHERE 1=1 ")
                .append(" AND GSSPM_DATE >= '" + startDate + "' ")
                .append(" AND GSSPM_DATE <= '" + endDate + "' ")
                .append(" AND PAY_MSG.CLIENT='" + user.getClient() + "' ");

        //门店维度的查询
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" AND PAY_MSG.GSSPM_BR_ID='" + homeChartRequestDTO.getStoreId() + "' ");
        }

            builder.append(" AND PAY_MSG.GSSPM_BR_ID ");
            builder.append(stoQueryWrapper);



        builder.append(" GROUP BY PAY_MSG.CLIENT,PAY_MSG.GSSPM_BR_ID,STO_NAME,GSSPM_ID ,GSSPM_NAME  ");
        if (homeChartRequestDTO.getType() == 6) { //	，为当前月1号到登录日期前一天日期
            builder.append(" ,GCD_MONTH ");
        }
        if (homeChartRequestDTO.getType() == 5) { //	//如果今天是一号 就去上个月一整年
            builder.append(" ,GCD_YEAR ");
        }
        builder.append(" ORDER BY SUM(GSSPM_AMT)");
        log.info("付款方式数据：{}", builder.toString());
        List<PaymentMethodVo> paymentMethodOut = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(PaymentMethodVo.class));
        log.info("付款方式数据返回结果:{}", JSONObject.toJSONString(paymentMethodOut));
        List<Map<String,String>> ids = getPaymentIds(user.getClient());
        if (ObjectUtils.isNotEmpty(paymentMethodOut)) {
            Map<String, String> map = new HashMap<>();
            map.put("client", user.getClient());
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
                map.put("depId", homeChartRequestDTO.getStoreId());
            }
            if (StringUtils.isNotBlank(stoQueryWrapper)) {
                map.put("stoQueryWrapper", stoQueryWrapper);
            }
            map.put("startDate", startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            map.put("endDate", endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            String saleDAmt = getSales(map);
            // “其他”支付方式金额=销售额-(现金+医保+支付宝+微信)
            BigDecimal otherPrice = BigDecimal.ZERO;
            for (Map<String,String> id : ids) {
                PaymentMethodVo other = new PaymentMethodVo();
                other.setId(id.get("GSPM_ID"));
                other.setName(id.get("GSPM_NAME"));
                BigDecimal payPrice = BigDecimal.ZERO;

                for (PaymentMethodVo item : paymentMethodOut) {
                    if (id.get("GSPM_ID").equals(item.getId())) {
                        payPrice = new BigDecimal(item.getAmt()).add(payPrice);
                    }
                }

                other.setAmt(String.valueOf(payPrice));
                otherPrice = payPrice.add(otherPrice);
                outData.add(other);
            }
    /*        PaymentMethodVo other = new PaymentMethodVo();
            other.setId("other");
            other.setName("其他");
            other.setAmt(String.valueOf(new BigDecimal(saleDAmt).subtract(otherPrice)));
            other.setColor(CommonEnum.PaymentMethod.getByCode("other").getColor());
            outData.add(other);*/
        } else {

            for (Map<String, String> id : ids) {
                PaymentMethodVo other = new PaymentMethodVo();
                other.setId(id.get("GSPM_ID"));
                other.setName(id.get("GSPM_NAME"));
                other.setAmt("0");
                outData.add(other);
            }

        }
        log.info("付款方式数据返回结果:{}", JSONObject.toJSONString(outData));
        Collections.sort(outData, new Comparator<PaymentMethodVo>() {
            @Override
            public int compare(PaymentMethodVo o1, PaymentMethodVo o2) {
                BigDecimal subtract = new BigDecimal(o1.getAmt()).subtract(new BigDecimal(o2.getAmt()));
                return BigDecimal.ZERO.compareTo(subtract);
            }
        });
        return ResultUtil.success(outData);
    }
    private List<Map<String,String>> getPaymentIds(String clientId) {
        return paymentMapper.getPaymentIds(clientId);
    }

    /**
     * 查询GAIA_SD_SALE_D销售额
     *
     * @param map
     * @return
     */
    private String getSales(Map<String, String> map) {
        StringBuilder builder = new StringBuilder()
                .append(" SELECT sum(GSSD_AMT) as GSSD_AMT ")
                .append(" FROM GAIA_SD_SALE_D  ")
                .append(" where client= '")
                .append(map.get("client"))
                .append("' ")
                .append("  and gssd_date>= '")
                .append(map.get("startDate"))
                .append("' ")
                .append("  and gssd_date<= '")
                .append(map.get("endDate"))
                .append("' ");
        if (map.containsKey("depId")) {
            builder.append("  and GSSD_BR_ID= '")
                    .append(map.get("depId"))
                    .append("' ");
        }

        if (map.containsKey("stoQueryWrapper")) {
            builder.append("  and GSSD_BR_ID ")
                    .append(map.get("stoQueryWrapper"));
        }
        log.info("用户营业实收金额数据：{}", builder.toString());
        Map<String, Object> objectMap = kylinJdbcTemplate.queryForMap(builder.toString());
        log.info("用户营业实收金额返回结果:{}", JSONObject.toJSONString(objectMap));
        if (!objectMap.isEmpty()) {
            return String.valueOf(objectMap.get("GSSD_AMT"));
        }
        return null;
    }

    private String getSaleCost(Map<String, String> map) {
        StringBuilder builder = new StringBuilder()
                .append(" SELECT sum(GSSD_MOV_PRICE) as SALE_COST ")
                .append(" FROM GAIA_SD_SALE_D  ")
                .append(" where client= '")
                .append(map.get("client"))
                .append("' ")
                .append("  and gssd_date>= '")
                .append(map.get("startDate"))
                .append("' ")
                .append("  and gssd_date<= '")
                .append(map.get("endDate"))
                .append("' ");
        if (map.containsKey("depId")) {
            builder.append("  and GSSD_BR_ID= '")
                    .append(map.get("depId"))
                    .append("' ");
        }
        if (map.containsKey("depIds")) {
            builder.append("  and GSSD_BR_ID IN (")
                    .append(map.get("depIds"))
                    .append(" )");
        }
        log.info("库存报表门店销售值数据：{}", builder.toString());
        Map<String, Object> objectMap = kylinJdbcTemplate.queryForMap(builder.toString());
        log.info("库存报表门店销售值返回结果:{}", JSONObject.toJSONString(objectMap));
        if (!objectMap.isEmpty()) {
            if (objectMap.get("SALE_COST") != null) {
                return objectMap.get("SALE_COST").toString();
            }
        }
        return null;
    }


    public LoginSalePlan getLoginSalePlanByMonth(HomeChartRequestDTO homeChartRequestDTO) {
        LoginSalePlan data = new LoginSalePlan();

//        String client = "10000001";
//        String startData = "2020-01-01";
//        String endData = "2020-12-31";
        int day = 0;//获取当前月的自然天数
        int day1 = 0;//获取当前日期实际天数
        String startData = DateUtils.getMonthStartData();
        String endData = DateUtils.getMonthEndData();
        if (homeChartRequestDTO.getType() == 2) {
            Date today = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 24);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            endData = simpleDateFormat.format(today);//获取昨天日期
            day1 = Integer.valueOf(endData.substring(8));
            try {
                day = getDaysOfMonth(simpleDateFormat.parse(endData));
            } catch (ParseException e) {
                e.getMessage();
            }
            String dateStr = endData.substring(0, 8);
            startData = dateStr + "01";
        } else if (homeChartRequestDTO.getType() == 1) {
            Date today = new Date(System.currentTimeMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            endData = simpleDateFormat.format(today);//获取今天日期
            day1 = Integer.valueOf(endData.substring(9));
            try {
                day = getDaysOfMonth(simpleDateFormat.parse(endData));
            } catch (ParseException e) {
                e.getMessage();
            }
        }

//        TokenUser user = commonService.getLoginInfo();
        SalePlan plan = new SalePlan();//计划数据
        EffectiveSale effectiveSale = new EffectiveSale();
        StringBuilder builder = new StringBuilder()
                .append(" select a.client, ");
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" a.gssd_br_id as \"brId\", ");
        }
        builder.append(" a.gcd_month as \"month\",")
                .append(" max(a.gssd_days) as \"day\",sum( a.GSSD_AMT ) AS \"aAmt\",sum( a.GSSD_GROSS_AMT ) AS \"aGross\" ")
//                .append(" b.t_amt as \"tAmt\" , ")
//                .append(" b.t_gross as \"tGross\", ")
//                .append(" b.t_mcard_qty as \"tMcardQty\" ")
                .append(" from ")
                /**  START
                 *第一张表开始 a,
                 */
                .append(" ( select ")
                .append(" GAIA_SD_SALE_D.client as client, ")//客户编号/加盟商编号
                .append(" D.GCD_YEAR||D.GCD_MONTH as gcd_month,");// 销售日期(年周)
        builder.append(" count(distinct GSSD_DATE) as GSSD_DAYS,")//销售天数
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")// 销售额
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT ")//毛利额
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_STORE_DATA as S ")
                .append(" ON GAIA_SD_SALE_D.CLIENT = S.CLIENT AND GAIA_SD_SALE_D.GSSD_BR_ID = S.STO_CODE ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ")
                .append(" and gssd_date BETWEEN '" + startData + "' AND '" + endData + "' ")
                .append(" and GAIA_SD_SALE_D.client='" + homeChartRequestDTO.getClientId() + "' ");
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID='" + homeChartRequestDTO.getStoreId() + "' ");
        }
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList())){
            builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID in (");
            for (int i = 0; i < homeChartRequestDTO.getStoCodeList().size(); i++){
                if (i == homeChartRequestDTO.getStoCodeList().size()-1){
                    builder.append("'" +homeChartRequestDTO.getStoCodeList().get(i)+ "'");
                }else {
                    builder.append("'" +homeChartRequestDTO.getStoCodeList().get(i)+ "',");
                }
            }
            builder.append(" ) ");
        }
        builder.append(" group by GAIA_SD_SALE_D.client ");
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            builder.append(" ,GAIA_SD_SALE_D.GSSD_BR_ID ");
        }
        builder.append(" ,D.GCD_YEAR||D.GCD_MONTH ")
                .append(" order by D.GCD_YEAR||D.GCD_MONTH asc ) as a ")
                /**END
                 *第一张表结束 a
                 */
                /**  START
                 *第二张表开始 b
                 */
//                .append(" LEFT JOIN (select ")
//                .append(" client,")
//                .append(" month_plan, sto_code,");
//        builder.append(" sum(T_SALE_AMT) as t_amt, ")
//                .append(" sum(T_SALE_GROSS) as t_gross, ")
//                .append(" sum(T_MCARD_QTY) as t_mcard_qty ")
//                .append("  from GAIA_SALETASK_DPLAN ")
//                .append(" group by client,sto_code,month_plan ");
//        builder.append(" ) as b")

                /**END
                 *第二张表结束 a
                 */
//                .append(" on a.client = b.client AND a.GSSD_BR_ID = b.sto_code and a.gcd_month = b.month_plan ")
//                .append(" GROUP BY a.client, a.gcd_month,a.gssd_days,b.t_amt,b.t_gross,b.t_mcard_qty");
                .append(" GROUP BY a.client, a.gcd_month,a.gssd_days");
//        System.out.println(builder.toString());
        log.info("销售计划-月计划数据查询：{}", builder.toString());
        List<SalePlan> salePlans = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalePlan.class));
        if (ObjectUtils.isNotEmpty(salePlans)) {
            System.out.println(salePlans.get(0).toString());
            plan = salePlans.get(0);
            data.setMonth(plan.getMonth());
            data.setDay(plan.getDay());
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(plan.gettAmt())) {
                data.settAmt(new BigDecimal(plan.gettAmt()));
            }
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(plan.gettGross())) {
                data.settGross(new BigDecimal(plan.gettGross()));
            }
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(plan.gettMcardQty())) {
                data.settMcardQty(new BigDecimal(plan.gettMcardQty()));
            }
            data.setaAmt(new BigDecimal(plan.getaAmt()));
            data.setaGross(new BigDecimal(plan.getaGross()));
        }
        Map map = new HashMap();
        map.put("client", homeChartRequestDTO.getClientId());
        map.put("depId", homeChartRequestDTO.getStoreId());
        map.put("stoCodeList",homeChartRequestDTO.getStoCodeList());
        map.put("startData", startData);
        map.put("endData", endData);
//        effectiveSale = saleDService.selectSalePlan(map);

        StringBuilder salePlanBuilder = new StringBuilder();
        salePlanBuilder.append(" SELECT ");
        salePlanBuilder.append(" a.CLIENT,");
        salePlanBuilder.append(" SUM(a.GSSD_AMT) amt, ");
        salePlanBuilder.append(" SUM(a.GSSD_AMT - NULLIF( a.GSSD_MOV_PRICE, 0 )) gross, ");
        salePlanBuilder.append(" SUM(a.noQty) noQty,");
        salePlanBuilder.append(" SUM( NULLIF( b.aMcardQty, 0 )) cardQty ");
        salePlanBuilder.append(" FROM ");
        salePlanBuilder.append(" (");
        salePlanBuilder.append(" SELECT ");
        salePlanBuilder.append(" CLIENT,GSSD_BR_ID, ");
        salePlanBuilder.append(" round( sum( GSSD_AMT ), 2 ) GSSD_AMT,");
        salePlanBuilder.append(" round( sum( GSSD_QTY ), 2 ) GSSD_QTY,");
        salePlanBuilder.append(" round( sum( GSSD_MOV_PRICE ), 2 ) GSSD_MOV_PRICE,");
        salePlanBuilder.append(" count( DISTINCT GSSD_BILL_NO ) noQty ");
        salePlanBuilder.append(" FROM ");
        salePlanBuilder.append(" GAIA_SD_SALE_D ");
        salePlanBuilder.append(" WHERE");
        salePlanBuilder.append(" CLIENT = '" + homeChartRequestDTO.getClientId() + "' ");
        if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            salePlanBuilder.append(" 	AND GSSD_BR_ID = '" + homeChartRequestDTO.getStoreId() + "'");
        }
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList())) {
            salePlanBuilder.append(" and GSSD_BR_ID in (");
            for (int i = 0; i < homeChartRequestDTO.getStoCodeList().size(); i++) {
                if (i == homeChartRequestDTO.getStoCodeList().size() - 1) {
                    salePlanBuilder.append("'" + homeChartRequestDTO.getStoCodeList().get(i) + "'");
                } else {
                    salePlanBuilder.append("'" + homeChartRequestDTO.getStoCodeList().get(i) + "',");
                }
            }
            salePlanBuilder.append(" ) ");
        }
        salePlanBuilder.append("");
        salePlanBuilder.append(" AND GSSD_DATE BETWEEN '" + startData + "' AND '" + endData + "' ");
        salePlanBuilder.append(" GROUP BY CLIENT,GSSD_BR_ID ");
        salePlanBuilder.append(" ) a");
        salePlanBuilder.append(" LEFT JOIN (");
        salePlanBuilder.append(" SELECT CLIENT, GSMBC_BR_ID, count( GSMBC_CARD_ID ) aMcardQty ");
        salePlanBuilder.append(" FROM GAIA_SD_MEMBER_CARD WHERE CLIENT = '" + homeChartRequestDTO.getClientId() + "'");
        if (StringUtils.isNotEmpty(homeChartRequestDTO.getStoreId())) {
            salePlanBuilder.append(" 	AND GSMBC_BR_ID = '" + homeChartRequestDTO.getStoreId() + "' ");
        }
        if (ObjectUtils.isNotEmpty(homeChartRequestDTO.getStoCodeList())) {
            salePlanBuilder.append(" 	AND GSMBC_BR_ID IN (");
            for (int i = 0; i < homeChartRequestDTO.getStoCodeList().size(); i++) {
                if (i == homeChartRequestDTO.getStoCodeList().size() - 1) {
                    salePlanBuilder.append("'" + homeChartRequestDTO.getStoCodeList().get(i) + "'");
                } else {
                    salePlanBuilder.append("'" + homeChartRequestDTO.getStoCodeList().get(i) + "',");
                }
            }
            salePlanBuilder.append(" ) ");
        }
        salePlanBuilder.append(" AND GSMBC_CREATE_DATE BETWEEN '" + startData + "' AND '" + endData + "' ");
        salePlanBuilder.append(" group by CLIENT, GSMBC_BR_ID ");
        salePlanBuilder.append(" ) b");
        salePlanBuilder.append(" ON a.CLIENT = b.CLIENT AND a.GSSD_BR_ID = b.GSMBC_BR_ID");
        salePlanBuilder.append(" GROUP BY a.CLIENT");

        log.info("销售计划-达成率数据查询：{}", salePlanBuilder.toString());
        List<EffectiveSale> effectiveSales = kylinJdbcTemplate.query(salePlanBuilder.toString(), RowMapper.getDefault(EffectiveSale.class));

        if (ObjectUtils.isNotEmpty(effectiveSales)) {
            effectiveSale = effectiveSales.get(0);
            log.info("销售计划-达成率数据结果：{}", JSONObject.toJSONString(effectiveSale));

            //实际数据
            if (ObjectUtils.isNotEmpty(effectiveSale.getCardQty())) {
                data.setaMcardQty(new BigDecimal(effectiveSale.getCardQty()));
            } else {
                data.setaMcardQty(BigDecimal.ZERO);
            }
            //百分比
            //            data.setrAmt((data.getaAmt().divide(data.gettAmt()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
            //            data.setrGross((data.getaGross().divide(data.gettGross()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
            //            data.setrMcardQty((data.getaMcardQty().divide(data.getaMcardQty()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");

            //            data.setrAmt( DecimalUtils.toPercentStr(data.getaAmt().divide(data.gettAmt(),2,BigDecimal.ROUND_DOWN)));
            //            data.setrGross(DecimalUtils.toPercentStr(data.getaGross().divide(data.gettGross(),2,BigDecimal.ROUND_DOWN)));
            //            data.setrMcardQty(DecimalUtils.toPercentStr(data.getaMcardQty().divide(data.getaMcardQty(),2,BigDecimal.ROUND_DOWN)));
            //日均达成率
            if (ObjectUtils.isNotEmpty(data.gettAmt())) {
                data.setrAmt(DecimalUtils.toPercentStr((data.getaAmt().divide(new BigDecimal(day1), 4, BigDecimal.ROUND_DOWN)).divide(data.gettAmt().divide(new BigDecimal(day), 4, BigDecimal.ROUND_DOWN), 2, BigDecimal.ROUND_DOWN)));
            } else {
                data.setrAmt("0.00");
            }
            if (ObjectUtils.isNotEmpty(data.gettGross()) && data.gettGross().compareTo(BigDecimal.ZERO) != 0) {
                data.setrGross(DecimalUtils.toPercentStr(data.getaGross().divide(new BigDecimal(day1), 4, BigDecimal.ROUND_DOWN).divide(data.gettGross().divide(new BigDecimal(day), 4, BigDecimal.ROUND_DOWN), 2, BigDecimal.ROUND_DOWN)));
            } else {
                data.setrGross("0.00");
            }
            if (ObjectUtils.isNotEmpty(data.gettMcardQty()) && data.gettMcardQty().compareTo(BigDecimal.ZERO) != 0) {
                data.setrMcardQty(DecimalUtils.toPercentStr(data.getaMcardQty().divide(new BigDecimal(day1), 4, BigDecimal.ROUND_DOWN).divide(data.gettMcardQty().divide(new BigDecimal(day), 4, BigDecimal.ROUND_DOWN), 2, BigDecimal.ROUND_DOWN)));
            } else {
                data.setrMcardQty("0.00");
            }


            //订单数量
            data.setNoQty(effectiveSale.getNoQty());
            if (ObjectUtils.isEmpty(plan.getDay())) {
                plan.setDay("0");
            }
            //日均交易
            data.setDayQty(String.valueOf(new BigDecimal(effectiveSale.getNoQty()).multiply(new BigDecimal(plan.getDay())).setScale(2, BigDecimal.ROUND_HALF_UP)));
            //客单价
            data.setPct(String.valueOf(new BigDecimal(effectiveSale.getAmt()).multiply(new BigDecimal(effectiveSale.getNoQty())).setScale(4, BigDecimal.ROUND_HALF_UP)));
            //        }
            //        if(StringUtils.isNotEmpty(depId)){
            //            List<ShopAssistantOutData> shopAssistants = this.getShopAssistantSalePlan(map);
            //            data.setShopAssistants(shopAssistants);
        }
        return data;
    }

    /**
     * 今日数据集
     *
     * @param homeChartRequestDTO
     * @param stoQueryWrapper
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByCurrentNew(HomeChartRequestDTO homeChartRequestDTO, String stoQueryWrapper) {

        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + homeChartRequestDTO.getClientId() + "' ");
        builder.append(" and GSSD_BR_ID ");
        builder.append(stoQueryWrapper);
        builder.append(" and gssd_date = '" + DateUtils.getCurrentDateStr() + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表今日数据：{}", builder.toString());
        List<HomeSalesVo> homeSalesVo = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(homeSalesVo));

        return homeSalesVo;
    }

    /**
     * @Author jiht
     * @Description 当日会员卡数量查询
     * @Date 2022/2/24 15:23
     * @Param [stoCodeList, client, dateTime]
     * @Return int
     **/
    private int selectCardCountByKylin(List<String> stoCodeList, String client, String dateTime) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT count(1) AS CARD_COUNT FROM GAIA_SD_MEMBER_CARD WHERE CLIENT = '" + client + "'");
        builder.append("and GSMBC_CREATE_DATE = '"+DateUtils.formatLocalDate(DateUtils.parseDate(dateTime),"yyyy-MM-dd")+"'");

        if (ObjectUtils.isNotEmpty(stoCodeList)){
            builder.append(" AND GSMBC_BR_ID IN (");
            for (int i = 0; i < stoCodeList.size(); i++){
                if (i == stoCodeList.size()-1){
                    builder.append("'" +stoCodeList.get(i)+ "'");
                }else {
                    builder.append("'" +stoCodeList.get(i)+ "',");
                }
            }
            builder.append(" ) ");
        }

        log.info("会员卡今日数据：{}", builder.toString());
        Map<String, Object> objectMap = kylinJdbcTemplate.queryForMap(builder.toString());
        log.info("会员卡今日数据返回结果:{}", JSONObject.toJSONString(objectMap));
        if (!objectMap.isEmpty()) {
            Number num = (Number)objectMap.get("CARD_COUNT");
            return num.intValue();
        }

        return 0;
    }

    /**
     * 昨日数据集
     *
     * @param homeChartRequestDTO
     * @param stoQueryWrapper
     * @return
     */
    private List<HomeSalesVo> getHomeSalesListByYesterdayNew(HomeChartRequestDTO homeChartRequestDTO, String stoQueryWrapper) {
        StringBuilder builder = new StringBuilder()
                .append(" SELECT ")
                .append(" client, ")
                .append(" GSSD_DATE, ")
                .append(" count(distinct GSSD_BR_ID) as gssd_store_count, ")
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT, ")
                .append(" round((CASE WHEN SUM(GSSD_AMT) > 0 THEN (SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE))/SUM(GSSD_AMT)*100  WHEN SUM(GSSD_AMT)<=0 THEN 0 end ),2) as  GSSD_GROSS_MAR, ")
                .append(" ROUND(CASE WHEN COUNT(distinct GSSD_DATE) = 0 THEN 0 ELSE (COUNT(distinct gssd_bill_no)*1.0)/(COUNT(distinct GSSD_DATE)*1.0) END, 2) as GSSD_TRANS, ")
                .append("  round((CASE WHEN COUNT(distinct gssd_bill_no)   >0 THEN SUM(gssd_amt)/COUNT(distinct gssd_bill_no) WHEN COUNT(distinct gssd_bill_no)<=0 THEN 0 end ),2) as GSSD_APRICE, ")
                .append("  round((CASE WHEN COUNT(distinct GSSD_DATE)   >0 THEN  SUM(GSSD_AMT)/COUNT(distinct GSSD_DATE) WHEN COUNT(distinct GSSD_DATE)<=0 THEN 0 end),2) as GSSD_AMT_AVG ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" where 1=1 ")
                .append(" and client = '" + homeChartRequestDTO.getClientId() + "' ");
        builder.append(" and GSSD_BR_ID ").append(stoQueryWrapper);

        builder.append(" and gssd_date = '" + LocalDate.now().plusDays(-1).format(DateUtils.DATE_FORMATTER) + "' ")
                .append(" group by client,gssd_date ");
        log.info("报表昨日数据：{}", builder.toString());
        List<HomeSalesVo> homeSalesVo = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(HomeSalesVo.class));
        log.info("返回结果:{}", JSONObject.toJSONString(homeSalesVo));

        return homeSalesVo;
    }

    public HomePushMoneyVo getMonthPushMoney(HomePushMoneyDTO homePushMoneyDTO) {
        String startDate = "";
        String endDate = "";
        if ("1".equals(homePushMoneyDTO.getType())) { //1 是今日
            endDate = DateUtils.getCurrentDateStrYYMMDD();
        } else if ("2".equals(homePushMoneyDTO.getType())) {
            endDate = DateUtils.getBeforeDateStrYYMMDD();
        }
        startDate = DateUtils.getMonthStartDataStr();

        homePushMoneyDTO.setStartDate(startDate);
        homePushMoneyDTO.setEndDate(endDate);
        homePushMoneyDTO.setSuitMonth(DateUtils.getCurrentYearMonthStr());

        HomePushMoneyVo pushMoneySale = this.saleDMapper.selectMonthSalesBySales(homePushMoneyDTO);
        HomePushMoneyVo pushMoneyPro = this.saleDMapper.selectMonthSalesByPro(homePushMoneyDTO);
        BigDecimal deductionWagePro = BigDecimal.ZERO;
        if (ObjectUtils.isNotEmpty(pushMoneyPro)) {
            deductionWagePro = DecimalUtils.stripTrailingZeros(pushMoneyPro.getDeductionWagePro());
        }

        if (ObjectUtils.isNotEmpty(pushMoneySale)) {
            pushMoneySale.setDeductionWagePro(deductionWagePro);
            pushMoneySale.setDeductionWage(DecimalUtils.stripTrailingZeros(pushMoneySale.getDeductionWageSales()).
                    add(deductionWagePro));
        }

        return pushMoneySale;
    }

    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    @Override
    public Result getZzOrgListByUserAuth() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = storeDataMapper.getZzOrgListByUserAuth(user.getClient(), user.getUserId());
        return ResultUtil.success(list);
    }

    /**
     * 获取有显示权限的门店
     * @param stoCodeList
     * @param user
     * @return
     */
    private List<String> getShowFlagList(List<String> stoCodeList, TokenUser user) {
        //获取门店数量
        int stoCount = stoCodeList.size();
        //有显示权限的门店List
        List<String> stoList = new ArrayList<>();
        //门店数量是否为1家
        if(stoCount > 1) {
            for(String stoCode : stoCodeList) {
                //遍历查看每家门店是否有显示权限
                String showFlag = saleDMapper.selectAuthByClientAndStoCode(user.getClient(), stoCode, "APP_STORE_SHOW_OUT_STOCK_FLAG");
                //如果被推送过则直接显示  否则不显示
                if("1".equals(showFlag)) {
                    stoList.add(stoCode);
                }
            }
        } else if(stoCount == 1) {
            List<StoreData> list = new ArrayList<>();
            StoreData storeData = new StoreData();
            storeData.setStoCode(stoCodeList.get(0));
            list.add(storeData);
            //查询是否为直营店
            List<String> directStoreList = getDirectStoreList(user.getClient(), list);
            //查询是否直营 如果不是直营店 则直接显示
            if(ObjectUtils.isEmpty(directStoreList)) {
                stoList.add(stoCodeList.get(0));
            } else {
                //如果是直营 则判断是否有权限显示
                String showFlag = saleDMapper.selectAuthByClientAndStoCode(user.getClient(), stoCodeList.get(0), "APP_STORE_SHOW_OUT_STOCK_FLAG");
                if("1".equals(showFlag)) {
                    stoList.add(stoCodeList.get(0));
                }
            }
        }
        return stoList;
    }
}
