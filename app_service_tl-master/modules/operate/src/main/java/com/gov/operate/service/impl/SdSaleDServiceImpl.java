package com.gov.operate.service.impl;

import com.gov.operate.dto.EffectiveSale;
import com.gov.operate.dto.MemberCard;
import com.gov.operate.dto.ShopAssistant;
import com.gov.operate.entity.SdSaleD;
import com.gov.operate.mapper.SdSaleDMapper;
import com.gov.operate.service.ISdSaleDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 门店销售明细表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-13
 */
@Service
public class SdSaleDServiceImpl extends ServiceImpl<SdSaleDMapper, SdSaleD> implements ISdSaleDService {

    @Resource
    private SdSaleDMapper sdSaleDMapper;

    @Override
    public EffectiveSale selectSalePlan(Map map ) {
        return sdSaleDMapper.selectSalePlan( map );
    }

    @Override
    public List<MemberCard> selectCardByUser(Map map) {
        return sdSaleDMapper.selectCardByUser( map );
    }
}
