package com.gov.operate.service.impl;

import com.gov.operate.entity.AuthorityAuthsClient;
import com.gov.operate.mapper.AuthorityAuthsClientMapper;
import com.gov.operate.service.IAuthorityAuthsClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 加盟商权限表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
@Service
public class AuthorityAuthsClientServiceImpl extends ServiceImpl<AuthorityAuthsClientMapper, AuthorityAuthsClient> implements IAuthorityAuthsClientService {

}
