package com.gov.operate.mapper;

import com.gov.operate.dto.compadmWms.CompadmWmsDto;
import com.gov.operate.entity.CompadmWms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
public interface CompadmWmsMapper extends BaseMapper<CompadmWms> {

    Integer getMaxNumCode(@Param("client") String client);

//    CompadmWms getCompadmWmsByClientAndCode(String client, String stoCode);


    List<CompadmWmsDto> select(String client);

    List<String> selectListForAuth(String client);
}
