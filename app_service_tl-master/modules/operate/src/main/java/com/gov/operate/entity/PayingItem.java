package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYING_ITEM")
@ApiModel(value="PayingItem对象", description="")
public class PayingItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款订单号")
    @TableField("FICO_ID")
    private String ficoId;

    @ApiModelProperty(value = "付款主体编码")
    @TableField("FICO_PAYINGSTORE_CODE")
    private String ficoPayingstoreCode;

    @ApiModelProperty(value = "组织")
    @TableField("FICO_COMPANY_CODE")
    private String ficoCompanyCode;

    @ApiModelProperty(value = "付款主体名称")
    @TableField("FICO_PAYINGSTORE_NAME")
    private String ficoPayingstoreName;

    @ApiModelProperty(value = "付款方式")
    @TableField("FICO_PAYMENT_METHOD")
    private String ficoPaymentMethod;

    @ApiModelProperty(value = "付款金额")
    @TableField("FICO_PAYMENT_AMOUNT")
    private BigDecimal ficoPaymentAmount;

    @ApiModelProperty(value = "上期截止日期")
    @TableField("FICO_LAST_DEADLINE")
    private String ficoLastDeadline;

    @ApiModelProperty(value = "本期开始日期")
    @TableField("FICO_STARTING_TIME")
    private String ficoStartingTime;

    @ApiModelProperty(value = "发票回调成功PDF地址")
    @TableField("FICO_PDF_URL")
    private String ficoPdfUrl;

    @ApiModelProperty(value = "本期截止日期")
    @TableField("FICO_ENDING_TIME")
    private String ficoEndingTime;

    @ApiModelProperty(value = "发票请求流水号")
    @TableField("FICO_FPQQLSH")
    private String ficoFpqqlsh;

    @ApiModelProperty(value = "发票号码")
    @TableField("FICO_FPHM")
    private String ficoFphm;

    @ApiModelProperty(value = "发票代码")
    @TableField("FICO_FPDM")
    private String ficoFpdm;

    @ApiModelProperty(value = "发票日期")
    @TableField("FICO_FPRQ")
    private String ficoFprq;


}
