package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.dto.GetProListVO;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SalaryObj;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SalaryObjMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IProductBusinessService;
import com.gov.operate.service.ISalaryObjService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryObjServiceImpl extends ServiceImpl<SalaryObjMapper, SalaryObj> implements ISalaryObjService {

    @Resource
    private SalaryObjMapper salaryObjMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private IProductBusinessService productBusinessService;

    /**
     * 方案适用范围
     */
    @Override
    public void saveApplyObj(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除该方案综合达成率 原始数据
        this.remove(new QueryWrapper<SalaryObj>().eq("GSO_GSP_ID", gspId));
        List<SalaryObj> applyObjList = vo.getApplyObjList();
        if (CollectionUtils.isEmpty(applyObjList)) {
            throw new CustomResultException("适用方案列表 不能为空");
        }
        for (int i = 0; i < applyObjList.size(); i++) {
            SalaryObj salaryObj = applyObjList.get(i);
            if (StringUtils.isEmpty(salaryObj.getGsoApplyObj())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条范围编码不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryObj.getGsoApplyObjName())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条范围名称不能为空", i + 1));
            }
            // 方案主键
            salaryObj.setGsoGspId(gspId);
        }
        this.saveBatch(applyObjList);
    }

    /**
     * 跟据方案主键获取适用范围
     */
    @Override
    public List<SalaryObj> getApplyObj(Integer gspId) {
        List<SalaryObj> applyObjList = this.list(new QueryWrapper<SalaryObj>().eq("GSO_GSP_ID", gspId));
        return applyObjList;
    }

    /**
     * 适用范围 (门店/连锁公司列表)
     */
    @Override
    public List<SalaryObj> salaryObjList() {
        TokenUser user = commonService.getLoginInfo();
        List<SalaryObj> salaryObjList = salaryObjMapper.salaryObjList(user.getClient());
        return salaryObjList;
    }

    /**
     * 商品列表
     */
    @Override
    public List<ProductBusiness> getProList(GetProListVO vo) {
        if (ObjectUtils.isEmpty(vo.getGspId())) {
            throw new CustomResultException("方案主键不能为空");
        }
        TokenUser user = commonService.getLoginInfo();

        String storeCode = null;
        // 获取任意门店编码
        storeCode = salaryObjMapper.getStoreList(user.getClient(), vo.getGspId());
        // 获取任意连锁门店下任意门店编码
        if (StringUtils.isEmpty(storeCode)) {
            storeCode = salaryObjMapper.getCompadmStoreList(user.getClient(), vo.getGspId());
        }
        // 商品列表
        List<ProductBusiness> list = salaryObjMapper.getBusiness(user.getClient(), storeCode, vo.getProSelfCode());
        return list;
    }
}
