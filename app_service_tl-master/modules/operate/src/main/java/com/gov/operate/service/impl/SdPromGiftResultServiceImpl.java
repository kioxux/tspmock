package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromGiftResult;
import com.gov.operate.mapper.SdPromGiftResultMapper;
import com.gov.operate.service.ISdPromGiftResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-11
 */
@Service
public class SdPromGiftResultServiceImpl extends ServiceImpl<SdPromGiftResultMapper, SdPromGiftResult> implements ISdPromGiftResultService {

}
