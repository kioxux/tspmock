package com.gov.operate.dto.marketing;

import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:50 2021/8/13
 * @Description：商品详情出参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class ProductDetailVO {

    private String productCode;

    private String productName;
}
