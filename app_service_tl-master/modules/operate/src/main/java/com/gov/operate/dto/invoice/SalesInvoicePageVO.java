package com.gov.operate.dto.invoice;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SalesInvoicePageVO {

    private String matType;

    private String client;

    /**
     * 业务机构
     */
    private String matSiteCode;

    private String id;

    public String getId() {
        return this.matId + "_" + this.matYear + "_" + this.matLineNo + "_" + this.matDnId;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String matId;

    private String matYear;

    /**
     * 业务单据号
     */
    private String matDnId;

    /**
     * 行号
     */
    private String matLineNo;

    /**
     * 发生时间
     */
    private String matDocDate;

    /**
     * 客户编码
     */
    private String stoCode;

    /**
     * 客户名称
     */
    private String stoName;

    /**
     * 商品编码
     */
    private String matProCode;

    /**
     * 商品编码
     */
    private String proName;

    /**
     * 税率
     */
    private BigDecimal rate;

    /**
     * 税率名
     */
    private String rateName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 数量
     */
    private BigDecimal matQty;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 金额
     */
    private BigDecimal totalAmount;

    /**
     * 税额
     */
    private BigDecimal addTax;

    /**
     * 税收编码
     */
    private String proTaxClass;

    /**
     * 状态
     */
    private String status;

    private boolean use;

    /**
     * 已开票数量
     */
    private BigDecimal gsiQty;

    /**
     * 总数
     */
    private BigDecimal totalQty;
}
