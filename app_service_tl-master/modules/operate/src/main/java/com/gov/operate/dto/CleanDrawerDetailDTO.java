package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Author staxc
 * @Date 2020/10/15 11:55
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CleanDrawerDetailDTO {
    /**
     * 商品编号
     */
    private String gscdProId;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 批号
     */
    private String gscdBatchNo;

    /**
     * 有效期
     */
    private String gscdValidDate;

    /**
     * 清斗数量
     */
    private BigDecimal gscdQty;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 批准文号
     */
    private String proRegisterNo;

}
