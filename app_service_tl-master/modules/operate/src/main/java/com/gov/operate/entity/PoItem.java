package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 采购订单明细表
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PO_ITEM")
@ApiModel(value="PoItem对象", description="采购订单明细表")
public class PoItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "采购凭证号")
    @TableField("PO_ID")
    private String poId;

    @ApiModelProperty(value = "订单行号")
    @TableField("PO_LINE_NO")
    private String poLineNo;

    @ApiModelProperty(value = "商品编码")
    @TableField("PO_PRO_CODE")
    private String poProCode;

    @ApiModelProperty(value = "采购订单数量")
    @TableField("PO_QTY")
    private BigDecimal poQty;

    @ApiModelProperty(value = "订单单位")
    @TableField("PO_UNIT")
    private String poUnit;

    @ApiModelProperty(value = "采购订单单价")
    @TableField("PO_PRICE")
    private BigDecimal poPrice;

    @ApiModelProperty(value = "订单行金额")
    @TableField("PO_LINE_AMT")
    private BigDecimal poLineAmt;

    @ApiModelProperty(value = "地点")
    @TableField("PO_SITE_CODE")
    private String poSiteCode;

    @ApiModelProperty(value = "库存地点")
    @TableField("PO_LOCATION_CODE")
    private String poLocationCode;

    @ApiModelProperty(value = "批次")
    @TableField("PO_BATCH")
    private String poBatch;

    @ApiModelProperty(value = "税率")
    @TableField("PO_RATE")
    private String poRate;

    @ApiModelProperty(value = "计划交货日期")
    @TableField("PO_DELIVERY_DATE")
    private String poDeliveryDate;

    @ApiModelProperty(value = "订单行备注")
    @TableField("PO_LINE_REMARK")
    private String poLineRemark;

    @ApiModelProperty(value = "删除标记")
    @TableField("PO_LINE_DELETE")
    private String poLineDelete;

    @ApiModelProperty(value = "交货已完成标记")
    @TableField("PO_COMPLETE_FLAG")
    private String poCompleteFlag;

    @ApiModelProperty(value = "已交货数量")
    @TableField("PO_DELIVERED_QTY")
    private BigDecimal poDeliveredQty;

    @ApiModelProperty(value = "已交货金额")
    @TableField("PO_DELIVERED_AMT")
    private BigDecimal poDeliveredAmt;

    @ApiModelProperty(value = "发票已完成标记")
    @TableField("PO_COMPLETE_INVOICE")
    private String poCompleteInvoice;

    @ApiModelProperty(value = "已发票校验数量")
    @TableField("PO_INVOICE_QTY")
    private BigDecimal poInvoiceQty;

    @ApiModelProperty(value = "已发票校验金额")
    @TableField("PO_INVOICE_AMT")
    private BigDecimal poInvoiceAmt;

    @ApiModelProperty(value = "付款已完成标记")
    @TableField("PO_COMPLETE_PAY")
    private String poCompletePay;

    @ApiModelProperty(value = "已付款数量")
    @TableField("PO_PAY_QTY")
    private BigDecimal poPayQty;

    @ApiModelProperty(value = "已付款金额")
    @TableField("PO_PAY_AMT")
    private BigDecimal poPayAmt;

    @ApiModelProperty(value = "前序订单单号")
    @TableField("PO_PRE_POID")
    private String poPrePoid;

    @ApiModelProperty(value = "前序订单行号")
    @TableField("PO_PRE_POITEM")
    private String poPrePoitem;

    @ApiModelProperty(value = "生产批号")
    @TableField("PO_BATCH_NO")
    private String poBatchNo;

    @ApiModelProperty(value = "生产日期")
    @TableField("PO_SCRQ")
    private String poScrq;

    @ApiModelProperty(value = "有效期")
    @TableField("PO_YXQ")
    private String poYxq;

    @ApiModelProperty(value = "拒收数量")
    @TableField("WM_JSSL")
    private BigDecimal wmJssl;

    @ApiModelProperty(value = "拒收原因")
    @TableField("WM_JSYY")
    private String wmJsyy;

    @ApiModelProperty(value = "发票价")
    @TableField("PO_FPJ")
    private BigDecimal poFpj;


}
