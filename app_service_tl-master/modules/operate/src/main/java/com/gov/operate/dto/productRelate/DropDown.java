package com.gov.operate.dto.productRelate;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.24
 */
@Data
public class DropDown {

    /**
     * 编码
     */
    public String code;

    /**
     * 名称
     */
    public String name;
}

