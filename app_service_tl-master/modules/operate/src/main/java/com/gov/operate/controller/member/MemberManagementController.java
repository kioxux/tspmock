package com.gov.operate.controller.member;


import com.gov.common.basic.DateUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.service.member.MemberManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;


@Api(tags = "会员管理模块")
@RestController
@RequestMapping("member-management")
public class MemberManagementController {

    @Autowired
    MemberManagementService memberManagementService;

    @ApiOperation(value = "根据手机号模糊查询会员列表")
    @GetMapping("getMemberListByNum")
    public Result getMemberListByNum(@ApiParam("手机号")  @RequestParam(required = false) String mobile) {
        return ResultUtil.success(memberManagementService.getMemberListByNum(mobile));
    }

    @ApiOperation(value = "根据会员id查询会员信息")
    @GetMapping("getMemberInfoById")
    public Result getMemberInfoById(@ApiParam("会员id")  @RequestParam String gsmbMemberId,String taskId) {
        return ResultUtil.success(memberManagementService.getMemberInfoById(gsmbMemberId,taskId));
    }

    @ApiOperation(value = "根据会员id查询会员消费记录")
    @GetMapping("getOrderInfoById")
    public Result getOrderInfoById(@ApiParam("会员id")  @RequestParam String gsmbMemberId,
                                   @ApiParam("开始日期")  @RequestParam String startDate,
                                   @ApiParam("结束日期")  @RequestParam String endDate) {
        return ResultUtil.success(memberManagementService.getOrderInfoById(gsmbMemberId, DateUtils.getYearMonthFirst(startDate), DateUtils.getYearMonthLast(endDate)));
    }

}
