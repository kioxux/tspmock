package com.gov.operate.mapper;

import com.gov.operate.entity.PayingItemNewRemind;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 付款提醒表 Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface PayingItemNewRemindMapper extends BaseMapper<PayingItemNewRemind> {

}
