package com.gov.operate.dto;

import com.gov.operate.entity.FiInvoiceRegistration;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class InvoicePaymentDTO extends FiInvoiceRegistration {

    /**
     * 该单据会使用到的金额
     */
    private BigDecimal matAmount;
}
