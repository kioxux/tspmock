package com.gov.operate.dto.weChat;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class GetCostomerListVO {

    @NotBlank(message = "素材mediaId不能为空")
    private String mediaId;

    /**
     * 公众号名
     */
    private String wechatName;

    /**
     * 类型 1:门店，2：连锁
     */
    private Integer goaType;

}
