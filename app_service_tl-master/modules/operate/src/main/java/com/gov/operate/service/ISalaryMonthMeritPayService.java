package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.dto.SalaryProgramDTO;
import com.gov.operate.entity.SalaryMonthMeritPay;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryMonthMeritPayService extends SuperService<SalaryMonthMeritPay> {

    /**
     * 薪资方案月绩效工资
     */
    void saveMeritPay(EditProgramVO vo);

    /**
     * 绩效列表
     */
    List<SalaryProgramDTO.SalaryMonthMeritPayOuter> getMeritPayList(Integer gspId);

    /**
     * 不分组绩效列表
     */
    List<SalaryMonthMeritPay> getMeritPayNoGroupingList(Integer gspId);
}
