package com.gov.operate.mapper;

import com.gov.operate.entity.WmsTupianzdrwcj;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-26
 */
public interface WmsTupianzdrwcjMapper extends BaseMapper<WmsTupianzdrwcj> {

    Integer getMaxWmXhByMatchCode(@Param("matchCode")String matchCode);
}
