package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_GIFT_RESULT")
@ApiModel(value="SdPromGiftResult对象", description="")
public class SdPromGiftResult extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPGR_VOUCHER_ID")
    private String gspgrVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPGR_SERIAL")
    private String gspgrSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPGR_PRO_ID")
    private String gspgrProId;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPGR_SERIES_ID")
    private String gspgrSeriesId;

    @ApiModelProperty(value = "赠品单品价格")
    @TableField("GSPGR_GIFT_PRC")
    private BigDecimal gspgrGiftPrc;

    @ApiModelProperty(value = "赠品单品折扣")
    @TableField("GSPGR_GIFT_REBATE")
    private String gspgrGiftRebate;


}
