package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchHeadSales;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 总部销售 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchHeadSalesService extends SuperService<BatchHeadSales> {

}
