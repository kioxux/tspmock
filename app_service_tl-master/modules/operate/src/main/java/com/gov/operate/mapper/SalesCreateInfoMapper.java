package com.gov.operate.mapper;

import com.gov.operate.entity.SalesCreateInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 开票人员基本信息 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
public interface SalesCreateInfoMapper extends BaseMapper<SalesCreateInfo> {

    /**
     * 查询手机号、邮箱是否存在
     * @param salesCreateInfo
     * @return
     */
    Integer selectCntByPhoneEmail(SalesCreateInfo salesCreateInfo);
}
