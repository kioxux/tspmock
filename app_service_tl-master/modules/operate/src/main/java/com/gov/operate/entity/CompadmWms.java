package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_COMPADM_WMS")
@ApiModel(value="CompadmWms对象", description="")
public class CompadmWms extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商ID")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "批发公司ID")
    @TableField("COMPADM_ID")
    private String compadmId;

    @ApiModelProperty(value = "批发公司名称")
    @TableField("COMPADM_NAME")
    private String compadmName;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("COMPADM_NO")
    private String compadmNo;

    @ApiModelProperty(value = "法人/负责人")
    @TableField("COMPADM_LEGAL_PERSON")
    private String compadmLegalPerson;

    @ApiModelProperty(value = "质量负责人")
    @TableField("COMPADM_QUA")
    private String compadmQua;

    @ApiModelProperty(value = "详细地址")
    @TableField("COMPADM_ADDR")
    private String compadmAddr;

    @ApiModelProperty(value = "创建日期")
    @TableField("COMPADM__CRE_DATE")
    private String compadmCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("COMPADM__CRE_TIME")
    private String compadmCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("COMPADM__CRE_ID")
    private String compadmCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("COMPADM__MODI_DATE")
    private String compadmModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("COMPADM__MODI_TIME")
    private String compadmModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("COMPADM__MODI_ID")
    private String compadmModiId;

    @ApiModelProperty(value = "LOGO地址")
    @TableField("COMPADM__LOGO")
    private String compadmLogo;

    @ApiModelProperty(value = "批发公司状态 N停用 Y启用")
    @TableField("COMPADM_STATUS")
    private String compadmStatus;


}
