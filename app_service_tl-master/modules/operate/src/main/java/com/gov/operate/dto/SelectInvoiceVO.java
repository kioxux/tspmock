package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SelectInvoiceVO extends Pageable {

    /**
     * 地点
     */
    private String site;
    /**
     * 单体店
     */
    private String stoCode;

    //@NotBlank(message = "供应商不能为空")
    private String supSelfCode;
    //@NotBlank(message = "发票日期开始时间不能为空")
    private String invoiceDateStart;
    //@NotBlank(message = "发票日期结束时间不能为空")
    private String invoiceDateEnd;
    //@NotBlank(message = "发票号码不能为空")
    private String invoiceNum;
    /**
     * 供应商
     */
    private String invoiceSupName;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 是否已核销
     */
    private Integer isEnd;

    /**
     * 付款单号
     */
    private String paymentOrderNo;

    /**
     * 业务员
     */
    private String invoiceSalesman;

    /**
     * 创建开始日期
     */
    private String invoiceCreationDateStart;

    /**
     * 创建结束日期
     */
    private String invoiceCreationDateEnd;
}
