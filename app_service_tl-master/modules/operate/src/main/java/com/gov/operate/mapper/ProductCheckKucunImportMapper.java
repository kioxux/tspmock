package com.gov.operate.mapper;

import com.gov.operate.entity.ProductCheckKucunImport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-26
 */
public interface ProductCheckKucunImportMapper extends BaseMapper<ProductCheckKucunImport> {

}
