package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 付款明细表（新）
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYING_ITEM_NEW")
@ApiModel(value="PayingItemNew对象", description="付款明细表（新）")
public class PayingItemNew extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "主表主键")
    @TableField("PID")
    private Integer pid;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款订单号")
    @TableField("FICO_ID")
    private String ficoId;

    @ApiModelProperty(value = "付款主体编码")
    @TableField("FICO_PAYINGSTORE_CODE")
    private String ficoPayingstoreCode;

    @ApiModelProperty(value = "门店")
    @TableField("FICO_SITE_CODE")
    private String ficoSiteCode;

    @ApiModelProperty(value = "付款主体名称")
    @TableField("FICO_PAYINGSTORE_NAME")
    private String ficoPayingstoreName;

    @ApiModelProperty(value = "付款方式")
    @TableField("FICO_PAYMENT_METHOD")
    private String ficoPaymentMethod;

    @ApiModelProperty(value = "付款金额")
    @TableField("FICO_PAYMENT_AMOUNT")
    private BigDecimal ficoPaymentAmount;

    @ApiModelProperty(value = "上期截止日期")
    @TableField("FICO_LAST_DEADLINE")
    private String ficoLastDeadline;

    @ApiModelProperty(value = "是否已发")
    @TableField("SP_SEND")
    private Integer spSend;

    @ApiModelProperty(value = "本期截止日期")
    @TableField("FICO_ENDING_TIME")
    private String ficoEndingTime;

    @ApiModelProperty(value = "本期开始日期")
    @TableField("FICO_STARTING_TIME")
    private String ficoStartingTime;

    @ApiModelProperty(value = "服务包ID")
    @TableField("SP_ID")
    private String spId;

    @ApiModelProperty(value = "系列")
    @TableField("SP_SERIES")
    private String spSeries;

    @ApiModelProperty(value = "服务包名称")
    @TableField("SP_NAME")
    private String spName;

    @ApiModelProperty(value = "本次优惠金额")
    @TableField("FICO_PAYMENT_DISCOUNT_AMOUNT")
    private BigDecimal ficoPaymentDiscountAmount;

    @ApiModelProperty(value = "本次应付金额")
    @TableField("FICO_PAYMENT_HANDLE_AMOUNT")
    private BigDecimal ficoPaymentHandleAmount;

    @ApiModelProperty(value = "本次余额付款")
    @TableField("FICO_PAYMENT_BALANCE_AMOUNT")
    private BigDecimal ficoPaymentBalanceAmount;

    @ApiModelProperty(value = "类型")
    @TableField("FICO_CLASS")
    private Integer ficoClass;

    @ApiModelProperty(value = "计费模式")
    @TableField("SP_MODE")
    private Integer spMode;

    @ApiModelProperty(value = "服务包主键ID")
    @TableField("R_ID")
    private Integer rId;
}
