package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_MESSAGE")
@ApiModel(value="UserMessage对象", description="")
public class UserMessage extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "接收人加盟商ID")
    @TableField("GUM_CLIENT")
    private String gumClient;

    @ApiModelProperty(value = "接收人员工编号")
    @TableField("GUM_USER_ID")
    private String gumUserId;

    @ApiModelProperty(value = "发送时间")
    @TableField("GUM_SEND_TIME")
    private String gumSendTime;

    @ApiModelProperty(value = "标题")
    @TableField("GUM_TITLE")
    private String gumTitle;

    @ApiModelProperty(value = "内容")
    @TableField("GUM_CONTENT")
    private String gumContent;

    @ApiModelProperty(value = "是否已读")
    @TableField("GUM_IS_READ")
    private Integer gumIsRead;

    @ApiModelProperty(value = "阅读时间")
    @TableField("GUM_READ_TIME")
    private String gumReadTime;

    @ApiModelProperty(value = "业务类型")
    @TableField("GUM_BUSINESS_TYPE")
    private Integer gumBusinessType;

    @ApiModelProperty(value = "是否跳转")
    @TableField("GUM_GO_PAGE")
    private Integer gumGoPage;

    @ApiModelProperty(value = "消息类型")
    @TableField("GUM_TYPE")
    private Integer gumType;

    @ApiModelProperty(value = "表示形式")
    @TableField("GUM_SHOW_TYPE")
    private Integer gumShowType;

    @ApiModelProperty(value = "参数")
    @TableField("GUM_PARAMS")
    private String gumParams;


}
