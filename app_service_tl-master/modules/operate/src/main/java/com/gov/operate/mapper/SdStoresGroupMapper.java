package com.gov.operate.mapper;

import com.gov.operate.dto.*;
import com.gov.operate.entity.SdStoresGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SdStoresGroupSet;
import com.gov.operate.entity.StoreData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
public interface SdStoresGroupMapper extends BaseMapper<SdStoresGroup> {

    /**
     * 根据加盟商获取门店分类
     * @param client
     * @return
     */
    List<SdStoresGroup> selectGroupList(@Param("client") String client);

    List<String> getByClientAndStoreAndType(@Param("client") String client, @Param("type") String type,
                                                   @Param("storeList") List<StoreData> storeList);
    /**
     * 批量添加门店分类
     * @param defaultGroups
     */
    void batchInsert(@Param("defaultGroups") List<SdStoresGroup> defaultGroups);

    /**
     * 根据加盟商和门店id获取门店分类
     * @param client
     * @param stoCode
     * @return
     */
    List<SdStoresGroup> selectGroupListByClientAndBrId(@Param("client") String client,@Param("stoCode") String stoCode);

    /**
     * 门店分类详情
     * @param storeGroupDto
     * @return
     */
    List<StoreGroupVo> getStoGroupDetails(StoreGroupDto storeGroupDto);

    /**
     * 根据加盟商、门店id、门店分类类型修改门店分类
     * @param sdStoresGroup
     */
    void updateStoGroupSetByPrimaryKey(SdStoresGroup sdStoresGroup);

    /** add by jinwencheng on 2021-12-23 16:25:03 添加门店分类 - 门店选择 - 查询 **/
    List<GetStoresForAddVO> getStoresForAddNonBatch(GetStoresForAddDTO getStoresForAddDTO);

    List<GetStoresForAddVO> getStoresForAddByBatch(GetStoresForAddDTO getStoresForAddDTO);

    BatchImportStoreDTO getStoreGroupSetInfos(String client, String gssgType, String gssgBrId);

    List<SdStoresGroupSet> getGroupSetByGssgType(String client, String gssgType);

    List<StoreData> getStoreDatasByClient(String client);

    void saveStoreGroup(List<SdStoresGroup> sdStoresGroup);

    void saveOneStoreGroup(BatchImportStoreDTO list);

    void updateStoreGroup(BatchImportStoreDTO batchImportStoreDTO);

    String getGssgTypeNameByClientAndGssgType(String client, String gssgType);

    List<String> getGroupSetParamsVal(String client, String stoCode, String gssgType, String gssgId);
    /** add end **/

}
