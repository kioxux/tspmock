package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddRechargeCardPayCheckVO {


    @NotBlank(message = "储值账户不能为空")
    private String gsrcAccountId;

    @NotBlank(message = "储值卡号不能为空")
    private String gsrcId;

    @NotNull(message = "应收金额不能为空")
    private BigDecimal gsrcpYsAmt;

    @NotNull(message = "充值金额不能为空")
    private BigDecimal gsrcpAfterAmt;

    @NotBlank(message = "行号不能为空")
    private String gspmSerial;

    @NotBlank(message = "编号不能为空")
    private String gspmId;


    @NotBlank(message = "充值比例不能为空")
    private String proportion;

    @NotNull(message = "充值类型不能为空")
    private Integer type;

    @NotNull(message = "充值门店")
    private String stoCode;

    private String payCode;

}
