package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_CLEANDRAWER_D")
@ApiModel(value="SdCleandrawerD对象", description="")
public class SdCleandrawerD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "清斗单号")
    @TableField("GSCD_VOUCHER_ID")
    private String gscdVoucherId;

    @ApiModelProperty(value = "清斗日期")
    @TableField("GSCD_DATE")
    private String gscdDate;

    @ApiModelProperty(value = "行号")
    @TableField("GSCD_SERIAL")
    private String gscdSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSCD_PRO_ID")
    private String gscdProId;

    @ApiModelProperty(value = "批号")
    @TableField("GSCD_BATCH_NO")
    private String gscdBatchNo;

    @ApiModelProperty(value = "有效期")
    @TableField("GSCD_VALID_DATE")
    private String gscdValidDate;

    @ApiModelProperty(value = "清斗数量")
    @TableField("GSCD_QTY")
    private String gscdQty;


}
