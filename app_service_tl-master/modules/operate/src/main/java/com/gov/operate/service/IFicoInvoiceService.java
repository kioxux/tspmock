package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.FicoInvoice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-17
 */
public interface IFicoInvoiceService extends SuperService<FicoInvoice> {

    void initInvoice();
}
