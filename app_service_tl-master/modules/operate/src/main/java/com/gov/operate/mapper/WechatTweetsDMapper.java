package com.gov.operate.mapper;

import com.gov.operate.entity.WechatTweetsD;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信推文管理明细 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-01-07
 */
public interface WechatTweetsDMapper extends BaseMapper<WechatTweetsD> {

}
