package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.dto.invoiceOptimiz.FicoInvoiceInformationRegistrationDto;
import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface FicoInvoiceInformationRegistrationMapper extends BaseMapper<FicoInvoiceInformationRegistration> {

    /**
     * 可用发票
     */
    List<FicoInvoiceInformationRegistrationDTO> selectAvailableInvoice(@Param("client") String client, @Param("supCode") String supCode,
                                                                       @Param("site") String site, @Param("type") String type);

    /**
     * 根据发票号和加盟商获取发票列表
     */
    List<FicoInvoiceInformationRegistrationDTO> getInvoiceList(@Param("client") String client, @Param("invoiceNumList") List<String> invoiceNumList);

    /**
     * 发票分页查询
     *
     * @param page
     * @param invoiceSiteType
     * @param siteCode
     * @param supCode
     * @param invoiceType
     * @param invoiceNum
     * @param invoiceDate
     * @return
     */
    IPage<FicoInvoiceInformationRegistrationDto> getInvoiceListOptimiz(Page page,
                                                                       @Param("client") String client,
                                                                       @Param("invoiceSiteType") String invoiceSiteType,
                                                                       @Param("siteCode") String siteCode,
                                                                       @Param("supCode") String supCode,
                                                                       @Param("invoiceType") String invoiceType,
                                                                       @Param("invoiceNum") String invoiceNum,
                                                                       @Param("invoiceDate") String invoiceDate,
                                                                       @Param("status") Integer status,
                                                                       @Param("invoiceSalesman") String invoiceSalesman);

    /**
     * 发票合计
     *
     * @param client
     * @param invoiceSiteType
     * @param siteCode
     * @param supCode
     * @param invoiceType
     * @param invoiceNum
     * @param invoiceDate
     * @return
     */
    FicoInvoiceInformationRegistrationDto getInvoiceTotalOptimiz(@Param("client") String client,
                                                                 @Param("invoiceSiteType") String invoiceSiteType,
                                                                 @Param("siteCode") String siteCode,
                                                                 @Param("supCode") String supCode,
                                                                 @Param("invoiceType") String invoiceType,
                                                                 @Param("invoiceNum") String invoiceNum,
                                                                 @Param("invoiceDate") String invoiceDate,
                                                                 @Param("status") Integer status);
}
