package com.gov.operate.mapper;

import com.gov.operate.entity.AuthorityAuthsClient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加盟商权限表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
public interface AuthorityAuthsClientMapper extends BaseMapper<AuthorityAuthsClient> {

}
