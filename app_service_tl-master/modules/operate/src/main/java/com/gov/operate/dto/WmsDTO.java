package com.gov.operate.dto;

import com.gov.operate.entity.WmsRkys;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class WmsDTO extends WmsRkys {

    /**
     * 业务类型
     */
    private String type;
    /**
     * 单据号
     */
    private String matDnId;
    /**
     * 单据去税金额
     */
    private BigDecimal excludingTaxAmount;
    /**
     * 单据税额
     */
    private BigDecimal taxAmount;
    /**
     * 单据总金额
     */
    private BigDecimal totalAmount;
    /**
     * 单据已登记金额
     */
    private BigDecimal registeredAmount;
    /**
     * 单据剩余金额
     */
    private BigDecimal chargeAmount;


    private String wmGysBh;

    private String proSite;

}
