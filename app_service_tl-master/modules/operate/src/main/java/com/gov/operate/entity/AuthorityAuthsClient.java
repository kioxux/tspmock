package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 加盟商权限表
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHORITY_AUTHS_CLIENT")
@ApiModel(value="AuthorityAuthsClient对象", description="加盟商权限表")
public class AuthorityAuthsClient extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "权限ID")
    @TableField("GAAC_AUTH_ID")
    private Integer gaacAuthId;


}
