package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryMonthSalaryGrade;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryMonthSalaryGradeService extends SuperService<SalaryMonthSalaryGrade> {

    /**
     * 薪资方案月工资级别
     */
    void saveSalaryGrade(EditProgramVO vo);

    /**
     * 薪资方案列表
     */
    List<SalaryMonthSalaryGrade> getSalaryGradeList(Integer gspId);
}
