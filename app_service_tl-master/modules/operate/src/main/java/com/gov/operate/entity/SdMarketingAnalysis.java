package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING_ANALYSIS")
@ApiModel(value="SdMarketingAnalysis对象", description="")
public class SdMarketingAnalysis extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "营销活动ID")
    @TableField("MK_MARKETID")
    private String mkMarketid;

    @ApiModelProperty(value = "门店编码")
    @TableField("MK_BR_ID")
    private String mkBrId;

    @ApiModelProperty(value = "分析类型")
    @TableField("MK_ANTYPE")
    private String mkAntype;

    @ApiModelProperty(value = "本期销售额")
    @TableField("MK_SAMT")
    private BigDecimal mkSamt;

    @ApiModelProperty(value = "本期毛利率")
    @TableField("MK_GM")
    private BigDecimal mkGm;

    @ApiModelProperty(value = "本期交易次数")
    @TableField("MKTANS")
    private Integer mktans;

    @ApiModelProperty(value = "本期客单价")
    @TableField("MK_PRICE")
    private BigDecimal mkPrice;

    @ApiModelProperty(value = "本期毛利额")
    @TableField("MK_GAMT")
    private BigDecimal mkGamt;

    @ApiModelProperty(value = "本期日均交易额")
    @TableField("MK_EAMT")
    private BigDecimal mkEamt;

    @ApiModelProperty(value = "去年同期销售额")
    @TableField("MK_SAMT1")
    private BigDecimal mkSamt1;

    @ApiModelProperty(value = "去年同期毛利率")
    @TableField("MK_GM1")
    private BigDecimal mkGm1;

    @ApiModelProperty(value = "去年同期交易次数")
    @TableField("MKTANS1")
    private Integer mktans1;

    @ApiModelProperty(value = "去年同期客单价")
    @TableField("MK_PRICE1")
    private BigDecimal mkPrice1;

    @ApiModelProperty(value = "去年同期毛利额")
    @TableField("MK_GAMT1")
    private BigDecimal mkGamt1;

    @ApiModelProperty(value = "去年同期日均交易额")
    @TableField("MK_EAMT1")
    private BigDecimal mkEamt1;

    @ApiModelProperty(value = "更新时间戳")
    @TableField("CRE_DATE")
    private String creDate;


}
