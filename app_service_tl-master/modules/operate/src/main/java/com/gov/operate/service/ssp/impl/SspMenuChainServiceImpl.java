package com.gov.operate.service.ssp.impl;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.JsonUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.ssp.SspMenuChainDTO;
import com.gov.operate.entity.SspLog;
import com.gov.operate.entity.SspMenuChain;
import com.gov.operate.entity.SspUserChain;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SspLogMapper;
import com.gov.operate.mapper.SspMenuChainMapper;
import com.gov.operate.mapper.SspMenuMapper;
import com.gov.operate.mapper.SspUserChainMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ssp.ISspMenuChainService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SspMenuChainServiceImpl extends ServiceImpl<SspMenuChainMapper, SspMenuChain>
        implements ISspMenuChainService {

    @Resource
    private CommonService commonService;

    @Resource
    private SspMenuMapper sspMenuMapper;

    @Resource
    private SspUserChainMapper sspUserChainMapper;

    @Resource
    private SspLogMapper sspLogMapper;


    @Override
    public Result getMenuChainList(Long userId) {
        TokenUser currentUser = commonService.getLoginInfo();
//        TokenUser currentUser = new TokenUser();
//        currentUser.setClient("10000013");
        List<SspMenuChainDTO> resultList = sspMenuMapper.selectMenuDTOList();
        List<SspMenuChain> userCheckedList = baseMapper.selectList(
                new LambdaQueryWrapper<SspMenuChain>().eq(SspMenuChain::getUserId, userId)
                        .eq(SspMenuChain::getClient, currentUser.getClient()));
        List<SspUserChain> userChainList = sspUserChainMapper.selectList(
                new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, userId)
                        .eq(SspUserChain::getClient, currentUser.getClient()));
        if (userChainList.size() == 0) {
            throw new CustomResultException("用户未绑定主体");
        }

        Map<String, Boolean> chainList = userChainList.stream().collect(Collectors.toMap(t -> String.valueOf(t.getId()), t -> false));

        resultList.forEach(t -> {
            // 用户ID
            t.setUserId(userId);
            if (MapUtil.isEmpty(t.getParams())) {
                t.setParams(new HashMap<>());
            }
            t.getParams().putAll(chainList);

            t.getParams().putAll(userCheckedList.stream()
                    .filter(a -> a.getMenuId().equals(t.getMenuId()))
                    .collect(Collectors.toMap(b -> String.valueOf(b.getUserChainId()), b -> true)));
        });

        return ResultUtil.success(resultList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result bindMenuChainList(List<SspMenuChainDTO> menuChainDTOList, Long userId) {
        TokenUser currentUser = commonService.getLoginInfo();
//        TokenUser currentUser = new TokenUser();
//        currentUser.setClient("10000013");
        baseMapper.delete(new LambdaQueryWrapper<SspMenuChain>().eq(SspMenuChain::getUserId, userId)
                .eq(SspMenuChain::getClient, currentUser.getClient()));
        List<SspUserChain> userChainList = sspUserChainMapper.selectList(
                new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, userId)
                        .eq(SspUserChain::getClient, currentUser.getClient()));
        if (userChainList.size() == 0) {
            throw new CustomResultException("主体未绑定");
        }

        List<SspMenuChain> addList = new ArrayList<>();

        menuChainDTOList.forEach(t -> {
            t.getParams().forEach((k, v) -> {
                if (v) {
                    SspUserChain userChain = userChainList.stream().filter(a ->
                            StrUtil.equals(k, String.valueOf(a.getId()))).findFirst().orElse(null);
                    if (userChain == null) {
                        throw new CustomResultException("主体未绑定");
                    }
                    SspMenuChain menuChain = new SspMenuChain();
                    BeanUtils.copyProperties(t, menuChain);
                    menuChain.setClient(userChain.getClient());
                    menuChain.setUserChainId(userChain.getId());
                    menuChain.setChainHead(userChain.getChainHead());
                    menuChain.setChainName(userChain.getChainName());
                    menuChain.setChainType(userChain.getChainType());
                    menuChain.setIsDelete(0);
                    menuChain.setCreateBy(currentUser.getUserId());
                    menuChain.setCreateTime(new Date());
                    addList.add(menuChain);
                }
            });
        });
        saveBatch(addList);

        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        log.setContent("授权报表更新");
        log.setJson(JsonUtils.beanToJson(addList));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);

        return ResultUtil.success();
    }

    @Override
    public int deleteChainHead(Long userId, String client, Set<String> keySet) {
        return baseMapper.deleteChainHead(userId, client, keySet);
    }
}




