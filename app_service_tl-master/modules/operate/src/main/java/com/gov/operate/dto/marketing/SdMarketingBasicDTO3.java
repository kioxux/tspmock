package com.gov.operate.dto.marketing;

import com.gov.operate.entity.SdMarketingBasic;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SdMarketingBasicDTO3 extends SdMarketingBasic {

    /**
     * 创建人名称
     */
    private String gsmCreateUserName;
}
