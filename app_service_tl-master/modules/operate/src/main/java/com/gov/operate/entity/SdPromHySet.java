package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_HY_SET")
@ApiModel(value="SdPromHySet对象", description="")
public class SdPromHySet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPHS_VOUCHER_ID")
    private String gsphsVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPHS_SERIAL")
    private String gsphsSerial;

    @ApiModelProperty(value = "编码")
    @TableField("GSPHS_PRO_ID")
    private String gsphsProId;

    @ApiModelProperty(value = "促销价")
    @TableField("GSPHS_PRICE")
    private BigDecimal gsphsPrice;

    @ApiModelProperty(value = "促销折扣")
    @TableField("GSPHS_REBATE")
    private String gsphsRebate;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPHS_INTE_FLAG")
    private String gsphsInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPHS_INTE_RATE")
    private String gsphsInteRate;


}
