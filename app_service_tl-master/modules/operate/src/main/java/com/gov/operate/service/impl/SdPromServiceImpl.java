package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.enums.PromTypeEnum;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.redis.jedis.RedisClient;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xx
 * @since 2021-02-2
 */
@Service
public class SdPromServiceImpl implements ISdPromService {
    //营销号redis前缀
    private static final String MARKET_PREFIX = "MARKET_PREFIX";
    //单号Redis前缀
    private static final String VOUCHER_PREFIX = "VOUCHER_PREFIX";
    /**
     * 是否发送电子券设置 KEY
     */
    public static final String SEND_TICKET_FLAG = "SEND_TICKET_FLAG";
    @Resource
    private ClSystemParaMapper clSystemParaMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private IProductBusinessService productBusinessService;
    @Resource
    private ISdPromHeadService sdPromHeadService;
    @Resource
    private SdPromHeadMapper sdPromHeadMapper;
    @Resource
    private ISdPromUnitarySetService sdPromUnitarySetService;
    @Resource
    private ISdPromSeriesSetService sdPromSeriesSetService;
    @Resource
    private ISdPromSeriesCondsService sdPromSeriesCondsService;
    @Resource
    private ISdPromGiftSetService sdPromGiftSetService;
    @Resource
    private ISdPromGiftCondsService sdPromGiftCondsService;
    @Resource
    private ISdPromGiftResultService sdPromGiftResultService;
    @Resource
    private ISdPromAssoSetService sdPromAssoSetService;
    @Resource
    private ISdPromAssoCondsService sdPromAssoCondsService;
    @Resource
    private ISdPromAssoResultService sdPromAssoResultService;
    @Resource
    private ISdPromHySetService sdPromHySetService;
    @Resource
    private ISdPromHyrDiscountService sdPromHyrDiscountService;
    @Resource
    private ISdPromHyrPriceService sdPromHyrPriceService;
    @Resource
    private ISdPromChmedSetService sdPromChmedSetService;
    @Resource
    private ProductBusinessMapper productBusinessMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    SdPromHySetMapper promHySetMapper;
    @Resource
    SdPromHyrPriceMapper promHyrPriceMapper;


    @Override
    public List<StoreData> getStoreList(GetStoreListVO getStoreListVO) {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> storeDataList = sdPromHeadMapper.getStoreList(user.getClient(), getStoreListVO.getKeyLike(), getStoreListVO.getGssgId(), getStoreListVO.getStoDistrict());
        return storeDataList;
    }

    @Override
    public IPage<QueryProListByStoreDto> getStoreProductList(GetStoreProductVO getStoreProductVO) {
        TokenUser user = commonService.getLoginInfo();
        Page<QueryProListByStoreDto> page = new Page<>(getStoreProductVO.getPageNum(), getStoreProductVO.getPageSize());
        IPage<QueryProListByStoreDto> productBusinessList = sdPromHeadMapper.getStoreProductList(page, getStoreProductVO.getProKey(), user.getClient());
        return productBusinessList;
    }

    @Override
    @Transactional
    public Integer savePromList(SavePromListVO savePromListVO) {
        TokenUser user = commonService.getLoginInfo();
        //促销单列表
        List<SavePromListVO.Prom> promList = savePromListVO.getPromList();
        //门店列表
        List<String> stoCodeList = savePromListVO.getStoCodeList();
        //生成活动主表
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        List<SdPromHead> promHeadList = new ArrayList<>();
        List<SdPromUnitarySet> proSingleList = new ArrayList<>();
        List<SdPromSeriesSet> promSeriesSetList = new ArrayList<>();
        List<SdPromSeriesConds> promSeriesCondsList = new ArrayList<>();
        List<SdPromGiftSet> promGiftSetList = new ArrayList<>();
        List<SdPromGiftConds> promGiftCondsList = new ArrayList<>();
        List<SdPromGiftResult> promGiftResultList = new ArrayList<>();
        List<SdPromAssoSet> promAssoSetList = new ArrayList<>();
        List<SdPromAssoConds> promAssoCondsList = new ArrayList<>();
        List<SdPromAssoResult> promAssoResultList = new ArrayList<>();
        List<SdPromChmedSet> sdPromChmedSetList = new ArrayList<>();
        //获取单号
        String curVoucherId = getVoucherId();
        //获取营销主键
        Integer marketId = getMarketId();
        Long voucherNum = Long.valueOf(curVoucherId.substring(("CX" + year).length()));
        for (int i = 0; i < promList.size(); i++) {
            voucherNum++;
            while (redisClient.exists(VOUCHER_PREFIX + voucherNum)) {
                voucherNum++;
            }
            redisClient.set(VOUCHER_PREFIX + voucherNum, VOUCHER_PREFIX + voucherNum, 120);
            String voucherId = "CX" + year + String.format("%0" + 10 + "d", voucherNum);
            //生成主表列表
            promHeadList.addAll(createPromHeadList(marketId, savePromListVO, promList.get(i), stoCodeList, voucherId, user.getClient(), user.getUserId()));
            if (PromTypeEnum.PROM_SINGLE.getCode().equals(promList.get(i).getGsphType())) {
                //添加单品促销
                proSingleList.addAll(createPromSingleList(promList.get(i), voucherId, user.getClient()));
            } else if (PromTypeEnum.PROM_SERIES.getCode().equals(promList.get(i).getGsphType())) {
                //添加系列促销
                addPromSeries(promSeriesSetList, promSeriesCondsList, promList.get(i), voucherId, user.getClient());
            } else if (PromTypeEnum.PROM_GIFT.getCode().equals(promList.get(i).getGsphType())) {
                //添加赠品促销
                addPromGift(promGiftSetList, promGiftCondsList, promGiftResultList, promList.get(i), voucherId, user.getClient());
            } else if (PromTypeEnum.PROM_COMBIN.getCode().equals(promList.get(i).getGsphType())) {
                //添加组合促销
                addPromAsso(promAssoSetList, promAssoCondsList, promAssoResultList, promList.get(i), voucherId, user.getClient());
            } else if (PromTypeEnum.PROM_CHMED.getCode().equals(promList.get(i).getGsphType())) {
                //添加单品促销
                sdPromChmedSetList.addAll(createPromChmed(promList.get(i), voucherId, user.getClient()));
            }
        }
        if (!CollectionUtils.isEmpty(promHeadList)) {
            promHeadParaEmptyStrToNull(promHeadList);
            sdPromHeadMapper.insertList(promHeadList);
        }
        if (!CollectionUtils.isEmpty(proSingleList)) {
            proSingleParaEmptyStrToNull(proSingleList);
            sdPromUnitarySetService.saveBatch(proSingleList);
        }
        if (!CollectionUtils.isEmpty(promSeriesSetList)) {
            promSeriesSetParaEmptyStrToNull(promSeriesSetList);
            sdPromSeriesSetService.saveBatch(promSeriesSetList);
        }
        if (!CollectionUtils.isEmpty(promSeriesCondsList)) {
            promSeriesCondsParaEmptyStrToNull(promSeriesCondsList);
            sdPromSeriesCondsService.saveBatch(promSeriesCondsList);
        }
        if (!CollectionUtils.isEmpty(promGiftSetList)) {
            promGiftSetParaEmptyStrToNull(promGiftSetList);
            sdPromGiftSetService.saveBatch(promGiftSetList);
        }
        if (!CollectionUtils.isEmpty(promGiftCondsList)) {
            promGiftCondsParaEmptyStrToNull(promGiftCondsList);
            sdPromGiftCondsService.saveBatch(promGiftCondsList);
        }
        if (!CollectionUtils.isEmpty(promGiftResultList)) {
            promGiftResultParaEmptyStrToNull(promGiftResultList);
            sdPromGiftResultService.saveBatch(promGiftResultList);
        }
        if (!CollectionUtils.isEmpty(promAssoSetList)) {
            promAssoSetListParaEmptyStrToNull(promAssoSetList);
            sdPromAssoSetService.saveBatch(promAssoSetList);
        }
        if (!CollectionUtils.isEmpty(promAssoCondsList)) {
            promAssoCondsParaEmptyStrToNull(promAssoCondsList);
            sdPromAssoCondsService.saveBatch(promAssoCondsList);
        }
        if (!CollectionUtils.isEmpty(promAssoResultList)) {
            promAssoResultParaEmptyStrToNull(promAssoResultList);
            sdPromAssoResultService.saveBatch(promAssoResultList);
        }
        if (!CollectionUtils.isEmpty(sdPromChmedSetList)) {
            sdPromChmedSetService.saveBatch(sdPromChmedSetList);
        }
        return marketId;
    }

    private void promHeadParaEmptyStrToNull(List<SdPromHead> promHeadList) {
        promHeadList.forEach(proHead -> {
            proHead.setGsphStatus(StringUtils.isBlank(proHead.getGsphStatus()) ? null : proHead.getGsphStatus());
            proHead.setGsphBeginDate(StringUtils.isBlank(proHead.getGsphBeginDate()) ? null : proHead.getGsphBeginDate());
            proHead.setGsphEndDate(StringUtils.isBlank(proHead.getGsphEndDate()) ? null : proHead.getGsphEndDate());
            proHead.setGsphDateFrequency(StringUtils.isBlank(proHead.getGsphDateFrequency()) ? null : proHead.getGsphDateFrequency());
            proHead.setGsphWeekFrequency(StringUtils.isBlank(proHead.getGsphWeekFrequency()) ? null : proHead.getGsphWeekFrequency());
            proHead.setGsphBeginTime(StringUtils.isBlank(proHead.getGsphBeginTime()) ? null : proHead.getGsphBeginTime());
            proHead.setGsphEndTime(StringUtils.isBlank(proHead.getGsphEndTime()) ? null : proHead.getGsphEndTime());
            proHead.setGsphType(StringUtils.isBlank(proHead.getGsphType()) ? null : proHead.getGsphType());
            proHead.setGsphPart(StringUtils.isBlank(proHead.getGsphPart()) ? null : proHead.getGsphPart());
            proHead.setGsphPara1(StringUtils.isBlank(proHead.getGsphPara1()) ? null : proHead.getGsphPara1());
            proHead.setGsphPara2(StringUtils.isBlank(proHead.getGsphPara2()) ? null : proHead.getGsphPara2());
            proHead.setGsphPara3(StringUtils.isBlank(proHead.getGsphPara3()) ? null : proHead.getGsphPara3());
            proHead.setGsphPara4(StringUtils.isBlank(proHead.getGsphPara4()) ? null : proHead.getGsphPara4());
            proHead.setGsphExclusion(StringUtils.isBlank(proHead.getGsphExclusion()) ? null : proHead.getGsphExclusion());
            proHead.setGsphFlag(StringUtils.isBlank(proHead.getGsphFlag()) ? null : proHead.getGsphFlag());
        });
    }

    private void proSingleParaEmptyStrToNull(List<SdPromUnitarySet> proSingleList) {
        proSingleList.forEach(proSingle -> {
            proSingle.setGspusVaild(StringUtils.isBlank(proSingle.getGspusVaild()) ? null : proSingle.getGspusVaild());
            proSingle.setGspusQty1(StringUtils.isBlank(proSingle.getGspusQty1()) ? null : proSingle.getGspusQty1());
            proSingle.setGspusRebate1(StringUtils.isBlank(proSingle.getGspusRebate1()) ? null : proSingle.getGspusRebate1());
            proSingle.setGspusQty2(StringUtils.isBlank(proSingle.getGspusQty2()) ? null : proSingle.getGspusQty2());
            proSingle.setGspusRebate2(StringUtils.isBlank(proSingle.getGspusRebate2()) ? null : proSingle.getGspusRebate2());
            proSingle.setGspusQty3(StringUtils.isBlank(proSingle.getGspusQty3()) ? null : proSingle.getGspusQty3());
            proSingle.setGspusRebate3(StringUtils.isBlank(proSingle.getGspusRebate3()) ? null : proSingle.getGspusRebate3());
            proSingle.setGspusMemFlag(StringUtils.isBlank(proSingle.getGspusMemFlag()) ? null : proSingle.getGspusMemFlag());
            proSingle.setGspusInteFlag(StringUtils.isBlank(proSingle.getGspusInteFlag()) ? null : proSingle.getGspusInteFlag());
            proSingle.setGspusInteRate(StringUtils.isBlank(proSingle.getGspusInteRate()) ? null : proSingle.getGspusInteRate());
        });
    }

    private void promSeriesSetParaEmptyStrToNull(List<SdPromSeriesSet> promSeriesSetList) {
        promSeriesSetList.forEach(promSeriesSet -> {
            promSeriesSet.setGspssSeriesId(StringUtils.isBlank(promSeriesSet.getGspssSeriesId()) ? null : promSeriesSet.getGspssSeriesId());
            promSeriesSet.setGspssReachQty1(StringUtils.isBlank(promSeriesSet.getGspssReachQty1()) ? null : promSeriesSet.getGspssReachQty1());
            promSeriesSet.setGspssResultRebate1(StringUtils.isBlank(promSeriesSet.getGspssResultRebate1()) ? null : promSeriesSet.getGspssResultRebate1());
            promSeriesSet.setGspssReachQty2(StringUtils.isBlank(promSeriesSet.getGspssReachQty2()) ? null : promSeriesSet.getGspssReachQty2());
            promSeriesSet.setGspssResultRebate2(StringUtils.isBlank(promSeriesSet.getGspssResultRebate2()) ? null : promSeriesSet.getGspssResultRebate2());
            promSeriesSet.setGspssReachQty3(StringUtils.isBlank(promSeriesSet.getGspssReachQty3()) ? null : promSeriesSet.getGspssReachQty3());
            promSeriesSet.setGspssResultRebate3(StringUtils.isBlank(promSeriesSet.getGspssResultRebate3()) ? null : promSeriesSet.getGspssResultRebate3());
        });
    }

    private void promSeriesCondsParaEmptyStrToNull(List<SdPromSeriesConds> promSeriesCondsList) {
        promSeriesCondsList.forEach(item -> {
            item.setGspscSeriesId(StringUtils.isBlank(item.getGspscSeriesId()) ? null : item.getGspscSeriesId());
            item.setGspscMemFlag(StringUtils.isBlank(item.getGspscMemFlag()) ? null : item.getGspscMemFlag());
            item.setGspscInteFlag(StringUtils.isBlank(item.getGspscInteFlag()) ? null : item.getGspscInteFlag());
            item.setGspscInteRate(StringUtils.isBlank(item.getGspscInteRate()) ? null : item.getGspscInteRate());
        });
    }

    private void promGiftSetParaEmptyStrToNull(List<SdPromGiftSet> promGiftSetList) {
        promGiftSetList.forEach(item -> {
            item.setGspgsSeriesProId(StringUtils.isBlank(item.getGspgsSeriesProId()) ? null : item.getGspgsSeriesProId());
            item.setGspgsReachQty1(StringUtils.isBlank(item.getGspgsReachQty1()) ? null : item.getGspgsReachQty1());
            item.setGspgsResultQty1(StringUtils.isBlank(item.getGspgsResultQty1()) ? null : item.getGspgsResultQty1());
            item.setGspgsReachQty2(StringUtils.isBlank(item.getGspgsReachQty2()) ? null : item.getGspgsReachQty2());
            item.setGspgsResultQty2(StringUtils.isBlank(item.getGspgsResultQty2()) ? null : item.getGspgsResultQty2());
            item.setGspgsReachQty3(StringUtils.isBlank(item.getGspgsReachQty3()) ? null : item.getGspgsReachQty3());
            item.setGspgsResultQty3(StringUtils.isBlank(item.getGspgsResultQty3()) ? null : item.getGspgsResultQty3());
        });
    }

    private void promGiftCondsParaEmptyStrToNull(List<SdPromGiftConds> promGiftCondsList) {
        promGiftCondsList.forEach(item -> {
            item.setGspgcSeriesId(StringUtils.isBlank(item.getGspgcSeriesId()) ? null : item.getGspgcSeriesId());
            item.setGspgcMemFlag(StringUtils.isBlank(item.getGspgcMemFlag()) ? null : item.getGspgcMemFlag());
            item.setGspgcInteFlag(StringUtils.isBlank(item.getGspgcInteFlag()) ? null : item.getGspgcInteFlag());
            item.setGspgcInteRate(StringUtils.isBlank(item.getGspgcInteRate()) ? null : item.getGspgcInteRate());
        });
    }

    private void promGiftResultParaEmptyStrToNull(List<SdPromGiftResult> promGiftResultList) {
        promGiftResultList.forEach(item -> {
            item.setGspgrSeriesId(StringUtils.isBlank(item.getGspgrSeriesId()) ? null : item.getGspgrSeriesId());
            item.setGspgrGiftRebate(StringUtils.isBlank(item.getGspgrGiftRebate()) ? null : item.getGspgrGiftRebate());
        });
    }

    private void promAssoSetListParaEmptyStrToNull(List<SdPromAssoSet> promAssoSetList) {
        promAssoSetList.forEach(item -> {
            item.setGspasSeriesId(StringUtils.isBlank(item.getGspasSeriesId()) ? null : item.getGspasSeriesId());
            item.setGspasRebate(StringUtils.isBlank(item.getGspasRebate()) ? null : item.getGspasRebate());
            item.setGspasQty(StringUtils.isBlank(item.getGspasQty()) ? null : item.getGspasQty());
        });
    }

    private void promAssoCondsParaEmptyStrToNull(List<SdPromAssoConds> promAssoCondsList) {
        promAssoCondsList.forEach(item -> {
            item.setGspacQty(StringUtils.isBlank(item.getGspacQty()) ? null : item.getGspacQty());
            item.setGspacSeriesId(StringUtils.isBlank(item.getGspacSeriesId()) ? null : item.getGspacSeriesId());
            item.setGspacMemFlag(StringUtils.isBlank(item.getGspacMemFlag()) ? null : item.getGspacMemFlag());
            item.setGspacInteFlag(StringUtils.isBlank(item.getGspacInteFlag()) ? null : item.getGspacInteFlag());
            item.setGspacInteRate(StringUtils.isBlank(item.getGspacInteRate()) ? null : item.getGspacInteRate());
        });
    }

    private void promAssoResultParaEmptyStrToNull(List<SdPromAssoResult> promAssoResultList) {
        promAssoResultList.forEach(item -> {
            item.setGsparSeriesId(StringUtils.isBlank(item.getGsparSeriesId()) ? null : item.getGsparSeriesId());
            item.setGsparGiftRebate(StringUtils.isBlank(item.getGsparGiftRebate()) ? null : item.getGsparGiftRebate());
        });
    }

    private void sdPromHySetParaEmptyStrToNull(List<SdPromHySet> sdPromHySetList) {
        sdPromHySetList.forEach(item -> {
            item.setGsphsRebate(StringUtils.isBlank(item.getGsphsRebate()) ? null : item.getGsphsRebate());
            item.setGsphsInteFlag(StringUtils.isBlank(item.getGsphsInteFlag()) ? null : item.getGsphsInteFlag());
            item.setGsphsInteRate(StringUtils.isBlank(item.getGsphsInteRate()) ? null : item.getGsphsInteRate());
        });
    }

    private void sdPromHyrDiscountParaEmptyStrToNull(List<SdPromHyrDiscount> sdPromHyrDiscountList) {
        sdPromHyrDiscountList.forEach(item -> {
            item.setGsppBeginDate(StringUtils.isBlank(item.getGsppBeginDate()) ? null : item.getGsppBeginDate());
            item.setGsppEndDate(StringUtils.isBlank(item.getGsppEndDate()) ? null : item.getGsppEndDate());
            item.setGsppBeginTime(StringUtils.isBlank(item.getGsppBeginTime()) ? null : item.getGsppBeginTime());
            item.setGsppEndTime(StringUtils.isBlank(item.getGsppEndTime()) ? null : item.getGsppEndTime());
            item.setGsppDateFrequency(StringUtils.isBlank(item.getGsppDateFrequency()) ? null : item.getGsppDateFrequency());
            item.setGsppTimeFrequency(StringUtils.isBlank(item.getGsppTimeFrequency()) ? null : item.getGsppTimeFrequency());
            item.setGsppRebate(StringUtils.isBlank(item.getGsppRebate()) ? null : item.getGsppRebate());
            item.setGsppInteFlag(StringUtils.isBlank(item.getGsppInteFlag()) ? null : item.getGsppInteFlag());
            item.setGsppInteRate(StringUtils.isBlank(item.getGsppInteRate()) ? null : item.getGsppInteRate());
        });
    }

    private void sdPromHyrPriceParaEmptyStrToNull(List<SdPromHyrPrice> sdPromHyrPriceList) {
        sdPromHyrPriceList.forEach(item -> {
            item.setGsphpRebate(StringUtils.isBlank(item.getGsphpRebate()) ? null : item.getGsphpRebate());
            item.setGsphpInteFlag(StringUtils.isBlank(item.getGsphpInteFlag()) ? null : item.getGsphpInteFlag());
            item.setGsphpInteRate(StringUtils.isBlank(item.getGsphpInteRate()) ? null : item.getGsphpInteRate());
        });
    }

    private Integer getMarketId() {
        TokenUser user = commonService.getLoginInfo();
        Integer marketId = sdPromHeadMapper.getMaxMarketId(user.getClient());
        //并发处理
        while (redisClient.exists(MARKET_PREFIX + marketId)) {
            marketId--;
        }
        redisClient.set(MARKET_PREFIX + marketId, MARKET_PREFIX + marketId, 120);
        return marketId;
    }


    @Override
    public Page<SdPromHead> queryPromList(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        Page<SdPromHead> page = new Page<>(queryPromListVO.getPageNum(), queryPromListVO.getPageSize());
        Page<SdPromHead> sdPromHeadList = sdPromHeadMapper.queryPromList(page, queryPromListVO, user.getClient());
        return sdPromHeadList;

    }

    @Override
    public SavePromListVO queryPromDetail(QueryPromDetailVO queryPromDetailVO) {
        TokenUser user = commonService.getLoginInfo();
        SavePromListVO savePromListVO = new SavePromListVO();
        //查询活动主表
        SdPromHead sdPromHead = sdPromHeadService.getOne(new LambdaQueryWrapper<SdPromHead>()
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphMarketid, queryPromDetailVO.getGsphMarketid())
                .groupBy(SdPromHead::getGsphMarketid));
        BeanUtils.copyProperties(sdPromHead, savePromListVO);
        //查询促销
        List<SdPromHead> sdPromHeadList = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphMarketid, queryPromDetailVO.getGsphMarketid())
                .groupBy(SdPromHead::getGsphVoucherId));
        //组装门店列表
        List<SdPromHead> storeHeadList = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .select(SdPromHead::getGsphBrId)
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphMarketid, queryPromDetailVO.getGsphMarketid()));
        Set<String> stoCodeList = storeHeadList.stream().map(SdPromHead::getGsphBrId).collect(Collectors.toSet());
        savePromListVO.setStoCodeList(new ArrayList<>(stoCodeList));
        //设置活动主表营销状态
        savePromListVO.setGsphAllStatus(getGsphAllStatus(sdPromHeadList));
        List<SavePromListVO.Prom> promList = sdPromHeadList.stream().map(s -> {
            SavePromListVO.Prom prom = new SavePromListVO.Prom();
            BeanUtils.copyProperties(s, prom);
            return prom;
        }).collect(Collectors.toList());
        savePromListVO.setPromList(promList);
        //查询产品信息
        List<ProductBusiness> productBusinessList = productBusinessService.list(new LambdaQueryWrapper<ProductBusiness>()
                .select(ProductBusiness::getProSelfCode, ProductBusiness::getProCommonname, ProductBusiness::getProFactoryName
                        , ProductBusiness::getProName, ProductBusiness::getProSpecs, ProductBusiness::getProPym)
                .eq(ProductBusiness::getClient, user.getClient())
                .groupBy(ProductBusiness::getProSelfCode));
        Map<String, ProductBusiness> proDataMap = productBusinessList.stream().collect(Collectors.toMap(ProductBusiness::getProSelfCode, s -> s, (key1, key2) -> key2));
        for (SavePromListVO.Prom prom : promList) {
            if (PromTypeEnum.PROM_SINGLE.getCode().equals(prom.getGsphType())) {
                //查询单品促销列表
                List<SdPromUnitarySet> unitarySetList = sdPromUnitarySetService.list(new LambdaQueryWrapper<SdPromUnitarySet>()
                        .eq(SdPromUnitarySet::getClient, user.getClient())
                        .eq(SdPromUnitarySet::getGspusVoucherId, prom.getGsphVoucherId())
                        .orderByAsc(SdPromUnitarySet::getGspusSerial, SdPromUnitarySet::getGspusProId));
                prom.setUnitarySetList(unitarySetList.stream().map(s -> {
                    SavePromListVO.Prom.UnitarySet unitarySet = new SavePromListVO.Prom.UnitarySet();
                    BeanUtils.copyProperties(s, unitarySet);
                    String gspusProId = s.getGspusProId();
                    if (StringUtils.isNotBlank(gspusProId) && !gspusProId.equalsIgnoreCase("all")) {
                        unitarySet.setProSelfCode(proDataMap.get(gspusProId).getProSelfCode());
                        unitarySet.setProCommonname(proDataMap.get(gspusProId).getProCommonname());
                        unitarySet.setProName(proDataMap.get(gspusProId).getProName());
                        unitarySet.setProSpecs(proDataMap.get(gspusProId).getProSpecs());
                        unitarySet.setProFactoryName(proDataMap.get(gspusProId).getProFactoryName());
                        unitarySet.setProPym(proDataMap.get(gspusProId).getProPym());
                    }
                    return unitarySet;
                }).collect(Collectors.toList()));
            } else if (PromTypeEnum.PROM_SERIES.getCode().equals(prom.getGsphType())) {
                //查询系列促销列表
                List<SdPromSeriesSet> sdPromSeriesSetList = sdPromSeriesSetService.list(new LambdaQueryWrapper<SdPromSeriesSet>()
                        .eq(SdPromSeriesSet::getClient, user.getClient())
                        .eq(SdPromSeriesSet::getGspssVoucherId, prom.getGsphVoucherId()));
                prom.setSeriesSetList(sdPromSeriesSetList.stream().map(s -> {
                    SavePromListVO.Prom.SeriesSet seriesSet = new SavePromListVO.Prom.SeriesSet();
                    BeanUtils.copyProperties(s, seriesSet);
                    return seriesSet;
                }).collect(Collectors.toList()));
                prom.getSeriesSetList().forEach(s -> {
                    List<SdPromSeriesConds> seriesCondsList = sdPromSeriesCondsService.list(new LambdaQueryWrapper<SdPromSeriesConds>()
                            .eq(SdPromSeriesConds::getClient, user.getClient())
                            .eq(SdPromSeriesConds::getGspscVoucherId, prom.getGsphVoucherId())
                            .eq(SdPromSeriesConds::getGspscSeriesId, s.getGspssSeriesId())
                            .orderByAsc(SdPromSeriesConds::getGspscSeriesId));
                    List<SavePromListVO.Prom.SeriesSet.SeriesConds> tempSeriesCondsList = new ArrayList<>();
                    seriesCondsList.forEach(sc -> {
                        SavePromListVO.Prom.SeriesSet.SeriesConds seriesConds = new SavePromListVO.Prom.SeriesSet.SeriesConds();
                        BeanUtils.copyProperties(sc, seriesConds);
                        seriesConds.setProSelfCode(proDataMap.get(sc.getGspscProId()).getProSelfCode());
                        seriesConds.setProCommonname(proDataMap.get(sc.getGspscProId()).getProCommonname());
                        seriesConds.setProName(proDataMap.get(sc.getGspscProId()).getProName());
                        seriesConds.setProSpecs(proDataMap.get(sc.getGspscProId()).getProSpecs());
                        seriesConds.setProFactoryName(proDataMap.get(sc.getGspscProId()).getProFactoryName());
                        seriesConds.setProPym(proDataMap.get(sc.getGspscProId()).getProPym());
                        tempSeriesCondsList.add(seriesConds);
                    });
                    s.setSeriesCondsList(tempSeriesCondsList);
                });
            } else if (PromTypeEnum.PROM_GIFT.getCode().equals(prom.getGsphType())) {
                //查询赠品促销列表
                List<SdPromGiftSet> sdPromGiftSetList = sdPromGiftSetService.list(new LambdaQueryWrapper<SdPromGiftSet>()
                        .eq(SdPromGiftSet::getClient, user.getClient())
                        .eq(SdPromGiftSet::getGspgsVoucherId, prom.getGsphVoucherId())
                        .orderByAsc(SdPromGiftSet::getGspgsSeriesProId));
                prom.setGiftSetList(sdPromGiftSetList.stream().map(s -> {
                    SavePromListVO.Prom.GiftSet giftSet = new SavePromListVO.Prom.GiftSet();
                    BeanUtils.copyProperties(s, giftSet);
                    return giftSet;
                }).collect(Collectors.toList()));
                prom.getGiftSetList().forEach(s -> {
                    List<SdPromGiftConds> giftCondsList = sdPromGiftCondsService.list(new LambdaQueryWrapper<SdPromGiftConds>()
                            .eq(SdPromGiftConds::getClient, user.getClient())
                            .eq(SdPromGiftConds::getGspgcVoucherId, prom.getGsphVoucherId())
                            .eq(SdPromGiftConds::getGspgcSeriesId, s.getGspgsSeriesProId()));
                    List<SavePromListVO.Prom.GiftSet.GiftConds> tempGiftCondsList = new ArrayList<>();
                    giftCondsList.forEach(sc -> {
                        SavePromListVO.Prom.GiftSet.GiftConds giftConds = new SavePromListVO.Prom.GiftSet.GiftConds();
                        BeanUtils.copyProperties(sc, giftConds);
                        giftConds.setProSelfCode(proDataMap.get(sc.getGspgcProId()).getProSelfCode());
                        giftConds.setProCommonname(proDataMap.get(sc.getGspgcProId()).getProCommonname());
                        giftConds.setProName(proDataMap.get(sc.getGspgcProId()).getProName());
                        giftConds.setProSpecs(proDataMap.get(sc.getGspgcProId()).getProSpecs());
                        giftConds.setProFactoryName(proDataMap.get(sc.getGspgcProId()).getProFactoryName());
                        giftConds.setProPym(proDataMap.get(sc.getGspgcProId()).getProPym());
                        tempGiftCondsList.add(giftConds);
                    });
                    s.setGiftCondsList(tempGiftCondsList);
                    List<SdPromGiftResult> giftResultList = sdPromGiftResultService.list(new LambdaQueryWrapper<SdPromGiftResult>()
                            .eq(SdPromGiftResult::getClient, user.getClient())
                            .eq(SdPromGiftResult::getGspgrVoucherId, prom.getGsphVoucherId())
                            .eq(SdPromGiftResult::getGspgrSeriesId, s.getGspgsSeriesProId()));
                    List<SavePromListVO.Prom.GiftSet.GiftResult> tempGiftResultList = new ArrayList<>();
                    giftResultList.forEach(sc -> {
                        SavePromListVO.Prom.GiftSet.GiftResult giftResult = new SavePromListVO.Prom.GiftSet.GiftResult();
                        BeanUtils.copyProperties(sc, giftResult);
                        giftResult.setProSelfCode(proDataMap.get(sc.getGspgrProId()).getProSelfCode());
                        giftResult.setProCommonname(proDataMap.get(sc.getGspgrProId()).getProCommonname());
                        giftResult.setProName(proDataMap.get(sc.getGspgrProId()).getProName());
                        giftResult.setProSpecs(proDataMap.get(sc.getGspgrProId()).getProSpecs());
                        giftResult.setProFactoryName(proDataMap.get(sc.getGspgrProId()).getProFactoryName());
                        giftResult.setProPym(proDataMap.get(sc.getGspgrProId()).getProPym());
                        tempGiftResultList.add(giftResult);
                    });
                    s.setGiftResultList(tempGiftResultList);
                });
            } else if (PromTypeEnum.PROM_COMBIN.getCode().equals(prom.getGsphType())) {
                //查询组合促销列表
                List<SdPromAssoSet> sdPromAssoSetList = sdPromAssoSetService.list(new LambdaQueryWrapper<SdPromAssoSet>()
                        .eq(SdPromAssoSet::getClient, user.getClient())
                        .eq(SdPromAssoSet::getGspasVoucherId, prom.getGsphVoucherId())
                        .orderByAsc(SdPromAssoSet::getGspasSeriesId));
                prom.setAssoSetList(sdPromAssoSetList.stream().map(s -> {
                    SavePromListVO.Prom.AssoSet assoSet = new SavePromListVO.Prom.AssoSet();
                    BeanUtils.copyProperties(s, assoSet);
                    return assoSet;
                }).collect(Collectors.toList()));
                prom.getAssoSetList().forEach(s -> {
                    List<SdPromAssoConds> assoCondsList = sdPromAssoCondsService.list(new LambdaQueryWrapper<SdPromAssoConds>()
                            .eq(SdPromAssoConds::getClient, user.getClient())
                            .eq(SdPromAssoConds::getGspacVoucherId, prom.getGsphVoucherId())
                            .eq(SdPromAssoConds::getGspacSeriesId, s.getGspasSeriesId()));
                    List<SavePromListVO.Prom.AssoSet.AssoConds> tempAssoCondsList = new ArrayList<>();
                    assoCondsList.forEach(sc -> {
                        SavePromListVO.Prom.AssoSet.AssoConds assoConds = new SavePromListVO.Prom.AssoSet.AssoConds();
                        BeanUtils.copyProperties(sc, assoConds);
                        assoConds.setProSelfCode(proDataMap.get(sc.getGspacProId()).getProSelfCode());
                        assoConds.setProCommonname(proDataMap.get(sc.getGspacProId()).getProCommonname());
                        assoConds.setProName(proDataMap.get(sc.getGspacProId()).getProName());
                        assoConds.setProSpecs(proDataMap.get(sc.getGspacProId()).getProSpecs());
                        assoConds.setProFactoryName(proDataMap.get(sc.getGspacProId()).getProFactoryName());
                        assoConds.setProPym(proDataMap.get(sc.getGspacProId()).getProPym());
                        tempAssoCondsList.add(assoConds);
                    });
                    s.setAssoCondsList(tempAssoCondsList);
                    List<SdPromAssoResult> assoResultList = sdPromAssoResultService.list(new LambdaQueryWrapper<SdPromAssoResult>()
                            .eq(SdPromAssoResult::getClient, user.getClient())
                            .eq(SdPromAssoResult::getGsparVoucherId, prom.getGsphVoucherId())
                            .eq(SdPromAssoResult::getGsparSeriesId, s.getGspasSeriesId()));
                    List<SavePromListVO.Prom.AssoSet.AssoResult> tempAssoResultList = new ArrayList<>();
                    assoResultList.forEach(sc -> {
                        SavePromListVO.Prom.AssoSet.AssoResult assoResult = new SavePromListVO.Prom.AssoSet.AssoResult();
                        BeanUtils.copyProperties(sc, assoResult);
                        assoResult.setProSelfCode(proDataMap.get(sc.getGsparProId()).getProSelfCode());
                        assoResult.setProCommonname(proDataMap.get(sc.getGsparProId()).getProCommonname());
                        assoResult.setProName(proDataMap.get(sc.getGsparProId()).getProName());
                        assoResult.setProSpecs(proDataMap.get(sc.getGsparProId()).getProSpecs());
                        assoResult.setProFactoryName(proDataMap.get(sc.getGsparProId()).getProFactoryName());
                        assoResult.setProPym(proDataMap.get(sc.getGsparProId()).getProPym());
                        tempAssoResultList.add(assoResult);
                    });
                    s.setAssoResultList(tempAssoResultList);
                });
            } else if (PromTypeEnum.PROM_CHMED.getCode().equals(prom.getGsphType())) {
                //查询中药类促销
                List<SdPromChmedSet> sdPromChmedSetList = sdPromChmedSetService.list(new LambdaQueryWrapper<SdPromChmedSet>()
                        .eq(SdPromChmedSet::getClient, user.getClient())
                        .eq(SdPromChmedSet::getGspmsVoucherId, prom.getGsphVoucherId()));
                prom.setChmedSetList(sdPromChmedSetList.stream().map(s -> {
                    SavePromListVO.Prom.ChmedSet chmedSet = new SavePromListVO.Prom.ChmedSet();
                    BeanUtils.copyProperties(s, chmedSet);
                    return chmedSet;
                }).collect(Collectors.toList()));
            }
        }
        return savePromListVO;
    }

    private String getGsphAllStatus(List<SdPromHead> sdPromHeadList) {
        List<SdPromHead> unverifyList = sdPromHeadList.stream().filter(s -> "0".equals(s.getGsphStatus())).collect(Collectors.toList());
        if (unverifyList.size() == sdPromHeadList.size()) {
            return "0";
        }
        if (unverifyList.size() == 0) {
            return "2";
        }
        return "1";
    }

    @Override
    @Transactional
    public void updatePromList(SavePromListVO savePromListVO) {
        TokenUser user = commonService.getLoginInfo();
        //过滤审核通过的
        savePromListVO.setPromList(savePromListVO.getPromList().stream().filter(s -> "0".equals(s.getGsphStatus())).collect(Collectors.toList()));
        //促销单列表
        List<SavePromListVO.Prom> promList = savePromListVO.getPromList();
        //门店列表
        List<String> stoCodeList = savePromListVO.getStoCodeList();
        //生成活动主表
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        List<SdPromHead> promHeadList = new ArrayList<>();
        List<SdPromUnitarySet> proSingleList = new ArrayList<>();
        List<SdPromSeriesSet> promSeriesSetList = new ArrayList<>();
        List<SdPromSeriesConds> promSeriesCondsList = new ArrayList<>();
        List<SdPromGiftSet> promGiftSetList = new ArrayList<>();
        List<SdPromGiftConds> promGiftCondsList = new ArrayList<>();
        List<SdPromGiftResult> promGiftResultList = new ArrayList<>();
        List<SdPromAssoSet> promAssoSetList = new ArrayList<>();
        List<SdPromAssoConds> promAssoCondsList = new ArrayList<>();
        List<SdPromAssoResult> promAssoResultList = new ArrayList<>();
        List<SdPromChmedSet> sdPromChmedSetList = new ArrayList<>();
        //获取单号
        String curVoucherId = getVoucherId();
        Long voucherNum = Long.valueOf(curVoucherId.substring(("CX" + year).length()));
        //获取营销主键
        Integer marketId = savePromListVO.getGsphMarketid();
        for (int i = 0; i < promList.size(); i++) {
            voucherNum++;
            while (redisClient.exists(VOUCHER_PREFIX + voucherNum)) {
                voucherNum++;
            }
            redisClient.set(VOUCHER_PREFIX + voucherNum, VOUCHER_PREFIX + voucherNum, 120);
            String voucherId = "CX" + year + String.format("%0" + 10 + "d", voucherNum);
            //生成主表列表
            promHeadList.addAll(createPromHeadList(marketId, savePromListVO, promList.get(i), stoCodeList, voucherId, user.getClient(), user.getUserId()));
            if (PromTypeEnum.PROM_SINGLE.getCode().equals(promList.get(i).getGsphType())) {
                //添加单品促销
                proSingleList.addAll(createPromSingleList(promList.get(i), voucherId, user.getClient()));
            } else if (PromTypeEnum.PROM_SERIES.getCode().equals(promList.get(i).getGsphType())) {
                //添加系列促销
                addPromSeries(promSeriesSetList, promSeriesCondsList, promList.get(i), voucherId, user.getClient());
            } else if (PromTypeEnum.PROM_GIFT.getCode().equals(promList.get(i).getGsphType())) {
                //添加赠品促销
                addPromGift(promGiftSetList, promGiftCondsList, promGiftResultList, promList.get(i), voucherId, user.getClient());
            } else if (PromTypeEnum.PROM_COMBIN.getCode().equals(promList.get(i).getGsphType())) {
                //添加组合促销
                addPromAsso(promAssoSetList, promAssoCondsList, promAssoResultList, promList.get(i), voucherId, user.getClient());
            } else if (PromTypeEnum.PROM_CHMED.getCode().equals(promList.get(i).getGsphType())) {
                //添加单品促销
                sdPromChmedSetList.addAll(createPromChmed(promList.get(i), voucherId, user.getClient()));
            }
        }
        //查询原来未审核单号并删除
        Set<String> delVoucherId = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphMarketid, savePromListVO.getGsphMarketid())
                .eq(SdPromHead::getGsphStatus, "0")).stream().map(SdPromHead::getGsphVoucherId).collect(Collectors.toSet());
        //删除该单号所有数据
        if (!CollectionUtils.isEmpty(delVoucherId)) {
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, delVoucherId));
            sdPromUnitarySetService.remove(new LambdaQueryWrapper<SdPromUnitarySet>()
                    .eq(SdPromUnitarySet::getClient, user.getClient())
                    .in(SdPromUnitarySet::getGspusVoucherId, delVoucherId));
            sdPromSeriesSetService.remove(new LambdaQueryWrapper<SdPromSeriesSet>()
                    .eq(SdPromSeriesSet::getClient, user.getClient())
                    .in(SdPromSeriesSet::getGspssVoucherId, delVoucherId));
            sdPromSeriesCondsService.remove(new LambdaQueryWrapper<SdPromSeriesConds>()
                    .eq(SdPromSeriesConds::getClient, user.getClient())
                    .in(SdPromSeriesConds::getGspscVoucherId, delVoucherId));
            sdPromGiftSetService.remove(new LambdaQueryWrapper<SdPromGiftSet>()
                    .eq(SdPromGiftSet::getClient, user.getClient())
                    .in(SdPromGiftSet::getGspgsVoucherId, delVoucherId));
            sdPromGiftCondsService.remove(new LambdaQueryWrapper<SdPromGiftConds>()
                    .eq(SdPromGiftConds::getClient, user.getClient())
                    .in(SdPromGiftConds::getGspgcVoucherId, delVoucherId));
            sdPromGiftResultService.remove(new LambdaQueryWrapper<SdPromGiftResult>()
                    .eq(SdPromGiftResult::getClient, user.getClient())
                    .in(SdPromGiftResult::getGspgrVoucherId, delVoucherId));
            sdPromAssoSetService.remove(new LambdaQueryWrapper<SdPromAssoSet>()
                    .eq(SdPromAssoSet::getClient, user.getClient())
                    .in(SdPromAssoSet::getGspasVoucherId, delVoucherId));
            sdPromAssoCondsService.remove(new LambdaQueryWrapper<SdPromAssoConds>()
                    .eq(SdPromAssoConds::getClient, user.getClient())
                    .in(SdPromAssoConds::getGspacVoucherId, delVoucherId));
            sdPromAssoResultService.remove(new LambdaQueryWrapper<SdPromAssoResult>()
                    .eq(SdPromAssoResult::getClient, user.getClient())
                    .in(SdPromAssoResult::getGsparVoucherId, delVoucherId));
            sdPromChmedSetService.remove(new LambdaQueryWrapper<SdPromChmedSet>()
                    .eq(SdPromChmedSet::getClient, user.getClient())
                    .in(SdPromChmedSet::getGspmsVoucherId, delVoucherId));
        }
        //保存新加数据
        if (!CollectionUtils.isEmpty(promHeadList)) {
            promHeadParaEmptyStrToNull(promHeadList);
            sdPromHeadMapper.insertList(promHeadList);
        }
        if (!CollectionUtils.isEmpty(proSingleList)) {
            proSingleParaEmptyStrToNull(proSingleList);
            sdPromUnitarySetService.saveBatch(proSingleList);
        }
        if (!CollectionUtils.isEmpty(promSeriesSetList)) {
            promSeriesSetParaEmptyStrToNull(promSeriesSetList);
            sdPromSeriesSetService.saveBatch(promSeriesSetList);
        }
        if (!CollectionUtils.isEmpty(promSeriesCondsList)) {
            promSeriesCondsParaEmptyStrToNull(promSeriesCondsList);
            sdPromSeriesCondsService.saveBatch(promSeriesCondsList);
        }
        if (!CollectionUtils.isEmpty(promGiftSetList)) {
            promGiftSetParaEmptyStrToNull(promGiftSetList);
            sdPromGiftSetService.saveBatch(promGiftSetList);
        }
        if (!CollectionUtils.isEmpty(promGiftCondsList)) {
            promGiftCondsParaEmptyStrToNull(promGiftCondsList);
            sdPromGiftCondsService.saveBatch(promGiftCondsList);
        }
        if (!CollectionUtils.isEmpty(promGiftResultList)) {
            promGiftResultParaEmptyStrToNull(promGiftResultList);
            sdPromGiftResultService.saveBatch(promGiftResultList);
        }
        if (!CollectionUtils.isEmpty(promAssoSetList)) {
            promAssoSetListParaEmptyStrToNull(promAssoSetList);
            sdPromAssoSetService.saveBatch(promAssoSetList);
        }
        if (!CollectionUtils.isEmpty(promAssoCondsList)) {
            promAssoCondsParaEmptyStrToNull(promAssoCondsList);
            sdPromAssoCondsService.saveBatch(promAssoCondsList);
        }
        if (!CollectionUtils.isEmpty(promAssoResultList)) {
            promAssoResultParaEmptyStrToNull(promAssoResultList);
            sdPromAssoResultService.saveBatch(promAssoResultList);
        }
        if (!CollectionUtils.isEmpty(sdPromChmedSetList)) {
            sdPromChmedSetService.saveBatch(sdPromChmedSetList);
        }
    }

    @Override
    public List<StoreData> queryPromStoreList(QueryPromStoreListVO queryPromStoreListVO) {
        TokenUser user = commonService.getLoginInfo();
        return sdPromHeadMapper.queryPromStoreList(queryPromStoreListVO.getGsphMarketid(), user.getClient());
    }

    @Override
    public SavePromListVO queryPromVoucherList(QueryPromStoreListVO queryPromStoreListVO) {
        TokenUser user = commonService.getLoginInfo();
        List<SdPromHead> promHeadList = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .select(SdPromHead::getGsphName, SdPromHead::getGsphTheme, SdPromHead::getGsphRemarks
                        , SdPromHead::getGsphVoucherId, SdPromHead::getGsphType, SdPromHead::getGsphStatus, SdPromHead::getGsphMarketid)
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphMarketid, queryPromStoreListVO.getGsphMarketid())
                .orderByAsc(SdPromHead::getGsphVoucherId)
                .groupBy(SdPromHead::getGsphVoucherId));
        SavePromListVO retVo = new SavePromListVO();
        if (CollectionUtils.isEmpty(promHeadList)) {
            return retVo;
        }
        retVo.setGsphName(promHeadList.get(0).getGsphName());
        retVo.setGsphTheme(promHeadList.get(0).getGsphTheme());
        retVo.setGsphRemarks(promHeadList.get(0).getGsphRemarks());
        retVo.setGsphMarketid(promHeadList.get(0).getGsphMarketid());
        List<SavePromListVO.Prom> promList = new ArrayList<SavePromListVO.Prom>();
        promHeadList.forEach(sdPromHead -> {
            SavePromListVO.Prom prom = new SavePromListVO.Prom();
            prom.setGsphVoucherId(sdPromHead.getGsphVoucherId());
            prom.setGsphType(sdPromHead.getGsphType());
            prom.setGsphStatus(sdPromHead.getGsphStatus());
            promList.add(prom);
        });
        retVo.setPromList(promList);
        return retVo;
    }

    @Override
    @Transactional
    public void updatePromVoucherStatus(UpdatePromVoucherStatusVO updatePromVoucherStatusVO) {
        TokenUser user = commonService.getLoginInfo();
        sdPromHeadMapper.updatePromVoucherStatus(updatePromVoucherStatusVO.getGsphMarketid(), updatePromVoucherStatusVO.getGsphVoucherId(), updatePromVoucherStatusVO.getGsphStatus(), user.getClient());
    }

    @Override
    public IPage<QueryProListByStoreDto> queryProListByStore(QueryProListByStoreVO queryProListByStoreVO) {
        List<QueryProListByStoreVO.ProSto> rowList = filterQueryProListByStoreVOProSto(queryProListByStoreVO.getHasSelectedRowList());
        if (Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()).equals(queryProListByStoreVO.getHasSelectedRow()) &&
                Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()).equals(queryProListByStoreVO.getUnHasSelectedRow())) {
            rowList.clear();
        } else if (Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()).equals(queryProListByStoreVO.getHasSelectedRow())
                && CollectionUtils.isEmpty(rowList)) {
            return new Page<>();
        }
        TokenUser user = commonService.getLoginInfo();
        Page<QueryProListByStoreDto> page = new Page<>(queryProListByStoreVO.getPageNum(), queryProListByStoreVO.getPageSize());
        IPage<QueryProListByStoreDto> productBusinessList = sdPromHeadMapper.queryProListByStore(page,
                queryProListByStoreVO.getStoCodeList(),
                queryProListByStoreVO.getProName(),
                queryProListByStoreVO.getStoreName(),
                user.getClient(),
                queryProListByStoreVO.getProSlaeClass(),
                queryProListByStoreVO.getProClass(),
                queryProListByStoreVO.getProStorageArea(),
                rowList,
                queryProListByStoreVO.getHasSelectedRow(),
                queryProListByStoreVO.getUnHasSelectedRow()
        );
        return productBusinessList;
    }

    /**
     * 批量设置折扣
     *
     * @param queryProListByStoreVO
     * @return
     */
    @Override
    public List<QueryProListByStoreDto> setDiscountBatch(QueryProListByStoreVO queryProListByStoreVO) {
        List<QueryProListByStoreVO.ProSto> rowList = filterQueryProListByStoreVOProSto(queryProListByStoreVO.getHasSelectedRowList());
        if (Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()).equals(queryProListByStoreVO.getHasSelectedRow())
                && CollectionUtils.isEmpty(rowList)) {
            return new ArrayList<>();
        }
        TokenUser user = commonService.getLoginInfo();
        return sdPromHeadMapper.queryProListByStoreBatch(
                queryProListByStoreVO.getStoCodeList(),
                queryProListByStoreVO.getProName(),
                queryProListByStoreVO.getStoreName(),
                user.getClient(),
                queryProListByStoreVO.getProSlaeClass(),
                queryProListByStoreVO.getProClass(),
                queryProListByStoreVO.getProStorageArea(),
                rowList,
                queryProListByStoreVO.getHasSelectedRow(),
                queryProListByStoreVO.getUnHasSelectedRow()
        );
    }

    @Override
    public List<String> getProSlaeClassList() {
        TokenUser user = commonService.getLoginInfo();
        List<ProductBusiness> list = productBusinessMapper.selectList(new QueryWrapper<ProductBusiness>().select("DISTINCT PRO_SLAE_CLASS")
                .eq("CLIENT", user.getClient())
                .apply(" PRO_SLAE_CLASS IS NOT NULL AND LENGTH(PRO_SLAE_CLASS) > 0 ")
        );
        List<String> result = list.stream().map(ProductBusiness::getProSlaeClass).collect(Collectors.toList());
        return result;
    }

    /**
     * 是否发送电子券设置获取
     *
     * @return
     */
    @Override
    public Result getTicketFlag() {
        TokenUser user = commonService.getLoginInfo();
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(
                new LambdaQueryWrapper<ClSystemPara>().eq(ClSystemPara::getClient, user.getClient())
                        .eq(ClSystemPara::getGcspId, SEND_TICKET_FLAG)
        );
        String result = clSystemPara == null ? "0" : clSystemPara.getGcspPara1();
        return ResultUtil.success(result);
    }

    /**
     * 是否发送电子券设置设置
     *
     * @param gcspPara1
     */
    @Override
    public void setTicketFlag(String gcspPara1) {
        TokenUser user = commonService.getLoginInfo();
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(
                new LambdaQueryWrapper<ClSystemPara>().eq(ClSystemPara::getClient, user.getClient())
                        .eq(ClSystemPara::getGcspId, SEND_TICKET_FLAG)
        );
        if (clSystemPara == null) {
            clSystemPara = new ClSystemPara();
            clSystemPara.setClient(user.getClient());
            clSystemPara.setGcspId(SEND_TICKET_FLAG);
            clSystemPara.setGcspName("是否发送电子券开关");
            clSystemPara.setGcspParaRemark("0：关 1：开");
            clSystemPara.setGcspPara1(gcspPara1);
            clSystemPara.setGcspUpdateEmp(user.getUserId());
            clSystemPara.setGcspUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
            clSystemPara.setGcspUpdateTime(DateUtils.getCurrentTimeStrHHMMSS());
            clSystemParaMapper.insert(clSystemPara);
        } else {
            clSystemPara.setGcspPara1(gcspPara1);
            clSystemPara.setGcspUpdateEmp(user.getUserId());
            clSystemPara.setGcspUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
            clSystemPara.setGcspUpdateTime(DateUtils.getCurrentTimeStrHHMMSS());
            clSystemParaMapper.update(clSystemPara,
                    new LambdaQueryWrapper<ClSystemPara>().eq(ClSystemPara::getClient, user.getClient())
                            .eq(ClSystemPara::getGcspId, SEND_TICKET_FLAG)
            );
        }
    }

    @Override
    public Page<PromGoodsListVO> queryPromGoodsList(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        Page<PromGoodsListVO> page = new Page<>(queryPromListVO.getPageNum(), queryPromListVO.getPageSize());
        Page<PromGoodsListVO> promGoodsListDTOPage = sdPromHeadMapper.queryPromGoodsList(page, queryPromListVO, user.getClient());
        return promGoodsListDTOPage;
    }

    @Override
    public Page<SdPromGiftResult> queryPromGiftProList(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        Page<SdPromGiftResult> page = new Page<>(queryPromListVO.getPageNum(), queryPromListVO.getPageSize());
        Page<SdPromGiftResult> promGiftResultPage = sdPromHeadMapper.queryPromGiftProList(page, queryPromListVO.getGsphVoucherId(), user.getClient(), queryPromListVO.getProSelfCode());
        return promGiftResultPage;
    }

    @Override
    public Page<SdPromAssoConds> queryPromAssoProList(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        Page<SdPromAssoConds> page = new Page<>(queryPromListVO.getPageNum(), queryPromListVO.getPageSize());
        Page<SdPromAssoConds> assoCondsPage = sdPromHeadMapper.queryPromAssoProList(page, queryPromListVO.getGsphVoucherId(), user.getClient(), queryPromListVO.getProSelfCode());
        return assoCondsPage;
    }

    @Override
    public List<PromUnitaryProductVO> queryPromUnitaryGood(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        return sdPromHeadMapper.queryPromUnitaryGood(queryPromListVO, user.getClient());
    }

    @Override
    public List<PromGiftProductVO> queryPromGiftGood(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        List<PromGiftProductVO> promGiftProductVOS = sdPromHeadMapper.queryPromGiftGood(queryPromListVO, user.getClient());
        return promGiftProductVOS;
    }

    @Override
    public List<PromSeriesVO> queryPromSeriesGood(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        return sdPromHeadMapper.queryPromSeriesGood(queryPromListVO, user.getClient());
    }

    @Override
    public List<PromAssoVO> queryPromAssoGood(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        return sdPromHeadMapper.queryPromAssoGood(queryPromListVO, user.getClient());
    }

    @Override
    public List<SdPromHead> queryPromListForVoucherId(QueryPromListVO queryPromListVO) {
        TokenUser user = commonService.getLoginInfo();
        return sdPromHeadMapper.queryPromListForVoucherId(queryPromListVO, user.getClient());
    }

    private List<QueryProListByStoreVO.ProSto> filterQueryProListByStoreVOProSto(List<QueryProListByStoreVO.ProSto> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.stream().filter(Objects::nonNull).filter(item -> {
            return !ObjectUtils.isEmpty(item.getGsphpPrice())
                    || !ObjectUtils.isEmpty(item.getGsphpRebate())
                    || !ObjectUtils.isEmpty(item.getGsphpInteFlag())
                    || !ObjectUtils.isEmpty(item.getGsphpInteRate())
                    || !ObjectUtils.isEmpty(item.getGsphsPrice())
                    || !ObjectUtils.isEmpty(item.getGsphsRebate())
                    || !ObjectUtils.isEmpty(item.getGsphsInteFlag())
                    || !ObjectUtils.isEmpty(item.getGsphsInteRate());
        }).collect(Collectors.toList());
    }

    @Override
    public PromHyDto queryHySet(String storeCode) {
        TokenUser user = commonService.getLoginInfo();
        List<SdPromHead> promHeadList = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphType, "PROM_HYJ"));
        if (CollectionUtils.isEmpty(promHeadList)) {
            return null;
        }

        PromHyDto promHyDto = new PromHyDto();
        promHyDto.setGsphBeginDate(promHeadList.get(0).getGsphBeginDate());
        promHyDto.setGsphBeginTime(promHeadList.get(0).getGsphBeginTime());
        promHyDto.setGsphEndDate(promHeadList.get(0).getGsphEndDate());
        promHyDto.setGsphEndTime(promHeadList.get(0).getGsphEndTime());
        promHyDto.setGsphDateFrequency(promHeadList.get(0).getGsphDateFrequency());
        promHyDto.setGsphWeekFrequency(promHeadList.get(0).getGsphWeekFrequency());
        if (StringUtils.isNotBlank(promHyDto.getGsphDateFrequency())) {
            promHyDto.setGsphNlFlag(promHeadList.get(0).getGsphNlFlag());
        }
        //组装门店列表
        List<String> stoCodeList = promHeadList.stream().map(SdPromHead::getGsphBrId).collect(Collectors.toList());
        promHyDto.setStoreCodeList(stoCodeList);

        //查询商品信息
//        List<String> voucherIds = promHeadList.stream().map(SdPromHead::getGsphVoucherId).collect(Collectors.toList());
        if (StringUtils.isBlank(storeCode)) {
            // 门店最小值
            SdPromHead sdPromHead = promHeadList.stream().filter(Objects::nonNull).min(Comparator.comparing(SdPromHead::getGsphBrId)).orElse(new SdPromHead());
            List<String> voucherIds = Collections.singletonList(sdPromHead.getGsphVoucherId());
            List<PromHyDto.HySet> promHySetList = sdPromHeadMapper.queryHySetByVoucherIds(user.getClient(), voucherIds);
            promHyDto.setHySetList(promHySetList);
        } else {
            SdPromHead sdPromHead = promHeadList.stream().filter(Objects::nonNull)
                    .filter(item -> storeCode.equals(item.getGsphBrId()))
                    .findFirst().orElse(null);
            if (ObjectUtils.isEmpty(sdPromHead)) {
                promHyDto.setHySetList(Collections.emptyList());
            } else {
                // 门店最小值
                List<String> voucherIds = Collections.singletonList(sdPromHead.getGsphVoucherId());
                List<PromHyDto.HySet> promHySetList = sdPromHeadMapper.queryHySetByVoucherIds(user.getClient(), voucherIds);
                promHyDto.setHySetList(promHySetList);
            }

        }
        return promHyDto;
    }

    @Override
    public PromHyDto queryHyrDiscount() {
        TokenUser user = commonService.getLoginInfo();
        List<SdPromHead> promHeadList = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphType, "PROM_HYRZ"));
        if (CollectionUtils.isEmpty(promHeadList)) {
            return null;
        }
        PromHyDto promHyDto = new PromHyDto();
        promHyDto.setGsphBeginDate(promHeadList.get(0).getGsphBeginDate());
        promHyDto.setGsphBeginTime(promHeadList.get(0).getGsphBeginTime());
        promHyDto.setGsphEndDate(promHeadList.get(0).getGsphEndDate());
        promHyDto.setGsphEndTime(promHeadList.get(0).getGsphEndTime());
        promHyDto.setGsphDateFrequency(promHeadList.get(0).getGsphDateFrequency());
        promHyDto.setGsphWeekFrequency(promHeadList.get(0).getGsphWeekFrequency());
        if (StringUtils.isNotBlank(promHyDto.getGsphDateFrequency())) {
            promHyDto.setGsphNlFlag(promHeadList.get(0).getGsphNlFlag());
        }
        //组装门店列表
        List<String> stoCodeList = promHeadList.stream().map(SdPromHead::getGsphBrId).collect(Collectors.toList());
        promHyDto.setStoreCodeList(stoCodeList);
        List<String> voucherIds = promHeadList.stream().map(SdPromHead::getGsphVoucherId).collect(Collectors.toList());
        List<PromHyDto.HyrDiscount> hyrDiscountList = sdPromHeadMapper.queryHyrDiscountByVoucherIds(user.getClient(), voucherIds);
        promHyDto.setHyrDiscountList(hyrDiscountList);
        return promHyDto;
    }

    @Override
    public PromHyDto queryHyrPrice(String storeCode) {
        TokenUser user = commonService.getLoginInfo();
        List<SdPromHead> promHeadList = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .eq(SdPromHead::getClient, user.getClient())
                .eq(SdPromHead::getGsphType, "PROM_HYRJ"));
        if (CollectionUtils.isEmpty(promHeadList)) {
            return null;
        }
        PromHyDto promHyDto = new PromHyDto();
        promHyDto.setGsphBeginDate(promHeadList.get(0).getGsphBeginDate());
        promHyDto.setGsphBeginTime(promHeadList.get(0).getGsphBeginTime());
        promHyDto.setGsphEndDate(promHeadList.get(0).getGsphEndDate());
        promHyDto.setGsphEndTime(promHeadList.get(0).getGsphEndTime());
        promHyDto.setGsphDateFrequency(promHeadList.get(0).getGsphDateFrequency());
        promHyDto.setGsphWeekFrequency(promHeadList.get(0).getGsphWeekFrequency());
        if (StringUtils.isNotBlank(promHyDto.getGsphDateFrequency())) {
            promHyDto.setGsphNlFlag(promHeadList.get(0).getGsphNlFlag());
        }
        //组装门店列表
        List<String> stoCodeList = promHeadList.stream().map(SdPromHead::getGsphBrId).collect(Collectors.toList());
        promHyDto.setStoreCodeList(stoCodeList);

//        List<String> voucherIds = promHeadList.stream().map(SdPromHead::getGsphVoucherId).collect(Collectors.toList());
        if (StringUtils.isBlank(storeCode)) {
            // 门店最小值
            SdPromHead sdPromHead = promHeadList.stream().filter(Objects::nonNull).min(Comparator.comparing(SdPromHead::getGsphBrId)).orElse(new SdPromHead());
            List<String> voucherIds = Collections.singletonList(sdPromHead.getGsphVoucherId());
            List<PromHyDto.HyrPrice> hyrPriceList = sdPromHeadMapper.queryHyrPriceByVoucherIds(user.getClient(), voucherIds);
            promHyDto.setHyrPriceList(hyrPriceList);
        } else {
            SdPromHead sdPromHead = promHeadList.stream().filter(Objects::nonNull)
                    .filter(item -> storeCode.equals(item.getGsphBrId()))
                    .findFirst().orElse(null);
            if (ObjectUtils.isEmpty(sdPromHead)) {
                promHyDto.setHyrPriceList(Collections.emptyList());
            } else {
                List<String> voucherIds = Collections.singletonList(sdPromHead.getGsphVoucherId());
                List<PromHyDto.HyrPrice> hyrPriceList = sdPromHeadMapper.queryHyrPriceByVoucherIds(user.getClient(), voucherIds);
                promHyDto.setHyrPriceList(hyrPriceList);
            }
        }

        return promHyDto;
    }

    @Override
    @Transactional
    public void saveHySet(PromHyDto promHyDto) {
        TokenUser user = commonService.getLoginInfo();
        String voucherPefix = "CXHYJ" + user.getClient().substring(user.getClient().length() - 4);
        // 去重
        if (CollectionUtils.isEmpty(promHyDto.getStoreCodeList())) {
            promHyDto.setStoreCodeList(promHyDto.getStoreCodeList().stream().distinct().collect(Collectors.toList()));
        }
        //门店编码列表
        List<String> stoCodeList = promHyDto.getStoreCodeList();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            return;
        }
        //单号编码列表
        Map<String, String> stoVoucherMap = stoCodeList.stream().collect(Collectors.toMap(s -> s, s -> (voucherPefix + s), (key1, key2) -> key2));
        List<SdPromHead> sdPromHeadList = new ArrayList<>();
        List<SdPromHySet> sdPromHySetList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(stoCodeList) && !CollectionUtils.isEmpty(promHyDto.getHySetList())) {
            //添加会员价主表数据
            addPromSetHead(sdPromHeadList, promHyDto, stoCodeList, stoVoucherMap, user.getClient());
            //添加会员价子表数据
            addPromHySet(sdPromHySetList, promHyDto, stoCodeList, stoVoucherMap, user.getClient());
        }
        //查询需要删除单号列表
        List<String> delVoucherIds = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .select(SdPromHead::getGsphVoucherId)
                .eq(SdPromHead::getClient, user.getClient())
                .in(SdPromHead::getGsphBrId, stoCodeList)
                .eq(SdPromHead::getGsphType, "PROM_HYJ")).stream()
                .map(SdPromHead::getGsphVoucherId).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(delVoucherIds)) {
            //删除原会员价主表数据
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, delVoucherIds));
            //删除原会员价子表数据
            sdPromHySetService.remove(new LambdaQueryWrapper<SdPromHySet>()
                    .eq(SdPromHySet::getClient, user.getClient())
                    .in(SdPromHySet::getGsphsVoucherId, delVoucherIds));
        }
        if (!CollectionUtils.isEmpty(stoVoucherMap)) {
            //删除原会员价主表数据
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, stoVoucherMap.values()));
            //删除原会员价子表数据
            sdPromHySetService.remove(new LambdaQueryWrapper<SdPromHySet>()
                    .eq(SdPromHySet::getClient, user.getClient())
                    .in(SdPromHySet::getGsphsVoucherId, stoVoucherMap.values()));
        }
        if (!CollectionUtils.isEmpty(sdPromHeadList)) {
            promHeadParaEmptyStrToNull(sdPromHeadList);
            sdPromHeadService.saveBatch(sdPromHeadList);
        }
        if (!CollectionUtils.isEmpty(sdPromHySetList)) {
            sdPromHySetParaEmptyStrToNull(sdPromHySetList);
            sdPromHySetService.saveBatch(sdPromHySetList);
        }
        for (int i = 1; i < stoCodeList.size(); i++) {
            // 根据已经保存的门店数据 复制到其他门店中
            promHySetMapper.saveOtherStoHySet(user.getClient(), stoVoucherMap.get(stoCodeList.get(0)), stoVoucherMap.get(stoCodeList.get(i)));
        }
    }

    @Override
    @Transactional
    public void saveHyrDiscount(PromHyDto promHyDto) {
        TokenUser user = commonService.getLoginInfo();
        //门店编码列表
        if (CollectionUtils.isEmpty(promHyDto.getStoreCodeList())) {
            return;
        }
        // 去重
        promHyDto.setStoreCodeList(promHyDto.getStoreCodeList().stream().distinct().collect(Collectors.toList()));
        //查询需要删除单号列表
        List<String> delVoucherIds = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .select(SdPromHead::getGsphVoucherId)
                .eq(SdPromHead::getClient, user.getClient())
                .in(SdPromHead::getGsphBrId, promHyDto.getStoreCodeList())
                .eq(SdPromHead::getGsphType, "PROM_HYRZ")).stream()
                .map(SdPromHead::getGsphVoucherId).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(delVoucherIds)) {
            //删除原会员价主表数据
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, delVoucherIds));
            //删除原会员价子表数据
            sdPromHyrDiscountService.remove(new LambdaQueryWrapper<SdPromHyrDiscount>()
                    .eq(SdPromHyrDiscount::getClient, user.getClient())
                    .in(SdPromHyrDiscount::getGsppVoucherId, delVoucherIds));
        }
        if (CollectionUtils.isEmpty(promHyDto.getHyrDiscountList())) {
            return;
        }

        String voucherPefix = "CXHYRZ" + user.getClient().substring(user.getClient().length() - 4);
        //门店编码列表
        List<String> stoCodeDiscountList = promHyDto.getHyrDiscountList().stream().filter(Objects::nonNull)
                .filter(item -> StringUtils.isNotBlank(item.getGsppRebate()))
                .map(PromHyDto.HyrDiscount::getStoCode)
                .collect(Collectors.toList());
        List<String> stoCodeList = promHyDto.getStoreCodeList().stream().filter(Objects::nonNull)
                .filter(item -> stoCodeDiscountList.contains(item)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(stoCodeList)) {
            return;
        }
        //单号编码列表
        Map<String, String> stoVoucherMap = stoCodeList.stream().collect(Collectors.toMap(s -> s, s -> (voucherPefix + s), (key1, key2) -> key2));
        if (!CollectionUtils.isEmpty(stoVoucherMap)) {
            //删除原会员价主表数据
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, stoVoucherMap.values()));
            //删除原会员价子表数据
            sdPromHyrDiscountService.remove(new LambdaQueryWrapper<SdPromHyrDiscount>()
                    .eq(SdPromHyrDiscount::getClient, user.getClient())
                    .in(SdPromHyrDiscount::getGsppVoucherId, stoVoucherMap.values()));
        }
        List<SdPromHead> sdPromHeadList = new ArrayList<>();
        List<SdPromHyrDiscount> sdPromHyrDiscountList = new ArrayList<>();
        //添加会员价主表数据
        addPromDiscountHead(sdPromHeadList, promHyDto, stoCodeList, stoVoucherMap, user.getClient());
        //添加会员价子表数据
        addPromHyDiscount(sdPromHyrDiscountList, promHyDto, stoCodeList, stoVoucherMap, user.getClient());
        if (!CollectionUtils.isEmpty(sdPromHeadList)) {
            promHeadParaEmptyStrToNull(sdPromHeadList);
            sdPromHeadService.saveBatch(sdPromHeadList);
        }
        if (!CollectionUtils.isEmpty(sdPromHyrDiscountList)) {
            sdPromHyrDiscountParaEmptyStrToNull(sdPromHyrDiscountList);
            sdPromHyrDiscountService.saveBatch(sdPromHyrDiscountList);
        }
    }

    @Override
    public List<QueryProStoreLsjDTO> queryProStoreLsj(QueryProStoreLsjVO queryProStoreLsjVO) {
        TokenUser user = commonService.getLoginInfo();
        List<QueryProStoreLsjDTO> queryProStoreLsjDTOs = sdPromHeadMapper.queryProStoreLsj(queryProStoreLsjVO.getProSelfCode(), user.getClient());
        return queryProStoreLsjDTOs;
    }

    @Override
    @Transactional
    public void saveHyrPrice(PromHyDto promHyDto) {
        TokenUser user = commonService.getLoginInfo();
        String voucherPefix = "CXHYRJ" + user.getClient().substring(user.getClient().length() - 4);
        //门店编码列表
        List<String> stoCodeList = promHyDto.getStoreCodeList();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            return;
        }
        // 去重
        promHyDto.setStoreCodeList(promHyDto.getStoreCodeList().stream().distinct().collect(Collectors.toList()));
        //单号编码列表
        Map<String, String> stoVoucherMap = stoCodeList.stream().collect(Collectors.toMap(s -> s, s -> (voucherPefix + s), (key1, key2) -> key2));
        List<SdPromHead> sdPromHeadList = new ArrayList<>();
        List<SdPromHyrPrice> sdPromHyrPriceList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(stoCodeList) && !CollectionUtils.isEmpty(promHyDto.getHyrPriceList())) {
            //添加会员价主表数据
            addPromPriceHead(sdPromHeadList, promHyDto, stoCodeList, stoVoucherMap, user.getClient());
            //添加会员价子表数据
            addPromHyrPrice(sdPromHyrPriceList, promHyDto, stoCodeList, stoVoucherMap, user.getClient());
        }
        //查询需要删除单号列表
        List<String> delVoucherIds = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .select(SdPromHead::getGsphVoucherId)
                .eq(SdPromHead::getClient, user.getClient())
                .in(SdPromHead::getGsphBrId, stoCodeList)
                .eq(SdPromHead::getGsphType, "PROM_HYRJ")).stream()
                .map(SdPromHead::getGsphVoucherId).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(delVoucherIds)) {
            //删除原会员价主表数据
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, delVoucherIds));
            //删除原会员价子表数据
            sdPromHyrPriceService.remove(new LambdaQueryWrapper<SdPromHyrPrice>()
                    .eq(SdPromHyrPrice::getClient, user.getClient())
                    .in(SdPromHyrPrice::getGsphpVoucherId, delVoucherIds));
        }
        if (!CollectionUtils.isEmpty(stoVoucherMap)) {
            //删除原会员价主表数据
            sdPromHeadService.remove(new LambdaQueryWrapper<SdPromHead>()
                    .eq(SdPromHead::getClient, user.getClient())
                    .in(SdPromHead::getGsphVoucherId, stoVoucherMap.values()));
            //删除原会员价子表数据
            sdPromHySetService.remove(new LambdaQueryWrapper<SdPromHySet>()
                    .eq(SdPromHySet::getClient, user.getClient())
                    .in(SdPromHySet::getGsphsVoucherId, stoVoucherMap.values()));
        }
        if (!CollectionUtils.isEmpty(sdPromHeadList)) {
            promHeadParaEmptyStrToNull(sdPromHeadList);
            sdPromHeadService.saveBatch(sdPromHeadList);
        }
        if (!CollectionUtils.isEmpty(sdPromHyrPriceList)) {
            sdPromHyrPriceParaEmptyStrToNull(sdPromHyrPriceList);
            sdPromHyrPriceService.saveBatch(sdPromHyrPriceList);
        }
        for (int i = 1; i < stoCodeList.size(); i++) {
            // 根据已经保存的门店数据 复制到其他门店中
            promHyrPriceMapper.saveOtherStoHySet(user.getClient(), stoVoucherMap.get(stoCodeList.get(0)), stoVoucherMap.get(stoCodeList.get(i)));
        }
    }

    /**
     * 修改促销活动状态
     *
     * @param gsphMarketid 活动id
     * @param gsphStatus
     */
    @Override
    public void updatePromStatus(Integer gsphMarketid, String gsphStatus) {
        if (ObjectUtils.isEmpty(gsphMarketid)) {
            throw new CustomResultException("活动id不能为空");
        }
        if (StringUtils.isBlank(gsphStatus)) {
            throw new CustomResultException("状态值不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        sdPromHeadMapper.updatePromStatus(gsphMarketid, gsphStatus, user.getClient());
    }

    private void addPromHyrPrice(List<SdPromHyrPrice> sdPromHyrPriceList, PromHyDto promHyDto, List<String> stoCodeList, Map<String, String> stoVoucherMap, String client) {
        AtomicLong gsphpSerial = new AtomicLong(0L);
        List<PromHyDto.HyrPrice> hyrPriceList = promHyDto.getHyrPriceList();
        if (CollectionUtils.isEmpty(hyrPriceList)) {
            return;
        }
        for (PromHyDto.HyrPrice s : hyrPriceList) {
            SdPromHyrPrice promHyrPrice = new SdPromHyrPrice();
            BeanUtils.copyProperties(s, promHyrPrice);
            promHyrPrice.setClient(client);
            promHyrPrice.setGsphpProId(s.getProSelfCode());
            promHyrPrice.setGsphpVoucherId(stoVoucherMap.get(stoCodeList.get(0)));
            promHyrPrice.setGsphpSerial(String.valueOf(gsphpSerial.incrementAndGet()));
            promHyrPrice.setGsphpInteFlag(StringUtils.isBlank(promHyrPrice.getGsphpInteFlag()) ? "1" : promHyrPrice.getGsphpInteFlag());
            promHyrPrice.setGsphpInteRate(StringUtils.isBlank(promHyrPrice.getGsphpInteRate()) ? "1" : promHyrPrice.getGsphpInteRate());
            sdPromHyrPriceList.add(promHyrPrice);
        }
    }

    private void addPromPriceHead(List<SdPromHead> sdPromHeadList, PromHyDto promHyDto, List<String> stoCodeList, Map<String, String> stoVoucherMap, String client) {
        for (String stoCode : stoCodeList) {
            SdPromHead sdPromHead = new SdPromHead();
            BeanUtils.copyProperties(promHyDto, sdPromHead);
            sdPromHead.setClient(client);
            sdPromHead.setGsphBrId(stoCode);
            sdPromHead.setGsphVoucherId(stoVoucherMap.get(stoCode));
            sdPromHead.setGsphTheme("会员日特价");
            sdPromHead.setGsphName("会员日特价");
            sdPromHead.setGsphRemarks("会员日特价");
            sdPromHead.setGsphType("PROM_HYRJ");
            sdPromHead.setGsphStatus("1");
            sdPromHeadList.add(sdPromHead);
        }
    }

    private void addPromHyDiscount(List<SdPromHyrDiscount> sdPromHyrDiscount1List, PromHyDto promHyDto, List<String> stoCodeList, Map<String, String> stoVoucherMap, String client) {
        if (CollectionUtils.isEmpty(promHyDto.getHyrDiscountList())) {
            return;
        }
        AtomicLong gsppSerial = new AtomicLong(0L);
        List<PromHyDto.HyrDiscount> hyrDiscountList = promHyDto.getHyrDiscountList();
        for (PromHyDto.HyrDiscount s : hyrDiscountList) {
            SdPromHyrDiscount promHyrDiscount = new SdPromHyrDiscount();
            BeanUtils.copyProperties(s, promHyrDiscount);
            promHyrDiscount.setGsppBeginDate(promHyDto.getGsphBeginDate());
            promHyrDiscount.setGsppBeginTime(promHyDto.getGsphBeginTime());
            promHyrDiscount.setGsppEndDate(promHyDto.getGsphEndDate());
            promHyrDiscount.setGsppEndTime(promHyDto.getGsphEndTime());
            promHyrDiscount.setGsppDateFrequency(promHyDto.getGsphDateFrequency());
            promHyrDiscount.setGsppTimeFrequency(promHyDto.getGsphWeekFrequency());
            promHyrDiscount.setClient(client);
            promHyrDiscount.setGsppVoucherId(stoVoucherMap.get(s.getStoCode()));
            promHyrDiscount.setGsppSerial(String.valueOf(gsppSerial.incrementAndGet()));
            promHyrDiscount.setGsppInteFlag(StringUtils.isEmpty(promHyrDiscount.getGsppInteFlag()) ? "1" : promHyrDiscount.getGsppInteFlag());
            promHyrDiscount.setGsppInteRate(StringUtils.isEmpty(promHyrDiscount.getGsppInteRate()) ? "1" : promHyrDiscount.getGsppInteRate());
            sdPromHyrDiscount1List.add(promHyrDiscount);
        }
    }

    private void addPromDiscountHead(List<SdPromHead> sdPromHeadList, PromHyDto promHyDto, List<String> stoCodeList, Map<String, String> stoVoucherMap, String client) {
        for (String stoCode : stoCodeList) {
            SdPromHead sdPromHead = new SdPromHead();
            BeanUtils.copyProperties(promHyDto, sdPromHead);
            sdPromHead.setClient(client);
            sdPromHead.setGsphBrId(stoCode);
            sdPromHead.setGsphVoucherId(stoVoucherMap.get(stoCode));
            sdPromHead.setGsphTheme("会员日折扣");
            sdPromHead.setGsphName("会员日折扣");
            sdPromHead.setGsphRemarks("会员日折扣");
            sdPromHead.setGsphType("PROM_HYRZ");
            sdPromHead.setGsphStatus("1");
            sdPromHeadList.add(sdPromHead);
        }
    }

    private void addPromHySet(List<SdPromHySet> sdPromHySetList, PromHyDto promHyDto, List<String> stoCodeList, Map<String, String> stoVoucherMap, String client) {
        AtomicLong gsphsSerial = new AtomicLong(0L);
        List<PromHyDto.HySet> hySetList = promHyDto.getHySetList();
        if (CollectionUtils.isEmpty(hySetList)) {
            return;
        }
        for (PromHyDto.HySet s : hySetList) {
            SdPromHySet promHySet = new SdPromHySet();
            BeanUtils.copyProperties(s, promHySet);
            promHySet.setClient(client);
            promHySet.setGsphsProId(s.getProSelfCode());
            promHySet.setGsphsVoucherId(stoVoucherMap.get(stoCodeList.get(0)));
            promHySet.setGsphsSerial(String.valueOf(gsphsSerial.incrementAndGet()));
            promHySet.setGsphsInteFlag(StringUtils.isBlank(promHySet.getGsphsInteFlag()) ? "1" : promHySet.getGsphsInteFlag());
            promHySet.setGsphsInteRate(StringUtils.isBlank(promHySet.getGsphsInteRate()) ? "1" : promHySet.getGsphsInteRate());
            sdPromHySetList.add(promHySet);
        }
    }

    private void addPromSetHead(List<SdPromHead> sdPromHeadList, PromHyDto promHyDto, List<String> stoCodeList, Map<String, String> stoVoucherMap, String client) {
        for (String stoCode : stoCodeList) {
            SdPromHead sdPromHead = new SdPromHead();
            BeanUtils.copyProperties(promHyDto, sdPromHead);
            sdPromHead.setClient(client);
            sdPromHead.setGsphBrId(stoCode);
            sdPromHead.setGsphVoucherId(stoVoucherMap.get(stoCode));
            sdPromHead.setGsphTheme("会员特价");
            sdPromHead.setGsphName("会员特价");
            sdPromHead.setGsphRemarks("会员特价");
            sdPromHead.setGsphType("PROM_HYJ");
            sdPromHead.setGsphStatus("1");
            sdPromHeadList.add(sdPromHead);
        }
    }

    private void addPromAsso(List<SdPromAssoSet> promAssoSetList, List<SdPromAssoConds> promAssoCondsList, List<SdPromAssoResult> promAssoResultList, SavePromListVO.Prom prom, String voucherId, String client) {
        List<SdPromAssoSet> sdPromAssoSetList = sdPromAssoSetService.list(new LambdaQueryWrapper<SdPromAssoSet>()
                .select(SdPromAssoSet::getGspasSerial).eq(SdPromAssoSet::getClient, client));
        Long serialSet = 0L;
        if (!CollectionUtils.isEmpty(sdPromAssoSetList)) {
            List<Long> serialList = sdPromAssoSetList.stream().map(s -> Long.valueOf(s.getGspasSerial())).sorted().collect(Collectors.toList());
            serialSet = Long.valueOf(serialList.get(0));
        }

        List<SdPromAssoConds> sdPromAssoCondsList = sdPromAssoCondsService.list(new LambdaQueryWrapper<SdPromAssoConds>()
                .select(SdPromAssoConds::getGspacSerial).eq(SdPromAssoConds::getClient, client));
        Long serialConds = 0L;
        if (!CollectionUtils.isEmpty(sdPromAssoCondsList)) {
            List<Long> serialList = sdPromAssoCondsList.stream().map(s -> Long.valueOf(s.getGspacSerial())).sorted().collect(Collectors.toList());
            serialConds = Long.valueOf(serialList.get(0));
        }

        List<SdPromAssoResult> sdPromAssoResultList = sdPromAssoResultService.list(new LambdaQueryWrapper<SdPromAssoResult>()
                .select(SdPromAssoResult::getGsparSerial).eq(SdPromAssoResult::getClient, client));
        Long serialResult = 0L;
        if (!CollectionUtils.isEmpty(sdPromAssoResultList)) {
            List<Long> serialList = sdPromAssoResultList.stream().map(s -> Long.valueOf(s.getGsparSerial())).sorted().collect(Collectors.toList());
            serialResult = Long.valueOf(serialList.get(0));
        }

        for (SavePromListVO.Prom.AssoSet assoSet : prom.getAssoSetList()) {
            SdPromAssoSet sdPromAssoSet = new SdPromAssoSet();
            BeanUtils.copyProperties(assoSet, sdPromAssoSet);
            sdPromAssoSet.setClient(client);
            sdPromAssoSet.setGspasVoucherId(voucherId);
            serialSet = serialSet + 1;
            sdPromAssoSet.setGspasSerial(String.valueOf(serialSet));
            promAssoSetList.add(sdPromAssoSet);
            for (SavePromListVO.Prom.AssoSet.AssoConds promAssoConds : assoSet.getAssoCondsList()) {
                SdPromAssoConds sdPromAssoConds = new SdPromAssoConds();
                BeanUtils.copyProperties(promAssoConds, sdPromAssoConds);
                sdPromAssoConds.setGspacProId(promAssoConds.getProSelfCode());
                sdPromAssoConds.setClient(client);
                sdPromAssoConds.setGspacVoucherId(voucherId);
                sdPromAssoConds.setGspacSeriesId(assoSet.getGspasSeriesId());
                serialConds = serialConds + 1;
                sdPromAssoConds.setGspacSerial(String.valueOf(serialConds));
                promAssoCondsList.add(sdPromAssoConds);
            }
            for (SavePromListVO.Prom.AssoSet.AssoResult promAssoResult : assoSet.getAssoResultList()) {
                SdPromAssoResult sdPromAssoResult = new SdPromAssoResult();
                BeanUtils.copyProperties(promAssoResult, sdPromAssoResult);
                sdPromAssoResult.setGsparProId(promAssoResult.getProSelfCode());
                sdPromAssoResult.setClient(client);
                sdPromAssoResult.setGsparVoucherId(voucherId);
                sdPromAssoResult.setGsparSeriesId(assoSet.getGspasSeriesId());
                serialResult = serialResult + 1;
                sdPromAssoResult.setGsparSerial(String.valueOf(serialResult));
                promAssoResultList.add(sdPromAssoResult);
            }
        }
    }

    private void addPromGift(List<SdPromGiftSet> promGiftSetList, List<SdPromGiftConds> promGiftCondsList, List<SdPromGiftResult> promGiftResultList, SavePromListVO.Prom prom, String voucherId, String client) {
        List<SdPromGiftSet> sdPromGiftSetList = sdPromGiftSetService.list(new LambdaQueryWrapper<SdPromGiftSet>()
                .select(SdPromGiftSet::getGspgsSerial).eq(SdPromGiftSet::getClient, client));
        Long serialSet = 0L;
        if (!CollectionUtils.isEmpty(sdPromGiftSetList)) {
            List<Long> serialList = sdPromGiftSetList.stream().map(s -> Long.valueOf(s.getGspgsSerial())).sorted().collect(Collectors.toList());
            serialSet = Long.valueOf(serialList.get(0));
        }

        List<SdPromGiftConds> sdPromGiftCondsList = sdPromGiftCondsService.list(new LambdaQueryWrapper<SdPromGiftConds>()
                .select(SdPromGiftConds::getGspgcSerial).eq(SdPromGiftConds::getClient, client));
        Long serialConds = 0L;
        if (!CollectionUtils.isEmpty(sdPromGiftCondsList)) {
            List<Long> serialList = sdPromGiftCondsList.stream().map(s -> Long.valueOf(s.getGspgcSerial())).sorted().collect(Collectors.toList());
            serialConds = Long.valueOf(serialList.get(0));
        }

        List<SdPromGiftResult> sdPromGiftResultList = sdPromGiftResultService.list(new LambdaQueryWrapper<SdPromGiftResult>()
                .select(SdPromGiftResult::getGspgrSerial).eq(SdPromGiftResult::getClient, client));
        Long serialResult = 0L;
        if (!CollectionUtils.isEmpty(sdPromGiftResultList)) {
            List<Long> serialList = sdPromGiftResultList.stream().map(s -> Long.valueOf(s.getGspgrSerial())).sorted().collect(Collectors.toList());
            serialResult = Long.valueOf(serialList.get(0));
        }

        for (SavePromListVO.Prom.GiftSet giftSet : prom.getGiftSetList()) {
            //判断各阶段参数是否正确
            if ("1".equals(prom.getGsphPart()) || "2".equals(prom.getGsphPart()) || "3".equals(prom.getGsphPart())) {
                if ((StringUtils.isEmpty(giftSet.getGspgsReachQty1()) && StringUtils.isEmpty(giftSet.getGspgsReachAmt1())) ||
                        (StringUtils.isNotEmpty(giftSet.getGspgsReachQty1()) && StringUtils.isNotEmpty(giftSet.getGspgsReachAmt1()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + giftSet.getGspgsSeriesProId() + "达到数量1和达到金额1有且只能填写一项");
                }
                if (StringUtils.isEmpty(giftSet.getGspgsResultQty1())) {
                    throw new CustomResultException(prom.getPromName() + ":" + giftSet.getGspgsSeriesProId() + "赠送数量1不能为空");
                }
            }
            if ("2".equals(prom.getGsphPart()) || "3".equals(prom.getGsphPart())) {
                if ((StringUtils.isEmpty(giftSet.getGspgsReachQty2()) && StringUtils.isEmpty(giftSet.getGspgsReachAmt2())) ||
                        (StringUtils.isNotEmpty(giftSet.getGspgsReachQty2()) && StringUtils.isNotEmpty(giftSet.getGspgsReachAmt2()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + giftSet.getGspgsSeriesProId() + "达到数量2和达到金额2有且只能填写一项");
                }
                if (StringUtils.isEmpty(giftSet.getGspgsResultQty2())) {
                    throw new CustomResultException(prom.getPromName() + ":" + giftSet.getGspgsSeriesProId() + "赠送数量2不能为空");
                }
            }
            if ("3".equals(prom.getGsphPart())) {
                if ((StringUtils.isEmpty(giftSet.getGspgsReachQty3()) && StringUtils.isEmpty(giftSet.getGspgsReachAmt3())) ||
                        (StringUtils.isNotEmpty(giftSet.getGspgsReachQty3()) && StringUtils.isNotEmpty(giftSet.getGspgsReachAmt3()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + giftSet.getGspgsSeriesProId() + "达到数量3和达到金额3有且只能填写一项");
                }
                if (StringUtils.isEmpty(giftSet.getGspgsResultQty3())) {
                    throw new CustomResultException(prom.getPromName() + ":" + giftSet.getGspgsSeriesProId() + "赠送数量3不能为空");
                }
            }
            SdPromGiftSet sdPromGiftSet = new SdPromGiftSet();
            BeanUtils.copyProperties(giftSet, sdPromGiftSet);
            sdPromGiftSet.setClient(client);
            sdPromGiftSet.setGspgsVoucherId(voucherId);
            serialSet = serialSet + 1;
            sdPromGiftSet.setGspgsSerial(String.valueOf(serialSet));
            promGiftSetList.add(sdPromGiftSet);
            if (!"assign1".equals(prom.getGsphPara3())) {
                for (SavePromListVO.Prom.GiftSet.GiftConds promGiftConds : giftSet.getGiftCondsList()) {
                    SdPromGiftConds sdPromGiftConds = new SdPromGiftConds();
                    BeanUtils.copyProperties(promGiftConds, sdPromGiftConds);
                    sdPromGiftConds.setGspgcProId(promGiftConds.getProSelfCode());
                    sdPromGiftConds.setClient(client);
                    sdPromGiftConds.setGspgcVoucherId(voucherId);
                    sdPromGiftConds.setGspgcSeriesId(giftSet.getGspgsSeriesProId());
                    serialConds = serialConds + 1;
                    sdPromGiftConds.setGspgcSerial(String.valueOf(serialConds));
                    promGiftCondsList.add(sdPromGiftConds);
                }
            }
            for (SavePromListVO.Prom.GiftSet.GiftResult promGiftResult : giftSet.getGiftResultList()) {
                SdPromGiftResult sdPromGiftResult = new SdPromGiftResult();
                BeanUtils.copyProperties(promGiftResult, sdPromGiftResult);
                sdPromGiftResult.setGspgrProId(promGiftResult.getProSelfCode());
                sdPromGiftResult.setClient(client);
                sdPromGiftResult.setGspgrVoucherId(voucherId);
                sdPromGiftResult.setGspgrSeriesId(giftSet.getGspgsSeriesProId());
                serialResult = serialResult + 1;
                sdPromGiftResult.setGspgrSerial(String.valueOf(serialResult));
                promGiftResultList.add(sdPromGiftResult);
            }
        }
    }

    private void addPromSeries(List<SdPromSeriesSet> promSeriesSetList, List<SdPromSeriesConds> promSeriesCondsList, SavePromListVO.Prom prom, String voucherId, String client) {
        List<SdPromSeriesSet> sdPromSeriesSetList = sdPromSeriesSetService.list(new LambdaQueryWrapper<SdPromSeriesSet>()
                .select(SdPromSeriesSet::getGspssSerial).eq(SdPromSeriesSet::getClient, client));
        Long serialSet = 0L;
        if (!CollectionUtils.isEmpty(sdPromSeriesSetList)) {
            List<Long> serialList = sdPromSeriesSetList.stream().map(s -> Long.valueOf(s.getGspssSerial())).sorted().collect(Collectors.toList());
            serialSet = Long.valueOf(serialList.get(0));
        }

        List<SdPromSeriesConds> sdPromSeriesCondsList = sdPromSeriesCondsService.list(new LambdaQueryWrapper<SdPromSeriesConds>()
                .select(SdPromSeriesConds::getGspscSerial).eq(SdPromSeriesConds::getClient, client)
                .orderByAsc(SdPromSeriesConds::getGspscSerial));
        Long serialConds = 0L;
        if (!CollectionUtils.isEmpty(sdPromSeriesCondsList)) {
            List<Long> serialList = sdPromSeriesCondsList.stream().map(s -> Long.valueOf(s.getGspscSerial())).sorted().collect(Collectors.toList());
            serialConds = Long.valueOf(serialList.get(0));
        }

        for (SavePromListVO.Prom.SeriesSet seriesSet : prom.getSeriesSetList()) {
            //判断各阶段参数是否正确
            if ("1".equals(prom.getGsphPart()) || "2".equals(prom.getGsphPart()) || "3".equals(prom.getGsphPart())) {
                if ((StringUtils.isEmpty(seriesSet.getGspssReachQty1()) && StringUtils.isEmpty(seriesSet.getGspssReachAmt1())) ||
                        (StringUtils.isNotEmpty(seriesSet.getGspssReachQty1()) && StringUtils.isNotEmpty(seriesSet.getGspssReachAmt1()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + seriesSet.getGspssSeriesId() + "达到数量1和达到金额1有且只能填写一项");
                }
                if ((StringUtils.isEmpty(seriesSet.getGspssResultAmt1()) && StringUtils.isEmpty(seriesSet.getGspssResultRebate1())) ||
                        (StringUtils.isNotEmpty(seriesSet.getGspssResultAmt1()) && StringUtils.isNotEmpty(seriesSet.getGspssResultRebate1()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + seriesSet.getGspssSeriesId() + "结果减额1和促销折扣1有且只能填写一项");
                }
            }
            if ("2".equals(prom.getGsphPart()) || "3".equals(prom.getGsphPart())) {
                if ((StringUtils.isEmpty(seriesSet.getGspssReachQty2()) && StringUtils.isEmpty(seriesSet.getGspssReachAmt2())) ||
                        (StringUtils.isNotEmpty(seriesSet.getGspssReachQty2()) && StringUtils.isNotEmpty(seriesSet.getGspssReachAmt2()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + seriesSet.getGspssSeriesId() + "达到数量2和达到金额2有且只能填写一项");
                }
                if ((StringUtils.isEmpty(seriesSet.getGspssResultAmt2()) && StringUtils.isEmpty(seriesSet.getGspssResultRebate2())) ||
                        (StringUtils.isNotEmpty(seriesSet.getGspssResultAmt2()) && StringUtils.isNotEmpty(seriesSet.getGspssResultRebate2()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + seriesSet.getGspssSeriesId() + "结果减额2和促销折扣2有且只能填写一项");
                }
            }
            if ("3".equals(prom.getGsphPart())) {
                if ((StringUtils.isEmpty(seriesSet.getGspssReachQty3()) && StringUtils.isEmpty(seriesSet.getGspssReachAmt3())) ||
                        (StringUtils.isNotEmpty(seriesSet.getGspssReachQty3()) && StringUtils.isNotEmpty(seriesSet.getGspssReachAmt3()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + seriesSet.getGspssSeriesId() + "达到数量3和达到金额3有且只能填写一项");
                }
                if ((StringUtils.isEmpty(seriesSet.getGspssResultAmt3()) && StringUtils.isEmpty(seriesSet.getGspssResultRebate3())) ||
                        (StringUtils.isNotEmpty(seriesSet.getGspssResultAmt3()) && StringUtils.isNotEmpty(seriesSet.getGspssResultRebate3()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + seriesSet.getGspssSeriesId() + "结果减额3和促销折扣3有且只能填写一项");
                }
            }
            SdPromSeriesSet sdPromSeriesSet = new SdPromSeriesSet();
            BeanUtils.copyProperties(seriesSet, sdPromSeriesSet);
            sdPromSeriesSet.setClient(client);
            sdPromSeriesSet.setGspssVoucherId(voucherId);
            serialSet = serialSet + 1;
            sdPromSeriesSet.setGspssSerial(String.valueOf(serialSet));
            promSeriesSetList.add(sdPromSeriesSet);
            if (!"assign1".equals(prom.getGsphPara3())) {
                for (SavePromListVO.Prom.SeriesSet.SeriesConds promSeriesConds : seriesSet.getSeriesCondsList()) {
                    SdPromSeriesConds sdPromSeriesConds = new SdPromSeriesConds();
                    BeanUtils.copyProperties(promSeriesConds, sdPromSeriesConds);
                    sdPromSeriesConds.setGspscProId(promSeriesConds.getProSelfCode());
                    sdPromSeriesConds.setClient(client);
                    sdPromSeriesConds.setGspscVoucherId(voucherId);
                    sdPromSeriesConds.setGspscSeriesId(seriesSet.getGspssSeriesId());
                    serialConds = serialConds + 1;
                    sdPromSeriesConds.setGspscSerial(String.valueOf(serialConds));
                    promSeriesCondsList.add(sdPromSeriesConds);
                }
            }
        }
    }

    /**
     * 中药类促销
     *
     * @param prom
     * @param voucherId
     * @param client
     * @return
     */
    private List<SdPromChmedSet> createPromChmed(SavePromListVO.Prom prom, String voucherId, String client) {
        List<SdPromChmedSet> retList = new ArrayList<>();
        Long serial = 0L;
        if (CollectionUtils.isEmpty(prom.getChmedSetList())) {
            throw new CustomResultException("请设置中药类促销");
        }
        for (SavePromListVO.Prom.ChmedSet chmedSet : prom.getChmedSetList()) {
            // 达到数量与促销折扣必填，是否会员必填
            if (StringUtils.isBlank(chmedSet.getGspmsQty1())) {
                throw new CustomResultException("达到数量不能为空");
            }
            if (StringUtils.isBlank(chmedSet.getGspmsRebate1())) {
                throw new CustomResultException("促销折扣c");
            }
            if (StringUtils.isBlank(chmedSet.getGspmsMemFlag())) {
                throw new CustomResultException("是否会员是否会员");
            }
            SdPromChmedSet sdPromChmedSet = new SdPromChmedSet();
            BeanUtils.copyProperties(chmedSet, sdPromChmedSet);
            sdPromChmedSet.setClient(client);
            sdPromChmedSet.setGspmsVoucherId(voucherId);
            serial = serial + 1;
            sdPromChmedSet.setGspmsSerial(String.valueOf(serial));
            retList.add(sdPromChmedSet);
        }
        return retList;
    }

    private List<SdPromUnitarySet> createPromSingleList(SavePromListVO.Prom prom, String voucherId, String client) {
        List<SdPromUnitarySet> retList = new ArrayList<>();

        List<SdPromUnitarySet> sdPromUnitarySetList = sdPromUnitarySetService.list(new LambdaQueryWrapper<SdPromUnitarySet>()
                .select(SdPromUnitarySet::getGspusSerial).eq(SdPromUnitarySet::getClient, client));
        Long serial = 0L;
        if (!CollectionUtils.isEmpty(sdPromUnitarySetList)) {
            List<Long> serialList = sdPromUnitarySetList.stream().map(s -> Long.valueOf(s.getGspusSerial())).sorted().collect(Collectors.toList());
            serial = Long.valueOf(serialList.get(0));
        }

        for (SavePromListVO.Prom.UnitarySet unitarySet : prom.getUnitarySetList()) {
            //判断各阶段参数是否正确
            if ("1".equals(prom.getGsphPart()) || "2".equals(prom.getGsphPart()) || "3".equals(prom.getGsphPart())) {
                if (StringUtils.isEmpty(unitarySet.getGspusQty1())) {
                    throw new CustomResultException(prom.getPromName() + ":" + unitarySet.getProSelfCode() + "达到数量1不能为空");
                }
                if ((StringUtils.isEmpty(unitarySet.getGspusRebate1()) && StringUtils.isEmpty(unitarySet.getGspusPrc1())) ||
                        (StringUtils.isNotEmpty(unitarySet.getGspusRebate1()) && StringUtils.isNotEmpty(unitarySet.getGspusPrc1()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + unitarySet.getProSelfCode() + "促销价1和促销折扣1有且只能填写一项");
                }
            }
            if ("2".equals(prom.getGsphPart()) || "3".equals(prom.getGsphPart())) {
                if (StringUtils.isEmpty(unitarySet.getGspusQty2())) {
                    throw new CustomResultException(prom.getPromName() + ":" + unitarySet.getProSelfCode() + "达到数量2不能为空");
                }
                if ((StringUtils.isEmpty(unitarySet.getGspusRebate2()) && StringUtils.isEmpty(unitarySet.getGspusPrc2())) ||
                        (StringUtils.isNotEmpty(unitarySet.getGspusRebate2()) && StringUtils.isNotEmpty(unitarySet.getGspusPrc2()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + unitarySet.getProSelfCode() + "促销价2和促销折扣2有且只能填写一项");
                }
            }
            if ("3".equals(prom.getGsphPart())) {
                if (StringUtils.isEmpty(unitarySet.getGspusQty3())) {
                    throw new CustomResultException(prom.getPromName() + ":" + unitarySet.getProSelfCode() + "达到数量3不能为空");
                }
                if ((StringUtils.isEmpty(unitarySet.getGspusRebate3()) && StringUtils.isEmpty(unitarySet.getGspusPrc3())) ||
                        (StringUtils.isNotEmpty(unitarySet.getGspusRebate3()) && StringUtils.isNotEmpty(unitarySet.getGspusPrc3()))) {
                    throw new CustomResultException(prom.getPromName() + ":" + unitarySet.getProSelfCode() + "促销价3和促销折扣3有且只能填写一项");
                }
            }
            SdPromUnitarySet sdPromUnitarySet = new SdPromUnitarySet();
            BeanUtils.copyProperties(unitarySet, sdPromUnitarySet);
            sdPromUnitarySet.setGspusProId(unitarySet.getProSelfCode());
            sdPromUnitarySet.setClient(client);
            sdPromUnitarySet.setGspusVoucherId(voucherId);
            serial = serial + 1;
            sdPromUnitarySet.setGspusSerial(String.valueOf(serial));
            if ("assign1".equals(prom.getGsphPara3()) && StringUtils.isBlank(sdPromUnitarySet.getGspusProId())) {
                sdPromUnitarySet.setGspusProId("all");
            }
            // 生成促销折扣1
            if (StringUtils.isNotBlank(sdPromUnitarySet.getGspusRebate1())) {
                sdPromUnitarySet.setGspusRebate1(NumberUtils.createBigDecimal(sdPromUnitarySet.getGspusRebate1()).setScale(4, BigDecimal.ROUND_HALF_UP).toPlainString());
            }
            // 生成促销折扣2
            if (StringUtils.isNotBlank(sdPromUnitarySet.getGspusRebate2())) {
                sdPromUnitarySet.setGspusRebate2(NumberUtils.createBigDecimal(sdPromUnitarySet.getGspusRebate2()).setScale(4, BigDecimal.ROUND_HALF_UP).toPlainString());
            }
            // 生成促销折扣3
            if (StringUtils.isNotBlank(sdPromUnitarySet.getGspusRebate3())) {
                sdPromUnitarySet.setGspusRebate3(NumberUtils.createBigDecimal(sdPromUnitarySet.getGspusRebate3()).setScale(4, BigDecimal.ROUND_HALF_UP).toPlainString());
            }
            // 1.	单品类促销设置中《是否积分》、《积分倍率》字段隐藏； 默认值
            // 是否积分
            if (StringUtils.isBlank(sdPromUnitarySet.getGspusInteFlag())) {
                sdPromUnitarySet.setGspusInteFlag("1");
            }
            // 积分倍率
            if (StringUtils.isBlank(sdPromUnitarySet.getGspusInteRate())) {
                sdPromUnitarySet.setGspusInteRate("1");
            }
            retList.add(sdPromUnitarySet);
        }
        return retList;
    }

    private List<SdPromHead> createPromHeadList(Integer marketId, SavePromListVO savePromListVO, SavePromListVO.Prom prom, List<String> stoCodeList, String voucherId, String client, String userId) {
        List<SdPromHead> retList = new ArrayList<>();
        for (String stoCode : stoCodeList) {
            SdPromHead sdPromHead = new SdPromHead();
            BeanUtils.copyProperties(savePromListVO, sdPromHead);
            sdPromHead.setClient(client);
            sdPromHead.setGsphBrId(stoCode);
            sdPromHead.setGsphVoucherId(voucherId);
            sdPromHead.setGsphType(prom.getGsphType());
            sdPromHead.setGsphPart(prom.getGsphPart());
            sdPromHead.setGsphPara1(prom.getGsphPara1());
            sdPromHead.setGsphPara2(StringUtils.isEmpty(prom.getGsphPara3()) || prom.getGsphPara3().equals("assign2") ? null : prom.getGsphPara2());
            sdPromHead.setGsphPara3(prom.getGsphPara3());
            sdPromHead.setGsphPara4(prom.getGsphPara4());
            sdPromHead.setGsphExclusion(StringUtils.isEmpty(prom.getGsphExclusion()) ? "exclusion3" : prom.getGsphExclusion());
            sdPromHead.setGsphMarketid(marketId);
            sdPromHead.setGsphFlag(prom.getGsphFlag());
            sdPromHead.setGsphStatus("0");
            sdPromHead.setGsphUpdateEmp(userId);
            sdPromHead.setGsphUpdateDate(DateUtils.getCurrentDateStrYYMMDD());

            if (PromTypeEnum.PROM_GIFT.getCode().equals(prom.getGsphType())) {
                //组合 para4 = 'gift1'
                sdPromHead.setGsphPara4("gift1");
            } else if (PromTypeEnum.PROM_COMBIN.getCode().equals(prom.getGsphType())) {
                //赠品 para2 = 'gift1'
                sdPromHead.setGsphPara2("gift1");
            } else if (PromTypeEnum.PROM_CHMED.getCode().equals(prom.getGsphType())) {
                sdPromHead.setGsphPart("1");
            }
            retList.add(sdPromHead);
        }
        return retList;
    }


    public String getVoucherId() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        TokenUser user = commonService.getLoginInfo();
        List<SdPromHead> curVoucherIds = sdPromHeadService.list(new LambdaQueryWrapper<SdPromHead>()
                .select(SdPromHead::getGsphVoucherId).eq(SdPromHead::getClient, user.getClient())
                .like(SdPromHead::getGsphVoucherId, "CX" + year)
                .orderByDesc(SdPromHead::getGsphVoucherId));
        if (CollectionUtils.isEmpty(curVoucherIds)) {
            return "CX" + year + "0000000001";
        }
        return curVoucherIds.get(0).getGsphVoucherId();
    }
}
