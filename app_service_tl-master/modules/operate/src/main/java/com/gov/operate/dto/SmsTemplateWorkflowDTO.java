package com.gov.operate.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsTemplateWorkflowDTO {


    @ApiModelProperty(value = "模板内容")
    private String smsContent;
}
