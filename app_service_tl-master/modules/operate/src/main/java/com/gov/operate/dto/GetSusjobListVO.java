package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetSusjobListVO extends Pageable {

//    @NotBlank(message = "任务id不能为空")
//    private String susJobid;

    /**
     * 任务名称
     */
    private String susJobname;

    /**
     *任务门店
     */
    private String susJobbr;

    /**
     * 创建时间开始时间
     */
    private String susTimeStart;

    /**
     * 创建时间结束时间
     */
    private String susTimeEnd;

    /**
     * 状态
     */
    private String susStatus;
}
