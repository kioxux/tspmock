package com.gov.operate.service;

import com.gov.operate.entity.WmsTupianzdrwcj;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-09-26
 */
public interface IWmsTupianzdrwcjService extends SuperService<WmsTupianzdrwcj> {

    List<WmsTupianzdrwcj> selectCjResult(String client,String stoCode, String matchCode);

    List<WmsTupianzdrwcj> selectListByStoCodeAndZdrwcjBh(String stoCode, String matchCode);

    Integer getMaxWmXhByMatchCode(String matchCode);
}
