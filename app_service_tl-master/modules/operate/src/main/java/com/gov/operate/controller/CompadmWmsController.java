package com.gov.operate.controller;


import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.compadmWms.CompadmWmsAddVO;
import com.gov.operate.dto.compadmWms.CompadmWmsEditVO;

import com.gov.operate.service.ICompadmWmsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
@RestController
@RequestMapping("/compadmWms/")
public class CompadmWmsController {

    @Resource
    private ICompadmWmsService compadmWmsService;

//    @GetMapping("getMaxCode")
//    @ApiOperation(value = "批发编码获取")
//    public Result getMaxCode() {
//        return ResultUtil.success(compadmWmsService.getMaxCode());
//    }


    @GetMapping("getCompadmWmsById")
    @ApiOperation(value = "批发详情")
    public Result getCompadmWmsById(@RequestParam(name = "compadmId", required = true) String compadmId) {
        return ResultUtil.success(compadmWmsService.getStoreInfo(compadmId));
    }

    @GetMapping("getCompadmWms")
    @ApiOperation(value = "批发列表")
    public Result getCompadmWms() {
        return ResultUtil.success(compadmWmsService.getCompadmWms());
    }

    @PostMapping("addCompadmWms")
    @ApiOperation(value = "批发新增")
    public Result addCompadmWms(@RequestBody @Valid CompadmWmsAddVO compadmWmsAddVO) {
        compadmWmsService.addCompadmWms(compadmWmsAddVO);
        return ResultUtil.success();
    }

    @PostMapping("editCompadmWms")
    @ApiOperation(value = "批发编辑")
    public Result editStore(@RequestBody @Valid CompadmWmsEditVO compadmWmsEditVO) {
        compadmWmsService.editCompadmWms(compadmWmsEditVO);
        return ResultUtil.success();
    }

}

