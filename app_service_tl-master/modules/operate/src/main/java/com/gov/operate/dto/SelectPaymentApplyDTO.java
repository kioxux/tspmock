package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author awang
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SelectPaymentApplyDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 对账单号
     */
    private String billNum;

    /**
     * 付款类型
     */
    private String businessType;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 供应商代码
     */
    private String supCode;

    /**
     * 应付金额
     */
    private BigDecimal payAmt;

    /**
     * 已付金额
     */
    private BigDecimal paidAmt;

    /**
     * 对账日期
     */
    private String createDate;

    /**
     * 银行名称
     */
    private String supBankName;

    /**
     * 银行账号
     */
    private String supBankAccount;
}
