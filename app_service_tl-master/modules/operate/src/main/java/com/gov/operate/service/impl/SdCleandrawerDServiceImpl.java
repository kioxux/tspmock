package com.gov.operate.service.impl;

import com.gov.operate.entity.SdCleandrawerD;
import com.gov.operate.mapper.SdCleandrawerDMapper;
import com.gov.operate.service.ISdCleandrawerDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Service
public class SdCleandrawerDServiceImpl extends ServiceImpl<SdCleandrawerDMapper, SdCleandrawerD> implements ISdCleandrawerDService {

}
