package com.gov.operate.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * GAIA_SD_RECIPEL_LIST
 * @author 
 */
@Data
public class GaiaSdRecipelList implements Serializable {
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编号
     */
    private String brId;

    /**
     * 处方图片编号
     */
    private String picId;

    /**
     * 处方图片名称
     */
    private String picName;

    /**
     * 处方图片地址
     */
    private String picAddress;

    /**
     * 上传人
     */
    private String emp;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 0 未绑定，1已经绑定
     */
    private String flag;

    private static final long serialVersionUID = 1L;
}