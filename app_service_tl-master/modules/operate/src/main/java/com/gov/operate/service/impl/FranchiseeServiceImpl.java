package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.DateUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.userutil.UserUtils;
import com.gov.operate.dto.FranchiseeDTO;
import com.gov.operate.dto.GetClientListVO;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.dto.Verification;
import com.gov.operate.dto.franchiseeData.FranchiseeVO;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.GaiaGrossMarginInterval;
import com.gov.operate.entity.GaiaPriceRange;
import com.gov.operate.entity.UserData;
import com.gov.operate.feign.OperationFeign;
import com.gov.operate.feign.dto.RegionDTO;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.redis.jedis.RedisClient;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-10
 */
@Service
public class FranchiseeServiceImpl extends ServiceImpl<FranchiseeMapper, Franchisee> implements IFranchiseeService {

    @Resource
    private FranchiseeMapper franchiseeMapper;
    @Resource
    private GaiaGrossMarginIntervalMapper grossMarginIntervalMapper;
    @Resource
    private GaiaPriceRangeMapper gaiaPriceRangeMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private ISmsTemplateService smsTemplateService;
    @Resource
    private CommonService commonService;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private IAuthconfiDataService authconfiService;
    @Autowired
    private StoreDataMapper storeDataMapper;
    @Autowired
    private DepDataMapper depDataMapper;
    @Autowired
    private CompadmMapper compadmMapper;
    @Autowired
    private CompadmWmsMapper compadmWmsMapper;
    @Autowired
    private IUserDataService userDataService;
    @Autowired
    private IStoreDataService storeDataService;
    @Resource
    private IMessageService messageService;

    @Resource
    private IUserRoleService userRoleService;
    @Resource
    private OperationFeign operationFeign;

    private static final Logger log = LoggerFactory.getLogger(FranchiseeServiceImpl.class);

    /**
     * 加盟商列表
     */
    @Override
    public List<Franchisee> getClientList(GetClientListVO vo) {
        List<Franchisee> list = franchiseeMapper.getClientList(vo);
        return list;
    }

    /**
     * 加盟商注册发生验证码
     *
     * @param inData
     */
    @Override
    public void getSmsAuthenticationCode(FranchiseeVO inData) {
        log.info("加盟商注册发送验证码：" + inData.getFrancPhone());
        String code = RandomStringUtils.randomNumeric(6);
        String seq = RandomStringUtils.randomNumeric(2);
        Verification verification = new Verification();
        verification.setTel(inData.getFrancPhone());
        verification.setCodeType("1");
        verification.setAuthCode(code);
        verification.setSeq(seq);
        smsTemplateService.getSmsAuthenticationCode(verification);
        redisClient.set("franchisee_" + inData.getFrancPhone(), code, 300);
    }

    @Override
    public FranchiseeDTO getFranchiseeById(FranchiseeVO inData) {
        TokenUser tokenUser = commonService.getLoginInfo();
        if (StringUtils.isEmpty(inData.getClient())) {
            inData.setClient(tokenUser.getClient());
        }
        Franchisee franchisee = (Franchisee) this.franchiseeMapper.selectById(inData.getClient());
        FranchiseeDTO outData = new FranchiseeDTO();
        if (ObjectUtils.isNotEmpty(franchisee)) {
            outData.setClient(franchisee.getClient());
            outData.setFrancAddr(franchisee.getFrancAddr());
            outData.setFrancLegalPerson(franchisee.getFrancLegalPerson());
            UserData userDataQua;
            UserData userDataAss;
            if (ObjectUtils.isNotEmpty(franchisee.getFrancLegalPerson())) {
                userDataQua = this.userDataMapper.selectOne(new QueryWrapper<UserData>()
                        .eq("CLIENT", inData.getClient())
                        .eq("USER_ID", franchisee.getFrancLegalPerson()));
                if (ObjectUtils.isNotEmpty(userDataQua)) {
                    outData.setFrancLegalPerson(userDataQua.getUserId());
                    outData.setFrancLegalPersonName(userDataQua.getUserNam());
                    outData.setFrancLegalPersonPhone(userDataQua.getUserTel());
                }
            }
            if (ObjectUtils.isNotEmpty(franchisee.getFrancLogo())) {
                String url = cosUtils.getFileUrl(franchisee.getFrancLogo());
                outData.setFrancLogoUrl(url);
            }
            outData.setFrancLogo(franchisee.getFrancLogo());
            outData.setFrancName(franchisee.getFrancName());
            outData.setFrancNo(franchisee.getFrancNo());
            outData.setFrancQua(franchisee.getFrancQua());
            outData.setFrancAss(franchisee.getFrancAss());
            if (ObjectUtils.isNotEmpty(franchisee.getFrancAss())) {
                userDataAss = (UserData) this.userDataMapper.selectOne(new QueryWrapper<UserData>()
                        .eq("CLIENT", inData.getClient())
                        .eq("USER_ID", franchisee.getFrancAss()));
                if (ObjectUtils.isNotEmpty(userDataAss)) {
                    outData.setFrancAss(userDataAss.getUserId());
                    outData.setFrancAssName(userDataAss.getUserNam());
                    outData.setFrancAssPhone(userDataAss.getUserTel());
                }
            }
            outData.setFrancType1(franchisee.getFrancType1());
            outData.setFrancType2(franchisee.getFrancType2());
            outData.setFrancType3(franchisee.getFrancType3());
        }
        return outData;
    }

    /**
     * 加盟商移动端注册
     *
     * @param inData
     */
    @Override
    @Transactional
    public void insertFranchisee(FranchiseeVO inData) {
        Object obj = this.redisClient.get("franchisee_" + inData.getFrancPhone());
        if (org.springframework.util.StringUtils.isEmpty(inData.getFrancName())) {
            throw new CustomResultException("提示：请输入品牌名称");
        }
        if (org.springframework.util.StringUtils.isEmpty(inData.getFrancLegalPersonName())) {
            throw new CustomResultException("提示：请输入负责人");
        }
        if (org.springframework.util.StringUtils.isEmpty(inData.getFrancPhone())) {
            throw new CustomResultException("提示：请输入负责人手机号");
        }
        if (org.springframework.util.StringUtils.isEmpty(inData.getCode())) {
            throw new CustomResultException("提示：请输入验证码");
        }
        if (inData.getFrancType1().equals('1') && inData.getFrancType1().equals('2') && inData.getFrancType1().equals('3')) {
            throw new CustomResultException("提示：至少选择一总公司性质");
        }
        if (ObjectUtils.isEmpty(obj)) {
            throw new CustomResultException("提示：短信验证码已过期");
        } else if (!inData.getCode().equals(obj.toString())) {
            throw new CustomResultException("提示：短信验证码错误");
        } else {
            List<Franchisee> list = this.franchiseeMapper.selectList(new QueryWrapper<Franchisee>()
                    .eq("FRANC_NAME", inData.getFrancName()));
            if (ObjectUtils.isNotEmpty(list)) {
                throw new CustomResultException("提示：加盟商名称已存在！");
            } else {
                int max = this.franchiseeMapper.selectMaxId();
                String client = String.valueOf(max + 1);
                Franchisee franchisee = new Franchisee();
                franchisee.setClient(client);
                franchisee.setFrancAddr(inData.getFrancAddr());
                franchisee.setFrancLogo(inData.getFrancLogo());
                franchisee.setFrancName(inData.getFrancName());
                franchisee.setFrancNo(inData.getFrancNo());
                franchisee.setFrancQua(inData.getFrancQua());
                franchisee.setFrancType1(inData.getFrancType1());
                franchisee.setFrancType2(inData.getFrancType2());
                franchisee.setFrancType3(inData.getFrancType3());
                franchisee.setFrancCreDate(DateUtils.getCurrentDateStrYYMMDD());
                franchisee.setFrancCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                franchisee.setFrancModiDate(DateUtils.getCurrentDateStrYYMMDD());
                franchisee.setFrancModiTime(DateUtils.getCurrentTimeStrHHMMSS());
                UserData userLegal = null;
                UserData userAss = null;
                userLegal = this.saveAuthConfigByLegal(franchisee.getClient(), inData);

                franchisee.setFrancModiId(userLegal.getUserId());
                franchisee.setFrancLegalPerson(userLegal.getUserId());
                //负责人账号
                //负责人和委托人是同一人  或者是  委托人为空
                if (!inData.getFrancPhone().equals(inData.getFrancLegalPerson()) && StringUtils.isNotEmpty(inData.getFrancAssPhone())) {
                    userAss = this.saveAuthConfigByAss(franchisee.getClient(), inData);//委托人账号

                    franchisee.setFrancAss(userAss.getUserId());
                }


                this.franchiseeMapper.insert(franchisee);

                /**
                 * 消息推送
                 */
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(client);
                messageParams.setUserId(userLegal.getUserId());
                messageParams.setId(CommonEnum.gmtId.MSG00002.getCode());
                messageService.sendMessage(messageParams);

                /**
                 * 添加默认毛利区间
                 */
                this.saveGrossMarginInterval(client);

                /**
                 * 添加默认价格区间值
                 */
                this.savePriceRange(client);

                //添加默认区域
                RegionDTO regionDTO = new RegionDTO();
                regionDTO.setClients(Lists.newArrayList(client));
                operationFeign.addDefaultRegion(regionDTO);

                //添加默认门店分类配置
                storeDataService.saveDefaultStoreGroupSet(client);
            }
        }
    }

    /**
     * 添加默认毛利区间
     */
    public void saveGrossMarginInterval(String client){
        for (int i = 0; i < 5; i++) {
            if (i == 0){
                GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                interval.setClient(client);
                interval.setIntervalType("A");
                interval.setStartValue(new BigDecimal("-999999"));
                interval.setEndValue(new BigDecimal("0"));
                interval.setCreateTime(new Date());
                this.grossMarginIntervalMapper.insert(interval);
            }
            if (i == 1){
                GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                interval.setClient(client);
                interval.setIntervalType("B");
                interval.setStartValue(new BigDecimal("0"));
                interval.setEndValue(new BigDecimal("10"));
                interval.setCreateTime(new Date());
                this.grossMarginIntervalMapper.insert(interval);
            }
            if (i == 2){
                GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                interval.setClient(client);
                interval.setIntervalType("C");
                interval.setStartValue(new BigDecimal("10"));
                interval.setEndValue(new BigDecimal("35"));
                interval.setCreateTime(new Date());
                this.grossMarginIntervalMapper.insert(interval);
            }
            if (i == 3){
                GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                interval.setClient(client);
                interval.setIntervalType("D");
                interval.setStartValue(new BigDecimal("35"));
                interval.setEndValue(new BigDecimal("50"));
                interval.setCreateTime(new Date());
                this.grossMarginIntervalMapper.insert(interval);
            }
            if (i == 4){
                GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                interval.setClient(client);
                interval.setIntervalType("E");
                interval.setStartValue(new BigDecimal("50"));
                interval.setEndValue(new BigDecimal("999999"));
                interval.setCreateTime(new Date());
                this.grossMarginIntervalMapper.insert(interval);
            }
        }
    }

    /**
     * 添加默认价格区间值
     */
    public void savePriceRange(String client){
        for (int i = 'A'; i <= 'D'; i++){
            if (i == 'A'){
                GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                gaiaPriceRange.setClient(client);
                gaiaPriceRange.setRangeLevel("A");
                gaiaPriceRange.setStartPrice(0);
                gaiaPriceRange.setEndPrice(10);
                gaiaPriceRange.setPriceInterval(1);
                this.gaiaPriceRangeMapper.insert(gaiaPriceRange);
            }
            if (i == 'B'){
                GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                gaiaPriceRange.setClient(client);
                gaiaPriceRange.setRangeLevel("B");
                gaiaPriceRange.setStartPrice(10);
                gaiaPriceRange.setEndPrice(50);
                gaiaPriceRange.setPriceInterval(5);
                this.gaiaPriceRangeMapper.insert(gaiaPriceRange);
            }
            if (i == 'C'){
                GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                gaiaPriceRange.setClient(client);
                gaiaPriceRange.setRangeLevel("C");
                gaiaPriceRange.setStartPrice(50);
                gaiaPriceRange.setEndPrice(100);
                gaiaPriceRange.setPriceInterval(10);
                this.gaiaPriceRangeMapper.insert(gaiaPriceRange);
            }
            if (i == 'D'){
                GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                gaiaPriceRange.setClient(client);
                gaiaPriceRange.setRangeLevel("D");
                gaiaPriceRange.setStartPrice(100);
                gaiaPriceRange.setEndPrice(999999);
                gaiaPriceRange.setPriceInterval(20);
                this.gaiaPriceRangeMapper.insert(gaiaPriceRange);
            }
        }
    }

    /**
     * 创建委托人
     *
     * @param client
     * @param inData
     * @return
     */
    @Transactional
    public UserData saveAuthConfigByAss(String client, FranchiseeVO inData) {
        UserData userAss = this.userDataMapper.selectOne(new QueryWrapper<UserData>()
                .eq("CLIENT", client)
                .eq("USER_TEL", inData.getFrancAssPhone()));
        UserData userData = new UserData();

        if (ObjectUtils.isNotEmpty(userAss)) {
            BeanUtils.copyProperties(userAss, userData);
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setDepId(inData.getFrancNo());
            userData.setDepName(inData.getFrancName());
            this.userDataMapper.update(userData, new QueryWrapper<UserData>()
                    .eq("CLIENT", client)
                    .eq("USER_TEL", userAss.getUserId()));

            /**
             * 更新CEO角色权限关联(手机)
             */
            Result result = userRoleService.createUserRole(client, CommonEnum.garFunc.CEO.getCode(), userData.getUserId(), userAss.getUserId());
            if (!StringUtils.equals(result.getCode(), "0")) {
                throw new CustomResultException(result.getMessage());
            }

        } else {
            String password = RandomStringUtils.randomNumeric(6);
            int userMax = this.userDataMapper.selectMaxId(client);
            userData.setUserId(String.valueOf(userMax + 1));
            userData.setClient(client);
            userData.setUserNam(inData.getFrancAssName());
            userData.setUserPassword(UserUtils.encryptMD5(password));
            userData.setUserTel(inData.getFrancAssPhone());
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserJoinDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setDepId(inData.getFrancNo());
            userData.setDepName(inData.getFrancName());
            this.userDataMapper.insert(userData);
            this.saveAuthConfig(userData.getClient(), userData.getUserId(), null, null);
            System.out.println("phone:" + userData.getUserTel());

            /**
             * 插入CEO角色权限关联(手机)
             */
            Result result = userRoleService.createUserRole(client, CommonEnum.garFunc.CEO.getCode(), userData.getUserId());
            if (!StringUtils.equals(result.getCode(), "0")) {
                throw new CustomResultException(result.getMessage());
            }

            this.sendSms(userData, password, "2");
        }
        return userData;

    }

    @Transactional
    public UserData saveAuthConfigByLegal(String client, FranchiseeVO inData) {
        UserData userLegal = this.userDataMapper.selectOne(new QueryWrapper<UserData>()
                .eq("CLIENT", client)
                .eq("USER_TEL", inData.getFrancPhone()));
        UserData userData = new UserData();

        if (ObjectUtils.isNotEmpty(userLegal)) {
            BeanUtils.copyProperties(userLegal, userData);
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setDepId(client);
            userData.setDepName(inData.getFrancName());
            this.userDataMapper.update(userData, new QueryWrapper<UserData>()
                    .eq("CLIENT", client)
                    .eq("USER_TEL", userLegal.getUserId()));

            /**
             * 插入CEO角色权限关联(手机)
             */
            Result result = userRoleService.createUserRole(client, CommonEnum.garFunc.CEO.getCode(), userData.getUserId(), userLegal.getUserId());
            if (!StringUtils.equals(result.getCode(), "0")) {
                throw new CustomResultException(result.getMessage());
            }

        } else {
            String password = RandomStringUtils.randomNumeric(6);
            int userMax = this.userDataMapper.selectMaxId(client);
            userData.setUserId(String.valueOf(userMax + 1));
            userData.setClient(client);
            userData.setUserNam(inData.getFrancLegalPersonName());
            userData.setUserPassword(UserUtils.encryptMD5(password));
            userData.setUserTel(inData.getFrancPhone());
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserJoinDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setDepId(client);
            userData.setDepName(inData.getFrancName());
            this.userDataMapper.insert(userData);
            this.saveAuthConfig(userData.getClient(), userData.getUserId(), null, null);
            System.out.println("phone:" + userData.getUserTel());

            /**
             * 插入CEO角色权限关联(手机)
             */
            Result result = userRoleService.createUserRole(client, CommonEnum.garFunc.CEO.getCode(), userData.getUserId());
            if (!StringUtils.equals(result.getCode(), "0")) {
                throw new CustomResultException(result.getMessage());
            }

            this.sendSms(userData, password, "1");
        }
        return userData;
    }


    @Transactional
    public void saveAuthConfig(String client, String userId, List<String> siteList, String delUserId) {

        List<String> groupList = new ArrayList();
        groupList.add("GAIA_AL_ADMIN");
        groupList.add("GAIA_AL_MANAGER");
        groupList.add("GAIA_WM_FZR");
        groupList.add("GAIA_MM_ZLZG");
        groupList.add("GAIA_MM_SCZG");
        groupList.add("SD_01");
        groupList.add("GAIA_FI_ZG");
        this.authconfiService.saveAuth(client, userId, groupList, siteList, delUserId);
    }

    @Transactional
    public void sendSms(UserData userData, String password, String type) {
        //1是发负责人  2是发委托人
        Verification verification = new Verification(userData.getUserTel(), "2", null, password);
        smsTemplateService.sendFranchiseeRegister(verification, type);
    }

    /**
     * 加盟商修改
     *
     * @param inData
     */
    @Override
    @Transactional
    public void updateFranchisee(FranchiseeVO inData) {
        TokenUser tokenUser = commonService.getLoginInfo();

        if (org.springframework.util.StringUtils.isEmpty(inData.getFrancName())) {
            throw new CustomResultException("提示：请输入品牌名称");
        }
        if (org.springframework.util.StringUtils.isEmpty(inData.getFrancLegalPerson())) {
            throw new CustomResultException("提示：请选择负责人");
        }
//      if(StringUtils.isEmpty(inData.getFrancAss())){
//         throw new BusinessException("提示：请选择委托人");
//      }else
        if (inData.getFrancType1().equals('0') && inData.getFrancType2().equals('0') && inData.getFrancType3().equals('0')) {
            throw new CustomResultException("提示：至少选择一总公司性质");
        } else {
            Franchisee franchisee = (Franchisee) this.franchiseeMapper.selectById(tokenUser.getClient());
            String oldLegal = franchisee.getFrancLegalPerson();
            String oldAss = franchisee.getFrancAss();
            franchisee.setFrancAddr(inData.getFrancAddr());
            franchisee.setFrancLegalPerson(inData.getFrancLegalPerson());
            franchisee.setFrancLogo(inData.getFrancLogo());
            franchisee.setFrancName(inData.getFrancName());
            franchisee.setFrancNo(inData.getFrancNo());
            franchisee.setFrancQua(inData.getFrancQua());
            franchisee.setFrancAss(inData.getFrancAss());
            franchisee.setFrancType1(inData.getFrancType1());
            franchisee.setFrancType2(inData.getFrancType2());
            franchisee.setFrancType3(inData.getFrancType3());
            franchisee.setFrancModiDate(DateUtils.getCurrentDateStrYYMMDD());
            franchisee.setFrancModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            franchisee.setFrancModiId(tokenUser.getUserId());
            this.franchiseeMapper.update(franchisee, new QueryWrapper<Franchisee>()
                    .eq("CLIENT", tokenUser.getClient()));
            List<String> siteList = new ArrayList<>();
            //获取旗下所有门店
            List<String> stoList = this.storeDataMapper.selectListForAuth(franchisee.getClient(), (String) null);
            //获取旗下所有部门
            List<String> depList = this.depDataMapper.selectListForAuth(franchisee.getClient(), (String) null);
            //获取旗下所有批发
            List<String> compadmList = this.compadmMapper.selectListForAuth(franchisee.getClient());
            //获取旗下所有连锁
            List<String> compadmWmsList = this.compadmWmsMapper.selectListForAuth(franchisee.getClient());
            siteList.addAll(stoList);
            siteList.addAll(depList);
            siteList.addAll(compadmList);
            siteList.addAll(compadmWmsList);
            if (!oldLegal.equals(inData.getFrancLegalPerson())) {
                this.userDataService.editDepById(franchisee.getClient(), franchisee.getFrancName(), franchisee.getFrancLegalPerson());
                this.saveAuthConfig(franchisee.getClient(), franchisee.getFrancLegalPerson(), siteList, oldLegal);
                /**
                 * 更新CEO角色权限关联(手机)
                 */
                Result result = userRoleService.createUserRole(tokenUser.getClient(), CommonEnum.garFunc.CEO.getCode(), inData.getFrancLegalPerson(), oldLegal);
                if (!com.gov.common.basic.StringUtils.equals(result.getCode(), "0")) {
                    throw new CustomResultException(result.getMessage());
                }
            }
            String delAss = "";
            if (StringUtils.isNotBlank(inData.getFrancAss()) && !inData.getFrancAss().equals(oldAss)) {
                delAss = oldAss;
                this.userDataService.editDepById(franchisee.getClient(), franchisee.getFrancName(), franchisee.getFrancAss());
                this.saveAuthConfig(franchisee.getClient(), franchisee.getFrancAss(), siteList, delAss);

                /**
                 * 更新CEO角色权限关联(手机)
                 */
                Result result = userRoleService.createUserRole(tokenUser.getClient(), CommonEnum.garFunc.CEO.getCode(), inData.getFrancAss(), oldAss);
                if (!com.gov.common.basic.StringUtils.equals(result.getCode(), "0")) {
                    throw new CustomResultException(result.getMessage());
                }
            }
        }
    }
}
