package com.gov.operate.service;

import org.springframework.web.multipart.MultipartFile;

public interface UserSignatureService {

    String uploadSignature(MultipartFile userSignature, String client,String userId);

    Boolean updateUserElectronicSignatureData(String signatureUrl,String client,String userId);

}
