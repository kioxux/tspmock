package com.gov.operate.controller;

import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.UserSignatureService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("electronicSignature")
public class ElectronicSignatureController {

    @Autowired
    UserSignatureService userSignatureService;
    @Resource
    private CommonService commonService;

    @ApiOperation(value = "上传电子签名")
    @PostMapping("uploadSignature")
    public Result uploadSignature(@RequestParam("userSignature") MultipartFile userSignature) {
        // 获取当前人
        TokenUser user = commonService.getLoginInfo();
        //上传签名
        String signatureUrl = userSignatureService.uploadSignature(userSignature, user.getClient(), user.getUserId());
        if (StringUtils.isBlank(signatureUrl.trim())) {
            return ResultUtil.error(ResultEnum.E0101);
        }
        //获取上传后的路径，存储至用户信息
        Boolean updateUserData = userSignatureService.updateUserElectronicSignatureData(signatureUrl, user.getClient(), user.getUserId());
        if (updateUserData) {
            return ResultUtil.success(signatureUrl);
        }
        return ResultUtil.error(ResultEnum.E0101);
    }

}
