package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.GetGsmTaskListDTO2;
import com.gov.operate.dto.GetProductListToCheckDTO;
import com.gov.operate.dto.GsmTaskListVO2;
import com.gov.operate.entity.SdMarketingClient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface SdMarketingClientMapper extends BaseMapper<SdMarketingClient> {

    IPage<GetGsmTaskListDTO2> selectMarketing(Page<GetGsmTaskListDTO2> page, @Param("client") String client, @Param("gsmTaskListVO2") GsmTaskListVO2 gsmTaskListVO2);

    IPage<GetProductListToCheckDTO> getProductList(Page<GetProductListToCheckDTO> page, @Param("id") Integer id, @Param("client") String client,
                                                   @Param("proKey") String proKey, @Param("mllStart") BigDecimal mllStart, @Param("mllEnd") BigDecimal mllEnd,
                                                   @Param("promFitsType") Integer promFitsType, @Param("promFitsStart") BigDecimal promFitsStart, @Param("promFitsEnd") BigDecimal promFitsEnd);

    IPage<GetProductListToCheckDTO> getProductBySelfCode(Page<GetProductListToCheckDTO> page,
                                                         @Param("id") Integer id, @Param("promId") Integer promId,
                                                         @Param("client") String client,
                                                         @Param("proKey") String proKey, @Param("proSelfCode") String proSelfCode,
                                                         @Param("mllStart") BigDecimal mllStart, @Param("mllEnd") BigDecimal mllEnd);
}
