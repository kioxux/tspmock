package com.gov.operate.mapper;

import com.gov.operate.entity.SdIndrawerD;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface SdIndrawerDMapper extends BaseMapper<SdIndrawerD> {

}
