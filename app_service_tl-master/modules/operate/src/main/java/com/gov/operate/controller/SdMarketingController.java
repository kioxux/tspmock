package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.service.ISdMarketingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zs
 * @since 2020-07-20
 */
@Api(tags = "营销活动设置")
@RestController
@RequestMapping("gsm")
public class SdMarketingController {

    @Resource
    private ISdMarketingService sdMarketingService;

    @Log("营销设置列表")
    @ApiOperation(value = "营销设置列表")
    @PostMapping("getGsmSettingList")
    public Result getGsmSettingList(@Valid @RequestBody GetGsmSettingListVO vo) {
        return ResultUtil.success(sdMarketingService.getGsmSettingList(vo));
    }

    @Log("营销设置详情")
    @ApiOperation(value = "营销设置详情")
    @GetMapping("getGsmSettingDetail")
    public Result getGsmSettingDetail(@RequestParam("gsmMarketid") String gsmMarketid) {
        return ResultUtil.success(sdMarketingService.getGsmSettingDetail(gsmMarketid));
    }

    @Log("营销设置保存")
    @ApiOperation(value = "营销设置保存")
    @PostMapping("saveGsmSetting")
    public Result saveGsmSetting(@Valid @RequestBody SaveGsmSettingVO vo) {
        sdMarketingService.saveGsmSetting(vo);
        return ResultUtil.success();
    }

    @Log("营销设置编辑")
    @ApiOperation(value = "营销设置编辑")
    @PostMapping("editGsmSetting")
    public Result editGsmSetting(@Valid @RequestBody EditGsmSettingVO vo) {
        sdMarketingService.editGsmSetting(vo);
        return ResultUtil.success();
    }

    @Log("活动方案推送")
    @ApiOperation(value = "活动方案推送")
    @PostMapping("pushGsm")
    public Result pushGsm(@Valid @RequestBody PushGsmVO vo) {
        sdMarketingService.pushGsm(vo);
        return ResultUtil.success();
    }

    @Log("营销设置删除")
    @ApiOperation(value = "营销设置删除")
    @GetMapping("deleteMarketSetting")
    public Result deleteMarketSetting(@RequestParam("gsmMarketid") String gsmMarketid) {
        sdMarketingService.deleteMarketSetting(gsmMarketid);
        return ResultUtil.success();
    }

    @Log("营销主题分析")
    @ApiOperation(value = "营销主题分析")
    @GetMapping("getStandardValue")
    public Result getStandardValue() {
        return ResultUtil.success(sdMarketingService.getStandardValue());
    }
}

