package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BILL_PAYMENT_APPLY")
@ApiModel(value="BillPaymentApply对象", description="")
public class BillPaymentApply extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "类型")
    @TableField("GBPA_TYPE")
    private Integer gbpaType;

    @ApiModelProperty(value = "对账单号")
    @TableField("GBPA_NUM")
    private String gbpaNum;

    @ApiModelProperty(value = "总金额")
    @TableField("GBPA_TOTAL_AMT")
    private BigDecimal gbpaTotalAmt;

    @ApiModelProperty(value = "本次申请金额")
    @TableField("GBPA_PAY_AMT")
    private BigDecimal gbpaPayAmt;

    @ApiModelProperty(value = "备注")
    @TableField("GBPA_REMARKS")
    private String gbpaRemarks;

    @ApiModelProperty(value = "申请人")
    @TableField("APPROVAL_USER")
    private String approvalUser;

    @ApiModelProperty(value = "审批状态")
    @TableField("APPROVAL_STATUS")
    private Integer approvalStatus;

    @ApiModelProperty(value = "工作流编码")
    @TableField("APPROVAL_FLOW_NO")
    private String approvalFlowNo;

    @ApiModelProperty(value = "创建人")
    @TableField("GBPA_CREATE_USER")
    private String gbpaCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBPA_CREATE_DATE")
    private String gbpaCreateDate;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBPA_CREATE_TIME")
    private String gbpaCreateTime;


}
