package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.Result;
import com.gov.operate.dto.GetMemberListDTO;
import com.gov.operate.dto.GetMemberListVO;
import com.gov.operate.entity.SdMemberBasic;
import com.gov.operate.mapper.SdMemberBasicMapper;
import com.gov.operate.service.ISdMemberBasicService;
import lombok.SneakyThrows;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-22
 */
@Service
public class SdMemberBasicServiceImpl extends ServiceImpl<SdMemberBasicMapper, SdMemberBasic> implements ISdMemberBasicService {

    @Resource
    private SdMemberBasicMapper sdMemberBasicMapper;
    @Resource
    private CosUtils cosUtils;

    /**
     * 精准查询
     */
    @Override
    public List<GetMemberListDTO> getMemberList(GetMemberListVO vo) {
        // 根据消费类型取天数
        if (OperateEnum.ConsumeType.FIVE.getCode().equals(vo.getConsumeType())) {
            vo.setConsumeTypeDay(0);
        } else {
            for (OperateEnum.ConsumeType type : OperateEnum.ConsumeType.values()) {
                if (!type.getCode().equals(vo.getConsumeType())) {
                    continue;
                }
                String consumeTypeDay = type.getMessage();
                vo.setConsumeTypeDay(Integer.valueOf(consumeTypeDay));
            }
        }

//        Page<GetMemberListDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        Page<GetMemberListDTO> page = null;
        List<GetMemberListDTO> memberList = sdMemberBasicMapper.getMemberList(page, vo);
        return memberList;
    }

    /**
     * 精准查询导出
     *
     * @return
     */
    @SneakyThrows
    @Override
    public Result exportMemberList(GetMemberListVO vo) {
        List<GetMemberListDTO> records = this.getMemberList(vo);

        List<List<Object>> dataList = new ArrayList<>();
        for (GetMemberListDTO dto : records) {

            // 打印行
            List<Object> item = new ArrayList<>();
            dataList.add(item);
            // 加盟商
            item.add(dto.getClient());
            // 会员卡号
            item.add(dto.getGmbCardId());
            // 会员名称
            item.add(dto.getGmbName());
            // 会员手机
            item.add(dto.getGmbMobile());
            // 会员性别
            if (StringUtils.isNotBlank(dto.getGmbSex())) {
                OperateEnum.Sex sex = EnumUtils.getEnumByCode(dto.getGmbSex(), OperateEnum.Sex.class);
                if (sex != null) {
                    item.add(sex.getMessage());
                } else {
                    item.add("");
                }
            } else {
                item.add("");
            }
            // 年龄
            item.add(dto.getGmbAge());
            // 生日
            item.add(formatDate(dto.getGmbBirth()));
            // 当前积分
            item.add(dto.getGmbIntegral());
            // 门店编号
            item.add(dto.getGmbBrId());
            // 门店名称
            item.add(dto.getGmbBrName());
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("精准查询");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 日期格式化
     */
    private String formatDate(String date) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, "/");
        sb.insert(7, "/");
        return sb.toString();
    }

    /**
     * 精准查询导出数据表头
     */
    private String[] headList = {
            "加盟商",
            "会员卡号",
            "会员名称",
            "会员手机",
            "会员性别",
            "年龄",
            "生日",
            "当前积分",
            "门店编号",
            "门店名称",
    };

}
