package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.SystemServicePackVO;
import com.gov.operate.entity.SystemServicePack;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemServicePackMapper extends BaseMapper<SystemServicePack> {
    List<SystemServicePackVO> selectResourceServicePackageRelationsByResourceIds(@Param("ids") List<Integer> ids);
}
