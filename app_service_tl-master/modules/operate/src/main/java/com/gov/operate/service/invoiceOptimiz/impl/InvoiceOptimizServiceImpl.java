package com.gov.operate.service.invoiceOptimiz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.invoiceOptimiz.FicoInvoiceInformationRegistrationDto;
import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import com.gov.operate.entity.InvoiceBillInvoice;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.FicoInvoiceInformationRegistrationMapper;
import com.gov.operate.mapper.InvoiceBillInvoiceMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IFicoInvoiceInformationRegistrationService;
import com.gov.operate.service.invoiceOptimiz.InvoiceOptimizService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.15
 */
@Slf4j
@Service
public class InvoiceOptimizServiceImpl implements InvoiceOptimizService {

    @Resource
    private CommonService commonService;
    @Resource
    private FicoInvoiceInformationRegistrationMapper ficoInvoiceInformationRegistrationMapper;
    @Resource
    private IFicoInvoiceInformationRegistrationService iFicoInvoiceInformationRegistrationService;
    @Resource
    private InvoiceBillInvoiceMapper invoiceBillInvoiceMapper;

    /**
     * 发票集合
     *
     * @param pageNum         分页
     * @param pageSize        分页
     * @param invoiceSiteType 地点类型
     * @param siteCode        地点
     * @param supCode         供应商
     * @param invoiceType     发票类型
     * @param invoiceNum      发票号
     * @param invoiceDate     发票日期
     * @return
     */
    @Override
    public IPage<FicoInvoiceInformationRegistrationDto> getInvoiceList(Integer pageNum, Integer pageSize, String invoiceSiteType, String siteCode, String supCode, String invoiceType, String invoiceNum, String invoiceDate, Integer status, String invoiceSalesman) {
        // 分页
        Page page = new Page(pageNum, pageSize);
        TokenUser user = commonService.getLoginInfo();
        IPage<FicoInvoiceInformationRegistrationDto> ipage = ficoInvoiceInformationRegistrationMapper.getInvoiceListOptimiz(page,
                user.getClient(), invoiceSiteType, siteCode, supCode, invoiceType, invoiceNum, invoiceDate, status, invoiceSalesman);
        return ipage;
    }

    /**
     * 发票保存
     *
     * @param list
     */
    @Override
    public void saveInvoiceList(List<FicoInvoiceInformationRegistration> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("未提交需要保存的数据");
        }
        TokenUser user = commonService.getLoginInfo();
        List<FicoInvoiceInformationRegistration> entityList = new ArrayList<>();
        for (FicoInvoiceInformationRegistration ficoInvoiceInformationRegistration : list) {
            // 发票号码
            if (StringUtils.isBlank(ficoInvoiceInformationRegistration.getInvoiceNum())) {
                throw new CustomResultException("发票号码不能为空");
            }
            // 采购地点
            if (StringUtils.isBlank(ficoInvoiceInformationRegistration.getSiteCode())) {
                throw new CustomResultException("采购地点不能为空");
            }
            // 供应商编码
            if (StringUtils.isBlank(ficoInvoiceInformationRegistration.getSupCode())) {
                throw new CustomResultException("供应商不能为空");
            }
            // 发票类型
            if (StringUtils.isBlank(ficoInvoiceInformationRegistration.getInvoiceType())) {
                throw new CustomResultException("发票类型不能为空");
            }
            // 发票日期
            if (StringUtils.isBlank(ficoInvoiceInformationRegistration.getInvoiceDate())) {
                throw new CustomResultException("发票日期不能为空");
            }
            // 发票去税金额
            if (ficoInvoiceInformationRegistration.getInvoiceAmountExcludingTax() == null) {
                throw new CustomResultException("发票去税金额不能为空");
            }
            // 发票税额
            if (ficoInvoiceInformationRegistration.getInvoiceTax() == null) {
                throw new CustomResultException("发票税额不能为空");
            }
            // 发票总额
            if (ficoInvoiceInformationRegistration.getInvoiceTotalInvoiceAmount() == null) {
                throw new CustomResultException("发票含税金额不能为空");
            }
            // 发票地点类型
            if (StringUtils.isBlank(ficoInvoiceInformationRegistration.getInvoiceSiteType())) {
                throw new CustomResultException("发票地点类型不能为空");
            }
            // 已存在数据验证
            List<FicoInvoiceInformationRegistration> boys = entityList.stream().filter(s -> s.getInvoiceNum().equals(ficoInvoiceInformationRegistration.getInvoiceNum())).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(boys)) {
                throw new CustomResultException(StringUtils.parse("发票号{}重复", ficoInvoiceInformationRegistration.getInvoiceNum()));
            }
            FicoInvoiceInformationRegistration exEntity = ficoInvoiceInformationRegistrationMapper.selectOne(
                    new QueryWrapper<FicoInvoiceInformationRegistration>()
                            .eq("CLIENT", user.getClient())
                            .eq("INVOICE_NUM", ficoInvoiceInformationRegistration.getInvoiceNum()));
            if (exEntity != null) {
                throw new CustomResultException(StringUtils.parse("发票号{}已存在", ficoInvoiceInformationRegistration.getInvoiceNum()));
            }
            // 数据保存
            FicoInvoiceInformationRegistration entity = new FicoInvoiceInformationRegistration();
            BeanUtils.copyProperties(ficoInvoiceInformationRegistration, entity);
            // 加盟商
            entity.setClient(user.getClient());
            // 登记状态
            entity.setInvoiceRegistrationStatus("0");
            // 付款状态
            entity.setInvoicePaymentStatus("0");
            // 创建日期
            entity.setInvoiceCreationDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建人
            entity.setInvoiceFounder(user.getUserId());
            // 发票确认金额
            entity.setInvoiceConfirmationAmount(ficoInvoiceInformationRegistration.getInvoiceTotalInvoiceAmount());
            entityList.add(entity);
        }
        // 批量保存
        iFicoInvoiceInformationRegistrationService.saveBatch(entityList);
    }

    /**
     * 发票详情
     *
     * @param invoiceNum
     * @return
     */
    @Override
    public FicoInvoiceInformationRegistration getInvoice(String invoiceNum) {
        TokenUser user = commonService.getLoginInfo();
        FicoInvoiceInformationRegistration entity = ficoInvoiceInformationRegistrationMapper.selectOne(
                new QueryWrapper<FicoInvoiceInformationRegistration>()
                        .eq("CLIENT", user.getClient())
                        .eq("INVOICE_NUM", invoiceNum));
        if (entity != null) {
            throw new CustomResultException("发票不存在");
        }
        return entity;
    }

    /**
     * 删除发票
     *
     * @param invoiceNum
     */
    @Override
    public void delInvoice(String invoiceNum) {
        TokenUser user = commonService.getLoginInfo();
        FicoInvoiceInformationRegistration entity = ficoInvoiceInformationRegistrationMapper.selectOne(
                new QueryWrapper<FicoInvoiceInformationRegistration>()
                        .eq("CLIENT", user.getClient())
                        .eq("INVOICE_NUM", invoiceNum));
        if (entity == null) {
            throw new CustomResultException("发票不存在");
        }
        if ((entity.getInvoicePaidAmount() != null && entity.getInvoicePaidAmount().compareTo(BigDecimal.ZERO) > 0) ||
                (entity.getInvoiceRegisteredAmount() != null && entity.getInvoiceRegisteredAmount().compareTo(BigDecimal.ZERO) > 0)) {
            throw new CustomResultException("此发票不可以删除");
        }
        // 已对账发票不可以删除
        List<InvoiceBillInvoice> invoiceBillInvoiceList = invoiceBillInvoiceMapper.selectList(
                new QueryWrapper<InvoiceBillInvoice>()
                        .eq("CLIENT", user.getClient())
                        .eq("GIBI_INVOICE_NUM", invoiceNum)
        );
        if (!CollectionUtils.isEmpty(invoiceBillInvoiceList)) {
            throw new CustomResultException("已对账发票不可以删除");
        }
        // 发票删除
        ficoInvoiceInformationRegistrationMapper.delete(
                new QueryWrapper<FicoInvoiceInformationRegistration>()
                        .eq("CLIENT", user.getClient())
                        .eq("INVOICE_NUM", invoiceNum));
    }

    /**
     * 发票编辑
     *
     * @param ficoInvoiceInformationRegistration
     */
    @Override
    public void editInvoice(FicoInvoiceInformationRegistrationDto ficoInvoiceInformationRegistration) {
        TokenUser user = commonService.getLoginInfo();
        FicoInvoiceInformationRegistration entity = ficoInvoiceInformationRegistrationMapper.selectOne(
                new QueryWrapper<FicoInvoiceInformationRegistration>()
                        .eq("CLIENT", user.getClient())
                        .eq("INVOICE_NUM", ficoInvoiceInformationRegistration.getOldInvoiceNum()));
        if (entity == null) {
            throw new CustomResultException("发票不存在");
        }
        if (!ficoInvoiceInformationRegistration.getOldInvoiceNum().equals(ficoInvoiceInformationRegistration.getInvoiceNum())) {
            FicoInvoiceInformationRegistration obj = ficoInvoiceInformationRegistrationMapper.selectOne(
                    new QueryWrapper<FicoInvoiceInformationRegistration>()
                            .eq("CLIENT", user.getClient())
                            .eq("INVOICE_NUM", ficoInvoiceInformationRegistration.getInvoiceNum()));
            if (obj != null) {
                throw new CustomResultException("发票号已存在");
            }
        }
        if ((entity.getInvoicePaidAmount() != null && entity.getInvoicePaidAmount().compareTo(BigDecimal.ZERO) > 0) ||
                (entity.getInvoiceRegisteredAmount() != null && entity.getInvoiceRegisteredAmount().compareTo(BigDecimal.ZERO) > 0)) {
            throw new CustomResultException("此发票不可以修改");
        }
        // 已对账发票不可以修改
        List<InvoiceBillInvoice> invoiceBillInvoiceList = invoiceBillInvoiceMapper.selectList(
                new QueryWrapper<InvoiceBillInvoice>()
                        .eq("CLIENT", user.getClient())
                        .eq("GIBI_INVOICE_NUM", ficoInvoiceInformationRegistration.getOldInvoiceNum())
        );
        if (!CollectionUtils.isEmpty(invoiceBillInvoiceList)) {
            throw new CustomResultException("已对账发票不可以删除");
        }
        entity.setInvoiceRegistrationStatus(null);
        entity.setInvoicePaymentStatus(null);
        entity.setInvoiceCreationDate(null);
        entity.setInvoiceFounder(null);
        BeanUtils.copyProperties(ficoInvoiceInformationRegistration, entity);
        entity.setInvoiceConfirmationAmount(entity.getInvoiceTotalInvoiceAmount());
        ficoInvoiceInformationRegistrationMapper.update(entity, new QueryWrapper<FicoInvoiceInformationRegistration>()
                .eq("INVOICE_NUM", ficoInvoiceInformationRegistration.getOldInvoiceNum())
                .eq("CLIENT", user.getClient()));
    }

    /**
     * 发票合计
     *
     * @param invoiceSiteType
     * @param siteCode
     * @param supCode
     * @param invoiceType
     * @param invoiceNum
     * @param invoiceDate
     * @return
     */
    @Override
    public FicoInvoiceInformationRegistrationDto getInvoiceTotal(String invoiceSiteType, String siteCode, String supCode,
                                                                 String invoiceType, String invoiceNum, String invoiceDate, Integer status) {
        // 分页
        TokenUser user = commonService.getLoginInfo();
        FicoInvoiceInformationRegistrationDto entity = ficoInvoiceInformationRegistrationMapper.getInvoiceTotalOptimiz(
                user.getClient(), invoiceSiteType, siteCode, supCode, invoiceType, invoiceNum, invoiceDate, status);
        return entity;
    }
}
