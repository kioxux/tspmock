package com.gov.operate.service.impl;

import com.gov.operate.entity.DocumentBill;
import com.gov.operate.mapper.DocumentBillMapper;
import com.gov.operate.service.IDocumentBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Service
public class DocumentBillServiceImpl extends ServiceImpl<DocumentBillMapper, DocumentBill> implements IDocumentBillService {

}
