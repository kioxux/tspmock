package com.gov.operate.service.ssp;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SspMenu;

/**
 *
 */
public interface ISspMenuService extends SuperService<SspMenu> {

}
