package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/15 15:02
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetHighlightVO {
    /**
     * 验收单号
     */
    @NotBlank(message = "验收单号不能为空")
    private String gsehVoucherId;
}
