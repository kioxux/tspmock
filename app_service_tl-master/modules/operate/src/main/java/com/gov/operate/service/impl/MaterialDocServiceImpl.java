package com.gov.operate.service.impl;

import com.gov.operate.entity.MaterialDoc;
import com.gov.operate.mapper.MaterialDocMapper;
import com.gov.operate.service.IMaterialDocService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
@Service
public class MaterialDocServiceImpl extends ServiceImpl<MaterialDocMapper, MaterialDoc> implements IMaterialDocService {

}
