package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.entity.SmsEntity;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.marketing.SmsRechargeRecordDTO;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SmsTemplate;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.FranchiseeMapper;
import com.gov.operate.mapper.SdMarketingSearchMapper;
import com.gov.operate.mapper.SmsTemplateMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.CustomMultiThreadingService;
import com.gov.operate.service.ISmsTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-13
 */
@Slf4j
@Service
public class SmsTemplateServiceImpl extends ServiceImpl<SmsTemplateMapper, SmsTemplate> implements ISmsTemplateService {

    @Resource
    private CustomMultiThreadingService customMultiThreadingService;
    @Resource
    private CommonService commonService;
    @Resource
    private SmsUtils smsUtils;
    @Resource
    private AuthFeign authFeign;
    @Resource
    private FranchiseeMapper franchiseeMapper;
    @Resource
    private SdMarketingSearchMapper marketingSearchMapper;

    /**
     * 营销短信列表
     */
    @Override
    public IPage<SmsTemplate> queryMarketingTemplate(QueryMarketingTemplateVO vo) {
        Page<SmsTemplate> page = new Page<SmsTemplate>(vo.getPageNum(), vo.getPageSize());
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<SmsTemplate> query = new QueryWrapper<SmsTemplate>()
                // 主题模糊查询
                .apply(StringUtils.isNotEmpty(vo.getSmsTheme()), "instr(SMS_THEME, {0})", vo.getSmsTheme())
                // 模板状态
                .eq(StringUtils.isNotEmpty(vo.getSmsStatus()), "SMS_STATUS", vo.getSmsStatus())
                // 审批状态：0-审批中，1-已审批，2-已拒绝
                .eq(StringUtils.isNotEmpty(vo.getSmsGspStatus()), "SMS_GSP_STATUS", vo.getSmsGspStatus())
                // 短信类别, 2:营销活动
                .eq("SMS_TYPE", "2")
                .and(wapper -> wapper.or().eq("CLIENT", user.getClient()).or().apply(" CLIENT IS NULL OR LENGTH(CLIENT) = 0 "))
                // 日期时间排序
                .orderByDesc("USER_CRE_DATE", "USER_CRE_TIME");
        return this.page(page, query);
    }

    /**
     * 营销短信模板
     */
    @Override
    public List<SmsTemplate> marketingTemplateList() {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<SmsTemplate> query = new QueryWrapper<SmsTemplate>()
                // 短信类别, 2:营销活动
                .eq("SMS_TYPE", "2")
                // 模板状态, 0:启用
                .eq("SMS_STATUS", "0")
                // 审批状态 = 1已经审批
                .eq("SMS_GSP_STATUS", "1")
                .and(wapper -> wapper.or().eq("CLIENT", user.getClient()).or().apply(" CLIENT IS NULL OR LENGTH(CLIENT) = 0 "))
                .orderByDesc("USER_CRE_DATE", "USER_CRE_TIME");

        return this.list(query);
    }

    /**
     * 营销短信模板新增
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addMarketingTemplate(AddMarketingTemplateVO vo) {
        TokenUser user = commonService.getLoginInfo();

        SmsTemplate template = new SmsTemplate();
        BeanUtils.copyProperties(vo, template);
        template.setSmsId(UUIDUtil.getUUID());
        // 短信类型
        template.setSmsType("2");
        // 模板状态
        template.setSmsStatus(OperateEnum.SmsStatus.YES.getCode());
        // 审批状态 = 0-审批中
        template.setSmsGspStatus(OperateEnum.GspStatus.FINISH.getCode());
        // 审批流程编号 = 时间戳 + 六位随机数
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        template.setSmsFlowNo(flowNo);

        template.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
        template.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        template.setUserCreId(user.getUserId());
        // 模版内容
        template.setSmsContent(vo.getSmsContent());
        template.setClient(user.getClient());
        this.save(template);
        // 发起短信模板审批
//        this.createSmsTemplateWorkflow(template);
    }

    /**
     * 营销短信模板编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editMarketingTemplate(EditMarketingTemplateVO vo) {
        TokenUser user = commonService.getLoginInfo();
        SmsTemplate smsTemplateExit = this.getById(vo.getSmsId());
        if (ObjectUtils.isEmpty(smsTemplateExit)) {
            throw new CustomResultException("没有该短信模板");
        }
//        if (OperateEnum.GspStatus.PROCESS.getCode().equals(smsTemplateExit.getSmsGspStatus())) {
//            throw new CustomResultException("该模板正在审批中，不可编辑");
//        }
        SmsTemplate template = new SmsTemplate();
        BeanUtils.copyProperties(vo, template);
        // 模板状态 1未启用
        template.setSmsStatus(OperateEnum.SmsStatus.YES.getCode());
        // 审批状态 = 0
        template.setSmsGspStatus(OperateEnum.GspStatus.FINISH.getCode());
        // 审批流程编号 = 时间戳 + 六位随机数
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        template.setSmsFlowNo(flowNo);
        template.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
        template.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        template.setUserModiId(user.getUserId());
        boolean update = this.updateById(template);
        if (!update) {
            throw new CustomResultException("编辑失败");
        }
        // 发起短信模板审批
//        this.createSmsTemplateWorkflow(template);
    }

    /**
     * 营销模板测试发送
     */
    @Override
    public Result testMarketingTemplate(TestMarketingTemplateVO vo) {
        SmsTemplate template = this.getById(vo.getSmsId());
//        if (ObjectUtils.isEmpty(template)) {
//            throw new CustomResultException("没有该模板");
//        }
//        if (!OperateEnum.GspStatus.FINISH.getCode().equals(template.getSmsGspStatus())) {
//            throw new CustomResultException("该模版审批未通过");
//        }
//        if (OperateEnum.SmsStatus.NO.getCode().equals(template.getSmsStatus())) {
//            throw new CustomResultException("该模版未启用");
//        }
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<Franchisee> franchiseeQueryWrapper = new QueryWrapper<>();
        franchiseeQueryWrapper.eq("CLIENT", user.getClient());
        Franchisee franchisee = franchiseeMapper.selectOne(franchiseeQueryWrapper);

        SmsEntity smsEntity = new SmsEntity();
//        smsEntity.setSmsId(template.getSmsTemplateId());
        smsEntity.setPhone(vo.getPhone());
        smsEntity.setMsg(StringUtils.parse(SmsUtils.SGIN, franchisee.getFrancName()) + vo.getMessage() + SmsUtils.EXT);
        return smsUtils.testMarketingTemplate(smsEntity);
    }

    /**
     * 营销短信发送
     */
    @Override
    public void sendMarketingSms(SendMarketingSmsVO vo) {
        SmsTemplate template = this.getById(vo.getSmsId());
        if (ObjectUtils.isEmpty(template)) {
            throw new CustomResultException("没有该模板");
        }
        if (!OperateEnum.GspStatus.FINISH.getCode().equals(template.getSmsGspStatus())) {
            throw new CustomResultException("该模版审批未通过");
        }
        if (OperateEnum.SmsStatus.NO.getCode().equals(template.getSmsStatus())) {
            throw new CustomResultException("该模版未启用");
        }
        TokenUser user = commonService.getLoginInfo();
        customMultiThreadingService.sendMarketingSms(vo, user);
    }

    /**
     * 营销模板删除
     */
    @Override
    public void delMarketingTemplate(DelMarketingTemplateVO vo) {
        QueryWrapper<SmsTemplate> query = new QueryWrapper<SmsTemplate>()
                .eq("SMS_ID", vo.getSmsId())
                // TODO 枚举
                .eq("SMS_TYPE", "2");
        boolean remove = this.remove(query);
        if (!remove) {
            throw new CustomResultException("删除失败");
        }
    }

    /**
     * 营销短信启、停用
     */
    @Override
    public void enableMarketingTemplate(EnableMarketingTemplateVO vo) {

        SmsTemplate temp = this.getById(vo.getSmsId());
        if (ObjectUtils.isEmpty(temp)) {
            throw new CustomResultException("没有该模板");
        }
//        if (OperateEnum.SmsStatus.YES.getCode().equals(vo.getEnable()) && !OperateEnum.GspStatus.FINISH.getCode().equals(temp.getSmsGspStatus())) {
//            throw new CustomResultException("该模版未通过审批,不可启用");
//        }

        SmsTemplate template = new SmsTemplate();
        BeanUtils.copyProperties(vo, template);
        template.setSmsStatus(vo.getEnable());
        this.updateById(template);
    }

    /**
     * 营销短详情
     */
    @Override
    public SmsTemplate getMarketingTemplate(String smsId) {
        if (StringUtils.isEmpty(smsId)) {
            throw new CustomResultException("id不能为空");
        }
        SmsTemplate template = this.getById(smsId);
        return template;
    }

    /**
     * 创建工作流
     */
    public void createSmsTemplateWorkflow(SmsTemplate template) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        // token
        param.put("token", token);
        // 类型
        param.put("wfDefineCode", "GAIA_WF_030");
        // 标题
        param.put("wfTitle", template.getSmsTheme());
        // 描述
        param.put("wfDescription", template.getSmsContent());
        // 业务单号
        param.put("wfOrder", template.getSmsFlowNo());

        log.info("短信模板流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        JsonResult result = authFeign.createWorkflow(param);
        log.info("短信模板流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        if (result.getCode().intValue() != 0) {
            throw new CustomResultException("短信模板审批调用失败");
        }
    }


    /**
     * 发送短信
     */
    @Override
    public Result sendStoreRegisterSms(Verification verification) {
        log.info("发送短信" + verification.getTel());
        String sign = "【药德】";
        SmsTemplate smsTemplate = new SmsTemplate();
//        iUserDataService.checkTel(verification.getTel());
        smsTemplate = this.getById(Constants.SMSid.SMS0000005);
        SmsEntity smsEntity = new SmsEntity();
        String storeName = "";
        if (verification.getStoreName().length() > 20) {
            storeName = verification.getStoreName().substring(0, 14) + "...";
        } else {
            storeName = verification.getStoreName();
        }
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign, storeName, verification.getTel(), verification.getPassWord()));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendStoreRegisterSms(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
//        if (!ResultUtil.hasError(result)) {
//            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
//        }
        return result;
    }

    /**
     * 修改店长发送短信
     */
    @Override
    public Result sendUpdateStoreRegisterSms(Verification verification) {
        log.info("发送短信" + verification.getTel());
        String sign = "【药德】";
        SmsTemplate smsTemplate = new SmsTemplate();
//        iUserDataService.checkTel(verification.getTel());
        smsTemplate = this.getById(Constants.SMSid.SMS0000006);
        SmsEntity smsEntity = new SmsEntity();
        String storeName = "";
        if (verification.getStoreName().length() > 20) {
            storeName = verification.getStoreName().substring(0, 14) + "...";
        } else {
            storeName = verification.getStoreName();
        }
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign, storeName, verification.getTel()));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendStoreRegisterSms(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
//        if (!ResultUtil.hasError(result)) {
//            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
//        }
        return result;
    }

    /**
     * 修改店长发送短信
     */
    @Override
    public Result getSmsAuthenticationCode(Verification verification) {
        log.info("发送短信" + verification.getTel());
        String sign = "【药德】";
        SmsTemplate smsTemplate = new SmsTemplate();
        smsTemplate = this.getById(Constants.SMSid.SMS0000007);
        SmsEntity smsEntity = new SmsEntity();
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign, verification.getAuthCode(), verification.getSeq()));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendSmsToGetVerification(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
//        if (!ResultUtil.hasError(result)) {
//            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
//        }
        return result;
    }

    /**
     * 新建加盟商发送短信
     */
    @Override
    public Result sendFranchiseeRegister(Verification verification, String type) {
        log.info("发送短信" + verification.getTel());
        String sign = "【药德】";
        SmsTemplate smsTemplate = new SmsTemplate();
        smsTemplate = this.getById(type.equals("1") ? Constants.SMSid.SMS0000008 : Constants.SMSid.SMS0000009);
        SmsEntity smsEntity = new SmsEntity();
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign, verification.getTel(), verification.getPassWord()));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendSmsToGetVerification(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
//        if (!ResultUtil.hasError(result)) {
//            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
//        }
        return result;
    }

    /**
     * 营销短信剩余条数
     */
    @Override
    public Integer checkSendMarketingSms() {
        TokenUser user = commonService.getLoginInfo();
        List<SmsRechargeRecordDTO> smsRechargeRecordList = marketingSearchMapper.smsRechargeRecord(user.getClient());
        int smsRemainCount = smsRechargeRecordList.stream().filter(Objects::nonNull).mapToInt(SmsRechargeRecordDTO::getGsrrRechargeRemainCount).sum();
        return smsRemainCount;
    }
}





























