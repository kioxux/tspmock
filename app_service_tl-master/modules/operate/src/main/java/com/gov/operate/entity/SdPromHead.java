package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_HEAD")
@ApiModel(value="SdPromHead对象", description="")
public class SdPromHead extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("GSPH_BR_ID")
    private String gsphBrId;

    @ApiModelProperty(value = "促销单号")
    @TableField("GSPH_VOUCHER_ID")
    private String gsphVoucherId;

    @ApiModelProperty(value = "促销主题")
    @TableField("GSPH_THEME")
    private String gsphTheme;

    @ApiModelProperty(value = "活动名称")
    @TableField("GSPH_NAME")
    private String gsphName;

    @ApiModelProperty(value = "活动说明")
    @TableField("GSPH_REMARKS")
    private String gsphRemarks;

    @ApiModelProperty(value = "状态")
    @TableField("GSPH_STATUS")
    private String gsphStatus;

    @ApiModelProperty(value = "起始日期")
    @TableField("GSPH_BEGIN_DATE")
    private String gsphBeginDate;

    @ApiModelProperty(value = "结束日期")
    @TableField("GSPH_END_DATE")
    private String gsphEndDate;

    @ApiModelProperty(value = "日期频率")
    @TableField("GSPH_DATE_FREQUENCY")
    private String gsphDateFrequency;

    @ApiModelProperty(value = "星期频率")
    @TableField("GSPH_WEEK_FREQUENCY")
    private String gsphWeekFrequency;

    @ApiModelProperty(value = "起始时间")
    @TableField("GSPH_BEGIN_TIME")
    private String gsphBeginTime;

    @ApiModelProperty(value = "结束时间")
    @TableField("GSPH_END_TIME")
    private String gsphEndTime;

    @ApiModelProperty(value = "促销类型")
    @TableField("GSPH_TYPE")
    private String gsphType;

    @ApiModelProperty(value = "阶梯")
    @TableField("GSPH_PART")
    private String gsphPart;

    @ApiModelProperty(value = "参数1")
    @TableField("GSPH_PARA1")
    private String gsphPara1;

    @ApiModelProperty(value = "参数2")
    @TableField("GSPH_PARA2")
    private String gsphPara2;

    @ApiModelProperty(value = "参数3")
    @TableField("GSPH_PARA3")
    private String gsphPara3;

    @ApiModelProperty(value = "参数4")
    @TableField("GSPH_PARA4")
    private String gsphPara4;

    @ApiModelProperty(value = "互斥条件")
    @TableField("GSPH_EXCLUSION")
    private String gsphExclusion;

    @ApiModelProperty(value = "营销活动ID")
    @TableField("GSPH_MARKETID")
    private Integer gsphMarketid;

    @ApiModelProperty(value = "是否传输")
    @TableField("GSPH_FLAG")
    private String gsphFlag;

    @ApiModelProperty(value = "更新人员")
    @TableField("GSPH_UPDATE_EMP")
    private String gsphUpdateEmp;

    @ApiModelProperty(value = "更新日期")
    @TableField("GSPH_UPDATE_DATE")
    private String gsphUpdateDate;

    @ApiModelProperty(value = "农历  1-农历")
    @TableField("GSPH_NL_FLAG")
    private String gsphNlFlag;

}
