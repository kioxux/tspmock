package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.productRelate.DropDown;
import com.gov.operate.dto.productRelate.ProductRelateDto;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.ProductRelate;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ProductBusinessMapper;
import com.gov.operate.mapper.ProductRelateMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IProductRelateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
@Service
public class ProductRelateServiceImpl extends ServiceImpl<ProductRelateMapper, ProductRelate> implements IProductRelateService {

    @Resource
    private ProductRelateMapper productRelateMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private ProductBusinessMapper productBusinessMapper;

    /**
     * 机构下拉框
     *
     * @return
     */
    @Override
    public Result getSiteList() {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 根据权限带出，连锁带出仓库地点，单体店带出门店地点。
        List<DropDown> list = productRelateMapper.getSiteList(tokenUser.getClient(), tokenUser.getUserId());
        return ResultUtil.success(list);
    }

    /**
     * 供应商下拉框
     *
     * @param site
     * @return
     */
    @Override
    public Result getSupList(String site) {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 根据地点带出供应商下拉框
        List<DropDown> list = productRelateMapper.getSupList(tokenUser.getClient(), site);
        return ResultUtil.success(list);
    }

    /**
     * 商品下拉框
     *
     * @param key
     * @param proSelfCode
     * @return
     */
    @Override
    public Result getProList(String site, String key, String proSelfCode) {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 根据地点带出商品下拉框
        List<ProductBusiness> list = productRelateMapper.getProList(tokenUser.getClient(), site, key, proSelfCode);
        return ResultUtil.success(list);
    }

    /**
     * 煎药列表
     *
     * @param site
     * @param supCode
     * @param proSelfCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getProductRelateList(String site, String supCode, String proSelfCode, Integer pageNum, Integer pageSize) {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 分页
        Page<ProductRelateDto> page = new Page<ProductRelateDto>(pageNum, pageSize);
        if (StringUtils.isNotBlank(proSelfCode) && proSelfCode.startsWith(",")) {
            proSelfCode = proSelfCode.replace(",", "");
        }
        if (StringUtils.isNotBlank(proSelfCode) && proSelfCode.endsWith(",")) {
            proSelfCode = proSelfCode.substring(0, proSelfCode.length() - 1);
        }
        IPage<ProductRelateDto> iPage = productRelateMapper.selectProductRelateList(page, tokenUser.getClient(), site, supCode, proSelfCode);
        return ResultUtil.success(iPage);
    }

    /**
     * 新增煎药
     *
     * @param site
     * @param supCode
     * @param proSelfCode
     * @param proRelateCode
     * @param proPrice
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addProductRelate(String site, String supCode, String proSelfCode, String proRelateCode, BigDecimal proPrice) {
        TokenUser user = commonService.getLoginInfo();
        // 商品验证
        ProductBusiness productBusiness = productBusinessMapper.selectOne(new QueryWrapper<ProductBusiness>()
                .eq("CLIENT", user.getClient())
                .eq("PRO_SITE", site)
                .eq("PRO_SELF_CODE", proSelfCode)
        );
        if (productBusiness == null) {
            throw new CustomResultException("请选择正确的商品");
        }
        QueryWrapper<ProductRelate> productRelateQueryWrapper = new QueryWrapper<>();
        // 加盟商
        productRelateQueryWrapper.eq("CLIENT", user.getClient());
        // 地点
        productRelateQueryWrapper.eq("PRO_SITE", site);
        // 商品编码
        productRelateQueryWrapper.eq("PRO_SELF_CODE", proSelfCode);
        // 供应商编码
        productRelateQueryWrapper.eq("SUP_SELF_CODE", supCode);
        ProductRelate productRelate = productRelateMapper.selectOne(productRelateQueryWrapper);

        // 关联药品验证
        QueryWrapper<ProductRelate> relateQueryWrapper = new QueryWrapper<>();
        // 加盟商
        relateQueryWrapper.eq("CLIENT", user.getClient());
        // 地点
        relateQueryWrapper.eq("PRO_SITE", site);
        // 关联商品编码
        relateQueryWrapper.eq("PRO_RELATE_CODE", proRelateCode);
        // 供应商编码
        relateQueryWrapper.eq("SUP_SELF_CODE", supCode);
        List<ProductRelate> list = productRelateMapper.selectList(relateQueryWrapper);

        if (productRelate == null) {
            // 关联商品已存在
            if (!CollectionUtils.isEmpty(list)) {
                throw new CustomResultException("当前代煎商品已存在");
            }
            productRelate = new ProductRelate();
            // 加盟商
            productRelate.setClient(user.getClient());
            // 地点
            productRelate.setProSite(site);
            // 商品编码
            productRelate.setProSelfCode(proSelfCode);
            // 供应商编码
            productRelate.setSupSelfCode(supCode);
            // 关联商品编码
            productRelate.setProRelateCode(proRelateCode);
            // 采购价格
            productRelate.setProPrice(proPrice);
            // 创建人
            productRelate.setProCreateUser(user.getUserId());
            // 创建日期
            productRelate.setProCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            productRelate.setProCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            productRelateMapper.insert(productRelate);
        } else {
            // 关联商品已存在
            if (!CollectionUtils.isEmpty(list) && (list.size() > 1 || !list.get(0).getProSelfCode().equals(proSelfCode))) {
                throw new CustomResultException("当前代煎商品已存在");
            }
            // 关联商品编码
            productRelate.setProRelateCode(proRelateCode);
            // 采购价格
            productRelate.setProPrice(proPrice);
            // 变更人
            productRelate.setProChangeUser(user.getUserId());
            // 变更日期
            productRelate.setProChangeDate(DateUtils.getCurrentDateStrYYMMDD());
            // 变更时间
            productRelate.setProChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
            productRelateMapper.update(productRelate, productRelateQueryWrapper);
        }
        return ResultUtil.success();
    }

    /**
     * 煎药删除
     *
     * @param site
     * @param supCode
     * @param proSelfCodeList
     * @return
     */
    @Override
    public Result delProductRelate(String site, String supCode, List<String> proSelfCodeList) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<ProductRelate> productRelateQueryWrapper = new QueryWrapper<>();
        // 加盟商
        productRelateQueryWrapper.eq("CLIENT", user.getClient());
        // 地点
        productRelateQueryWrapper.eq("PRO_SITE", site);
        // 商品编码
        productRelateQueryWrapper.in("PRO_SELF_CODE", proSelfCodeList);
        // 供应商编码
        productRelateQueryWrapper.eq("SUP_SELF_CODE", supCode);
        productRelateMapper.delete(productRelateQueryWrapper);
        return ResultUtil.success();
    }

    /**
     * 煎药查询导出
     *
     * @param site
     * @param supCode
     * @param proSelfCode
     * @return
     */
    @Override
    public Result getProductRelateListExport(String site, String supCode, String proSelfCode) throws IOException {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 分页
        Page<ProductRelateDto> page = new Page<ProductRelateDto>(-1, -1);
        IPage<ProductRelateDto> iPage = productRelateMapper.selectProductRelateList(page, tokenUser.getClient(), site, supCode, proSelfCode);
        List<List<Object>> dataList = new ArrayList<>();
        if (iPage != null && !CollectionUtils.isEmpty(iPage.getRecords())) {
            iPage.getRecords().forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 序号
                data.add(index + 1);
                // 商品编码
                data.add(item.getProSelfCode());
                // 商品名称
                data.add(item.getProName());
                // 规格
                data.add(item.getProSpecs());
                // 厂家
                data.add(item.getProFactoryName());
                // 单位
                data.add(item.getProUnit());
                // 代煎供应商
                data.add(item.getSupSelfCode() + "-" + item.getSupName());
                // 代煎商品
                data.add(item.getProRelateCode());
                // 采购价格
                data.add(item.getProPrice());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("委托煎药数据");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "序号",
            "商品编码",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "代煎供应商",
            "代煎商品",
            "采购价格",
    };

}
