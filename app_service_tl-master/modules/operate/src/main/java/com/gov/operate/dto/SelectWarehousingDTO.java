package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectWarehousingDTO {

    private String id;

    /**
     * 业务类型
     */
    private String type;
    /**
     * 业务类型名
     */
    private String typeName;
    /**
     * 单据号
     */
    private String matDnId;
    /**
     * 单据日期
     */
    private String xgrq;
    /**
     * 单据去税金额
     */
    private BigDecimal excludingTaxAmount;
    /**
     * 单据税额
     */
    private BigDecimal taxAmount;
    /**
     * 单据总金额
     */
    private BigDecimal totalAmount;
    /**
     * 单据已登记金额
     */
    private BigDecimal registeredAmount;
    /**
     * 本次登记金额
     */
    private BigDecimal settlementAmount;
    /**
     * 单据剩余金额
     */
    private BigDecimal chargeAmount;

    /**
     * 本次付款金额
     */
    private BigDecimal paymentAmt;

    /**
     * 发票号
     */
    private String invoiceNum;
    /**
     * 已付款金额
     */
    private BigDecimal invoiceAmountOfThisPayment;
    /**
     * 付款单号
     */
    private String paymentOrderNo;
    /**
     * 付款单号
     */
    private String paymenthod;
    /**
     * 供应商编码
     */
    private String supSelfCode;
    /**
     * 供应商名
     */
    private String supSelfName;
    /**
     * 地点
     */
    private String site;
    /**
     * 供应商编码
     */
    private String deliveryTypeStore;
    /**
     * 供应商名
     */
    private String stoName;
    /**
     * 完结
     */
    private Integer isEnd;

    /**
     * 备注
     */
    private String remark;
    /**
     * 是否禁用
     */
    private String disabled;

    /**
     * 已对账金额
     */
    private BigDecimal gdbhBillAmount;

    /**
     * 已对账金额
     */
    private BigDecimal gibdBillAmount;

    /**
     * 明细
     */
    private List<SelectWarehousingDetailsDTO> detailList;

    /**
     * 发票集合
     */
    private List<String> invoiceNumList;

    /**
     * 商品编号
     */
    private String proCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 结算金额
     */
    private BigDecimal amount;

    /**
     * 结算数量
     */
    private BigDecimal qty;

    /**
     * 总数
     */
    private BigDecimal matQty;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 业务员
     */
    private String invoiceSalesmanName;

    /**
     * 业务员姓名
     */
    private String gssName;
}
