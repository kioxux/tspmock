package com.gov.operate.mapper;

import com.gov.operate.entity.ChajiaZ;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 采购入库差价单主表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-12-20
 */
public interface ChajiaZMapper extends BaseMapper<ChajiaZ> {

    List<ChajiaZ> selectChajiaList(@Param("client") String client, @Param("cjIdList") List<String> cjIdList);

}
