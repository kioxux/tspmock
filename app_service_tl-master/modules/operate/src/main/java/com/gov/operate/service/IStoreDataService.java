package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.dto.storeData.StoreDataAddVO;
import com.gov.operate.dto.storeData.StoreDataCodeDTO;
import com.gov.operate.dto.storeData.StoreDataDTO;
import com.gov.operate.dto.storeData.StoreDataEditVO;
import com.gov.operate.entity.StoreData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.service.request.StoreSalesRankingRequest;
import com.gov.operate.service.request.StoreSalesStatisticsRequest;
import com.gov.operate.service.response.StoreSalesRankingResponse;
import com.gov.operate.service.response.StoreSalesStatisticsRes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface IStoreDataService extends SuperService<StoreData> {

    /**
     * 当前人门店列表
     */
    Result getCurrentStoreList();

    /**
     * 当前加盟商下所有的门店
     */
    List<StoreData> getStoreList();

    /**
     * 门店详情
     * @param stoCode 门店编码
     */
    StoreDataDTO getStoreInfo(String stoCode);

    /**
     * 门店新增
     * @param storeDataAddVO
     * @param tokenUser
     */
    void addStore(StoreDataAddVO storeDataAddVO, TokenUser tokenUser);

    /**
     * 门店编辑
     * @param storeDataEditVO
     */
    void editStore(StoreDataEditVO storeDataEditVO, TokenUser tokenUser);

    void saveDefaultStoreGroupSet(String client);

    /**
     * 门店编码获取
     */
    StoreDataCodeDTO getStoreCode();

    /**
     *门店分类获取
     */
    List<Map<String, Object>> getStoreGroupCode();

    /**
     * 门店分类详情
     * @param storeGroupDto
     * @return
     */
    List<StoreGroupVo> getStoreGroup(StoreGroupDto storeGroupDto);

    /**
     * 门店分类编辑
     * @param storeGroupVo
     */
    void editStoreGroup(StoreGroupVo storeGroupVo);

    /**
     * 设置默认门店分类
     */
    void setDefaultStoreGroup();

    /**
     * 门店分类导出
     * @param storeGroupDto
     * @return
     */
    Result storeGroupExport(StoreGroupDto storeGroupDto) throws IOException;

    void saveGroupSet(List<StoreGroupSetDto> dto);

    /** add by jinwencheng on 2021-12-23 15:41:35 添加门店分类 - 门店选择 - 查询 **/
    List<GetStoresForAddVO> getStoresForAdd(GetStoresForAddDTO getStoresForAddDTO);
    /** add end **/

    /** add by jinwencheng on 2021-12-23 15:41:35 添加门店分类 - 门店选择 - 查询 **/
    Result batchImport(HttpServletRequest request, MultipartFile file);
    /** add end **/

    /** add by jinwencheng on 2021-12-24 11:21:13 保存门店分类 **/
    void saveStoreGroup(List<StoresGroupAddDTO> storesGroupAddDTO);
    /** add end **/

    StoreSalesRankingResponse getStoreSalesRanking(StoreSalesRankingRequest request) throws ParseException;

    /**
     * 根据门店统计数据
     * @param request
     * @return
     */
    StoreSalesStatisticsRes getStoreStatistics(StoreSalesStatisticsRequest request) throws ParseException;

    /**
     * 查询一级区域
     */
    List<RegionalPlanningModel> getFristArea();

    /**
     * 根据子级区域查询父级区域
     */
    RegionalPlanningModel getRegionalIdByRegional(RegionalPlanningModel regionalPlanningModel);

    /**
     * 行政区域
     */
    List<GaiaArea> findAreaByLevel();

    /**
     * 行政区域
     */
//    List<GaiaArea> findAreaByParentId(GaiaArea area);
}
