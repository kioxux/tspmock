package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_COMPONENT")
@ApiModel(value="ProductComponent对象", description="")
public class ProductComponent extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "成分分类编码")
    @TableId("PRO_COMP_CODE")
    private String proCompCode;

    @ApiModelProperty(value = "成分分类名称")
    @TableField("PRO_COMP_NAME")
    private String proCompName;

    @ApiModelProperty(value = "成分分类状态")
    @TableField("PRO_COMP_STATUS")
    private String proCompStatus;


}
