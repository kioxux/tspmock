package com.gov.operate.dto;

import lombok.Data;

@Data
public class MemberCard {
    private String userId;
    private String userName;
    private String count;
}
