package com.gov.operate.mapper;

import com.gov.operate.entity.SdMemberTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
public interface SdMemberTagMapper extends BaseMapper<SdMemberTag> {

}
