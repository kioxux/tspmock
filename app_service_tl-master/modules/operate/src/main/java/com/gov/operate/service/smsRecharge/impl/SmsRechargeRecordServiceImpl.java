package com.gov.operate.service.smsRecharge.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.storePayment.InvoiceSms;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.smsRecharge.ISmsRechargeRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Service
public class SmsRechargeRecordServiceImpl extends ServiceImpl<SmsRechargeRecordMapper, SmsRechargeRecord> implements ISmsRechargeRecordService {

    @Resource
    private SmsRechargeRecordMapper smsRechargeRecordMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private SmsSendMapper smsSendMapper;
    @Resource
    private ClientInvoiceInfoMapper clientInvoiceInfoMapper;
    @Resource
    private FicoInvoiceMapper ficoInvoiceMapper;
    @Resource
    private UserDataMapper userDataMapper;

    /**
     * 短信购买记录
     *
     * @param pageNum
     * @param pageSize
     * @param gsrrRechargeTimeStart
     * @param gsrrRechargeTimeEnd
     * @return
     */
    @Override
    public Result getRechargeRecord(Integer pageNum, Integer pageSize, String gsrrRechargeTimeStart, String gsrrRechargeTimeEnd) {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 明细
        QueryWrapper<SmsRechargeRecord> smsRechargeRecordQueryWrapper = new QueryWrapper<>();
        smsRechargeRecordQueryWrapper.ge(StringUtils.isNotBlank(gsrrRechargeTimeStart), "LEFT(GSRR_RECHARGE_TIME, 8)", gsrrRechargeTimeStart);
        smsRechargeRecordQueryWrapper.le(StringUtils.isNotBlank(gsrrRechargeTimeEnd), "LEFT(GSRR_RECHARGE_TIME, 8)", gsrrRechargeTimeEnd);
        smsRechargeRecordQueryWrapper.eq("CLIENT", tokenUser.getClient());
        smsRechargeRecordQueryWrapper.eq("GSRR_PAY_STATUS", 1);
        smsRechargeRecordQueryWrapper.orderByDesc("GSRR_RECHARGE_TIME");
        Page<SmsRechargeRecord> page = new Page<>(pageNum, pageSize);
        IPage<SmsRechargeRecord> iPage = smsRechargeRecordMapper.selectPage(page, smsRechargeRecordQueryWrapper);
        return ResultUtil.success(iPage);
    }

    /**
     * 短信可用数量
     *
     * @return
     */
    @Override
    public Result getAvailableQuantity() {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 统计
        QueryWrapper<SmsRechargeRecord> smsRechargeRecordQueryWrapper = new QueryWrapper<>();
        smsRechargeRecordQueryWrapper.select("IFNULL(SUM(GSRR_RECHARGE_COUNT) - SUM(GSRR_RECHARGE_USE_COUNT), 0) AS GSRR_RECHARGE_COUNT");
        smsRechargeRecordQueryWrapper.eq("CLIENT", tokenUser.getClient());
        smsRechargeRecordQueryWrapper.eq("GSRR_PAY_STATUS", 1);
        SmsRechargeRecord smsRechargeRecord = smsRechargeRecordMapper.selectOne(smsRechargeRecordQueryWrapper);
        return ResultUtil.success(smsRechargeRecord.getGsrrRechargeCount());
    }

    /**
     * 短信发送记录
     *
     * @param pageNum
     * @param pageSize
     * @param gssTel
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    @Override
    public Result getSmsSendRecord(Integer pageNum, Integer pageSize, String gssTel, String gssSendTimeStart, String gssSendTimeEnd) {
        TokenUser tokenUser = commonService.getLoginInfo();
        QueryWrapper<SmsSend> smsSendQueryWrapper = new QueryWrapper<SmsSend>();
        smsSendQueryWrapper.eq("CLIENT", tokenUser.getClient());
        smsSendQueryWrapper.eq(StringUtils.isNotBlank(gssTel), "GSS_TEL", gssTel);
        smsSendQueryWrapper.ge(StringUtils.isNotBlank(gssSendTimeStart), "LEFT(GSS_SEND_TIME, 8)", gssSendTimeStart);
        smsSendQueryWrapper.le(StringUtils.isNotBlank(gssSendTimeEnd), "LEFT(GSS_SEND_TIME, 8)", gssSendTimeEnd);
        smsSendQueryWrapper.orderByDesc("GSS_SEND_TIME");
        Page<SmsSend> page = new Page<>(pageNum, pageSize);
        IPage<SmsSend> iPage = smsSendMapper.selectPage(page, smsSendQueryWrapper);
        return ResultUtil.success(iPage);
    }

    /**
     * 开票短信信查询
     *
     * @param pageNum
     * @param pageSize
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    @Override
    public Result getInvoiceSmsList(Integer pageNum, Integer pageSize, String gssSendTimeStart, String gssSendTimeEnd) {
        TokenUser tokenUser = commonService.getLoginInfo();
        QueryWrapper<SmsSend> smsSendQueryWrapper = new QueryWrapper<SmsSend>();
        smsSendQueryWrapper.eq("CLIENT", tokenUser.getClient());
        smsSendQueryWrapper.ge(StringUtils.isNotBlank(gssSendTimeStart), "LEFT(GSS_SEND_TIME, 8)", gssSendTimeStart);
        smsSendQueryWrapper.le(StringUtils.isNotBlank(gssSendTimeEnd), "LEFT(GSS_SEND_TIME, 8)", gssSendTimeEnd);
        smsSendQueryWrapper.eq("GSS_INVOICE", 0);
        smsSendQueryWrapper.orderByAsc("GSS_SEND_TIME");
        Page<SmsSend> page = new Page<>(pageNum, pageSize);
        IPage<SmsSend> iPage = smsSendMapper.selectPage(page, smsSendQueryWrapper);
        InvoiceSms invoiceSms = new InvoiceSms();
        invoiceSms.setIPage(iPage);
        // 总金额
        smsSendQueryWrapper.select("IFNULL(SUM(GSS_PRICE), 0) AS GSS_PRICE");
        SmsSend smsSend = smsSendMapper.selectOne(smsSendQueryWrapper);
        invoiceSms.setAmt(smsSend.getGssPrice());
        return ResultUtil.success(invoiceSms);
    }

    /**
     * 开票信息查询
     *
     * @return
     */
    @Override
    public Result getInvoiceInfo() {
        TokenUser tokenUser = commonService.getLoginInfo();
        QueryWrapper<ClientInvoiceInfo> clientInvoiceInfoQueryWrapper = new QueryWrapper<ClientInvoiceInfo>();
        clientInvoiceInfoQueryWrapper.eq("CLIENT", tokenUser.getClient());
        List<ClientInvoiceInfo> list = clientInvoiceInfoMapper.selectList(clientInvoiceInfoQueryWrapper);
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        return ResultUtil.success(list.get(0));
    }

    /**
     * 开票信息保存
     *
     * @param ficoSocialCreditCode
     * @param ficoTaxSubjectName
     * @param ficoBankName
     * @param ficoBankNumber
     * @return
     */
    @Override
    public Result saveInvoiceInfo(String ficoSocialCreditCode, String ficoTaxSubjectName, String ficoBankName, String ficoBankNumber) {
        TokenUser tokenUser = commonService.getLoginInfo();
        QueryWrapper<ClientInvoiceInfo> clientInvoiceInfoQueryWrapper = new QueryWrapper<ClientInvoiceInfo>();
        clientInvoiceInfoQueryWrapper.eq("CLIENT", tokenUser.getClient());
        List<ClientInvoiceInfo> list = clientInvoiceInfoMapper.selectList(clientInvoiceInfoQueryWrapper);
        // 新增
        ClientInvoiceInfo clientInvoiceInfo = new ClientInvoiceInfo();
        // 统一社会信用代码
        clientInvoiceInfo.setFicoSocialCreditCode(ficoSocialCreditCode);
        // 纳税主体名称
        clientInvoiceInfo.setFicoTaxSubjectName(ficoTaxSubjectName);
        // 开户行名称
        clientInvoiceInfo.setFicoBankName(ficoBankName);
        // 开户行账号
        clientInvoiceInfo.setFicoBankNumber(ficoBankNumber);
        if (CollectionUtils.isEmpty(list)) {
            // 加盟商
            clientInvoiceInfo.setClient(tokenUser.getClient());
            clientInvoiceInfoMapper.insert(clientInvoiceInfo);
        } else {
            clientInvoiceInfoMapper.update(clientInvoiceInfo, clientInvoiceInfoQueryWrapper);
        }
        return ResultUtil.success();
    }

    /**
     * 短信开票申请
     *
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result sendInvoice(String gssSendTimeStart, String gssSendTimeEnd) {
        TokenUser tokenUser = commonService.getLoginInfo();
        QueryWrapper<ClientInvoiceInfo> clientInvoiceInfoQueryWrapper = new QueryWrapper<ClientInvoiceInfo>();
        clientInvoiceInfoQueryWrapper.eq("CLIENT", tokenUser.getClient());
        List<ClientInvoiceInfo> clientInvoiceInfoList = clientInvoiceInfoMapper.selectList(clientInvoiceInfoQueryWrapper);
        if (CollectionUtils.isEmpty(clientInvoiceInfoList)) {
            throw new CustomResultException("请先维护开票信息");
        }
        // 开票信息
        ClientInvoiceInfo clientInvoiceInfo = clientInvoiceInfoList.get(0);
        // 开票短信
        QueryWrapper<SmsSend> smsSendQueryWrapper = new QueryWrapper<SmsSend>();
        smsSendQueryWrapper.eq("CLIENT", tokenUser.getClient());
        smsSendQueryWrapper.ge(StringUtils.isNotBlank(gssSendTimeStart), "LEFT(GSS_SEND_TIME, 8)", gssSendTimeStart);
        smsSendQueryWrapper.le(StringUtils.isNotBlank(gssSendTimeEnd), "LEFT(GSS_SEND_TIME, 8)", gssSendTimeEnd);
        smsSendQueryWrapper.eq("GSS_INVOICE", 0);
        List<SmsSend> smsSendList = smsSendMapper.selectList(smsSendQueryWrapper);
        if (CollectionUtils.isEmpty(smsSendList)) {
            throw new CustomResultException("未查询到可开票的短信发送记录");
        }
        // 总金额
        smsSendQueryWrapper.select("IFNULL(SUM(GSS_PRICE), 0) AS GSS_PRICE");
        SmsSend entity = smsSendMapper.selectOne(smsSendQueryWrapper);
        // 未查询到开票金额
        if (entity == null || entity.getGssPrice() == null || entity.getGssPrice().intValue() == 0) {
            throw new CustomResultException("发送短信金额为0");
        }
        // 更新短信开票记录
        SmsSend smsSend = new SmsSend();
        smsSend.setGssInvoice(1);
        int updates = smsSendMapper.update(smsSend, smsSendQueryWrapper);
        if (updates == 0) {
            throw new CustomResultException("未查询到可开票的短信发送记录");
        }
        // 生成发票信息
        FicoInvoice ficoInvoice = new FicoInvoice();
        // 加盟商
        ficoInvoice.setClient(tokenUser.getClient());
        // 付款订单号
        ficoInvoice.setFicoId(DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(7));
        // 发票订单号
        ficoInvoice.setFicoOrderNo(DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + org.apache.commons.lang.RandomStringUtils.randomNumeric(3));
        // 发票行号
        ficoInvoice.setFicoInvoceLineNum("1");
        // 纳税主体编码
        ficoInvoice.setFicoTaxSubjectCode("-");
        // 纳税主体名称
        ficoInvoice.setFicoTaxSubjectName(clientInvoiceInfo.getFicoTaxSubjectName());
        // 发票类型
        ficoInvoice.setFicoInvoiceType("1");
        // 统一社会信用代码
        ficoInvoice.setFicoSocialCreditCode(clientInvoiceInfo.getFicoSocialCreditCode());
        // 开户行名称
        ficoInvoice.setFicoBankName(clientInvoiceInfo.getFicoBankName());
        // 开户行账号
        ficoInvoice.setFicoBankNumber(clientInvoiceInfo.getFicoBankNumber());
        // 地址
        UserData userData = userDataMapper.selectOne(new QueryWrapper<UserData>().eq("CLIENT", tokenUser.getClient()).eq("USER_ID", tokenUser.getUserId()));
        ficoInvoice.setFicoCompanyPhoneNumber(userData.getUserTel());
        // 电话号码
        // 付款金额
        ficoInvoice.setFicoInvoiceAmount(new BigDecimal(entity.getGssPrice()).divide(new BigDecimal(100)));
        // 发票回调成功PDF地址
        // 发票请求流水号
        // 发票号码
        // 发票代码
        // 发票日期
        // 发票状态
        ficoInvoice.setFicoInvoiceStatus("0");
        // 开票返回结果
        // 发标唯一值
        // 发票区分
        ficoInvoice.setFicoType(1);
        // 业务类型
        ficoInvoice.setFicoClass(2);
        // 开票人
        ficoInvoice.setFicoOperator(tokenUser.getUserId());
        ficoInvoiceMapper.insert(ficoInvoice);
        return ResultUtil.success();
    }
}
