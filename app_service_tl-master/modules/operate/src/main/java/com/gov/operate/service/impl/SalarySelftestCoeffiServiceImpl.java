package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditSelftestCoeffiVO;
import com.gov.operate.entity.SalarySelftestCoeffi;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SalarySelftestCoeffiMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISalarySelftestCoeffiService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
@Service
public class SalarySelftestCoeffiServiceImpl extends ServiceImpl<SalarySelftestCoeffiMapper, SalarySelftestCoeffi> implements ISalarySelftestCoeffiService {

    @Resource
    private CommonService commonService;

    /**
     * 自测数据获取
     */
    @Override
    public List<SalarySelftestCoeffi> getSelftestCoeffi() {
        TokenUser user = commonService.getLoginInfo();
        List<SalarySelftestCoeffi> selftestCoeffiList = this.list(new QueryWrapper<SalarySelftestCoeffi>().eq("CLIENT", user.getClient()));
        selftestCoeffiList.forEach(coeffi->{
            coeffi.setGsscBasic(DecimalUtils.toPercent(coeffi.getGsscBasic()));
            coeffi.setGsscPost(DecimalUtils.toPercent(coeffi.getGsscPost()));
            coeffi.setGsscPerformance(DecimalUtils.toPercent(coeffi.getGsscPerformance()));
            coeffi.setGsscEvaluation(DecimalUtils.toPercent(coeffi.getGsscEvaluation()));
        });
        return selftestCoeffiList;
    }

    /**
     * 自测数据修改
     */
    @Override
    public void editSelftestCoeffi(EditSelftestCoeffiVO vo) {
        BigDecimal all = vo.getGsscBasic().add(vo.getGsscPost()).add(vo.getGsscPerformance()).add(vo.getGsscEvaluation());
        if (new BigDecimal(100).compareTo(all) != 0) {
            throw new CustomResultException("基本工资,岗位工资,绩效工资,考评工资 比例之和应为 100%");
        }
        TokenUser user = commonService.getLoginInfo();
        SalarySelftestCoeffi selftestCoeffiExit = this.getOne(new QueryWrapper<SalarySelftestCoeffi>().eq("CLIENT", user.getClient()).eq("GSSC_TYPE", vo.getGsscType()));

        SalarySelftestCoeffi selftestCoeffi = new SalarySelftestCoeffi();
        // 加盟商
        selftestCoeffi.setClient(user.getClient());
        // 系数类型
        selftestCoeffi.setGsscType(vo.getGsscType());
        // 基本工资
        selftestCoeffi.setGsscBasic(vo.getGsscBasic().divide(Constants.ONE_HUNDRED, Constants.SCALE, BigDecimal.ROUND_HALF_UP));
        // 岗位工资
        selftestCoeffi.setGsscPost(vo.getGsscPost().divide(Constants.ONE_HUNDRED, Constants.SCALE, BigDecimal.ROUND_HALF_UP));
        // 绩效工资
        selftestCoeffi.setGsscPerformance(vo.getGsscPerformance().divide(Constants.ONE_HUNDRED, Constants.SCALE, BigDecimal.ROUND_HALF_UP));
        // 考评工资
        selftestCoeffi.setGsscEvaluation(vo.getGsscEvaluation().divide(Constants.ONE_HUNDRED, Constants.SCALE, BigDecimal.ROUND_HALF_UP));
        // 新增
        if (ObjectUtils.isEmpty(selftestCoeffiExit)) {
            this.save(selftestCoeffi);
            return;
        }
        // 更新
        selftestCoeffi.setGsscId(selftestCoeffiExit.getGsscId());
        this.updateById(selftestCoeffi);
    }

}
