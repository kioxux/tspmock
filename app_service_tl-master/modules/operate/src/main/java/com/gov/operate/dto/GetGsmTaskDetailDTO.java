package com.gov.operate.dto;

import com.gov.operate.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetGsmTaskDetailDTO extends SdMarketingClient {

    /**
     * 门店编码（当前/历史营销用到）
     */
    private String stoCode;
    /**
     * 门店名称（当前/历史营销用到）
     */
    private String stoName;
    /**
     * 短信查询条件id （当前/历史营销用到）
     */
    private String gsmSmscondiId;
    /**
     * 电话查询条件id （当前/历史营销用到）
     */
    private String gsmTelcondiId;

    /**
     * 门店列表 （推荐营销用到）
     */
    private List<StoreData> stoList;
    /**
     * 促销列表
     */
    private List<Prom> promList;

    @Data
    @EqualsAndHashCode
    public static class Prom {
        private Integer id;

        private String client;

        private Integer marketingId;

        private String componentId;

        private String componentName;

        private String name;

        private String type;

        private String gspvsMode;

        private String gspvsModeName;

        private String gspvsType;

        private String gspvsTypeName;

        private String remarks1;

        private String remarks2;
        /**
         * 具体参数
         */
        private List<Param> paramList;
        /**
         * 选品数量
         */
        private Integer countPrd;
        /**
         * 赠品数量
         */
        private Integer countGiveaway;
        /**
         * 商品列表
         */
        private List<SdMarketingPrdDTO> prdList;
        /**
         * 赠品列表
         */
        private List<SdMarketingGiveawayDTO> giveawayList;
    }

    @Data
    @EqualsAndHashCode
    public static class Param extends SdPromVariableSet {
        /**
         * 列的值
         */
        private String gspvsColumnsValue;
    }

    @Data
    @EqualsAndHashCode
    public static class SdMarketingPrdDTO extends SdMarketingPrd {
        /**
         * 商品编码
         */
        private String proSelfCode;
    }

    @Data
    @EqualsAndHashCode
    public static class SdMarketingGiveawayDTO extends SdMarketingGiveaway {
        /**
         * 商品编码
         */
        private String proSelfCode;
    }


}
