package com.gov.operate.service;

import com.gov.operate.entity.PoHeader;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 采购订单主表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
public interface IPoHeaderService extends SuperService<PoHeader> {

}
