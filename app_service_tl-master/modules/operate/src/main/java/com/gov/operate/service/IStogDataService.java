package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.entity.StogData;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface IStogDataService extends SuperService<StogData> {

    Result selectStogDateList();
}
