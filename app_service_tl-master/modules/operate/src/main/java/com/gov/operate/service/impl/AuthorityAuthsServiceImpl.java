package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.utils.CommonUtil;
import com.gov.operate.dto.AuthVO;
import com.gov.operate.dto.SystemServicePackVO;
import com.gov.operate.dto.auth.ClientAuth;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IAuthorityAuthsClientService;
import com.gov.operate.service.IAuthorityAuthsService;
import com.gov.operate.service.IFranchiseeService;
import com.netflix.hystrix.contrib.javanica.utils.CommonUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Service
public class AuthorityAuthsServiceImpl extends ServiceImpl<AuthorityAuthsMapper, AuthorityAuths> implements IAuthorityAuthsService {
    @Resource
    private CommonService commonService;
    @Resource
    private AuthorityAuthsMapper authsMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private IAuthorityAuthsClientService iAuthorityAuthsClientService;
    @Resource
    private IFranchiseeService iFranchiseeService;
    @Resource
    private SystemServicePackMapper systemServicePackMapper;
    @Resource
    private PayingItemNewMapper payingItemNewMapper;

    /**
     * 获取权限列表
     */
    @Override
    public Result getAuthList(Integer gaurRoleId) {
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        map.put("ROLEID", gaurRoleId);
        List<AuthVO> authsList = authsMapper.SelectForAuthList(map);
        List<AuthVO> parentList = Parent(authsList);
        return ResultUtil.success(parentList);
    }

    private List<AuthVO> Parent(List<AuthVO> authsList) {
        List<AuthVO> filterList = new ArrayList<>();
        if (!authsList.isEmpty()) {
            //根据父节点的Id来分组
            Map<Integer, List<AuthVO>> map = authsList.stream()
                    .filter(o -> Objects.nonNull(o.getId()))
                    .collect(Collectors.groupingBy(AuthVO::getGaaParentId));
            //循环处理子节点 构建树状结构
            authsList.forEach(goodsType -> {
                if (map.containsKey(goodsType.getId())) {
                    goodsType.setChildren(map.get(goodsType.getId()));
                }
            });

            //过滤下面不是不是第一级开头的数据
            filterList = authsList.stream().filter(goodType ->
                    Objects.equals(goodType.getGaaParentId(), 0)).collect(Collectors.toList());

        }
        return filterList;
    }


    /**
     * 收费权限
     */
    @Override
    public Result getFeeAuth() {
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> returnMap = new HashMap<>();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        map.put("USER_ID", user.getUserId());

        Set<String> all = new HashSet<>();
        // 加盟商
        Franchisee franchisee = iFranchiseeService.getById(user.getClient());
        // 加盟商负责人
        if (user.getUserId().equals(franchisee.getFrancLegalPerson()) || user.getUserId().equals(franchisee.getFrancAss())) {
            // 当前加盟商所有权限
            List<String> authsList = authsMapper.selectAuthByUserClient(user.getClient());
            all.addAll(authsList);
        } else {
            List<AuthVO> authsList = authsMapper.SelectRoleAuthList(map);
            if (!CollectionUtils.isEmpty(authsList)) {
                //用户所拥有角色的权限
                authsList.forEach(item -> {
                    all.add(item.getGaaCode());
                });
            }
        }
        Set<String> feeList = new HashSet<>();
        String flag = "";
        //收费的权限 (前端显示收费的权限 ，但是功能不能使用)
        List<AuthorityAuths> auths = authsMapper.selectList(new QueryWrapper<AuthorityAuths>()
                .eq("GAA_SFSQ", "1")
                .eq("GAA_FLAG", "1")
        );
        //免费的
        List<AuthorityAuths> listAuth = authsMapper.selectList(new QueryWrapper<AuthorityAuths>()
                .eq("GAA_SFSQ", "1")
                .eq("GAA_FLAG", "0")
        );
        QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<UserData>();
        userDataQueryWrapper.eq("CLIENT", user.getClient());
        userDataQueryWrapper.eq("USER_ID", user.getUserId());
        UserData userData = userDataMapper.selectOne(userDataQueryWrapper);
        if (StringUtils.isBlank(userData.getDepId())) {
            // 用户不在门店表和配送中心主数据表
            // 前端显示勾选的 的权限  收费的权限 功能灰调
            flag = "1";
        } else {
            map.put("CODE", userData.getDepId());
            List<String> list = authsMapper.SelectCountExit(map);
            if (list.isEmpty()) {
                //用户不在门店表和配送中心主数据表
                // 前端显示勾选的 的权限  收费的权限 功能灰调
                flag = "1";
            } else {
                Integer a = authsMapper.SelectPayExit(map);
                if (a > 0) {
                    flag = "1";
                    // 前端显示勾选的 的权限  收费的权限 功能灰调
                } else {
                    flag = "2";
                    // 前端显示勾选的和免费的交集
                }
            }
        }

        returnMap.put("allAuthList", all); //用户勾选的权限 有免费 有收费
        if ("1".equals(flag)) {
            if (!listAuth.isEmpty()) {
                listAuth.forEach(item -> {
                    feeList.add(item.getGaaCode());
                });
            }
            if (!auths.isEmpty()) {
                auths.forEach(item -> {
                    feeList.add(item.getGaaCode());
                });
            }
        } else {
            if (!listAuth.isEmpty()) {
                listAuth.forEach(item -> {
                    feeList.add(item.getGaaCode());
                });
            }
        }
        returnMap.put("feeList", feeList); //所有收费的
        //收费包功能扩展
        returnMap.put("sysPackageList", getSysPackageList(feeList, user)); //所有收费的

//        returnMap.put("freeList", freeList); //所有免费的
        returnMap.put("flag", flag);
        return ResultUtil.success(returnMap);
    }

    /**
     * @param feeSet app端所有渲染的菜单集合
     * @return
     */
    private Map<String, String> getSysPackageList(Set<String> feeSet, TokenUser user) {
        Map<String, String> sysPackageMap = new HashMap<>();
        Set<String> ficoIds = new HashSet<>();
        String nowDate = CommonUtil.getyyyyMMdd();
        List<PayingItemNew> payInfoList = getPayInfo(user);
        List<PayingItemNew> inUsePayItemInfoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(payInfoList)) {
            for (PayingItemNew itemNew : payInfoList) {
                if (StringUtils.isNotEmpty(itemNew.getFicoStartingTime()) && StringUtils.isNotEmpty(itemNew.getFicoEndingTime())) {
                    if (Integer.parseInt(itemNew.getFicoStartingTime()) <= Integer.parseInt(nowDate) && Integer.parseInt(itemNew.getFicoEndingTime()) >= Integer.parseInt(nowDate)) {
                        ficoIds.add(itemNew.getFicoId());
                    }
                }
            }
        } else {
            //目前存在一种情况，用户实际没有走购买流程，按需求文档进行不放行操作，将所有的权限进行不放行处理
            for (String resouceId : feeSet) {
                sysPackageMap.put(resouceId, "N");
            }
        }
        if (ficoIds.size() > 0) {
//            String finalFicoId = ficoId;
            List<String> finalFicoIds = new ArrayList<>(ficoIds);
            //获取用户实际使用的功能包
            inUsePayItemInfoList = payInfoList.stream().filter(x -> finalFicoIds.contains(x.getFicoId()) && (Integer.parseInt(x.getFicoStartingTime()) <= Integer.parseInt(nowDate) && Integer.parseInt(x.getFicoEndingTime()) >= Integer.parseInt(nowDate))).collect(Collectors.toList());
            List<Integer> rIds = inUsePayItemInfoList.stream().map(x -> x.getRId()).collect(Collectors.toList());
            if(CollectionUtils.isEmpty(rIds)){
                for (String resouceCode : feeSet) {
                    sysPackageMap.put(resouceCode, "N");
                }
                return sysPackageMap;
            }
            List<String> resourceCodeList = new ArrayList<>();
            for (String resouceCode : feeSet) {
                resourceCodeList.add(resouceCode);
            }

            List<AuthorityAuths> resourceInfoList = getResourceInfoList(resourceCodeList);
            List<Integer> resourceIdList = new ArrayList<>();
            Map<Integer, String> resourceIdCodeMap = new HashMap<>();
            Map<String, Integer> resourceCodeIdMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(resourceInfoList)) {
                resourceInfoList.forEach(x -> {
                    resourceIdCodeMap.put(x.getId(), x.getGaaCode());
                    resourceCodeIdMap.put(x.getGaaCode(), x.getId());
                    resourceIdList.add(x.getId());
                });
            }
            //查询db的配置信息，转换成resourceId->功能包spId的格式
            List<SystemServicePackVO> packList = systemServicePackMapper.selectResourceServicePackageRelationsByResourceIds(resourceIdList);
            Map<String, List<Integer>> resourceRIdsMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(packList)) {
                for (SystemServicePackVO pack : packList) {
                    if (resourceRIdsMap.containsKey(pack.getResourceId())) {
                        List<Integer> dataList = resourceRIdsMap.get(pack.getResourceId());
                        dataList.add(Integer.parseInt(pack.getRId()));
                        resourceRIdsMap.put(pack.getResourceId(), dataList);
                    } else {
                        List<Integer> dataList = new ArrayList<>();
                        dataList.add(Integer.parseInt(pack.getRId()));
                        resourceRIdsMap.put(pack.getResourceId(), dataList);
                    }
                }
                sysPackageMap = getFinalResourceIdShowMap(resourceRIdsMap, new ArrayList<>(rIds), feeSet, resourceCodeIdMap);
            } else {
                //完全没有配置功能包的情况下，所有的菜单需要置灰
                for (String resouceId : feeSet) {
                    sysPackageMap.put(resouceId, "N");
                }
                return sysPackageMap;
            }
        } else {
            //用户购买过功能包，但是没有一个存在于有效期内，这个时候需要全部置灰
            for (String resouceId : feeSet) {
                sysPackageMap.put(resouceId, "N");
            }
            return sysPackageMap;
        }
        return sysPackageMap;
    }

    private List<AuthorityAuths> getResourceInfoList(List<String> resourceCodeList) {
        List<AuthorityAuths> res = new ArrayList<>();
        QueryWrapper<AuthorityAuths> queryWrapper = new QueryWrapper<AuthorityAuths>();
        queryWrapper.in("GAA_CODE", resourceCodeList);
        List<AuthorityAuths> auths = authsMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(auths)) {
            res = auths;
        }
        return res;
    }

    /**
     * @param resourceSpIdsMap db中存储的关系
     * @param spIds            用户购买的功能包
     * @return
     */
    private Map<String, String> getFinalResourceIdShowMap(Map<String, List<Integer>> resourceRIdsMap, List<Integer> rIds, Set<String> feeSet, Map<String, Integer> resourceCodeIdMap) {
        Map<String, String> res = new HashMap<>();
        feeSet.forEach(x -> {
            Integer resourceId = resourceCodeIdMap.get(x);
            List<Integer> rIdDbList = resourceRIdsMap.get(resourceId + "");
            if (CollectionUtils.isEmpty(rIdDbList) || CollectionUtils.isEmpty(rIds)) {
                res.put(x, "N");
            } else {
                if (!CollectionUtils.isEmpty(rIdDbList)) {
                    for(Integer rId: rIdDbList){
                        if(rIds.contains(rId)){
                            res.put(x, "Y");
                            break;
                        }else{
                            res.put(x, "N");
                        }
                    }
                }else{
                    res.put(x, "N");
                }
            }
        });
        return res;
    }

    private List<PayingItemNew> getPayInfo(TokenUser user) {
        List<PayingItemNew> resList = new ArrayList<>();
        QueryWrapper<PayingItemNew> queryWrapper = new QueryWrapper<PayingItemNew>();
        queryWrapper.eq("CLIENT", user.getClient());
        queryWrapper.eq("FICO_SITE_CODE", user.getDepId());
        List<PayingItemNew> payingItemNews = payingItemNewMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(payingItemNews)) {
            resList = payingItemNews;
        }
        return resList;
    }

    /**
     * 加盟商权限
     *
     * @param client
     * @return
     */
    @Override
    public Result getAuthByClient(String client) {
        List<ClientAuth> result = new ArrayList<>();
        List<ClientAuth> list = authsMapper.selectAuthByClient(client);
        initAuth(list, result, 0);
        return ResultUtil.success(result);
    }

    /**
     * 保存加盟商权限
     *
     * @param clientAuth
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveClientAuth(ClientAuth clientAuth) {
        // 删除原权限数据
        iAuthorityAuthsClientService.remove(new QueryWrapper<AuthorityAuthsClient>().eq("CLIENT", clientAuth.getClient()));
        if (!CollectionUtils.isEmpty(clientAuth.getAuthList())) {
            List<AuthorityAuthsClient> list = new ArrayList<>();
            for (Integer authId : clientAuth.getAuthList()) {
                AuthorityAuthsClient authorityAuthsClient = new AuthorityAuthsClient();
                authorityAuthsClient.setClient(clientAuth.getClient());
                authorityAuthsClient.setGaacAuthId(authId);
                list.add(authorityAuthsClient);
            }
            iAuthorityAuthsClientService.saveBatch(list);
        }
    }

    private void initAuth(List<ClientAuth> list, List<ClientAuth> result, int pid) {
        for (ClientAuth clientAuth : list) {
            if (pid == clientAuth.getGaaParentId().intValue()) {
                result.add(clientAuth);
                if (clientAuth.getChild() == null) {
                    clientAuth.setChild(new ArrayList<ClientAuth>());
                }
                initAuth(list, clientAuth.getChild(), clientAuth.getId());
            }
        }
    }
}
