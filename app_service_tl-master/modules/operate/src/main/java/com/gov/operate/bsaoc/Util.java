package com.gov.operate.bsaoc;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Date;

/**
 * @program: openPlatform
 * @description:   工具类
 * @author: Mr.YS
 * @CreatDate: 2019/5/20/020 17:20
 */
public class Util {

    /**
     * 获取到订单号
     *
     * @return 订单号
     */
    public static String getMerchantOrderId() {
        return DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(7);
    }
}
