package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SendMarketingSmsVO {

//    @NotBlank(message = "营销活动ID不能为空")
    private String marketId;

//    @NotBlank(message = "常用查询ID不能为空")
    private String gsmsId;

    @NotBlank(message = "模版id不能为空")
    private String smsId;

    @NotEmpty(message = "会员集合不能为空")
    private List<MemberVO> memberList;
}
