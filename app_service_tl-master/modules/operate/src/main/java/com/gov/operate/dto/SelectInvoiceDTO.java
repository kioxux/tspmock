package com.gov.operate.dto;

import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class SelectInvoiceDTO extends FicoInvoiceInformationRegistration {

    /**
     * 剩余金额
     */
    private BigDecimal chargeAmount;

    /**
     * 本次付款金额 默认等于剩余金额
     */
    private BigDecimal paymentAmount;

    /**
     * 付款单号
     */
    private String paymentOrderNo;

    /**
     * 付款方式
     */
    private String paymenthod;

    /**
     * 完结
     */
    private Integer isEnd;

    /**
     * 地点名
     */
    private String stoName;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 发票关联单据
     */
    private String billNum;

    /**
     * 业务员
     */
    private String invoiceSalesmanName;
}
