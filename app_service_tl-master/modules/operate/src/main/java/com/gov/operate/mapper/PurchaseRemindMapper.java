package com.gov.operate.mapper;

import com.gov.operate.dto.electron.PurchaseRemindDto;
import com.gov.operate.entity.PurchaseRemind;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-07-08
 */
public interface PurchaseRemindMapper extends BaseMapper<PurchaseRemind> {

    List<PurchaseRemindDto> selectPurchaseRemindList();

}
