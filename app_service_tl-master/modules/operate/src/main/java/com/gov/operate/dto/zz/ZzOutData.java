package com.gov.operate.dto.zz;

import lombok.Data;

@Data
public class ZzOutData {
   private String id;
   private String client;
   private String zzOrgid;
   private String zzOrgname;//所属机构
   private String zzId;
   private String zzName;//证照名称
   private String zzUserId;
   private String zzUserName; //证件持有人
   private String zzNo;//证照编码
   private String zzSdate;
   private String zzEdate;//证照截止日期
   private String zzModiName;
   private String expireDay; //证照效期

}
