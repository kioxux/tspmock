package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.EffectiveSale;
import com.gov.operate.dto.HomeStockVo;
import com.gov.operate.dto.MemberCard;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyVo;
import com.gov.operate.entity.SaleTaskPlan;
import com.gov.operate.entity.SdSaleD;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 门店销售明细表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-13
 */
public interface SdSaleDMapper extends BaseMapper<SdSaleD> {

    EffectiveSale selectSalePlan(Map map);

    List<MemberCard> selectCardByUser(Map map);

//    HomePushMoneyVo getMonthPushMoneyList(HomeChartRequestDTO homeChartRequestDTO);

    HomePushMoneyVo selectMonthSalesBySales(HomePushMoneyDTO homePushMoneyDTO);

//    MonthPushMoneyBySalespersonOutData selectMonthSalesBySales(HomePushMoneyDTO homePushMoneyDTO);

    HomePushMoneyVo selectMonthSalesByPro(HomePushMoneyDTO homePushMoneyDTO);

    List<HomePushMoneyVo> selectMonthSalesChartBySales(HomePushMoneyDTO homePushMoneyDTO);

    List<HomePushMoneyVo> selectMonthSalesChartByPros(HomePushMoneyDTO homePushMoneyDTO);

    //获取门店180天效期商品种类
    int selectExpiryStoCount(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList);

    //获取公司180天效期商品种类
    int selectExpiryProCount(@Param("clientId") String clientId);

    //获取公司缺货数量
    int selectOutStockCountByClient(@Param("clientId") String clientId, @Param("directStoreList") List<String> directStoreList);

    int selectDcOutStockCountByClient(@Param("clientId") String clientId);

    List<Map<String,String>> findStoChainByClient(@Param("clientId") String clientId);

    //获取门店缺货数量
    int selectOutStockCountBySto(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList);

    HomeStockVo selectStoreStock(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList);

    HomeStockVo selectClientStock(@Param("clientId") String clientId);

    HomeStockVo selectStoreStockV2(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList);

    HomeStockVo selectClientStockV2(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList, @Param("userId") String userId);

    int selectExpiryStoCountV2(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList);

    int selectOutStockCountByStoV2(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList);

    int selectExpiryProCountV2(@Param("clientId") String clientId, @Param("stoCodeList") List<String> stoCodeList, @Param("userId") String userId);

    int selectOutStockCountByClientV2(@Param("clientId") String client, @Param("stoCodeList") List<String> stoCodeList);

    String selectAuthByClientAndStoCode(@Param("clientId")String clientId, @Param("stoCode")String stoCode, @Param("paraId")String paraId);

    BigDecimal getSaleAmtByDateRangeExt(@Param("client") String client, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("stoCodeList") List<String> stoCodeList);

    SaleTaskPlan selectSaleTaskPlan(@Param("client") String client, @Param("dateTime") String dateTime,@Param("stoCodeList") List<String> stoCodeList);
}
