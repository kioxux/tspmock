package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.GetFeedBackListVO;
import com.gov.operate.service.IFeedbackService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
@RestController
@RequestMapping("feedback")
public class FeedbackController {

    @Resource
    private IFeedbackService feedbackService;

    @Log("建议反馈列表")
    @ApiOperation(value = "建议反馈列表")
    @PostMapping("getFeedBackList")
    public Result getFeedBackList(@Valid @RequestBody GetFeedBackListVO vo) {
        return ResultUtil.success(feedbackService.getFeedBackList(vo));
    }

    @Log("建议反馈详情")
    @ApiOperation(value = "建议反馈详情")
    @GetMapping("getFeedBack")
    public Result getFeedBack(@RequestParam("id") String id) {
        return ResultUtil.success(feedbackService.getFeedBack(id));
    }

    @Log("建议反馈删除")
    @ApiOperation(value = "建议反馈删除")
    @GetMapping("removeFeedBack")
    public Result removeFeedBack(@RequestParam("id") String id) {
        feedbackService.removeFeedBack(id);
        return ResultUtil.success();
    }


}

