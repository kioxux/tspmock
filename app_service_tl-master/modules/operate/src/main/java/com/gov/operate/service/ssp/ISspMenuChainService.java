package com.gov.operate.service.ssp;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.ssp.SspMenuChainDTO;
import com.gov.operate.entity.SspMenuChain;

import java.util.List;
import java.util.Set;

/**
 *
 */
public interface ISspMenuChainService extends SuperService<SspMenuChain> {

    /**
     *
     * @param userId
     * @return
     */
    Result getMenuChainList(Long userId);

    Result bindMenuChainList(List<SspMenuChainDTO> menuChainDTOList, Long userId);

    int deleteChainHead(Long userId, String client, Set<String> keySet);
}
