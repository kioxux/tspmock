package com.gov.operate.mapper;

import com.gov.operate.entity.SdMarketingProm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
public interface SdMarketingPromMapper extends BaseMapper<SdMarketingProm> {

}
