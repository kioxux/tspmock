package com.gov.operate.service.batchPurchaseSales.impl;

import com.gov.operate.entity.BatchStoreStock;
import com.gov.operate.mapper.BatchStoreStockMapper;
import com.gov.operate.service.batchPurchaseSales.IBatchStoreStockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店库存 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Service
public class BatchStoreStockServiceImpl extends ServiceImpl<BatchStoreStockMapper, BatchStoreStock> implements IBatchStoreStockService {

}
