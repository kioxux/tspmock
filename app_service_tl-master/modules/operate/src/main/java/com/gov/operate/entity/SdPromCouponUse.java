package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_COUPON_USE")
@ApiModel(value="SdPromCouponUse对象", description="")
public class SdPromCouponUse extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPCU_VOUCHER_ID")
    private String gspcuVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPCU_SERIAL")
    private String gspcuSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPCU_PRO_ID")
    private String gspcuProId;

    @ApiModelProperty(value = "电子券活动号")
    @TableField("GSPCU_ACT_NO")
    private String gspcuActNo;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPCU_REACH_QTY1")
    private String gspcuReachQty1;

    @ApiModelProperty(value = "达到金额1")
    @TableField("GSPCU_REACH_AMT1")
    private BigDecimal gspcuReachAmt1;

    @ApiModelProperty(value = "用券数量1")
    @TableField("GSPCU_RESULT_QTY1")
    private String gspcuResultQty1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPCU_REACH_QTY2")
    private String gspcuReachQty2;

    @ApiModelProperty(value = "达到金额2")
    @TableField("GSPCU_REACH_AMT2")
    private BigDecimal gspcuReachAmt2;

    @ApiModelProperty(value = "用券数量2")
    @TableField("GSPCU_RESULT_QTY2")
    private String gspcuResultQty2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPCU_REACH_QTY3")
    private String gspcuReachQty3;

    @ApiModelProperty(value = "达到金额3")
    @TableField("GSPCU_REACH_AMT3")
    private BigDecimal gspcuReachAmt3;

    @ApiModelProperty(value = "用券数量3")
    @TableField("GSPCU_RESULT_QTY3")
    private String gspcuResultQty3;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPCU_MEM_FLAG")
    private String gspcuMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPCU_INTE_FLAG")
    private String gspcuInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPCU_INTE_RATE")
    private String gspcuInteRate;


}
