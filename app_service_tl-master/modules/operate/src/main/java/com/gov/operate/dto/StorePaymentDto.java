package com.gov.operate.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.28
 */
@Data
public class StorePaymentDto {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 组织
     */
    private String ficoCompanyCode;
    /**
     * 组织名
     */
    private String ficoCompanyName;
    /**
     * 状态 0-正常，1-停用
     */
    private String status;
    /**
     * 今天
     */
    private String toDay;
    /**
     * 上期付款截止日期
     */
    private String ficoEndingTime;
    /**
     * 有效剩余天
     */
    private String difference;
    /**
     * 最后一次付款方式 0 月 1 季 2 半年 3 年
     */
    private String ficoPaymentMethod;
    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;
    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 门店个数
     */
    private Integer cnt;

    /**
     * 单价
     */
    private Integer ficoCurrentChargingStandard;
}

