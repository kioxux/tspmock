package com.gov.operate.dto;

import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SdMarketingBasic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetGsmSettingDetailDTO2 extends SdMarketingBasic {

    /**
     * 加盟商详情
     */
    List<Franchisee> clientList;
}
