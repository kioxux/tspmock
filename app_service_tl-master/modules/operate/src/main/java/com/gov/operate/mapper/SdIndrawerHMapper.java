package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SdIndrawerHVO;
import com.gov.operate.entity.SdIndrawerH;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface SdIndrawerHMapper extends BaseMapper<SdIndrawerH> {

    /**
     * 门店装斗记录查询
     *
     * @param page
     * @param client
     * @param deptId
     * @param searchContent
     * @param query
     * @return
     */
    IPage<SdIndrawerHVO> getPage(Page<SdIndrawerHVO> page,
                                 @Param("client") String client,
                                 @Param("deptId") String deptId,
                                 @Param("searchContent") String searchContent,
                                 @Param("ew") QueryWrapper<SdIndrawerH> query);
}
