package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

@Data
public class MemberReportRequestDto {

    /**
     * 分类
     * 1. 门店
     * 2.门店分类
     */
    private Integer type;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String gsmbcBrId;

    /**
     * 门店编码
     */
    private List<String> stoCodeList;

    /**
     * 门店分类
     */
    private String gssgId;

    /**
     * 标签集合
     */
    private List<String> tagList;

    /**
     * 开始月份
     */
    private String startMonth;

    /**
     * 结束月份
     */
    private String endMonth;
}
