package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ssp.SspUserPageDTO;
import com.gov.operate.dto.ssp.SspUserPageVO;
import com.gov.operate.entity.SspUser;
import org.apache.ibatis.annotations.Param;


/**
 * @Entity generator.domain.SspUser
 */
public interface SspUserMapper extends BaseMapper<SspUser> {

    IPage<SspUserPageVO> selectUserByPage(Page<SspUserPageVO> page, @Param("ew") SspUserPageDTO sspUserPageDTO);

    /**
     * 查询手机号码是否重复
     * @param mobile 手机号码
     * @param userId 用户id
     * @return
     */
    int selectUserMobileExist(@Param("client") String client, @Param("mobile") String mobile,@Param("userId") Long userId);
}




