package com.gov.operate.dto;

import com.gov.operate.entity.FiInvoiceRegistration;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class RegPaymentSurplus extends FiInvoiceRegistration {
    /**
     * 登记金额 - 已付款金额
     */
    private BigDecimal surplus;
}
