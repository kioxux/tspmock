package com.gov.operate.service;

import com.gov.operate.entity.SdMarketingSto;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface ISdMarketingStoService extends SuperService<SdMarketingSto> {

}
