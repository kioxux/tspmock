package com.gov.operate.service.impl;

import com.gov.operate.entity.WmsRkys;
import com.gov.operate.mapper.WmsRkysMapper;
import com.gov.operate.service.IWmsRkysService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class WmsRkysServiceImpl extends ServiceImpl<WmsRkysMapper, WmsRkys> implements IWmsRkysService {

}
