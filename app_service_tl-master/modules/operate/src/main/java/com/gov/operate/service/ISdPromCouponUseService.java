package com.gov.operate.service;

import com.gov.operate.entity.SdPromCouponUse;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface ISdPromCouponUseService extends SuperService<SdPromCouponUse> {

}
