package com.gov.operate.service.batchPurchaseSales.impl;

import com.gov.operate.entity.BatchHeadPurchase;
import com.gov.operate.mapper.BatchHeadPurchaseMapper;
import com.gov.operate.service.batchPurchaseSales.IBatchHeadPurchaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 总部采购 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Service
public class BatchHeadPurchaseServiceImpl extends ServiceImpl<BatchHeadPurchaseMapper, BatchHeadPurchase> implements IBatchHeadPurchaseService {

}
