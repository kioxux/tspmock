package com.gov.operate.service;

import com.gov.operate.entity.SdExamineD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface ISdExamineDService extends SuperService<SdExamineD> {

}
