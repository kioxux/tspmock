package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 门店采购
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_STORE_PURCHASE")
@ApiModel(value="BatchStorePurchase对象", description="门店采购")
public class BatchStorePurchase extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableField("ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBSP_QUERY_FACTORY")
    private String gbspQueryFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBSP_DC")
    private String gbspDc;

    @ApiModelProperty(value = "日期")
    @TableField("GBSP_DATE")
    private String gbspDate;

    @ApiModelProperty(value = "销售方代码")
    @TableField("GBSP_SALES_CODE")
    private String gbspSalesCode;

    @ApiModelProperty(value = "销售方名称")
    @TableField("GBSP_SALES_NAME")
    private String gbspSalesName;

    @ApiModelProperty(value = "采购方代码")
    @TableField("GBSP_PURCHASE_CODE")
    private String gbspPurchaseCode;

    @ApiModelProperty(value = "采购方名称")
    @TableField("GBSP_PURCHASE_NAME")
    private String gbspPurchaseName;

    @ApiModelProperty(value = "产品代码")
    @TableField("GBSP_PRO_CODE")
    private String gbspProCode;

    @ApiModelProperty(value = "产品名称")
    @TableField("GBSP_PRO_NAME")
    private String gbspProName;

    @ApiModelProperty(value = "产品规格")
    @TableField("GBSP_PRO_SPECS")
    private String gbspProSpecs;

    @ApiModelProperty(value = "批号")
    @TableField("GBSP_BATCH_NO")
    private String gbspBatchNo;

    @ApiModelProperty(value = "数量")
    @TableField("GBSP_QTY")
    private BigDecimal gbspQty;

    @ApiModelProperty(value = "单位")
    @TableField("GBSP_UNIT")
    private String gbspUnit;

    @ApiModelProperty(value = "单价")
    @TableField("GBSP_PRICE")
    private BigDecimal gbspPrice;

    @ApiModelProperty(value = "金额")
    @TableField("GBSP_AMT")
    private BigDecimal gbspAmt;

    @ApiModelProperty(value = "生产厂家")
    @TableField("GBSP_FACTORY")
    private String gbspFactory;

    @ApiModelProperty(value = "有效期")
    @TableField("GBSP_VALID")
    private String gbspValid;

    @ApiModelProperty(value = "采购类型")
    @TableField("GBSP_PRUCHASE_TYPE")
    private String gbspPruchaseType;

    @ApiModelProperty(value = "批准文号")
    @TableField("GBSP_APPROVAL_NUMBER")
    private String gbspApprovalNumber;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBSP_CREATE_DATE")
    private String gbspCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBSP_CREATE_TIME")
    private String gbspCreateTime;


}
