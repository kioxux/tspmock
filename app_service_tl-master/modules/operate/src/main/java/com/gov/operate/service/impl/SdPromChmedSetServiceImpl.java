package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromChmedSet;
import com.gov.operate.mapper.SdPromChmedSetMapper;
import com.gov.operate.service.ISdPromChmedSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 中药类促销设置表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-07-27
 */
@Service
public class SdPromChmedSetServiceImpl extends ServiceImpl<SdPromChmedSetMapper, SdPromChmedSet> implements ISdPromChmedSetService {

}
