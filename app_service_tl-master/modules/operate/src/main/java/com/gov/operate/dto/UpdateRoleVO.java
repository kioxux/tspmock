package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateRoleVO {

    @NotNull(message = "角色id不能为空")
    private Integer id;

    private String garRoleName;

    @NotBlank(message = "是否启用不能为空")
    private String garEnable;

    private String  garBz;

}
