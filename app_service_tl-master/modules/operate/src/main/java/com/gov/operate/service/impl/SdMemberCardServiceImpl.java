package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMemberCard;
import com.gov.operate.mapper.SdMemberCardMapper;
import com.gov.operate.service.ISdMemberCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-17
 */
@Service
public class SdMemberCardServiceImpl extends ServiceImpl<SdMemberCardMapper, SdMemberCard> implements ISdMemberCardService {

}
