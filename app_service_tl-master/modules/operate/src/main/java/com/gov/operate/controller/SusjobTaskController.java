package com.gov.operate.controller;

import com.gov.common.entity.Pageable;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.CreateSusjobVO;
import com.gov.operate.dto.SubmitSusjobDetail;
import com.gov.operate.dto.SusjobDetailsQuery;
import com.gov.operate.dto.GetSusjobListVO;
import com.gov.operate.dto.SusjobTaskListQuery;
import com.gov.operate.service.ISdSusjobHService;
import com.gov.operate.service.ISusjobTaskService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */

@RestController
@RequestMapping("susjobTask")
public class SusjobTaskController {

    @Resource
    private ISdSusjobHService sdSusjobHService;
    @Resource
    private ISusjobTaskService iSusjobTaskService;

    @Log("会员维系任务")
    @ApiOperation(value = "会员维系任务")
    @PostMapping("querySusjobTaskList")
    public Result querySusjobTaskList(@RequestBody @Valid SusjobTaskListQuery susjobTaskListQuery) {
        return iSusjobTaskService.querySusjobTaskList(susjobTaskListQuery);
    }

    @Log("会员维系任务")
    @ApiOperation(value = "会员维系任务")
    @PostMapping("querySusjobTaskListNew")
    public Result querySusjobTaskListNew(@RequestBody @Valid SusjobTaskListQuery susjobTaskListQuery) {
        return iSusjobTaskService.querySusjobTaskListNew(susjobTaskListQuery);
    }

    @Log("我的维系任务")
    @ApiOperation(value = "我的维系任务")
    @PostMapping("queryMySusjobTaskList")
    public Result queryMySusjobTaskList(@RequestBody @Valid Pageable pageable) {
        return iSusjobTaskService.queryMySusjobTaskList(pageable);
    }

    @Log("会员维系任务明细")
    @ApiOperation(value = "会员维系任务明细")
    @PostMapping("querySusjobDetails")
    public Result querySusjobDetails(@RequestBody @Valid SusjobDetailsQuery susjobDetailQuery) {
        return iSusjobTaskService.querySusjobDetails(susjobDetailQuery);
    }

    @Log("会员维系任务提交")
    @ApiOperation(value = "会员维系任务提交")
    @PostMapping("submitSusjobDetail")
    public Result submitSusjobDetail(@RequestBody @Valid SubmitSusjobDetail submitSusjobDetail) {
        return iSusjobTaskService.submitSusjobDetail(submitSusjobDetail);
    }

    @Log("生成会员维系任务")
    @ApiOperation(value = "生成会员维系任务")
    @PostMapping("createSusjob")
    public Result createSusjob(@RequestBody @Valid CreateSusjobVO vo) {
        sdSusjobHService.createSusjob(vo);
        return ResultUtil.success();
    }

    @Log("维系任务列表")
    @ApiOperation(value = "维系任务列表")
    @PostMapping("getSusjobList")
    public Result getSusjobList(@RequestBody GetSusjobListVO vo) {
        return ResultUtil.success(sdSusjobHService.getSusjobList(vo));
    }

    @Log("标签列表")
    @ApiOperation(value = "标签列表")
    @GetMapping("getTagList")
    public Result getTagList() {
        return iSusjobTaskService.getTagList();
    }


    @Log("更新推送状态")
    @PostMapping("updatePushStatus")
    public Result updatePushStatus() {
        sdSusjobHService.updatePushStatus();
        return ResultUtil.success();
    }

    @Log("更新任务状态")
    @PostMapping("updateTaskStatus")
    public Result updateTaskStatus() {
        sdSusjobHService.updateTaskStatus();
        return ResultUtil.success();
    }


}
