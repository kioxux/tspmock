package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户收费维护明细门店表
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_CHARGE_MAINTENANCE_M_STO")
@ApiModel(value="UserChargeMaintenanceMSto对象", description="用户收费维护明细门店表")
public class UserChargeMaintenanceMSto extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "明细表主键")
    @TableField("SPM_ID")
    private Integer spmId;

    @ApiModelProperty(value = "门店编号")
    @TableField("SP_SITE")
    private String spSite;


}
