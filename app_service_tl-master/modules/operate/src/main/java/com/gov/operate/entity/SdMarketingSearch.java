package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING_SEARCH")
@ApiModel(value="SdMarketingSearch对象", description="")
public class SdMarketingSearch extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询ID")
    @TableId("GSMS_ID")
    private String gsmsId;

    @ApiModelProperty(value = "查询名称")
    @TableField("GSMS_NAME")
    private String gsmsName;

    @ApiModelProperty(value = "查询明细")
    @TableField("GSMS_DETAIL")
    private String gsmsDetail;

    @ApiModelProperty(value = "营销活动加盟商分配表主键")
    @TableField("MARKETING_ID")
    private Integer marketingId;

    @ApiModelProperty(value = "门店编码")
    @TableField("GSMS_STORE")
    private String gsmsStore;

    @ApiModelProperty(value = "是否常用活动 0，常用查询，1,非常用查询")
    @TableField("GSMS_NORMAL")
    private String gsmsNormal;

    @ApiModelProperty(value = "删除标记 0-否，1-是")
    @TableField("GSMS_DELETE_FLAG")
    private String gsmsDeleteFlag;

    @ApiModelProperty(value = "1:短信，2：电话")
    @TableField("GSMS_TYPE")
    private Integer gsmsType;

    @ApiModelProperty(value = "短信模板")
    @TableField("GSMS_SMS_TEMP")
    private String gsmsSmsTemp;

    @ApiModelProperty(value = "发送时间")
    @TableField("GSMS_SMS_SEND_TIME")
    private String gsmsSmsSendTime;

    @ApiModelProperty(value = "短信签名")
    @TableField("GSMS_SMS_SGIN")
    private Integer gsmsSmsSgin;

    @ApiModelProperty(value = "发送人数")
    @TableField("GSMS_SEND_NUM")
    private Integer gsmsSendNum;


}
