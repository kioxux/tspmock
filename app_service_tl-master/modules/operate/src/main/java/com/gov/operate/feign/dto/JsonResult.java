package com.gov.operate.feign.dto;

import java.io.Serializable;

public class JsonResult implements Serializable {
   private static final long serialVersionUID = 1L;
   private static final String ERROR_MESSAGE = "服务器出现了一些小状况，正在修复中！";
   private Integer code;
   private Object data;
   private String message;

   public JsonResult(Integer code, Object data, String message) {
      this.data = data;
      this.message = message;
      this.code = code;
   }

   public static JsonResult success(Object data, String desc) {
      return new JsonResult(0, data, desc);
   }

   public static JsonResult fail(Integer code, String desc) {
      return new JsonResult(code, (Object)null, desc);
   }

   public static JsonResult process(Integer code, Object data, String desc) {
      return new JsonResult(code, data, desc);
   }

   public static JsonResult error() {
      return new JsonResult(500, (Object)null, "服务器出现了一些小状况，正在修复中！");
   }

   public JsonResult() {
   }

   public Integer getCode() {
      return this.code;
   }

   public void setCode(Integer code) {
      this.code = code;
   }

   public Object getData() {
      return this.data;
   }

   public void setData(Object data) {
      this.data = data;
   }

   public String getMessage() {
      return this.message;
   }

   public void setMessage(String message) {
      this.message = message;
   }
}
