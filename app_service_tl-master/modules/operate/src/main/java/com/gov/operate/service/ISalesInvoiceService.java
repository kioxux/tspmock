package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.invoice.*;
import com.gov.operate.entity.UserData;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * 销售开票
 *
 * @author 123
 */
public interface ISalesInvoiceService {

    /**
     * 销售开票查询
     */
    IPage<SalesInvoicePageVO> selectSalesInvoiceByPage(SalesInvoicePageDTO vo);

    /**
     * 开票并导出excel上传cos
     */
    String saveBill(List<SalesInvoicePageVO> list) throws IOException;

    /**
     * 销售开票登记查询
     */
    IPage<SalesInvoiceRegisterPageVO> selectSalesInvoiceRegisterByPage(SalesInvoiceRegisterPageDTO dto);

    /**
     * 登记明细
     *
     * @param dto
     * @return
     */
    SalesInvoiceDetailVO getRegistDetail(SalesInvoiceDetailDTO dto);

    /**
     * 保存明细
     *
     * @return
     */
    Result submitInvoiceDetail(String gsiOrderId, String gsiBill);

    /**
     * 金额明细
     *
     * @param dto
     * @return
     */
    Result submitAmountDetail(SalesInvoiceDetailVO dto);

    Result selectSalesInvoiceRegisterTotalByPage(SalesInvoiceRegisterPageDTO dto);

    /**
     * 开票人员
     *
     * @return
     */
    List<UserData> getInvoiceUserList(String type);

    /**
     * 当前开票人员
     *
     * @return
     */
    Result getInvoiceUser();

    /**
     * 开票回调
     *
     * @param httpServletRequest
     */
    void callback(HttpServletRequest httpServletRequest) throws Exception;

    /**
     * 自动开票NEW
     *
     * @param autoInvoice
     * @return
     */
    Result invoiceAuto(AutoInvoice autoInvoice) throws Exception;

    /**
     * 开票结果任务
     */
    void invoiceReturnNew() throws Exception;

    /**
     * 删除开标信息
     * @param gsiOrderId
     */
    void deleteInvoice(String gsiOrderId);

    /**
     * 重开票
     * @param gsiOrderId
     * @return
     */
    Result reInvoice(String gsiOrderId) throws Exception;
}
