package com.gov.operate.service;

import com.gov.operate.entity.InvoiceBillInvoice;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
public interface IInvoiceBillInvoiceService extends SuperService<InvoiceBillInvoice> {

}
