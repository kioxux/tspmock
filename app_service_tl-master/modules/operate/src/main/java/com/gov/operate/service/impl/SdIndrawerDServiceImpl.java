package com.gov.operate.service.impl;

import com.gov.operate.entity.SdIndrawerD;
import com.gov.operate.mapper.SdIndrawerDMapper;
import com.gov.operate.service.ISdIndrawerDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Service
public class SdIndrawerDServiceImpl extends ServiceImpl<SdIndrawerDMapper, SdIndrawerD> implements ISdIndrawerDService {

}
