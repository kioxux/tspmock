package com.gov.operate.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MarketingApproveDto {

    @ApiModelProperty(value = "销售额标值")
    private BigDecimal gsmIndexValue;

    @ApiModelProperty(value = "销售额活动值")
    private BigDecimal gsmTargetValue;

    @ApiModelProperty(value = "客单价")
    private BigDecimal gsmPriceinc;

    @ApiModelProperty(value = "客单价活动值")
    private BigDecimal gsmActPriceinc;

    @ApiModelProperty(value = "交易次数")
    private BigDecimal gsmStinc;

    @ApiModelProperty(value = "交易次数活动值")
    private BigDecimal gsmActStinc;

}
