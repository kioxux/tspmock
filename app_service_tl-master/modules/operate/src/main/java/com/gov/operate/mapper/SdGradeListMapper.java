package com.gov.operate.mapper;

import com.gov.operate.entity.SdGradeList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
public interface SdGradeListMapper extends BaseMapper<SdGradeList> {

}
