package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_WMS_TUIGONG_M")
@ApiModel(value="WmsTuigongM对象", description="")
public class WmsTuigongM extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "退供应商单号")
    @TableField("WM_TGYSDH")
    private String wmTgysdh;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private Long id;

    @ApiModelProperty(value = "商品编码")
    @TableField("WM_SP_BM")
    private String wmSpBm;

    @ApiModelProperty(value = "批次号")
    @TableField("WM_PCH")
    private String wmPch;

    @ApiModelProperty(value = "货位号")
    @TableField("WM_HWH")
    private String wmHwh;

    @ApiModelProperty(value = "库存状态编号")
    @TableField("WM_KCZT_BH")
    private String wmKcztBh;

    @ApiModelProperty(value = "出库价")
    @TableField("WM_CKJ")
    private BigDecimal wmCkj;

    @ApiModelProperty(value = "出库原数量")
    @TableField("WM_CKYSL")
    private BigDecimal wmCkysl;

    @ApiModelProperty(value = "过帐数量")
    @TableField("WM_GZSL")
    private BigDecimal wmGzsl;

    @ApiModelProperty(value = "修改日期")
    @TableField("WM_XGRQ")
    private String wmXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("WM_XGSJ")
    private String wmXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("WM_XGR")
    private String wmXgr;

    @ApiModelProperty(value = "修改人姓名")
    @TableField("WM_XGRXM")
    private String wmXgrxm;


}
