package com.gov.operate.service;

import com.gov.operate.entity.ExportTask;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 导出报表 服务类
 * </p>
 *
 * @author sy
 * @since 2022-01-20
 */
public interface IExportTaskService extends SuperService<ExportTask> {

    ExportTask initExportTask(Integer getStatus, Integer getType);
}
