package com.gov.operate.dto;

import com.gov.operate.entity.AuthorityAuths;
import lombok.Data;

import java.util.List;

@Data
public class AuthVO extends AuthorityAuths {

    private Integer check;

    /**
     * 子级
     */
    private List<AuthVO> children;


}
