package com.gov.operate.controller.Role;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.RoleVO;
import com.gov.operate.dto.UpdateRoleVO;
import com.gov.operate.dto.UserRoleDto;
import com.gov.operate.dto.UserRoleVO;
import com.gov.operate.dto.auth.ClientAuth;
import com.gov.operate.entity.AuthorityAuthsClient;
import com.gov.operate.entity.AuthorityRolesAuths;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 权限和角色相关的操作
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@RestController
@RequestMapping("/role/role")
public class AuthRoleController {

    @Resource
    private IAuthorityRolesService rolesService;
    @Resource
    private IAuthorityUserRolesService userRolesService;
    @Resource
    private IAuthorityAuthsService authsService;
    @Resource
    private IAuthorityRolesAuthsService rolesAuthsService;
    @Resource
    private IUserRoleService userRoleService;
    @Resource
    private IAuthorityAuthsService iAuthorityAuthsService;

    @Log("角色创建")
    @ApiOperation(value = "角色创建")
    @PostMapping("addRole")
    public Result createRole(@Valid @RequestBody RoleVO vo) {
        return rolesService.createRole(vo);
    }

    @Log("角色删除")
    @ApiOperation(value = "角色删除")
    @GetMapping("deleteRole")
    public Result deleteRole(Integer id) {
        return rolesService.deleteRole(id);
    }


    @Log("角色编辑")
    @ApiOperation(value = "角色编辑")
    @PostMapping("updateRole")
    public Result updateRole(@Valid @RequestBody UpdateRoleVO vo) {
        return rolesService.updateRole(vo);
    }

    @Log("角色查询")
    @ApiOperation(value = "角色查询")
    @GetMapping("roleList")
    public Result roleList() {
        return rolesService.roleList();
    }

    @Log("人员获取")
    @ApiOperation(value = "人员获取")
    @GetMapping("userList")
    public Result getUserList(Integer gaurRoleId) {
        return userRolesService.getUserList(gaurRoleId);
    }


    @Log("创建人员与角色的关联")
    @ApiOperation(value = "创建人员与角色的关联")
    @PostMapping("addUserRole")
    public Result addUserRole(@Valid @RequestBody UserRoleVO vo) {
        return userRolesService.addUserRole(vo);
    }

    @Log("获取人员与角色的关联")
    @ApiOperation(value = "获取人员与角色的关联")
    @GetMapping("userRoleList")
    public Result userRoleList(Integer gaurRoleId) {
        return userRolesService.userRoleList(gaurRoleId);
    }


    @Log("删除人员和角色的关联")
    @ApiOperation(value = "删除人员和角色的关联")
    @PostMapping("deleteUserRole")
    public Result deleteUserRole(@Valid @RequestBody List<Integer> idList) {
        return userRolesService.deleteUserRole(idList);
    }

    @Log("权限列表")
    @ApiOperation(value = "权限列表")
    @GetMapping("authList")
    public Result getAuthList(Integer gaurRoleId) {
        return authsService.getAuthList(gaurRoleId);
    }

    @Log("角色权限关联")
    @ApiOperation(value = "角色权限关联")
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdateAuth(@RequestBody List<AuthorityRolesAuths> rolesAuthsList) {
        return rolesAuthsService.saveOrUpdateAuth(rolesAuthsList);
    }


    @Log("收费权限")
    @ApiOperation(value = "收费权限")
    @PostMapping("feeAuth")
    public Result getFeeAuth() {
        return authsService.getFeeAuth();
    }

    @Log("新增用户角色")
    @PostMapping("createUserRole")
    public Result createUserRole(@RequestBody UserRoleDto userRoleDto) {
        return userRoleService.createUserRole(userRoleDto.getClient(), userRoleDto.getFuncCode(), userRoleDto.getUserId());
    }

    @Log("更新用户角色")
    @PostMapping("updateUserRole")
    public Result updateUserRole(@RequestBody UserRoleDto userRoleDto) {
        return userRoleService.createUserRole(userRoleDto.getClient(), userRoleDto.getFuncCode(), userRoleDto.getUserId(), userRoleDto.getOldUserId());
    }

    @Log("加盟商权限获取")
    @ApiOperation("加盟商权限获取")
    @PostMapping("getAuthByClient")
    public Result getAuthByClient(@RequestJson("client") String client) {
        return iAuthorityAuthsService.getAuthByClient(client);
    }

    @Log("保存加盟商权限")
    @ApiOperation("保存加盟商权限")
    @PostMapping("saveClientAuth")
    public Result saveClientAuth(@RequestBody ClientAuth clientAuth) {
        iAuthorityAuthsService.saveClientAuth(clientAuth);
        return ResultUtil.success();
    }
}

