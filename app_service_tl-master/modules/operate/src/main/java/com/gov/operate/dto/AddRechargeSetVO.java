package com.gov.operate.dto;

import com.gov.operate.entity.SdRechargeCardSet;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddRechargeSetVO extends SdRechargeCardSet {

    private String gsrcsCardPrefix;

    @NotBlank(message = "初始密码不能为空")
    private String gsrcsPassword;

    //充值比例
    private String proportion;

    //充值金额
    private BigDecimal minAmt;

    //类型1,2,3
    private Integer type;

    /**
     * 门店集合
     */
    private List<String> stoCodeList;

}
