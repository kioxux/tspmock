package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/15 15:04
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetHighlightDTO {
    /**
     * 商品编码
     */
    private String gsedProId;

    /**
     * 批号
     */
    private String gsedBatchNo;

    /**
     * 有效期
     */
    private String gsedValidDate;



}
