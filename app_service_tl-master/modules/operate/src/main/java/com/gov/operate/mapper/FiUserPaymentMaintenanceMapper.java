package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.UserPaymentMaintenance;
import com.gov.operate.entity.FiUserPaymentMaintenance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-01
 */
public interface FiUserPaymentMaintenanceMapper extends BaseMapper<FiUserPaymentMaintenance> {

    /**
     * 付款基础数据初始化
     */
    void initPaymentMaintenance();

    /**
     * 基础数据查询
     *
     * @param page
     * @param client
     * @param ficoPayingstoreCode
     * @param ficoPayingstoreName
     * @return
     */
    IPage<UserPaymentMaintenance> selectPaymentMaintenance(Page<UserPaymentMaintenance> page,
                                                           @Param("client") String client,
                                                           @Param("ficoPayingstoreCode") String ficoPayingstoreCode,
                                                           @Param("ficoPayingstoreName") String ficoPayingstoreName);

}
