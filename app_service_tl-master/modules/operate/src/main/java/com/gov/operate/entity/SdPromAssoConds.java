package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_ASSO_CONDS")
@ApiModel(value="SdPromAssoConds对象", description="")
public class SdPromAssoConds extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPAC_VOUCHER_ID")
    private String gspacVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPAC_SERIAL")
    private String gspacSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPAC_PRO_ID")
    private String gspacProId;

    @ApiModelProperty(value = "数量")
    @TableField("GSPAC_QTY")
    private String gspacQty;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPAC_SERIES_ID")
    private String gspacSeriesId;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPAC_MEM_FLAG")
    private String gspacMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPAC_INTE_FLAG")
    private String gspacInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPAC_INTE_RATE")
    private String gspacInteRate;


}
