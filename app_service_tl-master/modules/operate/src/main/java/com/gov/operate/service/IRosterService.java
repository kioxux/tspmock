package com.gov.operate.service;

import com.gov.operate.entity.Roster;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
public interface IRosterService extends SuperService<Roster> {

}
