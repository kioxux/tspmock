package com.gov.operate.dto.recharge;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class RechargeChangeDataExcel extends BaseRowModel {
    /**
     * 序号
     */
    @ExcelProperty(value = "序号")
    private Integer index;

    /**
     * 储值卡卡号
     */
    @ExcelProperty(value = "储值卡卡号")
    private String gsrcId;

    /**
     * 会员卡号
     */
    @ExcelProperty(value = "会员卡号")
    private String gsrcMemberCardId;

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名")
    private String gsrcName;

    /**
     * 性别
     */
    @ExcelProperty(value = "性别")
    private String gsrcSex;

    /**
     * 手机
     */
    @ExcelProperty(value = "手机")
    private String gsrcMobile;

    /**
     * 日期
     */
    @ExcelProperty(value = "日期")
    private String gsrcDate;

    /**
     * 时间
     */
    @ExcelProperty(value = "时间")
    private String gsrcTime;

    /**
     * 门店编码
     */
    @ExcelProperty(value = "门店编码")
    private String gsrcBrId;

    /**
     * 门店名称
     */
    @ExcelProperty(value = "门店名称")
    private String stoShortName;

    /**
     * 操作人员
     */
    @ExcelProperty(value = "操作人员")
    private String gsrcEmp;

    /**
     * 异动单号
     */
    @ExcelProperty(value = "单号")
    private String gsrcVoucherId;

    /**
     * 储值卡初始金额
     */
    @ExcelProperty(value = "储值卡初始金额")
    private BigDecimal gsrcInitialAmt;
    /**
     * 储值卡异动金额
     */
    @ExcelProperty(value = "储值卡异动金额")
    private BigDecimal gsrcChangeAmt;
    /**
     * 储值卡结果金额
     */
    @ExcelProperty(value = "储值卡结果金额")
    private BigDecimal gsrcResultAmt;

    /**
     * 异动类型 1：充值 2：退款 3：消费 4：退货
     */
    @ExcelProperty(value ="异动类型 1：充值 2：退款 3：消费 4：退货")
    private String gsrcType;
}
