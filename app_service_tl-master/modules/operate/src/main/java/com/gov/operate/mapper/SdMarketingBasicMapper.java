package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SdMarketingBasic;
import com.gov.operate.entity.SdMarketingPrd;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface SdMarketingBasicMapper extends BaseMapper<SdMarketingBasic> {

    /**
     * 活动ID最大值
     */
    String getMaxNumMarketid();

    /**
     * 促销方式列表
     */
    List<GetPromotionListDTO> getPromotionList();

    /**
     * 营销任务列表
     */
    IPage<GetGsmTaskListDTO2> getGsmTaskList(@Param("pageInfo") Page<GetGsmTaskListDTO2> pageInfo,
                                             @Param("client") String client,
                                             @Param("vo") GsmTaskListVO2 vo);

    /**
     * 加盟商列表
     */
    List<Franchisee> getClientList(@Param("id") Integer id);

    /**
     * 选品选择列表
     */
    IPage<GetProductListToCheckDTO> getProductListToCheck(Page<GetProductListToCheckDTO> page, @Param("vo") GetProductListToCheckVO vo);

    IPage<GetProductListToCheckDTO> getGiveawayListToCheck(Page<GetProductListToCheckDTO> page, @Param("vo") GetGiveawayListToCheckVO vo);


    /******************************************** 03版新添 **********************************************/

    /**
     * 同比日期营业额
     */
    BigDecimal getTurnover(@Param("client") String client, @Param("stoList") List<String> stoList, @Param("gsmStart") String gsmStart, @Param("gsmEnd") String gsmEnd);

    /**
     * 同比日期毛利额
     */
    BigDecimal getGrossprofit(@Param("client") String client, @Param("stoList") List<String> stoList, @Param("gsmStart") String gsmStart, @Param("gsmEnd") String gsmEnd);

    /**
     * 同比日期促销品销售额
     */
    BigDecimal getPromotionalpro(@Param("client") String client, @Param("stoList") List<String> stoList, @Param("gsmStart") String gsmStart, @Param("gsmEnd") String gsmEnd);

    /**
     * 营销设置列表
     */
    IPage<SdMarketingBasicDTO3> getGsmSettingList(Page<SdMarketingBasicDTO3> page, @Param("vo") GetGsmSettingListVO3 vo);

    /**
     * 查询商品列表
     */
    List<SelectProBatchFromBusinessDTO> selectProBatchFromProductBusiness(@Param("promIdList") List<Integer> promIdList);
    /**
     * GAIA_PRODUCT_BUSINESS 商品表数据 复制到 GAIA_SD_MARKETING_PRD 营销活动商品清单表
     */
    void insertProBatchFromProductBusiness(@Param("promIdList") List<Integer> promIdList);

    /**
     * 批量插入
     */
    void insertProBatchFromProductBusiness2(@Param("marketingPrdDOList") List<SdMarketingPrd> marketingPrdDOList);

    /**
     * 促销商品列表
     */
    IPage<GetPromDetailsDTO> getPromDetails(Page<GetPromDetailsDTO> page, @Param("vo") GetPromDetailsVO vo, @Param("client") String client);

    /**
     * 促销赠品列表
     * @param page
     * @param vo
     * @return
     */
    IPage<GetProductListToCheckDTO> getGiveawayList(Page<GetProductListToCheckDTO> page, @Param("vo") GetGiveawayListToCheckVO vo);

    /**
     * 当前促销下 已有商品的 促销价 + 成本价
     */
    List<PrdPriceAndCost> getMarketingPrdPriceAndCost(@Param("promId") Integer promId);
}
