package com.gov.operate.service;


import com.gov.operate.entity.GaiaAlPlQjmd;
import com.gov.operate.kylin.TestData;

import java.util.HashMap;
import java.util.List;

public interface Al_Pl_Analyse {
    List<TestData> getDistinctIds();
    List<GaiaAlPlQjmd> getSaleSummaryByArea(String area);
    List<GaiaAlPlQjmd> getSaleInfoByComp(HashMap<String,Object> inHashMap);
}
