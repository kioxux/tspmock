package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_WMS_TUIGONG_Z")
@ApiModel(value="WmsTuigongZ对象", description="")
public class WmsTuigongZ extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private Long id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "退供应商单号")
    @TableField("WM_TGYSDH")
    private String wmTgysdh;

    @ApiModelProperty(value = "退供应商订单号")
    @TableField("WM_TGYSDDH")
    private String wmTgysddh;

    @ApiModelProperty(value = "供应商编号")
    @TableField("WM_GYS_BH")
    private String wmGysBh;

    @ApiModelProperty(value = "供应商名称")
    @TableField("WM_GYS_MC")
    private String wmGysMc;

    @ApiModelProperty(value = "创建日期")
    @TableField("WM_CJRQ")
    private String wmCjrq;

    @ApiModelProperty(value = "创建时间")
    @TableField("WM_CJSJ")
    private String wmCjsj;

    @ApiModelProperty(value = "创建人")
    @TableField("WM_CJR")
    private String wmCjr;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("WM_CJRXM")
    private String wmCjrxm;

    @ApiModelProperty(value = "是否删除0是/1否（默认否）")
    @TableField("WM_SFSQ")
    private String wmSfsq;

    @ApiModelProperty(value = "修改日期")
    @TableField("WM_XGRQ")
    private String wmXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("WM_XGSJ")
    private String wmXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("WM_XGR")
    private String wmXgr;

    @ApiModelProperty(value = "修改人姓名")
    @TableField("WM_XGRXM")
    private String wmXgrxm;

    @ApiModelProperty(value = "是否过账")
    @TableField("WM_SFGZ")
    private String wmSfgz;

    @ApiModelProperty(value = "备注")
    @TableField("WM_BZ")
    private String wmBz;


}
