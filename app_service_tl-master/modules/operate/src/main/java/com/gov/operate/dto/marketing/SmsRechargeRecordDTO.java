package com.gov.operate.dto.marketing;

import com.gov.operate.entity.SmsRechargeRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SmsRechargeRecordDTO extends SmsRechargeRecord {

    private Integer gsrrRechargeRemainCount;
}
