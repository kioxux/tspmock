package com.gov.operate.bsaoc;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.09
 */
@Slf4j
@Component
public class BsaoCPay {

    @Resource
    private ApplicationConfig applicationConfig;

//    public static void main(String[] args) throws Exception {
//        String appId = "f0ec96ad2c3848b5b810e7aadf369e2f";
//        String appKey = "775481e2556e4564985f5439a5e6a277";
//        String merchantCode = "123456789900081";
//        String terminalCode = "00810001";
//        String paymentContent = "{ \"appInterface\": \"UnionPay\", \"appString\": { \"appId\": \"f0ec96ad2c3848b5b810e7aadf369e2f\", \"appKey\": \"775481e2556e4564985f5439a5e6a277\", \"merchantCode\": \"123456789900081\", \"terminalCode\": \"00810001\" }}";
//        payV2(paymentContent, 1, "储值卡充值", "287898541365698591", "", "");
//    }

    /**
     * * 进行pos通支付操作,整理参数
     * * 接口地址:
     * * 测试环境：http://58.247.0.18:29015/v2/poslink/transaction/pay
     * * 生产环境：https://api-mop.chinaums.com/v2/poslink/transaction/pay
     *
     * @param paymentContent 支付方式
     * @param amt            交易金额 （单位：分）
     * @param merchantRemark 商户备注
     * @param payCode        支付码
     * @param srcReserved    商户冗余信息
     * @param orderDesc      订单描述
     * @throws Exception
     */
    public Result payV2(String paymentContent, int amt, String merchantRemark, String payCode, String srcReserved, String orderDesc) {
        log.info("支付参数：{}", paymentContent);
        Result result = ResultUtil.success();
        try {
            // 支付方式为空
            if (StringUtils.isBlank(paymentContent)) {
                result = ResultUtil.error(ResultEnum.E0035);
                log.info("返回结果：{}", JSONObject.toJSONString(result));
                return result;
            }
            // 支付方式
            JSONObject payment = JSONObject.parseObject(paymentContent);
            String appInterface = payment.getString("appInterface");
            if (StringUtils.isBlank(appInterface)) {
                result = ResultUtil.error(ResultEnum.E0035);
                log.info("返回结果：{}", JSONObject.toJSONString(result));
                return result;
            }
            if ("UnionPay".equals(appInterface)) {
                // 终端信息
                JSONObject appString = payment.getJSONObject("appString");
                String appId = appString.getString("appId"),
                        appKey = appString.getString("appKey"),
                        merchantCode = appString.getString("merchantCode"),
                        terminalCode = appString.getString("terminalCode");
                /* post参数,格式:JSON */
                JSONObject body = new JSONObject();
                body.put("merchantCode", merchantCode);   // 商户号
                body.put("terminalCode", terminalCode);   // 终端号
                // 测试用 固定支付 1 分钱
                if (applicationConfig.getBsaocpaytest() != null && applicationConfig.getBsaocpaytest().intValue() == 1) {
                    body.put("transactionAmount", 1);  // 交易金额,遇到传goods的情况,得算出来
                } else {
                    body.put("transactionAmount", amt);  // 交易金额,遇到传goods的情况,得算出来
                }
                body.put("transactionCurrencyCode", "156"); // 交易币种
                body.put("merchantOrderId", Util.getMerchantOrderId());  // 商户订单号,自定义
                body.put("merchantRemark", merchantRemark); // 商户备注
                body.put("payMode", "CODE_SCAN");    // 支付方式:    E_CASH(电子现金) SOUNDWAVE(声波) NFC(NFC) CODE_SCAN(扫码) MANUAL(手输)
                body.put("payCode", payCode); // 这里需要真实的支付码eg:286656579659199426    287898541365698591
                body.put("srcReserved", srcReserved);  // 商户冗余信息
                body.put("orderDesc", orderDesc); // 订单描述
                String url = applicationConfig.getBsaocpayurl();
//                String url = "http://58.247.0.18:29015/v2/poslink/transaction/pay";
                log.info("请求参数:{}", body.toString());
                String send = send(appId, appKey, url, body.toString());
                log.info("返回结果:{}", send);
                JSONObject payResult = JSONObject.parseObject(send);
                // 结果
                String errCode = payResult.getString("errCode");
                if ("00".equals(errCode)) {
                    log.info("返回结果：{}", JSONObject.toJSONString(result));
                    return result;
                }
                result = ResultUtil.error(ResultEnum.UNKNOWN_ERROR.getCode(), payResult.getString("errInfo"));
                log.info("返回结果：{}", JSONObject.toJSONString(result));
                return result;
            } else {
                result = ResultUtil.error(ResultEnum.E0035);
                log.info("返回结果：{}", JSONObject.toJSONString(result));
                return result;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("支付失败");
            result = ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
            log.info("返回结果：{}", JSONObject.toJSONString(result));
            return result;
        }
    }

    /**
     * 发送请求
     *
     * @param url    eg:http://58.247.0.18:29015/v2/poslink/transaction/pay
     * @param entity eg:{"merchantCode":"123456789900081","terminalCode":"00810001","transactionAmount":1,"transactionCurrencyCode":"156","merchantOrderId":"201905211550526234902343","merchantRemark":"商户备注","payMode":"CODE_SCAN","payCode":"285059979526414634"}
     * @return
     * @throws Exception
     */
    public static String send(String appId, String appKey, String url, String entity) throws Exception {
        String authorization = getOpenBodySig(appId, appKey, entity);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(entity, "UTF-8");
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity1 = response.getEntity();
        String resStr = null;
        if (entity1 != null) {
            resStr = EntityUtils.toString(entity1, "UTF-8");
        }
        httpClient.close();
        response.close();
        return resStr;
    }

    /**
     * open-body-sig方式获取到Authorization 的值
     *
     * @param appId  f0ec96ad2c3848b5b810e7aadf369e2f
     * @param appKey 775481e2556e4564985f5439a5e6a277
     * @param body   json字符串 String body = "{\"merchantCode\":\"123456789900081\",\"terminalCode\":\"00810001\",\"merchantOrderId\":\"20123333644493200\",\"transactionAmount\":\"1\",\"merchantRemark\":\"测试\",\"payMode\":\"CODE_SCAN\",\"payCode\":\"285668667587422761\",\"transactionCurrencyCode\":\"156\"}";
     * @return
     * @throws Exception
     */
    public static String getOpenBodySig(String appId, String appKey, String body) throws Exception {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());   // eg:20190227113148
        String nonce = UUID.randomUUID().toString().replace("-", ""); // eg:be46cd581c9f46ecbd71b9858311ea12
        byte[] data = body.getBytes("UTF-8");
        log.info("data:{}", body);
        InputStream is = new ByteArrayInputStream(data);
        String bodyDigest = testSHA256(is); // eg:d60bc3aedeb853e2a11c0c096baaf19954dd9b752e48dea8e919e5fb29a42a8d
        log.info("bodyDigest:{}", bodyDigest);
        String str1_C = appId + timestamp + nonce + bodyDigest; // eg:f0ec96ad2c3848b5b810e7aadf369e2f + 20190227113148 + be46cd581c9f46ecbd71b9858311ea12 + d60bc3aedeb853e2a11c0c096baaf19954dd9b752e48dea8e919e5fb29a42a8d
        log.info("str1_C:{}", str1_C);
        byte[] localSignature = hmacSHA256(str1_C.getBytes(), appKey.getBytes());
        String localSignatureStr = Base64.encodeBase64String(localSignature);   // Signature
        log.info("Authorization:\n" + "OPEN-BODY-SIG AppId=" + "\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"\n");
        return ("OPEN-BODY-SIG AppId=" + "\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"");
    }

    /**
     * 进行加密
     *
     * @param is
     * @return 加密后的结果
     */
    private static String testSHA256(InputStream is) {
        try {
            return DigestUtils.sha256Hex(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param data
     * @param key
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public static byte[] hmacSHA256(byte[] data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data);
    }

}
