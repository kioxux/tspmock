package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditGsmSettingVO {

    @NotBlank(message = "营销活动ID不能为空")
    private String gsmMarketid;
    /**
     * 营销主题名称
     */
    private String gsmThename;
    /**
     * 营销活动简介
     */
    private String gsmTheintro;
    /**
     * 活动类型 (0:主题活动,1:周期活动, 2:会员日活动)
     */
    private String gsmType;
    /**
     * 活动开始时间
     */
    @Size(max = 8, min = 8, message = "活动开始时间格式yyyyMMdd")
    private String gsmStartd;
    /**
     * 活动结束时间
     */
    @Size(max = 8, min = 8, message = "活动结束时间格式yyyyMMdd")
    private String gsmEndd;
    /**
     * 销售额标值
     */
    private BigDecimal gsmIndexValue;
    /**
     * 销售额活动值
     */
    private BigDecimal gsmTargetValue;
    /**
     * 销售额增幅
     */
    private BigDecimal gsmSaleinc;
    /**
     * 客单价选择
     */
    private String gsmPricechoose;
    /**
     * 客单价
     */
    private BigDecimal gsmPriceinc;

    /**
     * 客单价活动值
     */
    private BigDecimal gsmActPriceinc;
    /**
     * 客单价对应交易次数修正值
     */
    private BigDecimal gsmPriceincSt;
    /**
     * 交易次数选择
     */
    private String gsmStchoose;
    /**
     * 交易次数
     */
    private BigDecimal gsmStinc;

    /**
     * 交易次数活动值
     */
    private BigDecimal gsmActStinc;

    /**
     * 交易次数对应客单价修正值
     */
    private BigDecimal gsmStincPrice;
    /**
     * 毛利率
     */
    private BigDecimal gsmGrprofit;
    /**
     * 选品条件
     */
    private String gsmProcondi;
    /**
     * 短信模板ID
     */
    private String gsmTempid;
    /**
     * 短信查询条件ID
     */
    private String gsmSmscondi;

    /**
     * 短信查询条件JSON
     */
    private String gsmSmscondiJson;
    /**
     * 短信通知人数
     */
    private BigDecimal gsmSmstimes;
    /**
     * 电话通知人数
     */
    private BigDecimal gsmTeltimes;
    /**
     * 电话通知条件
     */
    private String gsmTelcondi;
    /**
     * 电话通知条件JSON
     */
    private String gsmTelcondiJson;
    /**
     * DM单打印张数
     */
    private BigDecimal gsmDmtimes;
    /**
     * 折扣方式list
     */
    @Valid
    private List<SaveGsmSettingCountListVO> discountList;

}
