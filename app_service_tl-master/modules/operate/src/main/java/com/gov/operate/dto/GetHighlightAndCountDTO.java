package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/15 17:19
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetHighlightAndCountDTO extends GetHighlightDTO {

    /**
     * 数量
     */
    private String gssbQty;
}
