package com.gov.operate.storegroup;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.DateUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.BatchImportStoreDTO;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReadUtil {

    public static List<BatchImportStoreDTO> readCsv(String client, MultipartFile file){
        List<BatchImportStoreDTO> list = new ArrayList<>();
        if (!file.isEmpty()) {
            InputStreamReader isr = null;
            BufferedReader br = null;
            try {
                isr = new InputStreamReader(file.getInputStream(), "GBK");
                br = new BufferedReader(isr);
                String line = null;
                List<List<String>> strs = new ArrayList<>();
                while ((line = br.readLine()) != null) {
                    strs.add(Arrays.asList(line.split(",")));
                }
                JSONArray array = toJsonArray(strs);
                for (int i = 0; i < array.size(); i++) {
                    BatchImportStoreDTO dto = new BatchImportStoreDTO();
                    dto.setClient(client);
                    dto.setGssgBrId(array.getJSONObject(i).getString("门店编码"));
//                    dto.setStoName(array.getJSONObject(i).getString("门店名称"));
                    dto.setGssgType(array.getJSONObject(i).getString("分类类型编码"));
//                    dto.setGssgTypeName(array.getJSONObject(i).getString("分类类型名称"));
                    dto.setGssgId(array.getJSONObject(i).getString("分类名称编码"));
//                    dto.setGssgIdName(array.getJSONObject(i).getString("分类名称"));
                    list.add(dto);
                }
            } catch (IOException e) {
                throw new CustomResultException("导入门店分类发生问题");
            } finally {
                try {
                    if (br != null) {
                        br.close();
                    }
                    if (isr != null) {
                        isr.close();
                    }
                } catch (IOException e) {
                    throw new CustomResultException("导入门店分类发生问题");
                }
            }
        }
        return list;
    }

    private static JSONArray toJsonArray(List<List<String>> strs) {
        JSONArray array = new JSONArray();
        for (int i = 1; i < strs.size(); i++) {
            JSONObject object = new JSONObject();
            for (int j = 0; j < strs.get(1).size(); j++) {
                object.put(strs.get(0).get(j),strs.get(i).get(j));
            }
            array.add(object);
        }
        return array;
    }

}
