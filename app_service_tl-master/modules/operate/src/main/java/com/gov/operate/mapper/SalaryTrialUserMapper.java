package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.StoreSaleD;
import com.gov.operate.dto.UserDTO;
import com.gov.operate.entity.SalaryTrialUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface SalaryTrialUserMapper extends BaseMapper<SalaryTrialUser> {

    List<UserDTO> getUserList(@Param("client") String client, @Param("storeList") List<String> storeList);

    /**
     * 门店销售明细
     *
     * @param client    加盟商
     * @param stoCode   门店
     * @param dateBegin 上月开始时间
     * @param dateEnd   上月结束时间
     * @return
     */
    List<StoreSaleD> getStoreSaleDetails(@Param("client") String client,
                                         @Param("stoCode") String stoCode,
                                         @Param("dateBegin") String dateBegin,
                                         @Param("dateEnd") String dateEnd,
                                         @Param("negative") Integer negative);
}
