package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 开票人员基本信息
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALES_CREATE_INFO")
@ApiModel(value="SalesCreateInfo对象", description="开票人员基本信息")
public class SalesCreateInfo extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("GSI_SITE")
    private String gsiSite;

    @ApiModelProperty(value = "电话号码")
    @TableField("GSI_TELPHONE")
    private String gsiTelphone;

    @ApiModelProperty(value = "邮箱")
    @TableField("GSI_EMAIL")
    private String gsiEmail;

    @ApiModelProperty(value = "创建人")
    @TableField("GSI_CREATE_USER")
    private String gsiCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GSI_CREATE_DATE")
    private String gsiCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GSI_CREATE_TIME")
    private String gsiCreateTime;

    @ApiModelProperty(value = "状态")
    @TableField("GSI_STATUS")
    private String gsiStatus;


}
