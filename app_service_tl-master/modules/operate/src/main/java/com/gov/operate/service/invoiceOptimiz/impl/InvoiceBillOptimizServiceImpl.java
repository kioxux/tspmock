package com.gov.operate.service.invoiceOptimiz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDto;
import com.gov.operate.entity.InvoiceBill;
import com.gov.operate.entity.InvoiceBillDocument;
import com.gov.operate.entity.InvoiceBillDocumentItem;
import com.gov.operate.entity.InvoiceBillInvoice;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.InvoiceBillDocumentItemMapper;
import com.gov.operate.mapper.InvoiceBillInvoiceMapper;
import com.gov.operate.mapper.InvoiceBillMapper;
import com.gov.operate.mapper.InvoiceMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IInvoiceBillDocumentItemService;
import com.gov.operate.service.IInvoiceBillDocumentService;
import com.gov.operate.service.IInvoiceBillInvoiceService;
import com.gov.operate.service.invoiceOptimiz.InvoiceBillOptimizService;
import com.gov.redis.jedis.RedisClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.22
 */
@Service
public class InvoiceBillOptimizServiceImpl implements InvoiceBillOptimizService {
    /**
     * 对账单号key
     */
    private final static String INVOICEBILL_KEY = "INVOICEBILL";
    @Resource
    private RedisClient redisClient;
    @Resource
    private CommonService commonService;
    @Resource
    private InvoiceBillMapper invoiceBillMapper;
    @Resource
    private InvoiceBillInvoiceMapper invoiceBillInvoiceMapper;
    @Resource
    private InvoiceBillDocumentItemMapper invoiceBillDocumentItemMapper;
    @Resource
    private InvoiceMapper invoiceMapper;
    @Resource
    private IInvoiceBillInvoiceService iInvoiceBillInvoiceService;
    @Resource
    private IInvoiceBillDocumentService iInvoiceBillDocumentService;
    @Resource
    private IInvoiceBillDocumentItemService iInvoiceBillDocumentItemService;

    /**
     * 发票对账
     *
     * @param documentBillDto
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void invoiceBill(DocumentBillDto documentBillDto) {
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        // 单据未选择
        if (CollectionUtils.isEmpty(documentBillDto.getSelectWarehousingDTOList())) {
            throw new CustomResultException("请选择对账单据");
        }
        // 发票未选择
        if (CollectionUtils.isEmpty(documentBillDto.getFicoInvoiceInformationRegistrationDTOList())) {
            throw new CustomResultException("请选择对账发票");
        }
        // 验证
        // 收货金额 = 折扣 + 应付 + 差异
        if (documentBillDto.getGibDocTotalAmt() == null) {
            throw new CustomResultException("收货金额不能为空");
        }
        if (documentBillDto.getGibInvoiceTotalAmt() == null) {
            throw new CustomResultException("发票金额不能为空");
        }
        if (documentBillDto.getGibPayAmt() == null) {
            throw new CustomResultException("应付金额不能为空");
        }
        if (documentBillDto.getGibInvoiceDiff() != null &&
                documentBillDto.getGibInvoiceDiff().compareTo(BigDecimal.ZERO) != 0 && StringUtils.isBlank(documentBillDto.getGibReason())) {
            throw new CustomResultException("请填入差异原因");
        }
        // 付款差异 = 收款金额 - 折扣 - 应付
        documentBillDto.setGibDocDiff(documentBillDto.getGibDocDiff() == null ? BigDecimal.ZERO : documentBillDto.getGibDocDiff());
        if (documentBillDto.getGibDocDiff().compareTo(DecimalUtils.subtract(DecimalUtils.subtract(documentBillDto.getGibDocTotalAmt(), documentBillDto.getGibRebate()), documentBillDto.getGibPayAmt())) != 0) {
            throw new CustomResultException("付款差异=收款金额-折扣-应付，请核对");
        }
        // 发票差异 = 发票金额 - 折扣 - 应付
        documentBillDto.setGibInvoiceDiff(documentBillDto.getGibInvoiceDiff() == null ? BigDecimal.ZERO : documentBillDto.getGibInvoiceDiff());
        if (documentBillDto.getGibInvoiceDiff().compareTo(
                DecimalUtils.subtract(
                        DecimalUtils.subtract(documentBillDto.getGibInvoiceTotalAmt(), documentBillDto.getGibRebate()),
                        documentBillDto.getGibPayAmt())) != 0
        ) {
            throw new CustomResultException("付款差异=收款金额-折扣-应付，请核对");
        }

        Long billNum = invoiceBillMapper.selectBillNum(user.getClient());
        // 单号
        while (redisClient.exists(INVOICEBILL_KEY + billNum)) {
            billNum++;
        }
        redisClient.set(INVOICEBILL_KEY + billNum, billNum.toString(), 120);
        List<InvoiceBillDocument> invoiceBillDocumentList = new ArrayList<>();
        List<InvoiceBillDocumentItem> invoiceBillDocumentItemList = new ArrayList<>();
        // 收货金额
        BigDecimal gibDocTotalAmt = BigDecimal.ZERO;
        // 收货明细金额合计
        BigDecimal gibDocItemTotalAmt = BigDecimal.ZERO;
        // 供应商
        String supCode = "";
        // 单据
        for (SelectWarehousingDTO selectWarehousingDTO : documentBillDto.getSelectWarehousingDTOList()) {
            if (StringUtils.isBlank(supCode)) {
                supCode = selectWarehousingDTO.getSupSelfCode();
            } else {
                if (!supCode.equals(selectWarehousingDTO.getSupSelfCode())) {
                    throw new CustomResultException("请选择同一供应商单据");
                }
            }
            SelectWarehousingVO vo = new SelectWarehousingVO();
            vo.setClient(user.getClient());
            vo.setBillNo(selectWarehousingDTO.getMatDnId());
            vo.setType(selectWarehousingDTO.getType());
            SelectWarehousingDTO entity = invoiceMapper.selectDocumentItem(vo);
            if (entity == null) {
                throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "不存在");
            }
            // 本次对账金额
            gibDocTotalAmt = DecimalUtils.add(gibDocTotalAmt, selectWarehousingDTO.getPaymentAmt());
            // 单据对账金额
            BigDecimal gdbhBillAmount = BigDecimal.ZERO;
            for (SelectWarehousingDetailsDTO selectDocumentDetail : selectWarehousingDTO.getDetailList()) {
                SelectWarehousingDetailsVO selectWarehousingDetailsVO = new SelectWarehousingDetailsVO();
                selectWarehousingDetailsVO.setClient(user.getClient());
                selectWarehousingDetailsVO.setMatId(selectDocumentDetail.getMatId());
                selectWarehousingDetailsVO.setMatYear(selectDocumentDetail.getMatYear());
                selectWarehousingDetailsVO.setMatLineNo(selectDocumentDetail.getMatLineNo());
                SelectWarehousingDetailsDTO selectWarehousingDetailsDTO = invoiceMapper.selectDocumentDetail(selectWarehousingDetailsVO);
                if (selectWarehousingDetailsDTO == null) {
                    throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "明细商品" + selectDocumentDetail.getProCode() + "不存在");
                }
                List<InvoiceBillDocumentItem> documentBillDEntityList = invoiceBillDocumentItemMapper.selectList(
                        new QueryWrapper<InvoiceBillDocumentItem>()
                                .eq("CLIENT", user.getClient())
                                .eq("GIBDI_BUSINESS_TYPE", selectDocumentDetail.getType())
                                .eq("GIBDI_MAT_DN_ID", selectDocumentDetail.getMatDnId())
                                .eq("MAT_ID", selectDocumentDetail.getMatId())
                                .eq("MAT_YEAR", selectDocumentDetail.getMatYear())
                                .eq("MAT_LINE_NO", selectDocumentDetail.getMatLineNo())
                );
                if (!CollectionUtils.isEmpty(documentBillDEntityList)) {
                    throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "明细商品" + selectDocumentDetail.getProCode() + "已对账");
                }
                gdbhBillAmount = DecimalUtils.add(gdbhBillAmount, selectDocumentDetail.getTotalAmount());

                InvoiceBillDocumentItem invoiceBillDocumentItem = new InvoiceBillDocumentItem();
                // 加盟商
                invoiceBillDocumentItem.setClient(user.getClient());
                // 对账单号
                invoiceBillDocumentItem.setGibdiNum("FPDZ" + billNum);
                // 业务类型
                invoiceBillDocumentItem.setGibdiBusinessType(selectDocumentDetail.getType());
                // 业务单号
                invoiceBillDocumentItem.setGibdiMatDnId(selectDocumentDetail.getMatDnId());
                // 物料凭证号
                invoiceBillDocumentItem.setMatId(selectDocumentDetail.getMatId());
                // 物料凭证年份
                invoiceBillDocumentItem.setMatYear(selectDocumentDetail.getMatYear());
                // 物料凭证行号
                invoiceBillDocumentItem.setMatLineNo(selectDocumentDetail.getMatLineNo());
                // 商品编码
                invoiceBillDocumentItem.setMatProCode(selectDocumentDetail.getProCode());
                // 行金额
                invoiceBillDocumentItem.setGibdiLineAmount(selectDocumentDetail.getTotalAmount());
                // 数量
                invoiceBillDocumentItem.setMatQty(selectDocumentDetail.getMatQty());
                // 总金额（批次）
                invoiceBillDocumentItem.setMatBatAmt(selectDocumentDetail.getExcludingTaxAmount());
                // 税金（批次）
                invoiceBillDocumentItem.setMatRateBat(selectDocumentDetail.getRateBat());
                // 创建人
                invoiceBillDocumentItem.setGibdiCreateUser(user.getUserId());
                // 创建日期
                invoiceBillDocumentItem.setGibdiCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                // 创建时间
                invoiceBillDocumentItem.setGibdiCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                invoiceBillDocumentItemList.add(invoiceBillDocumentItem);
                gibDocItemTotalAmt = DecimalUtils.add(gibDocItemTotalAmt, selectDocumentDetail.getTotalAmount());
            }
            if (selectWarehousingDTO.getPaymentAmt().compareTo(gdbhBillAmount) != 0) {
                throw new CustomResultException("单据" + selectWarehousingDTO.getMatDnId() + "本次对账金额与明细不一致，请核对");
            }

            InvoiceBillDocument invoiceBillDocument = new InvoiceBillDocument();
            // 加盟商
            invoiceBillDocument.setClient(user.getClient());
            // 对账单号
            invoiceBillDocument.setGibdNum("FPDZ" + billNum);
            // 业务类型
            invoiceBillDocument.setGibdBusinessType(selectWarehousingDTO.getType());
            // 业务单号
            invoiceBillDocument.setGibdMatDnId(selectWarehousingDTO.getMatDnId());
            // 地点
            invoiceBillDocument.setGibdSite(selectWarehousingDTO.getSite());
            // 供应商
            invoiceBillDocument.setGibdSupCode(selectWarehousingDTO.getSupSelfCode());
            // 对账金额
            invoiceBillDocument.setGibdBillAmount(selectWarehousingDTO.getPaymentAmt());
            // 全部登记
            // 创建人
            invoiceBillDocument.setGibdCreateUser(user.getUserId());
            // 创建日期
            invoiceBillDocument.setGibdCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            invoiceBillDocument.setGibdCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            invoiceBillDocumentList.add(invoiceBillDocument);
        }
        if (gibDocTotalAmt.compareTo(gibDocItemTotalAmt) != 0) {
            throw new CustomResultException("收货金额与对账账单金额不匹配，请核对");
        }

        List<InvoiceBillInvoice> invoiceBillInvoices = new ArrayList<>();
        // 发票金额
        BigDecimal gibInvoiceTotalAmt = BigDecimal.ZERO;
        // 发票
        for (FicoInvoiceInformationRegistrationDTO ficoInvoiceInformationRegistrationDTO : documentBillDto.getFicoInvoiceInformationRegistrationDTOList()) {
            if (StringUtils.isBlank(supCode)) {
                supCode = ficoInvoiceInformationRegistrationDTO.getSupCode();
            } else {
                if (!supCode.equals(ficoInvoiceInformationRegistrationDTO.getSupCode())) {
                    throw new CustomResultException("请选择同一供应商单据");
                }
            }
            // 发票是否已使用
            List<InvoiceBillInvoice> invoiceBillInvoiceList =
                    invoiceBillInvoiceMapper.selectList(new QueryWrapper<InvoiceBillInvoice>().eq("CLIENT", user.getClient())
                            .eq("GIBI_INVOICE_NUM", ficoInvoiceInformationRegistrationDTO.getInvoiceNum()));
            if (!CollectionUtils.isEmpty(invoiceBillInvoiceList)) {
                throw new CustomResultException("发票" + ficoInvoiceInformationRegistrationDTO.getInvoiceNum() + "已对账");
            }
            // 发票总金额
            gibInvoiceTotalAmt = DecimalUtils.add(gibInvoiceTotalAmt, ficoInvoiceInformationRegistrationDTO.getInvoiceTotalInvoiceAmount());

            InvoiceBillInvoice invoiceBillInvoice = new InvoiceBillInvoice();
            // 加盟商
            invoiceBillInvoice.setClient(user.getClient());
            // 对账单号
            invoiceBillInvoice.setGibiNum("FPDZ" + billNum);
            // 发票号
            invoiceBillInvoice.setGibiInvoiceNum(ficoInvoiceInformationRegistrationDTO.getInvoiceNum());
            // 创建人
            invoiceBillInvoice.setGibiCreateUser(user.getUserId());
            // 创建日期
            invoiceBillInvoice.setGibiCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            invoiceBillInvoice.setGibiCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            invoiceBillInvoices.add(invoiceBillInvoice);
        }
        if (gibInvoiceTotalAmt.compareTo(documentBillDto.getGibInvoiceTotalAmt()) != 0) {
            throw new CustomResultException("发票金额不正确，请核对");
        }
        // 发票对账
        InvoiceBill invoiceBill = new InvoiceBill();
        // 加盟商
        invoiceBill.setClient(user.getClient());
        // 对账单号
        invoiceBill.setGibNum("FPDZ" + billNum);
        // 供应商
        invoiceBill.setGibSupCode(supCode);
        // 收货金额
        invoiceBill.setGibDocTotalAmt(documentBillDto.getGibDocTotalAmt());
        // 发票金额
        invoiceBill.setGibInvoiceTotalAmt(documentBillDto.getGibInvoiceTotalAmt());
        // 折扣
        invoiceBill.setGibRebate(documentBillDto.getGibRebate() == null ? BigDecimal.ZERO : documentBillDto.getGibRebate());
        // 应付金额
        invoiceBill.setGibPayAmt(documentBillDto.getGibPayAmt());
        // 付款差异
        invoiceBill.setGibDocDiff(documentBillDto.getGibDocDiff() == null ? BigDecimal.ZERO : documentBillDto.getGibDocDiff());
        // 发票差异
        invoiceBill.setGibInvoiceDiff(documentBillDto.getGibInvoiceDiff() == null ? BigDecimal.ZERO : documentBillDto.getGibInvoiceDiff());
        // 原因
        invoiceBill.setGibReason(documentBillDto.getGibReason());
        // 创建人
        invoiceBill.setGibCreateUser(user.getUserId());
        // 创建日期
        invoiceBill.setGibCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 创建时间
        invoiceBill.setGibCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        invoiceBillMapper.insert(invoiceBill);
        // 发票关联
        iInvoiceBillInvoiceService.saveBatch(invoiceBillInvoices);
        // 单据关联
        iInvoiceBillDocumentService.saveBatch(invoiceBillDocumentList);
        // 单据明细
        iInvoiceBillDocumentItemService.saveBatch(invoiceBillDocumentItemList);
    }

    /**
     * 单据集合
     * @param vo
     * @return
     */
    @Override
    public IPage<SelectWarehousingDTO> selectDocumentList(SelectWarehousingVO vo) {
        // 地点和单体店若只有一个有值，那么另一个赋特殊值”-1“
        if (!((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))
                || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
            if (StringUtils.isBlank(vo.getSite())) {
                vo.setSite("-1");
            } else {
                vo.setStoCode("-1");
            }
        }
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(vo.getPageNum(), vo.getPageSize());
        IPage<SelectWarehousingDTO> selectWarehousingDTOS = invoiceMapper.selectDocumentListForInvoice(page, vo);
        selectWarehousingDTOS.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
            }

            // 明细
            SelectWarehousingDetailsVO selectWarehousingDetailsVO = new SelectWarehousingDetailsVO();
            selectWarehousingDetailsVO.setClient(user.getClient());
            selectWarehousingDetailsVO.setType(item.getType());
            selectWarehousingDetailsVO.setMatDnId(item.getMatDnId());
            List<SelectWarehousingDetailsDTO> list = invoiceMapper.selectDocumentDetailsForInvoice(selectWarehousingDetailsVO);
            list.forEach(detail -> {
                // 如果为出库 则 金额均为 负数
                if ("GD".equals(detail.getType()) || "CJ".equals(detail.getType())) {
                    BigDecimal totalAmount = detail.getTotalAmount();
                    // 税额
                    detail.setRateBat(detail.getRateBat().abs().multiply(Constants.MINUS_ONE));
                    // 去税金额
                    detail.setExcludingTaxAmount(detail.getExcludingTaxAmount().multiply(Constants.MINUS_ONE));
                    // 总额
                    detail.setTotalAmount(totalAmount.multiply(Constants.MINUS_ONE));
                }
            });
            item.setDetailList(list);
        });
        return selectWarehousingDTOS;
    }

    /**
     * 发票集合
     * @param pageNum
     * @param pageSize
     * @param siteCode
     * @param supCode
     * @param invoiceNum
     * @param invoiceDateStart
     * @param invoiceDateEnd
     * @return
     */
    @Override
    public Result selectInvoiceList(Integer pageNum, Integer pageSize, String siteCode, String supCode, String invoiceNum, String invoiceDateStart, String invoiceDateEnd) {
        // 登录人
        TokenUser user = commonService.getLoginInfo();
        Page page = new Page<SelectWarehousingDTO>(pageNum, pageSize);
        IPage<FicoInvoiceInformationRegistrationDTO> iPage = invoiceBillMapper.selectInvoiceListForBill(page, user.getClient(), siteCode, supCode,
                invoiceNum, invoiceDateStart, invoiceDateEnd);
        return ResultUtil.success(iPage);
    }
}

