package com.gov.operate.service.impl;

import cn.hutool.core.util.StrUtil;
import com.gov.operate.entity.WmsTupianzdrwcj;
import com.gov.operate.mapper.WmsTupianzdrwcjMapper;
import com.gov.operate.service.IWmsTupianzdrwcjService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-09-26
 */
@Service
public class WmsTupianzdrwcjServiceImpl extends ServiceImpl<WmsTupianzdrwcjMapper, WmsTupianzdrwcj> implements IWmsTupianzdrwcjService {

    @Autowired
    private WmsTupianzdrwcjMapper wmsTupianzdrwcjMapper;


    @Override
    public List<WmsTupianzdrwcj> selectCjResult(String client, String stoCode, String matchCode) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("CLIENT",client);
        if(StrUtil.isNotBlank(stoCode)){
            queryMap.put("PRO_SITE",stoCode);
        }
        queryMap.put("ZDRWCJ_BH",matchCode);
        List<WmsTupianzdrwcj> wmsTupianzdrwcjs = wmsTupianzdrwcjMapper.selectByMap(queryMap);
        return wmsTupianzdrwcjs;
    }

    @Override
    public List<WmsTupianzdrwcj> selectListByStoCodeAndZdrwcjBh(String stoCode, String matchCode) {
        return null;
    }

    @Override
    public Integer getMaxWmXhByMatchCode(String matchCode) {
        return wmsTupianzdrwcjMapper.getMaxWmXhByMatchCode(matchCode);
    }
}
