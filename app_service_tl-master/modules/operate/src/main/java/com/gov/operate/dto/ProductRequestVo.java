package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductRequestVo extends Pageable {

    /**
     * 活动ID
     */
    @NotBlank(message = "活动ID不能为空")
    private String gsmMarketid;

    private String client;

    private String gsmStore;

    private List<String> componentList;
    /**
     * 毛利率
     */
    private BigDecimal minGrprofit;

    private BigDecimal maxGrprofit;
    /**
     * 零售价
     */
    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品名
     */
    private String proName;
    /**
     * 促销
     */
    private String gsmPromo;
}
