package com.gov.operate.controller.invoiceOptimiz;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.invoiceOptimiz.FicoInvoiceInformationRegistrationDto;
import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.invoiceOptimiz.InvoiceOptimizService;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.15
 */

@Validated
@RestController
@RequestMapping("invoiceOptimiz")
public class InvoiceOptimizController {

    @Resource
    public InvoiceOptimizService invoiceOptimizService;

    @Log("发票集合")
    @ApiOperation(value = "发票集合")
    @PostMapping("getInvoiceList")
    public Result getInvoiceList(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                 @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                 @RequestJson(value = "invoiceSiteType", required = false) String invoiceSiteType,
                                 @RequestJson(value = "siteCode", required = false) String siteCode,
                                 @RequestJson(value = "supCode", required = false) String supCode,
                                 @RequestJson(value = "invoiceType", required = false) String invoiceType,
                                 @RequestJson(value = "invoiceNum", required = false) String invoiceNum,
                                 @RequestJson(value = "invoiceDate", required = false) String invoiceDate,
                                 @RequestJson(value = "status", required = false) Integer status,
                                 @RequestJson(value = "invoiceSalesman", required = false) String invoiceSalesman) {
        return ResultUtil.success(invoiceOptimizService.getInvoiceList(pageNum, pageSize, invoiceSiteType, siteCode, supCode,
                invoiceType, invoiceNum, invoiceDate, status, invoiceSalesman));
    }

    @Log("发票合计")
    @ApiOperation(value = "发票合计")
    @PostMapping("getInvoiceTotal")
    public Result getInvoiceTotal(@RequestJson(value = "invoiceSiteType", required = false) String invoiceSiteType,
                                  @RequestJson(value = "siteCode", required = false) String siteCode,
                                  @RequestJson(value = "supCode", required = false) String supCode,
                                  @RequestJson(value = "invoiceType", required = false) String invoiceType,
                                  @RequestJson(value = "invoiceNum", required = false) String invoiceNum,
                                  @RequestJson(value = "invoiceDate", required = false) String invoiceDate,
                                  @RequestJson(value = "status", required = false) Integer status) {
        return ResultUtil.success(invoiceOptimizService.getInvoiceTotal(invoiceSiteType, siteCode, supCode, invoiceType, invoiceNum, invoiceDate, status));
    }

    @Log("发票新增")
    @ApiOperation(value = "发票新增")
    @PostMapping("saveInvoiceList")
    public Result saveInvoiceList(@RequestBody List<FicoInvoiceInformationRegistration> list) {
        invoiceOptimizService.saveInvoiceList(list);
        return ResultUtil.success();
    }

    @Log("发票详情")
    @ApiOperation(value = "发票详情")
    @PostMapping("getInvoice")
    public Result getInvoice(@RequestJson(value = "invoiceNum", name = "发票号码") String invoiceNum) {
        return ResultUtil.success(invoiceOptimizService.getInvoice(invoiceNum));
    }

    @Log("发票删除")
    @ApiOperation(value = "发票删除")
    @PostMapping("delInvoice")
    public Result delInvoice(@RequestJson(value = "invoiceNum", name = "发票号码") String invoiceNum) {
        invoiceOptimizService.delInvoice(invoiceNum);
        return ResultUtil.success();
    }

    @Log("发票编辑")
    @ApiOperation(value = "发票编辑")
    @PostMapping("editInvoice")
    public Result editInvoice(@RequestBody FicoInvoiceInformationRegistrationDto ficoInvoiceInformationRegistration) {
        invoiceOptimizService.editInvoice(ficoInvoiceInformationRegistration);
        return ResultUtil.success();
    }
}

