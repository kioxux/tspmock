package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_POST")
@ApiModel(value="Post对象", description="")
public class Post extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLENT")
    private String clent;

    @ApiModelProperty(value = "岗位编码")
    @TableField("POS_NO")
    private String posNo;

    @ApiModelProperty(value = "岗位描述")
    @TableField("POS_NAME")
    private String posName;


}
