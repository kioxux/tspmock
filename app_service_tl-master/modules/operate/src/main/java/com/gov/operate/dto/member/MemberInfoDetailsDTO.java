package com.gov.operate.dto.member;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel("会员信息")
@Data
public class MemberInfoDetailsDTO {

    @ApiModelProperty("会员ID")
    private String gsmbMemberId;

    @ApiModelProperty("卡类型")
    private String gsmbcClassId;

    @ApiModelProperty("累计积分")
    private String gsmbcTotalIntegral;

    @ApiModelProperty("会员姓名")
    private String gsmbName;

    @ApiModelProperty("性别")
    private String gsmbSex;

    @ApiModelProperty("生日")
    private String gsmbBirth;

    @ApiModelProperty("年龄")
    private String gsmbAge;

    @ApiModelProperty("手机")
    private String gsmbMobile;

    @ApiModelProperty("企微绑定状态，0、null：未绑定，1：已绑定")
    private String gsmbcWorkwechatBindStatus;

    @ApiModelProperty("开卡门店")
    private String stoCode;

    @ApiModelProperty("开卡")
    private String stoName;

    @ApiModelProperty("卡号")
    private String gsmbcCardId;

    @ApiModelProperty("新卡创建日期")
    private String gsmbcCreateDate;

    @ApiModelProperty("最后消费日期")
    private String lastSaleDate;

    @ApiModelProperty("最后一次交易门店名称")
    private String lastStoName;

    @ApiModelProperty("最近回访时间")
    private String susFeedbacktime;

    @ApiModelProperty("会员标签")
    private String susFlg;

    @ApiModelProperty("会员标签List")
    private List<String> susFlgList;

    @ApiModelProperty("维系标签类型0电话空号1不在维系")
    private List<String> susFlgaddType;

    @ApiModelProperty("回访反馈")
    private String susFeedback;


}
