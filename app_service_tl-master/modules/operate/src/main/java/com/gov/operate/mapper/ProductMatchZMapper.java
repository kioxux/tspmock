package com.gov.operate.mapper;

import com.gov.operate.entity.ProductMatchZ;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 导入商品匹配信息主表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
public interface ProductMatchZMapper extends BaseMapper<ProductMatchZ> {

}
