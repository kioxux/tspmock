package com.gov.operate.service.impl;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.recharge.RechargeChangeDataExcel;
import com.gov.operate.dto.recharge.RechargeChangeInData;
import com.gov.operate.dto.recharge.RechargeChangeOutData;
import com.gov.operate.mapper.SdRechargeCardMapper;
import com.gov.operate.mapper.SdRechargeChangeMapper;
import com.gov.operate.service.ISdRechargeChangeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SdRechargeChangeServiceImpl implements ISdRechargeChangeService {

    @Autowired
    private SdRechargeChangeMapper rechargeChangeMapper;

    @Autowired
    private SdRechargeCardMapper rechargeCardMapper;


    @Override
    public IPage<RechargeChangeOutData> rechargeChangePage(RechargeChangeInData inData) {
        Page<RechargeChangeOutData> page = new Page<>(inData.getPageNum(),inData.getPageSize());
        IPage<RechargeChangeOutData> rechargeChangeOutDataIPage = rechargeChangeMapper.rechargeChangePage(page, inData);
        List<RechargeChangeOutData> records = rechargeChangeOutDataIPage.getRecords();
        if (CollectionUtils.isEmpty(records)) {
            return new Page<>();
        } else {
            for(int i = 0; i < records.size(); ++i) {
                records.get(i).setIndex(i + 1);
            }
        }
        return rechargeChangeOutDataIPage;
    }

    @Override
    public void export(RechargeChangeInData inData, HttpServletResponse response) throws IOException {

        Page page = new Page();
        //查询要导出的数据
        IPage<RechargeChangeOutData> rechargeChangeOutDataIPage = rechargeChangeMapper.rechargeChangePage(page, inData);
        List<RechargeChangeOutData> rechargeChangeOutDataList = rechargeChangeOutDataIPage.getRecords();

        List<RechargeChangeDataExcel> dataExcels = new ArrayList<>();
        Integer i = 1;
        for (RechargeChangeOutData rechargeChangeOutData:rechargeChangeOutDataList) {
            RechargeChangeDataExcel rechargeChangeDataExcel = new RechargeChangeDataExcel();
            BeanUtils.copyProperties(rechargeChangeOutData,rechargeChangeDataExcel);
            //序号
            rechargeChangeDataExcel.setIndex(i);
            i++;
            //异动类型
            String gsrcType = rechargeChangeDataExcel.getGsrcType();
            if(("1").equals(gsrcType)){
                rechargeChangeDataExcel.setGsrcType("充值");
            }else if(("2").equals(gsrcType)){
                rechargeChangeDataExcel.setGsrcType("退款");
            }else if(("3").equals(gsrcType)){
                rechargeChangeDataExcel.setGsrcType("消费");
            }else if(("4").equals(gsrcType)){
                rechargeChangeDataExcel.setGsrcType("退货");
            }
            dataExcels.add(rechargeChangeDataExcel);
        }
//        String fileName = CommonConstant.RECHARGE_CHANGE_EXPORT_FILE_NAME + "-" + CommonUtil.getyyyyMMdd();
//        String fileName = new String("储值卡金额异动.xlsx".getBytes(StandardCharsets.UTF_8),StandardChaFFrsets.ISO_8859_1);
        String fileName = URLEncoder.encode("储值卡金额异动.xlsx","utf-8");
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        WriteSheet writeSheet1 = EasyExcel.writerSheet(0, fileName).head(RechargeChangeDataExcel.class).build();

        excelWriter.write(dataExcels, writeSheet1);
        excelWriter.finish();
    }
}
