package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYMENT_VAT_APPLICATIONS")
@ApiModel(value="PaymentVatApplications对象", description="")
public class PaymentVatApplications extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款单号")
    @TableField("PAYMENT_ORDER_NO")
    private String paymentOrderNo;

    @ApiModelProperty(value = "行号")
    @TableField("LINE_NO")
    private String lineNo;

    @ApiModelProperty(value = "发票号码")
    @TableField("INVOICE_NUM")
    private String invoiceNum;

    @ApiModelProperty(value = "付款单据日期")
    @TableField("PAYMENT_ORDER_DATE")
    private String paymentOrderDate;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "已付款金额")
    @TableField("INVOICE_PAID_AMOUNT")
    private BigDecimal invoicePaidAmount;

    @ApiModelProperty(value = "本次付款金额")
    @TableField("INVOICE_AMOUNT_OF_THIS_PAYMENT")
    private BigDecimal invoiceAmountOfThisPayment;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @TableField("APPLICATIONS_ID")
    private Integer applicationsId;


}
