package com.gov.operate.service;

import com.gov.operate.entity.AppLoginLog;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-11
 */
public interface IAppLoginLogService extends SuperService<AppLoginLog> {

}
