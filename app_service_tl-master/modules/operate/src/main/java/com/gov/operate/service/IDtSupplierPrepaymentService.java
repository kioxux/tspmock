package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.DtSupplierPrepayment;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2021-09-09
 */
public interface IDtSupplierPrepaymentService extends SuperService<DtSupplierPrepayment> {

}
