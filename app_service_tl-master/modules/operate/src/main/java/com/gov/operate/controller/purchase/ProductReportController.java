package com.gov.operate.controller.purchase;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.purchase.productReport.ProReplenishmentParams;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.purchase.productReport.ProductReportService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.10.09
 */
@RestController
@RequestMapping("productReport")
public class ProductReportController {

    @Resource
    private ProductReportService productReportService;

    @Log("DC补货端口数据报表")
    @ApiOperation("DC补货端口数据报表")
    @PostMapping("getProReplenishmentList")
    public Result getProReplenishmentList(@RequestJson(value = "pageSize") Integer pageSize,
                                          @RequestJson(value = "pageNum") Integer pageNum,
                                          @RequestJson(value = "dcCode", name = "补货仓库") String dcCode,
                                          @RequestJson(value = "stoCode", name = "零售价参考门店") String stoCode,
                                          @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                          @RequestJson(value = "proClass", required = false) String proClass,
                                          @RequestJson(value = "lastSupp", required = false) String lastSupp,
                                          @RequestJson(value = "recSupp", required = false) String recSupp,
                                          @RequestJson(value = "proZdy1", required = false) String proZdy1,
                                          @RequestJson(value = "proZdy2", required = false) String proZdy2,
                                          @RequestJson(value = "proZdy3", required = false) String proZdy3,
                                          @RequestJson(value = "proZdy4", required = false) String proZdy4,
                                          @RequestJson(value = "proZdy5", required = false) String proZdy5,
                                          @RequestJson(value = "proSlaeClass", required = false) String proSlaeClass,
                                          @RequestJson(value = "proSclass", required = false) String proSclass,
                                          @RequestJson(value = "proNoPurchase", required = false) Integer proNoPurchase,
                                          @RequestJson(value = "proPosition", required = false) Integer proPosition,
                                          @RequestJson(value = "proReplenishmentParamsList", required = false) List<ProReplenishmentParams> proReplenishmentParamsList,
                                          @RequestJson(value = "salesVolumeStartDate", required = false) String salesVolumeStartDate,
                                          @RequestJson(value = "salesVolumeEndDate", required = false) String salesVolumeEndDate,
                                          @RequestJson(value = "proPurchaseRate", required = false) String proPurchaseRate
    ) {
        return productReportService.getProReplenishmentList(pageSize, pageNum, dcCode, stoCode, proSelfCode, proClass, lastSupp, recSupp,
                proZdy1, proZdy2, proZdy3, proZdy4, proZdy5, proSlaeClass, proSclass, proNoPurchase, proPosition, proReplenishmentParamsList,
                salesVolumeStartDate, salesVolumeEndDate, proPurchaseRate);
    }

}
