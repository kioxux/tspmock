package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.dto.GetPromListVO;
import com.gov.operate.entity.SdPromHead;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-28
 */
public interface ISdPromHeadService extends SuperService<SdPromHead> {

    /**
     *
     */
    IPage<SdPromHead> getPromList(GetPromListVO vo);
}
