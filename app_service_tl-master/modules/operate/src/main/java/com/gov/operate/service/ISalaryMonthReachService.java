package com.gov.operate.service;

import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryMonthReach;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryMonthReachService extends SuperService<SalaryMonthReach> {

    /**
     * 设定月综合达成率/达成系数
     */
    void saveReachList(EditProgramVO vo);

    /**
     * 获取达成率/达成系数
     */
    List<SalaryMonthReach> getReachList(Integer gspId);

    /**
     * 获取达成率(小数)/达成系数
     */
    List<SalaryMonthReach> getReachDecimalList(Integer gspId);
}
