package com.gov.operate.dto;

import com.gov.operate.entity.Images;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ImagesDTO extends Images {

    /**
     * 图片绝对路径
     */
    private String pathUrl;
}
