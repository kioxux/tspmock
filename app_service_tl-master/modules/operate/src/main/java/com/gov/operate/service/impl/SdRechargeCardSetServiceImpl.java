package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.AddRechargeSetVO;
import com.gov.operate.entity.SdRechargeCardSet;
import com.gov.operate.entity.StoreData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdRechargeCardSetMapper;
import com.gov.operate.mapper.StoreDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdRechargeCardSetService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class SdRechargeCardSetServiceImpl extends ServiceImpl<SdRechargeCardSetMapper, SdRechargeCardSet> implements ISdRechargeCardSetService {

    @Resource
    private CommonService commonService;
    @Resource
    private SdRechargeCardSetMapper sdRechargeCardSetMapper;
    @Resource
    private StoreDataMapper storeDataMapper;

    /**
     * 储存卡的设置
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer addOrUpdateCard(AddRechargeSetVO vo) {
        // 门店未选择
        if (CollectionUtils.isEmpty(vo.getStoCodeList())) {
            throw new CustomResultException("请选择门店");
        }

        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());

        //支付必须输入密码
        if (StringUtils.isEmpty(vo.getGsrcsPwNeedOpt())) {
            vo.setGsrcsPassword("N");
        }
        //恢复密码需店长权限
        if (StringUtils.isEmpty(vo.getGsrcsRecoverpwOpt())) {
            vo.setGsrcsRecoverpwOpt("N");
        }
        //比例1
        if (StringUtils.isEmpty(vo.getGsrcsProportion1())) {
            vo.setGsrcsProportion1("100");
        }
        //起充金额1
        if (null == vo.getGsrcsMinAmt1()) {
            vo.setGsrcsMinAmt1(BigDecimal.ONE);
        }

        //是否活动充值2
        if (StringUtils.isEmpty(vo.getGsrcsPromotion2())) {
            vo.setGsrcsPromotion2("N");
        }
        //是否活动充值3
        if (StringUtils.isEmpty(vo.getGsrcsPromotion3())) {
            vo.setGsrcsPromotion3("N");
        }
        vo.setGsrcsUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        vo.setGsrcsUpdateEmp(user.getUserId());

        List<SdRechargeCardSet> setList = new ArrayList<>();
        vo.getStoCodeList().forEach(item -> {
            //门店编码
            vo.setGsrcsBrId(item);
            SdRechargeCardSet sdRechargeCardSet = new SdRechargeCardSet();
            BeanUtils.copyProperties(vo, sdRechargeCardSet);
            setList.add(sdRechargeCardSet);
        });
        //全删
        sdRechargeCardSetMapper.BatchDelete(user.getClient(), setList);
        //新增
        this.saveBatch(setList);
        return 1;
    }


    /**
     * 储存卡的获取
     */
    @Override
    public SdRechargeCardSet getClientCard() {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        String storeCode = user.getDepId();
        if (StringUtils.isEmpty(storeCode)) {
            List<String> codeList = sdRechargeCardSetMapper.getStoreCode(user.getUserId(), user.getClient());
            if (codeList.isEmpty()) {
                return new SdRechargeCardSet();
            } else {
                storeCode = codeList.get(0);
            }
        }
        List<SdRechargeCardSet> list = sdRechargeCardSetMapper.selectList(new QueryWrapper<SdRechargeCardSet>()
                .eq("CLIENT", user.getClient())
                .eq("GSRCS_BR_ID", storeCode));
        if (list.isEmpty()) {
            return new SdRechargeCardSet();
        }
        return list.get(0);

    }

    /**
     * 获取充值比例
     */
    @Override
    public List<AddRechargeSetVO> getProportionData() {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        List<AddRechargeSetVO> list = new ArrayList<>();

        String storeCode = user.getDepId();
        if (StringUtils.isEmpty(storeCode)) {
            List<String> codeList = sdRechargeCardSetMapper.getStoreCode(user.getUserId(), user.getClient());
            if (codeList.isEmpty()) {
                return list;
            } else {
                storeCode = codeList.get(0);
            }
        }

        SdRechargeCardSet cardSet = sdRechargeCardSetMapper.selectOne(new QueryWrapper<SdRechargeCardSet>()
                .eq("CLIENT", user.getClient())
                .eq("GSRCS_BR_ID", storeCode));
        if (null != cardSet) {
            if (StringUtils.isNotBlank(cardSet.getGsrcsProportion1()) && cardSet.getGsrcsMinAmt1() != null) {
                AddRechargeSetVO rechargeSetVO = new AddRechargeSetVO();
                rechargeSetVO.setProportion(cardSet.getGsrcsProportion1());
                rechargeSetVO.setMinAmt(cardSet.getGsrcsMinAmt1());
                rechargeSetVO.setType(1);
                list.add(rechargeSetVO);
            } else {
                AddRechargeSetVO rechargeSetVO = new AddRechargeSetVO();
                rechargeSetVO.setProportion("100");
                rechargeSetVO.setMinAmt(new BigDecimal(0.01));
                rechargeSetVO.setType(1);
                list.add(rechargeSetVO);
            }
            if ("Y".equals(cardSet.getGsrcsPromotion2())) {
                AddRechargeSetVO rechargeSetVO = new AddRechargeSetVO();
                rechargeSetVO.setProportion(cardSet.getGsrcsProportion2());
                rechargeSetVO.setMinAmt(cardSet.getGsrcsMinAmt2());
                rechargeSetVO.setType(2);
                list.add(rechargeSetVO);
            }
            if ("Y".equals(cardSet.getGsrcsPromotion3())) {
                AddRechargeSetVO rechargeSetVO = new AddRechargeSetVO();
                rechargeSetVO.setProportion(cardSet.getGsrcsProportion3());
                rechargeSetVO.setMinAmt(cardSet.getGsrcsMinAmt3());
                rechargeSetVO.setType(3);
                list.add(rechargeSetVO);
            }
        } else {
            AddRechargeSetVO rechargeSetVO = new AddRechargeSetVO();
            rechargeSetVO.setProportion("100");
            rechargeSetVO.setMinAmt(new BigDecimal(0.01));
            rechargeSetVO.setType(1);
            list.add(rechargeSetVO);
        }
        return list;
    }

    /**
     * 储值卡设置门店
     *
     * @return
     */
    @Override
    public Result getStoreList() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = storeDataMapper.selectList(
                new QueryWrapper<StoreData>().eq("CLIENT", user.getClient())
                        .orderByAsc("STO_CODE")
        );
        return ResultUtil.success(list);
    }


}
