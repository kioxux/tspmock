package com.gov.operate.service.dataImport.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.entity.dataImport.MarketingComponent;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.ProductComponent;
import com.gov.operate.mapper.ProductComponentMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.08
 */
@Service
public class MarketingComponentImport extends BusinessImport {

    @Resource
    private ProductComponentMapper productComponentMapper;

    // 导入
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 已选成分
        List<String> compCodeList = null;
        if (field.get("compCodeList") != null) {
            compCodeList = (List<String>) field.get("compCodeList");
        }
        if (compCodeList == null) {
            compCodeList = new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        List<ProductComponent> productComponentList = productComponentMapper.selectList(new QueryWrapper<ProductComponent>());
        if (!CollectionUtils.isEmpty(productComponentList)) {
            list = productComponentList.stream().map(ProductComponent::getProCompCode).collect(Collectors.toList());
        } else {
            throw new CustomResultException("数据库不存在有效成分");
        }
        List<String> errorList = new ArrayList<>();
        List<String> resultList = new ArrayList<>();
        // 行数据处理
        for (Integer key : map.keySet()) {
            // 行数据
            MarketingComponent marketingComponent = (MarketingComponent) map.get(key);
            if (compCodeList.contains(marketingComponent.getComponent())) {
                errorList.add(MessageFormat.format("第{0}行：成分已存在", key + 1));
                continue;
            }
            if (resultList.contains(marketingComponent.getComponent())) {
                errorList.add(MessageFormat.format("第{0}行：成分已存在", key + 1));
                continue;
            }
            // 成分存在验证
            if (list.contains(marketingComponent.getComponent())) {
                resultList.add(marketingComponent.getComponent());
            } else {
                errorList.add(MessageFormat.format("第{0}行：成分编码不存在", key + 1));
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(resultList);
    }
}
