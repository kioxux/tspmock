package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_KPI")
@ApiModel(value="SalaryKpi对象", description="")
public class SalaryKpi extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSK_ID", type = IdType.AUTO)
    private Integer gskId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSK_GSP_ID")
    private Integer gskGspId;

    @ApiModelProperty(value = "KPI得分最小值")
    @TableField("GSK_KPI_MIN")
    private BigDecimal gskKpiMin;

    @ApiModelProperty(value = "KPI得分最大值")
    @TableField("GSK_KPI_MAX")
    private BigDecimal gskKpiMax;

    @ApiModelProperty(value = "员工个人系数")
    @TableField("GSK_COEFFI")
    private BigDecimal gskCoeffi;

    @ApiModelProperty(value = "顺序")
    @TableField("GSK_SORT")
    private Integer gskSort;


}
