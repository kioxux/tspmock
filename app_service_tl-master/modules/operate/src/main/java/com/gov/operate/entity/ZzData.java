package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_ZZ_DATA")
@ApiModel(value="ZzData对象", description="")
public class ZzData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "唯一id")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "加盟商ID")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "机构编码")
    @TableField("ZZ_ORGID")
    private String zzOrgid;

    @ApiModelProperty(value = "机构名称")
    @TableField("ZZ_ORGNAME")
    private String zzOrgname;

    @ApiModelProperty(value = "资质编码")
    @TableField("ZZ_ID")
    private String zzId;

    @ApiModelProperty(value = "证照名称")
    @TableField("ZZ_NAME")
    private String zzName;

    @ApiModelProperty(value = "证照编号")
    @TableField("ZZ_NO")
    private String zzNo;

    @ApiModelProperty(value = "有效日期起")
    @TableField("ZZ_SDATE")
    private String zzSdate;

    @ApiModelProperty(value = "有效日期至")
    @TableField("ZZ_EDATE")
    private String zzEdate;

    @ApiModelProperty(value = "创建日期")
    @TableField("ZZ_CRE_DATE")
    private String zzCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("ZZ_CRE_TIME")
    private String zzCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("ZZ_CRE_ID")
    private String zzCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("ZZ_MODI_DATE")
    private String zzModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("ZZ_MODI_TIME")
    private String zzModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("ZZ_MODI_ID")
    private String zzModiId;

    @ApiModelProperty(value = "人员编码")
    @TableField("ZZ_USER_ID")
    private String zzUserId;

    @ApiModelProperty(value = "人员名称")
    @TableField("ZZ_USER_NAME")
    private String zzUserName;


}
