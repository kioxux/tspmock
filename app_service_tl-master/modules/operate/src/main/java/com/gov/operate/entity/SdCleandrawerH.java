package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_CLEANDRAWER_H")
@ApiModel(value="SdCleandrawerH对象", description="")
public class SdCleandrawerH extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "店号")
    @TableField("GSCH_BR_ID")
    private String gschBrId;

    @ApiModelProperty(value = "清斗单号")
    @TableField("GSCH_VOUCHER_ID")
    private String gschVoucherId;

    @ApiModelProperty(value = "清斗日期")
    @TableField("GSCH_DATE")
    private String gschDate;

    @ApiModelProperty(value = "验收单号")
    @TableField("GSCH_EXA_VOUCHER_ID")
    private String gschExaVoucherId;

    @ApiModelProperty(value = "清斗状态")
    @TableField("GSCH_STATUS")
    private String gschStatus;

    @ApiModelProperty(value = "清斗人员")
    @TableField("GSCH_EMP")
    private String gschEmp;

    @ApiModelProperty(value = "备注")
    @TableField("GSCH_REMAKS")
    private String gschRemaks;

    @ApiModelProperty(value = "复核人员")
    @TableField("GSCH_EMP1")
    private String gschEmp1;
}
