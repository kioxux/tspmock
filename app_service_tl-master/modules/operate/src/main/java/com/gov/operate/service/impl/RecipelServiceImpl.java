package com.gov.operate.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.GaiaSdRecipelList;
import com.gov.operate.mapper.RecipelMapper;
import com.gov.operate.service.RecipelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tzh
 * @since 2020-09-03
 */
@Service
public class RecipelServiceImpl extends ServiceImpl<RecipelMapper, GaiaSdRecipelList> implements RecipelService {

}
