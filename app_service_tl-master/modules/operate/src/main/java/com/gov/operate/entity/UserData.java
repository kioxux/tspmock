package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_DATA")
@ApiModel(value="UserData对象", description="")
public class UserData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工编号")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "姓名")
    @TableField("USER_NAM")
    private String userNam;

    @ApiModelProperty(value = "性别")
    @TableField("USER_SEX")
    private String userSex;

    @ApiModelProperty(value = "手机号")
    @TableField("USER_TEL")
    private String userTel;

    @ApiModelProperty(value = "身份证号")
    @TableField("USER_IDC")
    private String userIdc;

    @ApiModelProperty(value = "注册证号")
    @TableField("USER_YS_ID")
    private String userYsId;

    @ApiModelProperty(value = "职业范围")
    @TableField("USER_YS_ZYFW")
    private String userYsZyfw;

    @ApiModelProperty(value = "职业范围")
    @TableField("USER_YS_ZYLB")
    private String userYsZylb;

    @ApiModelProperty(value = "部门ID")
    @TableField("DEP_ID")
    private String depId;

    @ApiModelProperty(value = "部门名称")
    @TableField("DEP_NAME")
    private String depName;

    @ApiModelProperty(value = "地址")
    @TableField("USER_ADDR")
    private String userAddr;

    @ApiModelProperty(value = "在职状态")
    @TableField("USER_STA")
    private String userSta;

    @ApiModelProperty(value = "创建日期")
    @TableField("USER_CRE_DATE")
    private String userCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("USER_CRE_TIME")
    private String userCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("USER_CRE_ID")
    private String userCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("USER_MODI_DATE")
    private String userModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("USER_MODI_TIME")
    private String userModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("USER_MODI_ID")
    private String userModiId;

    @ApiModelProperty(value = "入职日期")
    @TableField("USER_JOIN_DATE")
    private String userJoinDate;

    @ApiModelProperty(value = "停用日期")
    @TableField("USER_DIS_DATE")
    private String userDisDate;

    @ApiModelProperty(value = "登陆密码")
    @TableField("USER_PASSWORD")
    private String userPassword;

    @ApiModelProperty(value = "连锁总部")
    @TableField("STO_CHAIN_HEAD")
    private String stoChainHead;

    @ApiModelProperty(value = "所在地")
    @TableField("USER_PROVINCE")
    private String userProvince;

    @ApiModelProperty(value = "邮箱")
    @TableField("USER_EMAIL")
    private String userEmail;

    @ApiModelProperty(value = "执业医生证号")
    @TableField("USER_PDC_ID")
    private String userPdcId;

    @ApiModelProperty(value = "科室")
    @TableField("USER_PREDEP")
    private String userPredep;

    @ApiModelProperty(value = "身份证照片地址")
    @TableField("USER_IDCPHO_ADDS")
    private String userIdcphoAdds;

    @ApiModelProperty(value = "身份证照片审核")
    @TableField("USER_IDCPHO_STATUS")
    private String userIdcphoStatus;

    @ApiModelProperty(value = "执业医生证照片地址")
    @TableField("USER_PDCIDPHO_ADDS")
    private String userPdcidphoAdds;

    @ApiModelProperty(value = "执业医生证照片审核")
    @TableField("USER_PDCIDCPHO_STATUS")
    private String userPdcidcphoStatus;

    @ApiModelProperty(value = "首次登录 0-首次登录 1-非首次登录")
    @TableField("USER_LOGIN_STA")
    private String userLoginSta;

}
