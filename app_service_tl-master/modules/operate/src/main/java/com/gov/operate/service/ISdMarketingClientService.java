package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.GetGsmTaskListDTO2;
import com.gov.operate.dto.GsmTaskListVO2;
import com.gov.operate.entity.SdMarketingClient;
import com.gov.mybatis.SuperService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface ISdMarketingClientService extends SuperService<SdMarketingClient> {

}
