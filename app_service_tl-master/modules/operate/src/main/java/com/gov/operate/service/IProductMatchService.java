package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.ProductMatchCheckManConfirmVO;
import com.gov.operate.dto.ProductMatchConfirmInData;
import com.gov.operate.dto.ProductMatchConfirmOtherInData;
import com.gov.operate.entity.ProductMatch;
import com.gov.mybatis.SuperService;
import com.gov.operate.feign.dto.MatchProductInData;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 导入商品匹配信息表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
public interface IProductMatchService extends SuperService<ProductMatch> {

    Map<String, Object> getDataStaticsByTelNum(String telNum);


    void confirm(ProductMatchConfirmInData inData);

    void confirmOthers(ProductMatchConfirmOtherInData inData);
}
