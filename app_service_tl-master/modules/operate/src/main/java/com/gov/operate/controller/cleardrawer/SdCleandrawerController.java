package com.gov.operate.controller.cleardrawer;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.service.ISdCleandrawerHService;
import com.gov.operate.service.ISdIndrawerHService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 清斗、装斗相关的操作
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@RestController
@RequestMapping("/cleandrawer/cleandrawer")
public class SdCleandrawerController {

    @Resource
    private ISdCleandrawerHService cleandrawerhService;

    @Resource
    private ISdIndrawerHService indrawerhService;

    @Log("验收单获取")
    @ApiOperation(value = "验收单获取")
    @GetMapping("getExamine")
    public Result getExamineInfo() {
        return ResultUtil.success(cleandrawerhService.getExamineInfo());
    }

    @Log("验收单高亮显示信息")
    @ApiOperation(value = "验收单高亮显示信息")
    @PostMapping("getHighlight")
    public Result getHighlightInfo(@Valid @RequestBody GetHighlightVO vo) {
        return cleandrawerhService.getHighlightInfo(vo);
    }

    @Log("清斗商品查询")
    @ApiOperation(value = "清斗商品查询")
    @PostMapping("getProduct")
    public Result getProductList(@Valid @RequestBody GetProductVO vo) {
        return ResultUtil.success(cleandrawerhService.getCleanDrawderProductList(vo));
    }

    @Log("清斗批号有效期选择")
    @ApiOperation(value = "清斗批号有效期选择")
    @PostMapping("getBatchNo")
    public Result getBatchNoValidList(@Valid @RequestBody GetBatchNoValidListVO vo) {
        return ResultUtil.success(cleandrawerhService.getBatchNoValidList(vo));
    }

    @Log("清斗人员")
    @ApiOperation(value = "清斗人员")
    @PostMapping("getPersonnel")
    public Result getCleanDrawerPersonnel(@Valid @RequestBody GetCleanDrawerPersonnelVO vo) {
        return ResultUtil.success(cleandrawerhService.getCleanDrawerPersonnel(vo));
    }

    @Log("清斗保存")
    @ApiOperation(value = "清斗保存")
    @PostMapping("saveCleanDrawer")
    public Result addCleanDrawer(@Valid @RequestBody AddCleanDrawerVO vo) {
        return ResultUtil.success(cleandrawerhService.addCleanDrawer(vo));
    }

    @Log("清斗修改")
    @ApiOperation(value = "清斗修改")
    @PostMapping("updateCleanDrawer")
    public Result updateCleanDrawer(@Valid @RequestBody UpdateCleanDrawerVO vo) {
        return cleandrawerhService.updateCleanDrawer(vo);
    }

    @Log("清斗审核")
    @ApiOperation(value = "清斗审核")
    @PostMapping("checkCleanDrawer")
    public Result checkCleanDrawer(@Valid @RequestBody CheckCleanDrawerVO vo) {
        return cleandrawerhService.checkCleanDrawer(vo);
    }

    @Log("门店清斗记录查询")
    @ApiOperation(value = "门店清斗记录查询")
    @PostMapping("listCleanDrawer")
    public Result listCleanDrawer(@RequestBody @Valid ListCleanDrawerVO vo) {
        return ResultUtil.success(cleandrawerhService.listCleanDrawer(vo));
    }

    @Log("清斗明细")
    @ApiOperation(value = "清斗明细")
    @GetMapping("getCleanDrawer")
    public Result getCleanDrawer(@RequestParam("gschVoucherId") String gschVoucherId) {
        return ResultUtil.success(cleandrawerhService.getCleanDrawer(gschVoucherId));
    }

    @Log("调取清斗单")
    @ApiOperation(value = "调取清斗单")
    @GetMapping("getCleanDrawerInfo")
    public Result getCleanDrawerInfo() {
        return indrawerhService.getCleanDrawerInfo();
    }

    @Log("清斗单高亮显示信息")
    @ApiOperation(value = "清斗单高亮显示信息")
    @PostMapping("getCleanHighlight")
    public Result getCleanHighlightInfo(@Valid @RequestBody GetCleanHighlightVO vo) {
        return indrawerhService.getCleanHighlightInfo(vo);
    }

    @Log("装斗保存")
    @ApiOperation(value = "装斗保存")
    @PostMapping("saveInDrawer")
    public Result addInDrawer(@Valid @RequestBody AddInDrawerVO vo) {
        return ResultUtil.success(indrawerhService.addInDrawer(vo));
    }

    @Log("装斗修改")
    @ApiOperation(value = "装斗修改")
    @PostMapping("updateInDrawer")
    public Result updateInDrawer(@Valid @RequestBody UpdateInDrawerVO vo) {
        return indrawerhService.updateInDrawer(vo);
    }

    @Log("装斗审核")
    @ApiOperation(value = "装斗审核")
    @PostMapping("checkInDrawer")
    public Result checkInDrawer(@Valid @RequestBody CheckInDrawerVO vo) {
        return indrawerhService.checkInDrawer(vo);
    }

    @Log("门店装斗记录查询")
    @ApiOperation(value = "门店装斗记录查询")
    @PostMapping("listInDrawer")
    public Result listInDrawer(@RequestBody @Valid ListInDrawerVO vo) {
        return ResultUtil.success(indrawerhService.listInDrawer(vo));
    }

    @Log("装斗明细")
    @ApiOperation(value = "装斗明细")
    @GetMapping("getInDrawer")
    public Result getInDrawer(@RequestParam("gsihVoucherId") String gsihVoucherId) {
        return ResultUtil.success(indrawerhService.getInDrawer(gsihVoucherId));
    }

    @Log("3月未清斗查询")
    @PostMapping("getDrawerList")
    public Result getDrawerList(){
        return ResultUtil.success(indrawerhService.getDrawerList());
    }

    @Log("清斗装斗控制维护开关查询")
    @ApiOperation(value = "清斗装斗控制维护开关查询")
    @PostMapping("getCheckShop")
    public Result getCheckShop() {
        return ResultUtil.success(cleandrawerhService.getCheckShop());
    }
}

