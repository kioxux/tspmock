package com.gov.operate.service;

import com.gov.operate.dto.zz.ZzInData;
import com.gov.operate.dto.zz.ZzOutData;
import com.gov.operate.dto.zz.ZzooOutData;
import com.gov.operate.entity.ZzData;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-23
 */
public interface IZzDataService extends SuperService<ZzData> {
    List<ZzooOutData> getZzooList();

    List<ZzOutData> getZzList(ZzInData inData);
}
