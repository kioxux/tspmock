package com.gov.operate.service;

import com.gov.operate.entity.Resource;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-17
 */
public interface IResourceService extends SuperService<Resource> {

}
