package com.gov.operate.dto.customer.order;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021-06-22 10:10
 */
@Data
public class GetCustomerOrderReplyListVO {

    @NotNull(message = "工单id不能为空")
    private Long id;

//    @NotNull(message = "指定回复id不能为空")
    private Long workOrderReplyId;

    @NotNull(message = "页大小不能为空")
    private Integer pageSize;
}
