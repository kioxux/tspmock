package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_OBJ")
@ApiModel(value="SalaryObj对象", description="")
public class SalaryObj extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSO_ID", type = IdType.AUTO)
    private Integer gsoId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSO_GSP_ID")
    private Integer gsoGspId;

    @ApiModelProperty(value = "适用范围")
    @TableField("GSO_APPLY_OBJ")
    private String gsoApplyObj;

    @ApiModelProperty(value = "适用范围")
    @TableField("GSO_APPLY_OBJ_NAME")
    private String gsoApplyObjName;


}
