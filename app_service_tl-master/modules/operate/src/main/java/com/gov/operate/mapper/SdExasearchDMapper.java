package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.operate.entity.SdExasearchD;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface SdExasearchDMapper extends BaseMapper<SdExasearchD> {

    List<SdExasearchD> selectMarketingCardList(@Param("ew") QueryWrapper<SdExasearchD> searchQuery);

    /**
     * 获取唯一记录
     */
    SdExasearchD getExaSearchD(@Param("client") String client,
                               @Param("gseSearchid") String gseSearchid,
                               @Param("memberCard") String memberCard);

    /**
     * 查询 指定活动，指定会员卡，短信发送成功的数量
     */
    int countSuccesMarket(@Param("client") String client,
                          @Param("memberCard") String memberCard,
                          @Param("storeCode") String storeCode,
                          @Param("marketId") String marketId,
                          @Param("smsId") String smsId);
}
