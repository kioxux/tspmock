package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.ssp.SspUserDTO;
import com.gov.operate.entity.SupplierSalesman;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity generator.domain.SupplierSalesman
 */
public interface SupplierSalesmanMapper extends BaseMapper<SupplierSalesman> {

    /**
     * 获取业务员名称和供应商信息列表
     *
     * @param client
     * @param mobile
     * @return
     */
    SspUserDTO selectSalesmanSupplierListByMobile(@Param("client") String client, @Param("mobile") String mobile);
}




