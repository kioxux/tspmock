package com.gov.operate.controller.popularize;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.charge.PayableVO;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IFicoInvoiceService;
import com.gov.operate.service.popularize.IPayingHeaderNewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@Api(tags = "用户付费")
@RestController
@RequestMapping("popularize/userPay")
public class UserPayController {
    @Resource
    private IPayingHeaderNewService iPayingHeaderNewService;

    @Resource
    private IFicoInvoiceService ficoInvoiceService;

    @Log("用户应付列表")
    @ApiOperation(value = "用户应付列表")
    @PostMapping("getPayableList")
    public Result getPayableList(@RequestJson(value = "spId", required = false) String spId,
                                 @RequestJson(value = "days", required = false) Integer days,
                                 @RequestJson(value = "client", required = false) String client,
                                 @RequestJson(value = "payFreq", required = false) Integer payFreq) {
        return iPayingHeaderNewService.getPayableList(spId, days, payFreq, client);
    }

    @Log("用户收费保存")
    @ApiOperation(value = "用户收费保存")
    @PostMapping("saveClientPay")
    public Result saveClientPay(@RequestBody PayableVO payableVO) {
        return iPayingHeaderNewService.saveClientPay(payableVO);
    }

    @Log("用户收费保存")
    @ApiOperation(value = "用户收费保存")
    @PostMapping("saveClientPayMaintain")
    public Result saveClientPayMaintain(@RequestBody PayableVO payableVO) {
        return iPayingHeaderNewService.saveClientPayMaintain(payableVO);
    }

    @Log("付款状态查询")
    @ApiOperation("付款状态查询")
    @GetMapping("getPayStatus")
    public Result getPayStatus(@RequestParam("ficoId") String ficoId, @RequestParam("client") String client) {
        return iPayingHeaderNewService.getPayStatus(ficoId, client);
    }

    @Log("支付回调事务")
    @ApiOperation("支付回调事务")
    @GetMapping("payCallback")
    public void payCallback() throws Exception {
        iPayingHeaderNewService.payCallback();
    }


    @Log("支付回调事务")
    @ApiOperation("支付回调事务")
    @GetMapping("payExpire")
    public void payExpire() throws Exception {
        iPayingHeaderNewService.payExpire();
    }

    @Log("开票定时任务")
    @ApiOperation("开票定时任务")
    @GetMapping("initInvoice")
    public void initInvoice() throws Exception {
        ficoInvoiceService.initInvoice();
    }

    @Log("提醒定时任务")
    @ApiOperation("提醒定时任务")
    @GetMapping("initRemind")
    public void initRemind() throws Exception {
        iPayingHeaderNewService.payReminder();
    }

    /**
     * 支付回调
     */
    @Log("扫码支付回调")
    @ApiOperation("扫码支付回调")
    @PostMapping("payResult")
    public void payResult(HttpServletRequest httpServletRequest) {
        iPayingHeaderNewService.payResult(httpServletRequest);
    }

    @Log("闭店")
    @ApiOperation("闭店")
    @PostMapping("closeStore")
    public Result closeStore(@RequestJson("client") String client,
                             @RequestJson("stoCode") String stoCode,
                             @RequestJson("closeDate") String closeDate) {
        return iPayingHeaderNewService.closeStore(client, stoCode, closeDate);
    }
}
