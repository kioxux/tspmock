package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromSeriesConds;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
public interface SdPromSeriesCondsMapper extends BaseMapper<SdPromSeriesConds> {


    void deleteList(@Param("list") List<SdPromSeriesConds> list);

    IPage<ProductResponseVo> selectProductList(Page<SdPromSeriesConds> page, @Param("product") ProductRequestVo productRequestVo);
}
