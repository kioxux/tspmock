package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SalaryTrialUserMapper;
import com.gov.operate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Slf4j
@Service
public class SalaryTrialUserServiceImpl extends ServiceImpl<SalaryTrialUserMapper, SalaryTrialUser> implements ISalaryTrialUserService {

    @Resource
    private CommonService commonService;
    @Resource
    private SalaryTrialUserMapper salaryTrialUserMapper;
    @Resource
    private ISalaryObjService salaryObjService;
    @Resource
    private ISalaryMonthReachService salaryMonthReachService;
    @Resource
    private ISalaryMonthSalaryGradeService salaryMonthSalaryGradeService;
    @Resource
    private ISalaryMonthWageJobsService salaryMonthWageJobsService;
    @Resource
    private ISalaryMonthMeritPayService salaryMonthMeritPayService;
    @Resource
    private ISalaryProCpsService salaryProCpsService;
    @Resource
    private ISalaryMonthAppraisalSalaryService salaryMonthAppraisalSalaryService;
    @Resource
    private ISalaryKpiService salaryKpiService;
    @Resource
    private ISalaryUserKpiService salaryUserKpiService;
    @Resource
    private ISalaryQuarterlyAwardsService salaryQuarterlyAwardsService;
    @Resource
    private ISalaryYearAwardsService salaryYearAwardsService;
    @Resource
    private ISalaryProgramService salaryProgramService;
    @Resource
    private IRosterService rosterService;

    /**
     * 返回当前方案试算结果
     */
    @Override
    public GetSalaryTrialDTO getSalaryTrial(Integer gspId) {
        if (ObjectUtils.isEmpty(gspId)) {
            throw new CustomResultException("主键不能为空");
        }
        List<SalaryTrialUser> trialUserList = this.list(new QueryWrapper<SalaryTrialUser>().eq("GSTU_GSP_ID", gspId));

        GetSalaryTrialDTO dto = new GetSalaryTrialDTO();
        BigDecimal gstuAmountTotal = new BigDecimal(0);
        BigDecimal gstuRealTotal = new BigDecimal(0);
        BigDecimal gstuDifferenceTotal = new BigDecimal(0);
        if (!CollectionUtils.isEmpty(trialUserList)) {
            for (SalaryTrialUser salaryTrialUser : trialUserList) {
                gstuAmountTotal = gstuAmountTotal.add(salaryTrialUser.getGstuAmount());
                gstuRealTotal = gstuRealTotal.add(salaryTrialUser.getGstuReal());
                gstuDifferenceTotal = gstuDifferenceTotal.add(salaryTrialUser.getGstuDifference());
            }
        }
        // 员工试算金额总值
        dto.setGstuAmountTotal(gstuAmountTotal);
        // 上月实发总额
        dto.setGstuRealTotal(gstuRealTotal);
        // 差异总额
        dto.setGstuDifferenceTotal(gstuDifferenceTotal);
        // 员工薪资明细
        dto.setSalaryTrialUserList(trialUserList);
        SalaryProgram program = salaryProgramService.getOne(new QueryWrapper<SalaryProgram>().select("GSP_TRIAL_REPORT", "GSP_TRIAL_SALARY").eq("GSP_ID", gspId));
        if (!ObjectUtils.isEmpty(program)) {
            // 报表json
            dto.setGspTrialReport(program.getGspTrialReport());
            // 明细json
            dto.setGspTrialSalary(program.getGspTrialSalary());
        }
        return dto;
    }

    /**
     * 方案试算
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void querySalaryTrial(QuerySalaryTrialVO vo) {
        Integer gspId = vo.getGspId();
        if (ObjectUtils.isEmpty(gspId)) {
            throw new CustomResultException("主键不能为空");
        }
        SalaryProgram salaryProgram = salaryProgramService.getById(gspId);

        // 营业额权重
        BigDecimal gspTurnover = salaryProgram.getGspTurnover();
        // 毛利额权重
        BigDecimal gspGrossProfit = salaryProgram.getGspGrossProfit();
        // 第三步和综合达成系数挂勾(基本工资)
        Integer gspSalaryReach = salaryProgram.getGspSalaryReach();
        // 第四步和综合达成率挂勾(岗位工资)
        Integer gspPostReach = salaryProgram.getGspPostReach();
        // 第五步和综合达成系数挂勾(绩效工资)
        Integer gspSalesReach = salaryProgram.getGspSalesReach();
        // 第六步和综合达成系数挂勾(考评工资)
        Integer gspAppraisalReach = salaryProgram.getGspAppraisalSalary();


        // 第五步1：销售额提成，2：毛利额提成
        Integer gspSalesCommission = salaryProgram.getGspSalesCommission();
        // 第五步1：商品毛利率，2：销售毛利率
        Integer gspGrossMargin = salaryProgram.getGspGrossMargin();
        // 第五步 负毛利率是否提成
        Integer gspNegativeGrossMargin = salaryProgram.getGspNegativeGrossMargin();
        // 第五步 单品是否参与销售提成
        Integer gspProReach = salaryProgram.getGspProReach();

        // 综合达成率
        List<SalaryMonthReach> reachList = salaryMonthReachService.getReachDecimalList(gspId);
        // 工资级别列表
        List<SalaryMonthSalaryGrade> salaryGradeList = salaryMonthSalaryGradeService.getSalaryGradeList(gspId);
        // 岗位薪资列表
        List<SalaryMonthWageJobs> wageJobsNoGroupingList = salaryMonthWageJobsService.getWageJobsNoGroupingList(gspId);
        // 绩效列表
        List<SalaryMonthMeritPay> meritPayNoGroupingList = salaryMonthMeritPayService.getMeritPayNoGroupingList(gspId);
        // 单品提成列表
        List<SalaryProCps> proCpsList = salaryProCpsService.getProCpsList(gspId);
        // 月考评工资列表
        List<SalaryMonthAppraisalSalary> appraisalSalaryList = salaryMonthAppraisalSalaryService.getAppraisalSalaryList(gspId);
        // 个人系数列表
        List<SalaryKpi> kpiList = salaryKpiService.getKpiList(gspId);
        // 员工KPI
        List<SalaryUserKpi> userKpiList = salaryUserKpiService.getUserKpiList();


        TokenUser user = commonService.getLoginInfo();
        // 门店/员工两级列表
        List<UserDTO> storeUserList = this.getUserList(gspId);
        // 门店list
        List<String> storeList = storeUserList.stream().filter(userDTO -> StringUtils.isNotEmpty(userDTO.getStoCode())).map(UserDTO::getStoCode).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(storeList)) {
            throw new CustomResultException("该方案的适用范围下没有员工，请选择合适适用");
        }
        // 员工花名册
        List<Roster> rosterList = rosterService.list(new QueryWrapper<Roster>().eq("CLENT", user.getClient()).in("STO_CODE", storeList));
        if (CollectionUtils.isEmpty(rosterList)) {
            throw new CustomResultException("花名册未维护");
        }
        // 上月开始和结束日期
        List<String> beginEndDate = DateUtils.getBeginEndDate(LocalDate.now().minusMonths(1));

        // 实发基本工资总和
        BigDecimal slaAmtActualTotal = new BigDecimal(0);
        // 实发岗位工资总和
        BigDecimal posSlaActualTotal = new BigDecimal(0);
        // 实发绩效工资总和
        BigDecimal commissionTotal = new BigDecimal(0);
        // 实发考评工资总和
        BigDecimal evaluationTotal = new BigDecimal(0);
        // 所有门店销售额总和
        BigDecimal allStoreSaleTotal = new BigDecimal(0);
        // 所有门店毛利额总和
        BigDecimal allStoreProfitTotal = new BigDecimal(0);
        // 员工集合
        List<SalaryTrialUser> salaryTrialUserList = new ArrayList<>();

        for (UserDTO store : storeUserList) {
            // 门店编码
            String stoCode = store.getStoCode();
            // 门店名称
            String stoName = store.getStoName();
            // 门店销售数据
            List<StoreSaleD> storeSaleDetails = salaryTrialUserMapper.getStoreSaleDetails(user.getClient(), stoCode, beginEndDate.get(0), beginEndDate.get(1), gspNegativeGrossMargin);
            // 实际达成营业额
            BigDecimal turnoverTotal = storeSaleDetails.stream().map(StoreSaleD::getGssdAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            allStoreSaleTotal = allStoreSaleTotal.add(turnoverTotal).setScale(Constants.SCALE, BigDecimal.ROUND_HALF_UP);
            // 实际毛利额
            BigDecimal grossprofitTotal = storeSaleDetails.stream().map(StoreSaleD::getGssdGrossAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            allStoreProfitTotal = allStoreProfitTotal.add(grossprofitTotal).setScale(Constants.SCALE, BigDecimal.ROUND_HALF_UP);

//            // 营业额计划 // TODO
//            BigDecimal turnoverInit = new BigDecimal(100);
//            // 毛利额计划 // TODO
//            BigDecimal grossprofitInit = new BigDecimal(100);

            // 营业额达成率 // TODO
            BigDecimal turnoverInit = new BigDecimal(0.95);// turnoverTotal.divide(turnoverInit, Constants.SCALE, BigDecimal.ROUND_HALF_UP);
            // 毛利额达成率 // TODO
            BigDecimal grossprofitInit = new BigDecimal(0.98); // grossprofitTotal.divide(grossprofitInit, Constants.SCALE, BigDecimal.ROUND_HALF_UP);

            // 综合达成率=营业额达成率×营业额权重+毛利额达成率×毛利额权重
            BigDecimal achieveRateTurnover = turnoverInit.multiply(gspTurnover).setScale(Constants.SCALE, BigDecimal.ROUND_HALF_UP);
            BigDecimal achieveRateProfit = grossprofitInit.multiply(gspGrossProfit).setScale(Constants.SCALE, BigDecimal.ROUND_HALF_UP);

            BigDecimal achieveRate = achieveRateTurnover.add(achieveRateProfit);
            // 综合达成 系数
            List<SalaryMonthReach> achieveCoefficeList = reachList.stream().filter(reach -> {
                boolean b1 = reach.getGsmrAchievRateMin().compareTo(achieveRate) <= 0;
                boolean b2 = reach.getGsmrAchievRateMax().compareTo(achieveRate) >= 0;
                return b1 && b2;
            }).collect(Collectors.toList());
            log.info("综合达达系数计算：{},{}", JSONObject.toJSONString(reachList), achieveRate);
            if (CollectionUtils.isEmpty(achieveCoefficeList)) {
                log.info("门店{},综合达成率:{}", stoName, achieveRate);
                throw new CustomResultException(MessageFormat.format("该方案综合达成率等级设置不完整；门店{0},综合达成率:{1}", stoName, achieveRate.toString()));
            }
            if (achieveCoefficeList.size() > 1) {
                throw new CustomResultException("该方案综合达成率等级设置有重复");
            }
            BigDecimal achieveCoeffice = achieveCoefficeList.get(0).getGsmrAchievCoeffi();

            // 日销售额 = sum(销售额)/(Count(distinct 销售日期))
            BigDecimal turnoverPerDay = new BigDecimal(0);
            long countDay = storeSaleDetails.stream().map(StoreSaleD::getGssdDate).distinct().count();
            if (0 != countDay) {
                turnoverPerDay = turnoverTotal.divide(new BigDecimal(countDay), Constants.SCALE, BigDecimal.ROUND_HALF_UP);
            }
            // lambda内部 final类型
            final BigDecimal turnoverPerDayFinal = turnoverPerDay;

            for (UserData staff : store.getUserDataList()) {
                String userId = staff.getUserId();
                String userName = staff.getUserNam();

                List<Roster> rosterListTemp = rosterList.stream().filter(item -> item.getStoCode().equals(stoCode) && item.getUserId().equals(userId)).collect(Collectors.toList());
                log.info("花名册比较：{},{},{}", JSONObject.toJSONString(rosterList), userId, userName);
                if (CollectionUtils.isEmpty(rosterListTemp)) {
                    log.info("用户[{0}]在花名册中未维护", userName);
                    throw new CustomResultException(MessageFormat.format("用户[{0}]在花名册中未维护", userName));
                }
                Roster roster = rosterListTemp.get(0);
                // (1)月基本工资=员工（GAIA_ROSTER）.基本工资
                // 花名册中 员工基本工资
                BigDecimal slaAmt = roster.getSlaAmt();
                if (ObjectUtils.isEmpty(slaAmt) || slaAmt.compareTo(BigDecimal.ZERO) == 0) {
                    // 基本工资级别
                    String slaClNo = roster.getSlaClNo();
                    // 工资级别
                    List<SalaryMonthSalaryGrade> slaAmtList = salaryGradeList.stream().filter(salaryGrade -> salaryGrade.getGsmsgGrade().equals(slaClNo)).collect(Collectors.toList());
                    log.info("工次级别比较：{},{}", JSONObject.toJSONString(salaryGradeList), slaClNo);
                    if (CollectionUtils.isEmpty(slaAmtList)) {
                        log.info("花名册中基本工资维护不完整，请做工资级别与工资对照设置,{}", slaClNo);
                        throw new CustomResultException("花名册中基本工资维护不完整，请做工资级别与工资对照设置");
                    }
                    if (slaAmtList.size() > 1) {
                        log.info("工资级别有误,请维护。同一方案中工资级别不能重复,{}", slaClNo);
                        throw new CustomResultException("工资级别有误,请维护。同一方案中工资级别不能重复");
                    }
                    slaAmt = new BigDecimal(slaAmtList.get(0).getGsmsgBasicWage());
                    if (ObjectUtils.isEmpty(slaAmt) || slaAmt.compareTo(BigDecimal.ZERO) == 0) {
                        log.info("薪资方案月工资级别[{}]基本工资未维护", slaClNo);
                        throw new CustomResultException(MessageFormat.format("薪资方案月工资级别[{0}]基本工资未维护", slaClNo));
                    }
                }
                // 实发基本工资 =  月基本工资*综合达成系数
                BigDecimal slaAmtActual = slaAmt;
                if (1 == gspSalaryReach) {
                    slaAmtActual = slaAmt.multiply(achieveCoeffice).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                }
                slaAmtActualTotal = slaAmtActualTotal.add(slaAmtActual);

                // (2) 岗位工资=员工（GAIA_ROSTER）.岗位工资
                // 花名册中 岗位工资
                BigDecimal posSla = roster.getPosSla();
                if (ObjectUtils.isEmpty(posSla) || posSla.compareTo(BigDecimal.ZERO) == 0) {
                    // 岗位
                    String posNo = roster.getPosNo();
                    // 工资级别
                    List<SalaryMonthWageJobs> posSlaList = wageJobsNoGroupingList.stream().filter(wageJobs -> wageJobs.getGsmwjJob().equals(posNo)).collect(Collectors.toList());
                    log.info("岗位工资级别：{},{}", JSONObject.toJSONString(wageJobsNoGroupingList), posNo);
                    if (CollectionUtils.isEmpty(posSlaList)) {
                        throw new CustomResultException(MessageFormat.format("花名册中[{0}]岗位工资维护不完整，请做岗位与岗位工资对照设定", userName));
                    }

                    posSlaList = posSlaList.stream().filter(wageJobs -> {
                        boolean b2 = wageJobs.getGsmwjSalesMin().compareTo(turnoverPerDayFinal) <= 0;
                        boolean b3 = wageJobs.getGsmwjSalesMax().compareTo(turnoverPerDayFinal) >= 0;
                        return b2 && b3;
                    }).collect(Collectors.toList());
                    log.info("岗位销售额：{},{}", JSONObject.toJSONString(posSlaList), turnoverPerDayFinal.toString());
                    if (CollectionUtils.isEmpty(posSlaList)) {
                        log.info("岗位[{}]日销售额等级不完整，请做日销售额与岗位工资对照设定", posNo);
                        throw new CustomResultException(MessageFormat.format("岗位[{0}]日销售额等级不完整，请做日销售额与岗位工资对照设定", posNo));
                    }

                    if (posSlaList.size() > 1) {
                        log.info("岗位[{}]日销售额等级多个区间重复。", posNo);
                        throw new CustomResultException(MessageFormat.format("岗位[{0}]日销售额等级多个区间重复。", posNo));
                    }
                    SalaryMonthWageJobs salaryMonthWageJobs = posSlaList.get(0);
                    posSla = salaryMonthWageJobs.getGsmwjWage();
                    if (ObjectUtils.isEmpty(posSla) || posSla.compareTo(BigDecimal.ZERO) == 0) {
                        log.info("岗位[{}]日销售额[{}~{}]工资未维护",
                                salaryMonthWageJobs.getGsmwjJobName(), salaryMonthWageJobs.getGsmwjSalesMin(), salaryMonthWageJobs.getGsmwjSalesMax());
                        throw new CustomResultException(MessageFormat.format("岗位[{0}]日销售额[{1}~{2}]工资未维护",
                                salaryMonthWageJobs.getGsmwjJobName(), salaryMonthWageJobs.getGsmwjSalesMin(), salaryMonthWageJobs.getGsmwjSalesMax()));
                    }
                }
                // 实发基本工资 =  月基本工资*综合达成系数
                BigDecimal posSlaActual = posSla;
                if (1 == gspPostReach) {
                    posSlaActual = posSla.multiply(achieveCoeffice).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                }
                posSlaActualTotal = posSlaActualTotal.add(posSlaActual);

                // (3)月绩效工资
                BigDecimal commission = new BigDecimal(0);
                // 指定员工销售记录
                List<StoreSaleD> userStoreSaleDetails = storeSaleDetails.stream().filter(saleD -> saleD.getGssdSalerId().equals(userId)).collect(Collectors.toList());
//                if (!CollectionUtils.isEmpty(userStoreSaleDetails)) {
                // (3.1) 销售总提成
                // 员工个人销售额
                BigDecimal userTotalAmt = userStoreSaleDetails.stream().map(StoreSaleD::getGssdAmt).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                // 员工销售商品毛利额
                BigDecimal userTotalGrossAmt = userStoreSaleDetails.stream().map(StoreSaleD::getGssdGrossAmt).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);

                // 商品毛利
                BigDecimal userTotalGrossProfit = userStoreSaleDetails.stream().map(saleD ->
                        saleD.getGssdQty().multiply(saleD.getGssdPrc1()).subtract(saleD.getGssdCostAmt())
                ).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 商品毛利应收总价
                BigDecimal userTotalReceivablePrice = userStoreSaleDetails.stream().map(saleD ->
                        saleD.getGssdQty().multiply(saleD.getGssdPrc1())
                ).reduce(BigDecimal.ZERO, BigDecimal::add);

                // 商品毛利率 = SUM(((销售数量*应收价）-成本额）) / SUM((销售数量*应收价）)
                BigDecimal userGrossProfitRate = new BigDecimal(0);
                if (userTotalReceivablePrice.compareTo(BigDecimal.ZERO) != 0) {
                    userGrossProfitRate = userTotalGrossProfit.divide(userTotalReceivablePrice, Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                }

                // 销售毛利率 = SUM(毛利额) / SUM(实收金额)
                BigDecimal userAmtRate = new BigDecimal(0);
                if (userTotalAmt.compareTo(BigDecimal.ZERO) != 0) {
                    userAmtRate = userTotalGrossAmt.divide(userTotalAmt, Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                }
                // 横坐标
                BigDecimal transverse = null;
                // 纵坐标
                BigDecimal longitudinal = null;
                if (1 == gspSalesCommission) {
                    // 横坐标为 销售额
                    transverse = userTotalAmt;
                } else {
                    // 横坐标为 毛利额
                    transverse = userTotalGrossAmt;
                }
                if (1 == gspGrossMargin) {
                    // 纵坐标为 商品毛利率
                    longitudinal = userGrossProfitRate;
                } else {
                    // 纵坐标为 销售毛利率
                    longitudinal = userAmtRate;
                }
                // 定位 提成百分比
                final BigDecimal finalTransverse = transverse;
                List<SalaryMonthMeritPay> meritPayList = meritPayNoGroupingList.stream().filter(meritPay -> {
                    boolean b2 = meritPay.getGsmmpSalesMin().compareTo(finalTransverse) <= 0;
                    boolean b3 = meritPay.getGsmmpSalesMax().compareTo(finalTransverse) >= 0;
                    return b2 && b3;
                }).collect(Collectors.toList());
                log.info("销售额等级：{},{}", JSONObject.toJSONString(meritPayNoGroupingList), finalTransverse.toString());
                if (CollectionUtils.isEmpty(meritPayList)) {
                    log.info("销售额等级列表不包含用户[{}]的" + (1 == gspSalesCommission ? "销售额" : "毛利额") + "[{}],请重新设定销售额/毛利额", userName, finalTransverse.toString());
                    throw new CustomResultException(MessageFormat.format("销售额等级列表不包含用户[{0}]的" + (1 == gspSalesCommission ? "销售额" : "毛利额") + "[{1}],请重新设定销售额/毛利额", userName, finalTransverse.toString()));
                }
                final BigDecimal finalLongitudinal = longitudinal;
                meritPayList = meritPayList.stream().filter(meritPay -> {
                    boolean b2 = meritPay.getGsmmpGrossProfitMin().compareTo(finalLongitudinal) <= 0;
                    boolean b3 = meritPay.getGsmmpGrossProfitMax().compareTo(finalLongitudinal) >= 0;
                    return b2 && b3;
                }).collect(Collectors.toList());
                log.info("利率等级：{},{}", JSONObject.toJSONString(meritPayList), finalLongitudinal.toString());
                if (CollectionUtils.isEmpty(meritPayList)) {
                    log.info("利率等级列表不包含用户[{}]的利率[{}],请重新设定商品毛利率/销售毛利率", userName, finalLongitudinal.toString());
                    throw new CustomResultException(MessageFormat.format("利率等级列表不包含用户[{0}]的利率[{1}],请重新设定商品毛利率/销售毛利率", userName, finalLongitudinal.toString()));
                }
                if (meritPayList.size() > 1) {
                    log.info("该方案利率/销售额区间 有重复部分,请重新设置");
                    throw new CustomResultException("该方案利率/销售额区间 有重复部分,请重新设置");
                }
                // 获取提成百分比
                BigDecimal gsmmpCoeffi = meritPayList.get(0).getGsmmpCoeffi();
                // 绩效提成金额
                if (1 == gspSalesReach) {
                    commission = transverse.multiply(gsmmpCoeffi).multiply(achieveCoeffice).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                } else {
                    commission = transverse.multiply(gsmmpCoeffi).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                }

                // (3.2) 单品提成 (单品参与提成,并且商品列表不为空)
                if (0 == gspProReach && !CollectionUtils.isEmpty(proCpsList)) {
                    // 计算每种商品的提成总额
                    BigDecimal singleCommissionAmtTotal = new BigDecimal(0);
                    for (SalaryProCps pro : proCpsList) {
                        // 商品编码
                        String gspcProCode = pro.getGspcProCode();
                        // 单品提成
                        BigDecimal gspcCommissionAmt = pro.getGspcCommissionAmt();
                        // gspcProCode 商品销售数量
                        BigDecimal proCount = userStoreSaleDetails
                                .stream()
                                .filter(uSaleD -> uSaleD.getGssdProId().equals(gspcProCode))
                                .map(StoreSaleD::getGssdQty)
                                .reduce(BigDecimal.ZERO, BigDecimal::add)
                                .setScale(0, BigDecimal.ROUND_HALF_UP);
                        // 每种商品的总提成金额累加
                        singleCommissionAmtTotal = singleCommissionAmtTotal.add(gspcCommissionAmt.multiply(proCount));
                    }
                    commission = commission.add(singleCommissionAmtTotal);
                }
                commissionTotal = commissionTotal.add(commission);
//                }

                // (4) 月考评工资
                // (4.1)考评工资基数
                // 花名册 考评工资基数
                BigDecimal evaBase = roster.getEvaBase();
                if (ObjectUtils.isEmpty(evaBase) || evaBase.compareTo(BigDecimal.ZERO) == 0) {
                    // 岗位
                    String posNo = roster.getPosNo();
                    // 工资级别
                    List<SalaryMonthAppraisalSalary> salaryList = appraisalSalaryList.stream().filter(appraisal -> appraisal.getGsmasJob().equals(posNo)).collect(Collectors.toList());
                    log.info("花名册工资：{},{}", JSONObject.toJSONString(appraisalSalaryList), salaryList);
                    if (CollectionUtils.isEmpty(salaryList)) {
                        log.info("花名册[{}]月考评工资维护不完整，请做岗位与工资对照设置", userName);
                        throw new CustomResultException(MessageFormat.format("花名册[{0}]月考评工资维护不完整，请做岗位与工资对照设置", userName));
                    }
                    if (salaryList.size() > 1) {
                        log.info("月考评工资数据有误。同一方案中月考评工资岗位不能重复");
                        throw new CustomResultException("月考评工资数据有误。同一方案中月考评工资岗位不能重复");
                    }
                    evaBase = salaryList.get(0).getGsmasWage();
                    if (ObjectUtils.isEmpty(slaAmt) || slaAmt.compareTo(BigDecimal.ZERO) == 0) {
                        throw new CustomResultException(MessageFormat.format("月考评工资岗位[{0}]月考评工资未维护", salaryList.get(0).getGsmasJobName()));
                    }
                }
                // (4.2)KPI评分
                // 花名册 KPI评分
                BigDecimal kpiTar = roster.getKpiTar();
                if (ObjectUtils.isEmpty(kpiTar) || kpiTar.compareTo(BigDecimal.ZERO) == 0) {
                    // KPI
                    List<SalaryUserKpi> salaryUserKpiList = userKpiList.stream().filter(userKpi -> userKpi.getGsukUserId().equals(userId)).collect(Collectors.toList());
                    log.info("kpi比较：{},{}", JSONObject.toJSONString(userKpiList), userId);
                    if (CollectionUtils.isEmpty(salaryUserKpiList)) {
                        log.info("薪资方案员工{}KPI未维护", userName);
                        throw new CustomResultException(MessageFormat.format("薪资方案员{0}工KPI未维护", userName));
                    }
                    if (salaryUserKpiList.size() > 1) {
                        log.info("薪资方案员工{}KPI数据有误", userName);
                        throw new CustomResultException(MessageFormat.format("薪资方案员工{0}KPI数据有误", userName));
                    }
                    kpiTar = salaryUserKpiList.get(0).getGsukKpi();
                }
                final BigDecimal finalKpiTar = kpiTar;
                // 个人系数
                List<SalaryKpi> kpiTarList = kpiList.stream().filter(kpi -> {
                    boolean b2 = kpi.getGskKpiMin().compareTo(finalKpiTar) <= 0;
                    boolean b3 = kpi.getGskKpiMax().compareTo(finalKpiTar) >= 0;
                    return b2 && b3;
                }).collect(Collectors.toList());
                log.info("月考评工资:{},{}", JSONObject.toJSONString(kpiList), finalKpiTar.toString());
                if (CollectionUtils.isEmpty(kpiTarList)) {
                    log.info("月考评工资 员工{}个人系数维护不完整，请做KPI与个人系数对照设置", userName);
                    throw new CustomResultException(MessageFormat.format("月考评工资 员工{0}系数维护不完整，请做KPI与个人系数对照设置", userName));
                }
                if (kpiTarList.size() > 1) {
                    throw new CustomResultException(MessageFormat.format("月考评工资 员工{0}系数维护不完整。KPI得分区间不能重叠", userName));
                }
                BigDecimal coeffi = kpiTarList.get(0).getGskCoeffi();
                if (ObjectUtils.isEmpty(coeffi) || coeffi.compareTo(BigDecimal.ZERO) == 0) {
                    log.info("月考评工资 员工{}系数 区间[{}~{}]外人系数不能为空", userName, kpiTarList.get(0).getGskKpiMin(), kpiTarList.get(0).getGskKpiMax());
                    throw new CustomResultException(MessageFormat.format("月考评工资 员工{0}系数 区间[{1}~{2}]外人系数不能为空", userName, kpiTarList.get(0).getGskKpiMin(), kpiTarList.get(0).getGskKpiMax()));
                }
                // 实际的考评工资
                BigDecimal evaluation = new BigDecimal(0);
                if (1 == gspAppraisalReach) {
                    evaluation = evaBase.multiply(coeffi).multiply(achieveCoeffice).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                } else {
                    evaluation = evaBase.multiply(coeffi).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                }
                evaluationTotal = evaluationTotal.add(evaluation);

                // 试算总金额
                BigDecimal amtTotal = slaAmtActual.add(posSlaActual).add(commission).add(evaluation).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
                BigDecimal amtTotalPreMonth = new BigDecimal(0);

                SalaryTrialUser trialUser = new SalaryTrialUser();
                salaryTrialUserList.add(trialUser);
                trialUser.setGstuGspId(gspId);
                trialUser.setGstuUserId(userId);
                trialUser.setGstuUserName(userName);
                trialUser.setGstuGroup(stoCode);
                trialUser.setGstuGroupName(stoName);
                trialUser.setGstuAmount(amtTotal);
                // TODO 上月实发
                trialUser.setGstuReal(amtTotalPreMonth);
                trialUser.setGstuDifference(amtTotal.subtract(amtTotalPreMonth));
                trialUser.setGstuSort(salaryTrialUserList.size());
            }
        }
        if (CollectionUtils.isEmpty(salaryTrialUserList)) {
            log.info("上月没有员工数据,无法进行试算");
            throw new CustomResultException("上月没有员工数据,无法进行试算");
        }
        // 员工列表试算结果保存
        this.remove(new QueryWrapper<SalaryTrialUser>().eq("GSTU_GSP_ID", gspId));
        this.saveBatch(salaryTrialUserList);
        // 饼图
        BigDecimal total = slaAmtActualTotal.add(posSlaActualTotal).add(commissionTotal).add(evaluationTotal).setScale(Constants.SCALE_TWO, BigDecimal.ROUND_HALF_UP);
        TrialDTO.TrialReport trialReport = new TrialDTO.TrialReport();
        trialReport.setTotal(total);
        trialReport.setList(Arrays.asList(
                new TrialDTO.TrialReportItem("基本工资", slaAmtActualTotal, slaAmtActualTotal.divide(total, Constants.SCALE, BigDecimal.ROUND_HALF_UP).multiply(Constants.ONE_HUNDRED)),
                new TrialDTO.TrialReportItem("岗位工资", posSlaActualTotal, posSlaActualTotal.divide(total, Constants.SCALE, BigDecimal.ROUND_HALF_UP).multiply(Constants.ONE_HUNDRED)),
                new TrialDTO.TrialReportItem("绩效工资", commissionTotal, commissionTotal.divide(total, Constants.SCALE, BigDecimal.ROUND_HALF_UP).multiply(Constants.ONE_HUNDRED)),
                new TrialDTO.TrialReportItem("考评工资", evaluationTotal, evaluationTotal.divide(total, Constants.SCALE, BigDecimal.ROUND_HALF_UP).multiply(Constants.ONE_HUNDRED))
        ));
        String trialReportJSON = JSON.toJSONString(trialReport);
        //表格
        List<TrialDTO.TrialSalary> trialSalaries = Arrays.asList(
                new TrialDTO.TrialSalary(1, "销售额", allStoreSaleTotal, total,
                        allStoreSaleTotal == null || allStoreSaleTotal.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.valueOf(100) : total.divide(allStoreSaleTotal, Constants.SCALE, BigDecimal.ROUND_HALF_UP).multiply(Constants.ONE_HUNDRED)),
                new TrialDTO.TrialSalary(2, "毛利额", allStoreProfitTotal, total,
                        allStoreProfitTotal == null || allStoreProfitTotal.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.valueOf(100) : total.divide(allStoreProfitTotal, Constants.SCALE, BigDecimal.ROUND_HALF_UP).multiply(Constants.ONE_HUNDRED))
        );
        String trialSalaryJSON = JSON.toJSONString(trialSalaries);

        // 报表JSON 保存
        SalaryProgram program = new SalaryProgram();
        program.setGspId(gspId);
        program.setGspTrialReport(trialReportJSON);
        program.setGspTrialSalary(trialSalaryJSON);
        salaryProgramService.updateById(program);

    }

    private List<UserDTO> getUserList(Integer gspId) {
        TokenUser user = commonService.getLoginInfo();
        List<SalaryObj> list = salaryObjService.list(new QueryWrapper<SalaryObj>().select("GSO_APPLY_OBJ").eq("GSO_GSP_ID", gspId));
        List<String> storeList = list.stream().map(SalaryObj::getGsoApplyObj).distinct().collect(Collectors.toList());
        List<UserDTO> userList = salaryTrialUserMapper.getUserList(user.getClient(), storeList);
        return userList;
    }

}
