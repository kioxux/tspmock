package com.gov.operate.dto;

import com.gov.operate.entity.SalaryTrialUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetSalaryTrialDTO {

    /**
     * 报表json
     */
    private String gspTrialReport;

    /**
     * 明细json
     */
    private String gspTrialSalary;

    /**
     * 员工试算列表
     */
    private List<SalaryTrialUser> salaryTrialUserList;

    /**
     * 员工试算金额总值
     */
    private BigDecimal gstuAmountTotal;

    /**
     * 上月实发总额
     */
    private BigDecimal gstuRealTotal;

    /**
     * 差异总额
     */
    private BigDecimal gstuDifferenceTotal;

}
