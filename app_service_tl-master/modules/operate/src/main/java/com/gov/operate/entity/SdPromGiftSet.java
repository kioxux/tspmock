package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_GIFT_SET")
@ApiModel(value = "SdPromGiftSet对象", description = "")
public class SdPromGiftSet extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPGS_VOUCHER_ID")
    private String gspgsVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPGS_SERIAL")
    private String gspgsSerial;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPGS_SERIES_PRO_ID")
    private String gspgsSeriesProId;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPGS_REACH_QTY1")
    private String gspgsReachQty1;

    @ApiModelProperty(value = "达到金额1")
    @TableField("GSPGS_REACH_AMT1")
    private BigDecimal gspgsReachAmt1;

    @ApiModelProperty(value = "赠送数量1")
    @TableField("GSPGS_RESULT_QTY1")
    private String gspgsResultQty1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPGS_REACH_QTY2")
    private String gspgsReachQty2;

    @ApiModelProperty(value = "达到金额2")
    @TableField("GSPGS_REACH_AMT2")
    private BigDecimal gspgsReachAmt2;

    @ApiModelProperty(value = "赠送数量2")
    @TableField("GSPGS_RESULT_QTY2")
    private String gspgsResultQty2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPGS_REACH_QTY3")
    private String gspgsReachQty3;

    @ApiModelProperty(value = "达到金额3")
    @TableField("GSPGS_REACH_AMT3")
    private BigDecimal gspgsReachAmt3;

    @ApiModelProperty(value = "赠送数量3")
    @TableField("GSPGS_RESULT_QTY3")
    private String gspgsResultQty3;

    @ApiModelProperty(value = "赠品金额1")
    @TableField("GSPGS_RESULT_PRC1")
    private BigDecimal gspgsResultPrc1;

    @ApiModelProperty(value = "赠品折扣1")
    @TableField("GSPGS_RESULT_REBATE1")
    private BigDecimal gspgsResultRebate1;

    @ApiModelProperty(value = "赠品金额2")
    @TableField("GSPGS_RESULT_PRC2")
    private BigDecimal gspgsResultPrc2;

    @ApiModelProperty(value = "赠品折扣2")
    @TableField("GSPGS_RESULT_REBATE2")
    private BigDecimal gspgsResultRebate2;

    @ApiModelProperty(value = "赠品金额3")
    @TableField("GSPGS_RESULT_PRC3")
    private BigDecimal gspgsResultPrc3;

    @ApiModelProperty(value = "赠品折扣3")
    @TableField("GSPGS_RESULT_REBATE3")
    private BigDecimal gspgsResultRebate3;
}
