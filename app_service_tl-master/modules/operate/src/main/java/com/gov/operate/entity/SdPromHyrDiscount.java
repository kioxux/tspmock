package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 会员类促销会员日折扣设置表
 * </p>
 *
 * @author sy
 * @since 2021-02-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_HYR_DISCOUNT")
@ApiModel(value="SdPromHyrDiscount对象", description="会员类促销会员日折扣设置表")
public class SdPromHyrDiscount extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPP_VOUCHER_ID")
    private String gsppVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPP_SERIAL")
    private String gsppSerial;

    @ApiModelProperty(value = "起始日期")
    @TableField("GSPP_BEGIN_DATE")
    private String gsppBeginDate;

    @ApiModelProperty(value = "结束日期")
    @TableField("GSPP_END_DATE")
    private String gsppEndDate;

    @ApiModelProperty(value = "起始时间")
    @TableField("GSPP_BEGIN_TIME")
    private String gsppBeginTime;

    @ApiModelProperty(value = "结束时间")
    @TableField("GSPP_END_TIME")
    private String gsppEndTime;

    @ApiModelProperty(value = "按日期")
    @TableField("GSPP_DATE_FREQUENCY")
    private String gsppDateFrequency;

    @ApiModelProperty(value = "按星期")
    @TableField("GSPP_TIME_FREQUENCY")
    private String gsppTimeFrequency;

    @ApiModelProperty(value = "折扣")
    @TableField("GSPP_REBATE")
    private String gsppRebate;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPP_INTE_FLAG")
    private String gsppInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPP_INTE_RATE")
    private String gsppInteRate;


}
