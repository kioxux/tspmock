package com.gov.operate.dto.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@ApiModel("消费信息DTO")
@Data
public class ConsumeInfoDTO {

    @ApiModelProperty("消费次数")
    private Integer count;

    @ApiModelProperty("消费金额")
    private BigDecimal sumOfMoney;

    @ApiModelProperty("消费订单DTO列表")
    private List<OrderInfoDTO> orderInfoDTOS;
}
