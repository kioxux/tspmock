package com.gov.operate.dto;

import com.gov.common.entity.dataImport.PromGiftResultProduct;
import com.gov.common.entity.dataImport.PromSeriesProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromGiftResultProductImportDto extends PromGiftResultProduct {
    private String proSelfCode;

    private String proCommonname;

    private String proName;

    private String proSpecs;

    private String proFactoryName;
}
