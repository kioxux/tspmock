package com.gov.operate.dto.customer.order;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021-06-21 16:07
 */
@Data
public class SaveOrderVO {

    @NotNull(message = "问题系统不能为空")
    private Integer gwoSystem;

    @NotNull(message = "工单类型不能为空")
    private Long gwoTypeId;

    @NotNull(message = "问题描述不能为空")
    private String gwoTitle;

    @NotNull(message = "紧急状态不能为空")
    private Integer gwoLevel;

    @NotNull(message = "备注说明不能为空")
    private String gwoRemarks;


    @NotBlank(message = "客户不能为空")
    private String gwoClient;

//    @NotBlank(message = "客户名不能为空")
    private String gwoClientName;

    @NotBlank(message = "用户ID不能为空")
    private String gwoUserId;

    @NotBlank(message = "用户名不能为空")
    private String gwoUserName;

    @NotBlank(message = "联系手机号不能为空")
    private String gwoUserTel;
}
