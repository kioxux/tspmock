package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;

@Data
public class GetProGroupListPageVO extends Pageable {

    /**
     * 商品组编码
     */
    private String gsyGroup;

    /**
     * 商品组名称
     */
    private String gsyGroupname;

    //    @NotBlank(message = "门店编码不能为空")
    private String gsyStore;
}
