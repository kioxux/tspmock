package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.auth.ClientAuth;
import com.gov.operate.entity.AuthorityAuths;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface IAuthorityAuthsService extends SuperService<AuthorityAuths> {

    Result getAuthList(Integer gaurRoleId);


    Result getFeeAuth();

    /**
     * 加盟商权限
     * @param client
     * @return
     */
    Result getAuthByClient(String client);

    /**
     * 保存加盟商权限
     * @param clientAuth
     */
    void saveClientAuth(ClientAuth clientAuth);
}
