package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddInvoiceVO {

    //    @NotBlank(message = "地点不能为空")
    private String site;

    //    @NotBlank(message = "供应商不能为空")
    private String supSelfCode;

    @NotBlank(message = "发票号码不能为空")
    private String invoiceNum;

    @NotBlank(message = "发票类型不能为空")
    private String invoiceType;

    @NotBlank(message = "发票日期不能为空")
    private String invoiceDate;

    @NotNull(message = "发票去税金额不能为空")
    private BigDecimal invoiceAmountExcludingTax;

    @NotNull(message = "发票税额不能为空")
    private BigDecimal invoiceTax;

    @NotNull(message = "发票总额总额不能空")
    private BigDecimal invoiceTotalInvoiceAmount;

    @NotBlank(message = "发票地点类型不能为空")
    private String invoiceSiteType;

    // @NotBlank(message = "供应商不能为空")
    private String invoiceSupName;

    /**
     * 折扣
     */
    private BigDecimal invoiceDiscount;

    /**
     * 业务员
     */
    private String invoiceSalesman;

    /**
     * 备注
     */
    private String invoiceRemark;

}
