package com.gov.operate.service;

import com.gov.operate.dto.compadmWms.CompadmWmsAddVO;
import com.gov.operate.dto.compadmWms.CompadmWmsDto;
import com.gov.operate.dto.compadmWms.CompadmWmsEditVO;
import com.gov.operate.dto.storeData.StoreDataCodeDTO;
import com.gov.operate.dto.storeData.StoreDataEditVO;
import com.gov.operate.entity.CompadmWms;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
public interface ICompadmWmsService extends SuperService<CompadmWms> {

//    StoreDataCodeDTO getMaxCode();

    CompadmWmsDto getStoreInfo(String compadmId);

    void addCompadmWms(CompadmWmsAddVO compadmWmsAddVO);

    void editCompadmWms(CompadmWmsEditVO compadmWmsEditVO);

    List<CompadmWmsDto> getCompadmWms();

}
