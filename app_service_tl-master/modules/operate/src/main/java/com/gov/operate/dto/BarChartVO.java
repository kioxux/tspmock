package com.gov.operate.dto;

import lombok.Data;

@Data
public class BarChartVO {

    /**
     * X轴
     */
    private String xaxis;

    /**
     * Y轴
     */
    private Object yaxis;
}
