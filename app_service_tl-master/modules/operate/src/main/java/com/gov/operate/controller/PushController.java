package com.gov.operate.controller;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.service.IMessageService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 推送 - 内部feign使用
 */
@RestController
@RequestMapping("push")
public class PushController {
    @Resource
    private IMessageService messageService;

    @Log("发送推送消息")
    @PostMapping("sendMessage")
    public Result sendMessage(@RequestBody MessageParams messageParams) {
        return messageService.sendMessage(messageParams);
    }

    @Log("发送推送消息")
    @PostMapping("sendMessageList")
    public Result sendMessageList(@RequestBody MessageParams messageParams) {
        return messageService.sendMessageList(messageParams);
    }

    @Log("pushPromotionReportMsg")
    @PostMapping("pushPromotionReportMsg")
    public Result pushMsg(@RequestBody MessageParams messageParams) {
        return messageService.pushPromotionReportMsg(messageParams);
    }

}
