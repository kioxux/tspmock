package com.gov.operate.service.impl;

import com.gov.operate.entity.FiInvoiceRegistration;
import com.gov.operate.mapper.FiInvoiceRegistrationMapper;
import com.gov.operate.service.IFiInvoiceRegistrationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class FiInvoiceRegistrationServiceImpl extends ServiceImpl<FiInvoiceRegistrationMapper, FiInvoiceRegistration> implements IFiInvoiceRegistrationService {

}
