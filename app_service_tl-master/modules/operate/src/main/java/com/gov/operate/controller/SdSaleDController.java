package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 门店销售明细表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-12-13
 */
@RestController
@RequestMapping("/operate/sd-sale-d")
public class SdSaleDController {

}

