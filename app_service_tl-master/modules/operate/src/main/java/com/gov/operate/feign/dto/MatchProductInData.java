package com.gov.operate.feign.dto;

import lombok.Data;

@Data
public class MatchProductInData {
    /**
     * 加盟商
     */
    private String clientId;
    /**
     * 门店编码字符串（多选）
     */
    private String[] stoCodes;
    /**
     * 导入时间开始时间
     */
    private String startDate;
    /**
     * 导入时间结束时间
     */
    private String endDate;
    /**
     * 匹配类型 0-未匹配，1-部分匹配，2-完全匹配
     */
    private String matchType;
    /**
     * 筛选状态 0-未处理，1-自动处理，2-手动处理
     */
    private String matchStatus;
    /**
     * 抽取比例
     */
    private String decimationRatio;
    /**
     * 是否门店匹配 0 否 1 是(1时门店编码必填)
     */
    private String type;

    private Integer pageNum;

    private String setSelect; //不为空 不查询手动确认 为空查询手动确认

    /**
     * 单门店查询
     */
    private String stoCode;

    /**
     * 已选中商品编码
     */
    private String proCode;

    private boolean clientFlag;//true为加盟商 false为门店
}
