package com.gov.operate.feign.dto;

import lombok.Data;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/7 01:13
 */
@Data
public class RegionDTO {
    /**加盟商列表*/
    private List<String> clients;
}
