package com.gov.operate.dto;

import lombok.Data;

@Data
public class EffectiveSale {
    //计划销售额
    private String amt;
    //计划销售数量
    private String gross;
    //计划会员卡数量
    private String cardQty;
    //订单数量
    private String noQty;
}
