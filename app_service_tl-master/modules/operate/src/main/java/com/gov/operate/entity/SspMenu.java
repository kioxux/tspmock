package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @TableName GAIA_SSP_MENU
 */
@TableName(value = "GAIA_SSP_MENU")
@Data
public class SspMenu extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 菜单code
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 排序
     */
    private Integer menuSort;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}