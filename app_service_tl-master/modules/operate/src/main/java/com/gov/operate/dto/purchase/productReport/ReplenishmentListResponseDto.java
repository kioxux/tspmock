package com.gov.operate.dto.purchase.productReport;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReplenishmentListResponseDto {

    /**
     * 加盟商
     */
    private String client;
    /**
     * DC编号
     */
    private String dcCode;
    /**
     * DC名称
     */
    private String dcName;
    /**
     * 连锁总部
     */
    private String dcChainHead;

    /**
     * 配送平均天数
     */
    private Integer dcDeliveryDays;

    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 商品名称
     */
    private String proName;

    /**
     * 商品分类
     */
    private String proClass;
    private String proClassName;

    /**
     * 商品大分类
     */
    private String proBigClass;
    private String proBigClassName;

    /**
     * 单位
     */
    private String proUnit;
    /**
     * 单位
     */
    private String proUnitName;

    /**
     * 商品描述
     */
    private String proDepict;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 中包装量
     */
    private String proMidPackage;
    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 商品仓储分区
     */
    private String proStorageArea;

    /**
     * 大仓库存
     */
    private BigDecimal dcStockTotal;

    /**
     * 门店库存
     */
    private BigDecimal storeStockTotal;

    /**
     * 总库存量
     */
    private BigDecimal stockTotal;
    /**
     * 最近一次进货日期
     */
    private String lastPurchaseDate;

    /**
     * 最后进货供应商自编码
     */
    private String lastSupplierId;

    /**
     * 供应商ID
     */
    private String poSupplierId;

    /**
     * 供应商送货前置期
     */
    private Integer supLeadTime;

    /**
     * 最后进货供应商
     */
    private String lastSupplier;

    /**
     * 付款条款
     */
    private String supPayTerm;

    /**
     * 末次进货价
     */
    private BigDecimal lastPurchasePrice;

    /**
     * 7天门店销售量
     */
    private BigDecimal daysSalesCountSeven;

    /**
     * 30天门店销售量
     */
    private BigDecimal daysSalesCount;

    /**
     * 90天门店销售量
     */
    private BigDecimal daysSalesCountNinety;

    /**
     * 30天对外批发量
     */
    private BigDecimal daysWholesaleCount;

    /**
     * 在途量
     */
    private BigDecimal trafficCount;

    /**
     * 待出量
     */
    private BigDecimal waitCount;

    /**
     * 补货门店数
     */
    private Integer storeCount;

    /**
     * 预计到货时间
     */
    private String poDeliveryDate;

    /**
     * 补货系数
     */
    private BigDecimal replenishmentRatio;

    /**
     * 建议补货量
     */
    private BigDecimal replenishmentValue;

    /**
     * 补货数量
     */
    private BigDecimal proposalReplenishment;

    /**
     * 供应商下拉列表
     */
//    private List<SupplierResponseDto> supplierResponseDtoList;

    /**
     * 备注
     */
    private String poLineRemark;

    /**
     * 采购价格
     */
    private BigDecimal poPrice;

    /**
     * N天销量
     */
    private BigDecimal daysSalesCountNinetyBghSqxsts;

    /**
     * 是否批发
     */
    private String proIfWholesale;

    /**
     * 生产批号
     */
    private String poBatchNo;
    /**
     * 生产日期
     */
    private String poScrq;
    /**
     * 有效期
     */
    private String poYxq;

    /**
     * 单店月均销量
     */
    private BigDecimal monthStoreSalesAvg;

    /**
     * 税率
     */
    private String taxCodename;

    /**
     * 自定义字段1
     */
    private String proZdy1;

    /**
     * 自定义字段2
     */
    private String proZdy2;

    /**
     * 自定义字段3
     */
    private String proZdy3;

    /**
     * 自定义字段4
     */
    private String proZdy4;

    /**
     * 自定义字段5
     */
    private String proZdy5;

    /**
     * 零售价
     */
    private BigDecimal gsppPriceNormal;

    /**
     * 会员价
     */
    private BigDecimal gsphsPrice;

    /**
     * 会员日价
     */
    private BigDecimal gsphpPrice;

    /**
     * 最新进价毛利
     */
    private BigDecimal grossProfit;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 商品自分类
     */
    private String proSclass;
    /**
     * 商品自分类
     */
    private String proSclassName;

    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 成分分类
     */
    private String proCompclass;
    /**
     * 成分分类描述
     */
    private String proCompclassName;
    /**
     * 剂型
     */
    private String proPorm;
    /**
     * 国际条形码
     */
    private String proBarcode;
    /**
     * 国际条形码2
     */
    private String proBarcode2;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 商品状态
     */
    private String proStatus;
    /**
     * 大包装量
     */
    private String proBigPackage;
    /**
     * 禁止采购
     */
    private String proNoPurchase;
    /**
     * 重点养护
     */
    private String proKeyCare;
    /**
     * 处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方
     */
    private String proPresclass;
    /**
     * 贮存条件 1-常温，2-阴凉，3-冷藏
     */
    private String proStorageCondition;
    /**
     * 管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制
     */
    private String proControlClass;
    /**
     * 保质期
     */
    private String proLife;
    /**
     * 保质期单位
     */
    private String proLifeUnit;
    /**
     * 是否医保
     */
    private String proIfMed;
    /**
     * 质量标准
     */
    private String proZlbz;
    /**
     * 采购员
     */
    private String proPurchaseRate;
    /**
     * 品牌标识名
     */
    private String proBrand;
    /**
     * 剂型
     */
    private String proForm;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 备注
     */
    private String proBeizhu;

    /**
     * 经营管理类别
     */
    private String proJygllb;

    /**
     * 档案号
     */
    private String proDah;

    /**
     * XX天门店销售量
     */
    private BigDecimal xxStoreSaleQty;
    /**
     * XX天动销门店数
     */
    private BigDecimal xxStoCount;
    /**
     * XX天动销率
     */
    private BigDecimal xxStoCountPercentage;
    /**
     * 库存门店数
     */
    private BigDecimal stoCountHasStock;
    /**
     * 铺货率
     */
    private BigDecimal stoCountHasStockPercentage;

    /**
     * 业务员下拉框
     */
//    private List<GaiaSupplierSalesman> salesList;

    /**
     * 供应商业务员
     */
    private String poSupplierSalesman;

    /**
     * 供应商业务员
     */
    private String poSupplierSalesmanName;
}
