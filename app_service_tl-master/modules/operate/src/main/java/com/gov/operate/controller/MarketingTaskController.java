package com.gov.operate.controller;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdMarketing;
import com.gov.operate.service.IMarketingTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "营销任务")
@RestController
@RequestMapping("gsm")
public class MarketingTaskController {

    @Resource
    private IMarketingTaskService marketingTaskService;

    @Log("当前人门店集合")
    @ApiOperation(value = "当前人门店集合")
    @GetMapping("getCurrentStoreList")
    public Result getCurrentStoreList() {
        return ResultUtil.success(marketingTaskService.getCurrentStoreList());
    }

    @Log("营销任务列表")
    @ApiOperation(value = "营销任务列表")
    @PostMapping("getGsmTaskList")
    public Result getGsmTaskList(@RequestBody GsmTaskListVO gsmTaskListVO) {
        return ResultUtil.success(marketingTaskService.getGsmTaskList(gsmTaskListVO));
    }

    @Log("营销任务详情")
    @ApiOperation(value = "营销任务详情")
    @GetMapping("getGsmTaskDetail")
    public Result getGsmTaskDetail(String client, String gsmMarketid, String gsmStore) {
        return ResultUtil.success(marketingTaskService.getGsmTaskDetail(client, gsmMarketid, gsmStore));
    }

    @Log("选品查询")
    @ApiOperation(value = "选品查询")
    @PostMapping("getProductList")
    public Result getProductList(@Valid @RequestBody ProductRequestVo productRequestVo) {
        return ResultUtil.success(marketingTaskService.getProductList(productRequestVo));
    }

    @Log("营销任务立即执行/提交审批")
    @ApiOperation(value = "营销任务立即执行/提交审批")
    @PostMapping("saveGsmTask")
    public Result saveGsmTask(@RequestBody @Valid GsmTaskSaveVo gsmTaskSaveVo) throws IllegalAccessException, InstantiationException {
        return marketingTaskService.saveGsmTask(gsmTaskSaveVo);
    }

    @Log("营销任务删除")
    @ApiOperation(value = "营销任务删除")
    @PostMapping("deleteMarketTask")
    public Result deleteMarketTask(@RequestBody SdMarketing marketing) {
        return marketingTaskService.deleteMarketTask(marketing);
    }

    @Log("评估列表查询")
    @ApiOperation(value = "评估列表查询")
    @PostMapping("getActiveEvalInfo")
    public Result getActiveEvalInfo(@Valid @RequestBody ActiveEvalVO activeEvalVO) {
        return marketingTaskService.getActiveEvalInfo(activeEvalVO);
    }

    /**
     * 已选活动商品
     *
     * @param productRequestVo
     * @return
     */
    @Log("已选活动商品")
    @ApiOperation(value = "已选活动商品")
    @PostMapping("getSelectedProductList")
    public Result getSelectedProductList(@Valid @RequestBody ProductRequestVo productRequestVo) {
        return marketingTaskService.getSelectedProductList(productRequestVo);
    }

    /**
     * 已选赠品
     *
     * @param productRequestVo
     * @return
     */
    @Log("已选赠品")
    @ApiOperation(value = "已选赠品")
    @PostMapping("selectGiftPorductList")
    public Result selectGiftPorductList(@RequestBody ProductRequestVo productRequestVo) {
        return marketingTaskService.selectGiftPorductList(productRequestVo);
    }

}
