package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 客户主数据业务信息表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/operate/customer-business")
public class CustomerBusinessController {

}

