package com.gov.operate.service;

import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryMonthAppraisalSalary;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryMonthAppraisalSalaryService extends SuperService<SalaryMonthAppraisalSalary> {

    /**
     * 月考评工资
     */
    void saveAppraial(EditProgramVO vo);

    /**
     * 月考评工资列表
     */
    List<SalaryMonthAppraisalSalary> getAppraisalSalaryList(Integer gspId);
}
