package com.gov.operate.service;

import com.gov.operate.entity.PoItem;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 采购订单明细表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
public interface IPoItemService extends SuperService<PoItem> {

}
