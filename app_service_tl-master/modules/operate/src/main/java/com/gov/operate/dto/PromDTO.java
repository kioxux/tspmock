package com.gov.operate.dto;

import com.gov.operate.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class PromDTO {

    /**
     * 促销主表类型
     */
    public final static String PROM_HEADER = "PROM_HEADER";
    /**
     * 单品类促销
     */
    public final static String PROM_SINGLE = "PROM_SINGLE";
    /**
     * 系列类促销
     */
    public final static String PROM_SERIES = "PROM_SERIES";
    /**
     * 赠品类促销
     */
    public final static String PROM_GIFT = "PROM_GIFT";
    /**
     * 电子券类促销
     */
    public final static String PROM_COUPON = "PROM_COUPON";
    /**
     * 会员类促销
     */
    public final static String PROM_HY = "PROM_HY";
    /**
     * 捆绑类促销
     */
    public final static String PROM_COMBIN = "PROM_COMBIN";


    /**
     * id : xxx
     * name : xxx
     * paramList : [{"key":"xxx","length":"xxx","name":"xxx","type":"xxx","value":"xxx"}]
     */

    private String id;
    private String name;
    /**
     * 是否需要反选商品 0:不需要， 1:需要
     */
    private String promType;
    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品数量
     */
    private Integer proCount;

    /**
     * 赠品数量
     */
    private Integer giftCount;

    /**
     * 传输参数
     */
    private List<ParamListBean> paramList;

    /**
     * 赠品、电子券、组合 赠送选品
     */
    private List<String> proCodeList;

    @NoArgsConstructor
    @Data
    public static class ParamListBean {
        /**
         * key : xxx
         * length : xxx
         * name : xxx
         * type : xxx
         * value : xxx
         */
        /**
         * 数据库列名
         */
        private String key;
        /**
         * 数据长度
         */
        private String length;
        /**
         * 描述
         */
        private String name;
        /**
         * 类型
         */
        private String type;
        /**
         * 值
         */
        private String value;

        /**
         * 促销类型
         */
        private String promType;

        /**
         * 促销表名
         */
        private String promTableName;
    }

    @Data
    public class PromCombin {
        /**
         * 组合类促销条件商品设置表
         */
        List<SdPromAssoConds> sdPromAssoCondsList;

        /**
         * 组合类促销系列编码设置表
         */
        SdPromAssoSet sdPromAssoSet;

        /**
         * 组合类促销赠送商品设置表
         */
        List<SdPromAssoResult> sdPromAssoResultList;
    }


    @Data
    public static class PromGift {
        /**
         * 赠品类促销条件商品设置表
         */
        List<SdPromGiftConds> sdPromGiftCondsList;

        /**
         * 赠品类促销系列编码设置表
         */
        SdPromGiftSet sdPromGiftSet;

        /**
         * 赠品类促销赠送商品设置表
         */
        List<SdPromGiftResult> sdPromGiftResultList;
    }

    @Data
    public static class PromSeries {
        /**
         * 系列类促销条件商品设置表
         */
        List<SdPromSeriesConds> sdPromSeriesCondsList;

        /**
         * 系列类促销系列编码设置表
         */
        SdPromSeriesSet sdPromSeriesSet;
    }

    @Data
    public static class PromCoupon {

        /**
         * 电子券类促销用券设置表
         */
        List<SdPromCouponUse> sdPromCouponUseList;

        /**
         * 电子券类促销系列编码设置表
         */
        SdPromCouponSet sdPromCouponSet;

        /*
         * 电子券类促销送券设置表
         */
        List<SdPromCouponGrant> sdPromCouponGrantList;

    }

    @Data
    public static class PromHy {

        /**
         * 会员类促销会员日折扣设置表
         */
        SdPromHyrDiscount sdPromHyrDiscount;
        /**
         * 会员类促销会员日价设置表
         */
        List<SdPromHyrPrice> sdPromHyrPriceList;

        /*
         * 会员类促销会员价设置表
         */
        List<SdPromHySet> sdPromHySetList;

    }



    /**
     * 促销主表
     */
    public class SD_PROM_HEAD {
        /**
         * 加盟商
         */
        public final static String CLIENT = "CLIENT";
        /**
         * 门店
         */
        public final static String GSPH_BR_ID = "GSPH_BR_ID";
        /**
         * 促销单号
         */
        public final static String GSPH_VOUCHER_ID = "GSPH_VOUCHER_ID";
        /**
         * 营销活动ID
         */
        public final static String GSPH_MARKETID = "GSPH_MARKETID";
    }

    /**
     * 单品设置表
     */
    public class GAIA_SD_PROM_UNITARY_SET {
        /**
         * 加盟商
         */
        public final static String CLIENT = "CLIENT";
        /**
         * 单号
         */
        public final static String GSPUS_VOUCHER_ID = "GSPUS_VOUCHER_ID";
        /**
         * 行号
         */
        public final static String GSPUS_SERIAL = "GSPUS_SERIAL";
        /**
         * 商品编码
         */
        public final static String GSPUS_PRO_ID = "GSPUS_PRO_ID";
    }


}
