package com.gov.operate.service.batchPurchaseSales.impl;

import com.gov.operate.entity.BatchStoreSales;
import com.gov.operate.mapper.BatchStoreSalesMapper;
import com.gov.operate.service.batchPurchaseSales.IBatchStoreSalesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店销售 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Service
public class BatchStoreSalesServiceImpl extends ServiceImpl<BatchStoreSalesMapper, BatchStoreSales> implements IBatchStoreSalesService {

}
