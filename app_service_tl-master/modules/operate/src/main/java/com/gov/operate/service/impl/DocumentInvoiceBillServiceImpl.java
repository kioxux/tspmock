package com.gov.operate.service.impl;

import com.gov.operate.entity.DocumentInvoiceBill;
import com.gov.operate.mapper.DocumentInvoiceBillMapper;
import com.gov.operate.service.IDocumentInvoiceBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Service
public class DocumentInvoiceBillServiceImpl extends ServiceImpl<DocumentInvoiceBillMapper, DocumentInvoiceBill> implements IDocumentInvoiceBillService {

}
