package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.AuthorityAuths;
import com.gov.operate.entity.AuthorityRoles;
import com.gov.operate.entity.AuthorityRolesAuths;
import com.gov.operate.entity.AuthorityUserRoles;
import com.gov.operate.mapper.AuthorityAuthsMapper;
import com.gov.operate.mapper.AuthorityRolesAuthsMapper;
import com.gov.operate.mapper.AuthorityRolesMapper;
import com.gov.operate.mapper.AuthorityUserRolesMapper;
import com.gov.operate.service.IUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserRoleServiceImpl implements IUserRoleService {

    @Resource
    private AuthorityRolesMapper authorityRolesMapper;

    @Resource
    private AuthorityAuthsMapper authorityAuthsMapper;

    @Resource
    private AuthorityRolesAuthsMapper authorityRolesAuthsMapper;

    @Resource
    private AuthorityUserRolesMapper authorityUserRolesMapper;

    /**
     * 新增用户角色关联
     *
     * @param client   加盟商
     * @param funcCode 功能Code
     * @param userId   新公司负责人
     * @return
     */
    @Override
    public Result createUserRole(String client, String funcCode, String userId) {
        Result result = validate(client, funcCode, userId);
        if (!StringUtils.equals(result.getCode(), "0")) {
            return result;
        }

        /**
         * 加盟商下的公司负责人角色是否创建
         */
        AuthorityRoles role = authorityRolesMapper.selectOne(
                new QueryWrapper<AuthorityRoles>().eq("CLIENT", client).eq("GAR_FUNC", funcCode));


        Integer roleId = null;
        // 需要创建角色
        if (role == null) {
            roleId = createRoleAuths(client, Objects.requireNonNull(CommonEnum.garFunc.getByCode(funcCode)));
        } else {
            roleId = role.getId();
        }

        // 创建新的用户角色关联
        QueryWrapper<AuthorityUserRoles> queryWrapper = new QueryWrapper<AuthorityUserRoles>()
                .eq("CLIENT", client)
                .eq("GAUR_ROLE_ID", roleId)
                .eq("GAUR_USER_ID", userId)
                .eq("GAUR_SFSQ", "1"); // 是否删除 - 否

        authorityUserRolesMapper.delete(queryWrapper);

        // 查询旧用户角色关联
        AuthorityUserRoles userRoles = new AuthorityUserRoles();
        userRoles.setClient(client);
        userRoles.setGaurRoleId(roleId);
        userRoles.setGaurUserId(userId);
        userRoles.setGaurCjrq(DateUtils.getCurrentDateStrYYMMDD());
        userRoles.setGaurCjsj(DateUtils.getCurrentTimeStrHHMMSS());
        userRoles.setGaurSfsq("1");
        authorityUserRolesMapper.insert(userRoles);

        return ResultUtil.success();
    }


    /**
     * 更新用户角色关联
     *
     * @param client    加盟商
     * @param funcCode  功能Code
     * @param userId    新公司负责人
     * @param oldUserId 旧公司负责人
     * @return
     */
    @Override
    @Transactional
    public Result createUserRole(String client, String funcCode, String userId, String oldUserId) {
        Result result = validate(client, funcCode, userId);
        if (!StringUtils.equals(result.getCode(), "0")) {
            return result;
        }
        /**
         * 加盟商下的公司负责人角色是否创建
         */
        AuthorityRoles role = authorityRolesMapper.selectOne(
                new QueryWrapper<AuthorityRoles>().eq("CLIENT", client).eq("GAR_FUNC", funcCode));
        Integer roleId = null;
        // 需要创建角色
        if (role == null) {
            roleId = createRoleAuths(client, Objects.requireNonNull(CommonEnum.garFunc.getByCode(funcCode)));
        } else {
            roleId = role.getId();
        }
        // 旧用户不等于空时 将旧用户角色关联删除  将新公司负责人绑定
        if (StringUtils.isNotEmpty(oldUserId)) {
            if (StringUtils.equals(userId, oldUserId)) {
                log.info("公司负责人一致无需插入/更新");
                return ResultUtil.success();
            }
            QueryWrapper<AuthorityUserRoles> queryWrapper = new QueryWrapper<AuthorityUserRoles>()
                    .eq("CLIENT", client)
                    .eq("GAUR_ROLE_ID", roleId)
                    .eq("GAUR_USER_ID", oldUserId);
            authorityUserRolesMapper.delete(queryWrapper);
        }
        if (StringUtils.isNotBlank(userId)) {
            AuthorityUserRoles userRoles = new AuthorityUserRoles();
            userRoles.setClient(client);
            userRoles.setGaurRoleId(roleId);
            userRoles.setGaurUserId(userId);
            userRoles.setGaurCjrq(DateUtils.getCurrentDateStrYYMMDD());
            userRoles.setGaurCjsj(DateUtils.getCurrentTimeStrHHMMSS());
            userRoles.setGaurSfsq("1");
            authorityUserRolesMapper.insert(userRoles);
        }
        return ResultUtil.success();
    }

    /**
     * 校验
     *
     * @param client
     * @param funcCode
     * @param userId
     * @return
     */
    public Result validate(String client, String funcCode, String userId) {
        if (StringUtils.isEmpty(client)) {
            return ResultUtil.error("0001", "加盟商不能为空");
        }

        if (StringUtils.isEmpty(funcCode)) {
            return ResultUtil.error("0002", "功能code不能为空");
        }

        if (StringUtils.isEmpty(userId)) {
            return ResultUtil.error(ResultEnum.E0027);
        }

        return ResultUtil.success();
    }


    /**
     * 创建角色和权限关联
     *
     * @param garFunc
     * @return
     */
    private Integer createRoleAuths(String client, CommonEnum.garFunc garFunc) {
        // 创建角色
        Integer roleId = createRole(client, garFunc);
        Integer[] authArr = {};
        switch (garFunc) {
            case ADMIN:
                authArr = createCEORoleAuthId();
                break;
            case CEO: // 公司负责人
                authArr = createCEORoleAuthId();
                break;
            case SHOP_OWNER: // 门店店长
                // 创建门店员工
                createRoleAuths(client, CommonEnum.garFunc.SHOP_USER);
                authArr = createShopOwnerAuths();
                break;
            case SHOP_USER: // 门店用户
                authArr = createShopUserAuths();
                break;
            case PURCHASE:  //商采负责人
                // 创建商采用户
                createRoleAuths(client, CommonEnum.garFunc.PURCHASE_USER);
                authArr = createPurchaseAuths();
                break;
            case PURCHASE_USER:  //商采员工
                authArr = createPurchaseUserAuths();
                break;
            case FINANCE: // 财务负责人
                // 创建财务用户
                createRoleAuths(client, CommonEnum.garFunc.FINANCE_USER);
                authArr = createFinanceAuths();
                break;
            case FINANCE_USER: // 财务
                authArr = createFinanceUserAuths();
                break;
            case HR: // 人资负责人
                // 创建人资用户
                createRoleAuths(client, CommonEnum.garFunc.HR_USER);
                authArr = createHRAuths();
                break;
            case HR_USER: // 人资用户
                authArr = createHRUserAuths();
                break;
            case LOGISTICS: // 物流负责人
                // 创建物流用户
                createRoleAuths(client, CommonEnum.garFunc.LOGISTICS_USER);
                authArr = createLogisticsAuths();
                break;
            case LOGISTICS_USER: // 物流用户
                authArr = createLogisticsUserAuths();
                break;
            case QUALITY:  // 质量负责人
                // 创建质量用户
                createRoleAuths(client, CommonEnum.garFunc.QUALITY_USER);
                authArr = createQualityAuths();
                break;
            case QUALITY_USER: // 质量用户
                authArr = createQualityUserAuths();
                break;
            default:
                break;
        }

        AuthorityRolesAuths rolesAuths = null;
        for (Integer authId : authArr) {
            rolesAuths = new AuthorityRolesAuths();
            rolesAuths.setClient(client);
            rolesAuths.setGaraRoleId(roleId);
            rolesAuths.setGaraAuthId(authId);
            rolesAuths.setGaraCjrq(DateUtils.getCurrentDateStrYYMMDD());
            rolesAuths.setGaraCjsj(DateUtils.getCurrentTimeStrHHMMSS());
            rolesAuths.setGaraSfsq("1"); // 是否删除-否
            authorityRolesAuthsMapper.insert(rolesAuths);
        }


        return roleId;
    }


    /**
     * 质量负责人
     *
     * @return
     */
    private Integer[] createQualityAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 14, 15, 16, 17, 18, 19, 39, 40, 41, 42, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 93, 94, 95, 78, 79, 91, 85};
    }

    /**
     * 质量用户
     *
     * @return
     */
    private Integer[] createQualityUserAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 14, 15, 16, 17, 18, 19, 39, 40, 41, 42, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 78, 79, 85};
    }


    /**
     * 门店用户
     */
    private Integer[] createShopUserAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 87, 88, 89, 90, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 45, 46, 47, 48, 106, 51, 107, 54, 108, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 85};
    }


    /**
     * 物流负责人
     *
     * @return
     */
    private Integer[] createLogisticsAuths() {
        return new Integer[]{3, 76, 77, 16, 17, 18, 19, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 82, 83, 100, 118, 101, 102, 103, 109, 85};
    }

    /**
     * 物流用户
     *
     * @return
     */
    private Integer[] createLogisticsUserAuths() {
        return new Integer[]{3, 76, 77, 16, 17, 18, 19, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 85};
    }


    /**
     * 人资负责人
     *
     * @return
     */
    private Integer[] createHRAuths() {
        return new Integer[]{3, 76, 77, 16, 17, 18, 19, 57, 58, 59, 60, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 93, 94, 95, 78, 79, 80, 91, 82, 83, 100, 118, 101, 102, 103, 109, 85};
    }

    /**
     * 人资用户
     *
     * @return
     */
    private Integer[] createHRUserAuths() {
        return new Integer[]{3, 76, 77, 16, 17, 18, 19, 57, 58, 59, 60, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 80, 85};

    }


    /**
     * 财务负责人
     *
     * @return
     */
    private Integer[] createFinanceAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 87, 88, 89, 90, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 106, 51, 107, 54, 108, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 91, 82, 83, 100, 118, 101, 102, 103, 109, 85, 86};
    }

    /**
     * 财务用户
     *
     * @return
     */
    private Integer[] createFinanceUserAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 87, 88, 89, 90, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 106, 51, 107, 54, 108, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 91, 85, 86};
    }

    /**
     * 商采负责人角色和权限的关联
     */
    private Integer[] createPurchaseAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 13, 87, 88, 89, 90, 16, 17, 18, 19, 45, 46, 47, 48, 49, 50, 106, 51, 52, 53, 107, 54, 55, 56, 108, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 82, 83, 100, 118, 101, 102, 103, 109, 85};
    }

    /**
     * 商采用户角色和权限的关联
     *
     * @return
     */
    private Integer[] createPurchaseUserAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 13, 87, 88, 89, 90, 16, 17, 18, 19, 45, 46, 47, 48, 49, 50, 106, 51, 52, 53, 107, 54, 55, 56, 108, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 95, 78, 79, 85};
    }

    /**
     * 创建门店店长角色和权限的关联
     */
    private Integer[] createShopOwnerAuths() {
        return new Integer[]{1, 2, 3, 76, 77, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 87, 88, 89, 90, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 45, 46, 47, 48, 106, 51, 107, 54, 108, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 92, 93, 94, 95, 78, 79, 91, 82, 83, 100, 118, 101, 102, 103, 109, 85};
    }

    /**
     * 创建公司负责人角色和权限的关联
     *
     * @return
     */
    private Integer[] createCEORoleAuthId() {
        List<AuthorityAuths> authList = authorityAuthsMapper.selectList(null);
        List<Integer> list = new ArrayList<>();
        if (authList != null && authList.size() > 0) {
            list = authList.stream().map(AuthorityAuths::getId).collect(Collectors.toList());
            return list.toArray(new Integer[list.size()]);
        }
        return new Integer[0];
    }

    /**
     * 创建角色
     *
     * @param client
     * @param garFunc
     * @return
     */
    private Integer createRole(String client, CommonEnum.garFunc garFunc) {
        AuthorityRoles role = new AuthorityRoles();
        role.setClient(client);
        role.setGarRoleName(garFunc.getName());
        role.setGarBz(garFunc.getName());
        role.setGarEnable("0"); // 是- 启用
        role.setGarCjrq(DateUtils.getCurrentDateStrYYMMDD());
        role.setGarCjsj(DateUtils.getCurrentTimeStrHHMMSS());
        role.setGarSfsq("1");
        role.setGarCjr("1"); // 创建人
        role.setGarFunc(garFunc.getCode());
        authorityRolesMapper.insert(role);
        return role.getId();
    }


}
