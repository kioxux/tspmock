package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchStoreSales;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 门店销售 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchStoreSalesService extends SuperService<BatchStoreSales> {

}
