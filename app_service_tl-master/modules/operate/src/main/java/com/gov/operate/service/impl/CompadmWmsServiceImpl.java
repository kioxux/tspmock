package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.userutil.UserUtils;
import com.gov.operate.dto.compadmWms.CompadmWmsAddVO;
import com.gov.operate.dto.compadmWms.CompadmWmsDto;
import com.gov.operate.dto.compadmWms.CompadmWmsEditVO;
import com.gov.operate.dto.storeData.StoreDataCodeDTO;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IAuthconfiDataService;
import com.gov.operate.service.ICompadmWmsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.service.IUserDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
@Service
public class CompadmWmsServiceImpl extends ServiceImpl<CompadmWmsMapper, CompadmWms> implements ICompadmWmsService {
    @Resource
    private CommonService commonService;
    @Resource
    private CompadmWmsMapper compadmWmsMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private IUserDataService userDataService;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private StoreDataMapper storeDataMapper;
    @Autowired
    private DepDataMapper depDataMapper;
    @Autowired
    private DcDataMapper dcDataMapper;
    @Resource
    private IAuthconfiDataService authconfiService;
    /**
     * 获取门店编码
     * @return
     */
    public String getMaxCode() {
        TokenUser user = commonService.getLoginInfo();
        Integer compadmWms = compadmWmsMapper.getMaxNumCode(user.getClient());
        if (StringUtils.isNotEmpty(compadmWms)) {
            Integer codeInteger = compadmWms + 1;
            return String.valueOf(codeInteger);
        }
        return "1000000";
    }

    /**
     * 通过加盟商 + id 获取批发
     *
     * @param client    加盟商
     * @param id 门店编码
     * @return
     */
    private CompadmWms getCompadmWmsByClientAndCode(String client, String id) {
        QueryWrapper<CompadmWms> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("COMPADM_ID", id);
        CompadmWms compadmWms = this.getOne(queryWrapper);
        return compadmWms;
    }


    /**
     * 门店详情
     *
     * @param compadmId 门店编码
     */
    @Override
    public CompadmWmsDto getStoreInfo(String compadmId) {
        TokenUser tokenUser = commonService.getLoginInfo();
        CompadmWms compadmWms =getCompadmWmsByClientAndCode(tokenUser.getClient(), compadmId);
        CompadmWmsDto compadmWmsDto = new CompadmWmsDto();
        BeanUtils.copyProperties(compadmWms, compadmWmsDto);
        compadmWmsDto.setCompadmLogo(cosUtils.urlAuth(compadmWms.getCompadmLogo()));
        // 法人
        UserData user = userDataService.getUserByClientAndUserId(tokenUser.getClient(), compadmWms.getCompadmLegalPerson());
        if (StringUtils.isNotEmpty(user)) {
            compadmWmsDto.setCompadmLegalPersonName(user.getUserNam());
            compadmWmsDto.setCompadmLegalPersonPhone(user.getUserTel());
        }

        // 质量负责人
        user = userDataService.getUserByClientAndUserId(tokenUser.getClient(), compadmWms.getCompadmQua());
        if (StringUtils.isNotEmpty(user)) {
            compadmWmsDto.setCompadmQuaName(user.getUserNam());
            compadmWmsDto.setCompadmQuaPhone(user.getUserTel());
        }

        return compadmWmsDto;
    }

    /**
     * 批发新增
     */
    @Override
    @Transactional
    public void addCompadmWms(CompadmWmsAddVO compadmWmsAddVO) {
        TokenUser tokenUser = commonService.getLoginInfo();
        // 校验批发编码是否被占用
        CompadmWms compadmWms =getCompadmWmsByClientAndCode(tokenUser.getClient(), compadmWmsAddVO.getCompadmId());
        if (StringUtils.isNotEmpty(compadmWms)) {
            throw new CustomResultException(ResultEnum.E0036);
        }
        compadmWmsAddVO.setCompadmId(this.getMaxCode());
        String userLeaderId="";
        String userQuaId="";
        compadmWms = new CompadmWms();
        BeanUtils.copyProperties(compadmWmsAddVO, compadmWms);
        if (StringUtils.isNotEmpty(compadmWmsAddVO.getCompadmQuaPhone())) {
            //如果店长和负责人是同一人
            if (compadmWmsAddVO.getCompadmLegalPersonPhone().equals(compadmWmsAddVO.getCompadmQua())) {
                userLeaderId = getUserId(tokenUser, compadmWmsAddVO, null);
            } else {
                userLeaderId = getUserId(tokenUser, compadmWmsAddVO, CommonEnum.UserType.STATUS_LEADER_PERSON.getCode());
                userQuaId = getUserId(tokenUser, compadmWmsAddVO, CommonEnum.UserType.STATUS_QUA.getCode());

            }
        }else {
            userLeaderId = getUserId(tokenUser, compadmWmsAddVO, null);
        }
        compadmWms.setCompadmLegalPerson(userLeaderId);
        compadmWms.setCompadmQua(userQuaId);
        compadmWms.setCompadmStatus(CommonEnum.StoStatus.STO_STATUS_YES.getCode());
        compadmWms.setClient(tokenUser.getClient());
        compadmWms.setCompadmCreDate(DateUtils.getCurrentDateStrYYMMDD());
        compadmWms.setCompadmCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        compadmWms.setCompadmCreId(tokenUser.getUserId());
        compadmWms.setCompadmModiDate(DateUtils.getCurrentDateStrYYMMDD());
        compadmWms.setCompadmModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        compadmWms.setCompadmModiId(tokenUser.getUserId());
        this.save(compadmWms);
    }


    /**
     * 判断手机号是否已经注册 如果存在直接拿编码 不存则在创建角色
     * @param tokenUser
     * @param compadmWmsAddVO
     * @param code
     * @return
     */
    @Transactional
    public String  getUserId(TokenUser tokenUser, CompadmWmsAddVO compadmWmsAddVO, String code) {
        String userId ="";
        UserData userData;
        if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
            userData = userDataService.getUserByClientAndPhone(tokenUser.getClient(),compadmWmsAddVO.getCompadmQuaPhone());
        }else {
            userData = userDataService.getUserByClientAndPhone(tokenUser.getClient(),compadmWmsAddVO.getCompadmLegalPersonPhone());
        }

        String passWord = String.valueOf(new Random().nextInt(900000) + 100000);
        if(StringUtils.isEmpty(userData)){
            userData = new UserData();
            int userMax = this.userDataService.selectMaxId(tokenUser.getClient());
            userData.setClient(tokenUser.getClient());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(tokenUser.getUserId());
            userData.setDepId(compadmWmsAddVO.getCompadmId()); //部门 改为门店
            userData.setDepName(compadmWmsAddVO.getCompadmName());
            userData.setUserId(String.valueOf(userMax + 1));
            if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
                userData.setUserNam(compadmWmsAddVO.getCompadmQuaName());
                userData.setUserTel(compadmWmsAddVO.getCompadmQuaPhone());
            }else {
                userData.setUserNam(compadmWmsAddVO.getCompadmLegalPersonName());
                userData.setUserTel(compadmWmsAddVO.getCompadmLegalPersonPhone());
            }
//            userData.setUserPassword(UserUtils.encryptMD5(passWord));
            userData.setUserPassword(UserUtils.encryptMD5("123456"));
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserJoinDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(tokenUser.getUserId());
            userData.setUserCreId(tokenUser.getUserId());
            this.userDataService.insert(userData);
            userId = String.valueOf(userMax + 1);
            //给店长发短信
        }else {
            userId = userData.getUserId();
            userData.setDepId(compadmWmsAddVO.getCompadmId()); //部门 改为门店
            userData.setDepName(compadmWmsAddVO.getCompadmName());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(tokenUser.getUserId());
            UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("CLIENT", tokenUser.getClient());
            updateWrapper.eq("USER_ID", userId);
            this.userDataService.update(userData,updateWrapper);
        }
        List<String> siteList = new ArrayList<>();
        siteList.add(compadmWmsAddVO.getCompadmId());
        this.saveAuthConfig(tokenUser.getClient(),userData.getUserId(),siteList,null);

        return userId;
    }


    @Transactional
    public void saveAuthConfig(String client ,String userId ,List<String> siteList, String delUserId ){

        List<String> groupList = new ArrayList();
        groupList.add("GAIA_AL_ADMIN");
        groupList.add("GAIA_AL_MANAGER");
        groupList.add("GAIA_WM_FZR");
        groupList.add("GAIA_MM_ZLZG");
        groupList.add("GAIA_MM_SCZG");
        groupList.add("SD_01");
        groupList.add("GAIA_FI_ZG");
        this.authconfiService.saveAuth(client, userId, groupList,siteList,delUserId);
    }

    /**
     * 批发编辑
     * @param compadmWmsEditVO 入参实体
     */
    @Override
    public void editCompadmWms(CompadmWmsEditVO compadmWmsEditVO) {
        TokenUser tokenUser = commonService.getLoginInfo();

        CompadmWms exitCompadmWms = getCompadmWmsByClientAndCode(tokenUser.getClient(), compadmWmsEditVO.getCompadmId());
        if (StringUtils.isEmpty(exitCompadmWms)) {
            throw new CustomResultException(ResultEnum.E0037);
        }
        // 门店属性是连锁，纳税主体不能为空
//        if (CommonEnum.StoreType.CHAIN_STORE.getCode().equals(compadmWmsEditVO.getStoAttribute())) {
//            if (StringUtils.isEmpty(compadmWmsEditVO.getStoTaxSubject())) {
//                throw new CustomResultException("纳税主体不能为空");
//            }
//        }
        compadmWmsEditVO.setClient(tokenUser.getClient());
        CompadmWms compadmWms = new CompadmWms();
        BeanUtils.copyProperties(compadmWmsEditVO, compadmWms);
        compadmWms.setCompadmModiDate(DateUtils.getCurrentDateStrYYMMDD());
        compadmWms.setCompadmModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        compadmWms.setCompadmModiId(tokenUser.getUserId());
        List<String> siteList = new ArrayList<>();
        //获取旗下所有部门
        List<String> depList = this.depDataMapper.selectListForAuth(tokenUser.getClient(),compadmWms.getCompadmId());
        siteList.addAll(depList);

        UpdateWrapper<CompadmWms> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", tokenUser.getClient());
        updateWrapper.eq("COMPADM_ID", compadmWmsEditVO.getCompadmId());
        this.update(compadmWms, updateWrapper);
        //判断当前连锁下有没有配送中心  有的话 修改配送中心和部门的name   连锁总部名+-配送中心
        int count = this.depDataMapper.selectCount( new QueryWrapper<DepData>().eq("CLIENT",tokenUser.getClient()).eq("STO_CHAIN_HEAD",compadmWmsEditVO.getCompadmId()).eq("DEP_CLS","10") );
        if(count>0){
            DepData inDepData = new DepData();
            inDepData.setDepName(compadmWmsEditVO.getCompadmName()+"-配送中心");
            this.depDataMapper.update(inDepData,new UpdateWrapper<DepData>().eq("CLIENT",tokenUser.getClient()).eq("STO_CHAIN_HEAD",compadmWmsEditVO.getCompadmId()).eq("DEP_CLS","10") );
            DcData inDcData = new DcData();
            inDcData.setDcName(compadmWmsEditVO.getCompadmName()+"-配送中心");
            this.dcDataMapper.update(inDcData,new UpdateWrapper<DcData>().eq("CLIENT",tokenUser.getClient()).eq("DC_CHAIN_HEAD",compadmWmsEditVO.getCompadmId()) );
        }
        if (!exitCompadmWms.getCompadmLegalPerson().equals(compadmWms.getCompadmLegalPerson())) {
            this.userDataService.editDepById(compadmWms.getCompadmId(),compadmWms.getCompadmName(),compadmWms.getCompadmLegalPerson());
            this.saveAuthConfig(tokenUser.getClient(), compadmWms.getCompadmLegalPerson(), siteList, exitCompadmWms.getCompadmLegalPerson());
        }
        if (!exitCompadmWms.getCompadmQua().equals(compadmWms.getCompadmQua())) {
            this.userDataService.editDepById(compadmWms.getCompadmId(),compadmWms.getCompadmName(),compadmWms.getCompadmQua());
            this.saveAuthConfig(tokenUser.getClient(), compadmWms.getCompadmQua(), siteList, exitCompadmWms.getCompadmQua());
        }
    }

    @Override
    public List<CompadmWmsDto> getCompadmWms() {
        TokenUser tokenUser = commonService.getLoginInfo();
//        QueryWrapper<CompadmWms> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("CLIENT", tokenUser.getClient());
        List<CompadmWmsDto> compadmWms = this.compadmWmsMapper.select(tokenUser.getClient());
        return compadmWms;
    }

}
