package com.gov.operate.mapper;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InvoiceMapper {

    /**
     * 出入库查询
     */
    IPage<SelectWarehousingDTO> selectWarehousing(Page page, @Param("vo") SelectWarehousingVO vo);

    /**
     * 出入库明细
     */
    List<SelectWarehousingDetailsDTO> selectWarehousingDetails(@Param("vo") SelectWarehousingDetailsVO vo);

    /**
     * 获取入库单据总金额
     */
    WmsDTO getWmsRkys(@Param("client") String client, @Param("matDnId") String matDnId);

    /**
     * 获取出库单据总金额
     */
    WmsDTO wmsTuiGong(@Param("client") String client, @Param("matDnId") String matDnId);

    WmsDTO getChajia(@Param("client") String client, @Param("matDnId") String matDnId);

    /**
     * 单据明细
     */
    List<SelectWarehousingDetailsDTO> getMaterialDocDetails(@Param("client") String client, @Param("matDnId") String matDnId, @Param("type") String type);

    /**
     * 订单列表
     */
    List<WmsDTO> getOrderList(@Param("client") String client, @Param("matDnIdList") List<String> matDnIdList);

    /**
     * 预付单查询
     *
     * @param page
     * @param vo
     * @return
     */
    IPage<SelectWarehousingDTO> selectPrepaidBill(Page page, @Param("vo") SelectWarehousingVO vo);

    /**
     * 出入库查询
     */
    IPage<SelectWarehousingDTO> selectDocumentList(Page page, @Param("vo") SelectWarehousingVO vo);

    /**
     * 单据明细
     *
     * @param selectWarehousingDetailsVO
     * @return
     */
    List<SelectWarehousingDetailsDTO> selectDocumentDetails(@Param("vo") SelectWarehousingDetailsVO selectWarehousingDetailsVO);

    SelectWarehousingDTO selectDocumentItem(@Param("vo") SelectWarehousingVO vo);

    /**
     * 单据明细
     *
     * @param selectWarehousingDetailsVO
     * @return
     */
    SelectWarehousingDetailsDTO selectDocumentDetail(@Param("vo") SelectWarehousingDetailsVO selectWarehousingDetailsVO);

    /**
     * 出入库查询
     */
    IPage<SelectWarehousingDTO> selectDocumentListForInvoice(Page page, @Param("vo") SelectWarehousingVO vo);

    /**
     * 单据明细
     *
     * @param selectWarehousingDetailsVO
     * @return
     */
    List<SelectWarehousingDetailsDTO> selectDocumentDetailsForInvoice(@Param("vo") SelectWarehousingDetailsVO selectWarehousingDetailsVO);

    IPage<SelectWarehousingDetailsDTO> selectWarehousingList(Page page, @Param("vo") SelectWarehousingVO vo);
}
