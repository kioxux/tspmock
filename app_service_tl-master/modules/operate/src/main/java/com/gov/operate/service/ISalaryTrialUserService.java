package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.GetSalaryTrialDTO;
import com.gov.operate.dto.QuerySalaryTrialVO;
import com.gov.operate.entity.SalaryTrialUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryTrialUserService extends SuperService<SalaryTrialUser> {

    /**
     * 返回当前方案试算结果
     */
    GetSalaryTrialDTO getSalaryTrial(Integer gspId);

    /**
     * 方案试算
     */
    void querySalaryTrial(QuerySalaryTrialVO vo);
}
