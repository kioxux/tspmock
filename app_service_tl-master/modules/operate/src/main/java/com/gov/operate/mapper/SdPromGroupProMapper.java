package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SdPromGroupPro;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 促销商品分明细组表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
public interface SdPromGroupProMapper extends BaseMapper<SdPromGroupPro> {

    List<ProductBusiness> getSdPromGroupProList(@Param("client") String client, @Param("groupId") String groupId);
}
