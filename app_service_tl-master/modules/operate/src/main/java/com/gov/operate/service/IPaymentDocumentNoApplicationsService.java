package com.gov.operate.service;

import com.gov.operate.entity.PaymentDocumentNoApplications;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-16
 */
public interface IPaymentDocumentNoApplicationsService extends SuperService<PaymentDocumentNoApplications> {

}
