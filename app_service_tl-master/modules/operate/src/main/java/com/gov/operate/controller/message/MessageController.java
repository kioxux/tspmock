package com.gov.operate.controller.message;

import com.gov.common.entity.Pageable;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.entity.Message;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.23
 */

@Api("消息")
@RestController
@RequestMapping("message/message")
public class MessageController {

    @Resource
    private IMessageService messageService;

    @Log("APP用户消息")
    @ApiOperation(value = "APP用户消息")
    @PostMapping("getMessageList")
    public Result getMessageList(@RequestJson(value = "pageNum", name = "页码") Integer pageNum,
                                 @RequestJson(value = "pageSize", name = "页长") Integer pageSize,
                                 @RequestJson(value = "gmBusinessType", name = "业务类型 1：工作，2：消息", required = false) Integer gmBusinessType,
                                 @RequestJson(value = "isRead", name = "已读", required = false) Integer isRead) {
        // 消息列表
        return messageService.getMessageList(pageNum, pageSize, gmBusinessType, isRead, 2);
    }

    @Log("APP用户消息")
    @ApiOperation(value = "APP用户消息")
    @PostMapping("getHomeMessageList")
    public Result getHomeMessageList(@RequestJson(value = "pageNum", name = "页码") Integer pageNum,
                                     @RequestJson(value = "pageSize", name = "页长") Integer pageSize,
                                     @RequestJson(value = "gmBusinessType", name = "业务类型 1：工作，2：消息", required = false) Integer gmBusinessType,
                                     @RequestJson(value = "isRead", name = "已读", required = false) Integer isRead) {
        // 消息列表
        return messageService.getMessageList(pageNum, pageSize, gmBusinessType, isRead, 1);
    }

    @Log("APP用户消息读取")
    @ApiOperation(value = "APP用户消息读取")
    @PostMapping("readMessage")
    public Result readMessage(@RequestJson("gmId") String gmId) {
        // 消息读取
        return messageService.readMessage(gmId);
    }

    @Log("APP用户消息批量已读")
    @ApiOperation(value = "APP用户消息批量已读")
    @PostMapping("readBatchMessage")
    public Result readBatchMessage(@RequestBody List<String> list) {
        // 批量已读
        return messageService.readBatchMessage(list);
    }

    @Log("APP用户消息删除")
    @ApiOperation(value = "APP用户消息删除")
    @PostMapping("delMessage")
    public Result delMessage(@RequestBody List<String> list) {
        // 消息删除
        return messageService.delMessage(list);
    }

    @Log("发送消息")
    @ApiOperation(value = "发送消息")
    @PostMapping("sendMessage")
    public Result sendMessage(@RequestBody MessageParams messageParams) {
        // 发送消息
        return messageService.sendMessage(messageParams);
    }

}
