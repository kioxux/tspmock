package com.gov.operate.service.impl;

import com.gov.operate.entity.CustomerBusiness;
import com.gov.operate.mapper.CustomerBusinessMapper;
import com.gov.operate.service.ICustomerBusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户主数据业务信息表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-06-10
 */
@Service
public class CustomerBusinessServiceImpl extends ServiceImpl<CustomerBusinessMapper, CustomerBusiness> implements ICustomerBusinessService {

}
