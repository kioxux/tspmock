package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class Verification {

    @NotBlank(message = "手机号不能为空")
    private String tel;

    @NotBlank(message = "业务类型不能为空")
    private String codeType;

    private String StoreName;

    private String passWord;
    /**
     * 加盟商
     */
    private String client;

    private String stoCode;
    //验证码
    private String authCode;
    //序列码
    private String seq;
    public Verification(@NotBlank String tel, @NotBlank String codeType, String StoreName, String passWord) {
        this.tel = tel;
        this.codeType = codeType;
        this.StoreName = StoreName;
        this.passWord = passWord;

    }

    public Verification() {
    }
}
