package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_RECHARGE_CARD_CHANGE")
@ApiModel(value="SdRechargeCardChange对象", description="")
public class SdRechargeCardChange extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "操作单号")
    @TableField("GSRCC_VOUCHER_ID")
    private String gsrccVoucherId;

    @ApiModelProperty(value = "门店")
    @TableField("GSRCC_BR_ID")
    private String gsrccBrId;

    @ApiModelProperty(value = "日期")
    @TableField("GSRCC_DATE")
    private String gsrccDate;

    @ApiModelProperty(value = "时间")
    @TableField("GSRCC_TIME")
    private String gsrccTime;

    @ApiModelProperty(value = "类型")
    @TableField("GSRCC_TYPE")
    private String gsrccType;

    @ApiModelProperty(value = "原因")
    @TableField("GSRCC_REMARK")
    private String gsrccRemark;

    @ApiModelProperty(value = "工号")
    @TableField("GSRCC_EMP")
    private String gsrccEmp;

    @ApiModelProperty(value = "储值卡账户")
    @TableField("GSRCC_ACCOUNT_ID")
    private String gsrccAccountId;

    @ApiModelProperty(value = "原储值卡号")
    @TableField("GSRCC_OLD_CARD_ID")
    private String gsrccOldCardId;

    @ApiModelProperty(value = "新储值卡号")
    @TableField("GSRCC_NEW_CARD_ID")
    private String gsrccNewCardId;


}
