package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

@Data
public class GradeDistributionVO {

    /**
     * 分级ID
     */
    private String gsglId;

    /**
     * 分级名称
     */
    private String gsglName;

    /**
     * 计算月份
     */
    private String gsmglCalcMonth;

    /**
     * 分级数量
     */
    private Integer gradeTotal;

    /**
     * 分级总数（趋势）
     */
    private Integer gradeAllTotal;

    /**
     * 分级颜色
     */
    private String gsglColor;

    /**
     * 数据集
     */
    private List<GradeDistributionVO> dataList;

}
