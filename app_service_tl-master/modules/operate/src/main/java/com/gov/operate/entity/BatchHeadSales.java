package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 总部销售
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_HEAD_SALES")
@ApiModel(value="BatchHeadSales对象", description="总部销售")
public class BatchHeadSales extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableField("ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBHS_QUERY_FACTORY")
    private String gbhsQueryFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBHS_DC")
    private String gbhsDc;

    @ApiModelProperty(value = "日期")
    @TableField("GBHS_DATE")
    private String gbhsDate;

    @ApiModelProperty(value = "销售方代码")
    @TableField("GBHS_SALES_CODE")
    private String gbhsSalesCode;

    @ApiModelProperty(value = "销售方名称")
    @TableField("GBHS_SALES_NAME")
    private String gbhsSalesName;

    @ApiModelProperty(value = "采购方代码")
    @TableField("GBHS_PURCHASE_CODE")
    private String gbhsPurchaseCode;

    @ApiModelProperty(value = "采购方名称")
    @TableField("GBHS_PURCHASE_NAME")
    private String gbhsPurchaseName;

    @ApiModelProperty(value = "产品代码")
    @TableField("GBHS_PRO_CODE")
    private String gbhsProCode;

    @ApiModelProperty(value = "产品名称")
    @TableField("GBHS_PRO_NAME")
    private String gbhsProName;

    @ApiModelProperty(value = "产品规格")
    @TableField("GBHS_PRO_SPECS")
    private String gbhsProSpecs;

    @ApiModelProperty(value = "批号")
    @TableField("GBHS_BATCH_NO")
    private String gbhsBatchNo;

    @ApiModelProperty(value = "数量")
    @TableField("GBHS_QTY")
    private BigDecimal gbhsQty;

    @ApiModelProperty(value = "单位")
    @TableField("GBHS_UNIT")
    private String gbhsUnit;

    @ApiModelProperty(value = "单价")
    @TableField("GBHS_PRICE")
    private BigDecimal gbhsPrice;

    @ApiModelProperty(value = "金额")
    @TableField("GBHS_AMT")
    private BigDecimal gbhsAmt;

    @ApiModelProperty(value = "生产厂家")
    @TableField("GBHS_FACTORY")
    private String gbhsFactory;

    @ApiModelProperty(value = "有效期")
    @TableField("GBHS_VALID")
    private String gbhsValid;

    @ApiModelProperty(value = "销售类型")
    @TableField("GBHS_SALES_TYPE")
    private String gbhsSalesType;

    @ApiModelProperty(value = "收货地址")
    @TableField("GBHS_RECEIPT_ADDR")
    private String gbhsReceiptAddr;

    @ApiModelProperty(value = "批准文号")
    @TableField("GBHS_APPROVAL_NUMBER")
    private String gbhsApprovalNumber;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBHS_CREATE_DATE")
    private String gbhsCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBHS_CREATE_TIME")
    private String gbhsCreateTime;


}
