package com.gov.operate.dto;

import lombok.Data;


/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/12/31 10:14
 */
@Data
public class SdPromGroupProVo {
    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 通用名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 销售级别
     */
    private String proSaleClass;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 是否医保
     */
    private String proIfMed;
}
