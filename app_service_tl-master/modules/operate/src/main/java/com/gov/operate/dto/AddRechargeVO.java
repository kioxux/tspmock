package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddRechargeVO {

    private String gsrcId;

    private String gsrcAddress;

    private String gsrcTel;

    @NotBlank(message = "手机不能为空")
    private String gsrcMobile;

    @NotBlank(message = "性别不能为空")
    private String gsrcSex;

    @NotBlank(message = "姓名不能为空")
    private String gsrcName;

}
