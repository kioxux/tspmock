package com.gov.operate.service.impl;

import com.gov.operate.entity.FiInvoiceRegistrationDetailed;
import com.gov.operate.mapper.FiInvoiceRegistrationDetailedMapper;
import com.gov.operate.service.IFiInvoiceRegistrationDetailedService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class FiInvoiceRegistrationDetailedServiceImpl extends ServiceImpl<FiInvoiceRegistrationDetailedMapper, FiInvoiceRegistrationDetailed> implements IFiInvoiceRegistrationDetailedService {

}
