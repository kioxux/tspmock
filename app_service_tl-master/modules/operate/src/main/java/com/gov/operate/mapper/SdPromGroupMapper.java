package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.prom.SdPromGroupDto;
import com.gov.operate.entity.SdPromGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 促销商品分组表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
public interface SdPromGroupMapper extends BaseMapper<SdPromGroup> {


    IPage<SdPromGroupDto> getSdPromGroupList(Page<SdPromGroupDto> page,
                                             @Param("client") String client, @Param("groupName") String groupName, @Param("groupProId") String groupProId);

    Integer selectGroupId(@Param("client") String client);

}
