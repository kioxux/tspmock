package com.gov.operate.service.ssp;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gov.common.response.Result;
import com.gov.operate.dto.ssp.SspLogPageDTO;
import com.gov.operate.entity.SspLog;

/**
 *
 */
public interface ISspLogService extends IService<SspLog> {

    Result selectLogByPage(SspLogPageDTO sspLogPageDTO);
}
