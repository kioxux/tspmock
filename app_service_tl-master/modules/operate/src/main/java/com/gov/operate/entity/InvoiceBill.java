package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_INVOICE_BILL")
@ApiModel(value="InvoiceBill对象", description="")
public class InvoiceBill extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GIB_NUM")
    private String gibNum;

    @ApiModelProperty(value = "供应商")
    @TableField("GIB_SUP_CODE")
    private String gibSupCode;

    @ApiModelProperty(value = "收货金额")
    @TableField("GIB_DOC_TOTAL_AMT")
    private BigDecimal gibDocTotalAmt;

    @ApiModelProperty(value = "发票金额")
    @TableField("GIB_INVOICE_TOTAL_AMT")
    private BigDecimal gibInvoiceTotalAmt;

    @ApiModelProperty(value = "折扣")
    @TableField("GIB_REBATE")
    private BigDecimal gibRebate;

    @ApiModelProperty(value = "应付金额")
    @TableField("GIB_PAY_AMT")
    private BigDecimal gibPayAmt;

    @ApiModelProperty(value = "付款差异")
    @TableField("GIB_DOC_DIFF")
    private BigDecimal gibDocDiff;

    @ApiModelProperty(value = "发票差异")
    @TableField("GIB_INVOICE_DIFF")
    private BigDecimal gibInvoiceDiff;

    @ApiModelProperty(value = "原因")
    @TableField("GIB_REASON")
    private String gibReason;

    @ApiModelProperty(value = "创建人")
    @TableField("GIB_CREATE_USER")
    private String gibCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GIB_CREATE_DATE")
    private String gibCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GIB_CREATE_TIME")
    private String gibCreateTime;


}
