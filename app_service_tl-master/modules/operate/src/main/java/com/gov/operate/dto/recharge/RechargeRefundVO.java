package com.gov.operate.dto.recharge;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author zhoushuai
 * @date 2021/4/27 11:54
 */
@Data
public class RechargeRefundVO {

    /**
     * 储值卡账户
     */
    @NotBlank(message = "储值卡账户不能为空")
    private String gsrcAccountId;
    /**
     * 充值卡号
     */
    @NotBlank(message = "充值卡号不能为空")
    private String gsrcId;
    /**
     * 充值单号
     */
    @NotBlank(message = "充值单号不能为空")
    private String gsrccVoucherId;
    /**
     * 充值门店
     */
    @NotBlank(message = "充值门店不能为空")
    private String gsrcpBrId;
    /**
     * 充值日期
     */
    @NotBlank(message = "充值日期不能为空")
    private String gsrcpDate;
    /**
     * 支付编码
     */
    @NotBlank(message = "支付编码不能为空")
    private String gsspmId;
    /**
     * 应收金额
     */
    @NotNull(message = "应收金额不能为空")
    private BigDecimal gsrcpYsAmt;

    /**
     * 充值金额
     */
    @NotNull(message = "充值金额不能为空")
    private BigDecimal gsrcpAfterAmt;

    /**
     * 退款金额
     */
    @NotNull(message = "退款金额不能为空")
    private BigDecimal refundYsAmt;
    /**
     * 退款方式
     */
    @NotBlank(message = "退款方式")
    private String refundMethodId;
}
