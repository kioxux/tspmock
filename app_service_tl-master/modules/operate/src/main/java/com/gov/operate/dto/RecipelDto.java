package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Auther: tzh
 * @Date: 2021/11/29 10:06
 * @Description: RecipelDto
 * @Version 1.0.0
 */
@Data
public class RecipelDto {
    //图片名称
    @NotBlank(message = "图片名称不为空")
    private String picName;
    //图片地址
    @NotBlank(message = "图片地址不为空")
    private String picAddress;
    @NotBlank(message = "部门id不为空")
    private String depId;
    @NotBlank(message = "登陆名不为空")
    private String loginName;
}
