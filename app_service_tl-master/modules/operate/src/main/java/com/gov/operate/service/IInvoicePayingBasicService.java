package com.gov.operate.service;

import com.gov.operate.entity.InvoicePayingBasic;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
public interface IInvoicePayingBasicService extends SuperService<InvoicePayingBasic> {

}
