package com.gov.operate.dto;

import com.gov.operate.entity.InvoicePayingBasic;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.12
 */
@Data
public class InvoicePayingBasicVO extends InvoicePayingBasic {

    /**
     * 门店件数
     */
    private Integer cnt;

    /**
     * 门店名称
     */
    private String ficoCompanyName;

    /**
     * 门店明细
     */
    private List<InvoicePayingBasicVO> invoicePayingBasicVOList;
}

