package com.gov.operate.dto.weChat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class GetCostomerMaterialListVO extends Pageable {

    @NotBlank(message = "客户不能为空")
    private String gwthCustomer;
    /**
     * 是否发布
     */
    private Integer gwthRelease;

    /**
     * 推送时间
     */
    private String gwthPushTimeStart;
    private String gwthPushTimeEnd;

    /**
     * 发布时间
     */
    private String gwthTimeStart;
    private String gwthTimeEnd;

    private String status;
}
