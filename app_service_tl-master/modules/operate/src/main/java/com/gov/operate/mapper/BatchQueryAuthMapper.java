package com.gov.operate.mapper;

import com.gov.operate.entity.BatchQueryAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 查询数据权限 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface BatchQueryAuthMapper extends BaseMapper<BatchQueryAuth> {

}
