package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ProductBusinessDTO{

    /**
     * 加盟商
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;
    /**
     * 商品编号
     */
    private String proSelfCode;
    /**
     * 通用名称
     */
    private String gsyCommonname;
    /**
     * 描述
     */
    private String gsyDepict;
    /**
     * 助记码
     */
    private String gsyPym;
    /**
     * 规格
     */
    private String gsySpecs;
    /**
     * 单位
     */
    private String gsyUnit;
    /**
     * 单位名称
     */
    private String unitName;

    /**
     * 地点编码
     */
    private String gsyStore;

    /**
     * 地点名称
     */
    private String gsyStoreName;
}
