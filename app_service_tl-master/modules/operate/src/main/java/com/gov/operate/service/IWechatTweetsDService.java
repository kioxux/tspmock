package com.gov.operate.service;

import com.gov.operate.entity.WechatTweetsD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 微信推文管理明细 服务类
 * </p>
 *
 * @author sy
 * @since 2021-01-07
 */
public interface IWechatTweetsDService extends SuperService<WechatTweetsD> {

}
