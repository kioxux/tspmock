package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.SmsEntity;
import com.gov.common.entity.invoice.InvoiceSendResponseDto;
import com.gov.operate.bsaoc.Feistel;
import com.gov.operate.dto.GetPayingItemDetailDTO;
import com.gov.operate.dto.InvoiceApplyDetailRequestDto;
import com.gov.operate.dto.InvoiceApplyOrderRequestDto;
import com.gov.operate.dto.InvoiceApplyRequestDto;
import com.gov.operate.entity.FicoInvoice;
import com.gov.operate.entity.PayingHeaderNew;
import com.gov.operate.entity.SmsTemplate;
import com.gov.operate.entity.UserData;
import com.gov.operate.mapper.*;
import com.gov.operate.service.IFicoInvoiceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-17
 */
@Slf4j
@Service
public class FicoInvoiceServiceImpl extends ServiceImpl<FicoInvoiceMapper, FicoInvoice> implements IFicoInvoiceService {

    @Resource
    private PayingHeaderNewMapper payingHeaderNewMapper;
    @Resource
    private PayingItemNewMapper payingItemNewMapper;
    @Resource
    private FicoInvoiceMapper ficoInvoiceMapper;
    @Resource
    private ApplicationConfig invoiceConfig;
    @Resource
    CosUtils cosUtils;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SmsTemplateMapper smsTemplateMapper;
    @Resource
    private SmsUtils smsUtils;

    @Override
    public void initInvoice() {
        log.info("发票请求任务开始。");
        {
            try {
                List<PayingHeaderNew> list = payingHeaderNewMapper.selectList(new QueryWrapper<PayingHeaderNew>()
                        .eq("FICO_PAYMENT_STATUS", "1")
                        .eq("FICO_INVOICE_STATUS", "0")
                        .eq("FICO_INVOICE_TYPE", "1")
                        .eq("FICO_CLASS", 1));
                log.info("请求发票数据：{}", JSONObject.toJSONString(list));
                if (!CollectionUtils.isEmpty(list)) {
                    for (PayingHeaderNew gaiaPayingHeader : list) {
                        // 订单开票状态
                        gaiaPayingHeader.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.INVOICING.getCode());
                        payingHeaderNewMapper.updateById(gaiaPayingHeader);
                        createInvoice(gaiaPayingHeader.getFicoId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.info("生成发票数据报错。");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            }
        }

        {
            // 开票中数据
            List<Map<String, String>> smsList = new ArrayList<>();
            List<FicoInvoice> items = ficoInvoiceMapper.selectList(new QueryWrapper<FicoInvoice>()
                    .eq("FICO_TYPE", "1")
                    .in("FICO_INVOICE_STATUS", "1", "3"));
            log.info("生成中发票数据：{}", JSONObject.toJSONString(items));
            if (!items.isEmpty()) {
                //过滤 发票订单号不为空、发票请求流水号不为空、pdf为空 的发票数据
                for (FicoInvoice item : items) {
                    List<Map<String, String>> itemList = new ArrayList<>();
                    Map<String, Object> map = new HashMap<>();
                    map.put("identity", invoiceConfig.getInvoiceAuth());
                    List<String> list = new ArrayList<>();
                    list.add(item.getFicoFpqqlsh());
                    map.put("fpqqlsh", list);
                    try {
                        log.info("发票流水号请求发票信息参数 map={}", JsonUtils.beanToJson(map));
                        InvoiceSendResponseDto resDto = InvoiceUtils.resSend(invoiceConfig, JsonUtils.beanToJson(map));
                        log.info("发票流水号请求发票信息返回值 resDto={}", resDto);
                        if (resDto != null && StringUtils.isNotBlank(resDto.getCMsg())) {
                            //更新 发票表
                            FicoInvoice record = new FicoInvoice();
                            BeanUtils.copyProperties(item, record);
                            record.setClient(item.getClient());
                            record.setFicoTaxSubjectCode(item.getFicoTaxSubjectCode());
                            record.setFicoKprq(resDto.getFicoFprq());
                            BeanUtils.copyProperties(resDto, record);
                            record.setFicoId(item.getFicoId());
                            //pdf地址
                            String cUrl = resDto.getCUrl();
                            log.info("发票流水号请求发票信息返回PDF地址 cUrl={}", cUrl);
                            //开票中
                            if (resDto.getCMsg().startsWith("开票完成")) {
                                if (StringUtils.isNotBlank(cUrl)) {
                                    record.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.SUCCESS.getCode());
                                    String path = invoiceConfig.getInvoicePath() + cUrl.substring(cUrl.lastIndexOf("/") + 1);
                                    log.info("PDF相对path={}", path);
                                    record.setFicoPdfUrl(path);
                                    //上传至腾讯云
                                    cosUtils.uploadFileInvoice(InvoiceUtils.parse(new URL(cUrl).openStream()), cUrl.substring(cUrl.lastIndexOf("/") + 1));
                                    List<PayingHeaderNew> phnList = payingHeaderNewMapper.selectList(new QueryWrapper<PayingHeaderNew>()
                                            .eq("CLIENT", item.getClient())
                                            .eq("FICO_ID", item.getFicoId())
                                    );
                                    if (CollectionUtils.isNotEmpty(phnList)) {
                                        PayingHeaderNew gaiaPayingHeader = phnList.get(0);
                                        List<UserData> userList = userDataMapper.selectList(new QueryWrapper<UserData>()
                                                .eq("CLIENT", item.getClient())
                                                .eq("USER_ID", gaiaPayingHeader.getFicoOperator())
                                        );

                                        if (CollectionUtils.isNotEmpty(userList)) {
                                            UserData gaiaUserData = userList.get(0);
                                            Map<String, String> smsMap = new HashMap<>();
                                            smsMap.put("date", record.getFicoKprq());
                                            smsMap.put("title", item.getFicoTaxSubjectName());
                                            Feistel feistel = new Feistel();
                                            String encrypt = feistel.encrypt(item.getFicoIndex().toString());
                                            smsMap.put("url", this.invoiceConfig.getDownloadPath() + encrypt);
                                            smsMap.put("phone", gaiaUserData.getUserTel());
                                            smsList.add(smsMap);
                                        }
                                    }
                                    record.setFicoOrderNo(item.getFicoOrderNo());
                                    record.setFicoInvoiceResult(JsonUtils.beanToJson(resDto));
                                    ficoInvoiceMapper.update(record, new QueryWrapper<FicoInvoice>()
                                            .eq("CLIENT", record.getClient())
                                            .eq("FICO_ID", record.getFicoId())
                                            .eq("FICO_INVOCE_LINE_NUM", record.getFicoInvoceLineNum())
                                            .eq("FICO_TAX_SUBJECT_CODE", record.getFicoTaxSubjectCode())
                                    );
                                }
                            } else if (resDto.getCMsg().contains("开票失败")) {
                                record.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.FAILE.getCode());
                                record.setFicoOrderNo(item.getFicoOrderNo());
                                record.setFicoInvoiceResult(JsonUtils.beanToJson(resDto));
                                ficoInvoiceMapper.update(record, new QueryWrapper<FicoInvoice>()
                                        .eq("CLIENT", record.getClient())
                                        .eq("FICO_ID", record.getFicoId())
                                        .eq("FICO_INVOCE_LINE_NUM", record.getFicoInvoceLineNum())
                                        .eq("FICO_TAX_SUBJECT_CODE", record.getFicoTaxSubjectCode())
                                );
                            }
                        }
                    } catch (Exception e) {
                        log.error("发票流水号调用发票结果查询失败！");
                        e.printStackTrace();
                        smsList.clear();
                        continue;
                    }
                }
            }
            log.info("发票请求任务结束。");

            // 发送短信
            if (CollectionUtils.isNotEmpty(smsList)) {
                try {
                    // 短息内容
                    List<SmsTemplate> smsTemplateList = smsTemplateMapper.selectList(new QueryWrapper<SmsTemplate>()
                            .eq("SMS_ID", Constants.SMSid.SMS0000010)
                    );
                    if (CollectionUtils.isNotEmpty(smsTemplateList)) {
                        SmsTemplate smsTemplate = smsTemplateList.get(0);
                        log.info("发送短信：{}", JSONObject.toJSONString(smsList));
                        // 发送短信
                        smsList.forEach(item -> {
                            SmsEntity smsEntity = new SmsEntity();
                            smsEntity.setSmsId(Constants.SMSid.SMS0000010);
                            smsEntity.setPhone(item.get("phone"));
                            String date = item.get("date");
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                            String sd = sdf.format(new Date(Long.parseLong(date))); // 时间戳转换日期
                            String message = com.gov.common.basic.StringUtils.parse(smsTemplate.getSmsContent(),
                                    this.invoiceConfig.getSgin() + StringUtils.right(StringUtils.left(sd, 6), 2),
                                    StringUtils.right(sd, 2),
                                    item.get("title"),
                                    item.get("url")
                            );
                            smsEntity.setMsg(message);
                            smsUtils.sendSms(smsEntity);
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    log.info("短信发送失败");
                }
            }
        }
    }

    /**
     * 发票生成
     *
     * @param ficoId
     */
    public void createInvoice(String ficoId) throws Exception {
        log.info("发票生成订单号：{}", ficoId);
        List<String> orderList = new ArrayList<>();
        // 付款主表
        PayingHeaderNew payingHeader = payingHeaderNewMapper.selectList(new QueryWrapper<PayingHeaderNew>()
                .eq("FICO_ID", ficoId)).get(0);
        //查询付款明细
        List<GetPayingItemDetailDTO> items = payingItemNewMapper.selectByClientAndPicoId(payingHeader.getClient(), ficoId);
        if (!items.isEmpty()) {
            for (GetPayingItemDetailDTO item : items) {
                // 订单号
                String orderNo = "";
                //打印发票数量
                int invoiceCount;
                //付款金额
                BigDecimal amount = item.getFicoPaymentAmount();
                //发票最大额度
                BigDecimal quota = new BigDecimal(invoiceConfig.getInvoiceMaxQuota());
                // 付款金额 <= 发票最大额度 开一张票  > 整数 + 1（余数 不为0.0000）
                if (amount.compareTo(quota) > 0) {
                    // 0取整  1取余
                    BigDecimal[] decimals = amount.divideAndRemainder(quota);
                    invoiceCount = Integer.valueOf(decimals[0].toString()) + (decimals[1].equals(new BigDecimal("0.0000")) ? 0 : 1);
                    for (int i = 0; i < invoiceCount; i++) {
                        while (true) {
                            orderNo = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
                            if (!orderList.contains(orderNo)) {
                                orderList.add(orderNo);
                                break;
                            }
                        }
                        item.setFicoOrderNo(orderNo);
                        //填写最大余额
                        item.setFicoPaymentAmount(quota);
                        //最后一张发票 填写余数
                        if (invoiceCount - 1 == i && !decimals[1].equals(new BigDecimal("0.0000"))) {
                            item.setFicoPaymentAmount(decimals[1]);
                        }
                        item.setFicoOperator(payingHeader.getFicoOperator());
                        insertItem(item, orderNo, i + 1, NumberUtils.toInt(payingHeader.getFicoInvoiceType()));
                    }
                } else {
                    while (true) {
                        orderNo = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
                        if (!orderList.contains(orderNo)) {
                            orderList.add(orderNo);
                            break;
                        }
                    }
                    item.setFicoOrderNo(orderNo);
                    item.setFicoOperator(payingHeader.getFicoOperator());
                    insertItem(item, orderNo, 1, NumberUtils.toInt(payingHeader.getFicoInvoiceType()));
                }
            }
        }
    }

    /**
     * 生成发票数据
     *
     * @param item
     * @param orderNo
     * @param ficoInvoiceLineNum
     * @throws Exception
     */
    private void insertItem(GetPayingItemDetailDTO item, String orderNo, Integer ficoInvoiceLineNum, Integer ficoType) throws Exception {
        String reqJson = getReqOrderJson(item, "信息技术服务");
        log.info("发票生成请求参数reqJson={}", reqJson);
        String fpqqlsh = InvoiceUtils.reqSend(invoiceConfig.getInvoiceRequestUrl(), reqJson);
        //获取发票请求流水号 新增发票表数据
        FicoInvoice gaiaFicoInvoice = new FicoInvoice();
        BeanUtils.copyProperties(item, gaiaFicoInvoice);
        gaiaFicoInvoice.setFicoTaxSubjectName(item.getFicoPayingstoreName());
        gaiaFicoInvoice.setFicoTaxSubjectCode(item.getFicoPayingstoreCode());
        gaiaFicoInvoice.setFicoInvoiceAmount(item.getFicoPaymentAmount());
        //发票行号
        log.info("发票生成请求返回值ficoInvoiceLineNum={}", ficoInvoiceLineNum.toString());
        gaiaFicoInvoice.setFicoInvoceLineNum(ficoInvoiceLineNum.toString());
        //未开票 fpqqlsh为failure 保存发票数据时，默认发票订单号、发票流水号都为空
//        if ("failure".equals(fpqqlsh)) {
        //直接未开票
        gaiaFicoInvoice.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
//        }
        //开票中、开票成功、开票失败  都会返回 fpqqlsh
        if (!"failure".equals(fpqqlsh) && StringUtils.isNotBlank(fpqqlsh)) {
            gaiaFicoInvoice.setFicoFpqqlsh(fpqqlsh);
            gaiaFicoInvoice.setFicoOrderNo(orderNo);
            gaiaFicoInvoice.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.INVOICING.getCode());
        }
        // 开票区分
        gaiaFicoInvoice.setFicoType(ficoType);
        if (CommonEnum.InvioceType.SPECIAL.getCode().equals(ficoType.toString())) {
            gaiaFicoInvoice.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
        }
        // 门店服务费
        gaiaFicoInvoice.setFicoClass(1);
        gaiaFicoInvoice.setFicoOperator(item.getFicoOperator());
        ficoInvoiceMapper.insert(gaiaFicoInvoice);
    }

    /**
     * 诺诺网开票请求入参 order
     */
    private String getReqOrderJson(GetPayingItemDetailDTO item, String goodsname) {
        String phone = invoiceConfig.getPhone();
        // 发票请求
        InvoiceApplyRequestDto reqDto = new InvoiceApplyRequestDto();
        reqDto.setIdentity(invoiceConfig.getInvoiceAuth());
        InvoiceApplyOrderRequestDto order = new InvoiceApplyOrderRequestDto();
        order.setBuyername(item.getFicoTaxSubjectName());
        //购方企业 税号、地址、开户行及账号、电话
        order.setTaxnum(item.getFicoSocialCreditCode());
        order.setAddress(item.getFicoCompanyAddress());
        order.setAccount((StringUtils.isBlank(item.getFicoBankName()) ? "" : item.getFicoBankName())
                + (StringUtils.isBlank(item.getFicoBankNumber()) ? "" : item.getFicoBankName()));
        order.setTelephone(item.getFicoCompanyPhoneNumber());
        order.setOrderno(item.getFicoOrderNo());
        order.setInvoicedate(DateUtils.getCurrentDateTimeStr());
        //销方企业税号、地址、开户行及账号、电话
        order.setSaletaxnum(invoiceConfig.getSaletaxnum());
        order.setSaleaddress(invoiceConfig.getSaleaddress());
        order.setSaleaccount(invoiceConfig.getSaleaccount());
        order.setSalephone(invoiceConfig.getSalephone());
        order.setKptype("1");
        //开票员 需要提供
        order.setClerk(invoiceConfig.getClerk());
        //推送手机(开票成功会短信提醒购方) 需要提供
        order.setPhone(phone);
        order.setQdbz("0");
        order.setDkbz("0");
        //发票类型 增值税普通发票 P 目前不支持增值税专用发票 TODO
        order.setInvoiceLine("p");
        order.setCpybz("0");
        order.setTsfs("-1");
        //发票明细
        List<InvoiceApplyDetailRequestDto> list = new ArrayList<>();
        InvoiceApplyDetailRequestDto dto = new InvoiceApplyDetailRequestDto();
        //商品名称 需要提
        dto.setGoodsname(goodsname);
        dto.setNum("1");
        dto.setPrice("1");
        dto.setPrice(item.getFicoPaymentAmount().toString());
        dto.setHsbz("1");
        dto.setTaxrate("0.06");
        dto.setSpec("");
        //单位：升或者吨
        dto.setUnit("");
        //税收分类编码   需要提供
        dto.setSpbm("3040203");
        dto.setFphxz("0");
        dto.setYhzcbs("0");
        list.add(dto);
        order.setDetail(list);
        reqDto.setOrder(order);
        //入参 json
        return JsonUtils.beanToJson(reqDto);
    }
}
