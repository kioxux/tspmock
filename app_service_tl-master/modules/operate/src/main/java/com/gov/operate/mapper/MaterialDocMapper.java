package com.gov.operate.mapper;

import com.gov.operate.entity.MaterialDoc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
public interface MaterialDocMapper extends BaseMapper<MaterialDoc> {

}
