package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdMarketingBasic;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface ISdMarketingBasicService extends SuperService<SdMarketingBasic> {

    /**
     * 营销设置列表
     */
    IPage<SdMarketingBasic> getGsmSettingList(GetGsmSettingListVO2 vo);

    /**
     * 营销设置保存
     */
    void saveGsmSetting(SaveGsmSettingVO2 vo);

    /**
     * 营销设置编辑
     */
    void editGsmSetting(EditGsmSettingVO2 vo);

    /**
     * 营销设置详情
     */
    GetGsmSettingDetailDTO2 getGsmSettingDetail(Integer id);

    /**
     * 营销设置删除
     */
    void deleteMarketSetting(Integer id);

    /**
     * 活动方案推送
     */
    void pushGsm(PushGsmVO2 vo);

    /**
     * 促销方式列表
     */
    List<GetPromotionListDTO> getPromotionList();

    /***********************************************************************/

    /**
     * 营销任务列表
     */
    IPage<GetGsmTaskListDTO2> getGsmTaskList(GsmTaskListVO2 vo);

    /**
     * 营销任务详情
     */
    GetGsmTaskDetailDTO getGsmTaskDetail(Integer id, Integer type, String stoCode);

    /**
     * 营销任务保存
     */
    void saveGsmTask(SaveGsmTaskVO vo);

    /**
     * 选品选择列表
     */
    IPage<GetProductListToCheckDTO> getProductListToCheck(GetProductListToCheckVO vo);

    /**
     * 赠品选择列表
     */
    IPage<GetProductListToCheckDTO> getGiveawayListToCheck(GetGiveawayListToCheckVO vo);
}
