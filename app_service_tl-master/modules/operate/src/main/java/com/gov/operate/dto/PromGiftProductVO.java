package com.gov.operate.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 赠品类促销
 * @author: yzf
 * @create: 2022-01-24 11:01
 */
@Data
public class PromGiftProductVO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gspgcVoucherId;

    /**
     * 行号
     */
    private String gspgcSerial;

    /**
     * 商品编码
     */
    private String gspgcProId;

    /**
     * 系列编码
     */
    private String gspgcSeriesId;

    /**
     * 是否会员
     */
    private String gspgcMemFlag;

    /**
     * 是否积分
     */
    private String gspgcInteFlag;

    /**
     * 积分倍率
     */
    private String gspgcInteRate;

    /**
     * 达到数量1
     */
    private String gspgsReachQty1;

    /**
     * 达到金额1
     */
    private BigDecimal gspgsReachAmt1;

    /**
     * 赠送数量1
     */
    private String gspgsResultQty1;

    /**
     * 达到数量2
     */
    private String gspgsReachQty2;

    /**
     * 达到金额2
     */
    private BigDecimal gspgsReachAmt2;

    /**
     * 赠送数量2
     */
    private String gspgsResultQty2;

    /**
     * 达到数量3
     */
    private String gspgsReachQty3;

    /**
     * 达到金额3
     */
    private BigDecimal gspgsReachAmt3;

    /**
     * 赠送数量3
     */
    private String gspgsResultQty3;

    /**
     * 赠品金额1
     */
    private BigDecimal gspgsResultPrc1;

    /**
     * 赠品折扣1
     */
    private BigDecimal gspgsResultRebate1;

    /**
     * 赠品金额2
     */
    private BigDecimal gspgsResultPrc2;

    /**
     * 赠品折扣2
     */
    private BigDecimal gspgsResultRebate2;

    /**
     * 赠品金额3
     */
    private BigDecimal gspgsResultPrc3;

    /**
     * 赠品折扣3
     */
    private BigDecimal gspgsResultRebate3;
}
