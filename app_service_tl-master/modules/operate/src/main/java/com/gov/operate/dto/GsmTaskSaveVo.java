package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class GsmTaskSaveVo {

    @NotBlank(message = "加盟商不能为空")
    private String client;

    @NotBlank(message = "营销主题不能为空")
    private String gsmMarketid;

    @NotBlank(message = "保存类型不能为空")
    private String saveType;

    @NotBlank(message = "活动名称不能为空")
    private String gsmThename;

    @NotBlank(message = "活动类型不能为空")
    private String gsmType;

    @NotBlank(message = "活动开始时间不能为空")
    private String gsmStartd;

    @NotBlank(message = "活动结束时间不能为空")
    private String gsmEndd;
    /**
     * 活动简介
     */
    private String gsmTheintro;

    /**
     * 门店编码
     */
    private String gsmStore;

    @NotNull(message = "销售额标值不能为空")
    private BigDecimal gsmIndexValue;

    @NotNull(message = "销售额活动值不能为空")
    private BigDecimal gsmTargetValue;

    @NotNull(message = "涨幅不能为空")
    private BigDecimal gsmSaleinc;

    /**
     * 客单价选择(0不选择，1选择)
     */
    private String gsmPricechoose;
    /**
     * 客单价/标值
     */
    private BigDecimal gsmPriceinc;
    /**
     * 客单价/活动值
     */
    private BigDecimal gsmActPriceinc;
    /**
     * 客单价对应交易次数修正值
     */
    private BigDecimal gsmPriceincSt;


    /**
     * 交易次数选择(0不选择，1选择)
     */
    private String gsmStchoose;
    /**
     * 交易次数
     */
    private BigDecimal gsmStinc;
    /**
     * 交易次数/活动值
     */
    private BigDecimal gsmActStinc;
    /**
     * 交易次数对应客单价修正值
     */
    private BigDecimal gsmStincPrice;

    /**
     * 毛利率
     */
    private BigDecimal gsmGrprofit;

    /**
     * 选品条件
     */
    private String gsmProcondi;

//    @NotBlank(message = "短信模板不能为空")
    private String gsmTempid;

//    @NotBlank(message = "短信查询条件不能为空")
    private String gsmSmscondi;

//    @NotBlank(message = "短信查询条件JSON不能为空")
    private String gsmSmscondiJson;

//    @NotNull(message = "短信发送人数不能为空")
    private BigDecimal gsmSmstimes;
    /**
     * 电话通知人数
     */
    private BigDecimal gsmTeltimes;
    /**
     * 电话查询条件
     */
    private String gsmTelcondi;

    /**
     * 电话查询条件Json
     */
    private String gsmTelcondiJson;
    /**
     * DM单打印张数
     */
    private BigDecimal gsmDmtimes;

    /**
     * 短信人集合
     */
    private List<String> smsList;

    /**
     * 选品集合
     */
    private List<PromDTO> productList;

    /**
     * 电话通知集合
     */
    private List<String> phoneList;

    /**
     * 折扣集合
     */
    private List<PromDTO> discountList;





}
