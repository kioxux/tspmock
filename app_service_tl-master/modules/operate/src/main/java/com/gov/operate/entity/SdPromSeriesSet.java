package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_SERIES_SET")
@ApiModel(value="SdPromSeriesSet对象", description="")
public class SdPromSeriesSet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPSS_VOUCHER_ID")
    private String gspssVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPSS_SERIAL")
    private String gspssSerial;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPSS_SERIES_ID")
    private String gspssSeriesId;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPSS_REACH_QTY1")
    private String gspssReachQty1;

    @ApiModelProperty(value = "达到金额1")
    @TableField("GSPSS_REACH_AMT1")
    private BigDecimal gspssReachAmt1;

    @ApiModelProperty(value = "结果减额1")
    @TableField("GSPSS_RESULT_AMT1")
    private BigDecimal gspssResultAmt1;

    @ApiModelProperty(value = "结果折扣1")
    @TableField("GSPSS_RESULT_REBATE1")
    private String gspssResultRebate1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPSS_REACH_QTY2")
    private String gspssReachQty2;

    @ApiModelProperty(value = "达到金额2")
    @TableField("GSPSS_REACH_AMT2")
    private BigDecimal gspssReachAmt2;

    @ApiModelProperty(value = "结果减额2")
    @TableField("GSPSS_RESULT_AMT2")
    private BigDecimal gspssResultAmt2;

    @ApiModelProperty(value = "结果折扣2")
    @TableField("GSPSS_RESULT_REBATE2")
    private String gspssResultRebate2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPSS_REACH_QTY3")
    private String gspssReachQty3;

    @ApiModelProperty(value = "达到金额3")
    @TableField("GSPSS_REACH_AMT3")
    private BigDecimal gspssReachAmt3;

    @ApiModelProperty(value = "结果减额3")
    @TableField("GSPSS_RESULT_AMT3")
    private BigDecimal gspssResultAmt3;

    @ApiModelProperty(value = "结果折扣3")
    @TableField("GSPSS_RESULT_REBATE3")
    private String gspssResultRebate3;


}
