package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.GetWechatDTO;
import com.gov.operate.dto.GetWechatVO;
import com.gov.operate.dto.OfficialAccountsDTO;
import com.gov.operate.dto.QueryWechatListVO;
import com.gov.operate.dto.weChat.AppidAndSecretDTO;
import com.gov.operate.dto.weChat.GetCostomerDropDownList;
import com.gov.operate.dto.weChat.GetCustomerTotalAndPushedDO;
import com.gov.operate.entity.OfficialAccounts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-28
 */
public interface OfficialAccountsMapper extends BaseMapper<OfficialAccounts> {

    /**
     * 公众号列表
     */
    IPage<OfficialAccountsDTO> queryWechatList(Page<OfficialAccountsDTO> page,
                                               @Param("vo") QueryWechatListVO vo,
                                               @Param("client") String client,
                                               @Param("goaTypeToMyBatis") String goaTypeToMyBatis);

    /**
     * 公众号数据获取
     */
    GetWechatDTO getWechat(@Param("vo") GetWechatVO vo);

    /**
     * 公众号appid 和 secret
     *
     * @param client
     * @param goaCode
     * @return
     */
    AppidAndSecretDTO getAppidAndSecret(@Param("client") String client, @Param("goaCode") String goaCode);

    /**
     * 客户下拉框
     *
     * @param client
     * @return
     */
    List<GetCostomerDropDownList> getCostomerDropDownList(@Param("client") String client);

    /**
     * 客户列表
     *
     * @param client
     * @param mediaId
     * @return
     */
    List<OfficialAccountsDTO> getCostomerList(@Param("client") String client, @Param("mediaId") String mediaId);

    /**
     * 发布的数量
     *
     *
     * @param client
     * @param mediaIdList
     * @return
     */
    List<GetCustomerTotalAndPushedDO> getCustomerPushedCount(@Param("client") String client, @Param("mediaIdList") List<String> mediaIdList);

    /**
     * 总的客户数
     *
     * @param client
     * @return
     */
    int getCustomerTotalCount(@Param("client") String client);

    /**
     * WECHAT_OFFICIAL_WECHAT_TWEETS = 1 的数据
     *
     * @return
     */
    List<OfficialAccounts> getWechatConfigTweetsList();
}
