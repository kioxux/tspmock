package com.gov.operate.dto.storePayment;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.entity.SmsSend;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.17
 */
@Data
public class InvoiceSms {

    /**
     * 开票短信
     */
    private IPage<SmsSend> iPage;

    /**
     * 总金额
     */
    private Integer amt;
}

