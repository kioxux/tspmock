package com.gov.operate.service.invoiceOptimiz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.dto.SelectWarehousingDetailsDTO;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDto;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.16
 */
public interface DocumentBillOptimizService {
    /**
     * 单据列表
     *
     * @param vo
     * @return
     */
    IPage<SelectWarehousingDTO> selectDocumentList(SelectWarehousingVO vo);

    /**
     * 单据合计
     * @param vo
     * @return
     */
    Result selectDocumentTotal(SelectWarehousingVO vo);

    /**
     * 单据对账
     *
     * @param documentBillDto
     */
    void documentBill(DocumentBillDto documentBillDto);

    /**
     * 单据已登记 发票
     *
     * @param type
     * @param matDnId
     * @return
     */
    Result selectInvoiceListByDoc(String type, String matDnId);

    /**
     * 单据可登记 发票
     *
     * @param pageNum
     * @param pageSize
     * @param siteCode
     * @param supCode
     * @param invoiceNum
     * @return
     */
    Result selectInvoiceList(Integer pageNum, Integer pageSize,String siteCode, String supCode, String invoiceNum,
                             String invoiceDateStart, String invoiceDateEnd);

    /**
     * 发票绑定
     * @param selectWarehousingDTO
     */
    void bindInvoice(SelectWarehousingDTO selectWarehousingDTO);

    /**
     * 单据胆细导出
     * @param detailList
     * @return
     */
    Result exportDetailList(List<SelectWarehousingDetailsDTO> detailList) throws IOException;

}
