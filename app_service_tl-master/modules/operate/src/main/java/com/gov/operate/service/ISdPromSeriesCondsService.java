package com.gov.operate.service;

import com.gov.operate.entity.SdPromSeriesConds;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
public interface ISdPromSeriesCondsService extends SuperService<SdPromSeriesConds> {

}
