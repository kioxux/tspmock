package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING_GIVEAWAY")
@ApiModel(value="SdMarketingGiveaway对象", description="")
public class SdMarketingGiveaway extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "营销活动加盟商分配表主键")
    @TableField("MARKETING_ID")
    private Integer marketingId;

    @ApiModelProperty(value = "促销关联表")
    @TableField("PROM_ID")
    private Integer promId;

    @ApiModelProperty(value = "商品编码")
    @TableField("PRO_CODE")
    private String proCode;


}
