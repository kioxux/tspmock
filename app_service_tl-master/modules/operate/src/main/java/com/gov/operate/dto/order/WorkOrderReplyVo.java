package com.gov.operate.dto.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.List;

/**
 * @author 钱金华
 * @date 21-2-18 14:29
 */
@Data
public class WorkOrderReplyVo extends WorkOrderReply {



    /**
     * 发送时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private String gworSendTimeStr;

    /**
     * 客服名称
     */
    private String replyUserName;


    /**
     * 工单处理附件
     */
    List<WorkOrderReplyAtt> workOrderReplyAtts;

}
