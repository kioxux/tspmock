package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.GetProListForSaveGroupVO;
import com.gov.operate.dto.ProductBusinessDTO;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.entity.ProductBusiness;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface IProductBusinessService extends SuperService<ProductBusiness> {

    /**
     * 商品组添加查询列表
     */
    IPage<ProductBusinessDTO> getProListForSaveGroup(GetProListForSaveGroupVO vo);

    /**
     * 选品查询
     */
    IPage<ProductBusiness> getMarketProductList(ProductRequestVo productRequestVo);
}
