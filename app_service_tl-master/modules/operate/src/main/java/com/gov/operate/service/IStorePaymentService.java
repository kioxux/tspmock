package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.InvoicePayingBasicVO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.28
 */
public interface IStorePaymentService {

    /**
     * 付款组织
     *
     * @param type
     * @param ficoPayingstoreCode
     * @param ficoPayingstoreName
     * @param validDate
     * @return
     */
    Result storePaymentList(String type, String ficoPayingstoreCode, String ficoPayingstoreName, Integer validDate);

    /**
     * 门店付款提醒
     */
    void payReminder();

    /**
     * 合并纳税列表
     *
     * @return
     */
    Result getPayGroup();

    /**
     * 合并纳税明细
     *
     * @param ficoCompanyCode
     * @return
     */
    Result getPayGroupItem(String ficoCompanyCode);

    /**
     * 同一连锁下的组织
     *
     * @param ficoCompanyCode
     * @return
     */
    Result getPayList(String ficoCompanyCode);

    /**
     * 纳税合并保存
     *
     * @param invoicePayingBasicVO
     * @return
     */
    Result savePayItem(InvoicePayingBasicVO invoicePayingBasicVO);
}

