package com.gov.operate.dto.invoice;

import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.entity.SupplierPrepayment;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.02.01
 */
@Data
public class SupplierPrepaymentDto extends SupplierPrepayment {

    /**
     * 地点名
     */
    private String companyName;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 业务员姓名
     */
    private String gssName;

    /**
     * 采购员
     */
    private String userNam;

    /**
     * 序号
     */
    private Integer index;

    /**
     * 核销金额
     */
    private BigDecimal writeOffAmount;
    /**
     * 差异金额
     */
    private BigDecimal diffAmount;

    private List<SelectWarehousingDTO> poList;

    /**
     * 银行代码
     */
    private String gspSupBankCode;

    /**
     * 采购订单号
     */
    private List<String> poIds;

    /**
     * 备注
     */
    private String remark;

    /**
     * 工作流描述
     */
    private String wfDescription;
}

