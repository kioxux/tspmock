package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.AuthVO;
import com.gov.operate.dto.auth.ClientAuth;
import com.gov.operate.entity.AuthorityAuths;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface AuthorityAuthsMapper extends BaseMapper<AuthorityAuths> {

    List<AuthVO> SelectForAuthList(Map<Object, Object> map);

    List<AuthVO> SelectRoleAuthList(Map<Object, Object> map);

    List<String> SelectCountExit(Map<Object, Object> map);

    Integer SelectPayExit(Map<Object, Object> map);

    List<ClientAuth> selectAuthByClient(@Param("client") String client);

    List<String> selectAuthByUserClient(@Param("client") String client);
}
