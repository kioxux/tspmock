package com.gov.operate.dto.marketing;

import com.gov.operate.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetGsmTaskDetailDTO3 extends SdMarketingClient {

    /**
     * 门店编码（当前/历史营销用到）
     */
    private String stoCode;
    /**
     * 门店名称（当前/历史营销用到）
     */
    private String stoName;

    /**
     * 短信查询条件id （当前/历史营销用到）
     */
    private List<SdMarketingSearch> gsmSmscondiList;
    /**
     * 电话查询条件id （当前/历史营销用到）
     */
    private List<SdMarketingSearch> gsmTelcondiList;

    /**
     * 0:加盟商负责人/委托人 1:以外
     */
    private Integer legalPerson;

    /**
     * 宣传物料
     */
    List<SdMarketingBasicAttacDTO> marketingBasicAttacList;
    /**
     * 门店列表 （推荐营销用到）
     */
    private List<StoreData> stoList;

}
