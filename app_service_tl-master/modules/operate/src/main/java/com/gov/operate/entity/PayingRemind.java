package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 加盟商余额
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYING_REMIND")
@ApiModel(value="PayingRemind对象", description="加盟商余额")
public class PayingRemind extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "服务费退款余额")
    @TableField("GPR_SERVICE_CHARGE")
    private BigDecimal gprServiceCharge;


}
