package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 权限分配表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
@RestController
@RequestMapping("/operate/authconfi-data")
public class AuthconfiDataController {

}

