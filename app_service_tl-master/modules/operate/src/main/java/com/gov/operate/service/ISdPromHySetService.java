package com.gov.operate.service;

import com.gov.operate.entity.SdPromHySet;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface ISdPromHySetService extends SuperService<SdPromHySet> {

}
