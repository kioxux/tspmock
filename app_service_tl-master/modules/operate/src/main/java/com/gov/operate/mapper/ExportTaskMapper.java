package com.gov.operate.mapper;

import com.gov.operate.entity.ExportTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 导出报表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2022-01-20
 */
public interface ExportTaskMapper extends BaseMapper<ExportTask> {

}
