package com.gov.operate.mapper;

import com.gov.operate.entity.MessageTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.PushMsgUserWithRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
public interface MessageTemplateMapper extends BaseMapper<MessageTemplate> {

    /**
     * 获取需要推送的用户
     *
     * @param gmtId   gmtId
     * @param client client
     * @return List<PushMsgUserWithRule>
     */
    List<PushMsgUserWithRule> selectPushMsgUserWithRule(@Param("gmtId") String gmtId, @Param("client") String client);

    /**
     * 获取消息模板默认需要推送的用户
     *
     * @param client client
     * @param gmtId   gmtId
     * @return List<PushMsgUserWithRule>
     */
    List<PushMsgUserWithRule> selectPushMsgUserWithDefault(@Param("gmtId") String gmtId, @Param("client") String client);

    /**
     * 获取生效中的加盟商
     *
     * @param gmtId   gmtId
     * @return List<PushMsgUserWithRule>
     */
    List<String> selectClientsWithEffect(@Param("gmtId") String gmtId);


}
