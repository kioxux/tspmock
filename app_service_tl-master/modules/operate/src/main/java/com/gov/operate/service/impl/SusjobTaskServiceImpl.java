package com.gov.operate.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.enums.SqlLike;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.Pageable;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdSusjobD;
import com.gov.operate.entity.SdSusjobH;
import com.gov.operate.entity.SdTagList;
import com.gov.operate.entity.UserData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdSusjobDMapper;
import com.gov.operate.mapper.SdSusjobHMapper;
import com.gov.operate.mapper.SdTagListMapper;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISusjobTaskService;
import com.gov.operate.service.request.SdSusjobHRequest;
import com.gov.operate.service.request.SelectQuerySusjobMemberRequest;
import com.gov.operate.service.request.SelectStatusCountRequset;
import com.gov.operate.service.request.UpdateStatusRequest;
import com.gov.operate.service.response.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */
@Service
public class SusjobTaskServiceImpl implements ISusjobTaskService {

    @Resource
    private CommonService commonService;
    @Resource
    private SdSusjobHMapper sdSusjobHMapper;
    @Resource
    private SdSusjobDMapper sdSusjobDMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SdTagListMapper sdTagListMapper;

    /**
     * 会员维系任务
     *
     * @param susjobTaskListQuery
     * @return
     */
    @Override
    public Result querySusjobTaskList(SusjobTaskListQuery susjobTaskListQuery) {
        // 获取当前人
        TokenUser user = commonService.getLoginInfo();
        // 判断参数门店是否有权限
        // TODO
        susjobTaskListQuery.setClient(user.getClient());
        susjobTaskListQuery.setUserId(user.getUserId());
        Page<SusjobTaskListQuery> page = new Page<>(susjobTaskListQuery.getPageNum(), susjobTaskListQuery.getPageSize());
        IPage<SdSusjobH> ipage = sdSusjobHMapper.querySusjobTaskList(page, susjobTaskListQuery);
        return ResultUtil.success(ipage);
    }

    /**
     * 会员维系任务明细
     *
     * @param susjobDetailQuery
     * @return
     */
    @Override
    public Result querySusjobDetails(SusjobDetailsQuery susjobDetailQuery) {
        Page<SusjobDetailsQuery> page = new Page<>(susjobDetailQuery.getPageNum(), susjobDetailQuery.getPageSize());
        IPage<SusjobDetails> ipage = sdSusjobDMapper.querySusjobDetails(page, susjobDetailQuery);
        return ResultUtil.success(ipage);
    }

    /**
     * 维系提交保存
     *
     * @param submitSusjobDetail
     * @return
     */
    @Override
    public Result submitSusjobDetail(SubmitSusjobDetail submitSusjobDetail) {
        // 获取当前人
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<SdSusjobD> sdSusjobDQueryWrapper = new QueryWrapper();
        // 加盟商
        sdSusjobDQueryWrapper.eq("CLIENT", submitSusjobDetail.getClient());
        // 任务ID
        sdSusjobDQueryWrapper.eq("SUS_JOBID", submitSusjobDetail.getSusJobid());
        // 会员卡号
        sdSusjobDQueryWrapper.eq("SUS_CARD_ID", submitSusjobDetail.getSusCardId());
        // 任务门店
        sdSusjobDQueryWrapper.eq("SUS_JOBBR", submitSusjobDetail.getSusJobbr());
        SdSusjobD sdSusjobD = sdSusjobDMapper.selectOne(sdSusjobDQueryWrapper);
        // 维护的数据不存在
        if (sdSusjobD == null) {
            throw new CustomResultException("修改的数据不存在");
        }
        // 其他人已维护过当前数据
        if (StringUtils.isNotBlank(sdSusjobD.getSusUserid()) && !sdSusjobD.getSusUserid().equals(user.getUserId())) {
            throw new CustomResultException("当前会员数据已被其他人员维护");
        }
        // 维系标签
        sdSusjobD.setSusFlgadd(submitSusjobDetail.getSusFlgadd());
        // 维系结果
        sdSusjobD.setSusJobflg(submitSusjobDetail.getSusJobflg());
        // 文字维护
        sdSusjobD.setSusJobtxt(submitSusjobDetail.getSusJobtxt());
        // 不再维系
        sdSusjobD.setSusFreeze(submitSusjobDetail.getSusFreeze());
        // 维系人员工号
        sdSusjobD.setSusUserid(user.getUserId());
        // 维系人员姓名
        QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper();
        // 加盟商
        userDataQueryWrapper.eq("CLIENT", user.getClient());
        // 员工编号
        userDataQueryWrapper.eq("USER_ID", user.getUserId());
        // 员工编号
        UserData userData = userDataMapper.selectOne(userDataQueryWrapper);
        if (userData != null) {
            sdSusjobD.setSusUsername(userData.getUserNam());
        }
        // 维系时间
        sdSusjobD.setSusJobtime(DateUtils.getCurrentDateTimeStrTwo());
        // 是否提交
        sdSusjobD.setSusFinish("1");
        sdSusjobDMapper.update(sdSusjobD, sdSusjobDQueryWrapper);
        // 总件数
        QueryWrapper<SdSusjobD> susjobDQueryWrapper = new QueryWrapper();
        // 加盟商
        susjobDQueryWrapper.eq("CLIENT", submitSusjobDetail.getClient());
        // 任务ID
        susjobDQueryWrapper.eq("SUS_JOBID", submitSusjobDetail.getSusJobid());
        // 任务门店
        susjobDQueryWrapper.eq("SUS_JOBBR", submitSusjobDetail.getSusJobbr());
        // 总件数
        int count = sdSusjobDMapper.selectCount(susjobDQueryWrapper);
        // 是否提交
        susjobDQueryWrapper.eq("SUS_FINISH", "1");
        int finishCount = sdSusjobDMapper.selectCount(susjobDQueryWrapper);
        // 任务表更新
        QueryWrapper<SdSusjobH> sdSusjobHQueryWrapper = new QueryWrapper();
        // 加盟商
        sdSusjobHQueryWrapper.eq("CLIENT", submitSusjobDetail.getClient());
        // 任务ID
        sdSusjobHQueryWrapper.eq("SUS_JOBID", submitSusjobDetail.getSusJobid());
        // 任务门店
        sdSusjobHQueryWrapper.eq("SUS_JOBBR", submitSusjobDetail.getSusJobbr());
        SdSusjobH sdSusjobH = new SdSusjobH();
        // 进度
        sdSusjobH.setSusSche(finishCount + "/" + count);
        sdSusjobHMapper.update(sdSusjobH, sdSusjobHQueryWrapper);
        return ResultUtil.success();
    }

    @Override
    public Result queryMySusjobTaskList(Pageable pageable) {
        // 获取当前人
        TokenUser user = commonService.getLoginInfo();
        Page<SdSusjobH> page = new Page<>(pageable.getPageNum(), pageable.getPageSize());
        IPage<SdSusjobH> ipage = sdSusjobHMapper.queryMySusjobTaskList(page, user.getClient(), user.getUserId());
        return ResultUtil.success(ipage);
    }

    @Override
    public Result getTagList() {
        // TODO 获取标签列表
        QueryWrapper<SdTagList> sdTagListQueryWrapper = new QueryWrapper();
        sdTagListQueryWrapper.orderByAsc("GSTL_ID");
        List<SdTagList> list = sdTagListMapper.selectList(sdTagListQueryWrapper);
        return ResultUtil.success(list);
    }

    @Override
    public Result querySusjobTaskListNew(SusjobTaskListQuery susjobTaskListQuery) {
        if (CollectionUtils.isEmpty(susjobTaskListQuery.getStoCodeList())) {
            susjobTaskListQuery.setStoCodeList(commonService.getStoCodeList());
        }
        if (CollectionUtils.isEmpty(susjobTaskListQuery.getStoCodeList())) {
            return ResultUtil.success(new Page<SdSusjobH>());
        }

        // 获取当前人
        TokenUser user = commonService.getLoginInfo();
        susjobTaskListQuery.setClient(user.getClient());
        susjobTaskListQuery.setUserId(user.getUserId());
        Page<SusjobTaskListQuery> page = new Page<>(susjobTaskListQuery.getPageNum(), susjobTaskListQuery.getPageSize());
        IPage<SdSusjobH> ipage = sdSusjobHMapper.querySusjobTaskList(page, susjobTaskListQuery);
        return ResultUtil.success(ipage);
    }

    /**
     * 会员维系任务列表
     * @param type 0.我的维度任务 1.会员维度任务
     * @return
     */
    @Override
    public Result querySusjobTaskAppList(SdSusjobHRequest request) {
        // 获取当前人
        TokenUser user =commonService.getLoginInfo();
        /*TokenUser user =new TokenUser();//commonService.getLoginInfo();
        user.setClient("10000029");
        user.setUserId("1047");*/
        Page<SdSusjobHResponse> iPage = new Page<>(request.getPageNum(), request.getPageSize());
        //获取维系任务列表
        IPage<SdSusjobHResponse> susJobHIPage = sdSusjobHMapper.querySusjobTaskAppList(iPage, user.getClient(), user.getUserId(), request);
        //获取会员回访的数量
        Integer date = getDate();
        if (Objects.nonNull(susJobHIPage) && CollUtil.isNotEmpty(susJobHIPage.getRecords())) {
            List<SdSusjobHResponse> records = susJobHIPage.getRecords();
            List<SdSusjobHResponse> getTaskListCount = sdSusjobHMapper.getTaskCount(records.stream().map(SdSusjobHResponse::getSusJobid).collect(Collectors.toList()),user.getClient(),user.getUserId(),request.getStatus());
            if (CollUtil.isNotEmpty(getTaskListCount)) {
                records.forEach(rds -> {
                    //任务总数量
                    Integer totalTaskCount = 0;
                    //已经拨打包括已接听无应答已拦截
                    Integer taskCount = 0;
                    for (SdSusjobHResponse response : getTaskListCount) {
                        if (Objects.equals(rds.getClient(), response.getClient()) && Objects.equals(rds.getSusJobid(), response.getSusJobid()) && Objects.equals(rds.getSusJobbr(), response.getSusJobbr())) {
                            totalTaskCount += 1;
                            if (!Objects.equals(response.getSusTaskType(), "0")) {
                                taskCount += 1;
                            }
                        }
                    }
                    if (Objects.equals(request.getTaskType(), "1")) {
                        rds.setSusTaskTypeStr("已过期");
                        rds.setSusTaskTypeCode("3");
                    } else {


                        if (Convert.toInt(rds.getSusBegin()) <= date && Convert.toInt(rds.getSusEnd()) >= date) {
                            //说明任务开始了
                            if (Objects.equals(rds.getSusTaskType(), "1")) {
                                rds.setSusTaskTypeStr("进行中");
                                rds.setSusTaskTypeCode("2");
                            } else {
                                rds.setSusTaskTypeStr("未查看");
                                rds.setSusTaskTypeCode("1");
                            }
                        } else if (Convert.toInt(rds.getSusBegin()) >= date) {
                            rds.setSusTaskTypeStr("未开始");
                            rds.setSusTaskTypeCode("0");
                        }
                    }
                    //rds.setSusTaskTypeStr(Objects.equals(request.getTaskType(),"1")?"已过期":rds.getSusTaskTypeStr());
                    String startDate = DateUtil.format(DateUtil.parse(rds.getSusBegin(), "yyyyMMdd"), "yyyy-MM-dd");
                    String endDate = DateUtil.format(DateUtil.parse(rds.getSusEnd(), "yyyyMMdd"), "yyyy-MM-dd");
                    rds.setDate(startDate + "至" + endDate);
                    rds.setSusBeginStr(startDate);
                    rds.setSusEndStr(endDate);
                    rds.setTaskCount(taskCount);
                    rds.setTotalTaskCount(totalTaskCount);
                });
            }
        }
        return ResultUtil.success(susJobHIPage);
    }

    /**
     * 历史维系任务列表
     * @param request stoCode门店编码   taskName任务名称模糊查询
     * @return
     */
    @Override
    public Result querySusjobTaskAppHistoryList(SdSusjobHRequest request) {


        return null;
    }

    /**
     * 修改会员维系状态
     * @param taskType 1进行中
     * @return
     */
    @Override
    public Result updateQuerySusjobTaskApp(String jobId,String taskType) {
        // 获取当前人
        TokenUser loginInfo = commonService.getLoginInfo();
        LambdaQueryWrapper<SdSusjobH> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SdSusjobH::getSusJobid,jobId);
        queryWrapper.eq(SdSusjobH::getClient,loginInfo.getClient());
        List<SdSusjobH> sdSusjobHList = sdSusjobHMapper.selectList(queryWrapper);
        if(CollUtil.isEmpty(sdSusjobHList)){
            throw new CustomResultException("会员维度任务不存在");
        }
        if(Convert.toInt(sdSusjobHList.get(0).getSusEnd())<getDate()){
            throw new CustomResultException("会员维系任务已过期,不可更改");
        }
        if(Convert.toInt(sdSusjobHList.get(0).getSusBegin())>getDate()){
            throw new CustomResultException("会员维系任务未开始,不可更改");
        }
        SdSusjobH susjobH = new SdSusjobH();
        susjobH.setSusTaskType(taskType);
        sdSusjobHMapper.update(susjobH,queryWrapper);
        return ResultUtil.success();
    }

    /**
     * 校验数据
     * @param jobId
     * @return
     */
    private List<SdSusjobH>  check(String jobId,String client){
        LambdaQueryWrapper<SdSusjobH> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SdSusjobH::getSusJobid,jobId)
                    .eq(SdSusjobH::getClient,client);
        List<SdSusjobH> sdSusjobHList = sdSusjobHMapper.selectList(queryWrapper);
        if(CollUtil.isEmpty(sdSusjobHList)){
            throw new CustomResultException("会员维度任务不存在");
        }
        return sdSusjobHList;
    }

    /**
     * 会员维系任务会员列表
     *
     * @param request
     * @return
     */
    @Override
    public Result selectQuerySusjobMemberApp(SelectQuerySusjobMemberRequest request) {
        // 获取当前人
        TokenUser user =commonService.getLoginInfo();
        /*TokenUser user =new TokenUser();//commonService.getLoginInfo();
        user.setClient("10000029");
        user.setUserId("1047");*/
        List<SdSusjobH> check = check(request.getJobId(), user.getClient());
        IPage<SelectQuerySusjobMemberResponse> responseIPage = new Page<>();
        Integer startAge = 0;
        Integer endAge = 0;
        //获取当前年份
        int date = DateUtil.year(DateUtil.date());
        String str = "[0-9]+";
        if (CollUtil.isNotEmpty(request.getNumberStage())) {
            if (request.getNumberStage().size() < 2) {
                throw new CustomResultException("编号传入不合法!");
            } else {
                //判断是否是自然数
                if (!Convert.toStr(request.getNumberStage().get(0)).matches(str)) {
                    throw new CustomResultException("编号传入正正数!");
                }
                if (!Convert.toStr(request.getNumberStage().get(1)).matches(str)) {
                    throw new CustomResultException("编号传入正正数!");
                }
                if (request.getNumberStage().get(0) > request.getNumberStage().get(1)) {
                    throw new CustomResultException("前编号必须小于等于后编号");
                }
            }
        }
        if (CollUtil.isNotEmpty(request.getAgeStage())) {
            if (request.getAgeStage().size() < 2) {
                throw new CustomResultException("年龄段传入不合法!");
            } else {
                if (!Convert.toStr(request.getAgeStage().get(0)).matches(str)) {
                    throw new CustomResultException("年龄传入正正数!");
                }
                if (!Convert.toStr(request.getAgeStage().get(1)).matches(str)) {
                    throw new CustomResultException("年龄传入正正数!");
                }
                if (request.getAgeStage().get(0) > request.getAgeStage().get(1)) {
                    throw new CustomResultException("年龄必须小于等于后年龄");
                }
                startAge = date - request.getAgeStage().get(0);
                endAge = date - request.getAgeStage().get(1);
            }
        }
        Page<SdSusjobD> page = new Page<>(request.getPageNum(), request.getPageSize());
        String startAges = CollUtil.isNotEmpty(request.getAgeStage())?Convert.toStr(endAge).concat("0000"):null;
        String endAges = CollUtil.isNotEmpty(request.getAgeStage())?Convert.toStr(startAge).concat("1223"):null;
        Integer startNumber = CollUtil.isNotEmpty(request.getNumberStage()) ? request.getNumberStage().get(0):null;
        Integer endNumber = CollUtil.isNotEmpty(request.getNumberStage()) ? request.getNumberStage().get(1):null;
        SdSusjobH susjobH = check.get(0);
        IPage<SdSusjobD> susJobDPage = this.sdSusjobDMapper
                .selectPages(page,request.getJobId(),user.getClient(),user.getUserId(),request.getSusStatus(),request.getSusSex(),request.getStoCode(),startNumber,endNumber,startAges,endAges,susjobH);
        BeanUtil.copyProperties(susJobDPage, responseIPage);
        if (Objects.nonNull(susJobDPage) && CollUtil.isNotEmpty(susJobDPage.getRecords())) {
            responseIPage.setRecords(susJobDPage.getRecords().stream().map(sus -> {
                SelectQuerySusjobMemberResponse response = new SelectQuerySusjobMemberResponse();
                BeanUtil.copyProperties(sus, response);
                response.setGsmbMemberId(sus.getSusCardId());
                response.setAge(StrUtil.isBlank(sus.getSusBirth()) ? null : Convert.toStr(date - (DateUtil.year(DateUtil.parse(sus.getSusBirth(), "yyyyMMdd")))).concat("岁"));
                return response;
            }).collect(Collectors.toList()));
        }
        return ResultUtil.success(responseIPage);
    }


    /**
     * 当前日期格式化
     * @return
     */
    private Integer getDate() {
        return Convert.toInt(DateUtil.format(DateUtil.parse(DateUtil.today(), "yyyy-MM-dd"), "yyyyMMdd"));
    }

    /**
     * 修改接听状态
     * @param request 请求参数
     * @return
     */
    @Override
    public Result updateStatus(UpdateStatusRequest request) {
        TokenUser user = commonService.getLoginInfo();
        check(request.getJobId(),user.getClient());
        //会员用户是否存在
        LambdaQueryWrapper<SdSusjobD> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper
                .eq(SdSusjobD::getSusJobid,request.getJobId())
                .eq(SdSusjobD::getSusMobile,request.getMobile())
                .eq(SdSusjobD::getSusJobbr,request.getStoCode());
        List<SdSusjobD> susJobDList = this.sdSusjobDMapper.selectList(lambdaQueryWrapper);
        if(CollUtil.isEmpty(susJobDList)){
            throw new CustomResultException("维系任务门店下会员不存在");
        }
        //修改
        SdSusjobD sdSusjobD = new SdSusjobD();
        sdSusjobD.setSusStatus(request.getSusStatus());
        if(CollUtil.isNotEmpty(request.getFlgaddType())){
            sdSusjobD.setSusFlgaddType(StringUtils.join(request.getFlgaddType(), ","));
        }
        sdSusjobD.setSusFeedback(request.getSusFeedback());
        sdSusjobD.setSusFeedbacktime(DateUtil.format(new Date(), "yyyyMMdd"));
        sdSusjobDMapper.update(sdSusjobD,lambdaQueryWrapper);
        return ResultUtil.success();
    }

    /**
     * 任务详情
     * @param jobId 任务ID
     * @return
     */
    @Override
    public Result selectTaskDetail(String jobId) {

        TokenUser user = commonService.getLoginInfo();
        List<SdSusjobH> check = check(jobId, user.getClient());
        SdSusjobH susjobH = check.get(0);
        SelectTaskDetailResponse response = new SelectTaskDetailResponse();
        BeanUtil.copyProperties(susjobH,response);
        String startTime = DateUtil.format(DateUtil.parse(response.getSusBegin(), "yyyyMMdd"), "yyyy-MM-dd");
        String endTime = DateUtil.format(DateUtil.parse(response.getSusEnd(), "yyyyMMdd"), "yyyy-MM-dd");
        response.setDate(startTime+"至"+endTime);
        return ResultUtil.success(response);
    }

    @Override
    public Result selectStatusCount(SelectStatusCountRequset request) {
         TokenUser user =commonService.getLoginInfo();
      /*  //user.setClient("10000003");
        TokenUser user =new TokenUser();//commonService.getLoginInfo();
        user.setClient("10000029");
        user.setUserId("1047");*/
        List<SdSusjobH> check = check(request.getJobId(), user.getClient());
        List<SelectStatusCountResponse> responseList = new ArrayList<>();
        List<SdSusjobD> susJobDList = getSdSusjobD(request, user,check);
        Map<String,Integer> map = new HashMap();
        map.put("0",0);
        map.put("1",0);
        map.put("2",0);
        map.put("3",0);
        if(CollUtil.isNotEmpty(susJobDList)){
            susJobDList.forEach(sus->{
                if(Objects.equals(sus.getSusStatus(),"0")){
                    map.put("0",map.get("0")+1);
                }else if(Objects.equals(sus.getSusStatus(),"1")){
                    map.put("1",map.get("1")+1);
                }else if(Objects.equals(sus.getSusStatus(),"2")){
                    map.put("2",map.get("2")+1);
                }else if(Objects.equals(sus.getSusStatus(),"3")){
                    map.put("3",map.get("3")+1);
                }
            });
        }
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            SelectStatusCountResponse response = new SelectStatusCountResponse();
            response.setSusStatus(entry.getKey());
            response.setSusStatusCount(Convert.toStr(entry.getValue()));
            responseList.add(response);
        }
        return ResultUtil.success(responseList);
    }

    private List<SdSusjobD> getSdSusjobD(SelectStatusCountRequset request,TokenUser user,List<SdSusjobH> check){

        SdSusjobH susjobH = check.get(0);
        Integer startAge = 0;
        Integer endAge = 0;
        //获取当前年份
        int date = DateUtil.year(DateUtil.date());
        String str = "[0-9]+";
        if (CollUtil.isNotEmpty(request.getNumberStage())) {
            if (request.getNumberStage().size() < 2) {
                throw new CustomResultException("编号传入不合法!");
            } else {
                //判断是否是自然数
                if (!Convert.toStr(request.getNumberStage().get(0)).matches(str)) {
                    throw new CustomResultException("编号传入正正数!");
                }
                if (!Convert.toStr(request.getNumberStage().get(1)).matches(str)) {
                    throw new CustomResultException("编号传入正正数!");
                }
                if (request.getNumberStage().get(0) > request.getNumberStage().get(1)) {
                    throw new CustomResultException("前编号必须小于等于后编号");
                }
            }
        }
        if (CollUtil.isNotEmpty(request.getAgeStage())) {
            if (request.getAgeStage().size() < 2) {
                throw new CustomResultException("年龄段传入不合法!");
            } else {
                if (!Convert.toStr(request.getAgeStage().get(0)).matches(str)) {
                    throw new CustomResultException("年龄传入正正数!");
                }
                if (!Convert.toStr(request.getAgeStage().get(1)).matches(str)) {
                    throw new CustomResultException("年龄传入正正数!");
                }
                if (request.getAgeStage().get(0) > request.getAgeStage().get(1)) {
                    throw new CustomResultException("年龄必须小于等于后年龄");
                }
                startAge = date - request.getAgeStage().get(0);
                endAge = date - request.getAgeStage().get(1);
            }
        }
        String startAges = CollUtil.isNotEmpty(request.getAgeStage())?Convert.toStr(endAge).concat("0000"):null;
        String endAges = CollUtil.isNotEmpty(request.getAgeStage())?Convert.toStr(startAge).concat("1223"):null;
        Integer startNumber = CollUtil.isNotEmpty(request.getNumberStage()) ? request.getNumberStage().get(0):null;
        Integer endNumber = CollUtil.isNotEmpty(request.getNumberStage()) ? request.getNumberStage().get(1):null;
       List<SdSusjobD> susJobDPage = this.sdSusjobDMapper
                .selectLists(request.getJobId(),user.getClient(),user.getUserId(),request.getSusSex(),request.getStoCode(),startNumber,endNumber,startAges,endAges,susjobH);
        return susJobDPage;
    }


    /**
     * 会员用户信息
     * @param cardId 会员卡号
     * @return
     */
    @Override
    public Result selectCardUserDetail(String cardId) {
        TokenUser user = commonService.getLoginInfo();
        SelectCardUserDetailResponse selectCardUserDetail = this.sdSusjobDMapper.selectCardUserDetail(cardId, user.getClient());
        if (Objects.nonNull(selectCardUserDetail)) {
            if(StrUtil.isNotBlank(selectCardUserDetail.getLastSaleDate())){
                selectCardUserDetail.setLastSaleDate(DateUtil.format(DateUtil.parse(selectCardUserDetail.getLastSaleDate(), "yyyyMMdd"), "yyyy-MM-dd"));
            }
            if(StrUtil.isNotBlank(selectCardUserDetail.getGsmbcCreateDate())){
                selectCardUserDetail.setGsmbcCreateDate(DateUtil.format(DateUtil.parse(selectCardUserDetail.getGsmbcCreateDate(), "yyyyMMdd"), "yyyy-MM-dd"));
            }
            int date = DateUtil.year(DateUtil.date());
            selectCardUserDetail.setGsmbBirth(StrUtil.isBlank(selectCardUserDetail.getGsmbBirth()) ? null : Convert.toStr(date - DateUtil.year(DateUtil.parse(selectCardUserDetail.getGsmbBirth(), "yyyyMMdd"))));
            // 获取最后消费的信息
            SelectCardUserDetailResponse saleInfoDTO = sdSusjobDMapper.querySaleInfoByHykNo(user.getClient(), selectCardUserDetail.getGsmbcCardId());
            if (Objects.nonNull(saleInfoDTO)) {
                BeanUtils.copyProperties(saleInfoDTO, selectCardUserDetail);
                LambdaQueryWrapper<SdSusjobD> lambdaQueryWrapper = new LambdaQueryWrapper<>();
                lambdaQueryWrapper
                        .eq(SdSusjobD::getSusJobid, selectCardUserDetail.getGsmbcCardId())
                        .eq(SdSusjobD::getClient, user.getClient());
                List<SdSusjobD> susJobDList = this.sdSusjobDMapper.selectList(lambdaQueryWrapper);
                if (CollUtil.isNotEmpty(susJobDList)) {
                    SdSusjobD sdSusjobD = susJobDList.get(0);
                    if(StrUtil.isNotBlank(sdSusjobD.getSusFlgaddType())){
                        selectCardUserDetail.setSusFlgaddType(Arrays.asList(sdSusjobD.getSusFlgaddType().split(",")));
                    }
                    if(StrUtil.isNotBlank(sdSusjobD.getSusFlg())){
                        selectCardUserDetail.setSusFlg(Arrays.asList(sdSusjobD.getSusFlg().split(",")));
                    }
                    selectCardUserDetail.setSusFeedback(sdSusjobD.getSusFeedback());
                }
            }
        }
        return ResultUtil.success(selectCardUserDetail);
    }

    @Override
    public ConsumeInfoDTO getOrderInfoById(String cardId, String startDate, String endDate) {
        TokenUser user = commonService.getLoginInfo();
        // 根据会员卡号获取消费订单列表
        //List<OrderInfoDTO> orderInfoDTOS = sdSusjobDMapper.queryOrderInfoById("10000001","3010003", "20191110", "20211213");
        List<OrderInfoDTO> orderInfoDTOS = sdSusjobDMapper.queryOrderInfoById(user.getClient(),cardId, startDate, endDate);
        BigDecimal sumOfMoney = new BigDecimal(0);
        ConsumeInfoDTO consumeInfoDTO = new ConsumeInfoDTO();
        if (CollUtil.isNotEmpty(orderInfoDTOS)){
            for (OrderInfoDTO orderInfoDTO : orderInfoDTOS) {
                sumOfMoney =sumOfMoney.add(orderInfoDTO.getGsshYsAmt());
            }
            consumeInfoDTO.setCount(orderInfoDTOS.size());
            consumeInfoDTO.setSumOfMoney(sumOfMoney);
            consumeInfoDTO.setOrderInfoDTOS(orderInfoDTOS);
        }
        return consumeInfoDTO;
    }

}
