package com.gov.operate.dto.marketing;

import io.swagger.models.auth.In;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 14:02 2021/8/16
 * @Description：查询商品分类数据入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GetProductClassPageDto {

    private List<String> queryStringList;
    private String queryString;
    @NotNull(message = "分页参数不能为空")
    private Integer  pageNum;
    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;
}
