package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.FeedbackDTO;
import com.gov.operate.dto.GetFeedBackDTO;
import com.gov.operate.dto.GetFeedBackListVO;
import com.gov.operate.dto.ImagesDTO;
import com.gov.operate.entity.Feedback;
import com.gov.operate.entity.Images;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.FeedbackMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IFeedbackService;
import com.gov.operate.service.IImagesService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {

    @Resource
    private FeedbackMapper feedbackMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private IImagesService imagesService;
    @Resource
    private CosUtils cosUtils;

    /**
     * 建议反馈列表
     */
    @Override
    public IPage<FeedbackDTO> getFeedBackList(GetFeedBackListVO vo) {
        TokenUser user = commonService.getLoginInfo();

        // type预留字段 设置默认值
        if (StringUtils.isEmpty(vo.getType())) {
            vo.setType("1");
        }
        Page<FeedbackDTO> page = new Page<FeedbackDTO>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<FeedbackDTO> query = new QueryWrapper<FeedbackDTO>()
                .apply(StringUtils.isNotEmpty(vo.getTitle()), "instr(fe.TITLE, {0})", vo.getTitle())
                .eq("fe.CLIENT", user.getClient())
                .eq(StringUtils.isNotEmpty(vo.getType()), "fe.TYPE", vo.getType())
                .eq(StringUtils.isNotEmpty(vo.getUserId()), "fe.USER_ID", vo.getUserId())
                .ge(StringUtils.isNotEmpty(vo.getUserCreDateStart()), "fe.USER_CRE_DATE", vo.getUserCreDateStart())
                .le(StringUtils.isNotEmpty(vo.getUserCreDateEnd()), "fe.USER_CRE_DATE", vo.getUserCreDateEnd());

        return feedbackMapper.getFeedBackList(page, query);
    }

    /**
     * 建议反馈详情
     */
    @Override
    public GetFeedBackDTO getFeedBack(String id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        GetFeedBackDTO feedback = feedbackMapper.getById(id);
        if (ObjectUtils.isEmpty(feedback)) {
            throw new CustomResultException("获取详情失败");
        }
        // 培训图片
        QueryWrapper<Images> queryImage = new QueryWrapper<Images>()
                .eq("ID", id)
                .orderByAsc("SORT");
        List<Images> imagesList = imagesService.list(queryImage);
        if (CollectionUtils.isEmpty(imagesList)) {
            return feedback;
        }

        // 图片绝对路径添加
        List<ImagesDTO> imagesDTOList = new ArrayList<>();
        imagesList.forEach(images -> {
            ImagesDTO dto = new ImagesDTO();
            BeanUtils.copyProperties(images, dto);
            dto.setPathUrl(cosUtils.urlAuth(images.getPath()));
            imagesDTOList.add(dto);
        });
        feedback.setImagesList(imagesDTOList);
        return feedback;
    }

    /**
     * 建议反馈删除
     */
    @Override
    public void removeFeedBack(String id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        this.removeById(id);
    }
}
