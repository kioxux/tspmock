package com.gov.operate.service;

import com.gov.operate.entity.MessageTemplate;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
public interface IMessageTemplateService extends SuperService<MessageTemplate> {

}
