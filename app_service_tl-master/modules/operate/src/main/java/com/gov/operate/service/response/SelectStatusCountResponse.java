package com.gov.operate.service.response;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Objects;

@Data
public class SelectStatusCountResponse {

    @ApiModelProperty(value = "会员回访类型0待回访1已接听2无应答3已拦截")
    private String susStatus;


    public String getSusStatusStr() {
        if (StrUtil.isNotBlank(susStatus)) {
            if (Objects.equals(susStatus, "0")) {
                susStatusStr = "待回访";
            } else if (Objects.equals(susStatus, "1")) {
                susStatusStr = "已接听";
            } else if (Objects.equals(susStatus, "2")) {
                susStatusStr = "无应答";
            } else if (Objects.equals(susStatus, "3")) {
                susStatusStr = "已拦截";
            }
        }
        return susStatusStr;
    }

    @ApiModelProperty(value = "会员回访类型0待回访1已接听2无应答3已拦截")
    private String susStatusStr;

    @ApiModelProperty(value = "数量")
    private String susStatusCount;




}
