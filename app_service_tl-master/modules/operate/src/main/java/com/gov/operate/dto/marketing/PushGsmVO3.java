package com.gov.operate.dto.marketing;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class PushGsmVO3 {

    @NotNull(message = "id不能为空")
    private Integer id;

    @NotEmpty(message = "所选加盟商集合不能为空")
    private List<String> clientList;

    @NotNull(message = "推送时间不能为空(0:立即推送，1:定时推送)")
    private Integer releType;

    /**
     * 计划推送时间
     */
    private String gsmPlanPushTime;


}
