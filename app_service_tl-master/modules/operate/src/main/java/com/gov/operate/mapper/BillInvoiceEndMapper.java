package com.gov.operate.mapper;

import com.gov.operate.dto.invoice.BillEndBatchVO;
import com.gov.operate.entity.BillInvoiceEnd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-25
 */
public interface BillInvoiceEndMapper extends BaseMapper<BillInvoiceEnd> {

    List<BillInvoiceEnd> getBillInvoiceEndList(@Param("client") String client, @Param("gbieType") int gbieType, @Param("list") List<BillEndBatchVO> list);

    /**
     * 批量新增
     */
    void insertBatch(@Param("list") List<BillInvoiceEnd> collect);
}
