package com.gov.operate.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.utils.CommonUtil;
import com.gov.operate.dto.ProductInfoVo;
import com.gov.operate.dto.SdPromGroupProDto;
import com.gov.operate.dto.SdPromGroupProVo;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SdPromGroupPro;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ProductBusinessMapper;
import com.gov.operate.mapper.SdPromGroupProMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdPromGroupProService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 促销商品分明细组表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
@Service
public class SdPromGroupProServiceImpl extends ServiceImpl<SdPromGroupProMapper, SdPromGroupPro> implements ISdPromGroupProService {

    @Resource
    private CommonService commonService;
    @Resource
    private ISdPromGroupProService sdPromGroupProService;
    @Resource
    private ProductBusinessMapper productBusinessMapper;

    /**
     * 商品列表
     *
     * @param groupId
     * @param proClass
     * @param proBrandClass
     * @return
     */
    @Override
    public Result getSdPromGroupProList( String groupId, String proClass, String proBrandClass) {
        TokenUser user = commonService.getLoginInfo();
        List<ProductBusiness> list = baseMapper.getSdPromGroupProList(user.getClient(), groupId);
        return ResultUtil.success(list);
    }

    /**
     * 商品删除
     *
     * @param groupId
     * @param proList
     * @return
     */
    @Override
    public Result delSdPromGroupPro(String groupId, List<String> proList) {
        if (CollectionUtils.isEmpty(proList)) {
            return ResultUtil.success();
        }
        TokenUser user = commonService.getLoginInfo();
        baseMapper.delete(new LambdaQueryWrapper<SdPromGroupPro>().eq(SdPromGroupPro::getClient, user.getClient())
                .eq(SdPromGroupPro::getGroupId, groupId)
                .in(SdPromGroupPro::getGroupProId, proList));
        return ResultUtil.success();
    }

    /**
     * 商品保存
     *
     * @param groupId
     * @param proList
     * @return
     */
    @Override
    public Result saveSdPromGroupPro(String groupId, List<String> proList) {
        if (CollectionUtils.isEmpty(proList)) {
            return ResultUtil.success();
        }
        TokenUser user = commonService.getLoginInfo();
        baseMapper.delete(new LambdaQueryWrapper<SdPromGroupPro>().eq(SdPromGroupPro::getClient, user.getClient())
                .eq(SdPromGroupPro::getGroupId, groupId));
        List<SdPromGroupPro> list = new ArrayList<>();
        for (String proCode : proList) {
            SdPromGroupPro sdPromGroupPro = new SdPromGroupPro();
            sdPromGroupPro.setClient(user.getClient());
            sdPromGroupPro.setGroupId(groupId);
            sdPromGroupPro.setGroupProId(proCode);
            list.add(sdPromGroupPro);
        }
        sdPromGroupProService.saveBatch(list);
        return ResultUtil.success();
    }

    @Override
    public Result getProInfoList(SdPromGroupProDto dto) {
        if(ObjectUtil.isNotEmpty(dto.getClassArr())){
            dto.setProClass(CommonUtil.twoDimensionToOneDimensionArrar(dto.getClassArr()));
        }
        TokenUser user = commonService.getLoginInfo();
        dto.setClient(user.getClient());
        Page<SdPromGroupProVo> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        List<String> dcList = productBusinessMapper.getDcListByClient(dto.getClient());
        dto.setDcList(dcList);
        IPage<SdPromGroupProVo> list = productBusinessMapper.getProInfoList(page,dto);
        return ResultUtil.success(list);
    }

    @Override
    public Result getProductInfoList(Integer pageNum, Integer pageSize,String content) {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        List<String> dcList = productBusinessMapper.getDcListByClient(user.getClient());
        Page<ProductInfoVo> page = new Page<>(pageNum, pageSize);
        IPage<ProductInfoVo> list = productBusinessMapper.getProductInfo(page,client,dcList,content);
        Map<String,Object> map = new HashMap<>();
        if (list.getSize() > 0){
            map.put("list",list.getRecords());
            map.put("total",list.getTotal());
        }
        return ResultUtil.success(map);
    }

    @Override
    public Result getProBrandClass() {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        List<String> list = productBusinessMapper.getProBrandClass(client);
        return ResultUtil.success(list);
    }
}
