package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PAYMENT_METHOD")
@ApiModel(value="SdPaymentMethod对象", description="")
public class SdPaymentMethod extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店")
    @TableField("GSPM_BR_ID")
    private String gspmBrId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPM_SERIAL")
    private String gspmSerial;

    @ApiModelProperty(value = "编号")
    @TableField("GSPM_ID")
    private String gspmId;

    @ApiModelProperty(value = "名称")
    @TableField("GSPM_NAME")
    private String gspmName;

    @ApiModelProperty(value = "类型")
    @TableField("GSPM_TYPE")
    private String gspmType;

    @ApiModelProperty(value = "是否为接口")
    @TableField("GSPM_FALG")
    private String gspmFalg;

    @ApiModelProperty(value = "储值卡充值是否可用")
    @TableField("GSPM_RECHARGE")
    private String gspmRecharge;

    @ApiModelProperty(value = "财务客户编码")
    @TableField("GSPM_FI_ID")
    private String gspmFiId;

    @ApiModelProperty(value = "备注")
    @TableField("GSPM_REMARK")
    private String gspmRemark;


}
