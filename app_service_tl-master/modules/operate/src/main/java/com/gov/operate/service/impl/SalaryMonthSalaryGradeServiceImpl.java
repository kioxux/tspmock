package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryMonthSalaryGrade;
import com.gov.operate.mapper.SalaryMonthSalaryGradeMapper;
import com.gov.operate.service.ISalaryMonthSalaryGradeService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryMonthSalaryGradeServiceImpl extends ServiceImpl<SalaryMonthSalaryGradeMapper, SalaryMonthSalaryGrade> implements ISalaryMonthSalaryGradeService {

    /**
     * 薪资方案月工资级别
     */
    @Override
    public void saveSalaryGrade(EditProgramVO vo) {

        Integer gspId = vo.getGspId();
        // 删除基本工资级别 原始数据
        this.remove(new QueryWrapper<SalaryMonthSalaryGrade>().eq("GSMSG_GSP_ID", gspId));
        List<SalaryMonthSalaryGrade> salaryMonthSalaryGradeList = vo.getSalaryMonthSalaryGradeList();
        if (CollectionUtils.isEmpty(salaryMonthSalaryGradeList)) {
            throw new CustomResultException("基本工资级别 数据不能为空");
        }
        for (int i = 0; i < salaryMonthSalaryGradeList.size(); i++) {
            SalaryMonthSalaryGrade salaryMonthSalaryGrade = salaryMonthSalaryGradeList.get(i);
            if (StringUtils.isEmpty(salaryMonthSalaryGrade.getGsmsgGrade())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条,工资级别不能为空", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryMonthSalaryGrade.getGsmsgBasicWage())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条,基本工资不能为空", i + 1));
            }
            salaryMonthSalaryGrade.setGsmsgGspId(gspId);
            salaryMonthSalaryGrade.setGsmsgSort(i + 1);
        }
        this.saveBatch(salaryMonthSalaryGradeList);

    }

    /**
     * 薪资方案列表
     */
    @Override
    public List<SalaryMonthSalaryGrade> getSalaryGradeList(Integer gspId) {
        List<SalaryMonthSalaryGrade> list = this.list(new QueryWrapper<SalaryMonthSalaryGrade>()
                .eq("GSMSG_GSP_ID", gspId)
                .orderByAsc("GSMSG_SORT"));
        return list;
    }
}
