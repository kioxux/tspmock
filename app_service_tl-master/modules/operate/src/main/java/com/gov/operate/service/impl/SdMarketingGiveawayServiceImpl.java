package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMarketingGiveaway;
import com.gov.operate.mapper.SdMarketingGiveawayMapper;
import com.gov.operate.service.ISdMarketingGiveawayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Service
public class SdMarketingGiveawayServiceImpl extends ServiceImpl<SdMarketingGiveawayMapper, SdMarketingGiveaway> implements ISdMarketingGiveawayService {

}
