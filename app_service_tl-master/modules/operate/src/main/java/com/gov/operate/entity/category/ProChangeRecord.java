package com.gov.operate.entity.category;

import lombok.Data;

/**
 * 商品信息变更记录
 */

@Data
public class ProChangeRecord {
    private String client;
    private String stoCode;
    private String proCode;
    private String changeField;
    private String proChangeFrom;
    private String proChangeTo;
    private String userId;
    private String aplConText;
    private String proName;
    private String proSpecs;
    private String proFactoryName;
    private String proRegisterNo;
}
