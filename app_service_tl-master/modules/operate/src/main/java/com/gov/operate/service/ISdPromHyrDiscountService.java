package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SdPromHyrDiscount;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface ISdPromHyrDiscountService extends SuperService<SdPromHyrDiscount> {

}
