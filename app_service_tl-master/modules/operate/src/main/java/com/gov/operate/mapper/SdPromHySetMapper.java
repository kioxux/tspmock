package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromHySet;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface SdPromHySetMapper extends BaseMapper<SdPromHySet> {

    IPage<ProductResponseVo> selectProductList(Page<SdPromHySet> page, @Param("product") ProductRequestVo productRequestVo);

    void saveOtherStoHySet(@Param("client") String client, @Param("firstVoucherId") String firstVoucherId, @Param("otherVoucherId") String otherVoucherId);
}
