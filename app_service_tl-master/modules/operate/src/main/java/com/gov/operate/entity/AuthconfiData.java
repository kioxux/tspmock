package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 权限分配表
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHCONFI_DATA")
@ApiModel(value="AuthconfiData对象", description="权限分配表")
public class AuthconfiData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商ID")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "权限组(岗位)编号")
    @TableField("AUTH_GROUP_ID")
    private String authGroupId;

    @ApiModelProperty(value = "权限组(岗位)描述")
    @TableField("AUTH_GROUP_NAME")
    private String authGroupName;

    @ApiModelProperty(value = "SITE/门店/部门")
    @TableField("AUTHOBJ_SITE")
    private String authobjSite;

    @ApiModelProperty(value = "人员")
    @TableField("AUTHCONFI_USER")
    private String authconfiUser;


}
