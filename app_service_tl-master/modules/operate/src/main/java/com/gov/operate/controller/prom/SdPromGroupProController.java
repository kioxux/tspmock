package com.gov.operate.controller.prom;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.SdPromGroupProDto;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.ISdPromGroupProService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 促销商品分明细组表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
@RestController
@RequestMapping("sdPromGroupPro")
public class SdPromGroupProController {

    @Resource
    private ISdPromGroupProService sdPromGroupProService;

    @Log("促销商品组商品列表")
    @ApiOperation(value = "促销商品组商品列表")
    @PostMapping("getSdPromGroupProList")
    public Result getSdPromGroupProList(
                                        @RequestJson(value = "groupId") String groupId,
                                        @RequestJson(value = "proClass", required = false) String proClass,
                                        @RequestJson(value = "proBrandClass", required = false) String proBrandClass) {
        return sdPromGroupProService.getSdPromGroupProList(groupId, proClass, proBrandClass);
    }

    @Log("促销商品组商品删除")
    @ApiOperation(value = "促销商品组商品删除")
    @PostMapping("delSdPromGroupPro")
    public Result delSdPromGroupPro(@RequestJson(value = "groupId") String groupId, @RequestJson(value = "proList") List<String> proList) {
        return sdPromGroupProService.delSdPromGroupPro(groupId, proList);
    }

    @Log("促销商品组商品保存")
    @ApiOperation(value = "促销商品组商品保存")
    @PostMapping("saveSdPromGroupPro")
    public Result saveSdPromGroupPro(@RequestJson(value = "groupId") String groupId, @RequestJson(value = "proList") List<String> proList) {
        return sdPromGroupProService.saveSdPromGroupPro(groupId, proList);
    }

    @PostMapping({"getProInfoList"})
    public Result getProInfoList(@RequestBody SdPromGroupProDto dto){
        return sdPromGroupProService.getProInfoList(dto);
    }

    @PostMapping({"getProductInfoList"})
    public Result getProductInfoList(@RequestJson("pageNum") Integer pageNum,
                                     @RequestJson("pageSize") Integer pageSize,
                                     @RequestJson(value = "content",required = false) String content){
        return sdPromGroupProService.getProductInfoList(pageNum,pageSize,content);
    }

    @PostMapping({"getProBrandClass"})
    public Result getProBrandClass(){
        return sdPromGroupProService.getProBrandClass();
    }
}

