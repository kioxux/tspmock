package com.gov.operate.mapper;

import com.gov.operate.entity.SalesInvoiceDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-02-07
 */
public interface SalesInvoiceDetailMapper extends BaseMapper<SalesInvoiceDetail> {

    List<SalesInvoiceDetail> selectItems(@Param("client") String client, @Param("matLineNo") String matLineNo, @Param("matDnId") String matDnId,
                                         @Param("matSiteCode") String matSiteCode, @Param("stoCode") String stoCode, @Param("matProCode") String matProCode);

    List<SalesInvoiceDetail> selectItemsNew(@Param("client") String client, @Param("matLineNo") String matLineNo, @Param("matDnId") String matDnId,
                                         @Param("matSiteCode") String matSiteCode, @Param("stoCode") String stoCode, @Param("matProCode") String matProCode);

    /**
     * 开票中的数据
     * @return
     */
    List<SalesInvoiceDetail> selectInvoiceing();
}
