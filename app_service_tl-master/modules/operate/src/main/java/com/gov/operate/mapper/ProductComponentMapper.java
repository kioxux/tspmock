package com.gov.operate.mapper;

import com.gov.operate.entity.ProductComponent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
public interface ProductComponentMapper extends BaseMapper<ProductComponent> {

}
