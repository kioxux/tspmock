package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryMonthAppraisalSalary;
import com.gov.operate.mapper.SalaryMonthAppraisalSalaryMapper;
import com.gov.operate.service.ISalaryMonthAppraisalSalaryService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryMonthAppraisalSalaryServiceImpl extends ServiceImpl<SalaryMonthAppraisalSalaryMapper, SalaryMonthAppraisalSalary> implements ISalaryMonthAppraisalSalaryService {

    /**
     * 月考评工资
     */
    @Override
    public void saveAppraial(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除基本工资级别 原始数据
        this.remove(new QueryWrapper<SalaryMonthAppraisalSalary>().eq("GSMAS_GSP_ID", gspId));
        List<SalaryMonthAppraisalSalary> salaryMonthAppraisalSalaryList = vo.getSalaryMonthAppraisalSalaryList();
        if (CollectionUtils.isEmpty(salaryMonthAppraisalSalaryList)) {
            throw new CustomResultException("月评工资列表 数据不能为空");
        }
        for (int i = 0; i < salaryMonthAppraisalSalaryList.size(); i++) {
            SalaryMonthAppraisalSalary salaryMonthAppraisalSalary = salaryMonthAppraisalSalaryList.get(i);
            String jobName = salaryMonthAppraisalSalary.getGsmasJobName();
            if (StringUtils.isEmpty(jobName)) {
                throw new CustomResultException(MessageFormat.format("月评工资第[{0}]条 岗位名称不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryMonthAppraisalSalary.getGsmasJob())) {
                throw new CustomResultException(MessageFormat.format("[{0}]岗位编码不能为空", jobName));
            }
            if (ObjectUtils.isEmpty(salaryMonthAppraisalSalary.getGsmasWage())) {
                throw new CustomResultException(MessageFormat.format("[{0}]月评工资不能为空", jobName));
            }
            salaryMonthAppraisalSalary.setGsmasGspId(gspId);
            salaryMonthAppraisalSalary.setGsmsgSort(i + 1);
        }
        this.saveBatch(salaryMonthAppraisalSalaryList);

    }

    /**
     * 月考评工资列表
     */
    @Override
    public List<SalaryMonthAppraisalSalary> getAppraisalSalaryList(Integer gspId) {
        List<SalaryMonthAppraisalSalary> list = this.list(new QueryWrapper<SalaryMonthAppraisalSalary>()
                .eq("GSMAS_GSP_ID", gspId)
                .orderByAsc("GSMSG_SORT"));
        return list;
    }
}
