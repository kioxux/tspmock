package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromCouponSet;
import com.gov.operate.mapper.SdPromCouponSetMapper;
import com.gov.operate.service.ISdPromCouponSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Service
public class SdPromCouponSetServiceImpl extends ServiceImpl<SdPromCouponSetMapper, SdPromCouponSet> implements ISdPromCouponSetService {

}
