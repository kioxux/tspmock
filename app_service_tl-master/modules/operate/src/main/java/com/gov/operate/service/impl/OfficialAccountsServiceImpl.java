package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gov.common.basic.*;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.weChat.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.weChat.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.CompadmMapper;
import com.gov.operate.mapper.OfficialAccountsMapper;
import com.gov.operate.mapper.StoreDataMapper;
import com.gov.operate.mapper.WechatTweetsHMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IOfficialAccountsService;
import com.gov.operate.service.IWechatTweetsDService;
import com.gov.operate.service.IWechatTweetsHService;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-28
 */
@Slf4j
@Service
public class OfficialAccountsServiceImpl extends ServiceImpl<OfficialAccountsMapper, OfficialAccounts> implements IOfficialAccountsService {

    @Resource
    private OfficialAccountsMapper officialAccountsMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private CompadmMapper compadmMapper;
    @Resource
    private WeChatUtils weChatUtils;
    @Resource
    private RedisClient redisClient;
    @Resource
    private IWechatTweetsDService wechatTweetsDService;
    @Resource
    private IWechatTweetsHService wechatTweetsHService;
    @Resource
    private WechatTweetsHMapper wechatTweetsHMapper;
    @Resource
    private ApplicationConfig applicationConfig;
    /**
     * 公众号定时推送发布(一个公众号 一个推文 一个事务)(一个类是面，自己注入自己)
     */
    @Resource
    private IOfficialAccountsService officialAccountsService;


    /**
     * 公众号列表
     */
    @Override
    public IPage<OfficialAccountsDTO> queryWechatList(QueryWechatListVO vo) {
        String goaTypeToMyBatis = null;
        if (ObjectUtils.isEmpty(vo.getGoaType())) {

        } else if (1 == vo.getGoaType()) {
            goaTypeToMyBatis = "1";
        } else if (2 == vo.getGoaType()) {
            goaTypeToMyBatis = "2";
        } else {
            throw new CustomResultException("类型填写不正确");
        }
        TokenUser user = commonService.getLoginInfo();

        Page<OfficialAccountsDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<OfficialAccountsDTO> ipage = officialAccountsMapper.queryWechatList(page, vo, user.getClient(), goaTypeToMyBatis);
        ipage.getRecords().forEach(item -> {
            if (3 == item.getGoaType()) {
                item.setGoaType(1);
            }
            if (!ObjectUtils.isEmpty(item.getLogo())) {
                String logoUrl = cosUtils.urlAuth(item.getLogo());
                item.setLogoUrl(logoUrl);
            }
        });
        return ipage;
    }

    /**
     * 公众号列表(平台用户)
     *
     * @param vo
     * @return
     */
    @Override
    public IPage<OfficialAccountsDTO> queryWechatListByPlatform(QueryWechatListVO vo) {

        String goaTypeToMyBatis = null;
        if (ObjectUtils.isEmpty(vo.getGoaType())) {

        } else if (1 == vo.getGoaType()) {
            goaTypeToMyBatis = "1";
        } else if (2 == vo.getGoaType()) {
            goaTypeToMyBatis = "2";
        } else {
            throw new CustomResultException("类型填写不正确");
        }

        Page<OfficialAccountsDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<OfficialAccountsDTO> ipage = officialAccountsMapper.queryWechatList(page, vo, vo.getClient(), goaTypeToMyBatis);
        ipage.getRecords().forEach(item -> {
            if (3 == item.getGoaType()) {
                item.setGoaType(1);
            }
            if (!ObjectUtils.isEmpty(item.getLogo())) {
                String logoUrl = cosUtils.urlAuth(item.getLogo());
                item.setLogoUrl(logoUrl);
            }
        });
        return ipage;
    }

    /**
     * 推文预览
     *
     * @param url
     * @return
     */
    @Override
    public Result getTweetsContent(String url) {
        WebClient webClient = new WebClient(BrowserVersion.CHROME);//新建一个模拟谷歌Chrome浏览器的浏览器客户端对象
        webClient.getOptions().setThrowExceptionOnScriptError(false);//当JS执行出错的时候是否抛出异常, 这里选择不需要
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);//当HTTP的状态非200时是否抛出异常, 这里选择不需要
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setCssEnabled(true);//是否启用CSS, 因为需要展现页面, 所以需要启用
        webClient.getOptions().setJavaScriptEnabled(true); //很重要，启用JS
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());//很重要，设置支持AJAX
        HtmlPage page = null;
        try {
            page = webClient.getPage(url);//加载网页
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            webClient.close();
        }
        webClient.waitForBackgroundJavaScript(30000);//异步JS执行需要耗时,所以这里线程要阻塞30秒,等待异步JS执行结束
        String pageXml = page.asXml();//直接将加载完成的页面转换成xml格式的字符串
        pageXml = pageXml.replaceAll("\"/mp", "\"http://mp.weixin.qq.com/mp");//给微信页面内相对路径的请求设置域名
        return ResultUtil.success(pageXml);
    }

    /**
     * 公众号编辑
     */
    @Override
    public void editWechat(EditWechatVO vo) {
        TokenUser user = commonService.getLoginInfo();
        // 前端传1，入库传3
        if (1 == vo.getGoaType()) {
            vo.setGoaType(3);
        }

        // LOGO
        OfficialAccounts accounts = this.getOfficialAccounts(user, vo, OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_LOGO.getCode());
        OfficialAccounts officialAccountsLogo = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsLogo);
        officialAccountsLogo.setClient(user.getClient());
        officialAccountsLogo.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_LOGO.getCode());
        officialAccountsLogo.setGoaObjectValue(vo.getLogo());
        this.saveOfficialAccounts(officialAccountsLogo, accounts);

        // 特权说明
        accounts = this.getOfficialAccounts(user, vo, OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_MEMBER_BENEFITS.getCode());
        OfficialAccounts officialAccountsBenefits = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsBenefits);
        officialAccountsBenefits.setClient(user.getClient());
        officialAccountsBenefits.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_MEMBER_BENEFITS.getCode());
        officialAccountsBenefits.setGoaObjectValue(vo.getMemberBenefits());
        this.saveOfficialAccounts(officialAccountsBenefits, accounts);

        // 有效日期
        accounts = this.getOfficialAccounts(user, vo, OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_VALID_PERIOD.getCode());
        OfficialAccounts officialAccountsPeriod = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsPeriod);
        officialAccountsPeriod.setClient(user.getClient());
        officialAccountsPeriod.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_VALID_PERIOD.getCode());
        officialAccountsPeriod.setGoaObjectValue(vo.getAccountsValidPeriod());
        this.saveOfficialAccounts(officialAccountsPeriod, accounts);

        // 使用须知
        accounts = this.getOfficialAccounts(user, vo, OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_USAGE_NOTICE.getCode());
        OfficialAccounts officialAccountsNotice = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsNotice);
        officialAccountsNotice.setClient(user.getClient());
        officialAccountsNotice.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_USAGE_NOTICE.getCode());
        officialAccountsNotice.setGoaObjectValue(vo.getAccountsUsageNotice());
        this.saveOfficialAccounts(officialAccountsNotice, accounts);

        // 电话号码
        accounts = this.getOfficialAccounts(user, vo, OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_TEL.getCode());
        OfficialAccounts officialAccountsTel = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsTel);
        officialAccountsTel.setClient(user.getClient());
        officialAccountsTel.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_TEL.getCode());
        officialAccountsTel.setGoaObjectValue(vo.getAccountsTel());
        this.saveOfficialAccounts(officialAccountsTel, accounts);

        // 会员卡ID
        accounts = this.getOfficialAccounts(user, vo, OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_MEMBER_CARD_ID.getCode());
        OfficialAccounts officialAccountsMemberId = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsMemberId);
        officialAccountsMemberId.setClient(user.getClient());
        officialAccountsMemberId.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_MEMBER_CARD_ID.getCode());
        officialAccountsMemberId.setGoaObjectValue(vo.getMemberCardId());
        this.saveOfficialAccounts(officialAccountsMemberId, accounts);

    }

    /**
     * 公众号数据获取
     */
    @Override
    public GetWechatDTO getWechat(GetWechatVO vo) {
        GetWechatDTO dto = officialAccountsMapper.getWechat(vo);
        if (dto == null) {
            dto = new GetWechatDTO();
        }
        if (!ObjectUtils.isEmpty(dto.getLogo())) {
            String logoUrl = cosUtils.urlAuth(dto.getLogo());
            dto.setLogoUrl(logoUrl);
        }
        QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper();
        storeDataQueryWrapper.eq("CLIENT", vo.getClient());
        storeDataQueryWrapper.eq("STO_CODE", vo.getGoaCode());
        StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
        if (storeData != null) {
            dto.setWechatName(storeData.getStoName());
        } else {
            QueryWrapper<Compadm> compadmQueryWrapper = new QueryWrapper();
            compadmQueryWrapper.eq("CLIENT", vo.getClient());
            compadmQueryWrapper.eq("COMPADM_ID", vo.getGoaCode());
            Compadm compadm = compadmMapper.selectOne(compadmQueryWrapper);
            if (compadm != null) {
                dto.setWechatName(compadm.getCompadmName());
            }
        }
        return dto;
    }

    /**
     * 公众号APPID和SECRECT设置
     *
     * @param vo
     */
    @Override
    public void editAppidAndSecrect(AppidAndSecrectVO vo) {
        // 前端传1，入库传3
        if (1 == vo.getGoaType()) {
            vo.setGoaType(3);
        }
        // 验证appid 和 secret 是匹配
        String accessToken = weChatUtils.getAccessToken(vo.getAccountsAppid(), vo.getAccountsAppsecret());
        if (StringUtils.isBlank(accessToken)) {
            throw new CustomResultException("APPID和SECRET不匹配,请重新设置");
        }

        // APPID
        OfficialAccounts accounts = this.getOfficialAccounts(vo.getClient(), vo.getGoaType(), vo.getGoaCode(), OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_APPID.getCode());
        OfficialAccounts officialAccountsAppid = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsAppid);
        officialAccountsAppid.setClient(vo.getClient());
        officialAccountsAppid.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_APPID.getCode());
        officialAccountsAppid.setGoaObjectValue(vo.getAccountsAppid());
        this.saveOfficialAccounts(officialAccountsAppid, accounts);

        // APPSECRET
        accounts = this.getOfficialAccounts(vo.getClient(), vo.getGoaType(), vo.getGoaCode(), OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_APPSECRET.getCode());
        OfficialAccounts officialAccountsAppsecret = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialAccountsAppsecret);
        officialAccountsAppsecret.setClient(vo.getClient());
        officialAccountsAppsecret.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ACCOUNTS_APPSECRET.getCode());
        officialAccountsAppsecret.setGoaObjectValue(vo.getAccountsAppsecret());
        this.saveOfficialAccounts(officialAccountsAppsecret, accounts);

        // 微信原始ID
        accounts = this.getOfficialAccounts(vo.getClient(), vo.getGoaType(), vo.getGoaCode(), OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ORIGINAL_ID.getCode());
        OfficialAccounts officialOriginalId = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialOriginalId);
        officialOriginalId.setClient(vo.getClient());
        officialOriginalId.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_ORIGINAL_ID.getCode());
        officialOriginalId.setGoaObjectValue(vo.getOriginalId());
        this.saveOfficialAccounts(officialOriginalId, accounts);

        // 推文授权
        accounts = this.getOfficialAccounts(vo.getClient(), vo.getGoaType(), vo.getGoaCode(), OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_WECHAT_TWEETS.getCode());
        OfficialAccounts officialWechatTweets = new OfficialAccounts();
        BeanUtils.copyProperties(vo, officialWechatTweets);
        officialWechatTweets.setClient(vo.getClient());
        officialWechatTweets.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_WECHAT_TWEETS.getCode());
        officialWechatTweets.setGoaObjectValue(vo.getWechatTweets());
        this.saveOfficialAccounts(officialWechatTweets, accounts);
    }

    /**
     * 获取唯一配置项
     */
    private OfficialAccounts getOfficialAccounts(TokenUser user, EditWechatVO vo, String param) {
        return this.getOne(new QueryWrapper<OfficialAccounts>()
                .eq("CLIENT", user.getClient())
                .eq("GOA_TYPE", vo.getGoaType())
                .eq("GOA_CODE", vo.getGoaCode())
                .eq("GOA_OBJECT_PARAM", param));
    }

    /**
     * 获取唯一配置项
     */
    private OfficialAccounts getOfficialAccounts(String client, Integer goaType, String goaCode, String param) {
        return this.getOne(new QueryWrapper<OfficialAccounts>()
                .eq("CLIENT", client)
                .eq("GOA_TYPE", goaType)
                .eq("GOA_CODE", goaCode)
                .eq("GOA_OBJECT_PARAM", param));
    }

    /**
     * 保存配置项
     */
    private void saveOfficialAccounts(OfficialAccounts newAccounts, OfficialAccounts oldAccounts) {
        if (ObjectUtils.isEmpty(oldAccounts)) {
            newAccounts.setId(UUIDUtil.getUUID());
            this.save(newAccounts);
        } else {
            newAccounts.setId(oldAccounts.getId());
            this.updateById(newAccounts);
        }
    }

    /**
     * 公众号素材列表
     *
     * @param vo
     * @return
     */
    @Override
    public BatchgetMaterialRes getMaterialList(GetMaterialListVO vo) {
        TokenUser user = commonService.getLoginInfo();
        String accessToken = this.getAccessToken(applicationConfig.getWeChatAppid(), applicationConfig.getWeChatSecret());
        String result = "";
        if ("1".equals(vo.getMaterialType())) {
            result = weChatUtils.getFreepublishBatchgetList(accessToken, vo.getType(), vo.getPageNum() * vo.getPageSize(), vo.getPageSize());
        } else {
            result = weChatUtils.getMaterialList(accessToken, vo.getType(), vo.getPageNum() * vo.getPageSize(), vo.getPageSize());
        }
        this.checkSuccess(result, applicationConfig.getWeChatAppid());
        BatchgetMaterialRes batchgetMaterialRes = JsonUtils.jsonToBean(result, BatchgetMaterialRes.class);

        if (!ObjectUtils.isEmpty(batchgetMaterialRes) && !CollectionUtils.isEmpty(batchgetMaterialRes.getItem())) {
            // 素材标识集合
            List<String> mediaIdList = batchgetMaterialRes.getItem().stream().map(BatchgetMaterialRes.ItemBean::getMedia_id).collect(Collectors.toList());
            // 推送客户数量
            List<GetCustomerTotalAndPushedDO> pushedDOList = officialAccountsMapper.getCustomerPushedCount(user.getClient(), mediaIdList);
            // 客户总数量
            int totalCount = officialAccountsMapper.getCustomerTotalCount(user.getClient());
            batchgetMaterialRes.getItem().forEach(item -> {
                GetCustomerTotalAndPushedDO pushedDO = pushedDOList.stream().filter(Objects::nonNull)
                        .filter(media -> media.getGwthMediaId().equals(item.getMedia_id()))
                        .findFirst().orElse(null);
                item.setPushCount(pushedDO == null ? 0 : pushedDO.getPushCount());
                item.setTotalCount(totalCount);
            });
        }

        return batchgetMaterialRes;

    }

    /**
     * 公众号素材详情
     *
     * @param mediaId
     * @return
     */
    @Override
    public MediaIdResDTO getMaterial(String mediaId, String gwthMaterialType) {
        String accessToken = this.getAccessToken(applicationConfig.getWeChatAppid(), applicationConfig.getWeChatSecret());
        String result = "";
        if ("1".equals(gwthMaterialType)) {
            result = weChatUtils.getFreepublishBatchget(accessToken, mediaId);
        } else {
            result = weChatUtils.getMateria(accessToken, mediaId);
        }
        this.checkSuccess(result, applicationConfig.getWeChatAppid());
        log.info(result);
        return JsonUtils.jsonToBean(result, MediaIdResDTO.class);
    }

    /**
     * 公众号菜单列表
     *
     * @param vo
     * @return
     */
    @Override
    public WeChatMenuDTO getWeChatMenu(GetWeChatMenuVO vo) {
        // 获取指定公众号的 appid 和 secret
        AppidAndSecretDTO dto = officialAccountsMapper.getAppidAndSecret(vo.getClient(), vo.getGoaCode());
        if (ObjectUtils.isEmpty(dto)) {
            throw new CustomResultException("该客户未配置正确的APPID以及密钥");
        }
        // 获取该公众号的 密钥
        String accessToken = weChatUtils.getAccessToken(dto.getAccountsAppid(), dto.getAccountsAppsecret());
        // 请求 微信公众号 菜单接口
        String result = weChatUtils.getWeChatMenu(accessToken);
        checkSuccess(result, dto.getAccountsAppid());

        return JsonUtils.jsonToBean(result, WeChatMenuDTO.class);
    }

    /**
     * 公众号菜单编辑
     */
    @Override
    public void editWeChatMenu(EditWeChatMenuVO vo) {
        AppidAndSecretDTO dto = officialAccountsMapper.getAppidAndSecret(vo.getClient(), vo.getGoaCode());
        String accessToken = weChatUtils.getAccessToken(dto.getAccountsAppid(), dto.getAccountsAppsecret());
        // 直接新增覆盖
        CreateWeChatMenuParam param = new CreateWeChatMenuParam();
        BeanUtils.copyProperties(vo, param);
        String createResult = weChatUtils.createWeChatMenu(accessToken, param);
    }

    /**
     * 公众号文章推送
     *
     * @param vo
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void tweetsPush(TweetsPushVO vo) {
        TokenUser user = commonService.getLoginInfo();
        for (int i = 0; i < vo.getContent().getNews_item().size(); i++) {
            BatchgetMaterialRes.ItemBean.ContentBean.NewsItemBean newsItemBean = vo.getContent().getNews_item().get(i);
            if (StringUtils.isBlank(newsItemBean.getThumb_url())) {
                throw new CustomResultException(MessageFormat.format("该推文条目[{0}]没有设置封面", i + 1));
            }
        }
        // 该推文 对应的推送记录删除 (已推送，未操作)
        List<WechatTweetsH> weChatExitList = wechatTweetsHService.list(new LambdaQueryWrapper<WechatTweetsH>()
                .eq(WechatTweetsH::getGwthMediaId, vo.getMedia_id())
                .eq(WechatTweetsH::getGwthRelease, OperateEnum.GwthRelease.NO_OPERATE.getCode()));
        if (!CollectionUtils.isEmpty(weChatExitList)) {
            List<Long> wechatTweetsHIdList = weChatExitList.stream().map(WechatTweetsH::getId).collect(Collectors.toList());
            wechatTweetsHService.removeByIds(wechatTweetsHIdList);
            wechatTweetsDService.remove(new LambdaQueryWrapper<WechatTweetsD>().in(WechatTweetsD::getGwthId, wechatTweetsHIdList));
        }
        if (CollectionUtils.isEmpty(vo.getCustomList())) {
            return;
        }
        String localHour = DateUtils.getLocalHour();
        List<WechatTweetsH> wechatTweetsHList = new ArrayList<>();
        for (OfficialAccountsDTO officialAccountsDTO : vo.getCustomList()) {
            WechatTweetsH wechatTweetsH = new WechatTweetsH();
            wechatTweetsH.setGwthMediaId(vo.getMedia_id());
            wechatTweetsH.setClient(officialAccountsDTO.getClient());
            wechatTweetsH.setGwthCustomer(officialAccountsDTO.getGoaCode());
            // 是否发布 (0：未操作，1：已发布，2：不发布)
            wechatTweetsH.setGwthRelease(OperateEnum.GwthRelease.NO_OPERATE.getCode());
            // 不发布原因(初始为零)
            wechatTweetsH.setGwthReason("");
            // 发布时间(初始为零)
            wechatTweetsH.setGwthTime("");
            // 计划推送时间必须大于当前小时
            String gwthPlanPushTime = officialAccountsDTO.getGwthPlanPushTime();
            if (StringUtils.isNotBlank(gwthPlanPushTime) && localHour.compareTo(gwthPlanPushTime) >= 0) {
                throw new CustomResultException("计划推送时间必须大于当前小时");
            }
            wechatTweetsH.setGwthPlanPushTime(gwthPlanPushTime);
            // 如果计划推送时间为空，那么就立即推送
            if (StringUtils.isBlank(gwthPlanPushTime)) {
                wechatTweetsH.setGwthPushTime(DateUtils.getCurrentDateTimeStrFull());
            }
            wechatTweetsH.setGwthPushUser(user.getUserId());
            // 客户微信标识ID（推送时还没有生成，只有在客户发布之后才会生成）
            wechatTweetsH.setGwthClientMediaId("");
            wechatTweetsH.setGwthMaterialType(vo.getMaterialType());
            wechatTweetsHList.add(wechatTweetsH);
        }
        if (CollectionUtils.isEmpty(wechatTweetsHList)) {
            return;
        }
        wechatTweetsHService.saveBatch(wechatTweetsHList);

        // 明细表
        List<WechatTweetsD> wechatTweetsDList = wechatTweetsHList.stream().flatMap(wechatTweetsH -> vo.getContent().getNews_item().stream().map(newsItemBean -> {
            WechatTweetsD wechatTweetsD = new WechatTweetsD();
            wechatTweetsD.setGwthId(wechatTweetsH.getId());
            wechatTweetsD.setClient(wechatTweetsH.getClient());
            wechatTweetsD.setGwtdCustomer(wechatTweetsH.getGwthCustomer());
            wechatTweetsD.setGwtdTitle(newsItemBean.getTitle());
            wechatTweetsD.setGwtdThumbMediaId(newsItemBean.getThumb_media_id());
            wechatTweetsD.setGwtdShowCoverPic(newsItemBean.getShow_cover_pic());
            wechatTweetsD.setGwtdAuthor(newsItemBean.getAuthor());
            wechatTweetsD.setGwtdDigest(newsItemBean.getDigest());
            wechatTweetsD.setGwtdContent(newsItemBean.getContent());
            wechatTweetsD.setGwtdUrl(newsItemBean.getUrl());
            wechatTweetsD.setGwtdContetnSourceUrl(newsItemBean.getContent_source_url());
            wechatTweetsD.setGwtdThumbUrl(newsItemBean.getThumb_url());
            wechatTweetsD.setGwtdNeedOpenComment(newsItemBean.getNeed_open_comment());
            wechatTweetsD.setGwtdOnlyFansCanComment(newsItemBean.getOnly_fans_can_comment());
            // 是否发布 (0：未操作，1：已发布，2：不发布)
            wechatTweetsD.setGwtdRelease(OperateEnum.GwthRelease.NO_OPERATE.getCode());
            // 不发布原因(初始为零)
            wechatTweetsD.setGwtdReason("");
            return wechatTweetsD;
        })).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(wechatTweetsDList)) {
            wechatTweetsDService.saveBatch(wechatTweetsDList);
        }

        // 需要立即发送的加盟商列表
        List<OfficialAccounts> pushList = officialAccountsMapper.getWechatConfigTweetsList();
        if (!CollectionUtils.isEmpty(pushList)) {
            wechatTweetsHList.stream().filter(Objects::nonNull)
                    // 剔除掉计划推送部分
                    .filter(wechatTweetsH -> StringUtils.isNotBlank(wechatTweetsH.getGwthPushTime()))
                    .filter(wechatTweetsH -> {
                        return pushList.stream().anyMatch(config -> config.getClient().equals(wechatTweetsH.getClient())
                                && config.getGoaCode().equals(wechatTweetsH.getGwthCustomer()));
                    })
                    .forEach(wechatTweetsH -> {
                        try {
                            log.info("公众号推文,推送同时发布,加盟商[{}] 公众号[{}]", wechatTweetsH.getClient(), wechatTweetsH.getGwthCustomer());
                            tweetItemPushAndRelease(wechatTweetsH);
                        } catch (Exception e) {
                            log.info("公众号推文,推送同时发布失败,加盟商[{}] 公众号[{}]", wechatTweetsH.getClient(), wechatTweetsH.getGwthCustomer());
                        }
                    });
        }

    }

    /**
     * 公众号文章发布
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void tweetsRelease(TweetsReleaseVO vo) {

        // 1.获取公众号图文推文信息
        WechatTweetsH wechatTweetsHExit = wechatTweetsHService.getById(vo.getId());
        if (ObjectUtils.isEmpty(wechatTweetsHExit)) {
            throw new CustomResultException("素材不存在");
        }
        if (wechatTweetsHExit.getGwthRelease().equals(1)) {
            throw new CustomResultException("素材状态为[已发布]不可再次发布");
        }
        if (wechatTweetsHExit.getGwthRelease().equals(2)) {
            throw new CustomResultException("素材状态为[不发布],不进行发布操作");
        }
        // 2.不发布
        if (OperateEnum.GwthRelease.NO_RELEASE.getCode().equals(vo.getGwthRelease())) {
            WechatTweetsH wechatTweetsH = new WechatTweetsH();
            wechatTweetsH.setId(wechatTweetsHExit.getId());
            wechatTweetsH.setGwthRelease(OperateEnum.GwthRelease.NO_RELEASE.getCode());
            wechatTweetsH.setGwthReason(vo.getGwthReason());
            // 如果推送时间不存在(计划推送的情况)，推送时间设置为当前时间
            if (StringUtils.isBlank(wechatTweetsHExit.getGwthPushTime())) {
                wechatTweetsH.setGwthPushTime(DateUtils.getCurrentDateTimeStrFull());
            }
            wechatTweetsHService.updateById(wechatTweetsH);
            return;
        }

        // 3.发布
        // 平台公众号推文获取
        MediaIdResDTO material = getMaterial(wechatTweetsHExit.getGwthMediaId(), wechatTweetsHExit.getGwthMaterialType());
        // 客户推文上传成功后返回的推文id
        TweetsReleaseResultDTO releaseResultDTO = null;
        if (!ObjectUtils.isEmpty(material) && !CollectionUtils.isEmpty(material.getNews_item())) {
            releaseResultDTO = tweetsReleaseByPlatForm(wechatTweetsHExit.getClient(), wechatTweetsHExit.getGwthCustomer(), material.getNews_item());
        } else {
            List<WechatTweetsD> wechatTweetsDList = wechatTweetsDService.list(new QueryWrapper<WechatTweetsD>()
                    .eq("GWTH_ID", wechatTweetsHExit.getId()));
            releaseResultDTO = tweetsReleaseByDB(wechatTweetsHExit.getClient(), wechatTweetsHExit.getGwthCustomer(), wechatTweetsDList);
        }

        // 4.更新主表 GWTH_CLIENT_MEDIA_ID 字段
        WechatTweetsH wechatTweetsH = new WechatTweetsH();
        wechatTweetsH.setId(wechatTweetsHExit.getId());
        wechatTweetsH.setGwthClientMediaId(releaseResultDTO.getMediaId());
        wechatTweetsH.setGwthRelease(1);
        wechatTweetsH.setGwthTime(DateUtils.getCurrentDateTimeStrFull());
        // 如果推送时间不存在(计划推送的情况)，推送时间设置为当前时间
        if (StringUtils.isBlank(wechatTweetsHExit.getGwthPushTime())) {
            wechatTweetsH.setGwthPushTime(DateUtils.getCurrentDateTimeStrFull());
        }
        wechatTweetsHService.updateById(wechatTweetsH);
    }

    /**
     * 平台公众号抽取推文 发布到 客户公众号
     *
     * @param client       客户所属加盟商
     * @param gwthCustomer 客户编码
     * @param news_item    平台公众号推文列表
     * @return
     */
    private TweetsReleaseResultDTO tweetsReleaseByPlatForm(String client, String gwthCustomer, List<MediaIdResDTO.NewsItemBean> news_item) {
        // 1.获取客户公众号token
        AppidAndSecretDTO appidAndSecret = officialAccountsMapper.getAppidAndSecret(client, gwthCustomer);
        if (ObjectUtils.isEmpty(appidAndSecret)) {
            throw new CustomResultException("该客户未配置正确的APPID以及密钥");
        }
        String accessToken = getAccessToken(appidAndSecret.getAccountsAppid(), appidAndSecret.getAccountsAppsecret());

        // 2.上传图文
        List<String> thumbMediaIdList = new ArrayList<>();
        List<News> newsList = news_item.stream().map(newsItem -> {
            News news = new News();
            news.setTitle(newsItem.getTitle());
            news.setAuthor(newsItem.getAuthor());
            news.setDigest(newsItem.getDigest());
            news.setShow_cover_pic(newsItem.getShow_cover_pic());
            news.setContent(newsItem.getContent());
            news.setContent_source_url(newsItem.getContent_source_url());
            news.setNeed_open_comment(newsItem.getNeed_open_comment());
            news.setOnly_fans_can_comment(newsItem.getOnly_fans_can_comment());
            // 3.上传封面
            if (StringUtils.isNotBlank(newsItem.getThumb_url())) {
                String thumb_media_id = weChatUtils.uploadimgByUrl(accessToken, newsItem.getThumb_url());
                thumbMediaIdList.add(thumb_media_id);
                news.setThumb_media_id(thumb_media_id);
            } else {
                throw new CustomResultException("该推文在平台端已经被修改,存在未设置封面的条目");
            }
            return news;
        }).collect(Collectors.toList());
        String mediaId = weChatUtils.addNews(accessToken, newsList);
        if (StringUtils.isBlank(mediaId)) {
            try {
                // 删除本次上传的图片
                thumbMediaIdList.forEach(item -> weChatUtils.delMaterial(accessToken, item));
            } catch (Exception e) {
                log.info("图文发布回滚失败, mediaId:{}", thumbMediaIdList);
            }
            throw new CustomResultException("图文上传失败");
        }

        // 4.发布图文
        Integer success = weChatUtils.newsPulish(accessToken, mediaId);
        if (!success.equals(0)) {
            try {
                // 删除图文
                weChatUtils.delMaterial(accessToken, mediaId);
                // 删除图片
                thumbMediaIdList.forEach(item -> weChatUtils.delMaterial(accessToken, item));
            } catch (Exception e) {
                log.info("图文发布回滚失败");
            }

            if (success.equals(45028)) {
                throw new CustomResultException("没有发送权限了,已达本月发送上限");
            }
            throw new CustomResultException("图文发布失败");
        }
        return new TweetsReleaseResultDTO(mediaId, thumbMediaIdList);
    }

    /**
     * DB抽取推文 发布到 客户公众号
     *
     * @param client            客户所属加盟商
     * @param gwthCustomer      客户编码
     * @param wechatTweetsDList DB存储的推文详情
     * @return
     */
    private TweetsReleaseResultDTO tweetsReleaseByDB(String client, String gwthCustomer, List<WechatTweetsD> wechatTweetsDList) {
        // 1.获取token
        AppidAndSecretDTO appidAndSecret = officialAccountsMapper.getAppidAndSecret(client, gwthCustomer);
        if (ObjectUtils.isEmpty(appidAndSecret)) {
            throw new CustomResultException("该客户未配置正确的APPID以及密钥");
        }
        String accessToken = getAccessToken(appidAndSecret.getAccountsAppid(), appidAndSecret.getAccountsAppsecret());

        // 2.上传图文
        List<String> thumbMediaIdList = new ArrayList<>();
        List<News> newsList = wechatTweetsDList.stream().map(wechatTweetsD -> {
            News news = new News();
            news.setTitle(wechatTweetsD.getGwtdTitle());
            news.setAuthor(wechatTweetsD.getGwtdAuthor());
            news.setDigest(wechatTweetsD.getGwtdDigest());
            news.setShow_cover_pic(wechatTweetsD.getGwtdShowCoverPic());
            news.setContent(wechatTweetsD.getGwtdContent());
            news.setContent_source_url(wechatTweetsD.getGwtdContetnSourceUrl());
            news.setNeed_open_comment(wechatTweetsD.getGwtdNeedOpenComment());
            news.setOnly_fans_can_comment(wechatTweetsD.getGwtdOnlyFansCanComment());
            // 3.上传封面
            String thumbMediaId = weChatUtils.uploadimgByUrl(accessToken, wechatTweetsD.getGwtdThumbUrl());
            news.setThumb_media_id(thumbMediaId);
            thumbMediaIdList.add(thumbMediaId);
            return news;
        }).collect(Collectors.toList());
        String mediaId = weChatUtils.addNews(accessToken, newsList);
        if (StringUtils.isBlank(mediaId)) {
            try {
                // 删除本次上传的图片
                thumbMediaIdList.forEach(item -> weChatUtils.delMaterial(accessToken, item));
            } catch (Exception e) {
                log.info("图文发布回滚失败");
            }
            throw new CustomResultException("图文上传失败");
        }

        // 4.发布图文
        Integer success = weChatUtils.newsPulish(accessToken, mediaId);
        if (!success.equals(0)) {
            try {
                // 删除图文
                weChatUtils.delMaterial(accessToken, mediaId);
                // 删除图片
                thumbMediaIdList.forEach(item -> weChatUtils.delMaterial(accessToken, item));
            } catch (Exception e) {
                log.info("图文发布回滚失败");
            }
            if (success.equals(45028)) {
                throw new CustomResultException("没有发送权限了,已达本月发送上限");
            }
            throw new CustomResultException("图文发布失败");
        }
        return new TweetsReleaseResultDTO(mediaId, thumbMediaIdList);
    }

    /**
     * 客户列表
     *
     * @param vo
     * @return
     */
    @Override
    public List<OfficialAccountsDTO> getCostomerList(GetCostomerListVO vo) {
        TokenUser user = commonService.getLoginInfo();
        List<OfficialAccountsDTO> costomerList = officialAccountsMapper.getCostomerList(user.getClient(), vo.getMediaId());
        costomerList.forEach(item -> {
            if (3 == item.getGoaType()) {
                item.setGoaType(1);
            }
            if (!ObjectUtils.isEmpty(item.getLogo())) {
                String logoUrl = cosUtils.urlAuth(item.getLogo());
                item.setLogoUrl(logoUrl);
            }
        });
        return costomerList;
    }

    /**
     * 客户公众号素材列表
     *
     * @param vo
     * @return
     */
    @Override
    public IPage<GetCostomerMaterialListDTO> getCostomerMaterialList(GetCostomerMaterialListVO vo) {
        TokenUser user = commonService.getLoginInfo();

        Page page = new Page(vo.getPageNum(), vo.getPageSize());
        IPage<GetCostomerMaterialListDTO> list = wechatTweetsHMapper.getCostomerMaterialList(page, vo, user.getClient());
        list.getRecords().forEach(item -> {
            List<WechatTweetsD> wechatTweetsDList = wechatTweetsDService.list(new QueryWrapper<WechatTweetsD>()
                    .select("ID", "GWTH_ID", "CLIENT", "GWTD_CUSTOMER", "GWTD_TITLE", "GWTD_THUMB_MEDIA_ID", "GWTD_SHOW_COVER_PIC", "GWTD_AUTHOR", "GWTD_DIGEST",
                            "GWTD_URL", "GWTD_CONTETN_SOURCE_URL", "GWTD_RELEASE", "GWTD_REASON", "GWTD_THUMB_URL", "GWTD_NEED_OPEN_COMMENT", "GWTD_ONLY_FANS_CAN_COMMENT")
                    .eq("GWTH_ID", item.getId()));
            item.setNewsItem(wechatTweetsDList);
        });
        return list;
    }

    /**
     * 客户下拉框
     *
     * @return
     */
    @Override
    public List<GetCostomerDropDownList> getCostomerDropDownList() {
        TokenUser user = commonService.getLoginInfo();
        return officialAccountsMapper.getCostomerDropDownList(user.getClient());

    }

    /**
     * 获取token
     *
     * @param weChatAppid
     * @param weChatSecret
     * @return
     */
    private String getAccessToken(String weChatAppid, String weChatSecret) {
        String accessToken = weChatUtils.getAccessToken(weChatAppid, weChatSecret);
        return accessToken;
    }

    /**
     * 异常返回
     *
     * @param result
     * @param weChatAppid
     */
    private void checkSuccess(String result, String weChatAppid) {
        JSONObject json = JSONObject.parseObject(result);
        String errcode = json.getString("errcode");
        if (StringUtils.isNotBlank(errcode)) {
            if (errcode.equals("40001")) {
                redisClient.set(Constants.WECHAT_ACCESSTOKEN + weChatAppid, Constants.EMPTY_STRING);
                throw new CustomResultException("微信公众号认证失败,请重试");
            }
            if (errcode.equals("40017")) {
                throw new CustomResultException("不合法的按钮类型");
            }
            throw new CustomResultException("请求微信接口失败");
        }
    }

    /**
     * 微信公众号定时推送
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void tweetsPushJob() {
        String localHour = DateUtils.getLocalHour();
        List<WechatTweetsHJobDTO> planPushWechatTweetList = wechatTweetsHMapper.getPlanPushWechatTweetList(localHour);
        planPushWechatTweetList.stream().filter(Objects::nonNull)
                .forEach(item -> {
                    try {
                        log.info("公众号推文,定时推送发布,加盟商[{}] 公众号[{}]", item.getClient(), item.getGwthCustomer());
                        officialAccountsService.tweetItemPushAndRelease(item);
                    } catch (Exception e) {
                        log.info("公众号推文,定时推送发布失败,加盟商[{}] 公众号[{}]", item.getClient(), item.getGwthCustomer());
                    }
                });
    }

    /**
     * 一个公众号 一个推文 进行发布 (一个公众号 一个推文 一个事务)
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = REQUIRES_NEW)
    public void tweetItemPushAndRelease(WechatTweetsH wechatTweetsH) {
        TweetsReleaseVO pushVO = new TweetsReleaseVO();
        pushVO.setId(wechatTweetsH.getId());
        pushVO.setGwthRelease(OperateEnum.GwthRelease.RELEASE.getCode());
        tweetsRelease(pushVO);
    }

    /**
     * 推文授权
     *
     * @param goaCode
     * @param goaType
     * @param wechatTweets
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void wechatTweets(String goaCode, Integer goaType, String wechatTweets) {
        // 前端传1，入库传3
        if (1 == goaType) {
            goaType = 3;
        }
        TokenUser user = commonService.getLoginInfo();
        officialAccountsMapper.delete(
                new QueryWrapper<OfficialAccounts>().eq("CLIENT", user.getClient())
                        .eq("GOA_CODE", goaCode)
                        .eq("GOA_TYPE", goaType)
                        .eq("GOA_OBJECT_PARAM", "WECHAT_OFFICIAL_WECHAT_TWEETS")
        );
        OfficialAccounts officialAccounts = new OfficialAccounts();
        officialAccounts.setId(UUIDUtil.getUUID());
        // 加盟商
        officialAccounts.setClient(user.getClient());
        // 类型
        officialAccounts.setGoaType(goaType);
        // 对象编码
        officialAccounts.setGoaCode(goaCode);
        // 参数
        officialAccounts.setGoaObjectParam("WECHAT_OFFICIAL_WECHAT_TWEETS");
        // 值
        officialAccounts.setGoaObjectValue(wechatTweets);
        officialAccountsMapper.insert(officialAccounts);
    }

    /**
     * 草稿列表
     *
     * @param vo
     * @return
     */
    @Override
    public BatchgetMaterialRes getDraftBatchgetList(GetMaterialListVO vo) {
        String accessToken = this.getAccessToken(applicationConfig.getWeChatAppid(), applicationConfig.getWeChatSecret());
        String result = weChatUtils.getDraftBatchgetList(accessToken, vo.getPageNum() * vo.getPageSize(), vo.getPageSize());
        this.checkSuccess(result, applicationConfig.getWeChatAppid());
        BatchgetMaterialRes batchgetMaterialRes = JsonUtils.jsonToBean(result, BatchgetMaterialRes.class);
        return batchgetMaterialRes;
    }

    /**
     * 草稿发布
     *
     * @param media_id
     */
    @Override
    public void freepublishSubmit(String media_id) {
        String accessToken = this.getAccessToken(applicationConfig.getWeChatAppid(), applicationConfig.getWeChatSecret());
        weChatUtils.freepublishSubmit(accessToken, media_id);
    }

    @Override
    public void wechatMembershipCard(String goaCode, Integer goaType, String wechatMembershipCard) {
        // 前端传1，入库传3
        if (1 == goaType) {
            goaType = 3;
        }
        TokenUser user = commonService.getLoginInfo();

        if ("1".equals(wechatMembershipCard)) {
            OfficialAccounts accounts = officialAccountsMapper.selectOne(new QueryWrapper<OfficialAccounts>().eq("CLIENT", user.getClient())
                    .eq("GOA_CODE", goaCode)
                    .eq("GOA_TYPE", goaType)
                    .eq("GOA_OBJECT_PARAM", OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_MEMBER_CARD_ID.getCode()));
            if (accounts == null || StringUtils.isBlank(accounts.getGoaObjectValue())) {
                throw new CustomResultException("开启前请先填写会员卡ID");
            }

            // 获取指定公众号的 appid 和 secret
            AppidAndSecretDTO dto = officialAccountsMapper.getAppidAndSecret(user.getClient(), goaCode);
            if (ObjectUtils.isEmpty(dto)) {
                throw new CustomResultException("该客户未配置正确的APPID以及密钥");
            }
            // 获取该公众号的 密钥
            String accessToken = weChatUtils.getAccessToken(dto.getAccountsAppid(), dto.getAccountsAppsecret());
            if (StringUtils.isBlank(accessToken)) {
                throw new CustomResultException("APPID和SECRET不匹配,请重新设置");
            }

            String gzhUrl = applicationConfig.getGzhUrl() + "?client=" + user.getClient() + "&stoCode=" + goaCode;

            weChatUtils.updateMemberCard(accessToken, accounts.getGoaObjectValue(), applicationConfig.getActivateAfterSubmitUrl(), gzhUrl);

            weChatUtils.setActivateuserform(accessToken, accounts.getGoaObjectValue());

        }

        officialAccountsMapper.delete(
                new QueryWrapper<OfficialAccounts>().eq("CLIENT", user.getClient())
                        .eq("GOA_CODE", goaCode)
                        .eq("GOA_TYPE", goaType)
                        .eq("GOA_OBJECT_PARAM", OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_WECHAT_MEMBERSHIP_CARD.getCode())
        );

        OfficialAccounts officialAccounts = new OfficialAccounts();
        officialAccounts.setId(UUIDUtil.getUUID());
        // 加盟商
        officialAccounts.setClient(user.getClient());
        // 类型
        officialAccounts.setGoaType(goaType);
        // 对象编码
        officialAccounts.setGoaCode(goaCode);
        // 参数
        officialAccounts.setGoaObjectParam(OperateEnum.GoaObjectValue.WECHAT_OFFICIAL_WECHAT_MEMBERSHIP_CARD.getCode());
        // 值
        officialAccounts.setGoaObjectValue(wechatMembershipCard);
        officialAccountsMapper.insert(officialAccounts);
    }

}
