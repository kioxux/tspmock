package com.gov.operate.mapper;

import com.gov.operate.entity.SdPromChmedSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 中药类促销设置表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-07-27
 */
public interface SdPromChmedSetMapper extends BaseMapper<SdPromChmedSet> {

}
