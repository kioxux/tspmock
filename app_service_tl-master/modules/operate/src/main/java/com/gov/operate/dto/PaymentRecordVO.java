package com.gov.operate.dto;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.entity.PaymentApplications;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.20
 */
@Data
public class PaymentRecordVO {

    @ApiModelProperty(value = "付款记录")
    private IPage<PaymentRecordDetail> paymentRecordDetail;

    @ApiModelProperty(value = "付款合计")
    private BigDecimal paymentRecordTotal;

}

