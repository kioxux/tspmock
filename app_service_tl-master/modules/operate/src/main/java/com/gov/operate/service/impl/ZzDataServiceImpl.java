package com.gov.operate.service.impl;

import com.gov.operate.dto.zz.ZzInData;
import com.gov.operate.dto.zz.ZzOutData;
import com.gov.operate.dto.zz.ZzooOutData;
import com.gov.operate.entity.ZzData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ZzDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IZzDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-23
 */
@Service
public class ZzDataServiceImpl extends ServiceImpl<ZzDataMapper, ZzData> implements IZzDataService {

    @Autowired
    private ZzDataMapper zzDataMapper;
    @Resource
    private CommonService commonService;

    public List<ZzooOutData> getZzooList() {
        List<ZzooOutData> outData = this.zzDataMapper.getZzooList();
        if (ObjectUtils.isEmpty(outData)) {
            outData = new ArrayList();
        }

        return (List)outData;
    }

    public List<ZzOutData> getZzList(ZzInData inData) {
        TokenUser tokenUser = commonService.getLoginInfo();
        inData.setClient(tokenUser.getClient());
        List<ZzOutData> outData = this.zzDataMapper.getZzList(inData);
        return outData;
    }
}
