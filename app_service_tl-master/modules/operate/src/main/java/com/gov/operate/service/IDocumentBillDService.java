package com.gov.operate.service;

import com.gov.operate.entity.DocumentBillD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
public interface IDocumentBillDService extends SuperService<DocumentBillD> {

}
