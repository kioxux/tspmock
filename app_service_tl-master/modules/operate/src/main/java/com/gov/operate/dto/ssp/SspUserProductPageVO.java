package com.gov.operate.dto.ssp;

import lombok.Data;

/**
 * @TableName GAIA_SSP_USER_PRODUCT
 */
@Data
public class SspUserProductPageVO {
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 统一社会信用玛
     */
    private String creditCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 连锁总部
     */
    private String chainHead;

    /**
     * 连锁名称
     */
    private String chainName;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 生产厂家
     */
    private String proFactoryName;
}