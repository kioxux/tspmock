package com.gov.operate.service.impl;

import com.gov.operate.entity.SdMarketingSto;
import com.gov.operate.mapper.SdMarketingStoMapper;
import com.gov.operate.service.ISdMarketingStoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Service
public class SdMarketingStoServiceImpl extends ServiceImpl<SdMarketingStoMapper, SdMarketingSto> implements ISdMarketingStoService {

}
