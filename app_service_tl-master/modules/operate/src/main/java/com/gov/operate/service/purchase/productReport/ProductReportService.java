package com.gov.operate.service.purchase.productReport;

import com.gov.common.response.Result;
import com.gov.operate.dto.purchase.productReport.ProReplenishmentParams;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.10.09
 */
public interface ProductReportService {
    Result getProReplenishmentList(Integer pageSize, Integer pageNum, String dcCode, String stoCode, String proSelfCode, String proClass,
                                   String lastSupp, String recSupp, String proZdy1, String proZdy2, String proZdy3, String proZdy4, String proZdy5,
                                   String proSlaeClass, String proSclass, Integer proNoPurchase, Integer proPosition, List<ProReplenishmentParams> proReplenishmentParamsList,
                                   String salesVolumeStartDate, String salesVolumeEndDate, String proPurchaseRate);
}
