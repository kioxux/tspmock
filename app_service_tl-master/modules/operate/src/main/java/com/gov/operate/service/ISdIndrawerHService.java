package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdIndrawerH;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface ISdIndrawerHService extends SuperService<SdIndrawerH> {
    /**
     * 调取清斗单
     *
     * @return
     */
    Result getCleanDrawerInfo();

    /**
     * 清斗单高亮显示信息
     *
     * @param vo
     * @return
     */
    Result getCleanHighlightInfo(GetCleanHighlightVO vo);

    /**
     * 装斗保存
     *
     * @param vo
     * @return
     */
    String addInDrawer(AddInDrawerVO vo);

    /**
     * 装斗修改
     *
     * @param vo
     * @return
     */
    Result updateInDrawer(UpdateInDrawerVO vo);

    /**
     * 装斗审核
     *
     * @param vo
     * @return
     */
    Result checkInDrawer(CheckInDrawerVO vo);

    /**
     * 装斗明细
     *
     * @param gsihVoucherId
     * @return
     */
    List<InDrawerDetailDTO> getInDrawer(String gsihVoucherId);

    /**
     * 门店装斗记录查询
     *
     * @param vo
     * @return
     */
    IPage<SdIndrawerHVO> listInDrawer(ListInDrawerVO vo);

    List<CleanDrawerDetailDTO> getDrawerList();
}
