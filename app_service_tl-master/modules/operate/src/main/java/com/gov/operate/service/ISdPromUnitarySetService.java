package com.gov.operate.service;

import com.gov.operate.entity.SdPromUnitarySet;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
public interface ISdPromUnitarySetService extends SuperService<SdPromUnitarySet> {

}
