package com.gov.operate.service;

import com.gov.operate.entity.ProductMatchZ;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 导入商品匹配信息主表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-09-27
 */
public interface IProductMatchZService extends SuperService<ProductMatchZ> {

    void confirmNoNotify();

    void resultNotifyJobHandler();
}
