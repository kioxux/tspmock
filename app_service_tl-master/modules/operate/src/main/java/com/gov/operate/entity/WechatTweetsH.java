package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_WECHAT_TWEETS_H")
@ApiModel(value="WechatTweetsH对象", description="")
public class WechatTweetsH extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "微信标识")
    @TableField("GWTH_MEDIA_ID")
    private String gwthMediaId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "客户")
    @TableField("GWTH_CUSTOMER")
    private String gwthCustomer;

    @ApiModelProperty(value = "是否发布")
    @TableField("GWTH_RELEASE")
    private Integer gwthRelease;

    @ApiModelProperty(value = "原因")
    @TableField("GWTH_REASON")
    private String gwthReason;

    @ApiModelProperty(value = "发布时间")
    @TableField("GWTH_TIME")
    private String gwthTime;

    @ApiModelProperty(value = "推送时间")
    @TableField("GWTH_PUSH_TIME")
    private String gwthPushTime;

    @ApiModelProperty(value = "推送人")
    @TableField("GWTH_PUSH_USER")
    private String gwthPushUser;

    @ApiModelProperty(value = "客户微信标识ID")
    @TableField("GWTH_CLIENT_MEDIA_ID")
    private String gwthClientMediaId;

    @ApiModelProperty(value = "计划推送时间")
    @TableField("GWTH_PLAN_PUSH_TIME")
    private String gwthPlanPushTime;

    @ApiModelProperty(value = "null:历史素材，1：发布列表")
    @TableField("GWTH_MATERIAL_TYPE")
    private String gwthMaterialType;
}
