package com.gov.operate.dto.marketing;

import com.gov.operate.entity.SdMarketingPrd;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class PrdPriceAndCost extends SdMarketingPrd {

    /**
     * 零售价
     */
    private BigDecimal priceNormal;
    /**
     * 成本价
     */
    private BigDecimal priceCost;

}
