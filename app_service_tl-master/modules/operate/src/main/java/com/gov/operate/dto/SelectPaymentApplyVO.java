package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author awang
 */
@Data
@EqualsAndHashCode
public class SelectPaymentApplyVO extends Pageable {

    private String site;

    private String stoCode;

    private String supCode;

    private String startDate;

    private String billNum;

    private String endDate;

    private String client;

    /**
     * 付款类型（1:单据付款 2:发票付款）
     */
    private String businessType;
}
