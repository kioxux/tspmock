package com.gov.operate.controller.Recharge;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.recharge.RechargeRefundVO;
import com.gov.operate.entity.SdRechargeCard;
import com.gov.operate.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;

/**
 * <p>
 * 储值卡相关的操作
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@RestController
@RequestMapping("/recharge/recharge")
public class SdRechargeCardSetController {
    @Resource
    private ISdRechargeCardSetService sdRechargeCardSetService;

    @Resource
    private ISdRechargeCardService sdRechargeCardService;

    @Resource
    private ISdPaymentMethodService sdPaymentMethodService;
    @Resource
    private ISdRechargeCardPaycheckService sdRechargeCardPaycheckService;

    @Resource
    private ISdRechargeCardChangeService sdRechargeCardChangeService;

    @Log("储值卡设置门店")
    @ApiOperation(value = "储值卡设置门店")
    @GetMapping("getStoreList")
    public Result getStoreList() {
        return sdRechargeCardSetService.getStoreList();
    }

    @Log("储值卡设置获取")
    @ApiOperation(value = "储值卡设置获取")
    @GetMapping("getRechargeSet")
    public Result getCard() {
        return ResultUtil.success(sdRechargeCardSetService.getClientCard());
    }

    @Log("储值卡设置")
    @ApiOperation(value = "储值卡设置")
    @PostMapping("rechargeSet")
    public Result addOrUpdateCard(@Valid @RequestBody AddRechargeSetVO vo) {
        return ResultUtil.success(sdRechargeCardSetService.addOrUpdateCard(vo));
    }

    @Log("会员卡号查询")
    @ApiOperation(value = "会员卡号查询")
    @PostMapping("selectMemberCard")
    public Result getMemberCardNo(@Valid @RequestBody MemberNoVO vo) {
        return ResultUtil.success(sdRechargeCardService.getMemberCard(vo));
    }

    @Log("储值卡创建")
    @ApiOperation(value = "储值卡创建")
    @PostMapping("createRecharge")
    public Result addRecharge(@Valid @RequestBody AddRechargeVO vo) {
        return ResultUtil.success(sdRechargeCardService.addRecharge(vo));
    }

    @Log("储值卡编辑")
    @ApiOperation(value = "储值卡编辑")
    @PostMapping("editRecharte")
    public Result editRecharte(@RequestBody SdRechargeCard sdRechargeCard) {
        sdRechargeCardService.editRecharte(sdRechargeCard);
        return ResultUtil.success();
    }

    @Log("充值比例")
    @ApiOperation(value = "充值比例")
    @GetMapping("selectProportion")
    public Result getProportion() {
        return ResultUtil.success(sdRechargeCardSetService.getProportionData());
    }

    @Log("支付方式获取")
    @ApiOperation(value = "支付方式获取")
    @PostMapping("selectPaymentMethod")
    public Result getPayWay() {
        return ResultUtil.success(sdPaymentMethodService.getPayWay());
    }

    @Log("储值卡充值")
    @ApiOperation(value = "储值卡充值")
    @PostMapping("paycheck")
    public Result addPayCheck(@Valid @RequestBody AddRechargeCardPayCheckVO vo) {
        return ResultUtil.success(sdRechargeCardPaycheckService.addPayCheck(vo));
    }

    @Log("储值卡列表")
    @ApiOperation(value = "储值卡列表")
    @PostMapping("selectRechargeCard")
    public Result getRechargeCard(@Valid @RequestBody RechargeCardListVO vo) {
        Page<SdRechargeCard> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        return sdRechargeCardService.getRechargeCardList(vo, page);
    }

    @Log("储值门店列表")
    @ApiOperation(value = "储值门店列表")
    @PostMapping("getRechargeStoreList")
    public Result getRechargeStoreList() {
        return sdRechargeCardService.getRechargeStoreList();
    }

    @Log("恢复初始密码")
    @ApiOperation(value = "恢复初始密码")
    @PostMapping("resetPassword")
    public Result resetPassword(@Valid @RequestBody UpdateRechargeCardVO vo) {
        return sdRechargeCardService.resetPassword(vo);
    }

    @Log("修改密码")
    @ApiOperation(value = "修改密码")
    @PostMapping("changePassword")
    public Result changePassword(@Valid @RequestBody UpdateRechargeCardPasswordVO vo) {
        return sdRechargeCardService.changePassword(vo);
    }

    @Log("储值卡挂失")
    @ApiOperation(value = "储值卡挂失")
    @PostMapping("reportLoss")
    public Result reportLoss(@Valid @RequestBody UpdateRechargeCardVO vo) {
        return sdRechargeCardService.reportLoss(vo);
    }

    @Log("储值卡解挂")
    @ApiOperation(value = "储值卡解挂")
    @PostMapping("hangingSolutions")
    public Result hangingSolutions(@Valid @RequestBody UpdateRechargeCardVO vo) {
        return sdRechargeCardService.hangingSolutions(vo);
    }

    @Log("储值卡换卡")
    @ApiOperation(value = "储值卡换卡")
    @PostMapping("changeCard")
    public Result changeCard(@Valid @RequestBody UpdateRechargeCardVO vo) {
        return sdRechargeCardService.changeCard(vo);
    }

    @Log("储值卡充值查询")
    @ApiOperation(value = "储值卡充值查询")
    @PostMapping("rechargeRecord")
    public Result rechargeRecord(@Valid @RequestBody RechargeCardListVO vo) {
        Page<RechargeCardLVO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        return ResultUtil.success(sdRechargeCardPaycheckService.getRechargeCardList(vo, page));
    }

    @Log("储值卡充值查询导出")
    @ApiOperation(value = "储值卡充值查询导出")
    @PostMapping("rechargeRecordExport")
    public Result rechargeRecordExport(@Valid @RequestBody RechargeCardListVO vo) throws IOException {
        Page<RechargeCardLVO> page = new Page<>(-1, -1);
        return sdRechargeCardPaycheckService.rechargeRecordExport(vo, page);
    }

    @Log("储值卡操作记录查询")
    @ApiOperation(value = "储值卡操作记录查询")
    @PostMapping("changeRecord")
    public Result changeRecord(@Valid @RequestBody RechargeCardListVO vo) {
        Page<RechargeCardLVO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        return ResultUtil.success(sdRechargeCardChangeService.getRechargeCardList(vo, page));
    }

    @Log("当前登录人有权限的门店列表_储值卡充值查询")
    @ApiOperation(value = "当前登录人有权限的门店列表_储值卡充值查询")
    @PostMapping("getCurrentUserAuthStoList")
    public Result getCurrentUserAuthStoList() {
        return ResultUtil.success(sdRechargeCardChangeService.getCurrentUserAuthStoList());
    }

    @Log("退款")
    @ApiOperation(value = "退款")
    @PostMapping("rechargeRefund")
    public Result rechargeRefund(@Valid @RequestBody RechargeRefundVO vo) {
        sdRechargeCardPaycheckService.rechargeRefund(vo);
        return ResultUtil.success();
    }

}

