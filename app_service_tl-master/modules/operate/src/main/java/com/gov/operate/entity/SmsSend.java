package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SMS_SEND")
@ApiModel(value="SmsSend对象", description="")
public class SmsSend extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "手机号")
    @TableField("GSS_TEL")
    private String gssTel;

    @ApiModelProperty(value = "发送时间")
    @TableField("GSS_SEND_TIME")
    private String gssSendTime;

    @ApiModelProperty(value = "发送内容")
    @TableField("GSS_SEND_CONTENT")
    private String gssSendContent;

    @ApiModelProperty(value = "发送单价")
    @TableField("GSS_PRICE")
    private Integer gssPrice;

    @ApiModelProperty(value = "充值记录ID")
    @TableField("GSRR_ID")
    private Integer gsrrId;

    @ApiModelProperty(value = "是否已开票")
    @TableField("GSS_INVOICE")
    private Integer gssInvoice;

    @ApiModelProperty(value = "所属门店")
    @TableField("GSS_STORE")
    private String gssStore;
}
