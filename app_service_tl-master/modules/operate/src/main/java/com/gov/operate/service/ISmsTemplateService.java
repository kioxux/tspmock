package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SmsTemplate;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-13
 */
public interface ISmsTemplateService extends SuperService<SmsTemplate> {

    /**
     * 营销短信列表
     */
    IPage<SmsTemplate> queryMarketingTemplate(QueryMarketingTemplateVO vo);

    /**
     * 营销模板
     */
    List<SmsTemplate> marketingTemplateList();

    /**
     * 营销模板新增
     */
    void addMarketingTemplate(AddMarketingTemplateVO vo);

    /**
     * 营销模板编辑
     */
    void editMarketingTemplate(EditMarketingTemplateVO vo);

    /**
     * 营销模板测试发送
     */
    Result testMarketingTemplate(TestMarketingTemplateVO vo);

    /**
     * 营销短信发送
     */
    void sendMarketingSms(SendMarketingSmsVO vo);

    /**
     * 营销模板删除
     */
    void delMarketingTemplate(DelMarketingTemplateVO vo);

    /**
     * 营销短信启、停用
     */
    void enableMarketingTemplate(EnableMarketingTemplateVO vo);

    /**
     * 营销短详情
     */
    SmsTemplate getMarketingTemplate(String smsId);

    /**
     * 新增店长账号
     * @param verification
     * @return
     */
    Result sendStoreRegisterSms(Verification verification);
    /**
     * 将已存在账号修改为店长
     * @param verification
     * @return
     */
    Result sendUpdateStoreRegisterSms(Verification verification);

    Result getSmsAuthenticationCode(Verification verification);

    Result sendFranchiseeRegister(Verification verification, String type);

    /**
     * 营销短信剩余条数
     */
    Integer checkSendMarketingSms();
}
