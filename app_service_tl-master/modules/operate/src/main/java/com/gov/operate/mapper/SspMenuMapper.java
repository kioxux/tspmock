package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.ssp.SspMenuChainDTO;
import com.gov.operate.entity.SspMenu;

import java.util.List;

/**
 * @Entity com.gov.operate.entity.SspMenu
 */
public interface SspMenuMapper extends BaseMapper<SspMenu> {

    List<SspMenuChainDTO> selectMenuDTOList();
}




