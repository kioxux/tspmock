package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author sy
 * @since 2020-11-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_RESOURCE")
@ApiModel(value="Resource对象", description="菜单表")
public class Resource extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "权限编号")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "名称")
    @TableField("NAME")
    private String name;

    @ApiModelProperty(value = "标题")
    @TableField("TITLE")
    private String title;

    @ApiModelProperty(value = "路由/跳转地址")
    @TableField("PATH")
    private String path;

    @ApiModelProperty(value = "页面地址")
    @TableField("PAGE_PATH")
    private String pagePath;

    @ApiModelProperty(value = "请求路径")
    @TableField("REQUEST_PATH")
    private String requestPath;

    @ApiModelProperty(value = "图标")
    @TableField("ICON")
    private String icon;

    @ApiModelProperty(value = "类型（1模块 2页面 3按钮）")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "按钮类型 1-新增 2-编辑 3-查看 4-审核 5-导入 6-导出")
    @TableField("BUTTON_TYPE")
    private String buttonType;

    @ApiModelProperty(value = "父节点（空为最高节点）")
    @TableField("PARENT_ID")
    private String parentId;

    @ApiModelProperty(value = "状态(1启用 0禁用)")
    @TableField("STATUS")
    private String status;

    @ApiModelProperty(value = "排序")
    @TableField("SEQ")
    private BigDecimal seq;

    @ApiModelProperty(value = "1-web 2-运营")
    @TableField("MODULE")
    private String module;


}
