package com.gov.operate.dto.weChat;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class GetWeChatMenuVO {

    @NotBlank(message = "加盟不能为空")
    private String client;

    @NotBlank(message = "对象编码不能为空")
    private String goaCode;

}
