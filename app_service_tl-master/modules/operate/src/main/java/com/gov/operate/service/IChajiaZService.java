package com.gov.operate.service;

import com.gov.operate.entity.ChajiaZ;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 采购入库差价单主表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-12-20
 */
public interface IChajiaZService extends SuperService<ChajiaZ> {

}
