package com.gov.operate.mapper;

import com.gov.operate.entity.Train;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-26
 */
public interface TrainMapper extends BaseMapper<Train> {

}
