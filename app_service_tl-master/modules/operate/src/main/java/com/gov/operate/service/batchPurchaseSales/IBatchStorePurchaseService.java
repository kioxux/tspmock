package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchStorePurchase;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 门店采购 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchStorePurchaseService extends SuperService<BatchStorePurchase> {

}
