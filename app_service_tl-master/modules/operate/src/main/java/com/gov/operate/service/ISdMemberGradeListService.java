package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.entity.SdMemberGradeList;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
public interface ISdMemberGradeListService extends SuperService<SdMemberGradeList> {

    /**
     * 会员分级统计
     * @param requestDto
     * @return
     */
    Result gradeDistributionList(MemberReportRequestDto requestDto);

    /**
     * 会员分级趋势
     * @param requestDto
     * @return
     */
    Result gradeTrendList(MemberReportRequestDto requestDto);

    Result gradeDistributionListNew(MemberReportRequestDto requestDto);

    Result gradeTrendListNew(MemberReportRequestDto requestDto);
}
