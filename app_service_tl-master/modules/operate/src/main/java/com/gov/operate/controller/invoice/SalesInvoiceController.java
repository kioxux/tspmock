package com.gov.operate.controller.invoice;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.invoice.*;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.ISalesInvoiceService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * 销售开票
 */
@Validated
@RestController
@RequestMapping("invoice/salesInvoice")
public class SalesInvoiceController {

    @Resource
    private ISalesInvoiceService salesInvoiceService;

    @Log("销售开票查询")
    @PostMapping("selectSalesInvoiceByPage")
    public Result selectSalesInvoiceByPage(@RequestBody SalesInvoicePageDTO dto) {
        return ResultUtil.success(salesInvoiceService.selectSalesInvoiceByPage(dto));
    }

    @Log("销售开票保存")
    @PostMapping("saveBill")
    public Result saveBill(@RequestBody List<SalesInvoicePageVO> list) {
        try {
            return ResultUtil.success(salesInvoiceService.saveBill(list));
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error("0001", "开票失败");
        }
    }

    @Log("开票人员")
    @PostMapping("getInvoiceUserList")
    public Result getInvoiceUserList(@RequestJson(value = "type", required = false) String type) {
        return ResultUtil.success(salesInvoiceService.getInvoiceUserList(type));
    }

    @Log("当前开票人员")
    @PostMapping("getInvoiceUser")
    public Result getInvoiceUser() {
        return salesInvoiceService.getInvoiceUser();
    }

    @Log("自动开票NEW")
    @PostMapping("invoiceAuto")
    public Result invoiceAuto(@RequestBody AutoInvoice autoInvoice) throws Exception {
        return salesInvoiceService.invoiceAuto(autoInvoice);
    }

    /**
     * 重开票
     *
     * @param gsiOrderId
     * @return
     */
    @Log("重开票")
    @PostMapping("reInvoice")
    public Result reInvoice(@RequestJson("gsiOrderId") String gsiOrderId) throws Exception {
        return salesInvoiceService.reInvoice(gsiOrderId);
    }

    /**
     * 开票信息删除
     *
     * @param gsiOrderId
     * @return
     */
    @Log("开票信息删除")
    @PostMapping("deleteInvoice")
    public Result deleteInvoice(@RequestJson("gsiOrderId") String gsiOrderId) {
        salesInvoiceService.deleteInvoice(gsiOrderId);
        return ResultUtil.success();
    }

    @Log("开票结果NEW")
    @PostMapping("invoiceReturnNew")
    public Result invoiceReturnNew() throws Exception {
        salesInvoiceService.invoiceReturnNew();
        return ResultUtil.success();
    }

    @Log("销售开票登记查询")
    @PostMapping("selectSalesInvoiceRegisterByPage")
    public Result selectSalesInvoiceRegisterByPage(@RequestBody SalesInvoiceRegisterPageDTO dto) {
        return ResultUtil.success(salesInvoiceService.selectSalesInvoiceRegisterByPage(dto));
    }

    @Log("销售开票登记合计查询")
    @PostMapping("selectSalesInvoiceRegisterTotalByPage")
    public Result selectSalesInvoiceRegisterTotalByPage(@RequestBody SalesInvoiceRegisterPageDTO dto) {
        return salesInvoiceService.selectSalesInvoiceRegisterTotalByPage(dto);
    }

    @Log("销售开票登记明细")
    @PostMapping("getRegistDetail")
    public Result getRegistDetail(@RequestBody SalesInvoiceDetailDTO dto) {
        return ResultUtil.success(salesInvoiceService.getRegistDetail(dto));
    }

    @Log("销售开票发票明细保存")
    @PostMapping("submitInvoiceDetail")
    public Result submitInvoiceDetail(@RequestJson("gsiOrderId") String gsiOrderId,
                                      @RequestJson("gsiBill") String gsiBill) {
        return salesInvoiceService.submitInvoiceDetail(gsiOrderId, gsiBill);
    }

    @Log("销售开票金额明细保存")
    @PostMapping("submitAmountDetail")
    public Result submitAmountDetail(@RequestBody SalesInvoiceDetailVO dto) {
        return salesInvoiceService.submitAmountDetail(dto);
    }

    @Log("开票回调")
    @PostMapping("callback")
    public void callback(HttpServletRequest httpServletRequest) throws Exception {
        salesInvoiceService.callback(httpServletRequest);
    }

}
