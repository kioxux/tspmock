package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_HYR_PRICE")
@ApiModel(value="SdPromHyrPrice对象", description="")
public class SdPromHyrPrice extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPHP_VOUCHER_ID")
    private String gsphpVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPHP_SERIAL")
    private String gsphpSerial;

    @ApiModelProperty(value = "编码")
    @TableField("GSPHP_PRO_ID")
    private String gsphpProId;

    @ApiModelProperty(value = "促销价")
    @TableField("GSPHP_PRICE")
    private BigDecimal gsphpPrice;

    @ApiModelProperty(value = "促销折扣")
    @TableField("GSPHP_REBATE")
    private String gsphpRebate;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPHP_INTE_FLAG")
    private String gsphpInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPHP_INTE_RATE")
    private String gsphpInteRate;


}
