package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.PromVariableTranDTO;
import com.gov.operate.entity.SdPromVariableTran;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
public interface SdPromVariableTranMapper extends BaseMapper<SdPromVariableTran> {

    /**
     * 促销获取默认值
     */
    List<PromVariableTranDTO> getPromDefaultValue(@Param("client") String client, @Param("promIdList") List<String> promIdList);

    /**
     * 促销类型
     *
     * @param client           加盟商
     * @param gspvt_voucher_id 促销单号
     * @param gspvt_type       类型
     * @return
     */
    List<String> selectTypes(@Param("client") String client, @Param("gspvt_voucher_id") String gspvt_voucher_id, @Param("gspvt_type") String gspvt_type);

    /**
     * 传输表批量更新
     */
    void updateValue(@Param("tran") SdPromVariableTran tran);

}
