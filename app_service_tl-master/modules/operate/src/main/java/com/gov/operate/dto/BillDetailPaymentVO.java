package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class BillDetailPaymentVO {

    @NotBlank(message = "业务类型不能为空")
    private String type;
    @NotBlank(message = "单据号不能为空")
    private String matDnId;
    @NotEmpty(message = "付款明细列表不能为空")
    private List<BillDetailPayment> billDetailPaymentList;

    @Data
    @EqualsAndHashCode
    public static class BillDetailPayment{
        @NotBlank(message = "商品编码不能为空")
        private String proCode;
        @NotBlank(message = "本次付款金额不能为空")
        private BigDecimal paymentAmount;
        @NotBlank(message = "物料凭证号不能为空")
        private String matId;
        @NotBlank(message = "物料凭证年份不能为空")
        private String matYear;
        @NotBlank(message = "物料凭证行号不能为空")
        private String matLineNo;
        @NotBlank(message = "业务类型不能为空")
        private String businessType;
    }

    /**
     * 加盟商
     */
    private String client;

}
