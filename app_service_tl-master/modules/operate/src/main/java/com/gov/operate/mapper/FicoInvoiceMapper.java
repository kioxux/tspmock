package com.gov.operate.mapper;

import com.gov.operate.entity.FicoInvoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-17
 */
public interface FicoInvoiceMapper extends BaseMapper<FicoInvoice> {

}
