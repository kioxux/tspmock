package com.gov.operate.controller.prom;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.ISdPromService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zs
 * @since 2020-07-20
 */
@Api(tags = "促销活动")
@RestController
@RequestMapping("gsp")
public class SdPromController {

    @Resource
    private ISdPromService sdFromService;

    @Log("查询门店列表")
    @ApiOperation(value = "查询门店列表")
    @PostMapping("getStoreList")
    public Result getStoreList(@Valid @RequestBody GetStoreListVO getStoreListVO) {
        return ResultUtil.success(sdFromService.getStoreList(getStoreListVO));
    }

    @Log("根据门店查询商品列表")
    @ApiOperation(value = "根据门店查询商品列表")
    @PostMapping("getStoreProductList")
    public Result getStoreProductList(@Valid @RequestBody GetStoreProductVO getStoreProductVO) {
        return ResultUtil.success(sdFromService.getStoreProductList(getStoreProductVO));
    }

    @Log("添加促销活动")
    @ApiOperation(value = "添加促销活动")
    @PostMapping("savePromList")
    public Result savePromList(@Valid @RequestBody SavePromListVO savePromListVO) {
        return ResultUtil.success(sdFromService.savePromList(savePromListVO));
    }

    @Log("查询促销活动列表")
    @ApiOperation(value = "查询促销活动列表")
    @PostMapping("queryPromList")
    public Result queryPromList(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromList(queryPromListVO));
    }

    @Log("查询促销活动详情")
    @ApiOperation(value = "查询促销活动详情")
    @PostMapping("queryPromDetail")
    public Result queryPromDetail(@Valid @RequestBody QueryPromDetailVO queryPromDetailVO) {
        return ResultUtil.success(sdFromService.queryPromDetail(queryPromDetailVO));
    }

    @Log("修改促销活动")
    @ApiOperation(value = "修改促销活动")
    @PostMapping("updatePromList")
    public Result updatePromList(@Valid @RequestBody SavePromListVO savePromListVO) {
        sdFromService.updatePromList(savePromListVO);
        return ResultUtil.success();
    }


    @Log("查询活动生效门店")
    @ApiOperation(value = "查询促销活动生效门店")
    @PostMapping("queryPromStoreList")
    public Result queryPromStoreList(@Valid @RequestBody QueryPromStoreListVO queryPromStoreListVO) {
        return ResultUtil.success(sdFromService.queryPromStoreList(queryPromStoreListVO));
    }

    @Log("查询促销活动简介和单号列表")
    @ApiOperation(value = "查询促销活动简介和单号列表")
    @PostMapping("queryPromVoucherList")
    public Result queryPromVoucherList(@Valid @RequestBody QueryPromStoreListVO queryPromStoreListVO) {
        return ResultUtil.success(sdFromService.queryPromVoucherList(queryPromStoreListVO));
    }


    @Log("修改促销活动状态")
    @ApiOperation(value = "修改促销活动状态")
    @PostMapping("updatePromVoucherStatus")
    public Result updatePromVoucherStatus(@Valid @RequestBody UpdatePromVoucherStatusVO updatePromVoucherStatusVO) {
        sdFromService.updatePromVoucherStatus(updatePromVoucherStatusVO);
        return ResultUtil.success();
    }

    @Log("修改促销活动状态")
    @ApiOperation(value = "修改促销活动状态")
    @PostMapping("updatePromStatus")
    public Result updatePromStatus(@RequestJson(value = "gsphMarketid", name = "活动id") Integer gsphMarketid,
                                   @RequestJson(value = "gsphStatus", name = "状态值") String gsphStatus) {
        sdFromService.updatePromStatus(gsphMarketid, gsphStatus);
        return ResultUtil.success();
    }

    @Log("查询商品各门店零售价")
    @ApiOperation(value = "查询商品各门店零售价")
    @PostMapping("queryProStoreLsj")
    public Result queryProStoreLsj(@Valid @RequestBody QueryProStoreLsjVO queryProStoreLsjVO) {
        return ResultUtil.success(sdFromService.queryProStoreLsj(queryProStoreLsjVO));
    }


    @Log("会员促销根据门店查询商品列表")
    @ApiOperation(value = "会员促销根据门店查询商品列表")
    @PostMapping("queryProListByStore")
    public Result queryProListByStore(@Valid @RequestBody QueryProListByStoreVO queryProListByStoreVO) {
        return ResultUtil.success(sdFromService.queryProListByStore(queryProListByStoreVO));
    }

    @Log("批量设置折扣")
    @ApiOperation(value = "批量设置折扣")
    @PostMapping("setDiscountBatch")
    public Result setDiscountBatch(@Valid @RequestBody QueryProListByStoreVO queryProListByStoreVO) {
        return ResultUtil.success(sdFromService.setDiscountBatch(queryProListByStoreVO));
    }


    @Log("查询会员特价详情")
    @ApiOperation(value = "查询会员特价详情")
    @PostMapping("queryHySet")
    public Result queryHySet(@RequestJson(value = "storeCode", name = "门店编码", required = false) String storeCode) {
        return ResultUtil.success(sdFromService.queryHySet(storeCode));
    }

    @Log("查询会员日折扣详情")
    @ApiOperation(value = "查询会员日折扣详情")
    @PostMapping("queryHyrDiscount")
    public Result queryHyrDiscount() {
        return ResultUtil.success(sdFromService.queryHyrDiscount());
    }

    @Log("查询会员日特价详情")
    @ApiOperation(value = "查询会员日特价详情")
    @PostMapping("queryHyrPrice")
    public Result queryHyrPrice(@RequestJson(value = "storeCode", name = "门店编码", required = false) String storeCode) {
        return ResultUtil.success(sdFromService.queryHyrPrice(storeCode));
    }

    @Log("保存会员特价详情")
    @ApiOperation(value = "保存会员特价详情")
    @PostMapping("saveHySet")
    public Result saveHySet(@Valid @RequestBody PromHyDto promHyDto) {
        sdFromService.saveHySet(promHyDto);
        return ResultUtil.success();
    }

    @Log("保存会员日折扣详情")
    @ApiOperation(value = "保存会员日折扣详情")
    @PostMapping("saveHyrDiscount")
    public Result saveHyrDiscount(@Valid @RequestBody PromHyDto promHyDto) {
        sdFromService.saveHyrDiscount(promHyDto);
        return ResultUtil.success();
    }


    @Log("保存会员日特价详情")
    @ApiOperation(value = "保存会员日特价详情")
    @PostMapping("saveHyrPrice")
    public Result saveHyrPrice(@Valid @RequestBody PromHyDto promHyDto) {
        sdFromService.saveHyrPrice(promHyDto);
        return ResultUtil.success();
    }

    @Log("商品销售级别")
    @ApiOperation(value = "商品销售级别")
    @PostMapping("getProSlaeClassList")
    public Result getProSlaeClassList() {
        return ResultUtil.success(sdFromService.getProSlaeClassList());
    }

    @Log("是否发送电子券设置获取")
    @ApiOperation(value = "是否发送电子券设置获取")
    @PostMapping("getTicketFlag")
    public Result getTicketFlag() {
        return sdFromService.getTicketFlag();
    }

    @Log("是否发送电子券设置设置")
    @ApiOperation(value = "是否发送电子券设置设置")
    @PostMapping("setTicketFlag")
    public Result setTicketFlag(@RequestJson("gcspPara1") String gcspPara1) {
        sdFromService.setTicketFlag(gcspPara1);
        return ResultUtil.success();
    }

    @Log("查询促销商品列表")
    @ApiOperation(value = "查询促销商品列表")
    @PostMapping("queryPromGoodsList")
    public Result queryPromGoodsList(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromGoodsList(queryPromListVO));
    }

    @Log("单品类促销查询")
    @ApiOperation(value = "单品类促销查询")
    @PostMapping("queryPromUnitaryGood")
    public Result queryPromUnitaryGood(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromUnitaryGood(queryPromListVO));
    }

    @Log("赠品类促销查询")
    @ApiOperation(value = "赠品类促销查询")
    @PostMapping("queryPromGiftGood")
    public Result queryPromGiftGood(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromGiftGood(queryPromListVO));
    }

    @Log("赠品类促销商品列表")
    @ApiOperation(value = "赠品类促销商品列表")
    @PostMapping("queryPromGiftProList")
    public Result queryPromGiftProList(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromGiftProList(queryPromListVO));
    }

    @Log("系列类促销查询")
    @ApiOperation(value = "系列类促销查询")
    @PostMapping("queryPromSeriesGood")
    public Result queryPromSeriesGood(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromSeriesGood(queryPromListVO));
    }

    @Log("查询促销活动列表")
    @ApiOperation(value = "查询促销活动列表")
    @PostMapping("queryPromListForVoucherId")
    public Result queryPromListForVoucherId(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromListForVoucherId(queryPromListVO));
    }

    @Log("组合类促销查询")
    @ApiOperation(value = "组合类促销查询")
    @PostMapping("queryPromAssoGood")
    public Result queryPromAssoGood(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromAssoGood(queryPromListVO));
    }

    @Log("组合类促销商品列表")
    @ApiOperation(value = "组合类促销商品列表")
    @PostMapping("queryPromAssoProList")
    public Result queryPromAssoProList(@Valid @RequestBody QueryPromListVO queryPromListVO) {
        return ResultUtil.success(sdFromService.queryPromAssoProList(queryPromListVO));
    }
}
