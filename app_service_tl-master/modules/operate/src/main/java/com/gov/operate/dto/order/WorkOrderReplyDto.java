package com.gov.operate.dto.order;

import lombok.Data;

import java.util.List;

/**
 * @author 钱金华
 * @date 21-2-18 17:30
 */
@Data
public class WorkOrderReplyDto extends WorkOrderReply {

    /**
     * 发送时间
     */
    private String gworSendTimeStr;

    /**
     * 客服名称
     */
    private String replyUserName;


    /**
     * 附件
     */
    List<WorkOrderReplyAtt> workOrderReplyAtts;



}
