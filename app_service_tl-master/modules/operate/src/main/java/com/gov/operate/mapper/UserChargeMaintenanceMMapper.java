package com.gov.operate.mapper;

import com.gov.operate.entity.UserChargeMaintenanceM;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户收费维护明细表 Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface UserChargeMaintenanceMMapper extends BaseMapper<UserChargeMaintenanceM> {

}
