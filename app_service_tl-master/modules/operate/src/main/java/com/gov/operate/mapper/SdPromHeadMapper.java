package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdPromAssoConds;
import com.gov.operate.entity.SdPromGiftResult;
import com.gov.operate.entity.SdPromHead;
import com.gov.operate.entity.StoreData;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-28
 */
public interface SdPromHeadMapper extends BaseMapper<SdPromHead> {

    void insertList(@Param("list") List<SdPromHead> sdPromHeadInsertList);

    Integer createVoucherId(@Param("client") String client);

    //    @Select("SELECT DISTINCT " +
//            "  DISTINCT(a.STO_CODE ) as stoCode," +
//            "  a.STO_NAME as stoName " +
//            "FROM " +
//            "  GAIA_STORE_DATA a " +
//            "  LEFT JOIN GAIA_SD_PROM_HEAD b ON b.GSPH_BR_ID = a.STO_CODE " +
//            "WHERE " +
//            "  b.GSPH_MARKETID = #{gsphMarketid}  " +
//            "  AND a.CLIENT = #{client} " +
//            " ORDER By a.STO_CODE")
    List<StoreData> queryPromStoreList(@Param("gsphMarketid") Integer gsphMarketid, @Param("client") String client);

    @Update("update GAIA_SD_PROM_HEAD set GSPH_STATUS = #{gsphStatus} where GSPH_MARKETID = #{gsphMarketid} and GSPH_VOUCHER_ID = #{gsphVoucherId} and CLIENT = #{client}")
    void updatePromVoucherStatus(@Param("gsphMarketid") Integer gsphMarketid, @Param("gsphVoucherId") String gsphVoucherId, @Param("gsphStatus") String gsphStatus, @Param("client") String client);

    @Update("update GAIA_SD_PROM_HEAD set GSPH_STATUS = #{gsphStatus} where GSPH_MARKETID = #{gsphMarketid} and CLIENT = #{client}")
    void updatePromStatus(@Param("gsphMarketid") Integer gsphMarketid, @Param("gsphStatus") String gsphStatus, @Param("client") String client);

    IPage<QueryProListByStoreDto> queryProListByStore(@Param("page") Page<QueryProListByStoreDto> page,
                                                      @Param("stoCodeList") List<String> stoCodeList,
                                                      @Param("proName") String proName,
                                                      @Param("storeName") String storeName,
                                                      @Param("client") String client,
                                                      @Param("proSlaeClass") String proSlaeClass,
                                                      @Param("proClass") String proClass,
                                                      @Param("proStorageArea") Integer proStorageArea,
                                                      @Param("rowList") List<QueryProListByStoreVO.ProSto> rowList,
                                                      @Param("hasSelectedRow") Integer hasSelectedRow,
                                                      @Param("unHasSelectedRow") Integer unHasSelectedRow);

    List<QueryProListByStoreDto> queryProListByStoreBatch(@Param("stoCodeList") List<String> stoCodeList,
                                                          @Param("proName") String proName,
                                                          @Param("storeName") String storeName,
                                                          @Param("client") String client,
                                                          @Param("proSlaeClass") String proSlaeClass,
                                                          @Param("proClass") String proClass,
                                                          @Param("proStorageArea") Integer proStorageArea,
                                                          @Param("rowList") List<QueryProListByStoreVO.ProSto> rowList,
                                                          @Param("hasSelectedRow") Integer hasSelectedRow,
                                                          @Param("unHasSelectedRow") Integer unHasSelectedRow);

    List<StoreData> getStoreList(@Param("client") String client, @Param("keyLike") String keyLike, @Param("gssgId") String gssgId, @Param("stoDistrict") String stoDistrict);

    IPage<QueryProListByStoreDto> getStoreProductList(Page<QueryProListByStoreDto> page, @Param("proKey") String proKey, @Param("client") String client);

    @Select("SELECT\n" +
            "\ta.PRO_LSJ as proLsj,\n" +
            "\tb.STO_CODE as stoCode,\n" +
            "\tb.STO_NAME as stoName\n" +
            "FROM\n" +
            "\tGAIA_PRODUCT_BUSINESS a\n" +
            "\tINNER JOIN GAIA_STORE_DATA b ON a.PRO_SITE = b.STO_CODE \n" +
            "WHERE\n" +
            "\ta.PRO_SELF_CODE = #{proSelfCode} \n" +
            "\tAND a.CLIENT = #{client} \n" +
            "\tAND b.CLIENT = #{client} \n" +
            "ORDER BY\n" +
            "\ta.PRO_SITE ASC ")
    List<QueryProStoreLsjDTO> queryProStoreLsj(@Param("proSelfCode") String proSelfCode, @Param("client") String client);

    List<PromHyDto.HySet> queryHySetByVoucherIds(@Param("client") String client, @Param("voucherIds") List<String> voucherIds);

    List<PromHyDto.HyrDiscount> queryHyrDiscountByVoucherIds(@Param("client") String client, @Param("voucherIds") List<String> voucherIds);

    List<PromHyDto.HyrPrice> queryHyrPriceByVoucherIds(@Param("client") String client, @Param("voucherIds") List<String> voucherIds);

    @Select("SELECT IFNULL(MIN(CAST(GSPH_MARKETID AS SIGNED)),0) - 1 FROM GAIA_SD_PROM_HEAD WHERE CLIENT = #{client} AND GSPH_MARKETID IS NOT NULL AND LENGTH(GSPH_MARKETID) > 0")
    Integer getMaxMarketId(@Param("client") String client);

    /**
     * 促销列表
     *
     * @param page
     * @param queryPromListVO
     * @param client
     * @return
     */
    Page<SdPromHead> queryPromList(IPage<SdPromHead> page, @Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);

    /**
     * 促销商品列表
     *
     * @param page
     * @param queryPromListVO
     * @param client
     * @return
     */
    Page<PromGoodsListVO> queryPromGoodsList(IPage<PromGoodsListVO> page, @Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);

    /**
     * 赠品列表
     *
     * @param page
     * @param client
     * @param proCode
     * @param voucherId
     * @return
     */
    Page<SdPromGiftResult> queryPromGiftProList(IPage<SdPromGiftResult> page, @Param("voucherId") String voucherId, @Param("client") String client, @Param("proCode") String proCode);

    /**
     * 组合商品列表
     *
     * @param page
     * @param client
     * @param proCode
     * @param voucherId
     * @return
     */
    Page<SdPromAssoConds> queryPromAssoProList(IPage<SdPromAssoConds> page, @Param("voucherId") String voucherId, @Param("client") String client, @Param("proCode") String proCode);

    /**
     * 单品类促销商品列表查询
     *
     * @param queryPromListVO
     * @return
     */
    List<PromUnitaryProductVO> queryPromUnitaryGood(@Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);

    /**
     * 赠品类促销系列查询
     *
     * @param queryPromListVO
     * @param client
     * @return
     */
    List<PromGiftProductVO> queryPromGiftGood(@Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);

    /**
     * 系列类促销系列查询
     *
     * @param queryPromListVO
     * @param client
     * @return
     */
    List<PromSeriesVO> queryPromSeriesGood(@Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);

    /**
     * 系列类促销系列查询
     *
     * @param queryPromListVO
     * @param client
     * @return
     */
    List<PromAssoVO> queryPromAssoGood(@Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);

    /**
     * 会员特价、会员日特价
     *
     * @return
     */
    List<SdPromHead> selectHyProm();

    /**
     * 获取促销活动
     *
     * @param clients clients
     * @return List<SdPromHead>
     */
    List<SdPromHead> selectPromotions(@Param("clients") List<String> clients);

    /**
     * 促销活动
     *
     * @param queryPromListVO
     * @param client
     * @return
     */
    List<SdPromHead> queryPromListForVoucherId(@Param("vo") QueryPromListVO queryPromListVO, @Param("client") String client);
}
