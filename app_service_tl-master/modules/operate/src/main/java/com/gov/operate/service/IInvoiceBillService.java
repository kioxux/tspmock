package com.gov.operate.service;

import com.gov.operate.entity.InvoiceBill;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
public interface IInvoiceBillService extends SuperService<InvoiceBill> {

}
