package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchHeadStock;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 总部库存 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchHeadStockService extends SuperService<BatchHeadStock> {

}
