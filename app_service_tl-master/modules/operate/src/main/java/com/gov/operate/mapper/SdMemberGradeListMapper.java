package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.GradeDistributionVO;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.entity.SdMemberGradeList;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
public interface SdMemberGradeListMapper extends BaseMapper<SdMemberGradeList> {

    List<GradeDistributionVO> getGradeDistributionList(MemberReportRequestDto requestDto);

    List<GradeDistributionVO> getGradeTrendList(MemberReportRequestDto requestDto);

    List<GradeDistributionVO> getGradeDistributionListNew(MemberReportRequestDto requestDto);

    List<GradeDistributionVO> getGradeTrendListNew(MemberReportRequestDto requestDto);
}