package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.SdExasearchD;
import com.gov.operate.entity.SdMarketingSearch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SmsSend;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.ResultHandler;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-23
 */
public interface SdMarketingSearchMapper extends BaseMapper<SdMarketingSearch> {

    String getMaxId(@Param("client") String client);

    /**
     * 精准查询03版
     */
/*    IPage<GetMemberListDTO> getMemberList(Page page, @Param("vo") GetMemberListVO3 vo);

    List<GetMemberListDTO> getMemberList3(@Param("vo") GetMemberListVO3 vo);*/

    IPage<GetMemberListDTO> getMemberPage3(Page page, @Param("vo") GetMemberListVO3 vo);

    List<SdMarketingSearch> getCurrentSendSearch();

    /**
     * 短信发送记录保存
     */
    void insertExasearchDList(@Param("list") List<SdExasearchD> exasearchDList);

    /**
     * 短信记录保存
     */
    void insertSmsSendList(@Param("list") List<SmsSend> smsSendList);

    /**
     * 短信充值记录
     */
    List<SmsRechargeRecordDTO> smsRechargeRecord(@Param("client") String client);

    void seachDataList(@Param("vo") GetMemberListVO3 vo, ResultHandler<GetMemberListDTO> getMemberListDTOResultHandler);

    List<Map<String, String>> getPaymentList(String client);

    IPage<FactoryDetailVo> getFactoryPage(Page page,@Param("dto") GetFactoryPageDto dto);

    IPage<DiseaseDetailVo> getDiseasePage(Page page,@Param("dto") GetDiseasePageDto dto);

    IPage<ProductDetailVO> getProductPage(Page page,@Param("dto") GetProductPageDto dto);

    IPage<ProComDetailVO> getProComPage(Page page, @Param("dto") GetProComPageDto dto);

    IPage<BrandDetailVo> getBrandPage(Page page,@Param("dto") GetBrandPageDto dto);

    IPage<ProductClassDetailVo> getProductClassPage(Page page,@Param("dto") GetProductClassPageDto dto);

    IPage<ProductGroupVo> getProGroupPage(Page page,@Param("dto") GetProGroupPageDto dto);

    List<String> getProCompByDisaseCode(String diseaseQueryCondition);

    List<String> getClients();

    List<Map<String,Object>> getMemberActiveList(String client);

    void dailyInertMemberActive(List<Map<String, Object>> insertMemberActiveList);

    List<Map<String, Object>> getMemberActiveData(String client);

    void dailyUpdateMemberActive(List<Map<String, Object>> updateMemberActiveList);

    /**
     * 电子券主题列表
     *
     * @param client
     * @param page
     */
    IPage<ElectronThemeDTO> getElectronThemeList(Page page, @Param("client") String client);

    /**
     * 电子券业务基础列表
     *
     * @param client
     */
    List<ElectronBusinessDTO> getElectronBusinessList(@Param("client")  String client);

    /**
     * 电子券自动发券列表
     *
     * @param client
     * @param gsetsId
     */
    List<ElectronAutoDTO> getElectronAutoList(@Param("client")  String client, @Param("gsetsId")  String gsetsId);
}
