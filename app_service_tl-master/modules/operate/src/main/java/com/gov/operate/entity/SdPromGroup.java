package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销商品分组表
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_GROUP")
@ApiModel(value="SdPromGroup对象", description="促销商品分组表")
public class SdPromGroup extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "商品组ID")
    @TableField("GROUP_ID")
    private String groupId;

    @ApiModelProperty(value = "分组名")
    @TableField("GROUP_NAME")
    private String groupName;

    @ApiModelProperty(value = "创建人")
    @TableField("CREATE_USER")
    private String createUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("CREATE_DATE")
    private String createDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("CREATE_TIEM")
    private String createTiem;

    @ApiModelProperty(value = "标记")
    @TableField("GROUP_FLAG")
    private String groupFlag;


}
