package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.service.IOfficialAccountsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-28
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Resource
    private IOfficialAccountsService officialAccountsService;

    @Log("推文定时推送测试")
    @ApiOperation(value = "推文定时推送测试")
    @GetMapping("tweetsPushJob")
    public Result getTweetsContent() {
        officialAccountsService.tweetsPushJob();
        return ResultUtil.success();
    }
}

