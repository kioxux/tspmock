package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统模块参数表
 * </p>
 *
 * @author sy
 * @since 2021-09-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SYSTEM_PARA")
@ApiModel(value="SystemPara对象", description="系统模块参数表")
public class SystemPara extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "编码")
    @TableField("CODE")
    private String code;

    @ApiModelProperty(value = "名称")
    @TableField("NAME")
    private String name;

    @ApiModelProperty(value = "分类(1.门店模块 , 2.后台模块,3.APP模块,4.后台加盟商配置)")
    @TableField("CLASSIFY")
    private String classify;

    @ApiModelProperty(value = "模块ID")
    @TableField("MODEL")
    private String model;

    @ApiModelProperty(value = "平台(1.web 2.app)")
    @TableField("PLATFORM")
    private String platform;

    @ApiModelProperty(value = "适用加盟商(默认AL)")
    @TableField("APPLY_CLIENT")
    private String applyClient;

    @ApiModelProperty(value = "备注")
    @TableField("REMARK")
    private String remark;

    @ApiModelProperty(value = "删除(Y.删除  N.不删除)")
    @TableField("DELETE_FLAG")
    private String deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @TableField("CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人")
    @TableField("CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @TableField("MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人")
    @TableField("MOD_ID")
    private String modId;


}
