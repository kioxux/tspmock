package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.LambdaUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdCleandrawerDService;
import com.gov.operate.service.ISdCleandrawerHService;
import com.gov.operate.service.ISdExamineHService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Service
public class SdCleandrawerHServiceImpl extends ServiceImpl<SdCleandrawerHMapper, SdCleandrawerH> implements ISdCleandrawerHService {

    @Resource
    private CommonService commonService;

    @Resource
    private ProductBusinessMapper productBusinessMapper;

    @Resource
    private UserDataMapper userDataMapper;

    @Resource
    private SdStockBatchMapper sdStockBatchMapper;

    @Resource
    private SdCleandrawerDMapper sdcleandrawerdmapper;

    @Resource
    private ISdExamineHService isdexaminehservice;

    @Resource
    private ISdCleandrawerDService cleandrawerdService;

    @Resource
    private SdExamineDMapper examinedMapper;

    @Resource
    private ClSystemParaMapper clSystemParaMapper;

    @Override
    public List<SdExamineH> getExamineInfo() {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //获取所有清斗单
        List<SdCleandrawerH> clearList = this.list(new QueryWrapper<SdCleandrawerH>()
                .eq("CLIENT", user.getClient())
                .eq("GSCH_BR_ID", user.getDepId()));
        //获取清斗中已存在的验收单号
        List<String> exaIds = null;
        if (!clearList.isEmpty()) {
            exaIds = clearList.stream().map(SdCleandrawerH::getGschExaVoucherId).collect(Collectors.toList());
        }
        //获取验收单信息
        QueryWrapper<SdExamineH> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("GSEH_VOUCHER_ID", "GSEH_DATE", "GSEH_TOTAL_AMT", "GSEH_TOTAL_QTY")
                .eq("CLIENT", user.getClient())
                .eq("GSEH_BR_ID", user.getDepId());

        queryWrapper.apply(" GSEH_VOUCHER_ID IN (" +
                        "SELECT A.GSED_VOUCHER_ID FROM GAIA_SD_EXAMINE_D A " +
                        "LEFT JOIN GAIA_PRODUCT_BUSINESS B ON A.CLIENT = B.CLIENT AND A.GSED_BR_ID = B.PRO_SITE and A.GSED_PRO_ID = B.PRO_SELF_CODE " +
                        "WHERE A.CLIENT = {0} AND B.PRO_STORAGE_AREA = '3'" +
                        ")",
                user.getClient());

        if (exaIds != null) {
            queryWrapper.notIn("GSEH_VOUCHER_ID", exaIds);
        }
        queryWrapper.ne("GSEH_TYPE", "代销验收单");
        queryWrapper.orderByDesc("GSEH_DATE", "GSEH_VOUCHER_ID")
                .last("limit 0,15");
        return isdexaminehservice.list(queryWrapper);
    }

    @Override
    public IPage<GetProductDTO> getCleanDrawderProductList(GetProductVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        Page<GetProductDTO> page = new Page<GetProductDTO>(vo.getPageNum(), vo.getPageSize());
        IPage<GetProductDTO> list = productBusinessMapper.getCleanDrawderProductList(page, vo.getSearcheContent(), user.getClient(), user.getDepId());
        if (list.getRecords().isEmpty()) {
            throw new CustomResultException("搜索无此商品");
        }
        return list;
    }

    @Override
    public List<BatchNoValidDto> getBatchNoValidList(GetBatchNoValidListVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //查询批号
        List<SdStockBatch> list = sdStockBatchMapper.selectList(new QueryWrapper<SdStockBatch>()
                .select("GSSB_PRO_ID", "GSSB_BATCH_NO", "GSSB_VAILD_DATE", "GSSB_QTY")
                .eq("CLIENT", user.getClient())
                .eq("GSSB_BR_ID", user.getDepId())
                .eq("GSSB_PRO_ID", vo.getGssbProId())
                .gt("GSSB_QTY", 0));
        if (list.isEmpty()) {
            throw new CustomResultException("该商品无批号效期记录，请核对后输入！");
        }
        List<BatchNoValidDto> result = new ArrayList<>();
        list.forEach(item -> {
            BatchNoValidDto batchNoValidDto = new BatchNoValidDto();
            BeanUtils.copyProperties(item, batchNoValidDto);
            batchNoValidDto.setGscdValidDate(item.getGssbVaildDate());
            result.add(batchNoValidDto);
        });
        return result;
    }

    @Override
    public List<UserData> getCleanDrawerPersonnel(GetCleanDrawerPersonnelVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        List<UserData> list = new ArrayList<>();
        if (vo.getCheckShop().equals("0")) {
            list = userDataMapper.getCleanDrawerPersonnel(user.getClient(), user.getDepId(), "");

        } else if (vo.getCheckShop().equals("1")) {
            list = userDataMapper.getCurrentUser(user.getClient(), user.getUserId());
        }
        if (list.isEmpty()) {
            throw new CustomResultException("暂无人员,请核查该登录人员是否配置药师权限！");
        }
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String addCleanDrawer(AddCleanDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //校验参数
        if (StringUtils.isEmpty(vo.getGschEmp())
                || vo.getDetails().isEmpty()
        ) {
            throw new CustomResultException("请填写必填信息后保存");
        }
        return addCleanDrawer(vo, user, 1);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result checkCleanDrawer(CheckCleanDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //判断清斗编号是否为空  是 直接保存 否 修改审核状态
        if (StringUtils.isEmpty(vo.getGschVoucherId())) {
            if ("1".equals(vo.getCheckShop())) {
                throw new CustomResultException("未传入清斗单号，不允许审核");
            } else {
                addCleanDrawer(vo, user, 2);
            }
        } else {
            //校验清斗单号
            QueryWrapper<SdCleandrawerH> wrapper = new QueryWrapper();
            wrapper.eq("CLIENT", user.getClient())
                    .eq("GSCH_BR_ID", user.getDepId())
                    .eq("GSCH_VOUCHER_ID", vo.getGschVoucherId());
            SdCleandrawerH cleanH = this.getOne(wrapper);
            if (cleanH == null) {
                throw new CustomResultException("清斗单号不存在");
            }
            if ("1".equals(cleanH.getGschStatus())) {
                throw new CustomResultException("清斗单已审核");
            }
            SdCleandrawerH clean = new SdCleandrawerH();
            if ("1".equals(vo.getCheckShop())) {
                clean.setGschEmp1(user.getUserId());
            }
            clean.setGschStatus("1");
            this.update(clean, wrapper);
            //审核之前 删除数据 在审核
            sdcleandrawerdmapper.delete(new QueryWrapper<SdCleandrawerD>()
                    .eq("CLIENT", user.getClient())
                    .eq("GSCD_VOUCHER_ID", cleanH.getGschVoucherId())
                    .eq("GSCD_DATE", cleanH.getGschDate()));
            //重新插入页面数据
            List<AddCleanDrawerDetailVO> details = vo.getDetails();
            List<SdCleandrawerD> cleanDs = new ArrayList<>();
            details.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                //校验参数
                if (StringUtils.isEmpty(item.getGscdProId())
                        || StringUtils.isEmpty(item.getGscdBatchNo())
                        || StringUtils.isEmpty(item.getGscdQty())
                        || StringUtils.isEmpty(item.getGscdValidDate())
                ) {
                    throw new CustomResultException("请填写必填信息后保存");
                }
                //校验清斗明细
                SdCleandrawerD cleanDrawerD = sdcleandrawerdmapper.selectOne(new QueryWrapper<SdCleandrawerD>()
                        .eq("CLIENT", user.getClient())
                        .eq("GSCD_VOUCHER_ID", cleanH.getGschVoucherId())
                        .eq("GSCD_DATE", cleanH.getGschDate())
                        .eq("GSCD_PRO_ID", item.getGscdProId())
                        .eq("GSCD_BATCH_NO", item.getGscdBatchNo()));
                if (cleanDrawerD != null) {
                    throw new CustomResultException("清斗明细已存在");
                }
                //新增清斗明细
                SdCleandrawerD cleanD = new SdCleandrawerD();
                BeanUtils.copyProperties(item, cleanD);
                cleanD.setClient(user.getClient());
                cleanD.setGscdVoucherId(cleanH.getGschVoucherId());
                cleanD.setGscdDate(cleanH.getGschDate());
                cleanD.setGscdQty(item.getGscdQty());
                cleanD.setGscdSerial(String.valueOf(index));
                cleanDs.add(cleanD);
            }));
            if (!cleanDs.isEmpty()) {
                cleandrawerdService.saveBatch(cleanDs);
            }
        }
        return ResultUtil.success(1);
    }

    private String addCleanDrawer(AddCleanDrawerVO vo, TokenUser user, Integer type) {
        //查询已经存在的清斗数据
        List<SdCleandrawerH> hList = this.list();
        Integer num = hList.isEmpty() ? 1 : hList.size() + 1;
       /* //重复校验
        SdCleandrawerH clean = this.getOne(new QueryWrapper<SdCleandrawerH>()
                .eq("CLIENT", user.getClient())
                .eq("GSCH_BR_ID", user.getDepId())
                .eq("GSCH_EXA_VOUCHER_ID", vo.getGschExaVoucherId())
                .eq("GSCH_EMP", vo.getGschEmp()));
        if (clean != null) {
            throw new CustomResultException("清斗单中包含已存在数据");
        }*/
        //清斗日期
        String gschDate = DateUtils.getCurrentDateStrYYMMDD();
        //  清斗编号
        String gscdVoucherId = "QD" + DateUtils.getCurrentYearStr()
                + String.format("%7d", num).replace(" ", "0");
        // 新增清斗
        SdCleandrawerH cleanH = new SdCleandrawerH();
        BeanUtils.copyProperties(vo, cleanH);
        cleanH.setGschVoucherId(gscdVoucherId);
        cleanH.setClient(user.getClient());
        cleanH.setGschBrId(user.getDepId());
        cleanH.setGschStatus(type == 1 ? "0" : "1");
        cleanH.setGschDate(gschDate);
        baseMapper.insert(cleanH);
        //循环处理清斗数据
        for (int i = 0; i < vo.getDetails().size(); i++) {
            AddCleanDrawerDetailVO drawerVO = vo.getDetails().get(i);
            //校验参数
            if (StringUtils.isEmpty(drawerVO.getGscdProId())
                    || StringUtils.isEmpty(drawerVO.getGscdBatchNo())
                    || StringUtils.isEmpty(drawerVO.getGscdQty())
                    || StringUtils.isEmpty(drawerVO.getGscdValidDate())
            ) {
                throw new CustomResultException("请填写必填信息后保存");
            }
            //校验清斗明细
            SdCleandrawerD cleanDrawerD = sdcleandrawerdmapper.selectOne(new QueryWrapper<SdCleandrawerD>()
                    .eq("CLIENT", user.getClient())
                    .eq("GSCD_VOUCHER_ID", gscdVoucherId)
                    .eq("GSCD_DATE", gschDate)
                    .eq("GSCD_PRO_ID", drawerVO.getGscdProId())
                    .eq("GSCD_BATCH_NO", drawerVO.getGscdBatchNo()));
            if (cleanDrawerD != null) {
                throw new CustomResultException("清斗明细已存在");
            }
            //新增清斗明细
            SdCleandrawerD cleanD = new SdCleandrawerD();
            BeanUtils.copyProperties(drawerVO, cleanD);
            cleanD.setClient(user.getClient());
            cleanD.setGscdVoucherId(gscdVoucherId);
            cleanD.setGscdDate(gschDate);
            cleanD.setGscdQty(drawerVO.getGscdQty());
            cleanD.setGscdSerial(String.valueOf(i + 1));
            sdcleandrawerdmapper.insert(cleanD);
        }
        return gscdVoucherId;
    }

    @Override
    public IPage<CleanDrawerVO> listCleanDrawer(ListCleanDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        List<String> siteCodes = new ArrayList<>();
        if (vo.getSiteCodes() != null && vo.getSiteCodes().length != 0) {
            siteCodes= Arrays.asList(vo.getSiteCodes());
        } else {
            if (StringUtils.isEmpty(user.getDepId())) {
                throw new CustomResultException("请选择门店");
            } else {
                siteCodes.add(user.getDepId());
            }
        }
        Page<CleanDrawerVO> page = new Page<>(vo.getPageNum(), vo.getPageSize());

        QueryWrapper<SdCleandrawerH> query = new QueryWrapper<>();
        query.eq(" h.CLIENT", user.getClient());
        query.in(" h.GSCH_BR_ID", siteCodes);
        if (StringUtils.isNotBlank(vo.getGschVoucherId())) {
            query.eq(" h.GSCH_VOUCHER_ID", vo.getGschVoucherId());
        }
        if (StringUtils.isNotBlank(vo.getBeginDate())) {
            query.ge(" h.GSCH_DATE", vo.getBeginDate());
        }
        if (StringUtils.isNotBlank(vo.getEndDate())) {
            query.le(" h.GSCH_DATE", vo.getEndDate());
        }
        if (StringUtils.isNotBlank(vo.getGschEmp1())) {
            query.eq(" h.GSCH_EMP1", vo.getGschEmp1());
        }
        query.orderByDesc(" h.GSCH_DATE");
        IPage<CleanDrawerVO> iPage = baseMapper.getPage(page, user.getClient(), user.getDepId(), vo.getGscdProId(), query);
        return iPage;
    }

    @Override
    public List<CleanDrawerDetailDTO> getCleanDrawer(String gschVoucherId) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //清斗单主表
        SdCleandrawerH cleanH = this.getOne(new QueryWrapper<SdCleandrawerH>()
                .eq("CLIENT", user.getClient())
                .eq("GSCH_BR_ID", user.getDepId())
                .eq("GSCH_VOUCHER_ID", gschVoucherId));
        if (cleanH == null) {
            throw new CustomResultException("清斗单号不存在");
        }
        //清斗单明细
        List<SdCleandrawerD> cleanDs = sdcleandrawerdmapper.selectList(new QueryWrapper<SdCleandrawerD>()
                .eq("CLIENT", user.getClient())
                .eq("GSCD_DATE", cleanH.getGschDate())
                .eq("GSCD_VOUCHER_ID", gschVoucherId));
        if (cleanDs.isEmpty()) {
            throw new CustomResultException("清斗单明细不存在");
        }
        List<CleanDrawerDetailDTO> details = new ArrayList<>();
        cleanDs.forEach(item -> {
            CleanDrawerDetailDTO dto = new CleanDrawerDetailDTO();
            BeanUtils.copyProperties(item, dto);
            //商品主数据业务信息
            ProductBusiness productBusiness = productBusinessMapper.selectOne(new QueryWrapper<ProductBusiness>()
                    .select("PRO_COMMONNAME", "PRO_SPECS", "PRO_FACTORY_NAME", "PRO_PLACE", "PRO_UNIT", "PRO_REGISTER_NO")
                    .eq("CLIENT", user.getClient())
                    .eq("PRO_SITE", cleanH.getGschBrId())
                    .eq("PRO_SELF_CODE", item.getGscdProId()));
            if (productBusiness == null) {
                throw new CustomResultException("商品不存在");
            }
            BeanUtils.copyProperties(productBusiness, dto);
            dto.setGscdQty(new BigDecimal(item.getGscdQty()));
            details.add(dto);
        });
        return details;
    }

    @Override
    public Result getHighlightInfo(GetHighlightVO vo) {
        return getHighlightInfoNew(vo);
    }

    /**
     * 通过验收单获取完整的清斗信息
     */
    private Result getHighlightInfoNew(GetHighlightVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        List<GetHighlightDTO> list = examinedMapper.getHighlightInfo(user.getClient(), user.getDepId(), vo.getGsehVoucherId());
        //返回数据
        List<CleanDrawerDetailDTO> details = new ArrayList<>();
        if (!list.isEmpty()) {
            list.forEach(item -> {
                List<SdStockBatch> stockBatch = sdStockBatchMapper.selectList(new QueryWrapper<SdStockBatch>()
                        .select("GSSB_QTY")
                        .eq("CLIENT", user.getClient())
                        .eq("GSSB_BR_ID", user.getDepId())
                        .eq("GSSB_PRO_ID", item.getGsedProId())
                        .in("GSSB_BATCH_NO", item.getGsedBatchNo().split(","))
                        .ge(StringUtils.isNotBlank(item.getGsedValidDate()), "GSSB_VAILD_DATE", item.getGsedValidDate())
                        .ne("GSSB_QTY", 0));
                if (!CollectionUtils.isEmpty(stockBatch)) {
                    CleanDrawerDetailDTO dto = new CleanDrawerDetailDTO();
                    dto.setGscdProId(item.getGsedProId());
                    dto.setGscdBatchNo(item.getGsedBatchNo());
                    dto.setGscdValidDate(item.getGsedValidDate());
                    BigDecimal sum = stockBatch.stream().map(SdStockBatch::getGssbQty).reduce(BigDecimal.ZERO, BigDecimal::add);
                    if (sum.compareTo(BigDecimal.ZERO) >= 0) {
                        dto.setGscdQty(sum);
                        //通用信息
                        GetFormDTO info = productBusinessMapper.getFormInfo(user.getClient(), user.getDepId(), item.getGsedProId());
                        if (info != null) {
                            BeanUtils.copyProperties(info, dto);
                            details.add(dto);
                        }

                    }
                }
            });
        }
        if (CollectionUtils.isEmpty(details)) {
            throw new CustomResultException("该验收单没有中药或超过库存有效期");
        }
        return ResultUtil.success(details);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result updateCleanDrawer(UpdateCleanDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        QueryWrapper<SdCleandrawerH> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", user.getClient())
                .eq("GSCH_BR_ID", user.getDepId())
                .eq("GSCH_VOUCHER_ID", vo.getGschVoucherId())
                .eq("GSCH_STATUS", "0");
        SdCleandrawerH cleanH = this.getOne(queryWrapper);
        if (cleanH == null) {
            throw new CustomResultException("清斗单不存在");
        }
        if ("1".equals(cleanH.getGschStatus())) {
            throw new CustomResultException("装斗单已审核");
        }
        //更新清斗单
        BeanUtils.copyProperties(vo, cleanH);
        String gschDate = DateUtils.getCurrentDateStrYYMMDD();
        cleanH.setGschDate(gschDate);
        this.update(cleanH, queryWrapper);
        //删除清斗单明细 然后添加
        sdcleandrawerdmapper.delete(new QueryWrapper<SdCleandrawerD>()
                .eq("CLIENT", user.getClient())
                .eq("GSCD_VOUCHER_ID", vo.getGschVoucherId()));
        //新增清斗明细
        List<SdCleandrawerD> cleanDs = new ArrayList<>();
        vo.getDetails().forEach(LambdaUtils.consumerWithIndex((item, index) -> {
            SdCleandrawerD cleanD = new SdCleandrawerD();
            BeanUtils.copyProperties(vo, cleanD);
            cleanD.setClient(user.getClient());
            cleanD.setGscdVoucherId(vo.getGschVoucherId());
            cleanD.setGscdDate(gschDate);
            cleanD.setGscdSerial(String.valueOf(index));
            // 商品编码
            cleanD.setGscdProId(item.getGscdProId());
            // 批号
            cleanD.setGscdBatchNo(item.getGscdBatchNo());
            // 有效期
            cleanD.setGscdValidDate(item.getGscdValidDate());
            // 清斗数量
            cleanD.setGscdQty(item.getGscdQty());
            cleanDs.add(cleanD);
        }));
        cleandrawerdService.saveBatch(cleanDs);
        return ResultUtil.success();
    }

    @Override
    public ClSystemPara getCheckShop() {
        TokenUser user = commonService.getLoginInfo();
        ClSystemPara param = clSystemParaMapper.selectByPrimaryKey(user.getClient(), "CHECK_SHOP");
        if (ObjectUtils.isEmpty(param)) {
            ClSystemPara paramIn = new ClSystemPara();
            paramIn.setClient(user.getClient());
            paramIn.setGcspId("CHECK_SHOP");
            paramIn.setGcspName("清斗、装斗控制维护功能");
            paramIn.setGcspParaRemark("para1为参数值 0：不开启 1：开启");
            paramIn.setGcspPara1("0");
            clSystemParaMapper.insertClSystemPara(paramIn);
            return paramIn;
        }
        return param;
    }
}
