package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.service.IFranchiseeGrpService;
import com.gov.operate.service.IFranchiseeService;
import com.gov.operate.service.IProductBusinessService;
import com.gov.operate.service.ISdYxmclassService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@RestController
@RequestMapping("gsm")
public class FranchiseeGrpController {

    @Resource
    private IFranchiseeService franchiseeService;
    @Resource
    private IFranchiseeGrpService franchiseeGrpService;
    @Resource
    private ISdYxmclassService sdYxmclassService;
    @Resource
    private IProductBusinessService productBusinessService;

    @Log("加盟商查询")
    @ApiOperation(value = "加盟商查询")
    @PostMapping("getClientList")
    public Result getClientList(@Valid @RequestBody GetClientListVO vo) {
        return ResultUtil.success(franchiseeService.getClientList(vo));
    }

    @Log("加盟商组获取")
    @ApiOperation(value = "加盟商组获取")
    @GetMapping("getClientGrpList")
    public Result getClientGrpList() {
        return ResultUtil.success(franchiseeGrpService.getClientGrpList());
    }

    @Log("商品组列表")
    @ApiOperation(value = "商品组列表")
    @GetMapping("getProGroupList")
    public Result getProGroupList() {
        return ResultUtil.success(sdYxmclassService.getProGroupList());
    }

    @Log("商品组列表分页")
    @ApiOperation(value = "商品组列表分页")
    @PostMapping("getProGroupListPage")
    public Result getProGroupListPage(@RequestBody @Valid GetProGroupListPageVO vo) {
        return ResultUtil.success(sdYxmclassService.getProGroupListPage(vo));
    }

    @Log("商品组删除")
    @ApiOperation(value = "商品组删除")
    @PostMapping("deleteProGroup")
    public Result deleteProGroup(@RequestBody DeleteProGroupVO vo) {
        sdYxmclassService.deleteProGroup(vo);
        return ResultUtil.success();
    }

    @Log("商品组保存")
    @ApiOperation(value = "商品组保存")
    @PostMapping("saveProGroup")
    public Result saveProGroup(@RequestBody @Valid SaveProGroupVO vo) {
        sdYxmclassService.saveProGroup(vo);
        return ResultUtil.success();
    }

    @Log("商品组所含商品列表")
    @ApiOperation(value = "商品组所含商品列表")
    @GetMapping("getProListOfGroup")
    public Result getProListOfGroup(@RequestParam("gsyGroup") String gsyGroup, @RequestParam("gsyStore") String gsyStore) {
        return ResultUtil.success(sdYxmclassService.getProListOfGroup(gsyGroup, gsyStore));
    }

    @Log("商品组添加查询列表")
    @ApiOperation(value = "商品组添加查询列表")
    @PostMapping("getProListForSaveGroup")
    public Result getProListForSaveGroup(@RequestBody @Valid GetProListForSaveGroupVO vo) {
        return ResultUtil.success(productBusinessService.getProListForSaveGroup(vo));
    }

}

