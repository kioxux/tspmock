package com.gov.operate.service;

import com.gov.operate.entity.CustomerBusiness;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 客户主数据业务信息表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-06-10
 */
public interface ICustomerBusinessService extends SuperService<CustomerBusiness> {

}
