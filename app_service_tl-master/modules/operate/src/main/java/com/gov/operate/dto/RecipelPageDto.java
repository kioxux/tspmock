package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.PrintWriter;

/**
 * @Auther: tzh
 * @Date: 2021/11/29 10:06
 * @Description: RecipelDto
 * @Version 1.0.0
 */
@Data
public class RecipelPageDto {
    @NotNull
    private Integer pageNum;
    @NotNull
    private Integer pageSize;
}
