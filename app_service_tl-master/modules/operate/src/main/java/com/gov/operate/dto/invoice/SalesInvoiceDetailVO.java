package com.gov.operate.dto.invoice;

import com.gov.operate.entity.SalesAmount;
import com.gov.operate.entity.SalesBill;
import lombok.Data;

import java.util.List;

@Data
public class SalesInvoiceDetailVO {

    private List<SalesInvoiceDetailItemDTO> detailList;

    private List<SalesAmount> amountList;

    private List<String> billList;

    private String gsiOrderId;

}
