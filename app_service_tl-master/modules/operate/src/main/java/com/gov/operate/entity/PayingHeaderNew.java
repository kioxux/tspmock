package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 * 付款订单主表（新）
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYING_HEADER_NEW")
@ApiModel(value="PayingHeaderNew对象", description="付款订单主表（新）")
public class PayingHeaderNew extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款订单号")
    @TableField("FICO_ID")
    private String ficoId;

    @ApiModelProperty(value = "本次付款总计")
    @TableField("FICO_PAYMENT_AMOUNT_TOTAL")
    private BigDecimal ficoPaymentAmountTotal;

    @ApiModelProperty(value = "下单时间")
    @TableField("FICO_ORDER_DATE")
    private String ficoOrderDate;

    @ApiModelProperty(value = "操作者")
    @TableField("FICO_OPERATOR")
    private String ficoOperator;

    @ApiModelProperty(value = "发票状态")
    @TableField("FICO_INVOICE_STATUS")
    private String ficoInvoiceStatus;

    @ApiModelProperty(value = "付款状态")
    @TableField("FICO_PAYMENT_STATUS")
    private String ficoPaymentStatus;

    @ApiModelProperty(value = "流水号")
    @TableField("FICO_SERIAL_NUMBER")
    private String ficoSerialNumber;

    @ApiModelProperty(value = "是否已发")
    @TableField("SP_SEND")
    private Integer spSend;

    @ApiModelProperty(value = "付款时间")
    @TableField("FICO_PAYING_DATE")
    private String ficoPayingDate;

    @ApiModelProperty(value = "支付结果")
    @TableField("FICO_PAY_RESULT")
    private String ficoPayResult;

    @ApiModelProperty(value = "支付请求参数")
    @TableField("FICO_REQUEST_RESULT")
    private String ficoRequestResult;

    @ApiModelProperty(value = "支付方式")
    @TableField("FICO_TYPE")
    private Integer ficoType;

    @ApiModelProperty(value = "发票类型")
    @TableField("FICO_INVOICE_TYPE")
    private String ficoInvoiceType;

    @ApiModelProperty(value = "订单类型")
    @TableField("FICO_ORDER_TYPE")
    private Integer ficoOrderType;

    @ApiModelProperty(value = "本次优惠金额")
    @TableField("FICO_PAYMENT_DISCOUNT_AMOUNT")
    private BigDecimal ficoPaymentDiscountAmount;

    @ApiModelProperty(value = "本次应付金额")
    @TableField("FICO_PAYMENT_HANDLE_AMOUNT")
    private BigDecimal ficoPaymentHandleAmount;

    @ApiModelProperty(value = "本次余额付款")
    @TableField("FICO_PAYMENT_BALANCE_AMOUNT")
    private BigDecimal ficoPaymentBalanceAmount;

    @ApiModelProperty(value = "合同编号")
    @TableField("FICO_CONTRACT_NUMBE")
    private String ficoContractNumbe;

    @ApiModelProperty(value = "合同付款序号")
    @TableField("FICO_CONTRACT_INDEX")
    private Integer ficoContractIndex;

    @ApiModelProperty(value = "当前付款序号")
    @TableField("FICO_CURRENT_INDEX")
    private Integer ficoCurrentIndex;

    @ApiModelProperty(value = "类型")
    @TableField("FICO_CLASS")
    private Integer ficoClass;

    @ApiModelProperty(value = "用户收费维护主表ID")
    @TableField("SP_ID")
    private Integer spId;

    @ApiModelProperty(value = "余额变动记录ID")
    @TableField("REMIND_RECORD_ID")
    private Integer remindRecordId;
}
