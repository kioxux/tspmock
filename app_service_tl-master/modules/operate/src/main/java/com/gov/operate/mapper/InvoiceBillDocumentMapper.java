package com.gov.operate.mapper;

import com.gov.operate.entity.InvoiceBillDocument;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
public interface InvoiceBillDocumentMapper extends BaseMapper<InvoiceBillDocument> {

}
