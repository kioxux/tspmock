package com.gov.operate.dto.ssp;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SspLogPageDTO {

    @NotNull(message = "分页不能为空")
    private Integer pageNum;

    @NotNull(message = "分页不能为空")
    private Integer pageSize;

    private String startDate;

    private String endDate;

    private String keyword;
}
