package com.gov.operate.dto.customer.order;

import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021-06-23 17:55
 */

@Data
public class GetCustomerOrderAttListVO {

    /**
     * 工单主键
     */
    private Long gwoId;
}
