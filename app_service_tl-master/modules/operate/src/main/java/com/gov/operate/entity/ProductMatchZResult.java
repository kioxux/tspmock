package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_MATCH_Z_RESULT")
@ApiModel(value="ProductMatchZResult对象", description="")
public class ProductMatchZResult extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商id")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "匹配编号")
    @TableField("MATCH_CODE")
    private String matchCode;

    @ApiModelProperty(value = "门店编码")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "商品核验人手机号")
    @TableField("CHECK_TOUCH_TEL")
    private String checkTouchTel;

    @ApiModelProperty(value = "商品核验总数")
    @TableField("TOTAL_LINE")
    private Integer totalLine;

    @ApiModelProperty(value = "匹配商品数量")
    @TableField("MATCH_LINE")
    private Integer matchLine;

    @ApiModelProperty(value = "校验前完整度")
    @TableField("OLD_MATCH_SCHEDULE")
    private String oldMatchSchedule;

    @TableField(exist = false)
    private String nowMatchSchedule;

    public String getNowMatchSchedule() {
        String res = "";
        if(totalLine!=null && totalLine!=0 && matchLine!=null && matchLine!=0){
            res = new BigDecimal(matchLine).divide(new BigDecimal(totalLine),2, RoundingMode.HALF_UP).setScale(2).toPlainString();
        }
        return res;
    }

    @ApiModelProperty(value = "创建时间")
    @TableField("CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "是否核验标识 1是0否")
    @TableField("CHECK_FLAG")
    private String checkFlag;

    @ApiModelProperty(value = "是否门店匹配 0 否 1 是(1时门店编码必填)")
    @TableField("MATCH_TYPE")
    private String matchType;

    @TableField(exist = false)
    private boolean clientFlag;//true为加盟商 false为门店

}
