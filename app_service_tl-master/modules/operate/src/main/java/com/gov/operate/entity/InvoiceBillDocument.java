package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_INVOICE_BILL_DOCUMENT")
@ApiModel(value="InvoiceBillDocument对象", description="")
public class InvoiceBillDocument extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GIBD_NUM")
    private String gibdNum;

    @ApiModelProperty(value = "业务类型")
    @TableField("GIBD_BUSINESS_TYPE")
    private String gibdBusinessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("GIBD_MAT_DN_ID")
    private String gibdMatDnId;

    @ApiModelProperty(value = "地点")
    @TableField("GIBD_SITE")
    private String gibdSite;

    @ApiModelProperty(value = "供应商")
    @TableField("GIBD_SUP_CODE")
    private String gibdSupCode;

    @ApiModelProperty(value = "全部登记")
    @TableField("GIBD_END")
    private Integer gibdEnd;

    @ApiModelProperty(value = "创建人")
    @TableField("GIBD_CREATE_USER")
    private String gibdCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GIBD_CREATE_DATE")
    private String gibdCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GIBD_CREATE_TIME")
    private String gibdCreateTime;

    @ApiModelProperty(value = "对账金额")
    @TableField("GIBD_BILL_AMOUNT")
    private BigDecimal gibdBillAmount;

}
