package com.gov.operate.dto.monthPushMoney;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class HomePushMoneyVo {

    private String client;

    /**
     * 数据状态 D:本期 S:上周
     */
    private String gssdType;


    @ApiModelProperty(value = "日期")
    private String gssdDate;

    @ApiModelProperty(value = "门店编码")
    private String stoCode ;

    @ApiModelProperty(value = "营业员编码")
    private String salesclerk ;

    @ApiModelProperty(value = "营业额")
    private String gssdAmtAvg = "0";

    @ApiModelProperty(value = "交易次数")
    private String gssdTrans = "0";

    @ApiModelProperty(value = "客单价")
    private String gssdAprice = "0";

    @ApiModelProperty(value = "提成合计")
    private BigDecimal deductionWage;

    @ApiModelProperty(value = "销售提成")
    private BigDecimal  deductionWageSales;

    @ApiModelProperty(value = "单品提成")
    private BigDecimal  deductionWagePro;
}
