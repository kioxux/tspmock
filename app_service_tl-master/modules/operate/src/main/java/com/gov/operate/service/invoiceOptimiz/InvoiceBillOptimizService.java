package com.gov.operate.service.invoiceOptimiz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDto;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.22
 */
public interface InvoiceBillOptimizService {

    /**
     * 发票对账
     * @param documentBillDto
     */
    void invoiceBill(DocumentBillDto documentBillDto);

    /**
     * 单据集合
     * @param vo
     * @return
     */
    IPage<SelectWarehousingDTO> selectDocumentList(SelectWarehousingVO vo);

    /**
     * 发票集合
     * @param pageNum
     * @param pageSize
     * @param siteCode
     * @param supCode
     * @param invoiceNum
     * @param invoiceDateStart
     * @param invoiceDateEnd
     * @return
     */
    Result selectInvoiceList(Integer pageNum, Integer pageSize, String siteCode, String supCode, String invoiceNum, String invoiceDateStart, String invoiceDateEnd);
}