package com.gov.operate.controller;


import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.zz.ZzInData;
import com.gov.operate.service.IZzDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-12-23
 */
@RestController
@RequestMapping("zz")
public class ZzDataController {

    @Autowired
    private IZzDataService zzDataService;

    @PostMapping({"getZzooList"})
    public Result getZzooList() {
        return ResultUtil.success(this.zzDataService.getZzooList());
    }

    @PostMapping({"getZzList"})
    public Result getZzList(@RequestBody ZzInData inData) {
        return ResultUtil.success(this.zzDataService.getZzList(inData));
    }


}

