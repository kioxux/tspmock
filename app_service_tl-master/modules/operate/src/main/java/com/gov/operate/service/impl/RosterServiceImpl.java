package com.gov.operate.service.impl;

import com.gov.operate.entity.Roster;
import com.gov.operate.mapper.RosterMapper;
import com.gov.operate.service.IRosterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
@Service
public class RosterServiceImpl extends ServiceImpl<RosterMapper, Roster> implements IRosterService {

}
