package com.gov.operate.service.impl;

import com.gov.operate.service.request.StoreSalesStatisticsRequest;
import com.gov.operate.service.response.StoreSalesStatisticsRes;

public interface StoreSalesStatisticsQueryByDataTypeStrategy {
    public StoreSalesStatisticsRes handleData(StoreSalesStatisticsRequest inData);
}
