package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.FeedbackDTO;
import com.gov.operate.dto.GetFeedBackDTO;
import com.gov.operate.dto.GetFeedBackListVO;
import com.gov.operate.entity.Feedback;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
public interface IFeedbackService extends SuperService<Feedback> {

    /**
     * 建议反馈列表
     */
    IPage<FeedbackDTO> getFeedBackList(GetFeedBackListVO vo);

    /**
     * 建议反馈详情
     */
    GetFeedBackDTO getFeedBack(String id);

    /**
     * 建议反馈详情
     */
    void removeFeedBack(String id);
}
