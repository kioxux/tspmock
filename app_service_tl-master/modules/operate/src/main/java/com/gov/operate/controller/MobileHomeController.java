package com.gov.operate.controller;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.HomeChartRequestDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyVo;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IMobileHomeService;
import com.gov.operate.service.IMobileHomeV2Service;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("mobile/home")
public class MobileHomeController {

    @Resource
    private IMobileHomeService mobileHomeService;

    @Resource
    private IMobileHomeV2Service mobileHomeV2Service;

    @Log("首页销售数据")
    @PostMapping("getHomeSalesList")
    @ApiOperation(value = "首页销售数据")
    public Result getHomeSalesList(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeService.getHomeSalesList(homeChartRequestDTO);
    }

    @Log("首页销售数据")
    @PostMapping("getHomeSalesListNew")
    @ApiOperation(value = "首页销售数据")
    public Result getHomeSalesListNew(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeService.getHomeSalesListNew(homeChartRequestDTO);
    }

    @Log("首页报表门店")
    @PostMapping("getStoListByUserAuth")
    @ApiOperation(value = "报表门店")
    public Result getStoListByUserAuth() {
        return mobileHomeService.getStoListByUserAuth();
    }

    @Log("首页报表门店")
    @PostMapping("getStoList")
    @ApiOperation(value = "报表门店")
    public Result getStoList() {
        return mobileHomeService.getStoList();
    }


    @PostMapping("getHomeStockList")
    @ApiOperation(value = "首页库存数据")
    public Result getHomeStockList(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeService.getHomeStockList(homeChartRequestDTO);
    }

    @PostMapping("getPaymentMethodList")
    @ApiOperation(value = "收银方式汇总")
    public Result getPaymentMethodList(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeService.getPaymentMethodList(homeChartRequestDTO);
    }

    @PostMapping("getMonthPushMoneyList")
    @ApiOperation(value = "月度销售提成首页", response = HomePushMoneyVo.class)
    public Result getMonthPushMoneyList(@RequestBody HomePushMoneyDTO homePushMoneyDTO) {
        return mobileHomeService.getMonthPushMoneyList(homePushMoneyDTO);
    }

    @PostMapping("getMonthPushMoneyChart")
    @ApiOperation(value = "月度销售提成图表", response = HomePushMoneyVo.class)
    public Result getMonthPushMoneyChart(@RequestBody HomePushMoneyDTO homePushMoneyDTO) {
        return mobileHomeService.getMonthPushMoneyChart(homePushMoneyDTO);
    }

    @PostMapping("loginAppSaleTotal")
    @ApiOperation(value = "APP首页调整汇总")
    public Result loginAppSaleTotal(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeService.loginAppSaleTotal(homeChartRequestDTO);
    }

    @Log("证照预警门店列表")
    @PostMapping("getZzOrgListByUserAuth")
    @ApiOperation(value = "证照预警门店列表")
    public Result getZzOrgListByUserAuth() {
        return mobileHomeV2Service.getZzOrgListByUserAuth();
    }
}
