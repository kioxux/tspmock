package com.gov.operate.service;

import com.gov.operate.entity.SupplierBusiness;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface ISupplierBusinessService extends SuperService<SupplierBusiness> {

}
