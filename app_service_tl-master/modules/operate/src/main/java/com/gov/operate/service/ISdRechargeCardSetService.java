package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.AddRechargeSetVO;
import com.gov.operate.entity.SdRechargeCardSet;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface ISdRechargeCardSetService extends SuperService<SdRechargeCardSet> {
    /**
     * 储值卡设置
     * 新增或编辑
     */
    Integer addOrUpdateCard(AddRechargeSetVO vo);

    SdRechargeCardSet getClientCard();

    List<AddRechargeSetVO> getProportionData();

    /**
     * 储值卡设置门店
     * @return
     */
    Result getStoreList();
}
