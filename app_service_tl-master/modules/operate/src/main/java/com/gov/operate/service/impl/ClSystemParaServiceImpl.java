package com.gov.operate.service.impl;

import com.gov.operate.entity.ClSystemPara;
import com.gov.operate.mapper.ClSystemParaMapper;
import com.gov.operate.service.IClSystemParaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司参数表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-06-02
 */
@Service
public class ClSystemParaServiceImpl extends ServiceImpl<ClSystemParaMapper, ClSystemPara> implements IClSystemParaService {

}
