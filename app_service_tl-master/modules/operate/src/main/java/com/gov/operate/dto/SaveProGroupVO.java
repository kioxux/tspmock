package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode
public class SaveProGroupVO {

    /**
     * 商品组ID
     */
    private String gsyGroup;

    @NotBlank(message = "商品组组名不能为空")
    private String gsyGroupname;

    @Valid
    @NotEmpty(message = "商品集合不能为空")
    private List<SaveProGroupProductVO> productList;
}
