package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_VARIABLE_SET")
@ApiModel(value="SdPromVariableSet对象", description="")
public class SdPromVariableSet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "促销方式")
    @TableId("GSPVS_MODE")
    private String gspvsMode;

    @ApiModelProperty(value = "促销方式描述")
    @TableField("GSPVS_MODE_NAME")
    private String gspvsModeName;

    @ApiModelProperty(value = "促销类型")
    @TableField("GSPVS_TYPE")
    private String gspvsType;

    @ApiModelProperty(value = "促销类型描述")
    @TableField("GSPVS_TYPE_NAME")
    private String gspvsTypeName;

    @ApiModelProperty(value = "促销表名")
    @TableField("GSPVS_TABLE")
    private String gspvsTable;

    @ApiModelProperty(value = "变量字段名")
    @TableField("GSPVS_COLUMNS")
    private String gspvsColumns;

    @ApiModelProperty(value = "变量字段描述")
    @TableField("GSPVS_COLUMNS_NAME")
    private String gspvsColumnsName;

    @ApiModelProperty(value = "字段类型")
    @TableField("GSPVS_COLUMNS_TYPE")
    private String gspvsColumnsType;

    @ApiModelProperty(value = "字段长度")
    @TableField("GSPVS_COLUMNS_LENGTH")
    private String gspvsColumnsLength;

    @ApiModelProperty(value = "是否可编辑")
    @TableField("GSPVS_FALG")
    private String gspvsFalg;


}
