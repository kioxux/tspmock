package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_ASSO_SET")
@ApiModel(value="SdPromAssoSet对象", description="")
public class SdPromAssoSet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPAS_VOUCHER_ID")
    private String gspasVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPAS_SERIAL")
    private String gspasSerial;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPAS_SERIES_ID")
    private String gspasSeriesId;

    @ApiModelProperty(value = "组合金额")
    @TableField("GSPAS_AMT")
    private BigDecimal gspasAmt;

    @ApiModelProperty(value = "组合折扣")
    @TableField("GSPAS_REBATE")
    private String gspasRebate;

    @ApiModelProperty(value = "赠送数量")
    @TableField("GSPAS_QTY")
    private String gspasQty;


}
