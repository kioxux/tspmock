package com.gov.operate.service.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel("消费订单明细列表")
@Data
public class OrderInfoDetailDTO {

    @ApiModelProperty("商品编码")
    private String gssdProId;

    @ApiModelProperty("商品名称")
    private String gssdProName;

    @ApiModelProperty("商品规格")
    private String proSpecs;

    @ApiModelProperty("商品数量+单位")
    private String proQtyAndUnit;

    @ApiModelProperty("汇总应收金额")
    private BigDecimal gssdAmt;

}
