package com.gov.operate.controller.invoiceOptimiz;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.dto.SelectWarehousingDetailsDTO;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDto;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.invoiceOptimiz.DocumentBillOptimizService;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.16
 */
@Validated
@RestController
@RequestMapping("documentBill")
public class DocumentBillController {

    @Resource
    private DocumentBillOptimizService documentBillOptimizService;

    @Log("单据集合")
    @ApiOperation(value = "单据集合")
    @PostMapping("selectDocumentList")
    public Result selectDocumentList(@RequestBody SelectWarehousingVO vo) {
        return ResultUtil.success(documentBillOptimizService.selectDocumentList(vo));
    }

    @Log("单据合计")
    @ApiOperation(value = "单据合计")
    @PostMapping("selectDocumentTotal")
    public Result selectDocumentTotal(@RequestBody SelectWarehousingVO vo) {
        return documentBillOptimizService.selectDocumentTotal(vo);
    }

    @Log("单据对账")
    @ApiOperation(value = "单据对账")
    @PostMapping("documentBill")
    public Result documentBill(@RequestBody DocumentBillDto documentBillDto) {
        documentBillOptimizService.documentBill(documentBillDto);
        return ResultUtil.success();
    }

    @Log("已据已登记发票")
    @ApiOperation(value = "已据已登记发票")
    @PostMapping("selectInvoiceListByDoc")
    public Result selectInvoiceListByDoc(@RequestJson(value = "type", name = "单据类型") String type,
                                         @RequestJson(value = "matDnId", name = "单据号") String matDnId
    ) {
        return documentBillOptimizService.selectInvoiceListByDoc(type, matDnId);
    }

    @Log("可登记发票")
    @ApiOperation(value = "可登记发票")
    @PostMapping("selectInvoiceList")
    public Result selectInvoiceList(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                    @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                    @RequestJson(value = "siteCode", required = false) String siteCode,
                                    @RequestJson(value = "supCode", required = false) String supCode,
                                    @RequestJson(value = "invoiceNum", required = false) String invoiceNum,
                                    @RequestJson(value = "invoiceDateStart", required = false) String invoiceDateStart,
                                    @RequestJson(value = "invoiceDateEnd", required = false) String invoiceDateEnd
    ) {
        return documentBillOptimizService.selectInvoiceList(pageNum, pageSize, siteCode, supCode, invoiceNum, invoiceDateStart, invoiceDateEnd);
    }

    @Log("发票绑定")
    @ApiOperation(value = "发票绑定")
    @PostMapping("bindInvoice")
    public Result bindInvoice(@RequestBody SelectWarehousingDTO selectWarehousingDTO) {
        documentBillOptimizService.bindInvoice(selectWarehousingDTO);
        return ResultUtil.success();
    }

    @Log("明细导出")
    @ApiOperation(value = "明细导出")
    @PostMapping("exportDetailList")
    public Result exportDetailList(@RequestBody List<SelectWarehousingDetailsDTO> detailList) throws IOException {
        return documentBillOptimizService.exportDetailList(detailList);
    }
}
