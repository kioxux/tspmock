package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.GetMemberListDTO;
import com.gov.operate.dto.GetMemberListVO;
import com.gov.operate.entity.SdMemberBasic;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-22
 */
public interface ISdMemberBasicService extends SuperService<SdMemberBasic> {

    /**
     * 精准查询
     */
    List<GetMemberListDTO> getMemberList(GetMemberListVO vo);

    /**
     * 精准查询导出
     */
    Result exportMemberList(GetMemberListVO vo);
}
