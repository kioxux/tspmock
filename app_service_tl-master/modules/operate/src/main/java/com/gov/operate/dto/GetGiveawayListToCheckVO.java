package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode
public class GetGiveawayListToCheckVO extends Pageable {

    private Integer promId;

    /**
     * 营销活动加盟商分配表主键
     */
    private Integer marketingId;

    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品名
     */
    private String proName;

    private Integer hasChecked;
    /**
     * 商品编码集合
     */
    private List<String> prdList;

    private String client;
}
