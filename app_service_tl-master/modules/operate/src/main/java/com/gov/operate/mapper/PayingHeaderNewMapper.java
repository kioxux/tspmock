package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.PayingHeaderNew;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 付款订单主表（新） Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface PayingHeaderNewMapper extends BaseMapper<PayingHeaderNew> {
    List<Integer> getPayTimes(@Param("sphId") Integer sphId,
                              @Param("clientId") String clientId,
                              @Param("spId") String spId);
}
