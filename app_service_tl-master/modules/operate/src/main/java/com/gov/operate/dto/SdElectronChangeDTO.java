package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 发送老会员电子券主题
 * @author: yzf
 * @create: 2022-01-17 13:12
 */
@Data
public class SdElectronChangeDTO {

    /**
     * 会员卡号
     */
    private List<String> numberIds;

    /**
     * 主题单号
     */
    private List<GsetsIdObj> gsetsIdObjList;

    @Data
    public class GsetsIdObj {
        private String gsetsId;
        /**
         * 送券次数
         */
        private Integer qty;
    }
}
