package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdYxmclass;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdYxmclassMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdYxmclassService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
@Service
public class SdYxmclassServiceImpl extends ServiceImpl<SdYxmclassMapper, SdYxmclass> implements ISdYxmclassService {

    @Resource
    private SdYxmclassMapper sdYxmclassMapper;
    @Resource
    private CommonService commonService;

    @Override
    public Result selectGroupAndProductList(String gsmStore) {
        if (StringUtils.isEmpty(gsmStore)) {
            throw new CustomResultException("缺少门店编号");
        }
        List<ProductGroupVo> productGroupVoList = new ArrayList<>();
        // 获取当前人加盟商
        TokenUser user = commonService.getLoginInfo();

        QueryWrapper<SdYxmclass> queryWrapper = new QueryWrapper<SdYxmclass>()
                .eq("CLIENT", user.getClient())
                .eq("GSY_STORE", gsmStore);
        List<SdYxmclass> classList = baseMapper.selectList(queryWrapper);
        if (classList != null && classList.size() > 0) {
            Map<String, List<SdYxmclass>> mapList = classList.stream()
                    .collect(Collectors.groupingBy(SdYxmclass::getGsyGroup));
            ProductGroupVo productGroupVo = null;
            for (Map.Entry<String, List<SdYxmclass>> entry : mapList.entrySet()) {
                productGroupVo = new ProductGroupVo();
                productGroupVo.setGsyGroup(entry.getKey());
                productGroupVo.setGsyGroupname(entry.getValue().get(0).getGsyGroupname());
                productGroupVo.setProductList(entry.getValue());
                productGroupVoList.add(productGroupVo);
            }
        }
        return ResultUtil.success(productGroupVoList);
    }

    /**
     * 商品组列表
     */
    @Override
    public List<SdYxmclass> getProGroupList() {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<SdYxmclass> query = new QueryWrapper<SdYxmclass>()
                .select("CLIENT", "GSY_GROUP", "GSY_GROUPNAME")
                // 加盟商
                .eq("CLIENT", user.getClient())
                .groupBy("CLIENT", "GSY_GROUP", "GSY_GROUPNAME");
        return this.list(query);
    }

    /**
     * 商品组列表分页
     */
    @Override
    public IPage<GetProGroupListPageDTO> getProGroupListPage(GetProGroupListPageVO vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<GetProGroupListPageDTO> page = new Page<GetProGroupListPageDTO>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<GetProGroupListPageDTO> query = new QueryWrapper<GetProGroupListPageDTO>()
                // 加盟商
                .eq("yxm.CLIENT", user.getClient())
                // 门店编码
                .eq(StringUtils.isNotEmpty(vo.getGsyStore()), "yxm.GSY_STORE", vo.getGsyStore())
                // 商品组编码，模糊查询
                .like(StringUtils.isNotEmpty(vo.getGsyGroup()), "yxm.GSY_GROUP", vo.getGsyGroup())
                // 商品组名， 模糊查询
                .like(StringUtils.isNotEmpty(vo.getGsyGroupname()), "yxm.GSY_GROUPNAME", vo.getGsyGroupname());
        return sdYxmclassMapper.getProGroupListPage(page, query);
    }

    /**
     * 商品组删除
     */
    @Override
    public void deleteProGroup(DeleteProGroupVO vo) {
        QueryWrapper<SdYxmclass> query = new QueryWrapper<SdYxmclass>()
                .eq("GSY_GROUP", vo.getGsyGroup());
        this.remove(query);
    }

    /**
     * 商品组保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveProGroup(SaveProGroupVO vo) {

        if (StringUtils.isNotEmpty(vo.getGsyGroup())) {
            // 更新，删除旧的数据
            this.deleteProGroup(new DeleteProGroupVO(vo.getGsyGroup()));
        } else {
            // 新增，设置新的商品组id
            vo.setGsyGroup(getMaxGroup());
        }
        // 新增
        TokenUser user = commonService.getLoginInfo();
        List<SdYxmclass> list = new ArrayList<>();
        for (SaveProGroupProductVO proVO : vo.getProductList()) {
            SdYxmclass sdYxmclass = new SdYxmclass();
            list.add(sdYxmclass);
            BeanUtils.copyProperties(proVO, sdYxmclass);
            BeanUtils.copyProperties(vo, sdYxmclass);
            // 加盟商
            sdYxmclass.setClient(user.getClient());
            // 商品编码
            sdYxmclass.setGsyCode(proVO.getProSelfCode());
        }
        this.saveBatch(list);

    }

    /**
     * 商品组所含商品列表
     */
    @Override
    public List<SdYxmclass> getProListOfGroup(String gsyGroup, String gsyStore) {
        QueryWrapper<SdYxmclass> query = new QueryWrapper<SdYxmclass>()
                .eq("GSY_GROUP", gsyGroup)
                .eq("GSY_STORE", gsyStore);
        return this.list(query);
    }

    /**
     * 获取数字编码最大值
     */
    private String getMaxGroup() {
        String maxGroup = sdYxmclassMapper.getMaxGroup();
        if (StringUtils.isEmpty(maxGroup)) {
            return "100001";
        }
        Integer maxNumGroup = Integer.valueOf(maxGroup);
        return String.valueOf(maxNumGroup + 1);
    }

}
