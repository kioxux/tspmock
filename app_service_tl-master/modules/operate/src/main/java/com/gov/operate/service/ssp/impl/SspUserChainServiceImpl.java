package com.gov.operate.service.ssp.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.SspUserChain;
import com.gov.operate.mapper.SspUserChainMapper;
import com.gov.operate.service.ssp.ISspUserChainService;
import org.springframework.stereotype.Service;

@Service
public class SspUserChainServiceImpl extends ServiceImpl<SspUserChainMapper, SspUserChain> implements ISspUserChainService {


}
