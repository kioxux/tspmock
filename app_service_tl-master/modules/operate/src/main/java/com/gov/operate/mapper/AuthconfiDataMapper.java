package com.gov.operate.mapper;

import com.gov.operate.entity.AuthconfiData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 权限分配表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
public interface AuthconfiDataMapper extends BaseMapper<AuthconfiData> {
    int deleteOne(@Param("client") String client, @Param("groupId") String groupId, @Param("site") String site, @Param("user") String user);
    int insertIgnoreList(@Param("authconfiDataList") List<AuthconfiData> authconfiDataList);

    List<String> selectUserByGourp(@Param("client") String client);
}
