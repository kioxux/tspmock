package com.gov.operate.dto;

import com.gov.operate.entity.SupplierPrepaymentDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class SupplierPaymentDetailVO extends SupplierPrepaymentDetail {

    /**
     * 业务类型
     */
    private String type;
    /**
     * 业务类型名
     */
    private String typeName;
    /**
     * 单据号
     */
    private String matDnId;
    /**
     * 单据日期
     */
    private String xgrq;
    /**
     * 单据去税金额
     */
    private BigDecimal excludingTaxAmount;
    /**
     * 单据税额
     */
    private BigDecimal taxAmount;
    /**
     * 单据总金额
     */
    private BigDecimal totalAmount;

    /**
     * 供应商编码
     */
    private String supSelfCode;
    /**
     * 供应商名
     */
    private String supSelfName;

    /**
     * 门店名
     */
    private String stoName;

    /**
     * 地点
     */
    private String site;
}
