package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetCommonSearchListVO {

    /**
     * 类型：1:精准查询菜单入口
     */
    private String type;
    /**
     * 查询ID
     */
    private String gsmsId;
    /**
     * 门店编码(当前营销/历史营销 入口)
     */
    private String stoCode;

    @NotNull(message = "查询类型不能为空 1:短信，2：电话")
    private Integer gsmsType;
}
