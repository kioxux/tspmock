package com.gov.operate.dto;

import com.gov.operate.entity.SdPromHead;
import lombok.Data;

/**
 * @description: 促销商品列表
 * @author: yzf
 * @create: 2022-01-21 16:28
 */

@Data
public class PromGoodsListVO extends SdPromHead {
    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 描述
     */
    private String proDepict;

    /**
     * 规格
     */
    private String proSpecs;
}
