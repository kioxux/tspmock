package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.dto.invoiceOptimiz.PaymentBill;
import com.gov.operate.entity.BillPaymentApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-23
 */
public interface BillPaymentApplyMapper extends BaseMapper<BillPaymentApply> {

//    IPage<PaymentBill> selectBillList(Page page, @Param("vo") SelectWarehousingVO vo);
}
