package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @TableName GAIA_SSP_MENU_COMPADM
 */
@TableName(value = "GAIA_SSP_MENU_CHAIN")
@Data
public class SspMenuChain extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户菜单ID
     */
    private Long menuId;

    /**
     * 菜单code
     */
    private String menuCode;

    /**
     * 用户主体ID
     */
    private Long userChainId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 连锁总部
     */
    private String chainHead;

    /**
     * 主体名称
     */
    private String chainName;

    /**
     * 主体类型
     */
    private Integer chainType;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

}