package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserRoleVO {

    @NotNull(message = "角色id不能为空")
    private Integer gaurRoleId;

    private List<String> gaurUserIdList;

}
