package com.gov.operate.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@Data
public class SaveAsGsmVO {

    @NotBlank(message = "加盟商不能为空")
    private String client;

    @NotBlank(message = "营销活动ID不能为空")
    private String gsmMarketid;

    @NotBlank(message = "活动名称不能为空")
    private String gsmThename;

    /**
     * 门店
     */
    private String gsmStore;
}
