package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.weChat.GetCostomerMaterialListDTO;
import com.gov.operate.dto.weChat.GetCostomerMaterialListVO;
import com.gov.operate.dto.weChat.WechatTweetsHJobDTO;
import com.gov.operate.entity.WechatTweetsH;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 微信推文管理 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-01-07
 */
public interface WechatTweetsHMapper extends BaseMapper<WechatTweetsH> {

    /**
     * 客户公众号素材列表
     *
     * @param page
     * @param vo
     * @param client
     * @return
     */
    IPage<GetCostomerMaterialListDTO> getCostomerMaterialList(Page page, @Param("vo") GetCostomerMaterialListVO vo, @Param("client") String client);

    /**
     * 计划发布时间为当前时间的公众号文章
     * @param localHour
     */
    List<WechatTweetsHJobDTO> getPlanPushWechatTweetList(@Param("localHour") String localHour);

}
