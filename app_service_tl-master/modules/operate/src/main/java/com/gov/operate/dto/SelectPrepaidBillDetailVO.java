package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectPrepaidBillDetailVO extends Pageable {


    /**
     * 采购地点
     */
    private String site;
    /**
     * 单体店编码
     */
    private String stoCode;

    private Integer applicationsId;

    // @NotBlank(message = "供应商不能为空")
    private String supSelfCode;

    /**
     * 对帐单号
     */
    private String billNum;

    /**
     * 对账日期
     */
    private String billDateStart;

    /**
     * 对账日期
     */
    private String billDateEnd;

    /**
     * 供应商名
     */
    private String supSelfName;
    /**
     * 类型 CD-采购、GD-退厂
     */
    private String type;
    /**
     * 单据起始
     */
    private String billNoStart;
    /**
     * 单据结束
     */
    private String billNoEnd;
    /**
     * 单据编号
     */
    private String billNo;
    /**
     * 日期起始
     */
    private String dateStart;
    /**
     * 日期结束
     */
    private String dateEnd;

    /**
     * 加盟商 不作为入参
     */
    private String client;
    /**
     * 门店编码 入参
     */
    private String deliveryTypeStore;

    /**
     * 1:已完成,0:未完成
     */
    private String endFlg;

    /**
     * 是否已对账 0 未对账，1 已对账
     */
    private Integer status;

    /**
     * 导出参数 单据编号列表
     */
    private List<String> matDnIdList;

    /**
     * 是否已核销
     */
    private Integer isEnd;

    /**
     * 付款单号
     */
    private String paymentOrderNo;

    private String id;
    private String typeName;
    private String matDnId;
    private String matId;
    private String matYear;
    private String matLineNo;
    private String proCode;
    private String proUnit;
    private String stoName;

    private String proName;
    private BigDecimal excludingTaxAmount;
    private BigDecimal rateBat;
    /**
     * 含税金额(总金额)
     */
    private BigDecimal totalAmount;
    /**
     * 已登记金额
     */
    private BigDecimal registeredAmount;
    private BigDecimal settlementAmount;
    private BigDecimal chargeAmount;
    private BigDecimal paymentAmt;
    /**
     * 已付款金额
     */
    private BigDecimal invoiceAmountOfThisPayment;
    /**
     * 数量
     */
    private BigDecimal matQty;
    /**
     * 已结算数量
     */
    private BigDecimal settlementQty;
    /**
     * 本次数量
     */
    private BigDecimal paymentNum;

    /**
     * 已对账
     */
    private Integer selected;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 业务员姓名
     */
    private String gssName;

    /**
     * 单据日期
     */
    private String xgrq;

}
