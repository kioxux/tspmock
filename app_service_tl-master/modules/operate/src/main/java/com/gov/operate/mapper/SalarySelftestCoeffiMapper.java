package com.gov.operate.mapper;

import com.gov.operate.entity.SalarySelftestCoeffi;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
public interface SalarySelftestCoeffiMapper extends BaseMapper<SalarySelftestCoeffi> {

}
