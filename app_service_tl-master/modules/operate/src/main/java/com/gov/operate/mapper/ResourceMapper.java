package com.gov.operate.mapper;

import com.gov.operate.entity.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-17
 */
public interface ResourceMapper extends BaseMapper<Resource> {

}
