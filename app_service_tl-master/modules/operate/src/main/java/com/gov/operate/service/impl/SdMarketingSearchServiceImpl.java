package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.electron.ElectronReserved;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdElectronChangeMapper;
import com.gov.operate.mapper.SdMarketingSearchMapper;
import com.gov.operate.mapper.UserDataMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ICompadmService;
import com.gov.operate.service.ISdMarketingSearchService;
import com.gov.operate.service.IStoreDataService;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-23
 */
@Slf4j
@Service
public class SdMarketingSearchServiceImpl extends ServiceImpl<SdMarketingSearchMapper, SdMarketingSearch> implements ISdMarketingSearchService {


    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingSearchMapper sdMarketingSearchMapper;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private ICompadmService compadmService;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SdElectronChangeMapper sdElectronChangeMapper;
    @Resource
    private RedisClient redisClient;


    /**
     * 电子券号
     **/
    private static final String GSEC_ID = "GAIA_SD_ELECTRON_CHANGE_GSEC_ID";

    /**
     * 常用查询列表
     */
    @Override
    public List<SdMarketingSearch> getCommonSearchList(GetCommonSearchListVO vo) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        QueryWrapper<SdMarketingSearch> query = new QueryWrapper<SdMarketingSearch>()
                // 加盟商
                .eq(StringUtils.isNotEmpty(user.getClient()), "CLIENT", user.getClient())
                // 查询类型 1:短信，2：电话
                .eq("GSMS_TYPE", vo.getGsmsType())
                // 是否常用  0，常用查询，1,非常用查询
                .eq("GSMS_NORMAL", OperateEnum.YesOrNo.ZERO.getCode())
                // 删除标记 0-否，1-是
                .eq("GSMS_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode())
                // 排序
                .orderByAsc("GSMS_ID");

        if (!"1".equals(vo.getType())) {
            // 根据id查询 (营销设置，推荐营销，当前营销，历史营销)
            String stoCode = vo.getStoCode();
            if (StringUtils.isEmpty(stoCode)) {
                throw new CustomResultException("门店编码不能为空");
            }
            query.eq("GSMS_STORE", stoCode);
            List<SdMarketingSearch> list = this.list(query);
            this.addExtend(vo, list);
            return list;
        } else {
            // 精准查询菜单入口
            // 用户部门ID
            String depId = null;
            UserData userData = userDataMapper.selectOne(new QueryWrapper<UserData>().select("DEP_ID", "DEP_NAME").eq("CLIENT", user.getClient()).eq("USER_ID", user.getUserId()));
            if (!ObjectUtils.isEmpty(userData)) {
                depId = userData.getDepId();
            }
            // 单体门店
            StoreData storeData = storeDataService.getOne(new QueryWrapper<StoreData>().select("STO_CODE", "STO_NAME").eq("CLIENT", user.getClient()).eq("STO_CODE", depId));
            if (!ObjectUtils.isEmpty(storeData)) {
                query.eq("GSMS_STORE", storeData.getStoCode());
                List<SdMarketingSearch> list = this.list(query);
                this.addExtend(vo, list);
                return list;
            }
            // 连锁门店
            Compadm compadm = compadmService.getOne(new QueryWrapper<Compadm>().eq("CLIENT", user.getClient()).eq("COMPADM_ID", depId));
            if (!ObjectUtils.isEmpty(compadm)) {
                query.inSql("GSMS_STORE", "SELECT STO_CODE FROM GAIA_STORE_DATA WHERE STO_CHAIN_HEAD = '" + compadm.getCompadmId() + "'");
                List<SdMarketingSearch> list = this.list(query);
                this.addExtend(vo, list);
                return list;
            }
            List<SdMarketingSearch> list = this.list(query);
            this.addExtend(vo, list);
            return list;
        }

    }

    /**
     * 针对具体id 添加到常用查询列表
     */
    private void addExtend(GetCommonSearchListVO vo, List<SdMarketingSearch> list) {
        // 针对id查询
        if (StringUtils.isNotEmpty(vo.getGsmsId())) {
            SdMarketingSearch search = this.getById(vo.getGsmsId());
            // 如果集合里面不包括该元素->添加
            if (!ObjectUtils.isEmpty(search)) {
                boolean anyMatch = list.stream().anyMatch(item -> item.getGsmsId().equals(search.getGsmsId()));
                if (!anyMatch) {
                    list.add(search);
                }
            }
        }
    }

    /**
     * 常用查询保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCommonSearch(SaveCommonSearchVO searchVO) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        SdMarketingSearch search = new SdMarketingSearch();
        BeanUtils.copyProperties(searchVO, search);
        // 加盟商
        search.setClient(user.getClient());
        // 门店编码，当前用户的部门ID
        UserData userData = userDataMapper.selectOne(new QueryWrapper<UserData>().select("DEP_ID", "DEP_NAME").eq("CLIENT", user.getClient()).eq("USER_ID", user.getUserId()));
        if (!ObjectUtils.isEmpty(userData)) {
            search.setGsmsStore(userData.getDepId());
        }
        // 查询ID
        search.setGsmsId(UUIDUtil.getUUID());
        // 非删除
        search.setGsmsDeleteFlag(OperateEnum.YesOrNo.ZERO.getCode());
        this.save(search);
    }

    /**
     * 常用查询覆盖
     */
    @Override
    public void editCommonSearch(EditCommonSearchVO vo) {
        // 更新
        SdMarketingSearch searchExit = this.getById(vo.getGsmsId());
        if (ObjectUtils.isEmpty(searchExit)) {
            throw new CustomResultException(ResultEnum.E0021);
        }
        SdMarketingSearch search = new SdMarketingSearch();
        BeanUtils.copyProperties(vo, search);
        this.updateById(search);
    }

    @Override
    public Result getElectronThemeList(ElectronThemeDTO dto) {
        TokenUser user = commonService.getLoginInfo();
        List<ElectronThemeDTO> outDataList = new ArrayList<>();
        Page<ElectronThemeDTO> page = new Page<ElectronThemeDTO>(dto.getPageNum(), dto.getPageSize());
        // 电子券主题列表分页查询（老会员）
        IPage<ElectronThemeDTO> themeList = sdMarketingSearchMapper.getElectronThemeList(page, user.getClient());
        // 电子券业务基础设置表
        List<ElectronBusinessDTO> businessList = sdMarketingSearchMapper.getElectronBusinessList(user.getClient());
        List<ElectronThemeDTO> themeDTOList = themeList.getRecords();
        if (!CollectionUtils.isEmpty(themeDTOList)) {
            List<String> Ids = themeDTOList.stream().map(ElectronThemeDTO::getGsetsId).collect(Collectors.toList());
            // 电子券业务基础（业务类型--用券）
            List<ElectronBusinessDTO> voucherMapperAll = new ArrayList<>();
            if (CollUtil.isNotEmpty(businessList)) {
                for (ElectronBusinessDTO businessSet : businessList) {
                    //  0为送券，1为用券',
                    if (Ids.contains(businessSet.getGsetsId()) && "1".equals(businessSet.getGsebsType())) {
                        voucherMapperAll.add(businessSet);
                    }
                }
            }
            for (ElectronThemeDTO themeDTO : themeDTOList) {
                if (CollUtil.isNotEmpty(voucherMapperAll)) {
                    List<ElectronBusinessDTO> voucherData = voucherMapperAll.stream().collect(
                            Collectors.collectingAndThen(
                                    Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ElectronBusinessDTO::getGsetsId))), ArrayList::new)
                    );
                    ElectronBusinessDTO voucherbusinessSet = voucherData.stream().filter(t -> t.getGsetsId().equals(themeDTO.getGsetsId())).findFirst().orElse(null);
                    //用券时间
                    if (voucherbusinessSet != null) {
                        themeDTO.setVoucherStartDate(voucherbusinessSet.getGsebsBeginDate());
                        themeDTO.setVoucherEndDate(voucherbusinessSet.getGsebsEndDate());
                    }
                }
                // 电子券自动发券设置表
                List<ElectronAutoDTO> autoSetList = sdMarketingSearchMapper.getElectronAutoList(user.getClient(), themeDTO.getGsetsId());
                if (CollUtil.isNotEmpty(autoSetList)) {
                    List<ElectronAutoDTO> autoSets = autoSetList.stream().collect(
                            Collectors.collectingAndThen(
                                    Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ElectronAutoDTO::getGseasId))), ArrayList::new)
                    );
                    ElectronAutoDTO electronAutoSet = autoSets.stream().filter(t -> t.getGseasId().equals(themeDTO.getGsetsId())).findFirst().orElse(null);
                    //送券时间
                    if (electronAutoSet != null) {
                        themeDTO.setCouponStartDate(electronAutoSet.getGseasBeginDate());
                        themeDTO.setCouponEndDate(electronAutoSet.getGseasEndDate());
                    }
                }
                outDataList.add(themeDTO);
            }
        }
        themeList.setRecords(outDataList);
        return ResultUtil.success(themeList);
    }

    @Override
    public Result saveElectronChange(SdElectronChangeDTO dto) {
        if (dto != null && !CollectionUtils.isEmpty(dto.getNumberIds()) && !CollectionUtils.isEmpty(dto.getGsetsIdObjList())) {
            TokenUser user = commonService.getLoginInfo();
            Map<String, Integer> gsecIdMap = new HashMap<>();
            List<SdElectronChange> changeList = new ArrayList<>();
            // 电子券 生成
            List<String> gsetsIdLit = dto.getGsetsIdObjList().stream().map(SdElectronChangeDTO.GsetsIdObj::getGsetsId).collect(Collectors.toList());
            List<ElectronReserved> electronReservedList = sdElectronChangeMapper.selectElectronReservedList(user.getClient(), gsetsIdLit);
            for (String numberId : dto.getNumberIds()) {
                for (int i = 0; !CollectionUtils.isEmpty(electronReservedList) && i < electronReservedList.size(); i++) {
                    // 赠送电子券
                    ElectronReserved electronReserved = electronReservedList.get(i);
                    SdElectronChangeDTO.GsetsIdObj gsetsIdObj = dto.getGsetsIdObjList().stream().filter(t ->
                            t.getGsetsId().equals(electronReserved.getGseasId())
                    ).findFirst().orElse(null);
                    Integer qty = 1;
                    if (gsetsIdObj != null && gsetsIdObj.getQty() != null) {
                        qty = gsetsIdObj.getQty().intValue();
                    }
                    // 面值
                    BigDecimal gsebAmt = electronReserved.getGsebAmt();
                    // 电子券活动号
                    String gsebId = electronReserved.getGsebId();
                    // 电子券描述
                    String gsebName = electronReserved.getGsebName();
                    // 发送数量
                    int gseasQty = NumberUtils.toInt(electronReserved.getGseasQty(), 0) * qty;
                    // 电子券号
                    Integer gsecId = gsecIdMap.get(gsebId);
                    if (gsecId == null) {
                        gsecId = sdElectronChangeMapper.selectGsecId(user.getClient(), gsebId);
                    }
                    if (gsecId == null) {
                        gsecId = 0;
                    }
                    while (redisClient.exists(Constants.GAIA_SD_ELECTRON_CHANGE_GSEC_ID + user.getClient() + gsebId + gsecId)) {
                        gsecId++;
                    }
                    redisClient.set(Constants.GAIA_SD_ELECTRON_CHANGE_GSEC_ID + user.getClient() + gsebId + gsecId, String.valueOf(gsecId), 1800);
                    for (int r = 0; r < gseasQty; r++) {
                        // 电子券异动表
                        SdElectronChange sdElectronChange = new SdElectronChange();
                        // 加盟商
                        sdElectronChange.setClient(user.getClient());
                        // 会员卡号
                        sdElectronChange.setGsecMemberId(numberId);
                        // 电子券号
                        gsecId++;
                        String gsedCode = org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(gsecId), 10, "0");
                        sdElectronChange.setGsecId(gsebId + gsedCode);
                        // 电子券活动号
                        sdElectronChange.setGsebId(gsebId);
                        // 是否使用
                        sdElectronChange.setGsecStatus("N");
                        // 电子券金额
                        sdElectronChange.setGsecAmt(gsebAmt);
                        // 途径
                        sdElectronChange.setGsecFlag("1");
                        // 创建日期
                        sdElectronChange.setGsecCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                        // 电子券描述
                        sdElectronChange.setGsebName(gsebName);
                        changeList.add(sdElectronChange);
                    }
                    gsecIdMap.put(gsebId, gsecId);
                }
            }
            sdElectronChangeMapper.insertBatch(changeList);
        }
        return ResultUtil.success();
    }


    /**
     * 常用查询删除
     */
    @Override
    public void deleteCommonSearch(String gsmsId) {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user)) {
            throw new CustomResultException(ResultEnum.E0020);
        }
        QueryWrapper<SdMarketingSearch> del = new QueryWrapper<SdMarketingSearch>()
                // 加盟商
                .eq(StringUtils.isNotEmpty(user.getClient()), "CLIENT", user.getClient())
                // 查询ID
                .eq("GSMS_ID", gsmsId);
        this.remove(del);
    }

    /**
     * 获取查询id 数字编码最大值
     */
    private String getMaxIdStr(String client) {
        String maxId = sdMarketingSearchMapper.getMaxId(client);
        if (StringUtils.isEmpty(maxId)) {
            return "1001";
        }
        Integer maxIdNum = Integer.valueOf(maxId);
        return String.valueOf(maxIdNum + 1);
    }
}
