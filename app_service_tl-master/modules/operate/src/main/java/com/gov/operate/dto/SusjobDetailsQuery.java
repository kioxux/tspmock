package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */
@Data
public class SusjobDetailsQuery extends Pageable {

    @ApiModelProperty(value = "加盟商")
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "任务ID")
    @NotBlank(message = "任务ID不能为空")
    private String susJobid;

    @ApiModelProperty(value = "任务门店")
    @NotBlank(message = "任务门店不能为空")
    private String susJobbr;

}

