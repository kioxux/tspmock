package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ssp.SspLogPageDTO;
import com.gov.operate.entity.SspLog;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.gov.operate.entity.SspLog
 */
public interface SspLogMapper extends BaseMapper<SspLog> {

    IPage<SspLog> selectUserByPage(Page<SspLog> page,@Param("ew") SspLogPageDTO sspLogPageDTO);
}




