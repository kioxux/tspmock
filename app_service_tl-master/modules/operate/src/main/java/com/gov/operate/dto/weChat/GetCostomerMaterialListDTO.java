package com.gov.operate.dto.weChat;

import com.gov.operate.entity.WechatTweetsD;
import com.gov.operate.entity.WechatTweetsH;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetCostomerMaterialListDTO extends WechatTweetsH {

    /**
     * 明细
     */
    private List<WechatTweetsD> newsItem;

}
