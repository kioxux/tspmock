package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.storeData.StoreDataAddVO;
import com.gov.operate.entity.GaiaRegionalStore;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @author Zhangchi
 * @since 2021/10/14/15:24
 */
public interface GaiaRegionalStoreMapper extends BaseMapper<GaiaRegionalStore> {
     Integer getMaxSortNumber(@Param("fristArea") Long fristArea,@Param("client") String client);

     Integer getCurrentSortNumber(@Param("client") String client,@Param("stoCode") String stoCode);

     Integer getLastMaxSortNumber(@Param("client") String client,@Param("stoCode") String stoCode);

     Long findFirstAreaId(@Param("stoCode") String stoCode,@Param("client") String client);

     List<GaiaRegionalStore> findRegionalList(@Param("client") String client,@Param("stoCode") String stoCode);
}
