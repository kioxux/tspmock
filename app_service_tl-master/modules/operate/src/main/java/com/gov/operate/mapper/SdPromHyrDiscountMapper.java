package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SdPromHyrDiscount;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface SdPromHyrDiscountMapper extends BaseMapper<SdPromHyrDiscount> {

}
