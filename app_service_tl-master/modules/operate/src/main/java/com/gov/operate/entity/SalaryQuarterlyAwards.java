package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_QUARTERLY_AWARDS")
@ApiModel(value="SalaryQuarterlyAwards对象", description="")
public class SalaryQuarterlyAwards extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSQA_ID", type = IdType.AUTO)
    private Integer gsqaId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSQA_GSP_ID")
    private Integer gsqaGspId;

    @ApiModelProperty(value = "日销售额最小值")
    @TableField("GSQA_SALES_MIN")
    private BigDecimal gsqaSalesMin;

    @ApiModelProperty(value = "日销售额最大值")
    @TableField("GSQA_SALES_MAX")
    private BigDecimal gsqaSalesMax;

    @ApiModelProperty(value = "提成比例")
    @TableField("GSQA_COMMISION")
    private BigDecimal gsqaCommision;

    @ApiModelProperty(value = "顺序")
    @TableField("GSQA_SORT")
    private Integer gsqaSort;


}
