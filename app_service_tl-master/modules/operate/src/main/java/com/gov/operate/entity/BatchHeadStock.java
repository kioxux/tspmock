package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 总部库存
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_HEAD_STOCK")
@ApiModel(value="BatchHeadStock对象", description="总部库存")
public class BatchHeadStock extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableField("ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBHS_QUERY_FACTORY")
    private String gbhsQueryFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBHS_DC")
    private String gbhsDc;

    @ApiModelProperty(value = "日期")
    @TableField("GBHS_DATE")
    private String gbhsDate;

    @ApiModelProperty(value = "公司代码")
    @TableField("GBHS_COMPANY_CODE")
    private String gbhsCompanyCode;

    @ApiModelProperty(value = "公司名称")
    @TableField("GBHS_COMPANY_NAME")
    private String gbhsCompanyName;

    @ApiModelProperty(value = "产品代码")
    @TableField("GBHS_PRO_CODE")
    private String gbhsProCode;

    @ApiModelProperty(value = "产品名称")
    @TableField("GBHS_PRO_NAME")
    private String gbhsProName;

    @ApiModelProperty(value = "产品规格")
    @TableField("GBHS_PRO_SPECS")
    private String gbhsProSpecs;

    @ApiModelProperty(value = "批号")
    @TableField("GBHS_BATCH_NO")
    private String gbhsBatchNo;

    @ApiModelProperty(value = "数量")
    @TableField("GBHS_QTY")
    private BigDecimal gbhsQty;

    @ApiModelProperty(value = "单位")
    @TableField("GBHS_UNIT")
    private String gbhsUnit;

    @ApiModelProperty(value = "生产厂家")
    @TableField("GBHS_FACTORY")
    private String gbhsFactory;

    @ApiModelProperty(value = "有效期")
    @TableField("GBHS_VALID")
    private String gbhsValid;

    @ApiModelProperty(value = "库存状态")
    @TableField("GBHS_KCZT_BH")
    private String gbhsKcztBh;

    @ApiModelProperty(value = "批准文号")
    @TableField("GBHS_APPROVAL_NUMBER")
    private String gbhsApprovalNumber;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBHS_CREATE_DATE")
    private String gbhsCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBHS_CREATE_TIME")
    private String gbhsCreateTime;


}
