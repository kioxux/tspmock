package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.CleanDrawerVO;
import com.gov.operate.dto.GetCleanHighlightDTO;
import com.gov.operate.entity.SdCleandrawerH;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SdIndrawerH;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface SdCleandrawerHMapper extends BaseMapper<SdCleandrawerH> {
    /**
     * 清斗单高亮显示信息
     *
     * @param client
     * @param depId
     * @param gschVoucherId
     * @return
     */
    List<GetCleanHighlightDTO> getCleanHighlightInfo(@Param("client") String client, @Param("depId") String depId,
                                                     @Param("gschVoucherId") String gschVoucherId);

    /**
     * 门店清斗记录查询
     *
     * @param page
     * @param client
     * @param deptId
     * @param searchContent
     * @param query
     * @return
     */
    IPage<CleanDrawerVO> getPage(Page<CleanDrawerVO> page,
                                  @Param("client") String client,
                                  @Param("deptId") String deptId,
                                  @Param("searchContent") String searchContent,
                                  @Param("ew") QueryWrapper<SdCleandrawerH> query);

    /**
     * 调取清斗单信息
     *
     * @param query
     * @return
     */
    List<SdCleandrawerH> getList(@Param("ew") QueryWrapper<SdCleandrawerH> query);

    /**
     * 清斗单
     * @param page
     * @param client
     * @param gschBrId
     * @param gschVoucherId
     * @param beginDate
     * @param endDate
     * @param gschEmp1
     * @return
     */
    IPage<CleanDrawerVO> SdCleandrawerList(Page<CleanDrawerVO> page, @Param("client") String client, @Param("gschBrId") String gschBrId,
                                           @Param("gschVoucherId") String gschVoucherId, @Param("beginDate") String beginDate,
                                           @Param("endDate") String endDate, @Param("gschEmp1") String gschEmp1);
}
