package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.gov.operate.mapper.StoreDataMapper;
import com.gov.operate.service.request.StoreSalesStatisticsDayRequest;
import com.gov.operate.service.request.StoreSalesStatisticsRequest;
import com.gov.operate.service.response.StoreSalesStatisticsItemIndex;
import com.gov.operate.service.response.StoreSalesStatisticsRes;
import com.gov.operate.service.response.StoreSalesStatisticsResourceData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class StoreSalesStatisticsDayImpl implements StoreSalesStatisticsQueryByDataTypeStrategy {


//    private static final List<String> STATISTICS_DIMENSION = new ArrayList<>(Arrays.asList("销售额","毛利额","毛利率","日均销售额（元）","日均交易","单店日均销售额（元）","单店日均交易","客单价"));

    Map<String, List<StoreSalesStatisticsItemIndex>> STATISTICS_DIMENSION_MAP;

    private StoreDataMapper storeDataMapper;

    public StoreSalesStatisticsDayImpl(Map<String, List<StoreSalesStatisticsItemIndex>> STATISTICS_DIMENSION_MAP, StoreDataMapper storeDataMapper) {
        this.STATISTICS_DIMENSION_MAP = STATISTICS_DIMENSION_MAP;
        this.storeDataMapper = storeDataMapper;
    }

    @Override
    public StoreSalesStatisticsRes handleData(StoreSalesStatisticsRequest inData) {
        StoreSalesStatisticsRes res = new StoreSalesStatisticsRes();
        List<Map<String, List<StoreSalesStatisticsItemIndex>>> items = new ArrayList<>();
        Map<String, List<StoreSalesStatisticsItemIndex>> storeDataMap = new HashMap<>();
        //获取数据获取的时间范围
        List<String> allDays = new ArrayList<>();
        allDays.addAll(thisWeek());
        allDays.addAll(lastWeek());
        allDays.addAll(lastYearthisWeek());
        StoreSalesStatisticsDayRequest query = new StoreSalesStatisticsDayRequest();
        //查询每天门店的销售数据
        List<StoreSalesStatisticsResourceData> dbList = storeDataMapper.selectDataScope(query);
        if (CollectionUtil.isNotEmpty(dbList)) {
            res.setStoName(dbList.get(0).getStoName());
            res.setStore(dbList.get(0).getSite());
            res.setDateType(inData.getDateType());
            res.setStoreNum(1);

            List<StoreSalesStatisticsResourceData> thisWeekDbList = handleStoreSalesStatisticsItemIndex(dbList, thisWeek());
            List<StoreSalesStatisticsResourceData> lastWeekDbList = handleStoreSalesStatisticsItemIndex(dbList, lastWeek());
            List<StoreSalesStatisticsResourceData> lastYearThisWeekDbList = handleStoreSalesStatisticsItemIndex(dbList, lastYearthisWeek());
        }
        return res;
    }




    private List<StoreSalesStatisticsResourceData> handleStoreSalesStatisticsItemIndex(List<StoreSalesStatisticsResourceData> dbList, List<String> days) {
        List<StoreSalesStatisticsResourceData> resList = new ArrayList<>();
        for (StoreSalesStatisticsResourceData data : dbList) {
            for (String dayTime : days) {
                if (dayTime.equals(data.getSaleDate())) {
                    resList.add(data);
                }
            }
        }
        return resList;
    }


    /**
     * 根据日期返回星期几
     *
     * @param datetime
     * @return
     */
    public String dateToWeek(String datetime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三",
                "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance(); // 获得一个日历
        Date datet = null;
        try {
            datet = f.parse(datetime);
            cal.setTime(datet);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
        if (w < 0)
            w = 0;
        return weekDays[w];

    }


    /**
     * 获取上一周的每一天的日期
     *
     * @return
     * @throws ParseException
     */
    private List<String> lastWeek() {
        //获取本周的周一
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = new GregorianCalendar();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 0);
        String format = formater.format(cal.getTime());
        System.out.println(format);

        SimpleDateFormat aa = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        //上周一
        try {
            c.setTime(aa.parse(format));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, -7);
        Date d = c.getTime();
        String day = aa.format(d);
        System.out.println("上周一：" + day);
        //上周日
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 6);
        String lastSunday = formater.format(cal.getTime());
        try {
            c.setTime(aa.parse(lastSunday));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, -7);
        Date dd = c.getTime();
        String dayd = aa.format(dd);
        System.out.println("上周日：" + dayd);

        SimpleDateFormat datee = new SimpleDateFormat("yyyyMMdd");
        Calendar aaa = Calendar.getInstance();
        try {
            aaa.setTime(aa.parse(day));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        aaa.add(Calendar.DATE, -1);
        List<String> lastWeekList = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            aaa.add(Calendar.DAY_OF_MONTH, 1);
            System.out.println(formater.format(aaa.getTime()));
            lastWeekList.add(formater.format(aaa.getTime()));
        }
        return lastWeekList;
    }


    /**
     * 获取本周的每一天的日期
     *
     * @return
     */
    private List<String> thisWeek() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, -1);
        Date y = c.getTime();
        String year = format.format(y);
        //去年的今天
        System.out.println("过去一年：" + year);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar c1 = Calendar.getInstance();
        // 今天是一周中的第几天
        int dayOfWeek = c1.get(Calendar.DAY_OF_WEEK);

        if (c1.getFirstDayOfWeek() == Calendar.SUNDAY) {
            c1.add(Calendar.DAY_OF_MONTH, 1);
        }
        // 计算一周开始的日期
        c1.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
        List<String> lastWeekList = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            c1.add(Calendar.DAY_OF_MONTH, 1);
            System.out.println(sdf.format(c1.getTime()));
            lastWeekList.add(sdf.format(c1.getTime()));
        }
        return lastWeekList;
    }

    /**
     * 上一年的今天的这周每一天
     *
     * @return
     */
    private List<String> lastYearthisWeek() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, -1);
        Date y = c.getTime();
        String year = format.format(y);
        //去年的今天
        System.out.println("过去一年：" + year);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.YEAR, -1);
        // 今天是一周中的第几天
        int dayOfWeek = c1.get(Calendar.DAY_OF_WEEK);

        if (c1.getFirstDayOfWeek() == Calendar.SUNDAY) {
            c1.add(Calendar.DAY_OF_MONTH, 1);
        }
        // 计算一周开始的日期
        c1.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
        List<String> lastWeekList = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            c1.add(Calendar.DAY_OF_MONTH, 1);
            System.out.println(sdf.format(c1.getTime()));
            lastWeekList.add(sdf.format(c1.getTime()));
        }
        return lastWeekList;
    }
}
