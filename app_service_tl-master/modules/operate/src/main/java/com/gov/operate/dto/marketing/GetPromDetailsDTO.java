package com.gov.operate.dto.marketing;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.operate.entity.SdMarketingGiveaway;
import com.gov.operate.entity.SdMarketingPrd;
import com.gov.operate.entity.SdPromVariableSet;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class GetPromDetailsDTO extends SdMarketingPrd {

    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryCode;
    /**
     * 生产厂家名称
     */
    private String proFactoryName;
    /**
     * 成本价
     */
    private BigDecimal proCgj;
    /**
     * 零售价
     */
    private BigDecimal proLsj;



    /**
     * 促销方式名称
     */
    private String name;
    /**
     * 促销方式
     */
    private String gspvsMode;
    /**
     * 促销方式描述
     */
    private String gspvsModeName;
    /**
     * 促销类型
     */
    private String gspvsType;
    /**
     * 促销类型描述
     */
    private String gspvsTypeName;


    /**
     * 促销参数列表
     */
    private List<Param> paramList;
    /**
     * 赠品列表
     */
    private List<SdMarketingGiveawayDTO> giveawayList;

    @Data
    @EqualsAndHashCode
    public static class Param extends SdPromVariableSet {
        /**
         * 列的值
         */
        private String gspvsColumnsValue;
    }

    @Data
    @EqualsAndHashCode
    public static class SdMarketingGiveawayDTO extends SdMarketingGiveaway {
        /**
         * 商品编码
         */
        private String proSelfCode;
    }
}
