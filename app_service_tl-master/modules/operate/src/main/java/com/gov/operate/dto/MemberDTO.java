package com.gov.operate.dto;

import com.gov.operate.entity.SdMemberCard;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class MemberDTO extends SdMemberCard {

    /**
     * 会员电话
     */
    private String gsmbMobile;

    /**
     * 会员姓名
     */
    private String gsmbName;

    /**
     * 性别
     */
    private String gsmbSex;


    /**
     * 年龄
     */
    private String gsmbAge;

    /**
     * 生日
     */
    private String gsmbBirth;

    /**
     * 地址
     */
    private String gsmbAddress;
}
