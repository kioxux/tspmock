package com.gov.operate.dto;

import com.gov.operate.entity.PaymentApplications;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.20
 */
@Data
public class PaymentRecordDetail extends PaymentApplications {
    @ApiModelProperty(value = "供应商名")
    private String supName;

    @ApiModelProperty(value = "开户行名称")
    private String supBankName;

    @ApiModelProperty(value = "开户行账号")
    private String supBankAccount;

    @ApiModelProperty(value = "地点名")
    private String stoName;

    /**
     * 银行代码
     */
    @ApiModelProperty(value = "银行代码")
    private String supBankCode;

    /**
     * 备注
     */
    private String remark;

    /**
     * 工作流描述
     */
    private String wfDescription;
}
