package com.gov.operate.controller.storePayment;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.UserPaymentMaintenance;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IFiUserPaymentMaintenanceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-12-01
 */
@RestController
@RequestMapping("/maintenance/maintenance")
public class FiUserPaymentMaintenanceController {

    @Resource
    private IFiUserPaymentMaintenanceService fiUserPaymentMaintenanceService;

    @Log("获取付款维护数据")
    @ApiOperation(value = "获取付款维护数据")
    @PostMapping("getPaymentMaintenance")
    public Result getPaymentMaintenance(
            @RequestJson(value = "client", name = "用户") String client,
            @RequestJson(value = "ficoPayingstoreCode", required = false) String ficoPayingstoreCode,
            @RequestJson(value = "ficoPayingstoreName", required = false) String ficoPayingstoreName,
            @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
            @RequestJson(value = "pageSize", name = "分页") Integer pageSize
    ) {
        return fiUserPaymentMaintenanceService.getPaymentMaintenance(client, ficoPayingstoreCode, ficoPayingstoreName, pageNum, pageSize);
    }

    @Log("保存付款维护数据")
    @ApiOperation(value = "保存付款维护数据")
    @PostMapping("savePaymentMaintenance")
    public Result savePaymentMaintenance(@RequestBody List<UserPaymentMaintenance> list) {
        fiUserPaymentMaintenanceService.savePaymentMaintenance(list);
        return ResultUtil.success();
    }

}

