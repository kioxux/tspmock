package com.gov.operate.dto;

import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class FicoInvoiceInformationRegistrationDTO extends FicoInvoiceInformationRegistration {

    /**
     * 发票剩余登记金额
     */
    private BigDecimal invoiceBalance;

    /**
     * 地点名
     */
    private String stoName;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 业务员
     */
    private String invoiceSalesmanName;

}
