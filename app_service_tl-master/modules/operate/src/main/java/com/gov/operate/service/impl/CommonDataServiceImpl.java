package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.CommonData;
import com.gov.operate.mapper.CommonDataMapper;
import com.gov.operate.service.ICommonDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
@Service
public class CommonDataServiceImpl extends ServiceImpl<CommonDataMapper, CommonData> implements ICommonDataService {

    @Resource
    private CommonDataMapper commonDataMapper;
    /**
     * 功能说明
     */
    @Override
    public CommonData funDesc() {
        return commonDataMapper.funDesc();
    }

}
