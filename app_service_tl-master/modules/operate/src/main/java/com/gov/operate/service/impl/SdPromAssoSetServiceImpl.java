package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromAssoSet;
import com.gov.operate.mapper.SdPromAssoSetMapper;
import com.gov.operate.service.ISdPromAssoSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Service
public class SdPromAssoSetServiceImpl extends ServiceImpl<SdPromAssoSetMapper, SdPromAssoSet> implements ISdPromAssoSetService {

}
