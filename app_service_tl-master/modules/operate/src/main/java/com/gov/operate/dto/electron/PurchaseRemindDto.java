package com.gov.operate.dto.electron;

import com.gov.operate.entity.PurchaseRemind;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.07.13
 */
@Data
public class PurchaseRemindDto extends PurchaseRemind {

    /**
     * 会员姓名
     */
    private String gsmbName;

    /**
     * 微信openID
     */
    private String gsmbcOpenId;
}

