package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SalaryGrade;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
public interface ISalaryGradeService extends SuperService<SalaryGrade> {

    /**
     * 工资级别列表
     */
    List<SalaryGrade> getSalaryGradeList();
}
