package com.gov.operate.dto;

import com.gov.operate.entity.AppVersion;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AppVersionDTO extends AppVersion {

    /**
     * 下载地址绝对地址
     */
    private String downloadPathUrl;
}
