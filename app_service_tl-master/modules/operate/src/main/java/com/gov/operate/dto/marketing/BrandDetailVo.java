package com.gov.operate.dto.marketing;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 13:53 2021/8/16
 * @Description：品牌详细信息出参
 * @Modified By：guoyuxi.
 * @Version:
 */

@Data
public class BrandDetailVo {

    private String brandName;

}
