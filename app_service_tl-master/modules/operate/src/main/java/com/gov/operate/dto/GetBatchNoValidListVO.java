package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/14 14:36
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetBatchNoValidListVO {

    @NotBlank(message = "商品编号不能为空")
    private String gssbProId;
}
