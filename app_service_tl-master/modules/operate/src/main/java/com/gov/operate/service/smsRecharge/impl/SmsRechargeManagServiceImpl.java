package com.gov.operate.service.smsRecharge.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.storePayment.SmsSendRecord;
import com.gov.operate.dto.storePayment.SmsSendStatistic;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SmsRechargeRecord;
import com.gov.operate.mapper.FranchiseeMapper;
import com.gov.operate.mapper.SmsRechargeRecordMapper;
import com.gov.operate.mapper.SmsSendMapper;
import com.gov.operate.service.smsRecharge.ISmsRechargeManagService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Service
public class SmsRechargeManagServiceImpl implements ISmsRechargeManagService {
    @Resource
    private SmsRechargeRecordMapper smsRechargeRecordMapper;
    @Resource
    private SmsSendMapper smsSendMapper;
    @Resource
    private FranchiseeMapper franchiseeMapper;

    /**
     * 客户短信统计
     *
     * @param pageNum
     * @param pageSize
     * @param client
     * @return
     */
    @Override
    public Result getSmsSendStatistic(Integer pageNum, Integer pageSize, String client) {
        Page<SmsSendStatistic> page = new Page<>(pageNum, pageSize);
        IPage<SmsSendStatistic> iPage = smsRechargeRecordMapper.getSmsSendStatistic(page, client);
        // 统计数据
        SmsSendStatistic smsSendStatistic = new SmsSendStatistic();
        smsSendStatistic.setIPage(iPage);
        smsSendStatistic.setGsrrRechargeUseCount(smsRechargeRecordMapper.getSmsSendCount(client));
        return ResultUtil.success(smsSendStatistic);
    }

    /**
     * 客户短信发送记录
     *
     * @param pageNum
     * @param pageSize
     * @param client
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    @Override
    public Result getSmsSendRecord(Integer pageNum, Integer pageSize, String client, String gssSendTimeStart, String gssSendTimeEnd) {
        Page<SmsSendRecord> page = new Page<>(pageNum, pageSize);
        IPage<SmsSendRecord> iPage = smsSendMapper.getSmsSendRecord(page, client, gssSendTimeStart, gssSendTimeEnd);
        return ResultUtil.success(iPage);
    }

    /**
     * 客户充值记录
     *
     * @param pageNum
     * @param pageSize
     * @param client
     * @param gsrrRechargeTimeStart
     * @param gsrrRechargeTimeEnd
     * @return
     */
    @Override
    public Result getRechargeRecord(Integer pageNum, Integer pageSize, String client, String gsrrRechargeTimeStart, String gsrrRechargeTimeEnd) {
        Page<SmsSendStatistic> page = new Page<>(pageNum, pageSize);
        IPage<SmsSendStatistic> iPage = smsRechargeRecordMapper.getRechargeRecord(page, client, gsrrRechargeTimeStart, gsrrRechargeTimeEnd);
        // 统计数据
        SmsSendStatistic smsSendStatistic = new SmsSendStatistic();
        smsSendStatistic.setIPage(iPage);
        // 总计
        QueryWrapper<SmsRechargeRecord> smsRechargeRecordQueryWrapper = new QueryWrapper<>();
        smsRechargeRecordQueryWrapper.select(
                "IFNULL(SUM(GSRR_RECHARGE_COUNT), 0) AS GSRR_RECHARGE_COUNT",
                "IFNULL(SUM(GSRR_RECHARGE_AMT), 0) AS GSRR_RECHARGE_AMT",
                "IFNULL(SUM(GSRR_RECHARGE_USE_COUNT), 0) AS GSRR_RECHARGE_USE_COUNT");
        smsRechargeRecordQueryWrapper.ge(StringUtils.isNotBlank(gsrrRechargeTimeStart), "LEFT(GSRR_RECHARGE_TIME, 8)", gsrrRechargeTimeStart);
        smsRechargeRecordQueryWrapper.le(StringUtils.isNotBlank(gsrrRechargeTimeEnd), "LEFT(GSRR_RECHARGE_TIME, 8)", gsrrRechargeTimeEnd);
        smsRechargeRecordQueryWrapper.eq(StringUtils.isNotBlank(client), "CLIENT", client);
        smsRechargeRecordQueryWrapper.eq("GSRR_PAY_STATUS", 1);
        SmsRechargeRecord smsRechargeRecord = smsRechargeRecordMapper.selectOne(smsRechargeRecordQueryWrapper);
        smsSendStatistic.setGsrrRechargeAmt(smsRechargeRecord.getGsrrRechargeAmt());
        smsSendStatistic.setGsrrRechargeCount(smsRechargeRecord.getGsrrRechargeCount());
        smsSendStatistic.setGsrrRechargeUseCount(smsRechargeRecord.getGsrrRechargeUseCount());
        return ResultUtil.success(smsSendStatistic);
    }

    @Override
    public Result getClientList() {
        List<Franchisee> list = franchiseeMapper.selectList(new QueryWrapper<>());
        return ResultUtil.success(list);
    }
}
