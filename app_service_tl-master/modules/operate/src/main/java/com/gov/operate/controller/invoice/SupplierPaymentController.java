package com.gov.operate.controller.invoice;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.invoice.SupplierPrepaymentDto;
import com.gov.operate.entity.SupplierPrepayment;
import com.gov.operate.feign.PurchaseFeign;
import com.gov.operate.feign.dto.ApprovalInfo;
import com.gov.operate.feign.dto.FeignResult;
import com.gov.operate.feign.dto.SupplierPaymentAtt;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.ISupplierPrepaymentService;
import com.gov.operate.service.batchPurchaseSales.IBatchQueryAuthService;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.02.01
 */
@Validated
@RestController
@RequestMapping("invoice/supplierPayment")
public class SupplierPaymentController {
    @Resource
    private ISupplierPrepaymentService iSupplierPrepaymentService;
    @Resource
    private IBatchQueryAuthService iBatchQueryAuthService;
    @Resource
    private PurchaseFeign purchaseFeign;

    @Log("保存供应商付款保存")
    @ApiOperation(value = "保存供应商付款保存")
    @PostMapping("addSupplierPayment")
    public Result addSupplierPayment(@RequestBody SupplierPrepaymentDto supplierPrepayment) {
        Result result = iSupplierPrepaymentService.addSupplierPayment(supplierPrepayment);
        try {
            if (!CollectionUtils.isEmpty(supplierPrepayment.getPoIds())) {
                SupplierPrepaymentDto supplierPrepaymentResult = (SupplierPrepaymentDto) result.getData();
                SupplierPaymentAtt supplierPaymentAtt = new SupplierPaymentAtt();
                supplierPaymentAtt.setClient(supplierPrepaymentResult.getClient());
                supplierPaymentAtt.setPayOrderId(supplierPrepaymentResult.getPayOrderId());
                supplierPaymentAtt.setSupSelfCode(supplierPrepaymentResult.getSupCode());
                supplierPaymentAtt.setSupName(supplierPrepaymentResult.getSupName());
                supplierPaymentAtt.setSupBankName(supplierPrepaymentResult.getGspSupBankName());
                supplierPaymentAtt.setSupBankAccount(supplierPrepaymentResult.getGspSupBankNum());
                supplierPaymentAtt.setSupBankCode(supplierPrepaymentResult.getGspSupBankCode());
                supplierPaymentAtt.setPoIds(supplierPrepaymentResult.getPoIds());
                purchaseFeign.supplierPaymentAtt(supplierPaymentAtt);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Log("保存供应商付款申请")
    @ApiOperation(value = "保存供应商付款申请")
    @PostMapping("applySupplierPayment")
    public Result applySupplierPayment(@RequestBody SupplierPrepaymentDto supplierPrepayment) {
        Result result = iSupplierPrepaymentService.applySupplierPayment(supplierPrepayment);
        try {
            if (!CollectionUtils.isEmpty(supplierPrepayment.getPoIds())) {
                SupplierPrepaymentDto supplierPrepaymentResult = (SupplierPrepaymentDto) result.getData();
                SupplierPaymentAtt supplierPaymentAtt = new SupplierPaymentAtt();
                supplierPaymentAtt.setClient(supplierPrepaymentResult.getClient());
                supplierPaymentAtt.setPayOrderId(supplierPrepaymentResult.getPayOrderId());
                supplierPaymentAtt.setSupSelfCode(supplierPrepaymentResult.getSupCode());
                supplierPaymentAtt.setSupName(supplierPrepaymentResult.getSupName());
                supplierPaymentAtt.setSupBankName(supplierPrepaymentResult.getGspSupBankName());
                supplierPaymentAtt.setSupBankAccount(supplierPrepaymentResult.getGspSupBankNum());
                supplierPaymentAtt.setSupBankCode(supplierPrepaymentResult.getGspSupBankCode());
                supplierPaymentAtt.setPoIds(supplierPrepaymentResult.getPoIds());
                purchaseFeign.supplierPaymentAtt(supplierPaymentAtt);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Log("供应商核销")
    @ApiOperation(value = "供应商核销")
    @PostMapping("supplierPaymentWriteOff")
    public Result supplierPaymentWriteOff(@RequestBody SupplierPrepaymentDto supplierPrepaymentDto) {
        Result result = iSupplierPrepaymentService.supplierPaymentWriteOff(supplierPrepaymentDto);
        return result;
    }

    @Log("删除核销")
    @ApiOperation(value = "删除核销")
    @PostMapping("deleteWriteOff")
    public Result deleteWriteOff(@RequestBody List<Integer> idList) {
        Result result = iSupplierPrepaymentService.deleteWriteOff(idList);
        return result;
    }

    @Log("供应商付款查询")
    @ApiOperation(value = "供应商付款查询")
    @PostMapping("getSupplierPaymentList")
    public Result getSupplierPaymentList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                         @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                         @RequestJson(value = "gspApplyDateStart", required = false) String gspApplyDateStart,
                                         @RequestJson(value = "gspApplyDateEnd", required = false) String gspApplyDateEnd,
                                         @RequestJson(value = "payCompanyCode", required = false) String payCompanyCode,
                                         @RequestJson(value = "supCode", required = false) String supCode,
                                         @RequestJson(value = "payOrderId", required = false) String payOrderId,
                                         @RequestJson(value = "applicationPaymentAmountStart", required = false) BigDecimal applicationPaymentAmountStart,
                                         @RequestJson(value = "applicationPaymentAmountEnd", required = false) BigDecimal applicationPaymentAmountEnd,
                                         @RequestJson(value = "founder", required = false) String founder,
                                         @RequestJson(value = "applicationStatus", required = false) String applicationStatus,
                                         @RequestJson(value = "gspApprovalDateStart", required = false) String gspApprovalDateStart,
                                         @RequestJson(value = "gspApprovalDateEnd", required = false) String gspApprovalDateEnd,
                                         @RequestJson(value = "supSalesman", required = false) String supSalesman,
                                         @RequestJson(value = "approvalRemark", required = false) String approvalRemark,
                                         @RequestJson(value = "wfDescription", required = false) String wfDescription
    ) {
        return iSupplierPrepaymentService.getSupplierPaymentList(pageSize, pageNum, gspApplyDateStart, gspApplyDateEnd, payCompanyCode,
                supCode, payOrderId, applicationPaymentAmountStart, applicationPaymentAmountEnd, founder, applicationStatus, gspApprovalDateStart,
                gspApprovalDateEnd, supSalesman, approvalRemark, wfDescription);
    }

    @Log("供应商付款导出")
    @ApiOperation(value = "供应商付款导出")
    @PostMapping("getSupplierPaymentExport")
    public Result getSupplierPaymentExport(@RequestJson(value = "gspApplyDateStart", required = false) String gspApplyDateStart,
                                           @RequestJson(value = "gspApplyDateEnd", required = false) String gspApplyDateEnd,
                                           @RequestJson(value = "payCompanyCode", required = false) String payCompanyCode,
                                           @RequestJson(value = "supCode", required = false) String supCode,
                                           @RequestJson(value = "payOrderId", required = false) String payOrderId,
                                           @RequestJson(value = "applicationPaymentAmountStart", required = false) BigDecimal applicationPaymentAmountStart,
                                           @RequestJson(value = "applicationPaymentAmountEnd", required = false) BigDecimal applicationPaymentAmountEnd,
                                           @RequestJson(value = "founder", required = false) String founder,
                                           @RequestJson(value = "applicationStatus", required = false) String applicationStatus,
                                           @RequestJson(value = "gspApprovalDateStart", required = false) String gspApprovalDateStart,
                                           @RequestJson(value = "gspApprovalDateEnd", required = false) String gspApprovalDateEnd,
                                           @RequestJson(value = "supSalesman", required = false) String supSalesman,
                                           @RequestJson(value = "approvalRemark", required = false) String approvalRemark,
                                           @RequestJson(value = "wfDescription", required = false) String wfDescription
    ) throws IOException {
        return iSupplierPrepaymentService.getSupplierPaymentExport(gspApplyDateStart, gspApplyDateEnd, payCompanyCode,
                supCode, payOrderId, applicationPaymentAmountStart, applicationPaymentAmountEnd, founder, applicationStatus, gspApprovalDateStart,
                gspApprovalDateEnd, supSalesman, approvalRemark, wfDescription);
    }

    @Log("供应商付款核销列表")
    @ApiOperation(value = "供应商付款核销列表")
    @PostMapping("getWriteOffList")
    public Result getWriteOffList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                  @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                  @RequestJson(value = "gspApplyDateStart", required = false) String gspApplyDateStart,
                                  @RequestJson(value = "gspApplyDateEnd", required = false) String gspApplyDateEnd,
                                  @RequestJson(value = "orderCode", required = false) String orderCode,
                                  @RequestJson(value = "supSelfCode", required = false) String supSelfCode,
                                  @RequestJson(value = "type", required = false) String type,
                                  @RequestJson(value = "payCompanyCode", required = false) String payCompanyCode,
                                  @RequestJson(value = "invoiceSalesman", required = false) String invoiceSalesman
    ) {
        return iSupplierPrepaymentService.getWriteOffList(pageSize, pageNum, gspApplyDateStart, gspApplyDateEnd, orderCode, supSelfCode, payCompanyCode, type, invoiceSalesman);
    }

    @Log("供应商付款核销列表查看")
    @ApiOperation(value = "供应商付款核销列表查看")
    @PostMapping("viewWriteOffList")
    public Result viewWriteOffList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                   @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                   @RequestJson(value = "payOrderId", required = false) String payOrderId
    ) {
        return iSupplierPrepaymentService.viewWriteOffList(pageSize, pageNum, payOrderId);
    }

    @Log("业务机构")
    @ApiOperation(value = "业务机构")
    @GetMapping("getCompanyList")
    public Result getCompanyList() {
        return iSupplierPrepaymentService.getCompanyList();
    }


    @Log("工作流审批回调")
    @ApiOperation("审批回调")
    @PostMapping("payCallBack")
    public FeignResult payCallBack(@RequestBody ApprovalInfo info) {
        return iSupplierPrepaymentService.payCallBack(info);
    }

    /**
     * 销售开票业务机构
     *
     * @return
     */
    @Log("业务机构")
    @ApiOperation(value = "业务机构")
    @GetMapping("getDcList")
    public Result getDcList() {
        return iSupplierPrepaymentService.getDcList();
    }

    /**
     * 销售开票客户列表
     *
     * @return
     */
    @Log("客户列表")
    @ApiOperation(value = "客户列表")
    @GetMapping("getCustomerList")
    public Result getCustomerList(@RequestParam("matSiteCode") String matSiteCode) {
        return iSupplierPrepaymentService.getCustomerList(matSiteCode);
    }

    @Log("备注下拉框")
    @ApiOperation(value = "备注下拉框")
    @GetMapping("getApprovalRemarkList")
    public Result getApprovalRemarkList() {
        return iSupplierPrepaymentService.getApprovalRemarkList();
    }

    /**
     * 1123-数据直连需求-DDI
     *
     * @return
     */
    @Log("1123-数据直连需求-DDI")
    @ApiOperation(value = "1123-数据直连需求-DDI")
    @GetMapping("ddiHandler")
    public Result ddiHandler() {
        iBatchQueryAuthService.ddiHandler();
        return ResultUtil.success();
    }
}

