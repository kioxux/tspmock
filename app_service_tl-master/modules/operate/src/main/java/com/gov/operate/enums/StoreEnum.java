package com.gov.operate.enums;

import com.google.common.collect.Lists;
import com.gov.common.basic.DateUtils;
import com.gov.operate.entity.SdStoresGroup;
import com.gov.operate.entity.SdStoresGroupSet;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public enum StoreEnum {
    DX0001_1("DX0001","店型","1","社区店",0),
    DX0001_2("DX0001","店型","2","商业店",0),
    DX0001_3("DX0001","店型","3","院边店",0),
    DX0001_4("DX0001","店型","4","乡镇店",0),
    DX0002_1("DX0002","店型ABCD","1","A",0),
    DX0002_2("DX0002","店型ABCD","2","B",0),
    DX0002_3("DX0002","店型ABCD","3","C",0),
    DX0002_4("DX0002","店型ABCD","4","D",0),
    DX0003_0("DX0003","是否直营管理","0","是",0),
    DX0003_1("DX0003","是否直营管理","1","否",0),
    DX0004_1("DX0004","管理区域","1",null,1);


    private String gssgType;
    private String gssgTypeName;
    private String gssgId;
    private String gssgName;
    private Integer gssgIfConfig;

    public String getGssgType() {
        return gssgType;
    }

    public void setGssgType(String gssgType) {
        this.gssgType = gssgType;
    }

    public String getGssgTypeName() {
        return gssgTypeName;
    }

    public void setGssgTypeName(String gssgTypeName) {
        this.gssgTypeName = gssgTypeName;
    }

    public String getGssgId() {
        return gssgId;
    }

    public void setGssgId(String gssgId) {
        this.gssgId = gssgId;
    }

    public String getGssgName() {
        return gssgName;
    }

    public void setGssgName(String gssgName) {
        this.gssgName = gssgName;
    }

    public Integer getGssgIfConfig() {
        return gssgIfConfig;
    }

    public void setGssgIfConfig(Integer gssgIfConfig) {
        this.gssgIfConfig = gssgIfConfig;
    }

    public static List<SdStoresGroup> defaultStoreGroup(String client, String sroCode, String stoAttribute, boolean isDXOOO4){
        String dateStrYYMMDD = DateUtils.getCurrentDateStrYYMMDD();
        String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();
        ArrayList<SdStoresGroup> list = Lists.newArrayList(
                new SdStoresGroup(client, "1", "社区店", sroCode, null, "DX0001", "system", dateStrYYMMDD, timeStrHHMMSS),
                new SdStoresGroup(client, "1".equals(stoAttribute) ? "1" : "0", "1".equals(stoAttribute) ? "否" : "是", sroCode, null, "DX0003", "system", dateStrYYMMDD, timeStrHHMMSS),
                new SdStoresGroup(client, "2", "高", sroCode, null, "DX0002", "system", dateStrYYMMDD, timeStrHHMMSS)
        );
        if (isDXOOO4){
          list.add(new SdStoresGroup(client, "1", null, sroCode, null, "DX0004", "system", dateStrYYMMDD, timeStrHHMMSS));
        }
        return list;
    }

    public static List<SdStoresGroupSet> defaultStoreGroupSet(String client,boolean isDXOOO4){
        String dateStrYYMMDD = DateUtils.getCurrentDateStrYYMMDD();
        String timeStrHHMMSS = DateUtils.getCurrentTimeStrHHMMSS();
        ArrayList<SdStoresGroupSet> list = Lists.newArrayList(
                new SdStoresGroupSet(client, "DX0001", "1", "铺货店型", "社区店", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0001", "2", "铺货店型", "商业店", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0001", "3", "铺货店型", "院边店", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0001", "4", "铺货店型", "乡镇店", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0003", "0", "是否直营管理", "是", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0003", "1", "是否直营管理", "否", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0002", "1", "店效级别", "超高", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0002", "2", "店效级别", "高", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0002", "3", "店效级别", "中", "system", dateStrYYMMDD, timeStrHHMMSS,0),
                new SdStoresGroupSet(client, "DX0002", "4", "店效级别", "低", "system", dateStrYYMMDD, timeStrHHMMSS,0)
        );
        if (isDXOOO4){
            list.addAll(Lists.newArrayList(
                    new SdStoresGroupSet(client, "DX0004", "1", "管理区域", null, "system", dateStrYYMMDD, timeStrHHMMSS,1)
                    ));
        }
        return list;
    }
}
