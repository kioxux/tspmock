package com.gov.operate.dto.weChat;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class GetMaterialListVO extends Pageable {

    @NotBlank(message = "类型不能为空")
    private String type;

    /**
     * 发布列表 、 素材列表
     */
    private String materialType;
}
