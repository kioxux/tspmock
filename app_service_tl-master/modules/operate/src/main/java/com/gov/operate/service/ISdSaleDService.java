package com.gov.operate.service;

import com.gov.operate.dto.EffectiveSale;
import com.gov.operate.dto.MemberCard;
import com.gov.operate.dto.ShopAssistant;
import com.gov.operate.entity.SdSaleD;
import com.gov.mybatis.SuperService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 门店销售明细表 服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-13
 */
public interface ISdSaleDService extends SuperService<SdSaleD> {

    EffectiveSale selectSalePlan(Map map );

    List<MemberCard> selectCardByUser(Map map);

}
