package com.gov.operate.feign;

import com.alibaba.fastjson.JSONObject;
import com.gov.operate.feign.dto.GetWorkflowInData;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Component
@FeignClient(value = "gys-auth", fallback = AuthFeignFallback.class
       //,url = "http://172.19.1.120:10103/"
)
public interface AuthFeign {

    /**
     * 流程发起
     *
     * @param param
     */
    @PostMapping(value = "/createWorkflow")
    public JsonResult createWorkflow(Map<String, Object> param);

    /**
     * 当前登录人信息
     *
     * @param param
     * @return
     */
    @PostMapping(value = "/getLoginInfo")
    public JSONObject getLoginInfo(Map<String, Object> param);


    @PostMapping({"/workflow/selectOneApp"})
    public JsonResult selectOneApp(@RequestBody GetWorkflowInData inData);
}
