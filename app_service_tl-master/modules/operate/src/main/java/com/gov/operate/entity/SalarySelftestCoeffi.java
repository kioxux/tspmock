package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_SELFTEST_COEFFI")
@ApiModel(value="SalarySelftestCoeffi对象", description="")
public class SalarySelftestCoeffi extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSSC_ID", type = IdType.AUTO)
    private Integer gsscId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "系数类型")
    @TableField("GSSC_TYPE")
    private Integer gsscType;

    @ApiModelProperty(value = "基本工资")
    @TableField("GSSC_BASIC")
    private BigDecimal gsscBasic;

    @ApiModelProperty(value = "岗位工资")
    @TableField("GSSC_POST")
    private BigDecimal gsscPost;

    @ApiModelProperty(value = "绩效工资 ")
    @TableField("GSSC_PERFORMANCE")
    private BigDecimal gsscPerformance;

    @ApiModelProperty(value = "考评工资")
    @TableField("GSSC_EVALUATION")
    private BigDecimal gsscEvaluation;


}
