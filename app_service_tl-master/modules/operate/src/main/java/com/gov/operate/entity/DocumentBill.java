package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DOCUMENT_BILL")
@ApiModel(value="DocumentBill对象", description="")
public class DocumentBill extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GDB_NUM")
    private String gdbNum;

    @ApiModelProperty(value = "供应商")
    @TableField("GDB_SUP_CODE")
    private String gdbSupCode;

    @ApiModelProperty(value = "收货金额")
    @TableField("GDB_TOTAL_AMT")
    private BigDecimal gdbTotalAmt;

    @ApiModelProperty(value = "折扣")
    @TableField("GDB_REBATE")
    private BigDecimal gdbRebate;

    @ApiModelProperty(value = "应付金额")
    @TableField("GDB_PAY_AMT")
    private BigDecimal gdbPayAmt;

    @ApiModelProperty(value = "差异")
    @TableField("GDB_DIFF")
    private BigDecimal gdbDiff;

    @ApiModelProperty(value = "原因")
    @TableField("GDB_REASON")
    private String gdbReason;

    @ApiModelProperty(value = "创建人")
    @TableField("GDB_CREATE_USER")
    private String gdbCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GDB_CREATE_DATE")
    private String gdbCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GDB_CREATE_TIME")
    private String gdbCreateTime;


}
