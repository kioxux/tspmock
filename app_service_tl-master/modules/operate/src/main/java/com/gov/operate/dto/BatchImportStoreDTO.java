package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: jinwencheng
 * @Date: Created in 2021-12-24 10:18:32
 * @Description: 门店分类csv批量导入
 * @Version: 1.0
 */
@Data
public class BatchImportStoreDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String gssgBrId;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 分类类型编码
     */
    private String gssgType;

    /**
     * 分类类型名称
     */
    private String gssgTypeName;

    /**
     * 分类名称编码
     */
    private String gssgId;

    /**
     * 分类名称
     */
    private String gssgIdName;

    /**
     * 修改人
     */
    private String gssgUpdateEmp;

    /**
     * 修改日期
     */
    private String gssgUpdateDate;

    /**
     * 修改时间
     */
    private String gssgUpdateTime;

}
