package com.gov.operate.dto.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@ApiModel("消费订单列表")
@Data
public class OrderInfoDTO {

    @ApiModelProperty("销售日期")
    private String gsshDate;

    @ApiModelProperty("销售时间")
    private String gsshTime;

    @ApiModelProperty("门店名称")
    private String gsshBrName;

    @ApiModelProperty("应收金额")
    private BigDecimal gsshYsAmt;

    @ApiModelProperty("消费订单明细列表")
    private List<OrderInfoDetailDTO> orderInfoDetailDTOS;
}
