package com.gov.operate.controller.ssp;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.ssp.SspMenuChainDTO;
import com.gov.operate.dto.ssp.SspUserDTO;
import com.gov.operate.dto.ssp.SspUserPageDTO;
import com.gov.operate.dto.ssp.SspUserProductPageDTO;
import com.gov.operate.entity.SspUserChain;
import com.gov.operate.entity.SspUserProduct;
import com.gov.operate.service.ssp.ISspMenuChainService;
import com.gov.operate.service.ssp.ISspUserProductService;
import com.gov.operate.service.ssp.ISspUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 供应商
 */
@Api(tags = "授权流向")
@RestController
@RequestMapping("ssp")
public class SspGrantController {

    @Resource
    private ISspUserService sspUserService;

    @Resource
    private ISspUserProductService sspUserProductService;

    @Resource
    private ISspMenuChainService sspMenuChainService;

    @Log("流向授权列表")
    @PostMapping("userList")
    @ApiOperation(value = "流向授权列表")
    public Result userList(@Valid @RequestBody SspUserPageDTO sspUserPageDTO) {
        return sspUserService.selectUserByPage(sspUserPageDTO);
    }

    @Log("获取供应商销售员信息")
    @GetMapping("getSupSalesmanInfo")
    @ApiOperation(value = "获取供应商销售员信息")
    public Result getSupSalesmanInfo(@RequestParam String mobile) {
        return sspUserService.getSupSalesmanInfo(mobile);
    }


    @Log("获取供应商销售员信息")
    @GetMapping("getSupplierList")
    @ApiOperation(value = "获取供应商销售员信息")
    public Result getSupplierList(@RequestParam String mobile) {
        return sspUserService.getSupplierList(mobile);
    }

    @Log("流向授权用户添加")
    @PostMapping("add")
    @ApiOperation(value = "流向授权用户添加")
    public Result addUser(@Valid @RequestBody SspUserDTO sspUserDTO) {
        return sspUserService.addUser(sspUserDTO);
    }


    @Log("获取授权人信息")
    @GetMapping("getUserInfo")
    @ApiOperation(value = "获取授权人信息")
    public Result getUserInfo(@RequestParam("userId") Long userId) {
        return sspUserService.getUserInfo(userId);
    }

    @Log("获取授权人供应商信息")
    @GetMapping("getUserSupplierList")
    @ApiOperation(value = "获取授权人供应商信息")
    public Result getUserSupplierList(@RequestParam("userId") Long userId) {
        return ResultUtil.success(sspUserService.getUserSupplierList(userId));
    }

    @Log("流向授权用户更新")
    @PostMapping("update")
    @ApiOperation(value = "流向授权用户更新")
    public Result updateUser(@Valid @RequestBody SspUserDTO sspUserDTO) {
        return sspUserService.updateUser(sspUserDTO);
    }

    @Log("获取主体列表")
    @GetMapping("getChainList")
    @ApiOperation(value = "获取主体列表")
    public Result getChainList(@RequestParam("userId") Long userId) {
        return sspUserService.getChainList(userId);
    }

    @Log("获取用户主体列表")
    @GetMapping("getUserChainList")
    @ApiOperation(value = "获取用户主体列表")
    public Result getUserChainList(@RequestParam("userId") Long userId) {
        return sspUserService.getUserChainList(userId);
    }

    @Log("流向授权用户主体更新")
    @PostMapping("updateChain/{userId}")
    @ApiOperation(value = "流向授权用户主体更新")
    public Result updateChain(@RequestBody List<SspUserChain> chainList, @PathVariable("userId") Long userId) {
        return sspUserService.updateChain(chainList, userId);
    }

    @Log("获取供应商商品列表")
    @PostMapping("getSupplierProductList")
    @ApiOperation(value = "获取供应商商品列表")
    public Result getSupplierProductList(@RequestBody @Valid SspUserProductPageDTO sspUserProductPageDTO) {
        return sspUserProductService.getSupplierProductList(sspUserProductPageDTO);
    }


    @Log("导入供应商商品列表")
    @PostMapping("importSupplierProductList/{userId}")
    @ApiOperation(value = "导入供应商商品列表")
    public Result importSupplierProductList(@RequestParam("file") MultipartFile file, @PathVariable("userId") Long userId) {
        return sspUserProductService.importSupplierProductList(file, userId);
    }

    @Log("添加用户绑定产品")
    @PostMapping("addUserProduct/{userId}")
    @ApiOperation(value = "添加用户绑定产品")
    public Result addUserProduct(@RequestBody List<SspUserProduct> productList, @PathVariable("userId") Long userId) {
        return sspUserProductService.addUserProduct(productList, userId);
    }

    @Log("移除用户绑定产品")
    @PostMapping("delUserProduct")
    @ApiOperation(value = "移除用户绑定产品")
    public Result delUserProduct(@RequestBody List<SspUserProduct> productList) {
        return sspUserProductService.delUserProduct(productList);
    }

    @Log("获取用户商品列表")
    @PostMapping("getUserProductList")
    @ApiOperation(value = "获取用户商品列表")
    public Result getUserProductList(@RequestBody @Valid SspUserProductPageDTO sspUserProductPageDTO) {
        return sspUserProductService.getUserProductList(sspUserProductPageDTO);
    }

    @Log("获取授权报表列表")
    @GetMapping("getMenuChainList/{userId}")
    @ApiOperation(value = "获取授权报表列表")
    public Result getMenuChainList(@PathVariable("userId") Long userId) {
        return sspMenuChainService.getMenuChainList(userId);
    }

    @Log("绑定授权报表")
    @PostMapping("bindMenuChainList/{userId}")
    @ApiOperation(value = "绑定授权报表")
    public Result bindMenuChainList(@RequestBody List<SspMenuChainDTO> menuChainDTOList, @PathVariable("userId") Long userId) {
        return sspMenuChainService.bindMenuChainList(menuChainDTOList, userId);
    }


    @Log("冻结状态更新")
    @PostMapping("updateStatus")
    @ApiOperation(value = "冻结状态更新")
    public Result updateStatus(@Valid @RequestBody SspUserDTO sspUserDTO) {
        return sspUserService.updateStatus(sspUserDTO);
    }


}
