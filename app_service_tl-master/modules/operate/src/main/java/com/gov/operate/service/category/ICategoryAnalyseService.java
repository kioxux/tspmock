package com.gov.operate.service.category;

import com.gov.operate.dto.category.CategoryInData;
import com.gov.operate.dto.category.CategoryOutData;

import java.util.List;
import java.util.Map;

public interface ICategoryAnalyseService {
    Map<String,Object> selectAplProductList(CategoryInData inData);

    List<CategoryOutData>selectAplProduct(CategoryInData inData);

    void updateAdjustPrice(List<CategoryInData> list);

    void outProduct(List<CategoryInData> list);

    void updateProduct(List<CategoryInData> list);

    List<Map<String,String>> selectDateList(Map<String,String> inData);

    List<Map<String,String>> selectTableData(CategoryInData inData);

    void timerUpdateAdjustPrice(String inData);

    void timerCancelOperate(String inData);
}
