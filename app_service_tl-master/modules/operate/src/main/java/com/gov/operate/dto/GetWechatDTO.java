package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetWechatDTO {

    /**
     * LOGO
     **/
    private String logo;

    /**
     * LOGO绝对地址
     **/
    private String logoUrl;

    /**
     * 特权说明
     **/
    private String memberBenefits;

    /**
     * 有效日期
     **/
    private String accountsValidPeriod;

    /**
     * 使用须知
     **/
    private String accountsUsageNotice;

    /**
     * 电话号码
     **/
    private String accountsTel;

    /**
     *
     */
    private String wechatName;
}
