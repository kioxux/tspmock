package com.gov.operate.dto.marketing;

import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:59 2021/8/13
 * @Description：查询成分信息出参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class ProComDetailVO {

    private String proComCode;

    private String proComName;
}
