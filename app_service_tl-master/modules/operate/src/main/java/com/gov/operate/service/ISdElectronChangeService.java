package com.gov.operate.service;

import com.gov.operate.entity.SdElectronChange;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 电子券异动表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-07-09
 */
public interface ISdElectronChangeService extends SuperService<SdElectronChange> {

}
