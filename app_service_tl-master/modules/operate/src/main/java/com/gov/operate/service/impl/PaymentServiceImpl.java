package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.BillEndBatchVO;
import com.gov.operate.dto.invoice.FicoInvoiceInformationKey;
import com.gov.operate.dto.invoice.PaymentApplicationsGroupKey;
import com.gov.operate.dto.storePayment.PaymentApproval;
import com.gov.operate.dto.invoice.GetPaymentApplyDetails;
import com.gov.operate.entity.*;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private CommonService commonService;
    @Resource
    private PaymentMapper paymentMapper;
    @Resource
    private PaymentApplicationsMapper paymentApplicationsMapper;
    @Resource
    private AuthFeign authFeign;
    @Resource
    private IFicoInvoiceInformationRegistrationService ficoInvoiceInformationRegistrationService;
    @Resource
    private IPaymentDocumentNoApplicationsService paymentDocumentNoApplicationsService;
    @Resource
    private IPaymentVatApplicationsService paymentVatApplicationsService;
    @Resource
    private IFiInvoiceRegistrationService fiInvoiceRegistrationService;
    @Resource
    private IFiInvoiceRegistrationDetailedService fiInvoiceRegistrationDetailedService;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SupplierBusinessMapper supplierBusinessMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private FiSupplierPrepaymentMapper fiSupplierPrepaymentMapper;
    @Resource
    private BillInvoiceEndMapper billInvoiceEndMapper;
    @Resource
    private FicoInvoiceInformationRegistrationMapper invoiceInformationRegistrationMapper;
    @Resource
    private ChajiaZMapper chajiaZMapper;

    /**
     * 可付款单据
     */
    @Override
    public IPage<SelectBillDTO> selectBill(SelectBillVO vo) {
        // 地点和单体店只选一个时，另一个赋特殊值
        if (!((StringUtils.isBlank(vo.getSite()) && StringUtils.isBlank(vo.getStoCode()))
                || (StringUtils.isNotBlank(vo.getSite()) && StringUtils.isNotBlank(vo.getStoCode())))) {
            if (StringUtils.isBlank(vo.getSite())) {
                vo.setSite("-1");
            } else {
                vo.setStoCode("-1");
            }
        }
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(vo.getPageNum(), vo.getPageSize());
        IPage<SelectBillDTO> iPage = paymentMapper.selectBill(page, vo);

        List<String> matDnIdList = iPage.getRecords().stream().map(SelectBillDTO::getMatDnId).collect(Collectors.toList());
        List<ChajiaZ> chajiaZList = null;
        if (!CollectionUtils.isEmpty(matDnIdList)) {
            chajiaZList = chajiaZMapper.selectChajiaList(user.getClient(), matDnIdList);
        }
        for (SelectBillDTO item : iPage.getRecords()) {
            if (("CJ".equals(item.getBusinessType()) || "CJTZ".equals(item.getBusinessType())) && !CollectionUtils.isEmpty(chajiaZList)) {
                ChajiaZ chajiaZ = chajiaZList.stream().filter(t -> t.getCjId().equals(item.getMatDnId())).findFirst().orElse(null);
                if (chajiaZ != null) {
                    item.setMatDate(chajiaZ.getCjDate());
                }
            }
            if ("GD".equals(item.getBusinessType()) || "CJ".equals(item.getBusinessType())) {
                // 应付金额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
                // 已付金额
                item.setPaidAmount(item.getPaidAmount().abs().multiply(Constants.MINUS_ONE));
                // 剩余金额
                item.setChargeAmount(item.getChargeAmount().abs().multiply(Constants.MINUS_ONE));
            }
            // 发票付款判断
            String invoice = paymentMapper.getinvoiceNumListByBill(item.getMatDnId(), user.getClient());
            if (StringUtils.isNotBlank(invoice)) {
                // 发票付款记录
                List<PaymentVatApplications> paymentVatApplicationsList = paymentMapper.selectInvoicePayList(invoice, user.getClient());
                if (CollectionUtils.isEmpty(paymentVatApplicationsList)) {
                    item.setInvoicePayd(0);
                } else {
                    item.setInvoicePayd(1);
                    List<String> invoices = paymentVatApplicationsList.stream().map(PaymentVatApplications::getInvoiceNum).collect(Collectors.toList());
                    item.setInvoiceNums(String.join(",", invoices));
                }
            }
        }
        return iPage;
    }

    /**
     * 可付款单据明细
     */
    @Override
    public List<SelectBillDetailsDTO> selectBillDetails(SelectBillDetailsVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        List<SelectBillDetailsDTO> list = paymentMapper.selectBillDetails(vo);
        list.forEach(item -> {
            if ("GD".equals(item.getBusinessType()) || "CJ".equals(item.getBusinessType())) {
                // 应付金额
                item.setLineRegistrationAmount(item.getLineRegistrationAmount().abs().multiply(Constants.MINUS_ONE));
                // 已付金额
                item.setPaymentAmountOfLine(item.getPaymentAmountOfLine().abs().multiply(Constants.MINUS_ONE));
                // 剩余金额
                item.setChargeAmount(item.getChargeAmount().abs().multiply(Constants.MINUS_ONE));
            }
        });
        return list;
    }

    /**
     * 单据付款
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void billPayment(List<BillPaymentVO> list) {
        TokenUser user = commonService.getLoginInfo();
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("请选择单据");
        }
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getMatDnId())) {
                throw new CustomResultException("单据号不能为空");
            }
            if (StringUtils.isEmpty(item.getType())) {
                throw new CustomResultException("单据类型不能为空");
            }
            QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
            billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
            billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 1);
            billInvoiceEndQueryWrapper.eq("GBIE_NUM", item.getMatDnId());
            List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
            if (!CollectionUtils.isEmpty(billInvoiceEndList)) {
                throw new CustomResultException("单据[" + item.getMatDnId() + "]已核销");
            }
        });

        // 单据明细 明细list
        List<FiInvoiceRegistrationDetailedDTO> invoiceRegistrationDetailedList = paymentMapper.getInvoiceRegistrationDetailList(user.getClient(), list);
        // 单据发票 关系表list
        List<FiInvoiceRegistration> invoiceRegistrationList = paymentMapper.getInvoiceRegistrationList(user.getClient(), list);
        // 发票list
        List<String> invoiceNumList = invoiceRegistrationList.stream().map(FiInvoiceRegistration::getInvoiceNum).distinct().collect(Collectors.toList());
        List<FicoInvoiceInformationRegistration> invoiceInformationList = ficoInvoiceInformationRegistrationService.list(
                new QueryWrapper<FicoInvoiceInformationRegistration>()
                        .eq("CLIENT", user.getClient())
                        .in(!CollectionUtils.isEmpty(invoiceNumList), "INVOICE_NUM", invoiceNumList));
        // 验证当前订单是否预付过，如果订单已预付，则不可以选择已付款的发票
        list.forEach(item -> {
            QueryWrapper<FiSupplierPrepayment> fiSupplierPrepaymentQueryWrapper = new QueryWrapper<>();
            fiSupplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
            fiSupplierPrepaymentQueryWrapper.eq("MAT_DN_ID", item.getMatDnId());
            List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectList(fiSupplierPrepaymentQueryWrapper);
            // 单据已预付
            if (!CollectionUtils.isEmpty(fiSupplierPrepaymentList)) {
                throw new CustomResultException("单据[" + item.getMatDnId() + "]已预付，不可以付款");
            }
        });

        /**
         *  1.验证当前单据下的所有发票是否执行过发票付款（发票信息登记表.付款状态 = 1发票付款）
         *    如果存在 报错 “单据:{单据号}的发票已单独付款”
         */
        String matDnIdStr = list.stream().map(BillPaymentVO::getMatDnId).collect(Collectors.joining(","));
        List<PaymentVatApplications> paymentVatApplicationsList = paymentMapper.selectInvoiceListHasPayment(user.getClient(), matDnIdStr);
        if (!CollectionUtils.isEmpty(paymentVatApplicationsList)) {
            throw new CustomResultException("单据的发票已单独付款");
        }

        /**
         * 2.判断当前单据的已付款金额（GAIA_FI_INVOICE_REGISTRATION_发票登记.sum(单据已付款金额)）是否等于 GAIA_FI_INVOICE_REGISTRATION_发票登记.sum(单据已登记金额)
         * if 相等 报错 “当前单据已完成付款”
         */
        List<String> billPayFinishedList = paymentMapper.getBillPayFinished(user.getClient(), list);
        if (!CollectionUtils.isEmpty(billPayFinishedList)) {
            String collect = billPayFinishedList.stream().collect(Collectors.joining(","));
            throw new CustomResultException(MessageFormat.format("[{0}]单据已完成付款", collect));
        }

        /**
         * 3.更新发票表、单据发票、单据发票明细数据
         *   3.1更新 GAIA_FICO_INVOICE_INFORMATION_REGISTRATION_发票信息登记表数据，付款状态 = 2单据付款， 已付款金额 = 已付款金额 + (2.2(原单据已登记金额 - 原单据已付款金额))
         *   3.2更新当前单据 GAIA_FI_INVOICE_REGISTRATION_发票登记， 单据已付款金额 = 单据已登记金额
         *   3.3更新当前单据 GAIA_FI_INVOICE_REGISTRATION_DETAILED_发票登记明细表数据， 行已付款金额 = 行登记金额
         */
        // 3.1>更新 发票
        invoiceRegistrationList.forEach(item -> {
            // 原单据已付款金额
            BigDecimal paymentAmountOfDocuments = item.getPaymentAmountOfDocuments();
            // 原单据已登记金额
            BigDecimal amountOfDocumentRegistration = item.getAmountOfDocumentRegistration();
            // 该单据 该发票 本次登记金额
            BigDecimal currentRegAmount = amountOfDocumentRegistration.subtract(paymentAmountOfDocuments);
            // 发票信息
            FicoInvoiceInformationRegistration informationRegistration = invoiceInformationList.stream()
                    .filter(invoice -> invoice.getClient().equals(item.getClient()) && invoice.getInvoiceNum().equals(item.getInvoiceNum()))
                    .findFirst().get();
            // 已付款金额 = 已付款金额 + (2.2(原单据已登记金额 - 原单据已付款金额
            FicoInvoiceInformationRegistration ficoInvoiceInformationRegistration = invoiceInformationRegistrationMapper.selectOne(new QueryWrapper<FicoInvoiceInformationRegistration>()
                    .eq("INVOICE_NUM", informationRegistration.getInvoiceNum())
                    .eq("CLIENT", user.getClient()));
            if (ficoInvoiceInformationRegistration != null) {
                BigDecimal havPaymentAmount = (ficoInvoiceInformationRegistration.getInvoicePaidAmount() == null ? BigDecimal.ZERO : ficoInvoiceInformationRegistration.getInvoicePaidAmount()).add(currentRegAmount);
                ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                        // 发票已经付款金额
                        .set("INVOICE_PAID_AMOUNT", havPaymentAmount)
                        // 发票付款状态
                        .set("INVOICE_PAYMENT_STATUS", "2")
                        .eq("CLIENT", informationRegistration.getClient())
                        .eq("INVOICE_NUM", informationRegistration.getInvoiceNum()));
            }
        });
        // 3.2>更新 发票登记
        paymentMapper.updateInvoiceRegistration(user.getClient(), list);
        // 3.3>更新 发票登记明细
        paymentMapper.updateInvoiceRegistrationDetailed(user.getClient(), list);

        /**
         * 4.生成供应商单据付款申请数据
         *   付款单号 = DJ+年月日（8位）+ 4位流水
         *   for(3.1(原行已付款金额 != 原行登记金额))
         *   行号 = 索引
         *   已付款金额 = 3.1(原行已付款金额)
         *   本次付款金额 = 3.1(原行登记金额 - 原行已付款金额)
         *   申请状态 = 1, 审批状态 = -1
         */
        List<FiInvoiceRegistration> invoiceRegistrations = fiInvoiceRegistrationService.list(new QueryWrapper<FiInvoiceRegistration>()
                .eq("CLIENT", user.getClient())
                .in("BUSINESS_TYPE", list.stream().map(BillPaymentVO::getType).collect(Collectors.toList()))
                .in("MAT_DN_ID", list.stream().map(BillPaymentVO::getMatDnId).collect(Collectors.toList())));

        List<PaymentDocumentNoApplications> paymentDocumentNoApplicationsList = new ArrayList<>();
        for (int i = 0; i < invoiceRegistrationDetailedList.size(); i++) {
            FiInvoiceRegistrationDetailedDTO registrationDetailed = invoiceRegistrationDetailedList.get(i);

            PaymentDocumentNoApplications paymentDocumentNoApplications = new PaymentDocumentNoApplications();
            paymentDocumentNoApplicationsList.add(paymentDocumentNoApplications);
            // 加盟商
            paymentDocumentNoApplications.setClient(user.getClient());
            // 行号
            paymentDocumentNoApplications.setLineNo(String.valueOf(i + 1));
            // 付款单据日期
            paymentDocumentNoApplications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 业务单号
            paymentDocumentNoApplications.setMatDnId(registrationDetailed.getMatDnId());
            // 商品编码
            paymentDocumentNoApplications.setSoProCode(registrationDetailed.getSoProCode());
            // 供应商编码
            FiInvoiceRegistration invoiceRegistration = invoiceRegistrations.stream().filter(Objects::nonNull).filter(item -> {
                return item.getClient().equals(registrationDetailed.getClient())
                        && item.getBusinessType().equals(registrationDetailed.getBusinessType())
                        && item.getMatDnId().equals(registrationDetailed.getMatDnId());
            }).findFirst().orElse(null);
            paymentDocumentNoApplications.setSupCode(invoiceRegistration != null ? invoiceRegistration.getPoSupplierId() : "");
            // 地点
            paymentDocumentNoApplications.setProSite(invoiceRegistration != null ? invoiceRegistration.getSiteCode() : "");
            // 已付款金额
            paymentDocumentNoApplications.setInvoicePaidAmount(registrationDetailed.getPaymentAmountOfLine());
            // 本次付款金额
            paymentDocumentNoApplications.setInvoiceAmountOfThisPayment(registrationDetailed.getLineRegistrationAmount().subtract(registrationDetailed.getPaymentAmountOfLine()));
            // 物料凭证号
            paymentDocumentNoApplications.setMatId(registrationDetailed.getMatId());
            // matYear
            paymentDocumentNoApplications.setMatYear(registrationDetailed.getMatYear());
            // matLineNo
            paymentDocumentNoApplications.setMatLineNo(registrationDetailed.getMatLineNo());
            // 业务类型
            paymentDocumentNoApplications.setBusinessType(registrationDetailed.getBusinessType());
        }

        String no = getPaymentOrderNoByDB(user.getClient());
        int index = 0;
        // 地点+供应商分组
        Map<PaymentApplicationsGroupKey, List<PaymentDocumentNoApplications>> collect = paymentDocumentNoApplicationsList.stream()
                .collect(Collectors.groupingBy(item -> new PaymentApplicationsGroupKey(item.getSupCode(), item.getProSite())));
        // 地点+供应商分组 对应一条数据
        for (PaymentApplicationsGroupKey key : collect.keySet()) {
            List<PaymentDocumentNoApplications> paymentList = collect.get(key);
            index++;
            // 付款单号
            String paymentOrderNo = "DJ" + (NumberUtils.toLong(no) + index);
            // 本次付款总金额
            BigDecimal thisPaymentTotal = paymentList.stream().map(PaymentDocumentNoApplications::getInvoiceAmountOfThisPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
            PaymentApplications applications = new PaymentApplications();
            applications.setClient(user.getClient());
            // 供应商
            applications.setSupCode(key.getSupCode());
            // 地点
            applications.setProSite(key.getProSite());
            // 申请日期
            applications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 1单据付款，2发票付款
            applications.setType(1);
            // 本次付款总金额
            applications.setInvoiceAmountOfThisPayment(thisPaymentTotal);
            // 0-已申请；1-未申请
            applications.setApplicationStatus(1);
            // '-1未申请，0-审批中，1-已审批，2-已废弃
            applications.setApprovalStatus(-1);
            // 工作流编码
            applications.setApprovalFlowNo("");
            // 付款单号
            applications.setPaymentOrderNo(paymentOrderNo);
            paymentApplicationsMapper.insert(applications);

            paymentList.forEach(item -> {
                // 设置 主表id
                item.setApplicationsId(applications.getId());
                // 设置 付款单号
                item.setPaymentOrderNo(paymentOrderNo);
            });

        }
        paymentDocumentNoApplicationsService.saveBatch(paymentDocumentNoApplicationsList);
    }

    /**
     * 明细付款
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void billDetailPayment(BillDetailPaymentVO vo) {
        TokenUser user = commonService.getLoginInfo();
        // 加盟商
        String client = user.getClient();
        // 业务单号
        String matDnId = vo.getMatDnId();
        // 业务类型
        String type = vo.getType();
        vo.setClient(user.getClient());
        // 明细列表
        List<BillDetailPaymentVO.BillDetailPayment> billDetailPaymentList = vo.getBillDetailPaymentList();

        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
        billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 1);
        billInvoiceEndQueryWrapper.eq("GBIE_NUM", vo.getMatDnId());
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
        if (!CollectionUtils.isEmpty(billInvoiceEndList)) {
            throw new CustomResultException("该单据已核销");
        }

        // 验证当前订单是否预付过，如果订单已预付，则不可以选择已付款的发票
        QueryWrapper<FiSupplierPrepayment> fiSupplierPrepaymentQueryWrapper = new QueryWrapper<>();
        fiSupplierPrepaymentQueryWrapper.eq("CLIENT", user.getClient());
        fiSupplierPrepaymentQueryWrapper.eq("MAT_DN_ID", matDnId);
        List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectList(fiSupplierPrepaymentQueryWrapper);
        // 单据已预付
        if (!CollectionUtils.isEmpty(fiSupplierPrepaymentList)) {
            throw new CustomResultException("单据[" + matDnId + "]已预付，不可以付款");
        }

        /**
         * 1.验证当前单据下的所有发票是否执行过发票付款（发票信息登记表.付款状态 = 1发票付款）
         *   如果存在 报错 “单据:{单据号}的发票已单独付款”
         */
        List<PaymentVatApplications> paymentVatApplicationsList = paymentMapper.selectInvoiceListHasPayment(client, matDnId);
        if (!CollectionUtils.isEmpty(paymentVatApplicationsList)) {
            throw new CustomResultException(MessageFormat.format("单据:[{0}]的发票已单独付款", matDnId));
        }

        /**
         * 2.判断当前单据的已付款金额（GAIA_FI_INVOICE_REGISTRATION_发票登记.sum(单据已付款金额)）是否等于 GAIA_FI_INVOICE_REGISTRATION_发票登记.sum(单据已登记金额)
         *   if 相等 报错 “当前单据已完成付款”
         */
        PayMentInfo payMentInfo = paymentMapper.hasFinishPayment(vo);
        if (ObjectUtils.isEmpty(payMentInfo)) {
            throw new CustomResultException("未查询到该单据");
        }
        if (payMentInfo.getPayment().compareTo(payMentInfo.getRegistration()) == 0) {
            throw new CustomResultException("当前单据已完成付款");
        }

        /**
         * 3.验证单据明细数据本次付款金额是否合法
         *   本次付款金额 + 行已付款金额 > 行登记金额 报错“本次付款金额大于剩余金额”
         */

        // 单据登记明细 判断
        List<FiInvoiceRegistrationDetailed> regDetailList = paymentMapper.getRegDetail(vo);
        billDetailPaymentList.forEach(crrDetail -> {
            FiInvoiceRegistrationDetailed fiInvoiceRegistrationDetailed = regDetailList.stream().filter(item -> {
                boolean a = item.getMatId().equals(crrDetail.getMatId());
                boolean b = item.getMatYear().equals(crrDetail.getMatYear());
                boolean c = item.getMatLineNo().equals(crrDetail.getMatLineNo());
                return a && b && c;
            }).findFirst().get();
            // 本次付款之前 行已付款金额
            BigDecimal paymentAmountOfLine = fiInvoiceRegistrationDetailed.getPaymentAmountOfLine().abs();
            boolean d = paymentAmountOfLine.add(crrDetail.getPaymentAmount().abs()).compareTo(fiInvoiceRegistrationDetailed.getLineRegistrationAmount().abs()) > 0;
            if (d) {
                throw new CustomResultException("付款金额大于剩余金额");
            }
        });

        /**
         * 4.更新发票表、单据发票、单据发票明细数据
         *   4.1更新当前单据 GAIA_FI_INVOICE_REGISTRATION_DETAILED_发票登记明细表数据， 行已付款金额 = 行已付款金额 + 本次付款金额
         *   4.2更新当前单据 GAIA_FI_INVOICE_REGISTRATION_发票登记
         *      根据剩余金额升序，
         *      单据已付款金额 = 单据已付款金额 + 本次付款金额（3.1本次付款明细总和递减）
         *   4.3更新 GAIA_FICO_INVOICE_INFORMATION_REGISTRATION_发票信息登记表数据，付款状态 = 2单据付款， 已付款金额 = 已付款金额 + (4.2(本次付款金额))
         */
        // 更新登记明细
        billDetailPaymentList.forEach(crrDetail -> {
            FiInvoiceRegistrationDetailed fiInvoiceRegistrationDetailed = regDetailList.stream().filter(item -> {
                boolean a = item.getMatId().equals(crrDetail.getMatId());
                boolean b = item.getMatYear().equals(crrDetail.getMatYear());
                boolean c = item.getMatLineNo().equals(crrDetail.getMatLineNo());
                return a && b && c;
            }).findFirst().get();
            // 本次付款之前 行已付款金额
            BigDecimal paymentAmountOfLine = fiInvoiceRegistrationDetailed.getPaymentAmountOfLine().abs();
            // 本次付款之后 行已付款金额
            BigDecimal paymentAmount = paymentAmountOfLine.add(crrDetail.getPaymentAmount()).abs();
            if ("GD".equals(type) || "CJ".equals(type)) {
                paymentAmount = paymentAmount.abs().multiply(Constants.MINUS_ONE);
            }
            fiInvoiceRegistrationDetailedService.update(new UpdateWrapper<FiInvoiceRegistrationDetailed>()
                    .set("PAYMENT_AMOUNT_OF_LINE", paymentAmount)
                    .eq("CLIENT", client)
                    .eq("BUSINESS_TYPE", type)
                    .eq("MAT_DN_ID", matDnId)
                    .eq("MAT_ID", crrDetail.getMatId())
                    .eq("MAT_YEAR", crrDetail.getMatYear())
                    .eq("MAT_LINE_NO", crrDetail.getMatLineNo()));
        });
        // 更新登记主表
        // 本次付款金额（总）
        BigDecimal currentPaymentAmountTotal = billDetailPaymentList.stream()
                .map(BillDetailPaymentVO.BillDetailPayment::getPaymentAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // 本次发票付款情况列表（付款未完成的条目，剩余可付金额正序）
        List<RegPaymentSurplus> regPaymentSurplusList = paymentMapper.getRegPaymentInfo(vo);
        // 对应发票付款金额（发票号，付款金额）
        List<RegPayment> regPaymentList = new ArrayList<>();
        // 付款
        BigDecimal paymentTotal = BigDecimal.ZERO;
        for (int i = 0; i < regPaymentSurplusList.size(); i++) {
            RegPaymentSurplus regPaymentSurplus = regPaymentSurplusList.get(i);
            // 累加发票剩余金额
            paymentTotal = paymentTotal.add(regPaymentSurplus.getSurplus());
            if (paymentTotal.compareTo(currentPaymentAmountTotal) > 0) {
                // 最后一张会使用到的发票 超过部分
                BigDecimal outstrip = paymentTotal.abs().subtract(currentPaymentAmountTotal.abs());
                // 使用金额
                BigDecimal invoiceCurrentToPayment = regPaymentSurplus.getSurplus().abs().subtract(outstrip.abs());
                regPaymentList.add(new RegPayment(regPaymentSurplus.getInvoiceNum(), invoiceCurrentToPayment));
                if (invoiceCurrentToPayment.compareTo(BigDecimal.ZERO) > 0) {
                    // 该发票已经付款的金额
                    BigDecimal invoiceHasPaymented = regPaymentSurplus.getPaymentAmountOfDocuments().abs();
                    // 本次付款后 该发票的已付款金额(之前+本次)
                    BigDecimal invoicePayment = invoiceHasPaymented.add(invoiceCurrentToPayment);
                    // 如果退厂 则金额为负数
                    if ("GD".equals(type) || "CJ".equals(type)) {
                        invoicePayment = invoicePayment.abs().multiply(Constants.MINUS_ONE);
                    }
                    fiInvoiceRegistrationService.update(new UpdateWrapper<FiInvoiceRegistration>()
                            .set("PAYMENT_AMOUNT_OF_DOCUMENTS", invoicePayment)
                            .eq("CLIENT", client)
                            .eq("BUSINESS_TYPE", type)
                            .eq("MAT_DN_ID", matDnId)
                            .eq("INVOICE_NUM", regPaymentSurplus.getInvoiceNum()));
                }
                break;
            }
            // 本次登记操作后发票的已付款金额
            BigDecimal invoicePayment = regPaymentSurplusList.get(i).getAmountOfDocumentRegistration().abs();
            if ("GD".equals(type) || "CJ".equals(type)) {
                invoicePayment = invoicePayment.abs().multiply(Constants.MINUS_ONE);
            }
            regPaymentList.add(new RegPayment(regPaymentSurplus.getInvoiceNum(), invoicePayment));
            // 保存发票登记金额数据
            fiInvoiceRegistrationService.update(new UpdateWrapper<FiInvoiceRegistration>()
                    .set("PAYMENT_AMOUNT_OF_DOCUMENTS", invoicePayment)
                    .eq("CLIENT", client)
                    .eq("BUSINESS_TYPE", type)
                    .eq("MAT_DN_ID", matDnId)
                    .eq("INVOICE_NUM", regPaymentSurplus.getInvoiceNum()));
        }

        // 更新发票信息表数据
        // 一定会用到的发票 发票号列表
        List<String> invoiceNumList = regPaymentList.stream().map(RegPayment::getInvoiceNum).collect(Collectors.toList());
        // 一定会用到的发票列表
        List<FicoInvoiceInformationRegistration> invoiceList = ficoInvoiceInformationRegistrationService.list(new QueryWrapper<FicoInvoiceInformationRegistration>().eq("CLIENT", client).in("INVOICE_NUM", invoiceNumList));
        regPaymentList.forEach(item -> {
            FicoInvoiceInformationRegistration ficoInvoiceInformationRegistration = invoiceList.stream().filter(invoice -> invoice.getInvoiceNum().equals(item.getInvoiceNum())).findFirst().get();
            // 本次付款之前 发票已经付款的金额
            BigDecimal invoicePaidAmount = ficoInvoiceInformationRegistration.getInvoicePaidAmount();
            // 本次付款之后 发票已经付款的金额
            BigDecimal invoiceInfoItemPayment = invoicePaidAmount.abs().add(item.getPaymentAmount().abs());
            if ("GD".equals(type) || "CJ".equals(type)) {
                invoiceInfoItemPayment = invoiceInfoItemPayment.abs().multiply(Constants.MINUS_ONE);
            }
            ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                    // 发票已经付款金额
                    .set("INVOICE_PAID_AMOUNT", invoiceInfoItemPayment)
                    // 付款状态
                    .set("INVOICE_PAYMENT_STATUS", "2")
                    .eq("CLIENT", client)
                    .eq("INVOICE_NUM", item.getInvoiceNum()));
        });

        /**
         * 5.生成供应商单据付款申请数据
         *   付款单号 = DJ+年月日（8位）+ 4位流水
         *   for(3.1(原行已付款金额 != 原行登记金额))
         *   行号 = 索引
         *   已付款金额 = 3.1(原行已付款金额)
         *   本次付款金额 = 3.1(原行登记金额 - 原行已付款金额)
         *   申请状态 = 1, 审批状态 = -1
         */
        List<FiInvoiceRegistration> invoiceRegistrations = fiInvoiceRegistrationService.list(new QueryWrapper<FiInvoiceRegistration>()
                .eq("CLIENT", user.getClient())
                .eq("BUSINESS_TYPE", vo.getType())
                .eq("MAT_DN_ID", vo.getMatDnId()));
        // 供应商
        String wmGysBh = invoiceRegistrations.get(0).getPoSupplierId();
        // 地点
        String proSite = invoiceRegistrations.get(0).getSiteCode();
        // 付款单号
        String paymentOrderNo = "DJ" + getPaymentOrderNoByDB(client);

        List<FiInvoiceRegistrationDetailed> invoiceRegDetailList = fiInvoiceRegistrationDetailedService.list(new QueryWrapper<FiInvoiceRegistrationDetailed>()
                .eq("CLIENT", client)
                .eq("BUSINESS_TYPE", type)
                .eq("MAT_DN_ID", matDnId));
        List<PaymentDocumentNoApplications> paymentDocumentNoApplicationsList = new ArrayList<>();
        for (int i = 0; i < billDetailPaymentList.size(); i++) {
            BillDetailPaymentVO.BillDetailPayment billDetailPayment = billDetailPaymentList.get(i);

            PaymentDocumentNoApplications paymentDocumentNoApplications = new PaymentDocumentNoApplications();
            paymentDocumentNoApplicationsList.add(paymentDocumentNoApplications);
            // 加盟商
            paymentDocumentNoApplications.setClient(client);
            // 付款单号
            paymentDocumentNoApplications.setPaymentOrderNo(paymentOrderNo);
            // 行号
            paymentDocumentNoApplications.setLineNo(String.valueOf(i + 1));
            // 付款单据日期
            paymentDocumentNoApplications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 业务单号
            paymentDocumentNoApplications.setMatDnId(matDnId);
            // 商品编码
            paymentDocumentNoApplications.setSoProCode(billDetailPayment.getProCode());
            // 供应商编码
            paymentDocumentNoApplications.setSupCode(wmGysBh);
            // 地点
            paymentDocumentNoApplications.setProSite(proSite);
            FiInvoiceRegistrationDetailed fiInvoiceRegistrationDetailed = invoiceRegDetailList.stream().filter(item -> {
                return item.getMatId().equals(billDetailPayment.getMatId())
                        && item.getMatYear().equals(billDetailPayment.getMatYear())
                        && item.getMatLineNo().equals(billDetailPayment.getMatLineNo());
            }).findFirst().get();
            BigDecimal hasPayment = fiInvoiceRegistrationDetailed.getPaymentAmountOfLine().abs();
            BigDecimal invoicePaidAmount;
            if ("GD".equals(type) || "CJ".equals(type)) {
                invoicePaidAmount = hasPayment.abs().subtract(billDetailPayment.getPaymentAmount().abs()).multiply(Constants.MINUS_ONE);
            } else {
                invoicePaidAmount = hasPayment.abs().subtract(billDetailPayment.getPaymentAmount().abs());
            }
            // 已付款金额
            paymentDocumentNoApplications.setInvoicePaidAmount(invoicePaidAmount);
            // 本次付款金额
            BigDecimal paymentAmount = billDetailPayment.getPaymentAmount();
            if ("GD".equals(type) || "CJ".equals(type)) {
                paymentAmount = paymentAmount.abs().multiply(Constants.MINUS_ONE);
            }
            paymentDocumentNoApplications.setInvoiceAmountOfThisPayment(paymentAmount);
            // 工作流编码
            int num = (int) ((Math.random() * 4 + 1) * 100000);
            String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(num), 6, "0");
            // 物料凭证号
            paymentDocumentNoApplications.setMatId(billDetailPayment.getMatId());
            // matYear
            paymentDocumentNoApplications.setMatYear(billDetailPayment.getMatYear());
            // matLineNo
            paymentDocumentNoApplications.setMatLineNo(billDetailPayment.getMatLineNo());
            // 业务类型
            paymentDocumentNoApplications.setBusinessType(billDetailPayment.getBusinessType());
        }

        BigDecimal thisPaymentTotal = paymentDocumentNoApplicationsList.stream().map(PaymentDocumentNoApplications::getInvoiceAmountOfThisPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
        PaymentApplications applications = new PaymentApplications();
        {
            // 加盟商
            applications.setClient(client);
            // 供应商
            applications.setSupCode(wmGysBh);
            // 地点
            applications.setProSite(proSite);
            // 申请日期
            applications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 本次付款金额
            applications.setInvoiceAmountOfThisPayment(thisPaymentTotal);
            // 1单据付款，2发票付款
            applications.setType(1);
            // 0-已申请；1-未申请
            applications.setApplicationStatus(1);
            // '-1未申请，0-审批中，1-已审批，2-已废弃
            applications.setApprovalStatus(-1);
            // 付款单号
            applications.setPaymentOrderNo(paymentOrderNo);
            // 工作流编码
            applications.setApprovalFlowNo("");
        }
        paymentApplicationsMapper.insert(applications);
        // 主表id
        paymentDocumentNoApplicationsList.forEach(item -> item.setApplicationsId(applications.getId()));
        paymentDocumentNoApplicationsService.saveBatch(paymentDocumentNoApplicationsList);

    }

    /**
     * 发票付款
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void invoicePayment(List<InvoicePaymentVO> list) {
        TokenUser user = commonService.getLoginInfo();
        // 加盟商
        String client = user.getClient();
        // 数据校验
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("发票列表不能为空");
        }
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getInvoiceNum())) {
                throw new CustomResultException("发票号不能为空");
            }
            if (ObjectUtils.isEmpty(item.getPaymentAmount()) || item.getPaymentAmount().compareTo(BigDecimal.ZERO) == 0) {
                throw new CustomResultException("本次付款金额不能为空");
            }
            // 核销验证
            QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
            billInvoiceEndQueryWrapper.eq("CLIENT", client);
            billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 2);
            billInvoiceEndQueryWrapper.eq("GBIE_NUM", item.getInvoiceNum());
            List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
            if (!CollectionUtils.isEmpty(billInvoiceEndList)) {
                throw new CustomResultException("发票[" + item.getInvoiceNum() + "]已核销");
            }
        });

        // 发票号列表
        List<String> invoiceNumList = list.stream().map(InvoicePaymentVO::getInvoiceNum).collect(Collectors.toList());
        Map<String, BigDecimal> invoiceMap = list.stream().collect(Collectors.toMap(InvoicePaymentVO::getInvoiceNum, InvoicePaymentVO::getPaymentAmount));
        /**
         * 1.验证当前发票所关联的单据是否做过付款记录
         *     如关联单据已有付款记录 报错 “发票{发票号}关联的单据已申请付款”
         */
        String invoiceNumStr = invoiceNumList.stream().collect(Collectors.joining(","));
        List<PaymentDocumentNoApplications> billListHasPayment = paymentMapper.selectBillListHasPayment(invoiceNumStr, client);
        if (!CollectionUtils.isEmpty(billListHasPayment)) {
            throw new CustomResultException("发票关联的单据已申请付款");
        }
        // 单据预付款验证
        List<FiSupplierPrepayment> fiSupplierPrepaymentList = fiSupplierPrepaymentMapper.selectFiPrePayment(invoiceNumStr, client);
        if (!CollectionUtils.isEmpty(fiSupplierPrepaymentList)) {
            throw new CustomResultException("发票关联的单据已申请预付款");
        }

        /**
         * 2.验证当前发票剩余金额与本次付款金额大小
         *     本次付款金额 + 已付款金额 > 发票确认金额  报错 “发票{发票号}本次付款金额大于剩余金额”
         */
        List<FicoInvoiceInformationRegistration> invoiceList = ficoInvoiceInformationRegistrationService.list(new QueryWrapper<FicoInvoiceInformationRegistration>()
                .eq("CLIENT", client)
                .in("INVOICE_NUM", invoiceNumList));
        List<String> invoiceMoreConfirmList = invoiceList.stream().filter(item -> {
            // 已经付款金额
            BigDecimal invoicePaidAmount = item.getInvoicePaidAmount().abs();
            // 发票总额
            BigDecimal invoiceTotalInvoiceAmount = item.getInvoiceTotalInvoiceAmount().abs();
            // 本次付款金额
            BigDecimal currentPayment = invoiceMap.get(item.getInvoiceNum()).abs();
            // 本次付款金额 + 已付款金额 > 发票确认金额
            return invoicePaidAmount.add(currentPayment).compareTo(invoiceTotalInvoiceAmount) > 0;
        }).map(FicoInvoiceInformationRegistration::getInvoiceNum).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(invoiceMoreConfirmList)) {
            String invoiceListStr = invoiceMoreConfirmList.stream().collect(Collectors.joining(","));
            throw new CustomResultException(MessageFormat.format("发票[{0}]本次付款金额大于剩余金额", invoiceListStr));
        }
        /**
         * 3.更新发票表数据
         *   3.1更新 GAIA_FICO_INVOICE_INFORMATION_REGISTRATION_发票信息登记表数据，付款状态 = 1发票付款， 已付款金额 = 已付款金额 + 本次付款金额
         *
         */
        // 发票数据
        invoiceList.forEach(invoice -> {
            // 已经付款金额
            BigDecimal invoicePaidAmount = invoice.getInvoicePaidAmount().abs();
            // 本次付款金额
            BigDecimal currentPayment = invoiceMap.get(invoice.getInvoiceNum()).abs();
            // 总的已付款金额
            BigDecimal paymentAmount = invoicePaidAmount.add(currentPayment).abs();
            // 确定金额的正负
            BigDecimal invoiceTotalInvoiceAmount = invoice.getInvoiceTotalInvoiceAmount();
            if (invoiceTotalInvoiceAmount.compareTo(BigDecimal.ZERO) < 0) {
                paymentAmount = paymentAmount.abs().multiply(Constants.MINUS_ONE);
            }
            ficoInvoiceInformationRegistrationService.update(new UpdateWrapper<FicoInvoiceInformationRegistration>()
                    // 已付款金额
                    .set("INVOICE_PAID_AMOUNT", paymentAmount)
                    // 付款状态
                    .set("INVOICE_PAYMENT_STATUS", "1")
                    // 加盟商
                    .eq("CLIENT", invoice.getClient())
                    // 发票号
                    .eq("INVOICE_NUM", invoice.getInvoiceNum()));
        });

        /**
         * 4.生成发票付款申请数据
         *   付款单号 = FP+年月日（8位）+4位流水
         *   for(参数)
         *   行号 = 索引
         *   已付款金额 = 3.1(原行已付款金额)
         *   本次付款金额 = 3.1(原行登记金额 - 原行已付款金额)
         *   申请状态 = 1, 审批状态 = -1
         */

        List<PaymentVatApplications> paymentVatApplicationsList = new ArrayList<>();
        // 分组
        Map<FicoInvoiceInformationKey, List<FicoInvoiceInformationRegistration>> collect = invoiceList.stream()
                .collect(Collectors.groupingBy(item -> new FicoInvoiceInformationKey(item.getSiteCode(), item.getSupCode())));
        collect.forEach((key, invoiceGroupList) -> {

            List<String> numList = invoiceGroupList.stream().map(FicoInvoiceInformationRegistration::getInvoiceNum).collect(Collectors.toList());
            // 本次付款总额
            BigDecimal invoicePaymentTotal = list.stream().filter(Objects::nonNull).filter(item -> numList.contains(item.getInvoiceNum()))
                    .map(InvoicePaymentVO::getPaymentAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            // 付款单号
            String paymentOrderNoVat = "FP" + getPaymentOrderNoVat(client);
            // 付款申请主表
            PaymentApplications applications = new PaymentApplications();
            applications.setClient(client);
            applications.setSupCode(key.getSupCode());
            applications.setProSite(key.getSiteCode());
            // 申请日期
            applications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
            // 本次付款金额
            applications.setInvoiceAmountOfThisPayment(invoicePaymentTotal);
            // 1单据付款，2发票付款
            applications.setType(2);
            // 0-已申请；1-未申请
            applications.setApplicationStatus(1);
            // '-1未申请，0-审批中，1-已审批，2-已废弃
            applications.setApprovalStatus(-1);
            // 付款单号
            applications.setPaymentOrderNo(paymentOrderNoVat);
            // 工作流编码 (初始化置为空字符串)
            applications.setApprovalFlowNo("");
            paymentApplicationsMapper.insert(applications);
            // 发票付款申请表
            for (FicoInvoiceInformationRegistration ficoInvoiceInformationRegistration : invoiceGroupList) {
                PaymentVatApplications paymentVatApplications = new PaymentVatApplications();
                paymentVatApplicationsList.add(paymentVatApplications);
                paymentVatApplications.setClient(client);
                paymentVatApplications.setPaymentOrderNo(paymentOrderNoVat);
                paymentVatApplications.setLineNo(String.valueOf(paymentVatApplicationsList.size()));
                paymentVatApplications.setInvoiceNum(ficoInvoiceInformationRegistration.getInvoiceNum());
                paymentVatApplications.setPaymentOrderDate(DateUtils.getCurrentDateStrYYMMDD());
                paymentVatApplications.setSupCode(ficoInvoiceInformationRegistration.getSupCode());
                paymentVatApplications.setInvoicePaidAmount(ficoInvoiceInformationRegistration.getInvoicePaidAmount());
                paymentVatApplications.setInvoiceAmountOfThisPayment(invoiceMap.get(ficoInvoiceInformationRegistration.getInvoiceNum()));
                paymentVatApplications.setProSite(ficoInvoiceInformationRegistration.getSiteCode());
                paymentVatApplications.setApplicationsId(applications.getId());
            }
        });
        paymentVatApplicationsService.saveBatch(paymentVatApplicationsList);

    }

    /**
     * 付款记录
     *
     * @param paymentRecordDto
     * @return
     */
    @Override
    public Result paymentRecord(PaymentRecordDto paymentRecordDto) {
        if (StringUtils.isNotBlank(paymentRecordDto.getSite()) && StringUtils.isNotBlank(paymentRecordDto.getStoCode())) {
            throw new CustomResultException("[采购地点]和[单体店]只能选择其中一个");
        }
        TokenUser user = commonService.getLoginInfo();
        paymentRecordDto.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(paymentRecordDto.getPageNum(), paymentRecordDto.getPageSize());
        IPage<PaymentRecordDetail> iPage = paymentApplicationsMapper.paymentRecord(page, paymentRecordDto);
        PaymentRecordVO paymentRecordVO = new PaymentRecordVO();
        paymentRecordVO.setPaymentRecordDetail(iPage);
        // 合计
        BigDecimal paymentRecordTotal = paymentApplicationsMapper.paymentRecordTotal(paymentRecordDto);
        paymentRecordVO.setPaymentRecordTotal(paymentRecordTotal);
        return ResultUtil.success(paymentRecordVO);
    }

    /**
     * 付款单号获取DJ
     */
    private String getPaymentOrderNoByDB(String client) {
        String clientMaxOrderNo = paymentMapper.getClientMaxOrderNo(client);
        if (ObjectUtils.isEmpty(clientMaxOrderNo)) {
            // 付款单号 = DJ+年月日（8位）+ 4位流水(/加盟商/天)
            clientMaxOrderNo = DateUtils.getCurrentDateStrYYMMDD() + "0001";
        } else {
            clientMaxOrderNo = new BigDecimal(clientMaxOrderNo).add(BigDecimal.ONE).toPlainString();
        }
        return clientMaxOrderNo;
    }

    /**
     * 付款单号获取FP
     */
    private String getPaymentOrderNoVat(String client) {
        String clientMaxOrderNo = paymentMapper.getClientMaxOrderNoVat(client);
        if (ObjectUtils.isEmpty(clientMaxOrderNo)) {
            // 付款单号 = DJ+年月日（8位）+ 4位流水(/加盟商/天)
            clientMaxOrderNo = DateUtils.getCurrentDateStrYYMMDD() + "0001";
        } else {
            clientMaxOrderNo = new BigDecimal(clientMaxOrderNo).add(BigDecimal.ONE).toPlainString();
        }
        return clientMaxOrderNo;
    }

    /**
     * 发票查询
     */
    @Override
    public List<SelectInvoiceDTO> selectInvoice(SelectInvoiceVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
//        Page page = new Page(vo.getPageNum(), vo.getPageSize());
        Page page = new Page(-1, -1);
        IPage<SelectInvoiceDTO> selectInvoiceDTOIPage = paymentMapper.selectInvoice(page, vo);
        List<SelectInvoiceDTO> list = new ArrayList<>();
        selectInvoiceDTOIPage.getRecords().stream().forEach(item -> {
            BigDecimal chargeAmount = item.getInvoiceTotalInvoiceAmount().abs().subtract(item.getInvoicePaidAmount().abs());
            BigDecimal invoiceTotalInvoiceAmount = item.getInvoiceTotalInvoiceAmount();
            if (invoiceTotalInvoiceAmount.compareTo(BigDecimal.ZERO) < 0) {
                chargeAmount = chargeAmount.abs().multiply(Constants.MINUS_ONE);
            }
            item.setChargeAmount(chargeAmount);
            // 本次付款金额 默认等于剩余金额
            item.setPaymentAmount(chargeAmount);
            // 单据
            String billNum = item.getBillNum();
            if (StringUtils.isBlank(billNum)) {
                list.add(item);
            } else {
                List<Integer> payList = paymentMapper.selectCountPayBillCountByBillNum(user.getClient(), billNum);
                if (payList.get(0).intValue() == 0 && payList.get(1).intValue() == 0) {
                    list.add(item);
                }
            }
        });
        return list;
    }

    /**
     * 付款申请
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String paymentApproval(PaymentApproval paymentApproval) {
        if (paymentApproval == null || CollectionUtils.isEmpty(paymentApproval.getList())) {
            throw new CustomResultException("请选择付款单");
        }
        List<PaymentRecordDetail> list = paymentApproval.getList();
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<PaymentApplications> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", user.getClient());
        List<Integer> ids = list.stream().map(PaymentRecordDetail::getId).collect(Collectors.toList());
        queryWrapper.in("ID", ids);
        List<PaymentApplications> result = paymentApplicationsMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(result)) {
            throw new CustomResultException("未查询到付款记录");
        }
        if (list.size() != result.size()) {
            throw new CustomResultException("付款记录查询有误，请刷新后再试");
        }
        // 生成审批工作流编号
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        Map<String, Object> param = new HashMap<>();
        // token
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        param.put("token", token);
        param.put("wfDefineCode", "GAIA_WF_032");  // 类型
        param.put("wfOrder", flowNo);
        param.put("wfSite",
                StringUtils.isNotBlank(paymentApproval.getSite()) ? paymentApproval.getSite() :
                        StringUtils.isNotBlank(paymentApproval.getStoCode()) ? paymentApproval.getStoCode() : "-1"
        );   // 人员门店
        List<Map<String, Object>> paymentApplicationsList = new ArrayList<>();
        Map<String, PaymentRecordDetail> aplicationMap = new HashMap<>();
        for (PaymentApplications paymentApplications : result) {
            {
                List<PaymentRecordDetail> row = list.stream().filter(item -> item.getId().intValue() == paymentApplications.getId().intValue()).collect(Collectors.toList());
                // 验证当前付款单状态
                if (paymentApplications.getApprovalStatus() != null && paymentApplications.getApprovalStatus() != -1) {
                    throw new CustomResultException(MessageFormat.format("付款单{0}已发起申请付款", paymentApplications.getPaymentOrderNo()));
                }
                paymentApplications.setApplicationStatus(0);
                paymentApplications.setApprovalStatus(0);
                paymentApplications.setApprovalFlowNo(flowNo);
                if (!CollectionUtils.isEmpty(row)) {
                    paymentApplications.setGpaRemarks(row.get(0).getGpaRemarks());
                }
                paymentApplicationsMapper.updateById(paymentApplications);
            }
            {
                // 当前供应商申请记录
                PaymentRecordDetail entity = aplicationMap.get(paymentApplications.getSupCode());
                if (entity == null) {
                    entity = new PaymentRecordDetail();
                    QueryWrapper<SupplierBusiness> supplierBusinessQueryWrapper = new QueryWrapper();
                    supplierBusinessQueryWrapper.eq("CLIENT", user.getClient());
                    supplierBusinessQueryWrapper.eq("SUP_SITE", paymentApplications.getProSite());
                    supplierBusinessQueryWrapper.eq("SUP_SELF_CODE", paymentApplications.getSupCode());
                    SupplierBusiness supplierBusiness = supplierBusinessMapper.selectOne(supplierBusinessQueryWrapper);
                    if (supplierBusiness != null) {
                        // 供应商名称
                        entity.setSupName(supplierBusiness.getSupName());
                        // 开户行名称
                        entity.setSupBankName(supplierBusiness.getSupBankName());
                        // 开户行帐号
                        entity.setSupBankAccount(supplierBusiness.getSupBankAccount());
                    }
                    List<PaymentRecordDetail> row = list.stream().filter(item -> item.getSupCode().equals(paymentApplications.getSupCode())).collect(Collectors.toList());
                    if (!CollectionUtils.isEmpty(row)) {
                        entity.setGpaRemarks(row.get(0).getGpaRemarks());
                    }
                }
                entity.setSupCode(paymentApplications.getSupCode());
                entity.setInvoiceAmountOfThisPayment((entity.getInvoiceAmountOfThisPayment() == null ? BigDecimal.ZERO : entity.getInvoiceAmountOfThisPayment())
                        .add(paymentApplications.getInvoiceAmountOfThisPayment() == null ? BigDecimal.ZERO : paymentApplications.getInvoiceAmountOfThisPayment()).
                                setScale(2, BigDecimal.ROUND_HALF_UP));
                aplicationMap.put(paymentApplications.getSupCode(), entity);
            }
        }
        // 申请明细
        int index = 0;
        QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
        userDataQueryWrapper.eq("CLIENT", user.getClient());
        userDataQueryWrapper.eq("USER_ID", user.getUserId());
        UserData userData = userDataMapper.selectOne(userDataQueryWrapper);
        String userName = "";
        if (userData != null) {
            userName = userData.getUserNam();
        }
        for (String key : aplicationMap.keySet()) {
            PaymentRecordDetail entity = aplicationMap.get(key);
            if (index == 0) {
                param.put("wfTitle", entity.getSupCode() + "-" + entity.getSupName() + "(" + String.format("%.2f", entity.getInvoiceAmountOfThisPayment()) + "元)");
                param.put("wfDescription", entity.getGpaRemarks());  // 描述
            }
            Map<String, Object> map = new HashMap<>();
            // 序号
            index++;
            map.put("index", index);
            // 加盟商
            map.put("clent", user.getClient());
            // 供应商编码
            map.put("supCode", entity.getSupCode());
            // 申请付款金额
            map.put("applicationAmount", String.format("%.2f", entity.getInvoiceAmountOfThisPayment()));
            // 申请人
            map.put("applicant", user.getUserId());
            // 申请日期
            map.put("dateOfApplication", DateUtils.getCurrentDateStrYYMMDD());
            // 申请时间
            map.put("applicationTime", DateUtils.getCurrentTimeStrHHMMSS());
            // 供应商名称
            map.put("supName", entity.getSupName());
            // 申请人姓名
            map.put("applicantUserName", userName);
            // 开户行名称
            map.put("supBankName", entity.getSupBankName());
            // 开户行帐号
            map.put("supBankAccount", entity.getSupBankAccount());
            // 备注
            map.put("gpaRemarks", entity.getGpaRemarks());
            paymentApplicationsList.add(map);
        }
        param.put("wfDetail", paymentApplicationsList);
        // 工作流调用
        log.info("供应商付款流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        JsonResult jsonResult = authFeign.createWorkflow(param);
        log.info("供应商付款流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        if (jsonResult.getCode().intValue() != 0) {
            throw new CustomResultException(jsonResult.getMessage());
        } else {
            String reulst = "";
            if (jsonResult.getData() != null) {
                reulst = jsonResult.getData().toString();
            }
            return reulst;
        }
    }

    /**
     * 付款单细
     *
     * @param id
     * @return
     */
    @Override
    public Result paymentRecordDetails(Integer id) {
        QueryWrapper<PaymentApplications> paymentApplicationsQueryWrapper = new QueryWrapper<>();
        paymentApplicationsQueryWrapper.eq("ID", id);
        PaymentApplications paymentApplications = paymentApplicationsMapper.selectOne(paymentApplicationsQueryWrapper);
        if (paymentApplications == null) {
            throw new CustomResultException("付款单不存在");
        }
        // 单据付款
        if (CommonEnum.paymentApplicationsType.BILL.getCode().intValue() == paymentApplications.getType()) {
            List<SelectBillDTO> list = paymentApplicationsMapper.selectPayBillItems(paymentApplications.getClient(), paymentApplications.getId());
            return ResultUtil.success(list);
        }

        // 发票付款
        if (CommonEnum.paymentApplicationsType.INVOICE.getCode().intValue() == paymentApplications.getType()) {
            List<SelectInvoiceDTO> list = paymentApplicationsMapper.selectPayInvoiceItems(paymentApplications.getClient(), paymentApplications.getId());
            return ResultUtil.success(list);
        }

        // 单据预付
        if (CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode().intValue() == paymentApplications.getType()) {
            List<SelectWarehousingDTO> list = paymentApplicationsMapper.selectPayPrepaidBillItems(paymentApplications.getClient(), paymentApplications.getId());
            return ResultUtil.success(list);
        }
        return ResultUtil.success();
    }

    /**
     * 供应商发票登记付款查询
     *
     * @param selectInvoiceVO
     * @return
     */
    @Override
    public Result selectInvoicePayRecord(SelectInvoiceVO selectInvoiceVO) {
        // 地点和单体店只要选择一个， 另一个赋特殊值
        if (!((StringUtils.isBlank(selectInvoiceVO.getSite()) && StringUtils.isBlank(selectInvoiceVO.getStoCode()))
                || (StringUtils.isNotBlank(selectInvoiceVO.getSite()) && StringUtils.isNotBlank(selectInvoiceVO.getStoCode())))) {
            if (StringUtils.isBlank(selectInvoiceVO.getSite())) {
                selectInvoiceVO.setSite("-1");
            } else {
                selectInvoiceVO.setStoCode("-1");
            }
        }
        TokenUser user = commonService.getLoginInfo();
        selectInvoiceVO.setClient(user.getClient());
        Page page = new Page(selectInvoiceVO.getPageNum(), selectInvoiceVO.getPageSize());
        IPage<SelectInvoiceDTO> selectInvoiceDTOIPage = paymentMapper.selectInvoicePayRecord(page, selectInvoiceVO);
        return ResultUtil.success(selectInvoiceDTOIPage);
    }

    /**
     * 导出
     *
     * @param selectInvoiceVO
     * @return
     */
    @Override
    public Result selectInvoicePayRecordExport(SelectInvoiceVO selectInvoiceVO) throws IOException {
        TokenUser user = commonService.getLoginInfo();
        selectInvoiceVO.setClient(user.getClient());
        Page page = new Page(-1, -1);
        IPage<SelectInvoiceDTO> selectInvoiceDTOIPage = paymentMapper.selectInvoicePayRecord(page, selectInvoiceVO);
        List<List<Object>> dataList = new ArrayList<>();
        if (selectInvoiceDTOIPage != null && !CollectionUtils.isEmpty(selectInvoiceDTOIPage.getRecords())) {
            // 组建导出数据
            selectInvoiceDTOIPage.getRecords().forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 序号
                data.add(String.valueOf(index + 1));
                // 发票类型
                if (StringUtils.isNotBlank(item.getInvoiceType())) {
                    CommonEnum.Invoice_type invoice_type = CommonEnum.Invoice_type.getByCode(item.getInvoiceType());
                    if (invoice_type != null) {
                        data.add(invoice_type.getName());
                    } else {
                        data.add("");
                    }
                } else {
                    data.add("");
                }
                // 发票号码
                data.add(item.getInvoiceNum());
                // 地点
                data.add(StringUtils.parse("{}-{}", item.getSiteCode(), item.getStoName()));
                // 供应商
                data.add(StringUtils.parse("{}-{}", item.getSupCode(), item.getSupName()));
                // 发票日期
                if (StringUtils.isBlank(item.getInvoiceDate())) {
                    data.add("");
                } else {
                    StringBuffer stringBuffer = new StringBuffer(item.getInvoiceDate());
                    stringBuffer.insert(6, "/");
                    stringBuffer.insert(4, "/");
                    data.add(stringBuffer.toString());
                }
                // 发票金额
                data.add(item.getInvoiceTotalInvoiceAmount());
                // 发票已登记金额
                data.add(item.getInvoiceRegisteredAmount());
                // 发票已付款金额
                data.add(item.getInvoicePaidAmount());
                // 折扣
                data.add(item.getInvoiceDiscount());
                // 付款单号
                data.add(item.getPaymentOrderNo());
                // 付款方式
                data.add(item.getPaymenthod());
                // 业务员
                data.add(item.getInvoiceSalesmanName());
                // 去税金额
                data.add(item.getInvoiceAmountExcludingTax());
                // 税额
                data.add(item.getInvoiceTax());
                // 录入日期
                data.add(item.getInvoiceCreationDate());
                // 备注
                data.add(item.getInvoiceRemark());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(invoiceHeadList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("供应商发票登记付款");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 供应商单据登记付款查询
     *
     * @param selectWarehousingVO
     * @return
     */
    @Override
    public Result selectBillPayRecord(SelectWarehousingVO selectWarehousingVO) {
        // 地点和单体店只要选择其中一个，另一个赋特殊值
        if (!((StringUtils.isBlank(selectWarehousingVO.getSite()) && StringUtils.isBlank(selectWarehousingVO.getStoCode()))
                || (StringUtils.isNotBlank(selectWarehousingVO.getSite()) && StringUtils.isNotBlank(selectWarehousingVO.getStoCode())))) {
            if (StringUtils.isBlank(selectWarehousingVO.getSite())) {
                selectWarehousingVO.setSite("-1");
            } else {
                selectWarehousingVO.setStoCode("-1");
            }
        }
        TokenUser user = commonService.getLoginInfo();
        selectWarehousingVO.setClient(user.getClient());
        Page page = new Page(selectWarehousingVO.getPageNum(), selectWarehousingVO.getPageSize());
        IPage<SelectWarehousingDTO> iPage = paymentMapper.selectBillPayRecord(page, selectWarehousingVO);
        iPage.getRecords().forEach(item -> {
            // 如果为出库 则 金额均为 负数
            if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                // 总金额 (来自物料凭证，正数)
                BigDecimal totalAmount = item.getTotalAmount();
                // 剩余可登记金额 (计算得到，type为GD为负数--总额减已登记金额)
                BigDecimal multiply = totalAmount.abs().subtract(item.getRegisteredAmount().abs()).multiply(Constants.MINUS_ONE);
                // 去税金额
                item.setExcludingTaxAmount(item.getExcludingTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 税额
                item.setTaxAmount(item.getTaxAmount().abs().multiply(Constants.MINUS_ONE));
                // 总额
                item.setTotalAmount(item.getTotalAmount().abs().multiply(Constants.MINUS_ONE));
                // 剩余可登记金额
                item.setChargeAmount(multiply);
            }
        });
        return ResultUtil.success(iPage);
    }

    /**
     * 供应商单据登记付款查询导出
     *
     * @param selectWarehousingVO
     * @return
     * @throws IOException
     */
    @Override
    public Result selectBillPayRecordExport(SelectWarehousingVO selectWarehousingVO) throws IOException {
        TokenUser user = commonService.getLoginInfo();
        // 地点和单体店只要选择其中一个，另一个赋特殊值
        if (!((StringUtils.isBlank(selectWarehousingVO.getSite()) && StringUtils.isBlank(selectWarehousingVO.getStoCode()))
                || (StringUtils.isNotBlank(selectWarehousingVO.getSite()) && StringUtils.isNotBlank(selectWarehousingVO.getStoCode())))) {
            if (StringUtils.isBlank(selectWarehousingVO.getSite())) {
                selectWarehousingVO.setSite("-1");
            } else {
                selectWarehousingVO.setStoCode("-1");
            }
        }
        selectWarehousingVO.setClient(user.getClient());
        Page page = new Page(-1, -1);
        IPage<SelectWarehousingDTO> iPage = paymentMapper.selectBillPayRecord(page, selectWarehousingVO);
        List<List<Object>> dataList = new ArrayList<>();
        if (iPage != null && !CollectionUtils.isEmpty(iPage.getRecords())) {
            iPage.getRecords().forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                List<Object> data = new ArrayList<>();
                // 序号
                data.add(index + 1);
                // 业务类型
                data.add(item.getType() + "-" + item.getTypeName());
                // 供应商编码
                data.add(item.getSupSelfCode() + "-" + item.getSupSelfName());
                // 单据号
                data.add(item.getMatDnId());
                // 单据日期
                if (StringUtils.isBlank(item.getXgrq())) {
                    data.add("");
                } else {
                    StringBuffer stringBuffer = new StringBuffer(item.getXgrq());
                    stringBuffer.insert(6, "/");
                    stringBuffer.insert(4, "/");
                    data.add(stringBuffer.toString());
                }
                // 如果为出库 则 金额均为 负数
                if ("GD".equals(item.getType()) || "CJ".equals(item.getType())) {
                    // 单据总金额
                    BigDecimal totalAmount = item.getTotalAmount();
                    data.add(totalAmount.abs().multiply(Constants.MINUS_ONE));
                    // 单据已登记金额
                    BigDecimal registeredAmount = item.getRegisteredAmount();
                    data.add(registeredAmount.abs().multiply(Constants.MINUS_ONE));
                } else {
                    // 单据总金额
                    data.add(item.getTotalAmount());
                    // 单据已登记金额
                    data.add(item.getRegisteredAmount());
                }
                // 发票号码
                data.add(item.getInvoiceNum());
                // 单据已付款金额
                data.add(item.getInvoiceAmountOfThisPayment());
                // 付款单号
                data.add(item.getPaymentOrderNo());
                // 付款方式
                data.add(item.getPaymenthod());
                // 备注
                data.add(item.getRemark());
                dataList.add(data);
            }));
        }
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(billHeadList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("供应商单据登记付款");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 单据核销
     *
     * @param gbieNum
     * @param gbieBillType
     */
    @Override
    public void billEnd(String gbieNum, String gbieBillType) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
        billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 1);
        billInvoiceEndQueryWrapper.eq("GBIE_NUM", gbieNum);
        billInvoiceEndQueryWrapper.eq("GBIE_BILL_TYPE", gbieBillType);
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
        if (CollectionUtils.isEmpty(billInvoiceEndList)) {
            BillInvoiceEnd billInvoiceEnd = new BillInvoiceEnd();
            // 加盟商
            billInvoiceEnd.setClient(user.getClient());
            // 类型
            billInvoiceEnd.setGbieType(1);
            // 单据、发票号
            billInvoiceEnd.setGbieNum(gbieNum);
            // 单据类型
            billInvoiceEnd.setGbieBillType(gbieBillType);
            // 创建人
            billInvoiceEnd.setGbieCreateUser(user.getUserId());
            // 创建时间
            billInvoiceEnd.setGbieCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            billInvoiceEnd.setGbieCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            billInvoiceEndMapper.insert(billInvoiceEnd);
        }
    }

    /**
     * 发票核销
     *
     * @param gbieNum
     */
    @Override
    public void invoiceEnd(String gbieNum) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
        billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 2);
        billInvoiceEndQueryWrapper.eq("GBIE_NUM", gbieNum);
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper);
        if (CollectionUtils.isEmpty(billInvoiceEndList)) {
            BillInvoiceEnd billInvoiceEnd = new BillInvoiceEnd();
            // 加盟商
            billInvoiceEnd.setClient(user.getClient());
            // 类型
            billInvoiceEnd.setGbieType(2);
            // 单据、发票号
            billInvoiceEnd.setGbieNum(gbieNum);
            // 创建人
            billInvoiceEnd.setGbieCreateUser(user.getUserId());
            // 创建时间
            billInvoiceEnd.setGbieCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            billInvoiceEnd.setGbieCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            billInvoiceEndMapper.insert(billInvoiceEnd);
        }
    }

    /**
     * 导出数据表头
     */
    private String[] billHeadList = {
            "序号",
            "业务类型",
            "供应商编码",
            "单据号",
            "单据日期",
            "单据总金额",
            "单据已登记金额",
            "发票号码",
            "单据已付款金额",
            "付款单号",
            "付款方式",
            "备注",
    };

    /**
     * 导出数据表头
     */
    private String[] invoiceHeadList = {
            "序号",
            "发票类型",
            "发票号码",
            "地点",
            "供应商",
            "发票日期",
            "发票金额",
            "发票已登记金额",
            "发票已付款金额",
            "折扣",
            "付款单号",
            "付款方式",
            "业务员",
            "去税金额",
            "税额",
            "录入日期",
            "备注"
    };


    /**
     * 审批页面_通过工作流编号查询付款明细(结果是两层list)
     *
     * @param flowNo
     * @return
     */
    @Override
    public List<GetPaymentApplyDetails> getPaymentApplyDetails(String flowNo) {
        List<GetPaymentApplyDetails> getPaymentApplyDetails = new ArrayList<>();
        String client = commonService.getLoginInfo().getClient();
        // 单据集合
        List<String> matDnIds = paymentApplicationsMapper.selectMatDnIdByFlowNo(flowNo, client);
        if (CollectionUtils.isEmpty(matDnIds)) {
            return getPaymentApplyDetails;
        }
        // 外层（单据号 以及 单据的信息)
        getPaymentApplyDetails = paymentApplicationsMapper.getPaymentApplyDetails(matDnIds, client);
        // 里层（商品信息）
        if (!CollectionUtils.isEmpty(getPaymentApplyDetails)) {
            List<String> matDnIdList = getPaymentApplyDetails.stream().map(GetPaymentApplyDetails::getMatDnId).distinct().collect(Collectors.toList());
            List<SelectWarehousingDetailsDTO> detailsList = paymentApplicationsMapper.getPaymentApplyDetailsItemList(matDnIdList, client);
            Map<String, List<SelectWarehousingDetailsDTO>> collect = detailsList.stream().collect(Collectors.groupingBy(SelectWarehousingDetailsDTO::getMatDnId));
            getPaymentApplyDetails.forEach(paymentApply -> {
                paymentApply.setItemList(collect.get(paymentApply.getMatDnId()));
            });
        }
        return getPaymentApplyDetails;
    }

    /**
     * 发票批量核销
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void invoiceEndBatch(List<String> invoiceNumList) {
        if (CollectionUtils.isEmpty(invoiceNumList)) {
            throw new CustomResultException("发票列表不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<BillInvoiceEnd> billInvoiceEndQueryWrapper = new QueryWrapper<>();
        billInvoiceEndQueryWrapper.eq("CLIENT", user.getClient());
        billInvoiceEndQueryWrapper.eq("GBIE_TYPE", 2);
        billInvoiceEndQueryWrapper.in("GBIE_NUM", invoiceNumList);
        List<String> invoiceListExist = billInvoiceEndMapper.selectList(billInvoiceEndQueryWrapper).stream().filter(Objects::nonNull)
                .map(BillInvoiceEnd::getGbieNum).collect(Collectors.toList());
        invoiceNumList.stream().filter(StringUtils::isNotBlank).forEach(invoice -> {
            if (!invoiceListExist.contains(invoice)) {
                BillInvoiceEnd billInvoiceEnd = new BillInvoiceEnd();
                // 加盟商
                billInvoiceEnd.setClient(user.getClient());
                // 类型
                billInvoiceEnd.setGbieType(2);
                // 单据、发票号
                billInvoiceEnd.setGbieNum(invoice);
                // 创建人
                billInvoiceEnd.setGbieCreateUser(user.getUserId());
                // 创建时间
                billInvoiceEnd.setGbieCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                // 创建时间
                billInvoiceEnd.setGbieCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                billInvoiceEndMapper.insert(billInvoiceEnd);
            }
        });
    }

    /**
     * 单据批量核销
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void billEndBatch(List<BillEndBatchVO> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("单据不能为空");
        }
        list.stream().filter(Objects::nonNull).peek(item -> {
            if (StringUtils.isBlank(item.getGbieNum())) {
                throw new CustomResultException("单据编号不能为空");
            }
            if (StringUtils.isBlank(item.getGbieBillType())) {
                throw new CustomResultException("单据类型不能为空");
            }
        });
        TokenUser user = commonService.getLoginInfo();
        List<BillInvoiceEnd> billInvoiceEndList = billInvoiceEndMapper.getBillInvoiceEndList(user.getClient(), 1, list);

        List<BillInvoiceEnd> collect = list.stream().filter(Objects::nonNull)
                .filter(item -> {
                    BillInvoiceEnd billInvoiceEnd = billInvoiceEndList.stream().filter(Objects::nonNull)
                            .filter(billExist -> StringUtils.isNotBlank(billExist.getGbieNum()) && StringUtils.isNotBlank(billExist.getGbieBillType())
                                    && billExist.getGbieNum().equals(item.getGbieNum()) && billExist.getGbieBillType().equals(item.getGbieBillType()))
                            .findFirst().orElse(null);
                    return ObjectUtils.isEmpty(billInvoiceEnd);
                }).map(item -> {
                    BillInvoiceEnd billInvoiceEnd = new BillInvoiceEnd();
                    // 加盟商
                    billInvoiceEnd.setClient(user.getClient());
                    // 类型
                    billInvoiceEnd.setGbieType(1);
                    // 单据、发票号
                    billInvoiceEnd.setGbieNum(item.getGbieNum());
                    // 单据类型
                    billInvoiceEnd.setGbieBillType(item.getGbieBillType());
                    // 创建人
                    billInvoiceEnd.setGbieCreateUser(user.getUserId());
                    // 创建时间
                    billInvoiceEnd.setGbieCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    // 创建时间
                    billInvoiceEnd.setGbieCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    return billInvoiceEnd;
                }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(collect)) {
            billInvoiceEndMapper.insertBatch(collect);
        }
    }

    /**
     * 付款记录明细导出
     *
     * @param id
     * @return
     */
    @Override
    public Result paymentRecordDetailsExport(Integer id) throws IOException {
        QueryWrapper<PaymentApplications> paymentApplicationsQueryWrapper = new QueryWrapper<>();
        paymentApplicationsQueryWrapper.eq("ID", id);
        PaymentApplications paymentApplications = paymentApplicationsMapper.selectOne(paymentApplicationsQueryWrapper);
        if (paymentApplications == null) {
            throw new CustomResultException("付款单不存在");
        }
        List<String> headDList = new ArrayList<>();
        List<List<Object>> dataList = new ArrayList<List<Object>>();
        // 单据付款
        if (CommonEnum.paymentApplicationsType.BILL.getCode().intValue() == paymentApplications.getType()) {
            List<SelectBillDTO> list = paymentApplicationsMapper.selectPayBillItems(paymentApplications.getClient(), paymentApplications.getId());
            headDList = new ArrayList<String>() {{
                add("业务类型");
                add("单据编号");
                add("单据应付金额");
                add("单据剩余金额 ");
                add("已付金额");
            }};
            if (!CollectionUtils.isEmpty(list)) {
                for (SelectBillDTO selectBillDTO : list) {
                    List<Object> data = new ArrayList<>();
                    // 业务类型
                    data.add(selectBillDTO.getBusinessType() + selectBillDTO.getBusinessTypeName());
                    // 单据编号
                    data.add(selectBillDTO.getMatDnId());
                    // 单据应付金额
                    data.add(selectBillDTO.getTotalAmount());
                    // 单据剩余金额
                    data.add(selectBillDTO.getChargeAmount());
                    // 已付金额
                    data.add(selectBillDTO.getSettlementAmount());
                    dataList.add(data);
                }
            }
        }
        // 发票付款
        if (CommonEnum.paymentApplicationsType.INVOICE.getCode().intValue() == paymentApplications.getType()) {
            List<SelectInvoiceDTO> list = paymentApplicationsMapper.selectPayInvoiceItems(paymentApplications.getClient(), paymentApplications.getId());
            headDList = new ArrayList<String>() {{
                add("发票号码");
                add("发票类型");
                add("发票日期");
                add("发票金额");
                add("发票确认可付金额");
                add("已付金额");
                add("本次付款金额");
                add("发票剩余付款金额");
            }};
            if (!CollectionUtils.isEmpty(list)) {
                for (SelectInvoiceDTO selectInvoiceDTO : list) {
                    List<Object> data = new ArrayList<>();
                    // 发票号码
                    data.add(selectInvoiceDTO.getInvoiceNum());
                    // 发票类型
                    if (StringUtils.isNotBlank(selectInvoiceDTO.getInvoiceType())) {
                        CommonEnum.Invoice_type invoiceType = CommonEnum.Invoice_type.getByCode(selectInvoiceDTO.getInvoiceType());
                        if (invoiceType != null) {
                            data.add(invoiceType.getName());
                        } else {
                            data.add("");
                        }
                    } else {
                        data.add("");
                    }
                    // 发票日期
                    data.add(selectInvoiceDTO.getInvoiceDate());
                    // 发票金额
                    data.add(selectInvoiceDTO.getInvoiceTotalInvoiceAmount());
                    // 发票确认可付金额
                    data.add(selectInvoiceDTO.getInvoiceConfirmationAmount());
                    // 已付金额
                    data.add(selectInvoiceDTO.getInvoicePaidAmount());
                    // 本次付款金额
                    data.add(selectInvoiceDTO.getPaymentAmount());
                    // 发票剩余付款金额
                    data.add(selectInvoiceDTO.getChargeAmount());
                    dataList.add(data);
                }
            }
        }
        // 单据预付
        if (CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode().intValue() == paymentApplications.getType()) {
            List<SelectWarehousingDTO> list = paymentApplicationsMapper.selectPayPrepaidBillItems(paymentApplications.getClient(), paymentApplications.getId());
            headDList = new ArrayList<String>() {{
                add("供应商编码");
                add("供应商名称");
                add("业务类型");
                add("单据编号");
                add("单据日期");
                add("单据合计金额");
            }};
            for (SelectWarehousingDTO selectWarehousingDTO : list) {
                List<Object> data = new ArrayList<>();
                // 供应商编码
                data.add(selectWarehousingDTO.getSupSelfCode());
                // 供应商名称
                data.add(selectWarehousingDTO.getSupSelfName());
                // 业务类型
                data.add(selectWarehousingDTO.getType() + selectWarehousingDTO.getTypeName());
                // 单据编号
                data.add(selectWarehousingDTO.getMatDnId());
                // 单据日期
                data.add(selectWarehousingDTO.getXgrq());
                // 单据合计金额
                data.add(selectWarehousingDTO.getTotalAmount());
                dataList.add(data);
            }
        }
        List<String[]> heads = new ArrayList<>();
        heads.add(headDList.toArray(new String[headDList.size()]));
        List<List<List<Object>>> data = new ArrayList<>();
        data.add(dataList);
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                heads,
                data,
                new ArrayList<String>() {{
                    add("付款申请");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    @Override
    public Result paymentRecordItemsExport(Integer id) throws IOException {
        QueryWrapper<PaymentApplications> paymentApplicationsQueryWrapper = new QueryWrapper<>();
        paymentApplicationsQueryWrapper.eq("ID", id);
        PaymentApplications paymentApplications = paymentApplicationsMapper.selectOne(paymentApplicationsQueryWrapper);
        if (paymentApplications == null) {
            throw new CustomResultException("付款单不存在");
        }
        List<String> headDList = new ArrayList<>();
        List<List<Object>> dataList = new ArrayList<List<Object>>();
        // 单据付款
        if (CommonEnum.paymentApplicationsType.BILL.getCode().intValue() == paymentApplications.getType()) {
            List<SelectBillDTO> list = paymentApplicationsMapper.selectPayBillItemsExprot(paymentApplications.getClient(), paymentApplications.getId());
            headDList = new ArrayList<String>() {{
                add("单据编号");
                add("业务类型");
                add("商品编号");
                add("商品名称");
                add("应付金额 ");
                add("本次付款金额");
            }};
            if (!CollectionUtils.isEmpty(list)) {
                for (SelectBillDTO selectBillDTO : list) {
                    List<Object> data = new ArrayList<>();
                    // 单据编号
                    data.add(selectBillDTO.getMatDnId());
                    // 业务类型
                    data.add(selectBillDTO.getBusinessType() + selectBillDTO.getBusinessTypeName());
                    // 商品编号
                    data.add(selectBillDTO.getSoProCode());
                    // 商品名称
                    data.add(selectBillDTO.getProName());
                    // 应付金额
                    if ("GD".equals(selectBillDTO.getBusinessType()) || "CJ".equals(selectBillDTO.getBusinessType())) {
                        data.add(selectBillDTO.getLineAmount().abs().multiply(new BigDecimal(-1)));
                    } else {
                        data.add(selectBillDTO.getLineAmount());
                    }
                    // 本次付款金额
                    data.add(selectBillDTO.getSettlementAmount());
                    dataList.add(data);
                }
            }
        }

        // 单据预付
        if (CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode().intValue() == paymentApplications.getType()) {
            List<SelectWarehousingDTO> list = paymentApplicationsMapper.selectPayPrepaidBillItemsByPay(paymentApplications.getClient(), paymentApplications.getId());
            headDList = new ArrayList<String>() {{
                add("单据编号");
                add("业务类型");
                add("商品编号");
                add("商品名称");
                add("数量");
                add("金额");
                add("已结算数量");
                add("已结算金额");
            }};
            for (SelectWarehousingDTO selectWarehousingDTO : list) {
                List<Object> data = new ArrayList<>();
                // 单据编号
                data.add(selectWarehousingDTO.getMatDnId());
                // 业务类型
                data.add(selectWarehousingDTO.getType() + selectWarehousingDTO.getTypeName());
                // 商品编号
                data.add(selectWarehousingDTO.getProCode());
                // 商品名称
                data.add(selectWarehousingDTO.getProName());
                // 数量
                data.add(selectWarehousingDTO.getMatQty());
                // 金额
                if ("GD".equals(selectWarehousingDTO.getType()) || "CJ".equals(selectWarehousingDTO.getType())) {
                    if (selectWarehousingDTO.getMatQty() == null || selectWarehousingDTO.getPrice() == null) {
                        data.add(selectWarehousingDTO.getTotalAmount().abs().multiply(new BigDecimal(-1)));
                    } else {
                        data.add(selectWarehousingDTO.getMatQty().multiply(selectWarehousingDTO.getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP).abs().multiply(new BigDecimal(-1)));
                    }
                } else {
                    if (selectWarehousingDTO.getMatQty() == null || selectWarehousingDTO.getPrice() == null) {
                        data.add(selectWarehousingDTO.getTotalAmount());
                    } else {
                        data.add(selectWarehousingDTO.getMatQty().multiply(selectWarehousingDTO.getPrice()).setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                }
                // 已结算数量
                data.add(selectWarehousingDTO.getQty());
                // 已结算金额
                data.add(selectWarehousingDTO.getAmount());
                dataList.add(data);
            }
        }
        List<String[]> heads = new ArrayList<>();
        heads.add(headDList.toArray(new String[headDList.size()]));
        List<List<List<Object>>> data = new ArrayList<>();
        data.add(dataList);
        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                heads,
                data,
                new ArrayList<String>() {{
                    add("付款申请");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 付款记录删除
     *
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result delPaymentRecord(Integer id) {
        TokenUser user = commonService.getLoginInfo();
        // 申请主表 GAIA_PAYMENT_APPLICATIONS
        PaymentApplications paymentApplications = paymentApplicationsMapper.selectOne(new LambdaQueryWrapper<PaymentApplications>()
                .eq(PaymentApplications::getClient, user.getClient())
                .eq(PaymentApplications::getId, id));
        if (paymentApplications == null) {
            throw new CustomResultException("付款单不存在");
        }
        if (paymentApplications.getApprovalStatus().intValue() != -1) {
            throw new CustomResultException("当前付款单状态不可以作废");
        }
        if (paymentApplications.getType().intValue() != CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode().intValue()) {
            throw new CustomResultException("只有预付单据才可以作废");
        }
        PaymentApplications entity = new PaymentApplications();
        entity.setApprovalStatus(2);
        paymentApplicationsMapper.update(entity, new LambdaQueryWrapper<PaymentApplications>()
                .eq(PaymentApplications::getClient, user.getClient())
                .eq(PaymentApplications::getId, id));
        return ResultUtil.success();
//        for (Integer id : ids) {
//            // 申请主表 GAIA_PAYMENT_APPLICATIONS
//            PaymentApplications paymentApplications = paymentApplicationsMapper.selectOne(new LambdaQueryWrapper<PaymentApplications>().eq(PaymentApplications::getId, id));
//            if (paymentApplications == null) {
//                continue;
//            }
//            // 未审批
//            if (paymentApplications.getApprovalStatus().intValue() != 0) {
//                throw new CustomResultException("付款单号" + paymentApplications.getPaymentOrderNo() + "不可以作废");
//            }
//            // 发票付款
//            if (paymentApplications.getType().intValue() == CommonEnum.paymentApplicationsType.INVOICE.getCode().intValue()) {
//                // GAIA_PAYMENT_VAT_APPLICATIONS
//                List<PaymentVatApplications> paymentVatApplicationsList = paymentVatApplicationsMapper.selectList(new LambdaQueryWrapper<PaymentVatApplications>().eq(PaymentVatApplications::getApplicationsId, id));
//                // GAIA_FICO_INVOICE_INFORMATION_REGISTRATION
//                if (CollectionUtils.isEmpty(paymentVatApplicationsList)) {
//                    continue;
//                }
//                // 发票明细
//                List<String> invoiceNumList = paymentVatApplicationsList.stream().map(PaymentVatApplications::getInvoiceNum).collect(Collectors.toList());
//                List<FicoInvoiceInformationRegistration> ficoInvoiceInformationRegistrationList = invoiceInformationRegistrationMapper.selectList(
//                        new LambdaQueryWrapper<FicoInvoiceInformationRegistration>()
//                                .eq(FicoInvoiceInformationRegistration::getClient, user.getClient())
//                                .in(FicoInvoiceInformationRegistration::getInvoiceNum, invoiceNumList));
//                for (PaymentVatApplications paymentVatApplications : paymentVatApplicationsList) {
//                    FicoInvoiceInformationRegistration entity = ficoInvoiceInformationRegistrationList.stream().filter(t -> t.getInvoiceNum().equals(paymentVatApplications.getInvoiceNum())).findFirst().orElse(null);
//                    if (entity != null) {
//                        FicoInvoiceInformationRegistration ficoInvoiceInformationRegistration = new FicoInvoiceInformationRegistration();
//                        // 已付金额
//                        BigDecimal invoicePaidAmount = entity.getInvoicePaidAmount();
//                        invoicePaidAmount = invoicePaidAmount.subtract(paymentVatApplications.getInvoiceAmountOfThisPayment());
//                        // 付款状态
//                        if (invoicePaidAmount.compareTo(BigDecimal.ZERO) == 0) {
//                            ficoInvoiceInformationRegistration.setInvoicePaymentStatus("0");
//                        }
//                        ficoInvoiceInformationRegistration.setInvoicePaidAmount(invoicePaidAmount);
//                        invoiceInformationRegistrationMapper.update(ficoInvoiceInformationRegistration,
//                                new LambdaQueryWrapper<FicoInvoiceInformationRegistration>().eq(FicoInvoiceInformationRegistration::getClient, user.getClient())
//                                        .eq(FicoInvoiceInformationRegistration::getInvoiceNum, paymentVatApplications.getInvoiceNum())
//                        );
//                    }
//                }
//            }
//            // 单据付款
//            if (paymentApplications.getType().intValue() == CommonEnum.paymentApplicationsType.BILL.getCode().intValue()) {
//
//            }
//            // 单据预付
//            if (paymentApplications.getType().intValue() == CommonEnum.paymentApplicationsType.PREPAIDBILL.getCode().intValue()) {
//
//                // 单据明细预付
//
//            }
//
//        }
//
//        // 申请主表删除
//        paymentApplicationsMapper.delete(new LambdaQueryWrapper<PaymentApplications>().in(PaymentApplications::getId, ids));
//        // 发票付款删除
//        paymentVatApplicationsMapper.delete(new LambdaQueryWrapper<PaymentVatApplications>().eq(PaymentVatApplications::getApplicationsId, ids));
    }

    /**
     * 导出
     *
     * @param paymentRecordDto
     * @return
     */
    @Override
    public Result paymentRecordExport(PaymentRecordDto paymentRecordDto) throws IOException {
        if (StringUtils.isNotBlank(paymentRecordDto.getSite()) && StringUtils.isNotBlank(paymentRecordDto.getStoCode())) {
            throw new CustomResultException("[采购地点]和[单体店]只能选择其中一个");
        }
        TokenUser user = commonService.getLoginInfo();
        paymentRecordDto.setClient(user.getClient());
        Page page = new Page<SelectWarehousingDTO>(-1, -1);
        IPage<PaymentRecordDetail> iPage = paymentApplicationsMapper.paymentRecord(page, paymentRecordDto);
        List<List<Object>> dataList = new ArrayList<>();
        iPage.getRecords().forEach(s -> {
            List<Object> data = new ArrayList<>();
            // 付款单号
            data.add(s.getPaymentOrderNo());
            // 审批状态 // '-1未申请，0-审批中，1-已审批，2-已废弃
            if (s.getApprovalStatus() != null) {
                data.add(
                        s.getApprovalStatus().intValue() == -1 ? "未申请" :
                                s.getApprovalStatus().intValue() == 0 ? "审批中" :
                                        s.getApprovalStatus().intValue() == 1 ? "已审批" :
                                                s.getApprovalStatus().intValue() == 2 ? "已废弃" : ""
                );
            } else {
                data.add("");
            }
            // 付款类型
            if (s.getType() != null) {
                CommonEnum.paymentApplicationsType paymentApplicationsType = CommonEnum.paymentApplicationsType.getPaymentApplicationsType(s.getType());
                if (paymentApplicationsType != null) {
                    data.add(paymentApplicationsType.getMessage());
                } else {
                    data.add("");
                }
            } else {
                data.add("");
            }
            // 地点
            data.add(StringUtils.parse("{}-{}",
                    StringUtils.isBlank(s.getProSite()) ? "" : s.getProSite(), StringUtils.isBlank(s.getStoName()) ? "" : s.getStoName()));
            // 供应商
            data.add(StringUtils.parse("{}-{}",
                    StringUtils.isBlank(s.getSupCode()) ? "" : s.getSupCode(), StringUtils.isBlank(s.getSupName()) ? "" : s.getSupName()));
            // 付款日期
            data.add(s.getPaymentOrderDate());
            // 本次付款申请金额
            data.add(s.getInvoiceAmountOfThisPayment());
            // 开户行名称
            data.add(s.getSupBankName());
            // 开户行账号
            data.add(s.getSupBankAccount());
            // 审批完成日期
            data.add(s.getGpaApprovalDate());
            // 工作流描述
            data.add(s.getWfDescription());
            dataList.add(data);
        });

        HSSFWorkbook workbook = ExcelUtils.exportExcel(
                new ArrayList<String[]>() {{
                    add(headList);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add("付款申请");
                }});
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        return result;
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "付款单号",
            "审批状态",
            "付款类型",
            "地点",
            "供应商",
            "付款日期",
            "本次付款申请金额",
            "开户行名称",
            "开户行账号",
            "审批完成日期",
            "工作流描述",
    };
}
