package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class PushGsmVO2 {

    @NotNull(message = "id不能为空")
    private Integer id;

    @NotEmpty(message = "所选加盟商集合不能为空")
    private List<String> clientList;

}
