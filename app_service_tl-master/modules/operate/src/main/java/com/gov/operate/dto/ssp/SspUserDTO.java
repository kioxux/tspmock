package com.gov.operate.dto.ssp;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class SspUserDTO {
    /**
     * 主键
     */
    private Long id;

    /**
     * 手机号码
     */
    @NotBlank(message = "手机号码不能为空")
    private String mobile;

    /**
     * 业务员名称
     */
    @NotBlank(message = "业务员名称不能为空")
    private String userName;

    /**
     * 有效期起
     */
    @NotNull(message = "有效期不能为空")
    private Date expiryStartDate;

    /**
     * 有效期至
     */
    @NotNull(message = "有效期不能为空")
    private Date expiryEndDate;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 供应商列表
     */
    private List<SspUserSupplierDTO> supplierList;


}
