package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_STORES_GROUP")
@ApiModel(value="SdStoresGroup对象", description="")
public class SdStoresGroup extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "分类编号")
    @TableField("GSSG_ID")
    private String gssgId;

    @ApiModelProperty(value = "分类名称")
    @TableField("GSSG_NAME")
    private String gssgName;

    @ApiModelProperty(value = "门店编码")
    @TableField("GSSG_BR_ID")
    private String gssgBrId;

    @ApiModelProperty(value = "门店名称")
    @TableField("GSSG_BR_NAME")
    private String gssgBrName;

    @ApiModelProperty(value = "分类类型编码")
    @TableField("GSSG_TYPE")
    private String gssgType;

    @ApiModelProperty(value = "修改人")
    @TableField("GSSG_UPDATE_EMP")
    private String gssgUpdateEmp;

    @ApiModelProperty(value = "修改日期")
    @TableField("GSSG_UPDATE_DATE")
    private String gssgUpdateDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("GSSG_UPDATE_TIME")
    private String gssgUpdateTime;

}
