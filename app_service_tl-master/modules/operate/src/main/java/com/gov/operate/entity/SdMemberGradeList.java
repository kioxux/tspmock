package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_GRADE_LIST")
@ApiModel(value="SdMemberGradeList对象", description="")
public class SdMemberGradeList extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSMGL_CARD_ID")
    private String gsmglCardId;

    @ApiModelProperty(value = "分级编号")
    @TableField("GSMGL_GRADE_ID")
    private String gsmglGradeId;

    @ApiModelProperty(value = "周期内消费总额")
    @TableField("GSMGL_AMT")
    private BigDecimal gsmglAmt;

    @ApiModelProperty(value = "周期内消费次数")
    @TableField("GSMGL_SALE_TIMES")
    private String gsmglSaleTimes;

    @ApiModelProperty(value = "评估周期")
    @TableField("GSMGL_CYCLE")
    private String gsmglCycle;

    @ApiModelProperty(value = "最后一次交易时间")
    @TableField("GSMGL_LAST_SALE")
    private String gsmglLastSale;

    @ApiModelProperty(value = "更新日期")
    @TableField("GSMGL_UPDATE_DATE")
    private String gsmglUpdateDate;

    @ApiModelProperty(value = "计算月份")
    @TableField("GSMGL_CALC_MONTH")
    private String gsmglCalcMonth;


}
