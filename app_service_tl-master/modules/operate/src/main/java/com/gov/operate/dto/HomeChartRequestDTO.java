package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

@Data
public class HomeChartRequestDTO {
    /**
     * 查询类型： 1.今日，2：昨日，3：7日，4：7周，5：今年  6: 月
     */
    private Integer type;

    /**
     * 门店编码
     */
    private String storeId;
    /**
     * 加盟商
     */
    private String clientId;

    private List<String> stoCodeList;
}
