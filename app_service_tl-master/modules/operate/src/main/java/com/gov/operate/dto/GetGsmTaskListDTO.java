package com.gov.operate.dto;

import com.gov.operate.entity.SdMarketing;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetGsmTaskListDTO extends SdMarketing {

    /**
     * 门店名称
     */
    private String stoName;
}
