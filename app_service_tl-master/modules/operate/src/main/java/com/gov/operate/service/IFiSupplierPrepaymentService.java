package com.gov.operate.service;

import com.gov.operate.entity.FiSupplierPrepayment;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
public interface IFiSupplierPrepaymentService extends SuperService<FiSupplierPrepayment> {

}
