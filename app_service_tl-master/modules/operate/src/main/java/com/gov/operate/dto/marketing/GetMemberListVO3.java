package com.gov.operate.dto.marketing;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetMemberListVO3 extends Pageable {


    /**
     * 统一公共时间
     **/
    private String startDate;

    private String endDate;
    /********************************* 1会员属性 ***********************************/


    /**
     * 1.2 所属门店
     */
    private List<String> storeList;
    /**
     * 1.3 会员卡别
     */
    private List<String> classId;
    /**
     * 1.4 性别
     */
    private String sex;
    /**
     * 1.5.1 最小年龄
     */
    private Integer minAge;
    /**
     * 1.5.2 最大年龄
     */
    private Integer maxAge;
    /**
     * 1.6.1 最小积分
     */
    private Integer minIntegral;
    /**
     * 1.6.2 最大积分
     */
    private Integer maxIntegral;

    /**
     * 1.7.1星座类型
     */
    private String constellationType;
    /**
     * 1.7.2会员年龄阶段类型
     */
    private String ageLevelType;
    /**
     * 1.7.3会员生日开始日期
     */
    private String memberBirthStartDate;
    /**
     * 1.7.4会员生日结束日期
     */
    private String memberBirthEndDate;
    /**
     * 1.7.5会员生日开始年份
     */
    private String memberBirthStartYear;
    /**
     * 1.7.6会员生日结束年份
     */
    private String memberBirthEndYear;
    /**
     * 1.8.1 办卡日期 开始时间
     */
    private String gsmbcCreateDateStart;
    /**
     * 1.8.2 办卡日期 结束时间
     */
    private String gsmbcCreateDateEnd;


    /********************************* 2消费属性 ***********************************/
    /**
     * 2.1 消费属性（1-最近消费，2-最后消费）
     */
    private String consume;
    /**
     * 2.1 消费类型（1-7天内、2-15天内、3-30天内、4-60天内、5-自定义）
     */
    private String consumeType;
    /**
     * 消费类型具体对应的天数（1-7天内、2-15天内、3-30天内、4-60天内、5-自定义）（翻译成对应的天数）(前端不传)
     */
    private Integer consumeTypeDay;
    /**
     * 2.1 消费开始日期
     */
    private String consumeStartDate;
    /**
     * 2.1 消费结束日期
     */
    private String consumeEndDate;

    /**
     * 2.2 消费频率开始
     */
    private String consumptionFrequencyDateStart;
    /**
     * 2.2 消费频率结束
     */
    private String consumptionFrequencyDateEnd;
    /**
     * 2.2 消费次数
     */
    private Integer consumptionTimes;

    private Integer consumptionTimesMin;

    private Integer consumptionTimesMax;

    /**
     * 2.3 消费渠道list
     */
    private List<String> consumptionPaymentCodeList;

    /**
     * 4消费金额最小值
     */
    private Integer consumptionAmtMin;
    /**
     * 2.5消费金额最大值
     */
    private Integer consumptionAmtMax;

    /**
     * 1.1 活跃度  核心、普通、新增、睡眠、无效
     * <p>
     * * 1.无效会员 ---无消费记录的会员
     * * 2.新会员   ---30天内的新会员且有消费记录
     * * 3.核心会员   ---90天内消费次数>=3，|| 消费金额>=500元
     * * 4.普通会员   ---90天内有过消费的会员
     * * 5.睡眠会员   ---90天无消费的会员
     * * 6.流失会员   ---180天内无消费的会员
     */
    private List<String> gsmtgId;


    /********************************* 3商品属性 ***********************************/

    /**
     * 3.6 成分编码
     */
    private String proCompclass;

    private List<String> proCompclassList;

    private String proSaleStartDate;

    private String proSaleEndDate;
    /**
     * 3.7 成分名称
     */
    private String proCompclassName;

    private List<String> proCompclassNameList;
    /**
     * 3.1 商品编码
     */
    private String proSelfCode;

    private List<String> proSelfCodeList;
    /**
     * 3.2 商品名称
     */
    private String proName;

    private List<String> proNameList;
    /**
     * 3.3 商品分类
     */
    private String proClass;

    private List<String> proClassList;
    /**
     * 3.4 商品组ID
     */
    private String gsyGroup;

    private List<String> gsyGroupList;
    /**
     * 3.5 生产厂家
     */
    private String proFactoryName;

    private List<String> proFactoryNameList;
    /**
     * 3.6 品牌
     */
    private String proBrandCode;

    private List<String> proBrandCodeList;

    /********************************* 4.病种属性 ***********************************/

    /**
     * 4.1 疾病标签
     */
    private String diseaseCode;
    private List<String> diseaseCodeList;
    private String diseaseName;
    private List<String> diseaseNameList;

    /**
     * 4.2 疾病组
     */


    /********************************* 5.剔除会员 ***********************************/
    /**
     * 剔除类型（1-已发短信会员, 2:-自定义会员, 3.勿扰会员）
     */
    private String eliminateType;

    private String eliminateStartDate;

    private String eliminateEndDate;
    /**
     * 剔除会员卡号
     */
    private List<String> eliminateList;


    /********************************** 其他 ***********************************/

    private String gsmsId;

    /**
     * 门店编码 (精准查询入口携带)
     */
    private String gsmStore;

    /**
     * 短信发送人数,使用地方：异步发送短信人数上限
     */
    private Integer gsmSmstimes;

    private String client;

    private Integer limit;

    /**
     * 企微状态（0 - 未关注，1- 已关注）
     */
    private String bindStatus;

    /**
     * 企微绑定门店
     */
    private List<String> bindStoList;

    /**
     * 绑定开始时间
     */
    private String bindStartDate;

    /**
     * 绑定结束时间
     */
    private String bindEndDate;

    /**
     * 最后发送开始时间
     */
    private String lastSendStartDate;

    /**
     * 最后发送截止时间
     */
    private String lastSendEndDate;

    /**
     * 企微发送状态（1-是，0-否）
     */
    private String sendStatus;

    private String sendStartDate;

    private String sendEndDate;

    /**
     * 标签
     */
    private List<String> tagList;

    /**
     * 会员属性 手机号 卡号
     */
    private String memberAttrParams1;
}
