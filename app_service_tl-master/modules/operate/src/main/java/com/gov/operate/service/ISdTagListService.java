package com.gov.operate.service;

import com.gov.operate.entity.SdTagList;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
public interface ISdTagListService extends SuperService<SdTagList> {

    /**
     * 会员标签列表
     */
    List<SdTagList> getTagList();
}
