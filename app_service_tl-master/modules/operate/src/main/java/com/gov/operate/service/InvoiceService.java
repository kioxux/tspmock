package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.ExportSelectWarehousingDetailsVO;
import com.gov.operate.entity.DcData;
import com.gov.operate.entity.ExportTask;
import com.gov.operate.entity.StoreData;
import com.gov.operate.entity.SupplierBusiness;

import java.io.IOException;
import java.util.List;

public interface InvoiceService {

    /**
     * 采购地点
     */
    List<DcData> getPurcSite();

    /**
     * 供应商
     */
    List<SupplierBusiness> getSupplier(String site);

    /**
     * 出入库查询
     */
    IPage<SelectWarehousingDTO> selectWarehousing(SelectWarehousingVO vo);

    /**
     * 出入库明细
     */
    List<SelectWarehousingDetailsDTO> selectWarehousingDetails(SelectWarehousingDetailsVO vo);

    /**
     * 订单发票确认
     */
    void orderInvoice(OrderInvoiceVO vo);

    /**
     * 订单明细发票确认
     */
    void orderDetailInvoice(OrderDetailInvoiceVO vo);

    /**
     * 订单发票批量确认
     *
     * @param vo
     */
    void orderBatchInvoice(OrderInvoiceVO vo);

    /**
     * 订单发票批量确认2
     */
    void orderBatchInvoice2(OrderBatchInvoiceVO vo);

    /**
     * 单据预付
     *
     * @param vo
     * @return
     */
    Result selectPrepaidBill(SelectWarehousingVO vo);

    /**
     * 单据预付
     *
     * @param list
     * @return
     */
    Result savePrepaidBill(List<SelectWarehousingDTO> list);

    /**
     * 单体店
     */
    List<StoreData> getStoList();

    /**
     * 出入库查询金额合计
     *
     * @param vo
     * @return
     */
    Result selectWarehousingAmt(SelectWarehousingVO vo);

    /**
     * 导出
     */
    Result exportSelectPrepaidBill(SelectWarehousingVO vo) throws IOException;

    /**
     * 供应商单据预付明细导出
     */
    Result exportSelectWarehousingDetails(ExportSelectWarehousingDetailsVO vo);

    Result savePrepaidDetail(List<SelectPrepaidBillDetailVO> list);

    Result selectPrepaidBillDetail(SelectWarehousingVO vo);

    Result exportSelectPrepaidBillDetail(SelectWarehousingVO vo) throws IOException;

    Result gysdjyf();

    IPage<SelectWarehousingDetailsDTO> selectWarehousingList(SelectWarehousingVO vo);

    void selectWarehousingListExport(SelectWarehousingVO vo, ExportTask exportTask) throws IOException;

    void orderDetailInvoice2(List<OrderDetailInvoiceVO> vo);

    ExportTask initExportTask();
}
