package com.gov.operate.service.smsRecharge;

import com.gov.common.response.Result;
import com.gov.operate.entity.SmsRechargeRecord;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
public interface ISmsRechargeRecordService extends SuperService<SmsRechargeRecord> {

    /**
     * 购买记录
     *
     * @param pageNum
     * @param pageSize
     * @param gsrrRechargeTimeStart
     * @param gsrrRechargeTimeEnd
     * @return
     */
    Result getRechargeRecord(Integer pageNum, Integer pageSize, String gsrrRechargeTimeStart, String gsrrRechargeTimeEnd);

    /**
     * 短信可用数量
     *
     * @return
     */
    Result getAvailableQuantity();

    /**
     * 短信发送记录
     *
     * @param pageNum
     * @param pageSize
     * @param gssTel
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    Result getSmsSendRecord(Integer pageNum, Integer pageSize, String gssTel, String gssSendTimeStart, String gssSendTimeEnd);

    /**
     * 开票短信查询
     *
     * @param pageNum
     * @param pageSize
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    Result getInvoiceSmsList(Integer pageNum, Integer pageSize, String gssSendTimeStart, String gssSendTimeEnd);

    /**
     * 开票信息查询
     *
     * @return
     */
    Result getInvoiceInfo();

    /**
     * 开票信息保存
     *
     * @param ficoSocialCreditCode
     * @param ficoTaxSubjectName
     * @param ficoBankName
     * @param ficoBankNumber
     * @return
     */
    Result saveInvoiceInfo(String ficoSocialCreditCode, String ficoTaxSubjectName, String ficoBankName, String ficoBankNumber);

    /**
     * 短信开票申请
     *
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    Result sendInvoice(String gssSendTimeStart, String gssSendTimeEnd);
}
