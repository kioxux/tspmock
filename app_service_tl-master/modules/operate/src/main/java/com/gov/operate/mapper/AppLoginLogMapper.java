package com.gov.operate.mapper;

import com.gov.operate.entity.AppLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-11
 */
public interface AppLoginLogMapper extends BaseMapper<AppLoginLog> {

    /**
     * 用户最后一次登录数据
     *
     * @param client
     * @param userId
     * @return
     */
    AppLoginLog selectLastLogByUser(@Param("client") String client, @Param("userId") String userId);

    /**
     * 批量获取用户登录设备
     *
     * @param list list
     * @return List<AppLoginLog>
     */
    List<AppLoginLog> selectLastLoginLogByUserIds(List<AppLoginLog> list);

}
