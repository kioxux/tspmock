package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryYearAwards;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryYearAwardsService extends SuperService<SalaryYearAwards> {

    /**
     * 设定年终奖
     */
    void saveYearAwards(EditProgramVO vo);

    /**
     * 年终奖列表
     */
    List<SalaryYearAwards> getYearAwardsList(Integer gspId);
}
