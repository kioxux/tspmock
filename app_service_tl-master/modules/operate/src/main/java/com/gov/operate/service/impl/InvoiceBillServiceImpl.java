package com.gov.operate.service.impl;

import com.gov.operate.entity.InvoiceBill;
import com.gov.operate.mapper.InvoiceBillMapper;
import com.gov.operate.service.IInvoiceBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Service
public class InvoiceBillServiceImpl extends ServiceImpl<InvoiceBillMapper, InvoiceBill> implements IInvoiceBillService {

}
