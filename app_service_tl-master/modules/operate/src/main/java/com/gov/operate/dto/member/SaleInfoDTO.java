package com.gov.operate.dto.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("会员消费信息")
@Data
public class SaleInfoDTO {

    @ApiModelProperty("最后消费日期")
    private String lastSaleDate;

    @ApiModelProperty("最后一次交易门店名称")
    private String lastStoName;

}
