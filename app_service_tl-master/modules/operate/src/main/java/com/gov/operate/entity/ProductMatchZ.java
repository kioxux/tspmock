package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 导入商品匹配信息主表
 * </p>
 *
 * @author sy
 * @since 2021-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_MATCH_Z")
@ApiModel(value="ProductMatchZ对象", description="导入商品匹配信息主表")
public class ProductMatchZ extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "商品匹配编码")
    @TableField("MATCH_CODE")
    private String matchCode;

    @ApiModelProperty(value = "门店")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "创建人")
    @TableField("MATCH_CREATER")
    private String matchCreater;

    @ApiModelProperty(value = "创建日期")
    @TableField("MATCH_CREATE_DATE")
    private String matchCreateDate;

    @ApiModelProperty(value = "是否门店匹配 0 否 1 是(1时门店编码必填)")
    @TableField("MATCH_TYPE")
    private String matchType;

    @ApiModelProperty(value = "修改日期")
    @TableField("MATCH_UPDATE_DATE")
    private String matchUpdateDate;

    @ApiModelProperty(value = "修改人")
    @TableField("MATCH_UPDATER")
    private String matchUpdater;

    @ApiModelProperty(value = "商品主数据校验负责人电话")
    @TableField("MATCH_DATA_CHECK_TOUCH_TEL")
    private String matchDataCheckTouchTel;

    @ApiModelProperty(value = "商品主数据校验负责人是否确认标识 0表示取消 1表示确认 ")
    @TableField("MATCH_DATA_CHECK_FLAG")
    private String matchDataCheckFlag;

    @ApiModelProperty(value = "商品主数据校验负责人确认时间")
    @TableField("MATCH_DATA_CHECK_TIME")
    private LocalDateTime matchDataCheckTime;

    @ApiModelProperty(value = "是否通知商采负责人 1是0否")
    @TableField("MATCH_SC_FLAG")
    private String matchScFlag;

    @ApiModelProperty(value = "是否通知质量负责人1是0否")
    @TableField("MATCH_ZL_FLAG")
    private String matchZlFlag;

    @ApiModelProperty(value = "商品主数据校验负责人确认后任务的截止时间")
    @TableField("MATCH_DATA_CHECK_END_DATE")
    private String matchDataCheckEndDate;

    @ApiModelProperty(value = "是否需要触发二次推送 1表示需要 0表示不需要")
    @TableField("MATCH_SECOND_PUSH_FLAG")
    private String matchSecondPushFlag;

    @ApiModelProperty(value = "取消次数")
    @TableField("MATCH_CANCEL_COUNT")
    private Integer matchCancelCount;


}
