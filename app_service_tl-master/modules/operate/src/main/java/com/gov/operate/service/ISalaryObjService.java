package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.dto.GetProListVO;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SalaryObj;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryObjService extends SuperService<SalaryObj> {

    /**
     * 修改方适用范围
     */
    void saveApplyObj(EditProgramVO vo);

    /**
     * 获取适用范围
     */
    List<SalaryObj> getApplyObj(Integer gspId);

    /**
     * 适用范围 (门店/连锁公司列表)
     */
    List<SalaryObj> salaryObjList();

    /**
     * 商品列表
     */
    List<ProductBusiness> getProList(GetProListVO vo);
}
