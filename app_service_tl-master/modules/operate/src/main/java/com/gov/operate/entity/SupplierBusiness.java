package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SUPPLIER_BUSINESS")
@ApiModel(value="SupplierBusiness对象", description="")
public class SupplierBusiness extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("SUP_SITE")
    private String supSite;

    @ApiModelProperty(value = "供应商自编码")
    @TableField("SUP_SELF_CODE")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "匹配状态")
    @TableField("SUP_MATCH_STATUS")
    private String supMatchStatus;

    @ApiModelProperty(value = "助记码")
    @TableField("SUP_PYM")
    private String supPym;

    @ApiModelProperty(value = "供应商名称")
    @TableField("SUP_NAME")
    private String supName;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("SUP_CREDIT_CODE")
    private String supCreditCode;

    @ApiModelProperty(value = "营业期限")
    @TableField("SUP_CREDIT_DATE")
    private String supCreditDate;

    @ApiModelProperty(value = "供应商分类")
    @TableField("SUP_CLASS")
    private String supClass;

    @ApiModelProperty(value = "法人")
    @TableField("SUP_LEGAL_PERSON")
    private String supLegalPerson;

    @ApiModelProperty(value = "注册地址")
    @TableField("SUP_REG_ADD")
    private String supRegAdd;

    @ApiModelProperty(value = "供应商状态")
    @TableField("SUP_STATUS")
    private String supStatus;

    @ApiModelProperty(value = "许可证编号")
    @TableField("SUP_LICENCE_NO")
    private String supLicenceNo;

    @ApiModelProperty(value = "发证日期")
    @TableField("SUP_LICENCE_DATE")
    private String supLicenceDate;

    @ApiModelProperty(value = "有效期至")
    @TableField("SUP_LICENCE_VALID")
    private String supLicenceValid;

    @ApiModelProperty(value = "生产或经营范围")
    @TableField("SUP_SCOPE")
    private String supScope;

    @ApiModelProperty(value = "禁止采购")
    @TableField("SUP_NO_PURCHASE")
    private String supNoPurchase;

    @ApiModelProperty(value = "禁止退厂")
    @TableField("SUP_NO_SUPPLIER")
    private String supNoSupplier;

    @ApiModelProperty(value = "采购付款条件")
    @TableField("SUP_PAY_TERM")
    private String supPayTerm;

    @ApiModelProperty(value = "业务联系人")
    @TableField("SUP_BUSSINESS_CONTACT")
    private String supBussinessContact;

    @ApiModelProperty(value = "联系人电话")
    @TableField("SUP_CONTACT_TEL")
    private String supContactTel;

    @ApiModelProperty(value = "送货前置期")
    @TableField("SUP_LEAD_TIME")
    private String supLeadTime;

    @ApiModelProperty(value = "银行代码")
    @TableField("SUP_BANK_CODE")
    private String supBankCode;

    @ApiModelProperty(value = "银行名称")
    @TableField("SUP_BANK_NAME")
    private String supBankName;

    @ApiModelProperty(value = "账户持有人")
    @TableField("SUP_ACCOUNT_PERSON")
    private String supAccountPerson;

    @ApiModelProperty(value = "银行账号")
    @TableField("SUP_BANK_ACCOUNT")
    private String supBankAccount;

    @ApiModelProperty(value = "支付方式")
    @TableField("SUP_PAY_MODE")
    private String supPayMode;

    @ApiModelProperty(value = "铺底授信额度")
    @TableField("SUP_CREDIT_AMT")
    private String supCreditAmt;

    @ApiModelProperty(value = "起订金额")
    @TableField("SUP_MIX_AMT")
    private BigDecimal supMixAmt;


}
