package com.gov.operate.dto.ssp;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SspUserPageVO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 业务员名称
     */
    private String userName;

    /**
     * 有效期
     */
    private Date expiryStartDate;

    /**
     * 有效期
     */
    private Date expiryEndDate;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 供应商名称逗号隔开
     */
    private String supName;

    /**
     * 供应商名称
     */
    private List<String> supNames;
}
