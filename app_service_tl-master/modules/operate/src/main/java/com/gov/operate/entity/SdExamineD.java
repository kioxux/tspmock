package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_EXAMINE_D")
@ApiModel(value="SdExamineD对象", description="")
public class SdExamineD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "收货单号")
    @TableField("GSED_VOUCHER_ID")
    private String gsedVoucherId;

    @ApiModelProperty(value = "收货日期")
    @TableField("GSED_DATE")
    private String gsedDate;

    @ApiModelProperty(value = "行号")
    @TableField("GSED_SERIAL")
    private String gsedSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSED_PRO_ID")
    private String gsedProId;

    @ApiModelProperty(value = "批号")
    @TableField("GSED_BATCH_NO")
    private String gsedBatchNo;

    @ApiModelProperty(value = "批次")
    @TableField("GSED_BATCH")
    private String gsedBatch;

    @ApiModelProperty(value = "有效期")
    @TableField("GSED_VALID_DATE")
    private String gsedValidDate;

    @ApiModelProperty(value = "收货数量")
    @TableField("GSED_RECIPIENT_QTY")
    private String gsedRecipientQty;

    @ApiModelProperty(value = "合格数量")
    @TableField("GSED_QUALIFIED_QTY")
    private String gsedQualifiedQty;

    @ApiModelProperty(value = "不合格数量")
    @TableField("GSED_UNQUALIFIED_QTY")
    private String gsedUnqualifiedQty;

    @ApiModelProperty(value = "不合格事项")
    @TableField("GSED_UNQUALIFIED_CAUSE")
    private String gsedUnqualifiedCause;

    @ApiModelProperty(value = "验收人员")
    @TableField("GSED_EMP")
    private String gsedEmp;

    @ApiModelProperty(value = "验收结论")
    @TableField("GSED_RESULT")
    private String gsedResult;

    @ApiModelProperty(value = "库存状态")
    @TableField("GSED_STOCK_STATUS")
    private String gsedStockStatus;


}
