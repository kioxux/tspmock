package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 门店销售
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_STORE_SALES")
@ApiModel(value="BatchStoreSales对象", description="门店销售")
public class BatchStoreSales extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableField("ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBSS_QUERY_FACTORY")
    private String gbssQueryFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBSS_DC")
    private String gbssDc;

    @ApiModelProperty(value = "日期")
    @TableField("GBSS_DATE")
    private String gbssDate;

    @ApiModelProperty(value = "销售方代码")
    @TableField("GBSS_SALES_CODE")
    private String gbssSalesCode;

    @ApiModelProperty(value = "销售方名称")
    @TableField("GBSS_SALES_NAME")
    private String gbssSalesName;

    @ApiModelProperty(value = "采购方代码")
    @TableField("GBSS_PURCHASE_CODE")
    private String gbssPurchaseCode;

    @ApiModelProperty(value = "采购方名称")
    @TableField("GBSS_PURCHASE_NAME")
    private String gbssPurchaseName;

    @ApiModelProperty(value = "扩展字段")
    @TableField("GBSS_EXPAND")
    private String gbssExpand;

    @ApiModelProperty(value = "适应症")
    @TableField("GBSS_INDICATIONS")
    private String gbssIndications;

    @ApiModelProperty(value = "产品代码")
    @TableField("GBSS_PRO_CODE")
    private String gbssProCode;

    @ApiModelProperty(value = "产品名称")
    @TableField("GBSS_PRO_NAME")
    private String gbssProName;

    @ApiModelProperty(value = "产品规格")
    @TableField("GBSS_PRO_SPECS")
    private String gbssProSpecs;

    @ApiModelProperty(value = "批号")
    @TableField("GBSS_BATCH_NO")
    private String gbssBatchNo;

    @ApiModelProperty(value = "数量")
    @TableField("GBSS_QTY")
    private BigDecimal gbssQty;

    @ApiModelProperty(value = "单位")
    @TableField("GBSS_UNIT")
    private String gbssUnit;

    @ApiModelProperty(value = "单价")
    @TableField("GBSS_PRICE")
    private BigDecimal gbssPrice;

    @ApiModelProperty(value = "金额")
    @TableField("GBSS_AMT")
    private BigDecimal gbssAmt;

    @ApiModelProperty(value = "生产厂家")
    @TableField("GBSS_FACTORY")
    private String gbssFactory;

    @ApiModelProperty(value = "有效期")
    @TableField("GBSS_VALID")
    private String gbssValid;

    @ApiModelProperty(value = "销售类型")
    @TableField("GBSS_SALES_TYPE")
    private String gbssSalesType;

    @ApiModelProperty(value = "收货地址")
    @TableField("GBSS_RECEIPT_ADDR")
    private String gbssReceiptAddr;

    @ApiModelProperty(value = "批准文号")
    @TableField("GBSS_APPROVAL_NUMBER")
    private String gbssApprovalNumber;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBSS_CREATE_DATE")
    private String gbssCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBSS_CREATE_TIME")
    private String gbssCreateTime;


}
