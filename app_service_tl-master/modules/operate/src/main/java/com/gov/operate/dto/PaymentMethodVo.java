package com.gov.operate.dto;

import lombok.Data;

@Data
public class PaymentMethodVo {

    /**
     * 销售类型
     */
    private String id;

    /**
     * 销售名称
     */
    private String name;

    /**
     * 销售金额
     */
    private String amt;

    /**
     * 销售金额
     */
    private String color;

}
