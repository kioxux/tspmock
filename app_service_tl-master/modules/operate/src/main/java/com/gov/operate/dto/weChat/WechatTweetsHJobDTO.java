package com.gov.operate.dto.weChat;

import com.gov.operate.entity.WechatTweetsH;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhoushuai
 * @date 2021/4/12 12:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WechatTweetsHJobDTO extends WechatTweetsH {
    /**
     * 是否授权(0:否 1:是)
     */
    private String goaObjectValue;
}
