package com.gov.operate.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.response.Result;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.GetWorkflowInData;
import com.gov.operate.feign.dto.JsonResult;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuthFeignFallback implements AuthFeign {


    @Override
    public JsonResult createWorkflow(Map<String, Object> param) {
        JsonResult result = new JsonResult();
        result.setCode(9999);
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }

    @Override
    public JSONObject getLoginInfo(Map<String, Object> param) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return (JSONObject) JSONObject.toJSON(result);
    }

    @Override
    public JsonResult selectOneApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }
}
