package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.SdMarketingBasic;
import com.gov.operate.entity.SdMarketingProm;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface ISdMarketingBasic3Service extends SuperService<SdMarketingBasic> {

    /**
     * 营销设置列表
     */
    IPage<SdMarketingBasicDTO3> getGsmSettingList(GetGsmSettingListVO3 vo);

    /**
     * 营销设置保存
     */
    void saveGsmSetting(SaveGsmSettingVO3 vo);

    /**
     * 营销设置编辑
     */
    void editGsmSetting(EditGsmSettingVO3 vo);

    /**
     * 营销设置详情
     */
    GetGsmSettingDetailDTO3 getGsmSettingDetail(Integer id);

    /**
     * 营销设置删除
     */
    void deleteMarketSetting(Integer id);

    /**
     * 活动方案推送
     */
    void pushGsm(PushGsmVO3 vo);

    /**
     * 营销设置推送定时执行器
     */
    void marketingPushReminder();

    /**
     * 促销方式列表
     */
    List<GetPromotionListDTO> getPromotionList();

    /***********************************************************************/

    /**
     * 营销任务列表
     */
    IPage<GetGsmTaskListDTO2> getGsmTaskList(GsmTaskListVO2 vo);

    /**
     * 营销任务详情
     */
    GetGsmTaskDetailDTO3 getGsmTaskDetail(Integer id, Integer type, String stoCode);

    /**
     * 营销任务保存
     */
    void saveGsmTask(SaveGsmTaskVO3 vo);

    /**
     * 转交修改
     */
    void transferToEdit(Integer id, String gsmOpinion);

    /**
     * 不予执行
     */
    void noEnforcement(Integer id);

    /**
     * 选品选择列表
     */
    IPage<GetProductListToCheckDTO> getProductListToCheck(GetProductListToCheckVO vo);

    /**
     * 赠品选择列表
     */
    IPage<GetProductListToCheckDTO> getGiveawayListToCheck(GetGiveawayListToCheckVO vo);

    /**
     * 目标试算
     */
    MarketingTargetTrialDTO marketingAimsTrial(MarketingAimsTrialVO vo);

    /**
     * 当前营销 历史营销
     *
     * @param vo
     * @return
     */
    IPage<GetGsmTaskListDTO2> getTaskExecutedList(GsmTaskListVO2 vo);

    /**
     * 营销任务促销详情
     */
    IPage<GetPromDetailsDTO> getPromDetails(GetPromDetailsVO vo);

    /**
     * 活动商品列表
     *
     * @param pageNum
     * @param pageSize
     * @param id
     * @param proKey
     * @param promFitsType
     * @param promFitsStart
     * @param promFitsEnd
     * @return
     */
    IPage<GetProductListToCheckDTO> getProductList(Integer pageNum, Integer pageSize, Integer id, String proKey, BigDecimal mllStart, BigDecimal mllEnd, Integer promFitsType, BigDecimal promFitsStart, BigDecimal promFitsEnd);

    /**
     * 活动商品删除
     *
     * @param list
     */
    void deleteProduct(List<Integer> list);

    /**
     * 活动商品保存
     *
     * @param list
     */
    void saveProduct(Integer marketingId, List<GetProductListToCheckDTO> list);

    /**
     * 营销促销列表
     *
     * @param id
     * @return
     */
    List<SdMarketingProm> getPromList(Integer id);

    /**
     * 商品查询
     *
     * @param proSelfCode
     * @param mllStart
     * @param mllEnd
     * @return
     */
    IPage<GetProductListToCheckDTO> getProductBySelfCode(Integer id, Integer promId, String proKey, String proSelfCode, BigDecimal mllStart, BigDecimal mllEnd, Integer pageNum, Integer pageSize);

    /**
     * 赠品保存
     *
     * @param list
     */
    void saveGiveaway(List<GetProductListToCheckDTO> list);

    /**
     * 自定义参数获取
     *
     * @param promId
     * @return
     */
    List<CustomParam> getCustomParams(Integer promId);

    /**
     * 自定义参数保存
     *
     * @param list
     */
    void saveCoustomParams(List<CustomParam> list);


    /**
     * 营销任务会员宣传短信发送 定时任务
     */
    void marketingSmsSendReminder();

    /**
     * 客户新增促销
     *
     * @param sdMarketingProm
     */
    void addProm(SdMarketingProm sdMarketingProm);

    /**
     * 促销详情获取
     *
     * @param promId
     * @return
     */
    SdMarketingProm getParams(Integer promId);

    /**
     * 促销参数保存
     *
     * @param sdMarketingProm
     */
    void saveParams(SdMarketingPromPrd sdMarketingPromPrd);

    /**
     * 导出活动商品列表
     *
     * @param pageNum
     * @param pageSize
     * @param id
     * @param proKey
     * @param mllStart
     * @param mllEnd
     * @param promFitsType
     * @param promFitsStart
     * @param promFitsEnd
     * @return
     */
    Result exportProductList(Integer pageNum, Integer pageSize, Integer id, String proKey, BigDecimal mllStart, BigDecimal mllEnd, Integer promFitsType, BigDecimal promFitsStart, BigDecimal promFitsEnd) throws IOException;

}
