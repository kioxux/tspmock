package com.gov.operate.dto.customer.order;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021-06-23 11:13
 */
@Data
public class CustomerOrderCloseVO {
    
    @NotNull(message = "主键不能为空")
    private Long id;
}
