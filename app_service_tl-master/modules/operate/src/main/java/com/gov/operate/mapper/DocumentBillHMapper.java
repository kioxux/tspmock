package com.gov.operate.mapper;

import com.gov.operate.dto.invoiceOptimiz.DocumentBillDetailsDTO;
import com.gov.operate.entity.DocumentBillH;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
public interface DocumentBillHMapper extends BaseMapper<DocumentBillH> {

    /**
     * 对账单据列表
     *
     * @param client
     * @param gdbNum
     * @return
     */
    List<DocumentBillDetailsDTO.DocumentBillHDTO> getDocumentBillHList(@Param("client") String client, @Param("gdbNum") String gdbNum);

    /**
     * 单据明细列表
     *
     * @param client 加盟商
     * @param gdbNum 对账单号
     * @param businessType 单据类型
     * @param matDnId 单据号
     * @return
     */
    List<DocumentBillDetailsDTO.DocumentBillHDTO.DocumentBillDDTO> getDocumentBillDList(@Param("client") String client, @Param("gdbNum") String gdbNum, @Param("businessType") String businessType, @Param("matDnId") String matDnId);

    /**
     * 单据发票明细列表
     *
     * @param client 加盟商
     * @param gdbhBusinessType 单据类型
     * @param gdbhMatDnId 单据号
     * @return
     */
    List<DocumentBillDetailsDTO.DocumentBillHDTO.DocumentInvoiceBillDTO> getDocumentBillInvoiceList(@Param("client") String client, @Param("businessType") String gdbhBusinessType, @Param("matDnId") String gdbhMatDnId);
}
