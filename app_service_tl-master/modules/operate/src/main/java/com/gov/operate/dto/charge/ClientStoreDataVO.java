package com.gov.operate.dto.charge;

import com.gov.operate.entity.StoreData;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ClientStoreDataVO extends StoreData {
    private String ficoEndingTime;

    private String ficoStartingTime;

    private String ficoLastDeadLine;

    private BigDecimal spPrice;

    private BigDecimal payAmount;

    private Integer isChange;
}
