package com.gov.operate.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc: 商品查询返参
 * @author: ZhangChi
 * @createTime: 2022/1/6 14:20
 */
@Data
public class ProductInfoVo {
    /**
     * 商品编码
     */
    private String proCode ;

    /**
     * 商品通用名
     */
    private String proCommonName ;

    /**
     * 商品名称
     */
    private String name ;

    /**
     * 助记码
     */
    private String pym ;

    /**
     * 规格
     */
    private String specs ;

    /**
     * 计量单位
     */
    private String unit ;

    /**
     * 剂型
     */
    private String form ;

    /**
     * 产地
     */
    private String proPlace ;

    /**
     * 生产企业
     */
    private String proFactoryName ;

    /**
     * 总部数量
     */
    private String kysl ;

    /**
     * 是否医保
     */
    private String ifMed ;

    /**
     * 零售价
     */
    private BigDecimal priceNormal ;

    /**
     * 配送价
     */
    private BigDecimal addAmt ;

}
