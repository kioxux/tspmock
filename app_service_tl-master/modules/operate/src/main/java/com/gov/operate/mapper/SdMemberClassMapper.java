package com.gov.operate.mapper;

import com.gov.operate.entity.SdMemberClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface SdMemberClassMapper extends BaseMapper<SdMemberClass> {

}
