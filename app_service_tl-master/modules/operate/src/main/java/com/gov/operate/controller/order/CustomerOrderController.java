package com.gov.operate.controller.order;

import com.gov.common.basic.JsonUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.log.Log;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.rsa.AesEncodeUtils;
import com.gov.operate.dto.customer.order.*;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.mapper.FranchiseeMapper;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhoushuai
 * @date 2021-06-21 16:04
 */
@Slf4j
@RestController
@RequestMapping("customer/order")
public class CustomerOrderController {

    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private FranchiseeMapper franchiseeMapper;


    private String getFullUrl(String relativePath) {
        return applicationConfig.getOrderServiceUrl() + relativePath;
    }

    private <T> Result postRequestOrderService(String url, T vo, String des) {
        Map<String, Object> param = new HashMap<>();
        param.put("text", AesEncodeUtils.encrypt(JsonUtils.beanToJson(vo)));
        log.info("调用[{}]开始,请求地址[{}],参数:{}", des, url, param);
        Mono<String> response = WebClient.create()
                .post()
                .uri(url)
                .body(BodyInserters.fromObject(param))
                .retrieve()
                .bodyToMono(String.class);
        String result = response.block();
        log.info("调用[{}]结束,返回值:{}", des, result);
        return JsonUtils.jsonToBean(result, Result.class);
    }


    @Log("工单创建")
    @ApiOperation(value = "工单创建")
    @PostMapping("saveCustomerOrder")
    public Result saveCustomerOrder(@RequestBody @Valid SaveOrderVO vo) {
        String relativePath = "order/customer/order/saveCustomerOrder";
        Franchisee franchisee = franchiseeMapper.selectById(vo.getGwoClient());
        if (ObjectUtils.isEmpty(franchisee)) {
            throw new CustomResultException("获取当前加盟商名失败");
        }
        vo.setGwoClientName(franchisee.getFrancName());
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单创建");
    }

    @Log("工单列表")
    @ApiOperation(value = "工单列表")
    @PostMapping("getCustomerOrderList")
    public Result getCustomerOrderList(@RequestBody @Valid GetCustomerOrderListVO vo) {
        String relativePath = "order/customer/order/getCustomerOrderList";
        vo.setPageNo(vo.getPageNum());
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单列表");
    }

    @Log("工单删除")
    @ApiOperation(value = "工单删除")
    @PostMapping("removeCustomerOrder")
    public Result removeCustomerOrder(@RequestBody @Valid RemoveCustomerOrderVO vo) {
        String relativePath = "order/customer/order/removeCustomerOrder";
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单删除");
    }

    @Log("工单详情")
    @ApiOperation(value = "工单详情")
    @PostMapping("getCustomerOrderDetails")
    public Result getCustomerOrderDetails(@RequestBody @Valid GetCustomerOrderDetailsVO vo) {
        String relativePath = "order/customer/order/getCustomerOrderDetails";
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单详情");
    }

    @Log("附件列表")
    @ApiOperation(value = "附件列表")
    @PostMapping("getCustomerOrderAttList")
    public Result getCustomerOrderAttList(@RequestBody @Valid GetCustomerOrderAttListVO vo) {
        String relativePath = "order/customer/order/getCustomerOrderAttList";
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单详情");
    }

    @Log("工单聊天记录")
    @ApiOperation(value = "工单聊天记录")
    @PostMapping("getCustomerOrderReplyList")
    public Result getCustomerOrderReplyList(@RequestBody @Valid GetCustomerOrderReplyListVO vo) {
        String relativePath = "order/customer/order/getCustomerOrderReplyList";
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单聊天记录");
    }

    @Log("消息发送")
    @ApiOperation(value = "消息发送")
    @PostMapping("customerOrderSendMessage")
    public Result customerOrderSendMessage(@RequestBody @Valid WorkOrderReplyVO vo) {
        String relativePath = "order/customer/order/customerOrderSendMessage";
        return postRequestOrderService(getFullUrl(relativePath), vo, "消息发送");
    }

    @Log("工单关闭")
    @ApiOperation(value = "工单关闭")
    @PostMapping("customerOrderClose")
    public Result customerOrderClose(@RequestBody @Valid CustomerOrderCloseVO vo) {
        String relativePath = "order/customer/order/customerOrderClose";
        return postRequestOrderService(getFullUrl(relativePath), vo, "工单关闭");
    }

    @Log("评分")
    @ApiOperation(value = "评分")
    @PostMapping("customerOrderScore")
    public Result customerOrderScore(@RequestBody @Valid CustomerOrderScoreVO vo) {
        String relativePath = "order/customer/order/customerOrderScore";
        return postRequestOrderService(getFullUrl(relativePath), vo, "评分");
    }

}
