package com.gov.operate.dto.dataImport.materialDoc;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.operate.entity.MaterialDocImp;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/4/26 15:21
 */
@Data
public class SelectImportDTO extends MaterialDocImp {

    /**
     * 创建人姓名
     */
    private String gmdiCreateUser;

    /**
     * 执行人姓名
     */
    private String gmdiExeUser;
}
