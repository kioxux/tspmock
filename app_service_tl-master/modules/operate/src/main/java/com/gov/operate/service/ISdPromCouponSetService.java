package com.gov.operate.service;

import com.gov.operate.entity.SdPromCouponSet;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface ISdPromCouponSetService extends SuperService<SdPromCouponSet> {

}
