package com.gov.operate.service;

import com.gov.operate.dto.FranchiseeDTO;
import com.gov.operate.dto.GetClientListVO;
import com.gov.operate.dto.franchiseeData.FranchiseeVO;
import com.gov.operate.entity.Franchisee;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-10
 */
public interface IFranchiseeService extends SuperService<Franchisee> {

    /**
     * 获取加盟列表
     */
    List<Franchisee> getClientList(GetClientListVO vo);

    void getSmsAuthenticationCode(FranchiseeVO loginOutData);

    FranchiseeDTO getFranchiseeById(FranchiseeVO inData);

    void insertFranchisee(FranchiseeVO inData);

    void updateFranchisee(FranchiseeVO inData);
}
