package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.UserRoleVO;
import com.gov.operate.entity.AuthorityUserRoles;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
public interface IAuthorityUserRolesService extends SuperService<AuthorityUserRoles> {

    Result getUserList(Integer gaurRoleId);

    Result addUserRole(UserRoleVO vo);

    Result userRoleList(Integer gaurRoleId);

    Result deleteUserRole(List<Integer> vo);

}
