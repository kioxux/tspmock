package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.invoice.*;
import com.gov.operate.entity.SalesInvoice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SalesInvoiceMapper extends BaseMapper<SalesInvoice> {

    /**
     * 销售开票查询
     *
     * @param page
     * @param vo
     * @return
     */
    IPage<SalesInvoicePageVO> selectSalesInvoiceByPage(Page page, @Param("vo") SalesInvoicePageDTO vo);

    /**
     * 获取当天最大单号
     *
     * @param currentDate
     * @return
     */
    Integer selectMaxId(@Param("currentDate") String currentDate);

    /**
     * 销售开票登记查询
     * @param page
     * @return
     */
    IPage<SalesInvoiceRegisterPageVO> selectSalesInvoiceRegisterByPage(Page<SalesInvoiceRegisterPageVO> page,@Param("vo") SalesInvoiceRegisterPageDTO vo);


    List<SalesInvoiceDetailItemDTO> selectRegistDetail(SalesInvoiceDetailDTO dto);

    SalesInvoiceRegisterPageVO selectSalesInvoiceRegisterTotalByPage(@Param("vo") SalesInvoiceRegisterPageDTO dto);
}
