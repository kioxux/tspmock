package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromCouponGrant;
import com.gov.operate.mapper.SdPromCouponGrantMapper;
import com.gov.operate.service.ISdPromCouponGrantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Service
public class SdPromCouponGrantServiceImpl extends ServiceImpl<SdPromCouponGrantMapper, SdPromCouponGrant> implements ISdPromCouponGrantService {

}
