package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectBillDetailsDTO {
    /**
     * 物料凭证号
     */
    private String matId;
    /**
     * 物料凭证年份
     */
    private String matYear;
    /**
     * 物料凭证行号
     */
    private String matLineNo;
    /**
     * 业务类型
     */
    private String businessType;
    /**
     * 单据编码
     */
    private String matDnId;
    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 商品名称
     */
    private String proName;

    /**
     * 应付金额
     */
    private BigDecimal lineRegistrationAmount;
    /**
     * 已付金额
     */
    private BigDecimal paymentAmountOfLine;
    /**
     * 本次付款金额
     */
    private BigDecimal paymentAmount;
    /**
     * 剩余金额
     */
    private BigDecimal chargeAmount;


}
