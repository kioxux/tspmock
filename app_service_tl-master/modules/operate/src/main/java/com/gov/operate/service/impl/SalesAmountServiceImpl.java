package com.gov.operate.service.impl;

import com.gov.operate.entity.SalesAmount;
import com.gov.operate.mapper.SalesAmountMapper;
import com.gov.operate.service.ISalesAmountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-02-08
 */
@Service
public class SalesAmountServiceImpl extends ServiceImpl<SalesAmountMapper, SalesAmount> implements ISalesAmountService {

}
