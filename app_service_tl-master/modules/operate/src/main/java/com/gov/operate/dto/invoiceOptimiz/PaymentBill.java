package com.gov.operate.dto.invoiceOptimiz;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.23
 */
@Data
public class PaymentBill {

    /**
     * 对账单号
     */
    private String num;

    /**
     * 供应商
     */
    private String supCode;

    /**
     * 供应商
     */
    private String supName;

    /**
     * 类型 1 单据对账 2 发票对账
     */
    private String type;

    /**
     * 类型
     */
    private String typeName;

    /**
     * 创建日期
     */
    private String createDate;

    /**
     * 创建人
     */
    private String createName;

    /**
     * 应付金额
     */
    private BigDecimal payAmt;

    /**
     * 已付金额
     */
    private BigDecimal alreadPayAmt;

    /**
     * 本次付款金额
     */
    private BigDecimal currPayAmt;

}
