package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class DelMarketingTemplateVO {

    @NotBlank(message = "模版id不能为空")
    private String smsId;
}
