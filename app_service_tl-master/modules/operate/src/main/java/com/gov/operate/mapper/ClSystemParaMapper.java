package com.gov.operate.mapper;

import com.gov.operate.entity.ClSystemPara;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 公司参数表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-06-02
 */
public interface ClSystemParaMapper extends BaseMapper<ClSystemPara> {
    ClSystemPara selectByPrimaryKey(@Param("client") String client, @Param("gcspId") String gcspId);

    void insertClSystemPara(ClSystemPara in);
}
