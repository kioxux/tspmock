package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectWarehousingDetailsDTO {
    private String id;
    /**
     * 类型
     */
    private String type;
    /**
     * 类型名称
     */
    private String typeName;
    /**
     * 单号
     */
    private String matDnId;
    private String matId;
    private String matYear;
    private String matLineNo;
    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 商品名
     */
    private String proName;
    private BigDecimal excludingTaxAmount;
    private BigDecimal rateBat;
    /**
     * 含税金额(总金额)
     */
    private BigDecimal totalAmount;
    /**
     * 已登记金额
     */
    private BigDecimal registeredAmount;
    private BigDecimal settlementAmount;
    private BigDecimal chargeAmount;
    /**
     * 数量
     */
    private BigDecimal matQty;

    /**
     * 已对账
     */
    private Integer selected;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 地点编号
     */
    private String siteCode;

    /**
     * 地点名称
     */
    private String siteName;

    /**
     * 供应商编码
     */
    private String supSelfCode;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 单据日期
     */
    private String matDate;

    /**
     * 业务员姓名
     */
    private String gssName;
}
