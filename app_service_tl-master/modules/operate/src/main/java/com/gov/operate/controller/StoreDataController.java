package com.gov.operate.controller;

import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.service.IStoreDataService;
import com.gov.operate.service.request.StoreSalesRankingRequest;
import com.gov.operate.service.request.StoreSalesStatisticsRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;

/**
 * <p>
 * 门店主数据信息表 前端控制器
 * </p>
 *
 * @author flynn
 * @since 2021-09-07
 */
@RestController
@Api(tags = "门店主数据信息表接口")
@RequestMapping("/storeData")
public class StoreDataController  {

    @Autowired
    private IStoreDataService iStoreDataService;

    @ApiOperation(value = "根据加盟商获取门店")
    @GetMapping(value = "getStoreList")
    public Result getStoreList(){
        return ResultUtil.success(iStoreDataService.getStoreList());
    }


    @ApiOperation(value = "门店销售排名统计")
    @PostMapping(value = "getStoreSalesRanking")
    public Result getStoreSalesRanking(@Valid @RequestBody StoreSalesRankingRequest request) throws ParseException {
        return ResultUtil.success(iStoreDataService.getStoreSalesRanking(request));
    }

//    @ApiOperation(value = "根据门店获取统计")
//    @GetMapping(value = "getStoreStatistics")
//    public Result getStoreStatistics(@Valid StoreSalesStatisticsRequest request) throws ParseException {
//        return ResultUtil.success(iStoreDataService.getStoreStatistics(request));
//    }


}

