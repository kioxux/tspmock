package com.gov.operate.dto;

import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SdDiscount;
import com.gov.operate.entity.SdMarketing;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetGsmSettingDetailDTO extends SdMarketing {

    /**
     * 电话通知条件JSON
     */
    private String gsmTelcondiJson;


    /**
     * 短信查询条件JSON
     */
    private String gsmSmscondiJson;

    /**
     * 折扣详情
     */
    List<SdDiscount> discountList;

    /**
     * 加盟商详情
     */
    List<Franchisee> clientList;
}
