package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdMarketing;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
public interface SdMarketingMapper extends BaseMapper<SdMarketing> {

    String getMaxNumMarketid();

    /**
     * @param page
     * @param vo
     * @return
     */
    IPage<GetGsmTaskListDTO> selectGsmTaskPage(Page<GetGsmTaskListDTO> page, @Param("vo") GsmTaskListVO vo);

    /**
     * 选品查询
     * @param productRequestVo
     * @return
     */
    IPage<ProductResponseVo> selectProductList(Page<SdMarketing> page,@Param("product") ProductRequestVo productRequestVo);

    /**
     * 获取活动列表
     * @param page
     * @param query
     * @return
     */
    IPage<GetGsmSettingListDTO> getGsmSettingList(Page<SdMarketing> page, @Param("ew") QueryWrapper<SdMarketing> query);

    /**
     * 推送加盟商列表
     */
    List<String> getClientList(@Param("gsmMarketid") String gsmMarketid);
}
