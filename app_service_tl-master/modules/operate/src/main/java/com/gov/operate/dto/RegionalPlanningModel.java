package com.gov.operate.dto;

import lombok.Data;

/**
 * @author Zhangchi
 * @since 2021/10/15/15:51
 */
@Data
public class RegionalPlanningModel {
    /**
     * id
     */
    private int Id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 区域编码
     */
    private String regionalCode;

    /**
     * 区域名称
     */
    private String regionalName;

    /**
     * 上级Id
     */
    private int parentId;
}
