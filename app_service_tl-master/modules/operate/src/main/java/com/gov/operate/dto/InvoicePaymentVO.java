package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class InvoicePaymentVO {

    @NotBlank(message = "发票号码不能为空")
    private String invoiceNum;
    @NotNull(message = "本次付款金额不能为空")
    private BigDecimal paymentAmount;

}
