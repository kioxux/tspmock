package com.gov.operate.service.popularize.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.bsaoc.PayUtils;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.dto.PayParams;
import com.gov.operate.dto.QueryPayStatus;
import com.gov.operate.dto.SavePayingResponseDTO;
import com.gov.operate.dto.charge.ClientStoreDataVO;
import com.gov.operate.dto.charge.PayableItemVO;
import com.gov.operate.dto.charge.PayableVO;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IMessageService;
import com.gov.operate.service.popularize.IPayingHeaderNewService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("iPayingHeaderNewService")
public class PayingHeaderNewServiceImpl extends ServiceImpl<PayingHeaderNewMapper, PayingHeaderNew> implements IPayingHeaderNewService {

    @Resource
    private PayingHeaderNewMapper payingHeaderNewMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private UserChargeMaintenanceHMapper userChargeMaintenanceHMapper;
    @Resource
    private UserChargeMaintenanceFreeMapper userChargeMaintenanceFreeMapper;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private PayingItemNewRemindMapper payingItemNewRemindMapper;
    @Resource
    private PayingItemNewMapper payingItemNewMapper;
    @Resource
    private PayingRemindMapper payingRemindMapper;
    @Resource
    private PayingRemindRecordMapper payingRemindRecordMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private FranchiseeMapper franchiseeMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private IMessageService messageService;

    @Override
    public Result getPayableList(String spId, Integer days, Integer payFreq, String client) {
        List<PayableItemVO> voList = new ArrayList<>();
        PayableVO payableVO = new PayableVO();
        TokenUser user = commonService.getLoginInfo();
        String clientQ = StringUtils.isNotBlank(client) ? client : user.getClient();
        List<PayingHeaderNew> oldList = payingHeaderNewMapper.selectList(new QueryWrapper<PayingHeaderNew>().eq("CLIENT", clientQ).orderByDesc("FICO_PAYING_DATE"));
        if (!CollectionUtils.isEmpty(oldList)) {
            payableVO.setLastPayDate(oldList.get(0).getFicoPayingDate());
        }
        List<PayingRemind> remainList = payingRemindMapper.selectList(new LambdaQueryWrapper<PayingRemind>().eq(PayingRemind::getClient, clientQ));
        if (!CollectionUtils.isEmpty(remainList)) {
            payableVO.setRemainAmount(remainList.get(0).getGprServiceCharge());
        }
        List<PayableItemVO> list = userChargeMaintenanceHMapper.getPayableList(spId, days, clientQ, payFreq);
        if (!CollectionUtils.isEmpty(list)) {
            UserChargeMaintenanceFree umf = userChargeMaintenanceFreeMapper.selectList(new QueryWrapper<UserChargeMaintenanceFree>().eq("SPH_ID", list.get(0).getSphId())).get(0);
            Integer payNum = new Integer(umf.getPayNum());
            String freeNum = umf.getFreeNum();
            list.forEach(s -> {
                UserChargeMaintenanceH userChargeMaintenanceH = userChargeMaintenanceHMapper.selectById(s.getSphId());
                s.setPayFreq(userChargeMaintenanceH.getSpPayFreq());
                payableVO.setPayFreq(userChargeMaintenanceH.getSpPayFreq());
                Integer spMode = s.getSpMode();
                BigDecimal price = BigDecimal.ZERO;
                BigDecimal priceReal = BigDecimal.ZERO;

                PayingItemNew payingItemNewLast = userChargeMaintenanceHMapper.selectFicoEndingTime(clientQ);

                // 每店每月
                if (spMode.equals(1)) {
                    List<ClientStoreDataVO> storeList = userChargeMaintenanceHMapper.getStoreVOList(null, clientQ, s.getId());
                    if (CollectionUtils.isEmpty(storeList)) {
                        throw new CustomResultException("服务包" + s.getSpName() + "未设置门店");
                    }
                    List<String> stoCodeList = storeList.stream().map(ClientStoreDataVO::getStoCode).collect(Collectors.toList());
                    List<PayingItemNew> stoStartDateList = userChargeMaintenanceHMapper.selectStoreStartDate(clientQ, stoCodeList, s.getRId());
                    for (ClientStoreDataVO clientStoreDataVO : storeList) {
                        if (StringUtils.isNotEmpty(clientStoreDataVO.getStoShortName())) {
                            clientStoreDataVO.setStoName(clientStoreDataVO.getStoShortName());
                        }
                        PayingItemNew payingItemNew = stoStartDateList.stream().filter(Objects::nonNull)
                                .filter(item -> clientStoreDataVO.getStoCode().equals(item.getFicoSiteCode())).findFirst().orElse(null);
                        // 开始日期
                        LocalDate sDate = LocalDate.now();
                        if (payingItemNew != null && StringUtils.isNotBlank(payingItemNew.getFicoEndingTime())) {
                            sDate = DateUtils.parseLocalDate(payingItemNew.getFicoEndingTime(), "yyyyMMdd");
                            clientStoreDataVO.setFicoLastDeadLine(sDate.minusDays(1).format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                        }
                        // 赠送结束日
                        Long days0 = null;
                        if (freeNum.endsWith("D")) {
                            Long dmToAdd = new Long(freeNum.substring(0, freeNum.length() - 1));
                            days0 = sDate.plusMonths(payNum).plusDays(dmToAdd).toEpochDay() - sDate.toEpochDay();
                        } else {
                            days0 = sDate.plusMonths(payNum + new Long(freeNum.substring(0, freeNum.length() - 1))).toEpochDay() - sDate.toEpochDay();
                        }
                        // 付款结束日期
                        LocalDate eDate = sDate.plusMonths(new Long(payNum));
                        clientStoreDataVO.setFicoEndingTime(eDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                        if (payingItemNewLast != null && StringUtils.isNotBlank(payingItemNewLast.getFicoEndingTime())) {
                            // 合同最大截止日
                            LocalDate lastDate = DateUtils.parseLocalDate(payingItemNewLast.getFicoEndingTime(), "yyyyMMdd");
                            // 合同最大日小于付款结束日，大等于开始日
                            if (
                                    DateUtils.formatLocalDate(lastDate).compareTo(DateUtils.formatLocalDate(sDate)) >= 0 &&
                                            DateUtils.formatLocalDate(lastDate).compareTo(DateUtils.formatLocalDate(eDate)) < 0
                            ) {
                                clientStoreDataVO.setFicoEndingTime(lastDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                                clientStoreDataVO.setIsChange(1);
                                // 新天数
                                Long days1 = lastDate.toEpochDay() - sDate.toEpochDay() + 1;
                                //原价/原天数*新天数
                                price = price.add(s.getSpPrice().divide
                                        (new BigDecimal(days0), 2, BigDecimal.ROUND_HALF_UP)
                                        .multiply(new BigDecimal(days1)).multiply(new BigDecimal(payNum)).setScale(2));
                                priceReal = priceReal.add(s.getSpPriceRealistic().divide
                                        (new BigDecimal(days0), 2, BigDecimal.ROUND_HALF_UP)
                                        .multiply(new BigDecimal(days1)).multiply(new BigDecimal(payNum)).setScale(2));
                                sDate = LocalDate.now();
                            } else {
                                price = price.add(s.getSpPrice().multiply(new BigDecimal(payNum)));
                                priceReal = priceReal.add(s.getSpPriceRealistic().multiply(new BigDecimal(payNum)));
                            }
                        } else {
                            price = price.add(s.getSpPrice().multiply(new BigDecimal(payNum)));
                            priceReal = priceReal.add(s.getSpPriceRealistic().multiply(new BigDecimal(payNum)));
                        }
                        clientStoreDataVO.setFicoStartingTime(sDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                        if (StringUtils.isNotEmpty(client)) {
                            clientStoreDataVO.setFicoStartingTime("");
                            clientStoreDataVO.setFicoEndingTime("");
                        }
                    }
                    s.setStoreList(storeList);
                } else if (spMode.equals(2)) {
                    //每月
                    price = price.add(s.getSpPrice().multiply(new BigDecimal(payNum)));
                    priceReal = priceReal.add(s.getSpPriceRealistic().multiply(new BigDecimal(payNum)));
                    List<PayingItemNew> stoStartDateList = userChargeMaintenanceHMapper.selectStoreStartDate(clientQ, null, s.getRId());
                    // 开始日期
                    LocalDate sDate = LocalDate.now();
                    if (!CollectionUtils.isEmpty(stoStartDateList)) {
                        PayingItemNew payingItemNew = stoStartDateList.get(0);
                        if (payingItemNew != null && StringUtils.isNotBlank(payingItemNew.getFicoEndingTime())) {
                            sDate = DateUtils.parseLocalDate(payingItemNew.getFicoEndingTime(), "yyyyMMdd");
                        }
                    }
                    // 付款结束日期
                    LocalDate eDate = sDate.plusMonths(new Long(payNum));
                    s.setStartingDate(sDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                    s.setEndingDate(eDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                }
                s.setReceivableAmount(price);
                s.setContractAmount(priceReal);
                s.setDiscountAmount(price.subtract(priceReal));
                voList.add(s);
            });
        }
        payableVO.setItemList(voList);
        return ResultUtil.success(payableVO);
    }

    @Override
    @Transactional
    public Result saveClientPay(PayableVO payableVO) {
        if (CollectionUtils.isEmpty(payableVO.getItemList())) {
            throw new CustomResultException("请选择收费项");
        } else {
            TokenUser user = commonService.getLoginInfo();
            BigDecimal payAmountTotal = payableVO.getPayAmount();
            BigDecimal discountAmountTotal = BigDecimal.ZERO;
            BigDecimal deductAmount = payableVO.getDeductAmount();
            BigDecimal payAmountTotalTmp = BigDecimal.ZERO;
            String lastDeadline = null;
            List<String> codeList = new ArrayList<>();
            List<PayableItemVO> packList = payableVO.getItemList();
            for (PayableItemVO pack : packList) {
                BigDecimal payAmount = pack.getContractAmount();
                discountAmountTotal = discountAmountTotal.add(pack.getDiscountAmount());
                payAmountTotalTmp = payAmountTotalTmp.add(payAmount);
                List<PayingItemNew> oldList = new ArrayList<>();
                if (CollectionUtils.isEmpty(pack.getStoreList())) {
                    oldList = payingItemNewMapper.selectList(new QueryWrapper<PayingItemNew>()
                            .eq("CLIENT", user.getClient())
                            .eq("SP_ID", pack.getSpId()).orderByDesc("FICO_ENDING_TIME"));
                    if (CollectionUtils.isEmpty(oldList)) {
                        continue;
                    }
                    lastDeadline = oldList.get(0).getFicoEndingTime();
                    if (!CollectionUtils.isEmpty(oldList) && pack.getStartingDate().compareTo(lastDeadline) <= 0) {
                        throw new CustomResultException(pack.getSpName() + " 开始日期不正确");
                    }
                } else {
                    for (ClientStoreDataVO clientStoreDataVO : pack.getStoreList()) {
                        oldList = payingItemNewMapper.selectList(new QueryWrapper<PayingItemNew>()
                                .eq("CLIENT", user.getClient())
                                .eq("FICO_SITE_CODE", clientStoreDataVO.getStoCode())
                                .eq("SP_ID", pack.getSpId()).orderByDesc("FICO_ENDING_TIME"));
                        if (CollectionUtils.isEmpty(oldList)) {
                            continue;
                        }
                        lastDeadline = oldList.get(0).getFicoEndingTime();
                        if (!CollectionUtils.isEmpty(oldList) && clientStoreDataVO.getFicoStartingTime().compareTo(lastDeadline) <= 0) {
                            throw new CustomResultException(pack.getSpName() + " 开始日期不正确");
                        }
                    }
                }

            }
            if (deductAmount != null && deductAmount.compareTo(BigDecimal.ZERO) > 0) {
                payAmountTotalTmp = payAmountTotalTmp.subtract(deductAmount);
            }
            if (!payAmountTotalTmp.equals(payAmountTotal)) {
                throw new CustomResultException("总额计算错误");
            }
            for (PayableItemVO pack : packList) {
                if (!CollectionUtils.isEmpty(pack.getStoreList())) {
                    for (ClientStoreDataVO storeData : pack.getStoreList()) {
                        codeList.add(storeData.getStoCode());
                    }
                }
            }
            // 校验 二维码有效期（5分钟）之内 相同付款主体不可再次付款
            List<String> listExit = payingItemNewMapper.listNotPaying(user.getClient(), codeList, CommonEnum.PaymentStatus.UNPAID.getCode(), applicationConfig.getC2b_expireTime());
            if (!listExit.isEmpty()) {
                throw new CustomResultException(ResultEnum.E0168, StringUtils.join(listExit.toString(), ","));
            }
            // 生成付款二维码
            String billNo = "10VJ" + PayUtils.getBillNo(applicationConfig.getC2b_billNoPre());
            PayParams payParams = new PayParams();
            payParams.setMsgId("0001");
            payParams.setTotalAmount(payAmountTotal.multiply(new BigDecimal(100)).intValue());
            payParams.setBillNo(billNo);

            String res = null;
            String billQRCode = "";
            String qrCodeId = "";
            String ficoId = "";
            String b2bpayUrl = "";
            if (payAmountTotal.compareTo(BigDecimal.ZERO) > 0) {
                //兼容老接口
                if (payableVO.getPayType() == null || payableVO.getPayType() == 0) {
                    //默认扫码支付
                    payableVO.setPayType(CommonEnum.PayType.SCAN.getCode());
                }
                //扫码支付
                if (CommonEnum.PayType.SCAN.getCode().equals(payableVO.getPayType())) {
                    try {
                        res = PayUtils.payV2(payParams, applicationConfig);
                        Map map = JSONObject.parseObject(res);
                        billQRCode = (String) map.get("billQRCode");
                        qrCodeId = (String) map.get("qrCodeId");
                        ficoId = billNo.substring(8);
                        if (StringUtils.isBlank(billQRCode)) {
                            throw new CustomResultException(ResultEnum.E0164);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new CustomResultException(ResultEnum.E0164);
                    }
                    //银联对公支付
                } else if (CommonEnum.PayType.GATEWAY.getCode().equals(payableVO.getPayType())) {
                    try {
                        payParams.setBankName(payableVO.getBankName());
                        payParams.setChnlNo(payableVO.getChnlNo());
                        b2bpayUrl = PayUtils.payB2B(payParams, applicationConfig);
                        ficoId = billNo.substring(8);
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new CustomResultException(ResultEnum.E0172);
                    }
                }
            }
            UserChargeMaintenanceH userChargeMaintenanceH = userChargeMaintenanceHMapper.selectById(packList.get(0).getSphId());
            PayingHeaderNew phn = new PayingHeaderNew();
            BeanUtils.copyProperties(userChargeMaintenanceH, phn);
            phn.setId(null);
            phn.setFicoPaymentBalanceAmount(deductAmount);
            phn.setSpId(userChargeMaintenanceH.getId());
            phn.setFicoRequestResult(res);
            phn.setFicoPaymentAmountTotal(payAmountTotal);
            phn.setFicoOrderDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            phn.setFicoOperator(user.getUserId());
            phn.setFicoPaymentStatus("0");
            phn.setFicoOrderType(1);
            if (payAmountTotal.compareTo(BigDecimal.ZERO) <= 0) {
                phn.setFicoPaymentStatus("1");
            }
            phn.setFicoInvoiceStatus("0");
            phn.setFicoType(payableVO.getPayType());
            phn.setFicoInvoiceType(payableVO.getInvoiceType());
            phn.setFicoId(ficoId);
            phn.setFicoClass(1);
            phn.setFicoSerialNumber(billNo);
            phn.setFicoContractNumbe(userChargeMaintenanceH.getSpContractCode());
            phn.setFicoPaymentHandleAmount(payableVO.getReceivableAmount());
            phn.setFicoPaymentDiscountAmount(discountAmountTotal);

            if (deductAmount != null && deductAmount.compareTo(BigDecimal.ZERO) > 0) {
                PayingRemindRecord prr = new PayingRemindRecord();
                prr.setGprrAmt(deductAmount.multiply(new BigDecimal(-1)));
                prr.setGprrTime(LocalDateTime.now());
                prr.setGprrInforce(0);
                if (payAmountTotal.compareTo(BigDecimal.ZERO) <= 0) {
                    prr.setGprrInforce(1);
                }
                prr.setClient(userChargeMaintenanceH.getClient());
                prr.setGprrType(-1);
                payingRemindRecordMapper.insert(prr);
                PayingRemind pr = payingRemindMapper.selectOne(new QueryWrapper<PayingRemind>().eq("CLIENT", userChargeMaintenanceH.getClient()));
                pr.setGprServiceCharge(pr.getGprServiceCharge().add(prr.getGprrAmt()));
                payingRemindMapper.updateById(pr);
                phn.setRemindRecordId(prr.getId());
            }
            payingHeaderNewMapper.insert(phn);
            //赠送记录
            List<UserChargeMaintenanceFree> freeList = userChargeMaintenanceFreeMapper.selectList
                    (new QueryWrapper<UserChargeMaintenanceFree>().eq("SPH_ID", packList.get(0).getSphId()));
            for (int i = 0; i < packList.size(); i++) {
                PayableItemVO pack = packList.get(i);
                PayingItemNew pin = new PayingItemNew();
                BeanUtils.copyProperties(pack, pin);
                BigDecimal amountAvgPack = payAmountTotal.divide(new BigDecimal(packList.size()), 2, BigDecimal.ROUND_HALF_DOWN);
                if (i == packList.size() - 1) {
                    amountAvgPack = payAmountTotal.subtract(amountAvgPack.multiply(new BigDecimal(packList.size() - 1)));
                }
                pin.setId(null);
                pin.setFicoStartingTime(pack.getStartingDate());
                pin.setFicoEndingTime(pack.getEndingDate());
                pin.setPid(phn.getId());
                pin.setFicoPayingstoreCode(pack.getPayingstoreCode());
                pin.setFicoPaymentHandleAmount(pack.getSpPrice());
                pin.setFicoClass(1);
                pin.setRId(pack.getRId());
                pin.setFicoLastDeadline(lastDeadline);
                pin.setFicoId(ficoId);
                Integer yearLimit = userChargeMaintenanceH.getSpLimit();
                String free = "";
                Integer payTimes = payingHeaderNewMapper.getPayTimes(pack.getSphId(), user.getClient(), pack.getSpId()).size();
                if (payTimes <= yearLimit) {
                    free = freeList.get(payTimes).getFreeNum();
                } else {
                    free = freeList.get(yearLimit).getFreeNum();
                }
                if (CollectionUtils.isEmpty(pack.getStoreList())) {
                    pin.setFicoPaymentAmount(amountAvgPack);
                    payingItemNewMapper.insert(pin);
                    if (!CollectionUtils.isEmpty(freeList)) {
                        pin.setFicoStartingTime((LocalDate.parse(pin.getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(1)
                                .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                        Long dmToAdd = new Long(free.substring(0, free.length() - 1));
                        pin.setFicoClass(2);
                        if (free.endsWith("M")) {
                            pin.setFicoEndingTime((LocalDate.parse(pin.getFicoStartingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusMonths(dmToAdd)
                                    .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                        } else if (free.endsWith("D")) {
                            pin.setFicoEndingTime((LocalDate.parse(pin.getFicoStartingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(dmToAdd)
                                    .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                        }
                        pin.setFicoPaymentAmount(BigDecimal.ZERO);
                        payingItemNewMapper.insert(pin);
                    }
                } else {
                    for (int j = 0; j < pack.getStoreList().size(); j++) {
                        ClientStoreDataVO storeData = pack.getStoreList().get(j);
                        BigDecimal amountAvgStore = amountAvgPack.divide(new BigDecimal(pack.getStoreList().size()), 2, BigDecimal.ROUND_HALF_DOWN);
                        if (j == pack.getStoreList().size() - 1) {
                            amountAvgStore = amountAvgPack.subtract(amountAvgStore.multiply(new BigDecimal(pack.getStoreList().size() - 1)));
                        }
                        List<PayingItemNewRemind> storeOldList = payingItemNewRemindMapper.selectList(new QueryWrapper<PayingItemNewRemind>()
                                .eq("CLIENT", user.getClient())
                                .eq("FICO_SITE_CODE", storeData.getStoCode())
                                .eq("SP_ID", pack.getSpId()).orderByDesc("FICO_ENDING_TIME"));
                        if (CollectionUtils.isEmpty(storeOldList)) {
                            pin.setFicoLastDeadline(null);
                        } else {
                            pin.setFicoLastDeadline((LocalDate.parse(storeOldList.get(0).getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(1)
                                    .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                        }
                        pin.setFicoStartingTime(storeData.getFicoStartingTime());
                        pin.setFicoEndingTime(storeData.getFicoEndingTime());
                        pin.setFicoSiteCode(storeData.getStoCode());
                        pin.setFicoPaymentAmount(amountAvgStore);
                        if (storeData.getPayAmount() != null) {
                            pin.setFicoPaymentAmount(storeData.getPayAmount());
                        }
                        pin.setFicoPaymentHandleAmount(pin.getFicoPaymentAmount());
                        pin.setFicoPayingstoreCode(storeData.getStoCode());
                        pin.setFicoPayingstoreName(storeData.getStoName());
                        pin.setFicoClass(1);
                        payingItemNewMapper.insert(pin);
                        if (!CollectionUtils.isEmpty(freeList) && (storeData.getIsChange() == null || storeData.getIsChange().intValue() != 1)) {
                            pin.setFicoStartingTime((LocalDate.parse(pin.getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(1)
                                    .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                            Long dmToAdd = new Long(free.substring(0, free.length() - 1));
                            pin.setFicoClass(2);
                            if (free.endsWith("M")) {
                                pin.setFicoEndingTime((LocalDate.parse(pin.getFicoStartingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusMonths(dmToAdd)
                                        .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                            } else if (free.endsWith("D")) {
                                pin.setFicoEndingTime((LocalDate.parse(pin.getFicoStartingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(dmToAdd)
                                        .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                            }
                            pin.setFicoPaymentAmount(BigDecimal.ZERO);
                            payingItemNewMapper.insert(pin);
                        }
                    }
                }
            }
            if (payAmountTotal.compareTo(BigDecimal.ZERO) <= 0) {
                List<PayingItemNew> itemList = payingItemNewMapper.selectDeadline(phn.getId());
                itemList.forEach(s -> {
                    PayingItemNewRemind payingItemNewRemind = new PayingItemNewRemind();
                    BeanUtils.copyProperties(s, payingItemNewRemind);
                    payingItemNewRemind.setId(null);
                    payingItemNewRemind.setSpSend(0);
                    payingItemNewRemind.setFicoStringTime(s.getFicoStartingTime());
                    if (StringUtils.isNotEmpty(payingItemNewRemind.getFicoEndingTime())) {
                        payingItemNewRemind.setFicoMsgDate(LocalDate.parse(payingItemNewRemind.getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).minusDays(applicationConfig.getRemindDay())
                                .format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                    }
                    payingItemNewRemindMapper.insert(payingItemNewRemind);
                });
            }
            SavePayingResponseDTO resDto = new SavePayingResponseDTO();
            resDto.setClient(user.getClient());
            if (payAmountTotal.compareTo(BigDecimal.ZERO) <= 0) {
                resDto.setStatus("1");
                return ResultUtil.success(resDto);
            }
            resDto.setStatus("0");
            resDto.setBillQRCode(billQRCode);
            resDto.setFicoId(ficoId);
            resDto.setOrderUrl(b2bpayUrl);
            return ResultUtil.success(resDto);
        }
    }

    @Override
    public Result getPayStatus(String ficoId, String client) {
        Map resultMap = new HashMap();
        List<PayingHeaderNew> headerList = payingHeaderNewMapper.selectList(new QueryWrapper<PayingHeaderNew>()
                .eq("CLIENT", client).eq("FICO_ID", ficoId).eq("FICO_CLASS", 1));
        if (CollectionUtils.isEmpty(headerList)) {
            resultMap.put("status", CommonEnum.PaymentStatus.UNPAID.getCode());
            return ResultUtil.success(resultMap);
        }
        PayingHeaderNew header = headerList.get(0);
        if (CommonEnum.PaymentStatus.SUCCESS.getCode().equals(header.getFicoPaymentStatus())) {
            resultMap.put("status", header.getFicoPaymentStatus());
            return ResultUtil.success(resultMap);
        }

        // 构造查询参数
        QueryPayStatus query = new QueryPayStatus();
        query.setMsgId("001");
        query.setBillNo("10VJ" + applicationConfig.getC2b_billNoPre() + ficoId);
        query.setBillDate(DateUtils.getCurrentDateStr());
        query.setFicoType(header.getFicoType());

        String responseStr = null;
        try {
            // 发起查询
            responseStr = PayUtils.queryPayStatus(query, applicationConfig);
        } catch (Exception e) {
            resultMap.put("status", CommonEnum.PaymentStatus.UNPAID.getCode());
            return ResultUtil.success(resultMap);
        }
        // 支付状态
        String status = ((Map<String, String>) JSON.parse(responseStr)).get("billStatus");
        // 如果支付成功 保存 并返回
        if (CommonEnum.PayResult.PAID.getCode().equals(status)) {
            header.setFicoId(ficoId);
            // 支付状态
            header.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
            // 支付日期
            header.setFicoPayingDate(DateUtils.getCurrentDateTimeStr());
            // 支付结果
            header.setFicoPayResult(responseStr);
            payingHeaderNewMapper.updateById(header);

            List<PayingItemNew> itemList = payingItemNewMapper.selectDeadline(header.getId());
            if (header.getFicoPaymentBalanceAmount().compareTo(BigDecimal.ZERO) > 0
                    && header.getRemindRecordId() != null) {
                PayingRemindRecord record = payingRemindRecordMapper.selectById(header.getRemindRecordId());
                record.setGprrInforce(1);
                payingRemindRecordMapper.updateById(record);
            }
            itemList.forEach(s -> {
                PayingItemNewRemind payingItemNewRemind = new PayingItemNewRemind();
                BeanUtils.copyProperties(s, payingItemNewRemind);
                payingItemNewRemind.setId(null);
                payingItemNewRemind.setSpSend(0);
                payingItemNewRemind.setFicoStringTime(s.getFicoStartingTime());
                if (StringUtils.isNotEmpty(payingItemNewRemind.getFicoEndingTime())) {
                    payingItemNewRemind.setFicoMsgDate(LocalDate.parse(payingItemNewRemind.getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).minusDays(applicationConfig.getRemindDay())
                            .format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                }
                payingItemNewRemindMapper.insert(payingItemNewRemind);
            });
            resultMap.put("status", CommonEnum.PaymentStatus.SUCCESS.getCode());
            return ResultUtil.success(resultMap);
        }
        resultMap.put("status", CommonEnum.PaymentStatus.UNPAID.getCode());
        return ResultUtil.success(resultMap);
    }

    /**
     * 支付回调事务
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void payCallback() throws Exception {
        log.info("支付结果查询任务开始。");
        // 支付中的订单
        List<PayingHeaderNew> list = payingHeaderNewMapper.selectList(
                new QueryWrapper<PayingHeaderNew>().eq("FICO_PAYMENT_STATUS", "0").eq("FICO_CLASS", 1)
                        .gt("FICO_ORDER_DATE", LocalDateTime.now().minusMinutes(applicationConfig.getC2b_expireTime()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));

        if (CollectionUtils.isEmpty(list)) {
            log.info("支付结果查询任务结束。未查询到订单。");
            return;
        }

        // 遍历支付状态
        for (PayingHeaderNew gaiaPayingHeader : list) {
            // 扫码支付回调
            if (gaiaPayingHeader.getFicoType() != null && (
                    gaiaPayingHeader.getFicoType().intValue() == CommonEnum.PayType.SCAN.getCode()
                            || gaiaPayingHeader.getFicoType().intValue() == CommonEnum.PayType.GATEWAY.getCode())) {
                // 支付结果查询
                String payResult = PayUtils.query(applicationConfig, gaiaPayingHeader.getFicoSerialNumber(), gaiaPayingHeader.getFicoType());
                // 支付状态
                String status = ((Map<String, String>) JSON.parse(payResult)).get("billStatus");
                // 如果支付成功 保存 并返回
                if (CommonEnum.PayResult.PAID.getCode().equals(status)) {
                    // 付款订单号
                    gaiaPayingHeader.setFicoId(gaiaPayingHeader.getFicoId());
                    // 支付状态
                    gaiaPayingHeader.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
                    // 支付日期
                    gaiaPayingHeader.setFicoPayingDate(DateUtils.getCurrentDateTimeStr());
                    // 支付结果
                    gaiaPayingHeader.setFicoPayResult(payResult);
                    payingHeaderNewMapper.updateById(gaiaPayingHeader);
                    if (gaiaPayingHeader.getRemindRecordId() != null) {
                        PayingRemindRecord payingRemindRecord = payingRemindRecordMapper.selectById(gaiaPayingHeader.getRemindRecordId());
                        if (payingRemindRecord != null) {
                            payingRemindRecord.setGprrInforce(1);
                            payingRemindRecordMapper.updateById(payingRemindRecord);
                            PayingRemind pr = payingRemindMapper.selectList(new QueryWrapper<PayingRemind>().eq("CLIENT", payingRemindRecord.getClient())).get(0);
                            pr.setGprServiceCharge(pr.getGprServiceCharge().add(payingRemindRecord.getGprrAmt()));
                            payingRemindMapper.updateById(pr);
                        }
                    }
                }
            }
        }
        log.info("支付结果查询任务结束。");
    }

    /**
     * 支付回调
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void payResult(HttpServletRequest httpServletRequest) {
        Map<String, String[]> properties = httpServletRequest.getParameterMap();
        log.info("支付回调结果：{}", JsonUtils.beanToJson(properties));

        String[] billNo = properties.get("billNo");
        if (ObjectUtils.isEmpty(billNo)) {
            return;
        }
        // 单号
        String ficoId = billNo[0].substring(8);
        // 付款状态
        String status = properties.get("billStatus")[0];
        List<PayingHeaderNew> hList = payingHeaderNewMapper.selectList(new QueryWrapper<PayingHeaderNew>().eq("FICO_ID", ficoId));
        PayingHeaderNew header = new PayingHeaderNew();
        if (CollectionUtils.isEmpty(hList)) {
            return;
        } else {
            // 支付成功
            header = hList.get(0);
            if (CommonEnum.PayResult.PAID.getCode().equals(status)) {
                header.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
                header.setFicoPayingDate(DateUtils.getCurrentDateTimeStr());
                header.setFicoPayResult(JsonUtils.beanToJson(properties));
                payingHeaderNewMapper.updateById(header);
                List<PayingItemNew> itemList = payingItemNewMapper.selectDeadline(header.getId());
                if (header.getFicoPaymentBalanceAmount().compareTo(BigDecimal.ZERO) > 0
                        && header.getRemindRecordId() != null) {
                    PayingRemindRecord record = payingRemindRecordMapper.selectById(header.getRemindRecordId());
                    record.setGprrInforce(1);
                    payingRemindRecordMapper.updateById(record);
                    PayingRemind pm = payingRemindMapper.selectList(new QueryWrapper<PayingRemind>().eq("CLIENT", header.getClient())).get(0);
                    pm.setGprServiceCharge(pm.getGprServiceCharge().add(record.getGprrAmt()));
                    payingRemindMapper.updateById(pm);
                }
                itemList.forEach(s -> {
                    PayingItemNewRemind payingItemNewRemind = new PayingItemNewRemind();
                    BeanUtils.copyProperties(s, payingItemNewRemind);
                    payingItemNewRemind.setId(null);
                    payingItemNewRemind.setFicoStringTime(s.getFicoStartingTime());
                    if (StringUtils.isNotEmpty(payingItemNewRemind.getFicoEndingTime())) {
                        payingItemNewRemind.setFicoMsgDate(LocalDate.parse(payingItemNewRemind.getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).minusDays(applicationConfig.getRemindDay())
                                .format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                    }
                    payingItemNewRemindMapper.insert(payingItemNewRemind);
                });
                return;
            }
            // 支付失败
            header.setFicoPaymentStatus(CommonEnum.PaymentStatus.FAILE.getCode());
            header.setFicoPayResult(JsonUtils.beanToJson(properties));
            payingHeaderNewMapper.updateById(header);
        }
    }

    @Override
    @Transactional
    public Result saveClientPayMaintain(PayableVO payableVO) {
        if (CollectionUtils.isEmpty(payableVO.getItemList())) {
            throw new CustomResultException("请选择收费项");
        } else {
            TokenUser user = commonService.getLoginInfo();
            BigDecimal payAmountTotalTmp = BigDecimal.ZERO;
            List<PayableItemVO> packList = payableVO.getItemList();
            String lastDeadline = "";
            for (PayableItemVO pack : packList) {
                BigDecimal payAmount = pack.getPayAmount();
                if (payAmount == null) {
                    throw new CustomResultException("请输入金额");
                }
                payAmountTotalTmp = payAmountTotalTmp.add(payAmount);
                List<PayingItemNew> oldList = new ArrayList<>();
                if (CollectionUtils.isEmpty(pack.getStoreList())) {
                    oldList = payingItemNewMapper.selectList(new QueryWrapper<PayingItemNew>()
                            .eq("CLIENT", user.getClient())
                            .eq("SP_ID", pack.getSpId()).orderByDesc("FICO_ENDING_TIME"));
                    if (CollectionUtils.isEmpty(oldList)) {
                        continue;
                    }
                    lastDeadline = oldList.get(0).getFicoEndingTime();
                    if (!CollectionUtils.isEmpty(oldList) && pack.getStartingDate().compareTo(lastDeadline) <= 0) {
                        throw new CustomResultException(pack.getSpName() + " 开始日期不正确");
                    }
                } else {
                    for (ClientStoreDataVO clientStoreDataVO : pack.getStoreList()) {
                        oldList = payingItemNewMapper.selectList(new QueryWrapper<PayingItemNew>()
                                .eq("CLIENT", user.getClient())
                                .eq("FICO_SITE_CODE", clientStoreDataVO.getStoCode())
                                .eq("SP_ID", pack.getSpId()).orderByDesc("FICO_ENDING_TIME"));
                        if (CollectionUtils.isEmpty(oldList)) {
                            continue;
                        }
                        lastDeadline = oldList.get(0).getFicoEndingTime();
                        if (!CollectionUtils.isEmpty(oldList) && clientStoreDataVO.getFicoStartingTime().compareTo(lastDeadline) <= 0) {
                            throw new CustomResultException(pack.getSpName() + " 开始日期不正确");
                        }
                    }
                }
            }
            UserChargeMaintenanceH userChargeMaintenanceH = userChargeMaintenanceHMapper.selectById(packList.get(0).getSphId());
            PayingHeaderNew phn = new PayingHeaderNew();
            BeanUtils.copyProperties(userChargeMaintenanceH, phn);
            phn.setId(null);
            phn.setFicoPaymentAmountTotal(payAmountTotalTmp);
            phn.setSpId(userChargeMaintenanceH.getId());
            phn.setFicoPaymentHandleAmount(payAmountTotalTmp);
            phn.setFicoOrderType(1);
            phn.setFicoOrderDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            phn.setFicoOperator(user.getUserId());
            phn.setFicoPaymentStatus("1");
            phn.setFicoInvoiceStatus("0");
            phn.setFicoType(payableVO.getPayType());
            phn.setFicoInvoiceType(payableVO.getFicoInvoiceType());
            phn.setFicoClass(2);
            phn.setFicoContractNumbe(userChargeMaintenanceH.getSpContractCode());
            payingHeaderNewMapper.insert(phn);

            for (PayableItemVO pack : packList) {
                if (pack.getFicoStartingTime().compareTo(pack.getFicoEndingTime()) > 0) {
                    throw new CustomResultException("开始日期不能大于结束日期");
                }
                PayingItemNew pin = new PayingItemNew();
                BeanUtils.copyProperties(pack, pin);
                pin.setId(null);
                String lastDl = pin.getFicoLastDeadline();
                if (lastDl != null) {
                    pin.setFicoStartingTime(LocalDate.parse(lastDl, DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(1)
                            .format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                }
                pin.setPid(phn.getId());
                pin.setFicoPayingstoreCode(pack.getPayingstoreCode());
                pin.setFicoPaymentHandleAmount(pack.getPayAmount());
                pin.setFicoPaymentAmount(pack.getPayAmount());
                pin.setFicoClass(1);
                pin.setFicoStartingTime(pack.getFicoStartingTime());
                pin.setFicoEndingTime(pack.getFicoEndingTime());
                if (CollectionUtils.isEmpty(pack.getStoreList())) {
                    payingItemNewMapper.insert(pin);
                } else {
                    for (int j = 0; j < pack.getStoreList().size(); j++) {
                        ClientStoreDataVO storeData = pack.getStoreList().get(j);
                        if (storeData.getFicoStartingTime().compareTo(storeData.getFicoEndingTime()) > 0) {
                            throw new CustomResultException("开始日期不能大于结束日期");
                        }
                        BigDecimal amountAvgStore = pack.getPayAmount().divide(new BigDecimal(pack.getStoreList().size()), 2, BigDecimal.ROUND_HALF_DOWN);
                        if (j == pack.getStoreList().size() - 1) {
                            amountAvgStore = pack.getPayAmount().subtract(amountAvgStore.multiply(new BigDecimal(pack.getStoreList().size() - 1)));
                        }
                        List<PayingItemNewRemind> storeOldList = payingItemNewRemindMapper.selectList(new QueryWrapper<PayingItemNewRemind>()
                                .eq("CLIENT", user.getClient())
                                .eq("FICO_SITE_CODE", storeData.getStoCode())
                                .eq("SP_ID", pack.getSpId()).orderByDesc("FICO_ENDING_TIME"));
                        if (CollectionUtils.isEmpty(storeOldList)) {
                            pin.setFicoLastDeadline(null);
                        } else {
                            pin.setFicoLastDeadline((LocalDate.parse(storeOldList.get(0).getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).plusDays(1)
                                    .format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                        }
                        pin.setFicoStartingTime(storeData.getFicoStartingTime());
                        pin.setFicoEndingTime(storeData.getFicoEndingTime());
                        pin.setFicoSiteCode(storeData.getStoCode());
                        pin.setFicoPaymentAmount(amountAvgStore);
                        if (storeData.getPayAmount() != null) {
                            pin.setFicoPaymentAmount(storeData.getPayAmount());
                        }
                        pin.setFicoPayingstoreCode(storeData.getStoCode());
                        pin.setFicoPayingstoreName(storeData.getStoName());
                        pin.setFicoClass(1);
                        payingItemNewMapper.insert(pin);
                    }
                }

                PayingItemNewRemind payingItemNewRemind = new PayingItemNewRemind();
                BeanUtils.copyProperties(pin, payingItemNewRemind);
                payingItemNewRemind.setId(null);
                payingItemNewRemind.setSpSend(0);
                payingItemNewRemind.setFicoStringTime(pin.getFicoStartingTime());
                if (StringUtils.isNotEmpty(payingItemNewRemind.getFicoEndingTime())) {
                    payingItemNewRemind.setFicoMsgDate(LocalDate.parse(payingItemNewRemind.getFicoEndingTime(), DateTimeFormatter.ofPattern("yyyyMMdd")).minusDays(applicationConfig.getRemindDay())
                            .format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                }
                payingItemNewRemindMapper.insert(payingItemNewRemind);
            }
            SavePayingResponseDTO resDto = new SavePayingResponseDTO();
            resDto.setClient(user.getClient());
            resDto.setStatus("1");
            return ResultUtil.success(resDto);
        }
    }

    @Override
    public void payExpire() {
        // 支付超时的订单
        List<PayingHeaderNew> listExpire = payingHeaderNewMapper.selectList(
                new QueryWrapper<PayingHeaderNew>().eq("FICO_PAYMENT_STATUS", "0").eq("FICO_CLASS", 1)
                        .lt("FICO_ORDER_DATE", LocalDateTime.now().minusMinutes(applicationConfig.getC2b_expireTime()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
        if (CollectionUtils.isEmpty(listExpire)) {
            log.info("支付结果查询任务结束。未查询到订单。");
            return;
        }
        log.info("支付超时订单：{}", JSONObject.toJSONString(listExpire));
        for (PayingHeaderNew payingHeaderNew : listExpire) {
            payingHeaderNew.setFicoPaymentStatus(CommonEnum.PaymentStatus.FAILE.getCode());
            payingHeaderNewMapper.updateById(payingHeaderNew);
            if (payingHeaderNew.getRemindRecordId() != null) {
                PayingRemindRecord prr = payingRemindRecordMapper.selectById(payingHeaderNew.getRemindRecordId());
                PayingRemind pr = payingRemindMapper.selectOne(new QueryWrapper<PayingRemind>().eq("CLIENT", payingHeaderNew.getClient()));
                pr.setGprServiceCharge(pr.getGprServiceCharge().add(prr.getGprrAmt().multiply(new BigDecimal(-1))));
                payingRemindMapper.updateById(pr);
            }

        }
    }

    @Override
    @Transactional
    public Result closeStore(String client, String stoCode, String closeDate) {
        List<StoreData> storeList = storeDataMapper.selectList(
                new QueryWrapper<StoreData>().eq("CLIENT", client)
                        .eq("STO_CODE", stoCode));
        if (CollectionUtils.isEmpty(storeList)) {
            throw new CustomResultException("未找到对应门店：" + stoCode);
        } else {
            StoreData sd = storeList.get(0);
            BigDecimal amount = payingItemNewMapper.selectPayForCloseStore(client, stoCode, closeDate);
            if (amount.compareTo(BigDecimal.ZERO) > 0) {
                PayingRemindRecord prr = new PayingRemindRecord();
                prr.setGprrAmt(amount);
                prr.setGprrTime(LocalDateTime.now());
                prr.setGprrInforce(1);
                prr.setClient(client);
                prr.setGprrType(1);
                payingRemindRecordMapper.insert(prr);
                List<PayingRemind> prList = payingRemindMapper.selectList(
                        new QueryWrapper<PayingRemind>().eq("CLIENT", client)
                );
                if (CollectionUtils.isEmpty(prList)) {
                    PayingRemind pr = new PayingRemind();
                    pr.setClient(client);
                    pr.setGprServiceCharge(amount);
                    payingRemindMapper.insert(pr);
                } else {
                    PayingRemind pr = prList.get(0);
                    pr.setGprServiceCharge(pr.getGprServiceCharge().add(amount));
                    payingRemindMapper.updateById(pr);
                }
            }
            return ResultUtil.success();
        }
    }

    @Override
    public void payReminder() {
        log.info("门店付款推送通知开始。");
        // 存在需要通知的加盟商
        List<String> list = payingItemNewMapper.selectPayReminder();
        if (CollectionUtils.isEmpty(list)) {
            log.info("门店付款推送通知结束。没有需要发送的加盟商");
            return;
        }
        for (String client : list) {
            QueryWrapper<Franchisee> franchiseeQueryWrapper = new QueryWrapper<>();
            franchiseeQueryWrapper.eq("CLIENT", client);
            Franchisee franchisee = franchiseeMapper.selectOne(franchiseeQueryWrapper);
            // 加盟商未查询到
            if (franchisee == null) {
                continue;
            }
            String userId = "";
            // 委托人账号
            if (com.gov.common.basic.StringUtils.isNotBlank(franchisee.getFrancAss())) {
                userId = franchisee.getFrancAss();
                QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
                userDataQueryWrapper.eq("CLIENT", client);
                userDataQueryWrapper.eq("USER_ID", franchisee.getFrancAss());
                UserData user = userDataMapper.selectOne(userDataQueryWrapper);
                if (user != null) {
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(client);
                    messageParams.setUserId(user.getUserId());
                    messageParams.setId(CommonEnum.gmtId.MSG00001.getCode());
                    messageService.sendMessage(messageParams);
                    PayingItemNewRemind upd = new PayingItemNewRemind();
                    upd.setSpSend(1);
                    payingItemNewRemindMapper.update(upd, new UpdateWrapper<PayingItemNewRemind>()
                            .eq("CLIENT", client).eq("FICO_MSG_DATE", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                }
            }
            // 法人
            if (com.gov.common.basic.StringUtils.isNotBlank(franchisee.getFrancLegalPerson()) && !userId.equals(franchisee.getFrancLegalPerson())) {
                QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
                userDataQueryWrapper.eq("CLIENT", client);
                userDataQueryWrapper.eq("USER_ID", franchisee.getFrancLegalPerson());
                UserData user = userDataMapper.selectOne(userDataQueryWrapper);
                if (user != null) {
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(client);
                    messageParams.setUserId(user.getUserId());
                    messageParams.setId(CommonEnum.gmtId.MSG00001.getCode());
                    messageService.sendMessage(messageParams);
                    PayingItemNewRemind upd = new PayingItemNewRemind();
                    upd.setSpSend(1);
                    payingItemNewRemindMapper.update(upd, new UpdateWrapper<PayingItemNewRemind>()
                            .eq("CLIENT", client).eq("FICO_MSG_DATE", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
                }
            }
        }
    }
}
