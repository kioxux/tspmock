package com.gov.operate.dto;

import com.gov.common.entity.dataImport.PromGiftCondsProduct;
import com.gov.common.entity.dataImport.PromSeriesProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromGiftCondsProductImportDto extends PromGiftCondsProduct {
    private String proSelfCode;

    private String proCommonname;

    private String proName;

    private String proSpecs;

    private String proFactoryName;
}
