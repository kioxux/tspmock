package com.gov.operate.dto;

import com.gov.operate.entity.Feedback;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class FeedbackDTO extends Feedback {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 用户名
     */
    private String userNam;
}
