package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_BUSINESS")
@ApiModel(value="ProductBusiness对象", description="")
public class ProductBusiness extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "商品自编码")
    @TableField("PRO_SELF_CODE")
    private String proSelfCode;

    @ApiModelProperty(value = "商品编码")
    @TableField("PRO_CODE")
    private String proCode;

    @ApiModelProperty(value = "匹配状态")
    @TableField("PRO_MATCH_STATUS")
    private String proMatchStatus;

    @ApiModelProperty(value = "通用名称")
    @TableField("PRO_COMMONNAME")
    private String proCommonname;

    @ApiModelProperty(value = "商品描述")
    @TableField("PRO_DEPICT")
    private String proDepict;

    @ApiModelProperty(value = "助记码")
    @TableField("PRO_PYM")
    private String proPym;

    @ApiModelProperty(value = "商品名")
    @TableField("PRO_NAME")
    private String proName;

    @ApiModelProperty(value = "规格")
    @TableField("PRO_SPECS")
    private String proSpecs;

    @ApiModelProperty(value = "计量单位")
    @TableField("PRO_UNIT")
    private String proUnit;

    @ApiModelProperty(value = "剂型")
    @TableField("PRO_FORM")
    private String proForm;

    @ApiModelProperty(value = "细分剂型")
    @TableField("PRO_PARTFORM")
    private String proPartform;

    @ApiModelProperty(value = "最小剂量（以mg/ml计算）")
    @TableField("PRO_MINDOSE")
    private String proMindose;

    @ApiModelProperty(value = "总剂量（以mg/ml计算）")
    @TableField("PRO_TOTALDOSE")
    private String proTotaldose;

    @ApiModelProperty(value = "国际条形码1")
    @TableField("PRO_BARCODE")
    private String proBarcode;

    @ApiModelProperty(value = "国际条形码2")
    @TableField("PRO_BARCODE2")
    private String proBarcode2;

    @ApiModelProperty(value = "批准文号分类")
    @TableField("PRO_REGISTER_CLASS")
    private String proRegisterClass;

    @ApiModelProperty(value = "批准文号")
    @TableField("PRO_REGISTER_NO")
    private String proRegisterNo;

    @ApiModelProperty(value = "批准文号批准日期")
    @TableField("PRO_REGISTER_DATE")
    private String proRegisterDate;

    @ApiModelProperty(value = "批准文号失效日期")
    @TableField("PRO_REGISTER_EXDATE")
    private String proRegisterExdate;

    @ApiModelProperty(value = "商品分类")
    @TableField("PRO_CLASS")
    private String proClass;

    @ApiModelProperty(value = "商品分类描述")
    @TableField("PRO_CLASS_NAME")
    private String proClassName;

    @ApiModelProperty(value = "成分分类")
    @TableField("PRO_COMPCLASS")
    private String proCompclass;

    @ApiModelProperty(value = "成分分类描述")
    @TableField("PRO_COMPCLASS_NAME")
    private String proCompclassName;

    @ApiModelProperty(value = "处方类别")
    @TableField("PRO_PRESCLASS")
    private String proPresclass;

    @ApiModelProperty(value = "生产企业代码")
    @TableField("PRO_FACTORY_CODE")
    private String proFactoryCode;

    @ApiModelProperty(value = "生产企业")
    @TableField("PRO_FACTORY_NAME")
    private String proFactoryName;

    @ApiModelProperty(value = "商标")
    @TableField("PRO_MARK")
    private String proMark;

    @ApiModelProperty(value = "品牌标识名")
    @TableField("PRO_BRAND")
    private String proBrand;

    @ApiModelProperty(value = "品牌区分")
    @TableField("PRO_BRAND_CLASS")
    private String proBrandClass;

    @ApiModelProperty(value = "保质期")
    @TableField("PRO_LIFE")
    private String proLife;

    @ApiModelProperty(value = "保质期单位")
    @TableField("PRO_LIFE_UNIT")
    private String proLifeUnit;

    @ApiModelProperty(value = "上市许可持有人")
    @TableField("PRO_HOLDER")
    private String proHolder;

    @ApiModelProperty(value = "进项税率")
    @TableField("PRO_INPUT_TAX")
    private String proInputTax;

    @ApiModelProperty(value = "销项税率")
    @TableField("PRO_OUTPUT_TAX")
    private String proOutputTax;

    @ApiModelProperty(value = "药品本位码")
    @TableField("PRO_BASIC_CODE")
    private String proBasicCode;

    @ApiModelProperty(value = "税务分类编码")
    @TableField("PRO_TAX_CLASS")
    private String proTaxClass;

    @ApiModelProperty(value = "管制特殊分类")
    @TableField("PRO_CONTROL_CLASS")
    private String proControlClass;

    @ApiModelProperty(value = "生产类别")
    @TableField("PRO_PRODUCE_CLASS")
    private String proProduceClass;

    @ApiModelProperty(value = "贮存条件")
    @TableField("PRO_STORAGE_CONDITION")
    private String proStorageCondition;

    @ApiModelProperty(value = "商品仓储分区")
    @TableField("PRO_STORAGE_AREA")
    private String proStorageArea;

    @ApiModelProperty(value = "长（以MM计算）")
    @TableField("PRO_LONG")
    private String proLong;

    @ApiModelProperty(value = "宽（以MM计算）")
    @TableField("PRO_WIDE")
    private String proWide;

    @ApiModelProperty(value = "高（以MM计算）")
    @TableField("PRO_HIGH")
    private String proHigh;

    @ApiModelProperty(value = "中包装量")
    @TableField("PRO_MID_PACKAGE")
    private String proMidPackage;

    @ApiModelProperty(value = "大包装量")
    @TableField("PRO_BIG_PACKAGE")
    private String proBigPackage;

    @ApiModelProperty(value = "启用电子监管码")
    @TableField("PRO_ELECTRONIC_CODE")
    private String proElectronicCode;

    @ApiModelProperty(value = "生产经营许可证号")
    @TableField("PRO_QS_CODE")
    private String proQsCode;

    @ApiModelProperty(value = "最大销售量")
    @TableField("PRO_MAX_SALES")
    private String proMaxSales;

    @ApiModelProperty(value = "说明书代码")
    @TableField("PRO_INSTRUCTION_CODE")
    private String proInstructionCode;

    @ApiModelProperty(value = "说明书内容")
    @TableField("PRO_INSTRUCTION")
    private String proInstruction;

    @ApiModelProperty(value = "国家医保品种")
    @TableField("PRO_MED_PRODCT")
    private String proMedProdct;

    @ApiModelProperty(value = "国家医保品种编码")
    @TableField("PRO_MED_PRODCTCODE")
    private String proMedProdctcode;

    @ApiModelProperty(value = "生产国家")
    @TableField("PRO_COUNTRY")
    private String proCountry;

    @ApiModelProperty(value = "产地")
    @TableField("PRO_PLACE")
    private String proPlace;

    @ApiModelProperty(value = "可服用天数")
    @TableField("PRO_TAKE_DAYS")
    private String proTakeDays;

    @ApiModelProperty(value = "用法用量")
    @TableField("PRO_USAGE")
    private String proUsage;

    @ApiModelProperty(value = "禁忌说明")
    @TableField("PRO_CONTRAINDICATION")
    private String proContraindication;

    @ApiModelProperty(value = "商品状态")
    @TableField("PRO_STATUS")
    private String proStatus;

    @ApiModelProperty(value = "商品定位")
    @TableField("PRO_POSITION")
    private String proPosition;

    @ApiModelProperty(value = "禁止销售")
    @TableField("PRO_NO_RETAIL")
    private String proNoRetail;

    @ApiModelProperty(value = "禁止采购")
    @TableField("PRO_NO_PURCHASE")
    private String proNoPurchase;

    @ApiModelProperty(value = "禁止配送")
    @TableField("PRO_NO_DISTRIBUTED")
    private String proNoDistributed;

    @ApiModelProperty(value = "禁止退厂")
    @TableField("PRO_NO_SUPPLIER")
    private String proNoSupplier;

    @ApiModelProperty(value = "禁止退仓")
    @TableField("PRO_NO_DC")
    private String proNoDc;

    @ApiModelProperty(value = "禁止调剂")
    @TableField("PRO_NO_ADJUST")
    private String proNoAdjust;

    @ApiModelProperty(value = "禁止批发")
    @TableField("PRO_NO_SALE")
    private String proNoSale;

    @ApiModelProperty(value = "禁止请货")
    @TableField("PRO_NO_APPLY")
    private String proNoApply;

    @ApiModelProperty(value = "是否拆零")
    @TableField("PRO_IFPART")
    private String proIfpart;

    @ApiModelProperty(value = "拆零单位")
    @TableField("PRO_PART_UINT")
    private String proPartUint;

    @ApiModelProperty(value = "拆零比例")
    @TableField("PRO_PART_RATE")
    private String proPartRate;

    @ApiModelProperty(value = "采购单位")
    @TableField("PRO_PURCHASE_UNIT")
    private String proPurchaseUnit;

    @ApiModelProperty(value = "采购比例")
    @TableField("PRO_PURCHASE_RATE")
    private String proPurchaseRate;

    @ApiModelProperty(value = "销售单位")
    @TableField("PRO_SALE_UNIT")
    private String proSaleUnit;

    @ApiModelProperty(value = "销售比例")
    @TableField("PRO_SALE_RATE")
    private String proSaleRate;

    @ApiModelProperty(value = "最小订货量")
    @TableField("PRO_MIN_QTY")
    private BigDecimal proMinQty;

    @ApiModelProperty(value = "是否医保")
    @TableField("PRO_IF_MED")
    private String proIfMed;

    @ApiModelProperty(value = "销售级别")
    @TableField("PRO_SLAE_CLASS")
    private String proSlaeClass;

    @ApiModelProperty(value = "限购数量")
    @TableField("PRO_LIMIT_QTY")
    private BigDecimal proLimitQty;

    @ApiModelProperty(value = "中药规格")
    @TableField("PRO_TCM_SPECS")
    private String proTcmSpecs;

    @ApiModelProperty(value = "中药批准文号")
    @TableField("PRO_TCM_REGISTER_NO")
    private String proTcmRegisterNo;

    @ApiModelProperty(value = "中药生产企业代码")
    @TableField("PRO_TCM_FACTORY_CODE")
    private String proTcmFactoryCode;

    @ApiModelProperty(value = "中药产地")
    @TableField("PRO_TCM_PLACE")
    private String proTcmPlace;

    @ApiModelProperty(value = "含麻最大配货量")
    @TableField("PRO_MAX_QTY")
    private BigDecimal proMaxQty;

    @ApiModelProperty(value = "按中包装量倍数配货")
    @TableField("PRO_PACKAGE_FLAG")
    private String proPackageFlag;

    @ApiModelProperty(value = "是否重点养护")
    @TableField("PRO_KEY_CARE")
    private String proKeyCare;

    @ApiModelProperty(value = "固定货位")
    @TableField("PRO_FIX_BIN")
    private String proFixBin;

    @TableField("PRO_CREATE_DATE")
    private String proCreateDate;


}
