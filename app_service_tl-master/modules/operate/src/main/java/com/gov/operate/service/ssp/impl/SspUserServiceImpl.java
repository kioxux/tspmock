package com.gov.operate.service.ssp.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.JsonUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.ssp.SspUserDTO;
import com.gov.operate.dto.ssp.SspUserPageDTO;
import com.gov.operate.dto.ssp.SspUserPageVO;
import com.gov.operate.dto.ssp.SspUserSupplierDTO;
import com.gov.operate.entity.SspLog;
import com.gov.operate.entity.SspUser;
import com.gov.operate.entity.SspUserChain;
import com.gov.operate.entity.SspUserSupplier;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ssp.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SspUserServiceImpl extends ServiceImpl<SspUserMapper, SspUser> implements ISspUserService {

    @Resource
    private CommonService commonService;

    @Resource
    private ISspUserSupplierService sspUserSupplierService;

    @Resource
    private SupplierSalesmanMapper supplierSalesmanMapper;

    @Resource
    private SspUserSupplierMapper sspUserSupplierMapper;

    @Resource
    private SspUserChainMapper sspUserChainMapper;

    @Resource
    private ISspUserChainService sspUserChainService;

    @Resource
    private ISspUserProductService sspUserProductService;

    @Resource
    private ISspMenuChainService sspMenuChainService;

    @Resource
    private SspLogMapper sspLogMapper;


    /**
     * 流向授权用户列表
     *
     * @param sspUserPageDTO
     * @return
     */
    @Override
    public Result selectUserByPage(SspUserPageDTO sspUserPageDTO) {
        TokenUser currentUser = commonService.getLoginInfo();
        LocalDate nowLocalDate = LocalDate.now();
        Date date = Date.from(nowLocalDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        sspUserPageDTO.setCurrentDate(date);
        sspUserPageDTO.setClient(currentUser.getClient());
        Page<SspUserPageVO> page = new Page<>(sspUserPageDTO.getPageNum(), sspUserPageDTO.getPageSize());
        IPage<SspUserPageVO> userPageVOIPage = baseMapper.selectUserByPage(page, sspUserPageDTO);
        userPageVOIPage.getRecords().forEach(t -> {
            if (StrUtil.isNotBlank(t.getSupName())) {
                t.setSupNames(Arrays.asList(t.getSupName().split(",")));
            }
            LocalDate startDate = t.getExpiryStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate ednDate = t.getExpiryEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if (t.getStatus() == 0 && nowLocalDate.isBefore(startDate) || nowLocalDate.isAfter(ednDate)) {
                // 失效
                t.setStatus(-1);
            }
        });
        return ResultUtil.success(userPageVOIPage);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addUser(SspUserDTO sspUserDTO) {
        TokenUser currentUser = commonService.getLoginInfo();
        // 验证手机号是否已存在
        int count = baseMapper.selectUserMobileExist(currentUser.getClient(), sspUserDTO.getMobile(), null);
        if (count > 0) {
            throw new CustomResultException("业务员已存在");
        }

        SspUser user = baseMapper.selectOne(new LambdaQueryWrapper<SspUser>().eq(SspUser::getMobile, sspUserDTO.getMobile()));
        if (user == null) {
            user = new SspUser();
            BeanUtils.copyProperties(sspUserDTO, user);
            user.setCreateBy(currentUser.getUserId());
            user.setCreateTime(new Date());
            // 正常
            user.setStatus(0);
            // 未删除
            user.setIsDelete(0);
            baseMapper.insert(user);
        }


        if (sspUserDTO.getSupplierList() != null && sspUserDTO.getSupplierList().size() > 0) {
            SspUser finalUser = user;
            List<SspUserSupplier> supList = sspUserDTO.getSupplierList().stream().map(t -> {
                SspUserSupplier sup = new SspUserSupplier();
                BeanUtils.copyProperties(t, sup);
                sup.setUserId(finalUser.getId());
                sup.setCreateBy(currentUser.getUserId());
                sup.setCreateTime(new Date());
                sup.setExpiryStartDate(sspUserDTO.getExpiryStartDate());
                sup.setExpiryEndDate(sspUserDTO.getExpiryEndDate());
                // 正常
                sup.setStatus(0);
                // 未删除
                sup.setIsDelete(0);
                return sup;
            }).collect(Collectors.toList());
            sspUserSupplierService.saveBatch(supList);
        }
        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        log.setContent(StrUtil.format("添加用户-{}", user.getMobile()));
        log.setJson(JsonUtils.beanToJson(sspUserDTO));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);

        return ResultUtil.success();
    }

    @Override
    public Result getSupSalesmanInfo(String mobile) {
        if (StrUtil.isBlank(mobile)) {
            throw new CustomResultException("手机号不能为空");
        }
        TokenUser currentUser = commonService.getLoginInfo();

        // 验证手机号是否已存在
        int count = baseMapper.selectUserMobileExist(currentUser.getClient(), mobile, null);
        if (count > 0) {
            throw new CustomResultException("业务员已存在");
        }

        SspUserDTO sspUserDTO = supplierSalesmanMapper.selectSalesmanSupplierListByMobile(currentUser.getClient(), mobile);
        return ResultUtil.success(sspUserDTO);
    }

    @Override
    public Result getUserInfo(Long userId) {
        if (userId == null) {
            throw new CustomResultException("用户不存在");
        }
        SspUserDTO sspUserDTO = new SspUserDTO();
        SspUser user = baseMapper.selectById(userId);
        BeanUtils.copyProperties(user, sspUserDTO);
        sspUserDTO.setSupplierList(getUserSupplierList(userId));
        return ResultUtil.success(sspUserDTO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateUser(SspUserDTO sspUserDTO) {
        if (sspUserDTO.getId() == null) {
            throw new CustomResultException("用户不存在");
        }
        TokenUser currentUser = commonService.getLoginInfo();
        // 验证手机号是否已存在
        int count = baseMapper.selectUserMobileExist(currentUser.getClient(), sspUserDTO.getMobile(), sspUserDTO.getId());
        if (count > 0) {
            throw new CustomResultException("业务员已存在");
        }


        SspUser user = new SspUser();
        BeanUtils.copyProperties(sspUserDTO, user);
        user.setUpdateBy(currentUser.getUserId());
        user.setUpdateTime(new Date());
        baseMapper.updateById(user);

        if (sspUserDTO.getSupplierList() != null && sspUserDTO.getSupplierList().size() > 0) {
            // 主体
            List<SspUserChain> chainList = sspUserChainMapper.getSupplierChainList(user.getId(), currentUser.getClient());

            // 根据用户ID删除
            sspUserSupplierMapper.delete(
                    new LambdaQueryWrapper<SspUserSupplier>().eq(SspUserSupplier::getUserId, user.getId())
                            .eq(SspUserSupplier::getClient, currentUser.getClient())
            );

            List<SspUserChain> delChainList = new ArrayList<>();
            // 批量插入
            List<SspUserSupplier> supList = sspUserDTO.getSupplierList().stream().map(t -> {
                // 匹配删除元素

                chainList.removeIf(a -> {
                    if(a.getCreditCode().equals(t.getCreditCode())){
                        delChainList.add(a);
                        return true;
                    }
                   return false;
                });

                SspUserSupplier sup = new SspUserSupplier();
                BeanUtils.copyProperties(t, sup);
                sup.setUserId(user.getId());
                sup.setCreateBy(currentUser.getUserId());
                sup.setStatus(0);
                sup.setExpiryStartDate(sspUserDTO.getExpiryStartDate());
                sup.setExpiryEndDate(sspUserDTO.getExpiryEndDate());
                sup.setCreateTime(new Date());
                // 未删除
                sup.setIsDelete(0);
                return sup;
            }).collect(Collectors.toList());

            Set<String> keySet = chainList.stream().filter(t ->
                            delChainList.stream().noneMatch(a -> a.getChainHead().equals(t.getChainHead())))
                    .collect(Collectors.groupingBy(SspUserChain::getChainHead)).keySet();
            if (keySet.size() > 0) {
                // 主体
                sspUserChainMapper.deleteChainHead(user.getId(), currentUser.getClient(), keySet);
                // 品种
                sspUserProductService.deleteChainHead(user.getId(), currentUser.getClient(), keySet);
                // 报表
                sspMenuChainService.deleteChainHead(user.getId(), currentUser.getClient(), keySet);
            }

            // 更新主体状态，有效期
            SspUserChain sspUserChain = new SspUserChain();
            sspUserChain.setStatus(0);
            sspUserChain.setExpiryStartDate(sspUserDTO.getExpiryStartDate());
            sspUserChain.setExpiryEndDate(sspUserDTO.getExpiryEndDate());
            sspUserChainMapper.update(sspUserChain, new LambdaQueryWrapper<SspUserChain>()
                    .eq(SspUserChain::getClient, currentUser.getClient())
                    .eq(SspUserChain::getUserId, sspUserDTO.getId()));

            sspUserSupplierService.saveBatch(supList);
        }

        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        log.setContent(StrUtil.format("更新用户-{}", user.getMobile()));
        log.setJson(JsonUtils.beanToJson(sspUserDTO));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);

        return ResultUtil.success();
    }

    /**
     * 连锁列表
     *
     * @param userId
     * @return
     */
    @Override
    public Result getChainList(Long userId) {
        TokenUser currentUser = commonService.getLoginInfo();
        List<SspUserChain> chainList = sspUserChainMapper.getChainList(userId, currentUser.getClient());
        return ResultUtil.success(chainList);
    }

    /**
     * 用户主体列表
     *
     * @param userId
     * @return
     */
    @Override
    public Result getUserChainList(Long userId) {
        TokenUser currentUser = commonService.getLoginInfo();
        return ResultUtil.success(sspUserChainMapper.selectList(
                new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, userId)
                        .eq(SspUserChain::getClient, currentUser.getClient())));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateChain(List<SspUserChain> chainList, Long userId) {
        TokenUser currentUser = commonService.getLoginInfo();
        List<SspUserChain> userChainList = sspUserChainMapper.selectList(new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, userId)
                .eq(SspUserChain::getClient, currentUser.getClient()));

        sspUserChainMapper.delete(new LambdaQueryWrapper<SspUserChain>().eq(SspUserChain::getUserId, userId)
                .eq(SspUserChain::getClient, currentUser.getClient()));
        if (CollUtil.isNotEmpty(chainList)) {
            // 获取加盟商 生效日期 状态
            SspUserSupplier sspUserSupplier = sspUserSupplierMapper.selectListByClient(userId, currentUser.getClient());

            if (sspUserSupplier == null) {
                throw new CustomResultException("未绑定供应商");
            }
            chainList.forEach(t -> {
                userChainList.removeIf(a -> a.getChainHead().equals(t.getChainHead()));
                t.setUserId(userId);
                t.setCreateBy(currentUser.getUserId());
                t.setCreateTime(new Date());
                t.setIsDelete(0);
                t.setStatus(sspUserSupplier.getStatus());
                t.setExpiryStartDate(sspUserSupplier.getExpiryStartDate());
                t.setExpiryEndDate(sspUserSupplier.getExpiryEndDate());
            });
            sspUserChainService.saveBatch(chainList);
        }

        if (userChainList.size() > 0) {
            Set<String> keySet = userChainList.stream().map(SspUserChain::getChainHead).collect(Collectors.toSet());
            // 品种
            sspUserProductService.deleteChainHead(userId, currentUser.getClient(), keySet);
            // 报表
            sspMenuChainService.deleteChainHead(userId, currentUser.getClient(), keySet);
        }

        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        log.setContent(StrUtil.format("授权主体变更为：{}",
                chainList.stream().map(SspUserChain::getChainName).collect(Collectors.joining())));
        log.setJson(JsonUtils.beanToJson(chainList));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);
        return ResultUtil.success();
    }

    @Override
    public List<SspUserSupplierDTO> getUserSupplierList(Long userId) {
        TokenUser currentUser = commonService.getLoginInfo();
        List<SspUserSupplierDTO> supplierDTOList = new ArrayList<>();
        List<SspUserSupplier> supList = sspUserSupplierMapper.selectList(
                new LambdaQueryWrapper<SspUserSupplier>().eq(SspUserSupplier::getUserId, userId)
                        .eq(SspUserSupplier::getClient, currentUser.getClient())
        );
        if (supList.size() > 0) {
            supplierDTOList = supList.stream().map(t -> {
                SspUserSupplierDTO dto = new SspUserSupplierDTO();
                BeanUtils.copyProperties(t, dto);
                return dto;
            }).collect(Collectors.toList());
        }
        return supplierDTOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateStatus(SspUserDTO sspUserDTO) {
        TokenUser currentUser = commonService.getLoginInfo();

        SspUserSupplier userSupplier = new SspUserSupplier();
        userSupplier.setUserId(sspUserDTO.getId());
        userSupplier.setClient(currentUser.getClient());
        userSupplier.setStatus(sspUserDTO.getStatus());
        userSupplier.setUpdateBy(currentUser.getUserId());
        userSupplier.setUpdateTime(new Date());
        sspUserSupplierMapper.updateUserStatus(userSupplier);

        // 连锁状态更新
        SspUserChain chain = new SspUserChain();
        chain.setUserId(sspUserDTO.getId());
        chain.setClient(currentUser.getClient());
        chain.setStatus(sspUserDTO.getStatus());
        chain.setUpdateBy(currentUser.getUserId());
        chain.setUpdateTime(new Date());
        sspUserChainMapper.updateUserStatus(chain);

        SspLog log = new SspLog();
        log.setOperateName(currentUser.getLoginName());
        log.setOperateMobile(currentUser.getUserTel());
        String status = "解冻";
        if (userSupplier.getStatus() == 1) {
            status = "冻结";
        }
        log.setContent(StrUtil.format("加盟商：{},用户{},状态变更为：{}", sspUserDTO.getMobile(), status));
        log.setJson(JsonUtils.beanToJson(sspUserDTO));
        log.setCreateBy(currentUser.getUserId());
        log.setCreateTime(new Date());
        sspLogMapper.insert(log);

        return ResultUtil.success();
    }

    @Override
    public Result getSupplierList(String mobile) {
        if (StrUtil.isBlank(mobile)) {
            throw new CustomResultException("手机号不能为空");
        }
        TokenUser currentUser = commonService.getLoginInfo();
        SspUserDTO sspUserDTO = supplierSalesmanMapper.selectSalesmanSupplierListByMobile(currentUser.getClient(), mobile);
        return ResultUtil.success(sspUserDTO != null ? sspUserDTO.getSupplierList() : null);
    }

}
