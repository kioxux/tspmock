package com.gov.operate.service;

import com.gov.operate.entity.ProductComponent;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-28
 */
public interface IProductComponentService extends SuperService<ProductComponent> {

}
