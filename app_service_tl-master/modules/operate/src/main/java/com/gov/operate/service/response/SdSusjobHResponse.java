package com.gov.operate.service.response;

import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Data

public class SdSusjobHResponse extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "任务ID")
    private String susJobid;

    @ApiModelProperty(value = "任务名称")
    private String susJobname;

    @ApiModelProperty(value = "任务门店")
    private String susJobbr;

    @ApiModelProperty(value = "创建时间")
    private String susTime;

    @ApiModelProperty(value = "计划开始时间")
    private String susBegin;

    @ApiModelProperty(value = "计划开始时间中文")
    private String susBeginStr;

    @ApiModelProperty(value = "计划结束时间")
    private String susEnd;

    @ApiModelProperty(value = "计划结束时间中文")
    private String susEndStr;

    @ApiModelProperty(value = "会员数量")
    private BigDecimal susJobperson;

    @ApiModelProperty(value = "进度")
    private String susSche;

    @ApiModelProperty(value = "状态")
    private String susStatus;

    @ApiModelProperty(value = "维系说明")
    private String susExplain;

    @ApiModelProperty(value = "任务门店类型")
    private String susType;

    @ApiModelProperty(value = "任务员工")
    private String susEmp;

    @ApiModelProperty(value = "维系话术")
    private String susContent;

    @ApiModelProperty(value = "推送日期")
    private String susPushDate;

    @ApiModelProperty(value = "推送时间")
    private String susPushTime;

    @ApiModelProperty(value = "推送状态")
    private String susPushStatus;

    @ApiModelProperty(value = "0.未查看1.进行中")
    private String susTaskType;


    @ApiModelProperty(value = "")
    private String susTaskTypeStr;

    @ApiModelProperty(value = "任务的状态编码")
    private String susTaskTypeCode;

    @ApiModelProperty(value = "总任务数量")
    private Integer totalTaskCount;

    @ApiModelProperty(value = "查看任务数量")
    private Integer taskCount;

    @ApiModelProperty(value = "时间格式化")
    private String date;
}
