package com.gov.operate.controller.prom;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.ISdPromGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.02
 */
@Api(tags = "促销商品组")
@RestController
@RequestMapping("sdPromGroup")
public class SdPromGroupController {

    @Resource
    private ISdPromGroupService sdPromGroupService;

    @Log("促销商品组列表")
    @ApiOperation(value = "促销商品组列表")
    @PostMapping("getSdPromGroupList")
    public Result getSdPromGroupList(@RequestJson("pageNum") Integer pageNum, @RequestJson("pageSize") Integer pageSize,
                                     @RequestJson(value = "groupName", required = false) String groupName,
                                     @RequestJson(value = "proSelfCodes", required = false) String proSelfCodes) {
        return sdPromGroupService.getSdPromGroupList(pageNum, pageSize, groupName, proSelfCodes);
    }

    @Log("促销商品组新增")
    @ApiOperation(value = "促销商品组新增")
    @PostMapping("addSdPromGroup")
    public Result addSdPromGroup(@RequestJson(value = "groupName", name = "商品组名称") String groupName) {
        return sdPromGroupService.addSdPromGroup(groupName);
    }

    @Log("促销商品组编辑")
    @ApiOperation(value = "促销商品组编辑")
    @PostMapping("editSdPromGroup")
    public Result editSdPromGroup(@RequestJson(value = "groupId", name = "商品组编号") String groupId,
                                  @RequestJson(value = "groupName", name = "商品组名称") String groupName) {
        return sdPromGroupService.editSdPromGroup(groupId, groupName);
    }

    @Log("促销商品组删除")
    @ApiOperation(value = "促销商品组删除")
    @PostMapping("delSdPromGroup")
    public Result delSdPromGroup(@RequestJson(value = "groupId", name = "商品组编号") String groupId) {
        return sdPromGroupService.delSdPromGroup(groupId);
    }

    @Log("促销商品组下拉框")
    @ApiOperation(value = "促销商品组下拉框")
    @PostMapping("getGroupProList")
    public Result getGroupProList() {
        return sdPromGroupService.getGroupProList();
    }
}
