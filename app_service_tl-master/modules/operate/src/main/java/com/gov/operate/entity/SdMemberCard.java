package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_CARD")
@ApiModel(value="SdMemberCard对象", description="")
public class SdMemberCard extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员ID")
    @TableField("GSMBC_MEMBER_ID")
    private String gsmbcMemberId;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSMBC_CARD_ID")
    private String gsmbcCardId;

    @ApiModelProperty(value = "所属店号")
    @TableField("GSMBC_BR_ID")
    private String gsmbcBrId;

    @ApiModelProperty(value = "渠道")
    @TableField("GSMBC_CHANNEL")
    private String gsmbcChannel;

    @ApiModelProperty(value = "卡类型")
    @TableField("GSMBC_CLASS_ID")
    private String gsmbcClassId;

    @ApiModelProperty(value = "当前积分")
    @TableField("GSMBC_INTEGRAL")
    private String gsmbcIntegral;

    @ApiModelProperty(value = "最后积分日期")
    @TableField("GSMBC_INTEGRAL_LASTDATE")
    private String gsmbcIntegralLastdate;

    @ApiModelProperty(value = "清零积分日期")
    @TableField("GSMBC_ZERO_DATE")
    private String gsmbcZeroDate;

    @ApiModelProperty(value = "新卡创建日期")
    @TableField("GSMBC_CREATE_DATE")
    private String gsmbcCreateDate;

    @ApiModelProperty(value = "类型")
    @TableField("GSMBC_TYPE")
    private String gsmbcType;

    @ApiModelProperty(value = "openID")
    @TableField("GSMBC_OPEN_ID")
    private String gsmbcOpenId;

    @ApiModelProperty(value = "卡状态")
    @TableField("GSMBC_STATUS")
    private String gsmbcStatus;

    @ApiModelProperty(value = "开卡员工")
    @TableField("GSMBC_CREATE_SALER")
    private String gsmbcOpenCardName;
}
