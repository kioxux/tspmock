package com.gov.operate.service.ssp;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.ssp.SspUserDTO;
import com.gov.operate.dto.ssp.SspUserPageDTO;
import com.gov.operate.dto.ssp.SspUserSupplierDTO;
import com.gov.operate.entity.SspUser;
import com.gov.operate.entity.SspUserChain;

import java.util.List;

public interface ISspUserService extends SuperService<SspUser> {

    /**
     * 流向授权用户列表
     *
     * @param sspUserPageDTO
     * @return
     */
    Result selectUserByPage(SspUserPageDTO sspUserPageDTO);

    /**
     * 新增用户
     *
     * @param sspUserDTO
     * @return
     */
    Result addUser(SspUserDTO sspUserDTO);

    /**
     * 业务员信息
     *
     * @param mobile
     * @return
     */
    Result getSupSalesmanInfo(String mobile);

    /**
     * 获取授权人信息
     *
     * @param userId
     * @return
     */
    Result getUserInfo(Long userId);

    /**
     * 更新用户
     *
     * @param sspUserDTO
     * @return
     */
    Result updateUser(SspUserDTO sspUserDTO);

    /**
     * 连锁列表
     * @param userId
     * @return
     */
    Result getChainList(Long userId);

    /**
     * 用户主体列表
     * @param userId
     * @return
     */
    Result getUserChainList(Long userId);

    /**
     * 用户连锁绑定
     * @param chainList
     * @param userId
     * @return
     */
    Result updateChain(List<SspUserChain> chainList, Long userId);

    /**
     * 获取用户绑定的供应商
     * @param userId
     * @return
     */
    List<SspUserSupplierDTO> getUserSupplierList(Long userId);

    /**
     * 更新状态
     * @param sspUserDTO
     * @return
     */
    Result updateStatus(SspUserDTO sspUserDTO);

    Result getSupplierList(String mobile);
}
