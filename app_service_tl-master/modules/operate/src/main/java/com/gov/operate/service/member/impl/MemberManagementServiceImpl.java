package com.gov.operate.service.member.impl;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.operate.dto.member.*;
import com.gov.operate.entity.SdSusjobD;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.MemberManagementMapper;
import com.gov.operate.mapper.SdSusjobDMapper;
import com.gov.operate.mapper.SdSusjobHMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.member.MemberManagementService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Service
public class MemberManagementServiceImpl implements MemberManagementService {

    @Autowired
    private CommonService commonService;

    @Autowired
    private MemberManagementMapper memberManagementMapper;
    @Resource
    private SdSusjobHMapper sdSusjobHMapper;
    @Resource
    private SdSusjobDMapper sdSusjobDMapper;
    @Override
    public List<MemberInfoDTO> getMemberListByNum(String mobile) {
        TokenUser user = commonService.getLoginInfo();
        return memberManagementMapper.queryMemberListByMobile(user.getClient(), mobile);
    }



    @Override
    public MemberInfoDetailsDTO getMemberInfoById(String gsmbMemberId,String jobId) {
        TokenUser user = commonService.getLoginInfo();
        MemberInfoDetailsDTO memberInfoById = memberManagementMapper.queryMemberInfoById(user.getClient(), gsmbMemberId);
        if (!ObjectUtils.isEmpty(memberInfoById)){
            if (!StringUtils.isEmpty(memberInfoById.getSusFlg())){
                String[] split = memberInfoById.getSusFlg().split(",");
                memberInfoById.setSusFlgList(Arrays.asList(split));
            }
            if (StrUtil.isNotBlank(jobId)) {
                LambdaQueryWrapper<SdSusjobD> lambdaQueryWrapper = new LambdaQueryWrapper<>();
                lambdaQueryWrapper
                        .eq(SdSusjobD::getClient, user.getClient())
                        .eq(SdSusjobD::getSusCardId, memberInfoById.getGsmbcCardId())
                        .eq(SdSusjobD::getSusJobid, jobId);
                List<SdSusjobD> susJobDList = this.sdSusjobDMapper.selectList(lambdaQueryWrapper);
                if (CollUtil.isNotEmpty(susJobDList)) {
                    SdSusjobD sdSusjobD = susJobDList.get(0);
                    //最近回访时间
                    memberInfoById.setSusFeedbacktime(sdSusjobD.getSusFeedbacktime());
                    if (StrUtil.isNotBlank(sdSusjobD.getSusFlgadd())) {
                        memberInfoById.setSusFlgList(Arrays.asList(sdSusjobD.getSusFlgadd().split(",")));
                    }
                    if (StrUtil.isNotBlank(sdSusjobD.getSusFlgaddType())) {
                        memberInfoById.setSusFlgaddType(Arrays.asList(sdSusjobD.getSusFlgaddType().split(",")));
                    }
                    memberInfoById.setSusFeedback(sdSusjobD.getSusFeedback());
                }
            }
            // 获取最后消费的信息
            SaleInfoDTO saleInfoDTO = memberManagementMapper.querySaleInfoByHykNo(user.getClient(), memberInfoById.getGsmbcCardId());
            if (!ObjectUtils.isEmpty(saleInfoDTO)){
                BeanUtils.copyProperties(saleInfoDTO, memberInfoById);
            }
            String type = "";
            switch (memberInfoById.getGsmbcClassId()){
                case "1":
                    type = "钻石卡";
                    break;
                case "2":
                    type = "白金卡";
                    break;
                case "3":
                    type = "金卡";
                    break;
                case "4":
                    type = "银卡";
                    break;
                case "5":
                    type = "普通卡";
                    break;
            }
            memberInfoById.setGsmbcClassId(type);
        }

        return memberInfoById;
    }


    @Override
    public ConsumeInfoDTO getOrderInfoById(String gsmbMemberId, String startDate, String endDate) {
        TokenUser user = commonService.getLoginInfo();
        // 根据会员id获取对应卡号
        String hykNo = memberManagementMapper.queryIdByHykNo(user.getClient(), gsmbMemberId);
        // 根据会员卡号获取消费订单列表
        List<OrderInfoDTO> orderInfoDTOS = memberManagementMapper.queryOrderInfoById(user.getClient(),hykNo, startDate, endDate);
        BigDecimal sumOfMoney = new BigDecimal(0);
        if (orderInfoDTOS.size()>0){
            for (OrderInfoDTO orderInfoDTO : orderInfoDTOS) {
                if (orderInfoDTO.getOrderInfoDetailDTOS().size()>0){
                    for (OrderInfoDetailDTO orderInfoDetailDTO : orderInfoDTO.getOrderInfoDetailDTOS()) {
                        // 去掉数量小数点的0
                        orderInfoDetailDTO.setProQtyAndUnit(orderInfoDetailDTO.getGssdQty().stripTrailingZeros().toString()+orderInfoDetailDTO.getProUnit());
                    }
                }
                sumOfMoney= sumOfMoney.add(orderInfoDTO.getGsshYsAmt());
            }
        }
        ConsumeInfoDTO consumeInfoDTO = new ConsumeInfoDTO();
        consumeInfoDTO.setCount(orderInfoDTOS.size());
        consumeInfoDTO.setSumOfMoney(sumOfMoney);
        consumeInfoDTO.setOrderInfoDTOS(orderInfoDTOS);
        return consumeInfoDTO;
    }
}
