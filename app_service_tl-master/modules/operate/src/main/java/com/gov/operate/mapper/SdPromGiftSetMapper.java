package com.gov.operate.mapper;

import com.gov.operate.entity.SdPromGiftSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
public interface SdPromGiftSetMapper extends BaseMapper<SdPromGiftSet> {

    void deleteList(@Param("list") List<SdPromGiftSet> sdPromGiftSetsDeleteList);
}
