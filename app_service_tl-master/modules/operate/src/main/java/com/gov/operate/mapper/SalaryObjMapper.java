package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SalaryObj;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface SalaryObjMapper extends BaseMapper<SalaryObj> {

    /**
     * 适用范围
     */
    List<SalaryObj> salaryObjList(@Param("client") String client);

    /**
     * 获取任意门店编码
     */
    String getStoreList(@Param("client") String client, @Param("gspId") Integer gspId);

    /**
     * 获取任意连锁门店下任意门店编码
     */
    String getCompadmStoreList(@Param("client") String client, @Param("gspId") Integer gspId);

    /**
     * 商品列表
     */
    List<ProductBusiness> getBusiness(@Param("client") String client, @Param("storeCode") String storeCode, @Param("proSelfCode") String proSelfCode);
}
