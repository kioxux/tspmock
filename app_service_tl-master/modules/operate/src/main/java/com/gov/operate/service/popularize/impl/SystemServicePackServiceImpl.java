package com.gov.operate.service.popularize.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.JsonUtils;
import com.gov.common.basic.LambdaUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.rsa.AesEncodeUtils;
import com.gov.operate.dto.SystemServicePackVO;
import com.gov.operate.dto.charge.ClientPriceItemVO;
import com.gov.operate.dto.charge.ClientPricePackVO;
import com.gov.operate.dto.charge.SaveClientPriceDTO;
import com.gov.operate.dto.charge.SaveClientPricePackDTO;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IUserChargeMaintenanceFreeService;
import com.gov.operate.service.IUserChargeMaintenanceMService;
import com.gov.operate.service.IUserChargeMaintenanceMStoService;
import com.gov.operate.service.popularize.ISystemServicePackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 功能服务包基础信息 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-07-27
 */
@Slf4j
@Service
public class SystemServicePackServiceImpl extends ServiceImpl<SystemServicePackMapper, SystemServicePack> implements ISystemServicePackService {

    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private UserChargeMaintenanceHMapper userChargeMaintenanceHMapper;
    @Resource
    private UserChargeMaintenanceMMapper userChargeMaintenanceMMapper;
    @Resource
    private UserChargeMaintenanceFreeMapper userChargeMaintenanceFreeMapper;
    @Resource
    private UserChargeMaintenanceMStoMapper userChargeMaintenanceMStoMapper;
    @Resource
    private FranchiseeMapper franchiseeMapper;
    @Resource
    private IUserChargeMaintenanceMService iUserChargeMaintenanceMService;
    @Resource
    private IUserChargeMaintenanceMStoService iUserChargeMaintenanceMStoService;
    @Resource
    private IUserChargeMaintenanceFreeService iUserChargeMaintenanceFreeService;

    /**
     * 官网价维护
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editOfficialPrice(List<SystemServicePack> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
            if (item.getSpOfficialPrice() == null || item.getSpOfficialPrice().compareTo(BigDecimal.ZERO) < 0) {
                throw new CustomResultException(StringUtils.parse("第{}行号请输入正确的官网价", String.valueOf(index)));
            }
            SystemServicePack systemServicePack = new SystemServicePack();
            systemServicePack.setSpOfficialPrice(item.getSpOfficialPrice());
            baseMapper.update(systemServicePack, new LambdaQueryWrapper<SystemServicePack>().eq(SystemServicePack::getRId, item.getRId()));
        }));
    }

    /**
     * 收查账项目集合
     *
     * @param spId
     * @param spSeries
     * @return
     */
    @Override
    public Result getOfficialPriceList(String spId, String spSeries) {
        List<SystemServicePack> list = baseMapper.selectList(
                new LambdaQueryWrapper<SystemServicePack>().like(StringUtils.isNotBlank(spId), SystemServicePack::getSpId, spId)
                        .like(StringUtils.isNotBlank(spSeries), SystemServicePack::getSpSeries, spSeries)
        );
        return ResultUtil.success(list);
    }

    @Override
    public Result getList() {
        List<SystemServicePack> spList = baseMapper.selectList(null);
        List<SystemServicePackVO> voList = new ArrayList<>();
        spList.forEach(s -> {
            SystemServicePackVO vo = new SystemServicePackVO();
            BeanUtils.copyProperties(s, vo);
            vo.setArId2(s.getRId());
            voList.add(vo);
        });
        Map<String, List<SystemServicePackVO>> listMap = voList
                .stream().filter(Objects::nonNull)
                .collect(Collectors.groupingBy(SystemServicePackVO::getSpType));
        return ResultUtil.success(listMap);
        /*return  ResultUtil.success(baseMapper.selectList(new LambdaQueryWrapper<SystemServicePack>()
                .eq(SystemServicePack::getSpMode, spMode)));*/
    }

    /**
     * 用户收费项目
     *
     * @param contractCode
     */
    @Override
    public Result getClientPriceItemByContract(String contractCode, String clientId) {
        List<UserChargeMaintenanceH> userChargeMaintenanceHList = userChargeMaintenanceHMapper.selectList(new QueryWrapper<UserChargeMaintenanceH>()
                .eq("SP_CONTRACT_CODE", contractCode).eq("CLIENT", clientId).orderByDesc("ID"));
        ClientPriceItemVO clientPriceItemVO = new ClientPriceItemVO();
        if (CollectionUtils.isEmpty(userChargeMaintenanceHList)) {
            String relativePath = "popularize/web/yaoudOperate/selectContract";
            log.info("用户收费项目参数：{}", JsonUtils.beanToJson(contractCode));
            Result result = AesEncodeUtils.postRequestOrderService(getFullUrl(relativePath), contractCode);
            log.info("用户收费项目返回值：{}", JsonUtils.beanToJson(result));
            return result;
        } else {
            UserChargeMaintenanceH userChargeMaintenanceH = userChargeMaintenanceHList.get(0);
            clientPriceItemVO.setId(new Long(userChargeMaintenanceH.getId()));
            clientPriceItemVO.setGscClientId(new Long(userChargeMaintenanceH.getClient()));
            clientPriceItemVO.setGscContractCode(userChargeMaintenanceH.getSpContractCode());
            clientPriceItemVO.setGscActualPay(userChargeMaintenanceH.getSpActualPay());
            clientPriceItemVO.setGscAvgAmount(userChargeMaintenanceH.getSpAvgAmount());
            clientPriceItemVO.setGscGpPrice(userChargeMaintenanceH.getSpGpPrice());
            clientPriceItemVO.setGscCoName(userChargeMaintenanceH.getSpCoName());
            clientPriceItemVO.setGscDiscount(userChargeMaintenanceH.getSpDiscount());
            clientPriceItemVO.setGscDscntAmount(userChargeMaintenanceH.getSpDscntAmount());
            clientPriceItemVO.setGscExcptStartDate(userChargeMaintenanceH.getSpExcptStartDate());
            clientPriceItemVO.setGscExcptEndDate(userChargeMaintenanceH.getSpExcptEndDate());
            clientPriceItemVO.setGscLimit(userChargeMaintenanceH.getSpLimit());
            clientPriceItemVO.setGscStoCount(userChargeMaintenanceH.getSpStoCount());
            clientPriceItemVO.setGscPayFreq(userChargeMaintenanceH.getSpPayFreq());
            clientPriceItemVO.setGscSignDate(userChargeMaintenanceH.getSpSignDate());
            clientPriceItemVO.setGscPayable(userChargeMaintenanceH.getSpPayable());

            // 关联模板
            List<UserChargeMaintenanceM> signPackList = userChargeMaintenanceMMapper.selectList(
                    new LambdaQueryWrapper<UserChargeMaintenanceM>().eq(UserChargeMaintenanceM::getSphId, userChargeMaintenanceH.getId())

            );
            List<ClientPricePackVO> signPackVOList = new ArrayList<>();
            for (UserChargeMaintenanceM userChargeMaintenanceM : signPackList) {
                ClientPricePackVO packVO = new ClientPricePackVO();
                BeanUtils.copyProperties(userChargeMaintenanceM, packVO);
                packVO.setSpOfficialPrice(userChargeMaintenanceM.getSpPrice());
                packVO.setSpContractPrice(userChargeMaintenanceM.getSpPriceRealistic());
                List<StoreData> storeList = userChargeMaintenanceHMapper.getStoreList(null, clientId, userChargeMaintenanceM.getId());
                packVO.setStoreDataList(storeList);
                signPackVOList.add(packVO);
            }
            clientPriceItemVO.setSignPackList(signPackVOList);
            // 赠送天数
            List<UserChargeMaintenanceFree> signFreeList = userChargeMaintenanceFreeMapper.selectList(
                    new LambdaQueryWrapper<UserChargeMaintenanceFree>().eq(UserChargeMaintenanceFree::getSphId, userChargeMaintenanceH.getId())
            );
            clientPriceItemVO.setSignFreeList(signFreeList);
            return ResultUtil.success(clientPriceItemVO);
        }
    }

    /**
     * 收费门店集合
     *
     * @return
     */
    @Override
    public Result selectPayStore() {
        TokenUser user = commonService.getLoginInfo();
        List<StoreData> list = storeDataMapper.selectList(new LambdaQueryWrapper<StoreData>().eq(StoreData::getClient, user.getClient()));
        return ResultUtil.success(list);
    }

    @Override
    @Transactional
    public void saveClientPriceItemByContract(SaveClientPriceDTO saveClientPriceDTO) {
        TokenUser user = commonService.getLoginInfo();
        UserChargeMaintenanceH userChargeMaintenanceH = new UserChargeMaintenanceH();
        BeanUtils.copyProperties(saveClientPriceDTO, userChargeMaintenanceH);
        int index = userChargeMaintenanceHMapper.selectList(new QueryWrapper<UserChargeMaintenanceH>()
                .eq("CLIENT", user.getClient())).size();
        userChargeMaintenanceH.setSpIndex(index + 1);
        LocalDate sDate = saveClientPriceDTO.getGscExcptStartDate();
        LocalDate eDate = saveClientPriceDTO.getGscExcptEndDate();
        userChargeMaintenanceH.setSpStartingDate(sDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        userChargeMaintenanceH.setSpEndingDate(eDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        int months = Period.between(sDate, eDate).getMonths() + (eDate.getYear() - sDate.getYear()) * 12;
        userChargeMaintenanceH.setSpCreId(user.getUserId());
        userChargeMaintenanceH.setSpPayable(saveClientPriceDTO.getGscPayable());
        userChargeMaintenanceH.setSpActualPay(saveClientPriceDTO.getGscActualPay());
        userChargeMaintenanceH.setSpDscntAmount(saveClientPriceDTO.getGscDscntAmount());
        userChargeMaintenanceH.setSpCreDate(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        userChargeMaintenanceH.setSpCreTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss")));
        userChargeMaintenanceH.setSpClientId(saveClientPriceDTO.getGscClientId());
        userChargeMaintenanceHMapper.insert(userChargeMaintenanceH);
        BigDecimal price = BigDecimal.ZERO;
        BigDecimal priceReal = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(saveClientPriceDTO.getSignPackList())) {
            for (SaveClientPricePackDTO s : saveClientPriceDTO.getSignPackList()) {
                if (ObjectUtils.isEmpty(s.getSpPriceRealistic())) {
                    throw new CustomResultException("协议价不能为空");
                }
                UserChargeMaintenanceM userChargeMaintenanceM = new UserChargeMaintenanceM();
                BeanUtils.copyProperties(s, userChargeMaintenanceM);
                userChargeMaintenanceM.setStartingDate(userChargeMaintenanceH.getSpStartingDate());
                userChargeMaintenanceM.setEndingDate(userChargeMaintenanceH.getSpEndingDate());
                Integer spMode = s.getSpMode();
                if (spMode.equals(1)) {
                    // 每店每月
                    price = price.add(s.getSpPrice().multiply(new BigDecimal(months)
                            .multiply(new BigDecimal(saveClientPriceDTO.getGscStoCount()))));
                    priceReal = priceReal.add(s.getSpPriceRealistic().multiply(new BigDecimal(months)
                            .multiply(new BigDecimal(saveClientPriceDTO.getGscStoCount()))));
                } else if (spMode.equals(2)) {
                    //每月
                    price = price.add(s.getSpPrice().multiply(new BigDecimal(months)));
                    priceReal = priceReal.add(s.getSpPriceRealistic().multiply(new BigDecimal(months)));
                }
                userChargeMaintenanceM.setSphId(userChargeMaintenanceH.getId());
                userChargeMaintenanceM.setClient(userChargeMaintenanceH.getClient());
                userChargeMaintenanceMMapper.insert(userChargeMaintenanceM);
//                userChargeMaintenanceMStoMapper.delete(
//                        new LambdaQueryWrapper<UserChargeMaintenanceMSto>()
//                                .eq(UserChargeMaintenanceMSto::getSpmId, userChargeMaintenanceM.getId()));
                if (!CollectionUtils.isEmpty(s.getStoreDataList()) && s.getSpMode() != 2) {
                    List<UserChargeMaintenanceMSto> userChargeMaintenanceMStoList = new ArrayList<>();
                    s.getStoreDataList().forEach(t -> {
                        UserChargeMaintenanceMSto sto = new UserChargeMaintenanceMSto();
                        sto.setSpmId(userChargeMaintenanceM.getId());
                        sto.setSpSite(t.getStoCode());
//                        userChargeMaintenanceMStoMapper.insert(sto);
                        userChargeMaintenanceMStoList.add(sto);
                    });
                    iUserChargeMaintenanceMStoService.saveBatch(userChargeMaintenanceMStoList);
                }
            }
        }
        if (!CollectionUtils.isEmpty(saveClientPriceDTO.getSignFreeList())) {
            List<UserChargeMaintenanceFree> userChargeMaintenanceFreeList = new ArrayList<>();
            saveClientPriceDTO.getSignFreeList().forEach(s -> {
                s.setSphId(userChargeMaintenanceH.getId());
//                userChargeMaintenanceFreeMapper.insert(s);
                userChargeMaintenanceFreeList.add(s);
            });
            iUserChargeMaintenanceFreeService.saveBatch(userChargeMaintenanceFreeList);
        }
    }

    /**
     * 推广URL
     *
     * @param relativePath
     * @return
     */
    private String getFullUrl(String relativePath) {
        return applicationConfig.getOrderServiceUrl() + relativePath;
    }
}
