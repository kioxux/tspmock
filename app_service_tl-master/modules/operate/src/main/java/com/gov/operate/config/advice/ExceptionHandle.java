package com.gov.operate.config.advice;

import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

/**
 * 统一异常处理
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandle {

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = CustomResultException.class)
    public Result handle(CustomResultException e) {
        return ResultUtil.error(e);
    }

    /**
     * 空指针
     *
     * @param e 空指针
     * @return asyncResponse
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = NullPointerException.class)
    public Result nullException(NullPointerException e) {
        log.error("空指针异常：", e);
        return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
    }

    /**
     * 捕捉验证异常
     *
     * @param e @Validated
     * @return asyncResponse
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Result validateException(MethodArgumentNotValidException e) {
        e.printStackTrace();
        log.error("捕捉验证异常：", e);
        BindingResult bindingResult = e.getBindingResult();
        return ResultUtil.error(ResultEnum.VALID_ERROR.getCode(), Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());

    }

    /**
     * 捕捉参数验证异常
     *
     * @param e 参数验证
     * @return asyncResponse
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = ConstraintViolationException.class)
    public Result validateConstraintException(ConstraintViolationException e) {
        e.printStackTrace();
        log.error("验证参数异常：", e);
        String errorMessage = e.getMessage().substring(e.getMessage().lastIndexOf(" ") + 1);
        return ResultUtil.error(ResultEnum.VALID_ERROR.getCode(), errorMessage);
    }

    /**
     * 验证异常
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = BindException.class)
    public Result bindException(BindException e) {
        e.printStackTrace();
        log.error("bindException异常：", e);
        return ResultUtil.error(ResultEnum.VALID_ERROR.getCode(), e.getFieldError().getDefaultMessage());
    }

    /**
     * 参数缺失
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public Result MissingParamException(MissingServletRequestParameterException e) {
        e.printStackTrace();
        log.error("参数缺失异常：", e);
        return ResultUtil.error(ResultEnum.E0012);
    }

    /**
     * 参数缺失
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public Result httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        e.printStackTrace();
        log.error("请求方法异常：", e);
        return ResultUtil.error(ResultEnum.E0033);
    }

    /**
     * 请求参数json格式错误
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Result httpMessageNotReadableException(HttpMessageNotReadableException e) {
        e.printStackTrace();
        log.error("请求参数json格式错误：", e);
        return ResultUtil.error(ResultEnum.E0034);
    }
    /**
     * 异常
     *
     * @param e
     * @return asyncResponse
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = Exception.class)
    public Result exceptionHandler(Exception e) {
        log.error("系统出现异常：", e);
        return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
    }
}
