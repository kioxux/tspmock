package com.gov.operate.dto.zz;

import lombok.Data;

@Data
public class ZzooOutData {
   private String zzId;
   private String zzName;
}
