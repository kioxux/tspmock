package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 9:45 2021/7/29
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@AllArgsConstructor
public class StoreGroupDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     *分类类型编码
     */
    @NotBlank(message = "门店分类类型不能为空")
    private String gssgType;

    /**
     * 分类编码
     */
    private String[] gssgId;//选择全部请求参数给null

    /**
     * 门店编号
     */
    private String[] stoCode;

    /**
     * 门店名称
     */
    private String stoName;
}
