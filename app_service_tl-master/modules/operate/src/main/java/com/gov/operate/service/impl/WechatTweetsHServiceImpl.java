package com.gov.operate.service.impl;

import com.gov.operate.entity.WechatTweetsH;
import com.gov.operate.mapper.WechatTweetsHMapper;
import com.gov.operate.service.IWechatTweetsHService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信推文管理 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-01-07
 */
@Service
public class WechatTweetsHServiceImpl extends ServiceImpl<WechatTweetsHMapper, WechatTweetsH> implements IWechatTweetsHService {

}
