package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.operate.entity.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class SavePromListVO {

    private String client;

    private String gsphBrId;

    private String gsphVoucherId;

    private String gsphTheme;

    private String gsphName;

    private String gsphRemarks;

    private String gsphBeginDate;

    private String gsphEndDate;

    private String gsphDateFrequency;

    private String gsphWeekFrequency;

    private String gsphBeginTime;

    private String gsphEndTime;

    private Integer gsphMarketid;

    private String gsphAllStatus;


    private List<String> stoCodeList;

    private List<Prom> promList;


    @Data
    public static class Prom {

        private String promName;

        private String gsphType;

        private String gsphStatus;

        private String gsphPart;

        private String gsphPara1;

        private String gsphPara2;

        private String gsphPara3;

        private String gsphPara4;

        private String gsphExclusion;

        private Integer gsphMarketid;

        private String gsphFlag;

        private String gsphVoucherId;

        private List<UnitarySet> unitarySetList;

        private List<SeriesSet> seriesSetList;

        private List<GiftSet> giftSetList;

        private List<AssoSet> assoSetList;

        private List<ChmedSet> chmedSetList;

        @Data
        public static class UnitarySet extends SdPromUnitarySet {
            private String proSelfCode;

            private String proCommonname;

            private String proName;

            private String proSpecs;

            private String proFactoryName;

            private String proPym;
        }

        @Data
        public static class SeriesSet extends SdPromSeriesSet {
            private List<SeriesConds> seriesCondsList;

            @Data
            public static class SeriesConds extends SdPromSeriesConds {
                private String proSelfCode;

                private String proCommonname;

                private String proName;

                private String proSpecs;

                private String proFactoryName;

                private String proPym;
            }
        }

        @Data
        public static class GiftSet extends SdPromGiftSet {
            private List<GiftConds> giftCondsList;

            private List<GiftResult> giftResultList;

            @Data
            public static class GiftConds extends SdPromGiftConds {
                private String proSelfCode;

                private String proCommonname;

                private String proName;

                private String proSpecs;

                private String proFactoryName;

                private String proPym;
            }

            @Data
            public static class GiftResult extends SdPromGiftResult {
                private String proSelfCode;

                private String proCommonname;

                private String proName;

                private String proSpecs;

                private String proFactoryName;

                private String proPym;
            }
        }

        @Data
        public static class AssoSet extends SdPromAssoSet {
            private List<AssoConds> assoCondsList;

            private List<AssoResult> assoResultList;


            @Data
            public static class AssoConds extends SdPromAssoConds {
                private String proSelfCode;

                private String proCommonname;

                private String proName;

                private String proSpecs;

                private String proFactoryName;

                private String proPym;
            }

            @Data
            public static class AssoResult extends SdPromAssoResult {
                private String proSelfCode;

                private String proCommonname;

                private String proName;

                private String proSpecs;

                private String proFactoryName;

                private String proPym;
            }

        }

        @Data
        public static class ChmedSet extends SdPromChmedSet {

        }
    }
}


