package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryProListByStoreVO {

    private List<String> stoCodeList;

    private String storeName;

    private String proName;
    /**
     * 销售级别
     */
    private String proSlaeClass;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * (接收值为 0:否 1:是) 业务逻辑(是-->散装中药饮片=仓储分区为3)
     */
    private Integer proStorageArea;


    private Integer pageSize;

    private Integer pageNum;

    /**
     * 已设置商品flag(0:未选中 1:已选中)
     */
    private Integer hasSelectedRow;
    /**
     * 未设置商品flag(0:未选中 1:已选中)
     */
    private Integer unHasSelectedRow;
    private List<ProSto> hasSelectedRowList;

    @Data
    @EqualsAndHashCode
    public static class ProSto {
        @NotBlank(message = "商品编码不能为空")
        private String proSelfCode;
//        @NotBlank(message = "门店编码不能为空")
        private String stoCode;

        //促销价
        private BigDecimal gsphpPrice;
        //促销折扣
        private String gsphpRebate;
        //是否积分
        private String gsphpInteFlag;
        //积分倍率
        private String gsphpInteRate;

        // 促销价
        private BigDecimal gsphsPrice;
        // 促销折扣
        private String gsphsRebate;
        // 是否积分
        private String gsphsInteFlag;
        // 积分倍率
        private String gsphsInteRate;
    }

}
