package com.gov.operate.dto;

import com.gov.operate.entity.Feedback;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetFeedBackDTO extends Feedback {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 用户名
     */
    private String userNam;

    /**
     * 图片列表
     */
    private List<ImagesDTO> imagesList;
}
