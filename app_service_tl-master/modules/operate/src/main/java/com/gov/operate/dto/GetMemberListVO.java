package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetMemberListVO{

//    @NotBlank(message = "营销活动Id不能为空")
    private String gsmMarketid;

//    @NotBlank(message = "门店编码不能为空")
    private String gsmStore;

    /**
     * 活跃度  核心、普通、新增、睡眠、无效
     */
    private List<String> gsmtgId;

    /**
     * 会员卡别
     */
    private List<String> classId;

    /**
     * 最小积分
     */
    private Integer minIntegral;

    /**
     * 最大积分
     */
    private Integer maxIntegral;

    /**
     * 所属门店
     */
    private List<String> storeList;

    /**
     * 最小年龄
     */
    private Integer minAge;

    /**
     * 最大年龄
     */
    private Integer maxAge;

    /**
     * 性别
     */
    private String sex;

    /**
     * 商品标签
     */
    private String proTag;

    /**
     * 消费属性（1-最近消费，2-最后消费）
     */
    private String consume;

    /**
     * 消费类型（1-7天内、2-15天内、3-30天内、4-60天内、5-自定义）
     */
    private String consumeType;

    /**
     * 消费开始日期
     */
    private String consumeStartDate;

    /**
     * 消费结束日期
     */
    private String consumeEndDate;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 商品组ID
     */
    private String gsyGroup;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 剔除类型（1-已发短信会员, 2:-自定义会员）
     */
    private String eliminateType;

    /**
     * 剔除会员卡号
     */
    private List<String> eliminateList;

    /**
     * 消费类型具体对应的天数（1-7天内、2-15天内、3-30天内、4-60天内、5-自定义）
     */
    private Integer consumeTypeDay;

    /**
     * 短信发送人数,使用地方：异步发送短信人数上限
     */
    private BigDecimal gsmSmstimes;

    @NotBlank(message = "常用查询不能为空")
    private String gsmsId;
}
