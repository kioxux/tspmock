package com.gov.operate.mapper;

import com.gov.operate.entity.DocumentBillD;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
public interface DocumentBillDMapper extends BaseMapper<DocumentBillD> {

}
