package com.gov.operate.service;

import com.gov.operate.entity.InvoiceBillDocument;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
public interface IInvoiceBillDocumentService extends SuperService<InvoiceBillDocument> {

}
