package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_MONTH_MERIT_PAY")
@ApiModel(value="SalaryMonthMeritPay对象", description="")
public class SalaryMonthMeritPay extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSMMP_ID", type = IdType.AUTO)
    private Integer gsmmpId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSMMP_GSP_ID")
    private Integer gsmmpGspId;

    @ApiModelProperty(value = "销售额最大值")
    @TableField("GSMMP_SALES_MIN")
    private BigDecimal gsmmpSalesMin;

    @ApiModelProperty(value = "销售额最小值")
    @TableField("GSMMP_SALES_MAX")
    private BigDecimal gsmmpSalesMax;

    @ApiModelProperty(value = "毛利率最大值")
    @TableField("GSMMP_GROSS_PROFIT_MIN")
    private BigDecimal gsmmpGrossProfitMin;

    @ApiModelProperty(value = "毛利率最小值")
    @TableField("GSMMP_GROSS_PROFIT_MAX")
    private BigDecimal gsmmpGrossProfitMax;

    @ApiModelProperty(value = "提成")
    @TableField("GSMMP_COEFFI")
    private BigDecimal gsmmpCoeffi;

    @ApiModelProperty(value = "销售额顺序")
    @TableField("GSMMP_JOB_SORT")
    private Integer gsmmpJobSort;

    @ApiModelProperty(value = "毛利率顺序")
    @TableField("GSMMP_SALES_SORT")
    private Integer gsmmpSalesSort;


}
