package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetProListForSaveGroupVO extends Pageable {

    @NotBlank(message = "地点不能为空")
    private String proSite;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 商品编号
     */
    private String proSelfCode;

    /**
     * 商品分类
     */
    private String proClass;

}
