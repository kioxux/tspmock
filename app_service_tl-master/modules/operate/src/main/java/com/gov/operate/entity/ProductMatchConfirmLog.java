package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_MATCH_CONFIRM_LOG")
@ApiModel(value="ProductMatchConfirmLog对象", description="")
public class ProductMatchConfirmLog extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商id")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "确认类型 1负责人点击确认 2门店用户点击确认")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "门店code")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "用户手机号")
    @TableField("TEL")
    private String tel;

    @ApiModelProperty(value = "确认时间,单用户确认时使用，用于每天确认一次")
    @TableField("CRE_DATE")
    private String creDate;
}
