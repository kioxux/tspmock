package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RegPayment {

    /**
     * 发票号码
     */
    private String invoiceNum;

    /**
     * 付款金额
     */
    private BigDecimal paymentAmount;

}
