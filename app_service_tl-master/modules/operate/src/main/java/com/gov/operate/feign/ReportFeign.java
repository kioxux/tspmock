package com.gov.operate.feign;

import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.MatchProductInData;
import com.gov.operate.feign.fallback.ReportFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Component
@FeignClient(value = "gys-report", fallback = ReportFeignFallback.class
//        , url = "http://172.19.10.186:10212/"
)
public interface ReportFeign {

    @PostMapping(value = "/productMatch/matchSchedule")
    public JsonResult getProductMatch(@RequestBody MatchProductInData inData);
}

