package com.gov.operate.service;

import com.gov.operate.entity.ClSystemPara;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 公司参数表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-06-02
 */
public interface IClSystemParaService extends SuperService<ClSystemPara> {

}
