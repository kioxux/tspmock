package com.gov.operate.dto.productRelate;

import com.gov.operate.entity.ProductRelate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.24
 */
@Data
public class ProductRelateDto extends ProductRelate {
    /**
     * 商品名
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 供应商名
     */
    private String supName;
}

