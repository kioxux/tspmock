package com.gov.operate.dto;

import lombok.Data;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:43 2021/9/2
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class StoreGroupSetDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 分类类型编码
     */
    private String gssgType;

    /**
     * 分类类型名称
     */
    private String gssgTypeName;

    /**
     * 分类编码
     */
    private String gssgId;

    /**
     * 分类名称
     */
    private String gssgName;
}
