package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:41 2021/7/28
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("GAIA_SD_STORES_GROUP_SET")
@ApiModel(value="SdStoresGroupSet对象", description="")
public class SdStoresGroupSet {

    private static final long serialVersionUID = -7773659114937613567L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "分类类型编码")
    @TableField("GSSG_TYPE")
    private String gssgType;

    @ApiModelProperty(value = "分类编码")
    @TableField("GSSG_ID")
    private String gssgId;

    @ApiModelProperty(value = "分类类型名称")
    @TableField("GSSG_TYPE_NAME")
    private String gssgTypeName;

    @ApiModelProperty(value = "分类编码名称")
    @TableField("GSSG_ID_NAME")
    private String gssgIdName;

    @ApiModelProperty(value = "修改人")
    @TableField("GSSG_UPDATE_EMP")
    private String gssgUpdateEmp;

    @ApiModelProperty(value = "修改日期")
    @TableField("GSSG_UPDATE_DATE")
    private String gssgUpdateDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("GSSG_UPDATE_TIME")
    private String gssgUpdateTime;

    @ApiModelProperty(value = "是否前台配置")
    @TableField("GSSG_IF_CONFIG")
    private Integer gssgIfConfig;

}
