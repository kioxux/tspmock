package com.gov.operate.service.impl;

import com.gov.operate.entity.SdExamineD;
import com.gov.operate.mapper.SdExamineDMapper;
import com.gov.operate.service.ISdExamineDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Service
public class SdExamineDServiceImpl extends ServiceImpl<SdExamineDMapper, SdExamineD> implements ISdExamineDService {

}
