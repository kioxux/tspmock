package com.gov.operate.dto;

import com.gov.operate.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SalaryProgramDTO extends SalaryProgram {

    private List<SalaryObj> applyObjList;
    // 达成率 列表
    private List<SalaryMonthReach> salaryMonthReacheList;
    // 薪资方案列表
    private List<SalaryMonthSalaryGrade> salaryMonthSalaryGradeList;
    // 岗位工资列表
    private List<SalaryProgramDTO.SalaryMonthWageJobsOuter> salaryMonthWageJobsOuterList;
    // 月绩效工资列表
    private List<SalaryProgramDTO.SalaryMonthMeritPayOuter> salaryMonthMeritPayOuterList;
    // 单品提成列表
    private List<SalaryProCps> salaryProCpsList;
    // 月考评工资列表
    private List<SalaryMonthAppraisalSalary> salaryMonthAppraisalSalaryList;
    // 员工系数值列表
    private List<SalaryKpi> salaryKpiList;
    // 季度奖列表
    private List<SalaryQuarterlyAwards> salaryQuarterlyAwardsList;
    // 年终奖列表
    private List<SalaryYearAwards> salaryYearAwardsList;

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class SalaryMonthWageJobsOuter {
        /**
         * 方案主键
         */
        private Integer gsmwjGspId;
        /**
         * 岗位编号
         */
        private String gsmwjJob;
        /**
         * 岗位名称
         */
        private String gsmwjJobName;
        /**
         * 排序
         */
        private Integer gsmwjJobSort;

        private List<SalaryMonthWageJobs> salaryMonthWageJobsInnerList;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class SalaryMonthMeritPayOuter {
        /**
         * 方案主键
         */
        private Integer gsmmpGspId;
        /**
         * 销售额最小值
         */
        private BigDecimal gsmmpSalesMin;
        /**
         * 销售额最大值
         */
        private BigDecimal gsmmpSalesMax;
        /**
         * 排序
         */
        private Integer gsmmpJobSort;

        private List<SalaryMonthMeritPay> salaryMonthMeritPayInnerList;

    }
}
