package com.gov.operate.service.impl;

import com.gov.operate.entity.AppLoginLog;
import com.gov.operate.mapper.AppLoginLogMapper;
import com.gov.operate.service.IAppLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-11
 */
@Service
public class AppLoginLogServiceImpl extends ServiceImpl<AppLoginLogMapper, AppLoginLog> implements IAppLoginLogService {

}
