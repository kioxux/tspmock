package com.gov.operate.dto;

import lombok.Data;

/**
 * @description: 电子券自动发券设置表列表
 * @author: yzf
 * @create: 2022-01-14 17:01
 */
@Data
public class ElectronAutoDTO {

    private String client;

    /**
     * 主题单号
     */
    private String gseasId;

    /**
     * 主题属性
     */
    private String gseasFlag;

    /**
     * 电子券活动号
     */
    private String gsebId;

    /**
     * 发送数量
     */
    private String gseasQty;

    /**
     * 起始日期
     */
    private String gseasBeginDate;

    /**
     * 结束日期
     */
    private String gseasEndDate;

    /**
     * 创建日期
     */
    private String gseasCreateDate;

    /**
     * 创建时间
     */
    private String gseasCreateTime;

    /**
     * 是否有效   N为否，Y为是
     */
    private String gseasValid;
}
