package com.gov.operate.service.impl;

import com.gov.operate.entity.ChajiaZ;
import com.gov.operate.mapper.ChajiaZMapper;
import com.gov.operate.service.IChajiaZService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 采购入库差价单主表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-12-20
 */
@Service
public class ChajiaZServiceImpl extends ServiceImpl<ChajiaZMapper, ChajiaZ> implements IChajiaZService {

}
