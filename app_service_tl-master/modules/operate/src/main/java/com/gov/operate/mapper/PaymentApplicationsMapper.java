package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import com.gov.operate.dto.invoice.GetPaymentApplyDetails;
import com.gov.operate.entity.PaymentApplications;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
public interface PaymentApplicationsMapper extends BaseMapper<PaymentApplications> {

    /**
     * 付款记录
     *
     * @param page
     * @param paymentRecordDto
     * @return
     */
    IPage<PaymentRecordDetail> paymentRecord(Page page, @Param("paymentRecordDto") PaymentRecordDto paymentRecordDto);

    /**
     * 付款合计
     *
     * @param paymentRecordDto
     * @return
     */
    BigDecimal paymentRecordTotal(@Param("paymentRecordDto") PaymentRecordDto paymentRecordDto);

    /**
     * 发票付款明细
     *
     * @param client
     * @param applicationsId
     * @return
     */
    List<SelectInvoiceDTO> selectPayInvoiceItems(@Param("client") String client, @Param("applicationsId") Integer applicationsId);

    /**
     * 单据付款明细
     *
     * @param client
     * @param applicationsId
     * @return
     */
    List<SelectBillDTO> selectPayBillItems(@Param("client") String client, @Param("applicationsId") Integer applicationsId);

    /**
     * 单据预付
     *
     * @param client
     * @param applicationsId
     * @return
     */
    List<SelectWarehousingDTO> selectPayPrepaidBillItems(@Param("client") String client, @Param("applicationsId") Integer applicationsId);

    /**
     * 审批页面_通过工作流编号查询付款明细
     *
     * @param matDnIdList
     * @param client
     * @return
     */
    List<GetPaymentApplyDetails> getPaymentApplyDetails(@Param("matDnIdList") List<String> matDnIdList, @Param("client") String client);

    /**
     * 审批页面_通过工作流编号查询付款明细_具体商品明细
     *
     * @param matDnIdList
     * @param client
     * @return
     */
    List<SelectWarehousingDetailsDTO> getPaymentApplyDetailsItemList(@Param("matDnIdList") List<String> matDnIdList, @Param("client") String client);

    /**
     * 工作流单据集合
     *
     * @param flowNo
     * @param client
     * @return
     */
    List<String> selectMatDnIdByFlowNo(@Param("flowNo") String flowNo, @Param("client") String client);

    List<SelectWarehousingDTO> selectPayPrepaidBillItemsByPay(@Param("client") String client, @Param("applicationsId") Integer applicationsId);

    List<SelectBillDTO> selectPayBillItemsExprot(@Param("client") String client, @Param("applicationsId") Integer applicationsId);
}
