package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.HomeChartRequestDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;

public interface IMobileHomeV2Service {

    /**
     * APP首页调整汇总
     * @param homeChartRequestDTO
     * @return
     */
    Result loginAppSaleTotal(HomeChartRequestDTO homeChartRequestDTO);

    Result getHomeStockList(HomeChartRequestDTO homeChartRequestDTO);

    Result getPaymentMethodList(HomeChartRequestDTO homeChartRequestDTO);

    Result getZzOrgListByUserAuth();
}
