package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class RoleVO {

    @NotBlank(message = "角色名称不能为空")
    private String garRoleName;

    @NotBlank(message = "是否启用不能为空")
    private String garEnable;

    private String  garBz;

}
