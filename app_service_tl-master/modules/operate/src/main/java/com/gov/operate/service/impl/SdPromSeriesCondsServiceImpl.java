package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromSeriesConds;
import com.gov.operate.mapper.SdPromSeriesCondsMapper;
import com.gov.operate.service.ISdPromSeriesCondsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
@Service
public class SdPromSeriesCondsServiceImpl extends ServiceImpl<SdPromSeriesCondsMapper, SdPromSeriesConds> implements ISdPromSeriesCondsService {

}
