package com.gov.operate.service;

import com.gov.operate.entity.BillPaymentApply;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-03-23
 */
public interface IBillPaymentApplyService extends SuperService<BillPaymentApply> {

}
