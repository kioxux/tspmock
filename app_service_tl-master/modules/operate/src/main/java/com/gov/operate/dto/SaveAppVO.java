package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@EqualsAndHashCode(callSuper = false)
public class SaveAppVO {

    @NotBlank(message = "版本号不能为空")
    @Pattern(regexp = "\\d{1,3}.\\d{1,3}.\\d{1,3}", message = "请填写正确的版本号格式: 000.000.000")
    private String versionCode;

    @NotNull(message = "是否强制更新不能为空  1:否,2:是")
    private Integer forceUpdateFlg;

    @NotNull(message = "平台类型不能为空 1:android 2:ios")
    private Integer type;

    @NotBlank(message = "更新内容不能为空")
    private String content;

    @NotBlank(message = "下载地址不能为空")
    private String downloadPath;
}
