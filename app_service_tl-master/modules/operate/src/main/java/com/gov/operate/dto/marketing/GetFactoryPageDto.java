package com.gov.operate.dto.marketing;

import io.swagger.models.auth.In;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 11:05 2021/8/13
 * @Description：查询生产厂家入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GetFactoryPageDto {

    @NotNull(message = "分页参数不能为空")
    private Integer pageNum;

    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;

    private List<String> queryStringList;
    private String queryString;
}
