package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateRechargeCardPasswordVO {

    @NotBlank(message = "储值卡号不能为空")
    private String gsrcId;

    @NotBlank(message = "储值账户不能为空")
    private String gsrcAccountId;

    @NotBlank(message = "旧密码不能为空")
    private String oldPwd;

    @NotBlank(message = "新密码不能为空")
    private String newPwd;

}
