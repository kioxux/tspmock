package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.RechargeCardLVO;
import com.gov.operate.dto.RechargeCardListVO;
import com.gov.operate.entity.SdRechargeCardChange;
import com.gov.operate.entity.StoreData;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdRechargeCardChangeMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdRechargeCardChangeService;
import com.gov.operate.service.ISdRechargeCardService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
@Service
public class SdRechargeCardChangeServiceImpl extends ServiceImpl<SdRechargeCardChangeMapper, SdRechargeCardChange> implements ISdRechargeCardChangeService {

    @Resource
    private CommonService commonService;

    @Resource
    private SdRechargeCardChangeMapper sdRechargeCardChangeMapper;

    @Resource
    private ISdRechargeCardService sdRechargeCardService;

    @Override
    public Result getRechargeCardList(RechargeCardListVO entity, Page<RechargeCardLVO> page) {
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = sdRechargeCardService.common();
        //获取门店编码或连锁总部编码 与储值卡相关联
        IPage<RechargeCardLVO> pageList = sdRechargeCardChangeMapper.SelectCardList(page, new QueryWrapper<RechargeCardListVO>()
                .eq("recharge.CLIENT", user.getClient())
                .eq("card.GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq(StringUtils.isNoneBlank(entity.getGsrccType()), "recharge.GSRCC_TYPE", entity.getGsrccType())
                .ge(StringUtils.isNoneBlank(entity.getGsrcpKateStart()), " recharge.GSRCC_DATE", entity.getGsrcpKateStart())
                .le(StringUtils.isNoneBlank(entity.getGsrcpKateEnd()), " recharge.GSRCC_DATE", entity.getGsrcpKateEnd())
                .like(StringUtils.isNotEmpty(entity.getGsrccOldCardId()), " recharge.GSRCC_OLD_CARD_ID", entity.getGsrccOldCardId())
                .like(StringUtils.isNotEmpty(entity.getGsrccNewCardId()), " recharge.GSRCC_NEW_CARD_ID", entity.getGsrccNewCardId())
                .like(StringUtils.isNotEmpty(entity.getGsrcName()), "card.GSRC_NAME", entity.getGsrcName())
                .like(StringUtils.isNotEmpty(entity.getGsrcMobile()), "card.GSRC_MOBILE", entity.getGsrcMobile()));
        return ResultUtil.success(pageList);
    }

    /**
     * 当前登录人有权限的门店列表_储值卡充值查询
     *
     * @return 权限门店列表
     */
    @Override
    public List<StoreData> getCurrentUserAuthStoList() {
        String client = commonService.getLoginInfo().getClient();
        String userId = commonService.getLoginInfo().getUserId();
        return sdRechargeCardChangeMapper.getCurrentUserAuthStoList(client, userId);
    }

}
