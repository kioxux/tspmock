package com.gov.operate.service;

import com.gov.operate.entity.WechatTweetsH;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 微信推文管理 服务类
 * </p>
 *
 * @author sy
 * @since 2021-01-07
 */
public interface IWechatTweetsHService extends SuperService<WechatTweetsH> {

}
