package com.gov.operate.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductMatchConfirmOtherInData implements Serializable {


    private static final long serialVersionUID = -4189305448332917759L;

//    private String clientId;

    private String stoCode;

    private String telNum;

    private Boolean clientFlag;//true为加盟商 false为门店

    private String type;//确认类型 1负责人点击确认 2门店用户点击确认 3表示核验人接收到最终结果确认

}
