package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_UNITARY_SET")
@ApiModel(value="SdPromUnitarySet对象", description="")
public class SdPromUnitarySet extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPUS_VOUCHER_ID")
    private String gspusVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPUS_SERIAL")
    private String gspusSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPUS_PRO_ID")
    private String gspusProId;

    @ApiModelProperty(value = "效期天数")
    @TableField("GSPUS_VAILD")
    private String gspusVaild;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPUS_QTY1")
    private String gspusQty1;

    @ApiModelProperty(value = "生成促销价1")
    @TableField("GSPUS_PRC1")
    private BigDecimal gspusPrc1;

    @ApiModelProperty(value = "生成促销折扣1")
    @TableField("GSPUS_REBATE1")
    private String gspusRebate1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPUS_QTY2")
    private String gspusQty2;

    @ApiModelProperty(value = "生成促销价2")
    @TableField("GSPUS_PRC2")
    private BigDecimal gspusPrc2;

    @ApiModelProperty(value = "生成促销折扣2")
    @TableField("GSPUS_REBATE2")
    private String gspusRebate2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPUS_QTY3")
    private String gspusQty3;

    @ApiModelProperty(value = "生成促销价3")
    @TableField("GSPUS_PRC3")
    private BigDecimal gspusPrc3;

    @ApiModelProperty(value = "生成促销折扣3")
    @TableField("GSPUS_REBATE3")
    private String gspusRebate3;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPUS_MEM_FLAG")
    private String gspusMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPUS_INTE_FLAG")
    private String gspusInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPUS_INTE_RATE")
    private String gspusInteRate;

    @ApiModelProperty(value = "是否限购 0或为空:则按原逻辑生效；1:限购")
    @TableField("GSPUS_LIMIT_FLAG")
    private String  gspusLimitFlag;

    @ApiModelProperty(value = "限价类型：0每日，1合计")
    @TableField("GSPUS_LIMIT_TYPE")
    private String  gspusLimitType;

}
