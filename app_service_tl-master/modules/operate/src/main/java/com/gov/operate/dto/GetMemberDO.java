package com.gov.operate.dto;

import com.gov.operate.entity.SdMemberCard;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetMemberDO extends SdMemberCard {

    /**
     * 会员姓名
     */
    private String gsmbName;

    /**
     * 性别
     */
    private String gsmbSex;

    /**
     * 年龄
     */
    private String gsmbAge;

    /**
     * 生日
     */
    private String gsmbBirth;

    /**
     * 电话
     */
    private String gsmbMobile;
}
