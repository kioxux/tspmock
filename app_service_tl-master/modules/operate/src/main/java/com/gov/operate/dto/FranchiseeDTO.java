package com.gov.operate.dto;

import com.gov.operate.entity.Franchisee;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class FranchiseeDTO extends Franchisee {

    /**
     * 是否选中(0:未选中，1:选中)
     */
    private String selected;

    private String francLegalPersonName;//负责人姓名
    private String francLegalPersonPhone;//负责人手机号
    private String francLogoUrl;
    private String francAssName;//责任人姓名
    private String francAssPhone;//责任人手机号
}
