package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode
public class GetProductListToCheckVO extends Pageable {

    @NotBlank(message = "成分id不能为空")
    private String componentId;
    @NotNull(message = "促销id不能为空")
    private Integer promId;
//    /**
//     * 毛利率
//     */
//    private BigDecimal profitsBegin;
//
//    private BigDecimal profitsEnd;
//    /**
//     * 零售价
//     */
//    private BigDecimal retailPriceBegin;
//
//    private BigDecimal retailPriceEnd;

    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品名
     */
    private String proName;


    @NotNull(message = "“查询已勾选” 0:全部 1:已勾选")
    private Integer hasChecked;
    /**
     * 商品编码集合
     */
    private List<String> prdList;


    private String client;

}
