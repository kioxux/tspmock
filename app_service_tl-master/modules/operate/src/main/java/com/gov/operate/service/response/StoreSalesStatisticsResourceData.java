package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class StoreSalesStatisticsResourceData implements Serializable {


    private static final long serialVersionUID = -6520471887318833075L;
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店编码")
    private String site;

    @ApiModelProperty(value = "门店名称")
    private String stoName;

    @ApiModelProperty(value = "销售额")
    private BigDecimal actualAmount;

    @ApiModelProperty(value = "毛利额")
    private BigDecimal grossProfit;

    @ApiModelProperty(value = "毛利率")
    private String grossMargin;

    @ApiModelProperty(value = "客单价")
    private BigDecimal customerPrice;

    @ApiModelProperty(value = "客单价")
    private Integer tradeTime;

    @ApiModelProperty(value = "销售时间")
    private String saleDate;

    @ApiModelProperty(value = "销售时间-星期几")
    private String saleWeekDate;

    @ApiModelProperty(value = "单店日均销售额")
    private BigDecimal averageDailySales;

    @ApiModelProperty(value = "单店日均毛利额")
    private BigDecimal averageDailyGrossProfit;

    @ApiModelProperty(value = "单店日均交易")
    private BigDecimal averageDailyTransaction;

}
