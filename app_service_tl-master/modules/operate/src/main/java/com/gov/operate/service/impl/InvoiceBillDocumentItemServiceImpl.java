package com.gov.operate.service.impl;

import com.gov.operate.entity.InvoiceBillDocumentItem;
import com.gov.operate.mapper.InvoiceBillDocumentItemMapper;
import com.gov.operate.service.IInvoiceBillDocumentItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Service
public class InvoiceBillDocumentItemServiceImpl extends ServiceImpl<InvoiceBillDocumentItemMapper, InvoiceBillDocumentItem> implements IInvoiceBillDocumentItemService {

}
