package com.gov.operate.dto.customer.order;

import com.gov.common.entity.Pageable;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021-06-21 14:48
 */
@Data
public class GetCustomerOrderListVO extends Pageable {

    @NotBlank(message = "客户不能为空")
    private String gwoClient;

    /**
     * 用户ID
     */
    private String gwoUserId;

    /**
     * 标题
     */
    private String gwoTitle;

    /**
     * 状态
     */
    private Integer gwoStatus;

    /**
     * 类型
     */
    private Long gwoTypeId;

    /**
     * 提交时间
     */
    private String gwoSendTimeStart;
    private String gwoSendTimeEnd;

    /**
     * 两个系统分页参数不一样
     */
    private Integer pageNo;

}
