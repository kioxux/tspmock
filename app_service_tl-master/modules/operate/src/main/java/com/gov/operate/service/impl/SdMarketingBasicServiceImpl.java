package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdMarketingBasicMapper;
import com.gov.operate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.CaseUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Slf4j
@Service
public class SdMarketingBasicServiceImpl extends ServiceImpl<SdMarketingBasicMapper, SdMarketingBasic> implements ISdMarketingBasicService {

    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingBasicMapper marketingBasicMapper;
    @Resource
    private ISdMarketingClientService marketingClientService;
    @Resource
    private ISdMarketingPromService marketingPromService;
    @Resource
    private ISdMarketingPrdService marketingPrdService;
    @Resource
    private ISdMarketingGiveawayService marketingGiveawayService;
    @Resource
    private ISdPromVariableSetService promVariableSetService;
    @Resource
    private ISdMarketingStoService marketingStoService;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private IProductComponentService productComponentService;
    @Resource
    private ISdMarketingSearchService sdMarketingSearchService;


    /**
     * 营销设置列表
     */
    @Override
    public IPage<SdMarketingBasic> getGsmSettingList(GetGsmSettingListVO2 vo) {
        // 分页对象
        Page<SdMarketingBasic> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<SdMarketingBasic> query = new QueryWrapper<SdMarketingBasic>()
                // 活动类型
                .eq(StringUtils.isNotEmpty(vo.getGsmType()), "GSM_TYPE", vo.getGsmType())
                // 活动开始时间
                .ge(StringUtils.isNotEmpty(vo.getGsmStartd()), "GSM_STARTD", vo.getGsmStartd())
                // 活动结束时间
                .le(StringUtils.isNotEmpty(vo.getGsmEndd()), "GSM_ENDD", vo.getGsmEndd())
                // 活动名称
                .apply(StringUtils.isNotEmpty(vo.getGsmThename()), "instr(GSM_THENAME, {0})", vo.getGsmThename())
                // 状态(0已下发，1未下发)
                .eq(StringUtils.isNotEmpty(vo.getGsmRele()), "GSM_RELE", vo.getGsmRele())
                // 删除标记(0 否,1 是)
                .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode())
                // id倒序
                .orderByDesc("id");
        return this.page(page, query);
    }

    /**
     * 营销设置保存
     */
    @Override
    public void saveGsmSetting(SaveGsmSettingVO2 vo) {
        SdMarketingBasic basic = new SdMarketingBasic();
        BeanUtils.copyProperties(vo, basic);
        // 营销活动id
        String maxNumMarketId = getMaxNumMarketId();
        // 营销主题活动id
        basic.setGsmMarketid(maxNumMarketId);
        // 营销主题ID
        basic.setGsmThenid(maxNumMarketId);
        // 是否下发(0已下发，1未下发)
        basic.setGsmRele("1");
        // 删除标记(0-否，1-是)
        basic.setGsmDeleteFlag("0");
        this.save(basic);
    }

    /**
     * 营销设置编辑
     */
    @Override
    public void editGsmSetting(EditGsmSettingVO2 vo) {
        if (StringUtils.isEmpty(vo.getId())) {
            throw new CustomResultException("Id不能为空");
        }
        SdMarketingBasic basic = new SdMarketingBasic();
        BeanUtils.copyProperties(vo, basic);
        this.updateById(basic);
    }

    /**
     * 营销设置详情
     */
    @Override
    public GetGsmSettingDetailDTO2 getGsmSettingDetail(Integer id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        SdMarketingBasic marketingBasic = this.getById(id);
        if (ObjectUtils.isEmpty(marketingBasic)) {
            throw new CustomResultException("未查询到该营销设置");
        }
        GetGsmSettingDetailDTO2 dto = new GetGsmSettingDetailDTO2();
        BeanUtils.copyProperties(marketingBasic, dto);
        List<Franchisee> clientIdList = marketingBasicMapper.getClientList(id);
        dto.setClientList(clientIdList);
        return dto;
    }

    /**
     * 营销设置删除
     */
    @Override
    public void deleteMarketSetting(Integer id) {
        if (StringUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        SdMarketingBasic marketingBasic = this.getById(id);
        if (ObjectUtils.isEmpty(marketingBasic)) {
            throw new CustomResultException("该数据已经被删除");
        }
        if (!ObjectUtils.isEmpty(marketingBasic.getGsmRele()) && marketingBasic.getGsmRele().equals("0")) {
            throw new CustomResultException("该营销设置已经下发,不可删除");
        }
        this.removeById(id);
    }

    /**
     * 活动方案推送
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pushGsm(PushGsmVO2 vo) {
        // 营销活动id
        Integer basicId = vo.getId();
        // 加盟商列表
        List<String> clientList = vo.getClientList();
        if (ObjectUtils.isEmpty(basicId)) {
            throw new CustomResultException("营销活动主键不能");
        }
        if (CollectionUtils.isEmpty(clientList)) {
            throw new CustomResultException("加盟商列表不能为空");
        }
        SdMarketingBasic marketingBasic = this.getById(basicId);
        if (ObjectUtils.isEmpty(marketingBasic)) {
            throw new CustomResultException("该营销活动已经被删除");
        }
        if (StringUtils.isNotEmpty(marketingBasic.getGsmRele()) && marketingBasic.getGsmRele().equals("0")) {
            throw new CustomResultException("该营销活动已经推送,不可重复推送");
        }

        /**
         * 1. basic 表数据更新
         */
        SdMarketingBasic basic = new SdMarketingBasic();
        // id
        basic.setId(vo.getId());
        // 是否下发(0已下发，1未下发)
        basic.setGsmRele("0");
        // 下发时间
        basic.setGsmReletime(DateUtils.getCurrentDateTimeStrTwo());
        this.updateById(basic);

        /**
         * 2. GAIA_SD_MARKETING_CLIENT 营销活动加盟商分配表 数据插入
         */
        List<SdMarketingClient> marketingClientList = clientList.stream().map(item -> {
            SdMarketingClient marketingClient = new SdMarketingClient();
            BeanUtils.copyProperties(marketingBasic, marketingClient);
            // id
            marketingClient.setId(null);
            // basic表 Id
            marketingClient.setBasicId(marketingBasic.getId());
            // 加盟商
            marketingClient.setClient(item);
            // 审批状态(-1,未审批，0，审批中，1已审批，2，作废，3，立即执行)
            marketingClient.setGsmImpl("-1");
            return marketingClient;
        }).collect(Collectors.toList());
        marketingClientService.saveBatch(marketingClientList);


        /**
         * 4.GAIA_SD_MARKETING_PROM 营销活动促销关联表 数据插入
         */
        // 促销json
        String gsmProcondi = marketingBasic.getGsmProcondi();
        // 促销实体
        GsmProcondiDTO gsmProcondiDTO = null;
        try {
            gsmProcondiDTO = JSON.parseObject(gsmProcondi, GsmProcondiDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("促销json转实体异常");
        }
        List<SdMarketingProm> marketingPromList = new ArrayList<>();
        for (SdMarketingClient marketingClient : marketingClientList) {
            List<GsmProcondiDTO.ProComp> proCompList = gsmProcondiDTO.getProCompclass();
            for (GsmProcondiDTO.ProComp proComp : proCompList) {
                SdMarketingProm marketingProm = new SdMarketingProm();
                // 加盟商
                marketingProm.setClient(marketingClient.getClient());
                // 营销活动加盟商分配表主键
                marketingProm.setMarketingId(marketingClient.getId());
                // 成分
                marketingProm.setComponentId(proComp.getComponent());
                // 拷贝 (成分,类型,促销方式,促销方式描述,促销类型,促销类型描述,备注1,备注2)
                BeanUtils.copyProperties(proComp, marketingProm);
                // 拷贝 具体的促销方式
                BeanUtils.copyProperties(proComp.getProm(), marketingProm, "client", "marketingId", "componentId",
                        "type", "gspvsMode", "gspvsModeName", "gspvsType", "gspvsTypeName", "remarks1", "remarks2", "name");
                // 系列
                // 达到金额1
                marketingProm.setGspssReachAmt1(StringUtils.isNotBlank(proComp.getProm().getGspssReachAmt1()) ? new BigDecimal(proComp.getProm().getGspssReachAmt1()) : null);
                // 结果减额1
                marketingProm.setGspssResultAmt1(StringUtils.isNotBlank(proComp.getProm().getGspssResultAmt1()) ? new BigDecimal(proComp.getProm().getGspssResultAmt1()) : null);
                // 赠品
                // 达到金额1
                marketingProm.setGspgsReachAmt1(StringUtils.isNotBlank(proComp.getProm().getGspgsReachAmt1()) ? new BigDecimal(proComp.getProm().getGspgsReachAmt1()) : null);
                // 赠送数量1
                marketingProm.setGspgsResultQty1(proComp.getProm().getGspgsResultQty1());
                // 组合
                // 赠品单品价格
                marketingProm.setGsparGiftPrc(StringUtils.isNotBlank(proComp.getProm().getGsparGiftPrc()) ? new BigDecimal(proComp.getProm().getGsparGiftPrc()) : null);
                // 组合金额
                marketingProm.setGspasAmt(StringUtils.isNotBlank(proComp.getProm().getGspasAmt()) ? new BigDecimal(proComp.getProm().getGspasAmt()) : null);

                // 系列编码
                marketingProm.setGspgsSeriesProId(marketingProm.getGspgcSeriesId());
                marketingProm.setGspgrSeriesId(marketingProm.getGspgcSeriesId());
                marketingProm.setGsparSeriesId(marketingProm.getGspacSeriesId());
                marketingProm.setGspasSeriesId(marketingProm.getGspacSeriesId());

                marketingPromList.add(marketingProm);
            }
        }
        marketingPromService.saveBatch(marketingPromList);
    }

    /**
     * 促销方式列表
     */
    @Override
    public List<GetPromotionListDTO> getPromotionList() {
        return marketingBasicMapper.getPromotionList();
//        // todo 只筛选单品类
//        List<GetPromotionListDTO> promotionList = marketingBasicMapper.getPromotionList();
//        List<GetPromotionListDTO> singleList = promotionList.stream().filter(item -> item.getGspvsMode().startsWith("SINGLE")).collect(Collectors.toList());
//        return singleList;
    }

    /**
     * 活动ID最大值
     */
    private String getMaxNumMarketId() {
        String maxNumMarketid = marketingBasicMapper.getMaxNumMarketid();
        if (StringUtils.isEmpty(maxNumMarketid)) {
            return "1000000000";
        }
        return new BigDecimal(maxNumMarketid).add(BigDecimal.ONE).toString();
    }


    /*********************************************营销任务*********************************************/

    /**
     * 营销任务列表
     */
    @Override
    public IPage<GetGsmTaskListDTO2> getGsmTaskList(GsmTaskListVO2 vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<GetGsmTaskListDTO2> pageInfo = new Page<>(vo.getPageNum(), vo.getPageSize());
        return marketingBasicMapper.getGsmTaskList(pageInfo, user.getClient(), vo);
    }

    /**
     * 营销任务详情
     */
    @Override
    public GetGsmTaskDetailDTO getGsmTaskDetail(Integer id, Integer type, String stoCode) {
        if (ObjectUtils.isEmpty(id)) {
            throw new CustomResultException("id不能为空");
        }
        if (ObjectUtils.isEmpty(type)) {
            throw new CustomResultException("type不能为空,推荐营销Tal页type传1, 当前营销Tal页type传2");
        }
        if (type == 2 && StringUtils.isEmpty(stoCode)) {
            throw new CustomResultException("门店code不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();

        //1.营销活动主数据
        SdMarketingClient marketingClient = marketingClientService.getById(id);
        if (ObjectUtils.isEmpty(marketingClient)) {
            throw new CustomResultException("id错误,该数据不存在或被已被删除");
        }
        GetGsmTaskDetailDTO dto = new GetGsmTaskDetailDTO();
        BeanUtils.copyProperties(marketingClient, dto);
        // 门店列表
        List<SdMarketingSto> marketingStoList = marketingStoService.list(new QueryWrapper<SdMarketingSto>()
                .eq("CLIENT", client).eq("MARKETING_ID", id));
        if (!CollectionUtils.isEmpty(marketingStoList)) {
            List<String> proCodeList = marketingStoList.stream().map(SdMarketingSto::getStoCode).collect(Collectors.toList());
            List<StoreData> stoList = storeDataService.list(new QueryWrapper<StoreData>().select("STO_CODE", "STO_NAME").eq("CLIENT", client).in("STO_CODE", proCodeList));
            dto.setStoList(stoList);
        }

        //2.短信/电话详情 （若当前营销/推荐营销 则设置短信电话查询id，以及门店编码）
        if (type == 2) {
            // 门店编码
            dto.setStoCode(stoCode);
            List<SdMarketingSearch> searchList = sdMarketingSearchService.list(new QueryWrapper<SdMarketingSearch>()
                    .eq("MARKETING_ID", id).eq("GSMS_STORE", stoCode).eq("GSMS_DELETE_FLAG", "0"));
            // 短信查询条件
            List<SdMarketingSearch> smsSearchList = searchList.stream().filter(search -> search.getGsmsType().equals(1)).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(smsSearchList)) {
                dto.setGsmSmscondiId(smsSearchList.get(0).getGsmsId());
            }
            // 电话查询条件
            List<SdMarketingSearch> telSearchList = searchList.stream().filter(search -> search.getGsmsType().equals(2)).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(telSearchList)) {
                dto.setGsmTelcondiId(telSearchList.get(0).getGsmsId());
            }
        }

        //3.促销信息
        // 促销列表
        List<SdMarketingProm> marketingPromList = marketingPromService.list(new QueryWrapper<SdMarketingProm>()
                .eq("MARKETING_ID", id).eq("CLIENT", client));
        // 成分列表
        List<String> componentIdList = marketingPromList.stream().map(SdMarketingProm::getComponentId).collect(Collectors.toList());
        List<ProductComponent> componentList = productComponentService.list(new QueryWrapper<ProductComponent>().in(!CollectionUtils.isEmpty(componentIdList), "PRO_COMP_CODE", componentIdList));
        // 促销参数列表
        Map<String, List<SdPromVariableSet>> promVariableMap = promVariableSetService.list(new QueryWrapper<SdPromVariableSet>()
                .ne("GSPVS_TYPE", "PROM_HEADER").eq("GSPVS_FALG", "1"))
                .stream()
                .collect(Collectors.groupingBy(SdPromVariableSet::getGspvsMode));
        // 选品列表
        List<SdMarketingPrd> marketingPrdList = marketingPrdService.list(new QueryWrapper<SdMarketingPrd>()
                .eq("MARKETING_ID", id).eq("CLIENT", client));
        // 赠品列表
        List<SdMarketingGiveaway> marketingGiveawayList = marketingGiveawayService.list(new QueryWrapper<SdMarketingGiveaway>()
                .eq("MARKETING_ID", id).eq("CLIENT", client));


        List<GetGsmTaskDetailDTO.Prom> promList = marketingPromList.stream().map(item -> {
            GetGsmTaskDetailDTO.Prom prom = new GetGsmTaskDetailDTO.Prom();
            // 促销名称
            BeanUtils.copyProperties(item, prom);
            // 成分名称
            ProductComponent productComponent = componentList.stream()
                    .filter(component -> component.getProCompCode().equals(prom.getComponentId()))
                    .findFirst().get();
            prom.setComponentName(productComponent.getProCompName());

            // 促销参数列表
            Map map = JSON.parseObject(JSON.toJSONString(item), Map.class);
            // 筛选指定促销类型包含的参数
            List<GetGsmTaskDetailDTO.Param> paramList = promVariableMap.get(item.getGspvsMode()).stream().map(variable -> {
                GetGsmTaskDetailDTO.Param param = new GetGsmTaskDetailDTO.Param();
                BeanUtils.copyProperties(variable, param);
                return param;
            }).collect(Collectors.toList());
            // 参数赋值
            paramList.forEach(param -> {
                // 列名转成驼峰
                String gspvsColumns = CaseUtils.toCamelCase(param.getGspvsColumns(), false, '_');
                param.setGspvsColumns(gspvsColumns);
                // 赋值(参数具体的值)
                String gspvsColumnsValue = null;
                if (map.get(gspvsColumns) instanceof String || map.get(gspvsColumns) instanceof Integer) {
                    gspvsColumnsValue = String.valueOf(map.get(gspvsColumns));
                } else if (map.get(gspvsColumns) instanceof BigDecimal) {
                    gspvsColumnsValue = ((BigDecimal) map.get(gspvsColumns)).stripTrailingZeros().toPlainString();
                }
                param.setGspvsColumnsValue(gspvsColumnsValue);
            });
            prom.setParamList(paramList);

            // 选品
            List<GetGsmTaskDetailDTO.SdMarketingPrdDTO> promPrdList = marketingPrdList.stream()
                    .filter(prd -> prd.getPromId().equals(item.getId()))
                    .map(prd -> {
                        GetGsmTaskDetailDTO.SdMarketingPrdDTO prdDTO = new GetGsmTaskDetailDTO.SdMarketingPrdDTO();
                        BeanUtils.copyProperties(prd, prdDTO);
                        prdDTO.setProCode(null);
                        prdDTO.setProSelfCode(prd.getProCode());
                        return prdDTO;
                    }).collect(Collectors.toList());
            prom.setPrdList(promPrdList);
            prom.setCountPrd(promPrdList.size());

            // 赠品
            List<GetGsmTaskDetailDTO.SdMarketingGiveawayDTO> promGiveawayList = marketingGiveawayList.stream()
                    .filter(giveaway -> giveaway.getPromId().equals(item.getId()))
                    .map(giveaway -> {
                        GetGsmTaskDetailDTO.SdMarketingGiveawayDTO giveawayDTO = new GetGsmTaskDetailDTO.SdMarketingGiveawayDTO();
                        BeanUtils.copyProperties(giveaway, giveawayDTO);
                        giveawayDTO.setProCode(null);
                        giveawayDTO.setProSelfCode(giveaway.getProCode());
                        return giveawayDTO;
                    }).collect(Collectors.toList());
            prom.setGiveawayList(promGiveawayList);
            prom.setCountGiveaway(promGiveawayList.size());

            return prom;
        }).collect(Collectors.toList());
        dto.setPromList(promList);

        return dto;
    }

    /**
     * 营销任务保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveGsmTask(SaveGsmTaskVO vo) {
        List<String> stoList = vo.getStoList();
        if (CollectionUtils.isEmpty(stoList)) {
            throw new CustomResultException("门店列表不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        //
        String client = user.getClient();
        /**
         * 1.保存主数据
         */
        // 营销活动加盟商分配表主键
        Integer id = vo.getId();
        SdMarketingClient marketingClientExit = marketingClientService.getById(id);
        if (ObjectUtils.isEmpty(marketingClientExit)) {
            throw new CustomResultException("id错误,该数据不存在");
        }
        if (!marketingClientExit.getGsmImpl().equals("-1")) {
            throw new CustomResultException("当前营销已经执行/提交审批,不可编辑");
        }
        SdMarketingClient marketingClient = new SdMarketingClient();
        BeanUtils.copyProperties(vo, marketingClient);
        marketingClientService.updateById(marketingClient);

        /**
         * 2.营销活动门店分配表
         */
        // 删除旧数据
        marketingStoService.remove(new QueryWrapper<SdMarketingSto>().eq("MARKETING_ID", id));
        // 新数据
        List<SdMarketingSto> marketingStoList = stoList.stream().map(stoCode -> {
            SdMarketingSto marketingSto = new SdMarketingSto();
            //
            marketingSto.setClient(client);
            //
            marketingSto.setMarketingId(id);
            //
            marketingSto.setStoCode(stoCode);
            return marketingSto;
        }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(marketingStoList)) {
            marketingStoService.saveBatch(marketingStoList);
        }

        /**
         * 3.短信/电话查询条件
         */
        // 删除旧数据
        sdMarketingSearchService.remove(new QueryWrapper<SdMarketingSearch>().eq("MARKETING_ID", id));
        // 添加新数据
        List<SdMarketingSearch> searchList = new ArrayList<>();
        stoList.stream().forEach(item -> {
            // 短信
            SdMarketingSearch smsSearch = new SdMarketingSearch();
            smsSearch.setClient(client);
            smsSearch.setGsmsId(UUIDUtil.getUUID());
            smsSearch.setGsmsName(marketingClient.getGsmThename());
            smsSearch.setGsmsDetail(marketingClient.getGsmSmscondi());
            smsSearch.setMarketingId(id);
            smsSearch.setGsmsStore(item);
            smsSearch.setGsmsNormal("0");
            smsSearch.setGsmsDeleteFlag("0");
            smsSearch.setGsmsType(1);
            searchList.add(smsSearch);

            // 电话
            SdMarketingSearch telSearch = new SdMarketingSearch();
            telSearch.setClient(client);
            telSearch.setGsmsId(UUIDUtil.getUUID());
            telSearch.setGsmsName(marketingClient.getGsmThename());
            telSearch.setGsmsDetail(marketingClient.getGsmTelcondi());
            telSearch.setMarketingId(id);
            telSearch.setGsmsStore(item);
            telSearch.setGsmsNormal("0");
            telSearch.setGsmsDeleteFlag("0");
            telSearch.setGsmsType(2);
            searchList.add(telSearch);
        });
        if (!CollectionUtils.isEmpty(searchList)) {
            sdMarketingSearchService.saveBatch(searchList);
        }


        /**
         *  4.促销数据
         *  4.1 促销参数数据
         *  4.2 商品数据
         *  4.3 赠品数据
         */
        List<SaveGsmTaskVO.Prom> promList = vo.getPromList();
        if (CollectionUtils.isEmpty(promList)) {
            return;
        }
        // 4.1 促销参数数据
        promList.forEach(prom -> {
            List<SaveGsmTaskVO.Param> paramList = prom.getParamList();
            Map map = new HashMap();
            paramList.forEach(param -> {
                // 页面编辑后的参数
                map.put(param.getGspvsColumns(), param.getGspvsColumnsValue());
            });
            SdMarketingProm variable = JSON.parseObject(JSON.toJSONString(map), SdMarketingProm.class);
            variable.setId(prom.getId());

            marketingPromService.updateById(variable);
        });

        // 4.2 商品数据
        // 删除旧数据
        marketingPrdService.remove(new QueryWrapper<SdMarketingPrd>().eq("MARKETING_ID", id));
        // 插入新数据
        List<SdMarketingPrd> prdListNew = new ArrayList<>();
        promList.forEach(item -> {
            // 每个促销下的选品列表
            List<SdMarketingPrd> prdList = item.getPrdList().stream().map(prdDTO -> {
                SdMarketingPrd prd = new SdMarketingPrd();
                // 加盟商
                prd.setClient(client);
                // 营销活动加盟商分配表主键
                prd.setMarketingId(id);
                // 促销关联表
                prd.setPromId(item.getId());
                // 商品编码
                prd.setProCode(prdDTO.getProSelfCode());
                // 促销单价
                prd.setGsmpPrice(prdDTO.getGsmpPrice());
                // 促销毛利率
                prd.setGsmpFits(prdDTO.getGsmpFits());
                return prd;
            }).collect(Collectors.toList());
            prdListNew.addAll(prdList);
        });
        marketingPrdService.saveBatch(prdListNew);

        // 4.3 赠品数据
        // 删除旧数据
        marketingGiveawayService.remove(new QueryWrapper<SdMarketingGiveaway>().eq("MARKETING_ID", id));
        // 插入新数据
        List<SdMarketingGiveaway> giveawayListNew = new ArrayList<>();
        promList.forEach(item -> {
            // 每个促销下的赠品列表
            List<SdMarketingGiveaway> giveawayList = item.getGiveawayList().stream().map(giveawayDTO -> {
                SdMarketingGiveaway giveaway = new SdMarketingGiveaway();
                // 加盟商
                giveaway.setClient(client);
                // 营销活动加盟商分配表主键
                giveaway.setMarketingId(id);
                // 促销关联表
                giveaway.setPromId(item.getId());
                // 商品编码
                giveaway.setProCode(giveawayDTO.getProSelfCode());
                return giveaway;
            }).collect(Collectors.toList());
            giveawayListNew.addAll(giveawayList);
        });
        marketingGiveawayService.saveBatch(giveawayListNew);
    }

    /**
     * 选品选择列表
     */
    @Override
    public IPage<GetProductListToCheckDTO> getProductListToCheck(GetProductListToCheckVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page<GetProductListToCheckDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<GetProductListToCheckDTO> result = marketingBasicMapper.getProductListToCheck(page, vo);
        return result;
    }

    /**
     * 赠品选择列表
     */
    @Override
    public IPage<GetProductListToCheckDTO> getGiveawayListToCheck(GetGiveawayListToCheckVO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page<GetProductListToCheckDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        IPage<GetProductListToCheckDTO> result = marketingBasicMapper.getGiveawayListToCheck(page, vo);
        return result;
    }


}
