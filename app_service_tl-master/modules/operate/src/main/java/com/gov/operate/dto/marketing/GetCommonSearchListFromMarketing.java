package com.gov.operate.dto.marketing;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class GetCommonSearchListFromMarketing {

//    @NotBlank(message = "查询ID不能为空")
    private String gsmsId;

    @NotBlank(message = "门店编码不能为空")
    private String stoCode;

    @NotNull(message = "查询类型不能为空 1:短信，2：电话")
    private Integer gsmsType;
}
