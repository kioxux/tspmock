package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_SUSJOB_D")
@ApiModel(value = "SdSusjobD对象", description = "")
public class SdSusjobD extends BaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "序号")
    @TableField("SUS_NUMBER")
    private String susNumber;


    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "任务ID")
    @TableField("SUS_JOBID")
    private String susJobid;

    @ApiModelProperty(value = "会员卡号")
    @TableField("SUS_CARD_ID")
    private String susCardId;

    @ApiModelProperty(value = "任务门店")
    @TableField("SUS_JOBBR")
    private String susJobbr;

    @ApiModelProperty(value = "会员名称")
    @TableField("SUS_MENNAME")
    private String susMenname;

    @ApiModelProperty(value = "手机")
    @TableField("SUS_MOBILE")
    private String susMobile;

    @ApiModelProperty(value = "性别")
    @TableField("SUS_SEX")
    private String susSex;

    @ApiModelProperty(value = "生日")
    @TableField("SUS_BIRTH")
    private String susBirth;

    @ApiModelProperty(value = "当前积分")
    @TableField("SUS_INTEGRAL")
    private BigDecimal susIntegral;

    @ApiModelProperty(value = "开卡门店")
    @TableField("SUS_BR_ID")
    private String susBrId;

    @ApiModelProperty(value = "开卡门店名称")
    @TableField("SUS_BR_NAME")
    private String susBrName;

    @ApiModelProperty(value = "会员标签")
    @TableField("SUS_FLG")
    private String susFlg;

    @ApiModelProperty(value = "最后一次交易门店")
    @TableField("SUS_BR_ID1")
    private String susBrId1;

    @ApiModelProperty(value = "最后一次交易门店名称")
    @TableField("SUS_BR_NAME1")
    private String susBrName1;

    @ApiModelProperty(value = "最后一次交易时间")
    @TableField("SUS_LASTTIME")
    private String susLasttime;

    @ApiModelProperty(value = "维系标签")
    @TableField("SUS_FLGADD")
    private String susFlgadd;

    @ApiModelProperty(value = "维系结果")
    @TableField("SUS_JOBFLG")
    private String susJobflg;

    @ApiModelProperty(value = "不再维系")
    @TableField("SUS_FREEZE")
    private String susFreeze;

    @ApiModelProperty(value = "文字维系内容")
    @TableField("SUS_JOBTXT")
    private String susJobtxt;

    @ApiModelProperty(value = "维系人员工号")
    @TableField("SUS_USERID")
    private String susUserid;

    @ApiModelProperty(value = "维系人员姓名")
    @TableField("SUS_USERNAME")
    private String susUsername;

    @ApiModelProperty(value = "维系时间")
    @TableField("SUS_JOBTIME")
    private String susJobtime;

    @ApiModelProperty(value = "是否提交")
    @TableField("SUS_FINISH")
    private String susFinish;

    @ApiModelProperty(value = "开卡员工")
    @TableField("SUS_OPEN_CARD_EMP")
    private String susOpenCardEmp;

    @ApiModelProperty(value = "最后交易员工")
    @TableField("SUS_LAST_SALE_EMP")
    private String susLastSaleEmp;

    @ApiModelProperty(value = "会员回访类型0待回访1已接听2无应答3已拦截")
    @TableField("SUS_STATUS")
    private String susStatus;

    @ApiModelProperty(value = "维系标签类型0电话空号1不在维系")
    @TableField("SUS_FLGADD_TYPE")
    private String susFlgaddType;

    @ApiModelProperty(value = "回访反馈")
    @TableField("SUS_FEEDBACK")
    private String susFeedback;


    @ApiModelProperty(value = "最近回访时间")
    @TableField("SUS_FEEDBACKTIME")
    private String susFeedbacktime;


}
