package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING")
@ApiModel(value="SdMarketing对象", description="")
public class SdMarketing extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "营销活动ID")
    @TableField("GSM_MARKETID")
    private String gsmMarketid;

    @ApiModelProperty(value = "营销主题ID")
    @TableField("GSM_THENID")
    private String gsmThenid;

    @ApiModelProperty(value = "营销主题名称")
    @TableField("GSM_THENAME")
    private String gsmThename;

    @ApiModelProperty(value = "营销活动简介")
    @TableField("GSM_THEINTRO")
    private String gsmTheintro;

    @ApiModelProperty(value = "门店编码")
    @TableField("GSM_STORE")
    private String gsmStore;

    @ApiModelProperty(value = "活动类型")
    @TableField("GSM_TYPE")
    private String gsmType;

    @ApiModelProperty(value = "活动开始时间")
    @TableField("GSM_STARTD")
    private String gsmStartd;

    @ApiModelProperty(value = "活动结束时间")
    @TableField("GSM_ENDD")
    private String gsmEndd;

    @ApiModelProperty(value = "周期活动ID")
    @TableField("GSM_CYCLEID")
    private String gsmCycleid;

    @ApiModelProperty(value = "销售额标值")
    @TableField("GSM_INDEX_VALUE")
    private BigDecimal gsmIndexValue;

    @ApiModelProperty(value = "销售额活动值")
    @TableField("GSM_TARGET_VALUE")
    private BigDecimal gsmTargetValue;

    @ApiModelProperty(value = "销售额增幅")
    @TableField("GSM_SALEINC")
    private BigDecimal gsmSaleinc;

    @ApiModelProperty(value = "客单价选择")
    @TableField("GSM_PRICECHOOSE")
    private String gsmPricechoose;

    @ApiModelProperty(value = "客单价")
    @TableField("GSM_PRICEINC")
    private BigDecimal gsmPriceinc;

    @ApiModelProperty(value = "客单价活动值")
    @TableField("GSM_ACT_PRICEINC")
    private BigDecimal gsmActPriceinc;

    @ApiModelProperty(value = "客单价对应交易次数修正值")
    @TableField("GSM_PRICEINC_ST")
    private BigDecimal gsmPriceincSt;

    @ApiModelProperty(value = "交易次数选择")
    @TableField("GSM_STCHOOSE")
    private String gsmStchoose;

    @ApiModelProperty(value = "交易次数")
    @TableField("GSM_STINC")
    private BigDecimal gsmStinc;

    @TableField("GSM_ACT_STINC")
    private BigDecimal gsmActStinc;

    @ApiModelProperty(value = "交易次数对应客单价修正值")
    @TableField("GSM_STINC_PRICE")
    private BigDecimal gsmStincPrice;

    @ApiModelProperty(value = "毛利率")
    @TableField("GSM_GRPROFIT")
    private BigDecimal gsmGrprofit;

    @ApiModelProperty(value = "选品条件")
    @TableField("GSM_PROCONDI")
    private String gsmProcondi;

    @ApiModelProperty(value = "短信模板ID")
    @TableField("GSM_TEMPID")
    private String gsmTempid;

    @ApiModelProperty(value = "短信查询条件")
    @TableField("GSM_SMSCONDI")
    private String gsmSmscondi;

    @ApiModelProperty(value = "短信发送人数")
    @TableField("GSM_SMSTIMES")
    private BigDecimal gsmSmstimes;

    @ApiModelProperty(value = "电话通知查询条件")
    @TableField("GSM_TELCONDI")
    private String gsmTelcondi;

    @ApiModelProperty(value = "电话通知人数")
    @TableField("GSM_TELTIMES")
    private BigDecimal gsmTeltimes;

    @ApiModelProperty(value = "DM单打印张数")
    @TableField("GSM_DMTIMES")
    private BigDecimal gsmDmtimes;

    @ApiModelProperty(value = "DM单发放数")
    @TableField("GSM_DMSEND")
    private String gsmDmsend;

    @ApiModelProperty(value = "DM单成本")
    @TableField("GSM_DMPRICE")
    private BigDecimal gsmDmprice;

    @ApiModelProperty(value = "DM单回收数")
    @TableField("GSM_DMRECOVERY")
    private String gsmDmrecovery;

    @ApiModelProperty(value = "是否下发")
    @TableField("GSM_RELE")
    private String gsmRele;

    @ApiModelProperty(value = "状态(0:未推送, 1:已推送, 2:立即执行, 3:待审批, 4:审批通过, 5:审批拒绝)")
    @TableField("GSM_IMPL")
    private String gsmImpl;

    @ApiModelProperty(value = "推送时间")
    @TableField("GSM_RELETIME")
    private String gsmReletime;

    @ApiModelProperty(value = "执行时间")
    @TableField("GSM_IMPLTIME")
    private String gsmImpltime;

    @ApiModelProperty(value = "0-否，1-是")
    @TableField("GSM_DELETE_FLAG")
    private String gsmDeleteFlag;

    @ApiModelProperty(value = "审批流程编码")
    @TableField("GSM_FLOW_NO")
    private String gsmFlowNo;


}
