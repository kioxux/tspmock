package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.operate.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditProgramVO {

    @NotNull(message = "方案主键不能为空")
    @TableId(value = "GSP_ID", type = IdType.AUTO)
    private Integer gspId;

    @NotNull(message = "当前操作步骤不能为空")
    private Integer gspIndex;

    //    @NotNull(message = "系数类型不能为空")
    private Integer gspTypeSalary;

    //    @NotNull(message = "基本工资不能为空")
    private BigDecimal gspBasicSalary;

    //    @NotNull(message = "岗位工资不能为空")
    private BigDecimal gspPostSalary;

    //    @NotNull(message = "绩效工资不能为空")
    private BigDecimal gspPerformanceSalary;

    //    @NotNull(message = "考评工资不能为空")
    private BigDecimal gspEvaluationSalary;

    //    @NotNull(message = "方案名称不能为空")
    private String gspName;

    //    @NotNull(message = "生效日期不能为空")
    private String gspEffectiveDate;

    //    @NotNull(message = "目的不能为空")
    private String gspPurpose;

    //    @NotNull(message = "营业额权重不能为空")
    private BigDecimal gspTurnover;

    //    @NotNull(message = "毛利额权重不能为空")
    private BigDecimal gspGrossProfit;

    //    @NotNull(message = "新店综合达成率计算不能为空")
    private Integer gspIsNewStore;

    //    @NotNull(message = "开业前月综合达成率计算月分不能为空")
    private Integer gspOpenBeforeMonth;

    //    @NotNull(message = "新店综合达成率不能为空")
    private BigDecimal gspNewAchieveRate;

    //    @NotNull(message = "首月整月天数不能为空")
    private Integer gspFirstmonthDays;

    //    @NotNull(message = "月基本工资关联达成系数不能为空")
    private Integer gspSalaryReach;

    //    @NotNull(message = "月基岗位资关联达成系数不能为空")
    private Integer gspPostReach;

    //    @NotNull(message = "销售提成 不能为空")
    private Integer gspSalesCommission;

    //    @NotNull(message = "毛利提成不能为空")
    private Integer gspGrossMargin;

    //    @NotNull(message = "负毛利率是否提成不能为空")
    private Integer gspNegativeGrossMargin;

    //    @NotNull(message = "单品不参与销售提成不能为空")
    private Integer gspProReach;

    //    @NotNull(message = "月绩效关联达成系数不能为空")
    private Integer gspSalesReach;

    //    @NotNull(message = "考评工资关联达成系数不能为空")
    private Integer gspAppraisalSalary;

    //    @NotNull(message = "季度奖提成不能为空")
    private Integer gspQuarteQuarterlyAwards;

    //    @NotNull(message = "不参与季度奖提成不能为空")
    private Integer gspQuarteComputeAchievRate;

    //    @NotNull(message = "不参与季度奖提成最大值不能为空")
    private BigDecimal gspQuarteAchievRateMin;

    //    @NotNull(message = "关联季度员工个人系数不能为空")
    private Integer gspQuarterKpi;

    //    @NotNull(message = "关联季度综合达成系数不能为空")
    private Integer gspQuarterAppraisalSalary;

    //    @NotNull(message = "年终奖提成不能为空")
    private Integer gspYearQuarterlyAwards;

    //    @NotNull(message = "不参与年终奖提成不能为空")
    private Integer gspYearComputeAchievRate;

    //    @NotNull(message = "不参与年终奖提成最大值不能为空")
    private BigDecimal gspYearAchievRateMin;

    //    @NotNull(message = "关联年度员工个人系数")
    private Integer gspYearKpi;

    //    @NotNull(message = "关联年度综合达成系数")
    private Integer gspYearAppraisalSalary;

    //    @NotNull(message = "负毛利商品不参与季度奖提成不能为空")
    private Integer gspQuarterNegativeCommission;

    //    @NotNull(message = "负毛利商品不参与年终奖提成不能为空")
    private Integer gspYearNegativeCommission;

    /**
     * 第一步 使用方式使用范围
     */
    private List<SalaryObj> applyObjList;

    /**
     * 第二步 薪资方案 月综合达成系数
     */
    private List<SalaryMonthReach> salaryMonthReacheList;

    /**
     * 第三步 设定月基本工资级别
     */
    private List<SalaryMonthSalaryGrade> salaryMonthSalaryGradeList;

    /**
     * 第四步 岗位工资
     */
    private List<SalaryMonthWageJobsOuter> salaryMonthWageJobsOuterList;

    /**
     * 第五步 绩效工资
     */
    private List<SalaryMonthMeritPayOuter> salaryMonthMeritPayOuterList;

    /**
     * 第五步 薪资方案单品提成
     */
    private List<SalaryProCps> salaryProCpsList;

    /**
     * 第六步 月评工资
     */
    private List<SalaryMonthAppraisalSalary> salaryMonthAppraisalSalaryList;

    /**
     * 第六步 员工个人系数
     */
    private List<SalaryKpi> salaryKpiList;

    /**
     * 第七步 设定季度奖
     */
    private List<SalaryQuarterlyAwards> salaryQuarterlyAwardsList;

    /**
     * 第八步 设定年终奖
     */
    private List<SalaryYearAwards> salaryYearAwardsList;


    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class SalaryMonthWageJobsOuter {
        /**
         * 方案主键
         */
        private Integer gsmwjGspId;
        /**
         * 岗位编号
         */
        private String gsmwjJob;
        /**
         * 岗位名称
         */
        private String gsmwjJobName;

        private List<SalaryMonthWageJobs> salaryMonthWageJobsInnerList;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class SalaryMonthMeritPayOuter {
        /**
         * 方案主键
         */
        private Integer gsmmpGspId;
        /**
         * 销售额最小值
         */
        private BigDecimal gsmmpSalesMin;
        /**
         * 销售额最大值
         */
        private BigDecimal gsmmpSalesMax;

        private List<SalaryMonthMeritPay> salaryMonthMeritPayInnerList;

    }


}

