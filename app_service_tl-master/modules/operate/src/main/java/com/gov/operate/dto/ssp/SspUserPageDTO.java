package com.gov.operate.dto.ssp;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class SspUserPageDTO {

    @NotNull(message = "分页不能为空")
    private Integer pageNum;

    @NotNull(message = "分页不能为空")
    private Integer pageSize;

    private String mobile;

    private String userName;

    private String supName;

    private String status;

    private Date currentDate;

    private String client;
}
