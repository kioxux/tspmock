package com.gov.operate.controller;

import com.gov.common.basic.DateUtils;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.service.ISdSusjobHService;
import com.gov.operate.service.ISusjobTaskService;
import com.gov.operate.service.request.SdSusjobHRequest;
import com.gov.operate.service.request.SelectQuerySusjobMemberRequest;
import com.gov.operate.service.request.SelectStatusCountRequset;
import com.gov.operate.service.request.UpdateStatusRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */

@RestController
@RequestMapping("susjobTaskApp")
public class SusjobTaskAppController {

    @Resource
    private ISdSusjobHService sdSusjobHService;
    @Resource
    private ISusjobTaskService iSusjobTaskService;

    /**
     * 会员维系任务列表
     *
     * @param
     * @return
     */
    @Log("会员维系任务列表")
    @PostMapping("querySusjobTaskAppList")
    public Result querySusjobTaskAppList(@RequestBody @Valid SdSusjobHRequest request) {
        return iSusjobTaskService.querySusjobTaskAppList(request);
    }

   /* @Log("历史会员维系任务列表")
    @GetMapping("querySusjobTaskAppHistoryList")
    public Result querySusjobTaskAppHistoryList(@Valid SdSusjobHRequest request){
        return iSusjobTaskService.querySusjobTaskAppHistoryList(request);
    }*/

    /**
     * 修改会员维系状态
     *
     * @param taskType 1进行中
     * @return
     */
    @Log("修改会员维系状态")
    @PutMapping("updateQuerySusjobTaskApp/{jobId}")
    public Result updateQuerySusjobTaskApp(@PathVariable String jobId, @RequestParam String taskType) {
        return iSusjobTaskService.updateQuerySusjobTaskApp(jobId, taskType);
    }

    /**
     * 会员维系任务会员列表
     *
     * @param request
     * @return
     */
    @Log("会员维系任务会员列表")
    @PostMapping("selectQuerySusjobMemberApp")
    public Result selectQuerySusjobMemberApp(@RequestBody @Valid SelectQuerySusjobMemberRequest request) {
        return iSusjobTaskService.selectQuerySusjobMemberApp(request);
    }

    @Log("获取状态数量")
    @PostMapping("selectStatusCount")
    public Result selectStatusCount(@RequestBody @Valid SelectStatusCountRequset request) {
        return iSusjobTaskService.selectStatusCount(request);
    }


    @Log("接听状态修改")
    @PutMapping("updateStatus")
    public Result updateStatus(@RequestBody @Valid UpdateStatusRequest request) {
        return iSusjobTaskService.updateStatus(request);
    }

    /**
     * 任务详情
     *
     * @param jobId 任务ID
     * @return
     */
    @Log("任务详情")
    @GetMapping("selectTaskDetail/{jobId}")
    public Result selectTaskDetail(@PathVariable String jobId) {
        return iSusjobTaskService.selectTaskDetail(jobId);
    }


    @Log("会员用户信息")
    @GetMapping("selectCardUserDetail/{cardId}")
    public Result selectCardUserDetail(@PathVariable String cardId){
        return iSusjobTaskService.selectCardUserDetail(cardId);
    }

    @ApiOperation(value = "根据会员id查询会员消费记录")
    @GetMapping("getOrderInfoById")
    public Result getOrderInfoById(@ApiParam("会员卡号")  @RequestParam String cardId,
                                   @ApiParam("开始日期")  @RequestParam String startDate,
                                   @ApiParam("结束日期")  @RequestParam String endDate) {
        return ResultUtil.success(iSusjobTaskService.getOrderInfoById(cardId, DateUtils.getYearMonthFirst(startDate), DateUtils.getYearMonthLast(endDate)));
    }
}
