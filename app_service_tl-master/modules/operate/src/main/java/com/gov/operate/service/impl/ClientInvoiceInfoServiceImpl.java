package com.gov.operate.service.impl;

import com.gov.operate.entity.ClientInvoiceInfo;
import com.gov.operate.mapper.ClientInvoiceInfoMapper;
import com.gov.operate.service.IClientInvoiceInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@Service
public class ClientInvoiceInfoServiceImpl extends ServiceImpl<ClientInvoiceInfoMapper, ClientInvoiceInfo> implements IClientInvoiceInfoService {

}
