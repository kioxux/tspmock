package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销商品分明细组表
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_GROUP_PRO")
@ApiModel(value="SdPromGroupPro对象", description="促销商品分明细组表")
public class SdPromGroupPro extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "商品组ID")
    @TableField("GROUP_ID")
    private String groupId;

    @ApiModelProperty(value = "商品编码")
    @TableField("GROUP_PRO_ID")
    private String groupProId;


}
