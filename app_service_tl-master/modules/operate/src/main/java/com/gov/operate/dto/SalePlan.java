package com.gov.operate.dto;

import lombok.Data;

import java.math.BigDecimal;

public class SalePlan {
    //客户编号
    private String client;
    //销售日期(年周)
    private String month;
    //销售天数
    private String day;
    //累计销售额
    private String aAmt;
    //累计销售数量
    private String aGross;
    //计划销售额
    private String tAmt;
    //计划销售数量
    private String tGross;
    //计划会员卡数量
    private String tMcardQty;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getaAmt() {
        return aAmt;
    }

    public void setaAmt(String aAmt) {
        this.aAmt = aAmt;
    }

    public String getaGross() {
        return aGross;
    }

    public void setaGross(String aGross) {
        this.aGross = aGross;
    }

    public String gettAmt() {
        return tAmt;
    }

    public void settAmt(String tAmt) {
        this.tAmt = tAmt;
    }

    public String gettGross() {
        return tGross;
    }

    public void settGross(String tGross) {
        this.tGross = tGross;
    }

    public String gettMcardQty() {
        return tMcardQty;
    }

    public void settMcardQty(String tMcardQty) {
        this.tMcardQty = tMcardQty;
    }


    @Override
    public String toString() {
        return "SalePlan{" +
                "client='" + client + '\'' +
                ", month='" + month + '\'' +
                ", day='" + day + '\'' +
                ", aAmt='" + aAmt + '\'' +
                ", aGross='" + aGross + '\'' +
                ", tAmt='" + tAmt + '\'' +
                ", tGross='" + tGross + '\'' +
                ", tMcardQty='" + tMcardQty + '\'' +
                '}';
    }
}
