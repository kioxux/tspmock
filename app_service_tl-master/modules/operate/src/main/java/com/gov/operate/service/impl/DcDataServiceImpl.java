package com.gov.operate.service.impl;

import com.gov.operate.entity.DcData;
import com.gov.operate.mapper.DcDataMapper;
import com.gov.operate.service.IDcDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class DcDataServiceImpl extends ServiceImpl<DcDataMapper, DcData> implements IDcDataService {

}
