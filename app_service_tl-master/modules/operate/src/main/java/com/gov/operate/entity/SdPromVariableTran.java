package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_VARIABLE_TRAN")
@ApiModel(value="SdPromVariableTran对象", description="")
public class SdPromVariableTran extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "促销单号")
    @TableField("GSPVT_VOUCHER_ID")
    private String gspvtVoucherId;

    @ApiModelProperty(value = "促销类型")
    @TableField("GSPVT_TYPE")
    private String gspvtType;

    @ApiModelProperty(value = "变量字段名")
    @TableField("GSPVT_COLUMNS")
    private String gspvtColumns;

    @ApiModelProperty(value = "变量字段描述")
    @TableField("GSPVT_COLUMNS_NAME")
    private String gspvtColumnsName;

    @ApiModelProperty(value = "变量字段值")
    @TableField("GSPVT_COLUMNS_VALUE")
    private String gspvtColumnsValue;


}
