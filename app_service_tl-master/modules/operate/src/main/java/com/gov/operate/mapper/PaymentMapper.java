package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.*;
import com.gov.operate.entity.FiInvoiceRegistration;
import com.gov.operate.entity.FiInvoiceRegistrationDetailed;
import com.gov.operate.entity.PaymentDocumentNoApplications;
import com.gov.operate.entity.PaymentVatApplications;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PaymentMapper {

    /**
     * 可付款单据
     */
    IPage<SelectBillDTO> selectBill(Page page, @Param("vo") SelectBillVO vo);

    /**
     * 可付款单据明细
     */
    List<SelectBillDetailsDTO> selectBillDetails(@Param("vo") SelectBillDetailsVO vo);

    /**
     * 根据 [加盟商，业务类型，单据号] 列表获取 单据发票关系list
     */
    List<FiInvoiceRegistration> getInvoiceRegistrationList(@Param("client") String client, @Param("list") List<BillPaymentVO> list);

    /**
     * 根据 [加盟商，业务类型，单据号] 列表获取 单据发票明细关系list
     */
    List<FiInvoiceRegistrationDetailedDTO> getInvoiceRegistrationDetailList(@Param("client") String client, @Param("list") List<BillPaymentVO> list);

    /**
     * 已经完成付款的单据
     */
    List<String> getBillPayFinished(@Param("client") String client, @Param("list") List<BillPaymentVO> list);

    /**
     * 单据付款 更新发票登记明细
     */
    void updateInvoiceRegistrationDetailed(@Param("client") String client, @Param("list") List<BillPaymentVO> list);

    /**
     * 单据付款 更新发票登记
     */
    void updateInvoiceRegistration(@Param("client") String client, @Param("list") List<BillPaymentVO> list);

    /**
     * 明细付款 查询当前单据 所用发票 付款状态为 “1:发票付款”
     */
    List<String> countHasInvoicePayment(@Param("vo") BillDetailPaymentVO vo);

    /**
     * 明细付款 当前单据总登记金额以及总付款金额
     */
    PayMentInfo hasFinishPayment(@Param("vo") BillDetailPaymentVO vo);

    /**
     * 明细付款 单据登记明细
     */
    List<FiInvoiceRegistrationDetailed> getRegDetail(@Param("vo") BillDetailPaymentVO vo);

    /**
     * 明细付款 本次发票付款情况列表（付款未完成的条目，剩余可付金额正序）
     */
    List<RegPaymentSurplus> getRegPaymentInfo(@Param("vo") BillDetailPaymentVO vo);

    /**
     * 发票找单据
     */
    List<String> getMatDnIdListByInvoice(@Param("invoiceNumList") List<String> invoiceNumList);

    /**
     * 单据找发票
     */
    List<String> getInvoiceListByMatDnId(@Param("matDnIdList") List<String> matDnIdList);

    /**
     * 已经付款的单据
     */
    List<PaymentDocumentNoApplications> selectBillListHasPayment(@Param("invoiceNumStr") String invoiceNumStr, @Param("client") String client);

    /**
     * 当前加盟商 单据付款单号最大值
     */
    String getClientMaxOrderNo(@Param("client") String client);

    /**
     * 当前加盟商 发票付款单号最大值
     */
    String getClientMaxOrderNoVat(@Param("client") String client);

    /**
     * 发票列表
     */
    IPage<SelectInvoiceDTO> selectInvoice(Page page, @Param("vo") SelectInvoiceVO vo);

    /**
     * 单据是否付款
     *
     * @param client
     * @param billNum
     * @return
     */
    List<Integer> selectCountPayBillCountByBillNum(@Param("client") String client, @Param("billNum") String billNum);

    /**
     * 已单独付款发票
     */
    List<PaymentVatApplications> selectInvoiceListHasPayment(@Param("client") String client, @Param("matDnId") String matDnId);

    /**
     * 供应商发票登记付款查询
     *
     * @param page
     * @param selectInvoiceVO
     * @return
     */
    IPage<SelectInvoiceDTO> selectInvoicePayRecord(Page page, @Param("selectInvoiceVO") SelectInvoiceVO selectInvoiceVO);

    /**
     * 供应商单据登记付款查询
     *
     * @param page
     * @param selectWarehousingVO
     * @return
     */
    IPage<SelectWarehousingDTO> selectBillPayRecord(Page page, @Param("vo") SelectWarehousingVO selectWarehousingVO);

    /**
     * 根据单号查询发票
     *
     * @param matDnId
     * @param client
     * @return
     */
    String getinvoiceNumListByBill(@Param("matDnId") String matDnId, @Param("client") String client);

    /**
     * 发票付款记录
     *
     * @param invoice
     * @param client
     * @return
     */
    List<PaymentVatApplications> selectInvoicePayList(@Param("invoice") String invoice, @Param("client") String client);

    List<Map<String, String>> getPaymentIds(String clientId);

}
