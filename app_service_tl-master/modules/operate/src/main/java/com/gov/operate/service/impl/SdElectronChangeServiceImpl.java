package com.gov.operate.service.impl;

import com.gov.operate.entity.SdElectronChange;
import com.gov.operate.mapper.SdElectronChangeMapper;
import com.gov.operate.service.ISdElectronChangeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 电子券异动表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-07-09
 */
@Service
public class SdElectronChangeServiceImpl extends ServiceImpl<SdElectronChangeMapper, SdElectronChange> implements ISdElectronChangeService {

}
