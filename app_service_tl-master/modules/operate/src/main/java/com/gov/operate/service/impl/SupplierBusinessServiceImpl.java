package com.gov.operate.service.impl;

import com.gov.operate.entity.SupplierBusiness;
import com.gov.operate.mapper.SupplierBusinessMapper;
import com.gov.operate.service.ISupplierBusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Service
public class SupplierBusinessServiceImpl extends ServiceImpl<SupplierBusinessMapper, SupplierBusiness> implements ISupplierBusinessService {

}
