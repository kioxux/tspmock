package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SusjobDetails;
import com.gov.operate.dto.SusjobDetailsQuery;
import com.gov.operate.entity.SdSusjobD;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SdSusjobH;
import com.gov.operate.service.request.SelectQuerySusjobMemberRequest;
import com.gov.operate.service.request.SelectStatusCountRequset;
import com.gov.operate.service.response.OrderInfoDTO;
import com.gov.operate.service.response.SelectCardUserDetailResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
public interface SdSusjobDMapper extends BaseMapper<SdSusjobD> {

    IPage<SusjobDetails> querySusjobDetails(Page<SusjobDetailsQuery> page,@Param("susjobDetailQuery") SusjobDetailsQuery susjobDetailQuery);

    IPage<SdSusjobD> selectPageList(Page<SdSusjobD> page, @Param("request")SelectQuerySusjobMemberRequest request);

     SelectCardUserDetailResponse  selectCardUserDetail(@Param("cardId")String cardId,@Param("client") String client);

    SelectCardUserDetailResponse querySaleInfoByHykNo(@Param("client")String client, @Param("gsmbcCardId")String gsmbcCardId);

    // 根据会员卡号获取消费订单列表
    List<OrderInfoDTO> queryOrderInfoById(@Param("client") String client, @Param("hykNo") String hykNo, @Param("startDate") String startDate, @Param("endDate") String endDate);

    IPage<SdSusjobD> selectPages(Page<SdSusjobD> page, @Param("jobId") String jobId, @Param("client")String client, @Param("userId")String userId, @Param("susStatus")String susStatus, @Param("susSex") String susSex, @Param("stoCode") String stoCode, @Param("startNumber") Integer startNumber, @Param("endNumber") Integer endNumber, @Param("startAges") String startAges, @Param("endAges") String endAges,@Param("susjobH") SdSusjobH susjobH);

    List<SdSusjobD> selectLists(@Param("jobId") String jobId, @Param("client")String client,@Param("userId")String userId,@Param("susSex") String susSex,@Param("stoCode") String stoCode,@Param("startNumber") Integer startNumber,@Param("endNumber") Integer endNumber,@Param("startAges") String startAges,@Param("endAges") String endAges,@Param("susjobH")SdSusjobH susjobH);

}
