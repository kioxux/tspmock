package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceBalanceDTO {

    /**
     * 发票号
     */
    private String invoiceNum;

    /**
     * 本次登记后 发票已登记金额
     */
    private BigDecimal invoiceRegistered;
}

