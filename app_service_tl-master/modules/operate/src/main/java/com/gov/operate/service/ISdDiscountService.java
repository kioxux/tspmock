package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.SaveGsmSettingCountListVO;
import com.gov.operate.entity.SdDiscount;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
public interface ISdDiscountService extends SuperService<SdDiscount> {

    /**
     * 保存对应活动设置主表下的折扣数据
     */
    void saveBatchOfMarketing(List<SaveGsmSettingCountListVO> list, String client, String marketId);

    /**
     * 对应营销活动折扣数据另存
     */
    void saveAsDiscount(String cient, String marketId, String marketIdNew);

    /**
     * 最大数字编码 活动id+门店+加盟商
     */
    String getMaxNumCountId(String client, String marketId, String storeCode);
}
