package com.gov.operate.mapper;

import com.gov.operate.entity.SalaryMonthWageJobs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface SalaryMonthWageJobsMapper extends BaseMapper<SalaryMonthWageJobs> {

}
