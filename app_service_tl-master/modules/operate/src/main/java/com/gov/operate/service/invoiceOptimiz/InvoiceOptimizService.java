package com.gov.operate.service.invoiceOptimiz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.dto.invoiceOptimiz.FicoInvoiceInformationRegistrationDto;
import com.gov.operate.entity.FicoInvoiceInformationRegistration;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.15
 */
public interface InvoiceOptimizService {

    /**
     * 发票集合
     *
     * @param pageNum
     * @param pageSize
     * @param invoiceSiteType
     * @param siteCode
     * @param supCode
     * @param invoiceType
     * @param invoiceNum
     * @param invoiceDate
     * @return
     */
    IPage<FicoInvoiceInformationRegistrationDto> getInvoiceList(Integer pageNum, Integer pageSize, String invoiceSiteType, String siteCode, String supCode, String invoiceType, String invoiceNum, String invoiceDate, Integer status, String invoiceSalesman);

    /**
     * 发票保存
     *
     * @param list
     */
    void saveInvoiceList(List<FicoInvoiceInformationRegistration> list);

    /**
     * 发票详情
     *
     * @param invoiceNum
     * @return
     */
    FicoInvoiceInformationRegistration getInvoice(String invoiceNum);

    /**
     * 发票删除
     *
     * @param invoiceNum
     */
    void delInvoice(String invoiceNum);

    /**
     * 发票编辑
     *
     * @param ficoInvoiceInformationRegistration
     */
    void editInvoice(FicoInvoiceInformationRegistrationDto ficoInvoiceInformationRegistration);

    /**
     * 发票合计
     *
     * @param invoiceSiteType
     * @param siteCode
     * @param supCode
     * @param invoiceType
     * @param invoiceNum
     * @param invoiceDate
     * @return
     */
    FicoInvoiceInformationRegistrationDto getInvoiceTotal(String invoiceSiteType, String siteCode, String supCode, String invoiceType,
                                                          String invoiceNum, String invoiceDate, Integer status);
}
