package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_PROGRAM")
@ApiModel(value="SalaryProgram对象", description="")
public class SalaryProgram extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSP_ID", type = IdType.AUTO)
    private Integer gspId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "系数类型")
    @TableField("GSP_TYPE_SALARY")
    private Integer gspTypeSalary;

    @ApiModelProperty(value = "基本工资")
    @TableField("GSP_BASIC_SALARY")
    private BigDecimal gspBasicSalary;

    @ApiModelProperty(value = "岗位工资")
    @TableField("GSP_POST_SALARY")
    private BigDecimal gspPostSalary;

    @ApiModelProperty(value = "绩效工资 ")
    @TableField("GSP_PERFORMANCE_SALARY")
    private BigDecimal gspPerformanceSalary;

    @ApiModelProperty(value = "考评工资")
    @TableField("GSP_EVALUATION_SALARY")
    private BigDecimal gspEvaluationSalary;

    @ApiModelProperty(value = "方案名称")
    @TableField("GSP_NAME")
    private String gspName;

    @ApiModelProperty(value = "生效日期")
    @TableField("GSP_EFFECTIVE_DATE")
    private String gspEffectiveDate;

    @ApiModelProperty(value = "目的")
    @TableField("GSP_PURPOSE")
    private String gspPurpose;

    @ApiModelProperty(value = "当前索引")
    @TableField("GSP_INDEX")
    private Integer gspIndex;

    @ApiModelProperty(value = "营业额权重")
    @TableField("GSP_TURNOVER")
    private BigDecimal gspTurnover;

    @ApiModelProperty(value = "毛利额权重")
    @TableField("GSP_GROSS_PROFIT")
    private BigDecimal gspGrossProfit;

    @ApiModelProperty(value = "新店综合达成率计算")
    @TableField("GSP_IS_NEW_STORE")
    private Integer gspIsNewStore;

    @ApiModelProperty(value = "开业前月综合达成率计算月分")
    @TableField("GSP_OPEN_BEFORE_MONTH")
    private Integer gspOpenBeforeMonth;

    @ApiModelProperty(value = "新店综合达成率")
    @TableField("GSP_NEW_ACHIEVE_RATE")
    private BigDecimal gspNewAchieveRate;

    @ApiModelProperty(value = "首月整月天数")
    @TableField("GSP_FIRSTMONTH_DAYS")
    private Integer gspFirstmonthDays;

    @ApiModelProperty(value = "月基本工资关联达成系数")
    @TableField("GSP_SALARY_REACH")
    private Integer gspSalaryReach;

    @ApiModelProperty(value = "月基岗位资关联达成系数")
    @TableField("GSP_POST_REACH")
    private Integer gspPostReach;

    @ApiModelProperty(value = "销售提成 ")
    @TableField("GSP_SALES_COMMISSION")
    private Integer gspSalesCommission;

    @ApiModelProperty(value = "毛利提成")
    @TableField("GSP_GROSS_MARGIN")
    private Integer gspGrossMargin;

    @ApiModelProperty(value = "负毛利率是否提成")
    @TableField("GSP_NEGATIVE_GROSS_MARGIN")
    private Integer gspNegativeGrossMargin;

    @ApiModelProperty(value = "单品不参与销售提成")
    @TableField("GSP_PRO_REACH")
    private Integer gspProReach;

    @ApiModelProperty(value = "月绩效关联达成系数")
    @TableField("GSP_SALES_REACH")
    private Integer gspSalesReach;

    @ApiModelProperty(value = "考评工资关联达成系数")
    @TableField("GSP_APPRAISAL_SALARY")
    private Integer gspAppraisalSalary;

    @ApiModelProperty(value = "季度奖提成")
    @TableField("GSP_QUARTE_QUARTERLY_AWARDS")
    private Integer gspQuarteQuarterlyAwards;

    @ApiModelProperty(value = "不参与季度奖提成")
    @TableField("GSP_QUARTE_COMPUTE_ACHIEV_RATE")
    private Integer gspQuarteComputeAchievRate;

    @ApiModelProperty(value = "不参与季度奖提成最大值")
    @TableField("GSP_QUARTE_ACHIEV_RATE_MIN")
    private BigDecimal gspQuarteAchievRateMin;

    @ApiModelProperty(value = "关联季度员工个人系数")
    @TableField("GSP_QUARTER_KPI")
    private Integer gspQuarterKpi;

    @ApiModelProperty(value = "关联季度综合达成系数")
    @TableField("GSP_QUARTER_APPRAISAL_SALARY")
    private Integer gspQuarterAppraisalSalary;

    @ApiModelProperty(value = "年终奖提成")
    @TableField("GSP_YEAR_QUARTERLY_AWARDS")
    private Integer gspYearQuarterlyAwards;

    @ApiModelProperty(value = "不参与年终奖提成")
    @TableField("GSP_YEAR_COMPUTE_ACHIEV_RATE")
    private Integer gspYearComputeAchievRate;

    @ApiModelProperty(value = "不参与年终奖提成最大值")
    @TableField("GSP_YEAR_ACHIEV_RATE_MIN")
    private BigDecimal gspYearAchievRateMin;

    @ApiModelProperty(value = "关联年度员工个人系数")
    @TableField("GSP_YEAR_KPI")
    private Integer gspYearKpi;

    @ApiModelProperty(value = "关联年度综合达成系数")
    @TableField("GSP_YEAR_APPRAISAL_SALARY")
    private Integer gspYearAppraisalSalary;

    @ApiModelProperty(value = "试算报表")
    @TableField("GSP_TRIAL_REPORT")
    private String gspTrialReport;

    @ApiModelProperty(value = "试算薪资")
    @TableField("GSP_TRIAL_SALARY")
    private String gspTrialSalary;

    @ApiModelProperty(value = "说明版本")
    @TableField("GSP_DESCRIPTION")
    private String gspDescription;

    @ApiModelProperty(value = "工作流编号")
    @TableField("GSP_FLOW_NO")
    private String gspFlowNo;

    @ApiModelProperty(value = "审批状态")
    @TableField("GSP_APPROVAL_STATUS")
    private Integer gspApprovalStatus;

    @ApiModelProperty(value = "审批日期")
    @TableField("GSP_APPROVAL_DATE")
    private String gspApprovalDate;

    @ApiModelProperty(value = "审批时间")
    @TableField("GSP_APPROVAL_TIME")
    private String gspApprovalTime;

    @ApiModelProperty(value = "创建日期")
    @TableField("GSP_CREATE_DATE")
    private String gspCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GSP_CREATE_TIME")
    private String gspCreateTime;

    @ApiModelProperty(value = "创建人")
    @TableField("GSP_CREATE_USER")
    private String gspCreateUser;

    @ApiModelProperty(value = "修改日期")
    @TableField("GSP_CHANGE_DATE")
    private String gspChangeDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("GSP_CHANGE_TIME")
    private String gspChangeTime;

    @ApiModelProperty(value = "修改人")
    @TableField("GSP_CHANGE_USER")
    private String gspChangeUser;

    @ApiModelProperty(value = "负毛利商品不参与季度奖提成")
    @TableField("GSP_QUARTER_NEGATIVE_COMMISSION")
    private Integer gspQuarterNegativeCommission;

    @ApiModelProperty(value = "负毛利商品不参与年终奖提成")
    @TableField("GSP_YEAR_NEGATIVE_COMMISSION")
    private Integer gspYearNegativeCommission;


}
