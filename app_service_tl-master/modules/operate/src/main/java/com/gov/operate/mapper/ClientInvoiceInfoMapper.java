package com.gov.operate.mapper;

import com.gov.operate.entity.ClientInvoiceInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
public interface ClientInvoiceInfoMapper extends BaseMapper<ClientInvoiceInfo> {

}
