package com.gov.operate.mapper;

import com.gov.operate.dto.ActiveEvalVO;
import com.gov.operate.entity.SdMarketingAnalysis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
public interface SdMarketingAnalysisMapper extends BaseMapper<SdMarketingAnalysis> {

    /**
     * 报表分析查询
     * @param activeEvalVO
     * @return
     */
    SdMarketingAnalysis selectAllAnalysisInfo(@Param("activeEvalVO") ActiveEvalVO activeEvalVO);

    /**
     * 根据查询条件查询活动分析
     * @param activeEvalVO
     * @return
     */
    SdMarketingAnalysis selectAnalysisInfoByParam(@Param("activeEvalVO") ActiveEvalVO activeEvalVO);

    /**
     * 营销活动主题分析
     */
    SdMarketingAnalysis getStandardValue(@Param("client") String client);
}
