package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.entity.SdPromGroup;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 促销商品分组表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
public interface ISdPromGroupService extends SuperService<SdPromGroup> {

    /**
     * 促销商品组列表
     *
     * @param pageNum
     * @param pageSize
     * @param groupName
     * @param groupProId
     * @return
     */
    Result getSdPromGroupList(Integer pageNum, Integer pageSize, String groupName, String groupProId);

    /**
     * 新增
     *
     * @param groupName
     * @return
     */
    Result addSdPromGroup(String groupName);

    /**
     * 编辑
     *
     * @param groupId
     * @param groupName
     * @return
     */
    Result editSdPromGroup(String groupId, String groupName);

    /**
     * 删除
     *
     * @param groupId
     * @return
     */
    Result delSdPromGroup(String groupId);

    /**
     * 促销商品组下拉框
     *
     * @return
     */
    Result getGroupProList();
}
