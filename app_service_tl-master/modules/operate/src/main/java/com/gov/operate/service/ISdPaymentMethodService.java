package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SdPaymentMethod;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface ISdPaymentMethodService extends SuperService<SdPaymentMethod> {
    List<SdPaymentMethod> getPayWay();

}
