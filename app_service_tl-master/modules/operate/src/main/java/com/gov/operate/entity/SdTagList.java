package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_TAG_LIST")
@ApiModel(value="SdTagList对象", description="")
public class SdTagList extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "标签编号")
    @TableId("GSTL_ID")
    private String gstlId;

    @ApiModelProperty(value = "标签名称")
    @TableField("GSTL_NAME")
    private String gstlName;

    @ApiModelProperty(value = "成分编号")
    @TableField("GSTL_COMP_ID")
    private String gstlCompId;

    @ApiModelProperty(value = "成分名称")
    @TableField("GSTL_COMP_NAME")
    private String gstlCompName;

    @ApiModelProperty(value = "标签颜色")
    @TableField("GSTL_COLOR")
    private String gstlColor;


}
