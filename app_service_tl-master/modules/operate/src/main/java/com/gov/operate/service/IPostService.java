package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.Post;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
public interface IPostService extends SuperService<Post> {

    /**
     * 岗位列表
     */
    List<Post> getPostList();
}
