package com.gov.operate.dto;

import lombok.Data;

/**
 * @author: Yzf
 * description: TODO
 * create time: 2022/1/24 10:13
 * @params 
 * @return
 */
@Data
public class PromUnitaryProductVO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gspusVoucherId;

    /**
     * 行号
     */
    private String gspusSerial;

    /**
     * 商品编码
     */
    private String gspusProId;

    /**
     * 效期天数
     */
    private String gspusVaild;

    /**
     * 达到数量1
     */
    private String gspusQty1;

    /**
     * 促销价1
     */
    private String gspusPrc1;

    /**
     * 促销折扣1
     */
    private String gspusRebate1;

    /**
     * 达到数量2
     */
    private String gspusQty2;

    /**
     * 促销价2
     */
    private String gspusPrc2;

    /**
     * 促销折扣2
     */
    private String gspusRebate2;

    /**
     * 达到数量3
     */
    private String gspusQty3;

    /**
     * 促销价3
     */
    private String gspusPrc3;


    /**
     * 促销折扣3
     */
    private String gspusRebate3;

    /**
     * 是否会员
     */
    private String gspusMemFlag;

    /**
     * 是否积分
     */
    private String gspusInteFlag;

    /**
     * 积分倍率
     */
    private String gspusInteRate;

    /**
     * 是否限价
     */
    private String gspusLimitFlag;

    /**
     * 限价类型
     */
    private String gspusLimitType;

}

