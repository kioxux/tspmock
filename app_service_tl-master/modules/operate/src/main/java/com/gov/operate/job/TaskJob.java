package com.gov.operate.job;

import com.alibaba.fastjson.JSON;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.common.basic.StringUtils;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.service.*;
import com.gov.operate.service.batchPurchaseSales.IBatchQueryAuthService;
import com.gov.operate.service.category.ICategoryAnalyseService;
import com.gov.operate.service.popularize.IPayingHeaderNewService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.12
 */
@Component
public class TaskJob {

    @Resource
    private IStorePaymentService storePaymentService;
    @Resource
    private ICategoryAnalyseService categoryAnalyseService;
    @Resource
    private ISdMarketingBasic3Service sdMarketingBasicService;
    @Resource
    private ISdSusjobHService sdSusjobHService;
    @Resource
    private IOfficialAccountsService officialAccountsService;
    @Resource
    private ISalesInvoiceService iSalesInvoiceService;
    @Resource
    private IPurchaseRemindService iPurchaseRemindService;
    @Resource
    private IPayingHeaderNewService payingHeaderNewService;
    @Resource
    private IFicoInvoiceService ficoInvoiceService;
    @Resource
    private IProductMatchZService iProductMatchZService;
    @Resource
    private IBatchQueryAuthService iBatchQueryAuthService;
    @Resource
    private IMessageService messageService;

    /**
     * 付款提醒推送
     *
     * @param param
     * @return
     */
    @XxlJob("payReminderHandler")
    public ReturnT<String> payReminder(String param) {
        XxlJobHelper.log("付款提醒推送 定时任务开始 ");
        storePaymentService.payReminder();
        XxlJobHelper.log("付款提醒推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    @XxlJob("clinetPayReminder")
    public ReturnT<String> clinetPayReminder(String param) {
        XxlJobHelper.log("付款提醒推送 定时任务开始 ");
        payingHeaderNewService.payReminder();
        XxlJobHelper.log("付款提醒推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    @XxlJob("payCallbackHandler")
    public ReturnT<String> payCallback(String param) throws Exception {
        XxlJobHelper.log("门店付款 定时任务开始 ");
        payingHeaderNewService.payCallback();
        XxlJobHelper.log("门店付款 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    @XxlJob("payExpireHandler")
    public ReturnT<String> payExpire(String param) throws Exception {
        XxlJobHelper.log("付款超时 定时任务开始 ");
        payingHeaderNewService.payExpire();
        XxlJobHelper.log("付款超时 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 生成发票 定时任务
     *
     * @throws Exception
     */
    @XxlJob("initInvoiceHandler")
    public ReturnT<String> initInvoice(String param) throws Exception {
        XxlJobHelper.log("生成发票 定时任务开始 ");
        ficoInvoiceService.initInvoice();
        XxlJobHelper.log("生成发票 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 品类分析定时执行推送
     *
     * @param param
     * @return
     */
    @XxlJob("modifyCategoryAnalyseHandler")
    public ReturnT<String> modifyAdjustPrice(String param) {
        XxlJobHelper.log("品类分析定时执行推送 定时任务开始 ");
        categoryAnalyseService.timerUpdateAdjustPrice(param);
        XxlJobHelper.log("品类分析定时执行推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 品类分析自动放弃推送
     *
     * @param param
     * @return
     */
    @XxlJob("cancelCategoryAnalyseHandler")
    public ReturnT<String> timerCancelOperate(String param) {
        XxlJobHelper.log("品类分析自动放弃推送 定时任务开始 ");
        categoryAnalyseService.timerCancelOperate(param);
        XxlJobHelper.log("品类分析自动放弃推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 营销推送
     */
    @XxlJob("marketingPushReminderHandler")
    public ReturnT<String> marketingPushReminderHandler(String param) {
        XxlJobHelper.log("营销推送 定时任务开始 ");
        sdMarketingBasicService.marketingPushReminder();
        XxlJobHelper.log("营销推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 营销任务会员宣传短信发送
     */
    @XxlJob("marketingSmsSendReminderHandler")
    public ReturnT<String> marketingSmsSendReminderHandler(String param) {
        XxlJobHelper.log("营销任务会员宣传短信发送 定时任务开始 ");
        sdMarketingBasicService.marketingSmsSendReminder();
        XxlJobHelper.log("营销任务会员宣传短信发送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }


    /**
     * 会员维系任务更新
     */
    @XxlJob("susJobUpdateHandler")
    public ReturnT<String> susJobUpdateHandler(String param) {
        XxlJobHelper.log("会员维系任务消息推送 定时任务开始 ");
        sdSusjobHService.pushMessage(param);
        XxlJobHelper.log("会员维系任务消息推送 定时任务结束 ");

        XxlJobHelper.log("会员维系任务推送状态更新 定时任务开始 ");
        sdSusjobHService.updatePushStatus();
        XxlJobHelper.log("会员维系任务推送状态更新 定时任务结束 ");


        XxlJobHelper.log("会员维系任务状态更新 定时任务开始 ");
        sdSusjobHService.updateTaskStatus();
        XxlJobHelper.log("会员维系任务状态更新 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 微信公众号定时推送
     */
    @XxlJob("tweetsPushJobHandler")
    public ReturnT<String> tweetsPushJobHandler(String param) {
        XxlJobHelper.log("微信公众号推送 定时任务开始 ");
        officialAccountsService.tweetsPushJob();
        XxlJobHelper.log("微信公众号推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 销售开票定时任务
     */
    @XxlJob("salesInvoiceJobHandler")
    public ReturnT<String> salesInvoiceJobHandler(String param) throws Exception {
        XxlJobHelper.log("销售开票发票开票结果 定时任务开始 ");
        iSalesInvoiceService.invoiceReturnNew();
        XxlJobHelper.log("销售开票发票开票结果 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 发送微信购药提醒
     *
     * @param param
     * @return
     */
    @XxlJob("sendPurchaseRemindJobHandler")
    public ReturnT<String> sendPurchaseRemindJobHandler(String param) {
        XxlJobHelper.log("发送微信购药提醒 定时任务开始 ");
        iPurchaseRemindService.sendPurchaseRemind();
        XxlJobHelper.log("发送微信购药提醒 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 商品校验如果校验负责人没有确认，上线5天后进行二次通知
     *
     * @param
     * @return
     */
    @XxlJob("productMatchCheckConfirmNoNotifyJobHandler")
    public ReturnT<String> productMatchCheckConfirmNoNotifyJobHandler() {
        XxlJobHelper.log("商品校验如果校验负责人没有确认，二次推送触发 ");
        iProductMatchZService.confirmNoNotify();
        XxlJobHelper.log("商品校验如果校验负责人没有确认，二次推送结束");
        return ReturnT.SUCCESS;
    }

    /**
     * @param
     * @return
     */
    @XxlJob("productMatchCheckResultNotifyJobHandler")
    public ReturnT<String> productMatchCheckResultNotifyJobHandler() {
        XxlJobHelper.log("商品校验结果推送开始");
        iProductMatchZService.resultNotifyJobHandler();
        XxlJobHelper.log("商品校验结果推送结束");
        return ReturnT.SUCCESS;
    }

    /**
     * 经营月报
     *
     * @return ReturnT.SUCCESS
     */
    @XxlJob("businessMonthlyReport")
    public ReturnT<String> businessMonthlyReport() {
        String param = XxlJobHelper.getJobParam();
        if (StringUtils.isEmpty(param)) {
            XxlJobHelper.log(new IllegalArgumentException("参数不能为空"));
            return ReturnT.FAIL;
        }
        XxlJobHelper.log("经营月报推送开始 {}", param);
        MessageParams messageParams = JsonUtils.jsonToBean(param, MessageParams.class);
        LocalDate localDate = LocalDate.now().minusMonths(1);
        messageParams.setContentParmas(Collections.singletonList(localDate.format(DateTimeFormatter.ofPattern("yyyy年MM月"))));
        Map<String, String> extParam = new HashMap<>();
        extParam.put("date", localDate.format(DateTimeFormatter.ofPattern("yyyyMM")));
        messageParams.setParams(extParam);
        messageService.pushBusinessMonthlyReportMsg(messageParams);
        XxlJobHelper.log("经营月报推送结束");
        return ReturnT.SUCCESS;
    }

    /**
     * 促销报告
     *
     * @return ReturnT.SUCCESS
     */
    @XxlJob("promotionReport")
    public ReturnT<String> promotionReport() {
        String param = XxlJobHelper.getJobParam();
        if (StringUtils.isEmpty(param)) {
            XxlJobHelper.log(new IllegalArgumentException("参数不能为空"));
            return ReturnT.FAIL;
        }
        MessageParams messageParams = JsonUtils.jsonToBean(param, MessageParams.class);
        XxlJobHelper.log("促销报告推送开始 {}", JSON.toJSONString(param));
        messageService.pushPromotionReportMsg(messageParams);
        XxlJobHelper.log("促销报告推送结束");
        return ReturnT.SUCCESS;
    }

    @XxlJob("ddiHandler")
    public ReturnT<String> ddiHandler(String param) {
        XxlJobHelper.log("总部采购开始");
        iBatchQueryAuthService.ddiHandler();
        XxlJobHelper.log("总部采购结束");
        return ReturnT.SUCCESS;
    }


}
