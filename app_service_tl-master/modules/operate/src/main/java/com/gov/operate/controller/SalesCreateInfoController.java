package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 开票人员基本信息 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
@RestController
@RequestMapping("/operate/sales-create-info")
public class SalesCreateInfoController {

}

