package com.gov.operate.service.ssp.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.SspMenu;
import com.gov.operate.service.ssp.ISspMenuService;
import com.gov.operate.mapper.SspMenuMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SspMenuServiceImpl extends ServiceImpl<SspMenuMapper, SspMenu>
    implements ISspMenuService {

}




