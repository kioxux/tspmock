package com.gov.operate.dto.invoice;

import lombok.Data;

@Data
public class SalesInvoiceDetailDTO {

    private String client;

    /**
     * 单号
     */
    private String gsiOrderId;

}
