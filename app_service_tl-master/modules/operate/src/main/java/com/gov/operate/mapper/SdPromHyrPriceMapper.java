package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromHyrPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
public interface SdPromHyrPriceMapper extends BaseMapper<SdPromHyrPrice> {

    IPage<ProductResponseVo> selectProductList(Page<SdPromHyrPrice> page, @Param("product") ProductRequestVo productRequestVo);

    void saveOtherStoHySet(@Param("client") String client, @Param("firstVoucherId") String firstVoucherId, @Param("otherVoucherId") String otherVoucherId);
}
