package com.gov.operate.service.impl;

import com.gov.operate.entity.PoHeader;
import com.gov.operate.mapper.PoHeaderMapper;
import com.gov.operate.service.IPoHeaderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 采购订单主表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
@Service
public class PoHeaderServiceImpl extends ServiceImpl<PoHeaderMapper, PoHeader> implements IPoHeaderService {

}
