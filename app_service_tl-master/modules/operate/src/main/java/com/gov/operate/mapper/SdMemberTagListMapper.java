package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.MemberDistributionVO;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.entity.SdMemberTagList;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
public interface SdMemberTagListMapper extends BaseMapper<SdMemberTagList> {

    List<MemberDistributionVO> getTagDistributionList(MemberReportRequestDto requestDto);

    List<MemberDistributionVO>  getTagDistributionListNew(MemberReportRequestDto requestDto);
}
