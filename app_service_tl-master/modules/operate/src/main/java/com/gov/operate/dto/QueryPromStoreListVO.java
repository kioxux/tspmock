package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class QueryPromStoreListVO {
    private Integer gsphMarketid;
}
