package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.operate.entity.SalaryUserKpi;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SalaryUserKpiMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISalaryUserKpiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryUserKpiServiceImpl extends ServiceImpl<SalaryUserKpiMapper, SalaryUserKpi> implements ISalaryUserKpiService {

    @Resource
    private CommonService commonService;
    /**
     * 上月所有员工KPI列表
     */
    @Override
    public List<SalaryUserKpi> getUserKpiList() {
        TokenUser user = commonService.getLoginInfo();
        LocalDate now = LocalDate.now();
        int year = now.getYear();
        int month = now.getMonth().minus(1).getValue();
        List<SalaryUserKpi> list = this.list(new QueryWrapper<SalaryUserKpi>()
                .eq("GSUK_YEAR", year)
                .eq("GSUK_MONTH", month)
                .eq("CLIENT", user.getClient()));
        return list;
    }
}
