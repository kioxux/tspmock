package com.gov.operate.service;

import com.gov.operate.entity.SalesInvoiceDetail;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2021-02-07
 */
public interface ISalesInvoiceDetailService extends SuperService<SalesInvoiceDetail> {

}
