package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_INVOICE_BILL_INVOICE")
@ApiModel(value="InvoiceBillInvoice对象", description="")
public class InvoiceBillInvoice extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GIBI_NUM")
    private String gibiNum;

    @ApiModelProperty(value = "发票号")
    @TableField("GIBI_INVOICE_NUM")
    private String gibiInvoiceNum;

    @ApiModelProperty(value = "创建人")
    @TableField("GIBI_CREATE_USER")
    private String gibiCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GIBI_CREATE_DATE")
    private String gibiCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GIBI_CREATE_TIME")
    private String gibiCreateTime;


}
