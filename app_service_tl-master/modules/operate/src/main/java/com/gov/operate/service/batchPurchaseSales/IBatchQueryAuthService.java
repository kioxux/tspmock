package com.gov.operate.service.batchPurchaseSales;

import com.gov.operate.entity.BatchQueryAuth;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 查询数据权限 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
public interface IBatchQueryAuthService extends SuperService<BatchQueryAuth> {

    void ddiHandler();
}
