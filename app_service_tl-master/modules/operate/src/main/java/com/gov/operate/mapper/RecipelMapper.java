package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.GaiaSdRecipelList;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2020-07-20
 */
public interface RecipelMapper extends BaseMapper<GaiaSdRecipelList> {

}
