package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_SERIES_CONDS")
@ApiModel(value="SdPromSeriesConds对象", description="")
public class SdPromSeriesConds extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPSC_VOUCHER_ID")
    private String gspscVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPSC_SERIAL")
    private String gspscSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPSC_PRO_ID")
    private String gspscProId;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPSC_SERIES_ID")
    private String gspscSeriesId;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPSC_MEM_FLAG")
    private String gspscMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPSC_INTE_FLAG")
    private String gspscInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPSC_INTE_RATE")
    private String gspscInteRate;


}
