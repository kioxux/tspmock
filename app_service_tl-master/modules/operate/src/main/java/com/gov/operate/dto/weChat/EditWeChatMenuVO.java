package com.gov.operate.dto.weChat;

import com.gov.common.entity.weChat.CreateWeChatMenuParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode
public class EditWeChatMenuVO extends CreateWeChatMenuParam {

    @NotBlank(message = "加盟不能为空")
    private String client;
    @NotBlank(message = "对象编码不能为空")
    private String goaCode;

}
