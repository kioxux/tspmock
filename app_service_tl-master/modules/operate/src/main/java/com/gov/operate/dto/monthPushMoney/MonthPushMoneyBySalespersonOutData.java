package com.gov.operate.dto.monthPushMoney;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MonthPushMoneyBySalespersonOutData {
    private String client;

    /**
     * 数据状态 D:本期 S:上周
     */
    private String gssdType;


    /**
     * 提成合计
     */
    private String deductionWage = "0";

    /**
     * 销售提成
     */
    private String  deductionWageSales = "0";

    /**
     * 单品提成
     */
    private String  deductionWagePro = "0";

}
