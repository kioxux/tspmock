package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.entity.SdMemberTagList;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
public interface ISdMemberTagListService extends SuperService<SdMemberTagList> {

    /**
     * 会员分布
     *
     * @param requestDto
     * @return
     */
    Result getTagDistributionList(MemberReportRequestDto requestDto);

    Result tagDistributionListNew(MemberReportRequestDto requestDto);
}
