package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/14 11:25
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetProductDTO {
    /**
     * 商品编号
     */
    private String gscdProId;

    /**
     * 通用名称
     */
    private String proCommonname;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产企业
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 批准文号
     */
    private String proRegisterNo;
}
