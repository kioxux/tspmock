package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.gov.operate.entity.FiUserPaymentMaintenance;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.01
 */
@Data
public class UserPaymentMaintenance extends FiUserPaymentMaintenance {

    /**
     * 加盟商名
     */
    private String francName;

    /**
     * 上期截止日期
     */
    private String ficoLastDeadline;
}
