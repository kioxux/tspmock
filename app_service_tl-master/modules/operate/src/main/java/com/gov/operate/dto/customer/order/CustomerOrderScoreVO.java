package com.gov.operate.dto.customer.order;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021-06-23 12:57
 */
@Data
public class CustomerOrderScoreVO {

    @NotNull(message = "主键不能为空")
    private Long id;
    /**
     * 是否已解决问题
     */
    private Integer gwoUserScore;
    /**
     * 服务态度
     */
    private Integer gwoServiceAttitudeScore;
    /**
     * 解决速度
     */
    private Integer gwoSpeedSolutionScore;
    /**
     * 系统是否好用
     */
    private Integer gwoEasyToUseScore;

}
