package com.gov.operate.mapper;


import com.gov.operate.entity.GaiaAlPlQjmd;

import java.util.HashMap;
import java.util.List;

public interface GaiaAlPlExecMapper {
    List<GaiaAlPlQjmd> getSaleSummaryByArea(GaiaAlPlQjmd inData);

    List<GaiaAlPlQjmd> getSaleInfoByComp(HashMap<String,Object> inHashMap);
}
