package com.gov.operate.service.impl;

import com.gov.operate.entity.SalesCreateInfo;
import com.gov.operate.mapper.SalesCreateInfoMapper;
import com.gov.operate.service.ISalesCreateInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 开票人员基本信息 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
@Service
public class SalesCreateInfoServiceImpl extends ServiceImpl<SalesCreateInfoMapper, SalesCreateInfo> implements ISalesCreateInfoService {

}
