package com.gov.operate.dto;

import lombok.Data;

@Data
public class ShopAssistant {
    private String client;
    private String day;
    private String amt;
    private String grossAmt;
    private String userId;
    private String userName;
    private String noQty;
    private String average;
    private String pct;


}
