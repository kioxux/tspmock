package com.gov.operate;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringCloudApplication
@ComponentScan(basePackages = {"com.gov.*"})
@MapperScan("com.gov.*.mapper")
@EnableFeignClients({"com.gov.*.feign"})
public class OperateApplication { 


    public static void main(String[] args) {
        SpringApplication.run(OperateApplication.class, args);
    }
}
