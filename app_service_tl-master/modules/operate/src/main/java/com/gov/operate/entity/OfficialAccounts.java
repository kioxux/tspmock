package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_OFFICIAL_ACCOUNTS")
@ApiModel(value="OfficialAccounts对象", description="")
public class OfficialAccounts extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "类型")
    @TableField("GOA_TYPE")
    private Integer goaType;

    @ApiModelProperty(value = "对象编码")
    @TableField("GOA_CODE")
    private String goaCode;

    @ApiModelProperty(value = "参数")
    @TableField("GOA_OBJECT_PARAM")
    private String goaObjectParam;

    @ApiModelProperty(value = "值")
    @TableField("GOA_OBJECT_VALUE")
    private String goaObjectValue;


}
