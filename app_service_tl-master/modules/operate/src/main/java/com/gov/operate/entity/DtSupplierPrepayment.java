package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DT_SUPPLIER_PREPAYMENT")
@ApiModel(value = "DtSupplierPrepayment对象", description = "")
public class DtSupplierPrepayment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "付款单号")
    @TableField("PAYMENT_ORDER_NO")
    private String paymentOrderNo;

    @ApiModelProperty(value = "业务单号")
    @TableField("MAT_DN_ID")
    private String matDnId;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "业务类型")
    @TableField("BUSINESS_TYPE")
    private String businessType;

    @ApiModelProperty(value = "付款类型 1单据2明细")
    @TableField("TYPE")
    private String type;

    @TableField("MAT_ID")
    private String matId;

    @TableField("MAT_YEAR")
    private String matYear;

    @TableField("MAT_LINE_NO")
    private String matLineNo;

    @TableField("PRO_CODE")
    private String proCode;

    @TableField("PRO_NAME")
    private String proName;

    @TableField("EXCLUDING_TAX_AMOUNT")
    private BigDecimal excludingTaxAmount;

    @TableField("RATE_BAT")
    private BigDecimal rateBat;

    @TableField("LINE_ID")
    private Integer lineId;

    @TableField("AMOUNT")
    private BigDecimal amount;

    @TableField("QTY")
    private BigDecimal qty;

    @TableField("APPLICATIONS_ID")
    private Integer applicationsId;

}
