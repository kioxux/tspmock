package com.gov.operate.mapper;

import com.gov.operate.dto.recharge.RechargeRefundVO;
import com.gov.operate.entity.SdSalePayMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface SdSalePayMsgMapper extends BaseMapper<SdSalePayMsg> {

    /**
     * 复合主键查询唯一值
     */
    SdSalePayMsg getSalePayMsgByCompositekey(@Param("vo") RechargeRefundVO vo, @Param("client") String client, @Param("gsspmType") String gsspmType);
}
