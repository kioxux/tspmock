package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;

@Data
public class MaterialImportVO extends Pageable {

    private String fileName;

    private String startDate;

    private String endDate;

    private Integer type;

    private Integer status;
}
