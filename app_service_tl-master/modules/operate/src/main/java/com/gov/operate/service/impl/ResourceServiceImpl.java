package com.gov.operate.service.impl;

import com.gov.operate.entity.Resource;
import com.gov.operate.mapper.ResourceMapper;
import com.gov.operate.service.IResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-17
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {

}
