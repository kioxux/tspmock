package com.gov.operate.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.service.ISmsTemplateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-13
 */
@RestController
@RequestMapping("sms")
public class SmsTemplateController {

    @Resource
    private ISmsTemplateService smsTemplateService;

    @Log("营销短信列表")
    @ApiOperation(value = "营销短信列表")
    @PostMapping("queryMarketingTemplate")
    public Result queryMarketingTemplate(@RequestBody @Valid QueryMarketingTemplateVO vo) {
        return ResultUtil.success(smsTemplateService.queryMarketingTemplate(vo));
    }

    @Log("营销模板")
    @ApiOperation(value = "营销模板")
    @PostMapping("marketingTemplateList")
    public Result marketingTemplateList() {
        return ResultUtil.success(smsTemplateService.marketingTemplateList());
    }

    @Log("营销模板新增")
    @ApiOperation(value = "营销模板新增")
    @PostMapping("addMarketingTemplate")
    public Result addMarketingTemplate(@RequestBody @Valid AddMarketingTemplateVO vo) {
        smsTemplateService.addMarketingTemplate(vo);
        return ResultUtil.success();
    }

    @Log("营销模板编辑")
    @ApiOperation(value = "营销模板编辑")
    @PostMapping("editMarketingTemplate")
    public Result editMarketingTemplate(@RequestBody @Valid EditMarketingTemplateVO vo) {
        smsTemplateService.editMarketingTemplate(vo);
        return ResultUtil.success();
    }

    @Log("营销模板测试发送")
    @ApiOperation(value = "营销模板测试发送")
    @PostMapping("testMarketingTemplate")
    public Result testMarketingTemplate(@RequestBody @Valid TestMarketingTemplateVO vo) {
        return smsTemplateService.testMarketingTemplate(vo);
    }

    @Log("营销短信发送数量是否满足校验")
    @ApiOperation(value = "营销短信发送数量是否满足校验")
    @PostMapping("checkSendMarketingSms")
    public Result checkSendMarketingSms() {
        return ResultUtil.success(smsTemplateService.checkSendMarketingSms());
    }

    @Log("营销短信发送")
    @ApiOperation(value = "营销短信发送")
    @PostMapping("sendMarketingSms")
    public Result sendMarketingSms(@RequestBody @Valid SendMarketingSmsVO vo) {
        smsTemplateService.sendMarketingSms(vo);
        return ResultUtil.success();
    }

    @Log("营销模板删除")
    @ApiOperation(value = "营销模板删除")
    @PostMapping("delMarketingTemplate")
    public Result delMarketingTemplate(@Valid @RequestBody DelMarketingTemplateVO vo) {
        smsTemplateService.delMarketingTemplate(vo);
        return ResultUtil.success();
    }

    @Log("营销短信启、停用")
    @ApiOperation(value = "营销短信启、停用")
    @PostMapping("enableMarketingTemplate")
    public Result enableMarketingTemplate(@Valid @RequestBody EnableMarketingTemplateVO vo) {
        smsTemplateService.enableMarketingTemplate(vo);
        return ResultUtil.success();
    }

    @Log("营销短详情")
    @ApiOperation(value = "营销短详情")
    @GetMapping("getMarketingTemplate")
    public Result getMarketingTemplate(@RequestParam("smsId") String smsId) {
        return ResultUtil.success(smsTemplateService.getMarketingTemplate(smsId));
    }

}

