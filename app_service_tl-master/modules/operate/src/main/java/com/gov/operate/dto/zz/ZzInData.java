package com.gov.operate.dto.zz;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ZzInData {
   private String id;
   private String client;
   private String zzOrgid;//所属部门
   //1门店 2连锁 3批发 4部门  预留
   private String zzType;
   private String zzId;//证照类别
   private String expireDay; //证照效期

   private List<String> stoCodeList;
}
