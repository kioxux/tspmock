package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_MONTH_REACH")
@ApiModel(value="SalaryMonthReach对象", description="")
public class SalaryMonthReach extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSMR_ID", type = IdType.AUTO)
    private Integer gsmrId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSMR_GSP_ID")
    private Integer gsmrGspId;

    @ApiModelProperty(value = "达成率最大值")
    @TableField("GSMR_ACHIEV_RATE_MIN")
    private BigDecimal gsmrAchievRateMin;

    @ApiModelProperty(value = "达成率最小值")
    @TableField("GSMR_ACHIEV_RATE_MAX")
    private BigDecimal gsmrAchievRateMax;

    @ApiModelProperty(value = "达成系数")
    @TableField("GSMR_ACHIEV_COEFFI")
    private BigDecimal gsmrAchievCoeffi;

    @ApiModelProperty(value = "顺序")
    @TableField("GSMR_SORT")
    private Integer gsmrSort;


}
