package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.storePayment.SmsSendRecord;
import com.gov.operate.entity.SmsSend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
public interface SmsSendMapper extends BaseMapper<SmsSend> {

    /**
     * 客户短信发送记录
     *
     * @param page
     * @param client
     * @param gssSendTimeStart
     * @param gssSendTimeEnd
     * @return
     */
    IPage<SmsSendRecord> getSmsSendRecord(Page<SmsSendRecord> page, @Param("client") String client,
                                          @Param("gssSendTimeStart") String gssSendTimeStart, @Param("gssSendTimeEnd") String gssSendTimeEnd);
}
