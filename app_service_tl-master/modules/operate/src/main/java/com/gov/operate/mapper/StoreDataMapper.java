package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.entity.StoreData;
import com.gov.operate.service.request.StoreSalesRankingRequest;
import com.gov.operate.service.request.StoreSalesStatisticsDayRequest;
import com.gov.operate.service.request.StoreSalesStatisticsRequest;
import com.gov.operate.service.response.StoreSalesRankingResponse;
import com.gov.operate.service.response.StoreSalesStatisticsResourceData;
import com.gov.operate.service.response.StoreSalesStatisticsResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface StoreDataMapper extends BaseMapper<StoreData> {

    /**
     * 获取加盟商下的门店编码组
     */
    List<String> getStoCodeList(List<String> clientList);

    /**
     * 指定加盟商下得门店
     */
    List<String> getStoreListByClient(@Param("client") String client);

    /**
     * 加盟商下门店
     */
    List<StoreData> getStoreList(String client);

    /**
     * 当前人门店列表
     *
     * @param client
     * @param userId
     * @return
     */
    List<StoreData> getCurrentStoreList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 获取门店编码
     */
    StoreData getMaxNumStoreCode(@Param("client") String client);

    List<String> selectListForAuth(@Param("client") String client, @Param("chainHead") String chainHead);

    StoreData getUserStore(@Param("client") String client, @Param("userId") String userId);

    /**
     * 门店集合，数据迁移用
     *
     * @param client
     * @return
     */
    List<Map<String, String>> getStoresList(@Param("client") String client);

    /**
     * 根据用户取权限 下的门店
     *
     * @param client
     * @param userId
     * @return
     */
    List<StoreData> getStoListByUserAuth(@Param("client") String client, @Param("userId") String userId);

    List<StoreData> getZzOrgListByUserAuth(@Param("client") String client, @Param("userId") String userId);

    /**
     * 查询所有门店
     *
     * @return
     */
    List<StoreData> getAllStore();

    IPage<List<StoreSalesRankingResponse.StoreSalesRanking>> getStoreSalesRanking(Page<StoreSalesRankingResponse> page, @Param("request") StoreSalesRankingRequest request);

    List<StoreSalesStatisticsResponse.StoreSalesThisWeek> getStoreStatistics(@Param("request") StoreSalesStatisticsRequest request, @Param("list") List<String> thisWeek);

    List<StoreSalesStatisticsResourceData> selectDataScope(StoreSalesStatisticsDayRequest query);

    List<Map<String, Object>> getSaleMember(@Param("client") String client, @Param("storeList") String[] storeList, @Param("date") List<String> date);

    List<Map<String, Object>> getStoreManager(@Param("client") String client, @Param("list") List<String> collect);

    List<Map<String, String>> selectUser(@Param("client") String client, @Param("stores") List<String> storeIds, @Param("name") String name);

    List<String> getAllClient();

    List<StoreData> selectStoreListByIsAdmin(@Param("client") String client, @Param("userId") String userId, @Param("depId") String depId);
}
