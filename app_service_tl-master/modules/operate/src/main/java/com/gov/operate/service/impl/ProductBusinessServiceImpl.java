package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.operate.dto.GetProListForSaveGroupVO;
import com.gov.operate.dto.ProductBusinessDTO;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.ProductBusinessMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IProductBusinessService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Service
public class ProductBusinessServiceImpl extends ServiceImpl<ProductBusinessMapper, ProductBusiness> implements IProductBusinessService {

    @Resource
    private ProductBusinessMapper productBusinessMapper;
    @Resource
    private CommonService commonService;
    /**
     * 商品组添加查询列表
     */
    @Override
    public IPage<ProductBusinessDTO> getProListForSaveGroup(GetProListForSaveGroupVO vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<ProductBusiness> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper query = new QueryWrapper<>()
                .eq("p.CLIENT", user.getClient())
                .eq("p.PRO_SITE", vo.getProSite())
                .like(StringUtils.isNotEmpty(vo.getProName()), "p.PRO_NAME", vo.getProName())
                .eq(StringUtils.isNotEmpty(vo.getProSelfCode()), "p.PRO_SELF_CODE", vo.getProSelfCode())
                .eq(StringUtils.isNotEmpty(vo.getProClass()), "SUBSTR(p.PRO_CLASS ,1, 1)", vo.getProClass());
        return productBusinessMapper.getProListForSaveGroup(page, query);
    }

    /**
     * 选品查询
     */
    @Override
    public IPage<ProductBusiness> getMarketProductList(ProductRequestVo productRequestVo) {

        return null;
    }
}
