package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_USER_KPI")
@ApiModel(value="SalaryUserKpi对象", description="")
public class SalaryUserKpi extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSUK_ID", type = IdType.AUTO)
    private Integer gsukId;

    @ApiModelProperty(value = "年")
    @TableField("GSUK_YEAR")
    private Integer gsukYear;

    @ApiModelProperty(value = "月")
    @TableField("GSUK_MONTH")
    private Integer gsukMonth;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工工号")
    @TableField("GSUK_USER_ID")
    private String gsukUserId;

    @ApiModelProperty(value = "KPI值")
    @TableField("GSUK_KPI")
    private BigDecimal gsukKpi;


}
