package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdRechargeCardService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Service
public class SdRechargeCardServiceImpl extends ServiceImpl<SdRechargeCardMapper, SdRechargeCard> implements ISdRechargeCardService {
    @Resource
    private CommonService commonService;
    @Resource
    private SdMemberCardMapper sdMemberCardMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private SdRechargeCardMapper sdRechargeCardMapper;
    @Resource
    private SdRechargeCardSetMapper sdRechargeCardSetMapper;
    @Resource
    private SdRechargeCardChangeMapper sdRechargeCardChangeMapper;
    @Resource
    private SdRechargeCardPaycheckMapper sdRechargeCardPaycheckMapper;

    /**
     * 会员卡号的获取
     */
    @Override
    public MemberDTO getMemberCard(MemberNoVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //门店编码
        MemberDTO memberDTO = null;
        memberDTO = sdMemberCardMapper.getMemberCard(user.getDepId(), user.getClient(), vo.getGsrcMobile());
        if (null == memberDTO) {
            throw new CustomResultException("未查询到会员卡信息");
        }
        List<SdRechargeCard> cardList = sdRechargeCardMapper.selectList(new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient()).eq("GSRC_ID", memberDTO.getGsmbcCardId()));
        if (!cardList.isEmpty()) {
            throw new CustomResultException("使用会员卡号与已有储值卡号重复，请使用自动生成卡号");
        }
        return memberDTO;

    }


    /**
     * 储值卡创建
     */
    @Override
    public SdRechargeCard addRecharge(AddRechargeVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        //查看时总部还是门店
        StoreData storeData = common();

        //验证当前门店是否存在储值卡设置表中
        SdRechargeCardSet cardSet = sdRechargeCardSetMapper.selectOne(new QueryWrapper<SdRechargeCardSet>()
                .eq("CLIENT", user.getClient())
                .eq("GSRCS_BR_ID", user.getDepId()));
        if (null == cardSet) {
            throw new CustomResultException("请先维护储值卡设置信息");
        }
        // 前缀
        String prefix = cardSet.getGsrcsCardPrefix();
        if (StringUtils.isBlank(prefix)) {
            prefix = "CZK";
        }

        //验证当前手机号是否已经绑定储值卡信息
        QueryWrapper<SdRechargeCard> queryWrapper = new QueryWrapper<SdRechargeCard>();
        queryWrapper.eq("CLIENT", user.getClient());
        queryWrapper.eq("GSRC_MOBILE", vo.getGsrcMobile());
        //连锁
        if ("2".equals(storeData.getStoAttribute())) {
            queryWrapper.eq("GSRC_BR_ID", StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead());
        } else {
            queryWrapper.eq("GSRC_BR_ID", user.getDepId());
        }
        if (sdRechargeCardMapper.selectCount(queryWrapper) > 0) {
            throw new CustomResultException("该手机号已绑定储值卡");
        }
        //验证储存卡是否唯一
        if (StringUtils.isNotBlank(vo.getGsrcId())) {
            Integer a = sdRechargeCardMapper.selectCount(new QueryWrapper<SdRechargeCard>()
                    .eq("CLIENT", user.getClient())
                    .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                    .eq("GSRC_ID", vo.getGsrcId()));
            if (a > 0) {
                throw new CustomResultException("使用会员卡号与已有储值卡号重复，请使用自动生成卡号");
            }
        }
        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        map.put("storeCode", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getClient());
        map.put("prefix", prefix);
        SdRechargeCard sdRechargeCard = sdRechargeCardMapper.getUserCardNumber(map);
        SdRechargeCard card = new SdRechargeCard();
        card.setGsrcEmp(user.getUserId());
        card.setClient(user.getClient());
        card.setGsrcAmt(BigDecimal.ZERO);
        card.setGsrcDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcPassword(cardSet.getGsrcsPassword());
        card.setGsrcStatus("1");
        card.setGsrcAccountId(sdRechargeCard.getGsrcAccountId());

        card.setGsrcBrId(user.getDepId());
        if ("2".equals(storeData.getStoAttribute())) {
            card.setGsrcBrId(StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead());
        }
        BeanUtils.copyProperties(vo, card);
        if (StringUtils.isNotBlank(vo.getGsrcId())) {
            card.setGsrcId(vo.getGsrcId());
            card.setGsrcMemberCardId(vo.getGsrcId());
        } else {
            card.setGsrcId(sdRechargeCard.getGsrcId());
        }
        sdRechargeCardMapper.insert(card);
        return card;
    }

    /**
     * 储值卡编辑
     *
     * @param sdRechargeCard
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editRecharte(SdRechargeCard sdRechargeCard) {
        TokenUser user = commonService.getLoginInfo();
        // 储值卡账户
        if (StringUtils.isBlank(sdRechargeCard.getGsrcAccountId())) {
            throw new CustomResultException("储值卡账户不能为空");
        }
        // 手机号
        if (StringUtils.isBlank(sdRechargeCard.getGsrcMobile())) {
            throw new CustomResultException("手机号不能为空");
        }
        SdRechargeCard entity = sdRechargeCardMapper.selectOne(
                new QueryWrapper<SdRechargeCard>().eq("CLIENT", user.getClient())
                        .eq("GSRC_ACCOUNT_ID", sdRechargeCard.getGsrcAccountId())
        );
        // 储值卡不存在
        if (entity == null) {
            throw new CustomResultException("当前储值卡不存在");
        }
        SdRechargeCard record = new SdRechargeCard();
        // 性别
        record.setGsrcSex(sdRechargeCard.getGsrcSex());
        // 手机不相等
        if (!sdRechargeCard.getGsrcMobile().equals(entity.getGsrcMobile())) {
            //验证当前手机号是否已经绑定储值卡信息
            QueryWrapper<SdRechargeCard> queryWrapper = new QueryWrapper<SdRechargeCard>();
            queryWrapper.eq("CLIENT", user.getClient());
            queryWrapper.eq("GSRC_MOBILE", sdRechargeCard.getGsrcMobile());
            queryWrapper.eq("GSRC_BR_ID", entity.getGsrcBrId());
            if (sdRechargeCardMapper.selectCount(queryWrapper) > 0) {
                throw new CustomResultException("该手机号已绑定储值卡");
            }
            record.setGsrcMobile(sdRechargeCard.getGsrcMobile());
        }
        // 余额
        if (sdRechargeCard.getGsrcAmt() != null) {
            // 历史余额
            BigDecimal amt = entity.getGsrcAmt() == null ? BigDecimal.ZERO : entity.getGsrcAmt();
            SdRechargeCardPaycheck sdRechargeCardPaycheck = new SdRechargeCardPaycheck();
            // 加盟商
            sdRechargeCardPaycheck.setClient(user.getClient());
            // 储值卡账户
            sdRechargeCardPaycheck.setGsrcpAccountId(entity.getGsrcAccountId());
            // 充值单号
            String payCode = sdRechargeCardPaycheckMapper.getUserCardNumber(user.getClient()).getGsrcpVoucherId();
            sdRechargeCardPaycheck.setGsrcpVoucherId(payCode);
            // 充值卡号
            sdRechargeCardPaycheck.setGsrcpCardId(entity.getGsrcId());
            // 充值门店
            sdRechargeCardPaycheck.setGsrcpBrId(entity.getGsrcBrId());
            // 充值日期
            sdRechargeCardPaycheck.setGsrcpDate(DateUtils.getCurrentDateStrYYMMDD());
            // 充值时间
            sdRechargeCardPaycheck.setGsrcpTime(DateUtils.getCurrentTimeStrHHMMSS());
            // 充值人员
            sdRechargeCardPaycheck.setGsrcpEmp(user.getUserId());
            // 应收金额
            sdRechargeCardPaycheck.setGsrcpYsAmt(sdRechargeCard.getGsrcAmt().subtract(amt));
            // 充值金额
            sdRechargeCardPaycheck.setGsrcpAfterAmt(sdRechargeCard.getGsrcAmt().subtract(amt));
            // 是否日结 0/空为否1为是
            // 余额变动
            if (amt.compareTo(sdRechargeCard.getGsrcAmt()) != 0) {
                sdRechargeCardPaycheckMapper.insert(sdRechargeCardPaycheck);
            }
            record.setGsrcAmt(sdRechargeCard.getGsrcAmt());
        }
        sdRechargeCardMapper.update(record,
                new QueryWrapper<SdRechargeCard>().eq("CLIENT", user.getClient())
                        .eq("GSRC_ACCOUNT_ID", sdRechargeCard.getGsrcAccountId())
        );
    }

    @Override
    public Result getRechargeStoreList() {
        TokenUser user = commonService.getLoginInfo();
        // A、是则查询所有储值卡信息
        // B、否则按照当前登录门店查询：
        // a、如是直营，则查直营门店数据
        // b、如是加盟，则查当前门店数据
        List<StoreData> storeDataList = storeDataMapper.selectStoreListByIsAdmin(user.getClient(), user.getUserId(), user.getDepId());
        return ResultUtil.success(storeDataList);
    }

    /**
     * 储值卡列表
     */
    @Override
    public Result getRechargeCardList(RechargeCardListVO entity, Page<SdRechargeCard> page) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        // A、是则查询所有储值卡信息
        // B、否则按照当前登录门店查询：
        // a、如是直营，则查直营门店数据
        // b、如是加盟，则查当前门店数据
        List<StoreData> storeDataList = storeDataMapper.selectStoreListByIsAdmin(user.getClient(), user.getUserId(), user.getDepId());
        if (CollectionUtils.isEmpty(storeDataList)) {
            ResultUtil.success(new Page<SdRechargeCard>());
        }
        List<String> gsrcBrIdList = storeDataList.stream().map(StoreData::getStoCode).collect(Collectors.toList());
        List<String> stoChainHeadList = storeDataList.stream().filter(t -> StringUtils.isNotBlank(t.getStoChainHead())).map(StoreData::getStoChainHead).collect(Collectors.toList());
        gsrcBrIdList.addAll(stoChainHeadList);
        IPage<SdRechargeCard> pageList = sdRechargeCardMapper.selectPage(page, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .in("GSRC_BR_ID", gsrcBrIdList)
                .eq(StringUtils.isNotBlank(entity.getGsrcStatus()), "GSRC_STATUS", entity.getGsrcStatus())
                .like(StringUtils.isNotBlank(entity.getGsrcId()), "GSRC_ID", entity.getGsrcId())
                .like(StringUtils.isNotBlank(entity.getGsrcName()), "GSRC_NAME", entity.getGsrcName())
                .like(StringUtils.isNotBlank(entity.getGsrcMobile()), "GSRC_MOBILE", entity.getGsrcMobile())
                .like(StringUtils.isNotBlank(entity.getGsrcMemberCardId()), "GSRC_MEMBER_CARD_ID", entity.getGsrcMemberCardId())
                .orderByAsc("GSRC_ACCOUNT_ID", "GSRC_DATE", "GSRC_ID"));
        return ResultUtil.success(pageList);
    }


    /**
     * 恢复初始密码
     */
    @Override
    public Result resetPassword(UpdateRechargeCardVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = common();
        //判断储值卡信息
        SdRechargeCard card = sdRechargeCardMapper.selectOne(new QueryWrapper<SdRechargeCard>()
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (null == card) {
            throw new CustomResultException("未查询到储值卡信息");
        }

        if ("3".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已作废");
        }

        String password = "123456";
        //获取卡设置的密码
        String storeCode = user.getDepId();
        if (StringUtils.isEmpty(storeCode)) {
            List<String> codeList = sdRechargeCardSetMapper.getStoreCode(user.getUserId(), user.getClient());
            if (!codeList.isEmpty()) {
                storeCode = codeList.get(0);
            }
        }

        //验证当前门店是否存在储值卡设置表中
        SdRechargeCardSet cardSet = sdRechargeCardSetMapper.selectOne(new QueryWrapper<SdRechargeCardSet>()
                .eq("CLIENT", user.getClient())
                .eq("GSRCS_BR_ID", storeCode));

        if (null != cardSet) {
            password = cardSet.getGsrcsPassword();
        }

        card.setGsrcPassword(password);
        card.setGsrcUpdateEmp(user.getUserId());
        card.setGsrcUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcUpdateBrId(user.getDepId());
        sdRechargeCardMapper.update(card, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));
        return ResultUtil.success(1);
    }

    /**
     * 修改密码
     */
    @Override
    public Result changePassword(UpdateRechargeCardPasswordVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = common();
        //判断储值卡信息
        SdRechargeCard card = sdRechargeCardMapper.selectOne(new QueryWrapper<SdRechargeCard>()
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (null == card) {
            throw new CustomResultException("未查询到储值卡信息");
        }
        if ("3".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已作废");
        }
        if (!vo.getOldPwd().equals(card.getGsrcPassword())) {
            throw new CustomResultException("原密码不符，请核对后重新输入！");
        }
        card.setGsrcPassword(vo.getNewPwd());
        card.setGsrcUpdateEmp(user.getUserId());
        card.setGsrcUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcUpdateBrId(user.getDepId());
        sdRechargeCardMapper.update(card, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));
        return ResultUtil.success(1);
    }


    /**
     * 储值卡挂失
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result reportLoss(UpdateRechargeCardVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = common();
        //判断储值卡信息
        SdRechargeCard card = sdRechargeCardMapper.selectOne(new QueryWrapper<SdRechargeCard>()
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (null == card) {
            throw new CustomResultException("未查询到储值卡信息");
        }

        if ("2".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已挂失");
        }

        if ("3".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已作废");
        }
        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        map.put("gsrccBrId", user.getDepId());
        SdRechargeCardChange cardNo = sdRechargeCardChangeMapper.getUserCardNumber(map);
        //插入储值卡操作记录表
        SdRechargeCardChange cardChange = new SdRechargeCardChange();
        cardChange.setClient(user.getClient());
        cardChange.setGsrccBrId(user.getDepId());
        cardChange.setGsrccVoucherId(cardNo.getGsrccVoucherId());
        cardChange.setGsrccDate(DateUtils.getCurrentDateStrYYMMDD());
        cardChange.setGsrccTime(DateUtils.getCurrentTimeStrHHMMSS());
        cardChange.setGsrccType("1");
        cardChange.setGsrccRemark(vo.getGsrccRemark());
        cardChange.setGsrccEmp(user.getUserId());
        cardChange.setGsrccAccountId(vo.getGsrcAccountId());
        cardChange.setGsrccOldCardId(vo.getGsrcId());
        sdRechargeCardChangeMapper.insert(cardChange);

        card.setGsrcStatus("2");
        card.setGsrcUpdateEmp(user.getUserId());
        card.setGsrcUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcUpdateBrId(user.getDepId());
        sdRechargeCardMapper.update(card, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));
        return ResultUtil.success(1);
    }

    /**
     * 储值卡解挂
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result hangingSolutions(UpdateRechargeCardVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = common();
        //判断储值卡信息
        SdRechargeCard card = sdRechargeCardMapper.selectOne(new QueryWrapper<SdRechargeCard>()
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (null == card) {
            throw new CustomResultException("未查询到储值卡信息");
        }

        if ("3".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已作废");
        }

        if ("1".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已解挂");
        }
        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        map.put("gsrccBrId", user.getDepId());
        SdRechargeCardChange cardNo = sdRechargeCardChangeMapper.getUserCardNumber(map);
        //插入储值卡操作记录表
        SdRechargeCardChange cardChange = new SdRechargeCardChange();
        cardChange.setClient(user.getClient());
        cardChange.setGsrccBrId(user.getDepId());
        cardChange.setGsrccVoucherId(cardNo.getGsrccVoucherId());
        cardChange.setGsrccDate(DateUtils.getCurrentDateStrYYMMDD());
        cardChange.setGsrccTime(DateUtils.getCurrentTimeStrHHMMSS());
        cardChange.setGsrccType("2");
        cardChange.setGsrccRemark(vo.getGsrccRemark());
        cardChange.setGsrccEmp(user.getUserId());
        cardChange.setGsrccAccountId(vo.getGsrcAccountId());
        cardChange.setGsrccOldCardId(vo.getGsrcId());
        sdRechargeCardChangeMapper.insert(cardChange);

        card.setGsrcStatus("1");
        card.setGsrcUpdateEmp(user.getUserId());
        card.setGsrcUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcUpdateBrId(user.getDepId());
        sdRechargeCardMapper.update(card, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));
        return ResultUtil.success(1);
    }


    /**
     * 储值卡换卡
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result changeCard(UpdateRechargeCardVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        StoreData storeData = common();

        //验证当前门店是否存在储值卡设置表中
        SdRechargeCardSet cardSet = sdRechargeCardSetMapper.selectOne(new QueryWrapper<SdRechargeCardSet>()
                .eq("CLIENT", user.getClient())
                .eq("GSRCS_BR_ID", user.getDepId()));
        if (null == cardSet) {
            throw new CustomResultException("请先维护储值卡设置信息");
        }
        // 前缀
        String prefix = cardSet.getGsrcsCardPrefix();
        if (StringUtils.isBlank(prefix)) {
            prefix = "CZK";
        }

        //判断储值卡信息
        SdRechargeCard card = sdRechargeCardMapper.selectOne(new QueryWrapper<SdRechargeCard>()
//                .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (null == card) {
            throw new CustomResultException("未查询到储值卡信息");
        }

        if ("3".equals(card.getGsrcStatus())) {
            throw new CustomResultException("当前储值卡已作废");
        }

        String no = vo.getGsrcIdNew();
        //新卡号不为空的时候，验证是否唯一
        if (StringUtils.isNotBlank(vo.getGsrcIdNew())) {
            //验证储存卡是否唯一
            Integer a = sdRechargeCardMapper.selectCount(new QueryWrapper<SdRechargeCard>()
                    .eq("CLIENT", user.getClient())
                    .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
//                    .eq("GSRC_BR_ID", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId())
                    .eq("GSRC_ID", vo.getGsrcIdNew()));
            if (a > 0) {
                throw new CustomResultException("使用会员卡号与已有储值卡号重复，请使用自动生成卡号");
            }
        } else {
            Map<Object, Object> map = new HashMap<>();
            map.put("client", user.getClient());
            map.put("storeCode", "2".equals(storeData.getStoAttribute()) ? StringUtils.isBlank(storeData.getStoChainHead()) ? user.getDepId() : storeData.getStoChainHead() : user.getDepId());
            map.put("prefix", prefix);
            SdRechargeCard sdRechargeCard = sdRechargeCardMapper.getUserCardNumber(map);
            no = sdRechargeCard.getGsrcId();
        }

        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        map.put("gsrccBrId", user.getDepId());
        SdRechargeCardChange cardNo = sdRechargeCardChangeMapper.getUserCardNumber(map);
        //插入储值卡操作记录表
        SdRechargeCardChange cardChange = new SdRechargeCardChange();
        cardChange.setClient(user.getClient());
        cardChange.setGsrccVoucherId(cardNo.getGsrccVoucherId());
        cardChange.setGsrccDate(DateUtils.getCurrentDateStrYYMMDD());
        cardChange.setGsrccTime(DateUtils.getCurrentTimeStrHHMMSS());
        cardChange.setGsrccType("3");
        cardChange.setGsrccBrId(user.getDepId());
        cardChange.setGsrccRemark(vo.getGsrccRemark());
        cardChange.setGsrccEmp(user.getUserId());
        cardChange.setGsrccAccountId(vo.getGsrcAccountId());
        cardChange.setGsrccOldCardId(vo.getGsrcId());
        cardChange.setGsrccNewCardId(no);
        sdRechargeCardChangeMapper.insert(cardChange);

        //作废之前的卡
        card.setGsrcStatus("3");
        card.setGsrcUpdateEmp(user.getUserId());
        card.setGsrcUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
        card.setGsrcUpdateBrId(user.getDepId());
        sdRechargeCardMapper.update(card, new QueryWrapper<SdRechargeCard>()
                .eq("CLIENT", user.getClient())
                .eq("GSRC_ACCOUNT_ID", vo.getGsrcAccountId())
                .eq("GSRC_ID", vo.getGsrcId()));

        if (StringUtils.isNotBlank(vo.getGsrcIdNew())) {
            card.setGsrcMemberCardId(vo.getGsrcIdNew());
        }

        //新增一个新的卡
        card.setGsrcId(no);
        card.setGsrcStatus("1");
        card.setGsrcDate(DateUtils.getCurrentDateStrYYMMDD());
        sdRechargeCardMapper.insert(card);

        return ResultUtil.success(1);
    }

    @Override
    public StoreData common() {
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        StoreData storeData = storeDataMapper.selectOne(new QueryWrapper<StoreData>()
                .eq("CLIENT", user.getClient())
                .eq("STO_CODE", user.getDepId()));
        if (null == storeData) {
            throw new CustomResultException("请选择门店");
        }
        return storeData;
    }

}
