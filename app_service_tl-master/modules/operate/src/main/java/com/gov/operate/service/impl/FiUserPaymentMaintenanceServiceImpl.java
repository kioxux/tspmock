package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.UserPaymentMaintenance;
import com.gov.operate.entity.FiUserPaymentMaintenance;
import com.gov.operate.mapper.FiUserPaymentMaintenanceMapper;
import com.gov.operate.service.IFiUserPaymentMaintenanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-12-01
 */
@Service
public class FiUserPaymentMaintenanceServiceImpl extends ServiceImpl<FiUserPaymentMaintenanceMapper, FiUserPaymentMaintenance> implements IFiUserPaymentMaintenanceService {

    @Resource
    private FiUserPaymentMaintenanceMapper fiUserPaymentMaintenanceMapper;

    /**
     * 获取付款维护数据
     *
     * @param client
     * @param ficoPayingstoreCode
     * @param ficoPayingstoreName
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getPaymentMaintenance(String client, String ficoPayingstoreCode, String ficoPayingstoreName, Integer pageNum, Integer pageSize) {
        // 数据初始化
        fiUserPaymentMaintenanceMapper.initPaymentMaintenance();
        // 分页
        Page<UserPaymentMaintenance> page = new Page<UserPaymentMaintenance>(pageNum, pageSize);
        IPage<UserPaymentMaintenance> iPage = fiUserPaymentMaintenanceMapper.selectPaymentMaintenance(page, client, ficoPayingstoreCode, ficoPayingstoreName);
        return ResultUtil.success(iPage);
    }

    /**
     * 保存付款维护数据
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void savePaymentMaintenance(List<UserPaymentMaintenance> list) {
        // 未提交数据
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        // 遍历保存
        for (UserPaymentMaintenance userPaymentMaintenance : list) {
            // 未维护数据
            if (userPaymentMaintenance.getFicoCurrentChargingStandard() == null || userPaymentMaintenance.getFicoCurrentChargingStandard().compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            // 生效日期为空
            if (StringUtils.isBlank(userPaymentMaintenance.getFicoEffectiveDate())) {
                throw new CustomResultException("生效日期不能为空");
            }
            // 上期截止日不为空
            if (StringUtils.isNotBlank(userPaymentMaintenance.getFicoLastDeadline())) {
                // 生效日期小于等于上期截止日期
                if (userPaymentMaintenance.getFicoEffectiveDate().compareTo(userPaymentMaintenance.getFicoLastDeadline()) <= 0) {
                    throw new CustomResultException("生效日期必须大于上期截止日期");
                }
            }
            // 数据更新
            QueryWrapper<FiUserPaymentMaintenance> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("CLIENT", userPaymentMaintenance.getClient());
            queryWrapper.eq("FICO_PAYINGSTORE_CODE", userPaymentMaintenance.getFicoPayingstoreCode());
            FiUserPaymentMaintenance fiUserPaymentMaintenance = fiUserPaymentMaintenanceMapper.selectOne(queryWrapper);
            if (fiUserPaymentMaintenance == null) {
                fiUserPaymentMaintenance = new FiUserPaymentMaintenance();
                // 加盟商
                fiUserPaymentMaintenance.setClient(userPaymentMaintenance.getClient());
                // 付款主体编码
                fiUserPaymentMaintenance.setFicoPayingstoreCode(userPaymentMaintenance.getFicoPayingstoreCode());
                // 付款主体名称
                fiUserPaymentMaintenance.setFicoPayingstoreName(userPaymentMaintenance.getFicoPayingstoreName());
                // 生效日期
                fiUserPaymentMaintenance.setFicoEffectiveDate(userPaymentMaintenance.getFicoEffectiveDate());
                // 收费标准
                fiUserPaymentMaintenance.setFicoCurrentChargingStandard(userPaymentMaintenance.getFicoCurrentChargingStandard());
                fiUserPaymentMaintenanceMapper.insert(fiUserPaymentMaintenance);
            } else {
                // 生效日期
                fiUserPaymentMaintenance.setFicoEffectiveDate(userPaymentMaintenance.getFicoEffectiveDate());
                // 收费标准
                fiUserPaymentMaintenance.setFicoCurrentChargingStandard(userPaymentMaintenance.getFicoCurrentChargingStandard());
                fiUserPaymentMaintenanceMapper.update(fiUserPaymentMaintenance, queryWrapper);
            }
        }
    }
}
