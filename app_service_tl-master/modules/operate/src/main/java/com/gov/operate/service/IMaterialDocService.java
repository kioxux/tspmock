package com.gov.operate.service;

import com.gov.operate.entity.MaterialDoc;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
public interface IMaterialDocService extends SuperService<MaterialDoc> {

}
