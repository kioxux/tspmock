package com.gov.operate.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 电子券主题列表
 * @author: yzf
 * @create: 2022-01-14 17:01
 */
@Data
public class ElectronThemeDTO {

    private String client;

    /**
     * 主题单号
     */
    private String gsetsId;

    /**
     * 主题属性
     */
    private String gsetsFlag;

    /**
     * 主题描述
     */
    private String gsetsName;

    /**
     * 送券起始日期
     */
    private String couponStartDate;

    /**
     * 送券结束日期
     */
    private String couponEndDate;

    /**
     * 用券起始日期
     */
    private String voucherStartDate;

    /**
     * 用券结束日期
     */
    private String voucherEndDate;

    /**
     * 创建人
     */
    private String createByName;

    /**
     * 创建日期
     */
    private String gsetsCreateDate;

    /**
     * 状态
     */
    private String gsebsStatus;

    private Integer pageSize;

    private Integer pageNum;
    /**
     * 送券数量
     */
    private BigDecimal gseasQty;
}
