package com.gov.operate.dto.charge;

import com.gov.operate.entity.UserChargeMaintenanceFree;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class ClientPriceItemVO {
    private Long id;

    private Long gscClientId;

    private String gscCoName;

    private LocalDate gscSignDate;

    private LocalDate gscExcptStartDate;

    private LocalDate gscExcptEndDate;

    private Integer gscLimit;

    private Integer gscStoCount;

    private Integer gscPayFreq;

    private Integer gscDiscount;

    private String gscContractCode;

    private String spRemark;

    private BigDecimal gscPayable;

    private BigDecimal gscActualPay;

    private BigDecimal gscDscntAmount;

    private BigDecimal gscAvgAmount;

    private BigDecimal gscGpPrice;

    private BigDecimal gscFreeMonths;

    private List<ClientPricePackVO> signPackList;

    private List<UserChargeMaintenanceFree> signFreeList;
}
