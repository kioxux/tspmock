package com.gov.operate.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SaleTaskPlan {
    private BigDecimal dailySaleAmt;
    private BigDecimal dailySaleGross;
    private BigDecimal dailyMcardQty;
    private BigDecimal saleAmt;
    private BigDecimal saleGross;
    private BigDecimal mcardQty;
}
