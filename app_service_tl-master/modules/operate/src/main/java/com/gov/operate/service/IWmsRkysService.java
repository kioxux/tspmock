package com.gov.operate.service;

import com.gov.operate.entity.WmsRkys;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface IWmsRkysService extends SuperService<WmsRkys> {

}
