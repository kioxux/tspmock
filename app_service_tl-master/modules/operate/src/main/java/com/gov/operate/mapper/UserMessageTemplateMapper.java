package com.gov.operate.mapper;

import com.gov.operate.entity.UserMessageTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
public interface UserMessageTemplateMapper extends BaseMapper<UserMessageTemplate> {

}
