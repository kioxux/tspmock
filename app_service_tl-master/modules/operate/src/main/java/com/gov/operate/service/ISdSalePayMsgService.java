package com.gov.operate.service;

import com.gov.operate.entity.SdSalePayMsg;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface ISdSalePayMsgService extends SuperService<SdSalePayMsg> {

}
