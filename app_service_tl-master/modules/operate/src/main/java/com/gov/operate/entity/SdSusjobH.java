package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_SUSJOB_H")
@ApiModel(value="SdSusjobH对象", description="")
public class SdSusjobH extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "任务ID")
    @TableField("SUS_JOBID")
    private String susJobid;

    @ApiModelProperty(value = "任务名称")
    @TableField("SUS_JOBNAME")
    private String susJobname;

    @ApiModelProperty(value = "任务门店")
    @TableField("SUS_JOBBR")
    private String susJobbr;

    @ApiModelProperty(value = "创建时间")
    @TableField("SUS_TIME")
    private String susTime;

    @ApiModelProperty(value = "计划开始时间")
    @TableField("SUS_BEGIN")
    private String susBegin;

    @ApiModelProperty(value = "计划结束时间")
    @TableField("SUS_END")
    private String susEnd;

    @ApiModelProperty(value = "会员数量")
    @TableField("SUS_JOBPERSON")
    private BigDecimal susJobperson;

    @ApiModelProperty(value = "进度")
    @TableField("SUS_SCHE")
    private String susSche;

    @ApiModelProperty(value = "状态")
    @TableField("SUS_STATUS")
    private String susStatus;

    @ApiModelProperty(value = "维系说明")
    @TableField("SUS_EXPLAIN")
    private String susExplain;

    @ApiModelProperty(value = "任务门店类型")
    @TableField("SUS_TYPE")
    private String susType;

    @ApiModelProperty(value = "任务员工")
    @TableField("SUS_EMP")
    private String susEmp;

    @ApiModelProperty(value = "维系话术")
    @TableField("SUS_CONTENT")
    private String susContent;

    @ApiModelProperty(value = "推送日期")
    @TableField("SUS_PUSH_DATE")
    private String susPushDate;

    @ApiModelProperty(value = "推送时间")
    @TableField("SUS_PUSH_TIME")
    private String susPushTime;

    @ApiModelProperty(value = "推送状态")
    @TableField("SUS_PUSH_STATUS")
    private String susPushStatus;


    @ApiModelProperty(value = "0.未查看1.进行中")
    @TableField("SUS_TASK_TYPE")
    private String susTaskType;




}
