package com.gov.operate.service.impl;


import com.gov.operate.entity.SdExamineH;
import com.gov.operate.mapper.SdExamineHMapper;
import com.gov.operate.service.ISdExamineHService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
@Service
public class SdExamineHServiceImpl extends ServiceImpl<SdExamineHMapper, SdExamineH> implements ISdExamineHService {

}
