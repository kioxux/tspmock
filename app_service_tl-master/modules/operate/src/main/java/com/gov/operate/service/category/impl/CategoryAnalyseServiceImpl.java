package com.gov.operate.service.category.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.operate.dto.category.CategoryInData;
import com.gov.operate.dto.category.CategoryOutData;
import com.gov.operate.entity.category.CategoryAnalyseData;
import com.gov.operate.entity.category.ProChangeRecord;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.category.CategoryAnalyseMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.category.ICategoryAnalyseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CategoryAnalyseServiceImpl extends ServiceImpl<CategoryAnalyseMapper, CategoryAnalyseData> implements ICategoryAnalyseService {

    @Resource
    private CategoryAnalyseMapper categoryAnalyseMapper;
    @Resource
    private CommonService commonService;

    @Override
    public Map<String, Object> selectAplProductList(CategoryInData inData) {
        TokenUser user = commonService.getLoginInfo();
        inData.setClient(user.getClient());
//        if (StringUtils.isNotEmpty(user.getDepId())) {
//            inData.setStoCode(user.getDepId());
//        }else{
//            inData.setStoCode("1000000001");
//        }
        // 当前人有权限门店集合
        List<String> stoCodeList = inData.getStoCodeList();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            stoCodeList = commonService.getStoCodeList();
        }
        inData.setStoCodeList(stoCodeList);

        if (ObjectUtils.isEmpty(inData.getAplConDate())) {
            String aplDate = categoryAnalyseMapper.selectMaxAplDate(inData);
            inData.setAplConDate(aplDate);
        }
        Map<String, Object> result = new HashMap<>();
//        if (StringUtils.isNotEmpty(aplCode)) {
        List<CategoryOutData> outData = categoryAnalyseMapper.selectAplProduct(inData);
        List<CategoryOutData> importData = categoryAnalyseMapper.selectImportProduct(inData);
        List<CategoryOutData> results = new ArrayList<>();
        if (outData.size() > 0) {
            results = outData.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(CategoryOutData::getAplText))), ArrayList::new)
            );
        }
        int typeCount = 0;
        if (importData.size() > 0) {
            typeCount = results.size() + 1;
        } else {
            typeCount = results.size();
        }
        result.put("typeCount", typeCount);
//            result.put("aplCode", results.get(0).getAplCode());
        List<Map<String, Object>> itemList = new ArrayList<>();
        List<String> detailList = new ArrayList<>();
        int count = 0;
        for (CategoryOutData item : results) {
            count++;
            BigDecimal profit = new BigDecimal("0.00");
            Integer profitCount = 0;
            BigDecimal outPrice = new BigDecimal("0.00");
            Integer outCount = 0;
            BigDecimal salePrice = new BigDecimal("0.00");
            Integer saleCount = 0;
            for (CategoryOutData detail : outData) {
                //调价公式
                if (detail.getAplText().equals("1")) {
                    profit = profit.add((new BigDecimal(detail.getAplConPrice()).subtract(
                            new BigDecimal(detail.getProMovPrice()))).multiply(new BigDecimal(detail.getProConQty())));
                    profitCount = profitCount + Integer.valueOf(detail.getProConQty());
                }
                //淘汰公式
                if (detail.getAplText().equals("2")) {
                    outPrice = outPrice.add(new BigDecimal(detail.getProMovPrice()).multiply(new BigDecimal(detail.getProConQty())));
                    outCount = outCount + Integer.valueOf(detail.getProConQty());
                }
                //促销公式
                if (detail.getAplText().equals("4")) {
                    salePrice = salePrice.add(new BigDecimal(detail.getProMovPrice()).multiply(new BigDecimal(detail.getProConQty())));
                    saleCount = saleCount + Integer.valueOf(detail.getProConQty());
                }
            }
            DecimalFormat decimalFormat = new DecimalFormat("0.00#");
            Map<String, Object> map = new HashMap<>();
            if (item.getAplText().equals("1")) {
                map.put("itemStr", "ProPriceAdjustmentDetails");
                map.put("itemName", "调价明细");
                map.put("itemCode", item.getAplText());
                String adjustprice = count + "、调价商品" + profitCount + "个，预计可每月每月增加净利润" + decimalFormat.format(profit) + "元";
                detailList.add(adjustprice);
            }
            if (item.getAplText().equals("2")) {
                map.put("itemStr", "ProEliminated");
                map.put("itemName", "淘汰明细");
                map.put("itemCode", item.getAplText());
                String outproduct = count + "、淘汰商品" + outCount + "个，预计可降低库存成本" + decimalFormat.format(outPrice) + "元";
                detailList.add(outproduct);
            }
            if (item.getAplText().equals("4")) {
                map.put("itemStr", "PromotionDetail");
                map.put("itemName", "促销明细");
                map.put("itemCode", item.getAplText());
                String saleproduct = count + "、促销商品" + saleCount + "个，预计可降低库存成本" + decimalFormat.format(salePrice) + "元";
                detailList.add(saleproduct);
            }
            itemList.add(map);
        }
        if (importData.size() > 0) {
            BigDecimal importPrice = new BigDecimal("0.00");
            BigDecimal importCount = new BigDecimal("0");
            for (CategoryOutData detail : importData) {
                //引进公式
                if (detail.getAplText().equals("3")) {
                    importPrice = importPrice.add(new BigDecimal(detail.getProConPrice()).multiply(new BigDecimal(detail.getProConQty())));
                    importCount = importCount.add(new BigDecimal("1"));
                }
            }
            DecimalFormat decimalFormat = new DecimalFormat("0.00#");
            Map<String, Object> map = new HashMap<>();
            map.put("itemStr", "ProIntroductionDetails");
            map.put("itemName", "引进明细");
            map.put("itemCode", importData.get(0).getAplText());
            String importproduct = (count + 1) + "、建议引进" + importCount + "个品种，预计每月增加销售额" + importPrice + "元";
            detailList.add(importproduct);
            itemList.add(map);
        }
        result.put("itemList", itemList);
        result.put("detailList", detailList);
//        }
        return result;
    }

    @Override
    public List<CategoryOutData> selectAplProduct(CategoryInData inData) {
        TokenUser user = commonService.getLoginInfo();
        inData.setClient(user.getClient());
//        if (StringUtils.isNotEmpty(user.getDepId())) {
//            inData.setStoCode(user.getDepId());
//        }else{
//            inData.setStoCode("1000000001");
//        }
        List<CategoryOutData> outData = new ArrayList<>();
        if (ObjectUtils.isEmpty(inData.getAplConDate())) {
            String aplDate = categoryAnalyseMapper.selectMaxAplDate(inData);
            inData.setAplConDate(aplDate);
        }
        if (inData.getAplText().equals("3")) {
            outData = categoryAnalyseMapper.selectImportProduct(inData);
        } else {
            outData = categoryAnalyseMapper.selectAplProduct(inData);
        }
//        for (CategoryOutData data : outData) {
//            if (data.getAplText().equals("adjustprice")){
//                data.setAplConReason("尾数定价原则");
//            }
//            if (data.getAplText().equals("outproduct")){
//                data.setAplConReason("三个月不动销");
//            }
//            if (data.getAplText().equals("importproduct")){
//                data.setAplConReason("引进新商品类");
//            }
//        }
        return outData;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAdjustPrice(List<CategoryInData> list) {
        TokenUser user = commonService.getLoginInfo();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(date);
        Calendar now = Calendar.getInstance();
        String hourStr = "0" + now.get(Calendar.HOUR_OF_DAY);
        String minStr = "0" + now.get(Calendar.MINUTE);
        String secStr = "0" + now.get(Calendar.SECOND);
        String timeStr = hourStr.substring(hourStr.length() - 2, hourStr.length())
                + minStr.substring(minStr.length() - 2, minStr.length())
                + secStr.substring(secStr.length() - 2, secStr.length());
        List<ProChangeRecord> proList = new ArrayList<>();
        for (CategoryInData inData : list) {
            inData.setClient(user.getClient());
//            if (StringUtils.isNotEmpty(user.getDepId())) {
//                inData.setStoCode(user.getDepId());
//            }else{
//                inData.setStoCode("1000000001");
//            }
            if (ObjectUtils.isEmpty(inData.getAplUpdateDate())) {
                inData.setAplUpdateDate(dateString);
//            inData.setAplStatus("1");

                inData.setAplUpdateTime(timeStr);
            }
            inData.setAplConEmp(user.getUserId());
            Map<String, String> proInfo = categoryAnalyseMapper.selectProInfo(inData.getClient(), inData.getStoCode(), inData.getProCode());
            ProChangeRecord record = new ProChangeRecord();
            record.setClient(user.getClient());
            record.setProCode(inData.getProCode());
            record.setStoCode(inData.getStoCode());
            record.setUserId(user.getUserId());
            record.setProChangeFrom(proInfo.get("proPrice"));
            record.setProFactoryName(proInfo.get("proFactoryName"));
            record.setProChangeTo(inData.getAplPrice());
            record.setChangeField("GSPP_PRICE_NORMAL");
            record.setProName(proInfo.get("proName"));
            record.setProRegisterNo(proInfo.get("proRegisterNo"));
            record.setProSpecs(proInfo.get("proSpecs"));
            proList.add(record);
        }
        if (proList.size() > 0) {
            changeProPrice(proList);
        }
        categoryAnalyseMapper.updateAdjustPrice(list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void outProduct(List<CategoryInData> list) {
        TokenUser user = commonService.getLoginInfo();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(date);
        Calendar now = Calendar.getInstance();
        String hourStr = "0" + now.get(Calendar.HOUR_OF_DAY);
        String minStr = "0" + now.get(Calendar.MINUTE);
        String secStr = "0" + now.get(Calendar.SECOND);
        String time = hourStr.substring(hourStr.length() - 2, hourStr.length())
                + minStr.substring(minStr.length() - 2, minStr.length())
                + secStr.substring(secStr.length() - 2, secStr.length());
        for (CategoryInData inData : list) {
            //淘汰录入表插入
            CategoryAnalyseData analyseData = new CategoryAnalyseData();
            analyseData.setClient(user.getClient());
            analyseData.setProSite(inData.getStoCode());
//            if (StringUtils.isNotEmpty(user.getDepId())) {
//                analyseData.setProSite(user.getDepId());
//            }else{
//                analyseData.setProSite("1000000001");
//            }
            analyseData.setProOutCreateDate(dateString);
            analyseData.setProOutDate(dateString);
            analyseData.setProOutCreateTime(time);
            analyseData.setProOutCreateUser(user.getUserId());
            analyseData.setProSelfCode(inData.getProCode());
            analyseData.setProOutReason(inData.getAplProReason());
            categoryAnalyseMapper.insert(analyseData);
            //商品调价状态表修改状态
            inData.setClient(user.getClient());
            inData.setAplInText(inData.getAplText());
//            if (StringUtils.isNotEmpty(user.getDepId())) {
//                inData.setStoCode(user.getDepId());
//            }else{
//                inData.setStoCode("1000000001");
//            }
            inData.setAplConEmp(user.getUserId());
            inData.setAplUpdateDate(dateString);
            inData.setAplUpdateTime(time);
            ProChangeRecord record = new ProChangeRecord();
            record.setClient(user.getClient());
            record.setProCode(inData.getProCode());
            record.setStoCode(inData.getStoCode());
            record.setUserId(user.getUserId());
            this.changeProPosition(record);
//            inData.setAplStatus("1");
        }
        categoryAnalyseMapper.updateAdjustPrice(list);
    }

    @Override
    public void updateProduct(List<CategoryInData> list) {
        TokenUser user = commonService.getLoginInfo();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(date);
        Calendar now = Calendar.getInstance();
        String hourStr = "0" + now.get(Calendar.HOUR_OF_DAY);
        String minStr = "0" + now.get(Calendar.MINUTE);
        String secStr = "0" + now.get(Calendar.SECOND);
        String time = hourStr.substring(hourStr.length() - 2, hourStr.length())
                + minStr.substring(minStr.length() - 2, minStr.length())
                + secStr.substring(secStr.length() - 2, secStr.length());

        for (CategoryInData inData : list) {
            //商品调价状态表修改状态
            inData.setClient(user.getClient());
            if (StringUtils.isNotEmpty(user.getDepId())) {
                inData.setStoCode(user.getDepId());
            } else {
                inData.setStoCode("1000000001");
            }
            inData.setAplUpdateDate(dateString);
            inData.setAplUpdateTime(time);
            inData.setAplConEmp(user.getUserId());
//            inData.setAplStatus("1");
        }
        categoryAnalyseMapper.updateAdjustPrice(list);
    }

    @Override
    public List<Map<String, String>> selectDateList(Map<String, String> inData) {
        TokenUser user = commonService.getLoginInfo();
        inData.put("client", user.getClient());
        if (!inData.containsKey("stoCode")) {
            inData.put("stoCode", "");
        }
        return categoryAnalyseMapper.selectDateList(inData);
    }

    @Override
    public List<Map<String, String>> selectTableData(CategoryInData inData) {
        List<Map<String, String>> list = new ArrayList<>();
        TokenUser user = commonService.getLoginInfo();
        inData.setClient(user.getClient());
        if (ObjectUtils.isEmpty(inData.getAplConDate())) {
            String aplDate = categoryAnalyseMapper.selectMaxAplDate(inData);
            inData.setAplConDate(aplDate);
        }
        Integer executedQtyAll = 0;//已执行
        Integer abandonedQtyAll = 0;//已放弃
        Integer confirmQtyAll = 0; // 待确认数量
        Map<String, String> totalMap = new HashMap<>();
        totalMap.put("name", "合计");

        // 当前人有权限门店集合
        List<String> stoCodeList = inData.getStoCodeList();
        if (CollectionUtils.isEmpty(stoCodeList)) {
            stoCodeList = commonService.getStoCodeList();
        }
        inData.setStoCodeList(stoCodeList);

        //结论 1调价；2淘汰；3引进；4促销
        List<CategoryOutData> outData = categoryAnalyseMapper.selectAplProduct(inData);
        if (outData.size() > 0) {
            List<CategoryOutData> results = outData.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(CategoryOutData::getAplText))), ArrayList::new)
            );
            for (CategoryOutData data1 : results) {
                Map<String, String> result = new HashMap<>();
                if (data1.getAplText().equals("1")) {
                    result.put("name", "调价");
                }
                if (data1.getAplText().equals("2")) {
                    result.put("name", "淘汰");
                }
                if (data1.getAplText().equals("4")) {
                    result.put("name", "促销");
                }
                Integer totalQty = 0;
                Integer executedQty = 0;//已执行
                Integer abandonedQty = 0;//已放弃
                Integer confirmQty = 0; // 待确认数量
                for (CategoryOutData data2 : outData) {
                    if (data2.getAplText().equals(data1.getAplText())) {
                        totalQty = totalQty + 1;
                        if (data2.getAplConStu().equals("1")) {
                            confirmQty = confirmQty + 1;
                            confirmQtyAll = confirmQtyAll + 1;
                        }
                        if (data2.getAplConStu().equals("2")) {
                            executedQty = executedQty + 1;
                            executedQtyAll = executedQtyAll + 1;
                        }
                        if (data2.getAplConStu().equals("3") || data2.getAplConStu().equals("4")) {
                            abandonedQty = abandonedQty + 1;
                            abandonedQtyAll = abandonedQtyAll + 1;
                        }
                    }
                }
                result.put("totalQty", totalQty.toString());
                result.put("executedQty", executedQty.toString());
                result.put("abandonedQty", abandonedQty.toString());
                result.put("confirmQty", confirmQty.toString());
                list.add(result);
            }
        }
        List<CategoryOutData> importData = categoryAnalyseMapper.selectImportProduct(inData);
        if (importData.size() > 0) {
            Map<String, String> item = new HashMap<>();
            item.put("name", "引进");
            Integer totalQty = importData.size();
            Integer executedQty = 0;//已执行
            Integer abandonedQty = 0;//已放弃
            Integer confirmQty = 0; // 待确认数量
            for (CategoryOutData data : importData) {
                if (data.getAplConStu().equals("1")) {
                    confirmQty = confirmQty + 1;
                    confirmQtyAll = confirmQtyAll + 1;
                }
                if (data.getAplConStu().equals("2")) {
                    executedQty = executedQty + 1;
                    executedQtyAll = executedQtyAll + 1;
                }
                if (data.getAplConStu().equals("3") || data.getAplConStu().equals("4")) {
                    abandonedQty = abandonedQty + 1;
                    abandonedQtyAll = abandonedQtyAll + 1;
                }
            }
            item.put("totalQty", totalQty.toString());
            item.put("executedQty", executedQty.toString());
            item.put("abandonedQty", abandonedQty.toString());
            item.put("confirmQty", confirmQty.toString());
            list.add(item);
        }
        Integer totalQtyAll = outData.size() + importData.size();
        totalMap.put("totalQty", totalQtyAll.toString());
        totalMap.put("executedQty", executedQtyAll.toString());
        totalMap.put("abandonedQty", abandonedQtyAll.toString());
        totalMap.put("confirmQty", confirmQtyAll.toString());
        list.add(totalMap);
        return list;
    }

    /**
     * xxjob 品类分析定时执行
     */
    @Override
    public void timerUpdateAdjustPrice(String inData) {
        List<CategoryInData> list = new ArrayList<>();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(date);
        Calendar now = Calendar.getInstance();
        String hourStr = "0" + now.get(Calendar.HOUR_OF_DAY);
        String minStr = "0" + now.get(Calendar.MINUTE);
        String secStr = "0" + now.get(Calendar.SECOND);
        String timeStr = hourStr.substring(hourStr.length() - 2, hourStr.length())
                + minStr.substring(minStr.length() - 2, minStr.length())
                + secStr.substring(secStr.length() - 2, secStr.length());
        if (ObjectUtils.isNotEmpty(inData)) {
            String[] storeArr = inData.split(";");
            for (int i = 0; i < storeArr.length; i++) {
                String[] itemArr = storeArr[i].split(":");
                String client = itemArr[0];
                //确保有门店编码
                if (itemArr.length > 1) {
                    String[] stoCode = itemArr[1].split(",");
                    for (int j = 0; j < stoCode.length; j++) {
                        CategoryInData data = new CategoryInData();
                        data.setClient(client);
                        data.setStoCode(stoCode[i]);
                        data.setAplInText("1");
                        data.setAplConDsDate(dateString);
                        data.setAplStatus("2");
                        data.setAplUpdateDate(dateString);
                        data.setAplUpdateTime(timeStr);
                        data.setAplInStatus("5");
                        list.add(data);
                    }
                } else {
                    CategoryInData data = new CategoryInData();
                    data.setClient(client);
//                    data.setStoCode(stoCode[i]);
                    data.setAplInText("1");
                    data.setAplConDsDate(dateString);
                    data.setAplStatus("2");
                    data.setAplUpdateDate(dateString);
                    data.setAplUpdateTime(timeStr);
                    data.setAplInStatus("5");
                    list.add(data);
                }
            }
        } else {
            CategoryInData data = new CategoryInData();
//            data.setClient(client);
//                    data.setStoCode(stoCode[i]);
            data.setAplInText("1");
            data.setAplConDsDate(dateString);
            data.setAplStatus("2");
            data.setAplUpdateDate(dateString);
            data.setAplUpdateTime(timeStr);
            data.setAplInStatus("5");
            list.add(data);
        }
        categoryAnalyseMapper.updateAdjustPrice(list);
        //查询已定时执行调价
        List<CategoryOutData> outDataList = categoryAnalyseMapper.selectTimerAplPro(dateString);
        if (outDataList.size() > 0) {
            List<ProChangeRecord> proList = new ArrayList<>();
            for (CategoryOutData outData : outDataList) {
                ProChangeRecord record = new ProChangeRecord();
                record.setClient(outData.getClient());
                record.setProCode(outData.getProCode());
                record.setStoCode(outData.getStoCode());
                record.setUserId(outData.getAplConEmp());
                record.setProChangeFrom(outData.getProPrice());
                record.setProFactoryName(outData.getProFactoryName());
                record.setProChangeTo(outData.getAplConPrice());
                record.setChangeField("GSPP_PRICE_NORMAL");
                record.setProName(outData.getProName());
                record.setProRegisterNo(outData.getProRegisterNo());
                record.setProSpecs(outData.getProSpecs());
                proList.add(record);
            }
            if (proList.size() > 0) {
                //修改商品主数据零售价并添加记录
                changeProPrice(proList);
            }
        }
    }

    /**
     * xxjob 品类分析自动放弃
     */
    @Override
    public void timerCancelOperate(String inData) {
        List<CategoryInData> list = new ArrayList<>();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        //过去七天
        c.setTime(new Date());
        c.add(Calendar.DATE, -7);
        Date d = c.getTime();
        String dateString = formatter.format(d);
        Calendar now = Calendar.getInstance();
        String nowDate = formatter.format(date);
        String hourStr = "0" + now.get(Calendar.HOUR_OF_DAY);
        String minStr = "0" + now.get(Calendar.MINUTE);
        String secStr = "0" + now.get(Calendar.SECOND);
        String timeStr = hourStr.substring(hourStr.length() - 2, hourStr.length())
                + minStr.substring(minStr.length() - 2, minStr.length())
                + secStr.substring(secStr.length() - 2, secStr.length());
        if (ObjectUtils.isNotEmpty(inData)) {
            String[] storeArr = inData.split(";");
            for (int i = 0; i < storeArr.length; i++) {
                String[] itemArr = storeArr[i].split(":");
                String client = itemArr[0];
                //确保有门店编码
                if (itemArr.length > 1) {
                    String[] stoCode = itemArr[1].split(",");
                    for (int j = 0; j < stoCode.length; j++) {
                        CategoryInData data = new CategoryInData();
                        data.setClient(client);
                        data.setStoCode(stoCode[i]);
//                        data.setAplInText("1");
//                        data.setAplConDsDate(dateString);
                        data.setAplConMixDate(dateString);
                        data.setAplStatus("4");
                        data.setAplUpdateDate(nowDate);
                        data.setAplUpdateTime(timeStr);
                        data.setAplInStatus("1");
                        list.add(data);
                    }
                } else {
                    CategoryInData data = new CategoryInData();
                    data.setClient(client);
//                    data.setStoCode(stoCode[i]);
//                        data.setAplInText("1");
//                        data.setAplConDsDate(dateString);
                    data.setAplStatus("4");
                    data.setAplUpdateDate(nowDate);
                    data.setAplUpdateTime(timeStr);
                    data.setAplConMixDate(dateString);
                    data.setAplInStatus("1");
                    list.add(data);
                }
            }
        } else {
            CategoryInData data = new CategoryInData();
//            data.setClient(client);
//                    data.setStoCode(stoCode[i]);
//                        data.setAplInText("1");
//                        data.setAplConDsDate(dateString);
            data.setAplStatus("4");
            data.setAplUpdateDate(nowDate);
            data.setAplUpdateTime(timeStr);
            data.setAplConMixDate(dateString);
            data.setAplInStatus("1");
            list.add(data);
        }
        categoryAnalyseMapper.updateAdjustPrice(list);
    }

    @Transactional
    public void changeProPosition(ProChangeRecord record) {
        Map<String, String> proInfo = categoryAnalyseMapper.selectProInfo(record.getClient(), record.getStoCode(), record.getProCode());
        record.setChangeField("PRO_POSITION");
        record.setProChangeFrom(proInfo.get("proPosition"));
        record.setProChangeTo("T");
        categoryAnalyseMapper.addChangeRecord(record);
        categoryAnalyseMapper.updateProPosition(record.getClient(), record.getStoCode(), record.getProCode());
    }

    @Transactional
    public void changeProPrice(List<ProChangeRecord> list) {
        List<Map<String, String>> updateList = new ArrayList<>();
        for (ProChangeRecord record : list) {
            Map<String, String> map = new HashMap<>();
            map.put("client", record.getClient());
            map.put("stoCode", record.getStoCode());
            map.put("proCode", record.getProCode());
            map.put("proPrice", record.getProChangeTo());
            updateList.add(map);
        }
        if (updateList.size() > 0) {
            categoryAnalyseMapper.updateProPrice(updateList);
        }
        categoryAnalyseMapper.batchChangeRecord(list);
    }
}
