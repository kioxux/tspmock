package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryProCps;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryProCpsService extends SuperService<SalaryProCps> {

    /**
     * 薪资方案单品提成
     */
    void saveProCps(EditProgramVO vo);

    /**
     * 单品提成列表
     */
    List<SalaryProCps> getProCpsList(Integer gspId);
}
