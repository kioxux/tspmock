package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_WMS_RKYS")
@ApiModel(value="WmsRkys对象", description="")
public class WmsRkys extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private Long id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "入库单号")
    @TableField("WM_RKDH")
    private String wmRkdh;

    @ApiModelProperty(value = "采购订单号")
    @TableField("WM_CGDDH")
    private String wmCgddh;

    @ApiModelProperty(value = "订单序号")
    @TableField("WM_DDXH")
    private String wmDdxh;

    @ApiModelProperty(value = "商品编码")
    @TableField("WM_SP_BM")
    private String wmSpBm;

    @ApiModelProperty(value = "批号")
    @TableField("WM_PH")
    private String wmPh;

    @ApiModelProperty(value = "生产日期")
    @TableField("WM_SCRQ")
    private String wmScrq;

    @ApiModelProperty(value = "有效期")
    @TableField("WM_YXQ")
    private String wmYxq;

    @ApiModelProperty(value = "订单价")
    @TableField("WM_DDJ")
    private BigDecimal wmDdj;

    @ApiModelProperty(value = "到货价")
    @TableField("WM_DHJ")
    private BigDecimal wmDhj;

    @ApiModelProperty(value = "生产厂家")
    @TableField("WM_SCCJ")
    private String wmSccj;

    @ApiModelProperty(value = "产地")
    @TableField("WM_CD")
    private String wmCd;

    @ApiModelProperty(value = "收货数量")
    @TableField("WM_SHSL")
    private BigDecimal wmShsl;

    @ApiModelProperty(value = "拒收数量")
    @TableField("WM_JSSL")
    private BigDecimal wmJssl;

    @ApiModelProperty(value = "拒收原因")
    @TableField("WM_JSYY")
    private String wmJsyy;

    @ApiModelProperty(value = "库存状态编号")
    @TableField("WM_KCZT_BH")
    private String wmKcztBh;

    @ApiModelProperty(value = "发票号")
    @TableField("WM_FPH")
    private String wmFph;

    @ApiModelProperty(value = "验收结论")
    @TableField("WM_YSJL")
    private String wmYsjl;

    @ApiModelProperty(value = "修改日期")
    @TableField("WM_XGRQ")
    private String wmXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("WM_XGSJ")
    private String wmXgsj;

    @ApiModelProperty(value = "修改人ID")
    @TableField("WM_XGR_ID")
    private String wmXgrId;

    @ApiModelProperty(value = "修改人")
    @TableField("WM_XGR")
    private String wmXgr;

    @ApiModelProperty(value = "验收确认日期")
    @TableField("WM_YSRQ")
    private String wmYsrq;

    @ApiModelProperty(value = "验收确认时间")
    @TableField("WM_YSSJ")
    private String wmYssj;

    @ApiModelProperty(value = "验收确认人ID")
    @TableField("WM_YSR_ID")
    private String wmYsrId;

    @ApiModelProperty(value = "验收确认人")
    @TableField("WM_YSR")
    private String wmYsr;

    @ApiModelProperty(value = "批次号")
    @TableField("BATCHNO")
    private String batchno;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "供应商编号")
    @TableField("WM_GYS_BH")
    private String wmGysBh;

    @ApiModelProperty(value = "备注")
    @TableField("REMARK")
    private String remark;

    @ApiModelProperty(value = "状态")
    @TableField("STATE")
    private Integer state;

    @ApiModelProperty(value = "已上架数量")
    @TableField("ACCEPTED_QUANTITY")
    private BigDecimal acceptedQuantity;

    @ApiModelProperty(value = "收货人")
    @TableField("CONSIGNEE")
    private String consignee;

    @ApiModelProperty(value = "收货日期")
    @TableField("RECEIVING_DATE")
    private String receivingDate;

    @ApiModelProperty(value = "收货时间")
    @TableField("RECEIVING_TIME")
    private String receivingTime;

    @ApiModelProperty(value = "收货区库存id")
    @TableField("KUCEN_ID")
    private Long kucenId;

    @ApiModelProperty(value = "采购人名称")
    @TableField("BUYER_NAME")
    private String buyerName;

    @ApiModelProperty(value = "采购日期")
    @TableField("PO_CREATE_DATE")
    private String poCreateDate;

    @ApiModelProperty(value = "收货人")
    @TableField("CONSIGNEE_ID")
    private String consigneeId;

    @ApiModelProperty(value = "推荐的货位号")
    @TableField("RECOMMENDED_CARGONO")
    private String recommendedCargono;

    @ApiModelProperty(value = "领取人")
    @TableField("WM_LQR")
    private String wmLqr;

    @ApiModelProperty(value = "领取人id")
    @TableField("WM_LQR_ID")
    private String wmLqrId;

    @ApiModelProperty(value = "领取时间")
    @TableField("WM_LQSJ")
    private String wmLqsj;

    @ApiModelProperty(value = "付款条件码")
    @TableField("PO_PAYMENT_ID")
    private String poPaymentId;

    @ApiModelProperty(value = "审批意见")
    @TableField("APPROVAL_COMMENTS")
    private String approvalComments;


}
