package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_RELATE")
@ApiModel(value="ProductRelate对象", description="")
public class ProductRelate extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "商品编码")
    @TableField("PRO_SELF_CODE")
    private String proSelfCode;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_SELF_CODE")
    private String supSelfCode;

    @ApiModelProperty(value = "关联商品编码")
    @TableField("PRO_RELATE_CODE")
    private String proRelateCode;

    @ApiModelProperty(value = "采购价格")
    @TableField("PRO_PRICE")
    private BigDecimal proPrice;

    @ApiModelProperty(value = "创建人")
    @TableField("PRO_CREATE_USER")
    private String proCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("PRO_CREATE_DATE")
    private String proCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("PRO_CREATE_TIME")
    private String proCreateTime;

    @ApiModelProperty(value = "变更人")
    @TableField("PRO_CHANGE_USER")
    private String proChangeUser;

    @ApiModelProperty(value = "变更日期")
    @TableField("PRO_CHANGE_DATE")
    private String proChangeDate;

    @ApiModelProperty(value = "变更时间")
    @TableField("PRO_CHANGE_TIME")
    private String proChangeTime;


}
