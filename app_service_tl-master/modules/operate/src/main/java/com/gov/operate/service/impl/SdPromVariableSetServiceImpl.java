package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromVariableSet;
import com.gov.operate.mapper.SdPromVariableSetMapper;
import com.gov.operate.service.ISdPromVariableSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
@Service
public class SdPromVariableSetServiceImpl extends ServiceImpl<SdPromVariableSetMapper, SdPromVariableSet> implements ISdPromVariableSetService {

}
