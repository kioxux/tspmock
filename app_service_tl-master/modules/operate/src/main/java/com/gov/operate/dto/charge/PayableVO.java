package com.gov.operate.dto.charge;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PayableVO {
    private List<PayableItemVO> itemList;
    //余额付款
    private BigDecimal remainAmount;
    //抵扣金额
    private BigDecimal deductAmount;
    //支付方式
    private Integer payType;
    //支付频率
    private Integer payFreq;
    //上次付款时间
    private String lastPayDate;
    //发票类型
    private String invoiceType;
    private String ficoInvoiceType;
    //实收
    private BigDecimal payAmount;
    /**
     * 银联银行
     */
    private String bankName;
    /**
     * 渠道号
     */
    private String chnlNo;
    //应收
    private BigDecimal receivableAmount;
}
