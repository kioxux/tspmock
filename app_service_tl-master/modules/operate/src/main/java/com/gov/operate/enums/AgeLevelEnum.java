package com.gov.operate.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:47 2021/8/13
 * @Description：年龄阶段枚举
 * @Modified By：guoyuxi.
 * @Version:
 */
@AllArgsConstructor
public enum AgeLevelEnum {

    AfterTen("1","年龄阶段10后","2010","2019"),
    AfterTwenty ("2","年龄阶段20后","2020","2029"),
    AfterThirty("3","年龄阶段30后","1930","1939"),
    AfterForty("4","年龄阶段40后","1940","1949"),
    AfterFifty("5","年龄阶段50后","1950","1959"),
    AfterSixty("6","年龄阶段60后","1960","1969"),
    AfterSeventy("7","年龄阶段70后","1970","1979"),
    AfterEighty("8","年龄阶段80后","1980","1989"),
    AfterNinety("9","年龄阶段90后","1990","1999"),
    AfterZero("0","年龄阶段00后","2000","2009");



    private String ageLevelCode;
    private String ageLevelDesc;
    private String ageLevelStartYear;
    private String ageLevelEndYear;

    public String getAgeLevelCode() {
        return ageLevelCode;
    }

    public void setAgeLevelCode(String ageLevelCode) {
        this.ageLevelCode = ageLevelCode;
    }

    public String getAgeLevelDesc() {
        return ageLevelDesc;
    }

    public void setAgeLevelDesc(String ageLevelDesc) {
        this.ageLevelDesc = ageLevelDesc;
    }

    public String getAgeLevelStartYear() {
        return ageLevelStartYear;
    }

    public void setAgeLevelStartYear(String ageLevelStartYear) {
        this.ageLevelStartYear = ageLevelStartYear;
    }

    public String getAgeLevelEndYear() {
        return ageLevelEndYear;
    }

    public void setAgeLevelEndYear(String ageLevelEndYear) {
        this.ageLevelEndYear = ageLevelEndYear;
    }
}
