package com.gov.operate.dto;

import lombok.Data;

/**
 * @description: 电子券业务基础列表
 * @author: yzf
 * @create: 2022-01-14 17:01
 */
@Data
public class ElectronBusinessDTO {

    private String client;

    /**
     * 主题单号
     */
    private String gsetsId;

    /**
     * 业务类型   0为送券，1为用券
     */
    private String gsebsType;

    /**
     * 业务单号
     */
    private String gsebsId;

    /**
     * 门店编码
     */
    private String gsebsBrId;

    /**
     * 起始日期
     */
    private String gsebsBeginDate;

    /**
     * 结束日期
     */
    private String gsebsEndDate;

    /**
     * 起始时间
     */
    private String gsebsBeginTime;

    /**
     * 结束时间
     */
    private String gsebsEndTime;

    /**
     * 是否审核   N为否，Y为是
     */
    private String gsebsStatus;

    /**
     * 是否停用   N为否，Y为是
     */
    private String gsebsFlag;

    /**
     * 促销商品是否参与   N为否，Y为是
     */
    private String gsebsProm;

    /**
     * 创建日期
     */
    private String gsebsCreateDate;

    /**
     * 创建时间
     */
    private String gsebsCreateTime;

    /**
     * 最大赠送次数
     */
    private String gsebsMaxQty;

    /**
     * 已赠送次数
     */
    private String gsebsQty;

    /**
     * 备注
     */
    private String gsebsRemark;

    /**
     * 是否有效   N为否，Y为是
     */
    private String gsebsValid;
}
