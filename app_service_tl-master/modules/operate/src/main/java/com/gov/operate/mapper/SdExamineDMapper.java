package com.gov.operate.mapper;

import com.gov.operate.dto.CleanDrawerDetailDTO;
import com.gov.operate.dto.GetHighlightDTO;
import com.gov.operate.entity.SdExamineD;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
public interface SdExamineDMapper extends BaseMapper<SdExamineD> {

    /**
     * 验收单高亮显示信息
     *
     * @param client
     * @param depId
     * @param gsehVoucherId
     * @return
     */
    List<GetHighlightDTO> getHighlightInfo(@Param("client") String client,
                                           @Param("depId") String depId,
                                           @Param("gsehVoucherId") String gsehVoucherId);
}
