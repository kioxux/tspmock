package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SdMemberClass;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface ISdMemberClassService extends SuperService<SdMemberClass> {

    public List<SdMemberClass> selectMemberClassList();
}
