package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/14 14:13
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetBatchNoValidListDTO {

    /**
     * 批号
     */
    private String gssbBatchNo;

    /**
     * 数量
     */
    private String gssbQty;

    /**
     * 效期
     */
    private String gssbVaildDate;
}
