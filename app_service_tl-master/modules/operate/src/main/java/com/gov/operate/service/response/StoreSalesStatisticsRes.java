package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class StoreSalesStatisticsRes {

    @ApiModelProperty(value = "门店名")
    private String stoName;

    @ApiModelProperty(value = "门店")
    private String store;

    @ApiModelProperty(value = "0：日 1：周 2：月")
    private Integer dateType;

    @ApiModelProperty(value = "门店数")
    private Integer storeNum;

    @ApiModelProperty(value = "指标汇总数据")
    List<Map<String,List<StoreSalesStatisticsItemIndex>>> items;
}
