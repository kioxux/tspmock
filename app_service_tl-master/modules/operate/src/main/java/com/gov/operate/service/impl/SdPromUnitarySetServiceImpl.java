package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromUnitarySet;
import com.gov.operate.mapper.SdPromUnitarySetMapper;
import com.gov.operate.service.ISdPromUnitarySetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-31
 */
@Service
public class SdPromUnitarySetServiceImpl extends ServiceImpl<SdPromUnitarySetMapper, SdPromUnitarySet> implements ISdPromUnitarySetService {

}
