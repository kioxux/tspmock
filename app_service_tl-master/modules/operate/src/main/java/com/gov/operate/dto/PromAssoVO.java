package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 组合类促销
 * @author: yzf
 * @create: 2022-01-24 13:38
 */
@Data
public class PromAssoVO {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gspacVoucherId;

    /**
     * 行号
     */
    private String gspacSerial;

    /**
     * 商品编码
     */
    private String gspacProId;

    /**
     * 数量
     */
    private String gspacQty;

    /**
     * 系列编码
     */
    private String gspacSeriesId;

    /**
     * 是否会员
     */
    private String gspacMemFlag;

    /**
     * 是否积分
     */
    private String gspacInteFlag;

    /**
     * 积分倍率
     */
    private String gspacInteRate;

    /**
     * 组合金额
     */
    private BigDecimal gspasAmt;

    /**
     * 组合折扣
     */
    private String gspasRebate;

    /**
     * 赠送数量
     */
    private String gspasQty;
}
