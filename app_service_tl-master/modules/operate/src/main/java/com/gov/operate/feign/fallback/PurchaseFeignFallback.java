package com.gov.operate.feign.fallback;

import com.gov.common.response.Result;
import com.gov.operate.feign.PurchaseFeign;
import com.gov.operate.feign.ReportFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.MatchProductInData;
import com.gov.operate.feign.dto.SupplierPaymentAtt;
import org.springframework.stereotype.Component;


@Component
public class PurchaseFeignFallback implements PurchaseFeign {

    @Override
    public Result supplierPaymentAtt(SupplierPaymentAtt supplierPaymentAtt) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }
}
