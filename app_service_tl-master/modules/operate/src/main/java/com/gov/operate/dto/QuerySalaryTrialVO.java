package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class QuerySalaryTrialVO {

    @NotNull(message = "方案主键不能为空")
    private Integer gspId;
}
