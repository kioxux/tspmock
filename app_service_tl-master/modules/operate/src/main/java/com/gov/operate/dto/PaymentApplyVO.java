package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author awang
 */
@Data
@EqualsAndHashCode
public class PaymentApplyVO extends Pageable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 对账单号
     */
    private String billNum;

    /**
     * 付款类型
     */
    private String businessType;

    /**
     * 供应商代码
     */
    private String supCode;

    /**
     * 应付金额
     */
    private BigDecimal payAmt;

    /**
     * 已付金额
     */
    private BigDecimal paidAmt;

    /**
     * 本次付款金额
     */
    private BigDecimal thisAmt;

    /**
     * 备注
     */
    private String remark;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 地点
     */
    private String site;

    /**
     * 银行名称
     */
    private String supBankName;

    /**
     * 银行账号
     */
    private String supBankAccount;
}
