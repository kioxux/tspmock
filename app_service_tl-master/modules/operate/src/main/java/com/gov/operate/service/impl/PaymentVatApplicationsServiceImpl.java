package com.gov.operate.service.impl;

import com.gov.operate.entity.PaymentVatApplications;
import com.gov.operate.mapper.PaymentVatApplicationsMapper;
import com.gov.operate.service.IPaymentVatApplicationsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
@Service
public class PaymentVatApplicationsServiceImpl extends ServiceImpl<PaymentVatApplicationsMapper, PaymentVatApplications> implements IPaymentVatApplicationsService {

}
