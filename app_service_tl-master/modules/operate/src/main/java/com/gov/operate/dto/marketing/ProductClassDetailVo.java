package com.gov.operate.dto.marketing;

import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 14:03 2021/8/16
 * @Description：商品分类详情出参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class ProductClassDetailVo {

    private String productClassCode;

    private String productClassName;
}
