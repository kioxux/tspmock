package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DOCUMENT_BILL_D")
@ApiModel(value="DocumentBillD对象", description="")
public class DocumentBillD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GDBD_NUM")
    private String gdbdNum;

    @ApiModelProperty(value = "业务类型")
    @TableField("GDBD_BUSINESS_TYPE")
    private String gdbdBusinessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("GDBD_MAT_DN_ID")
    private String gdbdMatDnId;

    @ApiModelProperty(value = "物料凭证号")
    @TableField("MAT_ID")
    private String matId;

    @ApiModelProperty(value = "物料凭证年份")
    @TableField("MAT_YEAR")
    private String matYear;

    @ApiModelProperty(value = "物料凭证行号")
    @TableField("MAT_LINE_NO")
    private String matLineNo;

    @ApiModelProperty(value = "商品编码")
    @TableField("MAT_PRO_CODE")
    private String matProCode;

    @ApiModelProperty(value = "行金额")
    @TableField("GDBD_LINE_AMOUNT")
    private BigDecimal gdbdLineAmount;

    @ApiModelProperty(value = "数量")
    @TableField("MAT_QTY")
    private BigDecimal matQty;

    @ApiModelProperty(value = "总金额（批次）")
    @TableField("MAT_BAT_AMT")
    private BigDecimal matBatAmt;

    @ApiModelProperty(value = "税金（批次）")
    @TableField("MAT_RATE_BAT")
    private BigDecimal matRateBat;

    @ApiModelProperty(value = "创建人")
    @TableField("GDBD_CREATE_USER")
    private String gdbdCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GDBD_CREATE_DATE")
    private String gdbdCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GDBD_CREATE_TIME")
    private String gdbdCreateTime;


}
