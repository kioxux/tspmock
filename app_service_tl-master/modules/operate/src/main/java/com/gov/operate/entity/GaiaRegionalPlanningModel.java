package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Zhangchi
 * @since 2021/10/15/10:16
 */
@Data
@TableName("GAIA_REGIONAL_PLANNING_MODEL")
public class GaiaRegionalPlanningModel implements Serializable {
    private static final long serialVersionUID=1L;
    /**
     * ID
     */
    @TableId("ID")
    private Integer Id;

    /**
     * 加盟商
     */
    @TableField("CLIENT")
    private String client;

    /**
     * 区域编码
     */
    @TableField("REGIONAL_CODE")
    private String regionalCode;

    /**
     * 区域名称
     */
    @TableField("REGIONAL_NAME")
    private String regionalName;

    /**
     * 区域级别 3：三级区域、2：二级区域、1：一级区域
     */
    @TableField("LEVEL")
    private Integer level;

    /**
     * 排序编码
     */
    @TableField("SORT_NUM")
    private Integer sortNum;

    /**
     * 上级Id
     */
    @TableField("PARENT_ID")
    private Integer parentId;

    /**
     * 创建人
     */
    @TableField("CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 修改人
     */
    @TableField("UPDATE_USER")
    private String updateUser;

    /**
     *修改时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;

    /**
     * 是否删除：0否，1是
     */
    @TableField("DELETE_FLAG")
    private String deleteFlag;

    /**
     * 是否为默认 0：否、1：是
     */
    @TableField("IS_DEFAULT")
    private String isDefault;
}
