package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_DISCOUNT")
@ApiModel(value="SdDiscount对象", description="")
public class SdDiscount extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "营销活动ID")
    @TableField("GSM_MARKETID")
    private String gsmMarketid;

    @ApiModelProperty(value = "折扣ID")
    @TableField("GSM_DISCOUNT")
    private String gsmDiscount;

    @ApiModelProperty(value = "门店编码")
    @TableField("GSM_STORE")
    private String gsmStore;

    @ApiModelProperty(value = "折扣名称")
    @TableField("GSM_DISCOUNT_NAME")
    private String gsmDiscountName;

    @ApiModelProperty(value = "促销ID")
    @TableField("GSM_PROMOT_ID")
    private String gsmPromotId;

    @ApiModelProperty(value = "参数1")
    @TableField("PARAMETER_1")
    private String parameter1;

    @ApiModelProperty(value = "参数2")
    @TableField("PARAMETER_2")
    private String parameter2;

    @ApiModelProperty(value = "参数3")
    @TableField("PARAMETER_3")
    private String parameter3;

    @ApiModelProperty(value = "参数4")
    @TableField("PARAMETER_4")
    private String parameter4;

}
