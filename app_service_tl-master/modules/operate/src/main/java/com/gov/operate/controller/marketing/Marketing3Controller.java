package com.gov.operate.controller.marketing;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.marketing.*;
import com.gov.operate.entity.SdMarketingProm;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IMarketingTask3Service;
import com.gov.operate.service.ISdMarketingBasic3Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Slf4j
@Api(tags = "营销活动任务")
@RestController
@RequestMapping("marketing03/marketing")
public class Marketing3Controller {

    @Resource
    private IMarketingTask3Service iMarketingTaskService;
    @Resource
    private ISdMarketingBasic3Service marketingBasicService;

    @Log("立即执行")
    @ApiOperation(value = "立即执行")
    @PostMapping("execute")
    public Result execute(@RequestBody Map<String, Object> map) {
        log.info("营销立即执行{}", JSONObject.toJSONString(map));
        Integer id = map.get("id") == null ? null : NumberUtils.toInt(map.get("id").toString());
        String clientId = map.get("clientId") == null ? null : map.get("clientId").toString();
        String flowNo = map.get("flowNo") == null ? null : map.get("flowNo").toString();
        String status = map.get("status") == null ? null : map.get("status").toString();
        iMarketingTaskService.execute(id, clientId, flowNo, status);
        return ResultUtil.success();
    }

    @Log("发送审批")
    @ApiOperation(value = "发送审批")
    @PostMapping("approval")
    public Result approval(@RequestJson(value = "id", name = "营销任务ID") Integer id) {
        iMarketingTaskService.approval(id);
        return ResultUtil.success();
    }

    @Log("转交修改")
    @ApiOperation(value = "转交修改")
    @PostMapping("transferToEdit")
    public Result transferToEdit(@RequestJson("id") Integer id, @RequestJson("gsmOpinion") String gsmOpinion) {
        marketingBasicService.transferToEdit(id, gsmOpinion);
        return ResultUtil.success();
    }

    @Log("不予执行")
    @ApiOperation(value = "不予执行")
    @PostMapping("noEnforcement")
    public Result noEnforcement(@RequestJson("id") Integer id) {
        marketingBasicService.noEnforcement(id);
        return ResultUtil.success();
    }

    @Log("推荐营销任务列表")
    @ApiOperation(value = "推荐营销任务列表")
    @PostMapping("getGsmTaskList")
    public Result getGsmTaskList(@RequestBody GsmTaskListVO2 vo) {
        return ResultUtil.success(marketingBasicService.getGsmTaskList(vo));
    }

    @Log("当前历史营销任务列表")
    @ApiOperation(value = "当前历史营销任务列表")
    @PostMapping("getTaskExecutedList")
    public Result getTaskExecutedList(@RequestBody GsmTaskListVO2 vo) {
        return ResultUtil.success(marketingBasicService.getTaskExecutedList(vo));
    }

    @Log("营销任务详情")
    @ApiOperation(value = "营销任务详情")
    @GetMapping("getGsmTaskDetail")
    public Result getGsmTaskDetail(@RequestParam("id") Integer id,
                                   @RequestParam("type") Integer type,
                                   @RequestParam(value = "stoCode", required = false) String stoCode) {
        return ResultUtil.success(marketingBasicService.getGsmTaskDetail(id, type, stoCode));
    }

    @Log("营销任务促销详情")
    @ApiOperation(value = "营销任务促销详情")
    @PostMapping("getPromDetails")
    public Result getPromDetails(@Valid @RequestBody GetPromDetailsVO vo) {
        return ResultUtil.success(marketingBasicService.getPromDetails(vo));
    }

    @Log("营销任务保存")
    @ApiOperation(value = "营销任务保存")
    @PostMapping("saveGsmTask")
    public Result saveGsmTask(@Valid @RequestBody SaveGsmTaskVO3 vo) {
        marketingBasicService.saveGsmTask(vo);
        return ResultUtil.success();
    }

    @Log("选品选择列表")
    @ApiOperation(value = "选品选择列表")
    @PostMapping("getProductListToCheck")
    public Result getProductListToCheck(@Valid @RequestBody GetProductListToCheckVO vo) {
        return ResultUtil.success(marketingBasicService.getProductListToCheck(vo));
    }

    @Log("活动商品列表")
    @ApiOperation(value = "活动商品列表")
    @PostMapping("getProductList")
    public Result getProductList(@RequestJson("pageNum") Integer pageNum, @RequestJson("pageSize") Integer pageSize, @RequestJson("id") Integer id,
                                 @RequestJson(value = "proKey", required = false) String proKey,
                                 @RequestJson(value = "mllStart", required = false) BigDecimal mllStart,
                                 @RequestJson(value = "mllEnd", required = false) BigDecimal mllEnd,
                                 @RequestJson(value = "promFitsType", required = false) Integer promFitsType,
                                 @RequestJson(value = "promFitsStart", required = false) BigDecimal promFitsStart,
                                 @RequestJson(value = "promFitsEnd", required = false) BigDecimal promFitsEnd) {
        return ResultUtil.success(marketingBasicService.getProductList(pageNum, pageSize, id, proKey, mllStart, mllEnd, promFitsType, promFitsStart, promFitsEnd));
    }


    @Log("导出活动商品列表")
    @ApiOperation(value = "导出活动商品列表")
    @PostMapping("exportProductList")
    public Result exportProductList(@RequestJson("pageNum") Integer pageNum, @RequestJson("pageSize") Integer pageSize, @RequestJson("id") Integer id,
                                 @RequestJson(value = "proKey", required = false) String proKey,
                                 @RequestJson(value = "mllStart", required = false) BigDecimal mllStart,
                                 @RequestJson(value = "mllEnd", required = false) BigDecimal mllEnd,
                                 @RequestJson(value = "promFitsType", required = false) Integer promFitsType,
                                 @RequestJson(value = "promFitsStart", required = false) BigDecimal promFitsStart,
                                 @RequestJson(value = "promFitsEnd", required = false) BigDecimal promFitsEnd) throws IOException {
        return marketingBasicService.exportProductList(pageNum, pageSize, id, proKey, mllStart, mllEnd, promFitsType, promFitsStart, promFitsEnd);
    }

    @Log("活动商品删除")
    @ApiOperation(value = "活动商品删除")
    @PostMapping("deleteProduct")
    public Result deleteProduct(@RequestJson("list") List<Integer> list) {
        marketingBasicService.deleteProduct(list);
        return ResultUtil.success();
    }

    @Log("活动商品保存")
    @ApiOperation(value = "活动商品保存")
    @PostMapping("saveProduct")
    public Result saveProduct(@RequestBody SaveProduct saveProduct) {
        marketingBasicService.saveProduct(saveProduct.getMarketingId(), saveProduct.getList());
        return ResultUtil.success();
    }

    @Log("营销促销列表")
    @ApiOperation(value = "营销促销列表")
    @PostMapping("getPromList")
    public Result getPromList(@RequestJson("id") Integer id) {
        return ResultUtil.success(marketingBasicService.getPromList(id));
    }

    @Log("促销商品")
    @ApiOperation(value = "促销商品")
    @PostMapping("getProductBySelfCode")
    public Result getProductBySelfCode(@RequestJson("id") Integer id,
                                       @RequestJson(value = "promId", required = false) Integer promId,
                                       @RequestJson(value = "proKey", required = false) String proKey,
                                       @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                       @RequestJson(value = "mllStart", required = false) BigDecimal mllStart,
                                       @RequestJson(value = "mllEnd", required = false) BigDecimal mllEnd,
                                       @RequestJson(value = "pageNum", required = false) Integer pageNum,
                                       @RequestJson(value = "pageSize", required = false) Integer pageSize) {
        return ResultUtil.success(marketingBasicService.getProductBySelfCode(id, promId, proKey, proSelfCode, mllStart, mllEnd, pageNum, pageSize));
    }

    @Log("赠品选择列表")
    @ApiOperation(value = "赠品选择列表")
    @PostMapping("getGiveawayListToCheck")
    public Result getGiveawayListToCheck(@Valid @RequestBody GetGiveawayListToCheckVO vo) {
        return ResultUtil.success(marketingBasicService.getGiveawayListToCheck(vo));
    }

    @Log("赠品保存")
    @ApiOperation(value = "赠品保存")
    @PostMapping("saveGiveaway")
    public Result saveGiveaway(@RequestBody List<GetProductListToCheckDTO> list) {
        marketingBasicService.saveGiveaway(list);
        return ResultUtil.success();
    }

    /**
     * 自定义参数
     *
     * @return
     */
    @Log("自定义参数获取")
    @ApiOperation(value = "自定义参数获取")
    @PostMapping("getCustomParams")
    public Result getCustomParams(@RequestJson("promId") Integer promId) {
        return ResultUtil.success(marketingBasicService.getCustomParams(promId));
    }

    @Log("促销参数详情获取")
    @ApiOperation(value = "促销参数详情获取")
    @PostMapping("getParams")
    public Result getParams(@RequestJson("promId") Integer promId) {
        return ResultUtil.success(marketingBasicService.getParams(promId));
    }

    @Log("自定义参数保存")
    @ApiOperation(value = "自定义参数保存")
    @PostMapping("saveCoustomParams")
    public Result saveCoustomParams(@RequestBody List<CustomParam> list) {
        marketingBasicService.saveCoustomParams(list);
        return ResultUtil.success();
    }

    @Log("促销参数详情保存")
    @ApiOperation(value = "促销参数详情保存")
    @PostMapping("saveParams")
    public Result saveParams(@RequestBody SdMarketingPromPrd sdMarketingPromPrd) {
        marketingBasicService.saveParams(sdMarketingPromPrd);
        return ResultUtil.success();
    }

    @Log("客户新增促销")
    @ApiOperation(value = "客户新增促销")
    @PostMapping("addProm")
    public Result addProm(@RequestBody SdMarketingProm sdMarketingProm) {
        marketingBasicService.addProm(sdMarketingProm);
        return ResultUtil.success();
    }

    @Log("目标试算")
    @ApiOperation(value = "目标试算")
    @PostMapping("marketingAimsTrial")
    public Result marketingAimsTrial(@Valid @RequestBody MarketingAimsTrialVO vo) {
        return ResultUtil.success(marketingBasicService.marketingAimsTrial(vo));
    }

}

