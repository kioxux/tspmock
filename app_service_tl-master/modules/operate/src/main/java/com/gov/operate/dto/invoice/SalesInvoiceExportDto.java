package com.gov.operate.dto.invoice;

import com.gov.common.excel.annotation.ExcelField;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SalesInvoiceExportDto {

    @ExcelField(title = "序号", type = 1, sort = 0)
    private Integer seq;

    @ExcelField(title = "货物或应税劳务、服务名称", type = 1, sort = 1)
    private String proName;

    @ExcelField(title = "规格型号", type = 1, sort = 2)
    private String proSpecs;

    @ExcelField(title = "计量单位", type = 1, sort = 3)
    private String proUnit;

    @ExcelField(title = "数量", type = 1, sort = 4, align = 3)
    private BigDecimal qty;

    @ExcelField(title = "金额", type = 1, sort = 5, align = 3)
    private BigDecimal amount;

    @ExcelField(title = "税率", type = 1, sort = 6)
    private BigDecimal tax;

    @ExcelField(title = "商品税目", type = 1, sort = 7)
    private String taxItem;

    @ExcelField(title = "折扣金额", type = 1, sort = 8)
    private String discountAmount;

    @ExcelField(title = "税额", type = 1, sort = 9, align = 3)
    private BigDecimal addTax;

    @ExcelField(title = "折扣税额", type = 1, sort = 10)
    private String discountTax;

    @ExcelField(title = "折扣率", type = 1, sort = 11)
    private String discountRate;

    @ExcelField(title = "单价", type = 1, sort = 12, align = 3)
    private BigDecimal price;

    @ExcelField(title = "价格方式", type = 1, sort = 13)
    private String priceMethod;

    @ExcelField(title = "税收分类编码版本", type = 1, sort = 14)
    private String taxVersion;

    @ExcelField(title = "税收分类编码", type = 1, sort = 15)
    private String proTaxClass;

    @ExcelField(title = "企业商品编码", type = 1, sort = 16)
    private String proCode;

    @ExcelField(title = "使用优惠政策标识", type = 1, sort = 17)
    private String offerSign;

    @ExcelField(title = "零税率标识", type = 1, sort = 18)
    private String rateSign;

    @ExcelField(title = "优惠政策说明", type = 1, sort = 19)
    private String policy;

    @ExcelField(title = "中外合作油气田标识", type = 1, sort = 20)
    private String flag;

}
