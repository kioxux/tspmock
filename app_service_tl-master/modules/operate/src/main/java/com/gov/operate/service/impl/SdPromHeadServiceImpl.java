package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.StringUtils;
import com.gov.operate.dto.GetPromListVO;
import com.gov.operate.entity.SdPromHead;
import com.gov.operate.mapper.SdPromHeadMapper;
import com.gov.operate.service.ISdPromHeadService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-28
 */
@Service
public class SdPromHeadServiceImpl extends ServiceImpl<SdPromHeadMapper, SdPromHead> implements ISdPromHeadService {

    /**
     *
     */
    @Override
    public IPage<SdPromHead> getPromList(GetPromListVO vo) {
        IPage<SdPromHead> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<SdPromHead> query = new QueryWrapper<SdPromHead>()
                .eq(StringUtils.isNotEmpty(vo.getClient()), "CLIENT", vo.getClient())
                // TODO 门店去掉
//                .eq(StringUtils.isNotEmpty(vo.getGsphBrId()), "GSPH_BR_ID", vo.getGsphBrId())
                .eq(StringUtils.isNotEmpty(vo.getGsphName()), "GSPH_NAME", vo.getGsphName())
                .eq(StringUtils.isNotEmpty(vo.getGsphStatus()), "GSPH_STATUS", vo.getGsphStatus())
                .orderByDesc("GSPH_BEGIN_DATE");
        return this.page(page, query);
    }
}
