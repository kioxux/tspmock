package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.MemberDTO;
import com.gov.operate.entity.SdMemberCard;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-17
 */
public interface SdMemberCardMapper extends BaseMapper<SdMemberCard> {

    /**
     * 指定加盟商，指定门店，所有会员
     */
    List<MemberDTO> getMemberList(@Param("stoCode") String stoCode, @Param("client") String client);

    /**
     * 指定加盟商，指定门店，指定手机号的指定会员
     */
    MemberDTO getMemberCard(@Param("stoCode") String stoCode, @Param("client") String client, @Param("gsmbMobile") String gsmbMobile);

    int selectCardCountByDate(@Param("storeId") String storeId, @Param("clientId") String client, @Param("dateTime") String dateTime);

    int selectCardCountByParam(@Param("stoCodeList") List<String> stoCodeList, @Param("clientId") String client, @Param("dateTime") String dateTime);
}
