package com.gov.operate.dto.marketing;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class GetCommonSearchListFromMenu {

}
