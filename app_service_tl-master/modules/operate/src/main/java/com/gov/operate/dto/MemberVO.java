package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class MemberVO {

    @NotBlank(message = "会员ID不能为空")
    private String memberId;

    @NotBlank(message = "会员卡号不能为空")
    private String memberCard;

    @NotBlank(message = "测试手机号不能为空")
    private String memberPhone;

    @NotBlank(message = "签名不能为空")
    private String smsSgin;

    @NotBlank(message = "门店编号不能为空")
    private String storeCode;

    private String gmbBrShortName;

}
