package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @TableName GAIA_SSP_USER_PRODUCT
 */
@TableName(value = "GAIA_SSP_USER_PRODUCT")
@Data
public class SspUserProduct extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 统一社会信用玛
     */
    private String creditCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 连锁总部
     */
    private String chainHead;

    /**
     * 连锁名称
     */
    private String chainName;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}