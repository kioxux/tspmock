package com.gov.operate.dto.marketing;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 14:24 2021/8/16
 * @Description：查询品牌信息入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GetBrandPageDto {

    @NotNull(message = "分页参数不能为空")
    private Integer pageNum;

    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;

    private List<String> queryStringList;
    private String queryString;
}
