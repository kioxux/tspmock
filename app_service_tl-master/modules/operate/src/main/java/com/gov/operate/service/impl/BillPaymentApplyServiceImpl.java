package com.gov.operate.service.impl;

import com.gov.operate.entity.BillPaymentApply;
import com.gov.operate.mapper.BillPaymentApplyMapper;
import com.gov.operate.service.IBillPaymentApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-23
 */
@Service
public class BillPaymentApplyServiceImpl extends ServiceImpl<BillPaymentApplyMapper, BillPaymentApply> implements IBillPaymentApplyService {

}
