package com.gov.operate.controller;


import com.gov.common.entity.Pageable;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.EditWechatVO;
import com.gov.operate.dto.GetWechatVO;
import com.gov.operate.dto.QueryWechatListVO;
import com.gov.operate.dto.weChat.*;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IOfficialAccountsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-28
 */
@RestController
@RequestMapping("wechat")
public class WechatController {

    @Resource
    private IOfficialAccountsService officialAccountsService;


    @Log("公众号列表(平台用户)")
    @ApiOperation(value = "公众号列表(平台用户)")
    @PostMapping("queryWechatListByPlatform")
    public Result queryWechatListByPlatform(@RequestBody @Valid QueryWechatListVO vo) {
        return ResultUtil.success(officialAccountsService.queryWechatListByPlatform(vo));
    }

    @Log("公众号列表")
    @ApiOperation(value = "公众号列表")
    @PostMapping("queryWechatList")
    public Result queryWechatList(@RequestBody @Valid QueryWechatListVO vo) {
        return ResultUtil.success(officialAccountsService.queryWechatList(vo));
    }

    @Log("公众号编辑")
    @ApiOperation(value = "公众号编辑")
    @PostMapping("editWechat")
    public Result editWechat(@RequestBody @Valid EditWechatVO vo) {
        officialAccountsService.editWechat(vo);
        return ResultUtil.success();
    }

    @Log("公众号推文授权")
    @ApiOperation(value = "公众号推文授权")
    @PostMapping("wechatTweets")
    public Result wechatTweets(@RequestJson(value = "goaType", name = "类型") Integer goaType,
                               @RequestJson(value = "goaCode", name = "公司编号") String goaCode,
                               @RequestJson(value = "wechatTweets", name = "是否授权") String wechatTweets) {
        officialAccountsService.wechatTweets(goaCode, goaType, wechatTweets);
        return ResultUtil.success();
    }

    @Log("公众号微信会员卡")
    @ApiOperation(value = "公众号微信会员卡")
    @PostMapping("wechatMembershipCard")
    public Result wechatMembershipCard(@RequestJson(value = "goaType", name = "类型") Integer goaType,
                               @RequestJson(value = "goaCode", name = "公司编号") String goaCode,
                               @RequestJson(value = "wechatMembershipCard", name = "是否授权") String wechatMembershipCard) {
        officialAccountsService.wechatMembershipCard(goaCode, goaType, wechatMembershipCard);
        return ResultUtil.success();
    }


    @Log("公众号数据获取")
    @ApiOperation(value = "公众号数据获取")
    @PostMapping("getWechat")
    public Result getWechat(@RequestBody @Valid GetWechatVO vo) {
        return ResultUtil.success(officialAccountsService.getWechat(vo));
    }

    @Log("公众号APPID和SECRECT设置")
    @ApiOperation(value = "公众号APPID和SECRECT设置")
    @PostMapping("editAppidAndSecrect")
    public Result editAppidAndSecrect(@RequestBody @Valid AppidAndSecrectVO vo) {
        officialAccountsService.editAppidAndSecrect(vo);
        return ResultUtil.success();
    }


    @Log("公众号菜单列表")
    @ApiOperation(value = "公众号菜单列表")
    @PostMapping("getWeChatMenu")
    public Result getWeChatMenu(@RequestBody @Valid GetWeChatMenuVO vo) {
        return ResultUtil.success(officialAccountsService.getWeChatMenu(vo));
    }

    @Log("公众号菜单编辑")
    @ApiOperation(value = "公众号菜单编辑")
    @PostMapping("editWeChatMenu")
    public Result editWeChatMenu(@RequestBody @Valid EditWeChatMenuVO vo) {
        officialAccountsService.editWeChatMenu(vo);
        return ResultUtil.success();
    }

    @Log("平台公众号草稿列表")
    @ApiOperation(value = "平台公众号草稿列表")
    @PostMapping("getDraftBatchgetList")
    public Result getDraftBatchgetList(@RequestBody @Valid GetMaterialListVO vo) {
        return ResultUtil.success(officialAccountsService.getDraftBatchgetList(vo));
    }

    @Log("平台公众号草稿发布")
    @ApiOperation(value = "平台公众号草稿发布")
    @PostMapping("freepublishSubmit")
    public Result freepublishSubmit(@RequestJson("media_id") String media_id) {
        officialAccountsService.freepublishSubmit(media_id);
        return ResultUtil.success();
    }

    @Log("平台公众号发布记录")
    @ApiOperation(value = "平台公众号发布记录")
    @PostMapping("getFreepublishBatchgeList")
    public Result getFreepublishBatchgeList(@RequestBody @Valid GetMaterialListVO vo) {
        vo.setMaterialType("1");
        return ResultUtil.success(officialAccountsService.getMaterialList(vo));
    }

    @Log("平台公众号素材列表")
    @ApiOperation(value = "公众号素材列表")
    @PostMapping("getMaterialList")
    public Result getMaterialList(@RequestBody @Valid GetMaterialListVO vo) {
        return ResultUtil.success(officialAccountsService.getMaterialList(vo));
    }

    @Log("客户列表弹框")
    @ApiOperation(value = "客户列表弹框")
    @PostMapping("getCostomerList")
    public Result getCostomerList(@RequestBody @Valid GetCostomerListVO vo) {
        return ResultUtil.success(officialAccountsService.getCostomerList(vo));
    }

    @Log("客户下拉框")
    @ApiOperation(value = "客户下拉框")
    @PostMapping("getCostomerDropDownList")
    public Result getCostomerDropDownList() {
        return ResultUtil.success(officialAccountsService.getCostomerDropDownList());
    }


    @Log("客户公众号素材列表")
    @ApiOperation(value = "客户公众号素材列表")
    @PostMapping("getCostomerMaterialList")
    public Result getCostomerMaterialList(@RequestBody @Valid GetCostomerMaterialListVO vo) {
        return ResultUtil.success(officialAccountsService.getCostomerMaterialList(vo));
    }

    @Log("公众号文章推送")
    @ApiOperation(value = "公众号文章推送")
    @PostMapping("tweetsPush")
    public Result tweetsPush(@RequestBody @Valid TweetsPushVO vo) {
        officialAccountsService.tweetsPush(vo);
        return ResultUtil.success();
    }

    @Log("公众号文章发布")
    @ApiOperation(value = "公众号文章发布")
    @PostMapping("tweetsRelease")
    public Result tweetsRelease(@RequestBody @Valid TweetsReleaseVO vo) {
        officialAccountsService.tweetsRelease(vo);
        return ResultUtil.success();
    }

    @Log("推文预览")
    @ApiOperation(value = "推文预览")
    @PostMapping("getTweetsContent")
    public Result getTweetsContent(@RequestJson("url") String url) {
        return officialAccountsService.getTweetsContent(url);
    }
}

