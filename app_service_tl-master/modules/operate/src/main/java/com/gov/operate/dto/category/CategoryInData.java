package com.gov.operate.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CategoryInData {
    /**
     * 加盟商
     */
    @ApiModelProperty("加盟商")
    private String client;
    /**
     * 门店
     */
    @ApiModelProperty("门店编码")
    private String stoCode;
    /**
     * 结论 : 1调价；2淘汰；3引进；4促销
     */
    @ApiModelProperty("结论 1调价；2淘汰；3引进；4促销")
    private String aplText;
    /**
     * 结论编号
     */
    @ApiModelProperty("结论编号")
    private String aplCode;

    /**
     * 建议调价
     */
    @ApiModelProperty("建议调价")
    private String aplPrice;

    /**
     * 商品编码
     */
    @ApiModelProperty("商品编码")
    private String proCode;

    /**
     * 淘汰原因
     */
    @ApiModelProperty("淘汰原因")
    private String aplProReason;

    /**
     * 更新日期
     */
    @ApiModelProperty("更新日期")
    private String aplUpdateDate;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private String aplUpdateTime;

    /**
     * 执行状态 1建议中；2用户执行；3用户放弃；4自动关闭
     */
    @ApiModelProperty("执行状态 1建议中；2用户执行；3用户放弃；4自动关闭")
    private String aplStatus;

    private String aplInStatus;

    @ApiModelProperty("变更人")
    private String aplConEmp;

    @ApiModelProperty("结论日期")
    private String aplConDate;

    @ApiModelProperty("定时日期")
    private String aplConSetDate;

    @ApiModelProperty("定时查询日期")
    private String aplConDsDate;

    @ApiModelProperty("入参状态")
    private String aplInText;

    @ApiModelProperty("入参日期")
    private String aplConInDate;

    @ApiModelProperty("当前日期前七天日期")
    private String aplConMixDate;

    private List<String> stoCodeList;
}
