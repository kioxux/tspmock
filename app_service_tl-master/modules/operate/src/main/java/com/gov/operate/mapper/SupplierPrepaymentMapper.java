package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.dto.SupplierPaymentDetailVO;
import com.gov.operate.dto.invoice.SupplierPrepaymentDto;
import com.gov.operate.entity.StoreData;
import com.gov.operate.entity.SupplierPrepayment;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-02-01
 */
public interface SupplierPrepaymentMapper extends BaseMapper<SupplierPrepayment> {

    /**
     * 付款单号
     *
     * @param client
     * @return
     */
    String selectPayOrderId(@Param("client") String client);

    IPage<SupplierPrepaymentDto> getSupplierPaymentList(Page<SupplierPrepaymentDto> page,
                                                        @Param("client") String client,
                                                        @Param("gspApplyDateStart") String gspApplyDateStart,
                                                        @Param("gspApplyDateEnd") String gspApplyDateEnd,
                                                        @Param("payCompanyCode") String payCompanyCode,
                                                        @Param("supCode") String supCode,
                                                        @Param("payOrderId") String payOrderId,
                                                        @Param("applicationPaymentAmountStart") BigDecimal applicationPaymentAmountStart,
                                                        @Param("applicationPaymentAmountEnd") BigDecimal applicationPaymentAmountEnd,
                                                        @Param("founder") String founder,
                                                        @Param("applicationStatus") String applicationStatus,
                                                        @Param("gspApprovalDateStart") String gspApprovalDateStart,
                                                        @Param("gspApprovalDateEnd") String gspApprovalDateEnd,
                                                        @Param("supSalesman") String supSalesman,
                                                        @Param("approvalRemark") String approvalRemark,
                                                        @Param("wfDescription") String remark);

    IPage<SelectWarehousingDTO> getSupplierPaymentDetail(Page<SelectWarehousingDTO> page,
                                                         @Param("client") String client,
                                                         @Param("gspApplyDateStart") String gspApplyDateStart,
                                                         @Param("gspApplyDateEnd") String gspApplyDateEnd,
                                                         @Param("payCompanyCode") String payCompanyCode,
                                                         @Param("orderCode") String orderCode,
                                                         @Param("supSelfCode") String supCode,
                                                         @Param("type") String type,
                                                         @Param("invoiceSalesman") String invoiceSalesman);

    List<StoreData> getCompanyList(@Param("client") String client);

    List<StoreData> getDcList(@Param("client") String client);

    List<StoreData> getCustomerList(@Param("client") String client, @Param("matSiteCode") String matSiteCode);

    IPage<SupplierPaymentDetailVO> viewWriteOffList(Page<SupplierPaymentDetailVO> page, @Param("client") String client, @Param("payOrderId") String payOrderId);

    List<String> getApprovalRemarkList(@Param("client") String client);
}
