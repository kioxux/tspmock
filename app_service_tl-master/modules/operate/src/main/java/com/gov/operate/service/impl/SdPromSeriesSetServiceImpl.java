package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromSeriesSet;
import com.gov.operate.mapper.SdPromSeriesSetMapper;
import com.gov.operate.service.ISdPromSeriesSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
@Service
public class SdPromSeriesSetServiceImpl extends ServiceImpl<SdPromSeriesSetMapper, SdPromSeriesSet> implements ISdPromSeriesSetService {

}
