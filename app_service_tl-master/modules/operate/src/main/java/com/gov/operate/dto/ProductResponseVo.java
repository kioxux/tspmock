package com.gov.operate.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductResponseVo {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 成分
     */
    private String proCompclass;


    /**
     * 成分
     */
    private String proCompclassName;


    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 零售价
     */
    private BigDecimal priceNormal;

    /**
     * 已选 0：未选，1：已选
     */
    private Integer selFlg;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 参考毛利率
     */
    private BigDecimal proMll;
}
