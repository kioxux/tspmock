package com.gov.operate.controller.v2;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.HomeChartRequestDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyDTO;
import com.gov.operate.dto.monthPushMoney.HomePushMoneyVo;
import com.gov.operate.service.IMobileHomeService;
import com.gov.operate.service.IMobileHomeV2Service;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("mobile/home/v2")
public class MobileHomeV2Controller {


    @Resource
    private IMobileHomeV2Service mobileHomeV2Service;

    @Log("证照预警门店列表")
    @PostMapping("getZzOrgListByUserAuth")
    @ApiOperation(value = "证照预警门店列表")
    public Result getZzOrgListByUserAuth() {
        return mobileHomeV2Service.getZzOrgListByUserAuth();
    }

    @PostMapping("getHomeStockList")
    @ApiOperation(value = "首页库存数据") 
    public Result getHomeStockList(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeV2Service.getHomeStockList(homeChartRequestDTO);
    }

    @PostMapping("loginAppSaleTotal")
    @ApiOperation(value = "APP首页调整汇总")
    public Result loginAppSaleTotal(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeV2Service.loginAppSaleTotal(homeChartRequestDTO);
    }

    @PostMapping("getPaymentMethodList")
    @ApiOperation(value = "收银方式汇总")
    public Result getPaymentMethodList(@RequestBody HomeChartRequestDTO homeChartRequestDTO) {
        return mobileHomeV2Service.getPaymentMethodList(homeChartRequestDTO);
    }


}
