package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_YEAR_AWARDS")
@ApiModel(value="SalaryYearAwards对象", description="")
public class SalaryYearAwards extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSYA_ID", type = IdType.AUTO)
    private Integer gsyaId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSYA_GSP_ID")
    private Integer gsyaGspId;

    @ApiModelProperty(value = "日销售额最小值")
    @TableField("GSYA_SALES_MIN")
    private BigDecimal gsyaSalesMin;

    @ApiModelProperty(value = "日销售额最大值")
    @TableField("GSYA_SALES_MAX")
    private BigDecimal gsyaSalesMax;

    @ApiModelProperty(value = "提成比例")
    @TableField("GSYA_COMMISION")
    private BigDecimal gsyaCommision;

    @ApiModelProperty(value = "顺序")
    @TableField("GSYA_SORT")
    private Integer gsyaSort;


}
