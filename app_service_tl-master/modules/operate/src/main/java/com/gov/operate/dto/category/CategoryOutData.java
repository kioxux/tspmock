package com.gov.operate.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CategoryOutData {
    /**
     * 加盟商
     */
    @ApiModelProperty("加盟商")
    private String client;
    /**
     * 门店
     */
    @ApiModelProperty("门店编码")
    private String stoCode;

    @ApiModelProperty("门店")
    private String stoName;
    /**
     * 商品编码
     */
    @ApiModelProperty("商品编码")
    private String proCode;
    /**
     * 商品名
     */
    @ApiModelProperty("商品名")
    private String proName;
    /**
     * 规格
     */
    @ApiModelProperty("规格")
    private String proSpecs;
    /**
     * 零售价
     */
    @ApiModelProperty("零售价")
    private String proPrice;
    /**
     * 建议数量
     */
    @ApiModelProperty("建议数量")
    private String proConQty;
    /**
     * 建议价格
     */
    @ApiModelProperty("建议价格")
    private String proConPrice;
    /**
     * 库存数量
     */
    @ApiModelProperty("库存数量")
    private String proStoctQty;
    /**
     * 移动平均价
     */
    @ApiModelProperty("移动平均价")
    private String proMovPrice;
    /**
     * 三个月月均销售量
     */
    @ApiModelProperty("三个月月均销售量")
    private String proMonthAvgQty;
    /**
     * 移动月销售量
     */
    @ApiModelProperty("移动月销售量")
    private String proMonthQty;
    /**
     * 含税成本
     */
    @ApiModelProperty("含税成本")
    private String allCostAmt;
    /**
     * 生产厂家
     */
    @ApiModelProperty("生产厂家")
    private String proFactoryName;
    /**
     * 结论编号
     */
    @ApiModelProperty("结论编号")
    private String aplCode;
    /**
     * 结论 : adjustprice 调价 outproduct 淘汰商品
     */
    @ApiModelProperty("结论")
    private String aplText;
    /**
     * 建议价格
     */
    @ApiModelProperty("建议价格")
    private String aplConPrice;

    /**
     * 原因
     */
    @ApiModelProperty("原因")
    private String aplConReason;

    /**
     * 库存成本
     */
    @ApiModelProperty("库存成本")
    private String proStoctAmt;

    @ApiModelProperty("结论日期")
    private String aplConDate;

    @ApiModelProperty("定时日期")
    private String aplConSetDate;

    @ApiModelProperty("结论方案")
    private String aplConPlan;

    @ApiModelProperty("更新时间")
    private String aplUpdateDate;

    @ApiModelProperty("执行状态 1建议中；2用户执行；3用户放弃；4自动关闭")
    private String aplConStu;

    @ApiModelProperty("毛利率")
    private String aplConProfit;

    @ApiModelProperty("更新人")
    private String aplConEmp;

    private String proRegisterNo;
}
