package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MARKETING_PROM")
@ApiModel(value="SdMarketingProm对象", description="")
public class SdMarketingProm extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "营销活动加盟商分配表主键")
    @TableField("MARKETING_ID")
    private Integer marketingId;

    @ApiModelProperty(value = "成分")
    @TableField("COMPONENT_ID")
    private String componentId;

    @ApiModelProperty(value = "促销名称")
    @TableField("NAME")
    private String name;

    @ApiModelProperty(value = "类型")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "促销方式")
    @TableField("GSPVS_MODE")
    private String gspvsMode;

    @ApiModelProperty(value = "促销方式描述")
    @TableField("GSPVS_MODE_NAME")
    private String gspvsModeName;

    @ApiModelProperty(value = "促销类型")
    @TableField("GSPVS_TYPE")
    private String gspvsType;

    @ApiModelProperty(value = "促销类型描述")
    @TableField("GSPVS_TYPE_NAME")
    private String gspvsTypeName;

    @ApiModelProperty(value = "备注1")
    @TableField("REMARKS1")
    private String remarks1;

    @ApiModelProperty(value = "备注2")
    @TableField("REMARKS2")
    private String remarks2;

    @ApiModelProperty(value = "效期天数")
    @TableField("GSPUS_VAILD")
    private String gspusVaild;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPUS_QTY1")
    private String gspusQty1;

    @ApiModelProperty(value = "生成促销价1")
    @TableField("GSPUS_PRC1")
    private BigDecimal gspusPrc1;

    @ApiModelProperty(value = "生成促销折扣1")
    @TableField("GSPUS_REBATE1")
    private String gspusRebate1;

    @ApiModelProperty(value = "达到数量2")
    @TableField("GSPUS_QTY2")
    private String gspusQty2;

    @ApiModelProperty(value = "生成促销价2")
    @TableField("GSPUS_PRC2")
    private BigDecimal gspusPrc2;

    @ApiModelProperty(value = "生成促销折扣2")
    @TableField("GSPUS_REBATE2")
    private String gspusRebate2;

    @ApiModelProperty(value = "达到数量3")
    @TableField("GSPUS_QTY3")
    private String gspusQty3;

    @ApiModelProperty(value = "生成促销价3")
    @TableField("GSPUS_PRC3")
    private BigDecimal gspusPrc3;

    @ApiModelProperty(value = "生成促销折扣3")
    @TableField("GSPUS_REBATE3")
    private String gspusRebate3;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPUS_MEM_FLAG")
    private String gspusMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPUS_INTE_FLAG")
    private String gspusInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPUS_INTE_RATE")
    private String gspusInteRate;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPSC_SERIES_ID")
    private String gspscSeriesId;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPSC_MEM_FLAG")
    private String gspscMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPSC_INTE_FLAG")
    private String gspscInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPSC_INTE_RATE")
    private String gspscInteRate;

    @ApiModelProperty(value = "行号")
    @TableField("GSPSS_SERIAL")
    private String gspssSerial;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPSS_SERIES_ID")
    private String gspssSeriesId;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPSS_REACH_QTY1")
    private String gspssReachQty1;

    @ApiModelProperty(value = "达到金额1")
    @TableField("GSPSS_REACH_AMT1")
    private BigDecimal gspssReachAmt1;

    @ApiModelProperty(value = "结果减额1")
    @TableField("GSPSS_RESULT_AMT1")
    private BigDecimal gspssResultAmt1;

    @ApiModelProperty(value = "结果折扣1")
    @TableField("GSPSS_RESULT_REBATE1")
    private String gspssResultRebate1;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPGC_SERIES_ID")
    private String gspgcSeriesId;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPGC_MEM_FLAG")
    private String gspgcMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPGC_INTE_FLAG")
    private String gspgcInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPGC_INTE_RATE")
    private String gspgcInteRate;

    @ApiModelProperty(value = "行号")
    @TableField("GSPGS_SERIAL")
    private String gspgsSerial;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPGS_SERIES_PRO_ID")
    private String gspgsSeriesProId;

    @ApiModelProperty(value = "达到数量1")
    @TableField("GSPGS_REACH_QTY1")
    private String gspgsReachQty1;

    @ApiModelProperty(value = "达到金额1")
    @TableField("GSPGS_REACH_AMT1")
    private BigDecimal gspgsReachAmt1;

    @ApiModelProperty(value = "赠送数量1")
    @TableField("GSPGS_RESULT_QTY1")
    private String gspgsResultQty1;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPGR_SERIES_ID")
    private String gspgrSeriesId;

    @ApiModelProperty(value = "赠品单品价格")
    @TableField("GSPGR_GIFT_PRC")
    private BigDecimal gspgrGiftPrc;

    @ApiModelProperty(value = "赠品单品折扣")
    @TableField("GSPGR_GIFT_REBATE")
    private String gspgrGiftRebate;

    @ApiModelProperty(value = "数量")
    @TableField("GSPAC_QTY")
    private String gspacQty;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPAC_SERIES_ID")
    private String gspacSeriesId;

    @ApiModelProperty(value = "是否会员")
    @TableField("GSPAC_MEM_FLAG")
    private String gspacMemFlag;

    @ApiModelProperty(value = "是否积分")
    @TableField("GSPAC_INTE_FLAG")
    private String gspacInteFlag;

    @ApiModelProperty(value = "积分倍率")
    @TableField("GSPAC_INTE_RATE")
    private String gspacInteRate;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPAR_SERIES_ID")
    private String gsparSeriesId;

    @ApiModelProperty(value = "赠品单品价格")
    @TableField("GSPAR_GIFT_PRC")
    private BigDecimal gsparGiftPrc;

    @ApiModelProperty(value = "赠品单品折扣")
    @TableField("GSPAR_GIFT_REBATE")
    private String gsparGiftRebate;

    @ApiModelProperty(value = "行号")
    @TableField("GSPAS_SERIAL")
    private String gspasSerial;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPAS_SERIES_ID")
    private String gspasSeriesId;

    @ApiModelProperty(value = "组合金额")
    @TableField("GSPAS_AMT")
    private BigDecimal gspasAmt;

    @ApiModelProperty(value = "组合折扣")
    @TableField("GSPAS_REBATE")
    private String gspasRebate;

    @ApiModelProperty(value = "赠送数量")
    @TableField("GSPAS_QTY")
    private String gspasQty;

    @ApiModelProperty(value = "促销类型名")
    @TableField("TYPE_NAME")
    private String typeName;

    @ApiModelProperty(value = "阶梯")
    @TableField("GSPH_PART")
    private String gsphPart;


}
