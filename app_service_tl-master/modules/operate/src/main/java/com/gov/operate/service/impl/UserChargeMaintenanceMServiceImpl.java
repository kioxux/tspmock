package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.UserChargeMaintenanceM;
import com.gov.operate.mapper.UserChargeMaintenanceMMapper;
import com.gov.operate.service.IUserChargeMaintenanceMService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Service
public class UserChargeMaintenanceMServiceImpl extends ServiceImpl<UserChargeMaintenanceMMapper, UserChargeMaintenanceM> implements IUserChargeMaintenanceMService {

}
