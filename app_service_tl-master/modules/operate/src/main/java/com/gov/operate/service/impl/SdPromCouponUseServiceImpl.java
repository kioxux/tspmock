package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromCouponUse;
import com.gov.operate.mapper.SdPromCouponUseMapper;
import com.gov.operate.service.ISdPromCouponUseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Service
public class SdPromCouponUseServiceImpl extends ServiceImpl<SdPromCouponUseMapper, SdPromCouponUse> implements ISdPromCouponUseService {

}
