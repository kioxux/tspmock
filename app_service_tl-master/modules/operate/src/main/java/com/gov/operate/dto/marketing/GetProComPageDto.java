package com.gov.operate.dto.marketing;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:57 2021/8/13
 * @Description：查询成分信息入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GetProComPageDto {

    private List<String> queryStringList;
    private String queryString;
    @NotNull(message = "分页参数不能为空")
    private Integer pageNum;
    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;
}
