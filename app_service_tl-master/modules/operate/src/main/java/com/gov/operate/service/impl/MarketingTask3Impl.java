package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.GetWorkflowInData;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
@Slf4j
@Service
public class MarketingTask3Impl extends ServiceImpl<SdMarketingMapper, SdMarketing> implements IMarketingTask3Service {

    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingAnalysisMapper sdMarketingAnalysisMapper;
    @Resource
    private SdPromVariableTranMapper sdPromVariableTranMapper;
    @Resource
    private SdPromUnitarySetMapper sdPromUnitarySetMapper;
    @Resource
    private SdPromSeriesCondsMapper sdPromSeriesCondsMapper;
    @Resource
    private SdPromGiftCondsMapper sdPromGiftCondsMapper;
    @Resource
    private SdPromGiftResultMapper sdPromGiftResultMapper;
    @Resource
    private SdPromCouponGrantMapper sdPromCouponGrantMapper;
    @Resource
    private SdPromCouponUseMapper sdPromCouponUseMapper;
    @Resource
    private SdPromHyrPriceMapper sdPromHyrPriceMapper;
    @Resource
    private SdPromHySetMapper sdPromHySetMapper;
    @Resource
    private SdPromAssoCondsMapper sdPromAssoCondsMapper;
    @Resource
    private SdPromAssoResultMapper sdPromAssoResultMapper;
    @Resource
    private AuthFeign authFeign;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private ISdMarketingSearchService sdMarketingSearchService;
    @Resource
    private ISdPromVariableTranService sdPromVariableTranService;
    @Resource
    private CustomMultiThreadingService customMultiThreadingService;
    @Resource
    private SdMarketingClientMapper sdMarketingClientMapper;
    @Resource
    private SdMarketingStoMapper sdMarketingStoMapper;
    @Resource
    private SdMarketingPromMapper sdMarketingPromMapper;
    @Resource
    private SdMarketingPrdMapper sdMarketingPrdMapper;
    @Resource
    private SdMarketingGiveawayMapper sdMarketingGiveawayMapper;
    @Resource
    private SdPromHeadMapper sdPromHeadMapper;
    @Resource
    private ISdPromHeadService iSdPromHeadService;
    @Resource
    private ISdPromUnitarySetService iSdPromUnitarySetService;
    @Resource
    private ISdPromSeriesSetService iSdPromSeriesSetService;
    @Resource
    private ISdPromSeriesCondsService iSdPromSeriesCondsService;
    @Resource
    private ISdPromGiftCondsService iSdPromGiftCondsService;
    @Resource
    private ISdPromGiftSetService iSdPromGiftSetService;
    @Resource
    private ISdPromGiftResultService iSdPromGiftResultService;
    @Resource
    private ISdPromAssoCondsService iSdPromAssoCondsService;
    @Resource
    private ISdPromAssoSetService iSdPromAssoSetService;
    @Resource
    private ISdPromAssoResultService iSdPromAssoResultService;
    @Resource
    private IFranchiseeService franchiseeService;


    @Override
    public List<StoreVo> getCurrentStoreList() {
        // 获取当前人加盟商
        TokenUser user = commonService.getLoginInfo();
        // 当前人有哪些门店可以查询
        return commonService.getCurrentUserStores(user.getClient(), user.getUserId());
    }

    /**
     * 营销任务列表
     *
     * @param vo
     * @return
     */
    @Override
    public IPage<GetGsmTaskListDTO> getGsmTaskList(GsmTaskListVO vo) {
        Page<GetGsmTaskListDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        // 获取当前人加盟商
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        if (vo.getStatus() == "0") {
            vo.setStogCode(Constants.EMPTY_STRING);
            vo.setGsmStore(Constants.EMPTY_STRING);
        }

        return baseMapper.selectGsmTaskPage(page, vo);
    }

    /**
     * 营销任务详情
     */
    @Override
    public MarketingTaskVO getGsmTaskDetail(String client, String gsmMarketid, String gsmStore) {
        MarketingTaskVO marketingTaskVO = new MarketingTaskVO();
        // 查询活动主表
        QueryWrapper<SdMarketing> query = new QueryWrapper<SdMarketing>()
                .eq("CLIENT", client)
                .eq("GSM_MARKETID", gsmMarketid)
                .eq("GSM_STORE", gsmStore)
                .eq("GSM_DELETE_FLAG", "0"); // 删除标记 = 未删除
        SdMarketing sdMarketing = baseMapper.selectOne(query);
        if (sdMarketing == null) {
            throw new CustomResultException("活动不存在");
        }
        BeanUtils.copyProperties(sdMarketing, marketingTaskVO);

        // 折扣方式
        List<PromDTO> promList = sdPromVariableTranService.getPromList(gsmMarketid);
        if (!CollectionUtils.isEmpty(promList)) {
            for (PromDTO promDTO : promList) {
                promDTO.setProCount(0);
                if (StringUtils.isNotBlank(promDTO.getId())) {
                    // 促销类型
                    List<String> types = sdPromVariableTranMapper.selectTypes(client, promDTO.getId(), PromDTO.PROM_HEADER);
                    // 促销类型非空
                    if (!CollectionUtils.isEmpty(types)) {
                        // 促销类型
                        String type = commonService.getPromType(types);
                        // 单品促销
                        if (PromDTO.PROM_SINGLE.equals(type)) {
                            QueryWrapper<SdPromUnitarySet> sdPromUnitarySetQueryWrapper = new QueryWrapper();
                            sdPromUnitarySetQueryWrapper.eq("CLIENT", client);
                            sdPromUnitarySetQueryWrapper.eq("GSPUS_VOUCHER_ID", promDTO.getId());
                            Integer count = sdPromUnitarySetMapper.selectCount(sdPromUnitarySetQueryWrapper);
                            promDTO.setProCount(count);
                        }
                        // 系列促销
                        if (PromDTO.PROM_SERIES.equals(type)) {
                            Integer count = sdPromSeriesCondsMapper.selectCount(new QueryWrapper<SdPromSeriesConds>()
                                    .eq("CLIENT", client)
                                    .eq("GSPSC_VOUCHER_ID", promDTO.getId()));
                            promDTO.setProCount(count);
                        }
                        // 赠品促销
                        if (PromDTO.PROM_GIFT.equals(type)) {
                            // 商品数量
                            Integer count = sdPromGiftCondsMapper.selectCount(new QueryWrapper<SdPromGiftConds>()
                                    .eq("CLIENT", client)
                                    .eq("GSPGC_VOUCHER_ID", promDTO.getId()));
                            promDTO.setProCount(count);
                            // 赠品数量
                            Integer countGift = sdPromGiftResultMapper.selectCount(new QueryWrapper<SdPromGiftResult>()
                                    .eq("CLIENT", client)
                                    .eq("GSPGR_VOUCHER_ID", promDTO.getId()));
                            promDTO.setGiftCount(countGift);
                        }
                        // 电子促销
                        if (PromDTO.PROM_COUPON.equals(type)) {
                            // 商品数量
                            Integer count = sdPromCouponUseMapper.selectCount(new QueryWrapper<SdPromCouponUse>()
                                    .eq("CLIENT", client)
                                    .eq("GSPCU_VOUCHER_ID", promDTO.getId()));
                            promDTO.setProCount(count);
                            // 赠品数量
                            Integer countGift = sdPromCouponGrantMapper.selectCount(new QueryWrapper<SdPromCouponGrant>()
                                    .eq("CLIENT", client)
                                    .eq("GSPCG_VOUCHER_ID", promDTO.getId()));
                            promDTO.setGiftCount(countGift);
                        }
                        // 会员促销
                        if (PromDTO.PROM_HY.equals(type)) {
                            // 商品数量
                            Integer count = sdPromHyrPriceMapper.selectCount(new QueryWrapper<SdPromHyrPrice>()
                                    .eq("CLIENT", client)
                                    .eq("GSPHP_VOUCHER_ID", promDTO.getId()));
                            if (count == 0) {
                                count = sdPromHySetMapper.selectCount(new QueryWrapper<SdPromHySet>()
                                        .eq("CLIENT", client)
                                        .eq("GSPHS_VOUCHER_ID", promDTO.getId()));
                            }
                            promDTO.setProCount(count);
                        }
                        // 组合促销
                        if (PromDTO.PROM_COMBIN.equals(type)) {
                            // 活动商品数量
                            QueryWrapper<SdPromAssoConds> sdPromAssoCondsQueryWrapper = new QueryWrapper();
                            sdPromAssoCondsQueryWrapper.eq("CLIENT", client);
                            sdPromAssoCondsQueryWrapper.eq("GSPAC_VOUCHER_ID", promDTO.getId());
                            Integer count = sdPromAssoCondsMapper.selectCount(sdPromAssoCondsQueryWrapper);
                            promDTO.setProCount(count);
                            // 赠品数量
                            QueryWrapper<SdPromAssoResult> sdPromAssoResultQueryWrapper = new QueryWrapper<>();
                            sdPromAssoResultQueryWrapper.eq("CLIENT", client);
                            sdPromAssoResultQueryWrapper.eq("GSPAR_VOUCHER_ID", promDTO.getId());
                            Integer giftCount = sdPromAssoResultMapper.selectCount(sdPromAssoResultQueryWrapper);
                            promDTO.setGiftCount(giftCount);
                        }
                    }
                }
            }
        }
        marketingTaskVO.setDiscountList(promList);

//        if (StringUtils.isEmpty(marketingTaskVO.getGsmStore())) {
//            return marketingTaskVO;
//        }
        // 短信查询条件
        if (StringUtils.isNotBlank(marketingTaskVO.getGsmSmscondi())) {
            SdMarketingSearch smsSearch = sdMarketingSearchService.getById(marketingTaskVO.getGsmSmscondi());
            if (!ObjectUtils.isEmpty(smsSearch)) {
                marketingTaskVO.setGsmSmscondiJson(smsSearch.getGsmsDetail());
            }
        }

        // 电话查询条件
        if (StringUtils.isNotBlank(marketingTaskVO.getGsmTelcondi())) {
            SdMarketingSearch telSearch = sdMarketingSearchService.getById(marketingTaskVO.getGsmTelcondi());
            if (!ObjectUtils.isEmpty(telSearch)) {
                marketingTaskVO.setGsmTelcondiJson(telSearch.getGsmsDetail());
            }
        }
        // 工作流数据
        if (StringUtils.isNotBlank(sdMarketing.getGsmFlowNo())) {
            GetWorkflowInData getWorkflowInData = new GetWorkflowInData();
            TokenUser user = commonService.getLoginInfo();
            getWorkflowInData.setClient(client);
            getWorkflowInData.setUserId(user.getUserId());
            getWorkflowInData.setWfCode(sdMarketing.getGsmFlowNo());
            JsonResult jsonResult = authFeign.selectOneApp(getWorkflowInData);
            if (jsonResult.getCode() == 0) {
                marketingTaskVO.setWfJsonResult(jsonResult);
            }
        }
        return marketingTaskVO;
    }

    /**
     * 选品列表
     *
     * @param productRequestVo
     * @return
     */
    @Override
    public IPage<ProductResponseVo> getProductList(ProductRequestVo productRequestVo) {
        TokenUser user = commonService.getLoginInfo();
        productRequestVo.setClient(user.getClient());
        Page<SdMarketing> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
        return baseMapper.selectProductList(page, productRequestVo);
    }

    /**
     * 营销任务立即执行/提交审批
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveGsmTask(GsmTaskSaveVo gsmTaskSaveVo) throws InstantiationException, IllegalAccessException {
        QueryWrapper<SdMarketing> query = new QueryWrapper<SdMarketing>()
                .eq("CLIENT", gsmTaskSaveVo.getClient())
                .eq("GSM_MARKETID", gsmTaskSaveVo.getGsmMarketid())
                .eq("GSM_STORE", Constants.EMPTY_STRING)
                .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode());
        SdMarketing mark = baseMapper.selectOne(query);
        // 数据校验
        if (ObjectUtils.isEmpty(mark)) {
            throw new CustomResultException("记录不存在");
        }
        if (OperateEnum.GsmImpl.IMMEDIATE_EXECUTION.getCode().equals(mark.getGsmImpl())) {
            throw new CustomResultException("活动已执行");
        }
        if (OperateEnum.GsmImpl.PENDING_APPROVAL.getCode().equals(mark.getGsmImpl())) {
            throw new CustomResultException("活动待审批");
        }
        if (gsmTaskSaveVo.getGsmTargetValue().compareTo(gsmTaskSaveVo.getGsmIndexValue()) < 0) {
            throw new CustomResultException("销售额活动值不能小于销售额标值");
        }
        if (!"1".equals(gsmTaskSaveVo.getGsmPricechoose()) && !"1".equals(gsmTaskSaveVo.getGsmStchoose())) {
            throw new CustomResultException("客单价或交易次数至少选择一个");
        }

        // 创建更新对象
        SdMarketing marketing = new SdMarketing();
        BeanUtils.copyProperties(gsmTaskSaveVo, marketing);
        SdMarketingSearch smsSearchExit1 = sdMarketingSearchService.getById(gsmTaskSaveVo.getGsmSmscondi());
        SdMarketingSearch telSearchExit1 = sdMarketingSearchService.getById(gsmTaskSaveVo.getGsmTelcondi());
        List<SdMarketingSearch> searchListMarket = new ArrayList<>();
        // 短信
        if (!ObjectUtils.isEmpty(smsSearchExit1)) {
            SdMarketingSearch smsSearch = new SdMarketingSearch();
            BeanUtils.copyProperties(smsSearchExit1, smsSearch);
            smsSearch.setGsmsId(UUIDUtil.getUUID());
            smsSearch.setGsmsStore("");
            smsSearch.setGsmsDetail(gsmTaskSaveVo.getGsmSmscondiJson());
            marketing.setGsmSmscondi(smsSearch.getGsmsId());
            searchListMarket.add(smsSearch);
        } else if (StringUtils.isNotBlank(gsmTaskSaveVo.getGsmSmscondiJson())) {
            SdMarketingSearch smsSearch = new SdMarketingSearch();
            smsSearch.setGsmsId(UUIDUtil.getUUID());
            smsSearch.setGsmsName(gsmTaskSaveVo.getGsmThename());
            smsSearch.setGsmsDetail(gsmTaskSaveVo.getGsmSmscondiJson());
            smsSearch.setGsmsStore("");
            smsSearch.setGsmsNormal("0");
            smsSearch.setGsmsDeleteFlag("0");
            smsSearch.setGsmsType(1);
            marketing.setGsmSmscondi(smsSearch.getGsmsId());
            searchListMarket.add(smsSearch);
        }
        // 电话
        if (!ObjectUtils.isEmpty(telSearchExit1)) {
            SdMarketingSearch telSearch = new SdMarketingSearch();
            BeanUtils.copyProperties(telSearchExit1, telSearch);
            telSearch.setGsmsId(UUIDUtil.getUUID());
            telSearch.setGsmsStore("");
            telSearch.setGsmsDetail(gsmTaskSaveVo.getGsmTelcondiJson());
            marketing.setGsmTelcondi(telSearch.getGsmsId());
            searchListMarket.add(telSearch);
        } else if (StringUtils.isNotBlank(gsmTaskSaveVo.getGsmTelcondiJson())) {
            SdMarketingSearch telSearch = new SdMarketingSearch();
            telSearch.setGsmsId(UUIDUtil.getUUID());
            telSearch.setGsmsName(gsmTaskSaveVo.getGsmThename());
            telSearch.setGsmsDetail(gsmTaskSaveVo.getGsmTelcondiJson());
            telSearch.setGsmsStore("");
            telSearch.setGsmsNormal("0");
            telSearch.setGsmsDeleteFlag("0");
            telSearch.setGsmsType(2);
            marketing.setGsmTelcondi(telSearch.getGsmsId());
            searchListMarket.add(telSearch);
        }
        sdMarketingSearchService.saveBatch(searchListMarket);

        // 1,立即执行
        if ("1".equals(gsmTaskSaveVo.getSaveType())) {
            marketing.setGsmImpl(OperateEnum.GsmImpl.IMMEDIATE_EXECUTION.getCode());
            // (1)主数据更新
            baseMapper.update(marketing, query);
            // (2)分配到加盟商下所有门店
            SdMarketing marketingExit = this.getOne(new QueryWrapper<SdMarketing>()
                    .eq("CLIENT", gsmTaskSaveVo.getClient())
                    .eq("GSM_MARKETID", gsmTaskSaveVo.getGsmMarketid())
                    .eq("GSM_STORE", Constants.EMPTY_STRING)
                    .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode()));
            List<StoreData> storeList = storeDataMapper.getStoreList(gsmTaskSaveVo.getClient());
            if (CollectionUtils.isEmpty(storeList)) {
                return ResultUtil.success();
            }

            List<SdMarketing> list = new ArrayList<>();
            List<SdMarketingSearch> searchList = new ArrayList<>();
            // (3)短信电话查询条件
            SdMarketingSearch smsSearchExit = sdMarketingSearchService.getById(marketingExit.getGsmSmscondi());
            SdMarketingSearch telSearchExit = sdMarketingSearchService.getById(marketingExit.getGsmTelcondi());
            storeList.forEach(store -> {
                SdMarketing marketingStore = new SdMarketing();
                list.add(marketingStore);
                BeanUtils.copyProperties(marketingExit, marketingStore);
                marketingStore.setGsmStore(store.getStoCode());
                // 另存常用查询/短信
                if (!ObjectUtils.isEmpty(smsSearchExit)) {
                    SdMarketingSearch smsSearch = new SdMarketingSearch();
                    searchList.add(smsSearch);
                    BeanUtils.copyProperties(smsSearchExit, smsSearch);
                    smsSearch.setGsmsId(UUIDUtil.getUUID());
                    smsSearch.setGsmsStore(store.getStoCode());
                    marketingStore.setGsmSmscondi(smsSearch.getGsmsId());
                }
                // 另存常用查询/电话
                if (!ObjectUtils.isEmpty(telSearchExit)) {
                    SdMarketingSearch telSearch = new SdMarketingSearch();
                    searchList.add(telSearch);
                    BeanUtils.copyProperties(telSearchExit, telSearch);
                    telSearch.setGsmsId(UUIDUtil.getUUID());
                    telSearch.setGsmsStore(store.getStoCode());
                    marketingStore.setGsmTelcondi(telSearch.getGsmsId());
                }
            });
            // (4)选品/促销
            this.saveProm(gsmTaskSaveVo.getGsmMarketid(), gsmTaskSaveVo.getClient(), gsmTaskSaveVo.getDiscountList(), gsmTaskSaveVo.getProductList());
            // 门店数据保存
            this.saveBatch(list);
            // 短信/电话条件保存
            sdMarketingSearchService.saveBatch(searchList);
            // 根据条件发送短信
//            customMultiThreadingService.sendMarketTaskSms(storeList, gsmTaskSaveVo);
            return ResultUtil.success();
        }
        // 2,提交审批
        if ("2".equals(gsmTaskSaveVo.getSaveType())) {
            // 创建工作流
            // 初始编码
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
            marketing.setGsmFlowNo(flowNo);
            // 更新为审批中
            marketing.setGsmImpl(OperateEnum.GsmImpl.PENDING_APPROVAL.getCode());
            createMarketingWorkflow(marketing);
            baseMapper.update(marketing, query);
            // 选品/促销
            this.saveProm(gsmTaskSaveVo.getGsmMarketid(), gsmTaskSaveVo.getClient(), gsmTaskSaveVo.getDiscountList(), gsmTaskSaveVo.getProductList());

            return ResultUtil.success();
        }
        // 3,暂存
        /**
         if ("3".equals(gsmTaskSaveVo.getSaveType())) {
         baseMapper.update(marketing, query);
         return ResultUtil.success();
         }
         */
        throw new CustomResultException("不存在该保存类型");
    }

    /**
     * 营销任务删除
     *
     * @param marketing
     * @return
     */
    @Override
    public Result deleteMarketTask(SdMarketing marketing) {
        if (StringUtils.isEmpty(marketing.getClient())) {
            throw new CustomResultException("加盟商不能为空");
        }
        if (StringUtils.isEmpty(marketing.getGsmMarketid())) {
            throw new CustomResultException("营销活动ID不能为空");
        }

        QueryWrapper<SdMarketing> query = new QueryWrapper<SdMarketing>()
                .eq("CLIENT", marketing.getClient())
                .eq("GSM_MARKETID", marketing.getGsmMarketid())
                .eq("GSM_STORE", Constants.EMPTY_STRING)
                .eq("GSM_DELETE_FLAG", "0"); // 删除标记 = 未删除
        SdMarketing mark = baseMapper.selectOne(query);
        if (ObjectUtils.isEmpty(mark)) {
            throw new CustomResultException("记录不存在");
        }
        if (OperateEnum.GsmImpl.IMMEDIATE_EXECUTION.getCode().equals(mark.getGsmImpl())) {
            throw new CustomResultException("活动已执行");
        }
        if (OperateEnum.GsmImpl.PENDING_APPROVAL.getCode().equals(mark.getGsmImpl())) {
            throw new CustomResultException("活动待审批");
        }
        SdMarketing mk = new SdMarketing();
        mk.setGsmDeleteFlag("1"); // 设置为删除
        baseMapper.update(mk, query);
        return ResultUtil.success();
    }

    @Override
    public Result getActiveEvalInfo(ActiveEvalVO activeEvalVO) {
        ActiveEvalResultVO activeEvalResultVO = new ActiveEvalResultVO();
        SdMarketingAnalysis sdMarketingAnalysis = null;
        // 如果 门店组 门店  商品组  商品编码 均为空时 查询所有门店汇总信息
        if (StringUtils.isEmpty(activeEvalVO.getStogCode())
                && StringUtils.isEmpty(activeEvalVO.getStoCode())
                && StringUtils.isEmpty(activeEvalVO.getGsyGroup())
                && StringUtils.isEmpty(activeEvalVO.getProSelfCode())) {
            sdMarketingAnalysis = sdMarketingAnalysisMapper.selectAllAnalysisInfo(activeEvalVO);
        } else {
            sdMarketingAnalysis = sdMarketingAnalysisMapper.selectAnalysisInfoByParam(activeEvalVO);
        }

        String activeX = "活动预估";  // X 轴显示
        if ("1".equals(activeEvalVO.getMkAntype())) {
            activeX = "活动实际";
        }

        if (sdMarketingAnalysis != null) {
            /**
             * 销售额
             */
            List<BarChartVO> salesList = new ArrayList<>();

            BarChartVO salesInfo = new BarChartVO();
            salesInfo.setXaxis(activeX);
            salesInfo.setYaxis(sdMarketingAnalysis.getMkSamt());
            salesList.add(salesInfo);

            BarChartVO salesInfo1 = new BarChartVO();
            salesInfo1.setXaxis("去年");
            salesInfo1.setYaxis(sdMarketingAnalysis.getMkSamt1());
            salesList.add(salesInfo1);

            activeEvalResultVO.setSalesList(salesList);

            /**
             * 毛利额
             */
            List<BarChartVO> grossProfitList = new ArrayList<>();

            BarChartVO grossProfitInfo = new BarChartVO();
            grossProfitInfo.setXaxis(activeX);
            grossProfitInfo.setYaxis(sdMarketingAnalysis.getMkGamt());
            grossProfitList.add(grossProfitInfo);

            BarChartVO grossProfitInfo1 = new BarChartVO();
            grossProfitInfo1.setXaxis("去年");
            grossProfitInfo1.setYaxis(sdMarketingAnalysis.getMkGamt1());
            grossProfitList.add(grossProfitInfo1);

            activeEvalResultVO.setGrossProfitList(grossProfitList);


            /**
             * 毛利率
             */
            List<BarChartVO> profitList = new ArrayList<>();

            BarChartVO profitInfo = new BarChartVO();
            profitInfo.setXaxis(activeX);
            profitInfo.setYaxis(sdMarketingAnalysis.getMkGm());
            profitList.add(profitInfo);

            BarChartVO profitInfo1 = new BarChartVO();
            profitInfo1.setXaxis("去年");
            profitInfo1.setYaxis(sdMarketingAnalysis.getMkGm1());
            profitList.add(profitInfo1);

            activeEvalResultVO.setProfitList(profitList);


            /**
             * 日均交易额
             */
            List<BarChartVO> avgList = new ArrayList<>();

            BarChartVO avgInfo = new BarChartVO();
            avgInfo.setXaxis(activeX);
            avgInfo.setYaxis(sdMarketingAnalysis.getMkEamt());
            avgList.add(avgInfo);

            BarChartVO avgInfo1 = new BarChartVO();
            avgInfo1.setXaxis("去年");
            avgInfo1.setYaxis(sdMarketingAnalysis.getMkEamt1());
            avgList.add(avgInfo1);

            activeEvalResultVO.setAvgList(avgList);


            /**
             * 交易次数
             */
            List<BarChartVO> stincList = new ArrayList<>();

            BarChartVO stincInfo = new BarChartVO();
            stincInfo.setXaxis(activeX);
            stincInfo.setYaxis(sdMarketingAnalysis.getMktans());
            stincList.add(stincInfo);

            BarChartVO stincInfo1 = new BarChartVO();
            stincInfo1.setXaxis("去年");
            stincInfo1.setYaxis(sdMarketingAnalysis.getMktans1());
            stincList.add(stincInfo1);

            activeEvalResultVO.setStincList(stincList);

            /**
             * 客单价
             */
            List<BarChartVO> priceincist = new ArrayList<>();

            BarChartVO priceInfo = new BarChartVO();
            priceInfo.setXaxis(activeX);
            priceInfo.setYaxis(sdMarketingAnalysis.getMkPrice());
            priceincist.add(priceInfo);

            BarChartVO priceInfo1 = new BarChartVO();
            priceInfo1.setXaxis("去年");
            priceInfo1.setYaxis(sdMarketingAnalysis.getMkPrice1());
            priceincist.add(priceInfo1);

            activeEvalResultVO.setPriceincist(priceincist);
        } else {
            /**
             * 销售额
             */
            List<BarChartVO> salesList = new ArrayList<>();

            BarChartVO salesInfo = new BarChartVO();
            salesInfo.setXaxis(activeX);
            salesInfo.setYaxis(BigDecimal.ZERO);
            salesList.add(salesInfo);

            BarChartVO salesInfo1 = new BarChartVO();
            salesInfo1.setXaxis("去年");
            salesInfo1.setYaxis(BigDecimal.ZERO);
            salesList.add(salesInfo1);

            activeEvalResultVO.setSalesList(salesList);

            /**
             * 毛利额
             */
            List<BarChartVO> grossProfitList = new ArrayList<>();

            BarChartVO grossProfitInfo = new BarChartVO();
            grossProfitInfo.setXaxis(activeX);
            grossProfitInfo.setYaxis(BigDecimal.ZERO);
            grossProfitList.add(grossProfitInfo);

            BarChartVO grossProfitInfo1 = new BarChartVO();
            grossProfitInfo1.setXaxis("去年");
            grossProfitInfo1.setYaxis(BigDecimal.ZERO);
            grossProfitList.add(grossProfitInfo1);

            activeEvalResultVO.setGrossProfitList(grossProfitList);


            /**
             * 毛利率
             */
            List<BarChartVO> profitList = new ArrayList<>();

            BarChartVO profitInfo = new BarChartVO();
            profitInfo.setXaxis(activeX);
            profitInfo.setYaxis(BigDecimal.ZERO);
            profitList.add(profitInfo);

            BarChartVO profitInfo1 = new BarChartVO();
            profitInfo1.setXaxis("去年");
            profitInfo1.setYaxis(BigDecimal.ZERO);
            profitList.add(profitInfo1);

            activeEvalResultVO.setProfitList(profitList);


            /**
             * 日均交易额
             */
            List<BarChartVO> avgList = new ArrayList<>();

            BarChartVO avgInfo = new BarChartVO();
            avgInfo.setXaxis(activeX);
            avgInfo.setYaxis(BigDecimal.ZERO);
            avgList.add(avgInfo);

            BarChartVO avgInfo1 = new BarChartVO();
            avgInfo1.setXaxis("去年");
            avgInfo1.setYaxis(BigDecimal.ZERO);
            avgList.add(avgInfo1);

            activeEvalResultVO.setAvgList(avgList);


            /**
             * 交易次数
             */
            List<BarChartVO> stincList = new ArrayList<>();

            BarChartVO stincInfo = new BarChartVO();
            stincInfo.setXaxis(activeX);
            stincInfo.setYaxis(BigDecimal.ZERO);
            stincList.add(stincInfo);

            BarChartVO stincInfo1 = new BarChartVO();
            stincInfo1.setXaxis("去年");
            stincInfo1.setYaxis(BigDecimal.ZERO);
            stincList.add(stincInfo1);

            activeEvalResultVO.setStincList(stincList);

            /**
             * 客单价
             */
            List<BarChartVO> priceincist = new ArrayList<>();

            BarChartVO priceInfo = new BarChartVO();
            priceInfo.setXaxis(activeX);
            priceInfo.setYaxis(BigDecimal.ZERO);
            priceincist.add(priceInfo);

            BarChartVO priceInfo1 = new BarChartVO();
            priceInfo1.setXaxis("去年");
            priceInfo1.setYaxis(BigDecimal.ZERO);
            priceincist.add(priceInfo1);

            activeEvalResultVO.setPriceincist(priceincist);
        }


        return ResultUtil.success(activeEvalResultVO);
    }

    /**
     * 创建工作流
     *
     * @param marketing
     */
    public void createMarketingWorkflow(SdMarketing marketing) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", "GAIA_WF_029");  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append(marketing.getGsmThename()); // 营销主题名称
        builder.append("活动时间");

        LocalDate startDate = DateUtils.parseLocalDate(marketing.getGsmStartd(), "yyyyMMdd");
        builder.append(startDate.getMonthValue());  //活动月
        builder.append("月");
        builder.append(startDate.getDayOfMonth()); //活动日
        builder.append("日");
        builder.append("-");
        LocalDate endDate = DateUtils.parseLocalDate(marketing.getGsmEndd(), "yyyyMMdd");
        builder.append(endDate.getMonthValue());  //活动月
        builder.append("月");
        builder.append(endDate.getMonthValue());  //活动月
        builder.append("日");

        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfDescription", builder.toString());  // 描述
        param.put("wfOrder", marketing.getGsmFlowNo());   // 业务单号

        List<MarketingApproveDto> marketingApproveDtoList = new ArrayList<>();

        MarketingApproveDto marketingApproveDto = new MarketingApproveDto();
        marketingApproveDto.setGsmIndexValue(marketing.getGsmIndexValue());  // 销售额标值
        marketingApproveDto.setGsmTargetValue(marketing.getGsmTargetValue()); // 销售额预估值
        if ("1".equals(marketing.getGsmPricechoose())) {  // 选择客单价
            marketingApproveDto.setGsmPriceinc(marketing.getGsmPriceinc());   //客单价
            marketingApproveDto.setGsmActPriceinc(marketing.getGsmActPriceinc());  //客单价预估
            marketingApproveDto.setGsmStinc(marketing.getGsmStinc());  // 交易次数标值
            marketingApproveDto.setGsmActStinc(marketing.getGsmPriceincSt());  // 客单价对应交易次数修正值
        } else {
            marketingApproveDto.setGsmStinc(marketing.getGsmStinc());   // 交易次数
            marketingApproveDto.setGsmActStinc(marketing.getGsmActStinc());
            marketingApproveDto.setGsmPriceinc(marketing.getGsmPriceinc());   //客单价
            marketingApproveDto.setGsmActPriceinc(marketing.getGsmStincPrice());  // 交易次数对应客单价修正值
        }
        // 添加到集合中
        marketingApproveDtoList.add(marketingApproveDto);
        param.put("wfDetail", marketingApproveDtoList); // 明细
        param.put("wfSite", marketing.getGsmStore()); // 门店
        log.info("营销活动流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        JsonResult result = authFeign.createWorkflow(param);
        if (result.getCode().intValue() != 0) {
            throw new CustomResultException(result.getMessage());
        }
        log.info("商品首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
    }

    /**
     * 选品/促销保存
     */
    private void saveProm(String gsph_marketid, String client, List<PromDTO> voucherList, List<PromDTO> proList) {
        try {
            commonService.saveProm(gsph_marketid, client, voucherList, proList);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new CustomResultException("选品保存失败");
        }
    }

    /**
     * 根据营销ID、促销单号 返回已选商品
     *
     * @param productRequestVo
     * @return
     */
    @Override
    public Result getSelectedProductList(ProductRequestVo productRequestVo) {
        // 促销类型
        List<String> types = sdPromVariableTranMapper.selectTypes(productRequestVo.getClient(), productRequestVo.getGsmPromo(), PromDTO.PROM_HEADER);
        // 促销类型为空
        if (CollectionUtils.isEmpty(types)) {
            return ResultUtil.success(new Page<ProductResponseVo>());
        }
        // 促销类型
        String type = commonService.getPromType(types);
        // 单品促销
        if (PromDTO.PROM_SINGLE.equals(type)) {
            Page<SdPromUnitarySet> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage = sdPromUnitarySetMapper.selectProductList(page, productRequestVo);
            return ResultUtil.success(ipage);
        }
        // 系列促销
        if (PromDTO.PROM_SERIES.equals(type)) {
            Page<SdPromSeriesConds> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage = sdPromSeriesCondsMapper.selectProductList(page, productRequestVo);
            return ResultUtil.success(ipage);
        }
        // 赠品促销
        if (PromDTO.PROM_GIFT.equals(type)) {
            Page<SdPromGiftConds> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage = sdPromGiftCondsMapper.selectProductList(page, productRequestVo);
            return ResultUtil.success(ipage);
        }
        // 电子促销
        if (PromDTO.PROM_COUPON.equals(type)) {
            Page<SdPromCouponUse> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage = sdPromCouponGrantMapper.selectProductList(page, productRequestVo);
            return ResultUtil.success(ipage);
        }
        // 会员
        if (PromDTO.PROM_HY.equals(type)) {
            Page<SdPromHyrPrice> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage = sdPromHyrPriceMapper.selectProductList(page, productRequestVo);
            if (ipage.getRecords().size() > 0) {
                return ResultUtil.success(ipage);
            }
            Page<SdPromHySet> page1 = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage1 = sdPromHySetMapper.selectProductList(page1, productRequestVo);
            return ResultUtil.success(ipage1);
        }

        // 组合商品
        if (PromDTO.PROM_COMBIN.equals(type)) {
            Page<SdPromAssoConds> page = new Page<>(productRequestVo.getPageNum(), productRequestVo.getPageSize());
            IPage<ProductResponseVo> ipage = sdPromAssoCondsMapper.selectProductList(page, productRequestVo);
            return ResultUtil.success(ipage);
        }
        return ResultUtil.success(new Page<ProductResponseVo>());
    }

    /**
     * 已选赠品
     *
     * @param productRequestVo
     * @return
     */
    @Override
    public Result selectGiftPorductList(ProductRequestVo productRequestVo) {
        // 促销类型
        List<String> types = sdPromVariableTranMapper.selectTypes(productRequestVo.getClient(), productRequestVo.getGsmPromo(), PromDTO.PROM_HEADER);
        // 促销类型为空
        if (CollectionUtils.isEmpty(types)) {
            return ResultUtil.success(new Page<ProductResponseVo>());
        }
        // 促销类型
        String type = commonService.getPromType(types);
        // 赠品系列
        if (PromDTO.PROM_GIFT.equals(type)) {
            List<ProductResponseVo> list = sdPromGiftCondsMapper.selectGiftProductList(productRequestVo);
            return ResultUtil.success(list);
        }
        // 电子券
        if (PromDTO.PROM_COUPON.equals(type)) {
            List<ProductResponseVo> list = sdPromCouponGrantMapper.selectGiftProductList(productRequestVo);
            return ResultUtil.success(list);
        }
        // 组合商品
        if (PromDTO.PROM_COMBIN.equals(type)) {
            List<ProductResponseVo> list = sdPromAssoCondsMapper.selectGiftProductList(productRequestVo);
            return ResultUtil.success(list);
        }
        return ResultUtil.success(new Page<ProductResponseVo>());
    }

    /**
     * 发送审批
     *
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approval(Integer id) {
        // 获取当前人加盟商
        TokenUser user = commonService.getLoginInfo();
        // 当前加盟商的营销数据
        QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper<>();
        sdMarketingClientQueryWrapper.eq("CLIENT", user.getClient());
        sdMarketingClientQueryWrapper.eq("ID", id);
        SdMarketingClient sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
        // 营销数据不存在
        if (sdMarketingClient == null) {
            throw new CustomResultException("营销数据不存在");
        }
        // 只有 修改中的数据 才可以提交发送审批
        if (!OperateEnum.MarketingImpl.CONSENT_EXECUTION.getCode().equals(sdMarketingClient.getGsmImpl())) {
            throw new CustomResultException("当前营销状态非[修改中]不能发送审批");
        }
        // 0:加盟商负责人/委托人 1:员工  (只有员工才可以提交发送审批)
        Integer legalPerson = getLegalPerson(user.getClient(), user.getUserId());
        if (0 == legalPerson) {
            throw new CustomResultException("当前用户为加盟商[负责人/委托人],不可执行[发送审批]操作");
        }
        // 状态 ： 审批中
        sdMarketingClient.setGsmImpl(OperateEnum.MarketingImpl.APPROVAL.getCode());
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        sdMarketingClient.setGsmFlowNo(flowNo);
        sdMarketingClientMapper.update(sdMarketingClient, sdMarketingClientQueryWrapper);
        // 工作流参数
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        param.put("token", token);
        param.put("wfDefineCode", "GAIA_WF_029");  // 类型
        param.put("wfTitle", "营销方案审批");
        param.put("wfOrder", flowNo);
        param.put("wfDescription", "营销方案审批,订单号：" + flowNo);  // 描述
        List<Map<String, String>> paramsList = new ArrayList<>();
        Map<String, String> paramsMap = new HashMap<>();
        // 营销主题
        paramsMap.put("gsmThename", sdMarketingClient.getGsmThename());
        // 计划开始时间
        paramsMap.put("gsmStartd", sdMarketingClient.getGsmStartd());
        // 计划结束时间
        paramsMap.put("gsmEndd", sdMarketingClient.getGsmEndd());
        // 目标销售额
        paramsMap.put("gsmTargetValue", sdMarketingClient.getGsmTargetValue() == null ? "" : sdMarketingClient.getGsmTargetValue().compareTo(BigDecimal.ZERO) == 0 ? "" : sdMarketingClient.getGsmTargetValue().toString());
        paramsList.add(paramsMap);
        param.put("wfDetail", paramsList);

        log.info("营销活动流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        JsonResult result = authFeign.createWorkflow(param);
        log.info("商品首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        if (result.getCode().intValue() != 0) {
            throw new CustomResultException(result.getMessage());
        }
    }

    /**
     * 立即执行
     *
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void execute(Integer id, String clientId, String flowNo, String status) {
        // 获取当前人加盟商
        String client = "";
        SdMarketingClient sdMarketingClient = null;
        if (id == null && StringUtils.isBlank(flowNo)) {
            throw new CustomResultException("参数为空");
        }
        // 审批
        if (StringUtils.isNotBlank(flowNo)) {
            if (StringUtils.isBlank(clientId)) {
                throw new CustomResultException("加盟商不能为空");
            }
            if (StringUtils.isBlank(status)) {
                throw new CustomResultException("审批状态不能为空");
            }
            client = clientId;
            // 待审批营销
            QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper<>();
            sdMarketingClientQueryWrapper.eq("CLIENT", client);
            sdMarketingClientQueryWrapper.eq("GSM_FLOW_NO", flowNo);
            sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
            // 营销数据不存在
            if (sdMarketingClient == null) {
                throw new CustomResultException("营销数据不存在");
            }
            if (!OperateEnum.MarketingImpl.APPROVAL.getCode().equals(sdMarketingClient.getGsmImpl())) {
                throw new CustomResultException("当前营销活动已审批");
            }
            // 审批结果
            if ("3".equals(status)) {
                // 审批状态 = 审批通过
                sdMarketingClient.setGsmImpl(OperateEnum.MarketingImpl.APPROVED.getCode());
                sdMarketingClient.setGsmImpltime(DateUtils.getCurrentDateTimeStrFull());
                sdMarketingClientMapper.update(sdMarketingClient, sdMarketingClientQueryWrapper);
            } else if ("1".equals(status)) {
                // 审批状态 = 审批驳回 (审批驳回后，营销的状态改为[修改中])
                sdMarketingClient.setGsmImpl(OperateEnum.MarketingImpl.CONSENT_EXECUTION.getCode());
                sdMarketingClient.setGsmImpltime(DateUtils.getCurrentDateTimeStrFull());
                sdMarketingClientMapper.update(sdMarketingClient, sdMarketingClientQueryWrapper);
                return;
            } else {
                return;
            }
        } else {// 非审批操作 (同意执行)
            TokenUser user = commonService.getLoginInfo();
            client = user.getClient();
            // 当前加盟商的营销数据
            QueryWrapper<SdMarketingClient> sdMarketingClientQueryWrapper = new QueryWrapper<>();
            sdMarketingClientQueryWrapper.eq("CLIENT", client);
            sdMarketingClientQueryWrapper.eq("ID", id);
            sdMarketingClient = sdMarketingClientMapper.selectOne(sdMarketingClientQueryWrapper);
            // 营销数据不存在
            if (sdMarketingClient == null) {
                throw new CustomResultException("营销数据不存在");
            }
            // 当前审批状态不是未审批
            if (!OperateEnum.MarketingImpl.NOTAPPROVED.getCode().equals(sdMarketingClient.getGsmImpl())) {
                throw new CustomResultException("当前营销状态非[待处理],不能立即执行");
            }
            // 0:加盟商负责人/委托人 1:员工  (只有负责人/委托人才可以同意执行)
            Integer legalPerson = getLegalPerson(user.getClient(), user.getUserId());
            if (1 == legalPerson) {
                throw new CustomResultException("当前用户为加盟商[员工],不可执行[同意执行]操作");
            }
            // 状态 ： 立即执行
            sdMarketingClient.setGsmImpl(OperateEnum.MarketingImpl.EXECUTE.getCode());
            sdMarketingClient.setGsmImpltime(DateUtils.getCurrentDateTimeStrFull());
            sdMarketingClientMapper.update(sdMarketingClient, sdMarketingClientQueryWrapper);
        }
        // 营销关联的门店
        QueryWrapper<SdMarketingSto> sdMarketingStoQueryWrapper = new QueryWrapper<>();
        sdMarketingStoQueryWrapper.eq("CLIENT", client);
        sdMarketingStoQueryWrapper.eq("MARKETING_ID", sdMarketingClient.getId());
        List<SdMarketingSto> sdMarketingStoList = sdMarketingStoMapper.selectList(sdMarketingStoQueryWrapper);
        if (CollectionUtils.isEmpty(sdMarketingStoList)) {
            return;
        }
        // 促销数据
        QueryWrapper<SdMarketingProm> sdMarketingPromQueryWrapper = new QueryWrapper<>();
        sdMarketingPromQueryWrapper.eq("CLIENT", client);
        sdMarketingPromQueryWrapper.eq("MARKETING_ID", sdMarketingClient.getId());
        List<SdMarketingProm> sdMarketingPromList = sdMarketingPromMapper.selectList(sdMarketingPromQueryWrapper);
        if (CollectionUtils.isEmpty(sdMarketingPromList)) {
            return;
        }
        // 促销主表
        List<SdPromHead> sdPromHeadList = new ArrayList<>();
        // 单品
        List<SdPromUnitarySet> sdPromUnitarySetList = new ArrayList<>();
        // 系列
        List<SdPromSeriesSet> sdPromSeriesSetList = new ArrayList<>();
        List<SdPromSeriesConds> sdPromSeriesCondsList = new ArrayList<>();
        // 赠品
        List<SdPromGiftConds> sdPromGiftCondsList = new ArrayList<>();
        List<SdPromGiftSet> sdPromGiftSetList = new ArrayList<>();
        List<SdPromGiftResult> sdPromGiftResultList = new ArrayList<>();
        // 组合
        List<SdPromAssoConds> sdPromAssoCondsList = new ArrayList<>();
        List<SdPromAssoSet> sdPromAssoSetList = new ArrayList<>();
        List<SdPromAssoResult> sdPromAssoResultList = new ArrayList<>();

        // 促销单号
        Integer maxVoucherId = sdPromHeadMapper.createVoucherId(client);
        // 回写单号
        List<SdMarketingPrd> sdMarketingPrdUpdateList = new ArrayList<>();
        // 生成促销 门店数据
        for (SdMarketingSto sdMarketingSto : sdMarketingStoList) {
            // 促销商品数据
            for (SdMarketingProm sdMarketingProm : sdMarketingPromList) {
                maxVoucherId++;
                // 促销单号
                String gsph_voucher_id = "CX" + DateUtils.getCurrentYearStr() + StringUtils.leftPad(maxVoucherId.toString(), 10, "0");
                // 促销主表
                SdPromHead sdPromHead = new SdPromHead();
                // 加盟商
                sdPromHead.setClient(client);
                // 门店
                sdPromHead.setGsphBrId(sdMarketingSto.getStoCode());
                // 促销单号
                sdPromHead.setGsphVoucherId(gsph_voucher_id);
                // 促销主题
                sdPromHead.setGsphTheme(sdMarketingClient.getGsmThename());
                // 活动名称
                sdPromHead.setGsphName(sdMarketingProm.getName());
                // 活动说明
                sdPromHead.setGsphRemarks(sdMarketingProm.getGspvsModeName());
                // 状态
                sdPromHead.setGsphStatus("1");
                // 起始日期
                sdPromHead.setGsphBeginDate(sdMarketingClient.getGsmStartd());
                // 结束日期
                sdPromHead.setGsphEndDate(sdMarketingClient.getGsmEndd());
                // 起始时间
                sdPromHead.setGsphBeginTime("000000");
                // 结束时间
                sdPromHead.setGsphEndTime("235959");
                // 促销类型
                sdPromHead.setGsphType(sdMarketingProm.getGspvsType());
                // 阶梯
                sdPromHead.setGsphPart("1");
                // 营销活动ID
                sdPromHead.setGsphMarketid(sdMarketingClient.getId());
                // 参数
                sdPromHead.setGsphPara1("repeat1");
                // 互斥
                sdPromHead.setGsphExclusion("exclusion4");
                sdPromHeadList.add(sdPromHead);

                // 活动商品
                QueryWrapper<SdMarketingPrd> sdMarketingPrdQueryWrapper = new QueryWrapper<>();
                sdMarketingPrdQueryWrapper.eq("CLIENT", client);
                sdMarketingPrdQueryWrapper.eq("MARKETING_ID", sdMarketingClient.getId());
                sdMarketingPrdQueryWrapper.eq("PROM_ID", sdMarketingProm.getId());
                List<SdMarketingPrd> sdMarketingPrdList = sdMarketingPrdMapper.selectList(sdMarketingPrdQueryWrapper);

                // 活动赠品
                QueryWrapper<SdMarketingGiveaway> sdMarketingGiveawayQueryWrapper = new QueryWrapper();
                sdMarketingGiveawayQueryWrapper.eq("CLIENT", client);
                sdMarketingGiveawayQueryWrapper.eq("MARKETING_ID", sdMarketingClient.getId());
                sdMarketingGiveawayQueryWrapper.eq("PROM_ID", sdMarketingProm.getId());
                List<SdMarketingGiveaway> sdMarketingGiveawayList = sdMarketingGiveawayMapper.selectList(sdMarketingGiveawayQueryWrapper);

                // 选品
                initProList(client, gsph_voucher_id, sdMarketingProm, sdMarketingPrdUpdateList, sdMarketingPrdList, sdMarketingGiveawayList,
                        sdPromUnitarySetList,
                        sdPromSeriesSetList, sdPromSeriesCondsList,
                        sdPromGiftCondsList, sdPromGiftSetList, sdPromGiftResultList,
                        sdPromAssoCondsList, sdPromAssoSetList, sdPromAssoResultList
                );
            }
        }
        // 促销主表
        if (!CollectionUtils.isEmpty(sdPromHeadList)) {
            iSdPromHeadService.saveBatch(sdPromHeadList);
        }
        // 单品商品
        if (!CollectionUtils.isEmpty(sdPromUnitarySetList)) {
            iSdPromUnitarySetService.saveBatch(sdPromUnitarySetList);
        }
        // 系列
        if (!CollectionUtils.isEmpty(sdPromSeriesSetList)) {
            iSdPromSeriesSetService.saveBatch(sdPromSeriesSetList);
        }
        if (!CollectionUtils.isEmpty(sdPromSeriesCondsList)) {
            iSdPromSeriesCondsService.saveBatch(sdPromSeriesCondsList);
        }
        // 赠品
        if (!CollectionUtils.isEmpty(sdPromGiftCondsList)) {
            iSdPromGiftCondsService.saveBatch(sdPromGiftCondsList);
        }
        if (!CollectionUtils.isEmpty(sdPromGiftSetList)) {
            iSdPromGiftSetService.saveBatch(sdPromGiftSetList);
        }
        if (!CollectionUtils.isEmpty(sdPromGiftResultList)) {
            iSdPromGiftResultService.saveBatch(sdPromGiftResultList);
        }
        // 组合
        if (!CollectionUtils.isEmpty(sdPromAssoCondsList)) {
            iSdPromAssoCondsService.saveBatch(sdPromAssoCondsList);
        }
        if (!CollectionUtils.isEmpty(sdPromAssoSetList)) {
            iSdPromAssoSetService.saveBatch(sdPromAssoSetList);
        }
        if (!CollectionUtils.isEmpty(sdPromAssoResultList)) {
            iSdPromAssoResultService.saveBatch(sdPromAssoResultList);
        }
        // 促销单号回写
        if (!CollectionUtils.isEmpty(sdMarketingPrdUpdateList)) {
            sdMarketingPrdMapper.resultVoucherId(sdMarketingPrdUpdateList);
        }
    }

    /**
     * 促销商品
     *
     * @param sdMarketingProm         促销
     * @param sdMarketingPrdList      活动商品
     * @param sdMarketingGiveawayList 活动赠品
     */
    private void initProList(String client, String voucher_id, SdMarketingProm sdMarketingProm, List<SdMarketingPrd> sdMarketingPrdUpdateList,
                             List<SdMarketingPrd> sdMarketingPrdList, List<SdMarketingGiveaway> sdMarketingGiveawayList,
                             List<SdPromUnitarySet> sdPromUnitarySetList,
                             List<SdPromSeriesSet> sdPromSeriesSetList, List<SdPromSeriesConds> sdPromSeriesCondsList,
                             List<SdPromGiftConds> sdPromGiftCondsList, List<SdPromGiftSet> sdPromGiftSetList, List<SdPromGiftResult> sdPromGiftResultList,
                             List<SdPromAssoConds> sdPromAssoCondsList, List<SdPromAssoSet> sdPromAssoSetList, List<SdPromAssoResult> sdPromAssoResultList) {
        // 系列
        if (OperateEnum.MarketingProm.SERIES.getCode().equals(sdMarketingProm.getType())) {
            SdPromSeriesSet sdPromSeriesSet = new SdPromSeriesSet();
            // 加盟商
            sdPromSeriesSet.setClient(client);
            // 单号
            sdPromSeriesSet.setGspssVoucherId(voucher_id);
            // 行号
            sdPromSeriesSet.setGspssSerial("1");
            // 系列编码
            sdPromSeriesSet.setGspssSeriesId(sdMarketingProm.getGspssSeriesId());
            // 达到数量1
            sdPromSeriesSet.setGspssReachQty1(sdMarketingProm.getGspssReachQty1());
            // 达到金额1
            sdPromSeriesSet.setGspssReachAmt1(sdMarketingProm.getGspssReachAmt1());
            // 结果减额1
            sdPromSeriesSet.setGspssResultAmt1(sdMarketingProm.getGspssResultAmt1());
            // 结果折扣1
            sdPromSeriesSet.setGspssResultRebate1(sdMarketingProm.getGspssResultRebate1());
            sdPromSeriesSetList.add(sdPromSeriesSet);
        }
        // 赠品
        if (OperateEnum.MarketingProm.GIFT.getCode().equals(sdMarketingProm.getType())) {
            SdPromGiftSet sdPromGiftSet = new SdPromGiftSet();
            // 加盟商
            sdPromGiftSet.setClient(client);
            // 单号
            sdPromGiftSet.setGspgsVoucherId(voucher_id);
            // 行号
            sdPromGiftSet.setGspgsSerial("1");
            // 系列编码
            sdPromGiftSet.setGspgsSeriesProId(sdMarketingProm.getGspgsSeriesProId());
            // 达到数量1
            sdPromGiftSet.setGspgsReachQty1(sdMarketingProm.getGspgsReachQty1());
            // 达到金额1
            sdPromGiftSet.setGspgsReachAmt1(sdMarketingProm.getGspgsReachAmt1());
            // 赠送数量1
            sdPromGiftSet.setGspgsResultQty1(sdMarketingProm.getGspgsResultQty1());
            sdPromGiftSetList.add(sdPromGiftSet);
        }
        // 组合
        if (OperateEnum.MarketingProm.COMBIN.getCode().equals(sdMarketingProm.getType())) {
            SdPromAssoSet sdPromAssoSet = new SdPromAssoSet();
            // 加盟商
            sdPromAssoSet.setClient(client);
            // 单号
            sdPromAssoSet.setGspasVoucherId(voucher_id);
            // 行号
            sdPromAssoSet.setGspasSerial("1");
            // 系列编码
            sdPromAssoSet.setGspasSeriesId(sdMarketingProm.getGspasSeriesId());
            // 组合金额
            sdPromAssoSet.setGspasAmt(sdMarketingProm.getGspasAmt());
            // 组合折扣
            sdPromAssoSet.setGspasRebate(sdMarketingProm.getGspasRebate());
            // 赠送数量
            sdPromAssoSet.setGspasQty(sdMarketingProm.getGspasQty());
            sdPromAssoSetList.add(sdPromAssoSet);
        }
        // 活动商品
        if (!CollectionUtils.isEmpty(sdMarketingPrdList)) {
            int index = 0;
            for (SdMarketingPrd sdMarketingPrd : sdMarketingPrdList) {
                index++;
                // 单品
                if (OperateEnum.MarketingProm.SINGLE.getCode().equals(sdMarketingProm.getType())) {
                    SdPromUnitarySet sdPromUnitarySet = new SdPromUnitarySet();
                    // 加盟商
                    sdPromUnitarySet.setClient(client);
                    // 单号
                    sdPromUnitarySet.setGspusVoucherId(voucher_id);
                    // 行号
                    sdPromUnitarySet.setGspusSerial(String.valueOf(index));
                    // 商品编码
                    sdPromUnitarySet.setGspusProId(sdMarketingPrd.getProCode());
                    // 效期天数
                    sdPromUnitarySet.setGspusVaild(sdMarketingProm.getGspusVaild());
                    // 达到数量1
                    sdPromUnitarySet.setGspusQty1(sdMarketingProm.getGspusQty1());
                    // 生成促销价1
                    sdPromUnitarySet.setGspusPrc1(sdMarketingProm.getGspusPrc1());
                    // 生成促销折扣1
                    sdPromUnitarySet.setGspusRebate1(sdMarketingProm.getGspusRebate1());
                    // 达到数量2
                    sdPromUnitarySet.setGspusQty2(sdMarketingProm.getGspusQty2());
                    // 生成促销价2
                    sdPromUnitarySet.setGspusPrc2(sdMarketingProm.getGspusPrc2());
                    // 生成促销折扣2
                    sdPromUnitarySet.setGspusRebate2(sdMarketingProm.getGspusRebate2());
                    // 达到数量3
                    sdPromUnitarySet.setGspusQty3(sdMarketingProm.getGspusQty3());
                    // 生成促销价3
                    sdPromUnitarySet.setGspusPrc3(sdMarketingProm.getGspusPrc3());
                    // 生成促销折扣3
                    sdPromUnitarySet.setGspusRebate3(sdMarketingProm.getGspusRebate3());
                    // 是否会员
                    sdPromUnitarySet.setGspusMemFlag(sdMarketingProm.getGspusMemFlag());
                    // 是否积分
                    sdPromUnitarySet.setGspusInteFlag(sdMarketingProm.getGspusInteFlag());
                    // 积分倍率
                    sdPromUnitarySet.setGspusInteRate(sdMarketingProm.getGspusInteRate());
                    sdPromUnitarySetList.add(sdPromUnitarySet);
                    // 促销单号回写
                    sdMarketingPrd.setGspacVoucherId(voucher_id);
                    sdMarketingPrd.setGspacSerial(String.valueOf(index));
                    sdMarketingPrdUpdateList.add(sdMarketingPrd);
                }
                // 系列
                if (OperateEnum.MarketingProm.SERIES.getCode().equals(sdMarketingProm.getType())) {
                    SdPromSeriesConds sdPromSeriesConds = new SdPromSeriesConds();
                    // 加盟商
                    sdPromSeriesConds.setClient(client);
                    // 单号
                    sdPromSeriesConds.setGspscVoucherId(voucher_id);
                    // 行号
                    sdPromSeriesConds.setGspscSerial(String.valueOf(index));
                    // 商品编码
                    sdPromSeriesConds.setGspscProId(sdMarketingPrd.getProCode());
                    // 系列编码
                    sdPromSeriesConds.setGspscSeriesId(sdMarketingProm.getGspscSeriesId());
                    // 是否会员
                    sdPromSeriesConds.setGspscMemFlag(sdMarketingProm.getGspscMemFlag());
                    // 是否积分
                    sdPromSeriesConds.setGspscInteFlag(sdMarketingProm.getGspscInteFlag());
                    // 积分倍率
                    sdPromSeriesConds.setGspscInteRate(sdMarketingProm.getGspscInteRate());
                    sdPromSeriesCondsList.add(sdPromSeriesConds);
                    // 促销单号回写
                    sdMarketingPrd.setGspacVoucherId(voucher_id);
                    sdMarketingPrd.setGspacSerial(String.valueOf(index));
                    sdMarketingPrdUpdateList.add(sdMarketingPrd);
                }
                // 赠品
                if (OperateEnum.MarketingProm.GIFT.getCode().equals(sdMarketingProm.getType())) {
                    SdPromGiftConds sdPromGiftConds = new SdPromGiftConds();
                    // 加盟商
                    sdPromGiftConds.setClient(client);
                    // 单号
                    sdPromGiftConds.setGspgcVoucherId(voucher_id);
                    // 行号
                    sdPromGiftConds.setGspgcSerial(String.valueOf(index));
                    // 商品编码
                    sdPromGiftConds.setGspgcProId(sdMarketingPrd.getProCode());
                    // 系列编码
                    sdPromGiftConds.setGspgcSeriesId(sdMarketingProm.getGspgcSeriesId());
                    // 是否会员
                    sdPromGiftConds.setGspgcMemFlag(sdMarketingProm.getGspgcMemFlag());
                    // 是否积分
                    sdPromGiftConds.setGspgcInteFlag(sdMarketingProm.getGspgcInteFlag());
                    // 积分倍率
                    sdPromGiftConds.setGspgcInteRate(sdMarketingProm.getGspgcInteRate());
                    sdPromGiftCondsList.add(sdPromGiftConds);
                    // 促销单号回写
                    sdMarketingPrd.setGspacVoucherId(voucher_id);
                    sdMarketingPrd.setGspacSerial(String.valueOf(index));
                    sdMarketingPrdUpdateList.add(sdMarketingPrd);
                }
                // 组合
                if (OperateEnum.MarketingProm.COMBIN.getCode().equals(sdMarketingProm.getType())) {
                    SdPromAssoConds sdPromAssoConds = new SdPromAssoConds();
                    // 加盟商
                    sdPromAssoConds.setClient(client);
                    // 单号
                    sdPromAssoConds.setGspacVoucherId(voucher_id);
                    // 行号
                    sdPromAssoConds.setGspacSerial(String.valueOf(index));
                    // 商品编码
                    sdPromAssoConds.setGspacProId(sdMarketingPrd.getProCode());
                    // 数量
                    sdPromAssoConds.setGspacQty(sdMarketingProm.getGspacQty());
                    // 系列编码
                    sdPromAssoConds.setGspacSeriesId(sdMarketingProm.getGspacSeriesId());
                    // 是否会员
                    sdPromAssoConds.setGspacMemFlag(sdMarketingProm.getGspacMemFlag());
                    // 是否积分
                    sdPromAssoConds.setGspacInteFlag(sdMarketingProm.getGspacInteFlag());
                    // 积分倍率
                    sdPromAssoConds.setGspacInteRate(sdMarketingProm.getGspacInteRate());
                    sdPromAssoCondsList.add(sdPromAssoConds);
                    // 促销单号回写
                    sdMarketingPrd.setGspacVoucherId(voucher_id);
                    sdMarketingPrd.setGspacSerial(String.valueOf(index));
                    sdMarketingPrdUpdateList.add(sdMarketingPrd);
                }
            }
        }
        // 活动赠品
        if (!CollectionUtils.isEmpty(sdMarketingGiveawayList)) {
            int index = 0;
            for (SdMarketingGiveaway sdMarketingGiveaway : sdMarketingGiveawayList) {
                index++;
                // 赠品
                if (OperateEnum.MarketingProm.GIFT.getCode().equals(sdMarketingProm.getType())) {
                    SdPromGiftResult sdPromGiftResult = new SdPromGiftResult();
                    // 加盟商
                    sdPromGiftResult.setClient(client);
                    // 单号
                    sdPromGiftResult.setGspgrVoucherId(voucher_id);
                    // 行号
                    sdPromGiftResult.setGspgrSerial(String.valueOf(index));
                    // 商品编码
                    sdPromGiftResult.setGspgrProId(sdMarketingGiveaway.getProCode());
                    // 系列编码
                    sdPromGiftResult.setGspgrSeriesId(sdMarketingProm.getGspgrSeriesId());
                    // 赠品单品价格
                    sdPromGiftResult.setGspgrGiftPrc(sdMarketingProm.getGspgrGiftPrc());
                    // 赠品单品折扣
                    sdPromGiftResult.setGspgrGiftRebate(StringUtils.isBlank(sdMarketingProm.getGspgrGiftRebate()) ? null : sdMarketingProm.getGspgrGiftRebate());
                    sdPromGiftResultList.add(sdPromGiftResult);
                }
                // 组合
                if (OperateEnum.MarketingProm.COMBIN.getCode().equals(sdMarketingProm.getType())) {
                    SdPromAssoResult sdPromAssoResult = new SdPromAssoResult();
                    // 加盟商
                    sdPromAssoResult.setClient(client);
                    // 单号
                    sdPromAssoResult.setGsparVoucherId(voucher_id);
                    // 行号
                    sdPromAssoResult.setGsparSerial(String.valueOf(index));
                    // 商品编码
                    sdPromAssoResult.setGsparProId(sdMarketingGiveaway.getProCode());
                    // 系列编码
                    sdPromAssoResult.setGsparSeriesId(sdMarketingProm.getGsparSeriesId());
                    // 赠品单品价格
                    sdPromAssoResult.setGsparGiftPrc(sdMarketingProm.getGsparGiftPrc());
                    // 赠品单品折扣
                    sdPromAssoResult.setGsparGiftRebate(sdMarketingProm.getGsparGiftRebate());
                    sdPromAssoResultList.add(sdPromAssoResult);
                }
            }
        }
    }


    /**
     * 判断当前用户是否为  加盟商负责人/委托人
     *
     * @return Integer 0:加盟商负责人/委托人 1:员工
     */
    private Integer getLegalPerson(String client, String userId) {
        Franchisee franchisee = franchiseeService.getById(client);
        if (!ObjectUtils.isEmpty(franchisee)) {
            String francLegalPerson = franchisee.getFrancLegalPerson();
            String francAss = franchisee.getFrancAss();
            if (!StringUtils.isEmpty(userId) && (userId.equals(francLegalPerson) || userId.equals(francAss))) {
                return 0;
            }
            return 1;
        }
        return 1;
    }
}
