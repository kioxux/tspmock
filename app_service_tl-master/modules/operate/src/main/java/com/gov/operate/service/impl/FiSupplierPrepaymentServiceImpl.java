package com.gov.operate.service.impl;

import com.gov.operate.entity.FiSupplierPrepayment;
import com.gov.operate.mapper.FiSupplierPrepaymentMapper;
import com.gov.operate.service.IFiSupplierPrepaymentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
@Service
public class FiSupplierPrepaymentServiceImpl extends ServiceImpl<FiSupplierPrepaymentMapper, FiSupplierPrepayment> implements IFiSupplierPrepaymentService {

}
