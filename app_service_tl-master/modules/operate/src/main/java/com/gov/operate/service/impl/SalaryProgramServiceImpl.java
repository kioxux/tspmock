package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SalaryProgramMapper;
import com.gov.operate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Slf4j
@Service
public class SalaryProgramServiceImpl extends ServiceImpl<SalaryProgramMapper, SalaryProgram> implements ISalaryProgramService {

    @Resource
    private CommonService commonService;
    @Resource
    private ISalaryObjService salaryObjService;
    @Resource
    private ISalaryMonthReachService salaryMonthReachService;
    @Resource
    private ISalaryMonthSalaryGradeService salaryMonthSalaryGradeService;
    @Resource
    private ISalaryMonthWageJobsService salaryMonthWageJobsService;
    @Resource
    private ISalaryMonthMeritPayService salaryMonthMeritPayService;
    @Resource
    private ISalaryProCpsService salaryProCpsService;
    @Resource
    private ISalaryMonthAppraisalSalaryService salaryMonthAppraisalSalaryService;
    @Resource
    private ISalaryKpiService salaryKpiService;
    @Resource
    private ISalaryQuarterlyAwardsService salaryQuarterlyAwardsService;
    @Resource
    private ISalaryYearAwardsService salaryYearAwardsService;
    @Resource
    private AuthFeign authFeign;
    @Resource
    private SalaryProgramMapper salaryProgramMapper;

    /**
     * 方案创建
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer addProgram(AddProgramVO vo) {
        TokenUser user = commonService.getLoginInfo();
        SalaryProgram salaryProgram = new SalaryProgram();
        BeanUtils.copyProperties(vo, salaryProgram);
        // 加盟商
        salaryProgram.setClient(user.getClient());
        // 索引
        salaryProgram.setGspIndex(1);
        // 审批状态
        salaryProgram.setGspApprovalStatus(-1);
        // 基本工资
        if (!ObjectUtils.isEmpty(vo.getGspBasicSalary())) {
            salaryProgram.setGspBasicSalary(DecimalUtils.toDecimal(vo.getGspBasicSalary()));
        }
        // 岗位工资
        if (!ObjectUtils.isEmpty(vo.getGspPostSalary())) {
            salaryProgram.setGspPostSalary(DecimalUtils.toDecimal(vo.getGspPostSalary()));
        }
        // 绩效工资
        if (!ObjectUtils.isEmpty(vo.getGspPerformanceSalary())) {
            salaryProgram.setGspPerformanceSalary(DecimalUtils.toDecimal(vo.getGspPerformanceSalary()));
        }
        // 考评工资
        if (!ObjectUtils.isEmpty(vo.getGspEvaluationSalary())) {
            salaryProgram.setGspEvaluationSalary(DecimalUtils.toDecimal(vo.getGspEvaluationSalary()));
        }
        // 创建 日期/时间/人
        salaryProgram.setGspCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        salaryProgram.setGspCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        salaryProgram.setGspCreateUser(user.getUserId());
        this.save(salaryProgram);

        // 薪资方适用范围
        List<SalaryObj> salaryObjList = vo.getApplyObjList().stream().map(objDTO -> {
            if (StringUtils.isEmpty(objDTO.getGsoApplyObj())) {
                throw new CustomResultException("适用范围不能为空");
            }
            if (StringUtils.isEmpty(objDTO.getGsoApplyObjName())) {
                throw new CustomResultException("适用范围名称不能为空");
            }
            SalaryObj salaryObj = new SalaryObj();
            BeanUtils.copyProperties(objDTO, salaryObj);
            salaryObj.setGsoGspId(salaryProgram.getGspId());
            return salaryObj;
        }).collect(Collectors.toList());
        salaryObjService.saveBatch(salaryObjList);
        return salaryProgram.getGspId();
    }

    /**
     * 方案修改
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editProgram(EditProgramVO vo) {
        SalaryProgram salaryProgramExit = this.getById(vo.getGspId());
        if (ObjectUtils.isEmpty(salaryProgramExit)) {
            throw new CustomResultException("不存在该方案或该方案已经被删除");
        }
        TokenUser user = commonService.getLoginInfo();
        SalaryProgram salaryProgram = new SalaryProgram();
        // 主键
        salaryProgram.setGspId(vo.getGspId());
        // 步骤
        if (salaryProgramExit.getGspIndex() < vo.getGspIndex()) {
            salaryProgram.setGspIndex(vo.getGspIndex());
        }
        // 创建 日期/时间/人
        salaryProgram.setGspChangeDate(DateUtils.getCurrentDateStrYYMMDD());
        salaryProgram.setGspChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
        salaryProgram.setGspChangeUser(user.getUserId());
        // 第一步 设定绩效基础信息
        if (vo.getGspIndex() == 1) {
            if (StringUtils.isEmpty(vo.getGspName())) {
                throw new CustomResultException("方案名称不能为空");
            }
            if (StringUtils.isEmpty(vo.getGspEffectiveDate())) {
                throw new CustomResultException("方案生效日期不能为空");
            }
            if (StringUtils.isEmpty(vo.getGspPurpose())) {
                throw new CustomResultException("方案目的不能为空");
            }
            salaryProgram.setGspName(vo.getGspName());
            salaryProgram.setGspEffectiveDate(vo.getGspEffectiveDate());
            salaryProgram.setGspPurpose(vo.getGspPurpose());
            this.updateById(salaryProgram);
            // 设定月综合达成率/达成系数
            salaryObjService.saveApplyObj(vo);
            return;
        }
        // 第二步 设定月综合达成率/达成系数
        if (vo.getGspIndex() == 2) {
            if (ObjectUtils.isEmpty(vo.getGspTurnover())) {
                throw new CustomResultException("营业额权重不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspGrossProfit())) {
                throw new CustomResultException("毛利额权重不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspIsNewStore())) {
                throw new CustomResultException("新店综合达成率计算不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspOpenBeforeMonth())) {
                throw new CustomResultException("开业前月综合达成率计算月分不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspNewAchieveRate())) {
                throw new CustomResultException("新店综合达成率不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspFirstmonthDays())) {
                throw new CustomResultException("首月整月天数不能为空");
            }
            salaryProgram.setGspTurnover(DecimalUtils.toDecimal(vo.getGspTurnover()));
            salaryProgram.setGspGrossProfit(DecimalUtils.toDecimal(vo.getGspGrossProfit()));
            salaryProgram.setGspIsNewStore(vo.getGspIsNewStore());
            salaryProgram.setGspOpenBeforeMonth(vo.getGspOpenBeforeMonth());
            salaryProgram.setGspNewAchieveRate(DecimalUtils.toDecimal(vo.getGspNewAchieveRate()));
            salaryProgram.setGspFirstmonthDays(vo.getGspFirstmonthDays());
            this.updateById(salaryProgram);
            // 设定月综合达成率/达成系数
            salaryMonthReachService.saveReachList(vo);
            return;
        }
        // 第三步 设定月基本工资
        if (vo.getGspIndex() == 3) {
            // 和综合达成系数挂钩传空则设为0
            if (ObjectUtils.isEmpty(vo.getGspSalaryReach())) {
                vo.setGspSalaryReach(0);
            }
            salaryProgram.setGspSalaryReach(vo.getGspSalaryReach());
            this.updateById(salaryProgram);
            // 薪资方案月工资级别
            salaryMonthSalaryGradeService.saveSalaryGrade(vo);
            return;
        }
        // 第四步 设定月岗位工资
        if (vo.getGspIndex() == 4) {
            // 和综合达成系数挂钩传空则设为0
            if (ObjectUtils.isEmpty(vo.getGspPostReach())) {
                vo.setGspPostReach(0);
            }
            salaryProgram.setGspPostReach(vo.getGspPostReach());
            this.updateById(salaryProgram);
            // 岗位工资
            salaryMonthWageJobsService.saveWageJobs(vo);
            return;
        }
        // 第五步 设定月绩效工资
        if (vo.getGspIndex() == 5) {
            if (ObjectUtils.isEmpty(vo.getGspSalesCommission())) {
                throw new CustomResultException("销售提成不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspGrossMargin())) {
                throw new CustomResultException("毛利提成不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspNegativeGrossMargin())) {
                vo.setGspNegativeGrossMargin(0);
            }
            // 不参与销售提成传空，则设为0
            if (!CollectionUtils.isEmpty(vo.getSalaryProCpsList()) && ObjectUtils.isEmpty(vo.getGspProReach())) {
                vo.setGspProReach(0);
            }
            // 和综合达成系数挂钩传空，则设为0
            if (ObjectUtils.isEmpty(vo.getGspSalesReach())) {
                vo.setGspSalesReach(0);
            }
            salaryProgram.setGspSalesCommission(vo.getGspSalesCommission());
            salaryProgram.setGspGrossMargin(vo.getGspGrossMargin());
            salaryProgram.setGspNegativeGrossMargin(vo.getGspNegativeGrossMargin());
            salaryProgram.setGspProReach(vo.getGspProReach());
            salaryProgram.setGspSalesReach(vo.getGspSalesReach());
            this.updateById(salaryProgram);
            //  月绩效工资等级
            salaryMonthMeritPayService.saveMeritPay(vo);
            // 薪资方案单品提成
            salaryProCpsService.saveProCps(vo);
            return;
        }
        // 第六步 月考评工资
        if (vo.getGspIndex() == 6) {
            // 综合达成系数挂勾传空，则设为0
            if (ObjectUtils.isEmpty(vo.getGspAppraisalSalary())) {
                vo.setGspAppraisalSalary(0);
            }
            salaryProgram.setGspAppraisalSalary(vo.getGspAppraisalSalary());
            this.updateById(salaryProgram);
            //月考评工资
            salaryMonthAppraisalSalaryService.saveAppraial(vo);
            //员工个人系数
            salaryKpiService.saveKpi(vo);
            return;
        }
        // 第七步 认定季度奖
        if (vo.getGspIndex() == 7) {
            if (ObjectUtils.isEmpty(vo.getGspQuarteQuarterlyAwards())) {
                throw new CustomResultException("季度奖提成不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspQuarteAchievRateMin())) {
                throw new CustomResultException("不参与季度奖提成最大值不能为空");
            }
            // 不参与季度奖提成传空，则为0
            if (ObjectUtils.isEmpty(vo.getGspQuarteComputeAchievRate())) {
                vo.setGspQuarteComputeAchievRate(0);
            }
            // 负毛利商品不参与季度奖提成传空，则为零
            if (ObjectUtils.isEmpty(vo.getGspQuarterNegativeCommission())) {
                vo.setGspQuarterNegativeCommission(0);
            }
            // 关联季度员工个人系数传空，则设为0
            if (ObjectUtils.isEmpty(vo.getGspQuarterKpi())) {
                vo.setGspQuarterKpi(0);
            }
            // 关联季度综合达成系数传空 则设为0
            if (ObjectUtils.isEmpty(vo.getGspQuarterAppraisalSalary())) {
                vo.setGspQuarterAppraisalSalary(0);
            }

            salaryProgram.setGspQuarteQuarterlyAwards(vo.getGspQuarteQuarterlyAwards());
            salaryProgram.setGspQuarteComputeAchievRate(vo.getGspQuarteComputeAchievRate());
            salaryProgram.setGspQuarteAchievRateMin(DecimalUtils.toDecimal(vo.getGspQuarteAchievRateMin()));
            salaryProgram.setGspQuarterKpi(vo.getGspQuarterKpi());
            salaryProgram.setGspQuarterAppraisalSalary(vo.getGspQuarterAppraisalSalary());
            salaryProgram.setGspQuarterNegativeCommission(vo.getGspQuarterNegativeCommission());
            this.updateById(salaryProgram);
            salaryQuarterlyAwardsService.saveQuarterlyAwards(vo);
            return;
        }
        // 第八步 认定年终奖
        if (vo.getGspIndex() == 8) {
            if (ObjectUtils.isEmpty(vo.getGspYearQuarterlyAwards())) {
                throw new CustomResultException("年终奖提成不能为空");
            }
            if (ObjectUtils.isEmpty(vo.getGspYearAchievRateMin())) {
                throw new CustomResultException("不参与年终奖提成最大值不能为空");
            }
            // 是否不参与年终奖提成传空，则为0
            if (ObjectUtils.isEmpty(vo.getGspYearComputeAchievRate())) {
                vo.setGspYearComputeAchievRate(0);
            }
            // 负毛利商品不参与年终奖提成传空，则设为0
            if (ObjectUtils.isEmpty(vo.getGspYearNegativeCommission())) {
                vo.setGspYearNegativeCommission(0);
            }
            // 是否关联年度员工个人系数传空，则设为0
            if (ObjectUtils.isEmpty(vo.getGspYearKpi())) {
                vo.setGspYearKpi(0);
            }
            // 是否关联年度综合达成系数传空则设为0
            if (ObjectUtils.isEmpty(vo.getGspYearAppraisalSalary())) {
                vo.setGspYearAppraisalSalary(0);
            }
            salaryProgram.setGspYearQuarterlyAwards(vo.getGspYearQuarterlyAwards());
            salaryProgram.setGspYearComputeAchievRate(vo.getGspYearComputeAchievRate());
            salaryProgram.setGspYearAchievRateMin(DecimalUtils.toDecimal(vo.getGspYearAchievRateMin()));
            salaryProgram.setGspYearKpi(vo.getGspYearKpi());
            salaryProgram.setGspYearAppraisalSalary(vo.getGspYearAppraisalSalary());
            salaryProgram.setGspYearNegativeCommission(vo.getGspYearNegativeCommission());
            this.updateById(salaryProgram);
            salaryYearAwardsService.saveYearAwards(vo);
            return;
        }

    }

    /**
     * 方案获取
     */
    @Override
    public SalaryProgramDTO getProgram(Integer gspId) {
        if (ObjectUtils.isEmpty(gspId)) {
            throw new CustomResultException("方案主键不能为空");
        }
        SalaryProgram salaryProgram = this.getById(gspId);
        if (ObjectUtils.isEmpty(salaryProgram)) {
            throw new CustomResultException("没有该方案");
        }
        SalaryProgramDTO salaryProgramDTO = new SalaryProgramDTO();
        BeanUtils.copyProperties(salaryProgram, salaryProgramDTO);
        // 基本工资占比
        salaryProgramDTO.setGspBasicSalary(salaryProgram.getGspBasicSalary().multiply(Constants.ONE_HUNDRED));
        // 岗位工资占比
        salaryProgramDTO.setGspPostSalary(salaryProgram.getGspPostSalary().multiply(Constants.ONE_HUNDRED));
        // 绩效工资占比
        salaryProgramDTO.setGspPerformanceSalary(salaryProgram.getGspPerformanceSalary().multiply(Constants.ONE_HUNDRED));
        // 考评工资占比
        salaryProgramDTO.setGspEvaluationSalary(salaryProgram.getGspEvaluationSalary().multiply(Constants.ONE_HUNDRED));
        // 营业额权重
        salaryProgramDTO.setGspTurnover(salaryProgram.getGspTurnover().multiply(Constants.ONE_HUNDRED));
        // 毛利额权重
        salaryProgramDTO.setGspGrossProfit(salaryProgram.getGspGrossProfit().multiply(Constants.ONE_HUNDRED));
        // 新店综合达成率
        salaryProgramDTO.setGspNewAchieveRate(salaryProgram.getGspNewAchieveRate().multiply(Constants.ONE_HUNDRED));
        // 季度综合达成率<={0}，不参与季度奖提成
        salaryProgramDTO.setGspQuarteAchievRateMin(salaryProgram.getGspQuarteAchievRateMin().multiply(Constants.ONE_HUNDRED));
        //
        salaryProgramDTO.setGspYearAchievRateMin(salaryProgram.getGspYearAchievRateMin().multiply(Constants.ONE_HUNDRED));

        // 第一步 适用范围列表
        List<SalaryObj> applyObjList = salaryObjService.getApplyObj(gspId);
        salaryProgramDTO.setApplyObjList(applyObjList);

        // 第二步 达成率 列表
        List<SalaryMonthReach> salaryMonthReacheList = salaryMonthReachService.getReachList(gspId);
        salaryProgramDTO.setSalaryMonthReacheList(salaryMonthReacheList);

        // 第三步 基本工资/薪资方案列表
        List<SalaryMonthSalaryGrade> salaryMonthSalaryGradeList = salaryMonthSalaryGradeService.getSalaryGradeList(gspId);
        salaryProgramDTO.setSalaryMonthSalaryGradeList(salaryMonthSalaryGradeList);

        // 第四步 岗位工资列表
        List<SalaryProgramDTO.SalaryMonthWageJobsOuter> salaryMonthWageJobsOuterList = salaryMonthWageJobsService.getWageJobsList(gspId);
        salaryProgramDTO.setSalaryMonthWageJobsOuterList(salaryMonthWageJobsOuterList);

        // 第五步 月绩效工资列表
        List<SalaryProgramDTO.SalaryMonthMeritPayOuter> salaryMonthMeritPayOuterList = salaryMonthMeritPayService.getMeritPayList(gspId);
        // 第五步 单品提成列表
        List<SalaryProCps> salaryProCpsList = salaryProCpsService.getProCpsList(gspId);
        salaryProgramDTO.setSalaryMonthMeritPayOuterList(salaryMonthMeritPayOuterList);
        salaryProgramDTO.setSalaryProCpsList(salaryProCpsList);

        // 第六步 月考评工资列表
        List<SalaryMonthAppraisalSalary> salaryMonthAppraisalSalaryList = salaryMonthAppraisalSalaryService.getAppraisalSalaryList(gspId);
        // 第六步 员工系数值列表
        List<SalaryKpi> salaryKpiList = salaryKpiService.getKpiList(gspId);
        salaryProgramDTO.setSalaryMonthAppraisalSalaryList(salaryMonthAppraisalSalaryList);
        salaryProgramDTO.setSalaryKpiList(salaryKpiList);

        // 第七步 季度奖列表
        List<SalaryQuarterlyAwards> salaryQuarterlyAwardsList = salaryQuarterlyAwardsService.getQuarterlyAwards(gspId);
        salaryProgramDTO.setSalaryQuarterlyAwardsList(salaryQuarterlyAwardsList);

        // 第八步 年终奖列表
        List<SalaryYearAwards> salaryYearAwardsList = salaryYearAwardsService.getYearAwardsList(gspId);
        salaryProgramDTO.setSalaryYearAwardsList(salaryYearAwardsList);

        return salaryProgramDTO;
    }

    /**
     * 方案列表
     */
    @Override
    public IPage<QueryProgramListDTO> queryProgramList(QueryProgramListVO vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<QueryProgramListDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        Page<QueryProgramListDTO> list = salaryProgramMapper.queryProgramList(page, user.getClient());
        return list;
    }

    /**
     * 提交申批
     */
    @Override
    public void subTrial(SubTrialVO vo) {
        Integer gspId = vo.getGspId();
        if (ObjectUtils.isEmpty(gspId)) {
            throw new CustomResultException("方案主键不能为空");
        }
        // 创建工作流
        // 初始编码
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        SalaryProgram salaryProgram = new SalaryProgram();
        salaryProgram.setGspId(gspId);
        salaryProgram.setGspApprovalStatus(0);
        salaryProgram.setGspFlowNo(flowNo);
        this.updateById(salaryProgram);
        createSalaryProgramWorkflow(this.getById(gspId));
    }

    /**
     * 创建工作流
     */
    public void createSalaryProgramWorkflow(SalaryProgram salaryProgram) {
        if (ObjectUtils.isEmpty(salaryProgram)) {
            throw new CustomResultException("未找到该方案");
        }
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", "GAIA_WF_031");  // 类型

        param.put("wfTitle", salaryProgram.getGspName());   // 标题
        param.put("wfOrder", salaryProgram.getGspFlowNo());   // 业务单号

        log.info("薪资方案申批提交！ 提交参数：{}", JsonUtils.beanToJson(param));
        JsonResult result = authFeign.createWorkflow(param);
        log.info("薪资方案申批提交！ 返回结果：{}", JsonUtils.beanToJson(result));
        if (result.getCode().intValue() != 0) {
            throw new CustomResultException(result.getMessage());
        }
    }


}
