package com.gov.operate.mapper;

import com.gov.operate.entity.ProductMatchZResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-27
 */
public interface ProductMatchZResultMapper extends BaseMapper<ProductMatchZResult> {

}
