package com.gov.operate.controller.dataImport;

import com.gov.common.entity.dataImport.ImportDto;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.service.dataImport.BusinessImportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Api(tags = "业务数据导入功能")
@RestController
@RequestMapping("import")
public class ImportController {

    @Resource
    BusinessImportService businessImportService;

    /**
     * 导入
     *
     * @return
     */
    @Log("导入")
    @ApiOperation("导入")
    @PostMapping("businessImport/{type}")
    public Result businessImport(@PathVariable String type, @Valid @RequestBody ImportDto dto) {
        dto.setType(type);
        return businessImportService.businessImport(dto);
    }

    @Log("促销商品导入")
    @PostMapping({"/importProductInfo"})
    public Result importProductInfo(@RequestParam("file") MultipartFile file){
        return ResultUtil.success(businessImportService.importProductInfo(file));
    }
}
