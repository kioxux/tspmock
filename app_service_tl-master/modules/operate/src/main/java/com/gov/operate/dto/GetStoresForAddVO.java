package com.gov.operate.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: jinwencheng
 * @Date: Created in 2021-12-23 17:03:55
 * @Description: 添加门店分类 - 门店选择 - 查询返回的参数
 * @Version: 1.0
 */
@Data
public class GetStoresForAddVO {

    /**
     * 是否已设置：1-是；0-否
     */
    private Integer isSet;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 分类类型编码
     */
    private String gssgType;

    /**
     * 分类类型名称
     */
    private String gssgTypeName;

    /**
     * 分类编码
     */
    private String gssgId;

    /**
     * 分类编码名称
     */
//    @JSONField(serialize = false)
    private String gssgIdName;

}
