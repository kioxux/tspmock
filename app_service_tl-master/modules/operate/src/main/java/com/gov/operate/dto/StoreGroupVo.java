package com.gov.operate.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:38 2021/7/29
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class StoreGroupVo {

    /**
     * 门店编码
     */
    @NotBlank(message = "门店编码不能为空")
    private String stoCode;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 分类类型编码
     */
    @NotBlank(message = "分类类型编码不能为空")
    private String stoGroupType;

    /**
     * 分类类型名称
     */
    private String stoGroupTypeName;

    /**
     * 分类编码
     */
    @NotBlank(message = "分类编码不能为空")
    private String stoGroupId;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称不能为空")
    private String stoGroupName;

    /**
     * 修改人
     */
    @JSONField(serialize = false)
    private String updateUser;

    /**
     * 修改日期
     */
    @JSONField(serialize = false)
    private String updateDate;

    /**
     * 修改时间
     */
    @JSONField(serialize = false)
    private String updateTime;
}
