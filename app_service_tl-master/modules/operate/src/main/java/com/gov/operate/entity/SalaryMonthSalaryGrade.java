package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_MONTH_SALARY_GRADE")
@ApiModel(value="SalaryMonthSalaryGrade对象", description="")
public class SalaryMonthSalaryGrade extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSMSG_ID", type = IdType.AUTO)
    private Integer gsmsgId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSMSG_GSP_ID")
    private Integer gsmsgGspId;

    @ApiModelProperty(value = "工资级别")
    @TableField("GSMSG_GRADE")
    private String gsmsgGrade;

    @ApiModelProperty(value = "基本工资")
    @TableField("GSMSG_BASIC_WAGE")
    private Integer gsmsgBasicWage;

    @ApiModelProperty(value = "顺序")
    @TableField("GSMSG_SORT")
    private Integer gsmsgSort;


}
