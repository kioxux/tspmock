package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALES_BILL")
@ApiModel(value="SalesBill对象", description="")
public class SalesBill extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @TableField("GSB_ORDER_ID")
    private String gsbOrderId;

    @TableField("GSB_INVOICE_NO")
    private String gsbInvoiceNo;

    @TableField("GSB_LINE_NO")
    private String gsbLineNo;


}
