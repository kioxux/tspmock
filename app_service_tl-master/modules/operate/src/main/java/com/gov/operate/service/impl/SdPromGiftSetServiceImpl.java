package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromGiftSet;
import com.gov.operate.mapper.SdPromGiftSetMapper;
import com.gov.operate.service.ISdPromGiftSetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-11
 */
@Service
public class SdPromGiftSetServiceImpl extends ServiceImpl<SdPromGiftSetMapper, SdPromGiftSet> implements ISdPromGiftSetService {

}
