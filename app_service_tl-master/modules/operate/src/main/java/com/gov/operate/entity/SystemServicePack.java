package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SYSTEM_SERVICE_PACK")
@ApiModel(value="SystemServicePack对象", description="")
public class SystemServicePack extends BaseEntity {
    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "模块id")
    @TableId("RID")
    private String rId;

    @ApiModelProperty(value = "系列")
    @TableField("SP_SERIES")
    private String spSeries;

    @ApiModelProperty(value = "服务包ID")
    @TableField("SP_ID")
    private String spId;

    @ApiModelProperty(value = "服务包名称")
    @TableField("SP_NAME")
    private String spName;

    @ApiModelProperty(value = "服务包说明")
    @TableField("SP_REMARK")
    private String spRemark;

    @ApiModelProperty(value = "服务包类型")
    @TableField("SP_TYPE")
    private String spType;

    @ApiModelProperty(value = "收费类型")
    @TableField("SP_FREE")
    private String spFree;

    @ApiModelProperty(value = "官网价格")
    @TableField("SP_OFFICIAL_PRICE")
    private BigDecimal spOfficialPrice;

    @ApiModelProperty(value = "收费方式")
    @TableField("SP_MODE")
    private Integer spMode;

    @ApiModelProperty(value = "模块说明")
    @TableField("SP_MODULE_DES")
    private String spModuleDes;

    @ApiModelProperty(value = "创建日期")
    @TableField("SP_CRE_DATE")
    private String spCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("SP_CRE_TIME")
    private String spCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("SP_CRE_ID")
    private String spCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("SP_MODI_DATE")
    private String spModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("SP_MODI_TIME")
    private String spModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("SP_MODI_ID")
    private String spModiId;

}
