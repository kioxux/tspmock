package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_GRADE")
@ApiModel(value="SalaryGrade对象", description="")
public class SalaryGrade extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLENT")
    private String clent;

    @ApiModelProperty(value = "工资级别编码")
    @TableField("SLA_CL_NO")
    private String slaClNo;

    @ApiModelProperty(value = "工资级别描述")
    @TableField("SLA_CLS")
    private String slaCls;


}
