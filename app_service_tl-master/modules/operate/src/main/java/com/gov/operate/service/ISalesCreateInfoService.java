package com.gov.operate.service;

import com.gov.operate.entity.SalesCreateInfo;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 开票人员基本信息 服务类
 * </p>
 *
 * @author sy
 * @since 2021-05-28
 */
public interface ISalesCreateInfoService extends SuperService<SalesCreateInfo> {

}
