package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-02-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SUPPLIER_PREPAYMENT")
@ApiModel(value = "SupplierPrepayment对象", description = "")
public class SupplierPrepayment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "采购主体")
    @TableField("PAY_COMPANY_CODE")
    private String payCompanyCode;

    @ApiModelProperty(value = "付款申请单号")
    @TableField("PAY_ORDER_ID")
    private String payOrderId;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "申请付款金额")
    @TableField("APPLICATION_PAYMENT_AMOUNT")
    private BigDecimal applicationPaymentAmount;

    @ApiModelProperty(value = "创建人")
    @TableField("FOUNDER")
    private String founder;

    @ApiModelProperty(value = "申请日期")
    @TableField("GSP_APPLY_DATE")
    private String gspApplyDate;

    @ApiModelProperty(value = "状态")
    @TableField("APPLICATION_STATUS")
    private String applicationStatus;

    @ApiModelProperty(value = "工作流编码")
    @TableField("APPROVAL_FLOW_NO")
    private String approvalFlowNo;

    @ApiModelProperty(value = "审批备注")
    @TableField("APPROVAL_REMARK")
    private String approvalRemark;

    @ApiModelProperty(value = "工作申请日期")
    @TableField("GSP_DATE")
    private String gspDate;

    @ApiModelProperty(value = "供应商开户行名称")
    @TableField("GSP_SUP_BANK_NAME")
    private String gspSupBankName;

    @ApiModelProperty(value = "供应商开户行账号")
    @TableField("GSP_SUP_BANK_NUM")
    private String gspSupBankNum;

    @ApiModelProperty(value = "文件路径")
    @TableField("GSP_FILE")
    private String gspFile;

    @ApiModelProperty(value = "文件名")
    @TableField("GSP_FILE_NAME")
    private String gspFileName;

    @ApiModelProperty(value = "审批完成日期")
    @TableField("GSP_APPROVAL_DATE")
    private String gspApprovalDate;

    @ApiModelProperty(value = "银行代码")
    @TableField("GSP_SUP_BANK_CODE")
    private String gspSupBankCode;

    @ApiModelProperty(value = "业务员")
    @TableField("SUP_SALESMAN")
    private String supSalesman;
}
