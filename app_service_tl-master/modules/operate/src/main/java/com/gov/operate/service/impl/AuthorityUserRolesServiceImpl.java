package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.UserDateVO;
import com.gov.operate.dto.UserRoleVO;
import com.gov.operate.entity.AuthorityUserRoles;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.AuthorityUserRolesMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IAuthorityUserRolesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Service
public class AuthorityUserRolesServiceImpl extends ServiceImpl<AuthorityUserRolesMapper, AuthorityUserRoles> implements IAuthorityUserRolesService {
    @Resource
    private CommonService commonService;

    @Resource
    private AuthorityUserRolesMapper userRolesMapper;

    /**
     * 人员列表
     */
    @Override
    public Result getUserList(Integer gaurRoleId) {
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        map.put("ROLEID", gaurRoleId);
        List<UserDateVO> userDataList = userRolesMapper.selectNoUserList(map);
        return ResultUtil.success(userDataList);
    }


    /**
     * 创建人员与角色的关联
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addUserRole(UserRoleVO vo) {
        TokenUser user = commonService.getLoginInfo();
        if (null == vo.getGaurUserIdList() || vo.getGaurUserIdList().isEmpty()) {
            throw new CustomResultException("用户编码不能为空");
        }
        List<AuthorityUserRoles> userRolesList = new ArrayList<>();
        vo.getGaurUserIdList().forEach(item -> {
            AuthorityUserRoles roles = new AuthorityUserRoles();
            roles.setClient(user.getClient());
            roles.setGaurRoleId(vo.getGaurRoleId());
            roles.setGaurUserId(item);
            roles.setGaurSfsq("1");
            roles.setGaurCjr(user.getUserId());
            roles.setGaurCjrxm(user.getLoginName());
            roles.setGaurCjrq(DateUtils.getCurrentDateStrYYMMDD());
            roles.setGaurCjsj(DateUtils.getCurrentTimeStrHHMMSS());
            userRolesList.add(roles);
        });
        this.saveBatch(userRolesList);
        return ResultUtil.success();
    }

    /**
     * 获取人员与角色的关联
     */
    @Override
    public Result userRoleList(Integer gaurRoleId) {
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        map.put("ROLEID", gaurRoleId);
        List<UserDateVO> userDataList = userRolesMapper.selectUserList(map);
        return ResultUtil.success(userDataList);
    }


    /**
     * 删除人员与角色的关联
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result deleteUserRole(List<Integer> vo) {
        TokenUser user = commonService.getLoginInfo();
        if (null == vo || vo.isEmpty() || vo.size() == 0) {
            throw new CustomResultException("用户角色关联id不能为空");
        }
        List<AuthorityUserRoles> userRolesList = new ArrayList<>();
        vo.forEach(item -> {
            AuthorityUserRoles roles = new AuthorityUserRoles();
            roles.setId(item);
            roles.setClient(user.getClient());
            roles.setGaurSfsq("0");
            roles.setGaurXgr(user.getUserId());
            roles.setGaurXgrxm(user.getLoginName());
            roles.setGaurXgrq(DateUtils.getCurrentDateStrYYMMDD());
            roles.setGaurXgsj(DateUtils.getCurrentTimeStrHHMMSS());
            userRolesList.add(roles);
        });
        this.updateBatchById(userRolesList);
        return ResultUtil.success();
    }


}
