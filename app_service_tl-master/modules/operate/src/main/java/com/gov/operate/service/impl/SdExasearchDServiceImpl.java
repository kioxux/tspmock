package com.gov.operate.service.impl;

import com.gov.operate.entity.SdExasearchD;
import com.gov.operate.mapper.SdExasearchDMapper;
import com.gov.operate.service.ISdExasearchDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@Service
public class SdExasearchDServiceImpl extends ServiceImpl<SdExasearchDMapper, SdExasearchD> implements ISdExasearchDService {

}
