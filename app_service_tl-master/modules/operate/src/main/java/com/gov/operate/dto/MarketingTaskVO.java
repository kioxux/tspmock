package com.gov.operate.dto;

import com.gov.operate.entity.SdMarketing;
import com.gov.operate.feign.dto.JsonResult;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class MarketingTaskVO extends SdMarketing {

    private Integer rePurchaseNum;  // 复购人数

    private String rePurchaseAmt;  // 复购总金额

    private String rePurchaseGrprofit; // 复购毛利率

    private String rePurchaseRate;  // 复购率

    // 复购毛利额

    // 商品编码集合
    private List<ProductVO> productList;

    // 短信卡号集合
    private List<String> smsList;

    //电话通知卡号集合
    private List<String> phoneList;

    // 折扣方式
    private List<PromDTO> discountList;

    /**
     * 电话通知条件JSON
     */
    private String gsmTelcondiJson;

    /**
     * 短信查询条件JSON
     */
    private String gsmSmscondiJson;
    /**
     * 工作流数据
     */
    private JsonResult wfJsonResult;
}
