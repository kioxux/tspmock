package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_INDRAWER_D")
@ApiModel(value="SdIndrawerD对象", description="")
public class SdIndrawerD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "装斗单号")
    @TableField("GSID_VOUCHER_ID")
    private String gsidVoucherId;

    @ApiModelProperty(value = "装斗日期")
    @TableField("GSID_DATE")
    private String gsidDate;

    @ApiModelProperty(value = "行号")
    @TableField("GSID_SERIAL")
    private String gsidSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSID_PRO_ID")
    private String gsidProId;

    @ApiModelProperty(value = "批号")
    @TableField("GSID_BATCH_NO")
    private String gsidBatchNo;

    @ApiModelProperty(value = "有效期")
    @TableField("GSID_VALID_DATE")
    private String gsidValidDate;

    @ApiModelProperty(value = "装斗数量")
    @TableField("GSID_QTY")
    private String gsidQty;


}
