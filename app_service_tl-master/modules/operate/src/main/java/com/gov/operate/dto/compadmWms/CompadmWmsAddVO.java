package com.gov.operate.dto.compadmWms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class CompadmWmsAddVO {

    @ApiModelProperty(value = "批发公司ID")
    private String compadmId;

    @ApiModelProperty(value = "批发公司名称")
    private String compadmName;

    @ApiModelProperty(value = "统一社会信用代码")
    private String compadmNo;

    @ApiModelProperty(value = "法人/负责人")
    private String compadmLegalPerson;
    @ApiModelProperty(value = "法人/负责人手机号")
    private String compadmLegalPersonPhone;
    @ApiModelProperty(value = "法人/负责人姓名")
    private String compadmLegalPersonName;

    @ApiModelProperty(value = "质量负责人")
    private String compadmQua;
    @ApiModelProperty(value = "质量负责人手机号")
    private String compadmQuaPhone;
    @ApiModelProperty(value = "质量负责人姓名")
    private String compadmQuaName;

    @ApiModelProperty(value = "详细地址")
    private String compadmAddr;

    @ApiModelProperty(value = "LOGO地址")
    private String compadmLogo;

    @ApiModelProperty(value = "批发公司状态 N停用 Y启用")
    private String compadmStatus;


}
