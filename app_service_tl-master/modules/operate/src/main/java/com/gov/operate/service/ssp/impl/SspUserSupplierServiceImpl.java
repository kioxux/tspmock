package com.gov.operate.service.ssp.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.SspUserSupplier;
import com.gov.operate.mapper.SspUserSupplierMapper;
import com.gov.operate.service.ssp.ISspUserSupplierService;
import org.springframework.stereotype.Service;

@Service
public class SspUserSupplierServiceImpl extends ServiceImpl<SspUserSupplierMapper, SspUserSupplier> implements ISspUserSupplierService {


}
