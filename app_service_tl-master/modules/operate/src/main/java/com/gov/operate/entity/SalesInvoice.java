package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-02-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALES_INVOICE")
@ApiModel(value = "SalesInvoice对象", description = "")
public class SalesInvoice extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId("CLIENT")
    private String client;

    @TableField("GSI_ORDER_ID")
    private String gsiOrderId;

    @TableField("GSI_SITE")
    private String gsiSite;

    @TableField("GSI_CUSTOMER")
    private String gsiCustomer;

    @TableField("GSI_AMOUNT")
    private BigDecimal gsiAmount;

    @TableField("GSI_USER")
    private String gsiUser;

    @TableField("GSI_STATUS")
    private String gsiStatus;

    @TableField("GSI_EXCEL_PATH")
    private String gsiExcelPath;

    @TableField("GSI_CREATE_DATE")
    private String gsiCreateDate;

    @ApiModelProperty(value = "发票号码")
    @TableField("GSI_BILL")
    private String gsiBill;

    @ApiModelProperty(value = "开票状态")
    @TableField("GSI_BILL_STATUS")
    private String gsiBillStatus;

    @ApiModelProperty(value = "发票类型")
    @TableField("GSI_BILL_TYPE")
    private String gsiBillType;

    @ApiModelProperty(value = "开票人")
    @TableField("GSI_BILL_USER")
    private String gsiBillUser;

    @ApiModelProperty(value = "收款人")
    @TableField("GSI_RECEIVER_USER")
    private String gsiReceiverUser;

    @ApiModelProperty(value = "复审人")
    @TableField("GSI_CHECK_USER")
    private String gsiCheckUser;

    @ApiModelProperty(value = "电话号码")
    @TableField("GSI_BILL_TEL")
    private String gsiBillTel;

    @ApiModelProperty(value = "邮箱")
    @TableField("GSI_BILL_EMAIL")
    private String gsiBillEmail;

    @ApiModelProperty(value = "是否推送 Y：是 N：否")
    @TableField("GSI_BILL_PUSH")
    private String gsiBillPush;

    @ApiModelProperty(value = "备注")
    @TableField("GSI_BILL_REMARK")
    private String gsiBillRemark;

    @ApiModelProperty(value = "创建人")
    @TableField("GSI_CREATE_ID")
    private String gsiCreateId;

    @ApiModelProperty(value = "创建时间")
    @TableField("GSI_CREATE_TIME")
    private String gsiCreateTime;

    @ApiModelProperty(value = "修改人")
    @TableField("GSI_MOD_ID")
    private String gsiModId;

    @ApiModelProperty(value = "修改日期")
    @TableField("GSI_MOD_DATE")
    private String gsiModDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("GSI_MOD_TIME")
    private String gsiModTime;

    @ApiModelProperty(value = "失败原因")
    @TableField("GSI_BILL_STATUS_REASON")
    private String gsiBillStatusReason;

    @ApiModelProperty(value = "发票种类　p:电子增值税普通发票，c:增值税普通发票(纸票)，s:增值税专用发票，e:收购发票(电子)，" +
            "f:收购发票(纸质)，r:增值税普通发票(卷式)，b:增值税电子专用发票")
    @TableField(value = "GSI_INVOICE_CLASS")
    private String gsiInvoiceClass;

    @ApiModelProperty(value = "开票结果")
    @TableField(value = "GSI_INVOICE_RETURN")
    private String gsiInvoiceReturn;

    @ApiModelProperty(value = "发票PDF")
    @TableField(value = "GSI_INVOICE_PDF")
    private String gsiInvoicePdf;

    @ApiModelProperty(value = "开票流水号")
    @TableField(value = "GSI_INVOICE_SEQ")
    private String gsiInvoiceSeq;

    @ApiModelProperty(value = "开票身份")
    @TableField("GSI_INVOICE_IDENTITY")
    private String gsiInvoiceIdentity;
}
