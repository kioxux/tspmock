package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_INDRAWER_H")
@ApiModel(value="SdIndrawerH对象", description="")
public class SdIndrawerH extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "店号")
    @TableField("GSIH_BR_ID")
    private String gsihBrId;

    @ApiModelProperty(value = "装斗单号")
    @TableField("GSIH_VOUCHER_ID")
    private String gsihVoucherId;

    @ApiModelProperty(value = "装斗日期")
    @TableField("GSIH_DATE")
    private String gsihDate;

    @ApiModelProperty(value = "清斗单号")
    @TableField("GSIH_CLE_VOUCHER_ID")
    private String gsihCleVoucherId;

    @ApiModelProperty(value = "装斗状态")
    @TableField("GSIH_STATUS")
    private String gsihStatus;

    @ApiModelProperty(value = "装斗人员")
    @TableField("GSIH_EMP")
    private String gsihEmp;

    @ApiModelProperty(value = "备注")
    @TableField("GSIH_REMAKS")
    private String gsihRemaks;

    @ApiModelProperty(value = "复核人员")
    @TableField("GSIH_EMP1")
    private String gsihEmp1;
}
