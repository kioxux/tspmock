package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.LambdaUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.SdCleandrawerH;
import com.gov.operate.entity.SdIndrawerD;
import com.gov.operate.entity.SdIndrawerH;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdCleandrawerHService;
import com.gov.operate.service.ISdIndrawerDService;
import com.gov.operate.service.ISdIndrawerHService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-13
 */
@Service
public class SdIndrawerHServiceImpl extends ServiceImpl<SdIndrawerHMapper, SdIndrawerH> implements ISdIndrawerHService {
    @Resource
    private CommonService commonService;

    @Resource
    private ISdCleandrawerHService cleandrawerhService;

    @Resource
    private ISdIndrawerDService indrawerdService;

    @Resource
    private ProductBusinessMapper productBusinessMapper;

    @Resource
    private SdCleandrawerHMapper cleandrawerhMapper;

    @Resource
    private SdStockBatchMapper sdStockBatchMapper;

    @Resource
    private SdIndrawerDMapper indrawerdMapper;

    @Resource
    private SdExamineHMapper sdExamineHMapper;


    @Override
    public Result getCleanDrawerInfo() {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //获取所有装斗信息
        List<SdIndrawerH> indrawerList = this.list(new QueryWrapper<SdIndrawerH>()
                .eq("CLIENT", user.getClient())
                .eq("GSIH_BR_ID", user.getDepId()));
        //获取装斗中已存在的清斗单号
        List<String> cleanIds = null;
        if (!indrawerList.isEmpty()) {
            cleanIds = indrawerList.stream().map(SdIndrawerH::getGsihCleVoucherId).collect(Collectors.toList());
        }
        //获取清斗单信息
        QueryWrapper<SdCleandrawerH> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("h.CLIENT", user.getClient())
                .eq("h.GSCH_BR_ID", user.getDepId())
                .eq("h.GSCH_STATUS", "1");
        if (cleanIds != null) {
            queryWrapper.notIn("h.GSCH_VOUCHER_ID", cleanIds);
        }
        queryWrapper.orderByDesc("h.GSCH_DATE", "h.GSCH_VOUCHER_ID")
                .last("limit 0,15");
        return ResultUtil.success(cleandrawerhMapper.getList(queryWrapper));
    }

    @Override
    public Result getCleanHighlightInfo(GetCleanHighlightVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        List<GetCleanHighlightDTO> list = cleandrawerhMapper.getCleanHighlightInfo(user.getClient(), user.getDepId(),
                vo.getGschVoucherId());
        //返回数据
        List<InDrawerDetailDTO> details = new ArrayList<>();
        if (!list.isEmpty()) {
            list.forEach(item -> {
//                SdStockBatch stockBatch = sdStockBatchMapper.selectOne(new QueryWrapper<SdStockBatch>()
//                        .select("GSSB_QTY")
//                        .eq("CLIENT", user.getClient())
//                        .eq("GSSB_BR_ID", user.getDepId())
//                        .eq("GSSB_PRO_ID", item.getGscdProId())
//                        .eq("GSSB_BATCH_NO", item.getGscdBatchNo())
//                        .eq("GSSB_VAILD_DATE", item.getGscdValidDate())
//                        .ne("GSSB_QTY", 0));
//                if (stockBatch != null) {
//                    InDrawerDetailDTO dto = new InDrawerDetailDTO();
//                    dto.setGsidProId(item.getGscdProId());
//                    dto.setGsidBatchNo(item.getGscdBatchNo());
//                    dto.setGsidValidDate(item.getGscdValidDate());
//                    dto.setGsidQty(stockBatch.getGssbQty());
//                    //通用信息
//                    GetFormDTO info = productBusinessMapper.getFormInfo(item.getGscdProId(), user.getClient(), user.getDepId());
//                    BeanUtils.copyProperties(info, dto);
//                    details.add(dto);
//                }
                InDrawerDetailDTO dto = new InDrawerDetailDTO();
                dto.setGsidProId(item.getGscdProId());
                dto.setGsidBatchNo(item.getGscdBatchNo());
                dto.setGsidValidDate(item.getGscdValidDate());
                dto.setGsidQty(item.getGscdQty());
                //通用信息
                GetFormDTO info = productBusinessMapper.getFormInfo(user.getClient(), user.getDepId(), item.getGscdProId());
                if (info != null) {
                    BeanUtils.copyProperties(info, dto);
                    details.add(dto);
                }
            });
        }
        return ResultUtil.success(details);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String addInDrawer(AddInDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //校验参数
        if (StringUtils.isEmpty(vo.getGsihEmp())
//                || StringUtils.isEmpty(vo.getGsihCleVoucherId())
                || vo.getDetails().isEmpty()
        ) {
            throw new CustomResultException("请填写必填信息后保存");
        }
        return addInDrawerAndDetail(vo, user, 1);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result updateInDrawer(UpdateInDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        if (CollectionUtils.isEmpty(vo.getDetails())) {
            throw new CustomResultException("明细数据不能为空");
        }
        QueryWrapper<SdIndrawerH> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", user.getClient())
                .eq("GSIH_BR_ID", user.getDepId())
                .eq("GSIH_VOUCHER_ID", vo.getGsihVoucherId());
        SdIndrawerH inH = this.getOne(queryWrapper);
        if (inH == null) {
            throw new CustomResultException("装斗单不存在");
        }
        if ("1".equals(inH.getGsihStatus())) {
            throw new CustomResultException("装斗单已审核");
        }
        //更新装斗单
        BeanUtils.copyProperties(vo, inH);
        String gsihDate = DateUtils.getCurrentDateStrYYMMDD();
        inH.setGsihDate(gsihDate);
        this.update(inH, queryWrapper);
        //删除装斗单明细 然后添加
        indrawerdMapper.delete(new QueryWrapper<SdIndrawerD>()
                .eq("CLIENT", user.getClient())
                .eq("GSID_VOUCHER_ID", vo.getGsihVoucherId()));

        for (int i = 0; i < vo.getDetails().size(); i++) {
            //新增装斗明细
            SdIndrawerD inD = new SdIndrawerD();
            BeanUtils.copyProperties(vo.getDetails().get(i), inD);
            inD.setClient(user.getClient());
            inD.setGsidVoucherId(vo.getGsihVoucherId());
            inD.setGsidDate(gsihDate);
            inD.setGsidSerial(String.valueOf(i + 1));
            indrawerdMapper.insert(inD);
        }
        return ResultUtil.success();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result checkInDrawer(CheckInDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //判断装斗编号是否为空  是 直接保存 否 修改审核状态
        if (StringUtils.isEmpty(vo.getGsihVoucherId())) {
            addInDrawerAndDetail(vo, user, 2);
        } else {
            //校验装斗单号
            QueryWrapper<SdIndrawerH> wrapper = new QueryWrapper();
            wrapper.eq("CLIENT", user.getClient())
                    .eq("GSIH_BR_ID", user.getDepId())
                    .eq("GSIH_VOUCHER_ID", vo.getGsihVoucherId());
            SdIndrawerH inH = this.getOne(wrapper);
            if (inH == null) {
                throw new CustomResultException("装斗单号不存在");
            }
            if ("1".equals(inH.getGsihStatus())) {
                throw new CustomResultException("装斗单已审核");
            }
            SdIndrawerH inclH = new SdIndrawerH();
            if ("1".equals(vo.getCheckShop())) {
                inclH.setGsihEmp1(user.getUserId());
            }
            inclH.setGsihStatus("1");
            this.update(inclH, wrapper);
            //审核之前 删除数据 在审核
            indrawerdMapper.delete(new QueryWrapper<SdIndrawerD>()
                    .eq("CLIENT", user.getClient())
                    .eq("GSID_VOUCHER_ID", inH.getGsihVoucherId())
                    .eq("GSID_DATE", inH.getGsihDate()));
            //重新插入页面数据
            List<AddInDrawerDetailVO> details = vo.getDetails();
            List<SdIndrawerD> inDs = new ArrayList<>();
            details.forEach(LambdaUtils.consumerWithIndex((item, index) -> {
                //校验参数
                if (StringUtils.isEmpty(item.getGsidProId())
                        || StringUtils.isEmpty(item.getGsidBatchNo())
                        || StringUtils.isEmpty(item.getGsidQty())
                        || StringUtils.isEmpty(item.getGsidValidDate())
                ) {
                    throw new CustomResultException("请填写必填信息后保存");
                }
                //校验装斗明细
                SdIndrawerD inDrawer = indrawerdMapper.selectOne(new QueryWrapper<SdIndrawerD>()
                        .eq("CLIENT", user.getClient())
                        .eq("GSID_VOUCHER_ID", inH.getGsihVoucherId())
                        .eq("GSID_DATE", inH.getGsihDate())
                        .eq("GSID_PRO_ID", item.getGsidProId())
                        .eq("GSID_BATCH_NO", item.getGsidBatchNo()));
                if (inDrawer != null) {
                    throw new CustomResultException("清斗明细已存在");
                }
                //新增装斗明细
                SdIndrawerD inD = new SdIndrawerD();
                BeanUtils.copyProperties(item, inD);
                inD.setClient(user.getClient());
                inD.setGsidVoucherId(inH.getGsihVoucherId());
                inD.setGsidDate(inH.getGsihDate());
                inD.setGsidQty(item.getGsidQty());
                inD.setGsidSerial(String.valueOf(index));
                inDs.add(inD);
            }));
            if (!inDs.isEmpty()) {
                indrawerdService.saveBatch(inDs);
            }
        }
        return ResultUtil.success(1);
    }

    @Override
    public List<InDrawerDetailDTO> getInDrawer(String gsihVoucherId) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        //装斗单主表
        SdIndrawerH inH = this.getOne(new QueryWrapper<SdIndrawerH>()
                .eq("CLIENT", user.getClient())
                .eq("GSIH_BR_ID", user.getDepId())
                .eq("GSIH_VOUCHER_ID", gsihVoucherId));
        if (inH == null) {
            throw new CustomResultException("装斗单不存在");
        }
        //装斗单明细
        List<SdIndrawerD> inDs = indrawerdMapper.selectList(new QueryWrapper<SdIndrawerD>()
                .eq("CLIENT", user.getClient())
                .eq("GSID_DATE", inH.getGsihDate())
                .eq("GSID_VOUCHER_ID", gsihVoucherId));
        if (inDs.isEmpty()) {
            throw new CustomResultException("装斗单明细不存在");
        }
        List<InDrawerDetailDTO> deatils = new ArrayList<>();
        inDs.forEach(item -> {
            InDrawerDetailDTO dto = new InDrawerDetailDTO();
            BeanUtils.copyProperties(item, dto);
            //商品主数据业务信息
            ProductBusiness productBusiness = productBusinessMapper.selectOne(new QueryWrapper<ProductBusiness>()
                    .select("PRO_COMMONNAME", "PRO_SPECS", "PRO_FACTORY_NAME", "PRO_PLACE", "PRO_UNIT", "PRO_REGISTER_NO")
                    .eq("CLIENT", user.getClient())
                    .eq("PRO_SITE", inH.getGsihBrId())
                    .eq("PRO_SELF_CODE", item.getGsidProId()));
            if (productBusiness == null) {
                throw new CustomResultException("商品不存在");
            }
            BeanUtils.copyProperties(productBusiness, dto);
            dto.setGsidQty(new BigDecimal(item.getGsidQty()));
            deatils.add(dto);
        });
        return deatils;
    }

    @Override
    public IPage<SdIndrawerHVO> listInDrawer(ListInDrawerVO vo) {
        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        List<String> siteCodes = new ArrayList<>();
        if (vo.getSiteCodes() != null && vo.getSiteCodes().length != 0) {
            siteCodes= Arrays.asList(vo.getSiteCodes());
        } else {
            if (StringUtils.isEmpty(user.getDepId())) {
                throw new CustomResultException("请选择门店");
            } else {
                siteCodes.add(user.getDepId());
            }
        }
        Page<SdIndrawerHVO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<SdIndrawerH> query = new QueryWrapper<>();
        query.eq(" h.CLIENT", user.getClient());
        query.in(" h.GSIH_BR_ID", siteCodes);
        if (StringUtils.isNotEmpty(vo.getGsihVoucherId())) {
            query.eq(" h.GSIH_VOUCHER_ID", vo.getGsihVoucherId());
        }
        if (StringUtils.isNotEmpty(vo.getBeginDate())) {
            query.ge(" h.GSIH_DATE", vo.getBeginDate());
        }
        if (StringUtils.isNotEmpty(vo.getEndDate())) {
            query.le(" h.GSIH_DATE", vo.getEndDate());
        }
        if (StringUtils.isNotEmpty(vo.getGsihEmp1())) {
            query.eq(" h.GSIH_EMP1", vo.getGsihEmp1());
        }
        query.orderByDesc(" h.GSIH_VOUCHER_ID");
        return baseMapper.getPage(page, user.getClient(), user.getDepId(), vo.getGsidProId(), query);
    }

    private String addInDrawerAndDetail(AddInDrawerVO vo, TokenUser user, Integer type) {
        //查询已经存在的装斗数据
        List<SdIndrawerH> hList = this.list();
        Integer num = hList.isEmpty() ? 1 : hList.size() + 1;
        //重复校验
        SdIndrawerH clean = this.getOne(new QueryWrapper<SdIndrawerH>()
                .eq("CLIENT", user.getClient())
                .eq("GSIH_BR_ID", user.getDepId())
                .eq("GSIH_VOUCHER_ID", vo.getGsihCleVoucherId())
                .eq("GSIH_EMP", vo.getGsihEmp()));
        if (clean != null) {
            throw new CustomResultException("装斗单中包含已存在数据");
        }
        //装斗日期
        String gsihDate = DateUtils.getCurrentDateStrYYMMDD();
        //装斗编号
        String gsidVoucherId = "ZD" + DateUtils.getCurrentYearStr()
                + String.format("%7d", num).replace(" ", "0");
        //新增装斗
        SdIndrawerH inH = new SdIndrawerH();
        BeanUtils.copyProperties(vo, inH);
        inH.setClient(user.getClient());
        inH.setGsihVoucherId(gsidVoucherId);
        inH.setGsihBrId(user.getDepId());
        inH.setGsihStatus(type == 1 ? "0" : "1");
        inH.setGsihDate(gsihDate);
        baseMapper.insert(inH);
        //循环处理装斗数据
        for (int i = 0; i < vo.getDetails().size(); i++) {
            AddInDrawerDetailVO drawerVO = vo.getDetails().get(i);
            //校验参数
            if (StringUtils.isEmpty(drawerVO.getGsidProId())
                    || StringUtils.isEmpty(drawerVO.getGsidBatchNo())
                    || StringUtils.isEmpty(drawerVO.getGsidQty())
                    || StringUtils.isEmpty(drawerVO.getGsidValidDate())
            ) {
                throw new CustomResultException("请填写必填信息后保存");
            }
            //校验装斗明细
            SdIndrawerD inDrawerD = indrawerdMapper.selectOne(new QueryWrapper<SdIndrawerD>()
                    .eq("CLIENT", user.getClient())
                    .eq("GSID_VOUCHER_ID", gsidVoucherId)
                    .eq("GSID_DATE", gsihDate)
                    .eq("GSID_PRO_ID", drawerVO.getGsidProId())
                    .eq("GSID_BATCH_NO", drawerVO.getGsidBatchNo()));
            if (inDrawerD != null) {
                throw new CustomResultException("装斗明细已存在");
            }
            //新增装斗明细
            SdIndrawerD cleanD = new SdIndrawerD();
            BeanUtils.copyProperties(drawerVO, cleanD);
            cleanD.setClient(user.getClient());
            cleanD.setGsidVoucherId(gsidVoucherId);
            cleanD.setGsidDate(gsihDate);
            cleanD.setGsidQty(drawerVO.getGsidQty());
            cleanD.setGsidSerial(String.valueOf(i + 1));
            indrawerdMapper.insert(cleanD);
        }
        return gsidVoucherId;
    }

    @Override
    public List<CleanDrawerDetailDTO> getDrawerList() {
        TokenUser user = commonService.getLoginInfo();
        String client = user.getClient();
        String store = user.getDepId();
        List<CleanDrawerDetailDTO> outList = sdExamineHMapper.getDrawerList(client, store);
        List<CleanDrawerDetailDTO> detailDTOList = sdExamineHMapper.getList(client, store);
        if (CollectionUtils.isEmpty(outList)) {
            throw new CustomResultException("暂无90天未清斗中药饮片!");
        }
        for (CleanDrawerDetailDTO dto : detailDTOList) {
            for (int i = 0; i < outList.size(); i++) {
                if (outList.get(i).getGscdProId().equals(dto.getGscdProId()) && outList.get(i).getGscdBatchNo().equals(dto.getGscdBatchNo())) {
                    outList.remove(i);
                    i--;
                }
            }

        }
        return outList;
    }
}
