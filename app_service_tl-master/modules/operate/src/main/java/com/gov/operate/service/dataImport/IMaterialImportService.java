package com.gov.operate.service.dataImport;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.MaterialImportVO;
import com.gov.operate.dto.dataImport.materialDoc.InsertItemVO;
import com.gov.operate.dto.dataImport.materialDoc.SelectImportDTO;
import com.gov.operate.entity.MaterialDocImp;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public interface IMaterialImportService extends SuperService<MaterialDocImp> {

    void deleteMaterialItem(Integer id);

    void executeImport(Integer id);

    void insertItem(InsertItemVO materialDocImp);

    IPage<SelectImportDTO> selectImport(MaterialImportVO materialImportVO);

    /**
     * 更新状态为‘执行中’
     */
    void updateStatusToExecuting(Integer id, String userId);

    /**
     * 导入操作
     */
    MaterialDocImp importDataByNormalType(MaterialDocImp materialDocImpExit, String gmdiType);
}
