package com.gov.operate.dto.marketing;

import com.gov.operate.entity.SdMarketingBasicAttac;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SdMarketingBasicAttacDTO extends SdMarketingBasicAttac {

    /**
     * 路径PS_Url
     */
    private String gsmbaPsUrl;

    /**
     * 路径图片_Url
     */
    private String gsmbaJpgUrl;

    /**
     * 路径场景_Url
     */
    private String gsmbaScenesUrl;
}
