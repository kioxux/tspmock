package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/14 11:25
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GetProductVO {
    /**
     * 搜索内容 模糊匹配
     * 商品自编码PRO_SELF_CODE，
     * 通用名称PRO_COMMONNAME，
     * 助记码PRO_PYM，
     * 商品名PRO_NAME，
     * 国际条形码1PRO_BARCODE
     */
    private String searcheContent;

    private Integer pageSize;

    private Integer pageNum;

}
