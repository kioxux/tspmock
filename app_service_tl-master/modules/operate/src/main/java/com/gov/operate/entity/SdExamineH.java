package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_EXAMINE_H")
@ApiModel(value="SdExamineH对象", description="")
public class SdExamineH extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "店号")
    @TableField("GSEH_BR_ID")
    private String gsehBrId;

    @ApiModelProperty(value = "验收单号")
    @TableField("GSEH_VOUCHER_ID")
    private String gsehVoucherId;

    @ApiModelProperty(value = "验收日期")
    @TableField("GSEH_DATE")
    private String gsehDate;

    @ApiModelProperty(value = "收货单号")
    @TableField("GSEH_VOUCHER_ACCEPT_ID")
    private String gsehVoucherAcceptId;

    @ApiModelProperty(value = "发货地点")
    @TableField("GSEH_FROM")
    private String gsehFrom;

    @ApiModelProperty(value = "收货地点")
    @TableField("GSEH_TO")
    private String gsehTo;

    @ApiModelProperty(value = "单据类型")
    @TableField("GSEH_TYPE")
    private String gsehType;

    @ApiModelProperty(value = "验收状态")
    @TableField("GSEH_STATUS")
    private String gsehStatus;

    @ApiModelProperty(value = "验收金额")
    @TableField("GSEH_TOTAL_AMT")
    private BigDecimal gsehTotalAmt;

    @ApiModelProperty(value = "验收数量")
    @TableField("GSEH_TOTAL_QTY")
    private String gsehTotalQty;

    @ApiModelProperty(value = "备注")
    @TableField("GSEH_REMAKS")
    private String gsehRemaks;


}
