package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetFeedBackListVO extends Pageable {

    // @NotBlank(message = "员工编号不能为空")
    private String userId;

    // @NotBlank(message = "标题不能为空")
    private String title;

    // @NotBlank(message = "类别不能为空")
    private String type;

    // @NotBlank(message = "创建日期开始日期不能为空")
    private String userCreDateStart;

    // @NotBlank(message = "创建日期结束日期不能为空")
    private String userCreDateEnd;

}
