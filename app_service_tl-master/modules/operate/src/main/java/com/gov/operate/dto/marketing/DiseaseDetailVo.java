package com.gov.operate.dto.marketing;

import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:29 2021/8/13
 * @Description：疾病详情信息实体
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class DiseaseDetailVo {

    /**
     * 疾病大类编码
     */
    private String diseaseTagCode;

    /**
     * 疾病大类描述
     */
    private String diseaseTagName;

    /**
     * 疾病中类编码
     */
    private String diseaseCode;

    /**
     * 疾病中类描述
     */
    private String diseaseName;

}
