package com.gov.operate.dto.ssp;

import lombok.Data;

import java.util.Date;

@Data
public class SspUserSupplierDTO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 有效期起
     */
    private Date expiryStartDate;

    /**
     * 有效期至
     */
    private Date expiryEndDate;


}
