package com.gov.operate.dto;

import lombok.Data;

@Data
public class UserRoleDto {

    private String client;

    private String funcCode;

    private String userId;

    private String oldUserId;
}
