package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class RechargeCardListVO extends Pageable {

    private String gsrcName;

    private String gsrcMobile;

    private String gsrcStatus;

    private String gsrcId;

    private String gsrcpKateStart;

    private String gsrcpKateEnd;

    private String gsrccOldCardId;

    private String gsrccNewCardId;

    private String gsrccType;
    /**
     * 会员卡号
     */
    private String gsrcMemberCardId;
    /**
     * 支付编码
     */
    private String gsspmId;
    /**
     * 门店列表
     */
    private List<String> stoList;
}
