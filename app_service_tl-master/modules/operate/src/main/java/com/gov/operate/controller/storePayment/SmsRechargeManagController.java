package com.gov.operate.controller.storePayment;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.smsRecharge.ISmsRechargeManagService;
import com.gov.operate.service.smsRecharge.ISmsRechargeRecordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-12-09
 */
@RestController
@RequestMapping("/smsRechargeManag")
public class SmsRechargeManagController {

    @Resource
    private ISmsRechargeManagService iSmsRechargeManagService;

    @Log("客户短信统计")
    @ApiOperation(value = "客户短信统计")
    @PostMapping("getSmsSendStatistic")
    public Result getSmsSendStatistic(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                      @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                      @RequestJson(value = "client", required = false) String client) {
        return iSmsRechargeManagService.getSmsSendStatistic(pageNum, pageSize, client);
    }

    @Log("短信发送记录")
    @ApiOperation(value = "短信发送记录")
    @PostMapping("getSmsSendRecord")
    public Result getSmsSendRecord(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                   @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                   @RequestJson(value = "client", required = false) String client,
                                   @RequestJson(value = "gssSendTimeStart", required = false) String gssSendTimeStart,
                                   @RequestJson(value = "gssSendTimeEnd", required = false) String gssSendTimeEnd) {
        return iSmsRechargeManagService.getSmsSendRecord(pageNum, pageSize, client, gssSendTimeStart, gssSendTimeEnd);
    }

    @Log("客户短信充值记录")
    @ApiOperation(value = "客户短信充值记录")
    @PostMapping("getRechargeRecord")
    public Result getRechargeRecord(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                    @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                    @RequestJson(value = "client", required = false) String client,
                                    @RequestJson(value = "gsrrRechargeTimeStart", required = false) String gsrrRechargeTimeStart,
                                    @RequestJson(value = "gsrrRechargeTimeEnd", required = false) String gsrrRechargeTimeEnd) {
        return iSmsRechargeManagService.getRechargeRecord(pageNum, pageSize, client, gsrrRechargeTimeStart, gsrrRechargeTimeEnd);
    }

    @Log("客户集合")
    @ApiOperation(value = "客户集合")
    @GetMapping("getClientList")
    public Result getClientList() {
        return iSmsRechargeManagService.getClientList();
    }
}

