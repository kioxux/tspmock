package com.gov.operate.enums;

import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 星座枚举
 */
public enum ConstellationEnum {
    Aquarius("1","水瓶座","0120","0218"),
    Pisces("2","双鱼座","0219","0320"),
    Aries("3","白羊座","0321","0419"),
    Taurus("4","金牛座","0420","0520"),
    Gemini("5","双子座","0521","0621"),
    Cancer("6","巨蟹座","0622","0722"),
    Leo("7","狮子座","0723","0822"),
    Virgo("8","处女座","0823","0922"),
    Libra("9","天枰座","0923","1023"),
    Scorpio("10","天蝎座","1024","1122"),
    Sagittarius ("11","射手座","1123","1221"),
    Capricorn("12","摩羯座","1222","0119");

    private  String constellationCode;
    private  String constellationName;
    private  String constellationStartDate;
    private  String constellationEndDate;


    ConstellationEnum(String constellationCode, String constellationName, String constellationStartDate, String constellationEndDate) {
        this.constellationCode = constellationCode;
        this.constellationName = constellationName;
        this.constellationStartDate = constellationStartDate;
        this.constellationEndDate = constellationEndDate;
    }

    public String getConstellationCode() {
        return constellationCode;
    }

    public void setConstellationCode(String constellationCode) {
        this.constellationCode = constellationCode;
    }

    public String getConstellationName() {
        return constellationName;
    }

    public void setConstellationName(String constellationName) {
        this.constellationName = constellationName;
    }

    public String getConstellationStartDate() {
        return constellationStartDate;
    }

    public void setConstellationStartDate(String constellationStartDate) {
        this.constellationStartDate = constellationStartDate;
    }

    public String getConstellationEndDate() {
        return constellationEndDate;
    }

    public void setConstellationEndDate(String constellationEndDate) {
        this.constellationEndDate = constellationEndDate;
    }
}
