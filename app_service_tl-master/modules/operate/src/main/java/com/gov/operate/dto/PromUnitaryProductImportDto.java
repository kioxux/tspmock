package com.gov.operate.dto;

import com.gov.common.entity.dataImport.PromUnitaryProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromUnitaryProductImportDto extends PromUnitaryProduct {
    private String proSelfCode;

    private String proCommonname;

    private String proName;

    private String proSpecs;

    private String proFactoryName;

    /**
     * 是否积分
     */
    private String gspusInteFlag;

    /**
     * 积分倍率
     */
    private String gspusInteRate;
}
