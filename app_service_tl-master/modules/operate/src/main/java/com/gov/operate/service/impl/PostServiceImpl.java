package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.entity.Post;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.PostMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IPostService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-28
 */
@Service
public class PostServiceImpl extends ServiceImpl<PostMapper, Post> implements IPostService {

    @Resource
    private CommonService commonService;

    /**
     * 岗位列表
     */
    @Override
    public List<Post> getPostList() {
        TokenUser user = commonService.getLoginInfo();
        return this.list(new QueryWrapper<Post>().eq("CLENT", user.getClient()));
    }
}
