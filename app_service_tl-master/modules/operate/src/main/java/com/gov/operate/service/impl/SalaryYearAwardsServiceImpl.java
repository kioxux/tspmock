package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryYearAwards;
import com.gov.operate.mapper.SalaryYearAwardsMapper;
import com.gov.operate.service.ISalaryYearAwardsService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryYearAwardsServiceImpl extends ServiceImpl<SalaryYearAwardsMapper, SalaryYearAwards> implements ISalaryYearAwardsService {

    /**
     * 设定年终奖
     */
    @Override
    public void saveYearAwards(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除基本工资级别 原始数据
        this.remove(new QueryWrapper<SalaryYearAwards>().eq("GSYA_GSP_ID", gspId));
        List<SalaryYearAwards> salaryYearAwardsList = vo.getSalaryYearAwardsList();
        if (CollectionUtils.isEmpty(salaryYearAwardsList)) {
            throw new CustomResultException("年终奖 数据等级不能为空");
        }
        for (int i = 0; i < salaryYearAwardsList.size(); i++) {
            SalaryYearAwards salaryYearAwards = salaryYearAwardsList.get(i);
            if (StringUtils.isEmpty(salaryYearAwards.getGsyaSalesMin())) {
                throw new CustomResultException(MessageFormat.format("季度奖等级[{0}] 日销售额最小值不能为空", i + 1));
            }
            if (StringUtils.isEmpty(salaryYearAwards.getGsyaSalesMax())) {
                throw new CustomResultException(MessageFormat.format("季度奖等级[{0}] 日销售额最大值不能为空", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryYearAwards.getGsyaCommision())) {
                throw new CustomResultException(MessageFormat.format("季度奖等级[{0}] 提成比例不能为空", i + 1));
            }
            salaryYearAwards.setGsyaGspId(gspId);
            salaryYearAwards.setGsyaSort(i + 1);
            // 提成比例百分数转小数
            salaryYearAwards.setGsyaCommision(DecimalUtils.toDecimal(salaryYearAwards.getGsyaCommision()));
        }
        // 校验范围重叠的问题
        salaryYearAwardsList.sort(Comparator.comparing(SalaryYearAwards::getGsyaSalesMin));
        for (int i = 0; i < salaryYearAwardsList.size(); i++) {
            if (i == 0) {
                continue;
            }
            SalaryYearAwards current = salaryYearAwardsList.get(i);
            SalaryYearAwards previous = salaryYearAwardsList.get(i - 1);
            if (current.getGsyaSalesMin().compareTo(previous.getGsyaSalesMax()) <= 0) {
                throw new CustomResultException(MessageFormat.format("年终奖日销售区间[{0}~{1}]与[{2}~{3}]存在重叠区,请重新输入",
                        previous.getGsyaSalesMin(), previous.getGsyaSalesMax(), current.getGsyaSalesMin(), current.getGsyaSalesMax()));
            }
        }
        this.saveBatch(salaryYearAwardsList);
    }

    @Override
    public List<SalaryYearAwards> getYearAwardsList(Integer gspId) {
        List<SalaryYearAwards> list = this.list(new QueryWrapper<SalaryYearAwards>()
                .eq("GSYA_GSP_ID", gspId)
                .orderByAsc("GSYA_SORT"));
        list.forEach(awards -> {
            awards.setGsyaCommision(awards.getGsyaCommision().multiply(Constants.ONE_HUNDRED));
        });
        return list;
    }
}
