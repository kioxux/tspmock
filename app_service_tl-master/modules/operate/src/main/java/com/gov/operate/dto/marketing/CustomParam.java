package com.gov.operate.dto.marketing;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.07
 */
@Data
public class CustomParam {

    /**
     * 促销ID
     */
    private Integer PromId;

    /**
     * 标题
     */
    private String label;

    /**
     * 值
     */
    private String value;

    /**
     * 字段类型
     */
    private String gspvsColumnsType;

    /**
     * 字段长度
     */
    private String gspvsColumnsLength;

    /**
     * 列名
     */
    private String column;
}

