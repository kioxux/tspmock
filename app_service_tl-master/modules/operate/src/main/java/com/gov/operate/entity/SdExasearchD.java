package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-08-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_EXASEARCH_D")
@ApiModel(value="SdExasearchD对象", description="")
public class SdExasearchD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询ID")
    @TableField("GSE_SEARCHID")
    private String gseSearchid;

    @ApiModelProperty(value = "查询主题ID")
    @TableField("GSE_THEID")
    private String gseTheid;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSE_CARD_ID")
    private String gseCardId;

    @ApiModelProperty(value = "会员名称")
    @TableField("GSE_MENNAME")
    private String gseMenname;

    @ApiModelProperty(value = "手机")
    @TableField("GSE_MOBILE")
    private String gseMobile;

    @ApiModelProperty(value = "性别")
    @TableField("GSE_SEX")
    private String gseSex;

    @ApiModelProperty(value = "年龄")
    @TableField("GSE_AGE")
    private String gseAge;

    @ApiModelProperty(value = "生日")
    @TableField("GSE_BIRTH")
    private String gseBirth;

    @ApiModelProperty(value = "当前积分")
    @TableField("GSE_INTEGRAL")
    private BigDecimal gseIntegral;

    @ApiModelProperty(value = "所属门店编号")
    @TableField("GSE_BR_ID")
    private String gseBrId;

    @ApiModelProperty(value = "所属门店名称")
    @TableField("GSE_BR_NAME")
    private String gseBrName;

    @ApiModelProperty(value = "会员卡类型")
    @TableField("GSE_CLASS_ID")
    private String gseClassId;

    @ApiModelProperty(value = "最后积分日期")
    @TableField("GSE_INTEGRAL_LASTDATE")
    private String gseIntegralLastdate;

    @ApiModelProperty(value = "会员级别")
    @TableField("GSE_GLID")
    private String gseGlid;

    @ApiModelProperty(value = "会员微信")
    @TableField("GSE_WXID")
    private String gseWxid;

    @ApiModelProperty(value = "是否发送短信")
    @TableField("GSE_SMS")
    private String gseSms;

    @ApiModelProperty(value = "是否发送微信")
    @TableField("GSE_WX")
    private String gseWx;

    @ApiModelProperty(value = "信息创建时间")
    @TableField("GSE_CRETIME")
    private String gseCretime;

    @ApiModelProperty(value = "信息发送时间")
    @TableField("GSE_SENDTIME")
    private String gseSendtime;

    @ApiModelProperty(value = "短信是否发送成功")
    @TableField("GSE_STATUS")
    private String gseStatus;

    @ApiModelProperty(value = "营销活动id")
    @TableField("GSE_MARKETID")
    private String gseMarketid;

    @ApiModelProperty(value = "1-短信, 2-电话")
    @TableField("GSE_NOTICE_TYPE")
    private String gseNoticeType;

    @ApiModelProperty(value = "模板ID")
    @TableField("GSE_TEMPID")
    private String gseTempid;


}
