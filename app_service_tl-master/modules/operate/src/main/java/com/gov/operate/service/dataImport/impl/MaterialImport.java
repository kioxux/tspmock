package com.gov.operate.service.dataImport.impl;

import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.dataImport.MarketingComponent;
import com.gov.common.entity.dataImport.MaterialDocData;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.validate.ExcelValidate;
import com.gov.operate.dto.dataImport.materialDoc.GaiaMaterialAssessDO;
import com.gov.operate.entity.MaterialDoc;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.MaterialDocMapper;
import com.gov.operate.mapper.MaterialImportMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IMaterialDocService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.velocity.util.ArrayListWrapper;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.MessageFormat;
import java.util.*;

/**
 * Description:
 *
 * @author: zp
 * @date: 2021.04.06
 */
@Service
public class MaterialImport extends BusinessImport {

    @Resource
    private CommonService commonService;

    @Resource
    private IMaterialDocService materialDocService;

    @Resource
    private MaterialImportMapper materialImportMapper;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        Integer gmdiType = (Integer) field.get("gmdiType");
        List<String> errorList = new ArrayList<>();
        List<MaterialDoc> dataList = new ArrayList();
        Set<String> clientSet = new HashSet<String>();
        Map<String, String> matIdMap = new HashMap<String, String>();
        Map<String, Integer> matLNoMap = new HashMap<String, Integer>();

        Set<String> checkSet = new HashSet<String>();
        //查询条件过滤
        List<MaterialDocData> querySiteList = new ArrayList<>();
        List<MaterialDocData> queryProCodeList = new ArrayList<>();
        List<MaterialDocData> queryProBatchList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            MaterialDocData data = (MaterialDocData) map.get(key);

            String site = "site|" + data.getClient() + "|" + data.getSiteCode();
            if (!checkSet.contains(site)) {
                checkSet.add(site);
                querySiteList.add(data);
            }

            String code = "code|" + data.getClient() + "|" + data.getSiteCode() + "|" + data.getProCode();
            if (!checkSet.contains(code)) {
                checkSet.add(code);
                queryProCodeList.add(data);
            }

            String batch = "batch|" + data.getClient() + "|" + data.getSiteCode() + "|" + data.getProCode() + "|" + data.getBatch();
            if (!checkSet.contains(batch)) {
                checkSet.add(batch);
                queryProBatchList.add(data);
            }
        }

        List<GaiaMaterialAssessDO> checkMaterialAssessList = materialImportMapper.selectMaterialAssessExit(queryProCodeList);
        //準備校驗參數
        List<String> siteResult = materialImportMapper.selectSiteList(querySiteList);
        List<Map<String, String>> proCodeResultlist = materialImportMapper.selectPCodeList(queryProCodeList);
        List<String> batchResult = materialImportMapper.selectBatchList(queryProBatchList);

        Map<String, String> QMaxPidResult = materialImportMapper.selectMaxPid("QC" + DateUtils.getCurrentYearStr());

        Map<String, String> qPcodeResult = new HashMap<String, String>();
        for (Map<String, String> m : proCodeResultlist) {
            qPcodeResult.put(m.get("PCODE"), m.get("PUNIT"));
        }

        int mPoId = 1;
        if (QMaxPidResult != null && StringUtils.isEmpty(QMaxPidResult.get("MPOID"))) {
            mPoId = Integer.parseInt(QMaxPidResult.get("MPOID")) + 1;
        }

        int mDnid = 1;
        if (QMaxPidResult != null && StringUtils.isEmpty(QMaxPidResult.get("MDNID"))) {
            mDnid = Integer.parseInt(QMaxPidResult.get("MPOID")) + 1;
        }

        BigDecimal fqty = new BigDecimal("0");
        BigDecimal famt = new BigDecimal("0");
        int fcount = 0;

        //封裝數據
        for (Integer key : map.keySet()) {
            fcount++;
            MaterialDocData data = (MaterialDocData) map.get(key);
            if (CollectionUtils.isNotEmpty(checkMaterialAssessList) && !ObjectUtils.isEmpty(gmdiType) && OperateEnum.GmdiType.NORMAL.getCode().intValue() == gmdiType) {
                boolean match = checkMaterialAssessList.stream().filter(Objects::nonNull)
                        .anyMatch(item -> item.getClient().equals(data.getClient())
                                && item.getMatAssessSite().equals(data.getSiteCode())
                                && item.getMatProCode().equals(data.getProCode()));
                if (match) {
                    errorList.add(MessageFormat.format("第{0}行：[加盟商:{1},地点:{2} 商品:{3}]已经存在", key + 1, data.getClient(), data.getSiteCode(), data.getProCode()));
                }
                continue;
            }

            String dataClient = data.getClient();
            MaterialDoc materialDoc = new MaterialDoc();

            if (!siteResult.contains(data.getSiteCode())) {
                errorList.add(MessageFormat.format("第{0}行：验证地点失败", key + 1));
                continue;
            }
            materialDoc.setMatSiteCode(data.getSiteCode());

            if (!qPcodeResult.containsKey(data.getProCode())) {
                errorList.add(MessageFormat.format("第{0}行：验证商品失败", key + 1));
                continue;
            }
            materialDoc.setMatProCode(data.getProCode());

            if (!batchResult.contains(data.getBatch())) {
                errorList.add(MessageFormat.format("第{0}行：验证批次失败", key + 1));
                continue;
            }
            materialDoc.setMatBatch(data.getBatch());

            materialDoc.setClient(dataClient);
            int matLNo = 1;
            if (clientSet.contains(dataClient)) {
                materialDoc.setMatId(matIdMap.get(dataClient));
                matLNo = matLNoMap.get(dataClient) + 1;
                matLNoMap.put(dataClient, matLNo);
            } else {
                int i = (int) ((Math.random() * 9 + 1) * 100000000);
                String matId = "WL" + DateUtils.getCurrentDateTimeStrFull() + i;
                materialDoc.setMatId("WL" + DateUtils.getCurrentDateTimeStrFull() + i);
                matIdMap.put(dataClient, matId);

                matLNoMap.put(dataClient, 1);
                clientSet.add(dataClient);
            }

            materialDoc.setMatLineNo(String.valueOf(matLNo));
            materialDoc.setMatYear(DateUtils.getCurrentYearStr());
            materialDoc.setMatType("QC");
            materialDoc.setMatDocDate(data.getMaterialDate());
            materialDoc.setMatPostDate(data.getMaterialDate());
            materialDoc.setMatLocationCode("1000");

            BigDecimal qty = new BigDecimal(data.getQty());
            BigDecimal batAmt = new BigDecimal(data.getBatchAmt());
            BigDecimal rateAmt = new BigDecimal(data.getRateAmt());
            fqty = fqty.add(qty);
            famt = famt.add(batAmt);
            if (!((qty.signum() >= 0 && batAmt.signum() >= 0 && rateAmt.signum() >= 0)
                    || (qty.signum() <= 0 && batAmt.signum() <= 0 && rateAmt.signum() <= 0))) {
                errorList.add(MessageFormat.format("第{0}行：库存数量,库存金额,库存税额 应该[全部大于等于0或全部小于等于0]", key + 1));
                continue;
            }

            materialDoc.setMatQty(qty.abs());
            materialDoc.setMatBatAmt(batAmt.abs());
            materialDoc.setMatUint(qPcodeResult.get(data.getProCode()));
            materialDoc.setMatMovAmt(batAmt.abs());
            materialDoc.setMatRateBat(rateAmt.abs());
            materialDoc.setMatRateMov(rateAmt.abs());
            materialDoc.setMatDebitCredit(qty.signum() == 1 ? "S" : "H");
            materialDoc.setMatPoId("QC" + DateUtils.getCurrentYearStr() + String.format("%06d", matLNo + mPoId));
            materialDoc.setMatPoLineno(String.valueOf(matLNo));
            materialDoc.setMatDnId("QC" + DateUtils.getCurrentYearStr() + String.format("%06d", matLNo + mDnid));
            materialDoc.setMatDnLineno(String.valueOf(matLNo));
            materialDoc.setMatCreateBy("SYSTEM");
            materialDoc.setMatCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            materialDoc.setMatCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            materialDoc.setMatAddAmt(batAmt.abs());
            materialDoc.setMatAddTax(rateAmt.abs());

            dataList.add(materialDoc);
        }
        if (CollectionUtils.isNotEmpty(dataList)) {
            materialDocService.saveBatch(dataList);
        }
        if (!ObjectUtils.isEmpty(gmdiType) && OperateEnum.GmdiType.NORMAL.getCode().intValue() == gmdiType
                && CollectionUtils.isNotEmpty(queryProCodeList)
                && CollectionUtils.isEmpty(checkMaterialAssessList)) {
            materialImportMapper.materialAssessBatchSave(queryProCodeList);
        }

        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setMessage("导入失败");
            result.setData(errorList);
            return result;
        }
        //导入记录
        Map<String, Object> successData = new HashMap<>();
        successData.put("gmdiCount", fcount);
        successData.put("gmdiQty", fqty);
        successData.put("gmdiAmt", famt);
        return ResultUtil.success(successData);
    }


}
