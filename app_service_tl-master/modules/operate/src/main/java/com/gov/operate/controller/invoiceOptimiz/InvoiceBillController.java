package com.gov.operate.controller.invoiceOptimiz;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.SelectWarehousingVO;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDto;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.invoiceOptimiz.InvoiceBillOptimizService;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.16
 */
@Validated
@RestController
@RequestMapping("invoiceBill")
public class InvoiceBillController {

    @Resource
    private InvoiceBillOptimizService invoiceBillOptimizService;

    @Log("发票对账")
    @ApiOperation(value = "发票对账")
    @PostMapping("invoiceBill")
    public Result invoiceBill(@RequestBody DocumentBillDto documentBillDto) {
        invoiceBillOptimizService.invoiceBill(documentBillDto);
        return ResultUtil.success();
    }

    @Log("单据集合")
    @ApiOperation(value = "单据集合")
    @PostMapping("selectDocumentList")
    public Result selectDocumentList(@RequestBody SelectWarehousingVO vo) {
        return ResultUtil.success(invoiceBillOptimizService.selectDocumentList(vo));
    }

    @Log("可登记发票")
    @ApiOperation(value = "可登记发票")
    @PostMapping("selectInvoiceList")
    public Result selectInvoiceList(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                    @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                    @RequestJson(value = "siteCode", required = false) String siteCode,
                                    @RequestJson(value = "supCode", required = false) String supCode,
                                    @RequestJson(value = "invoiceNum", required = false) String invoiceNum,
                                    @RequestJson(value = "invoiceDateStart", required = false) String invoiceDateStart,
                                    @RequestJson(value = "invoiceDateEnd", required = false) String invoiceDateEnd
    ) {
        return invoiceBillOptimizService.selectInvoiceList(pageNum, pageSize, siteCode, supCode, invoiceNum, invoiceDateStart, invoiceDateEnd);
    }

}
