package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHORITY_USER_ROLES")
@ApiModel(value="AuthorityUserRoles对象", description="")
public class AuthorityUserRoles extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "角色Id")
    @TableField("GAUR_ROLE_ID")
    private Integer gaurRoleId;

    @ApiModelProperty(value = "用户id")
    @TableField("GAUR_USER_ID")
    private String gaurUserId;

    @ApiModelProperty(value = "创建日期")
    @TableField("GAUR_CJRQ")
    private String gaurCjrq;

    @ApiModelProperty(value = "创建时间")
    @TableField("GAUR_CJSJ")
    private String gaurCjsj;

    @ApiModelProperty(value = "创建人")
    @TableField("GAUR_CJR")
    private String gaurCjr;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("GAUR_CJRXM")
    private String gaurCjrxm;

    @ApiModelProperty(value = "是否删除0是/1否（默认否）")
    @TableField("GAUR_SFSQ")
    private String gaurSfsq;

    @ApiModelProperty(value = "修改日期")
    @TableField("GAUR_XGRQ")
    private String gaurXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("GAUR_XGSJ")
    private String gaurXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("GAUR_XGR")
    private String gaurXgr;

    @ApiModelProperty(value = "修改人姓名")
    @TableField("GAUR_XGRXM")
    private String gaurXgrxm;


}
