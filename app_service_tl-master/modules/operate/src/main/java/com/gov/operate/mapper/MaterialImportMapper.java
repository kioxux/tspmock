package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.entity.dataImport.MaterialDocData;
import com.gov.operate.dto.MaterialImportVO;
import com.gov.operate.dto.dataImport.materialDoc.GaiaMaterialAssessDO;
import com.gov.operate.dto.dataImport.materialDoc.SelectImportDTO;
import com.gov.operate.entity.MaterialDocImp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MaterialImportMapper extends BaseMapper<MaterialDocImp> {

    List<Map<String, String>> selectPCodeList(@Param("params") List<MaterialDocData> list);

    List<String> selectBatchList(@Param("params") List<MaterialDocData> list);

    List<String> selectSiteList(@Param("params") List<MaterialDocData> list);

    Map<String, String> selectMaxPid(@Param("param") String str);

    /**
     * 物料凭证导入履历列表
     *
     * @param page 分页信息
     * @param vo   参数
     * @return
     */
    IPage<SelectImportDTO> selectItem(Page<SelectImportDTO> page, @Param("vo") MaterialImportVO vo);

    /**
     * 已经导入的商品列表
     */
    List<GaiaMaterialAssessDO> selectMaterialAssessExit(@Param("list") List<MaterialDocData> queryProCodeList);

    /**
     * 物料评估表批量保存
     */
    void materialAssessBatchSave(@Param("list") List<MaterialDocData> queryProCodeList);

}
