package com.gov.operate.dto;

import com.gov.operate.entity.SdMarketingPrd;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class GetProductListToCheckDTO extends SdMarketingPrd {

    /**
     * 地点
     */
    private String proSite;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 成分
     */
    private String proCompclass;


    /**
     * 成分
     */
    private String proCompclassName;


    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 零售价
     */
    private BigDecimal priceNormal;

    /**
     * 已选 0：未选，1：已选
     */
    private Integer selFlg;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 参考毛利率
     */
    private BigDecimal proMll;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 零售价
     */
    private BigDecimal gsppPriceNormal;
    /**
     * 进货价
     */
    private BigDecimal proCgj;
    /**
     * 毛利率
     */
    private BigDecimal mll;

    /**
     * 成分
     */
    private String componentId;

    /**
     * 促销名称
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 促销方式
     */
    private String gspvsMode;

    /**
     * 促销方式描述
     */
    private String gspvsModeName;

    /**
     * 促销类型
     */
    private String gspvsType;

    /**
     * 促销类型描述
     */
    private String gspvsTypeName;

    /**
     * 促销类型名
     */
    private String typeName;

    /**
     * 阶梯
     */
    private String gsphPart;
    /**
     * 0未选，1已选
     */
    private Integer seleted;

    /**
     * 赠品ID
     */
    private Integer giveawayId;

    /**
     * 促销id
     */
    private Integer promId;

    /**
     * 营销活动加盟商分配表主键
     */
    private Integer marketingId;

    /**
     * 库存
     */
    private BigDecimal wmKcsl;


}
