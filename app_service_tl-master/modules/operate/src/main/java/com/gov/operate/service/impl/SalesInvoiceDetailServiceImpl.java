package com.gov.operate.service.impl;

import com.gov.operate.entity.SalesInvoiceDetail;
import com.gov.operate.mapper.SalesInvoiceDetailMapper;
import com.gov.operate.service.ISalesInvoiceDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-02-07
 */
@Service
public class SalesInvoiceDetailServiceImpl extends ServiceImpl<SalesInvoiceDetailMapper, SalesInvoiceDetail> implements ISalesInvoiceDetailService {



}
