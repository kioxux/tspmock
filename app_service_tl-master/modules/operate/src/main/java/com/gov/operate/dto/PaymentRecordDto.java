package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.20
 */
@Data
public class PaymentRecordDto extends Pageable {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 采购地点
     */
    private String site;
    /**
     * 单体店
     */
    private String stoCode;

    @ApiModelProperty("供应商编号")
    private String supSelfCode;
    @ApiModelProperty("单据号")
    private String paymentOrderNo;
    @ApiModelProperty("单据付款日期")
    private String paymentOrderDateStart;
    @ApiModelProperty("单据付款日期")
    private String paymentOrderDateEnd;
    @ApiModelProperty("审批状态")
    private Integer approvalStatus;
    @ApiModelProperty("审批完成日期")
    private String gpaApprovalDateStart;
    @ApiModelProperty("审批完成日期")
    private String gpaApprovalDateEnd;
    /**
     * 备注
     */
    private String remark;

    /**
     * 工作流描述
     */
    private String wfDescription;
}
