package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdMarketingSearch;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-23
 */
public interface ISdMarketingSearchService extends SuperService<SdMarketingSearch> {

    /**
     * 常用查询列表
     */
    List<SdMarketingSearch> getCommonSearchList(GetCommonSearchListVO vo);

    /**
     * 常用查询保存
     */
    void saveCommonSearch(SaveCommonSearchVO searchVO);

    /**
     * 常用查询删除
     */
    void deleteCommonSearch(String gsmsId);

    /**
     * 常用查询覆盖
     */
    void editCommonSearch(EditCommonSearchVO vo);

    /**
     * 电子券主题列表
     *
     * @return
     */
    Result getElectronThemeList(ElectronThemeDTO dto);

    /**
     * 电子券异动保存
     *
     * @param dto
     * @return
     */
    Result saveElectronChange(SdElectronChangeDTO dto);

}
