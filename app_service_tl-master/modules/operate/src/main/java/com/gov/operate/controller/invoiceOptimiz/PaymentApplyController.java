package com.gov.operate.controller.invoiceOptimiz;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.PaymentApplyVO;
import com.gov.operate.dto.SelectPaymentApplyVO;
import com.gov.operate.feign.dto.ApprovalInfo;
import com.gov.operate.feign.dto.FeignResult;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.invoiceOptimiz.IPaymentApplyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: zhangp
 * @date: 2021.03.30
 */
@Validated
@RestController
@RequestMapping("invoice/paymentApply")
public class PaymentApplyController {

    @Resource
    private IPaymentApplyService ipaymentApplyServcie;

    @Log("供应商付款申请")
    @ApiOperation(value = "供应商付款申请")
    @PostMapping("addApply")
    public Result addApply(@RequestBody List<PaymentApplyVO> list) {
        Result result = ipaymentApplyServcie.addApply(list);
        return result;
    }

    @Log("付款申请查询")
    @ApiOperation(value = "付款申请查询")
    @PostMapping("selectPaymentApply")
    public Result selectPaymentApply(@RequestBody SelectPaymentApplyVO vo) {
        return ResultUtil.success(ipaymentApplyServcie.selectPaymentApply(vo));
    }

    @Log("单据对账详情")
    @ApiOperation(value = "单据对账详情")
    @PostMapping("getDocumentBill")
    public Result getDocumentBill(@RequestJson("id") String id) {
        return ResultUtil.success(ipaymentApplyServcie.getDocumentBill(id));
    }

    @Log("发票对账详情")
    @ApiOperation(value = "发票对账详情")
    @PostMapping("getInvoiceBill")
    public Result getInvoiceBill(@RequestJson("id") String id) {
        return ResultUtil.success(ipaymentApplyServcie.getInvoiceBill(id));
    }

    @Log("付款审批")
    @ApiOperation(value = "付款审批")
    @PostMapping("approvePayment")
    public FeignResult approvePayment(@RequestBody ApprovalInfo info) {
        try {
            return ipaymentApplyServcie.approvePayment(info);
        } catch (Exception e) {
            e.printStackTrace();
            FeignResult result = new FeignResult();
            result.setCode(9999);
            result.setMessage("系统异常");
            return result;
        }
    }
}
