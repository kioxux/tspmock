package com.gov.operate.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@Data
public class SalesAnalysis {

    private String client;

    private String brId;

    private String anYear;

    private String anDate;

    private String anAmt;

    private String anGm;

    private String anTans;

    private String anPrice;


}
