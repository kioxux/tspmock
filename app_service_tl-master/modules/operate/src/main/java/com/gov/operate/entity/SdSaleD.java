package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 门店销售明细表
 * </p>
 *
 * @author sy
 * @since 2020-12-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_SALE_D")
@ApiModel(value="SdSaleD对象", description="门店销售明细表")
public class SdSaleD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "销售单号")
    @TableField("GSSD_BILL_NO")
    private String gssdBillNo;

    @ApiModelProperty(value = "店名")
    @TableField("GSSD_BR_ID")
    private String gssdBrId;

    @ApiModelProperty(value = "销售日期")
    @TableField("GSSD_DATE")
    private String gssdDate;

    @ApiModelProperty(value = "行号")
    @TableField("GSSD_SERIAL")
    private String gssdSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSSD_PRO_ID")
    private String gssdProId;

    @ApiModelProperty(value = "商品批号")
    @TableField("GSSD_BATCH_NO")
    private String gssdBatchNo;

    @ApiModelProperty(value = "商品批次")
    @TableField("GSSD_BATCH")
    private String gssdBatch;

    @ApiModelProperty(value = "商品有效期")
    @TableField("GSSD_VALID_DATE")
    private String gssdValidDate;

    @ApiModelProperty(value = "监管码/条形码")
    @TableField("GSSD_ST_CODE")
    private String gssdStCode;

    @ApiModelProperty(value = "单品零售价")
    @TableField("GSSD_PRC1")
    private BigDecimal gssdPrc1;

    @ApiModelProperty(value = "单品应收价")
    @TableField("GSSD_PRC2")
    private BigDecimal gssdPrc2;

    @ApiModelProperty(value = "数量")
    @TableField("GSSD_QTY")
    private BigDecimal gssdQty;

    @ApiModelProperty(value = "汇总应收金额")
    @TableField("GSSD_AMT")
    private BigDecimal gssdAmt;

    @ApiModelProperty(value = "兑换单个积分")
    @TableField("GSSD_INTEGRAL_EXCHANGE_SINGLE")
    private String gssdIntegralExchangeSingle;

    @ApiModelProperty(value = "兑换积分汇总")
    @TableField("GSSD_INTEGRAL_EXCHANGE_COLLECT")
    private String gssdIntegralExchangeCollect;

    @ApiModelProperty(value = "单个加价兑换积分金额")
    @TableField("GSSD_INTEGRAL_EXCHANGE_AMT_SINGLE")
    private BigDecimal gssdIntegralExchangeAmtSingle;

    @ApiModelProperty(value = "加价兑换积分金额汇总")
    @TableField("GSSD_INTEGRAL_EXCHANGE_AMT_COLLECT")
    private BigDecimal gssdIntegralExchangeAmtCollect;

    @ApiModelProperty(value = "商品折扣总金额")
    @TableField("GSSD_ZK_AMT")
    private BigDecimal gssdZkAmt;

    @ApiModelProperty(value = "积分兑换折扣金额")
    @TableField("GSSD_ZK_JFDH")
    private BigDecimal gssdZkJfdh;

    @ApiModelProperty(value = "积分抵现折扣金额")
    @TableField("GSSD_ZK_JFDX")
    private BigDecimal gssdZkJfdx;

    @ApiModelProperty(value = "电子券折扣金额")
    @TableField("GSSD_ZK_DZQ")
    private BigDecimal gssdZkDzq;

    @ApiModelProperty(value = "抵用券折扣金额")
    @TableField("GSSD_ZK_DYQ")
    private BigDecimal gssdZkDyq;

    @ApiModelProperty(value = "促销折扣金额")
    @TableField("GSSD_ZK_PM")
    private BigDecimal gssdZkPm;

    @ApiModelProperty(value = "	营业员编号")
    @TableField("GSSD_SALER_ID")
    private String gssdSalerId;

    @ApiModelProperty(value = "医生编号")
    @TableField("GSSD_DOCTOR_ID")
    private String gssdDoctorId;

    @ApiModelProperty(value = "参加促销主题编号")
    @TableField("GSSD_PM_SUBJECT_ID")
    private String gssdPmSubjectId;

    @ApiModelProperty(value = "参加促销类型编号")
    @TableField("GSSD_PM_ID")
    private String gssdPmId;

    @ApiModelProperty(value = "参加促销类型说明")
    @TableField("GSSD_PM_CONTENT")
    private String gssdPmContent;

    @ApiModelProperty(value = "参加促销编号")
    @TableField("GSSD_PM_ACTIVITY_ID")
    private String gssdPmActivityId;

    @ApiModelProperty(value = "参加促销活动名称")
    @TableField("GSSD_PM_ACTIVITY_NAME")
    private String gssdPmActivityName;

    @ApiModelProperty(value = "参加促销活动参数")
    @TableField("GSSD_PM_ACTIVITY_FLAG")
    private String gssdPmActivityFlag;

    @ApiModelProperty(value = "是否登记处方信息 0否1是")
    @TableField("GSSD_RECIPEL_FLAG")
    private String gssdRecipelFlag;

    @ApiModelProperty(value = "是否为关联推荐")
    @TableField("GSSD_RELATION_FLAG")
    private String gssdRelationFlag;

    @ApiModelProperty(value = "特殊药品购买人身份证")
    @TableField("GSSD_SPECIALMED_IDCARD")
    private String gssdSpecialmedIdcard;

    @ApiModelProperty(value = "特殊药品购买人姓名")
    @TableField("GSSD_SPECIALMED_NAME")
    private String gssdSpecialmedName;

    @ApiModelProperty(value = "特殊药品购买人性别")
    @TableField("GSSD_SPECIALMED_SEX")
    private String gssdSpecialmedSex;

    @ApiModelProperty(value = "特殊药品购买人出生日期")
    @TableField("GSSD_SPECIALMED_BIRTHDAY")
    private String gssdSpecialmedBirthday;

    @ApiModelProperty(value = "特殊药品购买人手机")
    @TableField("GSSD_SPECIALMED_MOBILE")
    private String gssdSpecialmedMobile;

    @ApiModelProperty(value = "特殊药品购买人地址")
    @TableField("GSSD_SPECIALMED_ADDRESS")
    private String gssdSpecialmedAddress;

    @ApiModelProperty(value = "特殊药品电子监管码")
    @TableField("GSSD_SPECIALMED_ECODE")
    private String gssdSpecialmedEcode;

    @ApiModelProperty(value = "价格状态")
    @TableField("GSSD_PRO_STATUS")
    private String gssdProStatus;

    @ApiModelProperty(value = "批次成本价")
    @TableField("GSSD_BATCH_COST")
    private BigDecimal gssdBatchCost;

    @ApiModelProperty(value = "批次成本税额")
    @TableField("GSSD_BATCH_TAX")
    private BigDecimal gssdBatchTax;

    @ApiModelProperty(value = "移动平均单价")
    @TableField("GSSD_MOV_PRICE")
    private BigDecimal gssdMovPrice;

    @ApiModelProperty(value = "移动平均总价")
    @TableField("GSSD_MOV_PRICES")
    private BigDecimal gssdMovPrices;

    @ApiModelProperty(value = "税率")
    @TableField("GSSD_MOV_TAX")
    private BigDecimal gssdMovTax;

    @ApiModelProperty(value = "是否生成凭证 1-是 0-否")
    @TableField("GSSD_AD_FLAG")
    private String gssdAdFlag;

    @ApiModelProperty(value = "初始数量")
    @TableField("GSSD_ORI_QTY")
    private BigDecimal gssdOriQty;

    @ApiModelProperty(value = "帖数")
    @TableField("GSSD_DOSE")
    private String gssdDose;

    @ApiModelProperty(value = "移动税额")
    @TableField("GSSD_TAX_RATE")
    private BigDecimal gssdTaxRate;

    @TableField("LAST_UPDATE_TIME")
    private LocalDateTime lastUpdateTime;


}
