package com.gov.operate.dto.ssp;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class SspMenuChainDTO {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 菜单ID
     */
    private Long menuId;

    /**
     * 菜单编号
     */
    private String menuCode;

    /**
     * 菜单名称
     */
    private String menuName;


    private Map<String, Boolean> params = new HashMap<>();


}
