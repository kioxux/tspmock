package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户收费维护主表
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_CHARGE_MAINTENANCE_H")
@ApiModel(value="UserChargeMaintenanceH对象", description="用户收费维护主表")
public class UserChargeMaintenanceH extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "创建人账号")
    @TableField("SP_CRE_ID")
    private String spCreId;

    @ApiModelProperty(value = "创建日期")
    @TableField("SP_CRE_DATE")
    private String spCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("SP_CRE_TIME")
    private String spCreTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("SP_MODI_ID")
    private String spModiId;

    @ApiModelProperty(value = "修改日期")
    @TableField("SP_MODI_DATE")
    private String spModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("SP_MODI_TIME")
    private String spModiTime;

    @ApiModelProperty(value = "开始日期")
    @TableField("SP_STARTING_DATE")
    private String spStartingDate;

    @ApiModelProperty(value = "结束日期")
    @TableField("SP_ENDING_DATE")
    private String spEndingDate;

    @ApiModelProperty(value = "是否已发")
    @TableField("SP_SEND")
    private Integer spSend;

    @ApiModelProperty(value = "客户ID")
    @TableField("SP_CLIENT_ID")
    private Long spClientId;

    @ApiModelProperty(value = "甲方名称")
    @TableField("SP_CO_NAME")
    private String spCoName;

    @ApiModelProperty(value = "签约日期")
    @TableField("SP_SIGN_DATE")
    private LocalDate spSignDate;

    @ApiModelProperty(value = "预计上线日期")
    @TableField("SP_EXCPT_START_DATE")
    private LocalDate spExcptStartDate;

    @ApiModelProperty(value = "预计结束日期")
    @TableField("SP_EXCPT_END_DATE")
    private LocalDate spExcptEndDate;

    @ApiModelProperty(value = "合同期限")
    @TableField("SP_LIMIT")
    private Integer spLimit;

    @ApiModelProperty(value = "门店数量")
    @TableField("SP_STO_COUNT")
    private Integer spStoCount;

    @ApiModelProperty(value = "支付周期")
    @TableField("SP_PAY_FREQ")
    private Integer spPayFreq;

    @ApiModelProperty(value = "折扣率")
    @TableField("SP_DISCOUNT")
    private Integer spDiscount;

    @ApiModelProperty(value = "合同编号")
    @TableField("SP_CONTRACT_CODE")
    private String spContractCode;

    @ApiModelProperty(value = "应收")
    @TableField("SP_PAYABLE")
    private BigDecimal spPayable;

    @ApiModelProperty(value = "实收")
    @TableField("SP_ACTUAL_PAY")
    private BigDecimal spActualPay;

    @ApiModelProperty(value = "折让额")
    @TableField("SP_DSCNT_AMOUNT")
    private BigDecimal spDscntAmount;

    @ApiModelProperty(value = "单店月均成本")
    @TableField("SP_AVG_AMOUNT")
    private BigDecimal spAvgAmount;

    @ApiModelProperty(value = "GP收费标准")
    @TableField("SP_GP_PRICE")
    private BigDecimal spGpPrice;

    @ApiModelProperty(value = "免费期")
    @TableField("SP_FREE_MONTHS")
    private BigDecimal spFreeMonths;

    @ApiModelProperty(value = "维护索引")
    @TableField("SP_INDEX")
    private Integer spIndex;


}
