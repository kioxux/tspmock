package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 查询数据清单
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_QUERY_AUTH_DATA")
@ApiModel(value="BatchQueryAuthData对象", description="查询数据清单")
public class BatchQueryAuthData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键值")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBQAD_FACTORY")
    private String gbqadFactory;

    @ApiModelProperty(value = "商品编码")
    @TableField("GBQAD_PRO_CODE")
    private String gbqadProCode;


}
