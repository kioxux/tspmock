package com.gov.operate.dto;

import com.gov.operate.entity.SdPromVariableTran;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PromVariableTranDTO extends SdPromVariableTran {

    /**
     * 字段类型
     */
    private String gspvsColumnsType;

    /**
     * 字段长度
     */
    private String gspvsColumnsLength;

    /**
     * 促销表名
     */
    private String gspvsTable;
}
