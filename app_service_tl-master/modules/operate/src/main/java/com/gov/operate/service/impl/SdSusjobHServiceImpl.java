package com.gov.operate.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.CustomResultException;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.operate.service.marketing.ISdMarketingSearch3Service;
import com.gov.operate.tpns.PushContent;
import com.gov.operate.tpns.TpnsUtils;
import com.tencent.xinge.XingeApp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Executor;

import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-03
 */
@Slf4j
@Service
public class SdSusjobHServiceImpl extends ServiceImpl<SdSusjobHMapper, SdSusjobH> implements ISdSusjobHService {

    @Resource
    private CommonService commonService;
    @Resource
    private ISdSusjobDService sdSusjobDService;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private SdMarketingSearchMapper marketingSearchMapper;
    @Resource
    private ISdMarketingSearch3Service marketingSearch3Service;

    @Resource
    private FranchiseeMapper franchiseeMapper;
    @Resource
    private ICompadmService compadmService;
    @Resource
    private SdSusjobHMapper sdSusjobHMapper;
    @Resource
    private SdTagListMapper sdTagListMapper;
    @Resource
    private MessageMapper messageMapper;
    @Resource
    private SdMemberCardMapper memberCardMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private SdSusjobDMapper sdSusjobDMapper;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private AppLoginLogMapper appLoginLogMapper;
    /**
     * 创建维系任务
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createSusjob(CreateSusjobVO vo) {
        TokenUser user = commonService.getLoginInfo();
        // 会员维系主数据保存
        List<SdSusjobH> susjobHList = new ArrayList<>();
        List<SdSusjobD> susjobDList = new ArrayList<>();
        if (DateUtil.parse(vo.getSusPushDate(), "yyyyMMdd")
                .isAfter(DateUtil.parse(vo.getSusBegin(), "yyyyMMdd"))) {
            throw new CustomResultException("推送时间不能晚于任务开始时间");
        }

        vo.setClient(user.getClient());
        // 根据消费类型取天数
        if (OperateEnum.ConsumeType.FIVE.getCode().equals(vo.getConsumeType())) {
            vo.setConsumeTypeDay(0);
        } else {
            for (OperateEnum.ConsumeType type : OperateEnum.ConsumeType.values()) {
                if (!type.getCode().equals(vo.getConsumeType())) {
                    continue;
                }
                String consumeTypeDay = type.getMessage();
                vo.setConsumeTypeDay(Integer.valueOf(consumeTypeDay));
            }
        }

        Page page=new Page();
        page.setCurrent(-1);
        page.setSize(-1);
        log.info("getMemberPage3:{}", JsonUtils.beanToJson(vo));
        List<GetMemberListDTO> memberList = marketingSearch3Service.getMemberList(vo).getRecords()
                .stream()
                .filter(item -> {
                    if ("2".equals(vo.getSusType())) {  // 任务门店类型 = 最后交易门店
                        return !ObjectUtils.isEmpty(item.getLastSaleStore());
                    }
                    return true;
                }).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(memberList)) {
            throw new CustomResultException("没有查询到会员");
        }
        if (memberList.size() > 4000) {
            throw new CustomResultException("一次最多支持生成4000名会员的维系任务,请缩小查询范围再生成维系任务。");
        }
        Map<String, List<GetMemberListDTO>> collect = null;
        if ("2".equals(vo.getSusType())) {  // 任务门店类型 = 最后交易门店
            collect = memberList.stream().collect(Collectors.groupingBy(GetMemberListDTO::getLastSaleStore));
        } else {  // 任务门店类型 = 开卡门店
            collect = memberList.stream().collect(Collectors.groupingBy(GetMemberListDTO::getGmbBrId));
        }

        collect.forEach((key, itemList) -> {
            String uuid = UUIDUtil.getUUID();
            SdSusjobH susjobH = new SdSusjobH();
            susjobH.setClient(user.getClient());
            susjobH.setSusJobid(uuid);
            susjobH.setSusJobname(vo.getSusJobname());
            susjobH.setSusJobbr(key);
            susjobH.setSusTime(DateUtils.getCurrentDateTimeStrTwo());
            susjobH.setSusBegin(vo.getSusBegin());
            susjobH.setSusEnd(vo.getSusEnd());
            susjobH.setSusJobperson(new BigDecimal(itemList.size()));
            susjobH.setSusSche("0/" + String.valueOf(itemList.size()));
            susjobH.setSusEmp(StringUtils.isNotEmpty(vo.getSusEmp())?vo.getSusEmp():"0");
            // 计划结束时间 >= 今天 进行中,计划结束时间< 今天，已过期
            LocalDate endDate = LocalDate.parse(vo.getSusEnd(), DateUtils.DATE_FORMATTER_YYMMDD);
            LocalDate startDate = LocalDate.parse(vo.getSusBegin(), DateUtils.DATE_FORMATTER_YYMMDD);

            if (LocalDate.now().isAfter(endDate)) {
                susjobH.setSusStatus("1");
            } else if (LocalDate.now().isBefore(startDate)) {
                susjobH.setSusStatus("-1");
            } else {
                susjobH.setSusStatus("0");
            }
            susjobH.setSusExplain(vo.getSusExplain());
            susjobH.setSusType(StringUtils.isBlank(vo.getSusType()) ? "1" : vo.getSusType());
            susjobH.setSusContent(vo.getSusContent());
            susjobH.setSusPushDate(vo.getSusPushDate());
            susjobH.setSusPushTime(DateUtil.format(DateUtil.parse(vo.getSusPushTime(),"HH:mm:ss"),"HHmmss"));
            susjobH.setSusPushStatus("0");
            susjobHList.add(susjobH);
            List<String> primaryList = new ArrayList<>();
            AtomicReference<Integer> number = new AtomicReference<>(1);
            itemList.forEach(item -> {
                String primary = user.getClient() + "_" + uuid + "_" + item.getGmbCardId() + "_" + key;
                if (primaryList.contains(primary)) {
                    return;
                } else {
                    primaryList.add(primary);
                }
                SdSusjobD sdSusjobD = new SdSusjobD();
                //序号
                sdSusjobD.setSusNumber(Convert.toStr(number));
                number.updateAndGet(v -> v + 1);
                sdSusjobD.setClient(user.getClient());
                sdSusjobD.setSusJobid(uuid);
                sdSusjobD.setSusCardId(item.getGmbCardId());
                sdSusjobD.setSusJobbr(key);
                sdSusjobD.setSusMenname(item.getGmbName());
                sdSusjobD.setSusMobile(item.getGmbMobile());
                sdSusjobD.setSusSex(item.getGmbSex());
                sdSusjobD.setSusBirth(item.getGmbBirth());
                sdSusjobD.setSusIntegral(new BigDecimal(item.getGmbIntegral()));
                sdSusjobD.setSusBrId(item.getGmbBrId());
                sdSusjobD.setSusBrName(item.getGmbBrName());
                // 会员标签
                // TODO
                List<String> tagList = sdTagListMapper.getTagListByCardId(user.getClient(), item.getGmbCardId());
                if (!CollectionUtils.isEmpty(tagList)) {
                    String tagListString = tagList.stream().collect(Collectors.joining(","));
                    sdSusjobD.setSusFlg(tagListString);
                }
                // 最后一次交易门店编码
                sdSusjobD.setSusBrId1(key);
                // 最后一次交易门店名称
                StoreData storeData = storeDataService.getOne(new QueryWrapper<StoreData>().eq("CLIENT", user.getClient()).eq("STO_CODE", key));
                if (!ObjectUtils.isEmpty(storeData)) {
                    sdSusjobD.setSusBrName1(storeData.getStoName());
                } else {
                    Compadm compadm = compadmService.getOne(new QueryWrapper<Compadm>().eq("CLIENT", user.getClient()).eq("COMPADM_ID", key));
                    if (!ObjectUtils.isEmpty(compadm)) {
                        sdSusjobD.setSusBrName1(compadm.getCompadmName());
                    }
                }
                // 最后一次交易时间
                String lastDate = StringUtils.isNotEmpty(item.getGsshDate()) ? item.getGsshDate() : "";
                String lastTime = StringUtils.isNotEmpty(item.getGsshTime()) ? item.getGsshTime() : "";
                sdSusjobD.setSusLasttime(lastDate + lastTime);
                //开卡员工
                sdSusjobD.setSusOpenCardEmp(StringUtils.isNotEmpty(item.getGsmbcCreateSaler()) ? item.getGsmbcCreateSaler() : "");
                //最后一次交易员工
                sdSusjobD.setSusLastSaleEmp(StringUtils.isNotEmpty(item.getLastSaleEmp()) ? item.getLastSaleEmp() : "");
                sdSusjobD.setSusFlgadd("");
                sdSusjobD.setSusJobflg("");
                sdSusjobD.setSusFreeze("");
                sdSusjobD.setSusJobtxt("");
                sdSusjobD.setSusUserid("");
                sdSusjobD.setSusUsername("");
                sdSusjobD.setSusJobtime("");
                sdSusjobD.setSusFinish("0");
                susjobDList.add(sdSusjobD);

            });
        });
        this.saveBatch(susjobHList);
        sdSusjobDService.saveBatch(susjobDList);

    }


    private void insertMemberMainternMessage(SdSusjobH sdSusjobH, UserData userData) {
        // 用户消息
        Message message = new Message();
        // ID
        message.setGmId(UUID.randomUUID().toString());
        // 接收人加盟商ID
        message.setGmClient(sdSusjobH.getClient());
        // 接收人员工编号
        message.setGmUserId(userData.getUserId());
        // 发送时间
        message.setGmSendTime(DateUtils.getCurrentDateTimeStrFull());
        // 标题
        message.setGmTitle(sdSusjobH.getSusJobname());
        // 内容
        message.setGmContent("会员维系任务【"+sdSusjobH.getSusJobname()+"】已推送，任务时间【"+sdSusjobH.getSusBegin()+"-"+sdSusjobH.getSusEnd()+"】，请按时执行，谢谢！");
        // 是否已读
        message.setGmIsRead(CommonEnum.gmIsRead.UNREAD.getCode());
        // 业务类型
        message.setGmBusinessType(0);
        // 是否跳转
        message.setGmGoPage(1);
        // 消息类型
        message.setGmType(8);
        // 表示形式
        message.setGmShowType(3);   //消息+推送

        messageMapper.insert(message);

    }

    /**
     * 维系任务列表
     */
    @Override
    public IPage<SdSusjobHDTO> getSusjobList(GetSusjobListVO vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<SdSusjobHDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<SdSusjobHDTO> query = new QueryWrapper<SdSusjobHDTO>()
                .eq("h.CLIENT", user.getClient())
                .eq("h.SUS_PUSH_STATUS", "1")
                //  .eq("h.SUS_JOBID", vo.getSusJobid())
                .eq(StringUtils.isNotEmpty(vo.getSusJobbr()), "h.SUS_JOBBR", vo.getSusJobbr())
                .apply(StringUtils.isNotEmpty(vo.getSusJobname()), "instr(h.SUS_JOBNAME,{0})", vo.getSusJobname())
                .apply(StringUtils.isNotEmpty(vo.getSusTimeStart()), "LEFT(h.SUS_TIME, 8) >= {0}", vo.getSusTimeStart())
                .apply(StringUtils.isNotEmpty(vo.getSusTimeEnd()), "LEFT(h.SUS_TIME, 8) <= {0}", vo.getSusTimeEnd())
                .eq(StringUtils.isNotEmpty(vo.getSusStatus()), "h.SUS_STATUS", vo.getSusStatus());
        IPage<SdSusjobHDTO> susjobList = sdSusjobHMapper.getSusjobList(page, query, user.getClient(), user.getUserId());
        return susjobList;
    }

    /**
     * 更新推送状态
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updatePushStatus() {
        log.info("<会员维系任务><修改状态开始>");
        Date now = new Date();
        SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = yyyyMMdd.format(now);
        List<SdSusjobH> list = sdSusjobHMapper.selectByTimeAndStatus(date,"0");
        log.info("<会员维系任务><修改状态><修改列表：{}>", JSON.toJSONString(list));
        for (SdSusjobH sdSusjobH : list) {
            log.info("<会员维系任务><修改状态><修改参数：{}>", JSON.toJSONString(sdSusjobH));
            sdSusjobHMapper.updateStatus(sdSusjobH.getClient(),sdSusjobH.getSusJobid(),sdSusjobH.getSusJobbr(),"1");
        }
        log.info("<会员维系任务><修改状态结束>");
    }

    /**
     * 更新任务状态
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTaskStatus() {
        QueryWrapper<SdSusjobH> query = new QueryWrapper<SdSusjobH>()
                .in("SUS_STATUS", "0", "-1")
                .eq("SUS_PUSH_STATUS", "1");
        List<SdSusjobH> jobList = sdSusjobHMapper.selectList(query);
        for (SdSusjobH sdSusjobH : jobList) {
            LocalDate startDate = LocalDate.parse(sdSusjobH.getSusBegin(), DateTimeFormatter.ofPattern("yyyyMMdd"));
            LocalDate endDate = LocalDate.parse(sdSusjobH.getSusEnd(), DateTimeFormatter.ofPattern("yyyyMMdd"));
            LocalDate currentDate = LocalDate.now();
            if ("-1".equals(sdSusjobH.getSusStatus())) {  //  未开始
                if (currentDate.compareTo(startDate) >= 0) {
                    sdSusjobH.setSusStatus("0");
                    sdSusjobHMapper.update(sdSusjobH, new QueryWrapper<SdSusjobH>()
                            .eq("CLIENT", sdSusjobH.getClient())
                            .eq("SUS_JOBID", sdSusjobH.getSusJobid())
                            .eq("SUS_JOBBR", sdSusjobH.getSusJobbr()));
                }
            } else {
                if (currentDate.compareTo(endDate) >= 0) {
                    sdSusjobH.setSusStatus("1");
                    sdSusjobHMapper.update(sdSusjobH, new QueryWrapper<SdSusjobH>()
                            .eq("CLIENT", sdSusjobH.getClient())
                            .eq("SUS_JOBID", sdSusjobH.getSusJobid())
                            .eq("SUS_JOBBR", sdSusjobH.getSusJobbr()));
                }
            }
        }
    }

    /**
     * 推送消息
     * @param param
     */
    @Override
    public void pushMessage(String param) {
        GetClientListVO getClientListVO = new GetClientListVO();
        List<Franchisee> clientList=new ArrayList<>();
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(param)) {
            Franchisee clientInfo = franchiseeMapper.getClientInfo(param);
            clientList.add(clientInfo);
        } else {
            clientList = franchiseeMapper.getClientList(getClientListVO);
        }
        for (Franchisee client : clientList) {
            //查找还未推送过的维系任务
            LambdaQueryWrapper<SdSusjobH> wrapper = Wrappers.<SdSusjobH>lambdaQuery()
                    .eq(SdSusjobH::getClient, client.getClient())
                    .eq(SdSusjobH::getSusPushDate,DateUtil.format(DateUtil.date(),"yyyyMMdd"))
                    .eq(SdSusjobH::getSusPushTime,DateUtil.format(DateUtil.date(),"HH")+"0000")
                    .in(SdSusjobH::getSusPushStatus, "0", "-1");
            List<SdSusjobH> susjobHList = sdSusjobHMapper.selectList(wrapper);
            if (CollUtil.isNotEmpty(susjobHList)) {
                List<SdSusjobH> lastSaleTask = susjobHList.stream().filter(sush -> sush.getSusType().equals("2")).collect(Collectors.toList());
                List<SdSusjobH> openCardTask = susjobHList.stream().filter(sush -> sush.getSusType().equals("1")).collect(Collectors.toList());
                if (CollUtil.isNotEmpty(lastSaleTask)) {
                    getUserDataForLastSaleTask(lastSaleTask);
                }
                if (CollUtil.isNotEmpty(openCardTask)) {
                    getUserDataForOpenCardTask(openCardTask);
                }
            }
        }

    }

    private void getUserDataForOpenCardTask(List<SdSusjobH> openCardTask) {
        Set<UserData> userData=new HashSet<>();
        for (SdSusjobH sdSusjobH : openCardTask) {
            LambdaQueryWrapper<SdSusjobD> wrapper;
                wrapper = Wrappers.<SdSusjobD>lambdaQuery()
                        .eq(SdSusjobD::getClient, sdSusjobH.getClient())
                        .eq(SdSusjobD::getSusJobid, sdSusjobH.getSusJobid())
                        .eq(SdSusjobD::getSusBrId, sdSusjobH.getSusJobbr());
            List<SdSusjobD> sdSusjobDList = sdSusjobDMapper.selectList(wrapper);
            if (CollUtil.isNotEmpty(sdSusjobDList)) {
                for (SdSusjobD sdSusjobD : sdSusjobDList) {
                LambdaQueryWrapper<UserData> queryWrapper;
                    if (sdSusjobH.getSusEmp().equals("1")) {
                        queryWrapper=Wrappers.<UserData>lambdaQuery()
                                .eq(UserData::getClient,sdSusjobD.getClient())
                                .eq(UserData::getDepId,sdSusjobD.getSusBrId())
                                .eq(UserData::getUserId,sdSusjobD.getSusOpenCardEmp());
                    } else {
                        queryWrapper=Wrappers.<UserData>lambdaQuery()
                                .eq(UserData::getClient,sdSusjobD.getClient())
                                .eq(UserData::getDepId,sdSusjobD.getSusBrId());
                    }
                    userData.addAll(userDataMapper.selectList(queryWrapper));
                }
            }
            pushingMessage(userData,sdSusjobH);
        }


    }

    private void getUserDataForLastSaleTask(List<SdSusjobH> lastSaleTask) {
        Set<UserData> userData=new HashSet<>();
        for (SdSusjobH sdSusjobH : lastSaleTask) {
            LambdaQueryWrapper<SdSusjobD> wrapper;
            wrapper = Wrappers.<SdSusjobD>lambdaQuery()
                    .eq(SdSusjobD::getClient, sdSusjobH.getClient())
                    .eq(SdSusjobD::getSusJobid, sdSusjobH.getSusJobid())
                    .eq(SdSusjobD::getSusBrId, sdSusjobH.getSusJobbr());
            List<SdSusjobD> sdSusjobDList = sdSusjobDMapper.selectList(wrapper);
            if (CollUtil.isNotEmpty(sdSusjobDList)) {
                for (SdSusjobD sdSusjobD : sdSusjobDList) {
                    LambdaQueryWrapper<UserData> queryWrapper;
                    if (sdSusjobH.getSusEmp().equals("1")) {
                        queryWrapper=Wrappers.<UserData>lambdaQuery()
                                .eq(UserData::getClient,sdSusjobD.getClient())
                                .eq(UserData::getDepId,sdSusjobD.getSusBrId1())
                                .eq(UserData::getUserId,sdSusjobD.getSusLastSaleEmp());
                    } else {
                        queryWrapper=Wrappers.<UserData>lambdaQuery()
                                .eq(UserData::getClient,sdSusjobD.getClient())
                                .eq(UserData::getDepId,sdSusjobD.getSusBrId1());
                    }
                    userData.addAll(userDataMapper.selectList(queryWrapper));
                }
            }
            pushingMessage(userData,sdSusjobH);
        }
    }

    private void pushingMessage(Set<UserData> userDataList, SdSusjobH sdSusjobH) {

        for (UserData userData : userDataList) {
            insertMemberMainternMessage(sdSusjobH,userData);
        }

    }
}
