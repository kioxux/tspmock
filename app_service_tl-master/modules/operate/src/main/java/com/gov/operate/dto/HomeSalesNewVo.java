package com.gov.operate.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class HomeSalesNewVo {
    private String client;

    /**
     * 数据状态 D:本期 S:上周 T:去年同期
     */
    private String gssdType;


    /**
     * 门店
     */
    @ApiModelProperty(value = "门店")
    private String gssdBrId;


    /**
     * 销售日期
     */
    @ApiModelProperty(value = "销售日期")
    private String gssdDate;


    private String gcdWeek;

    /**
     * 门店数量
     */
    @ApiModelProperty(value = "门店数")
    private String gssdStoreCount = "0";

    /**
     * 销售额
     */
    @ApiModelProperty(value = "销售额")
    private String gssdAmt = "0";

    /**
     * 毛利额
     */
    @ApiModelProperty(value = "毛利额")
    private String gssdGrossAmt = "0";

    /**
     * 毛利率
     */
    @ApiModelProperty(value = "毛利率")
    private String gssdGrossMar = "0";

    /**
     * 来客数
     */
    @ApiModelProperty(value = "交易次数")
    private String gssdTrans = "0";

    /**
     * 客单价
     */
    @ApiModelProperty(value = "客单价")
    private String gssdAprice = "0";

    /**
     * 本店日均销售额
     */
    @ApiModelProperty(value = "本店日均销售额")
    private String gssdAmtAvg = "0";

    @ApiModelProperty(value = "提成合计")
    private BigDecimal deductionWage;

    @ApiModelProperty(value = "销售提成")
    private BigDecimal  deductionWageSales;

    @ApiModelProperty(value = "单品提成")
    private BigDecimal  deductionWagePro;
    //销售天数
    @ApiModelProperty(value = "销售天数")
    private String day;
    //营业额-月计划
    @ApiModelProperty(value = "营业额-月计划")
    private BigDecimal tAmt;
    //毛利额-月计划
    @ApiModelProperty(value = "毛利额-月计划")
    private BigDecimal tGross;
    //会员卡-月计划
    @ApiModelProperty(value = "会员卡-月计划")
    private BigDecimal tMcardQty;
    //营业额-达成
    @ApiModelProperty(value = "累计营业额")
    private BigDecimal aAmt;
    //毛利额-达成
    @ApiModelProperty(value = "累计毛利额")
    private BigDecimal aGross;
    //会员卡-达成
    @ApiModelProperty(value = "累计会员卡")
    private BigDecimal aMcardQty;
    //营业额-达成率
    @ApiModelProperty(value = "营业额日均达成率")
    private String rAmt;
    //毛利额-达成率
    @ApiModelProperty(value = "毛利额日均达成率")
    private String rGross;
    //会员卡-达成率
    @ApiModelProperty(value = "会员卡日均达成率")
    private String rMcardQty;
    //营业额达成率预警
    @ApiModelProperty(value = "营业额达成率预警")
    private String amtWarning;
    //毛利额达成率预警
    @ApiModelProperty(value = "毛利额达成率预警")
    private String grossWarning;
    //会员卡达成率预警
    @ApiModelProperty(value = "会员卡达成率预警")
    private String mcardQtyWarning;
    //会员卡-达成
    @ApiModelProperty(value = "会员卡")
    private BigDecimal mcardQty;
}
