package com.gov.operate.service.response;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class SelectCardUserDetailResponse {

    @ApiModelProperty("会员ID")
    private String gsmbMemberId;

    @ApiModelProperty("卡类型")
    private String gsmbcClassId;

    //卡类型编号1钻石，2白金，3金，4银，5普通
    public String getGsmbcClassIdStr() {
        if (Objects.equals(gsmbcClassId, "1")) {
            gsmbcClassIdStr = "钻石卡";
        } else if (Objects.equals(gsmbcClassId, "2")) {
            gsmbcClassIdStr = "白金卡";
        } else if (Objects.equals(gsmbcClassId, "3")) {
            gsmbcClassIdStr = "金卡";
        } else if (Objects.equals(gsmbcClassId, "4")) {
            gsmbcClassIdStr = "银卡";
        }else if(Objects.equals(gsmbcClassId, "5")){
            gsmbcClassIdStr = "普通卡";
        }
        return gsmbcClassIdStr;
    }

    private String gsmbcClassIdStr;

    @ApiModelProperty("累计积分")
    private String gsmbcTotalIntegral;

    @ApiModelProperty("会员姓名")
    private String gsmbName;

    @ApiModelProperty("性别")
    private String gsmbSex;

    @ApiModelProperty("生日")
    private String gsmbBirth;

    @ApiModelProperty("年龄")
    private String gsmbAge;

    @ApiModelProperty("手机")
    private String gsmbMobile;

    @ApiModelProperty("企微绑定状态，0、null：未绑定，1：已绑定")
    private String gsmbcWorkwechatBindStatus;

    public String getGsmbcWorkwechatBindStatusStr() {
        if (Objects.equals(gsmbcWorkwechatBindStatus, "0") || StrUtil.isBlank(gsmbcWorkwechatBindStatus)) {
            gsmbcWorkwechatBindStatusStr = "未绑定";
        } else if (Objects.equals(gsmbcWorkwechatBindStatus, "1")) {
            gsmbcWorkwechatBindStatusStr = "已绑定";
        }
        return gsmbcWorkwechatBindStatusStr;
    }

    private String gsmbcWorkwechatBindStatusStr;

    @ApiModelProperty("开卡门店")
    private String gsmbcCreateStoCode;

    @ApiModelProperty("卡号")
    private String gsmbcCardId;

    @ApiModelProperty("新卡创建日期")
    private String gsmbcCreateDate;

    @ApiModelProperty("最后消费日期")
    private String lastSaleDate;

    @ApiModelProperty("最后一次交易门店名称")
    private String lastStoName;

    @ApiModelProperty("最近回访时间")
    private String susFeedbacktime;

    @ApiModelProperty("会员标签")
    private List<String> susFlg;

    @ApiModelProperty("维系标签类型0电话空号1不在维系")
    private List<String> susFlgaddType;

    @ApiModelProperty("回访反馈")
    private String susFeedback;


}
