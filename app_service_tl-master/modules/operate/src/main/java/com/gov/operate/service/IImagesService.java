package com.gov.operate.service;

import com.gov.operate.entity.Images;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-12
 */
public interface IImagesService extends SuperService<Images> {

}
