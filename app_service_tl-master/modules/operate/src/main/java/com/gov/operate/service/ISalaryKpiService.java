package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryKpi;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryKpiService extends SuperService<SalaryKpi> {

    /**
     * 薪资方案员工系数值
     */
    void saveKpi(EditProgramVO vo);

    /**
     * 员工个人系数列表
     */
    List<SalaryKpi> getKpiList(Integer gspId);
}
