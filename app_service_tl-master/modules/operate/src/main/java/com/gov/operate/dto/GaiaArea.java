package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

/**
 * @author Zhangchi
 * @since 2021/10/15/13:44
 */
@Data
public class GaiaArea {
    private String areaId;
    private String areaName;
    private String parentId;
    private String level;
    private List<GaiaArea> areaList;
}
