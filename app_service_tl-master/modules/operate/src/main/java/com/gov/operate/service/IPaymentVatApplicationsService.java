package com.gov.operate.service;

import com.gov.operate.entity.PaymentVatApplications;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
public interface IPaymentVatApplicationsService extends SuperService<PaymentVatApplications> {

}
