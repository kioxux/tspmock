package com.gov.operate.dto.compadmWms;

import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-19
 */
@Data
@ApiModel(value="CompadmWms对象", description="")
public class CompadmWmsDto extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商ID")
    private String client;

    @ApiModelProperty(value = "批发公司ID")
    private String compadmId;

    @ApiModelProperty(value = "批发公司名称")
    private String compadmName;

    @ApiModelProperty(value = "统一社会信用代码")
    private String compadmNo;

    @ApiModelProperty(value = "法人/负责人")
    private String compadmLegalPerson;
    @ApiModelProperty(value = "法人/负责人手机号")
    private String compadmLegalPersonPhone;
    @ApiModelProperty(value = "法人/负责人姓名")
    private String compadmLegalPersonName;

    @ApiModelProperty(value = "质量负责人")
    private String compadmQua;
    @ApiModelProperty(value = "质量负责人手机号")
    private String compadmQuaPhone;
    @ApiModelProperty(value = "质量负责人姓名")
    private String compadmQuaName;

    @ApiModelProperty(value = "详细地址")
    private String compadmAddr;

    @ApiModelProperty(value = "创建日期")
    private String compadmCreDate;

    @ApiModelProperty(value = "创建时间")
    private String compadmCreTime;

    @ApiModelProperty(value = "创建人账号")
    private String compadmCreId;

    @ApiModelProperty(value = "修改日期")
    private String compadmModiDate;

    @ApiModelProperty(value = "修改时间")
    private String compadmModiTime;

    @ApiModelProperty(value = "修改人账号")
    private String compadmModiId;

    @ApiModelProperty(value = "LOGO地址")
    private String compadmLogo;

    @ApiModelProperty(value = "批发公司状态 N停用 Y启用")
    private String compadmStatus;


}
