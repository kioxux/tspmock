package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ActiveEvalVO {

    /**
     * 加盟商
     */
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 营销活动ID
     */
    @NotBlank(message = "营销活动不能为空")
    private String mkMarketId;

    /**
     * 预估类型(0-预估结果，1-实际分析结果)
     */
    @NotBlank(message = "预估类型不能为空")
    private String mkAntype;

    /**
     * 门店组
     */
    private String stogCode;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 商品组
     */
    private String gsyGroup;

    /**
     * 商品自编码
     */
    private String proSelfCode;

}
