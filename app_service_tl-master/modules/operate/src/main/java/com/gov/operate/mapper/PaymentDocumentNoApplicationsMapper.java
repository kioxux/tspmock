package com.gov.operate.mapper;

import com.gov.operate.entity.PaymentDocumentNoApplications;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-16
 */
public interface PaymentDocumentNoApplicationsMapper extends BaseMapper<PaymentDocumentNoApplications> {

}
