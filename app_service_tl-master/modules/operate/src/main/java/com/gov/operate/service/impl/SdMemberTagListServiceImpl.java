package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.MemberDistributionVO;
import com.gov.operate.dto.MemberReportRequestDto;
import com.gov.operate.entity.SdMemberTagList;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdMemberTagListMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdMemberTagListService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
@Service
public class SdMemberTagListServiceImpl extends ServiceImpl<SdMemberTagListMapper, SdMemberTagList> implements ISdMemberTagListService {

    @Resource
    private CommonService commonService;


    @Override
    public Result getTagDistributionList(MemberReportRequestDto requestDto) {
        TokenUser user = commonService.getLoginInfo();
        requestDto.setClient(user.getClient());
        return ResultUtil.success(baseMapper.getTagDistributionList(requestDto));
    }

    @Override
    public Result tagDistributionListNew(MemberReportRequestDto requestDto) {
        if (requestDto.getType().intValue() == 1) {
            if (CollectionUtils.isEmpty(requestDto.getStoCodeList())) {
                requestDto.setStoCodeList(commonService.getStoCodeList());
            }
            if (CollectionUtils.isEmpty(requestDto.getStoCodeList())) {
                return ResultUtil.success(new ArrayList<MemberDistributionVO>());
            }
        }
        TokenUser user = commonService.getLoginInfo();
        requestDto.setClient(user.getClient());
        return ResultUtil.success(baseMapper.getTagDistributionListNew(requestDto));
    }


}
