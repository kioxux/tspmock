package com.gov.operate.service.impl;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.kylin.RowMapper;
import com.gov.operate.mapper.FranchiseeMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISalesAnalysisService;
import com.gov.operate.service.ISdSaleDService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.*;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@Service
public class SalesAnalysisServiceImpl implements ISalesAnalysisService {

    @Resource
    private CommonService commonService;

    @Resource
    private FranchiseeMapper franchiseeMapper;

    @Resource
    private ISdSaleDService saleDService;

    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;


    @Override
    public Result getStoreReportByMonth() {
        TokenUser user = commonService.getLoginInfo();
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();
        // 当前年
        salesAnalysisReportVo.setYear(StringUtils.left(DateUtils.getCurrentMonthStr(), 4));
        // 当前周
        //   salesAnalysisReportVo.setWeek(String.valueOf(DateUtils.getWeekIndex(null)[1]));
        // 当前月份
        salesAnalysisReportVo.setMonth(StringUtils.rightPad(DateUtils.getCurrentMonthStr(), 2));
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(user.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());

        StringBuilder builder = new StringBuilder()
                .append(" select ")
                .append(" a.client as \"Client\",  ")
                .append(" a.GSSD_BR_ID as \"BrId\", ")
                .append(" YEAR(a.GSSD_DATE) as \"AnYear\", ")
                .append(" MONTH(a.GSSD_DATE) as \"AnDate\", ")
                .append(" count(distinct a.gssd_bill_no) as \"AnTans\", ")
                .append(" sum(a.gssd_amt) as \"AnAmt\", ")
                .append(" round(sum(a.gssd_amt)/count(distinct a.gssd_bill_no),2) as \"AnPrice\" ")
                .append(" FROM GAIA_SD_SALE_D a ")
//                .append(" WHERE  a.client = '" + "10000001" + "' ")
                .append(" WHERE  a.client = '" + user.getClient() + "' ")
                .append(" AND YEAR(a.GSSD_DATE) = ")
                .append(LocalDate.now().getYear())
                .append(" ")
                .append(" AND MONTH(a.GSSD_DATE) = ")
                .append(LocalDate.now().getMonthValue())
                .append(" ")
                .append(" GROUP BY a.client, a.GSSD_BR_ID, YEAR(a.GSSD_DATE), MONTH(a.GSSD_DATE) ")
                .append(" order by a.client, a.GSSD_BR_ID, YEAR(a.GSSD_DATE), MONTH(a.GSSD_DATE) ");
        List<SalesAnalysis> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalesAnalysis.class));
        if (CollectionUtils.isNotEmpty(list)) {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (SalesAnalysis salesAnalysis : list) {
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnAmt())) {
                    amt.setYAxis(new BigDecimal(salesAnalysis.getAnAmt()));
                } else {
                    amt.setYAxis(BigDecimal.ZERO);
                }
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnGm())) {
                    gm.setYAxis(new BigDecimal(salesAnalysis.getAnGm()));
                } else {
                    gm.setYAxis(BigDecimal.ZERO);
                }
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnTans())) {
                    tans.setYAxis(new BigDecimal(salesAnalysis.getAnTans()));
                } else {
                    tans.setYAxis(BigDecimal.ZERO);
                }
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnPrice())) {
                    price.setYAxis(new BigDecimal(salesAnalysis.getAnPrice()));
                } else {
                    price.setYAxis(BigDecimal.ZERO);
                }
                priceList.add(price);
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        }

        return ResultUtil.success(salesAnalysisReportVo);
    }

    @Override
    public Result getStoreReportByWeek() {

        TokenUser user = commonService.getLoginInfo();
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();

        int year = LocalDate.now().getYear();

        int week = LocalDate.now().get(WeekFields.ISO.weekOfWeekBasedYear());

        // 当前年
        salesAnalysisReportVo.setYear(String.valueOf(year));
        // 当前周
        salesAnalysisReportVo.setWeek(String.valueOf(week));
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(user.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());

        StringBuilder builder = new StringBuilder()
                .append(" select ")
                .append(" a.client as \"Client\",  ")
                .append(" a.GSSD_BR_ID as \"BrId\", ")
                .append(" YEAR(a.GSSD_DATE) as \"AnYear\", ")
                .append(" WEEK(a.GSSD_DATE) as \"AnDate\", ")
                .append(" count(distinct a.gssd_bill_no) as \"AnTans\", ")
                .append(" sum(a.gssd_amt) as \"AnAmt\", ")
                .append(" round(sum(a.gssd_amt)/count(distinct a.gssd_bill_no),2) as \"AnPrice\" ")
                .append(" FROM GAIA_SD_SALE_D a ")
                // .append(" WHERE  a.client = '" + "10000001" + "' ")
                .append(" WHERE  a.client = '" + user.getClient() + "' ")
                .append(" AND YEAR(a.GSSD_DATE) = ")
                .append(year)
                .append(" ")
                .append("  AND WEEK(a.GSSD_DATE) = ")
                .append(week)
                .append(" ")
                .append(" GROUP BY a.client, a.GSSD_BR_ID, YEAR(a.GSSD_DATE), WEEK(a.GSSD_DATE) ")
                .append(" order by a.client, a.GSSD_BR_ID, YEAR(a.GSSD_DATE), WEEK(a.GSSD_DATE) ");
        List<SalesAnalysis> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalesAnalysis.class));
        if (CollectionUtils.isNotEmpty(list)) {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (SalesAnalysis salesAnalysis : list) {
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnAmt())) {
                    amt.setYAxis(new BigDecimal(salesAnalysis.getAnAmt()));
                } else {
                    amt.setYAxis(BigDecimal.ZERO);
                }
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnGm())) {
                    gm.setYAxis(new BigDecimal(salesAnalysis.getAnGm()));
                } else {
                    gm.setYAxis(BigDecimal.ZERO);
                }
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnTans())) {
                    tans.setYAxis(new BigDecimal(salesAnalysis.getAnTans()));
                } else {
                    tans.setYAxis(BigDecimal.ZERO);
                }
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(salesAnalysis.getBrId());
                if (StringUtils.isNoneEmpty(salesAnalysis.getAnPrice())) {
                    price.setYAxis(new BigDecimal(salesAnalysis.getAnPrice()));
                } else {
                    price.setYAxis(BigDecimal.ZERO);
                }
                priceList.add(price);
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        }

        return ResultUtil.success(salesAnalysisReportVo);


    }

    @Override
    public Result getSummaryReportByMonth() {
        TokenUser user = commonService.getLoginInfo();
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();

        // 当前年
        salesAnalysisReportVo.setYear(StringUtils.left(DateUtils.getCurrentMonthStr(), 4));
        // 当前周
        //   salesAnalysisReportVo.setWeek(String.valueOf(DateUtils.getWeekIndex(null)[1]));
        // 当前月份
        salesAnalysisReportVo.setMonth(StringUtils.rightPad(DateUtils.getCurrentMonthStr(), 2));
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(user.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());

        //这个月的最后一天
        String thisMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        // 上2个月第一天
        String beforeMonth = LocalDate.now().plusMonths(-2).with(TemporalAdjusters.firstDayOfMonth()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        StringBuilder builder = new StringBuilder()
                .append(" select ")
                .append("  client as \"Client\", ")
                .append("  YEAR(GSSD_DATE) as \"AnYear\", ")
                .append("  MONTH(GSSD_DATE) as \"AnDate\", ")
                .append(" count(distinct gssd_bill_no) as \"AnTans\", ")  // 交易次数
                .append("  sum(gssd_amt) as \"AnAmt\", ")  // 销售额
                .append(" round(sum(gssd_amt)/count(distinct gssd_bill_no),2) as \"AnPrice\" ") // 客单价
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" WHERE ")
                //   .append(" client = '10000001' ")
                .append(" client = '" + user.getClient() + "' ")
                .append(" AND GSSD_DATE BETWEEN DATE '")
                .append(beforeMonth)
                .append("' AND DATE '")
                .append(thisMonth)
                .append("' ")
                .append("  GROUP BY client, YEAR(GSSD_DATE), MONTH(GSSD_DATE) ")
                .append("  order by client, YEAR(GSSD_DATE), MONTH(GSSD_DATE) ");
        List<SalesAnalysis> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalesAnalysis.class));

        // 销售额
        List<SalesAnalysisReportVo> amtList = new ArrayList<>();
        //毛利率
        List<SalesAnalysisReportVo> gmList = new ArrayList<>();
        //交易次数
        List<SalesAnalysisReportVo> tansList = new ArrayList<>();
        //客单价
        List<SalesAnalysisReportVo> priceList = new ArrayList<>();

        // 存在门店当前月统计数据
        if (CollectionUtils.isNotEmpty(list)) {
            for (int i = 2; i >= 0; i--) {
                // 当第几月
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -i);
                int month = calendar.get(Calendar.MONTH) + 1;
                boolean flg = false;
                for (SalesAnalysis salesAnalysis : list) {
                    // 年
                    String year = salesAnalysis.getAnYear();
                    // 报表月、周
                    String date = salesAnalysis.getAnDate();
                    // 存在当前月数据
                    if (String.valueOf(calendar.get(Calendar.YEAR)).equals(year) && String.valueOf(month).equals(date)) {

                        flg = true;
                        String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(month), 2, "0");
                        // 销售额
                        SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                        amt.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnAmt())) {
                            amt.setYAxis(new BigDecimal(salesAnalysis.getAnAmt()));
                        } else {
                            amt.setYAxis(BigDecimal.ZERO);
                        }
                        amtList.add(amt);
                        //毛利率
                        SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                        gm.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnGm())) {
                            gm.setYAxis(new BigDecimal(salesAnalysis.getAnGm()));
                        } else {
                            gm.setYAxis(BigDecimal.ZERO);
                        }

                        gmList.add(gm);
                        //交易次数
                        SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                        tans.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnTans())) {
                            tans.setYAxis(new BigDecimal(salesAnalysis.getAnTans()));
                        } else {
                            tans.setYAxis(BigDecimal.ZERO);
                        }
                        tansList.add(tans);
                        //客单价
                        SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                        price.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnPrice())) {
                            price.setYAxis(new BigDecimal(salesAnalysis.getAnPrice()));
                        } else {
                            price.setYAxis(BigDecimal.ZERO);
                        }
                        priceList.add(price);
                    }
                }
                if (!flg) {
                    String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(month), 2, "0");
                    // 销售额
                    SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                    amt.setXAxis(xAxis);
                    amt.setYAxis(BigDecimal.ZERO);
                    amtList.add(amt);
                    //毛利率
                    SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                    gm.setXAxis(xAxis);
                    gm.setYAxis(BigDecimal.ZERO);
                    gmList.add(gm);
                    //交易次数
                    SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                    tans.setXAxis(xAxis);
                    tans.setYAxis(BigDecimal.ZERO);
                    tansList.add(tans);
                    //客单价
                    SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                    price.setXAxis(xAxis);
                    price.setYAxis(BigDecimal.ZERO);
                    priceList.add(price);
                }
            }
        } else {
            for (int i = 2; i >= 0; i--) {
                // 当第几周
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -i);
                int month = calendar.get(Calendar.MONTH) + 1;
                String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(month), 2, "0");
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(xAxis);
                amt.setYAxis(BigDecimal.ZERO);
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(xAxis);
                gm.setYAxis(BigDecimal.ZERO);
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(xAxis);
                tans.setYAxis(BigDecimal.ZERO);
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(xAxis);
                price.setYAxis(BigDecimal.ZERO);
                priceList.add(price);
            }
        }
        // 销售额
        salesAnalysisReportVo.setAmtList(amtList);
        //毛利率
        salesAnalysisReportVo.setGmList(gmList);
        //交易次数
        salesAnalysisReportVo.setTansList(tansList);
        //客单价
        salesAnalysisReportVo.setPriceList(priceList);

        return ResultUtil.success(salesAnalysisReportVo);
    }

    @Override
    public Result getSummaryReportByWeek() {

        TokenUser user = commonService.getLoginInfo();
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();

        int year = LocalDate.now().getYear();

        int currentWeek = LocalDate.now().get(WeekFields.ISO.weekOfWeekBasedYear());

        int lastWeek = currentWeek - 11;
        if (lastWeek < 1) {
            lastWeek = 1;
        }

        // 当前年
        salesAnalysisReportVo.setYear(String.valueOf(year));
        // 当前周
        salesAnalysisReportVo.setWeek(String.valueOf(currentWeek));
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(user.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());

        StringBuilder builder = new StringBuilder()
                .append(" select ")
                .append(" a.client as \"Client\",  ")
                .append(" YEAR(a.GSSD_DATE) as \"AnYear\", ")
                .append(" WEEK(a.GSSD_DATE) as \"AnDate\", ")
                .append(" count(distinct a.gssd_bill_no) as \"AnTans\", ")
                .append(" sum(a.gssd_amt) as \"AnAmt\", ")
                .append(" round(sum(a.gssd_amt)/count(distinct a.gssd_bill_no),2) as \"AnPrice\" ")
                .append(" FROM GAIA_SD_SALE_D a ")
//                .append(" WHERE  a.client = '" + "10000001" + "' ")
                .append(" WHERE  a.client = '" + user.getClient() + "' ")
                .append(" AND YEAR(a.GSSD_DATE) = ")
                .append(year)
                .append(" ")
                .append("  AND  WEEK(a.GSSD_DATE) BETWEEN ")
                .append(lastWeek)
                .append(" AND ")
                .append(currentWeek)
                .append(" ")
                .append(" GROUP BY a.client, YEAR(a.GSSD_DATE), WEEK(a.GSSD_DATE) ")
                .append(" order by a.client, YEAR(a.GSSD_DATE), WEEK(a.GSSD_DATE) ");
        List<SalesAnalysis> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalesAnalysis.class));
        // 销售额
        List<SalesAnalysisReportVo> amtList = new ArrayList<>();
        //毛利率
        List<SalesAnalysisReportVo> gmList = new ArrayList<>();
        //交易次数
        List<SalesAnalysisReportVo> tansList = new ArrayList<>();
        //客单价
        List<SalesAnalysisReportVo> priceList = new ArrayList<>();

        // 存在门店当前月统计数据
        if (CollectionUtils.isNotEmpty(list)) {
            for (int i = lastWeek; i <= currentWeek; i++) {
                String xAxis = String.valueOf(i);
                boolean flg = false;
                for (SalesAnalysis salesAnalysis : list) {
                    if (String.valueOf(year).equals(salesAnalysis.getAnYear()) && xAxis.equals(salesAnalysis.getAnDate())) {
                        flg = true;
                        // 销售额
                        SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                        amt.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnAmt())) {
                            amt.setYAxis(new BigDecimal(salesAnalysis.getAnAmt()));
                        } else {
                            amt.setYAxis(BigDecimal.ZERO);
                        }
                        amtList.add(amt);
                        //毛利率
                        SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                        gm.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnGm())) {
                            gm.setYAxis(new BigDecimal(salesAnalysis.getAnGm()));
                        } else {
                            gm.setYAxis(BigDecimal.ZERO);
                        }

                        gmList.add(gm);
                        //交易次数
                        SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                        tans.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnTans())) {
                            tans.setYAxis(new BigDecimal(salesAnalysis.getAnTans()));
                        } else {
                            tans.setYAxis(BigDecimal.ZERO);
                        }
                        tansList.add(tans);
                        //客单价
                        SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                        price.setXAxis(xAxis);
                        if (StringUtils.isNoneEmpty(salesAnalysis.getAnPrice())) {
                            price.setYAxis(new BigDecimal(salesAnalysis.getAnPrice()));
                        } else {
                            price.setYAxis(BigDecimal.ZERO);
                        }
                        priceList.add(price);
                    }
                }
                if (!flg) {
                    // 销售额
                    SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                    amt.setXAxis(xAxis);
                    amt.setYAxis(BigDecimal.ZERO);
                    amtList.add(amt);
                    //毛利率
                    SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                    gm.setXAxis(xAxis);
                    gm.setYAxis(BigDecimal.ZERO);
                    gmList.add(gm);
                    //交易次数
                    SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                    tans.setXAxis(xAxis);
                    tans.setYAxis(BigDecimal.ZERO);
                    tansList.add(tans);
                    //客单价
                    SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                    price.setXAxis(xAxis);
                    price.setYAxis(BigDecimal.ZERO);
                    priceList.add(price);
                }
            }
        } else {
            for (int i = lastWeek; i <= currentWeek; i++) {
                String xAxis = String.valueOf(i);
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(xAxis);
                amt.setYAxis(BigDecimal.ZERO);
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(xAxis);
                gm.setYAxis(BigDecimal.ZERO);
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(xAxis);
                tans.setYAxis(BigDecimal.ZERO);
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(xAxis);
                price.setYAxis(BigDecimal.ZERO);
                priceList.add(price);
            }
        }

        // 销售额
        salesAnalysisReportVo.setAmtList(amtList);
        //毛利率
        salesAnalysisReportVo.setGmList(gmList);
        //交易次数
        salesAnalysisReportVo.setTansList(tansList);
        //客单价
        salesAnalysisReportVo.setPriceList(priceList);

        return ResultUtil.success(salesAnalysisReportVo);
    }

    @Override
    public List<SaleRank> getSaleRankTopTenByStore(Map<String,Object> map){
        TokenUser user = commonService.getLoginInfo();
        StringBuilder builder = new StringBuilder()
                .append(" select ")
                .append(" GSSD.GSSD_PRO_ID as \"proId\",  ") //销售商品门店内部编码
                .append(" GPB.PRO_NAME as \"proName\",  ")//销售商品名称
                .append(" ROUND(SUM( GSSD.GSSD_QTY ),0) as \"qty\",  ")//销售数量
                .append(" ROUND(SUM( GSSD.GSSD_AMT),2) as \"amt\",  ")//销售额
                .append(" ROUND(MAX(GSSD.GSSD_PRC2),0) as \"maxprc\",  ")//最高价
                .append(" ROUND(MIN(GSSD.GSSD_PRC2),0) as \"minprc\",  ")//最低价
                .append(" ROUND(SUM(GSSD.GSSD_AMT)-SUM(GSSD.GSSD_MOV_PRICE),2) as \"anMD\",  ")//毛利额
                .append(" ROUND(CASE WHEN SUM(GSSD.GSSD_AMT)>0  THEN  (SUM(GSSD.GSSD_AMT)-SUM(GSSD.GSSD_MOV_PRICE))/SUM( GSSD.GSSD_AMT)*100 \n" +
                        "WHEN SUM(GSSD.GSSD_AMT)<=0 THEN 0 end,2) as \"anGM\"  ") //毛利率
                .append(" FROM GAIA_SD_SALE_D GSSD ")
                .append(" INNER JOIN GAIA_PRODUCT_BUSINESS GPB ON GSSD.CLIENT = GPB.CLIENT  ")
                .append(" AND GSSD.GSSD_BR_ID = GPB.PRO_SITE AND GSSD.GSSD_PRO_ID = GPB.PRO_SELF_CODE ")
//                .append(" WHERE  GSSD.client = '" + "10000005" + "' ");
                .append(" WHERE  GSSD.client = '" + user.getClient() + "' ");
                if( ObjectUtils.isNotEmpty(map.get("type")) && "2".equals(map.get("type"))){ //1是昨天 2是今天
                    builder.append(" AND GSSD.GSSD_DATE =current_date() ");
                }else {
                    builder.append(" AND GSSD.GSSD_DATE =TIMESTAMPADD(DAY, -1, current_date()) ");
                }
        if(ObjectUtils.isNotEmpty(map.get("depId"))){
            builder.append(" AND GSSD.GSSD_BR_ID='" + map.get("depId") + "' ");
//            builder.append(" AND GSSD.GSSD_BR_ID='" + "10000" + "' ");
        }
        builder.append(" GROUP BY GSSD.GSSD_PRO_ID,GPB.PRO_NAME ")
                .append(" order by SUM( GSSD.GSSD_AMT) DESC ");
        if(ObjectUtils.isNotEmpty(map.get("amount")) || (Integer)map.get("amount")>=0 ){
            builder.append(" LIMIT " + map.get("amount") + " ");
        }
        System.out.println(builder.toString());
        List<SaleRank> list = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SaleRank.class));
        System.out.println(list.toArray());
        return list;
    }

    @Override
    public LoginSalePlan getLoginSalePlanByMonth(String depId) {
        LoginSalePlan data = new LoginSalePlan();

//        String client = "10000001";
//        String startData = "2020-01-01";
//        String endData = "2020-12-31";

        String startData = DateUtils.getMonthStartData();
        String endData = DateUtils.getMonthEndData();

        TokenUser user = commonService.getLoginInfo();
        SalePlan plan =new  SalePlan();//计划数据
        EffectiveSale effectiveSale = new EffectiveSale();
        StringBuilder builder = new StringBuilder()
                .append(" select a.client, ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" a.gssd_br_id as \"brId\", ");
        }
        builder.append(" a.gcd_month as \"month\",")
                .append(" a.gssd_days as \"day\", ")
                .append(" b.t_amt as \"tAmt\" , ")
                .append(" b.t_gross as \"tGross\", ")
                .append(" b.t_mcard_qty as \"tMcardQty\" ")
                .append(" from ")
                /**  START
                 *第一张表开始 a,
                 */
                .append(" ( select ")
                .append(" GAIA_SD_SALE_D.client as client, ")//客户编号/加盟商编号
                .append(" D.GCD_YEAR||D.GCD_MONTH as gcd_month, ");// 销售日期(年周)
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" count(distinct GSSD_DATE) as GSSD_DAYS,")//销售天数
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")// 销售额
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT ")//毛利额
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_STORE_DATA as S ")
                .append(" ON GAIA_SD_SALE_D.CLIENT = S.CLIENT AND GAIA_SD_SALE_D.GSSD_BR_ID = S.STO_CODE ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ")
                .append(" and gssd_date BETWEEN '"+ startData+"' AND '"+endData+"' ")
                .append(" and GAIA_SD_SALE_D.client='"+ user.getClient() +"' ");
                if(StringUtils.isNotEmpty(depId)){
                    builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID='"+ depId +"' ");
                }
                builder.append(" group by GAIA_SD_SALE_D.client, GSSD_BR_ID ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" ,GAIA_SD_SALE_D.GSSD_BR_ID ");
        }
        builder.append(" ,D.GCD_YEAR||D.GCD_MONTH ")
                .append(" order by D.GCD_YEAR||D.GCD_MONTH asc ) as a, ")
                /**END
                 *第一张表结束 a
                 */
                /**  START
                 *第二张表开始 b
                 */
                .append(" (select ")
                .append(" client,")
                .append(" month_plan, ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" sto_code,");
        }
        builder.append(" sum(T_SALE_AMT) as t_amt, ")
                .append(" sum(T_SALE_GROSS) as t_gross, ")
                .append(" sum(T_MCARD_QTY) as t_mcard_qty ")
                .append("  from GAIA_SALETASK_DPLAN ")
                .append(" group by client,month_plan ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" ,sto_code ");
        }
    builder.append( " ) as b")

                /**END
                 *第二张表结束 a
                 */
                .append(" where a.client = b.client ")
                .append(" and a.gcd_month = b.month_plan ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" and a.gssd_br_id = b.sto_code ");
        }
        System.out.println(builder.toString());
        List<SalePlan> salePlans = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalePlan.class));
        if(ObjectUtils.isNotEmpty(salePlans)){
            System.out.println(salePlans.get(0).toString());
            plan =salePlans.get(0);
            data.setMonth(plan.getMonth());
            data.setDay(plan.getDay());
            data.settAmt(new BigDecimal(plan.gettAmt()));
            data.settGross(new BigDecimal(plan.gettGross()));
            data.settMcardQty(new BigDecimal(plan.gettMcardQty()));
        }
        Map map = new HashMap();
        map.put("client",user.getClient());
        map.put("depId",depId);
        map.put("startData",startData);
        map.put("endData",endData);
        effectiveSale = saleDService.selectSalePlan(map);
        if(ObjectUtils.isNotEmpty(effectiveSale)){
            //实际数据
            data.setaAmt(new BigDecimal(effectiveSale.getAmt()));
            data.setaGross(new BigDecimal(effectiveSale.getGross()));
            data.setaMcardQty(new BigDecimal(effectiveSale.getCardQty()));
            //百分比
//            data.setrAmt((data.getaAmt().divide(data.gettAmt()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
//            data.setrGross((data.getaGross().divide(data.gettGross()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
//            data.setrMcardQty((data.getaMcardQty().divide(data.getaMcardQty()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");

            data.setrAmt( DecimalUtils.toPercentStr(data.getaAmt().divide(data.gettAmt(),2,BigDecimal.ROUND_DOWN)));
            data.setrGross(DecimalUtils.toPercentStr(data.getaGross().divide(data.gettGross(),2,BigDecimal.ROUND_DOWN)));
            data.setrMcardQty(DecimalUtils.toPercentStr(data.getaMcardQty().divide(data.getaMcardQty(),2,BigDecimal.ROUND_DOWN)));

            //订单数量
            data.setNoQty(effectiveSale.getNoQty());
            //日均交易
            data.setDayQty(String.valueOf(new BigDecimal(effectiveSale.getNoQty()).multiply(new BigDecimal(plan.getDay())).setScale(2, BigDecimal.ROUND_HALF_UP)));
            //客单价
            data.setPct(String.valueOf(new BigDecimal(effectiveSale.getAmt()).multiply(new BigDecimal(effectiveSale.getNoQty())).setScale(4, BigDecimal.ROUND_HALF_UP)));
        }
        if(StringUtils.isNotEmpty(depId)){
            List<ShopAssistantOutData> shopAssistants = this.getShopAssistantSalePlan(map);
            data.setShopAssistants(shopAssistants);
        }
        return data;
    }


    @Override
    public LoginSalePlan getLoginSalePlanByDay(String depId) {
        LoginSalePlan data = new LoginSalePlan();

//        String client = "10000001";
         String startData = DateUtils.getCurrentDateStr();
         String endData = DateUtils.getCurrentDateStr();
//        String startData = "2020-12-14";
//        String endData = "2020-12-14";
        TokenUser user = commonService.getLoginInfo();
        SalePlan plan =new  SalePlan();//计划数据
        EffectiveSale effectiveSale = new EffectiveSale();
        StringBuilder builder = new StringBuilder()
                .append(" select a.client, ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" a.gssd_br_id as \"brId\", ");
        }
        builder.append(" a.gcd_month as \"month\",")
                .append(" a.gssd_days as \"day\", ")
                .append(" b.t_amt as \"tAmt\" , ")
                .append(" b.t_gross as \"tGross\", ")
                .append(" b.t_mcard_qty as \"tMcardQty\" ")
                .append(" from ")
                /**  START
                 *第一张表开始 a,
                 */
                .append(" ( select ")
                .append(" GAIA_SD_SALE_D.client as client, ")//客户编号/加盟商编号
                .append(" D.GCD_YEAR||D.GCD_MONTH as gcd_month, ");// 销售日期(年周)
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" GSSD_BR_ID, ");
        }
        builder.append(" count(distinct GSSD_DATE) as GSSD_DAYS,")//销售天数
                .append(" round(sum(GSSD_AMT),2) as GSSD_AMT, ")// 销售额
                .append(" round((SUM(GSSD_AMT)-SUM(GSSD_MOV_PRICE)),2) as GSSD_GROSS_AMT ")//毛利额
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_STORE_DATA as S ")
                .append(" ON GAIA_SD_SALE_D.CLIENT = S.CLIENT AND GAIA_SD_SALE_D.GSSD_BR_ID = S.STO_CODE ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ")
                .append(" and gssd_date BETWEEN '"+ startData+"' AND '"+endData+"' ")
                .append(" and GAIA_SD_SALE_D.client='"+user.getClient() +"' ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID='"+ depId +"' ");
        }
        builder.append(" group by GAIA_SD_SALE_D.client, GSSD_BR_ID ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" ,GAIA_SD_SALE_D.GSSD_BR_ID ");
        }
        builder.append(" ,D.GCD_YEAR||D.GCD_MONTH ")
                .append(" order by D.GCD_YEAR||D.GCD_MONTH asc ) as a, ")
                /**END
                 *第一张表结束 a
                 */
                /**  START
                 *第二张表开始 b
                 */
                .append(" (select ")
                .append(" client,")
                .append(" month_plan, ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" sto_code,");
        }
        builder.append(" sum(A_SALE_AMT) as t_amt, ")
                .append(" sum(A_SALE_GROSS) as t_gross, ")
                .append(" sum(A_MCARD_QTY) as t_mcard_qty ")
                .append("  from GAIA_SALETASK_DPLAN ")
                .append(" group by client,month_plan ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" ,sto_code ");
        }
        builder.append( " ) as b")

                /**END
                 *第二张表结束 a
                 */
                .append(" where a.client = b.client ")
                .append(" and a.gcd_month = b.month_plan ");
        if(StringUtils.isNotEmpty(depId)){
            builder.append(" and a.gssd_br_id = b.sto_code ");
        }
        System.out.println(builder.toString());
        List<SalePlan> salePlans = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(SalePlan.class));
        if(ObjectUtils.isNotEmpty(salePlans)){
            System.out.println(salePlans.get(0).toString());
            plan =salePlans.get(0);
            data.setMonth(plan.getMonth());
            data.setDay(plan.getDay());
            data.settAmt(new BigDecimal(plan.gettAmt()));
            data.settGross(new BigDecimal(plan.gettGross()));
            data.settMcardQty(new BigDecimal(plan.gettMcardQty()));
        }
        Map map = new HashMap();
        map.put("client",user.getClient());
        map.put("depId",depId);
        map.put("startData",startData);
        map.put("endData",endData);
        effectiveSale = saleDService.selectSalePlan(map);
        if(ObjectUtils.isNotEmpty(effectiveSale)){
            //实际数据
            data.setaAmt(new BigDecimal(effectiveSale.getAmt()));
            data.setaGross(new BigDecimal(effectiveSale.getGross()));
            data.setaMcardQty(new BigDecimal(effectiveSale.getCardQty()));
            //百分比
//            data.setrAmt((data.getaAmt().divide(data.gettAmt()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
//            data.setrGross((data.getaGross().divide(data.gettGross()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");
//            data.setrMcardQty((data.getaMcardQty().divide(data.getaMcardQty()).setScale(2, BigDecimal.ROUND_DOWN)).multiply(BigDecimal.valueOf(100))+"%");

            data.setrAmt( DecimalUtils.toPercentStr(data.getaAmt().divide(data.gettAmt(),2,BigDecimal.ROUND_DOWN)));
            data.setrGross(DecimalUtils.toPercentStr(data.getaGross().divide(data.gettGross(),2,BigDecimal.ROUND_DOWN)));
            data.setrMcardQty(DecimalUtils.toPercentStr(data.getaMcardQty().divide(data.getaMcardQty(),2,BigDecimal.ROUND_DOWN)));

            //订单数量
            data.setNoQty(effectiveSale.getNoQty());
            //日均交易
            data.setDayQty(String.valueOf(new BigDecimal(effectiveSale.getNoQty()).multiply(new BigDecimal(plan.getDay())).setScale(2, BigDecimal.ROUND_HALF_UP)));
            //客单价
            data.setPct(String.valueOf(new BigDecimal(effectiveSale.getAmt()).multiply(new BigDecimal(effectiveSale.getNoQty())).setScale(4, BigDecimal.ROUND_HALF_UP)));
        }
        if(StringUtils.isNotEmpty(depId)){
            List<ShopAssistantOutData> shopAssistants = this.getShopAssistantSalePlan(map);
            data.setShopAssistants(shopAssistants);
        }
        return data;
    }

    /**
     * 销售员数据
     * @param map
     */
    @Override
    public List<ShopAssistantOutData>  getShopAssistantSalePlan(Map map)
    {
        List<ShopAssistantOutData> data = new ArrayList<>();

        StringBuilder builder = new StringBuilder()
                .append(" select ")
                .append(" a.gssd_days as \"day\",")
                .append(" a.GSSD_AMT as \"amt\" , ")
                .append(" a.GSSD_GROSS_AMT as \"grossAmt\", ")
                .append(" a.GSSD_SALER_ID as \"userId\", ")
                .append(" a.GSSD_BILL_NO AS \"noQty\", ")
                .append(" round(a.GSSD_BILL_NO/a.gssd_days, 2 ) \"average\", ")
                .append(" round( a.GSSD_AMT / a.GSSD_BILL_NO, 2 ) \"pct\" ")
                .append(" from ")
                /**  START
                 *第一张表开始 a
                 */
                .append(" ( select ")
                .append(" GAIA_SD_SALE_D.client as client, ")//客户编号/加盟商编号
                .append(" D.GCD_YEAR||D.GCD_MONTH as gcd_month, ")// 销售日期(年周)
                .append(" count(distinct GSSD_DATE) as GSSD_DAYS, ")//销售天数
                .append(" GSSD_BR_ID, ")// STO_NAME, --  门店名称，如果需要请删除注释符，并增加字段名至 group by后
                .append(" round( sum( GSSD_AMT ), 2 ) AS GSSD_AMT, ")
                .append(" round(( SUM( GSSD_AMT )- SUM( GSSD_MOV_PRICE )), 2 ) AS GSSD_GROSS_AMT, ")
                .append(" GSSD_SALER_ID, ")
                .append(" count( DISTINCT GSSD_BILL_NO ) AS GSSD_BILL_NO ")
                .append(" FROM GAIA_SD_SALE_D as GAIA_SD_SALE_D ")
                .append(" INNER JOIN GAIA_STORE_DATA as S ")
                .append(" ON GAIA_SD_SALE_D.CLIENT = S.CLIENT AND GAIA_SD_SALE_D.GSSD_BR_ID = S.STO_CODE ")
                .append(" INNER JOIN GAIA_CAL_DT as D ")
                .append(" ON GAIA_SD_SALE_D.GSSD_DATE = D.GCD_DATE ")
                .append(" where 1=1 ")
                .append(" and gssd_date BETWEEN '"+ map.get("startData")+"' AND '"+map.get("endData")+"' ")
                .append(" and GAIA_SD_SALE_D.client='"+map.get("client") +"' ");
        if(ObjectUtils.isNotEmpty( map.get("depId"))){
            builder.append(" and GAIA_SD_SALE_D.GSSD_BR_ID='"+  map.get("depId") +"' ");
        }
        builder.append(" group by GAIA_SD_SALE_D.client, GSSD_BR_ID ,GSSD_SALER_ID, D.GCD_YEAR||D.GCD_MONTH ")
                .append(" order by D.GCD_YEAR||D.GCD_MONTH asc ) as a, ")
        /**END
         *第一张表结束 a
         */
        /**  START
         *第二张表开始 b
         */
                .append(" (select ")
                .append(" client,")
                .append(" month_plan, ")
                .append(" sto_code,")
                .append(" sum(T_SALE_AMT) as t_amt, ")
                .append(" sum(T_SALE_GROSS) as t_gross, ")
                .append(" sum(T_MCARD_QTY) as t_mcard_qty ")
                .append("  from GAIA_SALETASK_DPLAN ")
                .append(" group by client,month_plan,sto_code ) as b")

                /**END
                 *第二张表结束 a
                 */
                .append(" where a.client = b.client ")
                .append(" and a.gcd_month = b.month_plan ")
                .append(" and a.GSSD_BR_ID = b.sto_code ");
        System.out.println(builder.toString());
        List<ShopAssistant> salePlans = kylinJdbcTemplate.query(builder.toString(), RowMapper.getDefault(ShopAssistant.class));
        System.out.println(salePlans.toArray());
        List<MemberCard>  cardByUser =  saleDService.selectCardByUser(map);
        //	销售员数据：当日营业额、毛利额，从麒麟直接获取。  员工当日新增会员卡，从系统数据库中获取，
        if(ObjectUtils.isNotEmpty(salePlans)){
            for (ShopAssistant shopAssistant:salePlans){
                for (MemberCard memberCard:cardByUser){
                    if(shopAssistant.getUserId().equals(memberCard.getUserId())){
                        ShopAssistantOutData assistant = new ShopAssistantOutData();
                        BeanUtils.copyProperties(shopAssistant,assistant);
                        assistant.setUserName(memberCard.getUserName());
                        assistant.setMcardQty(memberCard.getCount());
                        data.add(assistant);
                    }
                }
            }
        }else {
            return null;
        }

        return data;
    }

}
