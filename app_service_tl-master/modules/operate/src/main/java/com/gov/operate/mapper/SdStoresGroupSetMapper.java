package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SdStoresGroupSet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:57 2021/7/28
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
public interface SdStoresGroupSetMapper extends BaseMapper<SdStoresGroupSet> {
    void batchInsert(@Param("sdStoresGroupSets") List<SdStoresGroupSet> sdStoresGroupSets);

    List<SdStoresGroupSet> selectListByClient(String client);
}
