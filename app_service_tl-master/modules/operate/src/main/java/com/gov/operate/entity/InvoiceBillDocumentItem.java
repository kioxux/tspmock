package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_INVOICE_BILL_DOCUMENT_ITEM")
@ApiModel(value="InvoiceBillDocumentItem对象", description="")
public class InvoiceBillDocumentItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GIBDI_NUM")
    private String gibdiNum;

    @ApiModelProperty(value = "业务类型")
    @TableField("GIBDI_BUSINESS_TYPE")
    private String gibdiBusinessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("GIBDI_MAT_DN_ID")
    private String gibdiMatDnId;

    @ApiModelProperty(value = "物料凭证号")
    @TableField("MAT_ID")
    private String matId;

    @ApiModelProperty(value = "物料凭证年份")
    @TableField("MAT_YEAR")
    private String matYear;

    @ApiModelProperty(value = "物料凭证行号")
    @TableField("MAT_LINE_NO")
    private String matLineNo;

    @ApiModelProperty(value = "商品编码")
    @TableField("MAT_PRO_CODE")
    private String matProCode;

    @ApiModelProperty(value = "行金额")
    @TableField("GIBDI_LINE_AMOUNT")
    private BigDecimal gibdiLineAmount;

    @ApiModelProperty(value = "数量")
    @TableField("MAT_QTY")
    private BigDecimal matQty;

    @ApiModelProperty(value = "总金额（批次）")
    @TableField("MAT_BAT_AMT")
    private BigDecimal matBatAmt;

    @ApiModelProperty(value = "税金（批次）")
    @TableField("MAT_RATE_BAT")
    private BigDecimal matRateBat;

    @ApiModelProperty(value = "创建人")
    @TableField("GIBDI_CREATE_USER")
    private String gibdiCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GIBDI_CREATE_DATE")
    private String gibdiCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GIBDI_CREATE_TIME")
    private String gibdiCreateTime;


}
