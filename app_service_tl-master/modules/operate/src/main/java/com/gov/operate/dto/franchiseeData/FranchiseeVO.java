package com.gov.operate.dto.franchiseeData;

import lombok.Data;

@Data
public class FranchiseeVO {
   private String client; //加盟商
   private String francName; //品牌名称
   private String francNo;
   private String francLegalPerson;
   private String francLegalPersonName;//负责人
   private String francQua;
   private String francPhone; //手机号
   private String francAddr;
   private String francCreDate;
   private String francCreTime;
   private String francCreId;
   private String francModiDate;
   private String francModiTime;
   private String francModiId;
   private String francLogo;
   private String francType1;//公司性质-单体店
   private String francType2;//公司性质-连锁公司
   private String francType3;//公司性质-批发公司
   private String francAss;  //委托人
   private String francAssName;//委托人姓名
   private String francAssPhone;//委托人手机号
   private String userPassword;
   private String code;       //验证码
}
