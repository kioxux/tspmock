package com.gov.operate.service;

import com.gov.operate.entity.SdStockBatch;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
public interface ISdStockBatchService extends SuperService<SdStockBatch> {

}
