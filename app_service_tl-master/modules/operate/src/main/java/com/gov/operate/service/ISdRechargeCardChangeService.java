package com.gov.operate.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.RechargeCardLVO;
import com.gov.operate.dto.RechargeCardListVO;
import com.gov.operate.entity.SdRechargeCardChange;
import com.gov.operate.entity.StoreData;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
public interface ISdRechargeCardChangeService extends SuperService<SdRechargeCardChange> {

    Result getRechargeCardList(RechargeCardListVO entity, Page<RechargeCardLVO> page);

    /**
     * 当前登录人有权限的门店列表_储值卡充值查询
     *
     * @return 权限门店列表
     */
    List<StoreData> getCurrentUserAuthStoList();
}
