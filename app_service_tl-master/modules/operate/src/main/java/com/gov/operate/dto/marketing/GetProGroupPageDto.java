package com.gov.operate.dto.marketing;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 16:22 2021/8/16
 * @Description：查询商品分组入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GetProGroupPageDto {

    private String queryString;
    @NotNull(message = "分页参数不能为空")
    private Integer pageNum;

    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;
}
