package com.gov.operate.mapper;

import com.gov.operate.entity.FiInvoiceRegistrationDetailed;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface FiInvoiceRegistrationDetailedMapper extends BaseMapper<FiInvoiceRegistrationDetailed> {

}
