package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @TableName GAIA_SSP_USER_SUPPLIER
 */
@TableName(value = "GAIA_SSP_USER_SUPPLIER")
@Data
public class SspUserSupplier extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 有效期起
     */
    private Date expiryStartDate;

    /**
     * 有效期至
     */
    private Date expiryEndDate;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}