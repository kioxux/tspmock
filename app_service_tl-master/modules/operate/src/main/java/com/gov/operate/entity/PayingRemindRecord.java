package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 加盟商余额变动记录
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PAYING_REMIND_RECORD")
@ApiModel(value="PayingRemindRecord对象", description="加盟商余额变动记录")
public class PayingRemindRecord extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "变动金额")
    @TableField("GPRR_AMT")
    private BigDecimal gprrAmt;

    @ApiModelProperty(value = "说明 ")
    @TableField("GPRR_NOTE")
    private String gprrNote;

    @ApiModelProperty(value = "变动时间")
    @TableField("GPRR_TIME")
    private LocalDateTime gprrTime;

    @ApiModelProperty(value = "变动类型")
    @TableField("GPRR_TYPE")
    private Integer gprrType;

    @ApiModelProperty(value = "是否抵扣成功，1成功")
    @TableField("GPRR_INFORCE")
    private Integer gprrInforce;
}
