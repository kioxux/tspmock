package com.gov.operate.service;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.SdPromGroupProDto;
import com.gov.operate.entity.SdPromGroupPro;

import java.util.List;

/**
 * <p>
 * 促销商品分明细组表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-11-02
 */
public interface ISdPromGroupProService extends SuperService<SdPromGroupPro> {

    /**
     * 商品列表
     *
     * @param groupId
     * @param proClass
     * @param proBrandClass
     * @return
     */
    Result getSdPromGroupProList(String groupId, String proClass, String proBrandClass);

    /**
     * 商品删除
     *
     * @param groupId
     * @param proList
     * @return
     */
    Result delSdPromGroupPro(String groupId, List<String> proList);

    /**
     * 商品保存
     *
     * @param groupId
     * @param proList
     * @return
     */
    Result saveSdPromGroupPro(String groupId, List<String> proList);

    /**
     * 添加促销商品查询
     */
    Result getProInfoList(SdPromGroupProDto dto);

    Result getProductInfoList(Integer pageNum, Integer pageSize, String content);

    Result getProBrandClass();
}
