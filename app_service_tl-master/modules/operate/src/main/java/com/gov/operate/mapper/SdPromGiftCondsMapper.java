package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ProductRequestVo;
import com.gov.operate.dto.ProductResponseVo;
import com.gov.operate.entity.SdPromGiftConds;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
public interface SdPromGiftCondsMapper extends BaseMapper<SdPromGiftConds> {

    void deleteList(@Param("list") List<SdPromGiftConds> sdPromGiftCondsDeleteList);

    IPage<ProductResponseVo> selectProductList(Page<SdPromGiftConds> page, @Param("product") ProductRequestVo productRequestVo);

    List<ProductResponseVo> selectGiftProductList(@Param("product") ProductRequestVo productRequestVo);
}
