package com.gov.operate.service.impl;

import com.gov.operate.entity.Compadm;
import com.gov.operate.mapper.CompadmMapper;
import com.gov.operate.service.ICompadmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@Service
public class CompadmServiceImpl extends ServiceImpl<CompadmMapper, Compadm> implements ICompadmService {

}
