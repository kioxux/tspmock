package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.SdPromSeriesSet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-07
 */
public interface SdPromSeriesSetMapper extends BaseMapper<SdPromSeriesSet> {


    void deleteList(@Param("list") List<SdPromSeriesSet> sdPromSeriesSetDeleteList);
}
