package com.gov.operate.service;

import com.gov.operate.entity.SdMarketingProm;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
public interface ISdMarketingPromService extends SuperService<SdMarketingProm> {

}
