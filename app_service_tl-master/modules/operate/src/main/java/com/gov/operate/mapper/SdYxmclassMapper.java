package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.GetProGroupListPageDTO;
import com.gov.operate.entity.SdYxmclass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
public interface SdYxmclassMapper extends BaseMapper<SdYxmclass> {

    /**
     * 获取数字编码最大值
     */
    String getMaxGroup();

    /**
     * 商品组列表分页
     */
    IPage<GetProGroupListPageDTO> getProGroupListPage(Page<GetProGroupListPageDTO> page, @Param("ew") QueryWrapper<GetProGroupListPageDTO> ew);
}
