package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangp
 * @since 2021-03-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BILL_PAYMENT_APPLY")
@ApiModel(value = "PaymentApply对象", description = "")
public class PaymentApply extends BaseEntity {

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "总金额")
    @TableField("GBPA_TOTAL_AMT")
    private BigDecimal gbpaTotalAmt;

    @ApiModelProperty(value = "本次申请金额")
    @TableField("GBPA_PAY_AMT")
    private BigDecimal gbpaPayAmt;

    @ApiModelProperty(value = "申请人")
    @TableField("APPROVAL_USER")
    private String approvalUser;

    @ApiModelProperty(value = "审批状态")
    @TableField("APPROVAL_STATUS")
    private int approvalStatus;

    @ApiModelProperty(value = "工作流编码")
    @TableField("APPROVAL_FLOW_NO")
    private String approvalFlowNo;

    @ApiModelProperty(value = "创建人")
    @TableField("GBPA_CREATE_USER")
    private String gbpaCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBPA_CREATE_DATE")
    private String gbpaCreateDate;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBPA_CREATE_TIME")
    private String gbpaCreateTime;

    @ApiModelProperty(value = "付款单号")
    @TableField("GBPA_FLOW_NO")
    private String gbpaFlowNo;
}
