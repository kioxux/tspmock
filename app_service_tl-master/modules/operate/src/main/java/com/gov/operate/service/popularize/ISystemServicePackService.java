package com.gov.operate.service.popularize;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.charge.SaveClientPriceDTO;
import com.gov.operate.entity.SystemServicePack;

import java.util.List;

/**
 * <p>
 * 功能服务包基础信息 服务类
 * </p>
 *
 * @author sy
 * @since 2021-07-27
 */
public interface ISystemServicePackService extends SuperService<SystemServicePack> {

    /**
     * 官网价维护
     *
     * @param list
     */
    void editOfficialPrice(List<SystemServicePack> list);

    /**
     * 收费项目集合
     *
     * @param spId
     * @param spSeries
     * @return
     */
    Result getOfficialPriceList(String spId, String spSeries);

    Result getList();

    /**
     * 用户收费项目
     *
     * @param contractCode
     */
    Result getClientPriceItemByContract(String contractCode,String clientId);

    /**
     * 收费门店集合
     *
     * @return
     */
    Result selectPayStore();

    void saveClientPriceItemByContract(SaveClientPriceDTO saveClientPriceDTO);
}
