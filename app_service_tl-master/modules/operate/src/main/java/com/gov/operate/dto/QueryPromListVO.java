package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryPromListVO {

    private String gsphName;

    private String gsphBeginDate;

    private String gsphEndDate;

    private String gsphType;

    private String gsphStatus;

    private Integer pageNum;

    private Integer pageSize;

    /**
     * 创建人
     */
    private String gsphUpdateEmp;
    /**
     * 更新日期开始
     */
    private String gsphUpdateDateStart;
    /**
     * 更新日期结束
     */
    private String gsphUpdateDateEnd;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 促销单号
     */
    private String gsphVoucherId;

}
