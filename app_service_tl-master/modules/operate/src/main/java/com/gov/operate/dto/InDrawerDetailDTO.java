package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @Author staxc
 * @Date 2020/10/15 11:55
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class InDrawerDetailDTO {
    /**
     * 商品编号
     */
    private String gsidProId;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 批号
     */
    private String gsidBatchNo;

    /**
     * 有效期
     */
    private String gsidValidDate;

    /**
     * 装斗数量
     */
    private BigDecimal gsidQty;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 批准文号
     */
    private String proRegisterNo;

}
