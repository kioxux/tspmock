package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_SALE_PAY_MSG")
@ApiModel(value="SdSalePayMsg对象", description="")
public class SdSalePayMsg extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "店号")
    @TableField("GSSPM_BR_ID")
    private String gsspmBrId;

    @ApiModelProperty(value = "日期")
    @TableField("GSSPM_DATE")
    private String gsspmDate;

    @ApiModelProperty(value = "单号")
    @TableField("GSSPM_BILL_NO")
    private String gsspmBillNo;

    @ApiModelProperty(value = "支付编码")
    @TableField("GSSPM_ID")
    private String gsspmId;

    @ApiModelProperty(value = "支付类型")
    @TableField("GSSPM_TYPE")
    private String gsspmType;

    @ApiModelProperty(value = "支付名称")
    @TableField("GSSPM_NAME")
    private String gsspmName;

    @ApiModelProperty(value = "支付卡号")
    @TableField("GSSPM_CARD_NO")
    private String gsspmCardNo;

    @ApiModelProperty(value = "支付金额")
    @TableField("GSSPM_AMT")
    private BigDecimal gsspmAmt;

    @ApiModelProperty(value = "RMB收款金额")
    @TableField("GSSPM_RMB_AMT")
    private BigDecimal gsspmRmbAmt;

    @ApiModelProperty(value = "找零金额")
    @TableField("GSSPM_ZL_AMT")
    private BigDecimal gsspmZlAmt;


}
