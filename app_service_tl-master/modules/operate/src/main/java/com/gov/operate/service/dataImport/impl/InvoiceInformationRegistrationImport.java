package com.gov.operate.service.dataImport.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.dataImport.InvoiceInformationRegistration;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IFicoInvoiceInformationRegistrationService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 发票导入
 */
@Service
public class InvoiceInformationRegistrationImport extends BusinessImport {

    @Resource
    private CommonService commonService;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private DcDataMapper dcDataMapper;
    @Resource
    private SupplierBusinessMapper supplierBusinessMapper;
    @Resource
    private IFicoInvoiceInformationRegistrationService iFicoInvoiceInformationRegistrationService;
    @Resource
    private FicoInvoiceInformationRegistrationMapper ficoInvoiceInformationRegistrationMapper;
    @Resource
    private SupplierSalesmanMapper supplierSalesmanMapper;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = commonService.getLoginInfo();
        // 导入数据
        List<InvoiceInformationRegistration> invoiceInformationRegistrationList = new ArrayList<>();
        StringBuilder supplierSalesmanKey = new StringBuilder();
        for (Integer key : map.keySet()) {
            InvoiceInformationRegistration invoiceInformationRegistration = (InvoiceInformationRegistration) map.get(key);
            invoiceInformationRegistrationList.add(invoiceInformationRegistration);
            if (StringUtils.isNotBlank(invoiceInformationRegistration.getInvoiceSalesman())) {
                supplierSalesmanKey.append(",('" + invoiceInformationRegistration.getSiteCode()
                        + "', '"
                        + invoiceInformationRegistration.getSupCode() + "', '"
                        + invoiceInformationRegistration.getInvoiceSalesman()
                        + "')");
            }
        }
        supplierSalesmanKey.delete(0, 1);
        // 不重复的地点 + 供应商
        Map<String, List<InvoiceInformationRegistration>> invoiceInformationRegistrationMap = invoiceInformationRegistrationList.stream().collect(Collectors.groupingBy(t -> t.getSiteCode() + "_" + t.getSupCode()));
        StringBuilder supKey = new StringBuilder();
        invoiceInformationRegistrationMap.keySet().forEach(t -> {
            supKey.append(",('" + t.split("_")[0] + "', '" + t.split("_")[1] + "')");
        });
        supKey.delete(0, 1);
        // 供应商集合
        List<SupplierBusiness> supplierBusinesseList = supplierBusinessMapper.selectList(
                new LambdaQueryWrapper<SupplierBusiness>().eq(SupplierBusiness::getClient, user.getClient()).apply(" (SUP_SITE, SUP_SELF_CODE) IN " + "(" + supKey.toString() + ") ")
        );
        // 门店集合
        List<StoreData> storeDataList = storeDataMapper.selectList(new LambdaQueryWrapper<StoreData>().eq(StoreData::getClient, user.getClient())
                .in(StoreData::getStoCode, invoiceInformationRegistrationList.stream().map(InvoiceInformationRegistration::getSiteCode).collect(Collectors.toList())));
        // DC集合
        List<DcData> dcDataList = dcDataMapper.selectList(new LambdaQueryWrapper<DcData>().eq(DcData::getClient, user.getClient())
                .in(DcData::getDcCode, invoiceInformationRegistrationList.stream().map(InvoiceInformationRegistration::getSiteCode).collect(Collectors.toList())));
        // 发票集合
        List<FicoInvoiceInformationRegistration> existsList = ficoInvoiceInformationRegistrationMapper.selectList(new LambdaQueryWrapper<FicoInvoiceInformationRegistration>()
                .eq(FicoInvoiceInformationRegistration::getClient, user.getClient())
                .in(FicoInvoiceInformationRegistration::getInvoiceNum, invoiceInformationRegistrationList.stream().map(InvoiceInformationRegistration::getInvoiceNum).collect(Collectors.toList())));
        // 业务员
        List<SupplierSalesman> supplierSalesmanList = new ArrayList<>();
        if (StringUtils.isNotBlank(supplierSalesmanKey.toString())) {
            supplierSalesmanList = supplierSalesmanMapper.selectList(new LambdaQueryWrapper<SupplierSalesman>().eq(SupplierSalesman::getClient, user.getClient())
                    .apply(" (SUP_SITE, SUP_SELF_CODE, GSS_NAME) IN " + "(" + supplierSalesmanKey.toString() + ") "));
        }
        // 数据组装验证
        List<String> errorList = new ArrayList<>();
        List<FicoInvoiceInformationRegistration> ficoInvoiceInformationRegistrationList = new ArrayList<>();
        List<String> invoiceNumList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            InvoiceInformationRegistration invoiceInformationRegistration = (InvoiceInformationRegistration) map.get(key);
            FicoInvoiceInformationRegistration existsObj = existsList.stream().filter(t -> t.getInvoiceNum().equals(invoiceInformationRegistration.getInvoiceNum()))
                    .findFirst().orElse(null);
            if (existsObj != null) {
                errorList.add(MessageFormat.format("第{0}行：发票编号已存在", key + 1));
                continue;
            }
            if (invoiceNumList.contains(invoiceInformationRegistration.getInvoiceNum())) {
                errorList.add(MessageFormat.format("第{0}行：发票编号重复", key + 1));
                continue;
            }
            // 地点验证
            if (invoiceInformationRegistration.getInvoiceSiteTypeValue().equals(CommonEnum.InvoiceSiteType.InvoiceSiteType1.getCode())) {
                StoreData storeData = storeDataList.stream().filter(t -> t.getStoCode().equals(invoiceInformationRegistration.getSiteCode())).findFirst().orElse(null);
                if (storeData == null) {
                    errorList.add(MessageFormat.format("第{0}行：请填入正确的地点", key + 1));
                    continue;
                }
            }
            if (invoiceInformationRegistration.getInvoiceSiteTypeValue().equals(CommonEnum.InvoiceSiteType.InvoiceSiteType2.getCode())) {
                DcData dcData = dcDataList.stream().filter(t -> t.getDcCode().equals(invoiceInformationRegistration.getSiteCode())).findFirst().orElse(null);
                if (dcData == null) {
                    errorList.add(MessageFormat.format("第{0}行：请填入正确的地点", key + 1));
                    continue;
                }
            }
            // 供应商验证
            SupplierBusiness supplierBusiness = supplierBusinesseList.stream().filter(t ->
                    t.getSupSite().equals(invoiceInformationRegistration.getSiteCode()) && t.getSupSelfCode().equals(invoiceInformationRegistration.getSupCode())
            ).findFirst().orElse(null);
            if (supplierBusiness == null) {
                errorList.add(MessageFormat.format("第{0}行：请填入正确的供应商", key + 1));
                continue;
            }
            if (StringUtils.isNotBlank(invoiceInformationRegistration.getInvoiceSalesman())) {
                SupplierSalesman supplierSalesman = supplierSalesmanList.stream().filter(t ->
                        t.getGssName().equals(invoiceInformationRegistration.getInvoiceSalesman()) &&
                                t.getSupSite().equals(invoiceInformationRegistration.getSiteCode()) &&
                                t.getSupSelfCode().equals(invoiceInformationRegistration.getSupCode())
                ).findFirst().orElse(null);
                if (supplierSalesman == null) {
                    errorList.add(MessageFormat.format("第{0}行：业务员不存在", key + 1));
                    continue;
                } else {
                    invoiceInformationRegistration.setInvoiceSalesman(supplierSalesman.getGssCode());
                }
            }
            invoiceNumList.add(invoiceInformationRegistration.getInvoiceNum());
            FicoInvoiceInformationRegistration ficoInvoiceInformationRegistration = new FicoInvoiceInformationRegistration();
            BeanUtils.copyProperties(invoiceInformationRegistration, ficoInvoiceInformationRegistration);
            ficoInvoiceInformationRegistration.setClient(user.getClient());
            ficoInvoiceInformationRegistration.setInvoiceRate(BigDecimal.ZERO);
            ficoInvoiceInformationRegistration.setInvoiceRegisteredAmount(BigDecimal.ZERO);
            ficoInvoiceInformationRegistration.setInvoicePaidAmount(BigDecimal.ZERO);
            ficoInvoiceInformationRegistration.setInvoiceAmountExcludingTax(new BigDecimal(invoiceInformationRegistration.getInvoiceAmountExcludingTax()));
            ficoInvoiceInformationRegistration.setInvoiceTax(new BigDecimal(invoiceInformationRegistration.getInvoiceTax()));
            if (StringUtils.isNotBlank(invoiceInformationRegistration.getInvoiceDiscount())) {
                ficoInvoiceInformationRegistration.setInvoiceDiscount(new BigDecimal(invoiceInformationRegistration.getInvoiceDiscount()));
            }
            ficoInvoiceInformationRegistration.setInvoiceTotalInvoiceAmount(
                    ficoInvoiceInformationRegistration.getInvoiceAmountExcludingTax().add(ficoInvoiceInformationRegistration.getInvoiceTax())
            );
            // 登记状态
            ficoInvoiceInformationRegistration.setInvoiceRegistrationStatus("0");
            // 付款状态
            ficoInvoiceInformationRegistration.setInvoicePaymentStatus("0");
            // 创建日期
            ficoInvoiceInformationRegistration.setInvoiceCreationDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建人
            ficoInvoiceInformationRegistration.setInvoiceFounder(user.getUserId());
            // 发票确认金额
            ficoInvoiceInformationRegistration.setInvoiceConfirmationAmount(ficoInvoiceInformationRegistration.getInvoiceTotalInvoiceAmount());
            ficoInvoiceInformationRegistration.setInvoiceType(invoiceInformationRegistration.getInvoiceTypeValue());
            ficoInvoiceInformationRegistration.setInvoiceSiteType(invoiceInformationRegistration.getInvoiceSiteTypeValue());
            ficoInvoiceInformationRegistration.setInvoiceSalesman(invoiceInformationRegistration.getInvoiceSalesman());
            ficoInvoiceInformationRegistrationList.add(ficoInvoiceInformationRegistration);
        }
        // 验证不通过
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        // 批量导入
        if (!CollectionUtils.isEmpty(ficoInvoiceInformationRegistrationList)) {
            iFicoInvoiceInformationRegistrationService.saveBatch(ficoInvoiceInformationRegistrationList);
        }
        return ResultUtil.success();
    }
}
