package com.gov.operate.dto.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("会员管理")
@Data
public class MemberInfoDTO {

    @ApiModelProperty("会员ID")
    private String gsmbMemberId;

    @ApiModelProperty("会员姓名")
    private String gsmbName;

    @ApiModelProperty("性别")
    private String gsmbSex;

    @ApiModelProperty("会员年龄")
    private String gsmbAge;

    @ApiModelProperty("手机")
    private String gsmbMobile;

    @ApiModelProperty("开卡")
    private String stoName;

    @ApiModelProperty("卡号")
    private String gsmbcCardId;
}
