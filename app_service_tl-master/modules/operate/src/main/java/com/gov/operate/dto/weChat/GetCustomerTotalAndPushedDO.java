package com.gov.operate.dto.weChat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetCustomerTotalAndPushedDO {

    /**
     * mediaId 微信标识
     */
    private String gwthMediaId;

    /**
     * 已经发布的数量
     */
    private Integer pushCount;
}
