package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_MESSAGE_TEMPLATE")
@ApiModel(value="MessageTemplate对象", description="")
public class MessageTemplate extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("GMT_ID")
    private String gmtId;

    @ApiModelProperty(value = "消息名称")
    @TableField("GMT_NAME")
    private String gmtName;

    @ApiModelProperty(value = "消息标题")
    @TableField("GMT_TITLE")
    private String gmtTitle;

    @ApiModelProperty(value = "消息内容")
    @TableField("GMT_CONTENT")
    private String gmtContent;

    @ApiModelProperty(value = "业务类型")
    @TableField("GMT_BUSINESS_TYPE")
    private Integer gmtBusinessType;

    @ApiModelProperty(value = "是否跳转")
    @TableField("GMT_GO_PAGE")
    private Integer gmtGoPage;

    @ApiModelProperty(value = "消息类型")
    @TableField("GMT_TYPE")
    private Integer gmtType;

    @ApiModelProperty(value = "表示形式")
    @TableField("GMT_SHOW_TYPE")
    private Integer gmtShowType;

    @ApiModelProperty(value = "是否启用")
    @TableField("GMT_FLAG")
    private Integer gmtFlag;

    @ApiModelProperty(value = "按日期循环1勾选0未勾选")
    @TableField("GMT_SEND_DATE")
    private Integer gmtSendDate;

    @ApiModelProperty(value = "按日期循环设置值")
    @TableField("GMT_SEND_DATE_SETTING_NUM")
    private Integer gmtSendDateSettingNum;

    @ApiModelProperty(value = "按星期循环 1勾选0未勾选")
    @TableField("GMT_SEND_WEEK")
    private Integer gmtSendWeek;

    @ApiModelProperty(value = "按星期循环 设置值")
    @TableField("GMT_SEND_WEEK_SETTING_NUM")
    private Integer gmtSendWeekSettingNum;

    @ApiModelProperty(value = "按活动设置 1勾选0未勾选")
    @TableField("GMT_SEND_ACTIVITY")
    private Integer gmtSendActivity;

    @ApiModelProperty(value = "按活动设置值")
    @TableField("GMT_SEND_ACTIVITY_SETTING_NUM")
    private Integer gmtSendActivitySettingNum;

    @ApiModelProperty(value = "推送时间")
    @TableField("GMT_SEND_TIME")
    private Integer gmtSendTime;

    @ApiModelProperty(value = "启用或者停用 1立即启用 0立即停用 2定时启用 3 定时停用")
    @TableField("EFFECT_MODE")
    private Integer gmtEffectMode;

    @ApiModelProperty(value = "启用生效日期")
    @TableField("EFFECT_DATE")
    private Integer gmtEffectDate;

    @ApiModelProperty(value = "启用生效时间")
    @TableField("EFFECT_TIME")
    private Integer gmtEffectTime;

    @ApiModelProperty(value = "门店人员是否显示毛利数据 1是0否")
    @TableField("GMT_IF_SHOW_MAO")
    private Integer gmtIfShowMao;

}
