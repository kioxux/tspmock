package com.gov.operate.dto.ssp;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @TableName GAIA_SSP_USER_PRODUCT
 */
@Data
public class SspUserProductPageDTO {

    @NotNull(message = "分页不能为空")
    private Integer pageNum;

    @NotNull(message = "分页不能为空")
    private Integer pageSize;

    /**
     * 用户ID
     */
    @NotNull(message = "用户主键不能为空")
    private Long userId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商自编码
     */
    private List<String> creditCodeList;

    /**
     * 连锁总部
     */
    private List<String> chainHeadList;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品名称
     */
    private String proName;


}