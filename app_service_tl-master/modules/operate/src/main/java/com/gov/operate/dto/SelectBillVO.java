package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectBillVO extends Pageable {

    /**
     * 地点
     */
    private String site;
    /**
     * 单体店
     */
    private String stoCode;
    // @NotBlank(message = "供应商不能为空")
    private String supSelfCode;

    /**
     * 日期起始
     */
    private String dateStart;
    /**
     * 日期结束
     */
    private String dateEnd;
    /**
     * 单据起始
     */
    private String billNoStart;
    /**
     * 单据结束
     */
    private String billNoEnd;
    /**
     * 单据
     */
    private String billNum;

    private String client;

    /**
     * 业务员
     */
    private String invoiceSalesman;
}
