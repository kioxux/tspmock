package com.gov.operate.mapper;

import com.gov.operate.entity.SdMarketingSto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
public interface SdMarketingStoMapper extends BaseMapper<SdMarketingSto> {

}
