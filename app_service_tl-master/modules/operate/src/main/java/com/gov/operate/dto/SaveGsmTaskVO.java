package com.gov.operate.dto;

import com.gov.operate.entity.SdMarketingGiveaway;
import com.gov.operate.entity.SdMarketingPrd;
import com.gov.operate.entity.SdPromVariableSet;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class SaveGsmTaskVO {

    private Integer id;

    private String gsmThename;

    private String gsmTheintro;

    private String gsmType;

    private String gsmStartd;

    private String gsmEndd;

    private String gsmCycleid;

    private BigDecimal gsmIndexValue;

    private BigDecimal gsmTargetValue;

    private BigDecimal gsmSaleinc;

    private String gsmPricechoose;

    private BigDecimal gsmPriceinc;

    private BigDecimal gsmActPriceinc;

    private BigDecimal gsmPriceincSt;

    private String gsmStchoose;

    private BigDecimal gsmStinc;

    private BigDecimal gsmActStinc;

    private BigDecimal gsmStincPrice;

    private BigDecimal gsmGrprofit;

    private String gsmTempid;

    private String gsmSmscondi;

    private BigDecimal gsmSmstimes;

    private String gsmTelcondi;

    private BigDecimal gsmTeltimes;

    private BigDecimal gsmDmtimes;

    private String gsmDmsend;

    private BigDecimal gsmDmprice;

    private String gsmDmrecovery;

//    /**
//     * 零售价下限
//     */
//    private BigDecimal retailPriceBegin;
//    /**
//     * 零售价上限
//     */
//    private BigDecimal retailPriceEnd;
//    /**
//     * 毛利率下限
//     */
//    private BigDecimal profitsBegin;
//    /**
//     * 毛利率上限
//     */
//    private BigDecimal profitsEnd;

    /**
     * 分配门店列表
     */
    private List<String> stoList;

    /**
     * 促销列表
     */
    private List<SaveGsmTaskVO.Prom> promList;

    @Data
    @EqualsAndHashCode
    public static class Prom {
        private Integer id;

        private String client;

        private Integer marketingId;

        private String componentId;

        private String name;

        private String type;

        private String gspvsMode;

        private String gspvsModeName;

        private String gspvsType;

        private String gspvsTypeName;

        private String remarks1;

        private String remarks2;
        /**
         * 具体参数
         */
        private List<SaveGsmTaskVO.Param> paramList;
        /**
         * 选品数量
         */
        private Integer countPrd;
        /**
         * 赠品数量
         */
        private Integer countGiveaway;
        /**
         * 商品列表
         */
        private List<SdMarketingPrdDTO> prdList;
        /**
         * 赠品列表
         */
        private List<SdMarketingGiveawayDTO> giveawayList;
    }

    @Data
    @EqualsAndHashCode
    public static class Param extends SdPromVariableSet {
        /**
         * 列的值
         */
        private String gspvsColumnsValue;
    }

    @Data
    @EqualsAndHashCode
    public static class SdMarketingPrdDTO extends SdMarketingPrd {
        /**
         * 商品编码
         */
        private String proSelfCode;
    }

    @Data
    @EqualsAndHashCode
    public static class SdMarketingGiveawayDTO extends SdMarketingGiveaway {
        /**
         * 商品编码
         */
        private String proSelfCode;
    }

}
