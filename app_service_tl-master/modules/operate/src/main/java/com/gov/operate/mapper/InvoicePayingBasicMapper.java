package com.gov.operate.mapper;

import com.gov.operate.dto.InvoicePayingBasicVO;
import com.gov.operate.entity.InvoicePayingBasic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
public interface InvoicePayingBasicMapper extends BaseMapper<InvoicePayingBasic> {

    /**
     * 纳税主体
     *
     * @param client
     * @return
     */
    List<InvoicePayingBasicVO> selectPayGroup(@Param("client") String client);

    /**
     * 纳税明细
     *
     * @param client
     * @param ficoCompanyCode
     * @return
     */
    List<InvoicePayingBasicVO> selectPayGroupItem(@Param("client") String client, @Param("ficoCompanyCode") String ficoCompanyCode);

    /**
     * 同一连锁下的付款组织
     *
     * @param client
     * @param chainHead
     * @return
     */
    List<InvoicePayingBasicVO> getPayList(@Param("client") String client, @Param("chainHead") String chainHead, @Param("ficoCompanyCode") String ficoCompanyCode);

    /**
     * 初始化当前纳税主体下的门店
     *
     * @param client
     * @param ficoCompanyCode
     * @return
     */
    Integer initTax(@Param("client") String client, @Param("ficoCompanyCode") String ficoCompanyCode);

}
