package com.gov.operate.mapper;

import com.gov.operate.entity.FiInvoiceRegistration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
public interface FiInvoiceRegistrationMapper extends BaseMapper<FiInvoiceRegistration> {

}
