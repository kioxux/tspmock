package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.InvoicePayingBasicVO;
import com.gov.operate.dto.Message.MessageParams;
import com.gov.operate.dto.StorePaymentDto;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IMessageService;
import com.gov.operate.service.IStorePaymentService;
import com.gov.operate.tpns.PushContent;
import com.gov.operate.tpns.TpnsUtils;
import com.tencent.xinge.XingeApp;
import com.tencent.xinge.bean.Environment;
import com.tencent.xinge.bean.Platform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.28
 */
@Slf4j
@Service
public class StorePaymentServiceImpl implements IStorePaymentService {

    @Resource
    private CommonService commonService;
    @Resource
    private PayingItemMapper payingItemMapper;
    @Resource
    private InvoicePayingBasicMapper invoicePayingBasicMapper;
    @Resource
    private FranchiseeMapper franchiseeMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private AppLoginLogMapper appLoginLogMapper;
    @Resource
    private CompadmMapper compadmMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private IMessageService messageService;

    /**
     * 付款列表
     *
     * @param type
     * @param ficoPayingstoreCode
     * @param ficoPayingstoreName
     * @param validDate
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result storePaymentList(String type, String ficoPayingstoreCode, String ficoPayingstoreName, Integer validDate) {
        TokenUser user = commonService.getLoginInfo();
        payingItemMapper.initInvoice(user.getClient());
        payingItemMapper.syncInvoice(user.getClient());
        List<StorePaymentDto> list = payingItemMapper.selectPaymentList(user.getClient(), type, ficoPayingstoreCode, ficoPayingstoreName, validDate);
        return ResultUtil.success(list);
    }

    /**
     * 门店付款提醒
     */
    @Override
    public void payReminder() {
        log.info("门店付款推送通知开始。");
        // 存在需要通知的加盟商
        List<StorePaymentDto> list = payingItemMapper.selectPayReminder();
        if (CollectionUtils.isEmpty(list)) {
            log.info("门店付款推送通知结束。没有需要发送的加盟商");
            return;
        }
        for (StorePaymentDto storePaymentDto : list) {
            String client = storePaymentDto.getClient();
            QueryWrapper<Franchisee> franchiseeQueryWrapper = new QueryWrapper<>();
            franchiseeQueryWrapper.eq("CLIENT", client);
            Franchisee franchisee = franchiseeMapper.selectOne(franchiseeQueryWrapper);
            // 加盟商未查询到
            if (franchisee == null) {
                continue;
            }
            String userId = "";
            // 委托人账号
            if (StringUtils.isNotBlank(franchisee.getFrancAss())) {
                userId = franchisee.getFrancAss();
                QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
                userDataQueryWrapper.eq("CLIENT", client);
                userDataQueryWrapper.eq("USER_ID", franchisee.getFrancAss());
                UserData user = userDataMapper.selectOne(userDataQueryWrapper);
                if (user != null) {
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(client);
                    messageParams.setUserId(user.getUserId());
                    messageParams.setId(CommonEnum.gmtId.MSG00001.getCode());
                    messageService.sendMessage(messageParams);
                }
            }
            // 法人
            if (StringUtils.isNotBlank(franchisee.getFrancLegalPerson()) && !userId.equals(franchisee.getFrancLegalPerson())) {
                QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
                userDataQueryWrapper.eq("CLIENT", client);
                userDataQueryWrapper.eq("USER_ID", franchisee.getFrancLegalPerson());
                UserData user = userDataMapper.selectOne(userDataQueryWrapper);
                if (user != null) {
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(client);
                    messageParams.setUserId(user.getUserId());
                    messageParams.setId(CommonEnum.gmtId.MSG00001.getCode());
                    messageService.sendMessage(messageParams);
                }
            }
        }
    }

    /**
     * 合并纳税列表
     *
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getPayGroup() {
        TokenUser user = commonService.getLoginInfo();
        // 付款主体初始化
        payingItemMapper.initInvoice(user.getClient());
        payingItemMapper.syncInvoice(user.getClient());
        List<InvoicePayingBasicVO> list = invoicePayingBasicMapper.selectPayGroup(user.getClient());
        return ResultUtil.success(list);
    }

    /**
     * 纳税主体明细
     *
     * @param ficoCompanyCode
     * @return
     */
    @Override
    public Result getPayGroupItem(String ficoCompanyCode) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<InvoicePayingBasic> invoicePayingBasicQueryWrapper = new QueryWrapper<>();
        invoicePayingBasicQueryWrapper.eq("CLIENT", user.getClient());
        invoicePayingBasicQueryWrapper.eq("FICO_COMPANY_CODE", ficoCompanyCode);
        InvoicePayingBasic invoicePayingBasic = invoicePayingBasicMapper.selectOne(invoicePayingBasicQueryWrapper);
        if (invoicePayingBasic == null) {
            return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
        }
        InvoicePayingBasicVO invoicePayingBasicVO = new InvoicePayingBasicVO();
        BeanUtils.copyProperties(invoicePayingBasic, invoicePayingBasicVO);
        invoicePayingBasicVO.setFicoCompanyName(invoicePayingBasic.getFicoTaxSubjectName());
        // 纳税明细
        List<InvoicePayingBasicVO> list = invoicePayingBasicMapper.selectPayGroupItem(user.getClient(), ficoCompanyCode);
        invoicePayingBasicVO.setInvoicePayingBasicVOList(list);
        return ResultUtil.success(invoicePayingBasicVO);
    }

    /**
     * 同一连锁下的付款组织
     *
     * @param ficoCompanyCode
     * @return
     */
    @Override
    public Result getPayList(String ficoCompanyCode) {
        TokenUser user = commonService.getLoginInfo();
        // 纳税主体为连锁
        QueryWrapper<Compadm> compadmQueryWrapper = new QueryWrapper<Compadm>();
        compadmQueryWrapper.eq("CLIENT", user.getClient());
        compadmQueryWrapper.eq("COMPADM_ID", ficoCompanyCode);
        Compadm compadm = compadmMapper.selectOne(compadmQueryWrapper);
        if (compadm != null) {
            List<InvoicePayingBasicVO> list = invoicePayingBasicMapper.getPayList(user.getClient(), ficoCompanyCode, null);
            return ResultUtil.success(list);
        }
        // 纳税主体为门店
        QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<>();
        storeDataQueryWrapper.eq("CLIENT", user.getClient());
        storeDataQueryWrapper.eq("STO_CODE", ficoCompanyCode);
        StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
        if (storeData != null) {
            List<InvoicePayingBasicVO> list = invoicePayingBasicMapper.getPayList(user.getClient(), storeData.getStoChainHead(), ficoCompanyCode);
            return ResultUtil.success(list);
        }
        return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
    }

    /**
     * 纳税合并保存
     *
     * @param invoicePayingBasicVO
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result savePayItem(InvoicePayingBasicVO invoicePayingBasicVO) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<InvoicePayingBasic> invoicePayingBasicQueryWrapper = new QueryWrapper<>();
        invoicePayingBasicQueryWrapper.eq("CLIENT", user.getClient());
        invoicePayingBasicQueryWrapper.eq("FICO_COMPANY_CODE", invoicePayingBasicVO.getFicoCompanyCode());
        InvoicePayingBasic invoicePayingBasic = invoicePayingBasicMapper.selectOne(invoicePayingBasicQueryWrapper);
        if (invoicePayingBasic == null) {
            return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
        }
        // 纳税主体信息修改
        QueryWrapper<InvoicePayingBasic> invoicePayingUpdateBasicQueryWrapper = new QueryWrapper<>();
        invoicePayingUpdateBasicQueryWrapper.eq("CLIENT", user.getClient());
        invoicePayingUpdateBasicQueryWrapper.eq("FICO_COMPANY_CODE", invoicePayingBasicVO.getFicoCompanyCode());
        invoicePayingBasicMapper.update(invoicePayingBasicVO, invoicePayingUpdateBasicQueryWrapper);
        // 纳税主体合并明细初始化
        invoicePayingBasicMapper.initTax(user.getClient(), invoicePayingBasicVO.getFicoCompanyCode());
        if (!CollectionUtils.isEmpty(invoicePayingBasicVO.getInvoicePayingBasicVOList())) {
            for (InvoicePayingBasicVO entity : invoicePayingBasicVO.getInvoicePayingBasicVOList()) {
                QueryWrapper<InvoicePayingBasic> invoicePayingUpdateItemBasicQueryWrapper = new QueryWrapper<>();
                invoicePayingUpdateItemBasicQueryWrapper.eq("CLIENT", user.getClient());
                invoicePayingUpdateItemBasicQueryWrapper.eq("FICO_COMPANY_CODE", entity.getFicoCompanyCode());
                InvoicePayingBasic updateItemInvoicePayingBasic = new InvoicePayingBasic();
                updateItemInvoicePayingBasic.setFicoTaxSubjectCode(invoicePayingBasicVO.getFicoCompanyCode());
                updateItemInvoicePayingBasic.setFicoTaxSubjectName(invoicePayingBasicVO.getFicoCompanyName());
                invoicePayingBasicMapper.update(updateItemInvoicePayingBasic, invoicePayingUpdateItemBasicQueryWrapper);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 推送
     *
     * @param client
     * @param userId
     */
    private void push(String client, String userId, String phone) {
        // 用户登录数据
        AppLoginLog appLoginLog = appLoginLogMapper.selectLastLogByUser(client, userId);
        if (appLoginLog == null) {
            return;
        }
        XingeApp xingeApp = null;
        PushContent pushContent = new PushContent();
        pushContent.setTitle("付款提醒");
        pushContent.setContent("您有门店费用即将到期，请及时完成付费，以便您继续享有相关服务。");
        List<String> accountList = new ArrayList<>();
        // 安卓登录
        if ("android".equals(appLoginLog.getPlatform())) {
            xingeApp = TpnsUtils.xingeApp(applicationConfig.getTpnsAndroidAppId(), applicationConfig.getTpnsAndroidSecretKey(), applicationConfig.getTpnsDoMain());
            pushContent.setPlatform(Platform.android);
            accountList.add(client + "-" + phone);
        }
        // 苹果登录
        if ("ios".equals(appLoginLog.getPlatform())) {
            xingeApp = TpnsUtils.xingeApp(applicationConfig.getTpnsIosAppId(), applicationConfig.getTpnsIosSecretKey(), applicationConfig.getTpnsDoMain());
            pushContent.setPlatform(Platform.ios);
            if (applicationConfig.getEnvironment() == 1) {
                pushContent.setEnvironment(Environment.dev);
            }
            if (applicationConfig.getEnvironment() == 0) {
                pushContent.setEnvironment(Environment.product);
            }
            accountList.add(client + "-" + phone);
        }
        if (xingeApp == null) {
            return;
        }
        // 自定义参数
        Map<String, Object> map = new HashMap<>();
        map.put(PushContent.TYPE, PushContent.Type.PAYREMINDER.getCode());
        pushContent.setAttachKeys(map);
        pushContent.setAccountList(accountList);
        TpnsUtils.send(pushContent, xingeApp);
    }
}

