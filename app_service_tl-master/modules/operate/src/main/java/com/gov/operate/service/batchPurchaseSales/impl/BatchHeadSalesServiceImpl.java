package com.gov.operate.service.batchPurchaseSales.impl;

import com.gov.operate.entity.BatchHeadSales;
import com.gov.operate.mapper.BatchHeadSalesMapper;
import com.gov.operate.service.batchPurchaseSales.IBatchHeadSalesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 总部销售 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Service
public class BatchHeadSalesServiceImpl extends ServiceImpl<BatchHeadSalesMapper, BatchHeadSales> implements IBatchHeadSalesService {

}
