package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DecimalUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.entity.SalaryMonthReach;
import com.gov.operate.mapper.SalaryMonthReachMapper;
import com.gov.operate.service.ISalaryMonthReachService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
@Service
public class SalaryMonthReachServiceImpl extends ServiceImpl<SalaryMonthReachMapper, SalaryMonthReach> implements ISalaryMonthReachService {

    /**
     * 设定月综合达成率/达成系数
     */
    @Override
    public void saveReachList(EditProgramVO vo) {
        Integer gspId = vo.getGspId();
        // 删除该方案综合达成率 原始数据
        this.remove(new QueryWrapper<SalaryMonthReach>().eq("GSMR_GSP_ID", gspId));
        List<SalaryMonthReach> salaryMonthReacheList = vo.getSalaryMonthReacheList();
        if (CollectionUtils.isEmpty(salaryMonthReacheList)) {
            throw new CustomResultException("薪资方案月综合达成系数列表 不能为空");
        }
        for (int i = 0; i < salaryMonthReacheList.size(); i++) {
            SalaryMonthReach salaryMonthReach = salaryMonthReacheList.get(i);
            if (ObjectUtils.isEmpty(salaryMonthReach.getGsmrAchievRateMax())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条达成率最大值不能为空！", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryMonthReach.getGsmrAchievRateMin())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条达成率最小值不能为空！", i + 1));
            }
            if (ObjectUtils.isEmpty(salaryMonthReach.getGsmrAchievCoeffi())) {
                throw new CustomResultException(MessageFormat.format("第[{0}]条达成系数不能为空！", i + 1));
            }
            // 方案主键
            salaryMonthReach.setGsmrGspId(gspId);
            // 排序
            salaryMonthReach.setGsmrSort(i + 1);
            // 达成率最小值
            salaryMonthReach.setGsmrAchievRateMin(DecimalUtils.toDecimal(salaryMonthReach.getGsmrAchievRateMin()));
            // 达成率最大值
            salaryMonthReach.setGsmrAchievRateMax(DecimalUtils.toDecimal(salaryMonthReach.getGsmrAchievRateMax()));
        }
        // 校验范围重叠的问题
        salaryMonthReacheList.sort(Comparator.comparing(SalaryMonthReach::getGsmrAchievRateMin));
        for (int i = 0; i < salaryMonthReacheList.size(); i++) {
            if (i == 0) {
                continue;
            }
            SalaryMonthReach current = salaryMonthReacheList.get(i);
            SalaryMonthReach previous = salaryMonthReacheList.get(i - 1);
            if (current.getGsmrAchievRateMin().compareTo(previous.getGsmrAchievRateMax()) <= 0) {
                throw new CustomResultException(MessageFormat.format("综合达成率区间[{0}~{1}]与[{2}~{3}]存在重叠区,请重新输入",
                        DecimalUtils.toPercentStr(previous.getGsmrAchievRateMin()), DecimalUtils.toPercentStr(previous.getGsmrAchievRateMax()),
                        DecimalUtils.toPercentStr(current.getGsmrAchievRateMin()), DecimalUtils.toPercentStr(current.getGsmrAchievRateMax())));
            }
        }
        this.saveBatch(salaryMonthReacheList);
    }

    /**
     * 过滤当前员工综合达成系数区间
     *
     * @param salaryMonthReachList
     * @param rate
     * @return
     */
    public BigDecimal achievementRate(List<SalaryMonthReach> salaryMonthReachList, BigDecimal rate) {
        if (CollectionUtils.isEmpty(salaryMonthReachList)) {
            throw new CustomResultException("综合达成系数未设置！");
        }
        List<SalaryMonthReach> list = salaryMonthReachList.stream().filter(s -> s.getGsmrAchievRateMin().compareTo(rate) <= 0 && s.getGsmrAchievRateMax().compareTo(rate) >= 0).collect(Collectors.toList());
        // 未过滤到当前系数区间
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("综合达成系数设置不完整！");
        }
        // 该系数存在多个区间
        if (list.size() > 1) {
            throw new CustomResultException("综合达成系数设置重复！");
        }
        // 返回当前用户达成系数
        return list.get(0).getGsmrAchievCoeffi();
    }

    /**
     * 获取达成率/达成系数
     */
    @Override
    public List<SalaryMonthReach> getReachList(Integer gspId) {
        List<SalaryMonthReach> list = this.list(new QueryWrapper<SalaryMonthReach>()
                .eq("GSMR_GSP_ID", gspId)
                .orderByAsc("GSMR_SORT"));
        list.forEach(salaryMonthReach -> {
            salaryMonthReach.setGsmrAchievRateMin(salaryMonthReach.getGsmrAchievRateMin().multiply(Constants.ONE_HUNDRED));
            salaryMonthReach.setGsmrAchievRateMax(salaryMonthReach.getGsmrAchievRateMax().multiply(Constants.ONE_HUNDRED));
        });
        return list;
    }

    /**
     * 获取达成率(小数)/达成系数
     */
    @Override
    public List<SalaryMonthReach> getReachDecimalList(Integer gspId) {
        List<SalaryMonthReach> list = this.list(new QueryWrapper<SalaryMonthReach>()
                .eq("GSMR_GSP_ID", gspId)
                .orderByAsc("GSMR_SORT"));
        return list;
    }
}
