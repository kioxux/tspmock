package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_MATERIAL_DOC")
@ApiModel(value="MaterialDoc对象", description="")
public class MaterialDoc extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "物料凭证号")
    @TableField("MAT_ID")
    private String matId;

    @ApiModelProperty(value = "物料凭证年份")
    @TableField("MAT_YEAR")
    private String matYear;

    @ApiModelProperty(value = "物料凭证行号")
    @TableField("MAT_LINE_NO")
    private String matLineNo;

    @ApiModelProperty(value = "物料凭证类型")
    @TableField("MAT_TYPE")
    private String matType;

    @ApiModelProperty(value = "凭证日期")
    @TableField("MAT_DOC_DATE")
    private String matDocDate;

    @ApiModelProperty(value = "过账日期")
    @TableField("MAT_POST_DATE")
    private String matPostDate;

    @ApiModelProperty(value = "抬头备注")
    @TableField("MAT_HEAD_REMARK")
    private String matHeadRemark;

    @ApiModelProperty(value = "商品编码")
    @TableField("MAT_PRO_CODE")
    private String matProCode;

    @ApiModelProperty(value = "地点")
    @TableField("MAT_SITE_CODE")
    private String matSiteCode;

    @ApiModelProperty(value = "库存地点")
    @TableField("MAT_LOCATION_CODE")
    private String matLocationCode;

    @ApiModelProperty(value = "批次")
    @TableField("MAT_BATCH")
    private String matBatch;

    @ApiModelProperty(value = "物料凭证数量")
    @TableField("MAT_QTY")
    private BigDecimal matQty;

    @ApiModelProperty(value = "总金额（批次）")
    @TableField("MAT_BAT_AMT")
    private BigDecimal matBatAmt;

    @ApiModelProperty(value = "基本计量单位")
    @TableField("MAT_UINT")
    private String matUint;

    @ApiModelProperty(value = "总金额（移动）")
    @TableField("MAT_MOV_AMT")
    private BigDecimal matMovAmt;

    @ApiModelProperty(value = "税金（批次）")
    @TableField("MAT_RATE_BAT")
    private BigDecimal matRateBat;

    @ApiModelProperty(value = "税金（移动）")
    @TableField("MAT_RATE_MOV")
    private BigDecimal matRateMov;

    @ApiModelProperty(value = "借/贷标识")
    @TableField("MAT_DEBIT_CREDIT")
    private String matDebitCredit;

    @ApiModelProperty(value = "订单号")
    @TableField("MAT_PO_ID")
    private String matPoId;

    @ApiModelProperty(value = "订单行号")
    @TableField("MAT_PO_LINENO")
    private String matPoLineno;

    @ApiModelProperty(value = "业务单号")
    @TableField("MAT_DN_ID")
    private String matDnId;

    @ApiModelProperty(value = "业务单行号")
    @TableField("MAT_DN_LINENO")
    private String matDnLineno;

    @ApiModelProperty(value = "物料凭证行备注")
    @TableField("MAT_LINE_REMARK")
    private String matLineRemark;

    @ApiModelProperty(value = "创建人")
    @TableField("MAT_CREATE_BY")
    private String matCreateBy;

    @ApiModelProperty(value = "创建日期")
    @TableField("MAT_CREATE_DATE")
    private String matCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("MAT_CREATE_TIME")
    private String matCreateTime;

    @ApiModelProperty(value = "加点后金额")
    @TableField("MAT_ADD_AMT")
    private BigDecimal matAddAmt;

    @ApiModelProperty(value = "加点后税金")
    @TableField("MAT_ADD_TAX")
    private BigDecimal matAddTax;
}
