package com.gov.operate.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gov.operate.entity.SdMemberBasic;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetMemberListDTO extends SdMemberBasic {

    /**
     * id值 (前端使用)
     */
    private String id;


    /**
     * 所属店号
     */
    private String gmbBrId;
    /**
     * 当前积分
     */
    private String gmbIntegral;
    /**
     * 生日
     */
    private String gmbBirth;
    /**
     * 年龄
     */
    private String gmbAge;
    /**
     * 性别
     */
    private String gmbSex;
    /**
     * 手机
     */
    private String gmbMobile;

    /**
     * 会员姓名
     */
    private String gmbName;
    /**
     * 会员卡号
     */
    private String gmbCardId;

    /**
     * 所属门店简称
     */
    private String gmbBrShortName;

    /**
     * 所属门店名称
     */
    private String gmbBrName;

    /**
     * 会员ID
     */
    private String gsmbMemberId;

    /**
     * 最后销售门店
     */
    private String lastSaleStore;

    /**
     * 最后消费日期
     */
    private String gsshDate;

    /**
     * 最后消费时间
     */
    private String gsshTime;

    /**
     * 最后积分日期
     */
    private String gsmbcIntegralLastdate;

    /**
     * 卡类型
     */
    private String gsmbcClassId;

    /**
     * 是否发送短信 0:没有发送短信， 1:已经发送短信
     */
    private Integer hasSendSms;

    /**
     * 新卡创建日期
     */
    private String gsmbcCreateDate;

    /**
     * 门店属性
     */
    private String stoAttribute;
    /**
     * 开卡门店
     */
    private String gsmbcOpenCard;
    /**
     * 开卡门店名称
     */
    private String gsmbcOpenCardName;
    /**
     * 开卡员工
     */
    private String gsmbcCreateSaler;
    /**
     * 最后交易员工
     */
    private String lastSaleEmp;

    /**
     *  企微绑定状态(0 -未绑 ， 1- 已绑)
     */
    private String bindStatus;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastSendTime;
}
