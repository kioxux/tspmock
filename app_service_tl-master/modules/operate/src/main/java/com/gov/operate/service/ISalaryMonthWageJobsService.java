package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.dto.EditProgramVO;
import com.gov.operate.dto.SalaryProgramDTO;
import com.gov.operate.entity.SalaryMonthWageJobs;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryMonthWageJobsService extends SuperService<SalaryMonthWageJobs> {

    /**
     * 岗位工资等级
     */
    void saveWageJobs(EditProgramVO vo);

    /**
     * 岗位薪资列表
     */
    List<SalaryProgramDTO.SalaryMonthWageJobsOuter> getWageJobsList(Integer gspId);


    /**
     * 岗位薪资未分组列表
     */
    List<SalaryMonthWageJobs> getWageJobsNoGroupingList(Integer gspId);
}
