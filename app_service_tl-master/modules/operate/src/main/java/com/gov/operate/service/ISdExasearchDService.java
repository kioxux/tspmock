package com.gov.operate.service;

import com.gov.operate.entity.SdExasearchD;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface ISdExasearchDService extends SuperService<SdExasearchD> {

}
