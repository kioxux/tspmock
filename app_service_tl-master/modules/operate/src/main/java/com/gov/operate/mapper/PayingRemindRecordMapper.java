package com.gov.operate.mapper;

import com.gov.operate.entity.PayingRemindRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 加盟商余额变动记录 Mapper 接口
 * </p>
 *
 * @author tzh
 * @since 2021-08-02
 */
public interface PayingRemindRecordMapper extends BaseMapper<PayingRemindRecord> {

}
