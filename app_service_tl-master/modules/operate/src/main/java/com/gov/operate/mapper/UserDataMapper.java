package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.UserDataDTO;
import com.gov.operate.entity.UserData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-02
 */
@Mapper
public interface UserDataMapper extends BaseMapper<UserData> {
    int selectMaxId(String client);

    UserDataDTO hasStoreUser(@Param("client") String client, @Param("userId") String userId);

    List<UserData> getCleanDrawerPersonnel(@Param("client") String client, @Param("depId") String depId,
                                           @Param("userId") String userId);

    List<UserData> getCurrentUser(@Param("client") String client, @Param("userId") String userId);

    List<UserData> getCleanDrawerPersonnel(@Param("client") String client, @Param("depId") String depId);

    int updateUserElectronicSignatureData(@Param("signatureUrl") String signatureUrl, @Param("client") String client, @Param("userId") String userId);
}
