package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_WECHAT_TWEETS_D")
@ApiModel(value="WechatTweetsD对象", description="")
public class WechatTweetsD extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "推文主表ID")
    @TableField("GWTH_ID")
    private Long gwthId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "客户")
    @TableField("GWTD_CUSTOMER")
    private String gwtdCustomer;

    @ApiModelProperty(value = "标题")
    @TableField("GWTD_TITLE")
    private String gwtdTitle;

    @ApiModelProperty(value = "图文消息的封面图片素材id")
    @TableField("GWTD_THUMB_MEDIA_ID")
    private String gwtdThumbMediaId;

    @ApiModelProperty(value = "是否显示封面")
    @TableField("GWTD_SHOW_COVER_PIC")
    private Integer gwtdShowCoverPic;

    @ApiModelProperty(value = "作者")
    @TableField("GWTD_AUTHOR")
    private String gwtdAuthor;

    @ApiModelProperty(value = "图文消息的摘要")
    @TableField("GWTD_DIGEST")
    private String gwtdDigest;

    @ApiModelProperty(value = "图文消息的具体内容")
    @TableField("GWTD_CONTENT")
    private String gwtdContent;

    @ApiModelProperty(value = "图文页的URL")
    @TableField("GWTD_URL")
    private String gwtdUrl;

    @ApiModelProperty(value = "图文消息的原文地址")
    @TableField("GWTD_CONTETN_SOURCE_URL")
    private String gwtdContetnSourceUrl;

    @ApiModelProperty(value = "是否发布")
    @TableField("GWTD_RELEASE")
    private Integer gwtdRelease;

    @ApiModelProperty(value = "原因")
    @TableField("GWTD_REASON")
    private String gwtdReason;

    @ApiModelProperty(value = "封面")
    @TableField("GWTD_THUMB_URL")
    private String gwtdThumbUrl;

    @ApiModelProperty(value = "是否打开评论")
    @TableField("GWTD_NEED_OPEN_COMMENT")
    private Integer gwtdNeedOpenComment;

    @ApiModelProperty(value = "粉丝才可以评论")
    @TableField("GWTD_ONLY_FANS_CAN_COMMENT")
    private Integer gwtdOnlyFansCanComment;


}
