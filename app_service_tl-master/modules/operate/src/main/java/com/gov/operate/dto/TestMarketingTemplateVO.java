package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class TestMarketingTemplateVO {

    @NotBlank(message = "模版id不能为空")
    private String smsId;

    @NotBlank(message = "测试手机号不能为空")
    private String phone;

    @NotBlank(message = "测试短信内容")
    private String message;

}
