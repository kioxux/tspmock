package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALARY_PRO_CPS")
@ApiModel(value="SalaryProCps对象", description="")
public class SalaryProCps extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "GSPC_ID", type = IdType.AUTO)
    private Integer gspcId;

    @ApiModelProperty(value = "方案主键")
    @TableField("GSPC_GSP_ID")
    private Integer gspcGspId;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPC_PRO_CODE")
    private String gspcProCode;

    @ApiModelProperty(value = "提成金额")
    @TableField("GSPC_COMMISSION_AMT")
    private BigDecimal gspcCommissionAmt;

    @ApiModelProperty(value = "顺序")
    @TableField("GSPC_SORT")
    private Integer gspcSort;

    @ApiModelProperty(value = "品名")
    @TableField("PRO_NAME")
    private String proName;

    @ApiModelProperty(value = "规格")
    @TableField("PRO_SPECS")
    private String proSpecs;

    @ApiModelProperty(value = "生产厂家")
    @TableField("PRO_FACTORY_NAME")
    private String proFactoryName;


}
