package com.gov.operate.controller.popularize;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.charge.SaveClientPriceDTO;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.StoreData;
import com.gov.operate.entity.SystemServicePack;
import com.gov.operate.entity.UserData;
import com.gov.operate.request.RequestJson;
import com.gov.operate.service.IFranchiseeService;
import com.gov.operate.service.IStoreDataService;
import com.gov.operate.service.popularize.ISystemServicePackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Api(tags = "营销活动任务")
@RestController
@RequestMapping("popularize/systemServicePack")
public class SystemServicePackController {

    @Resource
    private ISystemServicePackService iSystemServicePackService;

    @Resource
    private IFranchiseeService iFranchiseeService;

    @Resource
    private IStoreDataService iStoreDataService;

    @Log("模块列表")
    @ApiOperation(value = "模块列表")
    @GetMapping("getList")
    public Result getList() {
        return iSystemServicePackService.getList();
    }

    /******************************************************收费项目维护*********************************************************/
    @Log("收费项目列表")
    @ApiOperation(value = "收费项目列表")
    @PostMapping("getOfficialPriceList")
    public Result getOfficialPriceList(@RequestJson(value = "spId", required = false) String spId,
                                       @RequestJson(value = "spSeries", required = false) String spSeries) {
        return iSystemServicePackService.getOfficialPriceList(spId, spSeries);
    }

    @Log("官网价维护")
    @ApiOperation(value = "官网价维护")
    @PostMapping("editOfficialPrice")
    public Result editOfficialPrice(@RequestBody List<SystemServicePack> list) {
        iSystemServicePackService.editOfficialPrice(list);
        return ResultUtil.success();
    }

    /******************************************************用户收费项目维护*********************************************************/

    @Log("用户收费项目维护")
    @ApiOperation(value = "用户收费项目维护")
    @PostMapping("getClienPriceItemByContract")
    public Result getClientPriceItemByContract(@RequestJson(value = "contractCode", name = "合同编号") String contractCode,
                                              @RequestJson(value = "clientId", name = "客户Id") String clientId) {
        Result result = iSystemServicePackService.getClientPriceItemByContract(contractCode,clientId);
        return result;
    }

    @Log("用户收费项目保存")
    @ApiOperation(value = "用户收费项目保存")
    @PostMapping("saveClientPriceItemByContract")
    public Result saveClientPriceItemByContract(@RequestBody SaveClientPriceDTO saveClientPriceDTO) {
        iSystemServicePackService.saveClientPriceItemByContract(saveClientPriceDTO);
        return ResultUtil.success();
    }

    @Log("用户选择列表")
    @ApiOperation(value = "用户选择列表")
    @PostMapping("selectFranchisee")
    public Result selectFranchisee() {
        return ResultUtil.success(iFranchiseeService.list(null));
    }

    @Log("门店选择列表")
    @ApiOperation(value = "门店选择列表")
    @PostMapping("selectStore")
    public Result selectStore(@RequestJson(value = "clientId", name = "客户ID") String clientId) {
        return ResultUtil.success(iStoreDataService.list(new QueryWrapper<StoreData>()
                .eq("CLIENT", clientId).eq("STO_STATUS",0)));
    }

    @Log("收费门店集合")
    @ApiOperation(value = "收费门店集合")
    @PostMapping("selectPayStore")
    public Result selectPayStore() {
        Result result = iSystemServicePackService.selectPayStore();
        return result;
    }

}
