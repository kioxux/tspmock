package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.RechargeCardLVO;
import com.gov.operate.dto.RechargeCardListVO;
import com.gov.operate.dto.recharge.RechargeRefundVO;
import com.gov.operate.entity.SdRechargeCardPaycheck;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface SdRechargeCardPaycheckMapper extends BaseMapper<SdRechargeCardPaycheck> {
    SdRechargeCardPaycheck getUserCardNumber(@Param("client") String client);

    IPage<RechargeCardLVO> SelectCardList(Page<RechargeCardLVO> page, @Param("ew") QueryWrapper<RechargeCardListVO> wrapper, @Param("client") String client);

    /**
     * 复合主键查询唯一值
     */
    SdRechargeCardPaycheck getPayCheckByCompositekey(@Param("vo") RechargeRefundVO vo, @Param("client") String client);
}
