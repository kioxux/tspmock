package com.gov.operate.dto;

import lombok.Data;

@Data
public class StoreVo {

    private String code;

    private String name;

    private String stogCode;

}
