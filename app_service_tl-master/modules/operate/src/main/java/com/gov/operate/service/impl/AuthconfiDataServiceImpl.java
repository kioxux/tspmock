package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.StringUtils;
import com.gov.operate.entity.AuthconfiData;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.GroupData;
import com.gov.operate.mapper.AuthconfiDataMapper;
import com.gov.operate.mapper.GroupDataMapper;
import com.gov.operate.service.IAuthconfiDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * 权限分配表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
@Service
public class AuthconfiDataServiceImpl extends ServiceImpl<AuthconfiDataMapper, AuthconfiData> implements IAuthconfiDataService {
    @Autowired
    private AuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private GroupDataMapper groupDataMapper;

    public static final List<String> GAD_GROUP_LIST = Arrays.asList("GAIA_AL_ADMIN", "GAIA_AL_MANAGER");

    /**
     *
     * @param client 加盟商
     * @param userId  账号id
     * @param groupList 权限集合
     * @param siteList   部门集合
     * @param delUserId  要删除的权限的账户id
     */
    @Transactional
    public void saveAuth(String client, String userId, List<String> groupList, List<String> siteList, String delUserId) {

        if (!StringUtils.isEmpty(client) && !StringUtils.isEmpty(userId) && !ObjectUtils.isEmpty(groupList)) {
            List<AuthconfiData> saveData = new ArrayList<>();
            groupList.forEach(item->{
                List<GroupData> groupDataList = this.groupDataMapper.selectList(new QueryWrapper<GroupData>()
                        .eq("AUTH_GROUP_ID",item));
                //要新增的权限
                QueryWrapper selQW=new QueryWrapper<AuthconfiData>()
                        .eq("CLIENT",client)
                        .eq("AUTH_GROUP_ID",item)
                        .eq("AUTHCONFI_USER",userId);
                //要删除的权限
                QueryWrapper delQW=new QueryWrapper<AuthconfiData>()
                        .eq("CLIENT",client)
                        .eq("AUTH_GROUP_ID",item)
                        .eq("AUTHCONFI_USER",delUserId);
                //获取权限名
                String groupName = groupDataList.get(0).getAuthGroupName();
                //权限是“GAIA_AL_ADMIN", "GAIA_AL_MANAGER" 单独操作
                if (GAD_GROUP_LIST.contains(item)) {
                    if (StringUtils.isNotBlank(delUserId)) {
                        this.authconfiDataMapper.delete(delQW);
                    }
                    AuthconfiData authconfiData = new AuthconfiData();
                    authconfiData.setAuthconfiUser(userId);
                    authconfiData.setAuthGroupId(item);
                    authconfiData.setAuthobjSite("GAD");
                    authconfiData.setClient(client);
                    selQW.eq("AUTHOBJ_SITE","GAD");
                    AuthconfiData record = this.authconfiDataMapper.selectOne(selQW);
                    if (!ObjectUtils.isNotEmpty(record)) {
                        authconfiData.setAuthGroupName(groupName);
//                        this.authconfiDataMapper.insert(authconfiData);
                        saveData.add(authconfiData);
                    }
                }else if (!ObjectUtils.isEmpty(siteList)) {
                    //给这个加盟商下的所有部门 门店都加上权限
                    Iterator var11 = siteList.iterator();
                    while(var11.hasNext()) {
                        String site = (String)var11.next();
                        if (StringUtils.isNotBlank(delUserId)) {
                            delQW.eq("AUTHOBJ_SITE",site);
                            this.authconfiDataMapper.delete(delQW);
                        }
                        AuthconfiData authconfiData = new AuthconfiData();
                        authconfiData.setAuthconfiUser(userId);
                        authconfiData.setAuthGroupId(item);
                        authconfiData.setAuthobjSite(site);
                        authconfiData.setClient(client);
                        selQW  .eq("AUTHOBJ_SITE",site);
                        AuthconfiData record = this.authconfiDataMapper.selectOne(selQW);
                        if (!ObjectUtils.isNotEmpty(record)) {
                            authconfiData.setAuthGroupName(groupName);
//                            this.authconfiDataMapper.insert(authconfiData);
                            saveData.add(authconfiData);

                        }
                    }
                }
            });
            if(!ObjectUtils.isEmpty(saveData)){
                this.authconfiDataMapper.insertIgnoreList(saveData);
            }
        }
    }
}
