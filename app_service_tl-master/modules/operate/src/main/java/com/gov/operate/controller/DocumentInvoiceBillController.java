package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@RestController
@RequestMapping("/operate/document-invoice-bill")
public class DocumentInvoiceBillController {

}

