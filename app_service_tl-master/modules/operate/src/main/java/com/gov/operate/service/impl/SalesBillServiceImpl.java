package com.gov.operate.service.impl;

import com.gov.operate.entity.SalesBill;
import com.gov.operate.mapper.SalesBillMapper;
import com.gov.operate.service.ISalesBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-02-08
 */
@Service
public class SalesBillServiceImpl extends ServiceImpl<SalesBillMapper, SalesBill> implements ISalesBillService {

}
