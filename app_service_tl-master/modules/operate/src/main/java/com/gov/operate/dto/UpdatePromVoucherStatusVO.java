package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class UpdatePromVoucherStatusVO {

    private Integer gsphMarketid;

    private String gsphStatus;

    private String gsphVoucherId;
}
