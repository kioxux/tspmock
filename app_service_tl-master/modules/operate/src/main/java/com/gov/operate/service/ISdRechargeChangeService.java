package com.gov.operate.service;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.operate.dto.recharge.RechargeChangeInData;
import com.gov.operate.dto.recharge.RechargeChangeOutData;
import com.gov.operate.entity.SdRechargeCardPaycheck;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ISdRechargeChangeService {
    /**
     * 储值卡充值消费明细分页查询
     * @param inData
     * @return
     */
    IPage<RechargeChangeOutData> rechargeChangePage(RechargeChangeInData inData);

    /**
     * 导出消费明细查询
     * @param inData
     * @param response
     * @throws IOException
     */
    void export(RechargeChangeInData inData,HttpServletResponse response) throws IOException;

//    /**
//     * 插入充值记录
//     * @param paycheck：充值信息
//     * @return
//     */
//    Boolean insertRecharge(SdRechargeCardPaycheck paycheck);
//
//    /**
//     * 插入消费记录
//     * @param payInData
//     * @param card
//     * @return
//     */
//    Boolean insertConsumption(GetPayInData payInData, GaiaSdRechargeCard card);
}
