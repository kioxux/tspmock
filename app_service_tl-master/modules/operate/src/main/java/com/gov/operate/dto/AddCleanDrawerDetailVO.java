package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/19 16:52
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AddCleanDrawerDetailVO {
    /**
     * 批号
     */
    @NotBlank(message = "批号不能为空")
    private String gscdBatchNo;

    /**
     * 有效期
     */
    @NotBlank(message = "有效期不能为空")
    private String gscdValidDate;

    /**
     * 商品编号
     */
    @NotBlank(message = "商品编号不能为空")
    private String gscdProId;

    /**
     * 清斗数量
     */
    @NotBlank(message = "清斗数量不能为空")
    private String gscdQty;

}
