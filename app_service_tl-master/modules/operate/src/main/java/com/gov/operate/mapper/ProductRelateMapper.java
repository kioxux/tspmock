package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.productRelate.DropDown;
import com.gov.operate.dto.productRelate.ProductRelateDto;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.entity.ProductRelate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
public interface ProductRelateMapper extends BaseMapper<ProductRelate> {

    /**
     * 根据权限带出，连锁带出仓库地点，单体店带出门店地点。
     *
     * @param client
     * @param userId
     * @return
     */
    List<DropDown> getSiteList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 供应商下拉框
     *
     * @param client
     * @param site
     * @return
     */
    List<DropDown> getSupList(@Param("client") String client, @Param("supSite") String site);

    /**
     * 商品集合
     *
     * @param client
     * @param site
     * @param key
     * @param proSelfCode
     * @return
     */
    List<ProductBusiness> getProList(@Param("client") String client, @Param("proSite") String site, @Param("key") String key, @Param("proSelfCode") String proSelfCode);

    /**
     * 分页查询煎药
     *
     * @param page        分页
     * @param client      加盟商
     * @param proSite        地点
     * @param supCode     供应商
     * @param proSelfCode 商品编码
     * @return
     */
    IPage<ProductRelateDto> selectProductRelateList(Page<ProductRelateDto> page,
                                                    @Param("client") String client,
                                                    @Param("proSite") String proSite,
                                                    @Param("supCode") String supCode,
                                                    @Param("proSelfCode") String proSelfCode);
}
