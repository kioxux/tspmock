package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangp
 * @since 2021-03-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BILL_PAYMENT_APPLY_ITEM")
@ApiModel(value = "PaymentApplyItem对象", description = "")
public class PaymentApplyItem extends BaseEntity {

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GBPAI_NUM")
    private String gbpaiNum;

    @ApiModelProperty(value = "供应商")
    @TableField("GBPAI_SUP_CODE")
    private String gbpaiSupCode;

    @ApiModelProperty(value = "创建人")
    @TableField("GBPAI_CREATE_USER")
    private String gbpaiCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBPAI_CREATE_DATE")
    private String gbpaiCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBPAI_CREATE_TIME")
    private String gbpaiCreateTime;

    @ApiModelProperty(value = "对账金额")
    @TableField("GBPAI_BILL_AMOUNT")
    private BigDecimal gbpaiBillAmount;

    @ApiModelProperty(value = "付款单号")
    @TableField("GBPAI_FLOW_NO")
    private String gbpaiFlowNo;

    @ApiModelProperty(value = "本次付款金额")
    @TableField("GBPAI_PAY_AMT")
    private BigDecimal gbpaiPayAmt;

    @ApiModelProperty(value = "备注")
    @TableField("GBPAI_REMARKS")
    private String gbpaiRemarks;

    @ApiModelProperty(value = "类型")
    @TableField("GBPAI_TYPE")
    private String gbpaiType;

    @ApiModelProperty(value = "银行名称")
    @TableField("GBPAI_BANK_NAME")
    private String gbpaiBankName;

    @ApiModelProperty(value = "银行账号")
    @TableField("GBPAI_BANK_ACCOUNT")
    private String gbpaiBankAccount;
}
