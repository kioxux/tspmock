package com.gov.operate.entity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/11/417:13
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PushMsgUserWithRule extends MessageTemplate {

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("接受人")
    private String receiveUserId;

    @ApiModelProperty("接收人手机号")
    private String receiveUserTel;

    private String titleParams;

    private String contentParams;

    private String params;



}
