package com.gov.operate.dto.weChat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class GetCostomerDropDownList {

    private String goaCode;

    private String goaName;
}
