//package com.gov.operate.config;
//
//import com.gov.operate.request.RequestJsonHandlerMethodArgumentResolver;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.method.support.HandlerMethodArgumentResolver;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import java.util.List;
//
///**
// *
// */
//@Configuration
//public class WebAppConfigurer implements WebMvcConfigurer {
//
//    @Override
//    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
//        argumentResolvers.add(new RequestJsonHandlerMethodArgumentResolver());
//    }
//}
