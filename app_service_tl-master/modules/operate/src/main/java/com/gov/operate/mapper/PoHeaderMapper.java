package com.gov.operate.mapper;

import com.gov.operate.entity.PoHeader;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 采购订单主表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-12-08
 */
public interface PoHeaderMapper extends BaseMapper<PoHeader> {

}
