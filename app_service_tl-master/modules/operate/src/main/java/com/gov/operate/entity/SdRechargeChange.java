package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("GAIA_SD_RECHARGE_CHANGE")
@ApiModel(value="储值卡金额异动表", description="")
public class SdRechargeChange extends BaseEntity {

    /**
     * 加盟商
     */
    @TableId
    @TableField( value = "CLIENT")
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 储值卡卡号
     */
    @TableField(value = "GSRC_ACCOUNT_ID")
    @ApiModelProperty(value = "储值卡卡号")
    private String gsrcAccountId;

    /**
     * 异动门店
     */
    @TableField(value = "GSRC_BR_ID")
    @ApiModelProperty(value = "异动门店")
    private String gsrcBrId;

    /**
     * 异动单号
     */
    @TableField(value = "GSRC_VOUCHER_ID")
    @ApiModelProperty(value = "异动单号")
    private String gsrcVoucherId;

    /**
     * 异动日期
     */
    @TableField(value = "GSRC_DATE")
    @ApiModelProperty(value = "异动日期")
    private String gsrcDate;

    /**
     * 异动时间
     */
    @TableField(value = "GSRC_TIME")
    @ApiModelProperty(value = "异动时间")
    private String gsrcTime;

    /**
     * 异动人员
     */
    @TableField(value = "GSRC_EMP")
    @ApiModelProperty(value = "异动人员")
    private String gsrcEmp;

    /**
     * 异动类型 1：充值 2：退款 3：消费 4：退货
     */
    @TableField(value ="GSRC_TYPE")
    @ApiModelProperty(value = "异动类型 1：充值 2：退款 3：消费 4：退货")
    private String gsrcType;

    /**
     * 储值卡初始金额
     */
    @TableField(value = "GSRC_INITIAL_AMT")
    @ApiModelProperty(value = "储值卡初始金额")
    private BigDecimal gsrcInitialAmt;
    /**
     * 储值卡异动金额
     */
    @TableField(value = "GSRC_CHANGE_AMT")
    @ApiModelProperty(value = "储值卡异动金额")
    private BigDecimal gsrcChangeAmt;
    /**
     * 储值卡结果金额
     */
    @TableField(value = "GSRC_RESULT_AMT")
    @ApiModelProperty(value = "储值卡结果金额")
    private BigDecimal gsrcResultAmt;

}
