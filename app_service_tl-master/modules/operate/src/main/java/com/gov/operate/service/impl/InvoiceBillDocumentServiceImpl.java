package com.gov.operate.service.impl;

import com.gov.operate.entity.InvoiceBillDocument;
import com.gov.operate.mapper.InvoiceBillDocumentMapper;
import com.gov.operate.service.IInvoiceBillDocumentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-03-22
 */
@Service
public class InvoiceBillDocumentServiceImpl extends ServiceImpl<InvoiceBillDocumentMapper, InvoiceBillDocument> implements IInvoiceBillDocumentService {

}
