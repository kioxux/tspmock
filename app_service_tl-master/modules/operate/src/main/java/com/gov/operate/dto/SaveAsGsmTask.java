package com.gov.operate.dto;

import com.gov.operate.entity.SdDiscount;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SaveAsGsmTask {

    /**
     * 加盟商
     */
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 活动名称
     */
    @NotBlank(message = "活动名称不能为空")
    private String gsmThename;

    /**
     * 活动简介
     */
    private String gsmTheintro;

    /**
     * 门店编码
     */
    @NotBlank(message = "门店不能为空")
    private String gsmStore;

    /**
     * 活动类型(0-主题活动，1-周期活动)
     */
    @NotBlank(message = "活动类型不能为空")
    private String gsmType;

    /**
     * 活动开始时间
     */
    @NotBlank(message = "活动开始时间不能为空")
    private String gsmStartd;
    /**
     * 活动结束时间
     */
    @NotBlank(message = "活动结束时间不能为空")
    private String gsmEndd;
    /**
     * 销售额标值
     */
    private BigDecimal gsmIndexValue;
    /**
     * 销售额活动值
     */
    private BigDecimal gsmTargetValue;
    /**
     * 活动涨幅
     */
    private BigDecimal gsmSaleinc;
    /**
     * 客单价选择(0不选择，1选择)
     */
    private String gsmPricechoose;

    /**
     * 客单价
     */
    private BigDecimal gsmPriceinc;

    /**
     * 客单价活动值
     */
    private BigDecimal gsmActPriceinc;

    /**
     * 客单价对应交易次数修正值
     */
    private BigDecimal gsmPriceincSt;

    /**
     * 交易次数选择(0不选择，1选择)
     */
    private String gsmStchoose;

    /**
     * 交易次数
     */
    private BigDecimal gsmStinc;

    /**
     * 交易次数对应客单价修正值
     */
    private String gsmStincPrice;

    /**
     * 毛利率
     */
    private BigDecimal gsmGrprofit;

    /**
     * 选品条件
     */
    private String gsmProcondi;

    /**
     * 短信模板ID
     */
    private String gsmTempid;

    /**
     * 短信查询条件
     */
    private String gsmSmscondi;

    /**
     * 短信发送人数
     */
    private BigDecimal gsmSmstimes;
    /**
     * 电话通知人数
     */
    private BigDecimal gsmTeltimes;
    /**
     * 电话查询条件
     */
    private String gsmTelcondi;
    /**
     * DM单打印张数
     */
    private BigDecimal gsmDmtimes;

    /**
     * 短信人集合
     */
    private List<String> smsList;

    /**
     * 选品集合
     */
    private List<ProductVO> productList;

    /**
     * 电话通知集合
     */
    private List<String> phoneList;

    /**
     * 折扣集合
     */
    private List<SdDiscount> discountList;


}
