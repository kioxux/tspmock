package com.gov.operate.feign;

import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.feign.dto.RegionDTO;
import com.gov.operate.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @desc: operation 调用服务
 * @author: Ryan
 * @createTime: 2021/11/7 01:02
 */
@Component
@FeignClient(value = "gys-operation", fallback = AuthFeignFallback.class)
public interface OperationFeign {

    /**
     * 生成加盟商默认区域信息
     * @param regionDTO 加盟商信息
     */
    @PostMapping(value = "/regional/defaultDate")
    JsonResult addDefaultRegion(@RequestBody RegionDTO regionDTO);

}
