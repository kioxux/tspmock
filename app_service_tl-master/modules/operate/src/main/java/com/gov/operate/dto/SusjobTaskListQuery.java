package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.03
 */
@Data
public class SusjobTaskListQuery extends Pageable {

    @ApiModelProperty(value = "任务名称")
    private String susJobname;

    @ApiModelProperty(value = "门店")
    private String susJobbr;

    private String userId;

    private String client;

    private List<String> stoCodeList;
}

