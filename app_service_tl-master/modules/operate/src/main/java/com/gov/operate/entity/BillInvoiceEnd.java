package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-12-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BILL_INVOICE_END")
@ApiModel(value="BillInvoiceEnd对象", description="")
public class BillInvoiceEnd extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "类型")
    @TableField("GBIE_TYPE")
    private Integer gbieType;

    @ApiModelProperty(value = "单据、发票号")
    @TableField("GBIE_NUM")
    private String gbieNum;

    @ApiModelProperty(value = "单据类型")
    @TableField("GBIE_BILL_TYPE")
    private String gbieBillType;

    @ApiModelProperty(value = "创建人")
    @TableField("GBIE_CREATE_USER")
    private String gbieCreateUser;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBIE_CREATE_DATE")
    private String gbieCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBIE_CREATE_TIME")
    private String gbieCreateTime;


}
