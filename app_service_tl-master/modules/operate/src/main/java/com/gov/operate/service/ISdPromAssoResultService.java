package com.gov.operate.service;

import com.gov.operate.entity.SdPromAssoResult;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
public interface ISdPromAssoResultService extends SuperService<SdPromAssoResult> {

}
