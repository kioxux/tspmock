package com.gov.operate.dto.invoice;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021-06-25 16:15
 */
@Data
public class ExportSelectWarehousingDetailsVO {

    @NotEmpty(message = "单据编号列表不能为空")
    private List<String> matDnIdList;

    private String client;

    private String matId;

    private String matYear;

    private String matLineNo;
}
