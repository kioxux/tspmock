package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-09-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_WMS_TUPIANZDRWCJ")
@ApiModel(value="WmsTupianzdrwcj对象", description="")
public class WmsTupianzdrwcj extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("ID")
    private Long id;

    @ApiModelProperty(value = "用户")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "指定任务采集编号")
    @TableField("ZDRWCJ_BH")
    private String zdrwcjBh;

    @ApiModelProperty(value = "序号")
    @TableField("WM_XH")
    private Integer wmXh;

    @ApiModelProperty(value = "商品自编码")
    @TableField("WM_SP_BM")
    private String wmSpBm;

    @ApiModelProperty(value = "手机号")
    @TableField("SHOUJIHAO")
    private String shoujihao;

    @ApiModelProperty(value = "姓名")
    @TableField("XINGMING")
    private String xingming;

    @ApiModelProperty(value = "总库编码")
    @TableField("WM_SP_ZKBM")
    private String wmSpZkbm;

    @ApiModelProperty(value = "创建日期")
    @TableField("WM_CJRQ")
    private String wmCjrq;

    @ApiModelProperty(value = "创建时间")
    @TableField("WM_CJSJ")
    private String wmCjsj;

    @ApiModelProperty(value = "创建人")
    @TableField("WM_CJR")
    private String wmCjr;

    @ApiModelProperty(value = "行状态  1-未完成；2-已确认；3-已拍照；4-已删除")
    @TableField("WM_ZT")
    private String wmZt;

    @ApiModelProperty(value = "删除原因")
    @TableField("WM_SCYY")
    private String wmScyy;

    @ApiModelProperty(value = "修改日期")
    @TableField("WM_XGRQ")
    private String wmXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("WM_XGSJ")
    private String wmXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("WM_XGR")
    private String wmXgr;

    @ApiModelProperty(value = "是否加盟商级别导入 1表示是 0表示否")
    @TableField("WM_CLIENT")
    private String wmClient;

    @ApiModelProperty(value = "任务失效时间，图片采集入口时存储，拉取任务列表时需要根据失效时间过滤")
    @TableField("WM_INVALID_DATE")
    private String wmInvalidDate;

    @ApiModelProperty(value = "是否存在任务失效标识 （目前用于区分是否是从图片匹配路口进入） 1表示存在失效 0表示不存在失效")
    @TableField("WM_INVALID_FLAG")
    private String wmInvalidFlag;
}
