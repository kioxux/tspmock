package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class DeductionDTO {

    /**
     * 索引
     */
    private int invoiceIndex;

    /**
     * 合适的金额
     */
    private BigDecimal money;
}
