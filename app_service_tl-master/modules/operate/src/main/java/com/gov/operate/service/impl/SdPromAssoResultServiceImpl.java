package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromAssoResult;
import com.gov.operate.mapper.SdPromAssoResultMapper;
import com.gov.operate.service.ISdPromAssoResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Service
public class SdPromAssoResultServiceImpl extends ServiceImpl<SdPromAssoResultMapper, SdPromAssoResult> implements ISdPromAssoResultService {

}
