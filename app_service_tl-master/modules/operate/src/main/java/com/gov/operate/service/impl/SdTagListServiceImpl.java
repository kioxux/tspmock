package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.operate.entity.SdTagList;
import com.gov.operate.mapper.SdTagListMapper;
import com.gov.operate.service.ISdTagListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
@Service
public class SdTagListServiceImpl extends ServiceImpl<SdTagListMapper, SdTagList> implements ISdTagListService {

    /**
     * 会员标签列表
     */
    @Override
    public List<SdTagList> getTagList() {
        return this.list(new QueryWrapper<SdTagList>().select("GSTL_ID", "GSTL_NAME", "GSTL_COLOR"));
    }
}
