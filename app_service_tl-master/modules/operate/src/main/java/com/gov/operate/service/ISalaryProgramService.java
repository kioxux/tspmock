package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SalaryProgram;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-23
 */
public interface ISalaryProgramService extends SuperService<SalaryProgram> {

    /**
     * 方案创建
     */
    Integer addProgram(AddProgramVO vo);

    /**
     * 方案修改
     */
    void editProgram(EditProgramVO vo);

    /**
     * 方案获取
     */
    SalaryProgramDTO getProgram(Integer gspId);

    /**
     * 方案列表
     */
    IPage<QueryProgramListDTO> queryProgramList(QueryProgramListVO vo);

    /**
     * 提交申批
     */
    void subTrial(SubTrialVO vo);
}
