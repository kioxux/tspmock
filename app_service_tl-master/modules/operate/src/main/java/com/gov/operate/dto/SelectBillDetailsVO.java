package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class SelectBillDetailsVO {

    @NotBlank(message = "单据号不能为空")
    private String matDnId;
    @NotBlank(message = "业务类型不能为空")
    private String type;

    private String client;
}
