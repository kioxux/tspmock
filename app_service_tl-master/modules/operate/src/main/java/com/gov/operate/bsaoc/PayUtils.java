package com.gov.operate.bsaoc;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.ApplicationConfig;
import com.gov.operate.dto.PayParams;
import com.gov.operate.dto.QueryPayStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Component
public class PayUtils {

    /**
     * 支付状态查询
     */
    public static String queryPayStatus(QueryPayStatus query, ApplicationConfig applicationConfig) throws Exception {
        JSONObject json = new JSONObject();
        // 消息Id,原样返回
        json.put("msgId", query.getMsgId());
        // 报文请求时间
        json.put("requestTimestamp", DateUtils.getCurrentDateTimeStr());
        // 商户号
        // 业务类型
        if(CommonEnum.PayType.GATEWAY.getCode().equals(query.getFicoType())){
            json.put("mid", applicationConfig.getB2b_mid());
            json.put("instMid", "UPG_BUSINESS");
        } else {
            json.put("mid", applicationConfig.getC2b_mid());
            json.put("instMid", "QRPAYDEFAULT");
        }
        // 终端号
        json.put("tid", applicationConfig.getC2b_tid());
        // 商户订单号
        json.put("billNo", query.getBillNo());
        // 账单日期
        json.put("billDate", query.getBillDate());
        if (CommonEnum.PayType.GATEWAY.getCode().equals(query.getFicoType())) {
            return b2bSend(applicationConfig.getC2b_queryUrl(), json.toString(), applicationConfig);
        } else {
            return c2bSend(applicationConfig.getC2b_queryUrl(), json.toString(), applicationConfig);
        }

    }

    /**
     * 二维码获取
     *
     * @param payParams
     * @return
     * @throws Exception
     */
    public static String payV2(PayParams payParams, ApplicationConfig applicationConfig) throws Exception {
        JSONObject json = new JSONObject();
        // 消息Id,原样返回
        json.put("msgId", payParams.getMsgId());
        // 报文请求时间
        json.put("requestTimestamp", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        // 商户号
        json.put("mid", applicationConfig.getC2b_mid());
        // 终端号
        json.put("tid", applicationConfig.getC2b_tid());
        // 业务类型
        json.put("instMid", applicationConfig.getC2b_instMid());
        // 商户订单号
        json.put("billNo", payParams.getBillNo());
        // 账单日期
        json.put("billDate", DateFormatUtils.format(new Date(), "yyyy-MM-dd"));
        // 账单描述
        json.put("billDesc", "账单描述");
        // 支付总金额
        json.put("totalAmount", payParams.getTotalAmount());
        // 订单过期时间,这里设置为十分钟后
        json.put("expireTime", DateFormatUtils.format(new Date().getTime() + 1000 * 60 * applicationConfig.getC2b_expireTime(), "yyyy-MM-dd HH:mm:ss"));
        // 支付结果通知地址,修改为商户自己的地址
        json.put("notifyUrl", applicationConfig.getC2b_notifyUrl());
        // 单钱包和多钱包,多钱包为:MULTIPLE
        json.put("walletOption", applicationConfig.getC2b_walletOption());
        log.info("申请二维码付款参数：{}", json.toJSONString());
        String result = c2bSend(applicationConfig.getC2b_url(), json.toString(), applicationConfig);
        log.info("申请二维码付款结果：{}", result);
        return result;
    }

    /**
     * 银联对公支付下单
     * @param payParams
     * @param applicationConfig
     * @return
     * @throws Exception
     */
    public static String payB2B(PayParams payParams, ApplicationConfig applicationConfig) throws Exception {
        JSONObject json = new JSONObject();
        json.put("mid", applicationConfig.getB2b_mid());
        json.put("tid", applicationConfig.getB2b_tid());
        json.put("instMid", "H5DEFAULT");
        json.put("totalAmount", payParams.getTotalAmount());
        json.put("merOrderId", payParams.getBillNo());
        // 订单过期时间,这里设置为十分钟后
        json.put("expireTime", DateFormatUtils.format(new Date().getTime() + 1000 * 60 * applicationConfig.getB2b_expireTime(), "yyyy-MM-dd HH:mm:ss"));
        json.put("requestTimestamp", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        // 支付结果通知地址,修改为商户自己的地址
        json.put("notifyUrl", applicationConfig.getB2b_notifyUrl());
        json.put("transType", "UPG_BUSINESS");
        json.put("bankName", Base64.encodeBase64String(payParams.getBankName().getBytes(StandardCharsets.UTF_8)));
        json.put("bizType", "100003");
        json.put("chnlNo", payParams.getChnlNo());
        json.put("termType", "PC");
        json.put("chnlType", "PC");

        log.info("B2B银联下单请求报文：{}", json.toJSONString());
        String param = getSignature(applicationConfig.getB2b_appId(), applicationConfig.getB2b_appKey(), json.toString());
        String url = applicationConfig.getB2b_order() + "?" + param;
        log.info("B2B银联下单请求URL：{}", url);
        return url;
    }

    /**
     * 发送请求
     *
     * @param entity
     * @return
     * @throws Exception
     */
    private static String c2bSend(String url, String entity, ApplicationConfig applicationConfig) throws Exception {
        String authorization = getOpenBodySig(applicationConfig.getC2b_appId(), applicationConfig.getC2b_appKey(), entity);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(entity, "UTF-8");
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity1 = response.getEntity();
        String resStr = null;
        if (entity1 != null) {
            resStr = EntityUtils.toString(entity1, "UTF-8");
        }
        httpClient.close();
        response.close();
        return resStr;
    }

    /**
     * 发送请求
     *
     * @param entity
     * @return
     * @throws Exception
     */
    private static String b2bSend(String url, String entity, ApplicationConfig applicationConfig) throws Exception {
        String authorization = getOpenBodySig(applicationConfig.getB2b_appId(), applicationConfig.getB2b_appKey(), entity);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(entity, "UTF-8");
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity1 = response.getEntity();
        String resStr = null;
        if (entity1 != null) {
            resStr = EntityUtils.toString(entity1, "UTF-8");
        }
        httpClient.close();
        response.close();
        return resStr;
    }

    /**
     * open-body-sig方式获取到Authorization 的值
     *
     * @param appId
     * @param appKey
     * @param body
     * @return
     * @throws Exception
     */
    private static String getOpenBodySig(String appId, String appKey, String body) throws Exception {
        // eg:20190227113148
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        // eg:be46cd581c9f46ecbd71b9858311ea12
        String nonce = UUID.randomUUID().toString().replace("-", "");
        byte[] data = body.getBytes("UTF-8");
        InputStream is = new ByteArrayInputStream(data);
        // eg:d60bc3aedeb853e2a11c0c096baaf19954dd9b752e48dea8e919e5fb29a42a8d
        String bodyDigest = testSHA256(is);
        // eg:f0ec96ad2c3848b5b810e7aadf369e2f + 20190227113148 + be46cd581c9f46ecbd71b9858311ea12 +
        String str1_C = appId + timestamp + nonce + bodyDigest;
        byte[] localSignature = hmacSHA256(str1_C.getBytes(), appKey.getBytes());
        // Signature
        String localSignatureStr = Base64.encodeBase64String(localSignature);
        return ("OPEN-BODY-SIG AppId=" + "\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"");
    }
    /**
     * 获取签名头
     *
     * @param appid
     * @param appkey
     * @param body      请求体
     * @return authorization 认证报文
     * @throws Exception
     */
    static String getSignature(String appid, String appkey, String body) throws Exception {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String nonce = UUID.randomUUID().toString().replace("-", "");
        byte[] data = body.getBytes("utf-8");
        InputStream is = new ByteArrayInputStream(data);
        String testSH = DigestUtils.sha256Hex(is);
        String s1 = appid+timestamp+nonce+testSH;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(appkey.getBytes("utf-8"),"HmacSHA256"));
        byte[] localSignature = mac.doFinal(s1.getBytes("utf-8"));
        String localSignatureStr = Base64.encodeBase64String(localSignature);

        String param = "authorization=OPEN-FORM-PARAM" + "&appId=" + appid + "&timestamp=" + timestamp + "&nonce=" + nonce + "&content=" + URLEncoder.encode(body.toString(), "UTF-8") + "&signature=" + URLEncoder.encode(localSignatureStr.trim(), "UTF-8");

        return param;
    }

    /**
     * 进行加密
     *
     * @param is
     * @return 加密后的结果
     */
    private static String testSHA256(InputStream is) {
        try {
            return DigestUtils.sha256Hex(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param data
     * @param key
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    private static byte[] hmacSHA256(byte[] data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data);
    }

    /**
     * 获取到订单号
     *
     * @param msgSrcId 消息来源编号  1017
     * @return 商户订单号    {来源编号(4位)}{时间(yyyyMMddmmHHssSSS)(17位)}{7位随机数}
     */
    public static String getBillNo(String msgSrcId) {
        return msgSrcId + DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
    }

//    public static void main(String[] args) throws Exception {
//        //   appId: 10037ca764636bbc01647d1ef4e10009
//        //  appKey: 389d830dcc0c46a58b0ea1962041e497
//        //  mid: 898340149000035
//        //  tid: 38557688
//        //  instMid: QRPAYDEFAULT
//        //  billNoPre: 1017
//        //  queryUrl: http://58.247.0.18:29015/v1/netpay/bills/query
//        //  notifyUrl: http://1172410739.uttcare.com:18008/purchase/invoicePaying/payResult
//        //  url: http://58.247.0.18:29015/v1/netpay/bills/get-qrcode
//        //  walletOption: SINGLE
//        //  expireTime: 5
//
//        ApplicationConfig applicationConfig = new ApplicationConfig();
//        applicationConfig.setAppId("10037ca764636bbc01647d1ef4e10009");
//        applicationConfig.setAppKey("389d830dcc0c46a58b0ea1962041e497");
//        applicationConfig.setMid("898340149000035");
//        applicationConfig.setTid("38557688");
//        applicationConfig.setInstMid("QRPAYDEFAULT");
//        applicationConfig.setBillNoPre("1017");
//        applicationConfig.setQueryUrl("http://58.247.0.18:29015/v1/netpay/bills/query");
//        applicationConfig.setNotifyUrl("http://1172410739.uttcare.com:18008/purchase/invoicePaying/payResult");
//        applicationConfig.setUrl("http://58.247.0.18:29015/v1/netpay/bills/get-qrcode");
//        applicationConfig.setWalletOption("SINGLE");
//        applicationConfig.setExpireTime(5);
//        //query(applicationConfig, "1017202011101401378080807418", "2020-11-10");
//
//    }

    public static String query(ApplicationConfig applicationConfig, String billNo, Integer type) throws Exception {
        /* post参数,格式:JSON */
        JSONObject body = new JSONObject();
        body.put("msgId", "001");   // 消息ID
        body.put("requestTimestamp", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));   // 报文请求时间
        String queryUrl = "";
        if(CommonEnum.PayType.GATEWAY.getCode().equals(type)) {
            body.put("mid", applicationConfig.getB2b_mid());  // 商户号
            body.put("instMid", "UPG_BUSINESS");    // 业务类型
            body.put("tid", applicationConfig.getB2b_tid());// 终端号
            queryUrl = applicationConfig.getB2b_queryUrl();
        }else {
            body.put("mid", applicationConfig.getC2b_mid());  // 商户号
            body.put("instMid", "QRPAYDEFAULT");    // 业务类型
            body.put("tid", applicationConfig.getC2b_tid());// 终端号
            queryUrl = applicationConfig.getC2b_queryUrl();
        }
        body.put("billNo", billNo);    // 账单号
        LocalDate localDate = DateUtils.stringToDate(StringUtils.right(StringUtils.left(billNo, 16), 8)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String date = DateUtils.formatLocalDate(localDate, "yyyy-MM-dd");
        body.put("billDate", date);    // 订单时间，格式yyyy-MM- dd
        log.info("任务，支付结果查询参数：{}", body.toJSONString());
        String send = "";
        if (CommonEnum.PayType.GATEWAY.getCode().equals(type)) {
            send = b2bSend(queryUrl, body.toString(), applicationConfig);
        } else {
            send = c2bSend(queryUrl, body.toString(), applicationConfig);
        }
        log.info("任务，支付结果查询返回：{}", send);
        return send;
    }
}
