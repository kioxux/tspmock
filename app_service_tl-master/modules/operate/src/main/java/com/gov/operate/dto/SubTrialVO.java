package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class SubTrialVO {

    @NotNull(message = "主键id不能为空")
    private Integer gspId;
}
