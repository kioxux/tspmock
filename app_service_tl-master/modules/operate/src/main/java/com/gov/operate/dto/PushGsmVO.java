package com.gov.operate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class PushGsmVO {

    @NotBlank(message = "营销活动ID不能为空")
    private String gsmMarketid;

    @NotEmpty(message = "所选加盟商集合不能为空")
    private List<String> clientList;

}
