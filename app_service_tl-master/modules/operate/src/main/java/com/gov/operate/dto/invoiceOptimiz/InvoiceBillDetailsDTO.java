package com.gov.operate.dto.invoiceOptimiz;

import com.gov.operate.entity.FicoInvoiceInformationRegistration;
import com.gov.operate.entity.InvoiceBill;
import com.gov.operate.entity.InvoiceBillDocument;
import com.gov.operate.entity.InvoiceBillDocumentItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/7 13:32
 */
@Data
@EqualsAndHashCode
public class InvoiceBillDetailsDTO extends InvoiceBill {

    private List<InvoiceBillInvoiceDTO> invoiceBillInvoiceDTOList;
    private List<InvoiceBillDocDTO> invoiceBillDocDTOList;


    @Data
    @EqualsAndHashCode
    public static class InvoiceBillInvoiceDTO extends FicoInvoiceInformationRegistration {
        private String client;
        private String gibiNum;
        private String gibiInvoiceNum;
        private String invoiceBalance;
        private String stoName;
        private String supName;
    }

    @Data
    @EqualsAndHashCode
    public static class InvoiceBillDocDTO extends InvoiceBillDocument {
        /**
         * 地点名
         */
        private String gibdSiteName;
        /**
         * 供应商名
         */
        private String gibdSupCodeName;

        private List<InvoiceBillDocDetailsDTO> invoiceBillDocDetailsDTOList;

        @Data
        @EqualsAndHashCode
        public static class InvoiceBillDocDetailsDTO extends InvoiceBillDocumentItem{
            private String id;
            private String type;
            private String typeName;
            private String proSelfCode;
            private String proName;
            private String proSpecs;
            private String proFactoryName;
        }
    }
}
