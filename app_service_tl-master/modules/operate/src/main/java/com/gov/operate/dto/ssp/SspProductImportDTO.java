package com.gov.operate.dto.ssp;

import com.gov.common.excel.annotation.ExcelField;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SspProductImportDTO {

    @ExcelField(title = "统一社会信用码", type = 2, sort = 0)
    @NotBlank(message = "统一社会信用码不能为空")
    private String creditCode;

    @ExcelField(title = "连锁公司编码", type = 2, sort = 1)
    @NotBlank(message = "连锁公司不能为空")
    private String chainHead;

    @ExcelField(title = "商品自编码", type = 2, sort = 2)
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;
}
