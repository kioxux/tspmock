package com.gov.operate.mapper;

import com.gov.operate.entity.FranchiseeGrp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface FranchiseeGrpMapper extends BaseMapper<FranchiseeGrp> {

}
