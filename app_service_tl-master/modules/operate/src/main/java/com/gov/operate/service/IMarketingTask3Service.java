package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdMarketing;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
public interface IMarketingTask3Service extends SuperService<SdMarketing> {

    /**
     * 当前人门店集合
     *
     * @return
     */
    List<StoreVo> getCurrentStoreList();

    /**
     * 营销任务列表
     */
    IPage<GetGsmTaskListDTO> getGsmTaskList(GsmTaskListVO gsmTaskListVO);

    /**
     * 营销任务明细
     *
     * @param client
     * @param gsmMarketid
     * @param gsmStore
     * @return
     */
    MarketingTaskVO getGsmTaskDetail(String client, String gsmMarketid, String gsmStore);

    /**
     * 选品查询
     *
     * @param productRequestVo
     * @return
     */
    IPage<ProductResponseVo> getProductList(ProductRequestVo productRequestVo);

    /**
     * 保存营销任务
     *
     * @param gsmTaskSaveVo
     * @return
     */
    Result saveGsmTask(GsmTaskSaveVo gsmTaskSaveVo) throws InstantiationException, IllegalAccessException;


    /**
     * 营销任务删除
     *
     * @param marketing
     * @return
     */
    Result deleteMarketTask(SdMarketing marketing);

    /**
     * 预估列表查询
     *
     * @param activeEvalVO
     * @return
     */
    Result getActiveEvalInfo(ActiveEvalVO activeEvalVO);

    /**
     * 已选商品查询
     *
     * @param productRequestVo
     * @return
     */
    Result getSelectedProductList(ProductRequestVo productRequestVo);

    /**
     * 已选赠品
     *
     * @param productRequestVo
     * @return
     */
    Result selectGiftPorductList(ProductRequestVo productRequestVo);

    /**
     * 发送审批
     *
     * @param id
     */
    void approval(Integer id);

    /**
     * 立即执行
     *
     * @param id
     */
    void execute(Integer id, String clientId, String flowNo, String status);
}
