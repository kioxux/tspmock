package com.gov.operate.service.impl;

import com.gov.operate.entity.WechatTweetsD;
import com.gov.operate.mapper.WechatTweetsDMapper;
import com.gov.operate.service.IWechatTweetsDService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信推文管理明细 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-01-07
 */
@Service
public class WechatTweetsDServiceImpl extends ServiceImpl<WechatTweetsDMapper, WechatTweetsD> implements IWechatTweetsDService {

}
