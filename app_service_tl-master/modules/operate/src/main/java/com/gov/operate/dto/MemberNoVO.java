package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class MemberNoVO {

    @NotBlank(message = "手机号不能为空")
    private String gsrcMobile;


}
