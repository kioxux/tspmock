package com.gov.operate.service;

import com.gov.mybatis.SuperService;
import com.gov.operate.entity.SupplierPrepaymentDetail;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2021-09-07
 */
public interface ISupplierPrepaymentDetailService extends SuperService<SupplierPrepaymentDetail> {

}
