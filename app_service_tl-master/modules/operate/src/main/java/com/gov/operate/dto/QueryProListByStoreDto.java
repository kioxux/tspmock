package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryProListByStoreDto {
    private String stoCode;

    private String stoName;

    private String proSelfCode;

    private String proCommonname;

    private String proSpecs;

    private String proFactoryName;

    private String proName;

    private String proPym;

    private String proLsj;
}
