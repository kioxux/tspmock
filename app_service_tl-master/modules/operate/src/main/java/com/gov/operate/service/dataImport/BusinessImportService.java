package com.gov.operate.service.dataImport;

import com.gov.common.entity.dataImport.ImportDto;
import com.gov.common.response.Result;
import com.gov.operate.entity.ProductBusiness;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
public interface BusinessImportService {
    /**
     * 导入
     *
     * @return
     */
    Result businessImport(ImportDto dto);

    List<ProductBusiness> importProductInfo(MultipartFile file);
}
