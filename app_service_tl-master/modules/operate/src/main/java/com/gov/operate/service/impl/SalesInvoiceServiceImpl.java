package com.gov.operate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.basic.*;
import com.gov.common.entity.ApplicationConfig;
import com.gov.common.entity.invoice.InvoiceEntity;
import com.gov.common.entity.invoice.InvoiceSendResponseDto;
import com.gov.common.excel.ExportExcel;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.invoice.*;
import com.gov.operate.entity.*;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.*;
import com.gov.operate.service.*;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SalesInvoiceServiceImpl implements ISalesInvoiceService {

    @Resource
    private CosUtils cosUtils;
    @Resource
    private CommonService commonService;
    @Resource
    private SalesInvoiceMapper salesInvoiceMapper;
    @Resource
    private SalesInvoiceDetailMapper salesInvoiceDetailMapper;
    @Resource
    private ISalesInvoiceDetailService salesInvoiceDetailService;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private SalesAmountMapper salesAmountMapper;
    @Resource
    private ISalesAmountService salesAmountService;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private DepDataMapper depDataMapper;
    @Resource
    private ClSystemParaMapper clSystemParaMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private CustomerBusinessMapper customerBusinessMapper;

    private static final String SALESINVOICESERVICEIMPL_ORDERID = "SALESINVOICESERVICEIMPL_ORDERID";

    private static int INVOICEITEMLENGTH = 100;

    @Override
    public IPage<SalesInvoicePageVO> selectSalesInvoiceByPage(SalesInvoicePageDTO vo) {
        TokenUser user = commonService.getLoginInfo();
        vo.setClient(user.getClient());
        Page<SalesInvoicePageVO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        return salesInvoiceMapper.selectSalesInvoiceByPage(page, vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String saveBill(List<SalesInvoicePageVO> list) throws IOException {
        if (list.size() == 0) {
            throw new CustomResultException("销售开票数据不能为空");
        }
        TokenUser user = commonService.getLoginInfo();
        double sum = list.stream().mapToDouble(t -> {
            if (t.getTotalAmount() != null) {
                // 退库 或 互调退库 或 销退  则为 负数
                if ("TD".equalsIgnoreCase(t.getMatType()) || "MD".equalsIgnoreCase(t.getMatType()) || "ED".equalsIgnoreCase(t.getMatType())) {
                    return t.getTotalAmount().multiply(new BigDecimal(-1)).doubleValue();
                }
                return t.getTotalAmount().doubleValue();
            } else {
                return BigDecimal.ZERO.doubleValue();
            }
        }).sum();
        String currentDate = DateUtils.getCurrentDateStrYYMMDD();
        Integer seq = salesInvoiceMapper.selectMaxId(currentDate);
        if (seq != null) {
            seq += 1;
        } else {
            seq = 1;
        }
        String orderId = currentDate + StringUtils.leftPad(String.valueOf(seq), 4, "0");
        List<SalesInvoiceExportDto> exportDtoList = new ArrayList<>();
        List<SalesInvoiceDetail> detailList = new ArrayList<>();
        Integer num = 1;
        String stoCode = "";
        for (SalesInvoicePageVO salesInvoicePageVO : list) {
            if (StringUtils.isBlank(stoCode)) {
                stoCode = salesInvoicePageVO.getStoCode();
            } else {
                if (!stoCode.equals(salesInvoicePageVO.getStoCode())) {
                    throw new CustomResultException("请选择同一客户进行开票");
                }
            }

            // 已申请数据
            List<SalesInvoiceDetail> exeList = salesInvoiceDetailMapper.selectItemsNew(user.getClient(), salesInvoicePageVO.getMatLineNo(), salesInvoicePageVO.getMatDnId(),
                    salesInvoicePageVO.getMatSiteCode(), salesInvoicePageVO.getStoCode(), salesInvoicePageVO.getMatProCode());
            // 验证开票数量
            if (CollectionUtils.isNotEmpty(exeList)) {
                double totalQty = exeList.stream().mapToDouble(t -> {
                    return t.getGsiQty().abs().doubleValue();
                }).sum();
                if (salesInvoicePageVO.getMatQty().abs().add(BigDecimal.valueOf(totalQty).abs()).compareTo(salesInvoicePageVO.getTotalQty()) > 0) {
                    throw new CustomResultException("单据" + salesInvoicePageVO.getMatDnId() + "商品" + salesInvoicePageVO.getMatProCode() + "开票数量已变更");
                }
            }

            //导出插入
            SalesInvoiceExportDto exportDto = new SalesInvoiceExportDto();
            exportDto.setSeq(num);
            exportDto.setProName(salesInvoicePageVO.getProName());
            exportDto.setProSpecs(salesInvoicePageVO.getProSpecs());
            exportDto.setProUnit(salesInvoicePageVO.getProUnit());
            if (StringUtils.isNotBlank(salesInvoicePageVO.getRateName())) {
                exportDto.setTax(new BigDecimal(salesInvoicePageVO.getRateName().replace("%", "")).divide(new BigDecimal((100)), 2, BigDecimal.ROUND_HALF_UP));
            }
            exportDto.setTaxItem("4002");
            exportDto.setProTaxClass(salesInvoicePageVO.getProTaxClass());

            BigDecimal totalQty = BigDecimal.ZERO;
            BigDecimal totalAmount = BigDecimal.ZERO;
            BigDecimal totalTax = BigDecimal.ZERO;

//            for (SalesInvoicePageVO vo : valueList) {
            // 退库 或 互调退库 或 销退  则为 减
            if ("TD".equalsIgnoreCase(salesInvoicePageVO.getMatType()) ||
                    "MD".equalsIgnoreCase(salesInvoicePageVO.getMatType()) ||
                    "ED".equalsIgnoreCase(salesInvoicePageVO.getMatType())) {
                if (salesInvoicePageVO.getMatQty() != null) {
                    totalQty = totalQty.subtract(salesInvoicePageVO.getMatQty());
                }
                if (salesInvoicePageVO.getTotalAmount() != null) {
                    totalAmount = totalAmount.subtract(salesInvoicePageVO.getTotalAmount());
                }
            } else {
                totalQty = totalQty.add(salesInvoicePageVO.getMatQty() == null ? BigDecimal.ZERO : salesInvoicePageVO.getMatQty());
                totalAmount = totalAmount.add(salesInvoicePageVO.getTotalAmount() == null ? BigDecimal.ZERO : salesInvoicePageVO.getTotalAmount());
            }
            totalTax = totalTax.add(salesInvoicePageVO.getAddTax());
//            }

            exportDto.setQty(totalQty);
            exportDto.setAmount(totalAmount.setScale(2, RoundingMode.HALF_UP));
            if (totalQty.compareTo(BigDecimal.ZERO) > 0) {
                exportDto.setPrice(totalAmount.divide(totalQty, 2, RoundingMode.HALF_UP));
            } else {
                exportDto.setPrice(BigDecimal.ZERO);
            }
            exportDto.setAddTax(totalTax.setScale(2, RoundingMode.HALF_UP));
            exportDto.setProCode(salesInvoicePageVO.getMatProCode());
            // 价格方式
            exportDto.setPriceMethod("1");
            // 税收分类编码版本
            exportDto.setTaxVersion("38.0");
            // 使用优惠政策标识
            exportDto.setOfferSign("0");
            // 中外合作油气田标识
            exportDto.setFlag("0");
            SalesInvoiceExportDto salesInvoiceExportDto = exportDtoList.stream().filter(t ->
                    t.getProCode().equals(exportDto.getProCode()) && t.getPrice().compareTo(exportDto.getPrice()) == 0
            ).findFirst().orElse(null);
            if (salesInvoiceExportDto == null) {
                exportDtoList.add(exportDto);
                num++;
            } else { // 单价、商品编码一致的记录 合并
                exportDtoList.forEach(item -> {
                    if (item.getProCode().equals(exportDto.getProCode()) && item.getPrice().compareTo(exportDto.getPrice()) == 0) {
                        // 数量 金额 税额
                        item.setQty(DecimalUtils.add(item.getQty(), exportDto.getQty()));
                        item.setAmount(DecimalUtils.add(item.getAmount(), exportDto.getAmount()));
                        item.setAddTax(DecimalUtils.add(item.getAddTax(), exportDto.getAddTax()));
                    }
                });
            }
        }

        String key = applicationConfig.getExportPath() + String.valueOf(System.currentTimeMillis()).concat(".xlsx");

        //保存销售开票主表
        SalesInvoice salesInvoice = new SalesInvoice();
        salesInvoice.setClient(user.getClient());
        salesInvoice.setGsiOrderId(orderId);
        salesInvoice.setGsiSite(list.get(0).getMatSiteCode());
        salesInvoice.setGsiCustomer(list.get(0).getStoCode());
        salesInvoice.setGsiAmount(BigDecimal.valueOf(sum));
        salesInvoice.setGsiUser(user.getUserId());
        salesInvoice.setGsiStatus("0");
        salesInvoice.setGsiExcelPath(key);
        salesInvoice.setGsiCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        salesInvoice.setGsiBillType("1");
        salesInvoice.setGsiBillStatus("1");
        salesInvoiceMapper.insert(salesInvoice);

        //销售开票明细表
        SalesInvoiceDetail detail = null;
        //设置自增值
        AtomicInteger index = new AtomicInteger(0);
        for (SalesInvoicePageVO vo : list) {
//            List<SalesInvoiceDetail> salesInvoiceDetailList = salesInvoiceDetailMapper.selectItems(user.getClient(), vo.getMatLineNo(), vo.getMatDnId(),
//                    vo.getMatSiteCode(), vo.getStoCode(), vo.getMatProCode());
//            if (CollectionUtils.isNotEmpty(salesInvoiceDetailList)) {
//                throw new CustomResultException("单据" + vo.getMatDnId() + "商品" + vo.getMatProCode() + "已开票");
//            }
            detail = new SalesInvoiceDetail();
            detail.setClient(user.getClient());
            detail.setGsiLineNo(vo.getMatLineNo());
            detail.setGsiOrderId(orderId);
            detail.setGsiDnId(vo.getMatDnId());
            detail.setGsiSite(vo.getMatSiteCode());
            detail.setGsiCustomer(vo.getStoCode());
            detail.setGsiProId(vo.getMatProCode());
            detail.setGsiTax(vo.getRateName());
            detail.setGsiSpecs(vo.getProSpecs());
            detail.setGsiUnit(vo.getProUnit());
            detail.setGsiPrice(vo.getPrice());
            detail.setGsiProName(vo.getProName());
            detail.setGsiMatType(vo.getMatType());
            // 退库 或 互调退库 或 销退  则为 负数
            if ("TD".equalsIgnoreCase(vo.getMatType()) || "MD".equalsIgnoreCase(vo.getMatType()) || "ED".equalsIgnoreCase(vo.getMatType())) {
                if (vo.getMatQty() != null) {
                    detail.setGsiQty(vo.getMatQty().multiply(new BigDecimal(-1)));
                }

                if (vo.getTotalAmount() != null) {
                    detail.setGsiAmount(vo.getTotalAmount().multiply(new BigDecimal(-1)));
                }
            } else {
                detail.setGsiQty(vo.getMatQty());
                detail.setGsiAmount(vo.getTotalAmount());
            }
            detail.setGsiTaxClass(vo.getProTaxClass());
            detail.setGsiAddTax(vo.getAddTax());
            detail.setGsiIndex(index.incrementAndGet());
            detailList.add(detail);
        }

        salesInvoiceDetailService.saveBatch(detailList);

        // 导出excel
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ExportExcel ee = new ExportExcel(null, SalesInvoiceExportDto.class, 1);
        ee.setDataList(exportDtoList);
        ee.write(bos);
        ee.dispose();
        // 上传 cos
        String path = cosUtils.uploadFileByKey(bos, key);
        if (StringUtils.isBlank(path)) {
            throw new CustomResultException("生成失败");
        }
        return cosUtils.urlAuth(path);

    }

    /**
     * 开票登记
     *
     * @param dto
     * @return
     */
    @Override
    public IPage<SalesInvoiceRegisterPageVO> selectSalesInvoiceRegisterByPage(SalesInvoiceRegisterPageDTO dto) {
        TokenUser user = commonService.getLoginInfo();
        dto.setClient(user.getClient());
        Page<SalesInvoiceRegisterPageVO> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        return salesInvoiceMapper.selectSalesInvoiceRegisterByPage(page, dto);
    }

    /**
     * 登记明细
     *
     * @param dto
     * @return
     */
    @Override
    public SalesInvoiceDetailVO getRegistDetail(SalesInvoiceDetailDTO dto) {
        SalesInvoiceDetailVO vo = new SalesInvoiceDetailVO();
        TokenUser user = commonService.getLoginInfo();
        dto.setClient(user.getClient());
        List<SalesInvoiceDetailItemDTO> detailList = salesInvoiceMapper.selectRegistDetail(dto);
        vo.setDetailList(detailList);

        QueryWrapper<SalesAmount> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", dto.getClient());
        queryWrapper.eq("GSA_ORDER_ID", dto.getGsiOrderId());

        List<SalesAmount> amountList = salesAmountMapper.selectList(queryWrapper);
        vo.setAmountList(amountList);

        if (CollectionUtils.isNotEmpty(detailList)) {
            String gsiBill = detailList.get(0).getGsiBill();
            if (StringUtils.isNotBlank(gsiBill)) {
                vo.setBillList(Arrays.asList(gsiBill.split(";")));
            }
        }
        return vo;
    }

    @Override
    @Transactional
    public Result submitInvoiceDetail(String gsiOrderId, String gsiBill) {
        TokenUser user = commonService.getLoginInfo();
        SalesInvoice entity = new SalesInvoice();
        entity.setGsiBill(StringUtils.isBlank(gsiBill) ? "" : gsiBill);
        salesInvoiceMapper.update(entity,
                new QueryWrapper<SalesInvoice>()
                        .eq("CLIENT", user.getClient())
                        .eq("GSI_ORDER_ID", gsiOrderId)
        );
        return ResultUtil.success();
    }

    @Override
    public Result submitAmountDetail(SalesInvoiceDetailVO dto) {
        TokenUser user = commonService.getLoginInfo();
        Map<String, Object> columnMap = new HashMap<>();
        columnMap.put("CLIENT", user.getClient());
        columnMap.put("GSA_ORDER_ID", dto.getGsiOrderId());
        salesAmountMapper.deleteByMap(columnMap);

        if (!dto.getAmountList().isEmpty()) {
            int i = 1;
            for (SalesAmount bill : dto.getAmountList()) {
                bill.setClient(user.getClient());
                bill.setGsaLineNo(String.valueOf(i));
                bill.setGsaOrderId(dto.getGsiOrderId());
                i += 1;
            }
            salesAmountService.saveBatch(dto.getAmountList());
        }

        return ResultUtil.success();
    }

    @Override
    public Result selectSalesInvoiceRegisterTotalByPage(SalesInvoiceRegisterPageDTO dto) {
        TokenUser user = commonService.getLoginInfo();
        dto.setClient(user.getClient());
        return ResultUtil.success(salesInvoiceMapper.selectSalesInvoiceRegisterTotalByPage(dto));
    }


    /**
     * 开票人员
     *
     * @return
     */
    @Override
    public List<UserData> getInvoiceUserList(String type) {
        TokenUser user = commonService.getLoginInfo();
        List<UserData> userDataList = new ArrayList<>();
        if (StringUtils.isBlank(type) || type.equals("1")) {
            userDataList = userDataMapper.selectList(new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_BILL", "Y"));
        } else if (type.equals("2")) {
            userDataList = userDataMapper.selectList(new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_RECEIVER", "Y"));
        } else if (type.equals("3")) {
            userDataList = userDataMapper.selectList(new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_CHECK", "Y"));
        }
        return userDataList;
    }

    /**
     * 当前开票人员
     *
     * @return
     */
    @Override
    public Result getInvoiceUser() {
        TokenUser user = commonService.getLoginInfo();
        UserData userData = userDataMapper.selectOne(new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_ID", user.getUserId()));
        return ResultUtil.success(userData);
    }

    @Override
    public void callback(HttpServletRequest httpServletRequest) throws Exception {
        Map<String, String[]> properties = httpServletRequest.getParameterMap();
//        log.info("开票回调结果：{}", JSONObject.toJSONString(properties));
        // 参数空
        if (properties == null || properties.isEmpty()) {
            return;
        }
        if (properties.get("content") == null || properties.get("content").length == 0) {
            return;
        }
        JSONObject content = JSONObject.parseObject(properties.get("content")[0]);
        if (content.get("c_fpqqlsh") == null || StringUtils.isBlank(content.get("c_fpqqlsh").toString())) {
            return;
        }
        // 开票流水号
        String fpqqlsh = content.get("c_fpqqlsh").toString();
        if (content.get("c_fphm") == null || StringUtils.isBlank(content.get("c_fphm").toString())) {
            return;
        }
        // 发票号
        String fphm = content.get("c_fphm").toString();
        if (content.get("c_url") == null || StringUtils.isBlank(content.get("c_url").toString())) {
            return;
        }
        // 发票地址
        String url = content.get("c_url").toString();
        SalesInvoice salesInvoice = new SalesInvoice();
        salesInvoice.setGsiBill(fphm);
        salesInvoice.setGsiBillStatus("1");
        String path = applicationConfig.getInvoicePath() + url.substring(url.lastIndexOf("/") + 1);
        salesInvoice.setGsiInvoicePdf(fphm + "|_|" + path);
        // salesInvoiceDetail.setGsiInvoiceReturn(JSONObject.toJSONString(properties));
        // 上传至腾讯云
        Result result = cosUtils.uploadInvoiceFile(applicationConfig, InvoiceUtils.parse(new URL(url).openStream()), url.substring(url.lastIndexOf("/") + 1));
        if (ResultUtil.hasError(result)) {
            return;
        }
//        salesInvoiceDetailMapper.update(salesInvoiceDetail,
//                new QueryWrapper<SalesInvoiceDetail>().eq("GSI_INVOICE_NUM", fpqqlsh)
//        );
        salesInvoiceMapper.update(salesInvoice,
                new QueryWrapper<SalesInvoice>().eq("GSI_INVOICE_SEQ", fpqqlsh)
        );
    }

    /**
     * 自动开票
     *
     * @param autoInvoice
     * @return
     */
    @Override
    public Result invoiceAuto(AutoInvoice autoInvoice) throws Exception {
        TokenUser user = commonService.getLoginInfo();
        List<SalesInvoicePageVO> list = autoInvoice.getList();
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("销售开票数据不能为空");
        }
        String cusName = "", cusNo = "", add = "", bankAccount = "", bankName = "", tel = "";
        StoreData store = storeDataMapper.selectOne(new QueryWrapper<StoreData>().eq("CLIENT", user.getClient()).eq("STO_CODE", list.get(0).getStoCode()));
        CustomerBusiness customerBusiness = customerBusinessMapper.selectOne(
                new QueryWrapper<CustomerBusiness>().eq("CLIENT", user.getClient())
                        .eq("CUS_SITE", list.get(0).getMatSiteCode()).eq("CUS_SELF_CODE", list.get(0).getStoCode())
        );
        if (store == null && customerBusiness == null) {
            throw new CustomResultException("购方信息未找到");
        }
        if (store != null) {
            cusName = store.getStoName();
            cusNo = store.getStoNo();
            add = store.getStoAdd();
            bankAccount = store.getStoBankAccount();
            bankName = store.getStoBankName();
            tel = store.getStoTelephone();
        } else if (customerBusiness != null) {
            cusName = customerBusiness.getCusName();
            cusNo = customerBusiness.getCusCreditCode();
            add = customerBusiness.getCusRegAdd();
            bankAccount = customerBusiness.getCusBankAccount();
            bankName = customerBusiness.getCusBankName();
            tel = customerBusiness.getCusContactTel();
        }
        // 增值税专用发票
        if (autoInvoice.getGsiInvoiceClass().toLowerCase().equals("s")) {
            if (StringUtils.isBlank(add)) {
                throw new CustomResultException("购方详细地址未维护 ");
            }
            if (StringUtils.isBlank(bankName)) {
                throw new CustomResultException("购方开户行名未维护");
            }
            if (StringUtils.isBlank(bankAccount)) {
                throw new CustomResultException("购方开户行账号未维护");
            }
            if (StringUtils.isBlank(tel)) {
                throw new CustomResultException("购方电话未维护");
            }
        }

        if (StringUtils.isBlank(autoInvoice.getGsiInvoiceUserId()) || StringUtils.isBlank(autoInvoice.getGsiInvoiceUserName())) {
            throw new CustomResultException("请选择开票人");
        }
        if (StringUtils.isBlank(autoInvoice.getGsiInvoicePhone())) {
            throw new CustomResultException("请输入手机号码");
        }
        // 是否推送
        if (autoInvoice.getGsiInvoicePush().intValue() == 1) {
            if (StringUtils.isBlank(autoInvoice.getGsiInvoiceEmail())) {
                throw new CustomResultException("请输入邮箱地址");
            }
        }
        // 销方企业税号
        Compadm compadm = depDataMapper.getTaxBySite(user.getClient(), list.get(0).getMatSiteCode());
        if (compadm == null) {
            throw new CustomResultException("销方开票信息未找到");
        }
        if (StringUtils.isBlank(compadm.getCompadmNo())) {
            throw new CustomResultException("销方开票税号未找到");
        }
        if (StringUtils.isBlank(compadm.getCompadmBillId())) {
            throw new CustomResultException("销方开票身份未找到");
        }
        // 单票额度
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(
                new QueryWrapper<ClSystemPara>().eq("CLIENT", user.getClient())
                        .eq("GCSP_ID", "INVOICE_AMOUNT_LIMIT_" + autoInvoice.getGsiInvoiceClass().toUpperCase() + "_" + compadm.getCompadmId())
        );
        double sum = list.stream().mapToDouble(t -> {
            if (t.getTotalAmount() != null) {
                // 退库 或 互调退库 或 销退  则为 负数
                if ("TD".equalsIgnoreCase(t.getMatType()) || "MD".equalsIgnoreCase(t.getMatType()) || "ED".equalsIgnoreCase(t.getMatType())) {
                    return t.getTotalAmount().abs().multiply(new BigDecimal(-1)).doubleValue();
                }
                return t.getTotalAmount().doubleValue();
            } else {
                return BigDecimal.ZERO.doubleValue();
            }
        }).sum();
        String key = null;

        String currentDate = DateUtils.getCurrentDateStrYYMMDD();
        Integer seq = salesInvoiceMapper.selectMaxId(currentDate);
        List<InvoiceEntity> invoiceEntityList = new ArrayList<>();
        // 拆单
        List<AutoInvoice> autoInvoiceList = new ArrayList<>();
        // 单张
        if (clSystemPara == null) {
            // 拆单
            autoInvoiceList = splitInvoice(list, autoInvoice);
        } else { // 拆分
            // 单票额度
            String gcspPara2 = clSystemPara.getGcspPara2();
            Double quota = NumberUtils.toDouble(gcspPara2, 0);
            // 单张
            if (StringUtils.isBlank(gcspPara2)) {
                // 拆单
                autoInvoiceList = splitInvoice(list, autoInvoice);
            } else if (quota >= sum) { // 单张
                if (quota <= 0) {
                    throw new CustomResultException("发票额度设置不正确");
                }
                // 拆单
                autoInvoiceList = splitInvoice(list, autoInvoice);
            } else {
                if (quota <= 0) {
                    throw new CustomResultException("发票额度设置不正确");
                }
                // 行金额大于发票额度数据
                List<SalesInvoicePageVO> uList = new ArrayList<>();
                for (int i = list.size() - 1; i >= 0; i--) {
                    // 单行数据
                    SalesInvoicePageVO salesInvoicePageVO = list.get(i);
                    if (salesInvoicePageVO.getTotalAmount().compareTo(BigDecimal.valueOf(quota)) > 0) {
                        uList.add(salesInvoicePageVO);
                        list.remove(i);
                    }
                }
                List<SalesInvoicePageVO> itemList = new ArrayList<>();
                // 拆分
                for (int i = 0; i < uList.size(); i++) {
                    SalesInvoicePageVO salesInvoicePageVO = uList.get(i);
                    if (salesInvoicePageVO.getPrice().compareTo(BigDecimal.valueOf(quota)) > 0) {
                        throw new CustomResultException("发票额度不够");
                    }
                    // N件商品一张发票
                    int minQty = BigDecimal.valueOf(quota).divide(salesInvoicePageVO.getPrice(), 0, BigDecimal.ROUND_DOWN).intValue();
                    int cnt = salesInvoicePageVO.getMatQty().divide(BigDecimal.valueOf(minQty), 0, BigDecimal.ROUND_UP).intValue();
                    BigDecimal qty = salesInvoicePageVO.getMatQty();
                    while (qty.compareTo(BigDecimal.ZERO) > 0) {
                        SalesInvoicePageVO entity = new SalesInvoicePageVO();
                        BeanUtils.copyProperties(salesInvoicePageVO, entity);
                        if (qty.compareTo(BigDecimal.valueOf(minQty)) > 0) {
                            entity.setMatQty(BigDecimal.valueOf(minQty));
                            entity.setTotalAmount(BigDecimal.valueOf(minQty).multiply(salesInvoicePageVO.getPrice()));
                            qty = qty.subtract(BigDecimal.valueOf(minQty));
                            itemList.add(entity);
                        } else {
                            entity.setMatQty(qty);
                            entity.setTotalAmount(qty.multiply(salesInvoicePageVO.getPrice()));
                            qty = BigDecimal.ZERO;
                            itemList.add(entity);
                        }
                    }
                }
                // 合并
                list.addAll(itemList);
                // 排序
                List<SalesInvoicePageVO> ascList = list.stream().sorted(Comparator.comparing(SalesInvoicePageVO::getTotalAmount)).collect(Collectors.toList());
                // 拆单
                for (int i = ascList.size() - 1; i >= 0; i--) {
                    SalesInvoicePageVO salesInvoicePageVOi = ascList.get(i);
                    if (salesInvoicePageVOi.isUse()) {
                        continue;
                    }
                    salesInvoicePageVOi.setUse(true);
                    // 拆单主表
                    AutoInvoice entity = new AutoInvoice();
                    BeanUtils.copyProperties(autoInvoice, entity);
                    List<SalesInvoicePageVO> itemEntity = new ArrayList<>();
                    itemEntity.add(salesInvoicePageVOi);
                    for (int j = ascList.size() - 1; j >= 0; j--) {
                        SalesInvoicePageVO salesInvoicePageVOj = ascList.get(j);
                        if (salesInvoicePageVOj.isUse()) {
                            continue;
                        }
                        double itemSum = itemEntity.stream().mapToDouble(t -> {
                            if (t.getTotalAmount() != null) {
                                // 退库 或 互调退库 或 销退  则为 负数
                                if ("TD".equalsIgnoreCase(t.getMatType()) || "MD".equalsIgnoreCase(t.getMatType()) || "ED".equalsIgnoreCase(t.getMatType())) {
                                    return t.getTotalAmount().abs().multiply(new BigDecimal(-1)).doubleValue();
                                }
                                return t.getTotalAmount().doubleValue();
                            } else {
                                return BigDecimal.ZERO.doubleValue();
                            }
                        }).sum();
                        if (BigDecimal.valueOf(itemSum).add(salesInvoicePageVOj.getTotalAmount()).compareTo(BigDecimal.valueOf(quota)) <= 0) {
                            salesInvoicePageVOj.setUse(true);
                            itemEntity.add(salesInvoicePageVOj);
                        }
                        // 单张发票明细长度
                        if (itemEntity.size() >= INVOICEITEMLENGTH) {
                            break;
                        }
                    }
                    entity.setList(itemEntity);
                    autoInvoiceList.add(entity);
                }
            }
        }
        // 下单
        for (AutoInvoice entity : autoInvoiceList) {
            invoiceAuto(entity, key, seq, invoiceEntityList, compadm, cusName, cusNo, add, bankAccount, bankName, tel);
        }
        // 生成发票
        craeteInvoice(invoiceEntityList, compadm);
        return ResultUtil.success();
    }

    private List<AutoInvoice> splitInvoice(List<SalesInvoicePageVO> list, AutoInvoice autoInvoice) {
        List<AutoInvoice> autoInvoiceList = new ArrayList<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            SalesInvoicePageVO salesInvoicePageVOi = list.get(i);
            if (salesInvoicePageVOi.isUse()) {
                continue;
            }
            salesInvoicePageVOi.setUse(true);
            // 拆单主表
            AutoInvoice entity = new AutoInvoice();
            BeanUtils.copyProperties(autoInvoice, entity);
            List<SalesInvoicePageVO> itemEntity = new ArrayList<>();
            itemEntity.add(salesInvoicePageVOi);
            for (int j = list.size() - 1; j >= 0; j--) {
                SalesInvoicePageVO salesInvoicePageVOj = list.get(j);
                if (salesInvoicePageVOj.isUse()) {
                    continue;
                }
                salesInvoicePageVOj.setUse(true);
                itemEntity.add(salesInvoicePageVOj);
                // 单张发票明细长度
                if (itemEntity.size() >= INVOICEITEMLENGTH) {
                    break;
                }
            }
            entity.setList(itemEntity);
            autoInvoiceList.add(entity);
        }
        return autoInvoiceList;
    }

    /**
     * 生成发票
     *
     * @param invoiceEntityList
     * @param compadm
     * @throws Exception
     */
    private void craeteInvoice(List<InvoiceEntity> invoiceEntityList, Compadm compadm) throws Exception {
        TokenUser user = commonService.getLoginInfo();
        // 开票
        if (CollectionUtils.isNotEmpty(invoiceEntityList)) {
            for (InvoiceEntity invoiceEntity : invoiceEntityList) {
                String failure = InvoiceUtils.reqSend(applicationConfig, invoiceEntity, compadm.getCompadmBillId());
                SalesInvoice salesInvoice = new SalesInvoice();
                // 开票成功
                if (!failure.startsWith("failure")) {
                    salesInvoice.setGsiInvoiceSeq(failure);
                } else {
                    salesInvoice.setGsiBillStatus("2");
                    salesInvoice.setGsiBillStatusReason(failure.replace("failure", ""));
                }
                salesInvoiceMapper.update(salesInvoice, new QueryWrapper<SalesInvoice>()
                        .eq("CLIENT", user.getClient()).eq("GSI_INVOICE_SEQ", invoiceEntity.getOrderId()));
            }
        }
    }

    @Override
    public void invoiceReturnNew() throws Exception {
        // 开票中的数据
        List<SalesInvoice> salesInvoiceList = salesInvoiceMapper.selectList(
                new QueryWrapper<SalesInvoice>().eq("GSI_BILL_STATUS", "0").eq("GSI_BILL_TYPE", "2")
        );
        if (CollectionUtils.isEmpty(salesInvoiceList)) {
            return;
        }
        for (SalesInvoice salesInvoice : salesInvoiceList) {
            Map<String, Object> map = new HashMap<>();
            map.put("identity", salesInvoice.getGsiInvoiceIdentity());
            List<String> list = new ArrayList<>();
            list.add(salesInvoice.getGsiInvoiceSeq());
            map.put("fpqqlsh", list);
            InvoiceSendResponseDto resDto = InvoiceUtils.resSend(applicationConfig, JsonUtils.beanToJson(map));
            if (resDto != null && org.apache.commons.lang3.StringUtils.isNotBlank(resDto.getCMsg())) {
                if (resDto.getCMsg().startsWith("开票完成")) {
                    //pdf地址
                    String cUrl = resDto.getCUrl();
                    if (org.apache.commons.lang3.StringUtils.isNotBlank(cUrl)) {
                        //上传至腾讯云
                        Result result = cosUtils.uploadInvoiceFile(applicationConfig, InvoiceUtils.parse(new URL(cUrl).openStream()), cUrl.substring(cUrl.lastIndexOf("/") + 1));
                        if (ResultUtil.hasError(result)) {
                            continue;
                        }
                        SalesInvoice entity = new SalesInvoice();
                        entity.setGsiBill((StringUtils.isBlank(entity.getGsiBill()) ? "" : entity.getGsiBill() + ";") + resDto.getFicoFphm());
                        String path = applicationConfig.getInvoicePath() + cUrl.substring(cUrl.lastIndexOf("/") + 1);
                        entity.setGsiInvoicePdf(resDto.getFicoFphm() + "|_|" + path);
                        entity.setGsiBillStatus("1");
                        entity.setGsiInvoiceReturn(JsonUtils.beanToJson(resDto));
                        salesInvoiceMapper.update(entity,
                                new QueryWrapper<SalesInvoice>()
                                        .eq("CLIENT", salesInvoice.getClient())
                                        .eq("GSI_ORDER_ID", salesInvoice.getGsiOrderId())
                                        .eq("GSI_INVOICE_SEQ", salesInvoice.getGsiInvoiceSeq())
                        );
                    }
                } else if (resDto.getCMsg().contains("开票失败")) {
                    SalesInvoice entity = new SalesInvoice();
                    entity.setGsiBillStatus("2");
                    entity.setGsiInvoiceReturn(JsonUtils.beanToJson(resDto));
                    entity.setGsiBillStatusReason(resDto.getCMsg());
                    salesInvoiceMapper.update(entity,
                            new QueryWrapper<SalesInvoice>()
                                    .eq("CLIENT", salesInvoice.getClient())
                                    .eq("GSI_ORDER_ID", salesInvoice.getGsiOrderId())
                                    .eq("GSI_INVOICE_SEQ", salesInvoice.getGsiInvoiceSeq())
                    );
                }
            }
        }
    }

    /**
     * 删除开票信息
     *
     * @param gsiOrderId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteInvoice(String gsiOrderId) {
        TokenUser user = commonService.getLoginInfo();
        // 开票主表删除
        salesInvoiceMapper.delete(
                new QueryWrapper<SalesInvoice>().eq("CLIENT", user.getClient())
                        .eq("GSI_ORDER_ID", gsiOrderId)
        );
        // 开票明细表删除
        salesInvoiceDetailMapper.delete(
                new QueryWrapper<SalesInvoiceDetail>().eq("CLIENT", user.getClient())
                        .eq("GSI_ORDER_ID", gsiOrderId)
        );
    }

    /**
     * 重开票
     *
     * @param gsiOrderId
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result reInvoice(String gsiOrderId) throws Exception {
        TokenUser user = commonService.getLoginInfo();
        SalesInvoice salesInvoice = salesInvoiceMapper.selectOne(
                new QueryWrapper<SalesInvoice>().eq("CLIENT", user.getClient()).eq("GSI_ORDER_ID", gsiOrderId)
        );
        List<SalesInvoiceDetail> salesInvoiceDetailList = salesInvoiceDetailMapper.selectList(
                new QueryWrapper<SalesInvoiceDetail>().eq("CLIENT", user.getClient()).eq("GSI_ORDER_ID", gsiOrderId)
        );
        if (salesInvoice == null || CollectionUtils.isEmpty(salesInvoiceDetailList)) {
            throw new CustomResultException("开票信息不存在");
        }
        // 销方企业税号
        Compadm compadm = depDataMapper.getTaxBySite(user.getClient(), salesInvoice.getGsiSite());
        if (compadm == null) {
            throw new CustomResultException("销方开票信息未找到");
        }
        if (StringUtils.isBlank(compadm.getCompadmNo())) {
            throw new CustomResultException("销方开票税号未找到");
        }
        if (StringUtils.isBlank(compadm.getCompadmBillId())) {
            throw new CustomResultException("销方开票身份未找到");
        }
        // 单票额度
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(
                new QueryWrapper<ClSystemPara>().eq("CLIENT", user.getClient())
                        .eq("GCSP_ID", "INVOICE_AMOUNT_LIMIT_" + salesInvoice.getGsiInvoiceClass().toUpperCase() + "_" + compadm.getCompadmId())
        );
        if (clSystemPara != null) {
            // 单票额度
            String gcspPara2 = clSystemPara.getGcspPara2();
            Double quota = NumberUtils.toDouble(gcspPara2, 0);
            if (quota <= 0) {
                throw new CustomResultException("发票额度设置不正确");
            }
            // 单张
            if (BigDecimal.valueOf(quota).compareTo(salesInvoice.getGsiAmount()) < 0) {
                throw new CustomResultException("发票额度不够，请删除后重开。");
            }
        }
        StoreData store = storeDataMapper.selectOne(
                new QueryWrapper<StoreData>().eq("CLIENT", user.getClient()).eq("STO_CODE", salesInvoice.getGsiCustomer())
        );
        CustomerBusiness customerBusiness = customerBusinessMapper.selectOne(
                new QueryWrapper<CustomerBusiness>().eq("CLIENT", user.getClient())
                        .eq("CUS_SITE", salesInvoice.getGsiSite()).eq("CUS_SELF_CODE", salesInvoice.getGsiCustomer())
        );
        if (store == null && customerBusiness == null) {
            throw new CustomResultException("购方信息未找到");
        }
        String cusName = "", cusNo = "", add = "", bankAccount = "", bankName = "", tel = "";
        if (store != null) {
            cusName = store.getStoName();
            cusNo = store.getStoNo();
            add = store.getStoAdd();
            bankAccount = store.getStoBankAccount();
            bankName = store.getStoBankName();
            tel = store.getStoTelephone();
        } else if (customerBusiness != null) {
            cusName = customerBusiness.getCusName();
            cusNo = customerBusiness.getCusCreditCode();
            add = customerBusiness.getCusRegAdd();
            bankAccount = customerBusiness.getCusBankAccount();
            bankName = customerBusiness.getCusBankName();
            tel = customerBusiness.getCusContactTel();
        }
        // 增值税专用发票
        if (salesInvoice.getGsiInvoiceClass().toLowerCase().equals("s")) {
            if (StringUtils.isBlank(add)) {
                throw new CustomResultException("购方详细地址未维护 ");
            }
            if (StringUtils.isBlank(bankName)) {
                throw new CustomResultException("购方开户行名未维护");
            }
            if (StringUtils.isBlank(bankAccount)) {
                throw new CustomResultException("购方开户行账号未维护");
            }
            if (StringUtils.isBlank(tel)) {
                throw new CustomResultException("购方电话未维护");
            }
        }

        // 开票
        InvoiceEntity invoiceEntity = new InvoiceEntity();
        String orderNo = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
        invoiceEntity.setOrderId(orderNo);
        // 购方企业名称
        invoiceEntity.setBuyername(cusName);
        // 购方企业税号
        invoiceEntity.setTaxnum(cusNo);
        // 购方企业地址
        invoiceEntity.setAddress(add);
        // 购方企业银行开户行及账号
        invoiceEntity.setAccount((StringUtils.isBlank(bankName) ? "" : bankName) + " " + (StringUtils.isBlank(bankAccount) ? "" : bankAccount));
        // 购方企业电话
        invoiceEntity.setTelephone(tel);
        // 订单号
        invoiceEntity.setOrderno(orderNo);
        // 回调
        invoiceEntity.setCallbackurl(applicationConfig.getInvoiceCallBack());
        // 当前提交时间
        invoiceEntity.setInvoicedate(DateUtils.getCurrentDateTimeStr());
        // 销方企业税号
        invoiceEntity.setSaletaxnum(compadm.getCompadmNo());
        invoiceEntity.setSaleaccount("");
        invoiceEntity.setSalephone("");
        invoiceEntity.setSaleaddress("");
        // 发票类型，1:正票;2：红票
        invoiceEntity.setKptype("1");
        invoiceEntity.setMessage("");
        // 开票员
        UserData userData1 = userDataMapper.selectOne(
                new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_ID", salesInvoice.getGsiBillUser())
        );
        if (userData1 != null) {
            invoiceEntity.setClerk(userData1.getUserNam());
        }
        // 收款人
        UserData userData2 = userDataMapper.selectOne(
                new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_ID", salesInvoice.getGsiReceiverUser())
        );
        if (userData2 != null) {
            invoiceEntity.setPayee(userData2.getUserNam());
        }
        // 复核人
        UserData userData3 = userDataMapper.selectOne(
                new QueryWrapper<UserData>().eq("CLIENT", user.getClient()).eq("USER_ID", salesInvoice.getGsiCheckUser())
        );
        if (userData3 != null) {
            invoiceEntity.setChecker(userData3.getUserNam());
        }
        // 对应蓝票发票代码
        invoiceEntity.setFpdm("");
        // 对应蓝票发票号码
        invoiceEntity.setFphm("");
        // 推送方式，-1:不推送;0:邮箱;1:手机(默认);2:邮箱&手机
        if (salesInvoice.getGsiBillPush() == null || salesInvoice.getGsiBillPush().equals("N")) {
            invoiceEntity.setTsfs("-1");
        } else {
            invoiceEntity.setTsfs("2");
        }
        // 推送邮箱（tsfs为0或2时，此项为必填）
        invoiceEntity.setEmail(salesInvoice.getGsiBillEmail());
        // 推送手机(开票成功会短信提醒购方)
        invoiceEntity.setPhone(salesInvoice.getGsiBillTel());
        // 清单标志，0:非清单,1:清单，根据项目名称数，自动生成清单;
        invoiceEntity.setQdbz("");
        // 注意：税局要求清单项目名称为（详见销货清单）
        invoiceEntity.setQdxmmc("");
        // 代开标志，0:非代开;1:代开。代开蓝票备注文案要求包含：“代开企业税号:***代开企业名称:***.”；
        // 代开红票备注文案要求：“对应正数发票代码:***号码:***代开企业税号:***代开企业名称:***.”。（代开企业税号与代开企业名称之间仅支持一个空格或无符号）
        invoiceEntity.setDkbz("");
        // 部门门店id（诺诺网系统中的id）
        invoiceEntity.setDeptid("");
        // 开票员id（诺诺网系统中的id）
        invoiceEntity.setClerkid("");
        // 发票种类，p:电子增值税普通发票，c:增值税普通发票(纸票)，s:增值税专用发票，e:收购发票(电子)，f:收购发票(纸质)，r:增值税普通发票(卷式)，b:增值税电子专用发票
        invoiceEntity.setInvoiceLine(salesInvoice.getGsiInvoiceClass().toLowerCase());
        // 成品油标志：0非成品油，1成品油，
        invoiceEntity.setCpybz("");
        // 红字信息表编号
        invoiceEntity.setBillInfoNo("");
        // 明细
        List<InvoiceEntity.InvoiceDetail> invoiceDetailList = new ArrayList<>();
        for (SalesInvoiceDetail salesInvoiceDetail : salesInvoiceDetailList) {
            // 发票明细
            InvoiceEntity invoiceEntityOjb = new InvoiceEntity();
            InvoiceEntity.InvoiceDetail invoiceDetail = invoiceEntityOjb.new InvoiceDetail();
            // 商品名称
            invoiceDetail.setGoodsname(salesInvoiceDetail.getGsiProName());
            // 退库 或 互调退库 或 销退  则为 负数
            if ("TD".equalsIgnoreCase(salesInvoiceDetail.getGsiMatType())
                    || "MD".equalsIgnoreCase(salesInvoiceDetail.getGsiMatType())
                    || "ED".equalsIgnoreCase(salesInvoiceDetail.getGsiMatType())) {
                // 数量
                invoiceDetail.setNum(salesInvoiceDetail.getGsiQty().abs().multiply(BigDecimal.valueOf(-1)).toString());
                // 单价
                invoiceDetail.setPrice(salesInvoiceDetail.getGsiPrice().abs().multiply(BigDecimal.valueOf(-1)).toString());
                // 单价含税标志，0:不含税,1:含税
                invoiceDetail.setHsbz("1");
                // 税率
                invoiceDetail.setTaxrate(salesInvoiceDetail.getGsiAddTax().toString());
            } else {
                // 数量
                invoiceDetail.setNum(salesInvoiceDetail.getGsiQty().abs().toString());
                // 单价
                invoiceDetail.setPrice(salesInvoiceDetail.getGsiPrice().abs().toString());
                // 单价含税标志，0:不含税,1:含税
                invoiceDetail.setHsbz("1");
                // 税率
                invoiceDetail.setTaxrate(salesInvoiceDetail.getGsiAddTax().toString());
            }
            // 规格型号
            invoiceDetail.setSpec(salesInvoiceDetail.getGsiSpecs());
            // 单位
            invoiceDetail.setUnit(salesInvoiceDetail.getGsiUnit());
            // 税收分类编码
            invoiceDetail.setSpbm(salesInvoiceDetail.getGsiTaxClass());
            // 自行编码
            invoiceDetail.setZxbm("");
            // 发票行性质，0:正常行;1:折扣行;2:被折扣行
            invoiceDetail.setFphxz("0");
            // 优惠政策标识,0:不使用;1:使用
            invoiceDetail.setYhzcbs("");
            // 增值税特殊管理，如：即征即退、免税、简易征收等
            invoiceDetail.setZzstsgl("");
            // 零税率标识
            invoiceDetail.setLslbs("");
            // 扣除额
            invoiceDetail.setKce("");
            // 不含税金额
            invoiceDetail.setTaxfreeamt("");
            // 税额
            invoiceDetail.setTax("");
            // 含税金额
            invoiceDetail.setTaxamt("");
            // 0 税率
            if (salesInvoiceDetail.getGsiAddTax().compareTo(BigDecimal.ZERO) == 0) {
                invoiceDetail.setYhzcbs("0");
                invoiceDetail.setZzstsgl("");
                invoiceDetail.setLslbs("3");
            }
            invoiceDetailList.add(invoiceDetail);
        }
        // 电子发票明细
        invoiceEntity.setDetail(invoiceDetailList);

        String failure = InvoiceUtils.reqSend(applicationConfig, invoiceEntity, compadm.getCompadmBillId());
        SalesInvoice entity = new SalesInvoice();
        // 开票成功
        if (!failure.startsWith("failure")) {
            entity.setGsiInvoiceSeq(failure);
            entity.setGsiBillStatus("0");
            entity.setGsiModDate(DateUtils.getCurrentDateStrYYMMDD());
            entity.setGsiModTime(DateUtils.getCurrentTimeStrHHMMSS());
            entity.setGsiModId(user.getUserId());
            salesInvoiceMapper.update(entity, new QueryWrapper<SalesInvoice>()
                    .eq("CLIENT", user.getClient()).eq("GSI_ORDER_ID", gsiOrderId));
        } else {
            throw new CustomResultException(failure);
        }
        return ResultUtil.success();
    }

    /**
     * 自动开票
     *
     * @param autoInvoice
     * @param key
     * @param seq
     */
    private void invoiceAuto(AutoInvoice autoInvoice, String key, Integer seq, List<InvoiceEntity> invoiceEntityList, Compadm compadm,
                             String cusName, String cusNo, String add, String bankAccount, String bankName, String tel) {
        TokenUser user = commonService.getLoginInfo();
        List<SalesInvoicePageVO> list = autoInvoice.getList();
        if (seq != null) {
            seq += 1;
        } else {
            seq = 1;
        }
        while (redisClient.exists(SALESINVOICESERVICEIMPL_ORDERID + String.valueOf(seq.intValue()))) {
            seq++;
        }
        redisClient.set(SALESINVOICESERVICEIMPL_ORDERID + String.valueOf(seq.intValue()), seq.toString(), 120);
        double sum = list.stream().mapToDouble(t -> {
            if (t.getTotalAmount() != null) {
                // 退库 或 互调退库 或 销退  则为 负数
                if ("TD".equalsIgnoreCase(t.getMatType()) || "MD".equalsIgnoreCase(t.getMatType()) || "ED".equalsIgnoreCase(t.getMatType())) {
                    return t.getTotalAmount().abs().multiply(new BigDecimal(-1)).doubleValue();
                }
                return t.getTotalAmount().doubleValue();
            } else {
                return BigDecimal.ZERO.doubleValue();
            }
        }).sum();
        String orderId = DateUtils.getCurrentDateStrYYMMDD() + StringUtils.leftPad(String.valueOf(seq), 4, "0");
        SalesInvoice salesInvoice = new SalesInvoice();
        salesInvoice.setClient(user.getClient());
        salesInvoice.setGsiOrderId(orderId);
        salesInvoice.setGsiSite(list.get(0).getMatSiteCode());
        salesInvoice.setGsiCustomer(list.get(0).getStoCode());
        salesInvoice.setGsiAmount(BigDecimal.valueOf(sum));
        salesInvoice.setGsiUser(user.getUserId());
        salesInvoice.setGsiStatus("0");
        salesInvoice.setGsiExcelPath(key);
        salesInvoice.setGsiCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 发票号码
        // 开票状态 0:开票中，1：成功　 2 开票失败
        salesInvoice.setGsiBillStatus("0");
        // 发票类型　1：原开票，2：自动开票
        salesInvoice.setGsiBillType("2");
        // 开票人
        salesInvoice.setGsiBillUser(autoInvoice.getGsiInvoiceUserId());
        // 收款人
        salesInvoice.setGsiReceiverUser(autoInvoice.getGsiReceiverUser());
        // 复审人
        salesInvoice.setGsiCheckUser(autoInvoice.getGsiCheckUser());
        // 电话号码
        salesInvoice.setGsiBillTel(autoInvoice.getGsiInvoicePhone());
        // 邮箱
        salesInvoice.setGsiBillEmail(autoInvoice.getGsiInvoiceEmail());
        // 是否推送 Y：是 N：否
        salesInvoice.setGsiBillPush("1".equals(autoInvoice.getGsiBillPush()) ? "Y" : "N");
        // 备注
        salesInvoice.setGsiBillRemark(autoInvoice.getGsiInvoiceNote());
        // 创建人
        salesInvoice.setGsiCreateId(user.getUserId());
        // 创建时间
        salesInvoice.setGsiCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        // 修改人
        // 修改日期
        // 修改时间
        // 失败原因
        // 发票种类　p:电子增值税普通发票，c:增值税普通发票(纸票)，s:增值税专用发票，e:收购发票(电子)，f:收购发票(纸质)，r:增值税普通发票(卷式)，b:增值税电子专用发票
        salesInvoice.setGsiInvoiceClass(autoInvoice.getGsiInvoiceClass().toLowerCase());
        // 开票结果
        // 发票PDF
        // 开票流水号
        String orderNo = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
        salesInvoice.setGsiInvoiceSeq(orderNo);
        salesInvoice.setGsiInvoiceIdentity(compadm.getCompadmBillId());
        salesInvoiceMapper.insert(salesInvoice);

        // 开票
        InvoiceEntity invoiceEntity = new InvoiceEntity();
        invoiceEntity.setOrderId(orderNo);
        // 购方企业名称
        invoiceEntity.setBuyername(cusName);
        // 购方企业税号
        invoiceEntity.setTaxnum(cusNo);
        // 购方企业地址
        invoiceEntity.setAddress(add);
        // 购方企业银行开户行及账号
        invoiceEntity.setAccount((StringUtils.isBlank(bankName) ? "" : bankName) + " " + (StringUtils.isBlank(bankAccount) ? "" : bankAccount));
        // 购方企业电话
        invoiceEntity.setTelephone(tel);
        // 订单号
        invoiceEntity.setOrderno(orderNo);
        // 回调
        invoiceEntity.setCallbackurl(applicationConfig.getInvoiceCallBack());
        // 当前提交时间
        invoiceEntity.setInvoicedate(DateUtils.getCurrentDateTimeStr());
        // 销方企业税号
        invoiceEntity.setSaletaxnum(compadm.getCompadmNo());
        invoiceEntity.setSaleaccount("");
        invoiceEntity.setSalephone("");
        invoiceEntity.setSaleaddress("");
        // 发票类型，1:正票;2：红票
        invoiceEntity.setKptype("1");
        invoiceEntity.setMessage("");
        // 开票员
        invoiceEntity.setClerk(autoInvoice.getGsiInvoiceUserName());
        // 收款人
        invoiceEntity.setPayee(autoInvoice.getGsiReceiverUserName());
        // 复核人
        invoiceEntity.setChecker(autoInvoice.getGsiCheckUserName());
        // 对应蓝票发票代码
        invoiceEntity.setFpdm("");
        // 对应蓝票发票号码
        invoiceEntity.setFphm("");
        // 推送方式，-1:不推送;0:邮箱;1:手机(默认);2:邮箱&手机
        if (autoInvoice.getGsiInvoicePush() == null || autoInvoice.getGsiInvoicePush().intValue() == 0) {
            invoiceEntity.setTsfs("-1");
        } else {
            invoiceEntity.setTsfs("2");
        }
        // 推送邮箱（tsfs为0或2时，此项为必填）
        invoiceEntity.setEmail(autoInvoice.getGsiInvoiceEmail());
        // 推送手机(开票成功会短信提醒购方)
        invoiceEntity.setPhone(autoInvoice.getGsiInvoicePhone());
        // 清单标志，0:非清单,1:清单，根据项目名称数，自动生成清单;
        invoiceEntity.setQdbz("");
        // 注意：税局要求清单项目名称为（详见销货清单）
        invoiceEntity.setQdxmmc("");
        // 代开标志，0:非代开;1:代开。代开蓝票备注文案要求包含：“代开企业税号:***代开企业名称:***.”；代开红票备注文案要求：“对应正数发票代码:***号码:***代开企业税号:***代开企业名称:***.”。（代开企业税号与代开企业名称之间仅支持一个空格或无符号）
        invoiceEntity.setDkbz("");
        // 部门门店id（诺诺网系统中的id）
        invoiceEntity.setDeptid("");
        // 开票员id（诺诺网系统中的id）
        invoiceEntity.setClerkid("");
        // 发票种类，p:电子增值税普通发票，c:增值税普通发票(纸票)，s:增值税专用发票，e:收购发票(电子)，f:收购发票(纸质)，r:增值税普通发票(卷式)，b:增值税电子专用发票
        invoiceEntity.setInvoiceLine(autoInvoice.getGsiInvoiceClass().toLowerCase());
        // 成品油标志：0非成品油，1成品油，
        invoiceEntity.setCpybz("");
        // 红字信息表编号
        invoiceEntity.setBillInfoNo("");
        List<InvoiceEntity.InvoiceDetail> invoiceDetailList = new ArrayList<>();
        List<SalesInvoiceDetail> detailList = new ArrayList<>();
        AtomicInteger index = new AtomicInteger(0);
        for (SalesInvoicePageVO vo : list) {
            SalesInvoiceDetail detail = new SalesInvoiceDetail();
            detail.setClient(user.getClient());
            detail.setGsiLineNo(vo.getMatLineNo());
            detail.setGsiOrderId(orderId);
            detail.setGsiDnId(vo.getMatDnId());
            detail.setGsiSite(vo.getMatSiteCode());
            detail.setGsiCustomer(vo.getStoCode());
            detail.setGsiProId(vo.getMatProCode());
            detail.setGsiTax(vo.getRateName());
            detail.setGsiSpecs(vo.getProSpecs());
            detail.setGsiUnit(vo.getProUnit());
            detail.setGsiPrice(vo.getPrice());
            detail.setGsiProName(vo.getProName());
            detail.setGsiMatType(vo.getMatType());
            // 退库 或 互调退库 或 销退  则为 负数
            if ("TD".equalsIgnoreCase(vo.getMatType()) || "MD".equalsIgnoreCase(vo.getMatType()) || "ED".equalsIgnoreCase(vo.getMatType())) {
                if (vo.getMatQty() != null) {
                    detail.setGsiQty(vo.getMatQty().multiply(new BigDecimal(-1)));
                }
                if (vo.getTotalAmount() != null) {
                    detail.setGsiAmount(vo.getTotalAmount().multiply(new BigDecimal(-1)));
                }
            } else {
                detail.setGsiQty(vo.getMatQty());
                detail.setGsiAmount(vo.getTotalAmount());
            }
            detail.setGsiTaxClass(vo.getProTaxClass());
            detail.setGsiAddTax(vo.getAddTax());
            detail.setGsiIndex(index.incrementAndGet());
            detailList.add(detail);

            // 发票明细
            InvoiceEntity invoiceEntityOjb = new InvoiceEntity();
            InvoiceEntity.InvoiceDetail invoiceDetail = invoiceEntityOjb.new InvoiceDetail();
            // 商品名称
            invoiceDetail.setGoodsname(vo.getProName());
            // 退库 或 互调退库 或 销退  则为 负数
            if ("TD".equalsIgnoreCase(vo.getMatType()) || "MD".equalsIgnoreCase(vo.getMatType()) || "ED".equalsIgnoreCase(vo.getMatType())) {
                // 数量
                invoiceDetail.setNum(vo.getMatQty().abs().multiply(BigDecimal.valueOf(-1)).toString());
                // 单价
                invoiceDetail.setPrice(vo.getPrice().abs().multiply(BigDecimal.valueOf(-1)).toString());
                // 单价含税标志，0:不含税,1:含税
                invoiceDetail.setHsbz("1");
                // 税率
                invoiceDetail.setTaxrate(vo.getRate().toString());
            } else {
                // 数量
                invoiceDetail.setNum(vo.getMatQty().abs().toString());
                // 单价
                invoiceDetail.setPrice(vo.getPrice().abs().toString());
                // 单价含税标志，0:不含税,1:含税
                invoiceDetail.setHsbz("1");
                // 税率
                invoiceDetail.setTaxrate(vo.getRate().toString());
            }
            // 规格型号
            invoiceDetail.setSpec(vo.getProSpecs());
            // 单位
            invoiceDetail.setUnit(vo.getProUnit());
            // 税收分类编码
            invoiceDetail.setSpbm(vo.getProTaxClass());
            // 自行编码
            invoiceDetail.setZxbm("");
            // 发票行性质，0:正常行;1:折扣行;2:被折扣行
            invoiceDetail.setFphxz("0");
            // 优惠政策标识,0:不使用;1:使用
            invoiceDetail.setYhzcbs("");
            // 增值税特殊管理，如：即征即退、免税、简易征收等
            invoiceDetail.setZzstsgl("");
            // 零税率标识
            invoiceDetail.setLslbs("");
            // 扣除额
            invoiceDetail.setKce("");
            // 不含税金额
            invoiceDetail.setTaxfreeamt("");
            // 税额
            invoiceDetail.setTax("");
            // 含税金额
            invoiceDetail.setTaxamt("");
            // 0 税率
            if (vo.getRate().compareTo(BigDecimal.ZERO) == 0) {
                invoiceDetail.setYhzcbs("0");
                invoiceDetail.setZzstsgl("");
                invoiceDetail.setLslbs("3");
            }
            invoiceDetailList.add(invoiceDetail);
        }
        // 电子发票明细
        invoiceEntity.setDetail(invoiceDetailList);
        invoiceEntityList.add(invoiceEntity);
        salesInvoiceDetailService.saveBatch(detailList);
    }


}
