package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.*;
import com.gov.operate.entity.SdMarketing;
import com.gov.operate.entity.SdMarketingAnalysis;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
public interface ISdMarketingService extends SuperService<SdMarketing> {

    /**
     * 营销设置列表
     */
    IPage<GetGsmSettingListDTO> getGsmSettingList(GetGsmSettingListVO vo);

    /**
     * 营销设置详情
     */
    GetGsmSettingDetailDTO getGsmSettingDetail(String gsmMarketid);

    /**
     * 营销设置保存
     */
    void saveGsmSetting(SaveGsmSettingVO vo);

    /**
     * 营销设置编辑
     */
    void editGsmSetting(EditGsmSettingVO vo);

    /**
     * 活动方案推送
     */
    void pushGsm(PushGsmVO vo);

    /**
     * 营销活动删除
     */
    void deleteMarketSetting(String gsmMarketid);

    /**
     * 营销活动主题分析
     */
    SdMarketingAnalysis getStandardValue();


}
