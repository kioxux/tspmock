package com.gov.operate.service.impl;

import com.gov.operate.entity.SdPromHyrPrice;
import com.gov.operate.mapper.SdPromHyrPriceMapper;
import com.gov.operate.service.ISdPromHyrPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-14
 */
@Service
public class SdPromHyrPriceServiceImpl extends ServiceImpl<SdPromHyrPriceMapper, SdPromHyrPrice> implements ISdPromHyrPriceService {

}
