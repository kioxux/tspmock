package com.gov.operate.controller.ssp;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.operate.dto.ssp.SspLogPageDTO;
import com.gov.operate.service.ssp.ISspLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "流向日志")
@RestController
@RequestMapping("ssp/log")
public class SspLogController {

    @Resource
    private ISspLogService sspLogService;

    @Log("流向日志列表")
    @PostMapping("list")
    @ApiOperation(value = "流向日志列表")
    public Result userList(@Valid @RequestBody SspLogPageDTO sspLogPageDTO) {
        return sspLogService.selectLogByPage(sspLogPageDTO);
    }

}
