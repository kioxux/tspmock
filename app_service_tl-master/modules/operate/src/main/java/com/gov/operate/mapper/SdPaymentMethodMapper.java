package com.gov.operate.mapper;

import com.gov.operate.entity.SdPaymentMethod;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
public interface SdPaymentMethodMapper extends BaseMapper<SdPaymentMethod> {

}
