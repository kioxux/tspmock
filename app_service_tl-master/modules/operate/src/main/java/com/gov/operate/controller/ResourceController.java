package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-11-17
 */
@RestController
@RequestMapping("/operate/resource")
public class ResourceController {

}

