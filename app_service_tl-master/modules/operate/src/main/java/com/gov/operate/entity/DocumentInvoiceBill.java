package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DOCUMENT_INVOICE_BILL")
@ApiModel(value="DocumentInvoiceBill对象", description="")
public class DocumentInvoiceBill extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "业务类型")
    @TableField("GDIB_BUSINESS_TYPE")
    private String gdibBusinessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("GDIB_MAT_DN_ID")
    private String gdibMatDnId;

    @ApiModelProperty(value = "发票号")
    @TableField("GDIB_INVOICE_NUM")
    private String gdibInvoiceNum;


}
