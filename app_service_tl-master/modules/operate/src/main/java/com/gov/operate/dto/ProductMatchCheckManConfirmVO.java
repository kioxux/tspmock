package com.gov.operate.dto;

import com.gov.operate.feign.dto.MatchSchedule;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductMatchCheckManConfirmVO implements Serializable {

    private static final long serialVersionUID = -7066364468395366414L;

    private String companyName;

    private String stoCode;

    private MatchSchedule statics;

    private List<String> demos;

    private Integer stoNum;

    private String averageCheckNum;

    private boolean clientFlag;//true为加盟商 false为门店

    private String productCheckTime;//系统参数设置中的校验任务时长

    private String matchDataCheckEndDate;//任务完成时间

    private String checkManTel;

    private String checkManName;

}
