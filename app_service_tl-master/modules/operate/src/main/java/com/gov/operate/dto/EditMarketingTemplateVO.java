package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditMarketingTemplateVO {

    @NotBlank(message = "模版id不能为空")
    private String smsId;

    @NotBlank(message = "模板不能为空")
    private String smsTheme;

    /**
     * 内容
     */
    private String smsContent;

}
