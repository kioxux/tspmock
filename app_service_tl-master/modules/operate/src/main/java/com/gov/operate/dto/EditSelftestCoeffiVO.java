package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditSelftestCoeffiVO {

    @NotNull(message = "系数类型不能为空")
    private Integer gsscType;

    @NotNull(message = "基本工资不能为空")
    private BigDecimal gsscBasic;

    @NotNull(message = "岗位工资不能为空")
    private BigDecimal gsscPost;

    @NotNull(message = "绩效工资不能为空")
    private BigDecimal gsscPerformance;

    @NotNull(message = "考评工资不能为空")
    private BigDecimal gsscEvaluation;

}
