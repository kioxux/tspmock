package com.gov.operate.dto;

import lombok.Data;

@Data
public class MemberDistributionVO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 标签ID
     */
    private String gstlId;

    /**
     * 标签名称
     */
    private String gstlName;

    /**
     * 标签总数
     */
    private Integer tagTotal;

    /**
     * 颜色
     */
    private String gstlColor;



}
