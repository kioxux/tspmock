package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.operate.entity.SdPaymentMethod;
import com.gov.operate.entity.SdRechargeCardSet;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.SdPaymentMethodMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdPaymentMethodService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-10-10
 */
@Service
public class SdPaymentMethodServiceImpl extends ServiceImpl<SdPaymentMethodMapper, SdPaymentMethod> implements ISdPaymentMethodService {

    @Resource
    private CommonService commonService;

    @Resource
    private SdPaymentMethodMapper sdPaymentMethodMapper;


    /**
     * 获取支付方式
     */
    @Override
    public List<SdPaymentMethod> getPayWay() {

        //根据当前用户的加盟商和门店来查看数据是否存在
        TokenUser user = commonService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        List<SdPaymentMethod> list = sdPaymentMethodMapper.selectList(new QueryWrapper<SdPaymentMethod>()
                .eq("CLIENT", user.getClient())
                .eq("GSPM_BR_ID", user.getDepId())
                .eq("GSPM_RECHARGE", 1));
        if (!list.isEmpty()) {
            list.forEach(item -> {
                item.setGspmRemark("");
            });
        }
        return list;

    }

}
