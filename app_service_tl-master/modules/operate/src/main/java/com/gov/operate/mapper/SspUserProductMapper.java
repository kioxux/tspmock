package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.ssp.SspUserProductPageDTO;
import com.gov.operate.dto.ssp.SspUserProductPageVO;
import com.gov.operate.entity.SspUserProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @Entity generator.domain.SspUserProduct
 */
public interface SspUserProductMapper extends BaseMapper<SspUserProduct> {

    IPage<SspUserProduct> selectSupplierProductByPage(Page<SspUserProduct> page,
                                                            @Param("ew") SspUserProductPageDTO sspUserProductPageDTO);

    IPage<SspUserProduct> selectUserProductByPage(Page<SspUserProduct> page,@Param("ew") SspUserProductPageDTO sspUserProductPageDTO);


    int deleteChainHead(@Param("userId") Long userId,@Param("client") String client,@Param("keySet") Set<String> keySet);


    List<SspUserProduct> selectProductByChain(@Param("client") String client,@Param("chainHeadList") List<String> chainHeadList);
}




