package com.gov.operate.mapper;

import com.gov.operate.entity.Compadm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface CompadmMapper extends BaseMapper<Compadm> {

    List<String> selectListForAuth(@Param("client") String client);
}
