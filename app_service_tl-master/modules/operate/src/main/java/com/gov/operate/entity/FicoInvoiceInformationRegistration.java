package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FICO_INVOICE_INFORMATION_REGISTRATION")
@ApiModel(value = "FicoInvoiceInformationRegistration对象", description = "")
public class FicoInvoiceInformationRegistration extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "发票号码")
    @TableField("INVOICE_NUM")
    private String invoiceNum;

    @ApiModelProperty(value = "采购地点")
    @TableField("SITE_CODE")
    private String siteCode;

    @ApiModelProperty(value = "供应商编码")
    @TableField("SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "发票类型")
    @TableField("INVOICE_TYPE")
    private String invoiceType;

    @ApiModelProperty(value = "发票日期")
    @TableField("INVOICE_DATE")
    private String invoiceDate;

    @ApiModelProperty(value = "税率")
    @TableField("INVOICE_RATE")
    private BigDecimal invoiceRate;

    @ApiModelProperty(value = "发票去税金额")
    @TableField("INVOICE_AMOUNT_EXCLUDING_TAX")
    private BigDecimal invoiceAmountExcludingTax;

    @ApiModelProperty(value = "发票税额")
    @TableField("INVOICE_TAX")
    private BigDecimal invoiceTax;

    @ApiModelProperty(value = "发票总额")
    @TableField("INVOICE_TOTAL_INVOICE_AMOUNT")
    private BigDecimal invoiceTotalInvoiceAmount;

    @ApiModelProperty(value = "已登记金额")
    @TableField("INVOICE_REGISTERED_AMOUNT")
    private BigDecimal invoiceRegisteredAmount;

    @ApiModelProperty(value = "已付款金额")
    @TableField("INVOICE_PAID_AMOUNT")
    private BigDecimal invoicePaidAmount;

    @ApiModelProperty(value = "发票确认金额")
    @TableField("INVOICE_CONFIRMATION_AMOUNT")
    private BigDecimal invoiceConfirmationAmount;

    @ApiModelProperty(value = "登记状态")
    @TableField("INVOICE_REGISTRATION_STATUS")
    private String invoiceRegistrationStatus;

    @ApiModelProperty(value = "付款状态")
    @TableField("INVOICE_PAYMENT_STATUS")
    private String invoicePaymentStatus;

    @ApiModelProperty(value = "创建日期")
    @TableField("INVOICE_CREATION_DATE")
    private String invoiceCreationDate;

    @ApiModelProperty(value = "创建人")
    @TableField("INVOICE_FOUNDER")
    private String invoiceFounder;

    @ApiModelProperty(value = "发票地点类型（1:单体 2.连锁）")
    @TableField("INVOICE_SITE_TYPE")
    private String invoiceSiteType;

    @ApiModelProperty(value = "供应商")
    @TableField("INVOICE_SUP_NAME")
    private String invoiceSupName;

    @ApiModelProperty(value = "折扣")
    @TableField(value = "INVOICE_DISCOUNT", updateStrategy = FieldStrategy.IGNORED)
    private BigDecimal invoiceDiscount;

    @ApiModelProperty(value = "业务员")
    @TableField("INVOICE_SALESMAN")
    private String invoiceSalesman;

    @ApiModelProperty(value = "备注")
    @TableField("INVOICE_REMARK")
    private String invoiceRemark;
}
