package com.gov.operate.service;

import com.gov.operate.dto.GsmTaskSaveVo;
import com.gov.operate.dto.SendMarketingSmsVO;
import com.gov.operate.entity.StoreData;
import com.gov.operate.feign.dto.TokenUser;

import java.util.List;

public interface CustomMultiThreadingService {

    void sendMarketingSms(SendMarketingSmsVO vo, TokenUser user);

    /**
     * 营销任务立即执行 发送短信到门店
     * @param storeList     门店列表
     * @param gsmTaskSaveVo 营销活动
     */
    void sendMarketTaskSms(List<StoreData> storeList, GsmTaskSaveVo gsmTaskSaveVo);
}
