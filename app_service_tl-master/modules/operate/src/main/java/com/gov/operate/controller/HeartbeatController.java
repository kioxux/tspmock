package com.gov.operate.controller;

import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.19
 */
@RestController
public class HeartbeatController {
    @GetMapping({"/heartbeat"})
    public Result heartbeat() {
        return ResultUtil.success("Alive");
    }
}
