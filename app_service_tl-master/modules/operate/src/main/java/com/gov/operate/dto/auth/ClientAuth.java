package com.gov.operate.dto.auth;

import com.gov.operate.entity.AuthorityAuths;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.28
 */
@Data
public class ClientAuth extends AuthorityAuths {

    /**
     * 公共权限禁选
     */
    private boolean disabled;

    /**
     * 加盟商权限ID
     */
    private Integer authClientId;

    /**
     * 子级权限
     */
    private List<ClientAuth> child;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 权限ID集合
     */
    private List<Integer> authList;
}

