package com.gov.operate.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author staxc
 * @Date 2020/10/15 11:11
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ListCleanDrawerVO extends Pageable {

    /**
     * 清斗单号
     */
    private String gschVoucherId;

    /**
     * 商品编码
     */
    private String gscdProId;

    /**
     * 起始日期
     */
    private String beginDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 复核人员
     */
    private String gschEmp1;

    /**
     * 门店列表
     */
    private String[] siteCodes;
}
