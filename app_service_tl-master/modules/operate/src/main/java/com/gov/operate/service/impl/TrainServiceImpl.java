package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.DateUtils;
import com.gov.operate.entity.Train;
import com.gov.operate.mapper.TrainMapper;
import com.gov.operate.service.ITrainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-26
 */
@Service
public class TrainServiceImpl extends ServiceImpl<TrainMapper, Train> implements ITrainService {

    /**
     * 根据 GT_PUSH[是否推送(0:未推送)] + GT_PLAN_PUSH_TIME[计划推送时间,当前小时] -->
     * 修改 GT_PUSH[是否推送(1:已推送)] + GT_PUSH_TIME[推送时间]
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void trainPush() {
        // 当前时间 yyyyMMddHH
        String localDateTime = DateUtils.formatLocalDateTime(LocalDateTime.now(), "yyyyMMddHH");
        // 查询
        List<Train> trainList = this.list(new QueryWrapper<Train>()
                .select("GT_ID")
                .eq("GT_PUSH", 0)
                .eq("GT_PLAN_PUSH_TIME", localDateTime));

        List<Train> trainDOList = trainList.stream().filter(Objects::nonNull).map(item -> {
            Train trainDO = new Train();
            // id
            trainDO.setGtId(item.getGtId());
            // 是否推送(1:已推送)
            trainDO.setGtPush(1);
            // 推送时间
            trainDO.setGtPushTime(DateUtils.getCurrentDateTimeStrTwo());
            return trainDO;
        }).collect(Collectors.toList());
        // 更新
        if (!CollectionUtils.isEmpty(trainDOList)) {
            this.updateBatchById(trainDOList);
        }

    }
}
