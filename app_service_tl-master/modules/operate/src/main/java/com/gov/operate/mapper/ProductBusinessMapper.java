package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.common.entity.dataImport.HyAndHyrPrice;
import com.gov.operate.dto.*;
import com.gov.operate.dto.dataImport.GetProByProCodeAndStoDTO;
import com.gov.operate.entity.ProductBusiness;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface ProductBusinessMapper extends BaseMapper<ProductBusiness> {

    /**
     * 商品组添加查询列表
     */
    IPage<ProductBusinessDTO> getProListForSaveGroup(Page page, @Param("ew") QueryWrapper ew);

    /**
     * 清斗商品查询
     *
     * @param searchContent
     * @param client
     * @param deptId
     * @return
     */
    IPage<GetProductDTO> getCleanDrawderProductList(Page page, @Param("searchContent") String searchContent,
                                                   @Param("client") String client,
                                                   @Param("deptId") String deptId);

    /**
     * 获取清斗单表格信息根据商品自编码
     *
     * @param gsedProId
     * @param client
     * @param site
     * @return
     */
    GetFormDTO getFormInfo(@Param("client") String client,
                           @Param("site") String site,
                           @Param("gsedProId") String gsedProId);

    /**
     * 会员促销_通过导入数据查询商品
     * @param client 加盟商
     * @param dataList 门店+商品列表
     * @return
     */
    List<GetProByProCodeAndStoDTO> getProByProCodeAndStoList(@Param("client") String client, @Param("dataList") List<HyAndHyrPrice> dataList);

    /**
     * 添加促销商品查询
     * @param dto
     * @return
     */
    IPage<SdPromGroupProVo> getProInfoList(Page<SdPromGroupProVo> page,SdPromGroupProDto dto);

    ProductBusiness getProductInfoList(@Param("client") String client,@Param("proSelfCode") String proSelfCode);

    List<String> getDcListByClient(@Param("client") String client);

    IPage<ProductInfoVo> getProductInfo(Page<ProductInfoVo> page, @Param("client") String client, @Param("dcList") List<String> dcList, @Param("content") String content);

    List<String> getProBrandClass(@Param("client") String client);
}
