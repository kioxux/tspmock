package com.gov.operate.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StoreSalesStatisticsRequest {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店")
    private String store;

    @ApiModelProperty(value = "0：日 1：周 2：月")
    private Integer dateType;


}
