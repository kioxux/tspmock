package com.gov.operate.mapper;

import com.gov.operate.entity.SdTagList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-17
 */
public interface SdTagListMapper extends BaseMapper<SdTagList> {

    /**
     * 根据加盟商+会员卡获得标签列表
     */
    List<String> getTagListByCardId(@Param("client") String client, @Param("gmbCardId") String gmbCardId);
}
