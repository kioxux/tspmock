package com.gov.operate.service;
import com.gov.mybatis.SuperService;
import com.gov.operate.entity.GaiaSdRecipelList;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface RecipelService extends SuperService<GaiaSdRecipelList> {


}
