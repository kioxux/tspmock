package com.gov.operate.service.dataImport.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.EnumUtils;
import com.gov.common.entity.dataImport.ImportDto;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.entity.ProductBusiness;
import com.gov.operate.mapper.ProductBusinessMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.dataImport.BusinessImportService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class BusinessImportServiceImpl implements BusinessImportService {

    @Resource
    private MarketingComponentImport marketingComponentImport;
    @Resource
    private PromUnitaryProductImport promUnitaryProductImport;
    @Resource
    private PromSeriesProductImport promSeriesProductImport;
    @Resource
    private PromGiftCondsProductImport promGiftCondsProductImport;
    @Resource
    private PromGiftResultProductImport promGiftResultProductImport;
    @Resource
    private PromAssoCondsProductImport promAssoCondsProductImport;
    @Resource
    private PromAssoResultProductImport promAssoResultProductImport;
    @Resource
    private MaterialImport materialImport;
    @Resource
    private HyAndHyrPriceImport hyAndHyrPriceImport;
    @Resource
    private PromGroupProImport promGroupProImport;
    @Resource
    private CommonService commonService;
    @Resource
    private ProductBusinessMapper productBusinessMapper;
    @Resource
    private InvoiceInformationRegistrationImport invoiceInformationRegistrationImport;
    /**
     * 业务数据导入
     *
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result businessImport(ImportDto dto) {
        // 业务不存在
        if (!EnumUtils.contains(dto.getType(), CommonEnum.BusinessImportTypet.class)) {
            throw new CustomResultException("导入业务不存在");
        }
        // 调价
        if (CommonEnum.BusinessImportTypet.MARKETINGCOMPONENT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return marketingComponentImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 单品促销商品
        if (CommonEnum.BusinessImportTypet.PROMUNITARYPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promUnitaryProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 系列促销商品
        if (CommonEnum.BusinessImportTypet.PROMSERIESPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promSeriesProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 促销赠品条件商品
        if (CommonEnum.BusinessImportTypet.PROMGIFTCONDSPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promGiftCondsProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 促销赠品赠送商品
        if (CommonEnum.BusinessImportTypet.PROMGIFTRESULTPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promGiftResultProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 促销组合条件商品
        if (CommonEnum.BusinessImportTypet.PROMASSOCONDSPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promAssoCondsProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 促销组合赠送商品
        if (CommonEnum.BusinessImportTypet.PROMASSORESULTPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promAssoResultProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 物料凭证导入
        if (CommonEnum.BusinessImportTypet.MaterialDoc.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return materialImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 会员特价/会员日特价导入
        if (CommonEnum.BusinessImportTypet.HYANDHYRPRICE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return hyAndHyrPriceImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 促销商品组导入
        if (CommonEnum.BusinessImportTypet.SD_PROM_GROUP_PRO.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return promGroupProImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 发票导入
        if (CommonEnum.BusinessImportTypet.INVOICE_INFORMATION_REGISTRATION.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return invoiceInformationRegistrationImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        return ResultUtil.success();
    }

    @Override
    public List<ProductBusiness> importProductInfo(MultipartFile file) {
        String client = commonService.getLoginInfo().getClient();
        List<ProductBusiness> list = new ArrayList<>();
        try {
            InputStream in = file.getInputStream();
            XSSFWorkbook wb = new XSSFWorkbook(in);
            Sheet sheet = wb.getSheetAt(0);
            list = getRowAndCell(sheet);
            in.close();
            wb.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<ProductBusiness> proList = new ArrayList<>();
        for (ProductBusiness pro : list){
            ProductBusiness productBusiness;
            productBusiness = productBusinessMapper.getProductInfoList(client,pro.getProSelfCode());
            if (ObjectUtil.isNotEmpty(productBusiness)) {
                pro.setProName(productBusiness.getProName());
                pro.setProSpecs(productBusiness.getProSpecs());
                pro.setProUnit(productBusiness.getProUnit());
                pro.setProFactoryName(productBusiness.getProFactoryName());
            }
            if (!proList.contains(pro)){
                proList.add(pro);
            }
        }
        return proList;
    }

    public List<ProductBusiness> getRowAndCell(Sheet sheet) {
        List<ProductBusiness> productBusinesses = new ArrayList<>();
        String client = commonService.getLoginInfo().getClient();
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        if (totalRows > 0) {
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                ProductBusiness business = new ProductBusiness();
                business.setClient(client);
                business.setProSelfCode(String.valueOf(row.getCell(0)));
                productBusinesses.add(business);
            } //行end
        }else{
            throw new CustomResultException("EXCEL导入信息为空");
        }
        return productBusinesses;
    }
}
