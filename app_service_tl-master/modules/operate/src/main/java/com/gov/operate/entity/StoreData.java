package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_STORE_DATA")
@ApiModel(value = "StoreData对象", description = "")
public class StoreData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "门店编码")
    @TableField("STO_CODE")
    private String stoCode;

    @ApiModelProperty(value = "门店名称")
    @TableField("STO_NAME")
    private String stoName;

    @ApiModelProperty(value = "助记码")
    @TableField("STO_PYM")
    private String stoPym;

    @ApiModelProperty(value = "门店简称")
    @TableField("STO_SHORT_NAME")
    private String stoShortName;

    @ApiModelProperty(value = "门店属性")
    @TableField("STO_ATTRIBUTE")
    private String stoAttribute;

    @ApiModelProperty(value = "配送方式")
    @TableField("STO_DELIVERY_MODE")
    private String stoDeliveryMode;

    @ApiModelProperty(value = "关联门店")
    @TableField("STO_RELATION_STORE")
    private String stoRelationStore;

    @ApiModelProperty(value = "门店状态")
    @TableField("STO_STATUS")
    private String stoStatus;

    @ApiModelProperty(value = "经营面积")
    @TableField("STO_AREA")
    private String stoArea;

    @ApiModelProperty(value = "开业日期")
    @TableField("STO_OPEN_DATE")
    private String stoOpenDate;

    @ApiModelProperty(value = "关店日期")
    @TableField("STO_CLOSE_DATE")
    private String stoCloseDate;

    @ApiModelProperty(value = "详细地址")
    @TableField("STO_ADD")
    private String stoAdd;

    @ApiModelProperty(value = "省")
    @TableField("STO_PROVINCE")
    private String stoProvince;

    @ApiModelProperty(value = "城市")
    @TableField("STO_CITY")
    private String stoCity;

    @ApiModelProperty(value = "区/县")
    @TableField("STO_DISTRICT")
    private String stoDistrict;

    @ApiModelProperty(value = "是否医保店")
    @TableField("STO_IF_MEDICALCARE")
    private String stoIfMedicalcare;

    @ApiModelProperty(value = "DTP")
    @TableField("STO_IF_DTP")
    private String stoIfDtp;

    @ApiModelProperty(value = "税分类")
    @TableField("STO_TAX_CLASS")
    private String stoTaxClass;

    @ApiModelProperty(value = "委托配送公司")
    @TableField("STO_DELIVERY_COMPANY")
    private String stoDeliveryCompany;

    @ApiModelProperty(value = "连锁总部")
    @TableField("STO_CHAIN_HEAD")
    private String stoChainHead;

    @ApiModelProperty(value = "纳税主体")
    @TableField("STO_TAX_SUBJECT")
    private String stoTaxSubject;

    @ApiModelProperty(value = "税率")
    @TableField("STO_TAX_RATE")
    private String stoTaxRate;

    @TableField("STO_DC_CODE")
    private String stoDcCode;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("STO_NO")
    private String stoNo;

    @ApiModelProperty(value = "法人")
    @TableField("STO_LEGAL_PERSON")
    private String stoLegalPerson;

    @ApiModelProperty(value = "质量负责人")
    @TableField("STO_QUA")
    private String stoQua;

    @ApiModelProperty(value = "创建日期")
    @TableField("STO_CRE_DATE")
    private String stoCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("STO_CRE_TIME")
    private String stoCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("STO_CRE_ID")
    private String stoCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("STO_MODI_DATE")
    private String stoModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("STO_MODI_TIME")
    private String stoModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("STO_MODI_ID")
    private String stoModiId;

    @ApiModelProperty(value = "LOGO地址")
    @TableField("STO_LOGO")
    private String stoLogo;

    @ApiModelProperty(value = "门店组编码")
    @TableField("STOG_CODE")
    private String stogCode;

    @ApiModelProperty(value = "店长")
    @TableField("STO_LEADER")
    private String stoLeader;

    @ApiModelProperty(value = "开户行账号")
    @TableField("STO_BANK_ACCOUNT")
    private String stoBankAccount;

    @ApiModelProperty(value = "开户行名称")
    @TableField("STO_BANK_NAME")
    private String stoBankName;

    @ApiModelProperty(value = "企业电话")
    @TableField("STO_TELEPHONE")
    private String stoTelephone;

    @ApiModelProperty(value = "医保机构编码")
    @TableField("STO_YBCODE")
    private String stoYbcode;

    /**
     * 是否为门诊 0：否、1：是
     */
    @TableField("IS_OUTPATIENT")
    private String isOutpatient;
}
