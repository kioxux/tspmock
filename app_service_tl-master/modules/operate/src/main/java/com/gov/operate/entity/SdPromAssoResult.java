package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_PROM_ASSO_RESULT")
@ApiModel(value="SdPromAssoResult对象", description="")
public class SdPromAssoResult extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "单号")
    @TableField("GSPAR_VOUCHER_ID")
    private String gsparVoucherId;

    @ApiModelProperty(value = "行号")
    @TableField("GSPAR_SERIAL")
    private String gsparSerial;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSPAR_PRO_ID")
    private String gsparProId;

    @ApiModelProperty(value = "系列编码")
    @TableField("GSPAR_SERIES_ID")
    private String gsparSeriesId;

    @ApiModelProperty(value = "赠品单品价格")
    @TableField("GSPAR_GIFT_PRC")
    private BigDecimal gsparGiftPrc;

    @ApiModelProperty(value = "赠品单品折扣")
    @TableField("GSPAR_GIFT_REBATE")
    private String gsparGiftRebate;


}
