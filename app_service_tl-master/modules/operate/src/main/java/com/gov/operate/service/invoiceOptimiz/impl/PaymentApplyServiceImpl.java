package com.gov.operate.service.invoiceOptimiz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.PaymentApplyVO;
import com.gov.operate.dto.SelectPaymentApplyDTO;
import com.gov.operate.dto.SelectPaymentApplyVO;
import com.gov.operate.dto.invoiceOptimiz.DocumentBillDetailsDTO;
import com.gov.operate.dto.invoiceOptimiz.InvoiceBillDetailsDTO;
import com.gov.operate.entity.BillPaymentApply;
import com.gov.operate.entity.PaymentApply;
import com.gov.operate.entity.PaymentApplyItem;
import com.gov.operate.entity.UserData;
import com.gov.operate.feign.AuthFeign;
import com.gov.operate.feign.dto.ApprovalInfo;
import com.gov.operate.feign.dto.FeignResult;
import com.gov.operate.feign.dto.JsonResult;
import com.gov.operate.mapper.*;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.IFiPaymentApplyItemService;
import com.gov.operate.service.invoiceOptimiz.IPaymentApplyService;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: zhangp
 * @date: 2021.03.30
 */
@Slf4j
@Service
public class PaymentApplyServiceImpl extends ServiceImpl<PaymentApplyMapper, PaymentApply> implements IPaymentApplyService {


    private final static String PAYMENTAPPLY_KEY = "PAYMENTAPPLY";
    @Resource
    private PaymentApplyMapper paymentApplyMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private RedisClient redisClient;
    @Resource
    private IFiPaymentApplyItemService iFiPaymentApplyItemService;
    @Resource
    private DocumentBillMapper documentBillMapper;
    @Resource
    private DocumentBillHMapper documentBillHMapper;
    @Resource
    private InvoiceBillMapper invoiceBillMapper;
    @Resource
    private AuthFeign authFeign;
    @Resource
    private UserDataMapper userDataMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addApply(List<PaymentApplyVO> list) {
        String client = commonService.getLoginInfo().getClient();
        String user = commonService.getLoginInfo().getUserId();
        //获取时间
        String date = DateUtils.getCurrentDateStrYYMMDD();
        String time = DateUtils.getCurrentTimeStrHHMMSS();
        //生产申请号
        Long billNum = paymentApplyMapper.selectBillNum(client);
        while (redisClient.exists(PAYMENTAPPLY_KEY + billNum)) {
            billNum++;
        }
        redisClient.set(PAYMENTAPPLY_KEY + billNum, billNum.toString(), 120);
        // 工作流编码
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        String sqdh = billNum.toString();
        List<PaymentApplyItem> paymentApplyItemList = new ArrayList<PaymentApplyItem>();
        PaymentApply paymentApply = new PaymentApply();
        BigDecimal gbpaTotalAmt = new BigDecimal(0);
        BigDecimal gbpaPayAmt = new BigDecimal(0);
        String supCode = list.get(0).getSupCode();
        String supName = list.get(0).getSupName();
        List<Map<String, Object>> paymentApplicationsList = new ArrayList<>();
        QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
        userDataQueryWrapper.eq("CLIENT", client);
        userDataQueryWrapper.eq("USER_ID", user);
        UserData userData = userDataMapper.selectOne(userDataQueryWrapper);
        String userName = "";
        if (userData != null) {
            userName = userData.getUserNam();
        }
        int index = 0;
        for (PaymentApplyVO item : list) {
            //比较金额
            PaymentApplyItem paymentApplyItem = new PaymentApplyItem();
            BigDecimal paidAmt = paymentApplyMapper.getPaidAmt(client, item.getBillNum());
            if (DecimalUtils.add(item.getThisAmt().abs(), paidAmt.abs()).compareTo(item.getPayAmt().abs()) > 0) {
                throw new CustomResultException("对账单号:" + item.getBillNum() + "付款金额不可以大于应付金额");
            }
            if (!supCode.equals(item.getSupCode())) {
//                throw new CustomResultException("同一批次只能选择同一供应商进行申请");
            }
            //组装明细
            paymentApplyItem.setClient(client);
            paymentApplyItem.setGbpaiNum(item.getBillNum());
            paymentApplyItem.setGbpaiSupCode(item.getSupCode());
            paymentApplyItem.setGbpaiCreateUser(user);
            paymentApplyItem.setGbpaiCreateDate(date);
            paymentApplyItem.setGbpaiCreateTime(time);
            paymentApplyItem.setGbpaiBillAmount(item.getPayAmt());
            paymentApplyItem.setGbpaiPayAmt(item.getThisAmt());
            paymentApplyItem.setGbpaiFlowNo(sqdh);
            paymentApplyItem.setGbpaiRemarks(item.getRemark());
            paymentApplyItem.setGbpaiType(item.getBusinessType());
            paymentApplyItem.setGbpaiBankName(item.getSupBankName());
            paymentApplyItem.setGbpaiBankAccount(item.getSupBankAccount());
            paymentApplyItemList.add(paymentApplyItem);
            gbpaTotalAmt = gbpaTotalAmt.add(item.getPayAmt());
            gbpaPayAmt = gbpaPayAmt.add(item.getThisAmt());

            Map<String, Object> map = new HashMap<>();
            // 序号
            index++;
            map.put("index", index);
            // 加盟商
            map.put("clent", client);
            // 供应商编码
            map.put("supCode", item.getSupCode());
            // 申请付款金额
            map.put("applicationAmount", String.format("%.2f", item.getThisAmt()));
            // 申请人
            map.put("applicant", user);
            // 申请日期
            map.put("dateOfApplication", DateUtils.getCurrentDateStrYYMMDD());
            // 申请时间
            map.put("applicationTime", DateUtils.getCurrentTimeStrHHMMSS());
            // 供应商名称
            map.put("supName", item.getSupName());
            // 申请人姓名
            map.put("applicantUserName", userName);
            // 备注
            map.put("gpaRemarks", item.getRemark());
            // 开户行名称
            map.put("supBankName", item.getSupBankName());
            // 开户行帐号
            map.put("supBankAccount", item.getSupBankAccount());
            paymentApplicationsList.add(map);
        }
        //组装申请
        paymentApply.setClient(client);
        paymentApply.setGbpaTotalAmt(gbpaTotalAmt);
        paymentApply.setGbpaPayAmt(gbpaPayAmt);
        paymentApply.setApprovalUser(user);
        paymentApply.setApprovalStatus(0);
        paymentApply.setApprovalFlowNo(flowNo);
        paymentApply.setGbpaCreateUser(user);
        paymentApply.setGbpaCreateDate(date);
        paymentApply.setGbpaCreateTime(time);
        paymentApply.setGbpaFlowNo(sqdh);

        iFiPaymentApplyItemService.saveBatch(paymentApplyItemList);
        paymentApplyMapper.insert(paymentApply);

        // 提交工作流 GAIA_WF_045
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        param.put("token", token);  // token
        param.put("wfDefineCode", "GAIA_WF_045");  // 类型
        param.put("wfTitle", supCode + "-" + supName + "(" + gbpaPayAmt.setScale(2, BigDecimal.ROUND_HALF_UP) + "元)"); // 标题
        param.put("wfDescription", list.get(0).getRemark());  // 描述
        param.put("wfOrder", flowNo);   // 业务单号
        param.put("wfDetail", paymentApplicationsList); // 明细
        param.put("wfSite", StringUtils.isNotBlank(list.get(0).getSite()) ? list.get(0).getSite() : "-1"); // 门店
        log.info("财务付款申请GAIA_WF_045，提交参数{}", JsonUtils.beanToJson(param));
        JsonResult result = authFeign.createWorkflow(param);
        log.info("财务付款申请GAIA_WF_045！ 返回结果：{}", JsonUtils.beanToJson(result));
        if (result.getCode().intValue() != 0) {
            throw new CustomResultException(result.getMessage());
        }
        return ResultUtil.success();
    }

    /**
     * 付款申请查询
     */
    @Override
    public IPage<SelectPaymentApplyDTO> selectPaymentApply(SelectPaymentApplyVO vo) {
        String client = commonService.getLoginInfo().getClient();
        vo.setClient(client);
        Page page = new Page(vo.getPageNum(), vo.getPageSize());
        if (StringUtils.isNotBlank(vo.getStoCode())) {
            vo.setSite(vo.getStoCode());
        }
        IPage<SelectPaymentApplyDTO> iPage = paymentApplyMapper.selectPaymentApply(page, vo);
        iPage.getRecords().stream().forEach(item -> {
            item.setPaidAmt(paymentApplyMapper.getPaidAmt(client, item.getBillNum()));
        });
        return iPage;
    }

    /**
     * 单据对账详情
     *
     * @param id
     * @return
     */
    @Override
    public DocumentBillDetailsDTO getDocumentBill(String id) {

        String client = commonService.getLoginInfo().getClient();
        if (StringUtils.isBlank(id)) {
            throw new CustomResultException("对账单号不能为空");
        }
        DocumentBillDetailsDTO document = documentBillMapper.getDocumentBill(id, client);
        if (ObjectUtils.isEmpty(document)) {
            throw new CustomResultException("该对账单不存在");
        }
        String gdbNum = document.getGdbNum();
        // 对账单据列表
        List<DocumentBillDetailsDTO.DocumentBillHDTO> documentBillHDTOList = documentBillHMapper.getDocumentBillHList(client, gdbNum);

        documentBillHDTOList.stream().filter(Objects::nonNull).forEach(docHDTO -> {
            // 单据明细列表
            List<DocumentBillDetailsDTO.DocumentBillHDTO.DocumentBillDDTO> docDList = documentBillHMapper.getDocumentBillDList(client, gdbNum, docHDTO.getGdbhBusinessType(), docHDTO.getGdbhMatDnId());
            docHDTO.setDocumentBillDList(docDList);
            // 单据绑定的发票
            List<DocumentBillDetailsDTO.DocumentBillHDTO.DocumentInvoiceBillDTO> documentInvoiceList = documentBillHMapper.getDocumentBillInvoiceList(client, docHDTO.getGdbhBusinessType(), docHDTO.getGdbhMatDnId());
            docHDTO.setDocumentInvoiceList(documentInvoiceList);
        });
        document.setSelectWarehousingDTOList(documentBillHDTOList);
        return document;
    }

    /**
     * 发票对账详情
     *
     * @param id 对账单号
     * @return
     */
    @Override
    public InvoiceBillDetailsDTO getInvoiceBill(String id) {
        String client = commonService.getLoginInfo().getClient();
        if (StringUtils.isBlank(id)) {
            throw new CustomResultException("对账单号不能为空");
        }
        // 发票对账信息
        InvoiceBillDetailsDTO invoiceBill = invoiceBillMapper.getInvoiceBill(client, id);
        if (ObjectUtils.isEmpty(invoiceBill)) {
            throw new CustomResultException("该对账单不存在");
        }
        // 发票对账_发票信息
        List<InvoiceBillDetailsDTO.InvoiceBillInvoiceDTO> invoiceBillInvoiceList = invoiceBillMapper.getInvoiceBillInvoiceList(client, id);
        invoiceBill.setInvoiceBillInvoiceDTOList(invoiceBillInvoiceList);
        // 发票对账_单据信息
        List<InvoiceBillDetailsDTO.InvoiceBillDocDTO> invoiceBillDocList = invoiceBillMapper.getInvoiceBillDocList(client, id);
        invoiceBill.setInvoiceBillDocDTOList(invoiceBillDocList);
        // 发票对账_单据明细
        invoiceBillDocList.forEach(item -> {
            List<InvoiceBillDetailsDTO.InvoiceBillDocDTO.InvoiceBillDocDetailsDTO> invoiceBillDocDetailsList =
                    invoiceBillMapper.getInvoiceBillDocDetailsList(client, id, item.getGibdMatDnId(), item.getGibdBusinessType());
            item.setInvoiceBillDocDetailsDTOList(invoiceBillDocDetailsList);
        });
        return invoiceBill;
    }

    /**
     * 付款审批
     *
     * @param info
     * @return
     */
    @Override
    public FeignResult approvePayment(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        // 审批单据
        List<PaymentApply> paymentApplyList = paymentApplyMapper.selectList(
                new QueryWrapper<PaymentApply>().eq("CLIENT", info.getClientId())
                        .eq("APPROVAL_FLOW_NO", info.getWfOrder())
                        .eq("APPROVAL_STATUS", 0)
        );
        if (CollectionUtils.isEmpty(paymentApplyList)) {
            result.setCode(1001);
            result.setMessage("审批数据不存在");
            return result;
        }
        for (int i = 0; i < paymentApplyList.size(); i++) {
            PaymentApply entity = paymentApplyList.get(i);
            PaymentApply paymentApply = new PaymentApply();
            // 驳回
            if ("1".equals(info.getWfStatus())) {
                paymentApply.setApprovalStatus(2);
            }
            // 通过
            if ("3".equals(info.getWfStatus())) {
                paymentApply.setApprovalStatus(1);
            }
            paymentApplyMapper.update(paymentApply,
                    new QueryWrapper<PaymentApply>().eq("CLIENT", entity.getClient())
                            .eq("GBPA_FLOW_NO", entity.getGbpaFlowNo())
            );
        }
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

}
