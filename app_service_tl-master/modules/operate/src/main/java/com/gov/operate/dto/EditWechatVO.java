package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditWechatVO {

    @NotBlank(message = "对象编码不能为空")
    private String goaCode;

    @NotNull(message = "类型不能为空，1:门店，2：连锁")
    private Integer goaType;

    /**
     * LOGO
     **/
    private String logo;

    /**
     * LOGO绝对地址
     **/
    private String logoUrl;

    /**
     * 特权说明
     **/
    private String memberBenefits;

    /**
     * 有效日期
     **/
    private String accountsValidPeriod;

    /**
     * 使用须知
     **/
    private String accountsUsageNotice;

    /**
     * 电话号码
     **/
    private String accountsTel;

    /**
     * 会员卡ID
     */
    private String memberCardId;

}
