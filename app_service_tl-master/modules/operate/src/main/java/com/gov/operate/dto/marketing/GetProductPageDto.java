package com.gov.operate.dto.marketing;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:48 2021/8/13
 * @Description：查询商品分页信息入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GetProductPageDto {

    private List<String> queryStringList;
    private String queryString;
    @NotNull(message = "分页参数不能为空")
    private Integer pageNum;
    @NotNull(message = "分页参数不能为空")
    private Integer pageSize;
}
