package com.gov.operate.service;

import com.gov.operate.entity.SdPromChmedSet;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 中药类促销设置表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-07-27
 */
public interface ISdPromChmedSetService extends SuperService<SdPromChmedSet> {

}
