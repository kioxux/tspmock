package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_STOCK_BATCH")
@ApiModel(value="SdStockBatch对象", description="")
public class SdStockBatch extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "店号")
    @TableField("GSSB_BR_ID")
    private String gssbBrId;

    @ApiModelProperty(value = "商品编码")
    @TableField("GSSB_PRO_ID")
    private String gssbProId;

    @ApiModelProperty(value = "批号")
    @TableField("GSSB_BATCH_NO")
    private String gssbBatchNo;

    @ApiModelProperty(value = "批次")
    @TableField("GSSB_BATCH")
    private String gssbBatch;

    @ApiModelProperty(value = "数量")
    @TableField("GSSB_QTY")
    private BigDecimal gssbQty;

    @ApiModelProperty(value = "效期")
    @TableField("GSSB_VAILD_DATE")
    private String gssbVaildDate;

    @ApiModelProperty(value = "更新日期")
    @TableField("GSSB_UPDATE_DATE")
    private String gssbUpdateDate;

    @ApiModelProperty(value = "更新人员编号")
    @TableField("GSSB_UPDATE_EMP")
    private String gssbUpdateEmp;


}
