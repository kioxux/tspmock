package com.gov.operate.service.batchPurchaseSales.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.operate.entity.BatchQueryAuth;
import com.gov.operate.entity.BatchQueryAuthData;
import com.gov.operate.mapper.*;
import com.gov.operate.service.batchPurchaseSales.IBatchQueryAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 查询数据权限 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Slf4j
@Service
public class BatchQueryAuthServiceImpl extends ServiceImpl<BatchQueryAuthMapper, BatchQueryAuth> implements IBatchQueryAuthService {

    @Resource
    private BatchQueryAuthDataMapper batchQueryAuthDataMapper;
    @Resource
    private BatchHeadPurchaseMapper batchHeadPurchaseMapper;
    @Resource
    private BatchHeadSalesMapper batchHeadSalesMapper;
    @Resource
    private BatchHeadStockMapper batchHeadStockMapper;
    @Resource
    private BatchStorePurchaseMapper batchStorePurchaseMapper;
    @Resource
    private BatchStoreSalesMapper batchStoreSalesMapper;
    @Resource
    private BatchStoreStockMapper batchStoreStockMapper;

    /**
     * 1123-数据直连需求-DDI
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void ddiHandler() {
        // 当前日期
        String nowDay = DateUtils.getCurrentDateStrYYMMDD();
        // 前一天
        String beforeDay = DateUtils.getBeforeDateStrYYMMDD();
        log.info("总部采购定时任务开始,当前日期{}，前一天{}", nowDay, beforeDay);
        // 查询 数据
        List<BatchQueryAuth> batchQueryAuthList = baseMapper.selectList(new LambdaQueryWrapper<BatchQueryAuth>().gt(BatchQueryAuth::getGbqaEndDate, nowDay));
        if (CollectionUtils.isEmpty(batchQueryAuthList)) {
            log.info("查询数据权限 表无数据");
            return;
        }
        // 查询数据清单
        List<BatchQueryAuthData> batchQueryAuthDataList = batchQueryAuthDataMapper.selectList(new LambdaQueryWrapper<>());
        if (CollectionUtils.isEmpty(batchQueryAuthDataList)) {
            log.info("查询数据清单 表无数据");
            return;
        }
        for (BatchQueryAuth batchQueryAuth : batchQueryAuthList) {
            log.info("查询数据权限{}", JsonUtils.beanToJson(batchQueryAuth));
            // 查询数据清单
            List<BatchQueryAuthData> itemList = batchQueryAuthDataList.stream().filter(t -> t.getClient().equals(batchQueryAuth.getClient()) && t.getGbqadFactory().equals(batchQueryAuth.getGbqaFactory())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(itemList)) {
                log.info("查询数据清单 表无数据");
                continue;
            }
            String exeName = "";
            try {
                // 总部采购
                if (batchQueryAuth.getGbqaHeadPurchase() != null && batchQueryAuth.getGbqaHeadPurchase().intValue() == 1) {
                    exeName = "总部采购";
                    Integer cnt = batchHeadPurchaseMapper.saveDdiList(batchQueryAuth, itemList, beforeDay);
                    log.info("总部采购生成{}", cnt);
                }
                // 总部库存
                if (batchQueryAuth.getGbqaHeadStock() != null && batchQueryAuth.getGbqaHeadStock().intValue() == 1) {
                    exeName = "总部库存";
                    Integer cnt = batchHeadStockMapper.saveDdiList(batchQueryAuth, itemList, beforeDay);
                    log.info("总部库存生成{}", cnt);
                }
                // 总部销售
                if (batchQueryAuth.getGbqaHeadSales() != null && batchQueryAuth.getGbqaHeadSales().intValue() == 1) {
                    exeName = "总部销售";
                    Integer cnt = batchHeadSalesMapper.saveDdiList(batchQueryAuth, itemList, beforeDay);
                    log.info("总部销售生成{}", cnt);
                }
                // 门店购进
                if (batchQueryAuth.getGbqaStorePurchase() != null && batchQueryAuth.getGbqaStorePurchase().intValue() == 1) {
                    exeName = "门店购进";
                    Integer cnt = batchStorePurchaseMapper.saveDdiList(batchQueryAuth, itemList, beforeDay);
                    log.info("门店购进生成{}", cnt);
                }
                // 门店销售
                if (batchQueryAuth.getGbqaStoreSales() != null && batchQueryAuth.getGbqaStoreSales().intValue() == 1) {
                    exeName = "门店销售";
                    Integer cnt = batchStoreSalesMapper.saveDdiList(batchQueryAuth, itemList, beforeDay);
                    log.info("门店销售生成{}", cnt);
                }
                // 门店库存
                if (batchQueryAuth.getGbqaStoreStock() != null && batchQueryAuth.getGbqaStoreStock().intValue() == 1) {
                    exeName = "门店库存";
                    Integer cnt = batchStoreStockMapper.saveDdiList(batchQueryAuth, itemList, beforeDay);
                    log.info("门店库存生成{}", cnt);
                }
            } catch (Exception e) {
                log.info(exeName + "生成失败");
                e.printStackTrace();
                throw e;
            }
        }
        log.info("总部采购定时任务结束");
    }
}
