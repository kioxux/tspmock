package com.gov.operate.service.batchPurchaseSales.impl;

import com.gov.operate.entity.BatchStorePurchase;
import com.gov.operate.mapper.BatchStorePurchaseMapper;
import com.gov.operate.service.batchPurchaseSales.IBatchStorePurchaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店采购 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Service
public class BatchStorePurchaseServiceImpl extends ServiceImpl<BatchStorePurchaseMapper, BatchStorePurchase> implements IBatchStorePurchaseService {

}
