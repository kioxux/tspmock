package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Zhangchi
 * @since 2021/10/14/15:27
 */
@Data
@TableName("GAIA_REGIONAL_STORE")
public class GaiaRegionalStore implements Serializable {
    private static final long serialVersionUID=1L;
    /**
     * 主键ID
     */
    @TableId("ID")
    private Long id;

    /**
     * 加盟商
     */
    @TableField("CLIENT")
    private String client;

    /**
     * 区域编码
     */
    @TableField("REGIONAL_ID")
    private Long regionalId;

    /**
     * 门店编码
     */
    @TableField("STO_CODE")
    private String stoCode;

    /**
     * 门店简称
     */
    @TableField("STO_NAME")
    private String stoName;

    /**
     * 排序编码
     */
    @TableField("SORT_NUM")
    private Integer sortNum;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     *修改时间
     */
    private Date updateTime;

    /**
     * 是否删除：0否，1是
     */
    private String deleteFlag;

}
