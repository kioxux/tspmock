package com.gov.operate.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 电子券异动表 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-07-09
 */
@RestController
@RequestMapping("/operate/sd-electron-change")
public class SdElectronChangeController {

}

