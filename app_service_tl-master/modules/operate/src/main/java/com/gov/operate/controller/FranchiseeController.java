package com.gov.operate.controller;


import com.alibaba.fastjson.JSON;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.*;
import com.gov.operate.dto.franchiseeData.FranchiseeVO;
import com.gov.operate.dto.storeData.EditStoreDataDto;
import com.gov.operate.dto.storeData.SaveStoreDataDto;
import com.gov.operate.dto.storeData.StoreDataAddVO;
import com.gov.operate.dto.storeData.StoreDataEditVO;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.service.IFranchiseeService;
import com.gov.operate.service.IStoreDataService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-06
 */
@Slf4j
@RestController
@RequestMapping("/franchisee/")
public class FranchiseeController {

    @Resource
    private IFranchiseeService franchiseeService;
    @Resource
    private IStoreDataService storeDataService;


    /**
     * 注册加盟商
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"insertFranchisee"})
    public Result insertFranchiseeByPhone(HttpServletRequest request, @RequestBody FranchiseeVO inData) {
//      GetLoginOutData userInfo = this.getLoginUser(request);
        this.franchiseeService.insertFranchisee(inData);
        return ResultUtil.success();
    }

    /**
     * 修改加盟商
     * @param inData
     * @return
     */
    @PostMapping({"updateFranchisee"})
    public Result updateFranchisee(@RequestBody FranchiseeVO inData) {
        this.franchiseeService.updateFranchisee(inData);
        return ResultUtil.success();
    }
    /**
     * 查询加盟商
     * @param inData
     * @return
     */
    @PostMapping({"getFranchiseeById"})
    public Result getFranchiseeById(@RequestBody FranchiseeVO inData) {
//        UserLoginModelClient currentUser = userService.getCurrentUser();
//        if (StringUtils.isEmpty(inData.getClient())) {
//            inData.setClient(currentUser.getClient());
//        }
        return ResultUtil.success(this.franchiseeService.getFranchiseeById(inData));
    }

    /**
     * 移动端加盟商注册发送短信验证码
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"getSmsAuthenticationCode"})
    public Result getSmsAuthenticationCode(HttpServletRequest request,@RequestBody FranchiseeVO inData) {
        this.franchiseeService.getSmsAuthenticationCode(inData);
        return ResultUtil.success();
    }

    @GetMapping("getStoreInfo")
    @ApiOperation(value = "门店详情")
    public Result getStoreInfo(@RequestParam(name = "stoCode", required = true) String stoCode) {
        return ResultUtil.success(storeDataService.getStoreInfo(stoCode));
    }

    @PostMapping("addStore")
    @ApiOperation(value = "门店新增")
    public Result addStore(@RequestBody @Valid StoreDataAddVO storeDataAddVO) {
        storeDataService.addStore(storeDataAddVO, null);
        return ResultUtil.success();
    }

    @PostMapping("addStoreFeign")
    @ApiOperation(value = "门店新增")
    public Result addStoreFeign(@RequestBody @Valid SaveStoreDataDto storeDataAddVO) {
        log.info("<新增门店调用成功><参数：{}>", JSON.toJSONString(storeDataAddVO));
        TokenUser tokenUser = new TokenUser();
        tokenUser.setClient(storeDataAddVO.getClient());
        tokenUser.setUserId(storeDataAddVO.getStoCreId());
        storeDataService.addStore(storeDataAddVO, tokenUser);
        return ResultUtil.success();
    }

    @PostMapping("editStore")
    @ApiOperation(value = "门店编辑")
    public Result editStore(@RequestBody @Valid StoreDataEditVO storeDataEditVO) {
        storeDataService.editStore(storeDataEditVO, null);
        return ResultUtil.success();
    }

    @PostMapping("editStoreFeign")
    @ApiOperation(value = "门店编辑")
    public Result editStoreFeign(@RequestBody @Valid EditStoreDataDto storeDataEditVO) {
        log.info("<修改门店调用成功><参数：{}>",JSON.toJSONString(storeDataEditVO));
        TokenUser tokenUser = new TokenUser();
        tokenUser.setClient(storeDataEditVO.getClient());
        tokenUser.setUserId(storeDataEditVO.getStoModiId());
        storeDataService.editStore(storeDataEditVO, tokenUser);
        return ResultUtil.success();
    }

    @PostMapping("findFristArea")
    @ApiOperation(value = "查询一级区域")
    public Result findFristArea() {
        return ResultUtil.success(storeDataService.getFristArea());
    }

    @PostMapping("findParentArea")
    @ApiOperation(value = "查询上级区域")
    public Result findParentArea(@RequestBody RegionalPlanningModel regionalPlanningModel) {
        storeDataService.getRegionalIdByRegional(regionalPlanningModel);
        return ResultUtil.success();
    }

    //行政区域
    @PostMapping("findAreaByLevel")
    @ApiOperation(value = "省")
    public Result findAreaByLevel() {
        return ResultUtil.success(storeDataService.findAreaByLevel());
    }

    //市 或 区
//    @PostMapping("findAreaByParentId")
//    @ApiOperation(value = "市或区")
//    public Result findAreaByParentId(@RequestBody GaiaArea area) {
//        return ResultUtil.success(storeDataService.findAreaByParentId(area));
//    }

    @GetMapping("getStoreCode")
    @ApiOperation(value = "门店编码获取")
    public Result getStoreCode() {
        return ResultUtil.success(storeDataService.getStoreCode());
    }

    @GetMapping("getStoreGroupCode")
    @ApiOperation(value = "门店分组编码获取")
    public Result getStoreGroupCode(){return ResultUtil.success(storeDataService.getStoreGroupCode());}

    @PostMapping("getStoreGroup")
    @ApiOperation(value = "门店分类详情")
    public Result getStoreGroup(@RequestBody @Valid StoreGroupDto storeGroupDto){
        return ResultUtil.success(storeDataService.getStoreGroup(storeGroupDto));
    }

    @PostMapping("editStoreGroup")
    @ApiOperation(value = "门店分类编辑")
    public Result editStoreGroup(@RequestBody @Valid StoreGroupVo storeGroupVo){
        storeDataService.editStoreGroup(storeGroupVo);
        return ResultUtil.success();
    }

    /**
     * 对已上线的用户，进行默认配置，用完后废弃
     * @return
     */
    @GetMapping("setDefaultStoreGroup")
    @ApiOperation(value = "设置默认门店分类")
    public Result setDefaultStoreGroup(){
        storeDataService.setDefaultStoreGroup();
        return ResultUtil.success();
    }

    @PostMapping("storeGroupExport")
    @ApiOperation(value = "门店分类导出")
    public Result storeGroupExport(@RequestBody @Valid StoreGroupDto storeGroupDto) throws IOException {
        return storeDataService.storeGroupExport(storeGroupDto);
    }

    /**
     * 门店分类名称维护
     * @param
     * @return
     */
    @PostMapping("saveGroupSet")
    public Result saveGroupSet(@RequestBody List<StoreGroupSetDto> dtoList){
        storeDataService.saveGroupSet(dtoList);
        return ResultUtil.success();
    }

    /**
     * add by jinwencheng on 2021-12-23 15:41:35 添加门店分类 - 门店选择 - 查询
     * 添加门店分类 - 门店选择 - 查询
     * @param getStoresForAddDTO
     * @return
     * add end
     */
    @ApiOperation(value = "添加门店分类 - 门店选择 - 查询")
    @PostMapping("getStoresForAdd")
    public Result getStoresForAdd(@Valid @RequestBody GetStoresForAddDTO getStoresForAddDTO) {
        return ResultUtil.success(storeDataService.getStoresForAdd(getStoresForAddDTO));
    }

    /**
     * add by jinwencheng on 2021-12-23 15:41:35 导入门店分类
     *
     * @param request
     * @param file
     * @return
     * add end
     */
    @PostMapping("/batchImport")
    @ApiOperation(value = "导入", notes = "导入")
    public Result batchImport(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        return ResultUtil.success(storeDataService.batchImport(request, file));
    }
    /** add end **/

    /**
     * add by jinwencheng on 2021-12-23 15:41:35 保存门店分类
     *
     * @param storesGroupAddDTO
     * @return
     * add end
     */
    @PostMapping("/saveStoreGroup")
    @ApiOperation(value = "保存门店分类", notes = "保存门店分类")
    public Result saveStoreGroup(@Valid @RequestBody List<StoresGroupAddDTO> storesGroupAddDTO) {
        storeDataService.saveStoreGroup(storesGroupAddDTO);
        return ResultUtil.success();
    }
    /** add end **/

}

