package com.gov.operate.mapper;

import com.gov.operate.entity.ProductBasic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-09-23
 */
public interface ProductBasicMapper extends BaseMapper<ProductBasic> {

}
