package com.gov.operate.service.popularize;

import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.charge.PayableVO;
import com.gov.operate.entity.PayingHeaderNew;

import javax.servlet.http.HttpServletRequest;

public interface IPayingHeaderNewService extends SuperService<PayingHeaderNew> {
    Result getPayableList(String spId, Integer days, Integer payFreq, String client);

    Result saveClientPay(PayableVO payableVO);

    Result getPayStatus(String ficoId, String client);

    void payCallback() throws Exception;

    void payResult(HttpServletRequest httpServletRequest);

    Result saveClientPayMaintain(PayableVO payableVO);

    void payExpire();

    Result closeStore(String client, String stoCode, String closeDate);

    void payReminder();
}
