package com.gov.operate.entity.category;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("GAIA_PRODUCT_OUT")
public class CategoryAnalyseData {
    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商ID")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "地点")
    @TableField("PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "商品自编码")
    @TableField("PRO_SELF_CODE")
    private String proSelfCode;

    @ApiModelProperty(value = "淘汰日期")
    @TableField("PRO_OUT_DATE")
    private String proOutDate;

    @ApiModelProperty(value = "淘汰原因")
    @TableField("PRO_OUT_REASON")
    private String proOutReason;

    @ApiModelProperty(value = "创建日期")
    @TableField("PRO_OUT_CREATE_DATE")
    private String proOutCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("PRO_OUT_CREATE_TIME")
    private String proOutCreateTime;

    @ApiModelProperty(value = "创建人")
    @TableField("PRO_OUT_CREATE_USER")
    private String proOutCreateUser;
}
