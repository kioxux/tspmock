package com.gov.operate.dto;

import com.gov.operate.entity.FiInvoiceRegistrationDetailed;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class FiInvoiceRegistrationDetailedDTO extends FiInvoiceRegistrationDetailed {

    /**
     * 商品编码
     */
    private String soProCode;
    /**
     * 供应商编码
     */
    private String supCode;
    /**
     * 地点
     */
    private String proSite;

}
