package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 总部采购
 * </p>
 *
 * @author sy
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_BATCH_HEAD_PURCHASE")
@ApiModel(value="BatchHeadPurchase对象", description="总部采购")
public class BatchHeadPurchase extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableField("ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "查询厂家")
    @TableField("GBHP_QUERY_FACTORY")
    private String gbhpQueryFactory;

    @ApiModelProperty(value = "查询仓库")
    @TableField("GBHP_DC")
    private String gbhpDc;

    @ApiModelProperty(value = "日期")
    @TableField("GBHP_DATE")
    private String gbhpDate;

    @ApiModelProperty(value = "销售方代码")
    @TableField("GBHP_SALES_CODE")
    private String gbhpSalesCode;

    @ApiModelProperty(value = "销售方名称")
    @TableField("GBHP_SALES_NAME")
    private String gbhpSalesName;

    @ApiModelProperty(value = "采购方代码")
    @TableField("GBHP_PURCHASE_CODE")
    private String gbhpPurchaseCode;

    @ApiModelProperty(value = "采购方名称")
    @TableField("GBHP_PURCHASE_NAME")
    private String gbhpPurchaseName;

    @ApiModelProperty(value = "产品代码")
    @TableField("GBHP_PRO_CODE")
    private String gbhpProCode;

    @ApiModelProperty(value = "产品名称")
    @TableField("GBHP_PRO_NAME")
    private String gbhpProName;

    @ApiModelProperty(value = "产品规格")
    @TableField("GBHP_PRO_SPECS")
    private String gbhpProSpecs;

    @ApiModelProperty(value = "批号")
    @TableField("GBHP_BATCH_NO")
    private String gbhpBatchNo;

    @ApiModelProperty(value = "数量")
    @TableField("GBHP_QTY")
    private BigDecimal gbhpQty;

    @ApiModelProperty(value = "单位")
    @TableField("GBHP_UNIT")
    private String gbhpUnit;

    @ApiModelProperty(value = "单价")
    @TableField("GBHP_PRICE")
    private BigDecimal gbhpPrice;

    @ApiModelProperty(value = "金额")
    @TableField("GBHP_AMT")
    private BigDecimal gbhpAmt;

    @ApiModelProperty(value = "生产厂家")
    @TableField("GBHP_FACTORY")
    private String gbhpFactory;

    @ApiModelProperty(value = "有效期")
    @TableField("GBHP_VALID")
    private String gbhpValid;

    @ApiModelProperty(value = "采购类型")
    @TableField("GBHP_PRUCHASE_TYPE")
    private String gbhpPruchaseType;

    @ApiModelProperty(value = "批准文号")
    @TableField("GBHP_APPROVAL_NUMBER")
    private String gbhpApprovalNumber;

    @ApiModelProperty(value = "创建日期")
    @TableField("GBHP_CREATE_DATE")
    private String gbhpCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GBHP_CREATE_TIME")
    private String gbhpCreateTime;


}
