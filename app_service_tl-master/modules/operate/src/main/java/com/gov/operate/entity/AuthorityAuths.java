package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHORITY_AUTHS")
@ApiModel(value="AuthorityAuths对象", description="")
public class AuthorityAuths extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "名称")
    @TableField("GAA_NAME")
    private String gaaName;

    @ApiModelProperty(value = "唯一code")
    @TableField("GAA_CODE")
    private String gaaCode;

    @ApiModelProperty(value = "父Id")
    @TableField("GAA_PARENT_ID")
    private Integer gaaParentId;

    @ApiModelProperty(value = "排序")
    @TableField("GAA_SORT")
    private Integer gaaSort;

    @ApiModelProperty(value = "收费标志(0免费：1收费)")
    @TableField("GAA_FLAG")
    private String gaaFlag;

    @ApiModelProperty(value = "创建日期")
    @TableField("GAA_CJRQ")
    private String gaaCjrq;

    @ApiModelProperty(value = "创建时间")
    @TableField("GAA_CJSJ")
    private String gaaCjsj;

    @ApiModelProperty(value = "创建人")
    @TableField("GAA_CJR")
    private String gaaCjr;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("GAA_CJRXM")
    private String gaaCjrxm;

    @ApiModelProperty(value = "是否删除0是/1否（默认否）")
    @TableField("GAA_SFSQ")
    private String gaaSfsq;

    @ApiModelProperty(value = "修改日期")
    @TableField("GAA_XGRQ")
    private String gaaXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("GAA_XGSJ")
    private String gaaXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("GAA_XGR")
    private String gaaXgr;

    @ApiModelProperty(value = "修改人姓名")
    @TableField("GAA_XGRXM")
    private String gaaXgrxm;

    @ApiModelProperty(value = "公共权限")
    @TableField("GAA_PUBLIC")
    private Integer gaaPublic;

}
