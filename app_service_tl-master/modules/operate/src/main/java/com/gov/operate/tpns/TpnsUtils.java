package com.gov.operate.tpns;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.gov.common.entity.ApplicationConfig;
import com.tencent.xinge.*;
import com.tencent.xinge.bean.*;
import com.tencent.xinge.bean.ios.Aps;
import com.tencent.xinge.push.app.PushAppRequest;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * rmk  信鸽推送工具类
 * yukun.xie
 */
@Slf4j
public class TpnsUtils {

    public static XingeApp xingeApp(String accessId, String secretKey, String domain) {
        XingeApp xingeApp = (new XingeApp.Builder()).appId(accessId).secretKey(secretKey).domainUrl(domain).build();
        return xingeApp;
    }

    /**
     * rmk  组装MessageAndroid
     *
     * @param attachKeys 自定义参数
     * @return
     */
    private static MessageAndroid buildMessageAndroid(Map<String, Object> attachKeys) {
        MessageAndroid messageAndroid = new MessageAndroid();
        if (null != attachKeys && attachKeys.size() > 0) {
            // 设置自定义参数
            messageAndroid.setCustom_content(new Gson().toJson(attachKeys));
        }
        return messageAndroid;
    }

    /**
     * rmk 组装推送消息体
     *
     * @param title
     * @param content
     * @param additionKey
     * @return
     */
    private static Message buildMessageAndroid(String title, String content, Map<String, Object> additionKey) {
        // 开始组装推送消息体
        Message message = new Message();
        // 推送标题
        message.setTitle(title);
        // 推送内容
        message.setContent(content);
        // 安卓推送参数
        message.setAndroid(buildMessageAndroid(additionKey));
        return message;
    }

    /**
     * rmk  组装MessageAndroid
     *
     * @param attachKeys 自定义参数
     * @return
     */
    private static MessageIOS buildMessageIos(Map<String, Object> attachKeys) {
        MessageIOS messageIOS = new MessageIOS();
        if (null != attachKeys && attachKeys.size() > 0) {
            // 设置自定义参数
            messageIOS.setCustom(new Gson().toJson(attachKeys));
        }
        Aps aps = new Aps();
        aps.setBadge_type(-2);
        aps.setSound("default");
        messageIOS.setAps(aps);
        return messageIOS;
    }

    private static Message buildMessageIos(String title, String content, Map<String, Object> additionKey) {
        // 开始组装推送消息体
        Message message = new Message();
        // 推送标题
        message.setTitle(title);
        // 推送内容
        message.setContent(content);
        // 安卓推送参数
        message.setIos(buildMessageIos(additionKey));
        return message;
    }

    /**
     * rmk 组装推送基础功能
     *
     * @param audienceType 推送的类型，基本上以 token_list(设备号推送)  和 account_list(指定id推送)
     * @param platform     推送的系统  目前只支持ios
     * @param title        推送的标题
     * @param content      推送的内容
     * @param additionKey  推送的自定义参数
     * @param accountList  推送的 拼装用户id 列表
     */
    private static PushAppRequest buildAppRequest(AudienceType audienceType, Platform platform, String title, String content, Map<String, Object> additionKey, List<String> accountList) {
        // 开始组装推送基础功能
        PushAppRequest pushAppRequest = new PushAppRequest();
        pushAppRequest.setAudience_type(audienceType);
        pushAppRequest.setPlatform(platform);
        pushAppRequest.setMessage_type(MessageType.notify);
        if (Platform.ios.equals(platform)) {
            pushAppRequest.setMessage(buildMessageIos(title, content, additionKey));
        } else {
            pushAppRequest.setMessage(buildMessageAndroid(title, content, additionKey));
        }
        pushAppRequest.setExpire_time(86400);
        // 推送账号列表
        pushAppRequest.setAccount_list((ArrayList<String>) accountList);
        return pushAppRequest;
    }

    /**
     * rmk  处理推送返回结果
     *
     * @param jsonRet 推送返回的接口
     */
    private static void rtnMsg(JSONObject jsonRet) {
        if (null != jsonRet) {
            log.info(" XinGePushUtil.rtnMsg jsonRet -->" + jsonRet.toString());
            if (null != jsonRet.get("ret_code") && !jsonRet.get("ret_code").equals(200) && !jsonRet.get("ret_code").equals(0)) {
//                Preconditions.checkArgument(false, "推送失败，调用推送返回code 为 ->" + jsonRet.get("ret_code"));
            }
        } else {
//            Preconditions.checkArgument(false, "推送失败");
        }
    }

    /**
     * 消息推送
     *
     * @param
     * @throws Exception
     */
    public static void send(PushContent pushContent, XingeApp xingeApp) {
        // 业务 参数
        Map<String, Object> additionKey = (null == pushContent.getAttachKeys() ? new HashMap<String, Object>() : pushContent.getAttachKeys());
        PushAppRequest pushAppRequest = buildAppRequest(AudienceType.account_list, pushContent.getPlatform(), pushContent.getTitle(), pushContent.getContent(), additionKey, pushContent.getAccountList());
        log.info("推送参数：{}", JSON.toJSON(pushAppRequest));
        pushAppRequest.setEnvironment(pushContent.getEnvironment());
        JSONObject jsonRet = xingeApp.pushApp(pushAppRequest);
        log.info("推送结果：" + jsonRet);
        rtnMsg(jsonRet);
    }

    /**
     * 推送参数
     *
     * @return
     */
    public static XingeApp getXingeApp(PushContent pushContent, ApplicationConfig applicationConfig, String platform) {
        XingeApp xingeApp = null;
        // 安卓登录
        if ("android".equals(platform)) {
            xingeApp = TpnsUtils.xingeApp(applicationConfig.getTpnsAndroidAppId(), applicationConfig.getTpnsAndroidSecretKey(), applicationConfig.getTpnsDoMain());
            pushContent.setPlatform(Platform.android);
        }
        //
        if ("ios".equals(platform)) {
            xingeApp = TpnsUtils.xingeApp(applicationConfig.getTpnsIosAppId(), applicationConfig.getTpnsIosSecretKey(), applicationConfig.getTpnsDoMain());
            pushContent.setPlatform(Platform.ios);
            if (applicationConfig.getEnvironment() == 1) {
                pushContent.setEnvironment(Environment.dev);
            }
            if (applicationConfig.getEnvironment() == 0) {
                pushContent.setEnvironment(Environment.product);
            }
        }
        return xingeApp;
    }

}
