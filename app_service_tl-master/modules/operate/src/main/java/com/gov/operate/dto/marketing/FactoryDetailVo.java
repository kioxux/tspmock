package com.gov.operate.dto.marketing;

import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 11:06 2021/8/13
 * @Description：生产厂家出参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class FactoryDetailVo {

    private String factoryCode;

    private String factoryName;
}
