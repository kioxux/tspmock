package com.gov.operate.controller.marketing;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.operate.dto.EditGsmSettingVO2;
import com.gov.operate.dto.GetGsmSettingListVO2;
import com.gov.operate.dto.PushGsmVO2;
import com.gov.operate.dto.SaveGsmSettingVO2;
import com.gov.operate.service.ISdMarketingBasicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-10-26
 */
@Api(tags = "营销活动设置")
@RestController
@RequestMapping("gsm/gsm")
public class MarketingBasicController {

    @Resource
    private ISdMarketingBasicService sdMarketingBasicService;


    @Log("营销设置列表")
    @ApiOperation(value = "营销设置列表")
    @PostMapping("getGsmSettingList")
    public Result getGsmSettingList(@Valid @RequestBody GetGsmSettingListVO2 vo) {
        return ResultUtil.success(sdMarketingBasicService.getGsmSettingList(vo));
    }

    @Log("营销设置保存")
    @ApiOperation(value = "营销设置保存")
    @PostMapping("saveGsmSetting")
    public Result saveGsmSetting(@Valid @RequestBody SaveGsmSettingVO2 vo) {
        sdMarketingBasicService.saveGsmSetting(vo);
        return ResultUtil.success();
    }

    @Log("营销设置编辑")
    @ApiOperation(value = "营销设置编辑")
    @PostMapping("editGsmSetting")
    public Result editGsmSetting(@Valid @RequestBody EditGsmSettingVO2 vo) {
        sdMarketingBasicService.editGsmSetting(vo);
        return ResultUtil.success();
    }

    @Log("营销设置详情")
    @ApiOperation(value = "营销设置详情")
    @GetMapping("getGsmSettingDetail")
    public Result getGsmSettingDetail(@RequestParam("id") Integer id) {
        return ResultUtil.success(sdMarketingBasicService.getGsmSettingDetail(id));
    }

    @Log("营销设置删除")
    @ApiOperation(value = "营销设置删除")
    @GetMapping("deleteMarketSetting")
    public Result deleteMarketSetting(@RequestParam("id") Integer id) {
        sdMarketingBasicService.deleteMarketSetting(id);
        return ResultUtil.success();
    }

    @Log("活动方案推送")
    @ApiOperation(value = "活动方案推送")
    @PostMapping("pushGsm")
    public Result pushGsm(@Valid @RequestBody PushGsmVO2 vo) {
        sdMarketingBasicService.pushGsm(vo);
        return ResultUtil.success();
    }

    @Log("促销方式列表")
    @ApiOperation(value = "促销方式列表")
    @GetMapping("getPromotionList")
    public Result getPromotionList() {
        return ResultUtil.success(sdMarketingBasicService.getPromotionList());
    }

}

