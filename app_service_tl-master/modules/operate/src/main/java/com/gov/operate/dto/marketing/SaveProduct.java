package com.gov.operate.dto.marketing;

import com.gov.operate.dto.GetProductListToCheckDTO;
import com.gov.operate.request.RequestJson;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.04
 */
@Data
public class SaveProduct {
    /**
     * 营销id
     */
    private Integer marketingId;
    /**
     * 商品集合
     */
    private List<GetProductListToCheckDTO> list;
}
