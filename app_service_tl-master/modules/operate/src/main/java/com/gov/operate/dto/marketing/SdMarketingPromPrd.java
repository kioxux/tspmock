package com.gov.operate.dto.marketing;

import com.gov.operate.entity.SdMarketingProm;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.21
 */
@Data
public class SdMarketingPromPrd extends SdMarketingProm {

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品关联表ID
     */
    private Integer prdId;

    /**
     * 零售价
     */
    private BigDecimal priceNormal;

    /**
     * 进货价
     */
    private BigDecimal proCgj;

}
