package com.gov.operate.dto;

import java.math.BigDecimal;
import java.util.List;

public class LoginSalePlan {

    //销售日期(年周)
    private String month;
    //销售天数
    private String day;
    //营业额-月计划
    private BigDecimal tAmt;
    //毛利额-月计划
    private BigDecimal tGross;
    //会员卡-月计划
    private BigDecimal tMcardQty;
    //营业额-达成
    private BigDecimal aAmt;
    //毛利额-达成
    private BigDecimal aGross;
    //会员卡-达成
    private BigDecimal aMcardQty;
    //营业额-达成率
    private String rAmt;
    //毛利额-达成率
    private String rGross;
    //会员卡-达成率
    private String rMcardQty;
    //累计数量
    private String noQty;
    //客单价
    private String pct;
    //日均交易
    private String dayQty;

    private List<ShopAssistantOutData>  shopAssistants;

    public void setPct(String pct) {
        this.pct = pct;
    }

    public String getDayQty() {
        return dayQty;
    }

    public void setDayQty(String dayQty) {
        this.dayQty = dayQty;
    }

    public String getNoQty() {
        return noQty;
    }

    public void setNoQty(String noQty) {
        this.noQty = noQty;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal gettAmt() {
        return tAmt;
    }

    public void settAmt(BigDecimal tAmt) {
        this.tAmt = tAmt;
    }

    public BigDecimal gettGross() {
        return tGross;
    }

    public void settGross(BigDecimal tGross) {
        this.tGross = tGross;
    }

    public BigDecimal gettMcardQty() {
        return tMcardQty;
    }

    public void settMcardQty(BigDecimal tMcardQty) {
        this.tMcardQty = tMcardQty;
    }

    public BigDecimal getaAmt() {
        return aAmt;
    }

    public void setaAmt(BigDecimal aAmt) {
        this.aAmt = aAmt;
    }

    public BigDecimal getaGross() {
        return aGross;
    }

    public void setaGross(BigDecimal aGross) {
        this.aGross = aGross;
    }

    public BigDecimal getaMcardQty() {
        return aMcardQty;
    }

    public void setaMcardQty(BigDecimal aMcardQty) {
        this.aMcardQty = aMcardQty;
    }

    public String getrAmt() {
        return rAmt;
    }

    public void setrAmt(String rAmt) {
        this.rAmt = rAmt;
    }

    public String getrGross() {
        return rGross;
    }

    public void setrGross(String rGross) {
        this.rGross = rGross;
    }

    public String getrMcardQty() {
        return rMcardQty;
    }

    public void setrMcardQty(String rMcardQty) {
        this.rMcardQty = rMcardQty;
    }

    public String getPct() {
        return pct;
    }

    public List<ShopAssistantOutData> getShopAssistants() {
        return shopAssistants;
    }

    public void setShopAssistants(List<ShopAssistantOutData> shopAssistants) {
        this.shopAssistants = shopAssistants;
    }
}
