package com.gov.operate.mapper;

import com.gov.operate.entity.SdDiscount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
public interface SdDiscountMapper extends BaseMapper<SdDiscount> {
    /**
     * 获取当前加盟商下数字编码最大值 折扣id
     */
    String getMaxNumDisCountId(@Param("client") String client, @Param("marketId") String marketId, @Param("storeCode") String storeCode);
}
