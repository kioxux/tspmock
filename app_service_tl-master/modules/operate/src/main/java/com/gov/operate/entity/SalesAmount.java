package com.gov.operate.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2021-02-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALES_AMOUNT")
@ApiModel(value="SalesAmount对象", description="")
public class SalesAmount extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @TableField("GSA_ORDER_ID")
    private String gsaOrderId;

    @TableField("GSA_AMOUNT")
    private BigDecimal gsaAmount;

    @TableField("GSA_LINE_NO")
    private String gsaLineNo;


}
