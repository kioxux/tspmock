package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
public class SaveMarketProductVO {

    @NotBlank(message = "营销活动ID")
    private String gsmMarketid;

    @NotBlank(message = "商品编码")
    private String gsmCode;

    private String gsmPromotId;

    /**
     * 已选 0：删除，1：保存
     */
    private Integer selFlg;

}
