package com.gov.operate.dto.invoiceOptimiz;

import com.gov.operate.dto.FicoInvoiceInformationRegistrationDTO;
import com.gov.operate.dto.SelectWarehousingDTO;
import com.gov.operate.entity.DocumentBillD;
import com.gov.operate.entity.DocumentBillH;
import com.gov.operate.entity.DocumentInvoiceBill;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * copy from com.gov.operate.dto.invoiceOptimiz.DocumentBillDto
 *
 * @author: ziyi
 * @date: 2021.03.17
 */
@Data
public class DocumentBillDetailsDTO {

    /**
     * 对账单号
     */
    private String gdbNum;

    /**
     * 收货金额
     */
    private BigDecimal gdbTotalAmt;

    /**
     * 折扣
     */
    private BigDecimal gdbRebate;

    /**
     * 应付金额
     */
    private BigDecimal gdbPayAmt;

    /**
     * 差异
     */
    private BigDecimal gdbDiff;

    /**
     * 原因
     */
    private String gdbReason;

    /**
     * 单据列表
     */
    private List<DocumentBillHDTO> selectWarehousingDTOList;


    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class DocumentBillHDTO extends DocumentBillH {

        /**
         * 地点名称
         */
        private String gdbhSiteName;
        /**
         * 供应商名称
         */
        private String gdbhSupCodeName;

        /**
         * 单据商品明细
         */
        private List<DocumentBillDDTO> documentBillDList;
        /**
         * 单据发标明细
         */
        private List<DocumentInvoiceBillDTO> documentInvoiceList;

        @Data
        @EqualsAndHashCode(callSuper = false)
        public static class DocumentBillDDTO extends DocumentBillD {
            /**
             * 主键
             */
            private String id;
            /**
             * 类型
             */
            private String type;
            /**
             * 类型名称
             */
            private String typeName;
            /**
             * 商品自编码
             */
            private String proSelfCode;
            /**
             * 商品名
             */
            private String proName;
            /**
             * 规格
             */
            private String proSpecs;
            /**
             * 厂家
             */
            private String proFactoryName;
        }

        @Data
        @EqualsAndHashCode(callSuper = false)
        public static class DocumentInvoiceBillDTO extends DocumentInvoiceBill {

            private String invoiceNum;

            private String siteCode;

            private String supCode;

            private String invoiceType;

            private String invoiceDate;

            private BigDecimal invoiceRate;

            private BigDecimal invoiceAmountExcludingTax;

            private BigDecimal invoiceTax;

            private BigDecimal invoiceTotalInvoiceAmount;

            private BigDecimal invoiceRegisteredAmount;

            private BigDecimal invoicePaidAmount;

            private BigDecimal invoiceConfirmationAmount;

            private String invoiceRegistrationStatus;

            private String invoicePaymentStatus;

            private String invoiceCreationDate;

            private String invoiceFounder;

            private String invoiceSiteType;

            private String invoiceSupName;

            private String stoName;

            private String supName;
        }
    }


}

