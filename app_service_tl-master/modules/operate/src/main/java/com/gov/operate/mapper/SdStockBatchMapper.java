package com.gov.operate.mapper;

import com.gov.operate.entity.SdStockBatch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
public interface SdStockBatchMapper extends BaseMapper<SdStockBatch> {

}
