package com.gov.operate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class OrderDetailInvoiceVO {

    @NotBlank(message = "业务单号")
    private String matDnId;

    /**
     * 总金额 明细对账用
     */
    private BigDecimal totalAmt;

    @NotEmpty(message = "发票明细集合不能为空")
    private List<String> invoiceNumList;

    @Valid
    @NotEmpty(message = "订单详情集合不能为空")
    private List<OrderDetail> orderDetailList;

    @Data
    @EqualsAndHashCode(callSuper = false)
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderDetail {
        @NotBlank(message = "业务类型不能为空")
        private String type;
        @NotBlank(message = "商品编码不能为空")
        private String proCode;
//        @NotBlank(message = "物料凭证号不能为空")
        private String matId;
//        @NotBlank(message = "物料凭证年份不能为空")
        private String matYear;
        @NotBlank(message = "物料凭证行号不能为空")
        private String matLineNo;
        @NotNull(message = "确认金额不能为空")
        private BigDecimal amt;
    }

}
