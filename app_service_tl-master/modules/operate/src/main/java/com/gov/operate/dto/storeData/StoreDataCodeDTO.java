package com.gov.operate.dto.storeData;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreDataCodeDTO {
    /**
     * 部门编码
     */
    private String stoCode;
}
