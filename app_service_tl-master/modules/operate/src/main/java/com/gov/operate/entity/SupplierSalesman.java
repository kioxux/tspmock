package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import lombok.Data;

/**
 * 供应商业务员表
 *
 * @TableName GAIA_SUPPLIER_SALESMAN
 */
@TableName(value = "GAIA_SUPPLIER_SALESMAN")
@Data
public class SupplierSalesman extends BaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商地点
     */
    private String supSite;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 业务员编号
     */
    private String gssCode;

    /**
     * 业务员姓名
     */
    private String gssName;

    /**
     * 身份证号
     */
    private String gssIdCard;

    /**
     * 电话
     */
    private String gssPhone;

    /**
     * 经营范围
     */
    private String gssBusinessScope;

    /**
     * 首营流程编号
     */
    private String gssFlowNo;

    /**
     * 默认业务员
     */
    private Integer gssDefault;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}