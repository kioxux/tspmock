package com.gov.operate.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author staxc
 * @Date 2020/10/16 10:17
 * @desc
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AddInDrawerVO {

//    @NotBlank(message = "清斗单号不能为空")
    private String gsihCleVoucherId;

    @NotBlank(message = "装斗人员不能为空")
    private String gsihEmp;

    /**
     * 备注
     */
    private String gsihRemaks;

    private List<AddInDrawerDetailVO> details;

    private String gsihEmp1;
}
