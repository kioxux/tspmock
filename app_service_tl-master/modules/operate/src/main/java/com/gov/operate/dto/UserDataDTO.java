package com.gov.operate.dto;

import com.gov.operate.entity.UserData;
import lombok.Data;

@Data
public class UserDataDTO extends UserData {

    private Integer isStore;

}
