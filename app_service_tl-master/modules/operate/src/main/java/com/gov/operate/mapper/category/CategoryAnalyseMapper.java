package com.gov.operate.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.category.CategoryInData;
import com.gov.operate.dto.category.CategoryOutData;
import com.gov.operate.entity.category.CategoryAnalyseData;
import com.gov.operate.entity.category.ProChangeRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CategoryAnalyseMapper extends BaseMapper<CategoryAnalyseData> {

    List<CategoryOutData> selectImportProduct(CategoryInData inData);

    List<CategoryOutData> selectAplProduct(CategoryInData inData);

    void updateAdjustPrice(List<CategoryInData> list);

    String selectMaxAplDate(CategoryInData inData);

    Map<String,String> selectProInfo(@Param("client") String client, @Param("stoCode") String stoCode, @Param("proCode") String proCode);

    void updateProPosition(@Param("client") String client, @Param("stoCode") String stoCode, @Param("proCode") String proCode);

    void updateProPrice(List<Map<String,String>> inData);

    void addChangeRecord(ProChangeRecord record) ;

    List<Map<String,String>> selectDateList(Map<String,String> inData);

    void batchChangeRecord(List<ProChangeRecord> inData);

    void batchTimerUpdatePrice(List<Map<String,String>> inData);

    void batchTimerChange(List<ProChangeRecord> inData);

    List<CategoryOutData>selectTimerAplPro(@Param("aplConSetDate") String aplConSetDate);
}
