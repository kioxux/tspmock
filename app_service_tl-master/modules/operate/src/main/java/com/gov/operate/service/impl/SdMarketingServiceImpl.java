package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.uuid.UUIDUtil;
import com.gov.operate.dto.*;
import com.gov.operate.entity.Franchisee;
import com.gov.operate.entity.SdMarketing;
import com.gov.operate.entity.SdMarketingAnalysis;
import com.gov.operate.entity.SdMarketingSearch;
import com.gov.operate.feign.dto.TokenUser;
import com.gov.operate.mapper.FranchiseeMapper;
import com.gov.operate.mapper.SdMarketingAnalysisMapper;
import com.gov.operate.mapper.SdMarketingMapper;
import com.gov.operate.service.CommonService;
import com.gov.operate.service.ISdMarketingSearchService;
import com.gov.operate.service.ISdMarketingService;
import com.gov.operate.service.IStoreDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-20
 */
@Service
public class SdMarketingServiceImpl extends ServiceImpl<SdMarketingMapper, SdMarketing> implements ISdMarketingService {

    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private SdMarketingMapper sdMarketingMapper;
    @Resource
    private CommonService commonService;
    @Resource
    private SdMarketingAnalysisMapper sdMarketingAnalysisMapper;
    @Resource
    private ISdMarketingSearchService sdMarketingSearchService;
    @Resource
    private FranchiseeMapper franchiseeMapper;
    /**
     * 营销设置列表
     */
    @Override
    public IPage<GetGsmSettingListDTO> getGsmSettingList(GetGsmSettingListVO vo) {
        // 分页对象
        Page<SdMarketing> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<SdMarketing> query = new QueryWrapper<SdMarketing>()
                // 活动类型
                .eq(StringUtils.isNotEmpty(vo.getGsmType()), "marketing.GSM_TYPE", vo.getGsmType())
                // 活动开始时间
                .ge(StringUtils.isNotEmpty(vo.getGsmStartd()), "marketing.GSM_STARTD", vo.getGsmStartd())
                // 活动结束时间
                .le(StringUtils.isNotEmpty(vo.getGsmEndd()), "marketing.GSM_ENDD", vo.getGsmEndd())
                // 活动名称
                .apply(StringUtils.isNotEmpty(vo.getGsmThename()), "instr(marketing.GSM_THENAME, {0})", vo.getGsmThename())
                // 状态(0:未推送， 1:已推送)
                .eq(StringUtils.isNotEmpty(vo.getGsmImpl()), "marketing.GSM_IMPL", vo.getGsmImpl())
                // 营销设置主数据，(加盟商为空)
                .eq("marketing.CLIENT", "")
                // 营销设置主数据，(加盟商和门店均为空)
                .eq("marketing.GSM_STORE", "")
                // 删除标记(0 否,1 是)
                .eq("marketing.GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode());
        return sdMarketingMapper.getGsmSettingList(page, query);
    }

    /**
     * 营销设置详情
     */
    @Override
    public GetGsmSettingDetailDTO getGsmSettingDetail(String gsmMarketid) {
        SdMarketing marketing = this.getOne(new QueryWrapper<SdMarketing>()
                // 营销活动ID
                .eq("GSM_MARKETID", gsmMarketid)
                // 加盟商为空
                .eq("CLIENT", Constants.EMPTY_STRING)
                // 门店为空
                .eq("GSM_STORE", Constants.EMPTY_STRING)
                // 删除标记(0 否,1 是)
                .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode()));
        if (StringUtils.isEmpty(marketing)) {
            throw new CustomResultException(ResultEnum.E0018);
        }

        GetGsmSettingDetailDTO detailDTO = new GetGsmSettingDetailDTO();
        BeanUtils.copyProperties(marketing, detailDTO);

        // 短信查询条件
        if (StringUtils.isNotEmpty(detailDTO.getGsmSmscondi())) {
            SdMarketingSearch smsSearch = sdMarketingSearchService.getById(detailDTO.getGsmSmscondi());
            if (!ObjectUtils.isEmpty(smsSearch)) {
                detailDTO.setGsmSmscondiJson(smsSearch.getGsmsDetail());
            }
        }

        // 电话查询条件
        if (StringUtils.isNotEmpty(detailDTO.getGsmTelcondi())) {
            SdMarketingSearch telSearch = sdMarketingSearchService.getById(detailDTO.getGsmTelcondi());
            if (!ObjectUtils.isEmpty(telSearch)) {
                detailDTO.setGsmTelcondiJson(telSearch.getGsmsDetail());
            }
        }

        // 推送加盟商列表
        List<String> clientIdList = sdMarketingMapper.getClientList(gsmMarketid);
        if (CollectionUtils.isEmpty(clientIdList)) {
            return detailDTO;
        }
        List<Franchisee> list = franchiseeMapper.selectList(new QueryWrapper<Franchisee>().select("CLIENT", "FRANC_NAME", "FRANC_ADDR").in("CLIENT", clientIdList));
        detailDTO.setClientList(list);
        return detailDTO;
    }

    /**
     * 营销设置保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveGsmSetting(SaveGsmSettingVO vo) {
        SdMarketing marketing = new SdMarketing();
        BeanUtils.copyProperties(vo, marketing);
        marketing.setClient(Constants.EMPTY_STRING);
        marketing.setGsmStore(Constants.EMPTY_STRING);
        // 保存
        String maxId = getMaxNumMarketId();
        // 取当前加盟商下数字营销活动ID最大值
        marketing.setGsmMarketid(maxId);
        // 主题id 设置一样
        marketing.setGsmThenid(maxId);
        // 下发状态(0下发，1没有下发)
        marketing.setGsmImpl(OperateEnum.GsmImpl.NOT_PUSHED.getCode());
        // 删除状态(0-否，1-是)
        marketing.setGsmDeleteFlag(OperateEnum.YesOrNo.ZERO.getCode());

        // 短信查询条件 /条件为空页产生一条记录
        if (StringUtils.isNotEmpty(vo.getGsmSmscondiJson())) {
            SdMarketingSearch smsSearch = new SdMarketingSearch();
            smsSearch.setGsmsId(UUIDUtil.getUUID());
            smsSearch.setGsmsName(vo.getGsmThename());
            smsSearch.setGsmsDetail(vo.getGsmSmscondiJson());
            smsSearch.setGsmsStore("");
            smsSearch.setGsmsNormal("0");
            smsSearch.setGsmsDeleteFlag("0");
            smsSearch.setGsmsType(1);
            sdMarketingSearchService.save(smsSearch);
            marketing.setGsmSmscondi(smsSearch.getGsmsId());
        }
        // 电话通知查询条件 /条件为空页产生一条记录
        if (StringUtils.isNotEmpty(vo.getGsmTelcondiJson())) {
            SdMarketingSearch telSearch = new SdMarketingSearch();
            telSearch.setGsmsId(UUIDUtil.getUUID());
            telSearch.setGsmsName(vo.getGsmThename());
            telSearch.setGsmsDetail(vo.getGsmTelcondiJson());
            telSearch.setGsmsStore("");
            telSearch.setGsmsNormal("0");
            telSearch.setGsmsDeleteFlag("0");
            telSearch.setGsmsType(2);
            sdMarketingSearchService.save(telSearch);
            marketing.setGsmTelcondi(telSearch.getGsmsId());
        }

        this.save(marketing);
    }

    /**
     * 营销设置编辑
     */
    @Override
    public void editGsmSetting(EditGsmSettingVO vo) {
        SdMarketing sdMarketingExit = this.getOne(new QueryWrapper<SdMarketing>()
                .eq("GSM_MARKETID", vo.getGsmMarketid())
                .eq("CLIENT", Constants.EMPTY_STRING)
                .eq("GSM_STORE", Constants.EMPTY_STRING)
                .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode()));
        if (ObjectUtils.isEmpty(sdMarketingExit)) {
            throw new CustomResultException("该营销设置不存在");
        }
        if (!OperateEnum.GsmImpl.NOT_PUSHED.getCode().equals(sdMarketingExit.getGsmImpl())) {
            throw new CustomResultException("该营销设置已推送，不可编辑");
        }
        SdMarketing marketing = new SdMarketing();
        BeanUtils.copyProperties(vo, marketing);
        // 更新短信查询条件
        if (StringUtils.isNotEmpty(vo.getGsmSmscondi())) {
            SdMarketingSearch smsSearch = new SdMarketingSearch();
            smsSearch.setGsmsId(vo.getGsmSmscondi());
            smsSearch.setGsmsDetail(vo.getGsmSmscondiJson());
            sdMarketingSearchService.updateById(smsSearch);
        } else if (StringUtils.isNotEmpty(vo.getGsmSmscondiJson())) {
            SdMarketingSearch smsSearch = new SdMarketingSearch();
            smsSearch.setGsmsId(UUIDUtil.getUUID());
            smsSearch.setGsmsName(vo.getGsmThename());
            smsSearch.setGsmsDetail(vo.getGsmSmscondiJson());
            smsSearch.setGsmsStore("");
            smsSearch.setGsmsNormal("0");
            smsSearch.setGsmsDeleteFlag("0");
            smsSearch.setGsmsType(1);
            sdMarketingSearchService.save(smsSearch);
            marketing.setGsmSmscondi(smsSearch.getGsmsId());
        }
        // 更新电话通知查询条件
        if (StringUtils.isNotEmpty(vo.getGsmTelcondi())) {
            SdMarketingSearch telSearch = new SdMarketingSearch();
            telSearch.setGsmsId(vo.getGsmTelcondi());
            telSearch.setGsmsDetail(vo.getGsmTelcondiJson());
            sdMarketingSearchService.updateById(telSearch);
        } else if (StringUtils.isNotEmpty(vo.getGsmTelcondiJson())) {
            SdMarketingSearch telSearch = new SdMarketingSearch();
            telSearch.setGsmsId(UUIDUtil.getUUID());
            telSearch.setGsmsName(vo.getGsmThename());
            telSearch.setGsmsDetail(vo.getGsmTelcondiJson());
            telSearch.setGsmsStore("");
            telSearch.setGsmsNormal("0");
            telSearch.setGsmsDeleteFlag("0");
            telSearch.setGsmsType(2);
            sdMarketingSearchService.save(telSearch);
            marketing.setGsmTelcondi(telSearch.getGsmsId());
        }
        // 更新主表
        UpdateWrapper<SdMarketing> updateWrapper = new UpdateWrapper<SdMarketing>()
                .eq("GSM_MARKETID", marketing.getGsmMarketid())
                .eq("CLIENT", Constants.EMPTY_STRING)
                .eq("GSM_STORE", Constants.EMPTY_STRING);
        boolean update = this.update(marketing, updateWrapper);
        if (!update) {
            throw new CustomResultException(ResultEnum.E0029);
        }
    }

    /**
     * 取当前加盟商下数字营销活动ID最大值
     */
    private String getMaxNumMarketId() {
        String maxNumMarketid = sdMarketingMapper.getMaxNumMarketid();
        if (StringUtils.isEmpty(maxNumMarketid)) {
            return "1000000000";
        }
        BigDecimal marketIdNum = new BigDecimal(maxNumMarketid);
        return marketIdNum.add(new BigDecimal(1)).toString();
    }

    /**
     * 营销活动删除
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteMarketSetting(String gsmMarketid) {
        QueryWrapper<SdMarketing> query = new QueryWrapper<SdMarketing>()
                .eq("GSM_MARKETID", gsmMarketid)
                .eq("CLIENT", Constants.EMPTY_STRING)
                .eq("GSM_STORE", Constants.EMPTY_STRING)
                .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode());
        SdMarketing sdMarketingExit = this.getOne(query);
        if (ObjectUtils.isEmpty(sdMarketingExit)) {
            throw new CustomResultException("该营销设置不存在");
        }
        if (!OperateEnum.GsmImpl.NOT_PUSHED.getCode().equals(sdMarketingExit.getGsmImpl())) {
            throw new CustomResultException("该营销设置已推送，不可删除");
        }
        this.remove(query);
        // 删除短信/电话查询条件
        String gsmSmscondi = sdMarketingExit.getGsmSmscondi();
        String gsmTelcondi = sdMarketingExit.getGsmTelcondi();
        if (StringUtils.isNotEmpty(gsmSmscondi)) {
            sdMarketingSearchService.removeById(gsmSmscondi);
        }
        if (StringUtils.isNotEmpty(gsmTelcondi)) {
            sdMarketingSearchService.removeById(gsmTelcondi);
        }
    }

    /**
     * 活动方案推送
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pushGsm(PushGsmVO vo) {
        QueryWrapper<SdMarketing> query = new QueryWrapper<SdMarketing>()
                .eq("GSM_MARKETID", vo.getGsmMarketid())
                .eq("CLIENT", Constants.EMPTY_STRING)
                .eq("GSM_STORE", Constants.EMPTY_STRING)
                .eq("GSM_DELETE_FLAG", OperateEnum.YesOrNo.ZERO.getCode());
        SdMarketing marketingExit = this.getOne(query);
        if (ObjectUtils.isEmpty(marketingExit)) {
            throw new CustomResultException("该营销设置不存在");
        }
        if (!OperateEnum.GsmImpl.NOT_PUSHED.getCode().equals(marketingExit.getGsmImpl())) {
            throw new CustomResultException("该营销设置已推送，不可重复推送");
        }
        if (!DateUtils.compareTime(LocalDate.now(), marketingExit.getGsmEndd())) {
            throw new CustomResultException("该营销设置已经过期，不可推送");
        }

        // 状态改为已推送
        SdMarketing sdMarketing = new SdMarketing();
        sdMarketing.setGsmImpl(OperateEnum.GsmImpl.PUSHED.getCode());
        sdMarketing.setGsmReletime(DateUtils.getCurrentDateTimeStrFull());
        this.update(sdMarketing, query);

        // 保存新数据(推送到各个加盟商)
        List<SdMarketing> marketingsList = new ArrayList<>();
        List<SdMarketingSearch> searchList = new ArrayList<>();
        //
        SdMarketingSearch smsSearchExit = sdMarketingSearchService.getById(marketingExit.getGsmSmscondi());
        SdMarketingSearch telSearchExit = sdMarketingSearchService.getById(marketingExit.getGsmTelcondi());

        vo.getClientList().forEach(client -> {
            SdMarketing marketing = new SdMarketing();
            marketingsList.add(marketing);
            BeanUtils.copyProperties(marketingExit, marketing);
            // 设置加盟商, 门店置空, 活动id、状态和主数据一样。
            marketing.setClient(client);
            marketing.setGsmStore(Constants.EMPTY_STRING);
            marketing.setGsmImpl(OperateEnum.GsmImpl.PUSHED.getCode());
            // 另存常用查询/短信
            if (!ObjectUtils.isEmpty(smsSearchExit)) {
                SdMarketingSearch smsSearch = new SdMarketingSearch();
                searchList.add(smsSearch);
                BeanUtils.copyProperties(smsSearchExit, smsSearch);
                smsSearch.setGsmsId(UUIDUtil.getUUID());
                smsSearch.setClient(client);
                marketing.setGsmSmscondi(smsSearch.getGsmsId());
            }
            // 另存常用查询/电话
            if (!ObjectUtils.isEmpty(telSearchExit)) {
                SdMarketingSearch telSearch = new SdMarketingSearch();
                searchList.add(telSearch);
                BeanUtils.copyProperties(telSearchExit, telSearch);
                telSearch.setGsmsId(UUIDUtil.getUUID());
                telSearch.setClient(client);
                marketing.setGsmTelcondi(telSearch.getGsmsId());
            }
        });
        this.saveBatch(marketingsList);
        sdMarketingSearchService.saveBatch(searchList);
    }

    /**
     * 营销活动主题分析
     */
    @Override
    public SdMarketingAnalysis getStandardValue() {
        TokenUser user = commonService.getLoginInfo();
        return sdMarketingAnalysisMapper.getStandardValue(user.getClient());
    }

}
