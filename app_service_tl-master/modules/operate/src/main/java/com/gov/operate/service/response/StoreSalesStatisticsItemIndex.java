package com.gov.operate.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class StoreSalesStatisticsItemIndex {

    @ApiModelProperty(value = "横轴显示日期")
    private String xTime;

    @ApiModelProperty(value = "本期数量")
    private BigDecimal amountNow;

    @ApiModelProperty(value = "上期数量")
    private BigDecimal amountPre;

    @ApiModelProperty(value = "同期数量")
    private BigDecimal amountSameTerm;

}
