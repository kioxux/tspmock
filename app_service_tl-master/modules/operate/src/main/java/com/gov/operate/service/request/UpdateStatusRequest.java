package com.gov.operate.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class UpdateStatusRequest {

    @ApiModelProperty(value = "任务编码")
    @NotBlank(message = "会员维系任务不能为空")
    private String jobId;

    @ApiModelProperty(value = "手机号")
    @NotBlank(message = "手机号不能为空")
    private String mobile;

    @ApiModelProperty(value = "门店")
    private String stoCode;


    @ApiModelProperty(value = "会员回访类型0待回访1已接听2无应答3已拦截")
    private String susStatus;

    @ApiModelProperty(value = "维系标签类型0电话空号1不在维系")
    private List<String> flgaddType;

    @ApiModelProperty(value = "回访反馈")
    private String susFeedback;
}
