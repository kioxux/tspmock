package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FI_INVOICE_REGISTRATION")
@ApiModel(value="FiInvoiceRegistration对象", description="")
public class FiInvoiceRegistration extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "业务类型")
    @TableField("BUSINESS_TYPE")
    private String businessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("MAT_DN_ID")
    private String matDnId;

    @ApiModelProperty(value = "发票号码")
    @TableField("INVOICE_NUM")
    private String invoiceNum;

    @ApiModelProperty(value = "供应商编码")
    @TableField("PO_SUPPLIER_ID")
    private String poSupplierId;

    @ApiModelProperty(value = "地点")
    @TableField("SITE_CODE")
    private String siteCode;

    @ApiModelProperty(value = "单据金额")
    @TableField("DOCUMENT_AMOUNT")
    private BigDecimal documentAmount;

    @ApiModelProperty(value = "单据已登记金额")
    @TableField("AMOUNT_OF_DOCUMENT_REGISTRATION")
    private BigDecimal amountOfDocumentRegistration;

    @ApiModelProperty(value = "单据已付款金额")
    @TableField("PAYMENT_AMOUNT_OF_DOCUMENTS")
    private BigDecimal paymentAmountOfDocuments;

    @ApiModelProperty(value = "创建日期")
    @TableField("CREATION_DATE")
    private String creationDate;

    @ApiModelProperty(value = "创建人")
    @TableField("FOUNDER")
    private String founder;


}
