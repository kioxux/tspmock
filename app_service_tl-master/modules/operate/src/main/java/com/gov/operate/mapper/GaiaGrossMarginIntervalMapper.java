package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.entity.GaiaGrossMarginInterval;

/**
 * @author Zhangchi
 * @since 2021/09/22/9:41
 */
public interface GaiaGrossMarginIntervalMapper extends BaseMapper<GaiaGrossMarginInterval> {

}
