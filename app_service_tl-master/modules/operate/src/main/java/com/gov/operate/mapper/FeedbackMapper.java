package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.operate.dto.FeedbackDTO;
import com.gov.operate.dto.GetFeedBackDTO;
import com.gov.operate.entity.Feedback;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-11
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

    /**
     * 建议反馈列表
     */
    IPage<FeedbackDTO> getFeedBackList(Page<FeedbackDTO> page, @Param("ew") QueryWrapper<FeedbackDTO> ew);

    /**
     * 获取详情
     */
    GetFeedBackDTO getById(@Param("id") String id);
}
