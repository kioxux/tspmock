package com.gov.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHORITY_ROLES_AUTHS")
@ApiModel(value="AuthorityRolesAuths对象", description="")
public class AuthorityRolesAuths extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "角色Id")
    @TableField("GARA_ROLE_ID")
    private Integer garaRoleId;

    @ApiModelProperty(value = "权限Id")
    @TableField("GARA_AUTH_ID")
    private Integer garaAuthId;

    @ApiModelProperty(value = "创建日期")
    @TableField("GARA_CJRQ")
    private String garaCjrq;

    @ApiModelProperty(value = "创建时间")
    @TableField("GARA_CJSJ")
    private String garaCjsj;

    @ApiModelProperty(value = "创建人")
    @TableField("GARA_CJR")
    private String garaCjr;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("GARA_CJRXM")
    private String garaCjrxm;

    @ApiModelProperty(value = "是否删除0是/1否（默认否）")
    @TableField("GARA_SFSQ")
    private String garaSfsq;

    @ApiModelProperty(value = "修改日期")
    @TableField("GARA_XGRQ")
    private String garaXgrq;

    @ApiModelProperty(value = "修改时间")
    @TableField("GARA_XGSJ")
    private String garaXgsj;

    @ApiModelProperty(value = "修改人")
    @TableField("GARA_XGR")
    private String garaXgr;

    @ApiModelProperty(value = "修改人姓名")
    @TableField("GARA_XGRXM")
    private String garaXgrxm;


}
