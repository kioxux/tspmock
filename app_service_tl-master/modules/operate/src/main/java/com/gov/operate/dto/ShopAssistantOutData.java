package com.gov.operate.dto;

import lombok.Data;

@Data
public class ShopAssistantOutData {
    private String client;
    private String day;//销售天数
    private String amt;//营业额
    private String grossAmt;//毛利额
    private String userId;
    private String userName;
    private String noQty;//交易数量
    private String average;//日均交易
    private String mcardQty;//会员卡
    private String pct;//客单价

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getGrossAmt() {
        return grossAmt;
    }

    public void setGrossAmt(String grossAmt) {
        this.grossAmt = grossAmt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNoQty() {
        return noQty;
    }

    public void setNoQty(String noQty) {
        this.noQty = noQty;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getMcardQty() {
        return mcardQty;
    }

    public void setMcardQty(String mcardQty) {
        this.mcardQty = mcardQty;
    }
}
