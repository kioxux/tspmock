package com.gov.operate.dto;

import lombok.Data;

import java.util.List;

/**
 * @desc: 添加促销商品查询入参
 * @author: ZhangChi
 * @createTime: 2021/12/31 10:14
 */
@Data
public class SdPromGroupProDto {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品分类
     */
    private String[][] classArr;
    private List<String> proClass;

    /**
     * 品牌区分
     */
    private List<String> proBrand;

    /**
     * 销售级别
     */
    private List<String> gsyGroups;

    /**
     *自定义1
     */
    private List<String> zdy1;

    /**
     *自定义2
     */
    private List<String> zdy2;

    /**
     *自定义3
     */
    private List<String> zdy3;

    /**
     *自定义4
     */
    private List<String> zdy4;

    /**
     *自定义5
     */
    private List<String> zdy5;

    /**
     * 商品key
     */
    private String proKey;

    /**
     * 页码
     */
    private Integer pageNum;

    /**
     * 每页条数
     */
    private Integer pageSize;

    /**
     * 仓库列表
     */
    private List<String> dcList;
}
