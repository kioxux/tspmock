package com.gov.operate.dto.invoice;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PaymentApplicationsGroupKey {
    /**
     * 供应商
     */
    private String supCode;
    /**
     * 地点
     */
    private String proSite;
}
