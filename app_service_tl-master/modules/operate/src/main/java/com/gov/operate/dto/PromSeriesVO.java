package com.gov.operate.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 系列类促销
 * @author: yzf
 * @create: 2022-01-24 12:00
 */
@Data
public class PromSeriesVO {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gspscVoucherId;

    /**
     * 行号
     */
    private String gspscSerial;

    /**
     * 商品编码
     */
    private String gspscProId;

    /**
     * 系列编码
     */
    private String gspscSeriesId;

    /**
     * 是否会员
     */
    private String gspscMemFlag;

    /**
     * 是否积分
     */
    private String gspscInteFlag;

    /**
     * 积分倍率
     */
    private String gspscInteRate;

    /**
     * 达到数量1
     */
    private String gspssReachQty1;

    /**
     * 达到金额1
     */
    private BigDecimal gspssReachAmt1;

    /**
     * 结果减额1
     */
    private BigDecimal gspssResultAmt1;

    /**
     * 结果减额1
     */
    private String gspssResultRebate1;

    /**
     * 达到数量2
     */
    private String gspssReachQty2;

    /**
     * 达到金额2
     */
    private BigDecimal gspssReachAmt2;

    /**
     * 结果减额2
     */
    private BigDecimal gspssResultAmt2;

    /**
     * 结果折扣2
     */
    private String gspssResultRebate2;

    /**
     * 达到数量3
     */
    private String gspssReachQty3;

    /**
     * 达到金额3
     */
    private BigDecimal gspssReachAmt3;

    /**
     * 结果减额3
     */
    private BigDecimal gspssResultAmt3;

    /**
     * 结果折扣3
     */
    private String gspssResultRebate3;
}
