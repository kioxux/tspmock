package com.gov.operate.dto.order;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 钱金华
 * @date 21-2-19 9:46
 */
@Data
public class WorkOrderDto {

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;


    @ApiModelProperty(value = "编号")
    private String gwoCode;

    @NotNull(message = "请选择问题系统")
    @ApiModelProperty(value = "问题系统")
    private Long gwoSystem;

//    @NotBlank(message = "请填写标题")
//    @Size(max = 128,message = "标题最多128个字符")
    @ApiModelProperty(value = "标题")
    private String gwoTitle;

    @NotNull(message = "请选择类型")
    @ApiModelProperty(value = "类型")
    private Long gwoTypeId;

    @NotBlank(message = "缺少客户")
    @Size(max = 10,message = "客户最多10个字符")
    @ApiModelProperty(value = "客户")
    private String gwoClient;

    @NotBlank(message = "缺少客户名")
    @Size(max = 128,message = "客户名最多128个字符")
    @ApiModelProperty(value = "客户名")
    private String gwoClientName;

    @NotBlank(message = "缺少用户ID")
    @Size(max = 50,message = "用户ID最多50个字符")
    @ApiModelProperty(value = "用户ID")
    private String gwoUserId;

    @NotBlank(message = "缺少用户名")
    @Size(max = 50,message = "用户名最多50个字符")
    @ApiModelProperty(value = "用户名")
    private String gwoUserName;

    @NotBlank(message = "缺少手机号")
    @Size(max = 20,message = "手机号最多20个字符")
    @ApiModelProperty(value = "手机号")
    private String gwoUserTel;

    @NotBlank(message = "发起人手机号不能为空")
    @Size(max = 20,message = "手机号最多20个字符")
    @ApiModelProperty(value = "手机号")
    private String gwoSendTel;

    @ApiModelProperty(value = "提交时间")
    private LocalDateTime gwoSendTime;

    @NotNull(message = "请选择级别")
    @ApiModelProperty(value = "级别")
    private Integer gwoLevel;

    @ApiModelProperty(value = "状态")
    private Integer gwoStatus;

    @ApiModelProperty(value = "接单人")
    private Long gwoTaker;

    @ApiModelProperty(value = "接单时间")
    private LocalDateTime gwoOrderTime;

    @ApiModelProperty(value = "当前处理人")
    private Long gwoCurrentTaker;

    @ApiModelProperty(value = "最后回复时间")
    private LocalDateTime gwoReplyTime;

    @ApiModelProperty(value = "备注")
    private String gwoRemarks;

    @ApiModelProperty(value = "关闭标识")
    private Integer gwoCloseFlag;

    @ApiModelProperty(value = "评分标识")
    private Integer gwoScoreFlag;

    @ApiModelProperty(value = "用户评分")
    private Integer gwoUserScore;

    /**
     * 详情
     */
    @Size(max = 1000,message = "详情最多1000个字符")
    @NotBlank(message = "请填写详情")
    private String workOrderContent;

    /**
     * 附件路径
     */
    private List<Attachment> attachments;


}
