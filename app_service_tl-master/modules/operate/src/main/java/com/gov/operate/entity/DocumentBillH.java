package com.gov.operate.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DOCUMENT_BILL_H")
@ApiModel(value = "DocumentBillH对象", description = "")
public class DocumentBillH extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "对账单号")
    @TableField("GDBH_NUM")
    private String gdbhNum;

    @ApiModelProperty(value = "业务类型")
    @TableField("GDBH_BUSINESS_TYPE")
    private String gdbhBusinessType;

    @ApiModelProperty(value = "业务单号")
    @TableField("GDBH_MAT_DN_ID")
    private String gdbhMatDnId;

    @ApiModelProperty(value = "地点")
    @TableField("GDBH_SITE")
    private String gdbhSite;

    @ApiModelProperty(value = "供应商")
    @TableField("GDBH_SUP_CODE")
    private String gdbhSupCode;

    @ApiModelProperty(value = "单据总金额")
    @TableField("GDBH_DOCUMENT_AMOUNT")
    private BigDecimal gdbhDocumentAmount;

    @ApiModelProperty(value = "对账金额")
    @TableField("GDBH_BILL_AMOUNT")
    private BigDecimal gdbhBillAmount;

    @ApiModelProperty(value = "创建人")
    @TableField("GDBH_CREATE_USER")
    private String gdbhCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("GDBH_CREATE_DATE")
    private String gdbhCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GDBH_CREATE_TIME")
    private String gdbhCreateTime;


}
