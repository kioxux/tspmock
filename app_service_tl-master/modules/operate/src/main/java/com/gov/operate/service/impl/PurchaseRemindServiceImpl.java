package com.gov.operate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.basic.WeChatUtils;
import com.gov.common.entity.weChat.SendTempMessage;
import com.gov.common.entity.weChat.SendTempMessageParams;
import com.gov.operate.dto.electron.ElectronReserved;
import com.gov.operate.dto.electron.PurchaseRemindDto;
import com.gov.operate.entity.*;
import com.gov.operate.mapper.*;
import com.gov.operate.service.IPurchaseRemindService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.operate.service.ISdElectronChangeService;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-07-08
 */
@Slf4j
@Service
public class PurchaseRemindServiceImpl extends ServiceImpl<PurchaseRemindMapper, PurchaseRemind> implements IPurchaseRemindService {

    //开发者ID(AppID)：wx2cf5a557e414eaff
    @Value("${purchaseRemind.appID}")
    private String appID;
    //开发者密码(AppSecret)：e943fa982b603ae2d7cf771ad874df16
    @Value("${purchaseRemind.appSecret}")
    private String AppSecret;
    @Resource
    private WeChatUtils weChatUtils;
    @Resource
    private ClSystemParaMapper clSystemParaMapper;
    @Resource
    private SdPromHeadMapper sdPromHeadMapper;
    @Resource
    private SdPromHySetMapper sdPromHySetMapper;
    @Resource
    private SdPromHyrPriceMapper sdPromHyrPriceMapper;
    @Resource
    private ISdElectronChangeService iSdElectronChangeService;
    @Resource
    private SdElectronChangeMapper sdElectronChangeMapper;
    @Resource
    private RedisClient redisClient;
    /**
     * 电子券号
     **/
    private static final String GSEC_ID = "GAIA_SD_ELECTRON_CHANGE_GSEC_ID";

    /**
     * 发送购药提醒
     */
    @Override
    public void sendPurchaseRemind() {
        // 发送对象
        List<PurchaseRemindDto> list = baseMapper.selectPurchaseRemindList();
        // 没有发送对象
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        // 微信token
        Map<String, String> tokenMap = new HashMap<>();
        // 电子券发送
        Map<String, List<SdElectronChange>> electronChangeMap = new HashMap<String, List<SdElectronChange>>();
        // 是否发送电子券设置
        List<ClSystemPara> clSystemParaList = clSystemParaMapper.selectList(
                new LambdaQueryWrapper<ClSystemPara>().eq(ClSystemPara::getGcspId, SdPromServiceImpl.SEND_TICKET_FLAG)
        );
        // 电子券
        Map<String, List<ElectronReserved>> electronReservedMap = new HashMap<>();
        // 电子券号
        Map<String, Integer> gsecIdMap = new HashMap<>();
        // 促销主表
        List<SdPromHead> sdPromHeadList = sdPromHeadMapper.selectHyProm();
        for (PurchaseRemindDto purchaseRemind : list) {
            try {
                // 加盟商
                String client = purchaseRemind.getClient();
                List<ElectronReserved> electronReservedList = electronReservedMap.get(client);
                if (electronReservedList == null) {
                    electronReservedList = sdElectronChangeMapper.selectElectronReserved(client);
                    electronReservedMap.put(client, electronReservedList);
                }
                // 会员卡号
                String memberCard = purchaseRemind.getMemberCard();
                // 手机
                String tel = purchaseRemind.getTel();
                // 单号
                String billNo = purchaseRemind.getBillNo();
                // 微信token
                String token = tokenMap.get(client);
                if (StringUtils.isBlank(token)) {
                    token = weChatUtils.getAccessToken(this.appID, this.AppSecret);
                    if (StringUtils.isBlank(token)) {
                        log.info("微信公众号{}获取token异常。", client);
                        continue;
                    }
                }
                tokenMap.put(client, token);
                // 用户OPENID
                String openId = purchaseRemind.getGsmbcOpenId();
                if (StringUtils.isBlank(openId)) {
                    continue;
                }
                // 发送微信模板消息
                Map<SendTempMessage.ParamValueKey, SendTempMessageParams> data = new HashMap<SendTempMessage.ParamValueKey, SendTempMessageParams>() {{
                    put(SendTempMessage.ParamValueKey.first, new SendTempMessageParams(
                            MessageFormat.format("尊敬的{0}会员您好，您在我店购买的慢病用药即将服用完毕，请按疗程用药", purchaseRemind.getGsmbName())
                            , null));
                    put(SendTempMessage.ParamValueKey.keyword1, new SendTempMessageParams(purchaseRemind.getChTradeName(), null));
                    put(SendTempMessage.ParamValueKey.keyword2, new SendTempMessageParams("1", null));
                    put(SendTempMessage.ParamValueKey.remark, new SendTempMessageParams("采芝灵药房连锁祝您早日康复！", null));
                }};
                // 微信模板消息发送
                boolean result = WeChatUtils.sendTempMessage(token, openId, SendTempMessage.MessageId.PURCHASE_REMIND, data);
                // 发送成功
                if (result) {
                    PurchaseRemind entity = new PurchaseRemind();
                    entity.setIsSend("Y");
                    baseMapper.update(entity,
                            new LambdaQueryWrapper<PurchaseRemind>()
                                    .eq(PurchaseRemind::getClient, purchaseRemind.getClient())
                                    .eq(PurchaseRemind::getTel, purchaseRemind.getTel())
                                    .eq(PurchaseRemind::getBillNo, purchaseRemind.getBillNo())
                                    .eq(PurchaseRemind::getIsSend, "N")
                                    .apply(" DATE_FORMAT( REMIND_DATE, '%Y%m%d' ) = DATE_FORMAT( NOW(), '%Y%m%d' ) ")
                    );
                    // 2.2	发送电子券（增值服务）
                    String key = client + "_" + tel;
                    List<SdElectronChange> sdElectronChangeList = electronChangeMap.get(key);
                    if (!CollectionUtils.isEmpty(sdElectronChangeList)) {
                        continue;
                    }
                    // 发放电子券判断
                    if (CollectionUtils.isEmpty(clSystemParaList)) {
                        continue;
                    }
                    ClSystemPara clSystemPara = clSystemParaList.stream().filter(Objects::nonNull).filter(item -> client.equals(item.getClient())).findFirst().orElse(null);
                    if (clSystemPara == null) {
                        continue;
                    }
                    // 会员促销判断
                    if (CollectionUtils.isEmpty(sdPromHeadList)) {
                        continue;
                    }
                    List<SdPromHead> sdPromHeadClientList = sdPromHeadList.stream().filter(item -> client.equals(item.getClient())).collect(Collectors.toList());
                    if (CollectionUtils.isEmpty(sdPromHeadClientList)) {
                        continue;
                    }
                    // 发放电子券
                    if ("1".equals(clSystemPara.getGcspPara1())) {
                        // 商品
                        String proId = purchaseRemind.getProId();
                        if (StringUtils.isBlank(proId)) {
                            continue;
                        }
                        String[] proIds = StringUtils.split(proId, ",");
                        if (proIds == null || proIds.length == 0) {
                            continue;
                        }
                        // 门店
                        String brId = purchaseRemind.getBrId();
                        if (StringUtils.isBlank(brId)) {
                            continue;
                        }
                        List<SdPromHead> sdPromHeadBrList = sdPromHeadClientList.stream().filter(Objects::nonNull).filter(item -> brId.equals(item.getGsphBrId())).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(sdPromHeadBrList)) {
                            continue;
                        }
                        for (SdPromHead sdPromHead : sdPromHeadBrList) {
                            // 会员特价
                            if (sdPromHead.getGsphVoucherId().startsWith("CXHYJ")) {
                                Integer cnt = sdPromHySetMapper.selectCount(
                                        new LambdaQueryWrapper<SdPromHySet>().eq(SdPromHySet::getClient, client)
                                                .eq(SdPromHySet::getGsphsVoucherId, sdPromHead.getGsphVoucherId())
                                                .in(SdPromHySet::getGsphsProId, proIds)
                                );
                                // 发放电子券
                                if (cnt > 0) {
                                    if (!CollectionUtils.isEmpty(electronReservedList)) {
                                        sdElectronChangeList = new ArrayList<>();
                                        for (ElectronReserved electronReserved : electronReservedList) {
                                            int gseasQty = NumberUtils.toInt(electronReserved.getGseasQty(), 0);
                                            for (int r = 0; r < gseasQty; r++) {
                                                SdElectronChange sdElectronChange = initSdElectronChange(electronReserved, gsecIdMap, client, memberCard, billNo, brId);
                                                sdElectronChangeList.add(sdElectronChange);
                                            }
                                        }
                                        electronChangeMap.put(key, sdElectronChangeList);
                                        break;
                                    }
                                }
                            }
                            // 会员日特价
                            if (sdPromHead.getGsphVoucherId().startsWith("CXHYRJ")) {
                                Integer cnt = sdPromHyrPriceMapper.selectCount(
                                        new LambdaQueryWrapper<SdPromHyrPrice>().eq(SdPromHyrPrice::getClient, client)
                                                .eq(SdPromHyrPrice::getGsphpVoucherId, sdPromHead.getGsphVoucherId())
                                                .in(SdPromHyrPrice::getGsphpProId, proIds)
                                );
                                // 发放电子券
                                if (cnt > 0) {
                                    if (!CollectionUtils.isEmpty(electronReservedList)) {
                                        sdElectronChangeList = new ArrayList<>();
                                        for (ElectronReserved electronReserved : electronReservedList) {
                                            int gseasQty = NumberUtils.toInt(electronReserved.getGseasQty(), 0);
                                            for (int r = 0; r < gseasQty; r++) {
                                                SdElectronChange sdElectronChange = initSdElectronChange(electronReserved, gsecIdMap, client, memberCard, billNo, brId);
                                                sdElectronChangeList.add(sdElectronChange);
                                            }
                                        }
                                        electronChangeMap.put(key, sdElectronChangeList);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.info("微信消息推送失败");
            }
        }
        try {
            // 电子券发放
            if (!electronChangeMap.isEmpty()) {
                log.info("发放电子券:{}", JsonUtils.beanToJson(electronChangeMap));
                for (List<SdElectronChange> sdElectronChangeList : electronChangeMap.values()) {
                    if (!CollectionUtils.isEmpty(sdElectronChangeList)) {
                        iSdElectronChangeService.saveBatch(sdElectronChangeList);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("电子券发放失败");
        }
    }

    private SdElectronChange initSdElectronChange(ElectronReserved electronReserved, Map<String, Integer> gsecIdMap,
                                                  String client, String memberCard, String billNo, String brId) {
        SdElectronChange sdElectronChange = new SdElectronChange();
        // 加盟商
        sdElectronChange.setClient(client);
        // 会员卡号
        sdElectronChange.setGsecMemberId(memberCard);
        // 电子券号
        String gsebId = electronReserved.getGsebId();
        Integer gsecId = gsecIdMap.get(gsebId);
        if (gsecId == null) {
            gsecId = sdElectronChangeMapper.selectGsecId(client, gsebId);
            if (gsecId == null) {
                gsecId = 0;
            }
        }
        gsecId++;
        while (redisClient.exists(GSEC_ID + gsebId + gsecId.toString())) {
            gsecId++;
        }
        redisClient.set(GSEC_ID + gsebId + gsecId.toString(), "1", 300);
        gsecIdMap.put(gsebId, gsecId);
        String gsedCode = StringUtils.leftPad(String.valueOf(gsecId), 10, "0");
        sdElectronChange.setGsecId(gsebId + gsedCode);
        // 电子券活动号
        sdElectronChange.setGsebId(electronReserved.getGsebId());
        // 是否使用  N为否，Y为是
        sdElectronChange.setGsecStatus("N");
        // 电子券金额
        sdElectronChange.setGsecAmt(electronReserved.getGsebAmt());
        // 电子券描述
        sdElectronChange.setGsebName(electronReserved.getGsebName());
        // 途径  0为线下，1为线上
        sdElectronChange.setGsecFlag("1");
        // 销售单号
        sdElectronChange.setGsecBillNo(billNo);
        // 销售门店
        sdElectronChange.setGsecBrId(brId);
        // 销售日期
        // 创建日期
        sdElectronChange.setGsecCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 失效日期
        return sdElectronChange;
    }
}
