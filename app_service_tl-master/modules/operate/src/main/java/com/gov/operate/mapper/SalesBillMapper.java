package com.gov.operate.mapper;

import com.gov.operate.entity.SalesBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-02-08
 */
public interface SalesBillMapper extends BaseMapper<SalesBill> {

}
