package com.gov.operate.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.operate.dto.DeleteProGroupVO;
import com.gov.operate.dto.GetProGroupListPageDTO;
import com.gov.operate.dto.GetProGroupListPageVO;
import com.gov.operate.dto.SaveProGroupVO;
import com.gov.operate.entity.SdYxmclass;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
public interface ISdYxmclassService extends SuperService<SdYxmclass> {

    /**
     * 获取商品组和商品数据
     * @return
     * @param gsmStore 门店编码
     */
    Result selectGroupAndProductList(String gsmStore);

    /**
     * 商品组列表
     */
    List<SdYxmclass> getProGroupList();

    /**
     * 商品组列表分页
     */
    IPage<GetProGroupListPageDTO> getProGroupListPage(GetProGroupListPageVO vo);

    /**
     * 商品组删除
     */
    void deleteProGroup(DeleteProGroupVO vo);

    /**
     * 商品组保存
     */
    void saveProGroup(SaveProGroupVO vo);

    /**
     * 商品组所含商品列表
     */
    List<SdYxmclass> getProListOfGroup(String gsyGroup, String gsyStore);
}
