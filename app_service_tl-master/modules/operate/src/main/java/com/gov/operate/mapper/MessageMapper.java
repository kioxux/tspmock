package com.gov.operate.mapper;

import com.gov.operate.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-23
 */
public interface MessageMapper extends BaseMapper<Message> {

    /**
     * 批量插入
     *
     * @param messageList messageList
     * @return int
     */
    int insertBatch(List<Message> messageList);

}
