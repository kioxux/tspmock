package com.gov.operate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.operate.dto.GaiaArea;
import com.gov.operate.dto.RegionalPlanningModel;
import com.gov.operate.entity.GaiaRegionalPlanningModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Zhangchi
 * @since 2021/10/15/10:15
 */
public interface GaiaRegionalPlanningModelMapper extends BaseMapper<GaiaRegionalPlanningModel> {
    List<RegionalPlanningModel> getFristArea(@Param("client") String client);

    RegionalPlanningModel getRegionalIdByRegional(@Param("regionalCode") String regionalCode,@Param("client") String client);

    List<GaiaArea> findAreaByLevel(@Param("level")String level);

    List<GaiaArea> findAreaByParentId(@Param("areaId") String areaId);

    GaiaArea findAreaName(@Param("areaId") String areaId);

    GaiaRegionalPlanningModel findRegionalName(@Param("Id")Long Id);

    Long findRegionalId(@Param("Id")Long Id);

    Integer getDefaultIdByClient(@Param("client") String client);
}
