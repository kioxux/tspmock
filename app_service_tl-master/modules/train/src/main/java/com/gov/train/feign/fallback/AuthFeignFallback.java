package com.gov.train.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.response.Result;
import com.gov.train.feign.AuthFeign;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuthFeignFallback implements AuthFeign {


//    @Override
//    public FeignResult createWorkflow(Map<String, Object> param) {
//        FeignResult result = new FeignResult();
//        result.setCode(9999);
//        result.setMessage("调用超时或系统异常，请稍后再试");
//        return result;
//    }

    @Override
    public JSONObject getLoginInfo(Map<String, Object> param) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return (JSONObject) JSONObject.toJSON(result);
    }
}
