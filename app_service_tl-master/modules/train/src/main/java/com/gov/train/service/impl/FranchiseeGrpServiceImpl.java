package com.gov.train.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.train.entity.FranchiseeGrp;
import com.gov.train.mapper.FranchiseeGrpMapper;
import com.gov.train.service.IFranchiseeGrpService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
@Service
public class FranchiseeGrpServiceImpl extends ServiceImpl<FranchiseeGrpMapper, FranchiseeGrp> implements IFranchiseeGrpService {

    /**
     * 加盟商组获取
     */
    @Override
    public List<FranchiseeGrp> getClientGrpList() {
        QueryWrapper<FranchiseeGrp> query = new QueryWrapper<FranchiseeGrp>()
                .select("FRANC_GRPID", "FRANC_GRPNAME")
                .groupBy("FRANC_GRPID", "FRANC_GRPNAME");
        return this.list(query);
    }
}
