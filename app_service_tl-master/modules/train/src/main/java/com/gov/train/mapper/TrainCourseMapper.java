package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.train.dto.CourseTypeDTO;
import com.gov.train.dto.EditCourseOnUsingVO;
import com.gov.train.dto.GetCourseListDTO;
import com.gov.train.entity.TrainCourse;
import com.gov.train.entity.TrainCourseQuestion;
import com.gov.train.entity.TrainCourseQuestionItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface TrainCourseMapper extends BaseMapper<TrainCourse> {

    /**
     * 获取各个类型对应的数量
     */
    List<CourseTypeDTO> countCourseType();

    /**
     * 课件列表
     */
    IPage<GetCourseListDTO> getCourseList(Page<GetCourseListDTO> page, @Param("ew") QueryWrapper<GetCourseListDTO> query);

    /**
     * 课件编辑_课件状态为启用
     */
    void updateCourseOnUsingById(@Param("trainCourse") TrainCourse trainCourseDO);

    /**
     * 更新课件题目_课件状态为启用
     */
    void updateQuestionOnUsingById(@Param("question") TrainCourseQuestion question);

    /**
     * 更新课件题目选项_课件状态为启用
     */
    void updateQuestionItemOnUsingById(@Param("questionItem") TrainCourseQuestionItem questionItem);
}
