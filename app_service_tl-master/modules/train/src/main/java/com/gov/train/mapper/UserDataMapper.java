package com.gov.train.mapper;

import com.gov.train.entity.UserData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-18
 */
public interface UserDataMapper extends BaseMapper<UserData> {

}
