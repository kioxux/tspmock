package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_USER_COURSE_QUESTION")
@ApiModel(value="TrainUserCourseQuestion对象", description="")
public class TrainUserCourseQuestion extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "培训ID")
    @TableId("TRAIN_ID")
    private Long trainId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工ID")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "课件ID")
    @TableField("COURSE_ID")
    private Long courseId;

    @ApiModelProperty(value = "试题ID")
    @TableField("QUESTION_ID")
    private Long questionId;

    @ApiModelProperty(value = "答案")
    @TableField("GTUCQ_ANSWER")
    private String gtucqAnswer;


}
