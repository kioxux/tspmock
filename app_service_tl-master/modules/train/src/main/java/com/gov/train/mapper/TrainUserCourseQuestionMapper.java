package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.train.dto.Accuracy;
import com.gov.train.entity.TrainUserCourseQuestion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
public interface TrainUserCourseQuestionMapper extends BaseMapper<TrainUserCourseQuestion> {

    Integer BatchUpdateAnswer(@Param("answerList") List<TrainUserCourseQuestion> answerList);

    Integer insertList(@Param("answerList") List<TrainUserCourseQuestion> answerList);

    /**
     * 指定培训下总的题目数
     */
    List<Accuracy> countAccuracyTotal(@Param("client") String client, @Param("gtId") Long gtId);

    /**
     * 指定培训下作答正确的题目数
     */
    List<Accuracy> countAccuracyRight(@Param("client") String client, @Param("gtId") Long gtId);
}
