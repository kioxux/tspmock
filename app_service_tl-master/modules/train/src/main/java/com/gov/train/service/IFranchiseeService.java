package com.gov.train.service;

import com.gov.mybatis.SuperService;
import com.gov.train.dto.GetClientListVO;
import com.gov.train.entity.Franchisee;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-10
 */
public interface IFranchiseeService extends SuperService<Franchisee> {

    /**
     * 获取加盟列表
     */
    List<Franchisee> getClientList(GetClientListVO vo);
}
