package com.gov.train.dto;

import com.gov.train.entity.TrainCourse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetCourseListDTO extends TrainCourse {

    /**
     * 创建人姓名
     */
    private String courseCreateUserName;
    /**
     * 类别名称
     */
    private String courseTypeName;
}
