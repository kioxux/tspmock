package com.gov.train.service.impl;

import com.gov.train.entity.TrainUserCourseQuestion;
import com.gov.train.mapper.TrainUserCourseQuestionMapper;
import com.gov.train.service.ITrainUserCourseQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Service
public class TrainUserCourseQuestionServiceImpl extends ServiceImpl<TrainUserCourseQuestionMapper, TrainUserCourseQuestion> implements ITrainUserCourseQuestionService {

}
