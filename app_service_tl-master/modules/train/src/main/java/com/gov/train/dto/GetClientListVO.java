package com.gov.train.dto;

import lombok.Data;

@Data
public class GetClientListVO {
    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 加盟商组ID
     */
    private String francGrpId;

}
