package com.gov.train.dto;

import com.gov.train.entity.TrainUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetTrainUserDTO extends TrainUser {

    /**
     * 姓名
     */
    private String userName;

    /**
     * 手机号
     */
    private String userTel;

    /**
     * 课件集合
     */
    private List<GetTrainUserCourseDTO> courseList;

}
