package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_COURSE_QUESTION_ITEM")
@ApiModel(value="TrainCourseQuestionItem对象", description="")
public class TrainCourseQuestionItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "选项ID")
    @TableId(value = "ITEM_ID", type = IdType.AUTO)
    private Long itemId;

    @ApiModelProperty(value = "试题ID")
    @TableField("QUESTION_ID")
    private Long questionId;

    @ApiModelProperty(value = "值")
    @TableField("ITEM_VALUE")
    private Integer itemValue;

    @ApiModelProperty(value = "选项")
    @TableField("ITEM_NAME")
    private String itemName;


}
