package com.gov.train.mapper;

import com.gov.train.entity.TrainUserCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
public interface TrainUserCourseMapper extends BaseMapper<TrainUserCourse> {

    Integer insertList(@Param("courseList")List<TrainUserCourse> courseList);

}
