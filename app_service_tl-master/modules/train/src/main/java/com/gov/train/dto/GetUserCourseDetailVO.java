package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetUserCourseDetailVO {

    @NotNull(message = "培训id不能为空")
    private Long trainId;

    @NotBlank(message = "加盟商编码不能为空")
    private String client;

    @NotBlank(message = "用户编码不能为空")
    private String userId;

    @Positive(message = "课件id不能为空")
    private Long courseId;
}
