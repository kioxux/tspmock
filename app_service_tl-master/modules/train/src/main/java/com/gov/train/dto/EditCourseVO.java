package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditCourseVO {

    @NotNull(message = "课件id不能为空")
    private Long courseId;

    @NotBlank(message = "课件名称不能为空")
    private String courseName;

    @NotNull(message = "课件模块不能为空")
    private Integer courseType;

    @NotNull(message = "重要程度不能为空")
    private Integer courseLevel;

    @NotNull(message = "课件类型不能为空")
    private Integer courseExtType;

    @NotBlank(message = "课件封面不能为空")
    private String courseCover;

    // @NotBlank(message = "视频附件不能为空")
    private String courseFile;

    // @NotNull(message = "图文内容不能为空")
    private String courseContent;

    @Valid
    @NotEmpty(message = "课件题目不能为空")
    private List<SaveCourseQuestionVO> questionList;
}
