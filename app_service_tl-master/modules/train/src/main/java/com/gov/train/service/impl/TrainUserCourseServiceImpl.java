package com.gov.train.service.impl;

import com.gov.train.entity.TrainUserCourse;
import com.gov.train.mapper.TrainUserCourseMapper;
import com.gov.train.service.ITrainUserCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Service
public class TrainUserCourseServiceImpl extends ServiceImpl<TrainUserCourseMapper, TrainUserCourse> implements ITrainUserCourseService {

}
