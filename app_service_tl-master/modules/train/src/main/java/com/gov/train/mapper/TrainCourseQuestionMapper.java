package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.train.dto.GetUserCourseDetailQuestionDTO;
import com.gov.train.dto.GetUserCourseDetailVO;
import com.gov.train.dto.TrainCourseQuestionDTO;
import com.gov.train.entity.TrainCourseQuestion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface TrainCourseQuestionMapper extends BaseMapper<TrainCourseQuestion> {

    /**
     * 课件题目列表
     */
    List<TrainCourseQuestionDTO> getQuestionList(@Param("courseId") Long courseId);

    /**
     * 指定课件下的题目个数
     */
    Integer getTotalQuestion(@Param("courseId") Long courseId);

    Integer getCorrectQuestion(@Param("trainId") Long trainId,
                               @Param("client") String client,
                               @Param("userId") String userId,
                               @Param("courseId") Long courseId);

    /**
     * 用户答题详情
     */
    List<GetUserCourseDetailQuestionDTO> getUserQuestionList(GetUserCourseDetailVO vo);
}
