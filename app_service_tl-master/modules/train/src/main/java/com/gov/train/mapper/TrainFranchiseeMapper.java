package com.gov.train.mapper;

import com.gov.train.entity.TrainFranchisee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
public interface TrainFranchiseeMapper extends BaseMapper<TrainFranchisee> {

}
