package com.gov.train.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetCourseListVO extends Pageable {

    /**
     * 课件名称
     */
    private String courseName;

    /**
     * 课件模块
     */
    private Integer courseType;

    /**
     * 重要程度（1、掌握 2、熟悉 3、了解）
     */
    private Integer courseLevel;

    /**
     * 课件状态(0启用，1停用)
     */
    private Integer courseStatus;

    /**
     * 课件类型（1：视频课程 2：图文内容）
     */
    private Integer courseExtType;


}
