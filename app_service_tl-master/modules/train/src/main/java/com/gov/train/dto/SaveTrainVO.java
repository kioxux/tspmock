package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SaveTrainVO {

    @NotNull(message = "培训类型不能为空")
    private Integer gtSubject;

    @NotBlank(message = "培训描述不能为空")
    private String gtName;

    @NotNull(message = "时间属性不能为空")
    private Integer gtEndType;

    /**
     * 培训保留期(单位：天)
     */
    private Integer gtDuration;

    /**
     * 培训开始日
     */
    private String gtStartDate;

    /**
     * 培训结束日
     */
    private String gtDeadline;

    @Length(min = 0, max = 10, message = "计划推送时间格式:yyyyMMddHH")
    private String gtPlanPushTime;

    @NotNull(message = "课件推送类型(1:模块随机，2：手动选择)")
    private Integer gtCourseType;

    /**
     * 模块随机列表
     */
    private List<CourseTypeCount> courseTypeCountList;

    /**
     * 手动选择课件列表
     */
    private List<Long> courseIdList;

    @NotEmpty(message = "部门列表不能为空")
    private List<String> clientList;

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class CourseTypeCount{

        @NotNull(message = "课件类型ID不能为空")
        private Long gtctTypeId;

        @NotNull(message = "课件数量不能为空")
        private Integer gtctCount;
    }


}
