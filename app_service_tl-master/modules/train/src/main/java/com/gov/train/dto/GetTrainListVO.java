package com.gov.train.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetTrainListVO extends Pageable {

    /**
     * 培训主旨(1、一般培训	2、考试)
     */
    private Integer gtSubject;

    /**
     * 主题名称
     */
    private String gtName;

    /**
     * 培训时间属性：
     */
    private Integer gtEndType;

    /**
     * 培训截止日期 开始日期
     */
    private String gtDeadlineStart;

    /**
     * 培训截止日期 结束日期
     */
    private String gtDeadlineEnd;

    /**
     * 创建日期开始时间
     */
    private String gtCreateDateStart;

    /**
     * 创建日期结束日期
     */
    private String gtCreateDateEnd;

    /**
     * 培训日期开始
     */
    private String gtStartDate;

    /**
     * 培训日期结束
     */
    private String gtDeadline;

}
