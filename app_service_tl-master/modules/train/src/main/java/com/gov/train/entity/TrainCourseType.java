package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_COURSE_TYPE")
@ApiModel(value="TrainCourseType对象", description="")
public class TrainCourseType extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "培训ID")
    @TableId("GTCT_ID")
    private Long gtctId;

    @ApiModelProperty(value = "课件类型ID")
    @TableField("GTCT_TYPE_ID")
    private Long gtctTypeId;

    @ApiModelProperty(value = "件数")
    @TableField("GTCT_COUNT")
    private Integer gtctCount;


}
