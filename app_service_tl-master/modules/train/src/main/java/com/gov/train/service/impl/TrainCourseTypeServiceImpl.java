package com.gov.train.service.impl;

import com.gov.train.entity.TrainCourseType;
import com.gov.train.mapper.TrainCourseTypeMapper;
import com.gov.train.service.ITrainCourseTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
@Service
public class TrainCourseTypeServiceImpl extends ServiceImpl<TrainCourseTypeMapper, TrainCourseType> implements ITrainCourseTypeService {

}
