package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_COURSE_QUESTION")
@ApiModel(value="TrainCourseQuestion对象", description="")
public class TrainCourseQuestion extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "试题ID")
    @TableId(value = "QUESTION_ID", type = IdType.AUTO)
    private Long questionId;

    @ApiModelProperty(value = "课件ID")
    @TableField("COURSE_ID")
    private Long courseId;

    @ApiModelProperty(value = "代码")
    @TableField("QUESTION_CODE")
    private String questionCode;

    @ApiModelProperty(value = "类别")
    @TableField("QUESTION_TYPE")
    private Integer questionType;

    @ApiModelProperty(value = "题目")
    @TableField("QUESTION_NAME")
    private String questionName;

    @ApiModelProperty(value = "答案")
    @TableField("QUESTION_ANSWER")
    private String questionAnswer;

    @ApiModelProperty(value = "排序")
    @TableField("QUESTION_SORT")
    private Integer questionSort;

    @ApiModelProperty(value = "题目释意")
    @TableField("QUESTION_REMARK")
    private String questionRemark;

    @ApiModelProperty(value = "难易级别")
    @TableField("QUESTION_LEVEL")
    private Integer questionLevel;

}
