package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.train.dto.GetClientListVO;
import com.gov.train.entity.Franchisee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-10
 */
public interface FranchiseeMapper extends BaseMapper<Franchisee> {

    /**
     * 加盟商列表
     */
    List<Franchisee> getClientList(@Param("vo") GetClientListVO vo);
}
