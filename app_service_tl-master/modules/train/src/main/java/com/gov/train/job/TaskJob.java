package com.gov.train.job;

import com.gov.train.service.ITrainService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.12
 */
@Component
public class TaskJob {
    @Resource
    private ITrainService trainService;

    /**
     * 培训推送
     *
     * @param param
     * @return
     */
    @XxlJob("trainPushHandler")
    public ReturnT<String> trainPush(String param) {
        XxlJobHelper.log("培训推送 定时任务开始 ");
        trainService.trainPush();
        XxlJobHelper.log("培训推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

}
