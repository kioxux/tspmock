package com.gov.train.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CourseRequestDTO  {

    @NotNull(message = "课件id不能为空")
    private Long courseId;

    @NotNull(message = "培训id不能为空")
    private Long gtId;

}
