package com.gov.train.dto;

import com.gov.train.entity.TrainCourseQuestion;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class AnswerCourseVO extends TrainCourseQuestion {

    private List<Long> itemList;


}
