package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class EditCourseTypeVO {

    @NotNull(message = "课件类型id不能为空")
    private Long gctId;

    @NotBlank(message = "课件类型名不能为空")
    private String gctName;
}
