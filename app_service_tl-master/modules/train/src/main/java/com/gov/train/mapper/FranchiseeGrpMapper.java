package com.gov.train.mapper;

import com.gov.train.entity.FranchiseeGrp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-06
 */
public interface FranchiseeGrpMapper extends BaseMapper<FranchiseeGrp> {

}
