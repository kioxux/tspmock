package com.gov.train.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.basic.TrainEnum;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.train.dto.*;
import com.gov.train.entity.Train;
import com.gov.train.entity.TrainCourse;
import com.gov.train.entity.TrainUser;
import com.gov.train.mapper.TrainCourseQuestionMapper;
import com.gov.train.mapper.TrainUserMapper;
import com.gov.train.service.ITrainCourseService;
import com.gov.train.service.ITrainService;
import com.gov.train.service.ITrainUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-07
 * @since 2020-08-05
 */
@Service
public class TrainUserServiceImpl extends ServiceImpl<TrainUserMapper, TrainUser> implements ITrainUserService {

    @Resource
    private ITrainService trainService;
    @Resource
    private TrainUserMapper trainUserMapper;
    @Resource
    private TrainCourseQuestionMapper trainCourseQuestionMapper;
    @Resource
    private ITrainCourseService trainCourseService;
    @Resource
    private CosUtils cosUtils;

    /**
     * 参加员工列表
     */
    @Override
    public IPage<GetTrainUserDTO> getTrainUser(GetTrainUserVO vo) {
        Page<GetTrainUserDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        // 获取 培训期限类型
        Train train = trainService.getById(vo.getGtId());
        if (ObjectUtils.isEmpty(train)) {
            throw new CustomResultException(ResultEnum.E0013);
        }
        IPage<GetTrainUserDTO> ipage = null;
        if (Integer.valueOf(TrainEnum.GtEndType.PERMANENT.getCode()).equals(train.getGtEndType())) {
            // 培训期限类型1:永久
            ipage = trainUserMapper.getTrainUserBasePermanent(page, vo);
        } else {
            // 培训期限类型1:期间
            ipage = trainUserMapper.getTrainUserBasePeriod(page, vo);
        }
        // 获取userId 列表
        List<String> userIdList = ipage.getRecords().stream().map(GetTrainUserDTO::getUserId).collect(Collectors.toList());
        Map<String, List<GetTrainUserCourseDTO>> userCourseMap = new HashMap<>();
        List<GetTrainUserDTO> dto = trainUserMapper.getTrainUser(vo.getGtId(), vo.getClient(), userIdList);
        dto.forEach(getTrainUserDTO -> {
            getTrainUserDTO.getCourseList().forEach(course -> {
                // 查询该课件下总的题目数
                Integer totalQuestion = trainCourseQuestionMapper.getTotalQuestion(course.getCourseId());
                course.setTotalQuestion(ObjectUtils.isEmpty(totalQuestion)? 0: totalQuestion);

                // 查询已经回答的题目数
                Integer correctQuestion = trainCourseQuestionMapper.getCorrectQuestion(
                        getTrainUserDTO.getTrainId(), getTrainUserDTO.getClient(), getTrainUserDTO.getUserId(), course.getCourseId());
                course.setCorrectQuestion(ObjectUtils.isEmpty(correctQuestion)? 0: correctQuestion);
            });
            userCourseMap.put(getTrainUserDTO.getUserId(), getTrainUserDTO.getCourseList());
        });
        // 设置课程信息
        if (!CollectionUtils.isEmpty(ipage.getRecords())) {
            ipage.getRecords().forEach(userDTO ->{
                userDTO.setCourseList(userCourseMap.get(userDTO.getUserId()));
            });
        }
        return ipage;
    }

    /**
     * 用户答题详情
     */
    @Override
    public GetUserCourseDetailDTO getTrainUserCourse(GetUserCourseDetailVO vo) {
        // 保证用户参加过该培训
        QueryWrapper<TrainUser> query = new QueryWrapper<TrainUser>()
                // 培训ID
                .eq("TRAIN_ID", vo.getTrainId())
                // 加盟商
                .eq("CLIENT", vo.getClient())
                // 员工ID
                .eq("USER_ID", vo.getUserId());
        TrainUser trainUser = this.getOne(query);
        if (ObjectUtils.isEmpty(trainUser)) {
            throw new CustomResultException(ResultEnum.E0032);
        }

        // 课件详情
        TrainCourse course = trainCourseService.getById(vo.getCourseId());
        if (StringUtils.isEmpty(course)) {
            throw new CustomResultException(ResultEnum.E0029);
        }
        GetUserCourseDetailDTO dto = new GetUserCourseDetailDTO();
        BeanUtils.copyProperties(course, dto);

        // 附件及图片的绝对地址
        dto.setCourseFileUrl(dto.getCourseFile());
        dto.setCourseCoverUrl(dto.getCourseCover());

        // 课件题目列表
        List<GetUserCourseDetailQuestionDTO> questionList = trainCourseQuestionMapper.getUserQuestionList(vo);
        questionList.forEach(question -> {
            // 正确答案
            String[] questionAnswer = question.getQuestionAnswer().split(",");
            List<Integer> questionAnswerList = Arrays.asList(questionAnswer).stream().map(Integer::valueOf).collect(Collectors.toList());
            question.setQuestionAnswerList(questionAnswerList);

            // 用户已选答案
            String gtucqAnswer = question.getGtucqAnswer();
            if (StringUtils.isEmpty(gtucqAnswer)) {
                question.setGtucqAnswerList(new ArrayList<>());
            } else {
                List<Integer> gtucqAnswerList = Arrays.asList(gtucqAnswer.split(",")).stream().map(Integer::valueOf).collect(Collectors.toList());
                question.setGtucqAnswerList(gtucqAnswerList);
            }

        });
        dto.setQuestionList(questionList);

        return dto;
    }
}
