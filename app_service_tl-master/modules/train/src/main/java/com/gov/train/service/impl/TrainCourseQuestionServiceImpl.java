package com.gov.train.service.impl;

import com.gov.train.entity.TrainCourseQuestion;
import com.gov.train.mapper.TrainCourseQuestionMapper;
import com.gov.train.service.ITrainCourseQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Service
public class TrainCourseQuestionServiceImpl extends ServiceImpl<TrainCourseQuestionMapper, TrainCourseQuestion> implements ITrainCourseQuestionService {

}
