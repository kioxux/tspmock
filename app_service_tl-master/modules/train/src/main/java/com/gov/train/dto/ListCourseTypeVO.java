package com.gov.train.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ListCourseTypeVO extends Pageable {

    /**
     * 课件名
     */
    private String gctName;
}
