package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.train.dto.TrainCourseTypeDTO;
import com.gov.train.entity.TrainCourse;
import com.gov.train.entity.TrainCourseType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
public interface TrainCourseTypeMapper extends BaseMapper<TrainCourseType> {

    List<TrainCourseTypeDTO> getTrainCourseTypeList(@Param("gtId") Long gtId);

    List<TrainCourse> selectTrainCourseByTrain(@Param("gtctId") Long gtctId);

    List<TrainCourseType> selectTrainCourseTypeByTrain(@Param("gtctId") Long gtctId);
}
