package com.gov.train.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.train.dto.*;
import com.gov.train.service.ICourseTypeService;
import com.gov.train.service.ITrainCourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Api("课件")
@RestController
@RequestMapping("course")
public class TrainCourseController {

    @Resource
    private ITrainCourseService trainCourseService;
    @Resource
    private ICourseTypeService courseTypeService;

    @Log("课件列表")
    @ApiOperation(value = "课件列表")
    @PostMapping("getCourseList")
    public Result getCourseList(@RequestBody @Valid GetCourseListVO vo) {
        return ResultUtil.success(trainCourseService.getCourseList(vo));
    }

    @Log("课件保存")
    @ApiOperation(value = "课件保存")
    @PostMapping("saveCourse")
    public Result saveCourse(@RequestBody @Valid SaveCourseVO vo) {
        return ResultUtil.success(trainCourseService.saveCourse(vo));
    }

    @Log("课件删除")
    @ApiOperation(value = "课件删除")
    @GetMapping("removeCourse")
    public Result removeCourse(@RequestParam("courseId")Long courseId) {
        trainCourseService.removeCourse(courseId);
        return ResultUtil.success();
    }

    @Log("课件详情")
    @ApiOperation(value = "课件详情")
    @GetMapping("getCourse")
    public Result getCourse(@RequestParam("courseId")Long courseId) {
        return ResultUtil.success(trainCourseService.getCourse(courseId));
    }

    @Log("课件编辑")
    @ApiOperation(value = "课件编辑")
    @PostMapping("editCourse")
    public Result editCourse(@RequestBody @Valid EditCourseVO vo) {
        trainCourseService.editCourse(vo);
        return ResultUtil.success();
    }

    @Log("课件编辑_课件状态为启用")
    @ApiOperation(value = "课件编辑_课件状态为启用")
    @PostMapping("editCourseOnUsing")
    public Result editCourseOnUsing(@RequestBody @Valid EditCourseOnUsingVO vo) {
        trainCourseService.editCourseOnUsing(vo);
        return ResultUtil.success();
    }

    @Log("启用/停用")
    @ApiOperation(value = "启用/停用")
    @GetMapping("startOrEnd")
    public Result startOrEnd(@RequestParam("courseId") Long courseId, @RequestParam("status") Integer status) {
        trainCourseService.startOrEnd(courseId,status);
        return ResultUtil.success();
    }



    @Log("课件类型新增")
    @ApiOperation(value = "课件类型新增")
    @PostMapping("addCourseType")
    public Result addCourseType(@RequestBody @Valid AddCourseTypeVO vo) {
        courseTypeService.addCourseType(vo);
        return ResultUtil.success();
    }

    @Log("课件类型编辑")
    @ApiOperation(value = "课件类型编辑")
    @PostMapping("editCourseType")
    public Result editCourseType(@RequestBody @Valid EditCourseTypeVO vo) {
        courseTypeService.editCourseType(vo);
        return ResultUtil.success();
    }

    @Log("课件类型删除")
    @ApiOperation(value = "课件类型删除")
    @GetMapping("removeCourseType")
    public Result removeCourseType(@RequestParam("gctId") Long gctId) {
        courseTypeService.removeCourseType(gctId);
        return ResultUtil.success();
    }

    @Log("课件类型列表")
    @ApiOperation(value = "课件类型列表")
    @PostMapping("listCourseType")
    public Result listCourseType(@RequestBody @Valid ListCourseTypeVO vo) {
        return ResultUtil.success(courseTypeService.listCourseType(vo));
    }

    @Log("课件类型下拉框")
    @ApiOperation(value = "课件类型下拉框")
    @GetMapping("listCourseTypeDropDown")
    public Result listCourseTypeDropDown(@RequestParam(value = "gctName", required = false) String gctName) {
        return ResultUtil.success(courseTypeService.listCourseTypeDropDown(gctName));
    }


}

