package com.gov.train.service.impl;

import com.gov.train.entity.TrainFranchisee;
import com.gov.train.mapper.TrainFranchiseeMapper;
import com.gov.train.service.ITrainFranchiseeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Service
public class TrainFranchiseeServiceImpl extends ServiceImpl<TrainFranchiseeMapper, TrainFranchisee> implements ITrainFranchiseeService {

}
