package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.train.dto.GetTrainDTO;
import com.gov.train.entity.TrainCourseJoin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
public interface TrainCourseJoinMapper extends BaseMapper<TrainCourseJoin> {

    /**
     * 培训下课件列表[手动新增]
     */
    List<GetTrainDTO.TrainCourseDTO> getTrainCourseList(@Param("gtId") Long gtId);

    /**
     * 手动选择的课件
     *
     * @param gtId
     * @return
     */
    List<TrainCourseJoin> selectTrainCourseList(@Param("gtId") Long gtId);
}
