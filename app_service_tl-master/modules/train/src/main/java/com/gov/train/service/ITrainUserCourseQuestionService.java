package com.gov.train.service;

import com.gov.train.entity.TrainUserCourseQuestion;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
public interface ITrainUserCourseQuestionService extends SuperService<TrainUserCourseQuestion> {

}
