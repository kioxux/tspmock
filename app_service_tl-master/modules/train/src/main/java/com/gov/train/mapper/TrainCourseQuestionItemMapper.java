package com.gov.train.mapper;

import com.gov.train.entity.TrainCourseQuestionItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface TrainCourseQuestionItemMapper extends BaseMapper<TrainCourseQuestionItem> {

}
