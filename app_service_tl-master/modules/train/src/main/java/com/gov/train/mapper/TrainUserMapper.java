package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.train.dto.CourseDTO;
import com.gov.train.dto.GetTrainUserDTO;
import com.gov.train.dto.GetTrainUserVO;
import com.gov.train.entity.TrainUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-07
 */
public interface TrainUserMapper extends BaseMapper<TrainUser> {

    /**
     * 参加员工列表
     */
    List<GetTrainUserDTO> getTrainUser(@Param("gtId") Long gtId, @Param("client") String client, @Param("userIdList") List<String> userIdList);

    /**
     * 获取员工列表,员工基础数据,当培训期限为永久时
     */
    IPage<GetTrainUserDTO> getTrainUserBasePermanent(Page<GetTrainUserDTO> page, @Param("vo") GetTrainUserVO vo);

    /**
     * 获取员工列表,员工基础数据,当培训期限为有效期时
     */
    IPage<GetTrainUserDTO> getTrainUserBasePeriod(Page<GetTrainUserDTO> page, @Param("vo") GetTrainUserVO vo);

    /**
     * 未参加过的培训
     * @param courseType
     * @param client
     * @param userId
     * @param limit
     * @return
     */
    List<CourseDTO> selectNotPartake(@Param("courseType") Long courseType, @Param("client") String client, @Param("userId") String userId, @Param("limit") int limit);

    /**
     * 参加过的非100%
     * @param courseType
     * @param client
     * @param userId
     * @param limit
     * @return
     */
    List<CourseDTO> selectNotAllRight(@Param("courseType") Long courseType, @Param("client") String client, @Param("userId") String userId, @Param("limit") int limit);

    /**
     * 未参加 非100% 以外的数据
     * @param courseType
     * @param client
     * @param userId
     * @return
     */
    List<CourseDTO> selectOtherCourse(@Param("courseType") Long courseType, @Param("client") String client, @Param("userId") String userId, @Param("limit") int limit);
}
