package com.gov.train.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetTrainUserVO extends Pageable {

    @NotNull(message = "培训id不能为空")
    private Long gtId;

    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 用户编码
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String userTel;


}
