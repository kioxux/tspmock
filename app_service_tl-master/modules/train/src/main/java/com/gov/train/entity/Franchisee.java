package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-08-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FRANCHISEE")
@ApiModel(value="Franchisee对象", description="")
public class Franchisee extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "加盟商名称")
    @TableField("FRANC_NAME")
    private String francName;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("FRANC_NO")
    private String francNo;

    @ApiModelProperty(value = "法人")
    @TableField("FRANC_LEGAL_PERSON")
    private String francLegalPerson;

    @ApiModelProperty(value = "质量负责人")
    @TableField("FRANC_QUA")
    private String francQua;

    @ApiModelProperty(value = "详细地址")
    @TableField("FRANC_ADDR")
    private String francAddr;

    @ApiModelProperty(value = "创建日期")
    @TableField("FRANC_CRE_DATE")
    private String francCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("FRANC_CRE_TIME")
    private String francCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("FRANC_CRE_ID")
    private String francCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("FRANC_MODI_DATE")
    private String francModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("FRANC_MODI_TIME")
    private String francModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("FRANC_MODI_ID")
    private String francModiId;

    @ApiModelProperty(value = "LOGO地址")
    @TableField("FRANC_LOGO")
    private String francLogo;

    @ApiModelProperty(value = "公司性质-单体店")
    @TableField("FRANC_TYPE1")
    private String francType1;

    @ApiModelProperty(value = "公司性质-连锁公司")
    @TableField("FRANC_TYPE2")
    private String francType2;

    @ApiModelProperty(value = "公司性质-批发公司")
    @TableField("FRANC_TYPE3")
    private String francType3;

    @ApiModelProperty(value = "委托人账号")
    @TableField("FRANC_ASS")
    private String francAss;


}
