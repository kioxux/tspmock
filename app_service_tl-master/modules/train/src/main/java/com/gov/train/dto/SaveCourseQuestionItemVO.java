package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class SaveCourseQuestionItemVO {

//    @NotNull(message = "值不能为空")
    private Integer itemValue;

    @NotNull(message = "选项不能为空")
    private String itemName;

}
