package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class TrainDownVO {

    @NotNull(message = "培训ID不能为空")
    private Long gtId;

    @NotBlank(message = "停用原因不能为空")
    private String gtReason;

    @NotBlank(message = "停用提示不能为空")
    private String gtPrompt;

    /**
     * 停用时间
     */
    private String gtDownTime;

    /**
     * 停用操作人
     */
    private String gtDownUser;


}
