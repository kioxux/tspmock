package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AddCourseTypeVO {

    /**
     * 课件名
     */
    private String gctName;
}
