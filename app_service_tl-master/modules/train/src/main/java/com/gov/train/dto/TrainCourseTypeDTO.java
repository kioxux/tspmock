package com.gov.train.dto;

import com.gov.train.entity.TrainCourseType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TrainCourseTypeDTO extends TrainCourseType {

    /**
     * 课件类型名称
     */
    private String gctName;
}
