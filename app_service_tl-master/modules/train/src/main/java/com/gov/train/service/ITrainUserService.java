package com.gov.train.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.train.dto.GetTrainUserDTO;
import com.gov.train.dto.GetTrainUserVO;
import com.gov.train.dto.GetUserCourseDetailDTO;
import com.gov.train.dto.GetUserCourseDetailVO;
import com.gov.train.entity.TrainUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-07
 */
public interface ITrainUserService extends SuperService<TrainUser> {

    /**
     * 参加员工列表
     */
    IPage<GetTrainUserDTO> getTrainUser(GetTrainUserVO vo);

    /**
     * 用户答题详情
     */
    GetUserCourseDetailDTO getTrainUserCourse(GetUserCourseDetailVO vo);
}
