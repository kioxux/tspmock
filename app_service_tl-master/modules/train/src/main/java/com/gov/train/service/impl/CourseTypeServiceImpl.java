package com.gov.train.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.train.dto.AddCourseTypeVO;
import com.gov.train.dto.EditCourseTypeVO;
import com.gov.train.dto.ListCourseTypeVO;
import com.gov.train.entity.CourseType;
import com.gov.train.entity.TrainCourse;
import com.gov.train.mapper.CourseTypeMapper;
import com.gov.train.service.ICourseTypeService;
import com.gov.train.service.ITrainCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
@Service
public class CourseTypeServiceImpl extends ServiceImpl<CourseTypeMapper, CourseType> implements ICourseTypeService {

    @Resource
    private ITrainCourseService trainCourseService;

    /**
     * 课件类型列表
     */
    @Override
    public IPage<CourseType> listCourseType(ListCourseTypeVO vo) {
        Page<CourseType> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<CourseType> query = new QueryWrapper<CourseType>()
                .apply(StringUtils.isNotEmpty(vo.getGctName()), "instr(GCT_NAME, {0})", vo.getGctName())
                .orderByDesc("GCT_ID");
        Page<CourseType> courseTypePage = this.page(page, query);
        return courseTypePage;
    }

    /**
     * 新增
     */
    @Override
    public void addCourseType(AddCourseTypeVO vo) {
        CourseType courseType = new CourseType();
        BeanUtils.copyProperties(vo, courseType);
        this.save(courseType);
    }

    /**
     * 编辑
     */
    @Override
    public void editCourseType(EditCourseTypeVO vo) {
        CourseType type = this.getById(vo.getGctId());
        if (ObjectUtils.isEmpty(type)) {
            throw new CustomResultException("该类型不存在");
        }
        CourseType courseType = new CourseType();
        BeanUtils.copyProperties(vo, courseType);
        this.updateById(courseType);
    }

    /**
     * 删除
     */
    @Override
    public void removeCourseType(Long gctId) {
        if (StringUtils.isEmpty(gctId)) {
            throw new CustomResultException("课件类型id不能为空");
        }
        // 如果该课件类型下有课件，则不可删除
        int courseType = trainCourseService.count(new QueryWrapper<TrainCourse>()
                .eq("COURSE_TYPE", gctId)
                .eq("COURSE_DELETED", Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode())));
        if (courseType > 0) {
            throw new CustomResultException("该类型下存在课件，不可删除");
        }
        this.removeById(gctId);
    }

    /**
     * 课件类型下拉框
     */
    @Override
    public List<CourseType> listCourseTypeDropDown(String gctName) {
        QueryWrapper<CourseType> query = new QueryWrapper<CourseType>()
                .apply(StringUtils.isNotEmpty(gctName), "instr(GCT_NAME, {0})", gctName)
                .orderByAsc("GCT_ID");
        return this.list(query);
    }
}
