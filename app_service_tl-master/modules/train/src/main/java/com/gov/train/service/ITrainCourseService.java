package com.gov.train.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.train.dto.*;
import com.gov.train.entity.TrainCourse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface ITrainCourseService extends SuperService<TrainCourse> {

    /**
     * 课件列表
     */
    IPage<GetCourseListDTO> getCourseList(GetCourseListVO vo);

    /**
     * 课件保存
     */
    Long saveCourse(SaveCourseVO vo);

    /**
     * 课件删除
     */
    void removeCourse(Long courseId);

    /**
     * 课件编辑
     */
    void editCourse(EditCourseVO vo);

    /**
     * 启用
     */
    void startOrEnd(Long courseId,Integer status);

    /**
     * 课件详情
     */
    TrainCourseDTO getCourse(Long courseId);

    /**
     * 课件编辑_课件状态为启用
     */
    void editCourseOnUsing(EditCourseOnUsingVO vo);
}
