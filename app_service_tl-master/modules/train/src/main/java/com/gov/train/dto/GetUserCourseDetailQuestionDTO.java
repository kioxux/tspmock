package com.gov.train.dto;

import com.gov.train.entity.TrainCourseQuestion;
import com.gov.train.entity.TrainCourseQuestionItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetUserCourseDetailQuestionDTO extends TrainCourseQuestion {

    /**
     * 正确答案
     */
    private String gtucqAnswer;

    /**
     * 正确答案列表
     */
    private List<Integer> gtucqAnswerList;

    /**
     * 答案列表
     */
    private List<Integer> questionAnswerList;

    /**
     * 选项列表
     */
    private List<TrainCourseQuestionItem> questionItemList;

}
