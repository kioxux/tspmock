package com.gov.train.controller;

import com.gov.common.entity.Pageable;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.train.dto.AnswerQuestionVO;
import com.gov.train.dto.CourseRequestDTO;
import com.gov.train.service.ITrainService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.08.10
 */
@Api(tags = "培训相关")
@RestController
@RequestMapping("task")
public class TrainTaskController {

    @Resource
    private ITrainService iTrainService;

    /**
     * 培训任务
     * @param pageNum
     * @param pageSize
     * @param type 1、永久,2、培训保留期
     * @return
     */
    @Log("APP培训列表")
    @GetMapping("taskList")
    @ApiOperation(value = "培训任务")
    public Result getTaskList(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, @RequestParam("type") Integer type) {
        return iTrainService.getTaskList(pageNum, pageSize, type);
    }

    @Log("APP参加培训")
    @GetMapping("startTrain")
    @ApiOperation(value = "参加培训")
    public Result startTask(Long gtId) {
        return iTrainService.startTask(gtId);
    }

    @Log("学习课件")
    @PostMapping("startCourse")
    @ApiOperation(value = "学习课件")
    public Result startCourse(@Valid @RequestBody CourseRequestDTO courseRequestDTO) {
        return iTrainService.startCourse(courseRequestDTO);
    }
    @Log("学习完成")
    @PostMapping("studyCompleted")
    @ApiOperation(value = "学习完成")
    public Result studyCompleted(@Valid @RequestBody CourseRequestDTO courseRequestDTO) {
        return iTrainService.studyCompleted(courseRequestDTO);
    }
    @Log("答题")
    @PostMapping("answer")
    @ApiOperation(value = "答题")
    public Result answerQuestion(@Valid @RequestBody AnswerQuestionVO answerQuestionVO) {
        return iTrainService.answerQuestion(answerQuestionVO);
    }
    @Log("培训人员")
    @PostMapping("trainers")
    @ApiOperation(value = "培训人员")
    public Result trainersList(Long gtId) {
        return iTrainService.trainersList(gtId);
    }
}
