package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Accuracy {

    private String userId;

    private Integer total;

    private Integer correct;
}
