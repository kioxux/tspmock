package com.gov.train.service.impl;

import com.gov.train.entity.TrainCourseQuestionItem;
import com.gov.train.mapper.TrainCourseQuestionItemMapper;
import com.gov.train.service.ITrainCourseQuestionItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Service
public class TrainCourseQuestionItemServiceImpl extends ServiceImpl<TrainCourseQuestionItemMapper, TrainCourseQuestionItem> implements ITrainCourseQuestionItemService {

}
