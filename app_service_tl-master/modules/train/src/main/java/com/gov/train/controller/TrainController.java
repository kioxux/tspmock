package com.gov.train.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.train.dto.*;
import com.gov.train.service.ITrainService;
import com.gov.train.service.ITrainUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
@RestController
@RequestMapping("train")
public class TrainController {

    @Resource
    private ITrainService trainService;
    @Resource
    private ITrainUserService trainUserService;

    @Log("培训列表")
    @ApiOperation(value = "培训列表")
    @PostMapping("getTrainList")
    public Result getTrainList(@RequestBody @Valid GetTrainListVO vo) {
        return ResultUtil.success(trainService.getTrainList(vo));
    }

    @Log("培训删除")
    @ApiOperation(value = "培训删除")
    @GetMapping("removeTrain")
    public Result removeTrain(@RequestParam("gtId") Long gtId) {
        trainService.removeTrain(gtId);
        return ResultUtil.success();
    }

    @Log("培训新增")
    @ApiOperation(value = "培训新增")
    @PostMapping("saveTrain")
    public Result saveTrain(@Valid @RequestBody SaveTrainVO vo) {
        return ResultUtil.success(trainService.saveTrain(vo));
    }

    @Log("培训修改")
    @ApiOperation(value = "培训修改")
    @PostMapping("editTrain")
    public Result editTrain(@Valid @RequestBody EditTrainVO vo) {
        return ResultUtil.success(trainService.editTrain(vo));
    }

    @Log("培训修改_培训已经发送")
    @ApiOperation(value = "培训修改_培训已经发送")
    @PostMapping("editTrainOnUsing")
    public Result editTrainOnUsing(@Valid @RequestBody EditTrainOnUsingVO vo) {
        return ResultUtil.success(trainService.editTrainOnUsing(vo));
    }

    @Log("培训推送")
    @ApiOperation(value = "培训推送")
    @GetMapping("trainPush")
    public Result trainPush(@RequestParam("gtId") Long gtId) {
        trainService.trainPush(gtId);
        return ResultUtil.success();
    }

    @Log("培训详情")
    @ApiOperation(value = "培训详情")
    @GetMapping("getTrain")
    public Result getTrain(@RequestParam("gtId") Long gtId) {
        return ResultUtil.success(trainService.getTrain(gtId));
    }

    @Log("培训停用")
    @ApiOperation(value = "培训停用")
    @PostMapping("trainDown")
    public Result trainDown(@Valid @RequestBody TrainDownVO vo) {
        trainService.trainDown(vo);
        return ResultUtil.success();
    }

    @Log("参加员工列表")
    @ApiOperation(value = "参加员工列表")
    @PostMapping("getTrainUser")
    public Result getTrainUser(@Valid @RequestBody GetTrainUserVO vo) {
        return ResultUtil.success(trainUserService.getTrainUser(vo));
    }

    @Log("用户答题详情")
    @ApiOperation(value = "用户答题详情")
    @PostMapping("getUserCourseDetail")
    public Result getTrainUserCourse(@Valid @RequestBody GetUserCourseDetailVO vo) {
        return ResultUtil.success(trainUserService.getTrainUserCourse(vo));
    }

    @Log("培训推送测试")
    @ApiOperation(value = "培训推送测试")
    @GetMapping("trainPushHandler")
    public Result trainPush() {
        trainService.trainPush();
        return ResultUtil.success();
    }
}

