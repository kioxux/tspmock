package com.gov.train.controller;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.train.dto.GetClientListVO;
import com.gov.train.service.IFranchiseeGrpService;
import com.gov.train.service.IFranchiseeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api("共通")
@RestController
@RequestMapping("common")
public class CommonController {

    @Resource
    private IFranchiseeService franchiseeService;
    @Resource
    private IFranchiseeGrpService franchiseeGrpService;

    @Log("加盟商查询")
    @ApiOperation(value = "加盟商查询")
    @PostMapping("getClientList")
    public Result getClientList(@RequestBody GetClientListVO vo) {
        return ResultUtil.success(franchiseeService.getClientList(vo));
    }

    @Log("加盟商组获取")
    @ApiOperation(value = "加盟商组获取")
    @GetMapping("getClientGrpList")
    public Result getClientGrpList() {
        return ResultUtil.success(franchiseeGrpService.getClientGrpList());
    }

}
