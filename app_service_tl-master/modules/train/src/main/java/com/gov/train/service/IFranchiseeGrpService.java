package com.gov.train.service;

import com.gov.mybatis.SuperService;
import com.gov.train.entity.FranchiseeGrp;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-21
 */
public interface IFranchiseeGrpService extends SuperService<FranchiseeGrp> {

    /**
     * 加盟商组获取
     */
    List<FranchiseeGrp> getClientGrpList();
}
