package com.gov.train.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class SaveCourseQuestionVO {

//    @NotBlank(message = "题目代码不能为空")
    private String questionCode;

    @NotNull(message = "题目类别不能为空")
    private Integer questionType;

    @NotBlank(message = "题目名称不能为空")
    private String questionName;

    @NotEmpty(message = "题目答案不能为空")
    private List<Integer> questionAnswerList;

    @NotNull(message = "题目编码不能为空")
    private Integer questionSort;

//    @NotBlank(message = "题目释意不能为空")
    private String questionRemark;

    @NotNull(message = "难易级别不能为空(1、难 2、中 3、易)")
    private Integer questionLevel;

    @Valid
    @NotEmpty(message = "题目选项不能为空")
    private List<SaveCourseQuestionItemVO> questionItemList;

}
