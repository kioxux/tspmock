package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_COURSE")
@ApiModel(value="TrainCourse对象", description="")
public class TrainCourse extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "课件ID")
    @TableId(value = "COURSE_ID", type = IdType.AUTO)
    private Long courseId;

    @ApiModelProperty(value = "课件名称")
    @TableField("COURSE_NAME")
    private String courseName;

    @ApiModelProperty(value = "课件类别")
    @TableField("COURSE_TYPE")
    private Integer courseType;

    @ApiModelProperty(value = "重要程度 (1、掌握 2、熟悉 3、了解)")
    @TableField("COURSE_LEVEL")
    private Integer courseLevel;

    @ApiModelProperty(value = "培训附件")
    @TableField("COURSE_FILE")
    private String courseFile;

    @ApiModelProperty(value = "课件图片")
    @TableField("COURSE_COVER")
    private String courseCover;

    @ApiModelProperty(value = "课件状态 (0启用，1停用)")
    @TableField("COURSE_STATUS")
    private Integer courseStatus;

    @ApiModelProperty(value = "发布日期")
    @TableField("COURSE_RELEASE_DATE")
    private String courseReleaseDate;

    @ApiModelProperty(value = "发布时间")
    @TableField("COURSE_RELEASE_TIME")
    private String courseReleaseTime;

    @ApiModelProperty(value = "创建人加盟商")
    @TableField("COURSE_CREATE_CLIENT")
    private String courseCreateClient;

    @ApiModelProperty(value = "创建人")
    @TableField("COURSE_CREATE_USER")
    private String courseCreateUser;

    @ApiModelProperty(value = "创建日期")
    @TableField("COURSE_CREATE_DATE")
    private String courseCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("COURSE_CREATE_TIME")
    private String courseCreateTime;

    @ApiModelProperty(value = "是否删除 (0未删除，1已删除)")
    @TableField("COURSE_DELETED")
    private Integer courseDeleted;

    @ApiModelProperty(value = "课件类型 (1：视频课程 2：图文内容)")
    @TableField("COURSE_EXT_TYPE")
    private Integer courseExtType;

    @ApiModelProperty(value = "COURSE_EXT_TYPE = 2")
    @TableField("COURSE_CONTENT")
    private String courseContent;


}
