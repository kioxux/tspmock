package com.gov.train.dto;

import com.gov.train.entity.TrainCourseQuestion;
import com.gov.train.entity.TrainCourseQuestionItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class TrainCourseQuestionDTO extends TrainCourseQuestion {

    @ApiModelProperty(value = "用户答案答案")
    private String gtucqAnswer;

    /**
     * 答案列表
     */
    private List<Integer> questionAnswerList;

    /**
     * 选项列表
     */
    private List<TrainCourseQuestionItem> questionItemList;
}
