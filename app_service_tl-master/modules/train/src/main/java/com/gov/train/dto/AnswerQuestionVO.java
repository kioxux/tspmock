package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class AnswerQuestionVO {

    @NotNull(message = "培训ID不能为空")
    private Long gtId;

    @NotNull(message = "课件ID不能为空")
    private Long courseId;


    @NotEmpty(message = "课件试题不能为空")
    private List<AnswerCourseVO> questionList;


}
