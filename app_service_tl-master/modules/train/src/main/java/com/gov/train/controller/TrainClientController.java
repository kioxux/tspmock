package com.gov.train.controller;

import com.gov.common.entity.Pageable;
import com.gov.common.response.Result;
import com.gov.train.dto.AnswerQuestionVO;
import com.gov.train.dto.CourseRequestDTO;
import com.gov.train.dto.SaveTrainVO;
import com.gov.train.entity.Train;
import com.gov.train.service.ITrainService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.08.10
 */
@Api(tags = "培训")
@RestController
@RequestMapping("clientTrain")
public class TrainClientController {

    @Resource
    private ITrainService iTrainService;

    @PostMapping("trainList")
    @ApiOperation(value = "培训列表")
    public Result getTaskList(@Valid @RequestBody Pageable pageable) {
        return iTrainService.getTrainClientList(pageable);
    }

    @GetMapping("trainDetail")
    @ApiOperation(value = "培训详情")
    public Result startTask(Long gtId) {
        return iTrainService.getTrainDetail(gtId);
    }

    @PostMapping("addTrain")
    @ApiOperation(value = "培训创建")
    public Result addTrain(@RequestBody SaveTrainVO saveTrainVO) {
        return iTrainService.addTrain(saveTrainVO);
    }

    @GetMapping("statReport")
    @ApiOperation(value = "统计报表 折线图")
    public Result polyline() {
        return iTrainService.polyline();
    }
}
