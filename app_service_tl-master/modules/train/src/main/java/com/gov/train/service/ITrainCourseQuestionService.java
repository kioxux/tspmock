package com.gov.train.service;

import com.gov.train.entity.TrainCourseQuestion;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface ITrainCourseQuestionService extends SuperService<TrainCourseQuestion> {

}
