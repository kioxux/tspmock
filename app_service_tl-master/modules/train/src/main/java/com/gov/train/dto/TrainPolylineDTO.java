package com.gov.train.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TrainPolylineDTO {

    /**
     * 培训id
     */
    private Long trainId;

    /**
     * 培训
     */
    private Long trainName;

    /**
     * 总的题目数
     */
    private Integer total;

    /**
     * 总的答对的题目数
     */
    private Integer correctTotal;

    /**
     * 正确率
     */
    private BigDecimal correctRate;
}
