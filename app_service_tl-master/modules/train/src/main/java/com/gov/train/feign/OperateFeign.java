package com.gov.train.feign;

import com.gov.common.response.Result;
import com.gov.train.feign.dto.MessageParams;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

@Component
@FeignClient(value = "gys-operate")
public interface OperateFeign {

    /**
     * 当前登录人信息
     *
     * @param messageParams
     * @return
     */
    @PostMapping(value = "/push/sendMessageList")
    Result sendMessageList(MessageParams messageParams);
}
