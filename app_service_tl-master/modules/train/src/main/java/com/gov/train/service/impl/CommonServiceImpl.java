package com.gov.train.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.ContextHolderUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.UserLoginModelClient;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.userutil.UserUtils;
import com.gov.redis.jedis.RedisClient;
import com.gov.train.feign.AuthFeign;
import com.gov.train.feign.dto.TokenUser;
import com.gov.train.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class CommonServiceImpl implements CommonService {

    @Resource
    private AuthFeign authFeign;

    @Resource
    private RedisClient redisClient;


    /**
     * 获取登录人信息
     */
    @Override
    public TokenUser getLoginInfo() {
//        String profile = env.getProperty("spring.profiles.active");
//        if (StringUtils.isEmpty(profile)) {
//            TokenUser user = new TokenUser();
//            user.setClient("20200805");
//            user.setUserId("1");
//            return user;
//        }
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isEmpty(token)) {
            log.info("X-Token为空");
            throw new CustomResultException("唯一标识不能为空");
        }
        // 解析失败 PC登录
        if (UserUtils.getToken() == null) {
            Map<String, Object> param = new HashMap<>();
            param.put("token", token);
            log.info("登录人信息！ 提交参数：{}", JsonUtils.beanToJson(param));
            JSONObject object = authFeign.getLoginInfo(param);
            log.info("登录人信息！ 返回结果：{}", object.toJSONString());
            if (object.isEmpty()) {
                log.info("PC登录解析失败1");
                throw new CustomResultException("返回结果异常");
            }
            if (object.containsKey("code")) {
                log.info("PC登录解析失败2");
                throw new CustomResultException(object.getString("message"));
            }
            return object.toJavaObject(TokenUser.class);
        } else {
            // APP、微信登录
            String tokenCode = UserUtils.getTokenCode();
            log.info("tokenCode:{}", tokenCode);
            String userStr = redisClient.get(tokenCode);
            log.info("userStr:{}", userStr);
            if (org.apache.commons.lang3.StringUtils.isBlank(userStr)) {
                log.info("移动端登录解析失败1");
                throw new CustomResultException(ResultEnum.E0007);
            }
            try {
                UserLoginModelClient userLoginModelClient = JsonUtils.jsonToBean(userStr, UserLoginModelClient.class);
                if (userLoginModelClient == null) {
                    log.info("移动端登录解析失败2");
                    throw new CustomResultException(ResultEnum.E0007);
                }
                log.info("UserLoginModelClient:{}", JsonUtils.beanToJson(userLoginModelClient));
                TokenUser tokenUser = new TokenUser();
                tokenUser.setClient(userLoginModelClient.getClient());
                tokenUser.setUserId(userLoginModelClient.getUserId());
                return tokenUser;
            } catch (Exception e) {
                log.info("移动端登录解析失败3");
                throw new CustomResultException(ResultEnum.E0007);
            }
        }
    }
}
