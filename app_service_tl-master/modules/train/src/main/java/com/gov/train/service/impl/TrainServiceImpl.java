package com.gov.train.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.entity.Pageable;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.train.dto.*;
import com.gov.train.entity.*;
import com.gov.train.feign.OperateFeign;
import com.gov.train.feign.dto.MessageParams;
import com.gov.train.feign.dto.TokenUser;
import com.gov.train.mapper.*;
import com.gov.train.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Slf4j
@Service
public class TrainServiceImpl extends ServiceImpl<TrainMapper, Train> implements ITrainService {

    @Resource
    private TrainMapper trainMapper;
    @Resource
    private TrainCourseMapper trainCourseMapper;
    @Resource
    private ITrainFranchiseeService trainFranchiseeService;
    @Resource
    private CommonService commonService;
    @Resource
    private TrainUserMapper trainUserMapper;
    @Resource
    private TrainUserCourseMapper trainUserCourseMapper;
    @Resource
    private TrainUserCourseQuestionMapper trainUserCourseQuestionMapper;
    @Resource
    private TrainFranchiseeMapper trainFranchiseeMapper;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private TrainCourseJoinMapper trainCourseJoinMapper;
    @Resource
    private ICourseTypeService courseTypeService;
    @Resource
    private ITrainCourseTypeService trainCourseTypeService;
    @Resource
    private TrainCourseTypeMapper trainCourseTypeMapper;
    @Resource
    private ITrainCourseJoinService trainCourseJoinService;
    @Resource
    private OperateFeign operateFeign;

    /**
     * 获取培训列表
     * 为过期的，在会员之后建立的过期的
     */
    @Override
    public Result getTaskList(Integer pageNum, Integer pageSize, Integer type) {
        TokenUser user = commonService.getLoginInfo();
        Page<TrainDTO> pageInfo = new Page<TrainDTO>();
        pageInfo.setCurrent(pageNum);
        pageInfo.setSize(pageSize);
        IPage<TrainDTO> taskList = trainMapper.selectPageList(pageInfo, user.getUserId(), user.getClient(), type);
        List<TrainDTO> list = taskList.getRecords();
        if (!CollectionUtils.isEmpty(list)) {
            for (TrainDTO trainDTO : list) {
                // 是否参加过
                int partake = trainDTO.getPartake();
                // 培训期限
                int gt_end_type = trainDTO.getGtEndType();
                // 培训截止日
                String gt_deadline = trainDTO.getGtDeadline();
                // 停用培训
                if (trainDTO.getGtStatus() != null && trainDTO.getGtStatus().intValue() == 0) {
                    trainDTO.setMessage(trainDTO.getGtPrompt());
                    continue;
                }
                // 未参加过当前培训
                if (partake == 0) {
                    // 培训保留期
                    if (TrainEnum.GtEndType.PERIOD.getCode().equals(String.valueOf(gt_end_type))) {
                        // 已过期
                        if (DateUtils.getCurrentDateStrYYMMDD().compareTo(gt_deadline) > 0) {
                            trainDTO.setMessage("当前培训已过期");
                            continue;
                        }
                    }
                    // 随机培训
                    if (trainDTO.getGtCourseType() == null || trainDTO.getGtCourseType().intValue() == 1) {
                        // 验证当前培训的课件类型是否存在有效课件
                        List<TrainCourse> trainCourseList = trainCourseTypeMapper.selectTrainCourseByTrain(trainDTO.getGtId());
                        if (CollectionUtils.isEmpty(trainCourseList)) {
                            trainDTO.setMessage("当前培训已下架");
                            continue;
                        }
                    } else { // 手动课件培训
                        // 手动选择的课件
                        List<TrainCourseJoin> trainCourseJoinList = trainCourseJoinMapper.selectTrainCourseList(trainDTO.getGtId());
                        if (CollectionUtils.isEmpty(trainCourseJoinList)) {
                            throw new CustomResultException("当前培训已下架");
                        }
                        trainDTO.setCourseTotal(trainCourseJoinList.size());
                    }
                } else {
                    //参加过此次培训
                    Map<Object, Object> map = new HashMap<>();
                    map.put("userId", user.getUserId());
                    map.put("client", user.getClient());
                    map.put("gtId", trainDTO.getGtId());
                    TrainDTO info = trainMapper.selectTaskInfo(map);
                    if (info != null && !CollectionUtils.isEmpty(info.getCourseList())) {
                        trainDTO.setCourseTotal(info.getCourseList().size());
                    }
                }
            }
        }
        return ResultUtil.success(taskList);
    }


    /**
     * 获取培训列表,
     * 并插入数据
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result startTask(Long gtId) {
        TokenUser user = commonService.getLoginInfo();
        //查看此次培训各个不同类型课件的数量
        QueryWrapper<Train> queryTrainInfo = new QueryWrapper<Train>();
        //培训
        queryTrainInfo.eq("GT_ID", gtId);
        queryTrainInfo.eq("GT_PUSH", 1);
        Train train = trainMapper.selectOne(queryTrainInfo);
        // 未查询到培训
        if (null == train) {
            throw new CustomResultException("当前培训已下架");
        }
        // 停用培训
        if (train.getGtStatus() != null && train.getGtStatus().intValue() == 0) {
            throw new CustomResultException(train.getGtPrompt());
        }
        QueryWrapper<TrainUser> queryUserRecord = new QueryWrapper<TrainUser>();
        //培训id
        queryUserRecord.eq("TRAIN_ID", gtId);
        //CLIENT
        queryUserRecord.eq("CLIENT", user.getClient());
        //员工ID
        queryUserRecord.eq("USER_ID", user.getUserId());
        Integer record = trainUserMapper.selectCount(queryUserRecord);
        if (record > 0) {
            //参加过此次培训
            Map<Object, Object> map = new HashMap<>();
            map.put("userId", user.getUserId());
            map.put("client", user.getClient());
            map.put("gtId", gtId);
            TrainDTO info = trainMapper.selectTaskInfo(map);
            info.getCourseList().forEach(item -> {
                // 当前培训课件已停用
                if (item.getGtucCourseStatus() == null || item.getGtucCourseStatus().intValue() == 0) {
                    item.setMessage("当前课件已停用");
                }
                item.setCourseCoverUrl(item.getCourseCover());
                item.setCourseFileUrl(item.getCourseFile());
            });
            return ResultUtil.success(info);
        } else {
            //未参加过此次培训
            //1当培训已经过期了 提示用户
            QueryWrapper<Train> queryTrain = new QueryWrapper<Train>();
            //培训
            queryTrain.eq("GT_ID", gtId);
            //培训保留期
            queryTrain.eq("GT_END_TYPE", 2);
            //培训截止日
            queryTrain.lt("GT_DEADLINE", DateUtils.getCurrentDateStrYYMMDD());
            Integer count = trainMapper.selectCount(queryTrain);
            if (count > 0) {
                return ResultUtil.error(ResultEnum.H0001);
            }
            // 课件推送类型
            Integer gt_course_type = train.getGtCourseType();
            List<CourseDTO> trainCourseList = new ArrayList<>();
            int total = 0;
            // 随机
            if (gt_course_type == null || gt_course_type.intValue() == 1) {
                // 验证当前培训的课件类型是否存在有效课件
                List<TrainCourse> extTrainCourseList = trainCourseTypeMapper.selectTrainCourseByTrain(gtId);
                if (CollectionUtils.isEmpty(extTrainCourseList)) {
                    throw new CustomResultException("当前培训已下架");
                }
                // 培训课件类型件数
                List<TrainCourseType> trainCourseTypeList = trainCourseTypeMapper.selectTrainCourseTypeByTrain(gtId);
                if (CollectionUtils.isEmpty(trainCourseTypeList)) {
                    throw new CustomResultException("当前培训已下架");
                }
                // 总件数
                total = trainCourseTypeList.stream().collect(Collectors.summingInt(TrainCourseType::getGtctCount));
                if (total <= 0) {
                    throw new CustomResultException("当前培训已下架");
                }
                // 遍历生成培训课件
                for (TrainCourseType trainCourseType : trainCourseTypeList) {
                    int courseCount = trainCourseType.getGtctCount();
                    initTrainData(trainCourseList, trainCourseType.getGtctTypeId(), courseCount);
                }
            } else {
                // 手动选择的课件
                List<TrainCourseJoin> trainCourseJoinList = trainCourseJoinMapper.selectTrainCourseList(gtId);
                if (CollectionUtils.isEmpty(trainCourseJoinList)) {
                    throw new CustomResultException("当前培训已下架");
                }
                trainCourseJoinList.forEach(item -> {
                    CourseDTO courseDTO = new CourseDTO();
                    courseDTO.setCourseId(item.getGtctCourseId());
                    trainCourseList.add(courseDTO);
                });
            }
            // 查看各个不同类型的课件数量
            TrainDTO dto = new TrainDTO();
            dto.setGtId(train.getGtId());
            dto.setGtName(train.getGtName());
            dto.setGtCreateDate(train.getGtCreateDate());
            dto.setGtCreateTime(train.getGtCreateTime());
            dto.setCourseTotal(total);

            // 课件未生成
            if (CollectionUtils.isEmpty(trainCourseList)) {
                throw new CustomResultException("当前培训已下架");
            }
            //插入培训人员记录表
            TrainUser trainUser = new TrainUser();
            trainUser.setClient(user.getClient());
            trainUser.setTrainId(gtId);
            trainUser.setUserId(user.getUserId());
            trainUser.setGtuStatus(1);
            trainUser.setGtuAddDate(DateUtils.getCurrentDateStrYYMMDD());
            trainUser.setGtuAddTime(DateUtils.getCurrentTimeStrHHMMSS());
            trainUserMapper.insert(trainUser);
            if (CollectionUtils.isEmpty(trainCourseList)) {
                return ResultUtil.success(dto);
            }
            String yyyyMMdd = DateUtils.getCurrentDateStrYYMMDD();
            String HHmmss = DateUtils.getCurrentTimeStrHHMMSS();
            List<TrainUserCourse> trainUserCourseList = new ArrayList<>();
            List<TrainUserCourseQuestion> trainUserCourseQuestionList = new ArrayList<>();
            List<CourseDTO> employeeList = new ArrayList<>();
            trainCourseList.forEach(item -> {
                TrainUserCourse trainUserCourse = new TrainUserCourse();
                // 培训ID
                trainUserCourse.setTrainId(gtId);
                // 加盟商
                trainUserCourse.setClient(user.getClient());
                // 员工ID
                trainUserCourse.setUserId(user.getUserId());
                // 课件ID
                trainUserCourse.setCourseId(item.getCourseId());
                // 参加日期
                trainUserCourse.setGtucAddDate(yyyyMMdd);
                // 参加时间
                trainUserCourse.setGtucAddTime(HHmmss);
                // 状态默认1
                trainUserCourse.setGtucCourseStatus(1);
                trainUserCourseList.add(trainUserCourse);
                employeeList.add(item);
            });
            // 批量插入培训人员课件表
            if (!CollectionUtils.isEmpty(trainUserCourseList)) {
                trainUserCourseMapper.insertList(trainUserCourseList);
                Map<Object, Object> maps = new HashMap<>();
                maps.put("useList", trainUserCourseList);
                List<TrainCourseQuestion> questionList = trainMapper.selectQuestionList(maps);
                if (!CollectionUtils.isEmpty(questionList)) {
                    questionList.forEach(item -> {
                        TrainUserCourseQuestion questionDTO = new TrainUserCourseQuestion();
                        questionDTO.setTrainId(gtId);
                        questionDTO.setClient(user.getClient());
                        questionDTO.setUserId(user.getUserId());
                        questionDTO.setCourseId(item.getCourseId());
                        questionDTO.setQuestionId(item.getQuestionId());
                        trainUserCourseQuestionList.add(questionDTO);
                    });
                }
            }
            //批量插入培训人员答题表
            if (!CollectionUtils.isEmpty(trainUserCourseQuestionList)) {
                trainUserCourseQuestionMapper.insertList(trainUserCourseQuestionList);
            }
            dto.setCourseList(employeeList);
            if (null != dto.getCourseList() && !dto.getCourseList().isEmpty()) {
                dto.getCourseList().forEach(item -> {
                    item.setCourseCoverUrl(item.getCourseCover());
                    item.setCourseFileUrl(item.getCourseFile());
                });
            }
//            return ResultUtil.success(dto);
            //参加过此次培训
            Map<Object, Object> map = new HashMap<>();
            map.put("userId", user.getUserId());
            map.put("client", user.getClient());
            map.put("gtId", gtId);
            TrainDTO info = trainMapper.selectTaskInfo(map);
            info.getCourseList().forEach(item -> {
                // 当前培训课件已停用
                if (item.getGtucCourseStatus() == null || item.getGtucCourseStatus().intValue() == 0) {
                    item.setMessage("当前课件已停用");
                }
                item.setCourseCoverUrl(item.getCourseCover());
                item.setCourseFileUrl(item.getCourseFile());
            });
            return ResultUtil.success(info);
        }
    }

    /**
     * 生成课件题目
     *
     * @param sum
     */
    private void initTrainData(List<CourseDTO> trainCourseList, Long courseType, int sum) {
        TokenUser user = commonService.getLoginInfo();
        int cnt = 0;
        if (sum == 0) {
            return;
        }
        cnt = sum - cnt;
        // 未参加过的课件
        List<CourseDTO> notPartakeList = trainUserMapper.selectNotPartake(courseType, user.getClient(), user.getUserId(), cnt);
        trainCourseList.addAll(notPartakeList);
        cnt = cnt - notPartakeList.size();
        if (cnt <= 0) {
            return;
        }
        // 已参加确率非100%
        List<CourseDTO> notAllRightList = trainUserMapper.selectNotAllRight(courseType, user.getClient(), user.getUserId(), cnt);
        trainCourseList.addAll(notAllRightList);
        cnt = cnt - notAllRightList.size();
        if (cnt <= 0) {
            return;
        }
        // 取剩余课件
        List<CourseDTO> otherCourseList = trainUserMapper.selectOtherCourse(courseType, user.getClient(), user.getUserId(), cnt);
        trainCourseList.addAll(otherCourseList);
    }

    /**
     * flag 为true的情况下去重新查下数据
     *
     * @param collect
     * @param employee
     * @param basic
     * @param manage
     * @param quality
     * @param marketing
     * @return
     */
    private Map<Object, Object> selectList(Map<Integer, List<CourseDTO>> collect, Integer employee, Integer basic, Integer manage, Integer quality, Integer marketing) {
        //已答对的课件
        Map<Object, Object> correctMap = new HashMap<>();
        String flag = "false";

        //判断各个的数量是否够了
        if (employee > 0) {
            //新员工的数量
            if (collect.containsKey(1)) {
                List<CourseDTO> list = collect.get(1);
                Integer Value = employee - list.size();
                if (Value > 0) {
                    //新员工培训
                    correctMap.put("employeeNumber", Value);
                    flag = "true";
                }
            }
        }

        if (basic > 0) {
            //新员工的数量
            if (collect.containsKey(2)) {
                List<CourseDTO> list = collect.get(2);
                Integer Value = basic - list.size();
                if (Value > 0) {
                    //新员工培训
                    correctMap.put("basicNumber", Value);
                    flag = "true";
                }
            }
        }

        if (manage > 0) {
            //新员工的数量
            if (collect.containsKey(3)) {
                List<CourseDTO> list = collect.get(3);
                Integer Value = manage - list.size();
                if (Value > 0) {
                    //新员工培训
                    correctMap.put("manageNumber", Value);
                    flag = "true";
                }
            }
        }

        if (quality > 0) {
            //新员工的数量
            if (collect.containsKey(4)) {
                List<CourseDTO> list = collect.get(4);
                Integer Value = quality - list.size();
                if (Value > 0) {
                    //新员工培训
                    correctMap.put("qualityNumber", Value);
                    flag = "true";
                }
            }
        }

        if (marketing > 0) {
            //新员工的数量
            if (collect.containsKey(5)) {
                List<CourseDTO> list = collect.get(5);
                Integer Value = marketing - list.size();
                if (Value > 0) {
                    //新员工培训
                    correctMap.put("marketingNumber", Value);
                    flag = "true";
                }
            }
        }

        correctMap.put("flag", flag);
        return correctMap;
    }


    /**
     * 开始学习课件
     */
    @Override
    public Result startCourse(CourseRequestDTO courseRequestDTO) {
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("userId", user.getUserId());
        map.put("client", user.getClient());
        map.put("gtId", courseRequestDTO.getGtId());
        map.put("courseId", courseRequestDTO.getCourseId());
        CourseDTO courseDTO = trainMapper.selectCourseInfo(map);
        if (courseDTO == null) {
            throw new CustomResultException("当前课件已停用");
        }
        if (courseDTO.getGtucCourseStatus() == null || courseDTO.getGtucCourseStatus().intValue() == 0) {
            throw new CustomResultException("当前课件已停用");
        }
        if (null != courseDTO) {
            courseDTO.setCourseFileUrl(courseDTO.getCourseFile());
            courseDTO.setCourseCoverUrl(courseDTO.getCourseCover());
        }
        return ResultUtil.success(courseDTO);
    }


    /**
     * 学习完成
     */
    @Override
    public Result studyCompleted(CourseRequestDTO courseRequestDTO) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<Train> queryTrain = new QueryWrapper<Train>();
        //培训
        queryTrain.eq("GT_ID", courseRequestDTO.getGtId());
        //培训保留期
        queryTrain.eq("GT_END_TYPE", 2);
        //培训截止日 小于当前日期
        queryTrain.lt("GT_DEADLINE", DateUtils.getCurrentDateStrYYMMDD());
        Integer train = trainMapper.selectCount(queryTrain);
        if (train > 0) {
            return ResultUtil.error(ResultEnum.H0001);
        }

        QueryWrapper<TrainUserCourse> query = new QueryWrapper<TrainUserCourse>();
        //培训id
        query.eq("TRAIN_ID", courseRequestDTO.getGtId());
        //CLIENT
        query.eq("CLIENT", user.getClient());
        //员工ID
        query.eq("USER_ID", user.getUserId());
        //课件id
        query.eq("COURSE_ID", courseRequestDTO.getCourseId());
        TrainUserCourse course = new TrainUserCourse();
        course.setGtucTrainStatus(0);
        trainUserCourseMapper.update(course, query);
        return ResultUtil.success();
    }


    /**
     * 答题
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result answerQuestion(AnswerQuestionVO answerQuestionVO) {
        TokenUser user = commonService.getLoginInfo();
        QueryWrapper<Train> queryTrain = new QueryWrapper<Train>();
        //培训
        queryTrain.eq("GT_ID", answerQuestionVO.getGtId());
        //培训保留期
        queryTrain.eq("GT_END_TYPE", 2);
        //培训截止日 小于当前日期
        queryTrain.lt("GT_DEADLINE", DateUtils.getCurrentDateStrYYMMDD());
        Integer train = trainMapper.selectCount(queryTrain);
        if (train > 0) {
            return ResultUtil.error(ResultEnum.H0001);
        }
        List<TrainUserCourseQuestion> itemList = new ArrayList<>();
        answerQuestionVO.getQuestionList().forEach(item -> {
            TrainUserCourseQuestion answer = new TrainUserCourseQuestion();
            answer.setTrainId(answerQuestionVO.getGtId());
            answer.setClient(user.getClient());
            answer.setUserId(user.getUserId());
            answer.setCourseId(answerQuestionVO.getCourseId());
            answer.setQuestionId(item.getQuestionId());
            QueryWrapper<TrainUserCourseQuestion> queryWrapper = new QueryWrapper();
            queryWrapper.eq("TRAIN_ID", answerQuestionVO.getGtId());
            queryWrapper.eq("CLIENT", user.getClient());
            queryWrapper.eq("USER_ID", user.getUserId());
            queryWrapper.eq("COURSE_ID", answerQuestionVO.getCourseId());
            queryWrapper.eq("QUESTION_ID", item.getQuestionId());

            //
            if (null != item.getItemList() && item.getItemList().size() > 0) {
                if (item.getItemList().size() == 1) {
                    //单选或者只选了一个答案
                    answer.setGtucqAnswer(item.getItemList().get(0).toString());
                } else {
                    //多选排序,拼接
                    String answerStr = item.getItemList().stream().sorted().map(String::valueOf).collect(Collectors.joining(","));
                    answer.setGtucqAnswer(answerStr);
                }
            }
            trainUserCourseQuestionMapper.update(answer, queryWrapper);
            itemList.add(answer);

        });
//        //批量更新培训人员答题表的用户答案
//        trainUserCourseQuestionMapper.BatchUpdateAnswer(itemList);
        //更新课件的状态为已完成
        QueryWrapper<TrainUserCourse> query = new QueryWrapper<TrainUserCourse>();
        //培训id
        query.eq("TRAIN_ID", answerQuestionVO.getGtId());
        //CLIENT
        query.eq("CLIENT", user.getClient());
        //员工ID
        query.eq("USER_ID", user.getUserId());
        //课件id
        query.eq("COURSE_ID", answerQuestionVO.getCourseId());
        TrainUserCourse course = new TrainUserCourse();
        course.setGtucExamStatus(0);
        trainUserCourseMapper.update(course, query);

        return ResultUtil.success();
    }


    /**
     * 培训列表
     */
    @Override
    public IPage<GetTrainListDTO> getTrainList(GetTrainListVO vo) {
        Page page = new Page(vo.getPageNum(), vo.getPageSize());
        IPage<GetTrainListDTO> iPage = trainMapper.getTrainList(page, vo);
        return iPage;

//
//
//        QueryWrapper<Train> query = new QueryWrapper<Train>()
//                // 培训主旨
//                .eq(StringUtils.isNotEmpty(vo.getGtSubject()), "GT_SUBJECT", vo.getGtSubject())
//                // 培训主题描述
//                .apply(StringUtils.isNotEmpty(vo.getGtName()), "instr(GT_NAME, {0})", vo.getGtName())
//                // 培训时间属性
//                .eq(StringUtils.isNotEmpty(vo.getGtEndType()), "GT_END_TYPE", vo.getGtEndType())
//                // 创建日期开始日期
//                .ge(StringUtils.isNotEmpty(vo.getGtCreateDateStart()), "GT_CREATE_DATE", vo.getGtCreateDateStart())
//                // 创建日期结束日期
//                .le(StringUtils.isNotEmpty(vo.getGtCreateDateEnd()), "GT_CREATE_DATE", vo.getGtCreateDateEnd())
//                // 加盟商为空
//                .eq("GT_CLIENT", "");
//
//
//        // 培训截至日期开始日期
//        if (StringUtils.isNotBlank(vo.getGtStartDate())) {
//            query.le("GT_START_DATE", vo.getGtStartDate());
//            query.ge("GT_DEADLINE", vo.getGtStartDate());
//        }
//        if (StringUtils.isNotBlank(vo.getGtDeadline())) {
//            query.le("GT_START_DATE", vo.getGtDeadline());
//            query.ge("GT_DEADLINE", vo.getGtDeadline());
//        }
//
//
//        // 排序
//        query.orderByDesc("GT_CREATE_DATE", "GT_CREATE_TIME");
//
//        return this.page(page, query);

    }

    /**
     * 培训删除
     */
    @Override
    public void removeTrain(Long gtId) {
        if (StringUtils.isEmpty(gtId)) {
            throw new CustomResultException("培训id不能为空");
        }
        boolean success = this.removeById(gtId);
        if (!success) {
            throw new CustomResultException(ResultEnum.E0030);
        }
    }

    /**
     * 培训新增
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveTrain(SaveTrainVO vo) {
        // 参数校验
        checkParams(vo);
        // [模块随机]列表
        List<SaveTrainVO.CourseTypeCount> courseTypeCountList = vo.getCourseTypeCountList();
        // [手动选择]课件列表
        List<Long> courseIdList = vo.getCourseIdList();
        // 课件推送类型 (1:模块随机，2：手动选择)
        Integer gtCourseType = vo.getGtCourseType();

        TokenUser user = commonService.getLoginInfo();
        Train train = new Train();
        BeanUtils.copyProperties(vo, train);
        // 创建人加盟商
        train.setGtCreateClient(user.getClient());
        // 创建人
        train.setGtCreateUser(user.getUserId());
        // 创建日期
        train.setGtCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 创建时间
        train.setGtCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        // 培训状态 （0：停用，1：启用）") 默认启用 1
        train.setGtStatus(TrainEnum.GtStatus.UP.getCode());
        // 是否推送(0:未推送 1：已推送) 默认未推送 0
        train.setGtPush(0);

        // 1.保存培训
        this.save(train);

        // 2.保存培训/加盟商关系表
        List<TrainFranchisee> list = vo.getClientList().stream().map(client -> {
            TrainFranchisee franchisee = new TrainFranchisee();
            franchisee.setTrainId(train.getGtId());
            franchisee.setClient(client);
            return franchisee;
        }).collect(Collectors.toList());
        trainFranchiseeService.saveBatch(list);

        // 3.1 [模块随机]保存该培训下各种课件类型的数量
        if (TrainEnum.GtCourseType.RANDOM.getCode().equals(gtCourseType)) {
            List<TrainCourseType> trainCourseTypeList = courseTypeCountList.stream().filter(Objects::nonNull)
                    .filter(courseType -> courseType.getGtctCount() > 0)
                    .map(courseType -> {
                        TrainCourseType trainCourseType = new TrainCourseType();
                        BeanUtils.copyProperties(courseType, trainCourseType);
                        trainCourseType.setGtctId(train.getGtId());
                        return trainCourseType;
                    }).collect(Collectors.toList());
            trainCourseTypeService.saveBatch(trainCourseTypeList);
            return train.getGtId();
        }
        // 3.2 [手动选择] 课件各培训的关系表
        if (TrainEnum.GtCourseType.MANUAL.getCode().equals(gtCourseType)) {
            List<TrainCourseJoin> trainCourseJoinList = courseIdList.stream().filter(Objects::nonNull).map(courseId -> {
                TrainCourseJoin trainCourseJoin = new TrainCourseJoin();
                trainCourseJoin.setGtctCourseId(courseId);
                trainCourseJoin.setGtctId(train.getGtId());
                return trainCourseJoin;
            }).collect(Collectors.toList());
            trainCourseJoinService.saveBatch(trainCourseJoinList);
        }
        return train.getGtId();

    }

    /**
     * 培训修改
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long editTrain(EditTrainVO vo) {
        Long gtId = vo.getGtId();
        if (ObjectUtils.isEmpty(gtId)) {
            throw new CustomResultException("培训ID不能为空");
        }
        Train trainExit = this.getById(gtId);
        if (ObjectUtils.isEmpty(trainExit)) {
            throw new CustomResultException("培训ID无效");
        }
        if (1 == trainExit.getGtPush()) {
            throw new CustomResultException("该培训已经推送，不可修改");
        }

        // 参数校验
        SaveTrainVO saveTrainVO = new SaveTrainVO();
        BeanUtils.copyProperties(vo, saveTrainVO);
        checkParams(saveTrainVO);
        // [模块随机]列表
        List<SaveTrainVO.CourseTypeCount> courseTypeCountList = vo.getCourseTypeCountList();
        // [手动选择]课件列表
        List<Long> courseIdList = vo.getCourseIdList();
        // 课件推送类型 (1:模块随机，2：手动选择)
        Integer gtCourseType = vo.getGtCourseType();

        TokenUser user = commonService.getLoginInfo();
        Train trainDO = new Train();
        BeanUtils.copyProperties(vo, trainDO);
        // 1.更新培训主数据
        this.updateById(trainDO);

        // 2.1 删除 培训/加盟商关系表
        trainFranchiseeService.remove(new QueryWrapper<TrainFranchisee>().eq("TRAIN_ID", trainExit.getGtId()));
        // 2.2 新增 培训/加盟商关系表
        List<TrainFranchisee> list = vo.getClientList().stream().map(client -> {
            TrainFranchisee franchisee = new TrainFranchisee();
            franchisee.setTrainId(trainDO.getGtId());
            franchisee.setClient(client);
            return franchisee;
        }).collect(Collectors.toList());
        trainFranchiseeService.saveBatch(list);

        // 3.1.1 [模块随机]删除旧数据
        trainCourseTypeService.remove(new QueryWrapper<TrainCourseType>().eq("GTCT_ID", trainDO.getGtId()));
        // 3.1.2 [手动选择]删除旧数据
        trainCourseJoinService.remove(new QueryWrapper<TrainCourseJoin>().eq("GTCT_ID", trainDO.getGtId()));
        // 3.2.1 [模块随机]新增数据
        if (TrainEnum.GtCourseType.RANDOM.getCode().equals(gtCourseType)) {
            List<TrainCourseType> trainCourseTypeList = courseTypeCountList.stream().filter(Objects::nonNull)
                    .filter(courseType -> courseType.getGtctCount() > 0)
                    .map(courseType -> {
                        TrainCourseType trainCourseType = new TrainCourseType();
                        BeanUtils.copyProperties(courseType, trainCourseType);
                        trainCourseType.setGtctId(trainDO.getGtId());
                        return trainCourseType;
                    }).collect(Collectors.toList());
            trainCourseTypeService.saveBatch(trainCourseTypeList);
            return trainDO.getGtId();
        }
        // 3.2.2 [手动选择]新增数据
        if (TrainEnum.GtCourseType.MANUAL.getCode().equals(gtCourseType)) {
            List<TrainCourseJoin> trainCourseJoinList = courseIdList.stream().filter(Objects::nonNull).map(courseId -> {
                TrainCourseJoin trainCourseJoin = new TrainCourseJoin();
                trainCourseJoin.setGtctCourseId(courseId);
                trainCourseJoin.setGtctId(trainDO.getGtId());
                return trainCourseJoin;
            }).collect(Collectors.toList());
            trainCourseJoinService.saveBatch(trainCourseJoinList);
        }
        return trainDO.getGtId();

    }

    /**
     * 培训新增并且推送  void trainPush(Long gtId);
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void trainPush(Long gtId) {
        if (ObjectUtils.isEmpty(gtId)) {
            throw new CustomResultException("推送培训ID不能为空");
        }
        Train trainExit = this.getById(gtId);
        if (ObjectUtils.isEmpty(trainExit)) {
            throw new CustomResultException("培训ID无效");
        }
        if (1 == trainExit.getGtPush()) {
            throw new CustomResultException("该培训已经推送，不可重复推送");
        }
        // 推送
        Train trainDO = new Train();
        // id
        trainDO.setGtId(gtId);
        // 是否推送(0:未推送 1：已推送)
        trainDO.setGtPush(1);
        // 推送时间
        trainDO.setGtPushTime(DateUtils.getCurrentDateTimeStrTwo());
        this.updateById(trainDO);

        // 推送
        tpnsMsg(new ArrayList<Train>() {{
            add(trainDO);
        }});
    }

    /**
     * 培训新增_参数校验
     */
    private void checkParams(SaveTrainVO vo) {
        // 1.参数校验_培训保留期
        if (Integer.valueOf(TrainEnum.GtEndType.PERIOD.getCode()).equals(vo.getGtEndType())) {
            if (StringUtils.isBlank(vo.getGtStartDate()) || StringUtils.isBlank(vo.getGtDeadline())) {
                throw new CustomResultException("培训日期不能为空");
            }
        }
        // 2.参数校验_课件选择方式
        if (!TrainEnum.GtCourseType.RANDOM.getCode().equals(vo.getGtCourseType())
                && !TrainEnum.GtCourseType.MANUAL.getCode().equals(vo.getGtCourseType())) {
            throw new CustomResultException("课件选择方式不正确");
        }

        // 2.1 参数校验_课件选择方式_[手动选择]_课件总数必须大于0
        if (TrainEnum.GtCourseType.MANUAL.getCode().equals(vo.getGtCourseType())) {
            // 选择的课件总数
            int count = vo.getCourseIdList().size();
            if (count < 1) {
                throw new CustomResultException("[手动选择]方式选择课件总数必须大于0");
            }
            return;
        }

        // 2.2 参数校验_课件选择方式_[随机模块]_课件总数量必须大于0
        List<SaveTrainVO.CourseTypeCount> courseTypeCountList = vo.getCourseTypeCountList();
        int sum = courseTypeCountList.stream().mapToInt(SaveTrainVO.CourseTypeCount::getGtctCount).sum();
        if (sum < 1) {
            throw new CustomResultException("[随机模块]至少选择一种培训课件");
        }
        // 2.2 参数校验，模块选择_每种课件数必须小于当前库已有课件数
        final Map<Integer, Integer> map = trainCourseMapper.countCourseType().stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(CourseTypeDTO::getCourseType, CourseTypeDTO::getCourseTypeCount));
        courseTypeCountList.forEach(courseType -> {
            if (StringUtils.isEmpty(courseType.getGtctCount())) {
                return;
            }
            if (courseType.getGtctCount() < 1) {
                return;
            }
            CourseType type = courseTypeService.getById(courseType.getGtctTypeId());
            if (ObjectUtils.isEmpty(type)) {
                throw new CustomResultException("[随机模块]存在无效的课件类型");
            }
            Integer coureTypeTotal = map.get(courseType.getGtctTypeId().intValue());
            if (ObjectUtils.isEmpty(coureTypeTotal)) {
                throw new CustomResultException(ResultEnum.E0031, type.getGctName(), "0");
            }
            if (courseType.getGtctCount() > coureTypeTotal) {
                throw new CustomResultException(ResultEnum.E0031, type.getGctName(), String.valueOf(coureTypeTotal));
            }
        });
    }

    /**
     * 培训详情
     */
    @Override
    public GetTrainDTO getTrain(Long gtId) {
        if (ObjectUtils.isEmpty(gtId)) {
            throw new CustomResultException("培训id不能为空");
        }
        GetTrainDTO dto = trainMapper.getTrain(gtId);
        if (ObjectUtils.isEmpty(dto)) {
            return dto;
        }
        // [随机模块]课件类型列表
        if (TrainEnum.GtCourseType.RANDOM.getCode().equals(dto.getGtCourseType())) {
            List<TrainCourseTypeDTO> courseTypeCountDto = trainCourseTypeMapper.getTrainCourseTypeList(gtId);
            dto.setCourseTypeCountList(courseTypeCountDto);
        }
        // [手动新增]课件列表
        if (TrainEnum.GtCourseType.MANUAL.getCode().equals(dto.getGtCourseType())) {
            List<GetTrainDTO.TrainCourseDTO> trainCourseList = trainCourseJoinMapper.getTrainCourseList(gtId);
            dto.setCourseList(trainCourseList);
        }
        return dto;
    }

    /**
     * 培训停用
     */
    @Override
    public void trainDown(TrainDownVO vo) {
        Train train = this.getById(vo.getGtId());
        if (ObjectUtils.isEmpty(train)) {
            throw new CustomResultException("该培训不存在");
        }
        if (TrainEnum.GtStatus.DOWN.getCode().equals(train.getGtStatus())) {
            throw new CustomResultException("该培训已经停用,不可重复操作");
        }
        TokenUser user = commonService.getLoginInfo();
        Train trainDO = new Train();
        BeanUtils.copyProperties(vo, trainDO);
        // 停用时间
        trainDO.setGtDownTime(DateUtils.getCurrentDateTimeStrTwo());
        // 停用操作人
        trainDO.setGtDownUser(user.getUserId());
        // 停用操作人加盟商
        trainDO.setGtDownClient(user.getClient());
        // 培训状态
        trainDO.setGtStatus(TrainEnum.GtStatus.DOWN.getCode());

        this.updateById(trainDO);
    }


    /**
     * 培训人员
     */
    @Override
    public Result trainersList(Long gtId) {
        if (StringUtils.isEmpty(gtId)) {
            throw new CustomResultException("培训id不能为空");
        }
        Train train = this.getById(gtId);
        if (ObjectUtils.isEmpty(train)) {
            throw new CustomResultException("该培训不存在");
        }
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        map.put("gtId", gtId);
        map.put("gtEndType", train.getGtEndType());
        List<TrainDTO> userInfoList = trainMapper.selectUserInfoList(map);

        // 题目总数
        List<Accuracy> totalList = trainUserCourseQuestionMapper.countAccuracyTotal(user.getClient(), gtId);
        Map<String, Integer> totalMap = totalList.stream().collect(Collectors.toMap(Accuracy::getUserId, Accuracy::getTotal));
        // 回答正确数
        List<Accuracy> correctList = trainUserCourseQuestionMapper.countAccuracyRight(user.getClient(), gtId);
        Map<String, Integer> correctMap = correctList.stream().collect(Collectors.toMap(Accuracy::getUserId, Accuracy::getCorrect));
        userInfoList.forEach(trainDTO -> {
            Integer total = totalMap.get(trainDTO.getUserId());
            Integer correct = correctMap.get(trainDTO.getUserId());
            // 计算正确率
            if (!ObjectUtils.isEmpty(total) && !ObjectUtils.isEmpty(correct)) {
                BigDecimal accuracy = new BigDecimal(correct).divide(new BigDecimal(total), 4, BigDecimal.ROUND_HALF_UP);
                String accuracyStr = accuracy.multiply(new BigDecimal(100)).stripTrailingZeros().toPlainString() + "%";
                trainDTO.setAccuracyBigDecimal(accuracy);
                trainDTO.setAccuracy(accuracyStr);
            } else {
                trainDTO.setAccuracyBigDecimal(new BigDecimal(0));
                if (trainDTO.getCompleted() > 0) {
                    trainDTO.setAccuracy("0%");
                } else {
                    trainDTO.setAccuracy("");
                }
            }
        });
        // 排序 正确率倒序
        userInfoList.sort((user1, user2) -> user2.getAccuracyBigDecimal().compareTo(user1.getAccuracyBigDecimal()));
        return ResultUtil.success(userInfoList);
    }


    /**
     * 获取培训列表
     * 为过期的，在会员之后建立的过期的
     */
    @Override
    public Result getTrainClientList(Pageable page) {
        TokenUser user = commonService.getLoginInfo();
        Page<Train> pageInfo = new Page<Train>();
        pageInfo.setCurrent(page.getPageNum());
        pageInfo.setSize(page.getPageSize());
        QueryWrapper<Train> query = new QueryWrapper<Train>();
        query.eq("GT_CLIENT", user.getClient());
        query.orderByDesc("GT_CREATE_DATE", "GT_CREATE_TIME");
        IPage<Train> taskList = trainMapper.selectPage(pageInfo, query);
        return ResultUtil.success(taskList);
    }


    /**
     * 培训详情
     */
    @Override
    public Result getTrainDetail(Long gtId) {
        if (gtId == null) {
            throw new CustomResultException("培训id不能为空");
        }
        GetTrainDTO dto = trainMapper.getTrain(gtId);
        if (!ObjectUtils.isEmpty(dto)) {
            List<TrainCourseTypeDTO> courseTypeCountDto = trainCourseTypeMapper.getTrainCourseTypeList(gtId);
            dto.setCourseTypeCountList(courseTypeCountDto);
        }
        return ResultUtil.success(dto);
    }


    /**
     * 创建培训
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addTrain(SaveTrainVO saveTrainVO) {
        if (StringUtils.isEmpty(saveTrainVO.getGtSubject())) {
            throw new CustomResultException("培训主旨不能为空");
        }
        if (StringUtils.isEmpty(saveTrainVO.getGtName())) {
            throw new CustomResultException("培训描述不能为空");
        }
        if (StringUtils.isEmpty(saveTrainVO.getGtEndType())) {
            throw new CustomResultException("培训期限不能为空");
        }
        if (Integer.valueOf(TrainEnum.GtEndType.PERIOD.getCode()).equals(saveTrainVO.getGtEndType())) {
            if (StringUtils.isBlank(saveTrainVO.getGtStartDate()) || StringUtils.isBlank(saveTrainVO.getGtDeadline())) {
                throw new CustomResultException("培训日期不能为空");
            }
        }

        // 参数校验-课件总数量必须大于0
        List<SaveTrainVO.CourseTypeCount> courseTypeCountList = saveTrainVO.getCourseTypeCountList();
        int sum = courseTypeCountList.stream().mapToInt(SaveTrainVO.CourseTypeCount::getGtctCount).sum();
        if (sum < 1) {
            throw new CustomResultException("至少选择一种培训课件");
        }

        // 参数校验，每种课件数必须小于当前库已有课件数
        final Map<Integer, Integer> map = trainCourseMapper.countCourseType().stream()
                .collect(Collectors.toMap(CourseTypeDTO::getCourseType, CourseTypeDTO::getCourseTypeCount));
        courseTypeCountList.forEach(courseType -> {
            if (StringUtils.isEmpty(courseType.getGtctCount())) {
                return;
            }
            if (courseType.getGtctCount() < 1) {
                return;
            }
            CourseType type = courseTypeService.getById(courseType.getGtctTypeId());
            if (ObjectUtils.isEmpty(type)) {
                throw new CustomResultException("存在无效的课件类型");
            }
            Integer coureTypeTotal = map.get(courseType.getGtctTypeId().intValue());
            if (ObjectUtils.isEmpty(coureTypeTotal)) {
                throw new CustomResultException(ResultEnum.E0031, type.getGctName(), "0");
            }
            if (courseType.getGtctCount() > coureTypeTotal) {
                throw new CustomResultException(ResultEnum.E0031, type.getGctName(), String.valueOf(coureTypeTotal));
            }
        });

        TokenUser user = commonService.getLoginInfo();
        Train train = new Train();
        //插入培训表
        BeanUtils.copyProperties(saveTrainVO, train);
        train.setGtClient(user.getClient());
        train.setGtCreateClient(user.getClient());
        train.setGtCreateUser(user.getUserId());
        train.setGtCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        train.setGtCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        train.setGtStatus(1);
        train.setGtPush(1);
        train.setGtCourseType(1);
        train.setGtPushTime(DateUtils.getCurrentDateTimeStrTwo());
        trainMapper.insert(train);

        //插入加盟培训关系表
        TrainFranchisee trainFranchisee = new TrainFranchisee();
        trainFranchisee.setClient(user.getClient());
        trainFranchisee.setTrainId(train.getGtId());
        trainFranchiseeMapper.insert(trainFranchisee);

        // 保存该培训下各种课件类型的数量
        List<TrainCourseType> trainCourseTypeList = courseTypeCountList.stream()
                .filter(courseType -> courseType.getGtctCount() > 0)
                .map(courseType -> {
                    TrainCourseType trainCourseType = new TrainCourseType();
                    BeanUtils.copyProperties(courseType, trainCourseType);
                    trainCourseType.setGtctId(train.getGtId());
                    return trainCourseType;
                }).collect(Collectors.toList());
        trainCourseTypeService.saveBatch(trainCourseTypeList);
        return ResultUtil.success();
    }


    /**
     * 培训折线图
     */
    @Override
    public Result polyline() {
        Map<Object, Object> returnMap = new HashMap<>();
        TokenUser user = commonService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        map.put("userId", user.getUserId());
        //个人
        List<TrainPolylineDTO> userRateList = trainMapper.selectUserRate(map);
        returnMap.put("userList", userRateList);
        QueryWrapper<UserData> userDataWrapper = new QueryWrapper<UserData>();
        userDataWrapper.eq("CLIENT", user.getClient());
        userDataWrapper.eq("USER_ID", user.getUserId());
        UserData userData = userDataMapper.selectOne(userDataWrapper);
        //门店
        map.put("depId", userData.getDepId());
        List<TrainPolylineDTO> userStoreRateList = trainMapper.selectUserStoreRate(map);

        if (!CollectionUtils.isEmpty(userStoreRateList)) {
            userStoreRateList.forEach(item -> {
                if (null != item.getCorrectRate() && !BigDecimal.ZERO.setScale(4, BigDecimal.ROUND_UP).equals(item.getCorrectRate())) {
                    map.put("trainId", item.getTrainId());
                    Integer number = trainMapper.selectUserStoreNumber(map);
                    if (number > 0) {
                        item.setCorrectRate(item.getCorrectRate().divide(new BigDecimal(number), 4, BigDecimal.ROUND_UP));
                    }

                }
            });
        }
        returnMap.put("storeList", userStoreRateList);
        //加盟商
        Map<Object, Object> mapClient = new HashMap<>();
        mapClient.put("client", user.getClient());
        List<TrainPolylineDTO> clientRateList = trainMapper.selectUserRate(mapClient);

        if (!CollectionUtils.isEmpty(clientRateList)) {
            clientRateList.forEach(item -> {
                if (null != item.getCorrectRate() && !BigDecimal.ZERO.setScale(4, BigDecimal.ROUND_UP).equals(item.getCorrectRate())) {
                    QueryWrapper<TrainUser> queryRecord = new QueryWrapper<TrainUser>();
                    queryRecord.eq("CLIENT", user.getClient());
                    queryRecord.eq("TRAIN_ID", item.getTrainId());
                    Integer number = trainUserMapper.selectCount(queryRecord);
                    if (number > 0) {
                        item.setCorrectRate(item.getCorrectRate().divide(new BigDecimal(number), 4, BigDecimal.ROUND_UP));
                    }

                }
            });
        }
        returnMap.put("clientList", clientRateList);

        return ResultUtil.success(returnMap);
    }

    /**
     * 根据 GT_PUSH[是否推送(0:未推送)] + GT_PLAN_PUSH_TIME[计划推送时间,当前小时] -->
     * 修改 GT_PUSH[是否推送(1:已推送)] + GT_PUSH_TIME[推送时间]
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void trainPush() {
        // 当前时间 yyyyMMddHH
        String localDateTime = DateUtils.formatLocalDateTime(LocalDateTime.now(), "yyyyMMddHH");
        // 查询
        List<Train> trainList = this.list(new QueryWrapper<Train>()
                .select("GT_ID")
                .eq("GT_PUSH", 0)
                .eq("GT_STATUS", 1)
                .eq("GT_PLAN_PUSH_TIME", localDateTime));

        List<Train> trainDOList = trainList.stream().filter(Objects::nonNull).map(item -> {
            Train trainDO = new Train();
            // id
            trainDO.setGtId(item.getGtId());
            // 是否推送(1:已推送)
            trainDO.setGtPush(1);
            // 推送时间
            trainDO.setGtPushTime(DateUtils.getCurrentDateTimeStrTwo());
            return trainDO;
        }).collect(Collectors.toList());
        // 更新
        if (!CollectionUtils.isEmpty(trainDOList)) {
            this.updateBatchById(trainDOList);
            // 推送
            tpnsMsg(trainDOList);
        }
    }

    /**
     * 培训修改_培训已经发送
     */
    @Override
    public Long editTrainOnUsing(EditTrainOnUsingVO vo) {
        Train train = trainMapper.selectById(vo.getGtId());
        if (ObjectUtils.isEmpty(train)) {
            throw new CustomResultException("该培训不存在");
        }
        trainMapper.editTrainOnUsing(vo);
        return vo.getGtId();
    }

    /**
     * 培训推送
     *
     * @param trainDOList
     */
    private void tpnsMsg(List<Train> trainDOList) {
        try {
            if (CollectionUtils.isEmpty(trainDOList)) {
                return;
            }
            for (Train train : trainDOList) {
                //查看此次培训各个不同类型课件的数量
                QueryWrapper<Train> trainQueryWrapper = new QueryWrapper<Train>();
                trainQueryWrapper.eq("GT_ID", train.getGtId());
                Train trainEntity = trainMapper.selectOne(trainQueryWrapper);
                if (trainEntity == null) {
                    continue;
                }
                QueryWrapper<TrainFranchisee> trainFranchiseeQueryWrapper = new QueryWrapper<>();
                trainFranchiseeQueryWrapper.eq("TRAIN_ID", train.getGtId());
                List<TrainFranchisee> trainFranchiseeList = trainFranchiseeMapper.selectList(trainFranchiseeQueryWrapper);
                for (TrainFranchisee trainFranchisee : trainFranchiseeList) {
                    QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
                    userDataQueryWrapper.eq("CLIENT", trainFranchisee.getClient());
                    List<UserData> userDataList = userDataMapper.selectList(userDataQueryWrapper);
                    if (CollectionUtils.isEmpty(userDataList)) {
                        continue;
                    }
                    // 1000条数据
                    int toIndex = 1000;
                    for (int i = 0; i < userDataList.size(); i += 1000) {
                        if (i + 1000 > userDataList.size()) {
                            // 下标
                            toIndex = userDataList.size() - i;
                        }
                        List<UserData> sendList = userDataList.subList(i, i + toIndex);
                        List<String> userIdList = sendList.stream().map(UserData::getUserId).collect(Collectors.toList());
                        MessageParams messageParams = new MessageParams();
                        messageParams.setClient(trainFranchisee.getClient());
                        messageParams.setUserIdList(userIdList);
                        messageParams.setContentParmas(new ArrayList<String>() {{
                            add(trainEntity.getGtName());
                            if (TrainEnum.GtEndType.PERMANENT.getCode().equals(trainEntity.getGtEndType().toString())) {
                                add(TrainEnum.GtEndType.PERMANENT.getMessage());
                            } else {
                                // 开始时间
                                LocalDate gtStartDateLd = DateUtils.parseLocalDate(trainEntity.getGtStartDate(), "yyyyMMdd");
                                // 结束时间
                                LocalDate gtDeadlineLd = DateUtils.parseLocalDate(trainEntity.getGtDeadline(), "yyyyMMdd");
                                add(gtStartDateLd.getMonthValue() + "月" + gtStartDateLd.getDayOfMonth() + "日" +
                                        "~" +
                                        gtDeadlineLd.getMonthValue() + "月" + gtDeadlineLd.getDayOfMonth() + "日");
                            }
                        }});
                        messageParams.setId(CommonEnum.gmtId.MSG00007.getCode());
                        log.info("培训推送消息参数:{}", JsonUtils.beanToJson(messageParams));
                        Result result = operateFeign.sendMessageList(messageParams);
                        log.info("培训推送消息结果:{}", JsonUtils.beanToJson(result));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("培训培训异常");
        }
    }
}
