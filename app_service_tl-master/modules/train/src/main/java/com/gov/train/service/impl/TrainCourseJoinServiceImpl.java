package com.gov.train.service.impl;

import com.gov.train.entity.TrainCourseJoin;
import com.gov.train.mapper.TrainCourseJoinMapper;
import com.gov.train.service.ITrainCourseJoinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
@Service
public class TrainCourseJoinServiceImpl extends ServiceImpl<TrainCourseJoinMapper, TrainCourseJoin> implements ITrainCourseJoinService {

}
