package com.gov.train.service;

import com.gov.train.entity.TrainCourseType;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
public interface ITrainCourseTypeService extends SuperService<TrainCourseType> {

}
