package com.gov.train.service;

import com.gov.train.feign.dto.TokenUser;
import org.springframework.stereotype.Service;

/**
 * 内部接口调用接口类
 */
@Service
public interface CommonService {

//    /**
//     * 商品首营流程创建
//     * @param gspInfo
//     * @return
//     */
//    public FeignResult createProductWorkflow(GaiaProductGspinfo gspInfo);

    /**
     * 获取当前登录人
     *
     * @return
     */
    public TokenUser getLoginInfo();


}
