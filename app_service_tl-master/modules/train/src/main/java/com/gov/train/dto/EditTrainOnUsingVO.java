package com.gov.train.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021/4/25 11:37
 */
@Data
public class EditTrainOnUsingVO {

    @NotNull(message = "培训ID不能为空")
    private Long gtId;

    @NotNull(message = "培训类型不能为空")
    private Integer gtSubject;

    @NotBlank(message = "培训描述不能为空")
    private String gtName;

}
