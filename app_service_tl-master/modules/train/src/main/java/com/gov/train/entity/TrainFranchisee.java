package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_FRANCHISEE")
@ApiModel(value="TrainFranchisee对象", description="")
public class TrainFranchisee extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "培训ID")
    @TableId("TRAIN_ID")
    private Long trainId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;


}
