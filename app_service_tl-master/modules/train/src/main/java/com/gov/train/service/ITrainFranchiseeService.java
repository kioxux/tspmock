package com.gov.train.service;

import com.gov.train.entity.TrainFranchisee;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-06
 */
public interface ITrainFranchiseeService extends SuperService<TrainFranchisee> {

}
