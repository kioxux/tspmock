package com.gov.train.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.train.dto.*;
import com.gov.train.entity.Train;
import com.gov.train.entity.TrainCourseQuestion;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
public interface TrainMapper extends BaseMapper<Train> {
    IPage<TrainDTO> selectPageList(Page<TrainDTO> page, @Param("userId") String userId, @Param("client") String client, @Param("type") Integer type);

    TrainDTO selectTaskInfo(Map<Object, Object> map);

    CourseDTO selectCourseInfo(Map<Object, Object> map);

    /**
     * 培训详情
     */
    GetTrainDTO getTrain(@Param("gtId") Long gtId);

    List<TrainCourseQuestion> selectQuestionList(Map<Object, Object> map);

    List<TrainDTO> selectUserInfoList(Map<Object, Object> map);

    List<CourseTypeDTO> selectTypeNumber();

    List<TrainPolylineDTO> selectUserRate(Map<Object, Object> map);

    List<TrainPolylineDTO> selectUserStoreRate(Map<Object, Object> map);

    Integer selectUserStoreNumber(Map<Object, Object> map);

    IPage<GetTrainListDTO> getTrainList(Page page, @Param("getTrainListVO") GetTrainListVO getTrainListVO);

    /**
     * 培训修改_培训已经发送
     */
    void editTrainOnUsing(@Param("vo") EditTrainOnUsingVO vo);
}
