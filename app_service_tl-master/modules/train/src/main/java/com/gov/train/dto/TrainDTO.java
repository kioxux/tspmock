package com.gov.train.dto;

import com.gov.train.entity.Train;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class TrainDTO extends Train {
    @ApiModelProperty(value = "总课件数")
    private Integer courseTotal;

    @ApiModelProperty(value = "已完成数")
    private Integer completed = 0;

    @ApiModelProperty(value = "是否参加过当前培训 0未参加，1已参加")
    private Integer participated = 0;

    @ApiModelProperty(value = "课件列表")
    private List<CourseDTO> courseList;

    @ApiModelProperty(value = "姓名")
    private String userName;

    @ApiModelProperty(value = "手机")
    private String userTel;

    @ApiModelProperty(value = "错误消息")
    private String message;

    /**
     * 是否参加过当前培训
     */
    private Integer partake;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "正确率")
    private String accuracy;

    @ApiModelProperty(value = "正确率")
    private BigDecimal accuracyBigDecimal;

    @ApiModelProperty(value = "门店人员")
    private Integer isStoreUser;
}
