package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CourseTypeDTO {

    /**
     * 类别
     */
    private Integer courseType;

    /**
     * 类别对应的数量
     */
    private Integer courseTypeCount;
}
