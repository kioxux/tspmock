package com.gov.train.dto;

import com.gov.train.entity.Train;
import com.gov.train.entity.TrainCourse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetTrainDTO extends Train {

    /**
     * 加盟商列表
     */
    private List<Franchisee> clientList;

    /**
     * [随机模块]课件类型列表
     */
    private List<TrainCourseTypeDTO> courseTypeCountList;

    /**
     * [手动更新]课件列表
     */
    private List<TrainCourseDTO> courseList;

    @Data
    @EqualsAndHashCode
    public static class TrainCourseDTO extends TrainCourse {

        /**
         * 课件模块名称
         */
        private String courseTypeName;

    }

}
