package com.gov.train.dto;

import com.gov.train.entity.Train;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetTrainListDTO extends Train {

    /**
     * 创建人姓名
     */
    private String gtCreateUserName;
}
