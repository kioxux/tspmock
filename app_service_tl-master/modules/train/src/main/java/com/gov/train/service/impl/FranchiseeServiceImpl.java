package com.gov.train.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.train.dto.GetClientListVO;
import com.gov.train.entity.Franchisee;
import com.gov.train.mapper.FranchiseeMapper;
import com.gov.train.service.IFranchiseeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-10
 */
@Service
public class FranchiseeServiceImpl extends ServiceImpl<FranchiseeMapper, Franchisee> implements IFranchiseeService {

    @Resource
    private FranchiseeMapper franchiseeMapper;

    /**
     * 加盟商列表
     */
    @Override
    public List<Franchisee> getClientList(GetClientListVO vo) {
        List<Franchisee> list = franchiseeMapper.getClientList(vo);
        return list;
    }
}
