package com.gov.train.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.OperateEnum;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.train.dto.*;
import com.gov.train.entity.TrainCourse;
import com.gov.train.entity.TrainCourseQuestion;
import com.gov.train.entity.TrainCourseQuestionItem;
import com.gov.train.entity.TrainUserCourse;
import com.gov.train.feign.dto.TokenUser;
import com.gov.train.mapper.TrainCourseMapper;
import com.gov.train.mapper.TrainCourseQuestionMapper;
import com.gov.train.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Slf4j
@Service
public class TrainCourseServiceImpl extends ServiceImpl<TrainCourseMapper, TrainCourse> implements ITrainCourseService {

    @Resource
    private CommonService commonService;
    @Resource
    private ITrainCourseQuestionService trainCourseQuestionService;
    @Resource
    private ITrainCourseQuestionItemService trainCourseQuestionItemService;
    @Resource
    private TrainCourseQuestionMapper trainCourseQuestionMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private TrainCourseMapper trainCourseMapper;
    @Resource
    private ICourseTypeService courseTypeService;
    @Resource
    private ITrainUserCourseService trainUserCourseService;

    /**
     * 课件列表
     */
    @Override
    public IPage<GetCourseListDTO> getCourseList(GetCourseListVO vo) {
        TokenUser user = commonService.getLoginInfo();
        Page<GetCourseListDTO> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<GetCourseListDTO> query = new QueryWrapper<GetCourseListDTO>()
                // 名称
                .apply(StringUtils.isNotEmpty(vo.getCourseName()), "instr(c.COURSE_NAME, {0})", vo.getCourseName())
                // 课件模块
                .eq(StringUtils.isNotEmpty(vo.getCourseType()), "c.COURSE_TYPE", vo.getCourseType())
                // 重要程度（1、掌握 2、熟悉 3、了解）
                .eq(StringUtils.isNotEmpty(vo.getCourseLevel()), "c.COURSE_LEVEL", vo.getCourseLevel())
                // 课件状态(0启用，1停用)
                .eq(StringUtils.isNotEmpty(vo.getCourseStatus()), "c.COURSE_STATUS", vo.getCourseStatus())
                // 课件类型（1：视频课程 2：图文内容）
                .eq(StringUtils.isNotEmpty(vo.getCourseExtType()), "c.COURSE_EXT_TYPE", vo.getCourseExtType())
                // 未删除
                .eq("c.COURSE_DELETED", Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()))
                // 当前加盟商
                .eq("c.COURSE_CREATE_CLIENT", user.getClient());
        IPage<GetCourseListDTO> courseList = trainCourseMapper.getCourseList(page, query);
        return courseList;

    }

    /**
     * 课件保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveCourse(SaveCourseVO vo) {
        TokenUser user = commonService.getLoginInfo();
        // 附件校验 (1：视频课程 2：图文内容)
        if (vo.getCourseExtType() == 1 && StringUtils.isEmpty(vo.getCourseFile())) {
            throw new CustomResultException("视频附件不能为空");
        }
        if (vo.getCourseExtType() == 2 && StringUtils.isEmpty(vo.getCourseContent())) {
            throw new CustomResultException("图文内容不能为空");
        }

        // 保存课件
        TrainCourse trainCourse = new TrainCourse();
        BeanUtils.copyProperties(vo, trainCourse);
        // 课件状态(默认启用0)
        trainCourse.setCourseStatus(Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()));
        // 创建人加盟商
        trainCourse.setCourseCreateClient(user.getClient());
        // 创建人
        trainCourse.setCourseCreateUser(user.getUserId());
        // 创建日期
        trainCourse.setCourseCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        // 创建时间
        trainCourse.setCourseCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        // 是否删除(默认未删除0)
        trainCourse.setCourseDeleted(Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()));
        boolean success = this.save(trainCourse);
        if (!success) {
            throw new CustomResultException(ResultEnum.E0011);
        }
        // 题目保存
        this.saveQuestionList(vo.getQuestionList(), trainCourse.getCourseId());
        return trainCourse.getCourseId();
    }

    /**
     * 课件删除(逻辑删除)
     *
     * @param courseId
     */
    @Override
    public void removeCourse(Long courseId) {
        TrainCourse trainCourse = this.getById(courseId);
        if (StringUtils.isEmpty(trainCourse)) {
            throw new CustomResultException("课件不存在");
        }
        if (Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()).equals(trainCourse.getCourseStatus())) {
            throw new CustomResultException("课件已经启用不可删除");
        }
        UpdateWrapper<TrainCourse> updateWrapper = new UpdateWrapper<TrainCourse>()
                .set("COURSE_DELETED", Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()))
                .eq("COURSE_ID", courseId);
        boolean update = this.update(updateWrapper);
        if (!update) {
            throw new CustomResultException(ResultEnum.E0030);
        }
    }

    /**
     * 课件编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editCourse(EditCourseVO vo) {
        // 附件校验 (1：视频课程 2：图文内容)
        if (vo.getCourseExtType() == 1 && StringUtils.isEmpty(vo.getCourseFile())) {
            throw new CustomResultException("视频附件不能为空");
        }
        if (vo.getCourseExtType() == 2 && StringUtils.isEmpty(vo.getCourseContent())) {
            throw new CustomResultException("图文内容不能为空");
        }

        // 查询 判断有无
        TrainCourse course = this.getById(vo.getCourseId());
        if (StringUtils.isEmpty(course)) {
            throw new CustomResultException("该数据已经被其他用户删除");
        }
        if (Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()).equals(course.getCourseStatus())) {
            throw new CustomResultException("该课件状态为启用，不可编辑");
        }
        // 更新课件
        TrainCourse trainCourse = new TrainCourse();
        BeanUtils.copyProperties(vo, trainCourse);
        this.updateById(trainCourse);
        // 删除题目
        this.removeQuestionReal(vo.getCourseId());
        // 新增题目
        this.saveQuestionList(vo.getQuestionList(), trainCourse.getCourseId());
    }

    /**
     * 课件编辑_课件状态为启用
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void editCourseOnUsing(EditCourseOnUsingVO vo) {
        // 更新课件
        TrainCourse trainCourseDO = new TrainCourse();
        BeanUtils.copyProperties(vo, trainCourseDO);
        trainCourseMapper.updateCourseOnUsingById(trainCourseDO);
        // 更新课件题目
        vo.getQuestionList().stream().filter(Objects::nonNull).forEach(question -> {
            TrainCourseQuestion trainCourseQuestion = new TrainCourseQuestion();
            BeanUtils.copyProperties(question, trainCourseQuestion);
            // 答案排序
            String questionAnswers = question.getQuestionAnswerList().stream().sorted().map(String::valueOf).collect(Collectors.joining(","));
            trainCourseQuestion.setQuestionAnswer(questionAnswers);
            trainCourseMapper.updateQuestionOnUsingById(trainCourseQuestion);

            // 更新课件题目选项
            List<EditCourseOnUsingVO.TrainCourseQuestionVO.TrainCourseQuestionItemVO> questionItemList = question.getQuestionItemList();
            questionItemList.stream().filter(Objects::nonNull).forEach(item -> {
                TrainCourseQuestionItem questionItem = new TrainCourseQuestionItem();
                BeanUtils.copyProperties(item, questionItem);
                trainCourseMapper.updateQuestionItemOnUsingById(questionItem);
            });
        });
    }

    /**
     * 启用/停用：0启用，1停用
     */
    @Override
    public void startOrEnd(Long courseId, Integer status) {
        // 查询是否存在
        QueryWrapper<TrainCourse> query = new QueryWrapper<TrainCourse>()
                // 名称
                .eq("COURSE_ID", courseId)
                // 未删除
                .eq("COURSE_DELETED", Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()));
        TrainCourse trainCourse = this.getOne(query);
        if (StringUtils.isEmpty(trainCourse)) {
            throw new CustomResultException(ResultEnum.E0029);
        }

        // 课件状态1停用，操作0启用
        if (Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()).equals(trainCourse.getCourseStatus())
                && Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()).equals(status)) {
            // 课件状态改为启用
            // 状态
            trainCourse.setCourseStatus(Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()));
            // 发布日期
            trainCourse.setCourseCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 发布时间
            trainCourse.setCourseCreateTime(DateUtils.getCurrentTimeStrHHMMSS());

            // 课件状态0启用，操作：1停用
        } else if (Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()).equals(trainCourse.getCourseStatus())
                && Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()).equals(status)) {
            // 课件状态改为停用
            trainCourse.setCourseStatus(Integer.valueOf(OperateEnum.YesOrNo.ONE.getCode()));
            // 用户课件状态改为停用
            trainUserCourseService.update(new UpdateWrapper<TrainUserCourse>()
                    .set("GTUC_COURSE_STATUS", Integer.valueOf(OperateEnum.YesOrNo.ZERO.getCode()))
                    .eq("COURSE_ID", courseId));
        }
        this.updateById(trainCourse);
    }

    /**
     * 课件详情
     */
    @Override
    public TrainCourseDTO getCourse(Long courseId) {
        // 课件详情
        TrainCourse course = this.getById(courseId);
        if (StringUtils.isEmpty(course)) {
            throw new CustomResultException(ResultEnum.E0029);
        }

        TrainCourseDTO dto = new TrainCourseDTO();
        BeanUtils.copyProperties(course, dto);

        // 附件及图片的绝对地址
        dto.setCourseFileUrl(dto.getCourseFile());
        dto.setCourseCoverUrl(dto.getCourseCover());

        // 课件题目列表
        List<TrainCourseQuestionDTO> questionList = trainCourseQuestionMapper.getQuestionList(courseId);
        questionList.forEach(question -> {
            String questionAnswer = question.getQuestionAnswer();
            String[] questionAnswerList = questionAnswer.split(",");
            List<Integer> questionAnswerListInt = new ArrayList<>();
            for (String answerItem : questionAnswerList) {
                questionAnswerListInt.add(Integer.valueOf(answerItem));
            }
            question.setQuestionAnswerList(questionAnswerListInt);
        });
        dto.setQuestionList(questionList);
        return dto;
    }

    /**
     * 题目保存
     */
    private void saveQuestionList(List<SaveCourseQuestionVO> list, Long courseId) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        // 保存题目
        List<TrainCourseQuestionItem> itemList = new ArrayList<>();
        for (SaveCourseQuestionVO question : list) {
            TrainCourseQuestion questionDO = new TrainCourseQuestion();
            BeanUtils.copyProperties(question, questionDO);
            // 答案排序
            String questionAnswers = question.getQuestionAnswerList().stream().sorted().map(String::valueOf).collect(Collectors.joining(","));
            questionDO.setQuestionAnswer(questionAnswers);
            // 课件ID
            questionDO.setCourseId(courseId);
            trainCourseQuestionService.save(questionDO);
            int i = 0;
            // 选项
            for (SaveCourseQuestionItemVO item : question.getQuestionItemList()) {
                TrainCourseQuestionItem itemDO = new TrainCourseQuestionItem();
                BeanUtils.copyProperties(item, itemDO);
                // 试题ID
                itemDO.setQuestionId(questionDO.getQuestionId());
                itemDO.setItemValue(i);
                itemList.add(itemDO);
                i++;
            }
        }
        // 保存选项
        trainCourseQuestionItemService.saveBatch(itemList);
    }

    /**
     * 题目删除(真删除)
     */
    private void removeQuestionReal(Long courseId) {
        // 删除题目
        QueryWrapper<TrainCourseQuestion> query = new QueryWrapper<TrainCourseQuestion>().eq("COURSE_ID", courseId);
        List<TrainCourseQuestion> listQuestion = trainCourseQuestionService.list(query);
        trainCourseQuestionService.remove(query);
        if (CollectionUtils.isEmpty(listQuestion)) {
            return;
        }
        // 删除选项
        List<Long> questionIdList = new ArrayList<>();
        listQuestion.forEach(question -> questionIdList.add(question.getQuestionId()));
        QueryWrapper<TrainCourseQuestionItem> queryItem = new QueryWrapper<TrainCourseQuestionItem>().in("QUESTION_ID", questionIdList);
        trainCourseQuestionItemService.remove(queryItem);

    }
}
