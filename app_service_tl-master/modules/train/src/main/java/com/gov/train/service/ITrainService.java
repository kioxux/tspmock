package com.gov.train.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.common.entity.Pageable;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;
import com.gov.train.dto.*;
import com.gov.train.entity.Train;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
public interface ITrainService extends SuperService<Train> {

    /**
     * 获取任务列表
     */
    Result getTaskList(Integer pageNum, Integer pageSize, Integer type);
    /**
     * 开始参加任务
     */
    Result startTask(Long gtId);

    /**
     * 学习课件
     */
    Result startCourse(CourseRequestDTO courseRequestDTO);
    /**
     * 学习完成
     */
    Result studyCompleted(CourseRequestDTO courseRequestDTO);

    /**
     * 答题
     */
    Result answerQuestion(AnswerQuestionVO answerQuestionVO);

    /*
     * 培训列表
     */
    IPage<GetTrainListDTO> getTrainList(GetTrainListVO vo);

    /**
     * 培训删除
     */
    void removeTrain(Long gtId);

    /**
     * 培训新增
     */
    Long saveTrain(SaveTrainVO vo);

    /**
     * 培训修改
     */
    Long editTrain(EditTrainVO vo);

    /**
     * 培训推送
     */
    void trainPush(Long gtId);

    /**
     * 培训详情
     */
    GetTrainDTO getTrain(Long gtId);

    /**
     * 培训停用
     */
    void trainDown(TrainDownVO vo);

    /**
     * 参加这个培训的人员
     *
     */
    Result trainersList(Long gtId);


    /**
     * 该加盟商创建的培训
     *
     */
    Result getTrainClientList(Pageable page);

    /**
     * 该培训详情
     *
     */
    Result getTrainDetail(Long gtId);

    /**
     * 培训创建
     *
     */
    Result addTrain(SaveTrainVO saveTrainVO);

    /**
     * 回去折线图
     *
     */
    Result polyline();

    /**
     * 培训推送
     */
    void trainPush();

    /**
     * 培训修改_培训已经发送
     */
    Long editTrainOnUsing(EditTrainOnUsingVO vo);
}
