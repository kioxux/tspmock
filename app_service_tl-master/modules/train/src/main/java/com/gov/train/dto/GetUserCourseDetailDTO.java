package com.gov.train.dto;

import com.gov.train.entity.TrainCourse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetUserCourseDetailDTO extends TrainCourse {
    /**
     * 题目列表
     */
    List<GetUserCourseDetailQuestionDTO> questionList;

    // 附件绝对地址
    private String courseFileUrl;

    // 图片绝对地址
    private String courseCoverUrl;
}
