package com.gov.train.dto;

import com.gov.train.entity.TrainCourse;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CourseDTO extends TrainCourse {
    @ApiModelProperty(value = "课件培训状态")
    private Integer gtucTrainStatus;

    @ApiModelProperty(value = "课件考试状态")
    private Integer gtucExamStatus;

    @ApiModelProperty(value = "试题条数")
    private Integer questionTotal;

    @ApiModelProperty(value = "已完成数")
    private Integer completed=0;

    @ApiModelProperty(value = "培训ID")
    private Long gtId;

    @ApiModelProperty(value = "培训描述")
    private String gtName;

    @ApiModelProperty(value = "培训主旨")
    private Integer gtSubject;

    @ApiModelProperty(value = "培训期限")
    private Integer gtEndType;

    @ApiModelProperty(value = "培训截止日")
    private String gtDeadline;

    @ApiModelProperty(value = "课件题目")
    private List<TrainCourseQuestionDTO> questionList;

    @ApiModelProperty(value = "培训附件绝对路径")
    private String courseFileUrl;

    @ApiModelProperty(value = "课件图片绝对路径")
    private String courseCoverUrl;

    @ApiModelProperty(value = "错误消息")
    private String message;

    @ApiModelProperty(value = "课件状态")
    private Integer gtucCourseStatus;

    @ApiModelProperty(value = "课件类型名")
    private String gctName;

}
