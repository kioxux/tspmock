package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_TRAIN_USER_COURSE")
@ApiModel(value="TrainUserCourse对象", description="")
public class TrainUserCourse extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "培训ID")
    @TableId("TRAIN_ID")
    private Long trainId;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工ID")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "课件ID")
    @TableField("COURSE_ID")
    private Long courseId;

    @ApiModelProperty(value = "参加日期")
    @TableField("GTUC_ADD_DATE")
    private String gtucAddDate;

    @ApiModelProperty(value = "参加时间")
    @TableField("GTUC_ADD_TIME")
    private String gtucAddTime;

    @ApiModelProperty(value = "课件培训状态")
    @TableField("GTUC_TRAIN_STATUS")
    private Integer gtucTrainStatus;

    @ApiModelProperty(value = "课件考试状态")
    @TableField("GTUC_EXAM_STATUS")
    private Integer gtucExamStatus;

    @ApiModelProperty(value = "课件状态")
    @TableField("GTUC_COURSE_STATUS")
    private Integer gtucCourseStatus;


}
