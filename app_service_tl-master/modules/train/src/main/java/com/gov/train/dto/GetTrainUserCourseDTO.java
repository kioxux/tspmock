package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetTrainUserCourseDTO {

    /**
     * 课件id
     */
    private Long courseId;

    /**
     * 课件名称
     */
    private String courseName;

    /**
     * 解答正解的题目
     */
    private Integer correctQuestion;

    /**
     * 该课件下的题目
     */
    private Integer totalQuestion;
}
