package com.gov.train.mapper;

import com.gov.train.entity.CourseType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {

}
