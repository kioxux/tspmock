package com.gov.train.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Franchisee {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 加盟商地址
     */
    private String francAddr;

}
