package com.gov.train.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_COURSE_TYPE")
@ApiModel(value="CourseType对象", description="")
public class CourseType extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "课件类型")
    @TableId(value = "GCT_ID", type = IdType.AUTO)
    private Long gctId;

    @ApiModelProperty(value = "课件类型名")
    @TableField("GCT_NAME")
    private String gctName;


}
