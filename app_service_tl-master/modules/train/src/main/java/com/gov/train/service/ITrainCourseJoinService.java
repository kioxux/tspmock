package com.gov.train.service;

import com.gov.train.entity.TrainCourseJoin;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
public interface ITrainCourseJoinService extends SuperService<TrainCourseJoin> {

}
