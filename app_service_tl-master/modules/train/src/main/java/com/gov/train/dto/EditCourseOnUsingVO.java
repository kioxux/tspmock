package com.gov.train.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.train.entity.TrainCourse;
import com.gov.train.entity.TrainCourseQuestion;
import com.gov.train.entity.TrainCourseQuestionItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/25 10:46
 */
@Data
public class EditCourseOnUsingVO extends TrainCourse {

    @NotNull(message = "课件id不能为空")
    private Long courseId;

    @NotBlank(message = "课件名称不能为空")
    private String courseName;

    @NotNull(message = "课件模块不能为空")
    private Integer courseType;

    @NotNull(message = "重要程度不能为空")
    private Integer courseLevel;

    @NotBlank(message = "课件封面不能为空")
    private String courseCover;

    // @NotBlank(message = "视频附件不能为空")
    private String courseFile;

    // @NotNull(message = "图文内容不能为空")
    private String courseContent;

    @Valid
    @NotEmpty(message = "课件题目不能为空")
    private List<TrainCourseQuestionVO> questionList;

    @Data
    public static class TrainCourseQuestionVO extends TrainCourseQuestion {

        @NotNull(message = "题目ID不能为空")
        private Long questionId;

        @NotNull(message = "题目类别不能为空")
        private Integer questionType;

        @NotBlank(message = "题目名称不能为空")
        private String questionName;

        @NotEmpty(message = "题目答案不能为空")
        private List<Integer> questionAnswerList;

        @NotNull(message = "难易级别不能为空(1、难 2、中 3、易)")
        private Integer questionLevel;

        @Valid
        @NotEmpty(message = "题目选项不能为空")
        private List<TrainCourseQuestionItemVO> questionItemList;

        @Data
        public static class TrainCourseQuestionItemVO extends TrainCourseQuestionItem{

            @NotNull(message = "选项ID不能为空")
            private Long itemId;
            @NotBlank(message = "选项不能为空")
            private String itemName;
        }

    }
}
