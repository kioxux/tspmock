package com.gov.train.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gov.mybatis.SuperService;
import com.gov.train.dto.AddCourseTypeVO;
import com.gov.train.dto.EditCourseTypeVO;
import com.gov.train.dto.ListCourseTypeVO;
import com.gov.train.entity.CourseType;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-09-08
 */
public interface ICourseTypeService extends SuperService<CourseType> {

    /**
     * 课件类型列表
     */
    IPage<CourseType> listCourseType(ListCourseTypeVO vo);

    /**
     * 新增
     */
    void addCourseType(AddCourseTypeVO vo);

    /**
     * 编辑
     */
    void editCourseType(EditCourseTypeVO vo);

    /**
     * 删除
     */
    void removeCourseType(Long gctId);

    /**
     * 课件类型下拉框
     */
    List<CourseType> listCourseTypeDropDown(String gctName);
}
