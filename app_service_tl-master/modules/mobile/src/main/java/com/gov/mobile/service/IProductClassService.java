package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.entity.ProductClass;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
public interface IProductClassService extends SuperService<ProductClass> {

    Result selectProductClassList();
}
