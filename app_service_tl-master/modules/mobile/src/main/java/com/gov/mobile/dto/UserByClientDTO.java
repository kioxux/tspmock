package com.gov.mobile.dto;

import lombok.Data;

@Data
public class UserByClientDTO {

    /**
     * 员工编号
     */
    private String userId;

    /**
     * 姓名
     */
    private String userNam;

    /**
     * 手机号
     */
    private String userTel;

}
