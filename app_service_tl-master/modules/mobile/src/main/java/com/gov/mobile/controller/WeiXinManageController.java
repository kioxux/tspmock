package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.mobile.dto.ActiveUserParams;
import com.gov.mobile.dto.UserGetCardInfo;
import com.gov.mobile.dto.WeiChatDto;
import com.gov.mobile.service.IWeiXinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Api(tags = "公众号获取openId")
@RestController
@RequestMapping("weiXinManage")
public class WeiXinManageController {

    @Resource
    private IWeiXinService weiXinService;

    @PostMapping("getWeiXinOpenId")
    @ApiOperation(value = "获取微信openIs")
    public Result getWeiXinOpenId(@Valid @RequestBody WeiChatDto weiChatDto) {
        return weiXinService.getOpenId(weiChatDto);
    }


    @PostMapping("getCusOpenId")
    @ApiOperation(value = "获取微信openId")
    public Result getCusOpenId(@Valid @RequestBody WeiChatDto weiChatDto) {
        return weiXinService.getCusOpenId(weiChatDto);
    }


    @PostMapping("getActiveUserInfo")
    @ApiOperation(value = "获取用户字段")
    public Result getActiveUserInfo(@RequestBody ActiveUserParams activeUserParams) {
        return weiXinService.getActiveUserInfo(activeUserParams);
    }


    @PostMapping("activeMember")
    @ApiOperation(value = "激活会员")
    public Result activeMember(@RequestBody ActiveUserParams activeUserParams) {
        return weiXinService.activeMember(activeUserParams);
    }


}

