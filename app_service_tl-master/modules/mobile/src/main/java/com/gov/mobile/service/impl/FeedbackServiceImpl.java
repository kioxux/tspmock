package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.uuid.UUIDUtil;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.Feedback;
import com.gov.mobile.entity.Images;
import com.gov.mobile.feign.AuthFeign;
import com.gov.mobile.feign.dto.GetWorkflowInData;
import com.gov.mobile.mapper.FeedbackMapper;
import com.gov.mobile.service.IFeedbackService;
import com.gov.mobile.service.IImagesService;
import com.gov.mobile.service.IUserDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-23
 */
@Slf4j
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {

    @Resource
    private FeedbackMapper feedbackMapper;

    @Resource
    private IImagesService iImagesService;

    @Resource
    private IUserDataService iUserDataService;

    @Resource
    private CosUtils cosUtils;

    @Resource
    AuthFeign authFeign;

    /**
     * 反馈列表
     */
    @Override
    public Result feedbackList(FeedbackListVO feedbackListVO) {
        UserLoginModelClient currentUser = iUserDataService.getCurrentUser();
        QueryWrapper<Feedback> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", currentUser.getClient());
        queryWrapper.eq("USER_ID", currentUser.getUserId());
        if (null != feedbackListVO.getType()) {
            queryWrapper.eq("TYPE", feedbackListVO.getType());
        }
        queryWrapper.orderByDesc("DATE_TIME");
        Page<Feedback> page = new Page<>(feedbackListVO.getPageNum(), feedbackListVO.getPageSize());
        IPage<Feedback> iPage = feedbackMapper.selectPage(page, queryWrapper);
        return ResultUtil.success(iPage);
    }

    /**
     * 建议反馈添加
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result feedbackAdd(FeedbackVO feedbackVO) {
        UserLoginModelClient currentUser = iUserDataService.getCurrentUser();
        Feedback feedback = new Feedback();
        BeanUtils.copyProperties(feedbackVO, feedback);
        feedback.setId(UUIDUtil.getUUID());
        feedback.setClient(currentUser.getClient());
        feedback.setUserId(currentUser.getUserId());
        feedback.setDateTime(DateUtils.getCurrentDateTimeStrFull());
        feedback.setType(Constants.FeedbackType.TYPE_ONE);
        feedback.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
        feedback.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        feedback.setUserCreId(currentUser.getUserId());
        boolean isSave = this.save(feedback);
        if (!isSave) {
            throw new CustomResultException(ResultEnum.E0011);
        }
        List<ImageVO> images = feedbackVO.getImages();
        if (CollectionUtils.isNotEmpty(images)) {
            List<Images> imagesDOList = new ArrayList<>();
            for (ImageVO imageVO : images) {
                Images imagesDO = new Images();
                BeanUtils.copyProperties(imageVO, imagesDO);
                imagesDO.setId(feedback.getId());
                if (StringUtils.isNotEmpty(imagesDO.getSort())) {
                    imagesDO.setSort(1);
                }
                imagesDOList.add(imagesDO);
            }
            iImagesService.saveBatch(imagesDOList);
        }
        return ResultUtil.success();
    }

    /**
     * 建议反馈详情
     */
    @Override
    public Result feedbackDetail(String id) {
        Feedback feedback = this.getById(id);
        FeedbackDto feedbackDto = new FeedbackDto();
        BeanUtils.copyProperties(feedback, feedbackDto);

        QueryWrapper<Images> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ID", id);
        queryWrapper.orderByAsc("ID");
        List<Images> imagesList = iImagesService.list(queryWrapper);
        List<ImagesEntity> imagesEntityList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(imagesList)) {
            for (Images images : imagesList) {
                ImagesEntity imagesEntity = new ImagesEntity();
                BeanUtils.copyProperties(images, imagesEntity);
                imagesEntity.setUrl(cosUtils.urlAuth(images.getPath()));
                imagesEntityList.add(imagesEntity);
            }
        }
        feedbackDto.setImages(imagesEntityList);
        return ResultUtil.success(feedbackDto);
    }


}
