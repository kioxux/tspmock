package com.gov.mobile.mapper;

import com.gov.mobile.dto.AuthconfiDataDto;
import com.gov.mobile.entity.AuthconfiData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
public interface AuthconfiDataMapper extends BaseMapper<AuthconfiData> {

    List<AuthconfiDataDto> selectAuthConfiA(@Param("authGroupId") String authGroupId, @Param("authobjId") String authobjId, @Param("stoCode") String stoCode, @Param("client") String client);

    List<AuthconfiDataDto> selectAuthConfiB(@Param("authGroupId") String authGroupId, @Param("authobjId") String authobjId, @Param("stoCode") String stoCode, @Param("client") String client);

    int insertList(@Param("params") List<AuthconfiData> params);

    int insertIgnoreList(@Param("authconfiDataList") List<AuthconfiData> authconfiDataList);

    int deleteByParams(@Param("authGroupId") String authGroupId, @Param("stoCode") String stoCode, @Param("userId") String userId, @Param("authconfis") List<AuthconfiDataDto> authconfis, @Param("client") String client);

    int deleteByParam(@Param("authGroupId") String authGroupId, @Param("userId") String userId, @Param("client") String client);

    List<AuthconfiData> selectRepeat(@Param("authconfiDataList") List<AuthconfiData> authconfiDataList);

    int deleteBySiteList(@Param("client")String client, @Param("authGroupId") String authGroupId, @Param("siteList") List<String> siteList, @Param("depHeadId") String depHeadId);

    int deleteByGroupIdList(@Param("client")String client, @Param("groupIdList") List<String> groupIdList, @Param("site") String site, @Param("depHeadId") String depHeadId);

    int deleteOne(@Param("client") String client, @Param("groupId") String groupId, @Param("site") String site, @Param("user") String user);
}
