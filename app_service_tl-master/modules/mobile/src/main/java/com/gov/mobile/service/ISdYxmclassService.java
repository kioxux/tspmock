package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.entity.SdYxmclass;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
public interface ISdYxmclassService extends SuperService<SdYxmclass> {


    /**
     * 获取商品组和商品数据
     * @return
     * @param gsmStore 门店编码
     */
    Result selectGroupAndProductList(String gsmStore);
}
