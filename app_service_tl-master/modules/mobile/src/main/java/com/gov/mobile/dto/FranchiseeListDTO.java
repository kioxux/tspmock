package com.gov.mobile.dto;

import lombok.Data;

import java.util.List;


@Data
public class FranchiseeListDTO {

    /**
     * 单体门店
     */
    private List<OrganizationDto> storeList;

    /**
     * 连锁
     */
    private List<OrganizationDto> compadmList;

    /**
     * 批发公司
     */
    private List<OrganizationDto> wholesaleList;

    /**
     * 公司部门
     */
    private List<OrganizationDepDTO> depList;

    /**
     * 公司仓库
     */
    private List<OrganizationDepDTO> warehouseList;
}
