package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.entity.CommonData;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface ICommonDataService extends SuperService<CommonData> {

    /**
     * 通用数据获取
     */
    Result getCommonQuery(String commonKey);
}
