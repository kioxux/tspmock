package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.UploadFile;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.24
 */
public interface IFileService {

    /**
     * 上传文件
     * @param uploadFile
     * @return
     */
    Result upload(UploadFile uploadFile);
}
