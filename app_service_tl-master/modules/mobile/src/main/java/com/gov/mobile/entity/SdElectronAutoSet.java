package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_ELECTRON_AUTO_SET")
@ApiModel(value = "SdElectronAutoSet对象", description = "")
public class SdElectronAutoSet extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "主题单号")
    @TableField("GSEAS_ID")
    private String gseasId;

    @ApiModelProperty(value = "主题属性")
    @TableField("GSEAS_FLAG")
    private String gseasFlag;

    @ApiModelProperty(value = "电子券活动号")
    @TableField("GSEB_ID")
    private String gsebId;

    @ApiModelProperty(value = "发送数量")
    @TableField("GSEAS_QTY")
    private String gseasQty;

    @ApiModelProperty(value = "起始日期")
    @TableField("GSEAS_BEGIN_DATE")
    private String gseasBeginDate;

    @ApiModelProperty(value = "结束日期")
    @TableField("GSEAS_END_DATE")
    private String gseasEndDate;

    @ApiModelProperty(value = "创建日期")
    @TableField("GSEAS_CREATE_DATE")
    private String gseasCreateDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("GSEAS_CREATE_TIME")
    private String gseasCreateTime;

    @ApiModelProperty(value = "是否有效")
    @TableField("GSEAS_VALID")
    private String gseasValid;

}
