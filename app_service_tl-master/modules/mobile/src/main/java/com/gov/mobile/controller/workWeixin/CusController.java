package com.gov.mobile.controller.workWeixin;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.workWeixin.WXBizMsgCrypt;
import com.gov.mobile.dto.workWeixin.CorpTagDTO;
import com.gov.mobile.dto.workWeixin.QyCustVo;
import com.gov.mobile.dto.workWeixin.TagDTO;
import com.gov.mobile.dto.workWeixin.TagGroupDTO;
import com.gov.mobile.service.IEnterpriseWechatService;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.04
 */
@Slf4j
@Validated
@RestController
@RequestMapping("workWeixin/cus")
public class CusController {

    @Value("${wx.cus.token}")
    private String token;

    @Value("${wx.cus.encodingAesKey}")
    private String encodingAesKey;

    @Resource
    private IEnterpriseWechatService enterpriseWechatService;

//    public static void main(String[] arge) throws Exception {
//        String accessToken = Utils.accessToken("wx66edc7e6921995a2", "cljxlj-2piQYWOxoVtnMbOUImSTizfGlxkVhCIYd7eE");
//        TagGroupDTO tagGroupDTO = new TagGroupDTO();
//        tagGroupDTO.setGroup_name("疾病名称");
//        tagGroupDTO.setOrder(1);
//        List<TagDTO> tagDTOList = new ArrayList<>();
//        tagDTOList.add(new TagDTO().setName("糖尿病").setOrder(1));
//        tagDTOList.add(new TagDTO().setName("脑瘫").setOrder(2));
//        tagGroupDTO.setTag(tagDTOList);
//       TagGroupDTO result = Utils.addCorpTag(accessToken, tagGroupDTO);
//        System.out.println(result);
//        CorpTagDTO corpTagDTO = new CorpTagDTO();
//        corpTagDTO.setGroup_id(Collections.singletonList("etgssqCAAAhyrOv46GIIkv4Mv68x9cdg"));
//            corpTagDTO.setTag_id(Collections.singletonList("etgssqCAAAcTzFbdYlUqkEQfi3mcd4gQ"));

//       boolean a = Utils.delCorpTag(accessToken, corpTagDTO);
//        System.out.println(a);

//        List<TagGroupDTO> resultList =  Utils.getCorpTagList(accessToken, corpTagDTO);
//        System.out.println(resultList.size());

//        String xml = "    <xml>\n" +
//                "        <ToUserName>\n" +
//                "            <![CDATA[wx66edc7e6921995a2]]>\n" +
//                "        </ToUserName>\n" +
//                "        <FromUserName>\n" +
//                "            <![CDATA[sys]]>\n" +
//                "        </FromUserName>\n" +
//                "        <CreateTime>1636079088</CreateTime>\n" +
//                "        <MsgType>\n" +
//                "            <![CDATA[event]]>\n" +
//                "        </MsgType>\n" +
//                "        <Event>\n" +
//                "            <![CDATA[change_external_contact]]>\n" +
//                "        </Event>\n" +
//                "        <ChangeType>\n" +
//                "            <![CDATA[del_external_contact]]>\n" +
//                "        </ChangeType>\n" +
//                "        <UserID>\n" +
//                "            <![CDATA[yuanfeng]]>\n" +
//                "        </UserID>\n" +
//                "        <ExternalUserID>\n" +
//                "            <![CDATA[wmgssqCAAATdE71jDR9t01Dt7eEd8yaQ]]>\n" +
//                "        </ExternalUserID>\n" +
//                "    </xml>";
//        parseXml(xml);
//
//        // 客户应用token
////        String accessToken = Utils.accessToken("wx66edc7e6921995a2", "cljxlj-2piQYWOxoVtnMbOUImSTizfGlxkVhCIYd7eE");
////        System.out.println("------------------token:" + accessToken);
////
////        Map<String, Object> paramMap = new HashMap<>();
////        paramMap.put("chat_type", "single");
////        paramMap.put("external_userid", Arrays.asList("wmgssqCAAATdE71jDR9t01Dt7eEd8yaQ"));
//////        paramMap.put("sender", "yuanfeng");
////        paramMap.put("msgtype", "text");
////        Map<String, Object> textMap = new HashMap<>();
////        textMap.put("content", "111122223333");
////        paramMap.put("text", textMap);
//////        Map<String, Object> linkMap = new HashMap<>();
//////        linkMap.put("url", "https://www.baidu.com");
//////        linkMap.put("desc", "给你看个黄网");
//////        paramMap.put("link", linkMap);
////
////        String mapStr = JSON.toJSONString(paramMap);
////        //获取accessToken
////        String url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_msg_template?access_token=" + accessToken;
////        String json = Utils.doPost(url, mapStr);
////        System.out.println(json);
//
//
////        String userJson = Utils.getUserJson(accessToken, "yuanfeng");
////        System.out.println("-------------------------userjson:" + userJson);
////
////        // 客户联系token
////        accessToken = Utils.accessToken("wx66edc7e6921995a2", "cljxlj-2piQYWOxoVtnMbOUImSTizfGlxkVhCIYd7eE");
////        System.out.println("-------------------------token:" + accessToken);
////
////        String cusJson = Utils.getCusJson(accessToken, "wmgssqCAAATdE71jDR9t01Dt7eEd8yaQ");
////        System.out.println("---------------------------cusJson:" + cusJson);
//
//    }

    // 
    //<xml>
    //    <ToUserName>
    //        <![CDATA[wx66edc7e6921995a2]]>
    //    </ToUserName>
    //    <FromUserName>
    //        <![CDATA[sys]]>
    //    </FromUserName>
    //    <CreateTime>1636078736</CreateTime>
    //    <MsgType>
    //        <![CDATA[event]]>
    //    </MsgType>
    //    <Event>
    //        <![CDATA[change_external_contact]]>
    //    </Event>
    //    <ChangeType>
    //        <![CDATA[add_external_contact]]>
    //    </ChangeType>
    //    <UserID>
    //        <![CDATA[yuanfeng]]>
    //    </UserID>
    //    <ExternalUserID>
    //        <![CDATA[wmgssqCAAATdE71jDR9t01Dt7eEd8yaQ]]>
    //    </ExternalUserID>
    //    <WelcomeCode>
    //        <![CDATA[j-0rTWVqh_tIY50Jd0pj8oEvLkv3Vsvh-pgdpW7x3KE]]>
    //    </WelcomeCode>
    //</xml>
    //
    //
//    <xml>
//        <ToUserName>
//            <![CDATA[wx66edc7e6921995a2]]>
//        </ToUserName>
//        <FromUserName>
//            <![CDATA[sys]]>
//        </FromUserName>
//        <CreateTime>1636079088</CreateTime>
//        <MsgType>
//            <![CDATA[event]]>
//        </MsgType>
//        <Event>
//            <![CDATA[change_external_contact]]>
//        </Event>
//        <ChangeType>
//            <![CDATA[del_external_contact]]>
//        </ChangeType>
//        <UserID>
//            <![CDATA[yuanfeng]]>
//        </UserID>
//        <ExternalUserID>
//            <![CDATA[wmgssqCAAATdE71jDR9t01Dt7eEd8yaQ]]>
//        </ExternalUserID>
//    </xml>

    /**
     * 外部联系人业务处理
     *
     * @param cropId
     * @return
     */
    @PostMapping(value = "/callback/{cropId}")
    public String callbackExternalUserMsg(HttpServletRequest request, HttpServletResponse response, @PathVariable String cropId) throws Exception {
        log.info("企业微信CorpId:{},企业微信外部联系人事件消息通知 ", cropId);

        InputStream in = request.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String sReqData = "";
        String itemStr = "";//作为输出字符串的临时串，用于判断是否读取完毕
        while ((itemStr = reader.readLine()) != null) {
            sReqData += itemStr;
        }
        in.close();
        reader.close();
        log.info("公众号接收到消息:{}", sReqData);
        //防止中文乱码
        response.setCharacterEncoding("UTF-8");
        JSONArray j = printNote(sReqData);
        JSONObject message = j.getJSONObject(0).getJSONArray("xml").getJSONObject(0);
        String toUserName = message.getString("ToUserName");
        log.info("toUserName:{}", toUserName);
        String encrypt = message.getString("Encrypt");
        log.info("encrypt:{}", encrypt);

        String msg_signature = request.getParameter("msg_signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        WXBizMsgCrypt wxBizMsgCrypt = new WXBizMsgCrypt(token, encodingAesKey, cropId);
        String result = wxBizMsgCrypt.DecryptMsg(msg_signature, timestamp, nonce, sReqData);
        log.info("接收事件消息解密后的消息体：{}", result);

        // 消息对象
        QyCustVo msgVo = parseXml(result);
        if ("change_external_contact".equals(msgVo.getEvent())) {
            // 添加客户
            if ("add_external_contact".equals(msgVo.getChangeType())) {
                enterpriseWechatService.sendCusWelcomeMessage(cropId, msgVo);
            }
            // 删除客户
            if ("del_external_contact".equals(msgVo.getChangeType()) || "del_follow_user".equals(msgVo.getChangeType())) {
                enterpriseWechatService.unbindCus(msgVo);
            }
        }
        return "success";
    }

    /**
     * Xml解析
     *
     * @param xml
     * @return
     * @throws Exception
     */
    private static QyCustVo parseXml(String xml) throws Exception {
        QyCustVo target = new QyCustVo();
        SAXReader reader = new SAXReader();
        InputStream in = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        org.dom4j.Document document = reader.read(in);
        org.dom4j.Element root = document.getRootElement();
        List<org.dom4j.Element> elementList = root.elements();
        for (Element e : elementList) {
            if (ReflectUtil.hasField(QyCustVo.class, StrUtil.lowerFirst(e.getName()))) {
                ReflectUtil.invoke(target, "set" + e.getName(), e.getTextTrim());
            }

        }
        return target;
    }

    private static JSONArray printNote(String xml) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new InputSource(new StringReader(xml)));
            NodeList sessions = document.getChildNodes();
            return printNote_1(sessions);
        } catch (Exception e) {

        }
        return null;
    }

    private static JSONArray printNote_1(NodeList nodeList) {
        JSONArray dataArr = new JSONArray();
        JSONObject dataObject = new JSONObject();
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                if (tempNode.hasChildNodes() && tempNode.getChildNodes().getLength() > 1) {
                    JSONArray temArr = printNote_1(tempNode.getChildNodes());
                    if (dataObject.containsKey(tempNode.getNodeName())) {
                        dataObject.getJSONArray(tempNode.getNodeName()).add(temArr.getJSONObject(0));
                    } else {
                        dataObject.put(tempNode.getNodeName(), temArr);
                    }
                } else {
                    dataObject.put(tempNode.getNodeName(), tempNode.getTextContent());
                }
            }
        }
        dataArr.add(dataObject);
        return dataArr;
    }

    /**
     * 外部联系人URL路径校验
     *
     * @param request
     * @param response
     * @param cropId
     * @throws IOException
     */
    @RequestMapping(value = "/callback/{cropId}", method = RequestMethod.GET)
    public void callbackExternalUser(HttpServletRequest request, HttpServletResponse response, @PathVariable String cropId) throws IOException {
        log.info("企业微信CorpId:{}", cropId);

        String nonce = request.getParameter("nonce").trim();
        String timestamp = request.getParameter("timestamp").trim();
        String msgSignature = request.getParameter("msg_signature").trim();
        String echostr = request.getParameter("echostr").trim();
        echostr = echostr.replace(" ", "+");
        log.info("企业微信加密签名：{}，时间戳:{},随机数:{},加密的字符串:{}", msgSignature, timestamp, nonce, echostr);
        String result = null;
        PrintWriter printWriter = response.getWriter();
        try {
            WXBizMsgCrypt wxBizMsgCrypt = new WXBizMsgCrypt(token, encodingAesKey, cropId);
            result = wxBizMsgCrypt.VerifyURL(msgSignature, timestamp, nonce, echostr);
            log.info("验证URL解密后的消息体：\n{}", result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isBlank(result)) {
            result = "success";
        }
        printWriter.print(result);
        printWriter.close();
    }

    /**
     * 同步标签测试类
     */
    @GetMapping("asyncTag")
    public void asyncTag() {
        try {
            enterpriseWechatService.asyncTag();
        } catch (Exception e) {
            log.error("同步标签发生异常，异常消息：{}", e.getMessage());
        }
    }

    @GetMapping("tagList")
    public Result getTagList() {
        return enterpriseWechatService.getTagList();
    }
}

