package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.workWeixin.QyCustVo;

public interface IEnterpriseWechatService {

    /**
     * 验证客户是否绑定 && 发送客户提示语
     *
     * @param msgVo
     */
    void sendCusWelcomeMessage(String cropId,  QyCustVo msgVo) throws Exception;

    /**
     * 解除企微绑定
     * @param msgVo
     */
    void unbindCus(QyCustVo msgVo);

    /**
     * 同步标签
     */
    void asyncTag() throws Exception;

    /**
     * 获取企微标签
     * @return
     */
    Result getTagList();
}
