package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.mobile.entity.DcData;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface DcDataMapper extends BaseMapper<DcData> {


    DcData getMaxNumDcCode(@Param("client") String client);
}
