package com.gov.mobile.mapper;

import com.gov.mobile.entity.MemberLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface MemberLoginLogMapper extends BaseMapper<MemberLoginLog> {

}
