package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CheckUserPswdVO {

    @NotBlank(message = "传入检测密码不能为空")
    private String userPswd;
}
