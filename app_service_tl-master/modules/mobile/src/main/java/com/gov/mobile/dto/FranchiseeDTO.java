package com.gov.mobile.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gov.mobile.entity.Franchisee;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(JsonInclude.Include.ALWAYS)
public class FranchiseeDTO extends Franchisee {

}
