package com.gov.mobile.service.impl;

import com.gov.mobile.entity.SdRechargeCard;
import com.gov.mobile.mapper.SdRechargeCardMapper;
import com.gov.mobile.service.ISdRechargeCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-31
 */
@Service
public class SdRechargeCardServiceImpl extends ServiceImpl<SdRechargeCardMapper, SdRechargeCard> implements ISdRechargeCardService {

}
