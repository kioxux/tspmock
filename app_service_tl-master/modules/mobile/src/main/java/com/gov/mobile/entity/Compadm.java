package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_COMPADM")
@ApiModel(value="Compadm对象", description="")
public class Compadm extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商ID")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "连锁总部ID")
    @TableField("COMPADM_ID")
    private String compadmId;

    @ApiModelProperty(value = "连锁总部名称")
    @TableField("COMPADM_NAME")
    private String compadmName;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("COMPADM_NO")
    private String compadmNo;

    @ApiModelProperty(value = "法人/负责人")
    @TableField("COMPADM_LEGAL_PERSON")
    private String compadmLegalPerson;

    @ApiModelProperty(value = "质量负责人")
    @TableField("COMPADM_QUA")
    private String compadmQua;

    @ApiModelProperty(value = "详细地址")
    @TableField("COMPADM_ADDR")
    private String compadmAddr;

    @ApiModelProperty(value = "创建日期")
    @TableField("COMPADM_CRE_DATE")
    private String compadmCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("COMPADM_CRE_TIME")
    private String compadmCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("COMPADM_CRE_ID")
    private String compadmCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("COMPADM_MODI_DATE")
    private String compadmModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("COMPADM_MODI_TIME")
    private String compadmModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("COMPADM_MODI_ID")
    private String compadmModiId;

    @ApiModelProperty(value = "LOGO地址")
    @TableField("COMPADM_LOGO")
    private String compadmLogo;

    @ApiModelProperty(value = "连锁总部状态 0停用 1启用")
    @TableField("COMPADM_STATUS")
    private String compadmStatus;

}
