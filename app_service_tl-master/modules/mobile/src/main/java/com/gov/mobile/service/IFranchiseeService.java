package com.gov.mobile.service;

import com.gov.mobile.dto.FranchiseeDTO;
import com.gov.mobile.dto.FranchiseeListDTO;
import com.gov.mobile.entity.Franchisee;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
public interface IFranchiseeService extends SuperService<Franchisee> {


    /**
     * 获取当前加盟商详情
     */
    FranchiseeDTO getFranchisee();

    /**
     * 加盟商组织获取
     */
    FranchiseeListDTO getFranchiseeOrg();


}
