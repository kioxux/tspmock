package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 公司参数表
 * </p>
 *
 * @author sy
 * @since 2021-12-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_CL_SYSTEM_PARA")
@ApiModel(value="ClSystemPara对象", description="公司参数表")
public class ClSystemPara extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "参数名")
    @TableField("GCSP_ID")
    private String gcspId;

    @ApiModelProperty(value = "参数描述")
    @TableField("GCSP_NAME")
    private String gcspName;

    @ApiModelProperty(value = "值描述")
    @TableField("GCSP_PARA_REMARK")
    private String gcspParaRemark;

    @ApiModelProperty(value = "值1")
    @TableField("GCSP_PARA1")
    private String gcspPara1;

    @ApiModelProperty(value = "值2")
    @TableField("GCSP_PARA2")
    private String gcspPara2;

    @ApiModelProperty(value = "修改人员")
    @TableField("GCSP_UPDATE_EMP")
    private String gcspUpdateEmp;

    @ApiModelProperty(value = "修改日期")
    @TableField("GCSP_UPDATE_DATE")
    private String gcspUpdateDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("GCSP_UPDATE_TIME")
    private String gcspUpdateTime;


}
