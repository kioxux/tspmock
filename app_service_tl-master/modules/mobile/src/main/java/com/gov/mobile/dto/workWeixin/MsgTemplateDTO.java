package com.gov.mobile.dto.workWeixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgTemplateDTO {

    private String chat_type = "single";

    private List<String> external_userid;

    private String sender;

    private TextDTO text;

    private Object attachments = new Object();

}
