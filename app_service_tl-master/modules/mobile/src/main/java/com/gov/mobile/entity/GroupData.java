package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_GROUP_DATA")
@ApiModel(value="GroupData对象", description="")
public class GroupData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "权限组(岗位)编号")
    @TableId("AUTH_GROUP_ID")
    private String authGroupId;

    @ApiModelProperty(value = "权限组(岗位)描述")
    @TableField("AUTH_GROUP_NAME")
    private String authGroupName;

    @ApiModelProperty(value = "功能编号")
    @TableField("AUTHOBJ_ID")
    private String authobjId;

    @ApiModelProperty(value = "功能编号描述")
    @TableField("AUTHOBJ_NAME")
    private String authobjName;

    @ApiModelProperty(value = "岗位所属模块")
    @TableField("AUTHOBJ_SITE")
    private String authobjSite;


}
