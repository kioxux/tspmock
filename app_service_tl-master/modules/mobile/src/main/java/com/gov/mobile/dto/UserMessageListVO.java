package com.gov.mobile.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;

@Data
public class UserMessageListVO extends Pageable {

    /**
     * 消息类型
     */
    private String messageType;
}
