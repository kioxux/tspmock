package com.gov.mobile.controller.workWeixin;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.mobile.dto.workWeixin.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.05
 */
@Slf4j
@Component
public class Utils {

    private final static String GETTOKENURL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={}&corpsecret={}";
    private final static String GETUSER = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={}&userid={}";
    private final static String GETCUS = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get?access_token={}&external_userid={}";
    private final static String SEND_WELCOME_MSG = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/send_welcome_msg?access_token={}";
    private final static String GET_CORP_TAG_LIST = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token={}";
    private final static String ADD_CORP_TAG = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_corp_tag?access_token={}";
    private final static String DEL_CORP_TAG = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_corp_tag?access_token={}";
    private final static String ADD_MSG_TEMPLATE = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_msg_template?access_token={}";
    private final static String UPLOAD_IMAGE_URL = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type={}";
    private final static String MARK_TAG = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag?access_token={}";

    public static String accessToken(String corpid, String corpsecret) {
        try {
            String url = StringUtils.parse(GETTOKENURL, corpid, corpsecret);
            log.info("生成企业微信token参数{}", url);
            String resStr = doGetUrl(url);
            log.info("生成token返回值{}", resStr);
            String accesstoken = JSONObject.parseObject(resStr).getString("access_token");
            return accesstoken;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getUserJson(String accessToken, String external_userid) {
        try {
            String url = StringUtils.parse(GETUSER, accessToken, external_userid);
            log.info("查询用户参数{}", url);
            String resStr = doGetUrl(url);
            log.info("查询用户返回值{}", resStr);
            return resStr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 发送客户欢迎语
     *
     * @param accessToken   客户accessToken
     * @param welcomeMsgDTO 消息语消息体
     */
    public static String sendWelcomeMsg(String accessToken, WxWelcomeMsgDTO welcomeMsgDTO) throws Exception {
        String url = StrUtil.format(SEND_WELCOME_MSG, accessToken);
        String jsonParam = JSON.toJSONString(welcomeMsgDTO);
        log.info("接口参数{}", jsonParam);
        String result = doPost(url, jsonParam);
        log.info("接口返回值{}", result);
        return result;
    }

    /**
     * 获取企业标签库
     *
     * @param accessToken
     * @param corpTagDTO
     * @return
     * @throws Exception
     */
    public static List<TagGroupDTO> getCorpTagList(String accessToken, CorpTagDTO corpTagDTO) throws Exception {
        String url = StrUtil.format(GET_CORP_TAG_LIST, accessToken);
        String jsonParam = JSON.toJSONString(corpTagDTO);
        log.info("【企业微信】获取企业标签库，传入参数：{}", jsonParam);
        String result = doPost(url, jsonParam);
        JSONObject jsonObject = JSON.parseObject(result);
        List<TagGroupDTO> tagList = new ArrayList<>();
        if (jsonObject.getInteger("errcode") == 0) {
            JSONArray array = jsonObject.getJSONArray("tag_group");
            String answerString = JSONObject.toJSONString(array);//将array数组转换成字符
            tagList = JSON.parseArray(answerString, TagGroupDTO.class);
        } else {
            log.error("【企业微信】获取企业标签库，返回结果：{}", result);
            throw new CustomResultException("获取企业标签库异常");
        }
        return tagList;
    }

    /**
     * 添加会员标签
     *
     * @param accessToken
     * @return
     */
    public static TagGroupDTO addCorpTag(String accessToken, TagGroupDTO tagGroupDTO) throws Exception {
        String url = StrUtil.format(ADD_CORP_TAG, accessToken);
        String jsonParam = JSON.toJSONString(tagGroupDTO);
        log.info("【企业微信】添加企业客户标签，传入参数：{}", jsonParam);
        String result = doPost(url, jsonParam);
        JSONObject jsonObject = JSON.parseObject(result);
        TagGroupDTO tag = null;
        if (jsonObject.getInteger("errcode") == 0) {
            log.info("【企业微信】添加企业客户标签成功");
            tag = JSON.parseObject(jsonObject.getString("tag_group"), TagGroupDTO.class);
        } else {
            log.warn("【企业微信】添加企业客户标签，返回结果：{}", result);
        }
        return tag;
    }

    /**
     * 删除会员标签
     *
     * @param accessToken
     * @param corpTagDTO
     * @return
     * @throws Exception
     */
    public static boolean delCorpTag(String accessToken, CorpTagDTO corpTagDTO) throws Exception {
        String url = StrUtil.format(DEL_CORP_TAG, accessToken);
        String jsonParam = JSON.toJSONString(corpTagDTO);
        log.info("【企业微信】添加企业客户标签，传入参数：{}", jsonParam);
        String postResult = doPost(url, jsonParam);
        log.info("【企业微信】添加企业客户标签，返回结果：{}", postResult);
        JSONObject jsonObject = JSON.parseObject(postResult);
        boolean result = false;
        if (jsonObject.getInteger("errcode") == 0) {
            result = true;
        } else {
            log.warn("【企业微信】删除企业客户标签，返回结果：{}", postResult);
        }
        return result;
    }

    /**
     * 创建企业群发
     *
     * @param accessToken
     * @param templateDTO
     * @return
     */
    public static List<String> addMsgTemplate(String accessToken, MsgTemplateDTO templateDTO) {
        List<String> failList = null;
        try {
            String url = StrUtil.format(ADD_MSG_TEMPLATE, accessToken);
            String jsonParam = JSON.toJSONString(templateDTO);
            log.info("【企业微信】创建企业群发，传入参数：{}", jsonParam);
            String postResult = doPost(url, jsonParam);
            log.info("【企业微信】创建企业群发，返回结果：{}", postResult);
            JSONObject jsonObject = JSON.parseObject(postResult);
            if (jsonObject.getInteger("errcode") == 0) {
                failList = new ArrayList<>();
                JSONArray array = jsonObject.getJSONArray("fail_list");
                if (array != null && array.size() > 0) {
                    failList = array.stream().map(String::valueOf).collect(Collectors.toList());
                }
            } else {
                log.warn("【企业微信】创建企业群发，返回结果：{}", postResult);
            }
        } catch (Exception e) {
            log.error("【企业微信】创建企业群发, 异常：{}", e.getMessage());
        }
        return failList;
    }

    /**
     * 编辑客户企业标签
     *
     * @param accessToken
     * @param markTagDTO
     * @return
     * @throws Exception
     */
    public static boolean markTag(String accessToken, MarkTagDTO markTagDTO) throws Exception {
        String url = StrUtil.format(MARK_TAG, accessToken);
        String jsonParam = JSON.toJSONString(markTagDTO);
        log.info("【企业微信】编辑客户企业标签，传入参数：{}", jsonParam);
        String postResult = doPost(url, jsonParam);
        log.info("【企业微信】编辑客户企业标签，返回结果：{}", postResult);
        JSONObject jsonObject = JSON.parseObject(postResult);
        boolean result = false;
        if (jsonObject.getInteger("errcode") == 0) {
            result = true;
        } else {
            log.warn("【企业微信】编辑客户企业标签，返回结果：{}", postResult);
        }
        return result;
    }

    public static String uploadByUrl(String accessToken, String url) {
        try {
            org.apache.commons.httpclient.HttpClient httpclient = new org.apache.commons.httpclient.HttpClient();
            GetMethod getMethod = new GetMethod(url);
            httpclient.executeMethod(getMethod);
            String info = new String(getMethod.getResponseBody());
            String type = "file";
            String fileSuffix = url.substring(url.lastIndexOf(".") + 1);
            if ("png".equalsIgnoreCase(fileSuffix) || "jpg".equalsIgnoreCase(fileSuffix)) {
                type = "image";
            }
//            else if ("wav".equalsIgnoreCase(fileSuffix) || "mp3".equalsIgnoreCase(fileSuffix)) {
//                type = "voice";
//            }
            else if ("mp4".equalsIgnoreCase(fileSuffix)) {
                type = "video";
            }

            String fileName = url.substring(url.lastIndexOf("/") + 1);


            URL urlObj = new URL(StrUtil.format(UPLOAD_IMAGE_URL, accessToken, type));
            HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false); // post方式不能使用缓存
            // 设置请求头信息
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Charset", "UTF-8");
            // 设置边界
            String BOUNDARY = "----------" + System.currentTimeMillis();
            con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
            // 请求正文信息
            // 第一部分：
            StringBuilder sb = new StringBuilder();
            sb.append("--"); // 必须多两道线
            sb.append(BOUNDARY);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"media\";filelength=\"" + info.length() + "\";filename=\""+ fileName + "\"\r\n");
            sb.append("Content-Type:application/octet-stream\r\n\r\n");
            byte[] head = sb.toString().getBytes(StandardCharsets.UTF_8);

            // 获得输出流
            OutputStream out = new DataOutputStream(con.getOutputStream());
            // 输出表头
            out.write(head);

            // 文件正文部分
            // 把文件已流文件的方式 推入到url中
            DataInputStream in = new DataInputStream(getMethod.getResponseBodyAsStream());
            int bytes = 0;
            byte[] bufferOut = new byte[1024];
            while ((bytes = in.read(bufferOut)) != -1) {
                out.write(bufferOut, 0, bytes);
            }
            in.close();

            // 结尾部分
            // 定义最后数据分隔线
            byte[] foot = ("\r\n--" + BOUNDARY + "--\r\n").getBytes(StandardCharsets.UTF_8);
            out.write(foot);
            out.flush();
            out.close();
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = null;
            String result = null;
            try {
                // 定义BufferedReader输入流来读取URL的响应
                reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                result = buffer.toString();

            } catch (IOException e) {
                log.info("上传图片失败，{}", e.getMessage());
                throw new CustomResultException("上传图片失败");
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
            log.info("上传图片,参数:{},返回值:{}", url, result);
            if (result != null && !"".equals(result)) {
                JSONObject json = JSONObject.parseObject(result);
                return (String) json.get("media_id");
            }
        } catch (Exception e) {
            log.info("上传图片失败，{}", e.getMessage());
        }
        return null;
    }


    public static String getCusJson(String accessToken, String userid) {
        try {
            String url = StringUtils.parse(GETCUS, accessToken, userid);
            log.info("查询客户参数{}", url);
            String resStr = doGetUrl(url);
            log.info("查询客户返回值{}", resStr);
            return resStr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String doGetUrl(String url) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = httpClient.execute(httpGet);
        HttpEntity entity = response.getEntity();
        String resStr = EntityUtils.toString(entity, "UTF-8");
        httpClient.close();
        response.close();
        return resStr;
    }

    /**
     * post请求
     *
     * @param url       url地址
     * @param jsonParam json格式字符串参数
     * @return
     */
    private static String doPost(String url, String jsonParam) throws Exception {
        String str = "";
        //post请求返回结果
        HttpClient httpClient = HttpClientBuilder.create().build();
        JSONObject jsonResult = null;
        HttpPost method = new HttpPost(url);
        if (null != jsonParam) {
            //解决中文乱码问题
            StringEntity entity = new StringEntity(jsonParam, "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            method.setEntity(entity);
        }
        HttpResponse result = httpClient.execute(method);

        /**请求发送成功，并得到响应**/
        if (result.getStatusLine().getStatusCode() == 200) {
            /**读取服务器返回过来的json字符串数据**/
            str = EntityUtils.toString(result.getEntity());
        }
        return str;

    }

}
