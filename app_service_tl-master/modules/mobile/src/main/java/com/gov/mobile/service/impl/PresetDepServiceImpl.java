package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.mobile.entity.PresetDep;
import com.gov.mobile.mapper.PresetDepMapper;
import com.gov.mobile.service.IPresetDepService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-30
 */
@Service
public class PresetDepServiceImpl extends ServiceImpl<PresetDepMapper, PresetDep> implements IPresetDepService {

    @Resource
    private PresetDepMapper presetDepMapper;
    /**
     * 获取不重复的部门列表
     */
    @Override
    public List<PresetDep> getAppLastVersion() {
        QueryWrapper<PresetDep> queryWrapper = new QueryWrapper();
        queryWrapper.select("DISTINCT DEP_CODE, DEP_NAME");
        return presetDepMapper.selectList(queryWrapper);
    }
}
