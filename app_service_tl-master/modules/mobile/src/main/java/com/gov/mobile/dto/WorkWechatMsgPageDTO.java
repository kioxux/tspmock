package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class WorkWechatMsgPageDTO {

    @NotNull(message = "分页不能为空")
    @Min(value = 1, message = "分页最少为1")
    private Integer pageNum;

    private Integer pageSize;

    /**
     * 任务名称
     */
    private String gwmTheme;

    /**
     * 发送状态（0-待发送，1-发送成功，2-发送失败）
     */
    private String gwmSendStatus;

    /**
     * 预计发送开始时间
     */
    private String gwmEstSendStartTime;

    /**
     * 预计发送结束时间
     */
    private String gwmEstSendEndTime;

}
