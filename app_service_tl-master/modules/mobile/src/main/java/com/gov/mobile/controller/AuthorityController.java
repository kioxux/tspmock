package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.AuthconfiDataDto;
import com.gov.mobile.dto.GroupDataDto;
import com.gov.mobile.service.IAuthorityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@Api(tags = "组织")
@RestController
@RequestMapping("authority")
public class AuthorityController {

    @Resource
    private IAuthorityService authorityService;

    @ApiOperation(value = "组织获取")
    @GetMapping("getAuthOrg")
    public Result getAuthOrg() {
        return ResultUtil.success(authorityService.getAuthOrg());
    }


    @ApiOperation("权限功能获取")
    @GetMapping("getAuthobjList")
    public Result getAuthobjList() {
        return ResultUtil.success(authorityService.getAuthobjList());
    }


    @ApiOperation("岗位数据获取")
    @GetMapping("getGroupList")
    public Result getGroupList(@RequestParam String authobjId) {
        return ResultUtil.success(authorityService.getGroupList(authobjId));
    }


    @ApiOperation("门店集合获取")
    @GetMapping("getStoreList")
    public Result getStoreList() {
        return ResultUtil.success(authorityService.getStoreList());
    }


    @ApiOperation("权限用户数据获取")
    @GetMapping("getAuthConfi")
    public Result getAuthConfi(@RequestParam String authGroupId, @RequestParam(required = false) String authobjId, @RequestParam(required = false) String stoCode) {
        return ResultUtil.success(authorityService.getAuthConfi(authGroupId, authobjId, stoCode));
    }


    @ApiOperation("用户权限数据保存")
    @PostMapping("setAuthConfi")
    public Result setAuthConfi(@RequestBody GroupDataDto param) {
        return ResultUtil.success(authorityService.setAuthConfi(param));
    }


    @ApiOperation(value = "获取加盟商用户数据")
    @GetMapping("getUserByClient")
    public Result getUserByClient() {
        return ResultUtil.success(authorityService.getUserByClient());
    }


    @ApiOperation("用户权限数据删除")
    @PostMapping("delAuthConfi")
    public Result delAuthConfi(@RequestBody GroupDataDto param) {
        return ResultUtil.success(authorityService.delAuthConfi(param));
    }


    @ApiOperation("用户权限数据修改")
    @PostMapping("editAuthConfi")
    public Result editAuthConfi(@RequestBody GroupDataDto param) {
        return ResultUtil.success(authorityService.editAuthConfi(param));
    }

}
