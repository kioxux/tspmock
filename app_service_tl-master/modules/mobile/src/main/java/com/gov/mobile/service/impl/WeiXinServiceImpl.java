package com.gov.mobile.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.entity.weChat.MemberActiveParams;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.userutil.UserUtils;
import com.gov.common.uuid.UUIDUtil;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.feign.service.OperationService;
import com.gov.mobile.mapper.*;
import com.gov.mobile.service.ISdMemberBasicService;
import com.gov.mobile.service.IWeiXinService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Slf4j
@Service
public class WeiXinServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements IWeiXinService {

    @Resource
    private OfficialAccountsMapper officialAccountsMapper;
    @Resource
    private SdMemberCardMapper sdMemberCardMapper;
    @Resource
    private MemberLoginLogMapper memberLoginLogMapper;

    @Resource
    private WeChatUtils weChatUtils;
    @Resource
    private SdMemberBasicMapper sdMemberBasicMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private ClSystemParaMapper clSystemParaMapper;
    @Resource
    private ISdMemberBasicService iSdMemberBasicService;
    @Resource
    private OperationService operationService;


    /**
     * 获取微信公众号的openId
     *
     * @param
     * @return 微信公众号为 scope=snsapi_base
     * <p>
     * 参数    是否必须	说明
     * appid	是	公众号的唯一标识
     * secret	是	公众号的appsecret
     * code	是	获取的code参数
     * grant_type	是	填写为authorization_code
     */
    @Override
    public Result getOpenId(WeiChatDto weiChatDto) {
        try {
            Map<Object, Object> map = new HashMap<>();
            //判断是否是连锁总店
            QueryWrapper<OfficialAccounts> query = new QueryWrapper<>();
            query.eq("CLIENT", weiChatDto.getClient());
            query.eq("GOA_CODE", weiChatDto.getStoCode());
            List<OfficialAccounts> accounts = officialAccountsMapper.selectList(query);
            if (accounts.isEmpty()) {
                return ResultUtil.error(ResultEnum.M0004);
            }
            String appid = "";
            String secret = "";
            for (OfficialAccounts item : accounts) {
                if (item.getGoaObjectParam().equals("WECHAT_OFFICIAL_ACCOUNTS_APPID")) {
                    appid = item.getGoaObjectValue();
                }
                if (item.getGoaObjectParam().equals("WECHAT_OFFICIAL_ACCOUNTS_APPSECRET")) {
                    secret = item.getGoaObjectValue();
                }
            }
            if (StringUtils.isBlank(appid) || StringUtils.isBlank(secret)) {
                return ResultUtil.error(ResultEnum.M0004);
            }
            String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret + "&code=" + weiChatDto.getCode() + "&grant_type=authorization_code";

            log.info("给微信的参数----------------------------------------------" + url);
            WebClient webClient = WebClient.create();
            Mono<String> response = webClient.get().uri(url).retrieve().bodyToMono(String.class);
            String result = response.block();
            log.info("微信返回的数据----------------------------------------------" + result);
            JSONObject json = JSONObject.parseObject(result);

            String openId = json.getString("openid");
            map.put("openId", openId);
            if (!StringUtils.isEmpty(openId) && weiChatDto.getLogin() == 0) {
                //登录 自动登录
                QueryWrapper<SdMemberCard> queryCard = new QueryWrapper<>();
                //验证 加盟商+openId+门店
                queryCard.eq("CLIENT", weiChatDto.getClient());
                queryCard.eq("GSMBC_ORG_ID", weiChatDto.getStoCode());
                queryCard.eq("GSMBC_OPEN_ID", openId);
                SdMemberCard card = sdMemberCardMapper.selectOne(queryCard);
                if (card != null) {
                    //绑定过
                    //当有会员时,根据会员卡号，门店，加盟商，openId 会员id 生成token
                    String token = UserUtils.createMemberToken(weiChatDto.getClient(), card.getGsmbcCardId(), openId, weiChatDto.getStoCode(), card.getGsmbcMemberId(), UserUtils.PLATFORM_2);
                    //塞当前登录人的信息到redis里面
                    map.put("token", token);
                    //登录日志表
                    MemberLoginLog log = new MemberLoginLog();
                    log.setId(UUIDUtil.getUUID());
                    log.setClient(weiChatDto.getClient());
                    log.setStoreCode(weiChatDto.getStoCode());
                    log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
                    log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                    log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
                    log.setMemberId(card.getGsmbcMemberId());
                    log.setMemberCard(card.getGsmbcCardId());
                    log.setPlatform("1");
                    log.setOpenId(openId);
                    log.setToken(token);
                    memberLoginLogMapper.insert(log);
                }
                return ResultUtil.success(map);
            } else {
                return ResultUtil.error(ResultEnum.M0005);
            }
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.M0005);
        }
    }

    @Override
    public Result getCusOpenId(WeiChatDto weiChatDto) {
        Map<Object, Object> map = new HashMap<>();
        //判断是否是连锁总店
        QueryWrapper<OfficialAccounts> query = new QueryWrapper<>();
        query.eq("CLIENT", weiChatDto.getClient());
        query.eq("GOA_CODE", weiChatDto.getStoCode());
        List<OfficialAccounts> accounts = officialAccountsMapper.selectList(query);
        if (accounts.isEmpty()) {
            return ResultUtil.error(ResultEnum.M0004);
        }
        String appid = "";
        String secret = "";
        for (OfficialAccounts item : accounts) {
            if (item.getGoaObjectParam().equals("WECHAT_OFFICIAL_ACCOUNTS_APPID")) {
                appid = item.getGoaObjectValue();
            }
            if (item.getGoaObjectParam().equals("WECHAT_OFFICIAL_ACCOUNTS_APPSECRET")) {
                secret = item.getGoaObjectValue();
            }
        }
        if (StringUtils.isBlank(appid) || StringUtils.isBlank(secret)) {
            return ResultUtil.error(ResultEnum.M0004);
        }
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret + "&code=" + weiChatDto.getCode() + "&grant_type=authorization_code";

        log.info("给微信的参数----------------------------------------------" + url);
        WebClient webClient = WebClient.create();
        Mono<String> response = webClient.get().uri(url).retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("微信返回的数据----------------------------------------------" + result);
        JSONObject json = JSONObject.parseObject(result);

        String openId = json.getString("openid");
        map.put("openId", openId);
        if (!StringUtils.isEmpty(openId) && weiChatDto.getLogin() == 0) {
            //登录 自动登录
            QueryWrapper<SdMemberCard> queryCard = new QueryWrapper<>();
            //验证 加盟商+openId+门店
            queryCard.eq("CLIENT", weiChatDto.getClient());
            queryCard.eq("GSMBC_ORG_ID", weiChatDto.getStoCode());
            queryCard.eq("GSMBC_OPEN_ID", openId);
            SdMemberCard card = sdMemberCardMapper.selectOne(queryCard);

            if (card != null) {
                //绑定过
                //当有会员时,根据会员卡号，门店，加盟商，openId 会员id 生成token
                String token = UserUtils.createMemberToken(weiChatDto.getClient(), card.getGsmbcCardId(), openId, weiChatDto.getStoCode(), card.getGsmbcMemberId(), UserUtils.PLATFORM_2);
                //塞当前登录人的信息到redis里面
                map.put("token", token);
                //登录日志表
                MemberLoginLog log = new MemberLoginLog();
                log.setId(UUIDUtil.getUUID());
                log.setClient(weiChatDto.getClient());
                log.setStoreCode(weiChatDto.getStoCode());
                log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
                log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
                log.setMemberId(card.getGsmbcMemberId());
                log.setMemberCard(card.getGsmbcCardId());
                log.setPlatform("1");
                log.setOpenId(openId);
                log.setToken(token);
                memberLoginLogMapper.insert(log);

            }
            return ResultUtil.success(map);
        }
        return ResultUtil.error(ResultEnum.M0005);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result activeMember(ActiveUserParams activeUserParams) {
        // 注册人openId
        String openId = activeUserParams.getOpenid();
        // 会员卡ID
        String cardId = activeUserParams.getCardId();

        OfficialAccountsDTO accounts = officialAccountsMapper.selectListByCardId(cardId);

        if (accounts == null) {
            log.error("用户未配置微信公众号的原始ID");
            throw new CustomResultException("用户未配置微信公众号的原始ID");
        }
        String accessToken = weChatUtils.getAccessToken(accounts.getAccountsAppid(), accounts.getAccountsAppsecret());
        if (StringUtils.isBlank(accessToken)) {
            log.error("该客户未配置正确的APPID以及密钥，appId: {},appSecret:{}", accounts.getAccountsAppid(), accounts.getAccountsAppsecret());
            throw new CustomResultException("该客户未配置正确的APPID以及密钥");
        }
        // 微信会员卡 code
        String code = weChatUtils.decryptCode(accessToken, activeUserParams.getEncryptCode());
        if(StringUtils.isBlank(code)){
            log.error("code未获取到");
            throw new CustomResultException("code未获取到");
        }
        // 拉取会员信息
        String result = weChatUtils.activatetempinfo(accessToken, activeUserParams.getTicket());
        JSONObject json = JSONObject.parseObject(result);
        JSONObject userInfo = json.getJSONObject("info");
        if (userInfo == null) {
            log.error("用户未获取到");
            throw new CustomResultException("用户未获取到");
        }
        WxUserInfo info = parseUserInfoList(userInfo);

        QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", accounts.getClient());
        queryWrapper.eq("GSMB_MOBILE", info.getMobile());
        SdMemberBasic member = sdMemberBasicMapper.selectOne(queryWrapper);

        SdMemberCard card = setSdMemberCard(openId, accounts);

        String userId = UUIDUtil.getUUID();
        if (null != member) {
            QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
            //验证 加盟商+会员ID+门店
            query.eq("CLIENT", accounts.getClient());
            query.eq("GSMBC_MEMBER_ID", member.getGsmbMemberId());
            query.eq("GSMBC_ORG_ID", accounts.getGoaCode());
            Integer count = sdMemberCardMapper.selectCount(query);
            if (count > 0) {
                return ResultUtil.error(ResultEnum.M0001);
            }
            userId = member.getGsmbMemberId();
        }

        // 清空之前绑定关系
        SdMemberCard cardOld = new SdMemberCard();
        cardOld.setGsmbcOpenId("");
        sdMemberCardMapper.update(cardOld,
                new QueryWrapper<SdMemberCard>().eq("CLIENT", accounts.getClient())
                        .eq("GSMBC_ORG_ID", accounts.getGoaCode())
                        .eq("GSMBC_OPEN_ID", openId)
        );

        //会员信息表
        SdMemberBasic basic = new SdMemberBasic();
        //加盟商
        basic.setClient(accounts.getClient());
        //会员id
        basic.setGsmbMemberId(userId);
        //会员姓名
        basic.setGsmbName(info.getName());
        //手机
        basic.setGsmbMobile(info.getMobile());
        //性别
        basic.setGsmbSex(info.getSex());
        //生日
        basic.setGsmbBirth(info.getBirth());
        //身份证
        basic.setGsmbCredentials(info.getCredentials());
        //年龄
        Integer year = Integer.valueOf(DateUtils.getCurrentDateStrYYMMDD().substring(0, 4)) - Integer.valueOf(info.getBirth().substring(0, 4));
        basic.setGsmbAge(year.toString());
        //是否接受本司信息
        basic.setGsmbContactAllowed("1");
        //会员状态
        basic.setGsmbStatus(CommonEnum.UserStatus.STATUS_ACTIVITY.getCode());
        sdMemberBasicMapper.insert(basic);

        //会员id
        card.setGsmbcMemberId(userId);
        //会员卡号
        Map<Object, Object> numMap = new HashMap<>();
        numMap.put("client", accounts.getClient());
        String cardNumber = sdMemberCardMapper.getUserCardNumber(numMap).getGsmbcCardId();
        card.setGsmbcCardId(cardNumber);
        // 推荐人
        card.setGsmbcRecommender(info.getRecommender());
        // 微信会员卡ID
        card.setGsmbcWxCardId(cardId);
        // 微信卡号
        card.setGsmbcWxCardCode(code);
        sdMemberCardMapper.insert(card);

        operationService.memberSync(card.getClient(), card.getGsmbcMemberId(), card.getGsmbcCardId());

        // 电子券 生成
        iSdMemberBasicService.addElectron(accounts.getClient(), cardNumber);


//        if(member == null){
//            // 企微关注绑定添加
//            WorkwechatCusUserBind bind = new WorkwechatCusUserBind();
//            bind.setClient(modelClient.getClient());
//            bind.setGwcId(modelClient.getGwcId());
//            bind.setGwcubUserId(modelClient.getUserId());
//            bind.setGwcubWwUserId(modelClient.getWwUserId());
//            bind.setGwcubWwMemberId(modelClient.getMemberId());
//            bind.setGwcubMemberId(card.getGsmbcMemberId());
//            // 开卡门店
//            bind.setGwcubBrId(card.getGsmbcOpenCard());
//            // 门店|连锁
//            bind.setGwcubOrgId(card.getGsmbcOrgId());
//            // 会员卡号
//            bind.setGwcubCardId(card.getGsmbcCardId());
//            // 所属组织类型，单体：1，连锁：2
//            bind.setGwcubOrgType(StrUtil.equals(card.getGsmbcOpenCard(), card.getGsmbcOrgId()) ? 1 : 2);
//            bind.setGwcubBindTime(new Date());
//            workwechatCusUserBindMapper.insert(bind);
//        }

        MemberActiveParams params = new MemberActiveParams();
        params.setCode(code);
        params.setMembership_number(cardNumber);
        params.setCard_id(cardId);
        Integer errCode = weChatUtils.membercardActive(accessToken, params);
        if (errCode != 0) {
            throw new CustomResultException("会员激活异常");
        }

        return ResultUtil.success(weChatUtils.getActiveUrl(accessToken, cardId, activeUserParams.getOuter_str()));
    }

    @Override
    public Result getActiveUserInfo(ActiveUserParams activeUserParams) {
        OfficialAccountsDTO accounts = officialAccountsMapper.selectListByCardId(activeUserParams.getCardId());
        if (accounts == null) {
            log.error("用户未配置微信公众号的原始ID");
            throw new CustomResultException("用户未配置微信公众号的原始ID");
        }
        String accessToken = weChatUtils.getAccessToken(accounts.getAccountsAppid(), accounts.getAccountsAppsecret());
        if (StringUtils.isBlank(accessToken)) {
            log.error("该客户未配置正确的APPID以及密钥，appId: {},appSecret:{}", accounts.getAccountsAppid(), accounts.getAccountsAppsecret());
            throw new CustomResultException("该客户未配置正确的APPID以及密钥");
        }

        String result = weChatUtils.activatetempinfo(accessToken, activeUserParams.getTicket());
        JSONObject json = JSONObject.parseObject(result);
        JSONObject userInfo = json.getJSONObject("info");
        WxUserInfo info = parseUserInfoList(userInfo);
        return ResultUtil.success(info);
    }

    private SdMemberCard setSdMemberCard(String openId, OfficialAccountsDTO accounts) {
        //会员卡信息表
        SdMemberCard card = new SdMemberCard();
        //加盟商
        card.setClient(accounts.getClient());
        // 所属组织
        card.setGsmbcOrgId(accounts.getGoaCode());
        //渠道
        card.setGsmbcChannel("1");
        //卡类型 - 普通
        card.setGsmbcClassId("5");
        //当前积分
        card.setGsmbcIntegral("0");
        //最后积分日期
        card.setGsmbcIntegralLastdate(DateUtils.getCurrentDateStrYYMMDD());
        //新卡创建日期
        card.setGsmbcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        //类型 - 虚拟卡
        card.setGsmbcType("1");
        //openID
        card.setGsmbcOpenId(openId);
        //卡状态
        card.setGsmbcStatus("0");

        //门店
        QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<>();
        storeDataQueryWrapper.eq("CLIENT", accounts.getClient());
        storeDataQueryWrapper.eq("STO_CODE", accounts.getGoaCode());
        StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
        if (storeData != null) {
            card.setGsmbcBrId(accounts.getGoaCode());
            // 开卡门店
            card.setGsmbcOpenCard(accounts.getGoaCode());
        } else {
            storeData = storeDataMapper.getMinStore(accounts.getClient(), accounts.getGoaCode());
            if (storeData != null) {
                // 开卡门店
                card.setGsmbcOpenCard(storeData.getStoCode());
                card.setGsmbcBrId(storeData.getStoCode());
            }
        }

        // 激活状态
        cardStatus(card);

        return card;
    }

    /**
     * 0-已激活（缺省）；1-待激活转正（预备会员）
     *
     * @param card
     */
    private void cardStatus(SdMemberCard card) {
        if (card == null || org.apache.commons.lang3.StringUtils.isBlank(card.getClient())) {
            return;
        }
        card.setGsmbcState("0");
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(new LambdaQueryWrapper<ClSystemPara>()
                .eq(ClSystemPara::getClient, card.getClient())
                .eq(ClSystemPara::getGcspId, "MEM_ONLINE_REGIST_MODE")
        );
        if (clSystemPara == null) {
            return;
        }
        // 0-免费注册并立即生效（缺省）；
        if ("0".equals(clSystemPara.getGcspPara1())) {
            card.setGsmbcState("0");
        }
        // 1-收费注册需线下转正（仁和模式）；
        if ("1".equals(clSystemPara.getGcspPara1())) {
            card.setGsmbcState("1");
        }
        // 2-收费注册线上支付后激活（字段GCSP_PARA2  可配置收费金额）
        if ("2".equals(clSystemPara.getGcspPara1())) {
            card.setGsmbcState("2");
            // TODO 生成订单 暂不实现 2021年12月3日 现场讨论暂不实现 2 的逻辑
        }
    }

    private WxUserInfo parseUserInfoList(JSONObject userInfo) {
        WxUserInfo info = new WxUserInfo();
        List<WxUserInfoItem> commonFieldList = JsonUtils.jsonToBeanList(userInfo.getJSONArray("common_field_list").toJSONString(), WxUserInfoItem.class);
        info.setName(commonFieldList.stream().filter(t -> "USER_FORM_INFO_FLAG_NAME".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue());
        info.setMobile(commonFieldList.stream().filter(t -> "USER_FORM_INFO_FLAG_MOBILE".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue());
        String sex = commonFieldList.stream().filter(t -> "USER_FORM_INFO_FLAG_SEX".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue();
        info.setSex("女".equals(sex) ? "0" : "1");
        String birth = commonFieldList.stream().filter(t -> "USER_FORM_INFO_FLAG_BIRTHDAY".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue();
        info.setBirth(DateUtils.parseLocalDate(birth, "yyyy-M-d").format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        info.setAddress(commonFieldList.stream().filter(t -> "USER_FORM_INFO_FLAG_LOCATION".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue());
        info.setCredentials(commonFieldList.stream().filter(t -> "USER_FORM_INFO_FLAG_IDCARD".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue());

        List<WxUserInfoItem> customFieldList = JsonUtils.jsonToBeanList(userInfo.getJSONArray("custom_field_list").toJSONString(), WxUserInfoItem.class);
        info.setRecommender(customFieldList.stream().filter(t -> "推荐人".equals(t.getName())).findFirst().orElse(new WxUserInfoItem()).getValue());

        return info;
    }
}
