package com.gov.mobile.controller.workWeixin;

import com.gov.common.basic.JsonUtils;
import com.gov.common.workWeixin.AesException;
import com.gov.common.workWeixin.WXBizMsgCrypt;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.04
 */
@Slf4j
@Validated
@RestController
@RequestMapping("workWeixin/getUrlVerify/{corpId}")
public class GetUrlVerifyController {

    @PostMapping
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        log.info("request:" + JsonUtils.beanToJson(request.getParameterMap()));

        InputStream in = request.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String sReqData = "";
        String itemStr = "";//作为输出字符串的临时串，用于判断是否读取完毕
        while ((itemStr = reader.readLine()) != null) {
            sReqData += itemStr;
        }
        in.close();
        reader.close();
        log.info("公众号接收到消息:{}", sReqData);
    }

    @GetMapping(produces = "text/plain;charset=utf-8")
    protected void doGet(HttpServletRequest request, HttpServletResponse response, @PathVariable String corpId) throws AesException, IOException {

        log.info("request:" + JsonUtils.beanToJson(request.getParameterMap()));

        String msg_signature = request.getParameter("msg_signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");

        log.info("接收数据  msgSignature：" + msg_signature);
        log.info("接收数据  timeStamp：" + timestamp);
        log.info("接收数据  nonce：" + nonce);
        log.info("接收数据  echoStr：" + echostr);

        String token = "IwCNepma";
//        String corpId = "wx66edc7e6921995a2";
        String encodingAesKey = "bmiga9E7tPzEmrYHGWA6VlB6XqnsiXw3t30pvCQ6y6n";
        String result = new WXBizMsgCrypt(token, encodingAesKey, corpId).VerifyURL(msg_signature, timestamp, nonce, echostr);
        log.info("明文消息：" + result);
        PrintWriter writer = response.getWriter();
        writer.println(result);
    }
//
//    /**
//     * response)
//     */
//    @PostMapping
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        InputStream in = request.getInputStream();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
//        String sReqData = "";
//        String itemStr = "";//作为输出字符串的临时串，用于判断是否读取完毕
//        while ((itemStr = reader.readLine()) != null) {
//            sReqData += itemStr;
//        }
//        in.close();
//        reader.close();
//        log.info("公众号接收到消息:{}", sReqData);
//    }

}
