package com.gov.mobile.controller.workWeixin;

import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.mobile.dto.workWeixin.WorkwechatConfDto;
import com.gov.mobile.entity.WorkwechatConf;
import com.gov.mobile.request.RequestJson;
import com.gov.mobile.service.workWeixin.IWorkwechatConfService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.18
 */
@RestController
@RequestMapping("workwechatConf")
public class WorkwechatConfController {

    @Resource
    private IWorkwechatConfService workwechatConfService;

    @Log("企业微信配置集合")
    @PostMapping("workwechatConfList")
    @ApiOperation(value = "企业微信配置集合")
    public Result workwechatConfList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                     @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                     @RequestJson(value = "client", required = false) String client,
                                     @RequestJson(value = "gwcName", required = false) String gwcName,
                                     @RequestJson(value = "gwcId", required = false) String gwcId) {
        return workwechatConfService.workwechatConfList(pageSize, pageNum, client, gwcName, gwcId);
    }

    @Log("企业微信新增")
    @PostMapping("addWorkwechatConf")
    @ApiOperation(value = "企业微信新增")
    public Result addWorkwechatConf(@RequestBody WorkwechatConf workwechatConf) {
        return workwechatConfService.addWorkwechatConf(workwechatConf);
    }

    @Log("企业微信编辑")
    @PostMapping("editWorkwechatConf")
    @ApiOperation(value = "企业微信编辑")
    public Result editWorkwechatConf(@RequestBody WorkwechatConfDto workwechatConf) {
        return workwechatConfService.editWorkwechatConf(workwechatConf);
    }

    @Log("企业微信刪除")
    @PostMapping("deleteWorkwechatConf")
    @ApiOperation(value = "企业微信刪除")
    public Result deleteWorkwechatConf(@RequestBody WorkwechatConf workwechatConf) {
        return workwechatConfService.deleteWorkwechatConf(workwechatConf);
    }
}