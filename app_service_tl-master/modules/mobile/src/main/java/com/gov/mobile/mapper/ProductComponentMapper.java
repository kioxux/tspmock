package com.gov.mobile.mapper;

import com.gov.mobile.entity.ProductComponent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface ProductComponentMapper extends BaseMapper<ProductComponent> {

}
