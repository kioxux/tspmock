package com.gov.mobile.dto.workWeixin;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.05
 */
@Data
public class QyCustVo {

    /**
     * 企业微信ID
     */
    private String toUserName;

    /**
     * 固定
     */
    private String fromUserName;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 消息事件
     */
    private String msgType;

    /**
     * 事件
     */
    private String event;

    /**
     * 事件
     */
    private String changeType;

    /**
     * 企业服务人员的UserID
     */
    private String userID;

    /**
     * 外部联系人的userid，注意不是企业成员的帐号
     */
    private String externalUserID;

    /**
     * 欢迎语
     */
    private String welcomeCode;

    /**
     * 原xml数据
     */
    private String xmlStr;
}

