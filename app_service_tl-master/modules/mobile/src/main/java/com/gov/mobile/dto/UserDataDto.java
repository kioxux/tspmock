package com.gov.mobile.dto;

import com.gov.mobile.entity.UserData;
import io.swagger.models.auth.In;
import lombok.Data;

@Data
public class UserDataDto extends UserData {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 加盟商法人、委托人
     */
    private Integer isAdmin;

    /**
     * 门店员工 0非，1是
     */
    private Integer isStore;
}
