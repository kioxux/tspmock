package com.gov.mobile.mapper;

import com.gov.mobile.entity.SalesAnalysis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
public interface SalesAnalysisMapper extends BaseMapper<SalesAnalysis> {

    /**
     * 门店报表，当前月份明细
     *
     * @return
     */
    List<SalesAnalysis> getStoreReportByMonth(@Param("client") String client);

    /**
     * 门店报表，当前周明细
     *
     * @return
     */
    List<SalesAnalysis> getStoreReportByWeek(@Param("client") String client, @Param("index") int index);

    /**
     * 三个月汇总数据
     * @param client
     * @return
     */
    List<SalesAnalysis> getSummaryReportByMonth(@Param("client") String client);

    /**
     * 十二周汇总数据
     * @param client
     * @param start
     * @return
     */
    List<SalesAnalysis> getSummaryReportByWeek(@Param("client") String client, @Param("start") String start);
}
