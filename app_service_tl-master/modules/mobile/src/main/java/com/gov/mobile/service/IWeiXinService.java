package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.ActiveUserParams;
import com.gov.mobile.dto.UserGetCardInfo;
import com.gov.mobile.dto.WeiChatDto;
import com.gov.mobile.entity.AppVersion;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface IWeiXinService extends SuperService<AppVersion> {

    /**
     * 获取微信公众号openId
     */
    Result getOpenId(WeiChatDto weiChatDto);

    /**
     * 获取微信公众号openId （判断是否绑定企微用户）
     * @param weiChatDto
     * @return
     */
    Result getCusOpenId(WeiChatDto weiChatDto);


    /**
     * 获取用户信息
     * @param activeUserParams
     * @return
     */
    Result getActiveUserInfo(ActiveUserParams activeUserParams);

    /**
     * 激活会员
     * @param activeUserParams
     * @return
     */
    Result activeMember(ActiveUserParams activeUserParams);
}
