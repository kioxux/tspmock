package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHOBJ_DATA")
@ApiModel(value="AuthobjData对象", description="")
public class AuthobjData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "功能编号")
    @TableId("AUTHOBJ_ID")
    private String authobjId;

    @ApiModelProperty(value = "功能编号描述")
    @TableField("AUTHOBJ_NAME")
    private String authobjName;


}
