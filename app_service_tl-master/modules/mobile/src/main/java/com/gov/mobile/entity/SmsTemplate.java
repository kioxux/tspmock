package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-06-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SMS_TEMPLATE")
@ApiModel(value="SmsTemplate对象", description="")
public class SmsTemplate extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "短信模板ID")
    @TableId("SMS_ID")
    private String smsId;

    @ApiModelProperty(value = "模板ID")
    @TableField("SMS_TEMPLATE_ID")
    private String smsTemplateId;

    @ApiModelProperty(value = "短信类别")
    @TableField("SMS_TYPE")
    private String smsType;

    @ApiModelProperty(value = "模板主题")
    @TableField("SMS_THEME")
    private String smsTheme;

    @ApiModelProperty(value = "模板内容")
    @TableField("SMS_CONTENT")
    private String smsContent;

    @ApiModelProperty(value = "模板状态")
    @TableField("SMS_STATUS")
    private String smsStatus;

    @ApiModelProperty(value = "创建日期")
    @TableField("USER_CRE_DATE")
    private String userCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("USER_CRE_TIME")
    private String userCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("USER_CRE_ID")
    private String userCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("USER_MODI_DATE")
    private String userModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("USER_MODI_TIME")
    private String userModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("USER_MODI_ID")
    private String userModiId;


}
