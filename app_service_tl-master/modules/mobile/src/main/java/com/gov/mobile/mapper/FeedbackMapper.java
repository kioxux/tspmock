package com.gov.mobile.mapper;

import com.gov.mobile.entity.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-23
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

}
