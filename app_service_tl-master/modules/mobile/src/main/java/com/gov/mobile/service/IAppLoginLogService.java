package com.gov.mobile.service;

import com.gov.mobile.entity.AppLoginLog;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
public interface IAppLoginLogService extends SuperService<AppLoginLog> {

    AppLoginLog selectTokenByMaxLogintime(String client, String userId);
}
