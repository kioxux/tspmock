package com.gov.mobile.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-06-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_user_info")
@ApiModel(value="UserInfo对象", description="")
public class UserInfo extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "数据版本号")
    private Integer version;

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "性别(0:男 1:女)")
    private Integer sex;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "出生日期")
    private LocalDate birthday;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "状态(1:有效 0:停用)")
    private Integer userStatus;

    @ApiModelProperty(value = "公司名称")
    private String companyName;

    @ApiModelProperty(value = "公司省")
    private String companyProvince;

    @ApiModelProperty(value = "公司市")
    private String companyCity;

    @ApiModelProperty(value = "公司区")
    private String companyDistrict;

    @ApiModelProperty(value = "公司地址")
    private String companyAddress;

    @ApiModelProperty(value = "公司性质")
    private String companyNature;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "身份证号")
    private String idCard;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "企业id")
    private Integer companyId;


}
