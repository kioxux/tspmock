package com.gov.mobile.service;

import com.gov.mobile.entity.MemberLoginLog;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface IMemberLoginLogService extends SuperService<MemberLoginLog> {

}
