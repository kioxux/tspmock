package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.Verification;
import com.gov.mobile.entity.SmsTemplate;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-22
 */
public interface ISmsTemplateService extends SuperService<SmsTemplate> {

    /**
     * 短信发送
     */
    Result sendSms(Verification verification);

    /**
     * 新增门店短信发送
     * @param verification
     * @return
     */
    Result sendStoreRegisterSms(Verification verification);

    Result sendUpdateStoreRegisterSms(Verification verification);
}
