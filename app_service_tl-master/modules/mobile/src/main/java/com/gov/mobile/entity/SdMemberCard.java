package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_CARD")
@ApiModel(value="SdMemberCard对象", description="")
public class SdMemberCard extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员ID")
    @TableField("GSMBC_MEMBER_ID")
    private String gsmbcMemberId;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSMBC_CARD_ID")
    private String gsmbcCardId;

    @ApiModelProperty(value = "所属店号")
    @TableField("GSMBC_BR_ID")
    private String gsmbcBrId;

    @ApiModelProperty(value = "渠道")
    @TableField("GSMBC_CHANNEL")
    private String gsmbcChannel;

    @ApiModelProperty(value = "卡类型")
    @TableField("GSMBC_CLASS_ID")
    private String gsmbcClassId;

    @ApiModelProperty(value = "当前积分")
    @TableField("GSMBC_INTEGRAL")
    private String gsmbcIntegral;

    @ApiModelProperty(value = "最后积分日期")
    @TableField("GSMBC_INTEGRAL_LASTDATE")
    private String gsmbcIntegralLastdate;

    @ApiModelProperty(value = "清零积分日期")
    @TableField("GSMBC_ZERO_DATE")
    private String gsmbcZeroDate;

    @ApiModelProperty(value = "新卡创建日期")
    @TableField("GSMBC_CREATE_DATE")
    private String gsmbcCreateDate;

    @ApiModelProperty(value = "类型")
    @TableField("GSMBC_TYPE")
    private String gsmbcType;

    @ApiModelProperty(value = "openID")
    @TableField("GSMBC_OPEN_ID")
    private String gsmbcOpenId;

    @ApiModelProperty(value = "卡状态")
    @TableField("GSMBC_STATUS")
    private String gsmbcStatus;

    @ApiModelProperty(value = "开卡人员")
    @TableField("GSMBC_CREATE_SALER")
    private String gsmbcCreateSaler;

    @ApiModelProperty(value = "开卡门店")
    @TableField("GSMBC_OPEN_CARD")
    private String gsmbcOpenCard;

    @ApiModelProperty(value = "所属组织，单体：店号，连锁：连锁编码")
    @TableField("GSMBC_ORG_ID")
    private String gsmbcOrgId;

    @ApiModelProperty(value = "企微绑定状态，0、null：未绑定，1：已绑定")
    @TableField("GSMBC_WORKWECHAT_BIND_STATUS")
    private Integer gsmbcWorkwechatBindStatus;

    @ApiModelProperty(value = "激活状态 0、null:维持现有模式不变，即免费注册后立即生效，1: 收费注册需线下激活转正，2:收费注册线上支付后激活")
    @TableField("GSMBC_STATE")
    private String gsmbcState;

    @ApiModelProperty(value = "微信会员卡ID")
    @TableField("GSMBC_WX_CARD_ID")
    private String gsmbcWxCardId;

    @ApiModelProperty(value = "微信会员卡号")
    @TableField("GSMBC_WX_CARD_CODE")
    private String gsmbcWxCardCode;

    @ApiModelProperty(value = "推荐人")
    @TableField("GSMBC_RECOMMENDER")
    private String gsmbcRecommender;

}
