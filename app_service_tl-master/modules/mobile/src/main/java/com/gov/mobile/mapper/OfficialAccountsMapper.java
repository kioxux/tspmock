package com.gov.mobile.mapper;

import com.gov.mobile.dto.OfficialAccountsDTO;
import com.gov.mobile.entity.OfficialAccounts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
public interface OfficialAccountsMapper extends BaseMapper<OfficialAccounts> {

    OfficialAccountsDTO selectListByOriginalId(@Param("originalId") String originalId);

    OfficialAccountsDTO selectListByCardId(@Param("cardId") String cardId);
}
