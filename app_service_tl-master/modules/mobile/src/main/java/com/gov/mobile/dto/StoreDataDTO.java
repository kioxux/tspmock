package com.gov.mobile.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude
public class StoreDataDTO {

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 统一社会信用代码
     */
    private String stoNo;

    /**
     * 法人
     */
    private String stoLegalPerson;

    /**
     * 法人姓名
     */
    private String stoLegalPersonName;

    /**
     * 质量负责人
     */
    private String stoQua;

    /**
     * 质量负责人姓名
     */
    private String stoQuaName;

    /**
     * 质量负责人姓名
     */
    private String stoQuaPhone;

    /**
     * 负责人
     */
    private String stoLeader;

    /**
     * 负责人姓名
     */
    private String stoLeaderName;

    /**
     * 负责人姓名
     */
    private String stoLeaderPhone;

    /**
     * LOGO地址
     */
    private String stoLogo;

    /**
     * 连锁总部
     */
    private String stoChainHead;

    /**
     * 门店地址
     */
    private String stoAdd;

    /**
     * 门店状态 0停用,1启用
     */
    private String stoStatus;

    /**
     * 门店组编码
     */
    private String stogCode;

    /**
     * 门店组名称
     */
    private String stogName;

    /**
     * 门店属性
     */
    private String stoAttribute;

    /**
     * 纳税主体
     */
    private String stoTaxSubject;

    private String logoUrl;
}
