package com.gov.mobile.mapper;

import com.gov.mobile.entity.SdStoresGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 门店分类表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2022-02-24
 */
public interface SdStoresGroupMapper extends BaseMapper<SdStoresGroup> {

}
