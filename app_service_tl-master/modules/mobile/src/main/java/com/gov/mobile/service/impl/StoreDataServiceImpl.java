package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.userutil.UserUtils;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.mapper.AuthconfiDataMapper;
import com.gov.mobile.mapper.StoreDataMapper;
import com.gov.mobile.service.ISmsTemplateService;
import com.gov.mobile.service.IStogDataService;
import com.gov.mobile.service.IStoreDataService;
import com.gov.mobile.service.IUserDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Slf4j
@Service
public class StoreDataServiceImpl extends ServiceImpl<StoreDataMapper, StoreData> implements IStoreDataService {

    @Resource
    private IUserDataService userDataService;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private IStogDataService stogDataService;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private SmsUtils smsUtils;
    @Resource
    private ISmsTemplateService smsTemplateService;
    /**
     * 通过加盟商获取门店列表
     *
     * @param client
     * @return
     */
    @Override
    public List<StoreData> getStoreListByClient(String client) {
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        // 门店属性
        queryWrapper.in("STO_ATTRIBUTE",
                CommonEnum.StoreType.MONOMER_STORE.getCode(),
                CommonEnum.StoreType.CHAIN_STORE.getCode());
        queryWrapper.orderByAsc("STO_STATUS").orderByAsc("STO_ATTRIBUTE");
        return this.list(queryWrapper);
    }


    /**
     * 通过加盟商 + 当前连锁总部 s获取门店列表
     *
     * @param client    加盟商
     * @param chainHead 连锁总部
     * @return
     */
    @Override
    public List<StoreData> getStoreListByClientAndChainHead(String client, String chainHead) {
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STO_ATTRIBUTE", CommonEnum.StoreType.CHAIN_STORE.getCode());
        queryWrapper.eq("STO_CHAIN_HEAD", chainHead);
        return this.list(queryWrapper);
    }

    /**
     * 通过加盟商 + 门店编码 获取门店
     *
     * @param client    加盟商
     * @param storeCode 门店编码
     * @return
     */
    private StoreData getStoreByClientAndCode(String client, String storeCode) {
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STO_CODE", storeCode);
        StoreData storeData = this.getOne(queryWrapper);
        return storeData;
    }

    /**
     * 获取门店编码
     * @return
     */
    @Override
    public StoreDataCodeDTO getStoreCode() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        StoreData storeData = storeDataMapper.getMaxNumStoreCode(currentUser.getClient());
        if (StringUtils.isNotEmpty(storeData)) {
            Integer codeInteger = Integer.valueOf(storeData.getStoCode()) + 1;
            return new StoreDataCodeDTO(codeInteger.toString());
        }
        return new StoreDataCodeDTO("1000000000");
    }

    @Override
    public Result selectGroupStoreList() {
        List<StoreGDTO> list = new ArrayList<>();
        // 获取当前人加盟商
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        List<StoreGDTO> storeList = baseMapper.selectStoreListByClient(currentUser.getClient());
        if (storeList != null && storeList.size() > 0) {
            Map<String, List<StoreGDTO>> mapList = storeList.stream().collect(Collectors.groupingBy(StoreGDTO::getStogCode));
            for (Map.Entry<String, List<StoreGDTO>> entry : mapList.entrySet()) {
                StoreGDTO dto = new StoreGDTO();
                dto.setStogCode(entry.getKey());
                dto.setStogName(entry.getValue().get(0).getStogName());
                dto.setStoreList(entry.getValue());
                list.add(dto);
            }
        }
        return ResultUtil.success(list);
    }

    /**
     * 门店详情
     *
     * @param stoCode 门店编码
     */
    @Override
    public StoreDataDTO getStoreInfo(String stoCode) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        StoreData storeData = getStoreByClientAndCode(currentUser.getClient(), stoCode);
        StoreDataDTO storeDataDTO = new StoreDataDTO();
        BeanUtils.copyProperties(storeData, storeDataDTO);
        storeDataDTO.setLogoUrl(cosUtils.urlAuth(storeData.getStoLogo()));
        // 法人
        UserData user = userDataService.getUserByClientAndUserId(currentUser.getClient(), storeData.getStoLegalPerson());
        if (StringUtils.isNotEmpty(user)) {
            storeDataDTO.setStoLegalPersonName(user.getUserNam());
        }

        // 质量负责人
        user = userDataService.getUserByClientAndUserId(currentUser.getClient(), storeData.getStoQua());
        if (StringUtils.isNotEmpty(user)) {
            storeDataDTO.setStoQuaName(user.getUserNam());
            storeDataDTO.setStoQuaPhone(user.getUserTel());
        }

        // 店长
        user = userDataService.getUserByClientAndUserId(currentUser.getClient(), storeData.getStoLeader());
        if (StringUtils.isNotEmpty(user)) {
            storeDataDTO.setStoLeaderName(user.getUserNam());
            storeDataDTO.setStoLeaderPhone(user.getUserTel());
        }

        // 门店组
        StogData stogData = stogDataService.getNameByCode(currentUser.getClient(), storeData.getStogCode());
        if (StringUtils.isNotEmpty(stogData)) {
            storeDataDTO.setStogName(stogData.getStogName());
        }

        return storeDataDTO;
    }

    /**
     * 门店新增
     */
    @Override
    @Transactional
    public void addStore(StoreDataAddVO storeDataAddVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 校验门店编码是否被占用
        StoreData storeByClientAndCode = getStoreByClientAndCode(currentUser.getClient(), storeDataAddVO.getStoCode());
        if (StringUtils.isNotEmpty(storeByClientAndCode)) {
            throw new CustomResultException(ResultEnum.E0015);
        }

        // 门店属性是连锁，则连锁总部以及纳税主体不能为空
//        if (CommonEnum.StoreType.CHAIN_STORE.getCode().equals(storeDataAddVO.getStoAttribute())) {
//            if (StringUtils.isEmpty(storeDataAddVO.getStoChainHead()) || StringUtils.isEmpty(storeDataAddVO.getStoTaxSubject())) {
//                throw new CustomResultException(ResultEnum.E0023);
//            }
//        }
        String userLeaderId="";
        String userQuaId="";
        StoreData storeData = new StoreData();
        BeanUtils.copyProperties(storeDataAddVO, storeData);
        if (StringUtils.isNotEmpty(storeDataAddVO.getStoQuaPhone())) {
            //如果店长和质量负责人是同一人
            if (storeDataAddVO.getStoLeaderPhone().equals(storeDataAddVO.getStoQuaPhone())) {
                userLeaderId = getUserId(currentUser, storeDataAddVO, null);

            } else {
                userLeaderId = getUserId(currentUser, storeDataAddVO, CommonEnum.UserType.STATUS_LEADER.getCode());
                userQuaId = getUserId(currentUser, storeDataAddVO, CommonEnum.UserType.STATUS_QUA.getCode());

            }
        }else {
            userLeaderId = getUserId(currentUser, storeDataAddVO, null);
        }
        //店长
        storeData.setStoLeader(userLeaderId);
        //质量负责人
        storeData.setStoQua(userQuaId);
        // 门店状态
        storeData.setStoStatus(CommonEnum.StoStatus.STO_STATUS_YES.getCode());
        // 加盟商
        storeData.setClient(currentUser.getClient());
        // 创建日期
        storeData.setStoCreDate(DateUtils.getCurrentDateStrYYMMDD());
        // 创建时间
        storeData.setStoCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        // 创建人id
        storeData.setStoCreId(currentUser.getUserId());
        this.save(storeData);

    }

    /**
     * 判断手机号是否已经注册 如果存在直接拿编码 不存则在创建角色
     * @param currentUser
     * @param storeDataAddVO
     * @param code
     * @return
     */
    @Transactional
    public String  getUserId(UserLoginModelClient currentUser, StoreDataAddVO storeDataAddVO, String code) {
        String userId ="";
        UserData userData;
        if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
            userData = userDataService.getUserByClientAndPhone(currentUser.getClient(),storeDataAddVO.getStoQuaPhone());
        }else {
            userData = userDataService.getUserByClientAndPhone(currentUser.getClient(),storeDataAddVO.getStoLeaderPhone());
        }

        String passWord = String.valueOf(new Random().nextInt(900000) + 100000);
        if(StringUtils.isEmpty(userData)){
            userData = new UserData();
            int userMax = this.userDataService.selectMaxId(currentUser.getClient());
            userData.setClient(currentUser.getClient());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(currentUser.getUserId());
            userData.setDepId(storeDataAddVO.getStoCode()); //部门 改为门店
            userData.setDepName(storeDataAddVO.getStoName());
            userData.setUserId(String.valueOf(userMax + 1));
            if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
                userData.setUserNam(storeDataAddVO.getStoQuaName());
                userData.setUserTel(storeDataAddVO.getStoQuaPhone());
            }else {
                userData.setUserNam(storeDataAddVO.getStoLeaderName());
                userData.setUserTel(storeDataAddVO.getStoLeaderPhone());
            }
            userData.setUserPassword(UserUtils.encryptMD5(passWord));
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(currentUser.getUserId());
            userData.setUserCreId(currentUser.getUserId());
            this.userDataService.insert(userData);
            userId = String.valueOf(userMax + 1);
            //给店长发短信
            this.sendSms(storeDataAddVO,code,passWord);
        }else {
            userId = userData.getUserId();
            userData.setDepId(storeDataAddVO.getStoCode()); //部门 改为门店
            userData.setDepName(storeDataAddVO.getStoName());
            UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("CLIENT", currentUser.getClient());
            updateWrapper.eq("USER_ID", userId);
            this.userDataService.update(userData,updateWrapper);
            StoreDataEditVO storeDataEditVO = new StoreDataEditVO();
            storeDataEditVO.setClient(userData.getClient());
            storeDataEditVO.setStoLegalPerson(userData.getUserId());
            storeDataEditVO.setStoName(storeDataAddVO.getStoName());
            //给店长发短信
            this.sendUpdateSms(storeDataEditVO.getClient(),storeDataEditVO.getStoLegalPerson(),storeDataEditVO.getStoName(),code);
        }
//        this.saveAuthconfiData(currentUser.getClient(),userData,code);

        return userId;
    }




    public void sendSms(StoreDataAddVO storeDataAddVO,String code,String passWord) {
//        if(CommonEnum.UserType.STATUS_LEADER.getCode().equals(code)){
        Verification verification = new Verification(storeDataAddVO.getStoLeaderPhone(),"2",storeDataAddVO.getStoName(),passWord);
        smsTemplateService.sendStoreRegisterSms(verification);
//        }
    }

    /**
     * 门店编辑
     * @param storeDataEditVO 入参实体
     */
    @Override
    public void editStore(StoreDataEditVO storeDataEditVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();

        StoreData exitStoreData = getStoreByClientAndCode(currentUser.getClient(), storeDataEditVO.getStoCode());
        if (StringUtils.isEmpty(exitStoreData)) {
            throw new CustomResultException(ResultEnum.E0016);
        }
        // 门店属性是连锁，纳税主体不能为空
//        if (CommonEnum.StoreType.CHAIN_STORE.getCode().equals(storeDataEditVO.getStoAttribute())) {
//            if (StringUtils.isEmpty(storeDataEditVO.getStoTaxSubject())) {
//                throw new CustomResultException("纳税主体不能为空");
//            }
//        }
        storeDataEditVO.setClient(currentUser.getClient());
        StoreData storeData = new StoreData();
        BeanUtils.copyProperties(storeDataEditVO, storeData);
        storeData.setStoModiDate(DateUtils.getCurrentDateStrYYMMDD());
        storeData.setStoModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        storeData.setStoModiId(currentUser.getUserId());

        UpdateWrapper<StoreData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", currentUser.getClient());
        updateWrapper.eq("STO_CODE", storeDataEditVO.getStoCode());
        this.update(storeData, updateWrapper);
        storeDataEditVO.setStoLegalPerson(exitStoreData.getStoLeader());
        if (!exitStoreData.getStoLeader().equals(storeData.getStoLeader())) {
            this.updateAuthconfiData(exitStoreData, storeDataEditVO,exitStoreData.getStoLeader(),CommonEnum.AuthGroup.SD_01);

        }
        if (StringUtils.isNotEmpty(storeData.getStoQua())) {
            if (!exitStoreData.getStoQua().equals(storeData.getStoQua())) {
//                this.updateAuthconfiData(exitStoreData, storeDataEditVO, exitStoreData.getStoQua(), CommonEnum.AuthGroup.GAIA_SD_ZLGL);
            }
        }
    }


    /**
     * 编辑时更新权限数据
     * @param exitStoreData
     * @param storeDataEditVO
     */
    private void updateAuthconfiData(StoreData exitStoreData, StoreDataEditVO storeDataEditVO,String userId, CommonEnum.AuthGroup auth) {

//        // 删除旧的权限数据
        authconfiDataMapper.deleteOne(exitStoreData.getClient(), auth.getCode(), exitStoreData.getStoCode(), userId);
        // 添加新的权限数据

        AuthconfiData authconfiData = new AuthconfiData();
        authconfiData.setClient(exitStoreData.getClient());
        authconfiData.setAuthGroupId(auth.getCode());
        authconfiData.setAuthGroupName(auth.getMessage());
        authconfiData.setAuthobjSite(exitStoreData.getStoCode());
        authconfiData.setAuthconfiUser(userId);
        authconfiDataMapper.insert(authconfiData);
        this.sendUpdateSms(storeDataEditVO.getClient(),storeDataEditVO.getStoLegalPerson(),storeDataEditVO.getStoName(),auth.getCode());

    }
    public void sendUpdateSms(String client,String legalPersonId, String stoName,String code) {
//        if(CommonEnum.UserType.STATUS_LEADER.getCode().equals(code)){
            UserData userData = userDataService.getUserByClientAndUserId(client,legalPersonId);
            Verification verification = new Verification(userData.getUserTel(),"2",stoName,null);
            smsTemplateService.sendUpdateStoreRegisterSms(verification);
//        }
    }


}
