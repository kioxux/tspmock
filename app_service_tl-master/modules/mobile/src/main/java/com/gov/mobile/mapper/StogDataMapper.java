package com.gov.mobile.mapper;

import com.gov.mobile.entity.StogData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-07
 */
public interface StogDataMapper extends BaseMapper<StogData> {

}
