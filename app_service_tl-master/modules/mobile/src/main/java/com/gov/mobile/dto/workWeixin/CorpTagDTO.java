package com.gov.mobile.dto.workWeixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CorpTagDTO {

    private List<String> tag_id;

    private List<String> group_id;

}

