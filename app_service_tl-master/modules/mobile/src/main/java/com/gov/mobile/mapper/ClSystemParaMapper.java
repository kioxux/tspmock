package com.gov.mobile.mapper;

import com.gov.mobile.entity.ClSystemPara;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司参数表 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-12-07
 */
public interface ClSystemParaMapper extends BaseMapper<ClSystemPara> {

}
