package com.gov.mobile.mapper;

import com.gov.mobile.entity.AppVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface AppVersionMapper extends BaseMapper<AppVersion> {

    /**
     * APP最新版本
     *
     * @param type        平台类型 1:android 2:ios
     * @param releaseFlag 是否发布 1:否,2:是
     * @return
     */
    AppVersion getAppLastVersion(@Param("type") Integer type, @Param("releaseFlag") String releaseFlag);
}
