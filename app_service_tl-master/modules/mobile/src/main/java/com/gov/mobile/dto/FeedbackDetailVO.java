package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class FeedbackDetailVO {

    @NotBlank(message = "id不能为空")
    private String id;
}
