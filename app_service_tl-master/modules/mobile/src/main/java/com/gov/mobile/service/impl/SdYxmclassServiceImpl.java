package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.ProductGroupVo;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.entity.SdYxmclass;
import com.gov.mobile.mapper.SdYxmclassMapper;
import com.gov.mobile.service.ISdYxmclassService;
import com.gov.mobile.service.IUserDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
@Service
public class SdYxmclassServiceImpl extends ServiceImpl<SdYxmclassMapper, SdYxmclass> implements ISdYxmclassService {

    @Resource
    private SdYxmclassMapper sdYxmclassMapper;
    @Resource
    private IUserDataService userDataService;

    @Override
    public Result selectGroupAndProductList(String gsmStore) {
        if (StringUtils.isEmpty(gsmStore)) {
            throw new CustomResultException("缺少门店编号");
        }
        List<ProductGroupVo> productGroupVoList = new ArrayList<>();

        UserLoginModelClient currentUser = userDataService.getCurrentUser();

        QueryWrapper<SdYxmclass> queryWrapper = new QueryWrapper<SdYxmclass>()
                .eq("CLIENT", currentUser.getClient())
                .eq("GSY_STORE", gsmStore);
        List<SdYxmclass> classList = baseMapper.selectList(queryWrapper);
        if (classList != null && classList.size() > 0) {
            Map<String, List<SdYxmclass>> mapList = classList.stream()
                    .collect(Collectors.groupingBy(SdYxmclass::getGsyGroup));
            ProductGroupVo productGroupVo = null;
            for (Map.Entry<String, List<SdYxmclass>> entry : mapList.entrySet()) {
                productGroupVo = new ProductGroupVo();
                productGroupVo.setGsyGroup(entry.getKey());
                productGroupVo.setGsyGroupname(entry.getValue().get(0).getGsyGroupname());
                productGroupVo.setProductList(entry.getValue());
                productGroupVoList.add(productGroupVo);
            }
        }
        return ResultUtil.success(productGroupVoList);
    }


}
