package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CompadmVO {
    private String client;
    @NotBlank(message = "连锁总部名称不能为空")
    private String compadmName;

    @NotBlank(message = "统一社会信用代码不能为空")
    private String compadmNo;

//    @NotBlank(message = "法人/负责人不能为空")
    private String compadmLegalPerson;

//    @NotBlank(message = "质量负责人不能为空")
    private String compadmQua;

//    @NotBlank(message = "详细地址不能为空")
    private String compadmAddr;

//    @NotBlank(message = "LOGO地址不能为空")
    private String compadmLogo;

    @NotBlank(message = "法人/负责人手机号不能为空")
    private String compadmLegalPhone;

    //    @NotBlank(message = "质量负责人手机号码不能为空")
    private String compadmQuaPhone;

    @NotBlank(message = "负责人名不能为空")
    private String compadmLegalName;

    //@NotBlank(message = "质量负责人名不能为空")
    private String compadmQuaName;
//    @NotBlank(message = "连锁总部ID")
    private String compadmId;

}
