package com.gov.mobile.service;

import com.gov.mobile.dto.SaveDepVO;
import com.gov.mobile.dto.UpdateDepVO;
import com.gov.mobile.entity.DepData;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface IDepDataService extends SuperService<DepData> {

    /**
     * 新增部门
     */
    void saveDep(SaveDepVO saveDepVO);

    /**
     * 部门编辑
     */
    void updateDep(UpdateDepVO updateDepVO);

    /**
     * 对应加盟商下面的产部门
     */
    List<DepData> getDepList(String client, String depType);

    /**
     * 对应加盟商下面的产部门
     */
    List<DepData> getDepList(String client, String depType, String chainHead);

    /**
     * 部门详情
     */
    DepData getDep(String depId);
}
