package com.gov.mobile.dto.workWeixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TagGroupDTO {
    private String group_id;

    private String group_name;

    private Long create_time;

    private Integer order;

    private Boolean deleted;

    private List<TagDTO> tag;

}
