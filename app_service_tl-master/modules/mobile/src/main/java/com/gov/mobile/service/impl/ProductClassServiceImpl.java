package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.entity.ProductClass;
import com.gov.mobile.mapper.ProductClassMapper;
import com.gov.mobile.service.IProductClassService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
@Service
public class ProductClassServiceImpl extends ServiceImpl<ProductClassMapper, ProductClass> implements IProductClassService {

    @Override
    public Result selectProductClassList() {
        QueryWrapper<ProductClass> queryWrapper = new QueryWrapper<ProductClass>()
                .eq("PRO_CLASS_STATUS", "0");
        return ResultUtil.success(baseMapper.selectList(queryWrapper));
    }
}
