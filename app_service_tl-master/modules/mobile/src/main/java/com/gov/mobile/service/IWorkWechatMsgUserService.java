package com.gov.mobile.service;

import com.gov.mobile.entity.WorkwechatMsgUser;
import com.gov.mybatis.SuperService;

public interface IWorkWechatMsgUserService extends SuperService<WorkwechatMsgUser> {

}
