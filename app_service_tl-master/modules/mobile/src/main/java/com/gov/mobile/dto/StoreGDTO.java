package com.gov.mobile.dto;

import com.gov.mobile.entity.StoreData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class StoreGDTO extends StoreData {

    /**
     * 门店组名
     */
    private String stogName;

    /**
     * 门店集合
     */
    private List<StoreGDTO> storeList;

}
