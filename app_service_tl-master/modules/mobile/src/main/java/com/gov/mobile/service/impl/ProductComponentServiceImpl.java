package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.entity.ProductClass;
import com.gov.mobile.entity.ProductComponent;
import com.gov.mobile.mapper.ProductComponentMapper;
import com.gov.mobile.service.IProductComponentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
@Service
public class ProductComponentServiceImpl extends ServiceImpl<ProductComponentMapper, ProductComponent> implements IProductComponentService {

    @Override
    public Result selectProductComponentList() {
        QueryWrapper<ProductComponent> queryWrapper = new QueryWrapper<ProductComponent>()
                .eq("PRO_COMP_STATUS", "0");
        return ResultUtil.success(baseMapper.selectList(queryWrapper));
    }
}
