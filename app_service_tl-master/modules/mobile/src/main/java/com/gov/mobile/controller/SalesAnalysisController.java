package com.gov.mobile.controller;

import com.gov.common.response.Result;
import com.gov.mobile.service.ISalesAnalysisService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 分析报表
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@RestController
@RequestMapping("salesAnalysis")
public class SalesAnalysisController {

    @Resource
    private ISalesAnalysisService iSalesAnalysisService;

    @PostMapping("getStoreReportByMonth")
    @ApiOperation(value = "门店报表，当前月份明细")
    public Result getStoreReportByMonth() {
        return iSalesAnalysisService.getStoreReportByMonth();
    }

    @PostMapping("getStoreReportByWeek")
    @ApiOperation(value = "门店报表，当前周明细")
    public Result getStoreReportByWeek() {
        return iSalesAnalysisService.getStoreReportByWeek();
    }

    @PostMapping("getSummaryReportByMonth")
    @ApiOperation(value = "汇总报表，三个月数据")
    public Result getSummaryReportByMonth() {
        return iSalesAnalysisService.getSummaryReportByMonth();
    }

    @PostMapping("getSummaryReportByWeek")
    @ApiOperation(value = "汇总报表，十二周数据")
    public Result getSummaryReportByWeek() {
        return iSalesAnalysisService.getSummaryReportByWeek();
    }
}

