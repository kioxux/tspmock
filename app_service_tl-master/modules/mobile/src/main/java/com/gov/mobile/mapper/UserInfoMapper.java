package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.entity.UserInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-01
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    IPage<UserInfo> selectPageList(Page<UserInfo> page, @Param("ew") QueryWrapper<UserInfo> wrapper);

}
