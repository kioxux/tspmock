package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_USER_MESSAGE")
@ApiModel(value="UserMessage对象", description="")
public class UserMessage extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "接收人加盟商ID")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "接收人员工编号")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "发送时间")
    @TableField("SEND_TIME")
    private String sendTime;

    @ApiModelProperty(value = "消息类型(预留:暂为1)")
    @TableField("MESSAGE_TYPE")
    private String messageType;

    @ApiModelProperty(value = "是否已读（0：未读，1：已读）")
    @TableField("IS_READ")
    private Integer isRead;

    @ApiModelProperty(value = "阅读时间")
    @TableField("READ_TIME")
    private String readTime;

    @ApiModelProperty(value = "标题")
    @TableField("TITLE")
    private String title;

    @ApiModelProperty(value = "内容")
    @TableField("CONTENT")
    private String content;


}
