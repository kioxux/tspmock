package com.gov.mobile.mapper;

import com.gov.mobile.entity.UserData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-17
 */
public interface UserDataMapper extends BaseMapper<UserData> {
    int selectMaxId(String client);

}
