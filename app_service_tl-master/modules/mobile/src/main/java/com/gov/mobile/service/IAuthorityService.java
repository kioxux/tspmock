package com.gov.mobile.service;

import com.gov.mobile.dto.GroupDataDto;
import com.gov.mobile.dto.OrganizationDto;
import com.gov.mobile.dto.OrganizationListDTO;
import com.gov.mobile.dto.UserByClientDTO;
import com.gov.mobile.entity.AuthobjData;
import com.gov.mobile.entity.StoreData;

import java.util.List;


public interface IAuthorityService {


    /**
     * 权限功能获取
     *
     * @return
     */
    List<AuthobjData> getAuthobjList();


    /**
     * 岗位数据获取
     *
     * @return
     */
    Object getGroupList(String authobjId);


    /**
     * 门店集合获取
     *
     * @return
     */
    List<StoreData> getStoreList();

    /**
     * 权限用户数据获取
     */
    Object getAuthConfi(String authGroupId, String authobjId, String stoCode);

    /**
     * 组织获取
     *
     * @return
     */
    OrganizationListDTO getAuthOrg();


    /**
     * 组织获取 保存到一个list中
     *
     * @return List<OrganizationDto>
     */
    List<OrganizationDto> getAuthOrgToOneList();


    /**
     * 用户权限数据保存
     *
     * @return
     */
    Integer setAuthConfi(GroupDataDto param);

    /**
     * 获取加盟商用户数据
     */
    List<UserByClientDTO> getUserByClient();


    /**
     * 用户权限数据删除
     *
     * @return
     */
    Object delAuthConfi(GroupDataDto param);


    /**
     * 用户权限数据修改
     *
     * @param param
     * @return
     */
    Object editAuthConfi(GroupDataDto param);
}
