package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_AUTHCONFI_DATA")
@ApiModel(value = "AuthconfiData对象", description = "")
public class AuthconfiData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商ID")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "权限组(岗位)编号")
    @TableField("AUTH_GROUP_ID")
    private String authGroupId;

    @ApiModelProperty(value = "权限组(岗位)描述")
    @TableField("AUTH_GROUP_NAME")
    private String authGroupName;

    @ApiModelProperty(value = "SITE=门店/部门")
    @TableField("AUTHOBJ_SITE")
    private String authobjSite;

    @ApiModelProperty(value = "人员")
    @TableField("AUTHCONFI_USER")
    private String authconfiUser;


}
