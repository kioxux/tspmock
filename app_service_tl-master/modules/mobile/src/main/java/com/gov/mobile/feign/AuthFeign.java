package com.gov.mobile.feign;

import com.gov.mobile.feign.dto.GetWorkflowInData;
import com.gov.mobile.feign.dto.JsonResult;
import com.gov.mobile.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(value = "gys-auth", fallback = AuthFeignFallback.class)
public interface AuthFeign {

    @PostMapping({"/workflow/selectApprovingListApp"})
    public JsonResult selectApprovingListApp(@RequestBody GetWorkflowInData inData);

    @PostMapping({"/workflow/selectApprovedListApp"})
    public JsonResult selectApprovedListApp(@RequestBody GetWorkflowInData inData);

    @PostMapping({"/workflow/selectOneApp"})
    public JsonResult selectOneApp(@RequestBody GetWorkflowInData inData);

    @PostMapping({"/workflow/approveApp"})
    public JsonResult approveApp(@RequestBody GetWorkflowInData inData);

    @PostMapping({"/workflow/ccApp"})
    public JsonResult ccApp(@RequestBody GetWorkflowInData inData);

    @PostMapping({"/workflow/cancelApp"})
    public JsonResult cancelApp(@RequestBody GetWorkflowInData inData);

    @PostMapping({"/workflow/getUserListByClientId"})
    public JsonResult getUserListByClientId(@RequestBody GetWorkflowInData inData);

}
