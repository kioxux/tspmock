package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.common.entity.Dictionary;
import com.gov.mobile.entity.Compadm;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface CompadmMapper extends BaseMapper<Compadm> {

    /**
     * 指定加盟商下，连锁总部id数字编码最大值
     */
    Compadm getMaxNumCompadmId(@Param("client") String client);

    /**
     * 纳税主体选择
     */
    List<Dictionary> getTaxSubject(@Param("client") String client, @Param("chainHead") String chainHead);

}
