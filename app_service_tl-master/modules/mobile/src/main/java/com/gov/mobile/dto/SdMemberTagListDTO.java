package com.gov.mobile.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
@Data
public class SdMemberTagListDTO  {

    private String client;

    private String gsmtlCardId;

    private String gsmtlTagId;

    private String disease;

    private String diseaseTagId;

    private String ingredientClassification;

    private String classTagId;


}
