package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 企业微信配置
 * @TableName GAIA_WORKWECHAT_CONF
 */
@TableName(value ="GAIA_WORKWECHAT_CONF")
@Data
public class WorkwechatConf implements Serializable {
    /**
     * 企业ID
     */
    @TableId
    private String gwcId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 企业微信名
     */
    private String gwcName;

    /**
     * 应用secret
     */
    private String gwcAppSecret;

    /**
     * 客户secret
     */
    private String gwcCusSecret;

    /**
     * 类型 1、加盟商，2、连锁，3、门店，4、DC
     */
    private Integer gwcWechatType;

    /**
     * 对象编码（公众号）
     */
    private String gwcWechatCode;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}