package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_MEMBER_TAG_LIST")
@ApiModel(value = "SdMemberTagList对象", description = "")
public class SdMemberTagList extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSMTL_CARD_ID")
    private String gsmtlCardId;

    @ApiModelProperty(value = "标签编号")
    @TableField("GSMTL_TAG_ID")
    private String gsmtlTagId;

    @ApiModelProperty(value = "消费金额")
    @TableField("GSMTL_AMT")
    private BigDecimal gsmtlAmt;

    @ApiModelProperty(value = "消费次数")
    @TableField("GSMTL_SALE_TIMES")
    private String gsmtlSaleTimes;

    @ApiModelProperty(value = "更新日期")
    @TableField("GSMTL_UPDATE_DATE")
    private String gsmtlUpdateDate;

    @ApiModelProperty(value = "计算月份")
    @TableField("GSMTL_CALC_MONTH")
    private String gsmtlCalcMonth;


}
