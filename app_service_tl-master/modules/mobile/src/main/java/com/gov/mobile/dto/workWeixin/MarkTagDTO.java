package com.gov.mobile.dto.workWeixin;

import lombok.Data;

import java.util.List;

@Data
public class MarkTagDTO {

    private String userid;

    private String external_userid;

    private List<String> add_tag;

    private List<String> remove_tag;
}
