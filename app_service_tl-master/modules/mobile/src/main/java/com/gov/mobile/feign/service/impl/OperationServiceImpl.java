package com.gov.mobile.feign.service.impl;

import com.gov.common.basic.JsonUtils;
import com.gov.common.response.Result;
import com.gov.mobile.feign.OperationFeign;
import com.gov.mobile.feign.service.OperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class OperationServiceImpl implements OperationService {

    @Resource
    private OperationFeign operationFeign;


    /**
     * 会员新增/修改
     *
     * @param client   加盟商
     * @param memberId 会员ID
     * @param cardId   会员卡号
     */
    @Override
    public void memberSync(String client, String memberId, String cardId) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("client", client);
            map.put("memberId", memberId);
            map.put("cardId", cardId);
            log.info("[会员新增/修改][/sync/memberSync]参数:{}", map);
            Result result = operationFeign.memberSync(map);
            log.info("[会员新增/修改][/sync/memberSync]返回结果{}", JsonUtils.beanToJson(result));
        } catch (Exception e) {
            log.info("[会员新增/修改 gys-operation调用 异常]",e.getMessage());
        }
    }
}
