package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SaveDepVO {

    @NotBlank(message = "部门名称不能为空")
    private String depName;

    @NotBlank(message = "部门负责人ID不能为空")
    private String depHeadId;

    @NotBlank(message = "部门负责人姓名不能为空")
    private String depHeadNam;

    @NotBlank(message = "类型不能为空")
    private String depType;

    //@NotBlank(message = "连锁总部不能为空")
    private String stoChainHead;

    @NotBlank(message = "是否停用不能为空")
    private String depStatus;

//    @NotBlank(message = "部门代码不能为空")
    //部门类型(11.质管部 12.商采部 13.运营部 14.财务部 16.人事行政部 15.信息部 17.物料仓 10.配送中心)
    private String depCls;

}
