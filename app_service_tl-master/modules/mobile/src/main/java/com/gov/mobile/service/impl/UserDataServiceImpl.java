package com.gov.mobile.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.userutil.UserUtils;
import com.gov.common.uuid.UUIDUtil;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.AppLoginLog;
import com.gov.mobile.entity.Franchisee;
import com.gov.mobile.entity.StoreData;
import com.gov.mobile.entity.UserData;
import com.gov.mobile.mapper.StoreDataMapper;
import com.gov.mobile.mapper.UserDataMapper;
import com.gov.mobile.service.IAppLoginLogService;
import com.gov.mobile.service.IFranchiseeService;
import com.gov.mobile.service.IUserDataService;
import com.gov.redis.jedis.RedisClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-17
 */
@Service
public class UserDataServiceImpl extends ServiceImpl<UserDataMapper, UserData> implements IUserDataService {
    @Resource
    private IFranchiseeService iFranchiseeService;
    @Resource
    private IAppLoginLogService iAppLoginLogService;
    @Resource
    private UserDataMapper userDataMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private StoreDataMapper storeDataMapper;


    /**
     * 获取token (手机号+密码)
     */
    @Override
    public Result getTokenByTelPasswd(UserLoginModel userLoginModel) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_TEL", userLoginModel.getUserTel());
        wrapper.eq("USER_PASSWORD", StringUtils.getMD5(userLoginModel.getUserPswd()));
        List<UserData> list = this.list(wrapper);
        if (list.size() < 1) {
            return ResultUtil.error(ResultEnum.E0002);
        }

        List<UserLoginModelClient> users = getClientsUserOnJob(list);
        if (users.size() < 1) {
            return ResultUtil.error(ResultEnum.E0003);
        }
        //刚好一个返回token
        if (users.size() == 1) {
            UserLoginModelClient user = users.get(0);
            return getTokenOnlyOneUser(user.getClient(), user.getUserId(), user.getDepId(),user.getLoginName(),userLoginModel.getPlatform(), userLoginModel.getDeviceNo());
        }
        //多个返回client列表
        return ResultUtil.success(new TokenData(Constants.TokenType.CLIENT_LIST, users));
    }

    /**
     * 获取token (选择加盟商方式)
     */
    @Override
    public Result getTokenByClient(UserLoginModelClient modelClient) {
        return getTokenOnlyOneUser(modelClient.getClient(), modelClient.getUserId(),modelClient.getDepId(),modelClient.getLoginName(), modelClient.getPlatform(), modelClient.getDeviceNo());
    }


    /**
     * 获取token（手机号+验证码）
     */
    @Override
    public Result getTokenByTelVerificationCode(VerificationToToken verificationToToken) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_TEL", verificationToToken.getUserTel());
        List<UserData> list = this.list(wrapper);
        //未查询到用户
        if (list.size() < 1) {
            return ResultUtil.error(ResultEnum.E0006);
        }
        //均为非在职
        List<UserLoginModelClient> users = getClientsUserOnJob(list);
        if (users.size() < 1) {
            return ResultUtil.error(ResultEnum.E0003);
        }
        //刚好一个返回token
        if (users.size() == 1) {
            UserLoginModelClient user = users.get(0);
            String redisVerification = redisClient.get(verificationToToken.getUserTel() + Constants.SMSBusinessType.BUSINESS_LOGIN);
            if (!verificationToToken.getCode().equals(redisVerification)) {
                return ResultUtil.error(ResultEnum.E0005);
            }
            return getTokenOnlyOneUser(user.getClient(), user.getUserId(), user.getDepId(),user.getLoginName(), verificationToToken.getPlatform(), verificationToToken.getDeviceNo());
        }
        //多个返回client列表
        return ResultUtil.success(new TokenData(Constants.TokenType.CLIENT_LIST, users));
    }

    /**
     * 只获得一个用户，且该用户在职
     */
    private Result getTokenOnlyOneUser(String client, String userId,String  depId, String userName,String platform, String deviceNo) {
        // JWT生成token
        String token = UserUtils.createToken(client, userId,depId, userName, UserUtils.PLATFORM_1);

        //清缓存
        AppLoginLog logTemp = iAppLoginLogService.selectTokenByMaxLogintime(client, userId);
        if (null != logTemp) {
            redisClient.del(logTemp.getToken());
        }
        //日志
        AppLoginLog appLoginLog = new AppLoginLog();
        appLoginLog.setId(UUIDUtil.getUUID());
        appLoginLog.setClient(client);
        appLoginLog.setUserId(userId);
        appLoginLog.setToken(token);
        appLoginLog.setPlatform(platform);
        appLoginLog.setDeviceNo(deviceNo);
        appLoginLog.setLogintime(DateUtils.getCurrentDateTimeStrFull());
        appLoginLog.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
        appLoginLog.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        appLoginLog.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
        appLoginLog.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        iAppLoginLogService.save(appLoginLog);

        //添加缓存
        UserLoginModelClient modelTemp = new UserLoginModelClient();
        modelTemp.setClient(client);
        modelTemp.setUserId(userId);
        modelTemp.setDepId(depId);
        modelTemp.setLoginName(userName);
        redisClient.set(token, JSONObject.toJSONString(modelTemp));

        TokenData tokenData = new TokenData();
        tokenData.setTokenType(Constants.TokenType.TOKEN);
        tokenData.setData(token);
        return ResultUtil.success(tokenData);
    }

    /**
     * 获取在职加盟商列表
     */
    private List<UserLoginModelClient> getClientsUserOnJob(List<UserData> list) {
        //用户不止一个，返回这些用户的 加盟商编号，加盟商名称，员工编号。
        List<UserLoginModelClient> users = new ArrayList<UserLoginModelClient>();
        for (UserData userData : list) {
            if (!CommonEnum.UserSta.ON_THE_JOB.getCode().equals(userData.getUserSta())) {
                continue;
            }
            UserLoginModelClient modelTemp = new UserLoginModelClient();
            Franchisee franchisee = iFranchiseeService.getById(userData.getClient());
            if (null == franchisee) {
                continue;
            }
            modelTemp.setFrancName(franchisee.getFrancName());
            modelTemp.setLoginName(userData.getUserNam());
            modelTemp.setClient(userData.getClient());
            modelTemp.setUserId(userData.getUserId());
            modelTemp.setDepId(userData.getDepId());
            users.add(modelTemp);
        }
        return users;
    }

    /**
     * 检查手机号是否存在
     */
    @Override
    public boolean checkTel(String tel) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("USER_TEL", tel);
        List<UserData> list = this.list(wrapper);
        if (list.size() < 1) {
            throw new CustomResultException(ResultEnum.E0006);
        }

        List<UserLoginModelClient> users = getClientsUserOnJob(list);
        if (users.size() < 1) {
            throw new CustomResultException(ResultEnum.E0003);
        }
        return true;
    }

    /**
     * 获取用户信息
     */
    @Override
    public Result getUser() {
        UserData userData = getCurentUserDetail();
        UserDataDto userDataDto = new UserDataDto();
        BeanUtils.copyProperties(userData, userDataDto, "userPassword");
        Franchisee franchisee = iFranchiseeService.getById(userData.getClient());
        if (StringUtils.isNotEmpty(franchisee)) {
            userDataDto.setFrancName(franchisee.getFrancName());
        }
        userDataDto.setIsAdmin(0);
        if (userData.getUserId().equals(franchisee.getFrancLegalPerson())) {
            userDataDto.setIsAdmin(1);
        }
        if (userData.getUserId().equals(franchisee.getFrancAss())) {
            userDataDto.setIsAdmin(1);
        }
        userDataDto.setIsStore(0);
        QueryWrapper<UserData> userDataQueryWrapper = new QueryWrapper<>();
        userDataQueryWrapper.eq("CLIENT", userData.getClient());
        userDataQueryWrapper.eq("USER_ID", userData.getUserId());
        UserData entity = userDataMapper.selectOne(userDataQueryWrapper);
        if (entity != null && StringUtils.isNotBlank(entity.getDepId())) {
            QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<StoreData>();
            storeDataQueryWrapper.eq("CLIENT", userData.getClient());
            storeDataQueryWrapper.eq("STO_CODE", entity.getDepId());
            StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
            if (storeData != null) {
                userDataDto.setIsStore(1);
            }
        }
        return ResultUtil.success(userDataDto);
    }

    /**
     * 修改密码
     */
    @Override
    public Result changeUserPswd(ChangeUserPswdVO changeUserPswdVO) {

        UserData userData = getCurentUserDetail();
        String oldPwdMD5 = StringUtils.getMD5(changeUserPswdVO.getOldUserPswd());
        if (!userData.getUserPassword().equals(oldPwdMD5)) {
            return ResultUtil.error(ResultEnum.E0008);
        }

        UserData et = new UserData();
        String newPwdMD5 = StringUtils.getMD5(changeUserPswdVO.getNewUserPswd());
        et.setUserPassword(newPwdMD5);
        QueryWrapper<UserData> updateWrapper = new QueryWrapper<>();
        updateWrapper.eq("USER_TEL", userData.getUserTel());
        updateWrapper.eq("USER_PASSWORD", userData.getUserPassword());
        int count = userDataMapper.update(et, updateWrapper);
        if (count < 1) {
            return ResultUtil.error(ResultEnum.E0009);
        }
        return ResultUtil.success();
    }

    /**
     * 检测密码是否正确
     */
    @Override
    public Result checkUserPswd(CheckUserPswdVO checkUserPswdVO) {
        UserData userData = getCurentUserDetail();
        String pwd = StringUtils.getMD5(checkUserPswdVO.getUserPswd());
        if (userData.getUserPassword().equals(pwd)) {
            return ResultUtil.success();
        }
        return ResultUtil.error(ResultEnum.E0010);
    }

    /**
     * 退出
     *
     * @return
     */
    @Override
    public Result logout() {
        try {
            String tokenCode = UserUtils.getTokenCode();
            redisClient.del(tokenCode);
        } catch (Exception e) {
        } finally {
            return ResultUtil.success();
        }
    }

    /**
     * 获取当前用户
     */
    @Override
    public UserLoginModelClient getCurrentUser() {
        String tokenCode = UserUtils.getTokenCode();
        String userStr = redisClient.get(tokenCode);
        if (org.apache.commons.lang3.StringUtils.isBlank(userStr)) {
            throw new CustomResultException(ResultEnum.E0007);
        }
        try {
            UserLoginModelClient userLoginModelClient = JsonUtils.jsonToBean(userStr, UserLoginModelClient.class);
            if (userLoginModelClient == null) {
                throw new CustomResultException(ResultEnum.E0007);
            }
            return userLoginModelClient;
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0007);
        }
    }

    /**
     * 获取当前用户详细信息
     */
    public UserData getCurentUserDetail() {
        UserLoginModelClient userModelClient = getCurrentUser();
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", userModelClient.getClient());
        wrapper.eq("USER_ID", userModelClient.getUserId());
        UserData userData = this.getOne(wrapper);
        if (null == userData) {
            throw new CustomResultException(ResultEnum.E0007);
        }
        return userData;
    }

    /**
     * 根据加盟商和员工编号获取用户
     *
     * @param client
     * @param userId
     * @return
     */
    @Override
    public UserData getUserByClientAndUserId(String client, String userId) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", client);
        wrapper.eq("USER_ID", userId);
        return this.getOne(wrapper);
    }

    /**
     * 根据加盟商和员工手机号获取用户
     *
     * @param client
     * @param phone
     * @return
     */
    @Override
    public UserData getUserByClientAndPhone(String client, String phone) {
        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", client);
        wrapper.eq("USER_TEL", phone);
        return this.getOne(wrapper);
    }

    /**
     * 获取userID
     *
     * @return
     */
    @Override
    public int selectMaxId(String client) {
        return userDataMapper.selectMaxId(client);
    }

    @Override
    public void insert(UserData userData) {
        userDataMapper.insert(userData);
    }

}
