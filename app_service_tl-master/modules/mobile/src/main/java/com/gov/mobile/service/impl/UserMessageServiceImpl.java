package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.dto.UserMessageListVO;
import com.gov.mobile.entity.UserMessage;
import com.gov.mobile.mapper.UserMessageMapper;
import com.gov.mobile.service.IUserDataService;
import com.gov.mobile.service.IUserMessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Service
public class UserMessageServiceImpl extends ServiceImpl<UserMessageMapper, UserMessage> implements IUserMessageService {

    @Resource
    private IUserDataService iUserDataService;

    @Resource
    private UserMessageMapper userMessageMapper;

    /**
     * 用户消息列表
     */
    @Override
    public Result messageList(UserMessageListVO userMessageListVO) {
        UserLoginModelClient currentUser = iUserDataService.getCurrentUser();
        QueryWrapper<UserMessage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", currentUser.getClient());
        queryWrapper.eq("USER_ID", currentUser.getUserId());
        if (StringUtils.isNotEmpty(userMessageListVO.getMessageType())) {
            queryWrapper.eq("MESSAGE_TYPE", userMessageListVO.getMessageType());
        }
        queryWrapper.orderByDesc("SEND_TIME");
        Page<UserMessage> page = new Page<>(userMessageListVO.getPageNum(), userMessageListVO.getPageSize());
        IPage<UserMessage> iPage = userMessageMapper.selectPage(page, queryWrapper);
        return ResultUtil.success(iPage);
    }

    /**
     * 用户消息详情(并标为已读)
     */
    @Override
    public Result messageDetail(String id) {
        UserMessage messageDetail = this.getById(id);
        if (StringUtils.isEmpty(messageDetail)) {
            return ResultUtil.error(ResultEnum.E0013);
        }

        UserMessage userMessage = new UserMessage();
        userMessage.setId(id);
        userMessage.setIsRead(Constants.UserMessage.READ);
        userMessage.setReadTime(DateUtils.getCurrentDateTimeStrFull());
        userMessageMapper.updateById(userMessage);

        return ResultUtil.success(messageDetail);
    }
}
