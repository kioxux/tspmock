package com.gov.mobile.dto.workWeixin;

import lombok.Data;

@Data
public class Dictionary {

    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;


    private Integer sort;


}
