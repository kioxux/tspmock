package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName GAIA_WORKWECHAT_MSG
 */
@TableName(value = "GAIA_WORKWECHAT_MSG")
@Data
public class WorkwechatMsg implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String client;

    /**
     * 任务名称
     */
    private String gwmTheme;

    /**
     * 任务说明
     */
    private String gwmRemark;

    /**
     * 任务开始时间
     */
    private Date gwmStartDate;

    /**
     * 任务结束时间
     */
    private Date gwmEndDate;

    /**
     * 任务类型（1-企微关注员工，2-离职继承后员工）
     */
    private String gwmType;

    /**
     * 活动开始时间
     */
    private Date gwmActStartDate;

    /**
     * 活动结束时间
     */
    private Date gwmActEndDate;

    /**
     * 内容
     */
    private String gwmContent;

    /**
     * 推送类型（1-立即推送，2-定时推送）
     */
    private String gwmSendType;

    /**
     * 预计发送时间
     */
    private Date gwmEstSendTime;

    /**
     * 消息类型（1-图片，2-图文，3-小程序）
     */
    private String gwmMsgType;

    /**
     * 附件数组json（包括图片，小程序，视频，语音）
     */
    private String gwmAttachement;

    /**
     * 发送状态（0-待发送，1-发送成功，2-发送失败）
     */
    private String gwmSendStatus;

    /**
     * 实际发送时间
     */
    private Date gwmActSendTime;

    /**
     * 创建人
     */
    private String gwmCreateBy;

    /**
     * 创建时间
     */
    private Date gwmCreateTime;

    /**
     * 更新人
     */
    private String gwmUpdateBy;

    /**
     *
     */
    private Date gwmUpdateTime;

    /**
     * 存cos路径键值对
     */
    private String gwmPaths;

    /**
     * 发送失败微信会员id
     */
    private String gwmFailMember;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}