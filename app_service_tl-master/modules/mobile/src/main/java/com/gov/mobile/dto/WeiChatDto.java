package com.gov.mobile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "获取微信公众号的openId")
public class WeiChatDto {

    @NotBlank(message = "加盟商id不能为空")
    @ApiModelProperty(value = "加盟商id")
    private String client;

    @NotBlank(message = "所属门店不能为空")
    @ApiModelProperty(value = "门店|连锁")
    private String stoCode;


    @NotBlank(message = "对象编码能为空")
    @ApiModelProperty(value = "对象编码")
    private String code;

    @NotNull(message = "0：登录，1：不登录")
    @ApiModelProperty(value = "0：登录，1：不登录")
    private Integer login;

    @ApiModelProperty(value = "门店")
    private String openStore;

}
