package com.gov.mobile.controller;

import com.gov.common.response.Result;
import com.gov.mobile.feign.dto.GetWorkflowInData;
import com.gov.mobile.feign.dto.JsonResult;
import com.gov.mobile.service.WorkflowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.08.12
 */
@Api(tags = "待办")
@RestController
@RequestMapping("workflow")
public class WorkflowController {

    @Resource
    private WorkflowService workflowService;

    /**
     * 待办
     * @param inData
     * @return
     */
    @PostMapping("selectApprovingList")
    @ApiOperation(value = "待办")
    public JsonResult selectApprovingList(@RequestBody GetWorkflowInData inData) {
        return workflowService.selectApprovingList(inData);
    }

    /**
     * 已办
     * @param inData
     * @return
     */
    @PostMapping("selectApprovedList")
    @ApiOperation(value = "已办")
    public JsonResult selectApprovedList(@RequestBody GetWorkflowInData inData) {
        return workflowService.selectApprovedList(inData);
    }

    /**
     * 详情
     * @param inData
     * @return
     */
    @PostMapping("selectOne")
    @ApiOperation(value = "详情")
    public JsonResult selectOne(@RequestBody GetWorkflowInData inData) {
        return workflowService.selectOne(inData);
    }

    /**
     * 审批
     * @param inData
     * @return
     */
    @PostMapping("approve")
    @ApiOperation(value = "审批")
    public JsonResult approve(@RequestBody GetWorkflowInData inData) {
        return workflowService.approve(inData);
    }

    /**
     * 会签
     * @param inData
     * @return
     */
    @PostMapping("cc")
    @ApiOperation(value = "会签")
    public JsonResult cc(@RequestBody GetWorkflowInData inData) {
        return workflowService.cc(inData);
    }

    /**
     * 取消
     * @param inData
     * @return
     */
    @PostMapping("cancel")
    @ApiOperation(value = "取消")
    public JsonResult cancel(@RequestBody GetWorkflowInData inData) {
        return workflowService.cancel(inData);
    }

    /**
     * 会签员工
     * @param inData
     * @return
     */
    @PostMapping("getUserListByClientId")
    @ApiOperation(value = "会签员工")
    public JsonResult getUserListByClientId(@RequestBody GetWorkflowInData inData) {
        return workflowService.getUserListByClientId(inData);
    }
}

