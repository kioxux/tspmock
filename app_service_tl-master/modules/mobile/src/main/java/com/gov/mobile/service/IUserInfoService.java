package com.gov.mobile.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.entity.UserInfo;
import com.gov.common.response.Result;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-01
 */
public interface IUserInfoService extends SuperService<UserInfo> {

    Result selectPage(Page<UserInfo> page, QueryWrapper<UserInfo> wrapper);

}
