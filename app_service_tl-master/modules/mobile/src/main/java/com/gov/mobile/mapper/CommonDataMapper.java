package com.gov.mobile.mapper;

import com.gov.mobile.entity.CommonData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface CommonDataMapper extends BaseMapper<CommonData> {

}
