package com.gov.mobile.service;

import com.gov.common.entity.Pageable;
import com.gov.common.response.Result;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.SdMemberBasic;
import com.gov.mybatis.SuperService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-29
 */
public interface ISdMemberBasicService extends SuperService<SdMemberBasic> {
    /**
     * 注册
     */
    Result register(MemberUserDto modelClient);

    /**
     *
     * @param client
     * @param cardNumber
     */
    void addElectron(String client, String cardNumber);

    /**
     * 登录
     */
    Result Login(MemberUserLoginDto modelClient);

    /**
     * 获取当前登录人
     */
    Result getUserInfo();

    /**
     * 绑定
     */
    Result bindUserInfo(MemberBindUserDto bindUserDto);

    /**
     * 获取积分记录
     */
    Result getPointList(Pageable page);
    /**
     * 获取余额
     */
    Result getMoneyRecordList(Pageable page);

    /**
     * 获取优惠券
     */
    Result getCouponRecordList(PageDto page);

    /**
     * 获取门店
     */
    Result getStoreList(String client, String stoCode);

    /**
     * 获取微信公众号的配置
     */
    Result getConfigList( ConfigDto configDto);

    /**
     * 公众号会员注册(含企微绑定)
     * @param modelClient
     * @return
     */
    Result cusRegister(MemberUserDto modelClient);
}
