package com.gov.mobile.mapper;

import com.gov.mobile.dto.ElectronReserved;
import com.gov.mobile.entity.SdElectronChange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-27
 */
public interface SdElectronChangeMapper extends BaseMapper<SdElectronChange> {

    /**
     * 预留优惠券
     *
     * @param client
     * @return
     */
    List<ElectronReserved> selectElectronReserved(@Param("client") String client);


    Integer selectGsecId(@Param("client") String client, @Param("gsecId") String gsecId);
}
