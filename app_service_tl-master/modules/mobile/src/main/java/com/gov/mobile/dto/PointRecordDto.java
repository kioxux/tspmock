package com.gov.mobile.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PointRecordDto{

    /**
     * 门店
     */
    private String stoName;

    /**
     * 日期
     */
    private String pointDate;
    /**
     * 积分
     */
    private String integral;

    /**
     * 0消耗，1增加
     */
    private Integer type;

    /**
     * 变动金额
     */
    private String amt;

    /**
     * 门店详细地址
     */
    private String stoAdd;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String stoCode;


    /**
     * 电子券金额
     */
    private BigDecimal gspcbCouponAmt;

    /**
     * 描述
     */
    private String gspcsCouponRemarks;

    /**
     * 生效日期 yyyyMMdd
     */
    private String gapcsBeginDate;

    /**
     * 生效时间 HHmmss
     */
    private String gapcsBeginTime;
    /**
     * 失效日期 yyyyMMdd
     */
    private String gapcsEndDate;

    /**
     * 失效时间 HHmmss
     */
    private String gapcsEndTime;
    /**
     * 日期 yyyyMMdd
     * 已使用列表用，表示为用券日期
     */
    private String gspcbDate;

    /**
     *1:通用券 2:品类券
     */
    private Integer gspcsCouponType;

    /**
     * 积分产生日期
     */
    private String date;

}
