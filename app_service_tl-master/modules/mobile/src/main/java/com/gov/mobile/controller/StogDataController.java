package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.service.IStogDataService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-07
 */
@RestController
@RequestMapping("stog")
public class StogDataController {



    @Resource
    private IStogDataService stogDataService;

    @GetMapping("getStogList")
    @ApiOperation(value = "获取门店列表")
    public Result getStogList() {
        return ResultUtil.success(stogDataService.getStogList());
    }
}

