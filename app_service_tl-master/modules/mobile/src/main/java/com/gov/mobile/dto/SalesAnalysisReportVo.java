package com.gov.mobile.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.07.01
 */
@Data
public class SalesAnalysisReportVo {

    /**
     * 年
     */
    private String year;

    /**
     * 周
     */
    private String week;

    /**
     * 月
     */
    private String month;

    /**
     * 横轴
     */
    private String xAxis;

    /**
     * 纵轴
     */
    private BigDecimal yAxis;

    /**
     * 加盟商名
     */
    private String francName;

    /**
     * 销售额
     */
    private List<SalesAnalysisReportVo> amtList;

    /**
     * 毛利率
     */
    private List<SalesAnalysisReportVo> gmList;

    /**
     * 交易次数
     */
    private List<SalesAnalysisReportVo> tansList;

    /**
     * 客单价
     */
    private List<SalesAnalysisReportVo> priceList;

}
