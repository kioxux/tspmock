package com.gov.mobile.mapper;

import com.gov.mobile.dto.StoreGDTO;
import com.gov.mobile.entity.StoreData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
public interface StoreDataMapper extends BaseMapper<StoreData> {

    /**
     * 获取门店编码
     */
    StoreData getMaxNumStoreCode(@Param("client") String client);

    /**
     * 获取门店列表
     *
     * @param client
     * @return
     */
    List<StoreGDTO> selectStoreListByClient(@Param("client") String client);

    List<String> selectListForAuth(@Param("client") String client, @Param("chainHead") String chainHead);

    StoreData getMinStore(@Param("client") String client, @Param("stoChainHead") String stoChainHead);

    StoreData getMinStoreByClient(@Param("client") String client);
}
