package com.gov.mobile.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.mobile.service.IAppVersionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@RestController
@RequestMapping("appManage")
public class AppManageController {

    @Resource
    private IAppVersionService iAppVersionService;

    @Log("APP最新版本")
    @GetMapping("getAppLastVersion")
    @ApiOperation(value = "APP最新版本")
    public Result getAppLastVersion(@RequestParam(name = "type", required = true) Integer type) {
        return iAppVersionService.getAppLastVersion(type);
    }

}

