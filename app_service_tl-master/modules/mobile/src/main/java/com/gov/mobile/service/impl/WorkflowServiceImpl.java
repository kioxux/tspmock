package com.gov.mobile.service.impl;

import com.gov.common.basic.JsonUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.feign.AuthFeign;
import com.gov.mobile.feign.dto.GetWorkflowInData;
import com.gov.mobile.feign.dto.JsonResult;
import com.gov.mobile.service.IUserDataService;
import com.gov.mobile.service.WorkflowService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.08.12
 */
@Slf4j
@Service
public class WorkflowServiceImpl implements WorkflowService {

    @Resource
    private AuthFeign authFeign;
    @Resource
    private IUserDataService userDataService;

    /**
     * 待办
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult selectApprovingList(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        inData.setPageNum(inData.getPageNum());
        inData.setPageSize(inData.getPageSize());
        log.info("待办列表调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.selectApprovingListApp(inData);
        log.info("待办列表调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 已办
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult selectApprovedList(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        inData.setPageNum(inData.getPageNum());
        inData.setPageSize(inData.getPageSize());
        log.info("已办列表调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.selectApprovedListApp(inData);
        log.info("已办列表调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 详情
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult selectOne(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        log.info("审批详情调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.selectOneApp(inData);
        log.info("审批详情调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 审批
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult approve(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        log.info("审批调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.approveApp(inData);
        log.info("审批调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 会签
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult cc(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        log.info("会签调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.ccApp(inData);
        log.info("会签调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 取消
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult cancel(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        log.info("取消调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.cancelApp(inData);
        log.info("取消调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 会签员工列表
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult getUserListByClientId(GetWorkflowInData inData) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        inData.setClient(currentUser.getClient());
        inData.setUserId(currentUser.getUserId());
        log.info("会签员工列表调用参数：{}", JsonUtils.beanToJson(inData));
        JsonResult result = authFeign.getUserListByClientId(inData);
        log.info("会签员工列表调用返回值：{}", JsonUtils.beanToJson(result));
        return result;
    }


}

