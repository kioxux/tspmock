package com.gov.mobile.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OfficialAccountsDTO {

    /**
     * 对象编码/公众号编码/门店/连锁总部
     **/
    private String goaCode;

    /**
     * 公众号名/门店名/连锁门店总部名
     **/
    private String goaName;

    /**
     * 1:门店，2：连锁
     **/
    private Integer goaType;

    /**
     * LOGO
     **/
    private String logo;

    /**
     * LOGO绝对地址
     **/
    private String logoUrl;

    /**
     * 特权说明
     **/
    private String memberBenefits;

    /**
     * 有效日期
     **/
    private String accountsValidPeriod;

    /**
     * 使用须知
     **/
    private String accountsUsageNotice;

    /**
     * 电话号码
     **/
    private String accountsTel;

    /**
     * APPID
     */
    private String accountsAppid;

    /**
     * 密钥
     */
    private String accountsAppsecret;

    /**
     * 是否推送（0:未推送，1:已推送）
     */
    private Integer gwthPush;

    /**
     * 是否发布（0：未操作，1：已发布，2：不发布）
     */
    private Integer gwthRelease;

    /**
     * 发布时间
     */
    private String gwthPushTime;

    /**
     * 加盟商编码
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;
    // 微信推文授权
    private String wechatTweets;

    // 微信会员卡
    private String wechatMembershipCard;

    /**
     * 计划推送时间
     */
    private String gwthPlanPushTime;

    /**
     * 推送记录主表id
     */
    private Long weChatTweetId;

    /**
     * 微信原始ID
     */
    private String originalId;

    /**
     * 会员卡ID
     */
    private String memberCardId;
}
