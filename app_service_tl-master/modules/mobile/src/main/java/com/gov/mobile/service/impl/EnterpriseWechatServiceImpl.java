package com.gov.mobile.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.controller.workWeixin.Utils;
import com.gov.mobile.dto.SdMemberTagListDTO;
import com.gov.mobile.dto.workWeixin.Dictionary;
import com.gov.mobile.dto.workWeixin.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.mapper.*;
import com.gov.mobile.service.IEnterpriseWechatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EnterpriseWechatServiceImpl implements IEnterpriseWechatService {


    private final static String DISEASE_NAME = "疾病名称";

    private final static String CLASSIFCATION_NAME = "成分名称";

    @Resource
    private WorkwechatConfMapper workwechatConfMapper;

    @Resource
    private OfficialAccountsMapper officialAccountsMapper;

    @Resource
    private UserDataMapper userDataMapper;

    @Resource
    private StoreDataMapper storeDataMapper;

    @Resource
    private WorkwechatCusUserBindMapper workwechatCusUserBindMapper;

    @Resource
    private SdMemberCardMapper sdMemberCardMapper;

    @Resource
    private DiseaseClassifcationMapper diseaseClassifcationMapper;

    @Resource
    private SdMemberTagListMapper sdMemberTagListMapper;
    @Resource
    private SdStoresGroupMapper sdStoresGroupMapper;


    @Value("${wx.register-url}")
    private String registerUrl;

    @Value("${wx.link-pic-url}")
    private String linkPicUrl;

    /**
     * 验证客户是否绑定 && 发送客户提示语
     *
     * @param msgVo
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendCusWelcomeMessage(String cropId, QyCustVo msgVo) throws Exception {
        WorkwechatConf conf = workwechatConfMapper.selectById(msgVo.getToUserName());
        if (conf == null) {
            log.info("企业微信未配置，微信ID:{}", msgVo.getToUserName());
            throw new CustomResultException("企业微信未配置");
        }
        Wrapper<OfficialAccounts> params = new QueryWrapper<OfficialAccounts>()
                .eq("CLIENT", conf.getClient())
//                .eq("GOA_TYPE", conf.getGwcWechatType())
                .eq("GOA_CODE", conf.getGwcWechatCode())
                //特权说明
                .eq("GOA_OBJECT_PARAM", "WECHAT_OFFICIAL_MEMBER_BENEFITS");
        OfficialAccounts accounts = officialAccountsMapper.selectOne(params);
        if (accounts == null) {
            log.error("未配置特权说明。微信ID:{}", msgVo.getToUserName());
            throw new CustomResultException("未配置特权说明");
        }
        // 获取企业微信应用accessToken
        String accessToken = Utils.accessToken(conf.getGwcId(), conf.getGwcAppSecret());
        // 二维码持有者信息
        String userJson = Utils.getUserJson(accessToken, msgVo.getUserID());
        if (StrUtil.isBlank(userJson)) {
            log.error("获取用户异常");
            throw new CustomResultException("获取用户异常");
        }

        JSONObject jsonObject = JSON.parseObject(userJson);
        if (jsonObject.getIntValue("errcode") != 0) {
            log.error("未获取到用户");
            throw new CustomResultException("未获取到用户");
        }

        // 获取用户手机
        String userTel = jsonObject.getString("mobile");

        QueryWrapper<UserData> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", conf.getClient());
        wrapper.eq("USER_STA", "0"); // 在职
        wrapper.eq("USER_TEL", userTel);
        List<UserData> userList = userDataMapper.selectList(wrapper);
        if (userList.size() == 0) {
            log.error("药德系统中未找到用户");
            throw new CustomResultException("药德系统中未找到用户");
        } else if (userList.size() > 1) {
            log.error("药德系统存在多个用户");
            throw new CustomResultException("药德系统存在多个用户");
        }
        UserData userData = CollUtil.get(userList, 0);

        if (StrUtil.isBlank(userData.getDepId())) {
            log.error("药德系统组织为空");
            throw new CustomResultException("药德系统组织为空");
        }

        // ---- S 用于返回链接
        String client = userData.getClient();
        String stoCode = "";
        String openStore = "";
        // ---- E 用于返回链接

        StoreData storeData = storeDataMapper.selectOne(new QueryWrapper<StoreData>()
                .eq("CLIENT", userData.getClient()).eq("STO_CODE", userData.getDepId()));
        if (storeData == null) {
            log.info("depid不是门店");
            // 查询最小门店
            StoreData sd = storeDataMapper.getMinStoreByClient(userData.getClient());
            if (sd == null) {
                log.error("连锁下没有门店");
                throw new CustomResultException("连锁下没有门店");
            }
            // stoCode 连锁号; openStore 最小门店号
            stoCode = StringUtils.isBlank(sd.getStoChainHead()) ? sd.getStoCode() : sd.getStoChainHead();
            openStore = sd.getStoCode();
        } else {
            // 判断门店是否为直营管理，“是”，关联企业微信，“否”不关联企业微信。
            SdStoresGroup sdStoresGroup = sdStoresGroupMapper.selectOne(new LambdaQueryWrapper<SdStoresGroup>()
                    .eq(SdStoresGroup::getClient, userData.getClient())
                    .eq(SdStoresGroup::getGssgType, "DX0003")
                    .eq(SdStoresGroup::getGssgBrId, userData.getDepId()));
            if (sdStoresGroup != null) {
                // “否”不关联企业微信
                // DX0003	0	是否直营管理	是
                // DX0003	1	是否直营管理	否
                if ("1".equals(sdStoresGroup.getGssgId())) {
                    log.info("“否”不关联企业微信。");
                    return;
                }
            } else {
                log.info(" 没维护按非直营 “否”不关联企业微信。");
                return;
            }
            // stoCode 门店号; openStore 门店号
            stoCode = StringUtils.isBlank(storeData.getStoChainHead()) ? storeData.getStoCode() : storeData.getStoChainHead();
            openStore = userData.getDepId();
        }

        // 获取accessToken
        String cusAccessToken = Utils.accessToken(conf.getGwcId(), conf.getGwcCusSecret());

        WxWelcomeMsgDTO welcomeMsgDTO = new WxWelcomeMsgDTO();
        // 欢迎code
        welcomeMsgDTO.setWelcome_code(msgVo.getWelcomeCode());
        // 设置欢迎消息
        welcomeMsgDTO.setText(new TextDTO().setContent(accounts.getGoaObjectValue()));

        String url = StrUtil.format("{}?client={}&stoCode={}&openStore={}&gwcId={}&userId={}&wwUserId={}&memberId={}",
                registerUrl, client, stoCode, openStore, cropId, userData.getUserId(), msgVo.getUserID(), msgVo.getExternalUserID());

        welcomeMsgDTO.setAttachments(Collections.singletonList(new WxAttachmentDTO()
                .setMsgtype("link").setLink(new LinkDTO().setTitle("点我进行会员开卡！").setUrl(url)
                        .setPicurl(linkPicUrl))));

        log.info("返回url地址：" + url);
        String result = Utils.sendWelcomeMsg(cusAccessToken, welcomeMsgDTO);
        log.info("企业微信发消息返回值{}", result);
        JSONObject resultObj = JSON.parseObject(result);
        if (resultObj.getIntValue("errcode") != 0) {
            log.error("欢迎语返回结果：" + result);
            throw new CustomResultException("发送欢迎语失败");
        }
    }


    /**
     * 解除企微绑定
     *
     * @param msgVo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void unbindCus(QyCustVo msgVo) {
        WorkwechatCusUserBind bind = workwechatCusUserBindMapper.selectOne(new QueryWrapper<WorkwechatCusUserBind>()
                .eq("GWC_ID", msgVo.getToUserName())
                .eq("GWCUB_WW_USER_ID", msgVo.getUserID())
                .eq("GWCUB_WW_MEMBER_ID", msgVo.getExternalUserID()));
        if (bind == null) {
            // 删除失败
            log.error("未找到会员卡无法删除，微信id:{},企微用户:{}, 客户：{}", msgVo.getToUserName(),
                    msgVo.getUserID(), msgVo.getExternalUserID());
            throw new CustomResultException("未找到会员卡无法删除");
        }
        int count = workwechatCusUserBindMapper.deleteById(bind.getId());
        if (count > 0) {
            SdMemberCard card = new SdMemberCard();
            // 未绑定
            card.setGsmbcWorkwechatBindStatus(0);
            sdMemberCardMapper.update(card,
                    new QueryWrapper<SdMemberCard>().eq("CLIENT", bind.getClient())
                            .eq("GSMBC_MEMBER_ID", bind.getGwcubMemberId())
                            .eq("GSMBC_BR_ID", bind.getGwcubBrId())
            );
            // 删除成功
            log.info("解除企微绑定成功，微信id:{},企微用户:{}, 客户：{}", msgVo.getToUserName(),
                    msgVo.getUserID(), msgVo.getExternalUserID());
        } else {
            // 删除失败
            log.error("解除企微绑定失败，微信id:{},企微用户:{}, 客户：{}", msgVo.getToUserName(),
                    msgVo.getUserID(), msgVo.getExternalUserID());
        }
    }

    /**
     * 同步标签
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void asyncTag() throws Exception {
        // 获取微信配置
        List<WorkwechatConf> configList = workwechatConfMapper.selectList(null);

        for (WorkwechatConf conf : configList) {
            // 获取accessToken
            String cusAccessToken = Utils.accessToken(conf.getGwcId(), conf.getGwcCusSecret());

            // 同步标签
            asyncTagList(conf, cusAccessToken);

            // 同步标签用户
            asyncTagUserList(conf, cusAccessToken);
        }

    }

    @Override
    public Result getTagList() {
        List<Dictionary> resultList = new ArrayList<>();
        List<DiseaseClassifcation> list = diseaseClassifcationMapper.selectList(null);
        if (list.size() > 0) {
            List<Dictionary> finalResultList = resultList;
            list.forEach(t -> {
                if (finalResultList.stream().noneMatch(a -> a.getValue().equals(t.getDisease()))) {
                    Dictionary tag = new Dictionary();
                    tag.setValue(t.getDisease());
                    tag.setLabel(t.getDisease());
                    tag.setSort(1);
                    finalResultList.add(tag);
                }

                if (finalResultList.stream().noneMatch(a -> a.getValue().equals(t.getIngredientClassification()))) {
                    Dictionary tag = new Dictionary();
                    tag.setValue(t.getIngredientClassification());
                    tag.setLabel(t.getIngredientClassification());
                    tag.setSort(2);
                    finalResultList.add(tag);
                }
            });
        }

        resultList = resultList.stream().sorted(Comparator.comparing(Dictionary::getSort)).collect(Collectors.toList());

        return ResultUtil.success(resultList);
    }

    /**
     * 同步标签用户
     *
     * @param conf
     * @param cusAccessToken
     * @throws Exception
     */
    private void asyncTagUserList(WorkwechatConf conf, String cusAccessToken) throws Exception {
        // 从微信端获取标签  疾病名称/成分名称
        List<TagGroupDTO> tagGroupDTOList = Utils.getCorpTagList(cusAccessToken, new CorpTagDTO());
        // 疾病标签
        TagGroupDTO diseaseTag = tagGroupDTOList.stream().filter(t -> t.getGroup_name().equals("疾病名称")).findFirst().orElse(new TagGroupDTO());
        // 成分标签
        TagGroupDTO componentTag = tagGroupDTOList.stream().filter(t -> t.getGroup_name().equals("成分名称")).findFirst().orElse(new TagGroupDTO());

        // 同步用户
        List<WorkwechatCusUserBind> bindUserList = workwechatCusUserBindMapper.selectList(
                new QueryWrapper<WorkwechatCusUserBind>().eq("GWC_ID", conf.getGwcId()));

        for (WorkwechatCusUserBind bind : bindUserList) {
            String cusJson = Utils.getCusJson(cusAccessToken, bind.getGwcubWwMemberId());
            if (StrUtil.isNotBlank(cusJson)) {
                JSONObject cusInfo = JSON.parseObject(cusJson);
                if (cusInfo.getIntValue("errcode") == 0) {
                    List<FollowUserDTO> followUserList = JSON.parseArray(cusInfo.getString("follow_user"), FollowUserDTO.class);
                    FollowUserDTO user = followUserList.stream()
                            .filter(t -> t.getUserid().equals(bind.getGwcubWwUserId())).findFirst().orElse(null);
                    if (user != null) {
                        List<SdMemberTagListDTO> bindTagList = sdMemberTagListMapper.selectMemberTagList(bind.getClient(), bind.getGwcubCardId());

                        bindTagList.stream().forEach(t -> {
                            TagDTO tag = diseaseTag.getTag().stream()
                                    .filter(a -> a.getName().equals(t.getDisease())).findFirst().orElse(null);
                            if (tag != null) {
                                t.setDiseaseTagId(tag.getId());
                            }
                            TagDTO tagDTO = componentTag.getTag().stream()
                                    .filter(a -> a.getName().equals(t.getIngredientClassification())).findFirst().orElse(null);
                            if (tagDTO != null) {
                                t.setClassTagId(tagDTO.getId());
                            }
                        });


//                            List<String> delTagList = user.getTags()
//                                    .stream().filter(t -> bindTagList
//                                            .stream().noneMatch(a ->
//                                                    a.getDisease().equals(t.getTag_name())
//                                                            || a.getIngredientClassification().equals(t.getTag_name())))
//                                    .map(CusTagDTO::getTag_id)
//                                    .collect(Collectors.toList());

                        List<String> addTagIdList = new ArrayList<>();
                        bindTagList.forEach(t -> {
                            addTagIdList.add(t.getClassTagId());
                            addTagIdList.add(t.getDiseaseTagId());
                        });

                        if (addTagIdList.size() > 0
//                                    || delTagList.size() > 0
                        ) {
                            MarkTagDTO dto = new MarkTagDTO();
                            dto.setUserid(user.getUserid());
                            dto.setExternal_userid(bind.getGwcubWwMemberId());
                            dto.setAdd_tag(addTagIdList);
//                                dto.setRemove_tag(delTagList);
                            Utils.markTag(cusAccessToken, dto);
                        }
                    }
                }
            }
        }
    }

    /**
     * 同步标签
     *
     * @param conf
     * @param cusAccessToken
     * @throws Exception
     */
    private void asyncTagList(WorkwechatConf conf, String cusAccessToken) throws Exception {
        // 获取药德系统中所有成分/疾病
        List<DiseaseClassifcation> diseaseClassifcationList =
                diseaseClassifcationMapper.getDiseaseClassifcationListBySite(conf.getClient(), conf.getGwcWechatCode());
        // 疾病名称集合
        Set<String> diseaseSet = new HashSet<>();
        // 成分名称集合
        Set<String> classifcationSet = new HashSet<>();

        if (diseaseClassifcationList.size() > 0) {
            diseaseSet = diseaseClassifcationList.stream().map(DiseaseClassifcation::getDisease).collect(Collectors.toSet());
            classifcationSet = diseaseClassifcationList.stream().map(DiseaseClassifcation::getIngredientClassification).collect(Collectors.toSet());
        }

        // 从微信端获取标签  疾病名称/成分名称
        List<TagGroupDTO> tagGroupDTOList = Utils.getCorpTagList(cusAccessToken, new CorpTagDTO());

        // 疾病标签添加
        TagGroupDTO diseaseTagDto = getAddTagGroup(DISEASE_NAME, diseaseSet, tagGroupDTOList);

        if (diseaseTagDto.getTag().size() > 0) {
            Utils.addCorpTag(cusAccessToken, diseaseTagDto);
        }

        // 成分标签添加
        TagGroupDTO classifcationTagDto = getAddTagGroup(CLASSIFCATION_NAME, classifcationSet, tagGroupDTOList);
        if (classifcationTagDto.getTag().size() > 0) {
            Utils.addCorpTag(cusAccessToken, classifcationTagDto);
        }
    }


    /**
     * 获取需要新增的标签
     *
     * @param typeName        类型名称
     * @param set             已存在的标签
     * @param tagGroupDTOList 数据库中的标签
     * @return
     */
    private TagGroupDTO getAddTagGroup(String typeName, Set<String> set, List<TagGroupDTO> tagGroupDTOList) {
        TagGroupDTO dto = null;
        if (tagGroupDTOList.size() > 0) {
            dto = tagGroupDTOList.stream()
                    .filter(t -> t.getGroup_name().equals(typeName)).findFirst().orElse(null);
            if (dto != null) {
                TagGroupDTO finalDto = dto;
                dto.setTag(set.stream().filter(t ->
                        finalDto.getTag().stream().noneMatch(a -> t.equals(a.getName())))
                        .map(t -> new TagDTO().setName(t)).collect(Collectors.toList()));
                return dto;
            }
        }
        dto = new TagGroupDTO();
        dto.setGroup_name(typeName);
        dto.setTag(set.stream().map(t -> new TagDTO().setName(t)).collect(Collectors.toList()));
        return dto;
    }


}
