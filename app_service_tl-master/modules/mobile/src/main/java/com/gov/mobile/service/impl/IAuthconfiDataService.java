package com.gov.mobile.service.impl;

import com.gov.mobile.entity.AuthconfiData;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 权限分配表 服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-12
 */
public interface IAuthconfiDataService extends SuperService<AuthconfiData> {

    void saveAuth(String client, String userId, List<String> groupList, List<String> siteList, String delUserId);
}
