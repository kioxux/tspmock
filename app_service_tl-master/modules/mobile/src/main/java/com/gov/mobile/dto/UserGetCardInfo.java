package com.gov.mobile.dto;

import lombok.Data;

@Data
public class UserGetCardInfo {

    private String ToUserName;

    private String FromUserName;

    private String CardId;

    private  String UserCardCode;

    private String OuterStr;

    private String IsRestoreMemberCard;

}
