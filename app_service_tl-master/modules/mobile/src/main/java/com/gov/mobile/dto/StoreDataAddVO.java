package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StoreDataAddVO {

    @NotBlank(message = "门店编码不能为空")
    private String stoCode;

    @NotBlank(message = "门店名称不能为空")
    private String stoName;

    @NotBlank(message = "统一社会信用代码不能为空")
    private String stoNo;

//    @NotBlank(message = "法人不能为空")
    private String stoLegalPerson;

    //    @NotBlank(message = "质量负责人不能为空")
    private String stoQua;

//    @NotBlank(message = "质量负责人姓名不能为空")
    private String stoQuaName;

//    @NotBlank(message = "质量负责人手机号不能为空")
    private String stoQuaPhone;

//    @NotBlank(message = "LOGO地址不能为空")
    private String stoLogo;

    @NotBlank(message = "门店属性不能为空")
    private String stoAttribute;

    // @NotBlank(message = "连锁总部不能为空")
    private String stoChainHead;

    // @NotBlank(message = "纳税主体不能为空")
    private String stoTaxSubject;

    //店长和负责人
//    @NotBlank(message = "负责人不能为空")
    private String stoLeader;

    @NotBlank(message = "店长姓名不能为空")
    private String stoLeaderName;

    @NotBlank(message = "店长手机号不能为空")
    private String stoLeaderPhone;

    // @NotBlank(message = "门店地址不能为空")
    private String stoAdd;

    // @NotBlank(message = "门店组编码不能为空")
    private String stogCode;

}
