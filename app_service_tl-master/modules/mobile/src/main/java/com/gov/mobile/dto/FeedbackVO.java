package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class FeedbackVO {

    @NotBlank(message = "标题不能为空")
    private String title;

    @NotBlank(message = "内容不能为空")
    private String content;

//    @NotBlank(message = "图片不能为空")
    private List<ImageVO> images;

}
