package com.gov.mobile.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.DateUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.controller.workWeixin.Utils;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.dto.WorkWechatMsgDTO;
import com.gov.mobile.dto.WorkWechatMsgPageDTO;
import com.gov.mobile.dto.WorkwechatMsgUserDTO;
import com.gov.mobile.dto.workWeixin.MsgTemplateDTO;
import com.gov.mobile.dto.workWeixin.TextDTO;
import com.gov.mobile.entity.WorkwechatConf;
import com.gov.mobile.entity.WorkwechatCusUserBind;
import com.gov.mobile.entity.WorkwechatMsg;
import com.gov.mobile.entity.WorkwechatMsgUser;
import com.gov.mobile.mapper.WorkwechatConfMapper;
import com.gov.mobile.mapper.WorkwechatCusUserBindMapper;
import com.gov.mobile.mapper.WorkwechatMsgMapper;
import com.gov.mobile.mapper.WorkwechatMsgUserMapper;
import com.gov.mobile.service.IUserDataService;
import com.gov.mobile.service.IWorkWechatMsgService;
import com.gov.mobile.service.IWorkWechatMsgUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WorkWechatMsgServiceImpl extends ServiceImpl<WorkwechatMsgMapper, WorkwechatMsg> implements IWorkWechatMsgService {

    @Resource
    private WorkwechatCusUserBindMapper workwechatCusUserBindMapper;

    @Resource
    private IWorkWechatMsgUserService workWechatMsgUserService;

    @Resource
    private WorkwechatMsgUserMapper workwechatMsgUserMapper;

    @Resource
    private WorkwechatConfMapper workwechatConfMapper;

    @Resource
    private IUserDataService userDataService;


    /**
     * 添加企业微信消息
     *
     * @param msgDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result addMsg(WorkWechatMsgDTO msgDTO) {
        WorkwechatMsg workWechatMsg = new WorkwechatMsg();
        BeanUtils.copyProperties(msgDTO, workWechatMsg);
        workWechatMsg.setGwmSendType("2");
        // 待发送
        workWechatMsg.setGwmSendStatus("0");
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        if (currentUser != null) {
            workWechatMsg.setClient(currentUser.getClient());
            workWechatMsg.setGwmCreateBy(currentUser.getUserId());
        }
        workWechatMsg.setGwmCreateTime(new Date());
        baseMapper.insert(workWechatMsg);

        List<WorkwechatMsgUser> addUserList = new ArrayList<>();
        for (WorkwechatMsgUserDTO workwechatMsgUser : msgDTO.getUserList()) {
            WorkwechatCusUserBind bind = workwechatCusUserBindMapper.selectOne(new QueryWrapper<WorkwechatCusUserBind>()
                    .eq("CLIENT", workwechatMsgUser.getClient())
                    .eq("GWCUB_MEMBER_ID", workwechatMsgUser.getMemeberId())
                    .eq("GWCUB_CARD_ID", workwechatMsgUser.getCardId()));
            if (bind != null) {
                WorkwechatMsgUser user = new WorkwechatMsgUser();
                user.setMsgId(workWechatMsg.getId());
                user.setGwcId(bind.getGwcId());
                user.setClient(bind.getClient());
                user.setMemeberId(bind.getGwcubMemberId());
                user.setWwMemberId(bind.getGwcubWwMemberId());
                user.setUserId(bind.getGwcubUserId());
                user.setWwUserId(bind.getGwcubWwUserId());
                user.setBrId(bind.getGwcubBrId());
                user.setCardId(bind.getGwcubCardId());
                user.setOrgType(bind.getGwcubOrgType());
                user.setOrgId(bind.getGwcubOrgId());
                addUserList.add(user);
            }
        }
        workWechatMsgUserService.saveBatch(addUserList);

        return ResultUtil.success();
    }

    /**
     * 定时企业微信推送
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendMsg() {
        // 获取当前时间段及之前的未发送的消息
        String currentTime = DateUtils.getCurrentDateTimeStr();
        List<WorkwechatMsg> msgList = baseMapper.selectList(new QueryWrapper<WorkwechatMsg>()
                .eq("GWM_SEND_STATUS", "0")
                .le("GWM_EST_SEND_TIME", currentTime));

        // 获取为发送消息人
        Map<String, Object> params = new HashMap<>();
        params.put("sendStatus", "0");
        params.put("sendTime", currentTime);
        List<WorkwechatMsgUser> userList = workwechatMsgUserMapper.selectUserListByStatus(params);

        //获取所有微信配置
        List<WorkwechatConf> configList = workwechatConfMapper.selectConfigList();
        if (configList == null) {
            log.error("未获取到配置");
            throw new CustomResultException("未获取到配置");
        }

        List<WorkwechatMsg> updateMsgList = new ArrayList<>();

        for (WorkwechatMsg msg : msgList) {
            if (userList.size() > 0) {
                Map<String, List<WorkwechatMsgUser>> mapList = userList.stream()
                        .filter(t -> t.getMsgId().equals(msg.getId()))
                        .collect(Collectors.groupingBy(WorkwechatMsgUser::getGwcId));
                for (Map.Entry<String, List<WorkwechatMsgUser>> entry : mapList.entrySet()) {
                    WorkwechatConf conf = configList.stream().filter(t -> t.getGwcId().equals(entry.getKey())).findFirst().orElse(null);
                    if (conf != null) {
                        // 获取accessToken
                        String cusAccessToken = Utils.accessToken(conf.getGwcId(), conf.getGwcCusSecret());
                        if (StrUtil.isBlank(cusAccessToken)) {
                            log.error("获取accessToken失败");
                            throw new CustomResultException("获取accessToken失败");
                        }
                        String attachments = msg.getGwmAttachement();
                        if (StrUtil.isNotBlank(msg.getGwmPaths())) {
                            Map<String, String> paths = JSON.parseObject(msg.getGwmPaths(), new TypeReference<Map<String, String>>() {
                            });
                            for (Map.Entry<String, String> e : paths.entrySet()) {
                                // 上传临时素材
                                String media_id = Utils.uploadByUrl(cusAccessToken, e.getValue());
                                if (media_id != null) {
                                    attachments = attachments.replaceAll(e.getKey(), media_id);
                                } else {
                                    log.error("上传素材错误");
                                    throw new CustomResultException("上传素材错误");
                                }
                            }
                        }
                        MsgTemplateDTO templateDTO = new MsgTemplateDTO();
                        // 非图文或小程序不发送文本
//                        if (!StrUtil.equals(msg.getGwmMsgType(), "1")) {
                        if (StrUtil.isNotBlank(msg.getGwmContent())) {
                            templateDTO.setText(new TextDTO().setContent(msg.getGwmContent()));
                        }
                        templateDTO.setExternal_userid(entry.getValue().stream()
                                .map(WorkwechatMsgUser::getWwMemberId).collect(Collectors.toList()));
                        templateDTO.setAttachments(JSON.parse(attachments));
                        List<String> failList = Utils.addMsgTemplate(cusAccessToken, templateDTO);

                        if (failList != null) {
                            // 发送状态 = 发送成功
                            msg.setGwmSendStatus("1");
                            // 设置未发送成功会员
                            msg.setGwmFailMember(String.join(",", failList));
                        } else {
                            // 发送状态 = 发送失败
                            msg.setGwmSendStatus("2");
                        }
                        msg.setGwmActSendTime(new Date());
                        msg.setGwmUpdateBy("1");
                        msg.setGwmUpdateTime(new Date());
                        updateMsgList.add(msg);
                    } else {
                        log.warn("未配置企业微信信息");
                    }
                }

                for (WorkwechatMsgUser user : userList) {
                    WorkwechatCusUserBind bind = new WorkwechatCusUserBind();
                    bind.setGwcubLastSendTime(new Date());
                    bind.setClient(user.getClient());
                    bind.setGwcubMemberId(user.getMemeberId());
                    bind.setGwcubCardId(user.getCardId());
                    workwechatCusUserBindMapper.updateLastSendTime(bind);
                }

            }
        }
        // 更新发送状态
        if (updateMsgList.size() > 0) {
            updateBatchById(updateMsgList);
        }
    }

    /**
     * 分页查询
     *
     * @param workWechatMsgPageDTO
     * @return
     */
    @Override
    public Result queryList(WorkWechatMsgPageDTO workWechatMsgPageDTO) {
        Page<WorkWechatMsgDTO> page = new Page<>(workWechatMsgPageDTO.getPageNum(), workWechatMsgPageDTO.getPageSize());
        IPage<WorkWechatMsgDTO> iPage = baseMapper.selectListByPage(page, workWechatMsgPageDTO);
        return ResultUtil.success(iPage);
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result deleteByIds(List<Integer> ids) {
        if (ids == null || ids.size() == 0) {
            throw new CustomResultException("请选择删除的数据");
        }
        // 查询是否存在已发送的数据
        int count = baseMapper.selectHasSendByIds(ids);
        if (count != ids.size()) {
            throw new CustomResultException("存在已发送的数据");
        }
        // 删除
        baseMapper.deleteBatchIds(ids);

        workwechatMsgUserMapper.deleteByMsgIds(ids);

        return ResultUtil.success();
    }

    /**
     * 获取消息信息
     *
     * @param msgId
     * @return
     */
    @Override
    public Result getMsgInfo(String msgId) {
        return ResultUtil.success(baseMapper.selectById(msgId));
    }


    @Override
    public Result getUserList(String msgId) {
        List<WorkwechatMsgUserDTO> userList = workwechatMsgUserMapper.selectUserListByMsgId(msgId);
        return ResultUtil.success(userList);
    }

    /**
     * 更新消息
     *
     * @param msgDTO
     * @return
     */
    @Override
    public Result updateMsgInfo(WorkWechatMsgDTO msgDTO) {
        if (msgDTO.getId() == null) {
            throw new CustomResultException("id不能为空");
        }
        WorkwechatMsg workWechatMsg = new WorkwechatMsg();
        BeanUtils.copyProperties(msgDTO, workWechatMsg);
        workWechatMsg.setGwmSendType("2");
        // 待发送
        workWechatMsg.setGwmSendStatus("0");
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        if (currentUser != null) {
            workWechatMsg.setGwmUpdateBy(currentUser.getUserId());
        }
        workWechatMsg.setGwmUpdateTime(new Date());
        baseMapper.updateById(workWechatMsg);
        return ResultUtil.success();
    }
}
