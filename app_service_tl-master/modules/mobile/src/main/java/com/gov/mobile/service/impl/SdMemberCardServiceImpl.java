package com.gov.mobile.service.impl;

import com.gov.mobile.entity.SdMemberCard;
import com.gov.mobile.mapper.SdMemberCardMapper;
import com.gov.mobile.service.ISdMemberCardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Service
public class SdMemberCardServiceImpl extends ServiceImpl<SdMemberCardMapper, SdMemberCard> implements ISdMemberCardService {

}
