package com.gov.mobile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "用户注册")
public class MemberUserDto{

    @NotBlank(message = "加盟商id不能为空")
    @ApiModelProperty(value = "加盟商id")
    private String client;

    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(value = "姓名")
    private String gsmbName;

    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号")
    private String gsmbMobile;

    @NotBlank(message = "性别不能为空")
    @ApiModelProperty(value = "性别：0，女；1，男")
    private String gsmbSex;

    @NotBlank(message = "生日不能为空")
    @ApiModelProperty(value = "生日 yyyyMMdd")
    private String gsmbBirth;

    @NotBlank(message = "所属门店不能为空")
    @ApiModelProperty(value = "所属门店")
    private String gsmbBrId;

    @NotBlank(message = "openID不能为空")
    @ApiModelProperty(value = "openID")
    private String gsmbOpenId;

    @ApiModelProperty(value = "身份证")
    private String gsmbCredentials;

    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value = "验证码")
    private String verificationCode;

    @ApiModelProperty(value = "开卡门店")
    private String openStore;

    @ApiModelProperty(value = "企业微信id")
    private String gwcId;

    @ApiModelProperty(value = "药德系统用户id")
    private String userId;

    @ApiModelProperty(value = "微信用户id")
    private String wwUserId;

    @ApiModelProperty(value = "微信客户id")
    private String memberId;
}
