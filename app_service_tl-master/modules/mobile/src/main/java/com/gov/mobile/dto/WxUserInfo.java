package com.gov.mobile.dto;

import lombok.Data;

@Data
public class WxUserInfo {

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 姓名
     */
    private String name;

    /**
     * 生日
     */
    private String birth;

    /**
     * 性别
     */
    private String sex;

    /**
     * 通讯地址
     */
    private String address;

    /**
     * 身份证
     */
    private String credentials;

    /**
     * 推荐人
     */
    private String recommender;
}
