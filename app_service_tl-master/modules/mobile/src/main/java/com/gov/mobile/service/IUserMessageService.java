package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.UserMessageListVO;
import com.gov.mobile.entity.UserMessage;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface IUserMessageService extends SuperService<UserMessage> {

    /**
     * 用户消息列表
     */
    Result messageList(UserMessageListVO userMessageListVO);

    /**
     * 用户消息详情(并标为已读)
     */
    Result messageDetail(String id);
}
