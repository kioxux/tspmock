package com.gov.mobile.dto;

import com.gov.mobile.entity.SdYxmclass;
import lombok.Data;

import java.util.List;

@Data
public class ProductGroupVo {

    private String gsyGroup;

    private String gsyGroupname;

    private List<SdYxmclass> productList;
}
