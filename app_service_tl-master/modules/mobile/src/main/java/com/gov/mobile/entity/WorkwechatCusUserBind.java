package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户企业微信绑定
 * @TableName GAIA_WORKWECHAT_CUS_USER_BIND
 */
@TableName(value ="GAIA_WORKWECHAT_CUS_USER_BIND")
@Data
public class WorkwechatCusUserBind implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 企业微信ID
     */
    private String gwcId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 员工ID
     */
    private String gwcubUserId;

    /**
     * 员工企业微信ID
     */
    private String gwcubWwUserId;

    /**
     * 企微客户ID
     */
    private String gwcubWwMemberId;

    /**
     * 会员ID
     */
    private String gwcubMemberId;

    /**
     * 所属门店
     */
    private String gwcubBrId;

    /**
     * 会员卡号
     */
    private String gwcubCardId;

    /**
     * 所属组织类型，单体：1，连锁：2
     */
    private Integer gwcubOrgType;

    /**
     * 所属组织，单体：店号，连锁：连锁编码
     */
    private String gwcubOrgId;

    /**
     * 绑定时间
     */
    private Date gwcubBindTime;

    /**
     * 解绑时间
     */
    private Date gwcubUnbindTime;

    /**
     * 最后发送时间
     */
    private Date gwcubLastSendTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}