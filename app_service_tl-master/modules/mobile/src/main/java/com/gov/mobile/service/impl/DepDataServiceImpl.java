package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.*;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.mobile.dto.OrganizationDto;
import com.gov.mobile.dto.SaveDepVO;
import com.gov.mobile.dto.UpdateDepVO;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.entity.AuthconfiData;
import com.gov.mobile.entity.DepData;
import com.gov.mobile.entity.GroupData;
import com.gov.mobile.entity.PresetDep;
import com.gov.mobile.feign.OperateFeign;
import com.gov.mobile.feign.dto.Message.MessageParams;
import com.gov.mobile.mapper.AuthconfiDataMapper;
import com.gov.mobile.mapper.DepDataMapper;
import com.gov.mobile.mapper.GroupDataMapper;
import com.gov.mobile.service.IAuthorityService;
import com.gov.mobile.service.IDepDataService;
import com.gov.mobile.service.IPresetDepService;
import com.gov.mobile.service.IUserDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Service
public class DepDataServiceImpl extends ServiceImpl<DepDataMapper, DepData> implements IDepDataService {

    @Resource
    private DepDataMapper depDataMapper;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Resource
    private IUserDataService userDataService;
    @Resource
    private IPresetDepService presetDepService;
    @Resource
    private GroupDataMapper groupDataMapper;
    @Resource
    private IAuthorityService authorityService;
    @Resource
    private OperateFeign operateFeign;


    /**
     * 部门详情
     */
    @Override
    public DepData getDep(String depId) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        QueryWrapper<DepData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", currentUser.getClient());
        queryWrapper.eq("DEP_ID", depId);
        return this.getOne(queryWrapper);
    }

    /**
     * 新增部门
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveDep(SaveDepVO saveDepVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // (加盟商 + 部门类型 + 部门名称)不能重复
        QueryWrapper<DepData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", currentUser.getClient());
        queryWrapper.eq("DEP_TYPE", saveDepVO.getDepType());
        queryWrapper.eq("DEP_NAME", saveDepVO.getDepName());
        // 如果连锁总部不为空，连锁总部要参与查询
        if (StringUtils.isNotEmpty(saveDepVO.getStoChainHead())) {
            queryWrapper.eq("STO_CHAIN_HEAD", saveDepVO.getStoChainHead());
        }
        // 启用
        if (Constants.DepStatus.DEP_ABLE.equals(saveDepVO.getDepStatus())) {
            queryWrapper.eq("DEP_STATUS", "1");
        }
        List<DepData> list = this.list(queryWrapper);
        if (!list.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0014);
        }

        DepData depData = new DepData();
        BeanUtils.copyProperties(saveDepVO, depData);
        //部门编码设置 (以加盟商为单位 取数字部门编码 + 1 → 100)
        DepData depDataTemp = depDataMapper.getMaxNumDepId(currentUser.getClient());
        if (StringUtils.isNotEmpty(depDataTemp)) {
            Integer depCreId = Integer.valueOf(depDataTemp.getDepId()) + 1;
            depData.setDepId(String.valueOf(depCreId));
        } else {
            depData.setDepId("100");
        }
        //停用日期设置
        if (Constants.DepStatus.DEP_DISABLE.equals(saveDepVO.getDepStatus())) {
            depData.setDepDisDate(DateUtils.getCurrentDateStrYYMMDD());
        }
        depData.setClient(currentUser.getClient());
        depData.setDepCreId(currentUser.getUserId());
        depData.setDepCreDate(DateUtils.getCurrentDateStrYYMMDD());
        depData.setDepCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        this.save(depData);
        // 权限分配
        this.saveAuthconfiData(depData);

        try {
            MessageParams messageParams = new MessageParams();
            messageParams.setClient(depData.getClient());
            messageParams.setUserId(currentUser.getUserId());
            messageParams.setId(CommonEnum.gmtId.MSG00004.getCode());
            operateFeign.sendMessage(messageParams);
        } catch (Exception e) {
            log.error("新增配送中心推送时异常", e);
        }
    }

    /**
     * 部门编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDep(UpdateDepVO updateDepVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 先跟据主键判断该部门是否存在
        QueryWrapper<DepData> queryWrapperExist = new QueryWrapper<>();
        queryWrapperExist.eq("CLIENT", currentUser.getClient());
        queryWrapperExist.eq("DEP_ID", updateDepVO.getDepId());
        DepData depDataExist = this.getOne(queryWrapperExist);
        if (StringUtils.isEmpty(depDataExist)) {
            throw new CustomResultException(ResultEnum.E0013);
        }

        // (加盟商 + 部门类型 + 部门名称)不能重复
        QueryWrapper<DepData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", currentUser.getClient());
        queryWrapper.eq("DEP_TYPE", depDataExist.getDepType());
        queryWrapper.eq("DEP_NAME", updateDepVO.getDepName());
        // 如果连锁总部不为空，连锁总部要参与查询
        if (StringUtils.isNotEmpty(depDataExist.getStoChainHead())) {
            queryWrapper.eq("STO_CHAIN_HEAD", depDataExist.getStoChainHead());
        }
        List<DepData> list = this.list(queryWrapper);
        if (!list.isEmpty()) {
            DepData depDataUpdate = list.get(0);
            // 更新时 传入的名字和原来的名字一样
            if (!depDataUpdate.getDepId().equals(depDataExist.getDepId())) {
                throw new CustomResultException(ResultEnum.E0014);
            }
        }

        DepData depData = new DepData();
        BeanUtils.copyProperties(updateDepVO, depData, "client", "depId");
        depData.setDepModiId(userDataService.getCurrentUser().getUserId());
        depData.setDepModiDate(DateUtils.getCurrentDateStrYYMMDD());
        depData.setDepModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        //停用日期设置
        String depDisDate = "";
        if (Constants.DepStatus.DEP_DISABLE.equals(updateDepVO.getDepStatus())) {
            depDisDate = DateUtils.getCurrentDateStrYYMMDD();
        }

        UpdateWrapper<DepData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", currentUser.getClient());
        updateWrapper.eq("DEP_ID", updateDepVO.getDepId());
        // depData.getDepDisDate可能为空 单独设置
        updateWrapper.set("DEP_DIS_DATE", depDisDate);
        this.update(depData, updateWrapper);

        // 如果部门负责人做了修改,要修改权限
        if (!depDataExist.getDepHeadId().equals(updateDepVO.getDepHeadId())) {
            this.updateAuthconfiDate(depData, depDataExist);

            try {
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(depData.getClient());
                messageParams.setUserId(updateDepVO.getDepHeadId());
                messageParams.setId(CommonEnum.gmtId.MSG00004.getCode());
                operateFeign.sendMessage(messageParams);
            } catch (Exception e) {
                log.error("编辑配送中心推送时异常", e);
            }
        }
    }


    /**
     * 新增部门时 权限分配
     */
    private void saveAuthconfiData(DepData saveDep) {

        // 岗位编码集合
        QueryWrapper<PresetDep> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("DEP_CODE", saveDep.getDepCls());
        List<PresetDep> presetDepList = presetDepService.list(queryWrapper);
        if (presetDepList.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0013);
        }

        // 连锁 20
        if (CommonEnum.DepType.CHAIN_DEP.getCode().equals(saveDep.getDepType())) {
            insertAuthconfiDataList(presetDepList, saveDep.getDepId(), saveDep.getClient(), saveDep.getDepHeadId());
            return;
        }

        // 批发公司 30
        if (CommonEnum.DepType.WHOLESALE_DEP.getCode().equals(saveDep.getDepType())) {
            insertAuthconfiDataList(presetDepList, saveDep.getDepId(), saveDep.getClient(), saveDep.getDepHeadId());
            return;
        }
        // 部门 / 加盟商 (10/40)
        insertAuthconfiDataList(presetDepList, saveDep.getClient(), saveDep.getDepHeadId());
    }

    /**
     * 编辑部门时 权限修改
     */
    private void updateAuthconfiDate(DepData updateDep, DepData depDataExist) {

        // 岗位编码集合获取
        QueryWrapper<PresetDep> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("DEP_CODE", depDataExist.getDepCls());
        List<PresetDep> presetDepList = presetDepService.list(queryWrapper);
        if (presetDepList.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0013);
        }
        List<String> inParams = new ArrayList<>();
        for (PresetDep presetDep : presetDepList) {
            inParams.add(presetDep.getAuthGroupId());
        }

        // 删除 原负责人权限数据
        QueryWrapper<AuthconfiData> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("CLIENT", depDataExist.getClient());
        deleteWrapper.eq("AUTHCONFI_USER", depDataExist.getDepHeadId());
        deleteWrapper.in("AUTH_GROUP_ID", inParams.toArray());
        authconfiDataMapper.delete(deleteWrapper);

        // 新增 新负责人权限数据
        DepData saveDep = new DepData();
        BeanUtils.copyProperties(depDataExist, saveDep);
        saveDep.setDepHeadId(updateDep.getDepHeadId());
        this.saveAuthconfiData(saveDep);
    }


    private void insertAuthconfiDataList(List<PresetDep> presetDepList, String depId, String client, String depHeadId) {
        List<AuthconfiData> authConfigDataList = new ArrayList<>();
        for (PresetDep presetDep : presetDepList) {
            // 权限组(岗位)编码
            String authGroupId = presetDep.getAuthGroupId();
            // 查询 岗位组描述
            String authGroupName = "";
            List<GroupData> groupDataList = groupDataMapper.getAuthGroupName(authGroupId);
            if (!groupDataList.isEmpty()) {
                authGroupName = groupDataList.get(0).getAuthGroupName();
            }


            AuthconfiData authconfiData = new AuthconfiData();
            authconfiData.setClient(client);
            authconfiData.setAuthGroupId(authGroupId);
            authconfiData.setAuthGroupName(authGroupName);
            authconfiData.setAuthobjSite(depId);
            authconfiData.setAuthconfiUser(depHeadId);
            authConfigDataList.add(authconfiData);
        }
        authconfiDataMapper.insertIgnoreList(authConfigDataList);
    }

    /**
     * 部门40 / 加盟商10 的情况下构造权限实体List
     *
     * @param presetDepList 部门集合
     * @param client        当前加盟商
     * @param depHeadId     部门负责人
     * @return
     */
    private void insertAuthconfiDataList(List<PresetDep> presetDepList, String client, String depHeadId) {
        // 加盟商/部门
        List<AuthconfiData> authconfiDataList = new ArrayList<>();
        List<OrganizationDto> authOrgToOneList = authorityService.getAuthOrgToOneList();
        for (PresetDep presetDep : presetDepList) {
            // 权限组(岗位)编码
            String authGroupId = presetDep.getAuthGroupId();
            // 查询 岗位组描述
            String authGroupName = "";
            List<GroupData> groupDataList = groupDataMapper.getAuthGroupName(authGroupId);
            if (!groupDataList.isEmpty()) {
                authGroupName = groupDataList.get(0).getAuthGroupName();
            }

            // 遍历部门
            for (OrganizationDto organizationDto : authOrgToOneList) {
                AuthconfiData authconfiData = new AuthconfiData();
                authconfiData.setClient(client);
                authconfiData.setAuthGroupId(authGroupId);
                authconfiData.setAuthGroupName(authGroupName);
                authconfiData.setAuthobjSite(organizationDto.getCode());
                authconfiData.setAuthconfiUser(depHeadId);
                authconfiDataList.add(authconfiData);
            }
        }
        authconfiDataMapper.insertIgnoreList(authconfiDataList);
    }

    /**
     * 对应加盟商下面的部门
     * @param client 加盟商编号
     * @return
     */
    @Override
    public List<DepData> getDepList(String client, String depType) {
        QueryWrapper<DepData> queryWrapperDep = new QueryWrapper<>();
        queryWrapperDep.eq("CLIENT", client);;
        return this.list(queryWrapperDep);
    }

    /**
     * 对加盟商下面的仓库
     * @param client 加盟商编号
     * @return
     */
    @Override
    public List<DepData> getDepList(String client, String depType, String chainHead) {
        QueryWrapper<DepData> queryWrapperWarehouse = new QueryWrapper<>();
        queryWrapperWarehouse.eq("CLIENT", client);
        queryWrapperWarehouse.eq("DEP_TYPE", depType);
        queryWrapperWarehouse.eq("STO_CHAIN_HEAD", chainHead);
        return this.list(queryWrapperWarehouse);
    }


}
