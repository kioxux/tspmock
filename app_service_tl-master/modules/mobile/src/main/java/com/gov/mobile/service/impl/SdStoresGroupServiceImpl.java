package com.gov.mobile.service.impl;

import com.gov.mobile.entity.SdStoresGroup;
import com.gov.mobile.mapper.SdStoresGroupMapper;
import com.gov.mobile.service.ISdStoresGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店分类表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2022-02-24
 */
@Service
public class SdStoresGroupServiceImpl extends ServiceImpl<SdStoresGroupMapper, SdStoresGroup> implements ISdStoresGroupService {

}
