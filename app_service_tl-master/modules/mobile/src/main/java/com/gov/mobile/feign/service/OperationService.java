package com.gov.mobile.feign.service;

import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface OperationService {

    /**
     * 会员新增/修改
     *
     * @param client   加盟商
     * @param memberId 会员ID
     * @param cardId   会员卡号
     */
    void memberSync(String client, String memberId, String cardId);
}
