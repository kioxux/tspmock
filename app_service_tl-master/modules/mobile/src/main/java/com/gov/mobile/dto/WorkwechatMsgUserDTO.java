package com.gov.mobile.dto;

import lombok.Data;

/**
 * @TableName GAIA_WORKWECHAT_MSG_USER
 */
@Data
public class WorkwechatMsgUserDTO  {

    private Integer msgId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 药德会员id
     */
    private String memeberId;

    /**
     * 企微客户id
     */
    private String wwMemberId;

    /**
     * 药德员工id
     */
    private String userId;

    /**
     *
     */
    private String wwUserId;

    /**
     * 企业微信iD
     */
    private String gwcId;

    /**
     * 所属门店
     */
    private String brId;

    /**
     * 会员卡号
     */
    private String cardId;

    /**
     * 所属组织类型，单体：1，连锁：2
     */
    private Integer orgType;

    /**
     * 所属组织，单体：店号，连锁：连锁编码
     */
    private String orgId;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 推广人姓名
     */
    private String userName;

    /**
     * 开卡门店
     */
    private String stoName;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 所属组织名称
     */
    private String orgName;
}