package com.gov.mobile.dto.workWeixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WxWelcomeMsgDTO {

    private String welcome_code;

    private TextDTO text;

    private List<WxAttachmentDTO> attachments;
}

