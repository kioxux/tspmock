package com.gov.mobile.dto;

import lombok.Data;

import java.util.List;

@Data
public class FeedbackDto {

    /**
     * id
     */
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建时间
     */
    private String userCreData;

    /**
     * 图片
     */
    private List<ImagesEntity> images;


}
