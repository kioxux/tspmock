package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.mobile.entity.AppLoginLog;
import com.gov.mobile.mapper.AppLoginLogMapper;
import com.gov.mobile.service.IAppLoginLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
@Service
public class AppLoginLogServiceImpl extends ServiceImpl<AppLoginLogMapper, AppLoginLog> implements IAppLoginLogService {

    @Resource
    private AppLoginLogMapper appLoginLogMapper;

    @Override
    public AppLoginLog selectTokenByMaxLogintime(String client, String userId) {
        return appLoginLogMapper.selectTokenByMaxLogintime(client, userId);
    }

}
