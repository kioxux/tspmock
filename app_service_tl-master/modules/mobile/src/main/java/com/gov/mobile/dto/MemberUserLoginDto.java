package com.gov.mobile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "用户登录")
public class MemberUserLoginDto {

    @NotBlank(message = "加盟商id不能为空")
    @ApiModelProperty(value = "加盟商id")
    private String client;

    @NotBlank(message = "openID不能为空")
    @ApiModelProperty(value = "openID")
    private String gsmbOpenId;

    @NotBlank(message = "所属门店不能为空")
    @ApiModelProperty(value = "所属门店")
    private String gsmbBrId;

    @ApiModelProperty(value = "企业微信id")
    private String gwcId;

    @ApiModelProperty(value = "药德系统用户id")
    private String userId;

    @ApiModelProperty(value = "微信用户id")
    private String wwUserId;

    @ApiModelProperty(value = "微信客户id")
    private String memberId;


}
