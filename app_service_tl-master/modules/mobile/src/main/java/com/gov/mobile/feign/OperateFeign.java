package com.gov.mobile.feign;

import com.gov.mobile.feign.dto.JsonResult;
import com.gov.mobile.feign.dto.Message.MessageParams;
import com.gov.mobile.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(value = "gys-operate", fallback = AuthFeignFallback.class)
public interface OperateFeign {

    /**
     * 发送推送
     * @param messageParams
     * @return
     */
    @PostMapping({"/push/sendMessage"})
    public JsonResult sendMessage(@RequestBody MessageParams messageParams);

}
