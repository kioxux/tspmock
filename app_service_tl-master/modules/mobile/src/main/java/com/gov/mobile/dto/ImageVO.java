package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ImageVO {

    @NotBlank(message = "路径不能为空")
    private String path;

    private String filename;

    private Integer sort;

}
