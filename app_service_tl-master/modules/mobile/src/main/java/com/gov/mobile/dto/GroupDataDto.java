package com.gov.mobile.dto;

import com.gov.mobile.entity.GroupData;
import lombok.Data;

import java.util.List;

/**
 * Author: 钱金华
 * Date: 2020-06-30
 * Time: 13:27
 */
@Data
public class GroupDataDto extends GroupData {

    List<AuthconfiDataDto> authconfis;
}
