package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.mobile.dto.FeedbackListVO;
import com.gov.mobile.dto.FeedbackVO;
import com.gov.mobile.service.IFeedbackService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-23
 */
@RestController
@RequestMapping("feedback")
public class FeedbackController {

    @Resource
    private IFeedbackService iFeedbackService;

    @ApiOperation(value = "建议反馈列表")
    @PostMapping("feedbackList")
    public Result feedbackList(@Valid @RequestBody FeedbackListVO feedbackListVO) {
        return iFeedbackService.feedbackList(feedbackListVO);
    }

    @ApiOperation(value = "建议反馈添加")
    @PostMapping("feedbackAdd")
    public Result feedbackAdd(@Valid @RequestBody FeedbackVO feedbackVO) {
        return iFeedbackService.feedbackAdd(feedbackVO);
    }

    @ApiOperation(value = "建议反馈详情")
    @GetMapping("feedbackDetail")
    public Result feedbackDetail(@RequestParam(name = "id", required = true) String id) {
        return iFeedbackService.feedbackDetail(id);
    }

}

