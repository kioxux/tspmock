package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.mobile.entity.WorkwechatCusUserBind;
import com.gov.mobile.entity.WorkwechatMsgUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity generator.domain.GaiaWorkwechatCusUserBind
 */
public interface WorkwechatCusUserBindMapper extends BaseMapper<WorkwechatCusUserBind> {

    int updateLastSendTime(@Param("bind") WorkwechatCusUserBind bind);
}




