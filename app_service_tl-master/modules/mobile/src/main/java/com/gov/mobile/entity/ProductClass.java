package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRODUCT_CLASS")
@ApiModel(value="ProductClass对象", description="")
public class ProductClass extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "商品分类编码")
    @TableId("PRO_CLASS_CODE")
    private String proClassCode;

    @ApiModelProperty(value = "商品分类名称")
    @TableField("PRO_CLASS_NAME")
    private String proClassName;

    @ApiModelProperty(value = "商品分类状态")
    @TableField("PRO_CLASS_STATUS")
    private String proClassStatus;


}
