package com.gov.mobile.controller;

import com.gov.common.response.Result;
import com.gov.mobile.dto.UserLoginModel;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.dto.VerificationToToken;
import com.gov.mobile.service.IUserDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "用户登录")
@RestController
@RequestMapping("userLogin")
public class UserLoginController {

    @Resource
    private IUserDataService userDataService;

    @PostMapping("getTokenByTelPasswd")
    @ApiOperation(value = "登录token获取（手机号+密码）")
    public Result getTokenByTelPasswd(@Valid @RequestBody UserLoginModel userLoginModel) {
        return userDataService.getTokenByTelPasswd(userLoginModel);
    }

    @PostMapping("getTokenByClient")
    @ApiOperation(value = "登录token获取（加盟商+用户编号）")
    public Result getTokenByClient(@Valid @RequestBody UserLoginModelClient modelClient) {
        return userDataService.getTokenByClient(modelClient);
    }


    @PostMapping("getTokenByTelVerificationCode")
    @ApiOperation(value = "登录token获取（手机号+验证码）")
    public Result getTokenByTelVerificationCode(@Valid @RequestBody VerificationToToken verificationToToken) {
        return userDataService.getTokenByTelVerificationCode(verificationToToken);
    }

    @GetMapping("logout")
    @ApiOperation(value = "退出登录")
    public Result logout() {
        return userDataService.logout();
    }


}
