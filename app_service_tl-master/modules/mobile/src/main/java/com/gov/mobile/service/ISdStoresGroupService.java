package com.gov.mobile.service;

import com.gov.mobile.entity.SdStoresGroup;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 门店分类表 服务类
 * </p>
 *
 * @author sy
 * @since 2022-02-24
 */
public interface ISdStoresGroupService extends SuperService<SdStoresGroup> {

}
