package com.gov.mobile.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_ELECTRON_CHANGE")
@ApiModel(value="SdElectronChange对象", description="")
public class SdElectronChange extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    @TableField("GSEC_MEMBER_ID")
    private String gsecMemberId;

    @ApiModelProperty(value = "电子券号")
    @TableField("GSEC_ID")
    private String gsecId;

    @ApiModelProperty(value = "电子券活动号")
    @TableField("GSEB_ID")
    private String gsebId;

    @ApiModelProperty(value = "是否使用")
    @TableField("GSEC_STATUS")
    private String gsecStatus;

    @ApiModelProperty(value = "电子券金额")
    @TableField("GSEC_AMT")
    private BigDecimal gsecAmt;

    @ApiModelProperty(value = "电子券描述")
    @TableField("GSEB_NAME")
    private String gsebName;

    @ApiModelProperty(value = "途径")
    @TableField("GSEC_FLAG")
    private String gsecFlag;

    @ApiModelProperty(value = "销售单号")
    @TableField("GSEC_BILL_NO")
    private String gsecBillNo;

    @ApiModelProperty(value = "销售门店")
    @TableField("GSEC_BR_ID")
    private String gsecBrId;

    @ApiModelProperty(value = "销售日期")
    @TableField("GSEC_SALE_DATE")
    private String gsecSaleDate;

    @ApiModelProperty(value = "创建日期")
    @TableField("GSEC_CREATE_DATE")
    private String gsecCreateDate;

    @ApiModelProperty(value = "失效日期")
    @TableField("GSEC_FAIL_DATE")
    private String gsecFailDate;


}
