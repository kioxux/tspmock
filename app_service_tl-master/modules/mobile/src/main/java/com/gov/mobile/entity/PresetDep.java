package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-06-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_PRESET_DEP")
@ApiModel(value="PresetDep对象", description="")
public class PresetDep extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "部门代码")
    @TableField("DEP_CODE")
    private String depCode;

    @ApiModelProperty(value = "部门名")
    @TableField("DEP_NAME")
    private String depName;

    @ApiModelProperty(value = "岗位编码")
    @TableField("AUTH_GROUP_ID")
    private String authGroupId;


}
