package com.gov.mobile.service;

import com.gov.mobile.entity.PresetDep;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-30
 */
public interface IPresetDepService extends SuperService<PresetDep> {

    /**
     * 获取不重复的部门列表
     */
    List<PresetDep> getAppLastVersion();
}
