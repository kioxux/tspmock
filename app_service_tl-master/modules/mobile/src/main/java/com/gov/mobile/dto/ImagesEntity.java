package com.gov.mobile.dto;

import com.gov.mobile.entity.Images;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.08.06
 */
@Data
public class ImagesEntity extends Images {
    /**
     * 图片全路径
     */
    private String url;
}
