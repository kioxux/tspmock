package com.gov.mobile.mapper;

import com.gov.mobile.entity.GroupData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
public interface GroupDataMapper extends BaseMapper<GroupData> {

    /**
     * 根据权限组(岗位)编号 查询
     *
     * @param authGroupId 岗位
     * @return GroupData  权限组对象
     */
    List<GroupData> getAuthGroupName(@Param("authGroupId") String authGroupId);

}
