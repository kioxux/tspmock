package com.gov.mobile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "用户登录，获取token请求参数")
public class UserLoginModel {

    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号")
    private String userTel;

    @NotBlank(message = "密码不能为空")
    @ApiModelProperty(value = "登陆密码")
    private String userPswd;

    @NotBlank(message = "设备类型不能为空")
    @ApiModelProperty(value = "设备类型（android/ios）")
    private String platform;

    @ApiModelProperty(value = "设备号")
    private String deviceNo;


}
