package com.gov.mobile.service.impl;

import com.gov.mobile.entity.OfficialAccounts;
import com.gov.mobile.mapper.OfficialAccountsMapper;
import com.gov.mobile.service.IOfficialAccountsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
@Service
public class OfficialAccountsServiceImpl extends ServiceImpl<OfficialAccountsMapper, OfficialAccounts> implements IOfficialAccountsService {

}
