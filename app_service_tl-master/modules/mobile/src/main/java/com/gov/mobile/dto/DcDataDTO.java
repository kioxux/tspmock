package com.gov.mobile.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gov.mobile.entity.DcData;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude
public class DcDataDTO extends DcData {

    /**
     * 法人/负责人姓名
     */
    private String dcLegalPersonName;

    /**
     * 质量负责人姓名
     */
    private String dcQuaName;

    /**
     * 法人/负责人
     */
    private String dcLegalPerson;

    /**
     * 质量负责人
     */
    private String dcQua;

    /**
     * 法人/负责人手机
     */
    private String dcLegalPhone;

    /**
     * 质量负责人手机
     */
    private String dcQuaPhone;

    private String dcCode;

    private String dcLegalName;

    /**
     * 部门
     */
    private List<OrganizationDepDTO> depList;

}
