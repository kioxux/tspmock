package com.gov.mobile.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gov.common.entity.ApplicationConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.text.MessageFormat;

@Slf4j
@Validated
@RestController
@RequestMapping("weChat")
public class WeChatController {

    @Resource
    private ApplicationConfig applicationConfig;

    @GetMapping(produces = "text/plain;charset=utf-8")
    public void authGet(@RequestParam(name = "signature", required = false) String signature,
                        @RequestParam(name = "timestamp", required = false) String timestamp,
                        @RequestParam(name = "nonce", required = false) String nonce,
                        @RequestParam(name = "echostr", required = false) String echostr, HttpServletResponse response) {
        PrintWriter print;
        try {
            print = response.getWriter();
            print.write(echostr);
            print.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //接受微信服务器发送过来的XML形式的消息
        InputStream in = request.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String sReqData = "";
        String itemStr = "";//作为输出字符串的临时串，用于判断是否读取完毕
        while ((itemStr = reader.readLine()) != null) {
            sReqData += itemStr;
        }
        in.close();
        reader.close();
        log.info("公众号接收到消息:{}", sReqData);
        //防止中文乱码
        response.setCharacterEncoding("UTF-8");
        JSONArray j = printNote(sReqData);
        JSONObject message = j.getJSONObject(0).getJSONArray("xml").getJSONObject(0);
        // 事件
        String event = message.getString("Event");
        // 关注
        if ("subscribe".equals(event)) {
            // 发送消息
            String toUserName = message.getString("ToUserName");
            String fromUserName = message.getString("FromUserName");
            Long createTime = System.currentTimeMillis();
            String eventKey = message.getString("EventKey");
            if (StringUtils.isNotBlank(eventKey) && eventKey.startsWith("qrscene_")) {
                String[] returnInfo = eventKey.substring(8).split(",");
                String returnMessage = MessageFormat.format(applicationConfig.getWeChatPushContent(), returnInfo[0], returnInfo[1], returnInfo[2]);
                String returnXml = "<xml>" +
                        "<ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>" +
                        "<FromUserName><![CDATA[" + toUserName + "]]></FromUserName>" +
                        "<CreateTime>" + createTime + "</CreateTime>" +
                        "<MsgType><![CDATA[text]]></MsgType>" +
                        "<Content><![CDATA[" + applicationConfig.getWeChatPushContent() + returnMessage + "]]></Content>" +
                        "</xml>";
                log.info("回复消息:{}", returnXml);
                PrintWriter print;
                try {
                    print = response.getWriter();
                    print.write(returnXml);
                    print.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static JSONArray printNote(String xml) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new InputSource(new StringReader(xml)));
            NodeList sessions = document.getChildNodes();
            return printNote_1(sessions);
        } catch (Exception e) {

        }
        return null;
    }

    private static JSONArray printNote_1(NodeList nodeList) {
        JSONArray dataArr = new JSONArray();
        JSONObject dataObject = new JSONObject();
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                if (tempNode.hasChildNodes() && tempNode.getChildNodes().getLength() > 1) {
                    JSONArray temArr = printNote_1(tempNode.getChildNodes());
                    if (dataObject.containsKey(tempNode.getNodeName())) {
                        dataObject.getJSONArray(tempNode.getNodeName()).add(temArr.getJSONObject(0));
                    } else {
                        dataObject.put(tempNode.getNodeName(), temArr);
                    }
                } else {
                    dataObject.put(tempNode.getNodeName(), tempNode.getTextContent());
                }
            }
        }
        dataArr.add(dataObject);
        return dataArr;
    }
}
