package com.gov.mobile.mapper;

import com.gov.mobile.entity.AppLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
public interface AppLoginLogMapper extends BaseMapper<AppLoginLog> {

    AppLoginLog selectTokenByMaxLogintime(@Param("client")String client, @Param("userId")String userId);

}
