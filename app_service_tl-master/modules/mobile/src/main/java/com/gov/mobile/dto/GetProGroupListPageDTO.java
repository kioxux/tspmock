package com.gov.mobile.dto;

import com.gov.mobile.entity.SdYxmclass;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetProGroupListPageDTO extends SdYxmclass {

    /**
     * 门店名称
     */
    private String gsyStoreName;
}
