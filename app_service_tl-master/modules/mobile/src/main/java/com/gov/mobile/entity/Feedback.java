package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-06-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_FEEDBACK")
@ApiModel(value="Feedback对象", description="")
public class Feedback extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "加盟商ID")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工编号")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(value = "标题")
    @TableField("TITLE")
    private String title;

    @ApiModelProperty(value = "内容")
    @TableField("CONTENT")
    private String content;

    @ApiModelProperty(value = "提交时间")
    @TableField("DATE_TIME")
    private String dateTime;

    @ApiModelProperty(value = "类别")
    @TableField("TYPE")
    private String type;

    @ApiModelProperty(value = "回复时间")
    @TableField("REPLY_TIME")
    private String replyTime;

    @ApiModelProperty(value = "回复人加盟商ID")
    @TableField("REPLY_CLIENT")
    private String replyClient;

    @ApiModelProperty(value = "回复人员工编号")
    @TableField("REPLY_USER_ID")
    private String replyUserId;

    @ApiModelProperty(value = "回复内容")
    @TableField("REPLY_CONTENT")
    private String replyContent;

    @ApiModelProperty(value = "创建日期")
    @TableField("USER_CRE_DATE")
    private String userCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("USER_CRE_TIME")
    private String userCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("USER_CRE_ID")
    private String userCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("USER_MODI_DATE")
    private String userModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("USER_MODI_TIME")
    private String userModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("USER_MODI_ID")
    private String userModiId;


}
