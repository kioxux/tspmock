package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.mobile.entity.DiseaseClassifcation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity generator.domain.GaiaDiseaseClassifcation
 */
public interface DiseaseClassifcationMapper extends BaseMapper<DiseaseClassifcation> {

    /**
     * 通過加盟商 + 连锁/门店 获取 疾病 和成分的标签
     * @param client
     * @param gwcWechatCode
     * @return
     */
    List<DiseaseClassifcation> getDiseaseClassifcationListBySite(@Param("client") String client,@Param("gwcWechatCode") String gwcWechatCode);
}




