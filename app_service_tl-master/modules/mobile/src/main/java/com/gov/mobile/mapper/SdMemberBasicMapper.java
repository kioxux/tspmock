package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.dto.PointRecordDto;
import com.gov.mobile.dto.TokenUserDto;
import com.gov.mobile.entity.SdMemberBasic;
import com.gov.mobile.entity.SdMemberCard;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-29
 */
public interface SdMemberBasicMapper extends BaseMapper<SdMemberBasic> {

    IPage<PointRecordDto> selectPageList(Page<PointRecordDto> page, @Param("ew") QueryWrapper<SdMemberBasic> wrapper);

    List<TokenUserDto> selectMemberInfo(Map<Object, Object> map);

    IPage<PointRecordDto> selectMoneyPageList(Page<PointRecordDto> page, @Param("ew") QueryWrapper<SdMemberCard> wrapper);

    Integer selectUnUseCouponNumber(@Param("client") String client, @Param("gsecMemberId") String gsecMemberId);

    IPage<PointRecordDto> selectCouponPageList(Page<PointRecordDto> pageInfo, @Param("client") String client, @Param("gsecMemberId") String gsecMemberId, @Param("type") Integer type);
}
