package com.gov.mobile.dto.workWeixin;

import lombok.Data;

@Data
public class CusTagDTO {

    private String group_name;

    private String tag_id;

    private String tag_name;

    private Integer type;

}
