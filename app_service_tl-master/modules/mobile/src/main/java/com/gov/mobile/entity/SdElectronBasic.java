package com.gov.mobile.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_ELECTRON_BASIC")
@ApiModel(value="SdElectronBasic对象", description="")
public class SdElectronBasic extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "电子券活动号")
    @TableField("GSEB_ID")
    private String gsebId;

    @ApiModelProperty(value = "电子券描述")
    @TableField("GSEB_NAME")
    private String gsebName;

    @ApiModelProperty(value = "面值")
    @TableField("GSEB_AMT")
    private BigDecimal gsebAmt;

    @ApiModelProperty(value = "是否启用")
    @TableField("GSEB_STATUS")
    private String gsebStatus;

    @ApiModelProperty(value = "有效时长")
    @TableField("GSEB_DURATION")
    private String gsebDuration;


}
