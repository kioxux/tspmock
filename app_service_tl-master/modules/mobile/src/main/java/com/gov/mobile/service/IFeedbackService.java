package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.FeedbackListVO;
import com.gov.mobile.dto.FeedbackVO;
import com.gov.mobile.entity.Feedback;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-23
 */
public interface IFeedbackService extends SuperService<Feedback> {

    /**
     * 建议反馈列表
     */
    Result feedbackList(FeedbackListVO feedbackListVO);

    /**
     * 建议反馈添加
     */
    Result feedbackAdd(FeedbackVO feedbackVO);

    /**
     * 建议反馈详情
     */
    Result feedbackDetail(String id);
}
