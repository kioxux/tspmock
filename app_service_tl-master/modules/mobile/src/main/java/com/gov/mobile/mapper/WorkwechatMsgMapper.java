package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.dto.WorkWechatMsgDTO;
import com.gov.mobile.dto.WorkWechatMsgPageDTO;
import com.gov.mobile.entity.WorkwechatMsg;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity generator.domain.WorkwechatMsg
 */
public interface WorkwechatMsgMapper extends BaseMapper<WorkwechatMsg> {

    /**
     * 分页查询
     * @param page
     * @param workWechatMsgPageDTO
     * @return
     */
    IPage<WorkWechatMsgDTO> selectListByPage(Page<WorkWechatMsgDTO> page,@Param("ew") WorkWechatMsgPageDTO workWechatMsgPageDTO);

    /**
     * 查询是否存在已发送的数据
     * @param ids
     * @return
     */
    int selectHasSendByIds(@Param("ids") List<Integer> ids);
}




