package com.gov.mobile.mapper;

import com.gov.mobile.entity.Franchisee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
public interface FranchiseeMapper extends BaseMapper<Franchisee> {

}
