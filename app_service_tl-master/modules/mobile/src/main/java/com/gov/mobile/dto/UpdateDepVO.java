package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UpdateDepVO {

    @NotBlank(message = "部门编码不能为空")
    private String depId;

    @NotBlank(message = "部门名称不能为空")
    private String depName;

    @NotBlank(message = "部门负责人ID不能为空")
    private String depHeadId;

    @NotBlank(message = "部门负责人姓名不能为空")
    private String depHeadNam;

    @NotBlank(message = "是否停用不能为空")
    private String depStatus;

}
