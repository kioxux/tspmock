package com.gov.mobile.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SD_RECHARGE_CARD")
@ApiModel(value="SdRechargeCard对象", description="")
public class SdRechargeCard extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "储值卡账户")
    @TableField("GSRC_ACCOUNT_ID")
    private String gsrcAccountId;

    @ApiModelProperty(value = "储值卡号")
    @TableField("GSRC_ID")
    private String gsrcId;

    @ApiModelProperty(value = "创建门店")
    @TableField("GSRC_BR_ID")
    private String gsrcBrId;

    @ApiModelProperty(value = "创建日期")
    @TableField("GSRC_DATE")
    private String gsrcDate;

    @ApiModelProperty(value = "创建人员")
    @TableField("GSRC_EMP")
    private String gsrcEmp;

    @ApiModelProperty(value = "状态")
    @TableField("GSRC_STATUS")
    private String gsrcStatus;

    @ApiModelProperty(value = "姓名")
    @TableField("GSRC_NAME")
    private String gsrcName;

    @ApiModelProperty(value = "性别")
    @TableField("GSRC_SEX")
    private String gsrcSex;

    @ApiModelProperty(value = "手机")
    @TableField("GSRC_MOBILE")
    private String gsrcMobile;

    @ApiModelProperty(value = "电话")
    @TableField("GSRC_TEL")
    private String gsrcTel;

    @ApiModelProperty(value = "地址")
    @TableField("GSRC_ADDRESS")
    private String gsrcAddress;

    @ApiModelProperty(value = "密码")
    @TableField("GSRC_PASSWORD")
    private String gsrcPassword;

    @ApiModelProperty(value = "当前余额")
    @TableField("GSRC_AMT")
    private BigDecimal gsrcAmt;

    @ApiModelProperty(value = "修改门店")
    @TableField("GSRC_UPDATE_BR_ID")
    private String gsrcUpdateBrId;

    @ApiModelProperty(value = "修改日期")
    @TableField("GSRC_UPDATE_DATE")
    private String gsrcUpdateDate;

    @ApiModelProperty(value = "修改人员")
    @TableField("GSRC_UPDATE_EMP")
    private String gsrcUpdateEmp;


}
