package com.gov.mobile.dto;

import com.gov.mobile.entity.AuthconfiData;
import lombok.Data;

import java.util.List;

/**
 * Author: 钱金华
 * Date: 2020-06-29
 * Time: 16:03
 */
@Data
public class AuthconfiDataDto extends AuthconfiData {

    /**
     * 员工编号
     */
    private String userId;

    /**
     * 用户姓名
     */
    private String userNam;

    /**
     * 手机号
     */
    private String userTel;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 权限集合数据
     */
    private List<String> authList;


}
