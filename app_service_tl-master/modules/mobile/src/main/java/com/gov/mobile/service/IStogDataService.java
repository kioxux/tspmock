package com.gov.mobile.service;

import com.gov.mobile.entity.StogData;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-07
 */
public interface IStogDataService extends SuperService<StogData> {

    /**
     * 获取门店组列表
     */
    List<StogData> getStogList();

    /**
     * 获取门店名
     */
    StogData getNameByCode(String client, String stogCode);
}
