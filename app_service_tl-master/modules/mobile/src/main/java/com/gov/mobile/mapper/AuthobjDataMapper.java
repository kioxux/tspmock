package com.gov.mobile.mapper;

import com.gov.mobile.entity.AuthobjData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
public interface AuthobjDataMapper extends BaseMapper<AuthobjData> {

}
