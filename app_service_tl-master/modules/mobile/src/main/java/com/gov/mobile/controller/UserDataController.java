package com.gov.mobile.controller;

import com.gov.common.response.Result;
import com.gov.mobile.dto.ChangeUserPswdVO;
import com.gov.mobile.dto.CheckUserPswdVO;
import com.gov.mobile.service.IUserDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-17
 */
@Api(tags = "用户信息")
@RestController
@RequestMapping("user")
public class UserDataController {

    @Resource
    private IUserDataService userDataService;

    @ApiOperation(value = "获取用户")
    @GetMapping("getUser")
    public Result getUser() {
        return userDataService.getUser();
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("changeUserPswd")
    public Result changeUserPswd(@Valid @RequestBody ChangeUserPswdVO changeUserPswdVO) {
        return userDataService.changeUserPswd(changeUserPswdVO);
    }

    @ApiOperation(value = "检测密码是否正确")
    @PostMapping("checkUserPswd")
    public Result checkUserPswd(@Valid @RequestBody CheckUserPswdVO checkUserPswdVO) {
        return userDataService.checkUserPswd(checkUserPswdVO);
    }

}

