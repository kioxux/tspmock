package com.gov.mobile.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PageDto extends Pageable {

    /**

    /**
     * 0未用，1已用
     */
    @NotNull(message = "类型不能为空")
    private Integer type;

}
