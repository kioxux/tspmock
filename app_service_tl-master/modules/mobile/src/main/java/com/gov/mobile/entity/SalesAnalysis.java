package com.gov.mobile.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_SALES_ANALYSIS")
@ApiModel(value="SalesAnalysis对象", description="")
public class SalesAnalysis extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "年度")
    @TableField("AN_YEAR")
    private String anYear;

    @ApiModelProperty(value = "门店编码")
    @TableField("GSSH_BR_ID")
    private String gsshBrId;

    @ApiModelProperty(value = "报表月、周")
    @TableField("AN_DATE")
    private String anDate;

    @ApiModelProperty(value = "统计周期")
    @TableField("DATE_CYCLE")
    private String dateCycle;

    @ApiModelProperty(value = "统计维度")
    @TableField("DATE_DETEAIL")
    private String dateDeteail;

    @ApiModelProperty(value = "销售额")
    @TableField("AN_AMT")
    private BigDecimal anAmt;

    @ApiModelProperty(value = "毛利率")
    @TableField("AN_GM")
    private BigDecimal anGm;

    @ApiModelProperty(value = "交易次数")
    @TableField("AN_TANS")
    private Integer anTans;

    @ApiModelProperty(value = "客单价")
    @TableField("AN_PRICE")
    private BigDecimal anPrice;

    @ApiModelProperty(value = "更新时间戳")
    @TableField("CRE_DATE")
    private String creDate;


}
