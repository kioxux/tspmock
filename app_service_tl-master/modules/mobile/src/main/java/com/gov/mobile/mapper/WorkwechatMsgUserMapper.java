package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.mobile.dto.WorkwechatMsgUserDTO;
import com.gov.mobile.entity.WorkwechatMsgUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Entity generator.domain.WorkwechatMsgUser
 */
public interface WorkwechatMsgUserMapper extends BaseMapper<WorkwechatMsgUser> {

    /**
     * 根据消息id删除
     * @param ids
     */
    int deleteByMsgIds(@Param("ids") List<Integer> ids);

    /**
     * 获取消息用户列表
     * @param msgId
     * @return
     */
    List<WorkwechatMsgUserDTO> selectUserListByMsgId(@Param("msgId") String msgId);

    /**
     * 根据发送状态获取需发送人员列表
     * @param params
     * @return
     */
    List<WorkwechatMsgUser> selectUserListByStatus(Map<String, Object> params);
}




