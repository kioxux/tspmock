package com.gov.mobile.service;

import com.gov.common.entity.Dictionary;
import com.gov.mobile.dto.CompadmDTO;
import com.gov.mobile.dto.CompadmEditVO;
import com.gov.mobile.dto.CompadmVO;
import com.gov.mobile.entity.Compadm;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface ICompadmService extends SuperService<Compadm> {

    /**
     * 获取对应加盟商下面的连锁总店
     * @param client
     * @return
     */
    List<Compadm> getCompadmListByClient(String client);

    /**
     * 获取连锁公司详情
     * @param compadmId
     * @return
     */
    CompadmDTO getCompadmInfo(String compadmId);

    /**
     * 连锁公司新增
     * @param compadmVO 入参
     * @return
     */
    void addCompadm(CompadmVO compadmVO);

    /**
     * 连锁公司编辑
     * @param compadmEditVO 入参
     */
    void editCompadm(CompadmEditVO compadmEditVO);

    /**
     * 选择纳税主体
     */
    List<Dictionary> getTaxSubject(String chainHead);
}
