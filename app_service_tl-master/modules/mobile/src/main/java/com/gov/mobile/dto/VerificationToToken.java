package com.gov.mobile.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class VerificationToToken {

    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value = "登录手机号")
    private String userTel;

    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value = "验证码")
    private String code;

    @NotBlank(message = "设备类型不能为空")
    @ApiModelProperty(value = "设备类型（android/ios）")
    private String platform;

    @ApiModelProperty(value = "设备号")
    private String deviceNo;

    public VerificationToToken() {
    }
}
