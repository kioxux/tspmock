package com.gov.mobile.dto;

import com.gov.mobile.entity.Compadm;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
public class CompadmDTO extends Compadm {

    /**
     * 法人/负责人姓名
     */
    private String LegalPersonName;

    /**
     * 质量负责人姓名
     */
    private String quaName;

    private String compadmLegalPhone;

    private String compadmQuaPhone;

    private String compadmLegalName;

    private String compadmQuaName;

    private String logoUrl;

    private String compadmLegalPerson;

    private String compadmQua;

    /**
     * 配送中心
     */
    private List<OrganizationDto> wholesaleList;

    /**
     * 门店
     */
    private List<OrganizationDto> storeList;

    /**
     * 部门
     */
    private List<OrganizationDepDTO> depList;

}
