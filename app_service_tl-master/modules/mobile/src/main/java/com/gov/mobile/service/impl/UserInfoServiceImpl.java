package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.entity.UserInfo;
import com.gov.mobile.mapper.UserInfoMapper;
import com.gov.mobile.service.IUserInfoService;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-01
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

    @Override
    public Result selectPage(Page<UserInfo> page, QueryWrapper<UserInfo> wrapper) {
        return ResultUtil.success(baseMapper.selectPageList(page, wrapper));
    }
}
