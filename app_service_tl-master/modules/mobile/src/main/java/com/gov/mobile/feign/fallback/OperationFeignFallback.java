package com.gov.mobile.feign.fallback;

import com.gov.common.response.Result;
import com.gov.mobile.feign.OperationFeign;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OperationFeignFallback implements OperationFeign {

    /**
     * 会员新增/修改
     *
     * @param map 入参
     * @return
     */
    @Override
    public Result memberSync(Map map) {
        Result result = new Result();
        result.setCode("500");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }
}
