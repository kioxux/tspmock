package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 成分疾病关联表
 * @TableName GAIA_DISEASE_CLASSIFCATION
 */
@TableName(value ="GAIA_DISEASE_CLASSIFCATION")
@Data
public class DiseaseClassifcation implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 疾病编码
     */
    private String diseaseCode;

    /**
     * 疾病
     */
    private String disease;

    /**
     * 疾病说明
     */
    private String diseaseDescription;

    /**
     * 成分细类编码
     */
    private String classificationCode;

    /**
     * 商品明细类成分名
     */
    private String ingredientClassification;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}