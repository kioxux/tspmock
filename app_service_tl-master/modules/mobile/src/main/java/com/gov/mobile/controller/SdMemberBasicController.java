package com.gov.mobile.controller;


import com.gov.common.entity.Pageable;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.*;
import com.gov.mobile.service.ISdMemberBasicService;
import com.gov.mobile.service.IUserDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-29
 */

@Api(tags = "公众号用户相关信息")
@RestController
@RequestMapping("member")
public class SdMemberBasicController {

    @Resource
    private ISdMemberBasicService iSdMemberBasicService;

    @Log("公众号会员注册")
    @PostMapping("register")
    @ApiOperation(value = "公众号会员注册")
    public Result memberRegister(@Valid @RequestBody MemberUserDto modelClient) {
        return iSdMemberBasicService.register(modelClient);
    }

    @Log("公众号会员注册(含企微绑定)")
    @PostMapping("cusRegister")
    @ApiOperation(value = "公众号会员注册(含企微绑定)")
    public Result cusRegister(@Valid @RequestBody MemberUserDto modelClient) {
        return iSdMemberBasicService.cusRegister(modelClient);
    }

    @Log("公众号会员登录")
    @PostMapping("loginByOpenId")
    @ApiOperation(value = "公众号会员登录")
    public Result memberLogin(@Valid @RequestBody MemberUserLoginDto loginDto) {
        return iSdMemberBasicService.Login(loginDto);
    }

    @Log("公众号获取会员信息")
    @GetMapping("getUserInfo")
    @ApiOperation(value = "公众号获取会员信息")
    public Result userInfo() {
        return iSdMemberBasicService.getUserInfo();
    }

    @Log("公众号获取会员绑定")
    @PostMapping("bindUser")
    @ApiOperation(value = "公众号获取会员绑定")
    public Result bindUserInfo(@Valid @RequestBody MemberBindUserDto bindUserDto) {
        return iSdMemberBasicService.bindUserInfo(bindUserDto);
    }

    @Log("公众号获取会员积分记录")
    @PostMapping("integralRecord")
    @ApiOperation(value = "公众号获取会员积分记录")
    public Result getPointRecord(@Valid @RequestBody Pageable page) {
        return iSdMemberBasicService.getPointList(page);
    }

    @Log("公众号获取会员余额")
    @PostMapping("getBalance")
    @ApiOperation(value = "公众号获取会员余额")
    public Result getMoneyRecord(@Valid @RequestBody Pageable page) {
        return iSdMemberBasicService.getMoneyRecordList(page);
    }

    @Log("公众号获取会员优惠券")
    @PostMapping("couponRecord")
    @ApiOperation(value = "公众号获取会员优惠券")
    public Result getCouponRecord(@Valid @RequestBody PageDto page) {
        return iSdMemberBasicService.getCouponRecordList(page);
    }

    @Log("公众号获取门店")
    @GetMapping("getStore")
    @ApiOperation(value = "公众号获取门店")
    public Result getStoreList(@RequestParam("client") String client, @RequestParam("stoCode") String stoCode) {
        return iSdMemberBasicService.getStoreList(client, stoCode);
    }

    @Log("获取公众号配置")
    @PostMapping("getConfig")
    @ApiOperation(value = "获取公众号配置")
    public Result getConfigList(@Valid @RequestBody ConfigDto configDto) {
        return iSdMemberBasicService.getConfigList(configDto);
    }

    @Log("注册生成电子券")
    @GetMapping("addRegisterElectron")
    @ApiOperation(value = "注册生成电子券")
    public Result addElectron(@RequestParam("client") String client, @RequestParam("cardNumber") String cardNumber) {
        iSdMemberBasicService.addElectron(client, cardNumber);
        return ResultUtil.success();
    }

}

