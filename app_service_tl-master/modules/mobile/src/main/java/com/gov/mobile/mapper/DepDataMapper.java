package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.mobile.entity.DepData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface DepDataMapper extends BaseMapper<DepData> {

    /**
     * 以加盟商为单位 取数字部门编码
     *
     * @param client 加盟商
     * @return
     */
    DepData getMaxNumDepId(@Param("client") String client);

    List<String> selectListForAuth(@Param("client") String client, @Param("chainHead") String chainHead);
}
