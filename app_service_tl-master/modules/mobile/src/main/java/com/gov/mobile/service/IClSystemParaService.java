package com.gov.mobile.service;

import com.gov.mobile.entity.ClSystemPara;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 公司参数表 服务类
 * </p>
 *
 * @author sy
 * @since 2021-12-07
 */
public interface IClSystemParaService extends SuperService<ClSystemPara> {

}
