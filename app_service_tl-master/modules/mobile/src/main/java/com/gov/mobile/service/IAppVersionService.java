package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.entity.AppVersion;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface IAppVersionService extends SuperService<AppVersion> {

    /**
     * APP最新版本
     */
    Result getAppLastVersion(Integer type);
}
