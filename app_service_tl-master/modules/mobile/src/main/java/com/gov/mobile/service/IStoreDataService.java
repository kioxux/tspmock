package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.StoreDataAddVO;
import com.gov.mobile.dto.StoreDataCodeDTO;
import com.gov.mobile.dto.StoreDataDTO;
import com.gov.mobile.dto.StoreDataEditVO;
import com.gov.mobile.entity.StoreData;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface IStoreDataService extends SuperService<StoreData> {

    /**
     * 通过加盟商获取门店列表
     *
     * @param client
     * @return
     */
    List<StoreData> getStoreListByClient(String client);


    /**
     * 通过加盟商 + 当前连锁总部 s获取门店列表
     *
     * @param client
     * @return
     */
    List<StoreData> getStoreListByClientAndChainHead(String client, String chainHead);

    /**
     * 门店详情
     * @param stoCode 门店编码
     */
    StoreDataDTO getStoreInfo(String stoCode);

    /**
     * 门店新增
     * @param storeDataAddVO
     */
    void addStore(StoreDataAddVO storeDataAddVO);

    /**
     * 门店编辑
     * @param storeDataEditVO
     */
    void editStore(StoreDataEditVO storeDataEditVO);

    /**
     * 门店编码获取
     */
    StoreDataCodeDTO getStoreCode();

    /**
     * 获取门店组及门店下拉框
     * @return
     */
    Result selectGroupStoreList();
}
