package com.gov.mobile.controller;

import com.gov.common.response.Result;
import com.gov.mobile.dto.UploadFile;
import com.gov.mobile.service.IFileService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.24
 */
@RestController
@RequestMapping("file")
public class FileController {

    @Resource
    private IFileService iFileService;

    @PostMapping("upload")
    @ApiOperation(value = "上传文件")
    public Result upload(UploadFile uploadFile) {
        return iFileService.upload(uploadFile);
    }

}

