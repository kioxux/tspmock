package com.gov.mobile.service;

import com.gov.mobile.dto.DcDataAddVO;
import com.gov.mobile.dto.DcDataDTO;
import com.gov.mobile.dto.DcDataEditVO;
import com.gov.mobile.entity.DcData;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface IDcDataService extends SuperService<DcData> {


    /**
     * 通过加盟商获取配送中心
     *
     * @param client
     * @return
     */
    List<DcData> getWholesaleListByClient(String client);


    /**
     * 通过加盟商+连锁总部 获取配送中心
     *
     * @param client    加盟商
     * @param chainHead 连锁总部
     * @return
     */
    List<DcData> getDcListByClientAndChainHead(String client, String chainHead);


    /**
     * 配送中心详情
     *
     * @param dcCode DC编码
     * @return
     */
    DcDataDTO getDcInfo(String dcCode);


    /**
     * 配送中心新增
     *
     * @param dcDataAddVO 新增入参
     * @return
     */
    void addDc(DcDataAddVO dcDataAddVO);


    /**
     * 配送中心编辑
     *
     * @param dataEditVO 编辑入参
     */
    void editDc(DcDataEditVO dataEditVO);
}
