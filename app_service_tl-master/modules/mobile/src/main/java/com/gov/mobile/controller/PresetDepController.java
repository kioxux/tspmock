package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.service.IPresetDepService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@RestController
@RequestMapping("presetDep")
public class PresetDepController {

    @Resource
    private IPresetDepService presetDepService;

    @GetMapping("getNoRepeatedDep")
    @ApiOperation(value = "获取不不重复的岗位")
    public Result getAppLastVersion() {
        return ResultUtil.success(presetDepService.getAppLastVersion());
    }
}

