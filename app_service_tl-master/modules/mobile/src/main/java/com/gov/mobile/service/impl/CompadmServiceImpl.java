package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.CosUtils;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.Dictionary;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.userutil.UserUtils;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.mapper.*;
import com.gov.mobile.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Service
public class CompadmServiceImpl extends ServiceImpl<CompadmMapper, Compadm> implements ICompadmService {


    @Resource
    private IUserDataService userDataService;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private IDcDataService dcDataService;
    @Resource
    private IDepDataService depDataService;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Resource
    private CompadmMapper compadmMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private ISmsTemplateService smsTemplateService;

    @Autowired
    private StoreDataMapper storeDataMapper;
    @Autowired
    private DepDataMapper depDataMapper;
    @Autowired
    private DcDataMapper dcDataMapper;
    @Autowired
    private UserDataMapper userDataMapper;
    @Autowired
    private IAuthconfiDataService authconfiService;
    /**
     * 获取对应加盟商下面的连锁总店
     */
    @Override
    public List<Compadm> getCompadmListByClient(String client) {
        QueryWrapper<Compadm> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        return this.list(queryWrapper);
    }

    /**
     * 根据主键获取连锁公司
     */
    private Compadm getCompadmByClientAndId(String client, String compadmId) {
        QueryWrapper<Compadm> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("COMPADM_ID", compadmId);
        return this.getOne(queryWrapper);
    }


    /**
     * 连锁公司详情
     */
    @Override
    public CompadmDTO getCompadmInfo(String compadmId) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        Compadm compadm = this.getCompadmByClientAndId(currentUser.getClient(), compadmId);
        if (StringUtils.isEmpty(compadm)) {
            throw new CustomResultException(ResultEnum.E0013);
        }
        CompadmDTO compadmDTO = new CompadmDTO();
        BeanUtils.copyProperties(compadm, compadmDTO);
        compadmDTO.setLogoUrl(cosUtils.urlAuth(compadm.getCompadmLogo()));
        // 法人姓名
        UserData user = userDataService.getUserByClientAndUserId(currentUser.getClient(), compadm.getCompadmLegalPerson());
        if (StringUtils.isNotEmpty(user)) {
            compadmDTO.setLegalPersonName(user.getUserNam());
            compadmDTO.setCompadmLegalPerson(user.getUserId());
            compadmDTO.setCompadmLegalName(user.getUserNam());
            compadmDTO.setCompadmLegalPhone(user.getUserTel());
        }

        if (StringUtils.isNotEmpty(compadm.getCompadmQua())) {
            // 质量负责人姓名
            user = userDataService.getUserByClientAndUserId(currentUser.getClient(), compadm.getCompadmQua());
            if (StringUtils.isNotEmpty(user)) {
                compadmDTO.setQuaName(user.getUserNam());
                compadmDTO.setCompadmQua(user.getUserId());
                compadmDTO.setCompadmQuaName(user.getUserNam());
                compadmDTO.setCompadmQuaPhone(user.getUserTel());
            }
        }

        // 配送中心
        List<OrganizationDto> dcList = new ArrayList<>();
        List<DcData> dcListByClientAndChainHead = dcDataService.getDcListByClientAndChainHead(currentUser.getClient(), compadmId);
        for (DcData dcData : dcListByClientAndChainHead) {
            dcList.add(new OrganizationDto(
                    dcData.getDcCode(),
                    dcData.getDcName(),
                    dcData.getDcStatus(),
                    CommonEnum.OrganizationType.DC.getCode()));
        }

        // 门店
        List<OrganizationDto> storeList = new ArrayList<>();
        List<StoreData> storeListByClientAndChainHead = storeDataService.getStoreListByClientAndChainHead(currentUser.getClient(), compadmId);
        for (StoreData storeData : storeListByClientAndChainHead) {
            storeList.add(new OrganizationDto(
                    storeData.getStoCode(),
                    storeData.getStoName(),
                    storeData.getStoStatus(),
                    CommonEnum.OrganizationType.STORE.getCode()));
        }

        // 部门
        List<OrganizationDepDTO> depList = new ArrayList<>();
        List<DepData> depListByClient = depDataService.getDepList(currentUser.getClient(), CommonEnum.DepType.CHAIN_DEP.getCode(), compadmId);
        for (DepData depData : depListByClient) {
            depList.add(new OrganizationDepDTO(
                    depData.getDepId(),
                    depData.getDepName(),
                    depData.getDepStatus(),
                    CommonEnum.OrganizationType.DEP.getCode(),
                    depData.getDepHeadId()));
        }

        compadmDTO.setWholesaleList(dcList);
        compadmDTO.setStoreList(storeList);
        compadmDTO.setDepList(depList);
        return compadmDTO;
    }


    /**
     * 连锁公司新增
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addCompadm(CompadmVO compadmVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        compadmVO.setClient(currentUser.getClient());
        String userLeaderId="";
        String userQuaId="";
        Compadm compadm = new Compadm();
        BeanUtils.copyProperties(compadmVO, compadm);
//        compadm.setClient(currentUser.getClient());
        compadm.setCompadmCreDate(DateUtils.getCurrentDateStrYYMMDD());
        compadm.setCompadmCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        compadm.setCompadmCreId(currentUser.getUserId());
        String compadmId = "100000";
        Compadm maxNumCompadm = compadmMapper.getMaxNumCompadmId(currentUser.getClient());
        if (StringUtils.isNotEmpty(maxNumCompadm)) {
            int compadmIdNum = Integer.valueOf(maxNumCompadm.getCompadmId()) + 1;
            compadmId = String.valueOf(compadmIdNum);
        }
        compadm.setCompadmId(compadmId);
        compadmVO.setCompadmId(compadmId);
        if (StringUtils.isNotEmpty(compadmVO.getCompadmQuaPhone())) {
            //如果店长和质量负责人是同一人
            if (compadmVO.getCompadmLegalPhone().equals(compadmVO.getCompadmQuaPhone())) {
                userLeaderId = getUserId(currentUser, compadmVO, null);

            } else {
                userLeaderId = getUserId(currentUser, compadmVO, CommonEnum.UserType.STATUS_LEADER.getCode());

                userQuaId = getUserId(currentUser, compadmVO, CommonEnum.UserType.STATUS_QUA.getCode());

            }
        }else {
            userLeaderId = getUserId(currentUser, compadmVO, null);
        }
        compadm.setCompadmLegalPerson(userLeaderId);
        compadm.setCompadmQua(userQuaId);
        // 设置主键

        this.save(compadm);
//        this.saveAuthconfiData(compadm);
    }


    /**
     * 连锁公司编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editCompadm(CompadmEditVO compadmEditVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        compadmEditVO.setClient(currentUser.getClient());
        Compadm compadmExit = this.getCompadmByClientAndId(currentUser.getClient(), compadmEditVO.getCompadmId());
        if (StringUtils.isEmpty(compadmExit)) {
            throw new CustomResultException(ResultEnum.E0013);
        }
        String userLeaderId="";
        String userQuaId="";
        Compadm compadm = new Compadm();
        BeanUtils.copyProperties(compadmEditVO, compadm, "compadmId");
        compadm.setCompadmModiDate(DateUtils.getCurrentDateStrYYMMDD());
        compadm.setCompadmModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        compadm.setCompadmModiId(currentUser.getUserId());
        CompadmVO compadmVO = new CompadmVO();
        BeanUtils.copyProperties(compadmEditVO, compadmVO);
        compadmVO.setClient(currentUser.getClient());
        if (StringUtils.isNotEmpty(compadmEditVO.getCompadmQuaPhone())) {
            //如果店长和质量负责人是同一人
            if (compadmVO.getCompadmLegalPhone().equals(compadmEditVO.getCompadmQuaPhone())) {
                userLeaderId = getUserId(currentUser, compadmVO, null);
                compadm.setCompadmLegalPerson(userLeaderId);
            } else {
                userLeaderId = getUserId(currentUser, compadmVO, CommonEnum.UserType.STATUS_LEADER.getCode());
                userQuaId = getUserId(currentUser, compadmVO, CommonEnum.UserType.STATUS_QUA.getCode());
                compadm.setCompadmLegalPerson(userLeaderId);
                compadm.setCompadmQua(userQuaId);
            }
        }else {
            userLeaderId = getUserId(currentUser, compadmVO, null);
            compadm.setCompadmLegalPerson(userLeaderId);

        }
        UpdateWrapper<Compadm> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", currentUser.getClient());
        updateWrapper.eq("COMPADM_ID", compadmEditVO.getCompadmId());
        this.update(compadm, updateWrapper);
        //判断当前连锁下有没有配送中心  有的话 修改配送中心和部门的name   连锁总部名+-配送中心
       int count = this.depDataMapper.selectCount( new QueryWrapper<DepData>().eq("CLIENT",currentUser.getClient()).eq("STO_CHAIN_HEAD",compadmEditVO.getCompadmId()).eq("DEP_CLS","10") );
        if(count>0){
            DepData inDepData = new DepData();
            inDepData.setDepName(compadmEditVO.getCompadmName()+"-配送中心");
            this.depDataMapper.update(inDepData,new UpdateWrapper<DepData>().eq("CLIENT",currentUser.getClient()).eq("STO_CHAIN_HEAD",compadmEditVO.getCompadmId()).eq("DEP_CLS","10") );
            DcData inDcData = new DcData();
            inDcData.setDcName(compadmEditVO.getCompadmName()+"-配送中心");
            this.dcDataMapper.update(inDcData,new UpdateWrapper<DcData>().eq("CLIENT",currentUser.getClient()).eq("DC_CHAIN_HEAD",compadmEditVO.getCompadmId()) );
        }
        List<String> siteList = new ArrayList<>();
        //获取旗下所有门店
        List<String> stoList = this.storeDataMapper.selectListForAuth(currentUser.getClient(), compadmEditVO.getCompadmId());
        //获取旗下所有部门
        List<String> depList = this.depDataMapper.selectListForAuth(currentUser.getClient(), compadmEditVO.getCompadmId());
        siteList.addAll(stoList);
        siteList.addAll(depList);
        if (!compadmExit.getCompadmLegalPerson().equals(compadmEditVO.getCompadmLegalPerson())) {
            this.editDepById(compadmEditVO.getCompadmId(),compadmEditVO.getCompadmName(),compadmEditVO.getCompadmLegalPerson());
            this.saveAuthConfig(currentUser.getClient(), compadmEditVO.getCompadmLegalPerson(), siteList, compadmExit.getCompadmLegalPerson());
        }
        if (!compadmEditVO.getCompadmQua().equals(compadm.getCompadmQua())) {
            this.editDepById(compadmEditVO.getCompadmId(),compadmEditVO.getCompadmName(),compadmEditVO.getCompadmQua());
            this.saveAuthConfig(currentUser.getClient(), compadmEditVO.getCompadmQua(), siteList, compadmExit.getCompadmQua());        }
    }

    @Transactional
    public void saveAuthConfig(String client ,String userId ,List<String> siteList, String delUserId ){

        List<String> groupList = new ArrayList();
        groupList.add("GAIA_AL_ADMIN");
        groupList.add("GAIA_AL_MANAGER");
        groupList.add("GAIA_WM_FZR");
        groupList.add("GAIA_MM_ZLZG");
        groupList.add("GAIA_MM_SCZG");
        groupList.add("SD_01");
        groupList.add("GAIA_FI_ZG");
        this.authconfiService.saveAuth(client, userId, groupList,siteList,delUserId);
    }

    @Transactional
    public void editDepById(String depId, String depName, String userId){
        UserLoginModelClient currentUser = userDataService.getCurrentUser();

        //修改店员的部门
        UserData userData = new UserData();
        userData.setDepId(depId);
        userData.setDepName(depName);
        userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
        userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        userData.setUserModiId(userId);
        UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", currentUser.getClient());
        updateWrapper.eq("USER_ID", userId);
        this.userDataMapper.update(userData,updateWrapper);
    }


    /**
     * 选择纳税主体
     */
    @Override
    public List<Dictionary> getTaxSubject(String chainHead) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        return compadmMapper.getTaxSubject(currentUser.getClient(), chainHead);
    }


    /**
     * 判断手机号是否已经注册 如果存在直接拿编码 不存则在创建角色
     * @param currentUser
     * @param code
     * @return
     */
    @Transactional
    public String  getUserId(UserLoginModelClient currentUser, CompadmVO compadmVO, String code) {
        String userId ="";
        UserData userData;
        if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
            userData = userDataService.getUserByClientAndPhone(currentUser.getClient(),compadmVO.getCompadmQuaPhone());
        }else {
            userData = userDataService.getUserByClientAndPhone(currentUser.getClient(),compadmVO.getCompadmLegalPhone());
        }

        String passWord =String.valueOf(new Random().nextInt(900000) + 100000);
        if(StringUtils.isEmpty(userData)){
            userData = new UserData();
            int userMax = this.userDataService.selectMaxId(currentUser.getClient());
            userData.setClient(currentUser.getClient());
            userData.setDepId(compadmVO.getCompadmId()); //部门 改为门店
            userData.setDepName(compadmVO.getCompadmName());
            userData.setUserId(String.valueOf(userMax + 1));
            if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
                userData.setUserNam(compadmVO.getCompadmQuaName());
                userData.setUserTel(compadmVO.getCompadmQuaPhone());
            }else {
                userData.setUserNam(compadmVO.getCompadmLegalName());
                userData.setUserTel(compadmVO.getCompadmLegalPhone());
            }
//            userData.setUserPassword(UserUtils.encryptMD5(passWord));
            userData.setUserPassword(UserUtils.encryptMD5("123456"));
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserJoinDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(currentUser.getUserId());
            userData.setUserCreId(currentUser.getUserId());
            this.userDataService.insert(userData);
            userId = String.valueOf(userMax + 1);
            compadmVO.setCompadmLegalPerson(userId);
//            this.sendSms(compadmVO,code,passWord);
        }else {
            userId = userData.getUserId();
            userData.setDepId(compadmVO.getCompadmId()); //部门 改为门店
            userData.setDepName(compadmVO.getCompadmName());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(currentUser.getUserId());
            userData.setUserCreId(currentUser.getUserId());
            UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("CLIENT", currentUser.getClient());
            updateWrapper.eq("USER_ID", userId);
            this.userDataService.update(userData,updateWrapper);
            compadmVO.setCompadmLegalPerson(userId);
//            this.sendUpdateSms(compadmVO);
        }
        this.saveAuthconfiData(currentUser.getClient(),compadmVO.getCompadmId(),userData.getUserId());

        //给店长发短信
        return userId;
    }

    /**
     * 新增时权限分配 // 新增的时候没有子DC/子门店/子部门
     */
    private void saveAuthconfiData(String client,String compadmId,String userId) {
        List<AuthconfiData> list = new ArrayList<>();
        for (CommonEnum.AuthGroup item : CommonEnum.AuthGroup.values()) {
            AuthconfiData auth = new AuthconfiData();
            auth.setClient(client);
            auth.setAuthGroupId(item.getCode());
            auth.setAuthGroupName(item.getMessage());
            auth.setAuthobjSite(compadmId);
            auth.setAuthconfiUser(userId);
            list.add(auth);
        }
        authconfiDataMapper.insertIgnoreList(list);
    }

    public void sendSms(CompadmVO compadmVO,String code,String passWord) {
//        if(CommonEnum.UserType.STATUS_LEADER.getCode().equals(code)){
            Verification verification = new Verification(compadmVO.getCompadmLegalPhone(),"2",compadmVO.getCompadmName(),passWord);
            smsTemplateService.sendStoreRegisterSms(verification);
//        }
    }


    public void sendUpdateSms(CompadmVO compadmVO) {
//        if(CommonEnum.UserType.STATUS_LEADER.getCode().equals(code)){
        UserData userData = userDataService.getUserByClientAndUserId(compadmVO.getClient(),compadmVO.getCompadmLegalPerson());
        Verification verification = new Verification(userData.getUserTel(),"2",compadmVO.getCompadmName(),null);
        smsTemplateService.sendUpdateStoreRegisterSms(verification);
//        }
    }

}











