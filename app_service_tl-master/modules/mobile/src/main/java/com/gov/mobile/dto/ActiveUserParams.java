package com.gov.mobile.dto;

import lombok.Data;

@Data
public class ActiveUserParams {

    private String cardId;

    private String ticket;

    private String openid;

    private String encryptCode;

    private String outer_str;



}
