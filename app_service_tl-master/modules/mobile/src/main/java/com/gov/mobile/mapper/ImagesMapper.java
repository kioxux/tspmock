package com.gov.mobile.mapper;

import com.gov.mobile.entity.Images;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-24
 */
public interface ImagesMapper extends BaseMapper<Images> {

}
