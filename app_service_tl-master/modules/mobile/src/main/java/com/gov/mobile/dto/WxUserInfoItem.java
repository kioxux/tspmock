package com.gov.mobile.dto;

import lombok.Data;

@Data
public class WxUserInfoItem {

    private String name;

    private String value;
}
