package com.gov.mobile.dto.workWeixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public
class LinkDTO {
    private String title;

    private String picurl;

    private String desc;

    private String url;
}