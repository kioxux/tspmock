package com.gov.mobile.service;

import com.gov.mobile.entity.SdMemberCard;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface ISdMemberCardService extends SuperService<SdMemberCard> {

}
