package com.gov.mobile.service.impl;

import com.gov.mobile.entity.MemberLoginLog;
import com.gov.mobile.mapper.MemberLoginLogMapper;
import com.gov.mobile.service.IMemberLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Service
public class MemberLoginLogServiceImpl extends ServiceImpl<MemberLoginLogMapper, MemberLoginLog> implements IMemberLoginLogService {

}
