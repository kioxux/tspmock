package com.gov.mobile.service.impl;

import com.gov.common.basic.CosUtils;
import com.gov.common.response.Result;
import com.gov.mobile.dto.UploadFile;
import com.gov.mobile.service.IFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.24
 */
@Service
public class FileServiceImpl implements IFileService {

    @Resource
    private CosUtils cosUtils;

    /**
     * 上传文件
     *
     * @param uploadFile
     * @return
     */
    @Override
    public Result upload(UploadFile uploadFile) {
        return cosUtils.uploadFile(uploadFile.getFile(), uploadFile.getType());
    }
}
