package com.gov.mobile.service;

import com.gov.mobile.entity.Images;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-24
 */
public interface IImagesService extends SuperService<Images> {

}
