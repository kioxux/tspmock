package com.gov.mobile.service;

import com.gov.mobile.entity.CompadmWms;
import com.gov.mybatis.SuperService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
public interface ICompadmWmsService extends SuperService<CompadmWms> {

    List<CompadmWms> getCompadmWmsListByClient(String client);
}
