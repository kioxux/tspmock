package com.gov.mobile.dto.workWeixin;

import lombok.Data;

import java.util.List;

@Data
public class FollowUserDTO {

    private String userid;

    private List<CusTagDTO> tags;
}
