package com.gov.mobile.service.impl;

import com.gov.common.basic.DateUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.SalesAnalysisReportVo;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.entity.Franchisee;
import com.gov.mobile.entity.SalesAnalysis;
import com.gov.mobile.mapper.FranchiseeMapper;
import com.gov.mobile.mapper.SalesAnalysisMapper;
import com.gov.mobile.service.ISalesAnalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.mobile.service.IUserDataService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
@Service
public class SalesAnalysisServiceImpl extends ServiceImpl<SalesAnalysisMapper, SalesAnalysis> implements ISalesAnalysisService {

    @Resource
    private SalesAnalysisMapper salesAnalysisMapper;
    @Resource
    private IUserDataService userDataService;
    @Resource
    private FranchiseeMapper franchiseeMapper;
    /**
     * 门店报表，当前月份明细
     *
     * @return
     */
    @Override
    public Result getStoreReportByMonth() {
        // 当前用户
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 当前月门店数据
        List<SalesAnalysis> list = salesAnalysisMapper.getStoreReportByMonth(currentUser.getClient());
        return ResultUtil.success(initStorePeport(list));
    }

    /**
     * 门店报表，当前周明细
     *
     * @return
     */
    @Override
    public Result getStoreReportByWeek() {
        // 当前用户
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 当第几周
        int[] index = DateUtils.getWeekIndex(null);
        // 当前月门店数据
        List<SalesAnalysis> list = salesAnalysisMapper.getStoreReportByWeek(currentUser.getClient(), index[1]);
        return ResultUtil.success(initStorePeport(list));
    }

    /**
     * 汇总报表，三个月数据
     *
     * @return
     */
    @Override
    public Result getSummaryReportByMonth() {
        // 当前用户
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 三个月数据
        List<SalesAnalysis> list = salesAnalysisMapper.getSummaryReportByMonth(currentUser.getClient());
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();
        // 当前年
        salesAnalysisReportVo.setYear(StringUtils.left(DateUtils.getCurrentMonthStr(), 4));
        // 当前周
        salesAnalysisReportVo.setWeek(String.valueOf(DateUtils.getWeekIndex(null)[1]));
        // 当前月份
        salesAnalysisReportVo.setMonth(StringUtils.rightPad(DateUtils.getCurrentMonthStr(), 2));
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(currentUser.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());
        // 存在门店当前月统计数据
        if (CollectionUtils.isNotEmpty(list)) {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (int i = 2; i >= 0; i--) {
                // 当第几月
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -i);
                int month = calendar.get(Calendar.MONTH) + 1;
                boolean flg = false;
                for (SalesAnalysis salesAnalysis : list) {
                    // 年
                    String year = salesAnalysis.getAnYear();
                    // 报表月、周
                    String date = salesAnalysis.getAnDate();
                    // 存在当前月数据
                    if (String.valueOf(calendar.get(Calendar.YEAR)).equals(year) &&
                            StringUtils.leftPad(String.valueOf(month), 2, "0").equals(date)) {
                        flg = true;
                        String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(month), 2, "0");
                        // 销售额
                        SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                        amt.setXAxis(xAxis);
                        amt.setYAxis(salesAnalysis.getAnAmt());
                        amtList.add(amt);
                        //毛利率
                        SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                        gm.setXAxis(xAxis);
                        gm.setYAxis(salesAnalysis.getAnGm());
                        gmList.add(gm);
                        //交易次数
                        SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                        tans.setXAxis(xAxis);
                        tans.setYAxis(BigDecimal.valueOf(salesAnalysis.getAnTans()));
                        tansList.add(tans);
                        //客单价
                        SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                        price.setXAxis(xAxis);
                        price.setYAxis(salesAnalysis.getAnPrice());
                        priceList.add(price);
                    }
                }
                if (!flg) {
                    String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(month), 2, "0");
                    // 销售额
                    SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                    amt.setXAxis(xAxis);
                    amt.setYAxis(BigDecimal.ZERO);
                    amtList.add(amt);
                    //毛利率
                    SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                    gm.setXAxis(xAxis);
                    gm.setYAxis(BigDecimal.ZERO);
                    gmList.add(gm);
                    //交易次数
                    SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                    tans.setXAxis(xAxis);
                    tans.setYAxis(BigDecimal.ZERO);
                    tansList.add(tans);
                    //客单价
                    SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                    price.setXAxis(xAxis);
                    price.setYAxis(BigDecimal.ZERO);
                    priceList.add(price);
                }
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        } else {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (int i = 2; i >= 0; i--) {
                // 当第几周
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -i);
                int month = calendar.get(Calendar.MONTH) + 1;
                String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(month), 2, "0");
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(xAxis);
                amt.setYAxis(BigDecimal.ZERO);
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(xAxis);
                gm.setYAxis(BigDecimal.ZERO);
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(xAxis);
                tans.setYAxis(BigDecimal.ZERO);
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(xAxis);
                price.setYAxis(BigDecimal.ZERO);
                priceList.add(price);
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        }
        return ResultUtil.success(salesAnalysisReportVo);
    }

    /**
     * 汇总报表，十二周数据
     *
     * @return
     */
    @Override
    public Result getSummaryReportByWeek() {
        // 当前用户
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 当第几周
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -77);
        int[] index = DateUtils.getWeekIndex(calendar);
        String start = index[0] + StringUtils.leftPad(String.valueOf(index[1]), 2, "0");
        // 十二周数据
        List<SalesAnalysis> list = salesAnalysisMapper.getSummaryReportByWeek(currentUser.getClient(), start);
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();
        // 当前年
        salesAnalysisReportVo.setYear(StringUtils.left(DateUtils.getCurrentMonthStr(), 4));
        // 当前周
        salesAnalysisReportVo.setWeek(String.valueOf(DateUtils.getWeekIndex(null)[1]));
        // 当前月
        salesAnalysisReportVo.setMonth(StringUtils.rightPad(DateUtils.getCurrentMonthStr(), 2));
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(currentUser.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());
        // 存在门店当前月统计数据
        if (CollectionUtils.isNotEmpty(list)) {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (int i = 11; i >= 0; i--) {
                // 当第几周
                Calendar calendarObj = Calendar.getInstance();
                calendarObj.add(Calendar.DATE, -i * 7);
                int[] iteIindex = DateUtils.getWeekIndex(calendarObj);
                boolean flg = false;
                for (SalesAnalysis salesAnalysis : list) {
                    // 年
                    String year = salesAnalysis.getAnYear();
                    // 报表月、周
                    String date = salesAnalysis.getAnDate();
                    // 存在当前月数据
                    if (String.valueOf(calendar.get(Calendar.YEAR)).equals(year) &&
                            StringUtils.leftPad(String.valueOf(iteIindex[1]), 2, "0").equals(date)) {
                        flg = true;
                        String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(iteIindex[1]), 2, "0");
                        // 销售额
                        SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                        amt.setXAxis(xAxis);
                        amt.setYAxis(salesAnalysis.getAnAmt());
                        amtList.add(amt);
                        //毛利率
                        SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                        gm.setXAxis(xAxis);
                        gm.setYAxis(salesAnalysis.getAnGm());
                        gmList.add(gm);
                        //交易次数
                        SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                        tans.setXAxis(xAxis);
                        tans.setYAxis(BigDecimal.valueOf(salesAnalysis.getAnTans()));
                        tansList.add(tans);
                        //客单价
                        SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                        price.setXAxis(xAxis);
                        price.setYAxis(salesAnalysis.getAnPrice());
                        priceList.add(price);
                    }
                }
                if (!flg) {
                    String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(iteIindex[1]), 2, "0");
                    // 销售额
                    SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                    amt.setXAxis(xAxis);
                    amt.setYAxis(BigDecimal.ZERO);
                    amtList.add(amt);
                    //毛利率
                    SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                    gm.setXAxis(xAxis);
                    gm.setYAxis(BigDecimal.ZERO);
                    gmList.add(gm);
                    //交易次数
                    SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                    tans.setXAxis(xAxis);
                    tans.setYAxis(BigDecimal.ZERO);
                    tansList.add(tans);
                    //客单价
                    SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                    price.setXAxis(xAxis);
                    price.setYAxis(BigDecimal.ZERO);
                    priceList.add(price);
                }
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        } else {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (int i = 11; i >= 0; i--) {
                // 当第几周
                Calendar calendarObj = Calendar.getInstance();
                calendarObj.add(Calendar.DATE, -i * 7);
                int[] iteIindex = DateUtils.getWeekIndex(calendarObj);
                String xAxis = calendar.get(Calendar.YEAR) + "-" + StringUtils.leftPad(String.valueOf(iteIindex[1]), 2, "0");
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(xAxis);
                amt.setYAxis(BigDecimal.ZERO);
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(xAxis);
                gm.setYAxis(BigDecimal.ZERO);
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(xAxis);
                tans.setYAxis(BigDecimal.ZERO);
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(xAxis);
                price.setYAxis(BigDecimal.ZERO);
                priceList.add(price);
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        }
        return ResultUtil.success(salesAnalysisReportVo);
    }

    /**
     * 初始化门店报表数据
     *
     * @param list
     */
    private SalesAnalysisReportVo initStorePeport(List<SalesAnalysis> list) {
        SalesAnalysisReportVo salesAnalysisReportVo = new SalesAnalysisReportVo();
        // 当前年
        salesAnalysisReportVo.setYear(StringUtils.left(DateUtils.getCurrentMonthStr(), 4));
        // 当前周
        salesAnalysisReportVo.setWeek(String.valueOf(DateUtils.getWeekIndex(null)[1]));
        // 当前月份
        salesAnalysisReportVo.setMonth(StringUtils.rightPad(DateUtils.getCurrentMonthStr(), 2));
        // 当前用户
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        // 当前加盟商
        Franchisee franchisee = franchiseeMapper.selectById(currentUser.getClient());
        salesAnalysisReportVo.setFrancName(franchisee.getFrancName());
        if (CollectionUtils.isEmpty(list)) {
            return salesAnalysisReportVo;
        }
        // 存在门店当前月统计数据
        if (CollectionUtils.isNotEmpty(list)) {
            // 销售额
            List<SalesAnalysisReportVo> amtList = new ArrayList<>();
            //毛利率
            List<SalesAnalysisReportVo> gmList = new ArrayList<>();
            //交易次数
            List<SalesAnalysisReportVo> tansList = new ArrayList<>();
            //客单价
            List<SalesAnalysisReportVo> priceList = new ArrayList<>();
            for (SalesAnalysis salesAnalysis : list) {
                // 销售额
                SalesAnalysisReportVo amt = new SalesAnalysisReportVo();
                amt.setXAxis(salesAnalysis.getGsshBrId());
                amt.setYAxis(salesAnalysis.getAnAmt());
                amtList.add(amt);
                //毛利率
                SalesAnalysisReportVo gm = new SalesAnalysisReportVo();
                gm.setXAxis(salesAnalysis.getGsshBrId());
                gm.setYAxis(salesAnalysis.getAnGm());
                gmList.add(gm);
                //交易次数
                SalesAnalysisReportVo tans = new SalesAnalysisReportVo();
                tans.setXAxis(salesAnalysis.getGsshBrId());
                tans.setYAxis(BigDecimal.valueOf(salesAnalysis.getAnTans()));
                tansList.add(tans);
                //客单价
                SalesAnalysisReportVo price = new SalesAnalysisReportVo();
                price.setXAxis(salesAnalysis.getGsshBrId());
                price.setYAxis(salesAnalysis.getAnPrice());
                priceList.add(price);
            }
            // 销售额
            salesAnalysisReportVo.setAmtList(amtList);
            //毛利率
            salesAnalysisReportVo.setGmList(gmList);
            //交易次数
            salesAnalysisReportVo.setTansList(tansList);
            //客单价
            salesAnalysisReportVo.setPriceList(priceList);
        }
        return salesAnalysisReportVo;
    }
}
