package com.gov.mobile.dto;

import lombok.Data;

@Data
public class TokenData {

    /**
     * 返回类型
     */
    private String tokenType;

    /**
     * 具体数据
     */
    private Object data;

    public TokenData() {
    }

    public TokenData(String tokenType, Object data) {
        this.tokenType = tokenType;
        this.data = data;
    }


}
