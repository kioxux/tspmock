package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.mobile.dto.UserMessageListVO;
import com.gov.mobile.service.IUserMessageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@RestController
@RequestMapping("userMessage")
public class UserMessageController {

    @Resource
    private IUserMessageService iUserMessageService;

    @PostMapping("messageList")
    @ApiOperation(value = "用户消息列表")
    public Result messageList(@Valid @RequestBody UserMessageListVO userMessageListVO) {
        return iUserMessageService.messageList(userMessageListVO);
    }

    @GetMapping("messageDetail")
    @ApiOperation(value = "用户消息详情(用户消息标为已读)")
    public Result messageDetail(@RequestParam(name = "id", required = true) String id) {
        return iUserMessageService.messageDetail(id);
    }

}

