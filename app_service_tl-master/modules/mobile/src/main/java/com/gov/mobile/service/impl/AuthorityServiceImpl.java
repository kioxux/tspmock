package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.common.basic.CommonEnum;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.mapper.AuthconfiDataMapper;
import com.gov.mobile.mapper.AuthobjDataMapper;
import com.gov.mobile.mapper.GroupDataMapper;
import com.gov.mobile.mapper.StoreDataMapper;
import com.gov.mobile.service.*;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class AuthorityServiceImpl implements IAuthorityService {

    @Resource
    private IUserDataService userDataService;
    @Resource
    private AuthobjDataMapper authobjDataMapper;
    @Resource
    private GroupDataMapper groupDataMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Resource
    private IDepDataService iDepDataService;
    @Resource
    private IDcDataService iDcDataService;
    @Resource
    private ICompadmService iCompadmService;
    @Resource
    private IStoreDataService iStoreDataService;


    /**
     * 权限功能获取
     *
     * @return
     */
    @Override
    public List<AuthobjData> getAuthobjList() {
        return authobjDataMapper.selectList(null);
    }


    /**
     * 岗位数据获取
     *
     * @return
     */
    @Override
    public List<GroupData> getGroupList(String authobjId) {
        QueryWrapper<GroupData> wrapper = new QueryWrapper<>();
        wrapper.eq("AUTHOBJ_SITE", authobjId).
                select("AUTH_GROUP_ID", "AUTH_GROUP_NAME").
                groupBy("AUTH_GROUP_ID", "AUTH_GROUP_NAME");

        return groupDataMapper.selectList(wrapper);
    }


    /**
     * 门店集合获取
     *
     * @return
     */
    @Override
    public List<StoreData> getStoreList() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();

        return storeDataMapper.selectList(new QueryWrapper<StoreData>().eq("CLIENT", currentUser.getClient()));
    }


    /**
     * 权限用户数据获取
     *
     * @param authGroupId 权限组(岗位)编号
     * @param stoCode     门店编码
     * @return
     */
    @Override
    public List<AuthconfiDataDto> getAuthConfi(String authGroupId, String authobjId, String stoCode) {
        if (stoCode == null || stoCode == "") {
            return authconfiDataMapper.selectAuthConfiA(authGroupId, authobjId, stoCode, userDataService.getCurrentUser().getClient());
        } else {
            return authconfiDataMapper.selectAuthConfiB(authGroupId, authobjId, stoCode, userDataService.getCurrentUser().getClient());
        }
    }


    /**
     * 用户权限数据保存
     *
     * @return
     */
    @Override
    public Integer setAuthConfi(GroupDataDto param) {
        if (CollectionUtils.isEmpty(param.getAuthconfis())) {
            throw new CustomResultException(ResultEnum.E0602);
        }

        List<AuthconfiData> authconfiDataList = new ArrayList<>();
        param.getAuthconfis().forEach(item -> {
            // 门店编码 不为空
            if (item.getStoCode() != null && item.getStoCode() != "") {
                AuthconfiData authconfiData = new AuthconfiData();
                authconfiData.setClient(userDataService.getCurrentUser().getClient());
                authconfiData.setAuthGroupId(param.getAuthGroupId());
                authconfiData.setAuthGroupName(param.getAuthGroupName());
                authconfiData.setAuthobjSite(item.getStoCode());
                authconfiData.setAuthconfiUser(item.getUserId());
                authconfiDataList.add(authconfiData);
                // 门店编码 为空
            } else {
                if (CollectionUtils.isNotEmpty(item.getAuthList())) {
                    item.getAuthList().forEach(itemAuth -> {
                        AuthconfiData authconfiData = new AuthconfiData();
                        authconfiData.setClient(userDataService.getCurrentUser().getClient());
                        authconfiData.setAuthGroupId(param.getAuthGroupId());
                        authconfiData.setAuthGroupName(param.getAuthGroupName());
                        authconfiData.setAuthobjSite(itemAuth);
                        authconfiData.setAuthconfiUser(item.getUserId());
                        authconfiDataList.add(authconfiData);
                    });
                }
            }
        });
        List<AuthconfiData> repeat = authconfiDataMapper.selectRepeat(authconfiDataList);
        if (CollectionUtils.isNotEmpty(repeat)) {
            throw new CustomResultException("人员设置重复");
        }
        return authconfiDataMapper.insertList(authconfiDataList);
    }

    @Override
    public List<UserByClientDTO> getUserByClient() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();

        QueryWrapper<UserData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", currentUser.getClient());
        List<UserData> listUser = userDataService.list(queryWrapper);
        List<UserByClientDTO> list = new ArrayList<>();
        for (UserData userData : listUser) {
            UserByClientDTO userByClientDTO = new UserByClientDTO();
            BeanUtils.copyProperties(userData, userByClientDTO);
            list.add(userByClientDTO);
        }
        return list;
    }

    /**
     * 用户权限数据删除
     *
     * @param param
     * @return
     */
    @Override
    public Object delAuthConfi(GroupDataDto param) {
        if (CollectionUtils.isEmpty(param.getAuthconfis())) {
            throw new CustomResultException(ResultEnum.E0601);
        }

        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        AuthconfiDataDto dto = param.getAuthconfis().get(0);
        authconfiDataMapper.deleteByParams(param.getAuthGroupId(), dto.getStoCode(), null, param.getAuthconfis(), currentUser.getClient());
        return null;
    }


    /**
     * 用户权限数据修改
     *
     * @param param
     * @return
     */
    @Override
    public Object editAuthConfi(GroupDataDto param) {
        if (CollectionUtils.isEmpty(param.getAuthconfis())) {
            throw new CustomResultException(ResultEnum.E0602);
        }

        // 删除 原关联数据
        authconfiDataMapper.deleteByParam(param.getAuthGroupId(), param.getAuthconfis().get(0).getUserId(), userDataService.getCurrentUser().getClient());
        // 保存 新关联数据
        return this.setAuthConfi(param);
    }


    /**
     * 组织获取
     */
    @Override
    public OrganizationListDTO getAuthOrg() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        String client = currentUser.getClient();

        OrganizationListDTO listDTO = new OrganizationListDTO();
        // 单体门店
        listDTO.setStoreList(getStoreList(client));
        // 连锁
        listDTO.setCompadmList(getCompadmList(client));
        // 批发公司
        listDTO.setWholesaleList(getWholesaleList(client));
        // 公司部门
        listDTO.setDepList(getDepList(client));
        // 公司仓库
        listDTO.setWarehouseList(getWarehouseList(client));

        return listDTO;
    }

    /**
     * 组织获取 保存到一个list中
     *
     * @return List<OrganizationDto>
     */
    @Override
    public List<OrganizationDto> getAuthOrgToOneList() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        String client = currentUser.getClient();

        List<OrganizationDto> list = new ArrayList<>();
        // 单体门店
        list.addAll(getStoreList(client));
        // 连锁
        list.addAll(getCompadmList(client));
        // 批发公司
        list.addAll(getWholesaleList(client));
        // 公司部门
        list.addAll(getDepList(client));
        // 公司仓库
        list.addAll(getWarehouseList(client));

        return list;
    }

    /**
     * 单体门店--门店主数据信息表（GAIA_STORE_DATA）  门店属性 in (1-单体、3-加盟、4-门诊) && 加盟商
     */
    private List<OrganizationDto> getStoreList(String client) {
        List<OrganizationDto> storeList = new ArrayList<>();
        QueryWrapper<StoreData> queryWrapperSto = new QueryWrapper<>();
        queryWrapperSto.eq("CLIENT", client);
        // 门店类型(1,3,4)
        queryWrapperSto.in("STO_ATTRIBUTE",
                CommonEnum.StoreType.MONOMER_STORE.getCode(),
                CommonEnum.StoreType.JOIN_STORE.getCode(),
                CommonEnum.StoreType.CLINIC_STORE.getCode());
        queryWrapperSto.orderByDesc("STO_ATTRIBUTE");
        List<StoreData> storeDataList = iStoreDataService.list(queryWrapperSto);
        for (StoreData storeData : storeDataList) {
            OrganizationDto org = new OrganizationDto();
            org.setCode(storeData.getStoCode());
            org.setName(storeData.getStoName());
            org.setStatus(storeData.getStoStatus());
            org.setType(CommonEnum.OrganizationType.STORE.getCode());
            storeList.add(org);
        }
        return storeList;
    }

    /**
     * 连锁
     */
    private List<OrganizationDto> getCompadmList(String client) {
        List<OrganizationDto> compadmList = new ArrayList<>();
        // 连锁总部
        QueryWrapper<Compadm> queryWrapperComp = new QueryWrapper<>();
        queryWrapperComp.eq("CLIENT", client);
        List<Compadm> compadmListList = iCompadmService.list(queryWrapperComp);
        for (Compadm compadm : compadmListList) {
            compadmList.add(new OrganizationDto(
                    compadm.getCompadmId(),
                    compadm.getCompadmName(),
                    CommonEnum.OrganizationType.COMPADM.getCode()
            ));

            // 对应连锁总部的 配送中心
            QueryWrapper<DcData> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("CLIENT", client);
            // 连锁总部
            queryWrapper1.eq("DC_CHAIN_HEAD", compadm.getCompadmId());
            // DC类型
            queryWrapper1.eq("DC_TYPE", CommonEnum.DCType.CHAIN_DC.getCode());
            // 没有批发资质
            queryWrapper1.eq("DC_WHOLESALE", CommonEnum.DC_WHOLESALE.DC_WHOLESALE_NO.getCode());

            List<DcData> list = iDcDataService.list(queryWrapper1);
            for (DcData dcData : list) {
                compadmList.add(new OrganizationDto(
                        dcData.getDcCode(),
                        dcData.getDcName(),
                        dcData.getDcStatus(),
                        CommonEnum.OrganizationType.DC.getCode()
                ));
            }

            // 对应连锁总部的 连锁门店
            QueryWrapper<StoreData> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("CLIENT", client);
            // 连锁总部
            queryWrapper2.eq("STO_CHAIN_HEAD", compadm.getCompadmId());
            // 门店类型(2,连锁)
            queryWrapper2.eq("STO_ATTRIBUTE", CommonEnum.StoreType.CHAIN_STORE.getCode());
            List<StoreData> list1 = iStoreDataService.list(queryWrapper2);
            for (StoreData storeData : list1) {
                compadmList.add(new OrganizationDto(
                        storeData.getStoCode(),
                        storeData.getStoName(),
                        storeData.getStoStatus(),
                        CommonEnum.OrganizationType.STORE.getCode()
                ));
            }

            // 对应连锁总部的 部门
            QueryWrapper<DepData> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("CLIENT", client);
            // 连锁总部
            queryWrapper3.eq("STO_CHAIN_HEAD", compadm.getCompadmId());
            // 部门类型(20:连锁)
            queryWrapper3.eq("DEP_TYPE", CommonEnum.DepType.CHAIN_DEP.getCode());
            List<DepData> list2 = iDepDataService.list(queryWrapper3);
            for (DepData depData : list2) {
                compadmList.add(new OrganizationDto(
                        depData.getDepId(),
                        depData.getDepName(),
                        depData.getDepStatus(),
                        CommonEnum.OrganizationType.DEP.getCode()));
            }
        }
        return compadmList;
    }

    /**
     * 批发公司
     */
    private List<OrganizationDto> getWholesaleList(String client) {
        List<OrganizationDto> wholesaleList = new ArrayList<>();

        QueryWrapper<DcData> queryWrapperDc = new QueryWrapper<>();
        queryWrapperDc.eq("CLIENT", client);
        // DC类型(批发公司)
        queryWrapperDc.eq("DC_TYPE", CommonEnum.DCType.WHOLESALE_DC.getCode());
        // 有批发资质
        queryWrapperDc.eq("DC_WHOLESALE", CommonEnum.DC_WHOLESALE.DC_WHOLESALE_YES.getCode());
        List<DcData> dcDataList = iDcDataService.list(queryWrapperDc);
        for (DcData dcData : dcDataList) {
            wholesaleList.add(new OrganizationDto(
                    dcData.getDcCode(),
                    dcData.getDcName(),
                    dcData.getDcStatus(),
                    CommonEnum.OrganizationType.DC.getCode()
            ));

            // 配送中心下面的部门
            QueryWrapper<DepData> queryWrapperDep = new QueryWrapper<>();
            queryWrapperDep.eq("CLIENT", client);
            // 部门类型:30
            queryWrapperDep.eq("DEP_TYPE", CommonEnum.DepType.WHOLESALE_DEP.getCode());
            // 部门总部是 批发公司编码
            queryWrapperDep.eq("STO_CHAIN_HEAD", dcData.getDcCode());
            List<DepData> list = iDepDataService.list(queryWrapperDep);
            for (DepData depData : list) {
                wholesaleList.add(new OrganizationDto(
                        depData.getDepId(),
                        depData.getDepName(),
                        depData.getDepStatus(),
                        CommonEnum.OrganizationType.DEP.getCode()
                ));
            }
        }
        return wholesaleList;
    }

    /**
     * 部门
     */
    private List<OrganizationDto> getDepList(String client) {
        List<OrganizationDto> depList = new ArrayList<>();
        QueryWrapper<DepData> queryWrapperDep = new QueryWrapper<>();
        queryWrapperDep.eq("CLIENT", client);
        //部门类型:10
        queryWrapperDep.eq("DEP_TYPE", CommonEnum.DepType.FR_DEP.getCode());
        List<DepData> list = iDepDataService.list(queryWrapperDep);
        for (DepData depData : list) {
            depList.add(new OrganizationDto(
                    depData.getDepId(),
                    depData.getDepName(),
                    depData.getDepStatus(),
                    CommonEnum.OrganizationType.DEP.getCode()));
        }
        return depList;
    }

    /**
     * 仓库
     */
    private List<OrganizationDto> getWarehouseList(String client) {
        List<OrganizationDto> warehouseList = new ArrayList<>();
        QueryWrapper<DepData> queryWrapperWarehouse = new QueryWrapper<>();
        queryWrapperWarehouse.eq("CLIENT", client);
        //部门类型:40
        queryWrapperWarehouse.eq("DEP_TYPE", CommonEnum.DepType.WAREHOUSE_DEP.getCode());
        List<DepData> list = iDepDataService.list(queryWrapperWarehouse);
        for (DepData depData : list) {
            warehouseList.add(new OrganizationDto(
                    depData.getDepId(),
                    depData.getDepName(),
                    depData.getDepStatus(),
                    CommonEnum.OrganizationType.DEP.getCode()));
        }
        return warehouseList;
    }


}
