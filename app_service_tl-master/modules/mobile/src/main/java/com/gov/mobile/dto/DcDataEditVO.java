package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DcDataEditVO {

    @NotBlank(message = "DC编码不能为空")
    private String dcCode;

    @NotBlank(message = "DC名称不能为空")
    private String dcName;

//    @NotBlank(message = "助记码不能为空")
    private String dcPym;

    @NotBlank(message = "DC状态不能为空 0-正常，1-停用")
    private String dcStatus;

//    @NotBlank(message = "虚拟仓标记不能为空 0-否，1-是")
    private String dcInvent;


//    @NotBlank(message = "部门负责人ID不能为空")
    private String dcHeadId;

//    @NotBlank(message = "部门负责人姓名不能为空")
    private String dcHeadNam;

    //    @NotBlank(message = "纳税主体不能为空")
    private String dcTaxSubject;

    /**
     * DC简称
     */
    private String dcShortName;

    /**
     * DC地址
     */
    private String dcAdd;

    /**
     * DC电话
     */
    private String dcTel;

    /**
     * 连锁总部
     */
    private String dcChainHead;

    /**
     * 配送平均天数
     */
    private String dcDeliveryDays;

    /**
     * 统一社会信用代码
     */
    private String dcNo;

    /**
     * 法人/负责人
     */
    private String dcLegalPerson;

    /**
     * 质量负责人
     */
    private String dcQua;

    /**
     * 法人/负责人手机
     */
    private String dcLegalPhone;

    /**
     * 质量负责人手机
     */
    private String dcQuaPhone;

}
