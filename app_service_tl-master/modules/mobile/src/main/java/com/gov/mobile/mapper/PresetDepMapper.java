package com.gov.mobile.mapper;

import com.gov.mobile.entity.PresetDep;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-30
 */
public interface PresetDepMapper extends BaseMapper<PresetDep> {

}
