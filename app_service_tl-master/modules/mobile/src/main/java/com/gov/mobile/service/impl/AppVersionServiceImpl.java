package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.entity.AppVersion;
import com.gov.mobile.mapper.AppVersionMapper;
import com.gov.mobile.service.IAppVersionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Service
public class AppVersionServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements IAppVersionService {

    @Resource
    private AppVersionMapper appVersionMapper;

    /**
     * APP最新版本
     *
     * @param type 平台类型 1:android 2:ios
     * @return
     */
    @Override
    public Result getAppLastVersion(Integer type) {
        AppVersion appLastVersion = appVersionMapper.getAppLastVersion(type, Constants.AppReleaseFlag.RELEASE_YES);
        return ResultUtil.success(appLastVersion);
    }

}
