package com.gov.mobile.service.workWeixin.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.workWeixin.WorkwechatConfDto;
import com.gov.mobile.entity.WorkwechatConf;
import com.gov.mobile.mapper.WorkwechatConfMapper;
import com.gov.mobile.service.workWeixin.IWorkwechatConfService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.18
 */
@Service
public class WorkwechatConfServiceImpl extends ServiceImpl<WorkwechatConfMapper, WorkwechatConf> implements IWorkwechatConfService {

    /**
     * 列表
     *
     * @param pageSize
     * @param pageNum
     * @param client
     * @param gwcName
     * @return
     */
    @Override
    public Result workwechatConfList(Integer pageSize, Integer pageNum, String client, String gwcName, String gwcId) {
        // 分页
        Page<WorkwechatConfDto> page = new Page<WorkwechatConfDto>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        IPage<WorkwechatConfDto> iPage = baseMapper.workwechatConfList(page, client, gwcName, gwcId);
        return ResultUtil.success(iPage);
    }

    /**
     * 新增
     *
     * @param workwechatConf
     * @return
     */
    @Override
    public Result addWorkwechatConf(WorkwechatConf workwechatConf) {
        // 加盟商
        if (StringUtils.isBlank(workwechatConf.getClient())) {
            throw new CustomResultException("加盟商不能为空");
        }
        // 企业微信名
        if (StringUtils.isBlank(workwechatConf.getGwcName())) {
            throw new CustomResultException("企业微信名不能为空");
        }
        // 应用secret
        if (StringUtils.isBlank(workwechatConf.getGwcAppSecret())) {
            throw new CustomResultException("应用secret不能为空");
        }
        // 客户secret
        if (StringUtils.isBlank(workwechatConf.getGwcAppSecret())) {
            throw new CustomResultException("客户secret不能为空");
        }
        baseMapper.insert(workwechatConf);
        return ResultUtil.success();
    }

    /**
     * 编辑
     *
     * @param workwechatConf
     * @return
     */
    @Override
    public Result editWorkwechatConf(WorkwechatConfDto workwechatConf) {
        baseMapper.updateByOldId(workwechatConf);
        return ResultUtil.success();
    }

    /**
     * 删除
     *
     * @param workwechatConf
     * @return
     */
    @Override
    public Result deleteWorkwechatConf(WorkwechatConf workwechatConf) {
        baseMapper.delete(new LambdaQueryWrapper<WorkwechatConf>().eq(WorkwechatConf::getGwcId, workwechatConf.getGwcId()));
        return ResultUtil.success();
    }
}
