package com.gov.mobile.mapper;

import com.gov.mobile.entity.CompadmWms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
public interface CompadmWmsMapper extends BaseMapper<CompadmWms> {

}
