package com.gov.mobile.controller;

import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.EnumUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.SaveDepVO;
import com.gov.mobile.dto.UpdateDepVO;
import com.gov.mobile.service.IDepDataService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-29
 */
@RestController
@RequestMapping("dep")
public class DepDataController {

    @Resource
    private IDepDataService depDataService;

    @ApiOperation(value = "部门详情")
    @GetMapping("getDepInfo")
        public Result getDep(@RequestParam(name = "depId", required = true) String depId) {
        return ResultUtil.success(depDataService.getDep(depId));
    }

    @ApiOperation(value = "新增部门")
    @PostMapping("saveDep")
    public Result saveDep(@Valid @RequestBody SaveDepVO saveDepVO) {
        // 参数校验
        Boolean contains = EnumUtils.contains(saveDepVO.getDepType(), CommonEnum.DepType.class);
        if (!contains) {
            return ResultUtil.error(ResultEnum.E0013);
        }
        // 连锁类型时,连锁总店不能为空
        if (CommonEnum.DepType.CHAIN_DEP.getCode().equals(saveDepVO.getDepType()) && StringUtils.isEmpty(saveDepVO.getStoChainHead())) {
            return ResultUtil.error(ResultEnum.E0013);
        }
        // 只有在类型为仓库时,部门代码可为空
        if (!CommonEnum.DepType.WAREHOUSE_DEP.getCode().equals(saveDepVO.getDepType()) && StringUtils.isEmpty(saveDepVO.getDepCls())) {
            return ResultUtil.error(ResultEnum.E0013);
        }
        // 若传入的类型为仓库 设置公司代码为17. 其它类型,代码前端有传入
        if (CommonEnum.DepType.WAREHOUSE_DEP.getCode().equals(saveDepVO.getDepType())) {
            saveDepVO.setDepCls(CommonEnum.DepCls.WAREHOUSE_DLS.getCode());
        }
        depDataService.saveDep(saveDepVO);
        return ResultUtil.success();
    }

    @ApiOperation(value = "部门编辑")
    @PostMapping("updateDep")
    public Result updateDep(@Valid @RequestBody UpdateDepVO updateDepVO) {
        depDataService.updateDep(updateDepVO);
        return ResultUtil.success();
    }

}

