package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.mobile.entity.WorkwechatMsgUser;
import com.gov.mobile.mapper.WorkwechatMsgUserMapper;
import com.gov.mobile.service.IWorkWechatMsgUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WorkWechatMsgUserServiceImpl extends ServiceImpl<WorkwechatMsgUserMapper, WorkwechatMsgUser> implements IWorkWechatMsgUserService {

}
