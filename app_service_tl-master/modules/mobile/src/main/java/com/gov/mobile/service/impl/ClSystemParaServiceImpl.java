package com.gov.mobile.service.impl;

import com.gov.mobile.entity.ClSystemPara;
import com.gov.mobile.mapper.ClSystemParaMapper;
import com.gov.mobile.service.IClSystemParaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司参数表 服务实现类
 * </p>
 *
 * @author sy
 * @since 2021-12-07
 */
@Service
public class ClSystemParaServiceImpl extends ServiceImpl<ClSystemParaMapper, ClSystemPara> implements IClSystemParaService {

}
