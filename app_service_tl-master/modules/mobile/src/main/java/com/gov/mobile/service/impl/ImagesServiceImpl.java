package com.gov.mobile.service.impl;

import com.gov.mobile.entity.Images;
import com.gov.mobile.mapper.ImagesMapper;
import com.gov.mobile.service.IImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-24
 */
@Service
public class ImagesServiceImpl extends ServiceImpl<ImagesMapper, Images> implements IImagesService {

}
