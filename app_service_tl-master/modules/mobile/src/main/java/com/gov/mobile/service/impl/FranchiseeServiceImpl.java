package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CommonEnum;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.mapper.FranchiseeMapper;
import com.gov.mobile.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
@Service
public class FranchiseeServiceImpl extends ServiceImpl<FranchiseeMapper, Franchisee> implements IFranchiseeService {

    @Resource
    private IUserDataService userDataService;
    @Resource
    private IStoreDataService storeDataService;
    @Resource
    private ICompadmService compadmService;
    @Resource
    private IDcDataService dcDataService;
    @Resource
    private IDepDataService depDataService;
    @Resource
    private ICompadmWmsService compadmWmsService;

    /**
     * 获取当前加盟商详情
     */
    @Override
    public FranchiseeDTO getFranchisee() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        QueryWrapper<Franchisee> wrapper = new QueryWrapper<>();
        wrapper.eq("CLIENT", currentUser.getClient());
        Franchisee franchisee = this.getOne(wrapper);
        FranchiseeDTO franchiseeDTO = new FranchiseeDTO();
        BeanUtils.copyProperties(franchisee, franchiseeDTO);
        return franchiseeDTO;
    }

    /**
     * 加盟商组织获取
     */
    @Override
    public FranchiseeListDTO getFranchiseeOrg() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        FranchiseeListDTO franchiseeListDTO = new FranchiseeListDTO();

        // 门店(单体门店,1,3,4)
        List<OrganizationDto> storeList = new ArrayList<>();
        List<StoreData> storeListByClient = storeDataService.getStoreListByClient(currentUser.getClient());
        for (StoreData storeData : storeListByClient) {
            storeList.add(new OrganizationDto(
                    storeData.getStoCode(),
                    storeData.getStoName(),
                    storeData.getStoStatus(),
                    CommonEnum.OrganizationType.STORE.getCode()
            ));
        }

        // 连锁总店
        List<OrganizationDto> compadmList = new ArrayList<>();
        List<Compadm> compadmListByClient = compadmService.getCompadmListByClient(currentUser.getClient());
        for (Compadm compadm : compadmListByClient) {
            compadmList.add(new OrganizationDto(
                    compadm.getCompadmId(),
                    compadm.getCompadmName(),
                    CommonEnum.OrganizationType.COMPADM.getCode()));
        }

        List<OrganizationDto> compadmWmsList = new ArrayList<>();
        List<CompadmWms> compadmWmsListByClient = compadmWmsService.getCompadmWmsListByClient(currentUser.getClient());
        for (CompadmWms compadmWms : compadmWmsListByClient) {
            compadmWmsList.add(new OrganizationDto(
                    compadmWms.getCompadmId(),
                    compadmWms.getCompadmName(),
                    compadmWms.getCompadmStatus(),
                    CommonEnum.OrganizationType.COMPADMWMS.getCode()));
        }
        // 批发公司
//        List<OrganizationDto> wholesaleList = new ArrayList<>();
//        List<DcData> wholesaleListByClient = dcDataService.getWholesaleListByClient(currentUser.getClient());
//        for (DcData dcData : wholesaleListByClient) {
//            wholesaleList.add(new OrganizationDto(
//                    dcData.getDcCode(),
//                    dcData.getDcName(),
//                    dcData.getDcStatus(),
//                    CommonEnum.OrganizationType.DC.getCode()));
//        }

        // 部门
//        List<OrganizationDepDTO> depList = new ArrayList<>();
//        List<DepData> depListByClient = depDataService.getDepList(currentUser.getClient(), CommonEnum.DepType.FR_DEP.getCode());
//        for (DepData depData : depListByClient) {
//            depList.add(new OrganizationDepDTO(
//                    depData.getDepId(),
//                    depData.getDepName(),
//                    depData.getDepStatus(),
//                    CommonEnum.OrganizationType.DEP.getCode(),
//                    depData.getDepHeadId()));
//        }

        // 部门
        List<OrganizationDepDTO> warehouseList = new ArrayList<>();
        List<DepData> warehouseListByClient = depDataService.getDepList(currentUser.getClient(), CommonEnum.DepType.WAREHOUSE_DEP.getCode());
        for (DepData depData : warehouseListByClient) {
            warehouseList.add(new OrganizationDepDTO(
                    depData.getDepId(),
                    depData.getDepName(),
                    depData.getDepStatus(),
                    CommonEnum.OrganizationType.DEP.getCode(),
                    depData.getDepHeadId()));
        }

        franchiseeListDTO.setStoreList(storeList);
        franchiseeListDTO.setCompadmList(compadmList);
        franchiseeListDTO.setWholesaleList(compadmWmsList);
        franchiseeListDTO.setDepList(warehouseList);
//        franchiseeListDTO.setWarehouseList(warehouseList);
        return franchiseeListDTO;
    }



}














