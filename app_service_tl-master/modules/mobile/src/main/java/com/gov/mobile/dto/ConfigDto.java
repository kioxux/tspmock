package com.gov.mobile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "获取公众号的配置")
public class ConfigDto {

    @NotBlank(message = "加盟商id不能为空")
    @ApiModelProperty(value = "加盟商id")
    private String client;

    @NotBlank(message = "对象编码能为空")
    @ApiModelProperty(value = "对象编码")
    private String stoCode;

}
