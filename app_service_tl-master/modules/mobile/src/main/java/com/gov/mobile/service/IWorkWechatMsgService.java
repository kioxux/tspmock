package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.WorkWechatMsgDTO;
import com.gov.mobile.dto.WorkWechatMsgPageDTO;
import com.gov.mobile.entity.WorkwechatMsg;
import com.gov.mybatis.SuperService;

import java.util.List;

public interface IWorkWechatMsgService extends SuperService<WorkwechatMsg> {

    /**
     * 添加企业微信消息
     *
     * @param msgDTO
     * @return
     */
    Result addMsg(WorkWechatMsgDTO msgDTO);

    /**
     * 定时发送消息
     */
    void sendMsg() throws Exception;

    /**
     * 分页查询
     *
     * @param workWechatMsgPageDTO
     * @return
     */
    Result queryList(WorkWechatMsgPageDTO workWechatMsgPageDTO);

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    Result deleteByIds(List<Integer> ids);

    /**
     * 获取消息详情
     *
     * @param msgId
     * @return
     */
    Result getMsgInfo(String msgId);

    /**
     * 获取消息用户列表
     *
     * @param msgId 消息id
     * @return
     */
    Result getUserList(String msgId);

    /**
     * 更新消息信息
     *
     * @param msgDTO
     * @return
     */
    Result updateMsgInfo(WorkWechatMsgDTO msgDTO);
}
