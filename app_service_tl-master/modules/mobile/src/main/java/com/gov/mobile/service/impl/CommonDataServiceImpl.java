package com.gov.mobile.service.impl;

import com.gov.common.basic.StringUtils;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.entity.CommonData;
import com.gov.mobile.mapper.CommonDataMapper;
import com.gov.mobile.service.ICommonDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Service
public class CommonDataServiceImpl extends ServiceImpl<CommonDataMapper, CommonData> implements ICommonDataService {

    /**
     * 通用数据获取
     */
    @Override
    public Result getCommonQuery(String commonKey) {
        CommonData commonData = this.getById(commonKey);
        if (StringUtils.isEmpty(commonData)) {
            return ResultUtil.error(ResultEnum.E0013);
        }
        return ResultUtil.success(commonData);
    }
}
