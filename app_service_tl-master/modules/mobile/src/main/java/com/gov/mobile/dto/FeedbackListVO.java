package com.gov.mobile.dto;

import com.gov.common.entity.Pageable;
import lombok.Data;

@Data
public class FeedbackListVO extends Pageable {

    /**
     * 建议返回类型
     */
    private String type;

}
