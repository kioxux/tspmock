package com.gov.mobile.mapper;

import com.gov.mobile.entity.SmsTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-22
 */
public interface SmsTemplateMapper extends BaseMapper<SmsTemplate> {

}
