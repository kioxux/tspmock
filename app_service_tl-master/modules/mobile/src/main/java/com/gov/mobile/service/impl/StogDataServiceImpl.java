package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.mobile.dto.UserLoginModelClient;
import com.gov.mobile.entity.StogData;
import com.gov.mobile.mapper.StogDataMapper;
import com.gov.mobile.service.IStogDataService;
import com.gov.mobile.service.IUserDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-07
 */
@Service
public class StogDataServiceImpl extends ServiceImpl<StogDataMapper, StogData> implements IStogDataService {

    @Resource
    private IUserDataService userDataService;

    @Override
    public List<StogData> getStogList() {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();

        QueryWrapper<StogData> queryWrapper = new QueryWrapper();
        queryWrapper.select("DISTINCT STOG_CODE, STOG_NAME");
        queryWrapper.eq("CLIENT", currentUser.getClient());
        List<StogData> list = this.list(queryWrapper);
        return list;
    }

    @Override
    public StogData getNameByCode(String client, String stogCode) {
        QueryWrapper<StogData> queryWrapper = new QueryWrapper();
        queryWrapper.eq("CLIENT", client);
        queryWrapper.eq("STOG_CODE", stogCode);
        return this.getOne(queryWrapper);
    }
}
