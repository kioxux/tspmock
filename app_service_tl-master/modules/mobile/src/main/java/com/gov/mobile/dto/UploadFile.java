package com.gov.mobile.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.24
 */
@Data
public class UploadFile {

    private String type;

    private MultipartFile file;

}
