package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.ResultEnum;
import com.gov.common.userutil.UserUtils;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.AuthconfiData;
import com.gov.mobile.entity.DcData;
import com.gov.mobile.entity.DepData;
import com.gov.mobile.entity.UserData;
import com.gov.mobile.feign.OperateFeign;
import com.gov.mobile.feign.dto.Message.MessageParams;
import com.gov.mobile.mapper.AuthconfiDataMapper;
import com.gov.mobile.mapper.DcDataMapper;
import com.gov.mobile.service.IDcDataService;
import com.gov.mobile.service.IDepDataService;
import com.gov.mobile.service.ISmsTemplateService;
import com.gov.mobile.service.IUserDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Slf4j
@Service
public class DcDataServiceImpl extends ServiceImpl<DcDataMapper, DcData> implements IDcDataService {


    @Resource
    private IUserDataService userDataService;
    @Resource
    private IDepDataService depDataService;
    @Resource
    private AuthconfiDataMapper authconfiDataMapper;
    @Resource
    private DcDataMapper dcDataMapper;
    @Resource
    private ISmsTemplateService smsTemplateService;

    @Resource
    private OperateFeign operateFeign;

    /**
     * 对应加盟商下面的批发公司
     *
     * @param client 加盟商编号
     * @return
     */
    @Override
    public List<DcData> getWholesaleListByClient(String client) {
        QueryWrapper<DcData> queryWrapperDc = new QueryWrapper<>();
        queryWrapperDc.eq("CLIENT", client);
        // 类型
        queryWrapperDc.eq("DC_TYPE", CommonEnum.DCType.WHOLESALE_DC.getCode());
        // 有批发资质
        queryWrapperDc.eq("DC_WHOLESALE", CommonEnum.DC_WHOLESALE.DC_WHOLESALE_YES.getCode());
        return this.list(queryWrapperDc);
    }


    /**
     * 通过加盟商+连锁总部 获取配送中心
     *
     * @param client    加盟商
     * @param chainHead 连锁总部
     * @return
     */
    @Override
    public List<DcData> getDcListByClientAndChainHead(String client, String chainHead) {
        QueryWrapper<DcData> queryWrapperDc = new QueryWrapper<>();
        queryWrapperDc.eq("CLIENT", client);
        queryWrapperDc.eq("DC_CHAIN_HEAD", chainHead);
        queryWrapperDc.eq("DC_WHOLESALE", CommonEnum.DC_WHOLESALE.DC_WHOLESALE_NO.getCode());
        return this.list(queryWrapperDc);
    }

    /**
     * @param client 加盟商
     * @param dcCode DC编码
     * @return
     */
    private DcData getDcListByClientAndCode(String client, String dcCode) {
        QueryWrapper<DcData> queryWrapperDc = new QueryWrapper<>();
        queryWrapperDc.eq("CLIENT", client);
        queryWrapperDc.eq("DC_CODE", dcCode);
        return this.getOne(queryWrapperDc);
    }

    /**
     * 配送中心详情
     *
     * @param dcCode DC编码
     * @return
     */
    @Override
    public DcDataDTO getDcInfo(String dcCode) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        DcData dcListByClientAndCode = getDcListByClientAndCode(currentUser.getClient(), dcCode);
        if (StringUtils.isEmpty(dcListByClientAndCode)) {
            throw new CustomResultException(ResultEnum.E0013);
        }

        DcDataDTO dcDataDTO = new DcDataDTO();
        BeanUtils.copyProperties(dcListByClientAndCode, dcDataDTO);

        // 法人/负责人姓名
        UserData user = userDataService.getUserByClientAndUserId(currentUser.getClient(), dcDataDTO.getDcLegalPerson());
        if (StringUtils.isNotEmpty(user)) {
            dcDataDTO.setDcLegalPersonName(user.getUserNam());
            dcDataDTO.setDcLegalName(user.getUserNam());
            dcDataDTO.setDcLegalPerson(user.getUserId());
            dcDataDTO.setDcLegalPhone(user.getUserTel());
        }
        if (StringUtils.isNotEmpty(dcDataDTO.getDcQua())) {
            // 质量负责人姓名
            user = userDataService.getUserByClientAndUserId(currentUser.getClient(), dcDataDTO.getDcQua());
            if (StringUtils.isNotEmpty(user)) {
                dcDataDTO.setDcQuaName(user.getUserNam());
                dcDataDTO.setDcQua(user.getUserId());
                dcDataDTO.setDcQuaPhone(user.getUserTel());
            }
        }

        // 部门
        List<OrganizationDepDTO> depList = new ArrayList<>();
        List<DepData> depListByClient = depDataService.getDepList(currentUser.getClient(), CommonEnum.DepType.WHOLESALE_DEP.getCode(), dcCode);
        for (DepData depData : depListByClient) {
            depList.add(new OrganizationDepDTO(
                    depData.getDepId(),
                    depData.getDepName(),
                    depData.getDepStatus(),
                    CommonEnum.OrganizationType.DEP.getCode(),
                    depData.getDepHeadId()));
        }
        dcDataDTO.setDepList(depList);
        return dcDataDTO;
    }

    /**
     * 配送中心新增
     *
     * @param dcDataAddVO 新增入参
     */
    @Override
    public void addDc(DcDataAddVO dcDataAddVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        dcDataAddVO.setClient(currentUser.getClient());
        String userLeaderId="";
        String userQuaId="";
        DcData dcData = new DcData();
        BeanUtils.copyProperties(dcDataAddVO, dcData);
        if (StringUtils.isNotEmpty(dcDataAddVO.getDcChainHead())) {
            // 若 连锁中心不空，则纳税主体也不空
            if (StringUtils.isEmpty(dcDataAddVO.getDcTaxSubject())) {
                throw new CustomResultException(ResultEnum.E0025);
            }
            // 没有批发资质
            dcData.setDcWholesale(CommonEnum.DC_WHOLESALE.DC_WHOLESALE_NO.getCode());
            // DC类型 连锁公司
            dcData.setDcType(CommonEnum.DCType.CHAIN_DC.getCode());
        } else {
            // 统一社会信用代码不能为空
            if (StringUtils.isEmpty(dcDataAddVO.getDcNo())) {
                throw new CustomResultException(ResultEnum.E0026);
            }
            // 法人/负责人不能为空
            if (StringUtils.isEmpty(dcDataAddVO.getDcLegalPhone())) {
                throw new CustomResultException(ResultEnum.E0027);
            }
//            // 质量负责人不能为空
//            if (StringUtils.isEmpty(dcDataAddVO.getDcQua())) {
//                throw new CustomResultException(ResultEnum.E0028);
//            }
            // 有批发资质
            dcData.setDcWholesale(CommonEnum.DC_WHOLESALE.DC_WHOLESALE_YES.getCode());
            // DC类型 批发公司
            dcData.setDcType(CommonEnum.DCType.WHOLESALE_DC.getCode());
        }
        dcData.setDcCreDate(DateUtils.getCurrentDateStrYYMMDD());
        dcData.setDcCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        dcData.setDcCreId(currentUser.getUserId());
        dcData.setClient(currentUser.getClient());
        // 设置主键 数子编码加一
        String dcCode = "1000";
        DcData maxNumDc = dcDataMapper.getMaxNumDcCode(currentUser.getClient());
        if (StringUtils.isNotEmpty(maxNumDc)) {
            int dcCodeNum = Integer.valueOf(maxNumDc.getDcCode()) + 1;
            dcCode = String.valueOf(dcCodeNum);
        }
        dcData.setDcCode(dcCode);
        dcDataAddVO.setDcCode(dcCode);
        if (StringUtils.isNotEmpty(dcDataAddVO.getDcQuaPhone())) {
            //如果店长和质量负责人是同一人
            if (dcDataAddVO.getDcLegalPhone().equals(dcDataAddVO.getDcQuaPhone())) {
                userLeaderId = getUserId(currentUser, dcDataAddVO, null);

            } else {
                userLeaderId = getUserId(currentUser, dcDataAddVO, CommonEnum.UserType.STATUS_LEADER.getCode());
                userQuaId = getUserId(currentUser, dcDataAddVO, CommonEnum.UserType.STATUS_QUA.getCode());
            }
        }else {
            userLeaderId = getUserId(currentUser, dcDataAddVO, null);
        }
        dcData.setDcLegalPerson(userLeaderId);
        dcData.setDcQua(userQuaId);
        this.save(dcData);


        try {
            MessageParams messageParams = new MessageParams();
            messageParams.setClient(dcData.getClient());
            messageParams.setUserId(userLeaderId);
            messageParams.setContentParmas(new ArrayList<String>() {{
                add(dcData.getDcName());
            }});
            messageParams.setId(CommonEnum.gmtId.MSG00003.getCode());
            operateFeign.sendMessage(messageParams);
            log.info("新增配送中心推送成功");
        } catch (Exception e) {
            log.error("新增配送中心推送时异常", e);
        }

//        this.saveAuthconfiData(dcData);
    }

    /**
     * 配送中心编辑
     *
     * @param dataEditVO 编辑入参
     */
    @Override
    public void editDc(DcDataEditVO dataEditVO) {
        UserLoginModelClient currentUser = userDataService.getCurrentUser();
        DcData dcData = this.getDcListByClientAndCode(currentUser.getClient(), dataEditVO.getDcCode());
        if (StringUtils.isEmpty(dcData)) {
            throw new CustomResultException(ResultEnum.E0013);
        }
        String userLeaderId="";
        String userQuaId="";
        if (StringUtils.isNotEmpty(dataEditVO.getDcChainHead())) {
            // 若 连锁中心不空，则纳税主体也不空
            if (StringUtils.isEmpty(dataEditVO.getDcTaxSubject())) {
                throw new CustomResultException(ResultEnum.E0025);
            }
            // 没有批发资质
            dcData.setDcWholesale(CommonEnum.DC_WHOLESALE.DC_WHOLESALE_NO.getCode());
            // DC类型 连锁公司
            dcData.setDcType(CommonEnum.DCType.CHAIN_DC.getCode());
        } else {
            // 统一社会信用代码不能为空
            if (StringUtils.isEmpty(dataEditVO.getDcNo())) {
                throw new CustomResultException(ResultEnum.E0026);
            }
            // 法人/负责人不能为空
            if (StringUtils.isEmpty(dataEditVO.getDcLegalPhone())) {
                throw new CustomResultException(ResultEnum.E0027);
            }
//            // 质量负责人不能为空
//            if (StringUtils.isEmpty(dataEditVO.getDcQua())) {
//                throw new CustomResultException(ResultEnum.E0028);
//            }
            // 有批发资质
            dcData.setDcWholesale(CommonEnum.DC_WHOLESALE.DC_WHOLESALE_YES.getCode());
            // DC类型 批发公司
            dcData.setDcType(CommonEnum.DCType.WHOLESALE_DC.getCode());
        }

        DcData upDcData = new DcData();
        BeanUtils.copyProperties(dataEditVO, upDcData);
        upDcData.setDcModiDate(DateUtils.getCurrentDateStrYYMMDD());
        upDcData.setDcModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        upDcData.setDcModiId(currentUser.getUserId());
        DcDataAddVO dcDataAddVO = new DcDataAddVO();
        BeanUtils.copyProperties(dataEditVO, dcDataAddVO);
        dcDataAddVO.setClient(currentUser.getClient());
        if (StringUtils.isNotEmpty(dataEditVO.getDcQuaPhone())) {
            //如果店长和质量负责人是同一人
            if (dataEditVO.getDcLegalPhone().equals(dataEditVO.getDcQuaPhone())) {
                userLeaderId = getUserId(currentUser, dcDataAddVO, null);

            } else {
                userLeaderId = getUserId(currentUser, dcDataAddVO, CommonEnum.UserType.STATUS_LEADER.getCode());
                userQuaId = getUserId(currentUser, dcDataAddVO, CommonEnum.UserType.STATUS_QUA.getCode());
            }
        }else {
            userLeaderId = getUserId(currentUser, dcDataAddVO, null);
        }
        upDcData.setDcLegalPerson(userLeaderId);
        upDcData.setDcQua(userQuaId);
        UpdateWrapper<DcData> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("CLIENT", currentUser.getClient());
        updateWrapper.eq("DC_CODE", dataEditVO.getDcCode());
        this.update(upDcData, updateWrapper);
//        if (!dcData.getDcHeadId().equals(upDcData.getDcHeadId())) {
//            this.updateAuthconfiData(dcData, dataEditVO);
//        }

        if(StringUtils.isNotEmpty(userLeaderId) && userLeaderId.equals(dcData.getDcLegalPerson())){
            try {
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(dcData.getClient());
                messageParams.setUserId(userLeaderId);
                messageParams.setContentParmas(new ArrayList<String>() {{
                    add(dcData.getDcName());
                }});
                messageParams.setId(CommonEnum.gmtId.MSG00003.getCode());
                operateFeign.sendMessage(messageParams);
                log.info("編輯配送中心推送成功");
            } catch (Exception e) {
                log.error("编辑配送中心推送时异常", e);
            }
        }
    }

    /**
     * 判断手机号是否已经注册 如果存在直接拿编码 不存则在创建角色
     * @param currentUser
     * @param code
     * @return
     */
    @Transactional
    public String  getUserId(UserLoginModelClient currentUser, DcDataAddVO dcDataAddVO, String code) {
        String userId ="";
        UserData userData;
        if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
            userData = userDataService.getUserByClientAndPhone(currentUser.getClient(),dcDataAddVO.getDcQuaPhone());
        }else {
            userData = userDataService.getUserByClientAndPhone(currentUser.getClient(),dcDataAddVO.getDcLegalPhone());
        }

        String passWord =String.valueOf(new Random().nextInt(900000) + 100000);
        if(StringUtils.isEmpty(userData)){
            userData = new UserData();
            int userMax = this.userDataService.selectMaxId(currentUser.getClient());
            userData.setClient(currentUser.getClient());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(currentUser.getUserId());
            userData.setDepId(dcDataAddVO.getDcCode()); //部门 改为门店
            userData.setDepName(dcDataAddVO.getDcName());
            userData.setUserId(String.valueOf(userMax + 1));
            if(CommonEnum.UserType.STATUS_QUA.getCode().equals(code)){    //质量负责人
                userData.setUserNam(dcDataAddVO.getDcQuaName());
                userData.setUserTel(dcDataAddVO.getDcQuaPhone());
            }else {
                userData.setUserNam(dcDataAddVO.getDcLegalName());
                userData.setUserTel(dcDataAddVO.getDcLegalPhone());
            }
            userData.setUserPassword(UserUtils.encryptMD5(passWord));
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiDate(DateUtils.getCurrentDateStrYYMMDD());
            userData.setUserModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            userData.setUserModiId(currentUser.getUserId());
            userData.setUserCreId(currentUser.getUserId());
            this.userDataService.insert(userData);
            userId = String.valueOf(userMax + 1);
            dcDataAddVO.setDcLegalPerson(userId);
            //给店长发短信
            this.sendSms(dcDataAddVO,code,passWord);
        }else {
            userId = userData.getUserId();
//            userData.setDepId(storeDataAddVO.getStoCode()); //部门 改为门店
//            userData.setDepName(storeDataAddVO.getStoName());
            UpdateWrapper<UserData> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("CLIENT", currentUser.getClient());
            updateWrapper.eq("USER_ID", userId);
            this.userDataService.update(userData,updateWrapper);
            dcDataAddVO.setDcLegalPerson(userId);
            this.sendUpdateSms(dcDataAddVO);
        }
//        this.saveAuthconfiData(currentUser.getClient(),userData,code);
        return userId;
    }

    public void sendSms(DcDataAddVO dcDataAddVO,String code,String passWord) {
//        if(CommonEnum.UserType.STATUS_LEADER.getCode().equals(code)){
            Verification verification = new Verification(dcDataAddVO.getDcLegalPhone(),"2",dcDataAddVO.getDcName(),passWord);
            smsTemplateService.sendStoreRegisterSms(verification);
//        }
    }
//    /**
//     * 新增时权限分配
//     */
//    private void saveAuthconfiData(DcData dcData) {
//        AuthconfiData authconfiData = new AuthconfiData();
//        authconfiData.setClient(dcData.getClient());
//        authconfiData.setAuthGroupId(CommonEnum.AuthGroup.GAIA_WM_FZR.getCode());
//        authconfiData.setAuthGroupName(CommonEnum.AuthGroup.GAIA_WM_FZR.getMessage());
//        authconfiData.setAuthobjSite(dcData.getDcCode());
////        authconfiData.setAuthconfiUser(dcData.getDcHeadId());
//        authconfiDataMapper.insert(authconfiData);
//    }

    private void updateAuthconfiData(DcData exitDcData, DcDataEditVO dataEditVO) {

        // 删除旧的权限数据
        authconfiDataMapper.deleteByParam(CommonEnum.AuthGroup.GAIA_WM_FZR.getCode(), exitDcData.getDcHeadId(), exitDcData.getClient());

        // 添加新的权限数据
        List<AuthconfiData> list = new ArrayList<>();

        // 本DC
        AuthconfiData auth = new AuthconfiData();
        auth.setClient(exitDcData.getClient());
        auth.setAuthGroupId(CommonEnum.AuthGroup.GAIA_WM_FZR.getCode());
        auth.setAuthGroupName(CommonEnum.AuthGroup.GAIA_WM_FZR.getMessage());
        auth.setAuthobjSite(exitDcData.getDcCode());
        auth.setAuthconfiUser(dataEditVO.getDcHeadId());
        list.add(auth);

        // DC 下面的部门
        QueryWrapper<DepData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", exitDcData.getClient());
        queryWrapper.eq("STO_CHAIN_HEAD", exitDcData.getDcCode());
        List<DepData> depDataList = depDataService.list(queryWrapper);
        for (DepData depData : depDataList) {
            AuthconfiData authConfig = new AuthconfiData();
            authConfig.setClient(depData.getClient());
            authConfig.setAuthGroupId(CommonEnum.AuthGroup.GAIA_WM_FZR.getCode());
            authConfig.setAuthGroupName(CommonEnum.AuthGroup.GAIA_WM_FZR.getMessage());
            authConfig.setAuthobjSite(depData.getDepId());
            authConfig.setAuthconfiUser(dataEditVO.getDcHeadId());
            list.add(authConfig);
        }

        authconfiDataMapper.insertIgnoreList(list);
    }

    public void sendUpdateSms(DcDataAddVO dcDataAddVO) {
//        if(CommonEnum.UserType.STATUS_LEADER.getCode().equals(code)){
        UserData userData = userDataService.getUserByClientAndUserId(dcDataAddVO.getClient(),dcDataAddVO.getDcLegalPerson());
        Verification verification = new Verification(userData.getUserTel(),"2",dcDataAddVO.getDcName(),null);
        smsTemplateService.sendUpdateStoreRegisterSms(verification);
//        }
    }
}
