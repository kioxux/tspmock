package com.gov.mobile.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.*;
import com.gov.mobile.service.ICompadmService;
import com.gov.mobile.service.IDcDataService;
import com.gov.mobile.service.IFranchiseeService;
import com.gov.mobile.service.IStoreDataService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-07-06
 */
@RestController
@RequestMapping("/franchisee/")
public class FranchiseeController {

    @Resource
    private IFranchiseeService franchiseeService;
    @Resource
    private ICompadmService compadmService;
    @Resource
    private IDcDataService dcDataService;
    @Resource
    private IStoreDataService storeDataService;


    @GetMapping("getFranchisee")
    @ApiOperation(value = "加盟商获取")
    public Result getFranchisee() {
        return ResultUtil.success(franchiseeService.getFranchisee());
    }


    @GetMapping("getFranchiseeOrg")
    @ApiOperation(value = "加盟商组织获取")
    public Result getFranchiseeOrg() {
        return ResultUtil.success(franchiseeService.getFranchiseeOrg());
    }


    @GetMapping("getCompadmInfo")
    @ApiOperation(value = "连锁公司详情")
    public Result getCompadmInfo(@RequestParam(name = "compadmId", required = true) String compadmId) {
        return ResultUtil.success(compadmService.getCompadmInfo(compadmId));
    }

    @PostMapping("addCompadm")
    @ApiOperation(value = "连锁公司新增")
    public Result addCompadm(@RequestBody @Valid CompadmVO compadmVO) {
        compadmService.addCompadm(compadmVO);
        return ResultUtil.success();
    }

    @PostMapping("editCompadm")
    @ApiOperation(value = "连锁公司编辑")
    public Result editCompadm(@RequestBody @Valid CompadmEditVO compadmEditVO) {
        compadmService.editCompadm(compadmEditVO);
        return ResultUtil.success();
    }


    @GetMapping("getDcInfo")
    @ApiOperation(value = "配送中心详情")
    public Result getDcInfo(@RequestParam(name = "dcCode", required = true) String dcCode) {
        return ResultUtil.success(dcDataService.getDcInfo(dcCode));
    }

    @PostMapping("addDc")
    @ApiOperation(value = "配送中心新增")
    public Result addDc(@RequestBody @Valid DcDataAddVO dcDataAddVO) {
        dcDataService.addDc(dcDataAddVO);
        return ResultUtil.success();
    }

    @PostMapping("editDc")
    @ApiOperation(value = "配送中心编辑")
    public Result editDc(@RequestBody @Valid DcDataEditVO dataEditVO) {
        dcDataService.editDc(dataEditVO);
        return ResultUtil.success();
    }


    @GetMapping("getStoreCode")
    @ApiOperation(value = "门店编码获取")
    public Result getStoreCode() {
        return ResultUtil.success(storeDataService.getStoreCode());
    }

    @GetMapping("getStoreInfo")
    @ApiOperation(value = "门店详情")
    public Result getStoreInfo(@RequestParam(name = "stoCode", required = true) String stoCode) {
        return ResultUtil.success(storeDataService.getStoreInfo(stoCode));
    }

    @PostMapping("addStore")
    @ApiOperation(value = "门店新增")
    public Result addStore(@RequestBody @Valid StoreDataAddVO storeDataAddVO) {
        storeDataService.addStore(storeDataAddVO);
        return ResultUtil.success();
    }

    @PostMapping("editStore")
    @ApiOperation(value = "门店编辑")
    public Result editStore(@RequestBody @Valid StoreDataEditVO storeDataEditVO) {
        storeDataService.editStore(storeDataEditVO);
        return ResultUtil.success();
    }

    @Log("选择纳税主体")
    @ApiOperation("选择纳税主体")
    @GetMapping("getTaxSubject")
    public Result getTaxSubject(@RequestParam("chainHead") String chainHead) {
        return ResultUtil.success(compadmService.getTaxSubject(chainHead));
    }

}

