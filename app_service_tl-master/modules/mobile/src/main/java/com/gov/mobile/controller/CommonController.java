package com.gov.mobile.controller;


import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.mobile.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
@Api(tags = "共通")
@RestController
@RequestMapping("common")
public class CommonController {

    @Resource
    private ICommonDataService iCommonDataService;

    @Resource
    private IProductClassService productClassService;

    @Resource
    private IProductComponentService productComponentService;

    @Resource
    private IStoreDataService storeDataService;

    @Resource
    private ISdYxmclassService sdYxmclassService;

    @GetMapping("getCommonQuery")
    @ApiOperation(value = "通用数据获取")
    public Result getCommonQuery(@RequestParam(name = "commonKey", required = true) String commonKey) {
        return iCommonDataService.getCommonQuery(commonKey);
    }

    @GetMapping("getProductClassList")
    @ApiOperation(value = "商品分类列表")
    public Result getProductClassList() {
        return productClassService.selectProductClassList();
    }


    @GetMapping("getProductComponentList")
    @ApiOperation(value = "商品成分列表")
    public Result getProductComponentList() {
        return productComponentService.selectProductComponentList();
    }

    @Log("商品组及商品集合")
    @ApiOperation(value = "商品组及商品集合")
    @GetMapping("getGroupAndProductList")
    public Result getGroupAndProductList(@RequestParam("gsmStore") String gsmStore) {
        return sdYxmclassService.selectGroupAndProductList(gsmStore);
    }

    @Log("门店组及门店集合")
    @ApiOperation(value = "门店组及门店集合")
    @GetMapping("getGroupStoreList")
    public Result getGroupStoreList() {
        return storeDataService.selectGroupStoreList();
    }




}

