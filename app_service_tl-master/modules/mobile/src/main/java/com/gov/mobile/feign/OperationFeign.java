package com.gov.mobile.feign;

import com.gov.common.response.Result;
import com.gov.mobile.feign.dto.JsonResult;
import com.gov.mobile.feign.dto.Message.MessageParams;
import com.gov.mobile.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Component
@FeignClient(value = "gys-operation", fallback = AuthFeignFallback.class)
public interface OperationFeign {

    /**
     * 会员新增/修改
     *
     * @param map t
     * @return
     */
    @PostMapping({"/sync/memberSync"})
    Result memberSync(@RequestBody Map map);

}
