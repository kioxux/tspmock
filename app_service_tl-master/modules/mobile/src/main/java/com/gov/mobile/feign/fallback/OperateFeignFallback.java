package com.gov.mobile.feign.fallback;

import com.gov.mobile.feign.OperateFeign;
import com.gov.mobile.feign.dto.JsonResult;
import com.gov.mobile.feign.dto.Message.MessageParams;
import org.springframework.stereotype.Component;

@Component
public class OperateFeignFallback implements OperateFeign {

    @Override
    public JsonResult sendMessage(MessageParams messageParams) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }
}
