package com.gov.mobile.job;

import com.gov.mobile.service.IEnterpriseWechatService;
import com.gov.mobile.service.IWorkWechatMsgService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class TaskJob {

    @Resource
    private IEnterpriseWechatService enterpriseWechatService;

    @Resource
    private IWorkWechatMsgService workWechatMsgService;


    /**
     * 会员标签同步
     *
     * @param param 传入参数
     * @return
     */
    @XxlJob("syncTagHandler")
    public ReturnT<String> syncTag(String param) {
        XxlJobHelper.log("同步标签 定时任务开始");
        try {
            enterpriseWechatService.asyncTag();
        } catch (Exception e) {
            log.error("同步标签发生异常，异常消息：{}", e.getMessage());
        }
        XxlJobHelper.log("同步标签 定时任务结束");
        return ReturnT.SUCCESS;
    }

    /**
     * 企业微信推送
     *
     * @param param
     * @return
     */
    @SneakyThrows
    @XxlJob("weComSendHandler")
    public ReturnT<String> weComSend(String param) {
        XxlJobHelper.log("企业微信推送 定时任务开始 ");
        workWechatMsgService.sendMsg();
        XxlJobHelper.log("企业微信推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }
}
