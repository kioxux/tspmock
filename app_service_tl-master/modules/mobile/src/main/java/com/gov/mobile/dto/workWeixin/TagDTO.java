package com.gov.mobile.dto.workWeixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TagDTO {

    private String id;

    private String name;

    private Long create_time;

    private Integer order;

    private Boolean deleted;
}
