package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_MEMBER_LOGIN_LOG")
@ApiModel(value="MemberLoginLog对象", description="")
public class MemberLoginLog extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "加盟商")
    @TableField("CLIENT")
    private String client;

    @ApiModelProperty(value = "员工编号")
    @TableField("MEMBER_ID")
    private String memberId;

    @ApiModelProperty(value = "会员卡号")
    @TableField("MEMBER_CARD")
    private String memberCard;

    @ApiModelProperty(value = "门店")
    @TableField("STORE_CODE")
    private String storeCode;

    @ApiModelProperty(value = "openId")
    @TableField("OPEN_ID")
    private String openId;

    @ApiModelProperty(value = "登录凭证")
    @TableField("TOKEN")
    private String token;

    @ApiModelProperty(value = "登录平台")
    @TableField("PLATFORM")
    private String platform;

    @ApiModelProperty(value = "登录时间")
    @TableField("LOGINTIME")
    private String logintime;

    @ApiModelProperty(value = "创建日期")
    @TableField("USER_CRE_DATE")
    private String userCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("USER_CRE_TIME")
    private String userCreTime;


}
