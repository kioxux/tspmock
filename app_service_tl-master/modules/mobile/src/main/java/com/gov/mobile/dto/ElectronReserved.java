package com.gov.mobile.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.09.27
 */
@Data
public class ElectronReserved {

    @ApiModelProperty(value = "面值")
    private BigDecimal gsebAmt;

    @ApiModelProperty(value = "电子券活动号")
    private String gsebId;

    @ApiModelProperty(value = "发送数量")
    private String gseasQty;

    @ApiModelProperty(value = "电子券描述")
    private String gsebName;
}
