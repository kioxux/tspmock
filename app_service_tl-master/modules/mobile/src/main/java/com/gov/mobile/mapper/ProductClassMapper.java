package com.gov.mobile.mapper;

import com.gov.mobile.entity.ProductClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-24
 */
public interface ProductClassMapper extends BaseMapper<ProductClass> {

}
