package com.gov.mobile.mapper;

import com.gov.mobile.entity.SdMemberBasic;
import com.gov.mobile.entity.SdMemberCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-30
 */
public interface SdMemberCardMapper extends BaseMapper<SdMemberCard> {
    /**
     * 获取会员卡号
     */
    SdMemberCard getUserCardNumber(Map<Object, Object> map);

}
