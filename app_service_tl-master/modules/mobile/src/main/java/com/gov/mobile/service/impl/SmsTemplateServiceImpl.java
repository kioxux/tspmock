package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.Constants;
import com.gov.common.basic.JsonUtils;
import com.gov.common.basic.SmsUtils;
import com.gov.common.basic.StringUtils;
import com.gov.common.entity.SmsEntity;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.Verification;
import com.gov.mobile.entity.*;
import com.gov.mobile.mapper.*;
import com.gov.mobile.service.ISmsTemplateService;
import com.gov.mobile.service.IUserDataService;
import com.gov.redis.jedis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-06-22
 */
@Slf4j
@Service
public class SmsTemplateServiceImpl extends ServiceImpl<SmsTemplateMapper, SmsTemplate> implements ISmsTemplateService {

    @Resource
    private IUserDataService iUserDataService;
    @Resource
    private RedisClient redisClient;
    @Resource
    private SmsUtils smsUtils;
    @Resource
    private SdMemberBasicMapper sdMemberBasicMapper;
    @Resource
    private SdMemberCardMapper sdMemberCardMapper;
    @Resource
    private CompadmMapper compadmMapper;
    @Resource
    private StoreDataMapper storeDataMapper;

    /**
     * 发送短信
     */
    @Override
    public Result sendSms(Verification verification) {
        log.info("发送短信");
        // 发送过快验证
        String check = redisClient.get(Constants.SMS + verification.getTel() + verification.getCodeType());
        if (org.apache.commons.lang3.StringUtils.isNotBlank(check)) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        String sign = "【公约树】";
        SmsTemplate smsTemplate = new SmsTemplate();
        if (Constants.SMSBusinessType.BUSINESS_LOGIN.equals(verification.getCodeType())) {
            iUserDataService.checkTel(verification.getTel());
            smsTemplate = this.getById(Constants.SMSid.SMS_ID_LOGIN);
        } else
        // 公众号注册、绑定
        if (Constants.SMSBusinessType.BUSINESS_REGISTER.equals(verification.getCodeType()) ||
                Constants.SMSBusinessType.BUSINESS_BIND.equals(verification.getCodeType())) {
            if (org.apache.commons.lang3.StringUtils.isBlank(verification.getClient())) {
                throw new CustomResultException(ResultEnum.E0103, "加盟商");
            }
            if (org.apache.commons.lang3.StringUtils.isBlank(verification.getClient())) {
                throw new CustomResultException(ResultEnum.E0103, "门店");
            }
            // 公众号注册
            if (Constants.SMSBusinessType.BUSINESS_REGISTER.equals(verification.getCodeType())) {
                smsTemplate = this.getById(Constants.SMSid.SMS_ID_REGISTER);
                // 手机号存在验证
                QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("CLIENT", verification.getClient());
                queryWrapper.eq("GSMB_MOBILE", verification.getTel());
                SdMemberBasic member = sdMemberBasicMapper.selectOne(queryWrapper);
                if (member != null) {
                    // 会员卡信息表
                    QueryWrapper<SdMemberCard> sdMemberCardQueryWrapper = new QueryWrapper<>();
                    //加盟商
                    sdMemberCardQueryWrapper.eq("CLIENT", verification.getClient());
                    //会员ID
                    sdMemberCardQueryWrapper.eq("GSMBC_MEMBER_ID", member.getGsmbMemberId());
                    //会员卡号
                    //所属店号
                    sdMemberCardQueryWrapper.eq("GSMBC_ORG_ID", verification.getStoCode());
                    SdMemberCard sdMemberCard = sdMemberCardMapper.selectOne(sdMemberCardQueryWrapper);
                    if (sdMemberCard != null) {
                        throw new CustomResultException(ResultEnum.M0001);
                    }
                }
            }
            // 绑定
            if (Constants.SMSBusinessType.BUSINESS_BIND.equals(verification.getCodeType())) {
                smsTemplate = this.getById(Constants.SMSid.SMS_ID_BIND);
                QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("CLIENT", verification.getClient());
                queryWrapper.eq("GSMB_MOBILE", verification.getTel());
                SdMemberBasic member = sdMemberBasicMapper.selectOne(queryWrapper);
                if (member == null) {
                    throw new CustomResultException(ResultEnum.M0003);
                }
                QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
                //验证 加盟商+会员ID+门店
                query.eq("CLIENT", verification.getClient());
                query.eq("GSMBC_MEMBER_ID", member.getGsmbMemberId());
                query.eq("GSMBC_ORG_ID", verification.getStoCode());
                SdMemberCard card = sdMemberCardMapper.selectOne(query);
                if (null == card) {
                    return ResultUtil.error(ResultEnum.M0003);
                }
            }

            //判断是否是连锁总店
            QueryWrapper<Compadm> compadmQueryWrapper = new QueryWrapper<>();
            compadmQueryWrapper.eq("CLIENT", verification.getClient());
            compadmQueryWrapper.eq("COMPADM_ID", verification.getStoCode());
            Compadm compadm = compadmMapper.selectOne(compadmQueryWrapper);
            if (compadm != null) {
                sign = "【" + StringUtils.left(compadm.getCompadmName(), 20) + "】";
            } else {
                QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<>();
                storeDataQueryWrapper.eq("CLIENT", verification.getClient());
                storeDataQueryWrapper.eq("STO_CODE", verification.getStoCode());
                StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
                if (storeData != null) {
                    sign = "【" + (StringUtils.isBlank(storeData.getStoShortName()) ? StringUtils.left(storeData.getStoName(), 20) : StringUtils.left(storeData.getStoShortName(), 20)) + "】";
                }
            }
        } else {
            throw new CustomResultException(ResultEnum.UNKNOWN_ERROR);
        }

        String randomStr = String.valueOf((Math.random() * 9 + 1) * 100000).substring(0, 6);
        //手机号和业务类型拼接作为key
        redisClient.set(verification.getTel() + verification.getCodeType(), randomStr, 300);
        SmsEntity smsEntity = new SmsEntity();
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign, randomStr));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendSmsToGetVerification(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
        if (!ResultUtil.hasError(result)) {
            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
        }
        return result;
    }

    /**
     * 发送短信
     */
    @Override
    public Result sendStoreRegisterSms(Verification verification) {
        log.info("发送短信");
        String sign = "【药德】";
        SmsTemplate smsTemplate = new SmsTemplate();
        iUserDataService.checkTel(verification.getTel());
        smsTemplate = this.getById(Constants.SMSid.SMS0000005);
        SmsEntity smsEntity = new SmsEntity();
        String storeName = "";
        if(verification.getStoreName().length() > 20){
            storeName =verification.getStoreName().substring(0,14)+"...";
        }else {
            storeName =verification.getStoreName();
        }
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign,storeName, verification.getTel(),verification.getPassWord()));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendStoreRegisterSms(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
        if (!ResultUtil.hasError(result)) {
            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
        }
        return result;
    }
    /**
     * 修改店长发送短信
     */
    @Override
    public Result sendUpdateStoreRegisterSms(Verification verification) {
        log.info("发送短信");
        String sign = "【药德】";
        SmsTemplate smsTemplate = new SmsTemplate();
        iUserDataService.checkTel(verification.getTel());
        smsTemplate = this.getById(Constants.SMSid.SMS0000006);
        SmsEntity smsEntity = new SmsEntity();
        String storeName = "";
        if(verification.getStoreName().length() > 20){
            storeName =verification.getStoreName().substring(0,14)+"...";
        }else {
            storeName =verification.getStoreName();
        }
        smsEntity.setMsg(StringUtils.parse(smsTemplate.getSmsContent(), sign,storeName , verification.getTel()));
        smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
        smsEntity.setPhone(verification.getTel());
        log.info("发送短信：{}", JsonUtils.beanToJson(smsEntity));
        Result result = smsUtils.sendStoreRegisterSms(smsEntity);
        log.info("发送结果：{}", JsonUtils.beanToJson(result));
        if (!ResultUtil.hasError(result)) {
            redisClient.set(Constants.SMS + verification.getTel() + verification.getCodeType(), String.valueOf(true), 30);
        }
        return result;
    }


}
