package com.gov.mobile.controller;


import com.gov.common.response.Result;
import com.gov.mobile.dto.Verification;
import com.gov.mobile.service.ISmsTemplateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-22
 */
@RestController
@RequestMapping("sms")
public class SmsTemplateController {

    @Resource
    private ISmsTemplateService iSmsTemplateService;

    @PostMapping("sendSms")
    @ApiOperation(value = "手机验证码获取")
    public Result sendSms(@Valid @RequestBody Verification verification) {
        return iSmsTemplateService.sendSms(verification);
    }
}

