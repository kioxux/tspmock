package com.gov.mobile.feign.fallback;

import com.gov.mobile.feign.AuthFeign;
import com.gov.mobile.feign.dto.GetWorkflowInData;
import com.gov.mobile.feign.dto.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class AuthFeignFallback implements AuthFeign {

    @Override
    public JsonResult selectApprovingListApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }

    @Override
    public JsonResult selectApprovedListApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }

    @Override
    public JsonResult selectOneApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }

    @Override
    public JsonResult approveApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }

    @Override
    public JsonResult ccApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }

    @Override
    public JsonResult cancelApp(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }

    @Override
    public JsonResult getUserListByClientId(GetWorkflowInData inData) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(500);
        jsonResult.setMessage("调用超时或系统异常，请稍后再试");
        return jsonResult;
    }
}
