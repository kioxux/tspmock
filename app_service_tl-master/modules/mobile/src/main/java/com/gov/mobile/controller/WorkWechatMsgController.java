package com.gov.mobile.controller;

import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.mobile.dto.WorkWechatMsgDTO;
import com.gov.mobile.dto.WorkWechatMsgPageDTO;
import com.gov.mobile.service.IWorkWechatMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "企业微信消息 controller")
@RestController
@RequestMapping("work/wechat/msg")
public class WorkWechatMsgController {

    @Resource
    private IWorkWechatMsgService workWechatMsgService;

    /**
     * 添加企业微信消息
     *
     * @param msgDTO
     * @return
     */
    @PostMapping("addMsg")
    @ApiOperation(value = "添加企业微信消息")
    public Result addMsg(@Valid @RequestBody WorkWechatMsgDTO msgDTO) {
        return workWechatMsgService.addMsg(msgDTO);
    }

    @PostMapping("list")
    @ApiOperation(value = "列表")
    public Result queryList(@Valid @RequestBody WorkWechatMsgPageDTO workWechatMsgPageDTO) {
        return workWechatMsgService.queryList(workWechatMsgPageDTO);
    }

    @PostMapping("deleteByIds")
    @ApiOperation(value = "删除")
    public Result deleteByIds(@RequestBody List<Integer> ids) {
        return workWechatMsgService.deleteByIds(ids);
    }

    @GetMapping("getMsgInfo/{msgId}")
    @ApiOperation(value = "获取消息信息")
    public Result getMsgInfo(@PathVariable("msgId") String msgId) {
        return workWechatMsgService.getMsgInfo(msgId);
    }

    @PostMapping("getUserList/{msgId}")
    @ApiOperation(value = "获取消息用户信息")
    public Result getUserList(@PathVariable("msgId") String msgId) {
        return workWechatMsgService.getUserList(msgId);
    }

    @PostMapping("updateMsgInfo")
    @ApiOperation(value = "更新消息")
    public Result updateMsgInfo(@RequestBody WorkWechatMsgDTO msgDTO) {
        return workWechatMsgService.updateMsgInfo(msgDTO);
    }

    /**
     * @return
     */
    @SneakyThrows
    @GetMapping("send")
    @ApiOperation(value = "发送消息")
    public Result sendMsg() {
        workWechatMsgService.sendMsg();
        return ResultUtil.success();
    }
}
