package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName GAIA_WORKWECHAT_MSG_USER
 */
@TableName(value ="GAIA_WORKWECHAT_MSG_USER")
@Data
public class WorkwechatMsgUser implements Serializable {
    /**
     * 企业微信ID
     */
    @TableField("GWMU_MSG_ID")
    private Integer msgId;

    /**
     * 加盟商
     */
    @TableField("CLIENT")
    private String client;

    /**
     * 药德会员id
     */
    @TableField("GWMU_MEMEBER_ID")
    private String memeberId;

    /**
     * 企微客户id
     */
    @TableField("GWMU_WW_MEMBER_ID")
    private String wwMemberId;

    /**
     * 药德员工id
     */
    @TableField("GWMU_USER_ID")
    private String userId;

    /**
     * 
     */
    @TableField("GWMU_WW_USER_ID")
    private String wwUserId;

    /**
     * 企业微信iD
     */
    @TableField("GWMU_GWC_ID")
    private String gwcId;

    /**
     * 所属门店
     */
    @TableField("GWMU_BR_ID")
    private String brId;

    /**
     * 会员卡号
     */
    @TableField("GWMU_CARD_ID")
    private String cardId;

    /**
     * 所属组织类型，单体：1，连锁：2
     */
    @TableField("GWMU_ORG_TYPE")
    private Integer orgType;

    /**
     * 所属组织，单体：店号，连锁：连锁编码
     */
    @TableField("GWMU_ORG_ID")
    private String orgId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}