package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CompadmEditVO {

    private String client;

    @NotBlank(message = "连锁总部ID不能为空")
    private String compadmId;

    @NotBlank(message = "连锁总部名称不能为空")
    private String compadmName;

    @NotBlank(message = "统一社会信用代码不能为空")
    private String compadmNo;

    @NotBlank(message = "法人/负责人不能为空")
    private String compadmLegalPerson;

//    @NotBlank(message = "质量负责人不能为空")
    private String compadmQua;

//    @NotBlank(message = "详细地址不能为空")
    private String compadmAddr;

//    @NotBlank(message = "LOGO地址不能为空")
    private String compadmLogo;

//    @NotBlank(message = "法人/负责人手机号不能为空")
    private String compadmLegalPhone;

    //    @NotBlank(message = "质量负责人手机号码不能为空")
    private String compadmQuaPhone;

}
