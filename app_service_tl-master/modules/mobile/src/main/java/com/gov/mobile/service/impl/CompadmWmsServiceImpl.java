package com.gov.mobile.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gov.mobile.entity.CompadmWms;
import com.gov.mobile.mapper.CompadmWmsMapper;
import com.gov.mobile.service.ICompadmWmsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-11-24
 */
@Service
public class CompadmWmsServiceImpl extends ServiceImpl<CompadmWmsMapper, CompadmWms> implements ICompadmWmsService {

    @Override
    public List<CompadmWms> getCompadmWmsListByClient(String client) {
        QueryWrapper<CompadmWms> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        return this.list(queryWrapper);
    }
}
