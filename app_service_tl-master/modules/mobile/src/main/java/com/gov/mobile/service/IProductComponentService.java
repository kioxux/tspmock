package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.entity.ProductComponent;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-27
 */
public interface IProductComponentService extends SuperService<ProductComponent> {

    /**
     * 商品成分列表
     * @return
     */
    Result selectProductComponentList();
}
