package com.gov.mobile.mapper;

import com.gov.mobile.entity.UserMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-06-28
 */
public interface UserMessageMapper extends BaseMapper<UserMessage> {

}
