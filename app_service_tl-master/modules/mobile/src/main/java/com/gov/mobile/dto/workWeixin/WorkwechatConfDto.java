package com.gov.mobile.dto.workWeixin;

import com.gov.mobile.entity.WorkwechatConf;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.18
 */
@Data
public class WorkwechatConfDto extends WorkwechatConf {

    /**
     * 加盟商名
     */
    private String francName;

    /**
     * 公众号名
     */
    private String wechatName;

    /**
     * 企业ID
     */
    private String oldGwcId;
}
