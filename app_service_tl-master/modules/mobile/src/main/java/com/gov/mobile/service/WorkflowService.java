package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.feign.dto.GetWorkflowInData;
import com.gov.mobile.feign.dto.JsonResult;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.08.12
 */
public interface WorkflowService {

    /**
     * 待办
     * @param inData
     * @return
     */
    JsonResult selectApprovingList(GetWorkflowInData inData);

    /**
     * 已办
     * @param inData
     * @return
     */
    JsonResult selectApprovedList(GetWorkflowInData inData);

    /**
     * 详情
     * @param inData
     * @return
     */
    JsonResult selectOne(GetWorkflowInData inData);

    /**
     * 审批
     * @param inData
     * @return
     */
    JsonResult approve(GetWorkflowInData inData);

    /**
     * 会签
     * @param inData
     * @return
     */
    JsonResult cc(GetWorkflowInData inData);

    /**
     * 取消
     * @param inData
     * @return
     */
    JsonResult cancel(GetWorkflowInData inData);

    /**
     * 会签员工
     * @param inData
     * @return
     */
    JsonResult getUserListByClientId(GetWorkflowInData inData);
}
