package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.entity.SalesAnalysis;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-01
 */
public interface ISalesAnalysisService extends SuperService<SalesAnalysis> {

    /**
     * 门店报表，当前月份明细
     *
     * @return
     */
    Result getStoreReportByMonth();

    /**
     * 门店报表，当前周明细
     *
     * @return
     */
    Result getStoreReportByWeek();

    /**
     * 汇总报表，三个月数据
     * @return
     */
    Result getSummaryReportByMonth();

    /**
     * 汇总报表，十二周数据
     * @return
     */
    Result getSummaryReportByWeek();
}
