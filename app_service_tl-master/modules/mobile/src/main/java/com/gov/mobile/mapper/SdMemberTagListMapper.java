package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gov.mobile.dto.SdMemberTagListDTO;
import com.gov.mobile.entity.SdMemberTagList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-21
 */
public interface SdMemberTagListMapper extends BaseMapper<SdMemberTagList> {


    List<SdMemberTagListDTO> selectMemberTagList(@Param("client") String client,@Param("cardId") String cardId);
}
