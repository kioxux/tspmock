package com.gov.mobile.mapper;

import com.gov.mobile.entity.SdElectronAutoSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-09-27
 */
public interface SdElectronAutoSetMapper extends BaseMapper<SdElectronAutoSet> {

}
