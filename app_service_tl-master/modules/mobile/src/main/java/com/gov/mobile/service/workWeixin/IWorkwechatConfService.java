package com.gov.mobile.service.workWeixin;

import com.gov.common.response.Result;
import com.gov.mobile.dto.workWeixin.WorkwechatConfDto;
import com.gov.mobile.entity.WorkwechatConf;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-18
 */
public interface IWorkwechatConfService extends SuperService<WorkwechatConf> {

    /**
     * 列表
     * @param pageSize
     * @param pageNum
     * @param client
     * @param gwcName
     * @return
     */
    Result workwechatConfList(Integer pageSize, Integer pageNum, String client, String gwcName, String gwcId);

    /**
     * 新增
     * @param workwechatConf
     * @return
     */
    Result addWorkwechatConf(WorkwechatConf workwechatConf);

    /**
     * 编辑
     * @param workwechatConf
     * @return
     */
    Result editWorkwechatConf(WorkwechatConfDto workwechatConf);

    /**
     * 删除
     * @param workwechatConf
     * @return
     */
    Result deleteWorkwechatConf(WorkwechatConf workwechatConf);
}
