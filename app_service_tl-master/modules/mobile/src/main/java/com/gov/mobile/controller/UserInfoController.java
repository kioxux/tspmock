package com.gov.mobile.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.entity.UserInfo;
import com.gov.mobile.service.IUserInfoService;
import com.gov.common.basic.StringUtils;
import com.gov.common.log.Log;
import com.gov.common.response.Result;
import com.gov.common.response.ResultUtil;
import com.gov.redis.jedis.RedisClient;
import com.gov.common.userutil.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-06-01
 */
@Api(tags = "用户")
@RestController
@RequestMapping("/auth/user-info")
public class UserInfoController {

    @Resource
    private IUserInfoService userInfoService;

    // redis 工具类
    @Resource
    private RedisClient redisClient;

    @Log("根据id获取用户")
    @GetMapping("getUserInfo")
    @ApiOperation(value = "根据id获取用户")
    public UserInfo getUserInfo(@RequestParam("id") Integer id) {
        String token =UserUtils.createToken("1","1", "1","1","1");
        System.out.println(token);
        System.out.println(UserUtils.getPlatform());
        System.out.println(token.length());
        return userInfoService.getById(id);
    }

    @PostMapping("insert")
    @ApiOperation(value = "插入用户")
    public Result insertUser(@RequestBody UserInfo userInfo) {
        return ResultUtil.success(userInfoService.save(userInfo));
    }

    @PostMapping("update")
    @ApiOperation(value = "更新用户")
    public Result updateUser(@RequestBody UserInfo userInfo) {
        return ResultUtil.success(userInfoService.updateById(userInfo));
    }

    @GetMapping("selectPage")
    @ApiOperation(value = "分页")
    public Result selectPage(Page<UserInfo> page, UserInfo entity) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(entity.getUserName()), "user_name", entity.getUserName());
        return userInfoService.selectPage(page, wrapper);
    }


}

