package com.gov.mobile.feign.dto.Message;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.23
 */
@Data
public class MessageParams {

    /**
     * 消息模板
     */
    private String id;

    /**
     * 标题参数
     */
    private List<String> titleParams;

    /**
     * 内容参数
     */
    private List<String> contentParmas;

    /**
     * 自定义参数
     */
    private Map<String, String> params;

    /**
     * 接收人加盟商
     */
    private String client;

    /**
     * 接收用户
     */
    private String userId;
}
