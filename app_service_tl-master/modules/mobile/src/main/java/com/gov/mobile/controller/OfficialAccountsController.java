package com.gov.mobile.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
@RestController
@RequestMapping("/mobile/official-accounts")
public class OfficialAccountsController {

}

