package com.gov.mobile.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gov.common.basic.CommonEnum;
import com.gov.common.basic.Constants;
import com.gov.common.basic.DateUtils;
import com.gov.common.basic.JsonUtils;
import com.gov.common.entity.Pageable;
import com.gov.common.response.CustomResultException;
import com.gov.common.response.Result;
import com.gov.common.response.ResultEnum;
import com.gov.common.response.ResultUtil;
import com.gov.common.userutil.UserUtils;
import com.gov.common.uuid.UUIDUtil;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.*;
import com.gov.mobile.feign.service.OperationService;
import com.gov.mobile.mapper.*;
import com.gov.mobile.service.ISdMemberBasicService;
import com.gov.redis.jedis.RedisClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sy
 * @since 2020-07-29
 */
@Service
public class SdMemberBasicServiceImpl extends ServiceImpl<SdMemberBasicMapper, SdMemberBasic> implements ISdMemberBasicService {
    @Resource
    private RedisClient redisClient;
    @Resource
    private SdMemberBasicMapper sdMemberBasicMapper;
    @Resource
    private SdMemberCardMapper sdMemberCardMapper;
    @Resource
    private MemberLoginLogMapper memberLoginLogMapper;
    @Resource
    private SdRechargeCardMapper sdRechargeCardMapper;
    @Resource
    private CompadmMapper compadmMapper;
    @Resource
    private StoreDataMapper storeDataMapper;
    @Resource
    private OfficialAccountsMapper officialAccountsMapper;
    @Resource
    private SdElectronChangeMapper sdElectronChangeMapper;
    @Resource
    private OperationService operationService;
    @Resource
    private WorkwechatCusUserBindMapper workwechatCusUserBindMapper;
    @Resource
    private ClSystemParaMapper clSystemParaMapper;

    /**
     * 公众号会员注册
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result register(MemberUserDto modelClient) {
        //判断验正码是否正确
        String redisVerification = redisClient.get(modelClient.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_REGISTER);
        if (!modelClient.getVerificationCode().equals(redisVerification)) {
            return ResultUtil.error(ResultEnum.M0002);
        } else {
            //删除redis里面的验证码
            redisClient.del(modelClient.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_REGISTER);
        }

        //验证当前加盟商+手机号 在表中是否存在
        QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", modelClient.getClient());
        queryWrapper.eq("GSMB_MOBILE", modelClient.getGsmbMobile());
        SdMemberBasic member = sdMemberBasicMapper.selectOne(queryWrapper);

        //会员卡信息表
        SdMemberCard card = new SdMemberCard();
        //加盟商
        card.setClient(modelClient.getClient());
        // 所属组织
        card.setGsmbcOrgId(modelClient.getGsmbBrId());
        //渠道
        card.setGsmbcChannel("1");
        //卡类型
        card.setGsmbcClassId(CommonEnum.CardType.CARD_TYPE_ORDINARY.getCode());
        //当前积分
        card.setGsmbcIntegral("0");
        //最后积分日期
        card.setGsmbcIntegralLastdate(DateUtils.getCurrentDateStrYYMMDD());
        //新卡创建日期
        card.setGsmbcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        //类型
        card.setGsmbcType(CommonEnum.UserCardType.STATUS_ACTIVITY.getCode());
        //openID
        card.setGsmbcOpenId(modelClient.getGsmbOpenId());
        //卡状态
        card.setGsmbcStatus(CommonEnum.UserStatus.STATUS_ACTIVITY.getCode());
        // 开卡门店
        if (StringUtils.isNotBlank(modelClient.getOpenStore()) && modelClient.getOpenStore().toUpperCase().equals("NULL")) {
            modelClient.setOpenStore(null);
        }
        card.setGsmbcOpenCard(modelClient.getOpenStore());
        //门店
        if (StringUtils.isNotBlank(modelClient.getOpenStore())) {
            card.setGsmbcBrId(modelClient.getOpenStore());
        } else {
            QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<>();
            storeDataQueryWrapper.eq("CLIENT", modelClient.getClient());
            storeDataQueryWrapper.eq("STO_CODE", modelClient.getGsmbBrId());
            StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
            if (storeData != null) {
                card.setGsmbcBrId(modelClient.getGsmbBrId());
            } else {
                storeData = storeDataMapper.getMinStore(modelClient.getClient(), modelClient.getGsmbBrId());
                if (storeData != null) {
                    card.setGsmbcBrId(storeData.getStoCode());
                }
            }
        }
        //登录日志表
        MemberLoginLog log = new MemberLoginLog();
        log.setId(UUIDUtil.getUUID());
        log.setClient(modelClient.getClient());
        log.setStoreCode(modelClient.getGsmbBrId());
        log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
        log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
        log.setPlatform("1");
        log.setOpenId(modelClient.getGsmbOpenId());
        //返回信息
        Map<Object, Object> map = new HashMap<>();
        Map<Object, Object> numMap = new HashMap<>();
        numMap.put("client", modelClient.getClient());

        if (null != member) {
            QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
            //验证 加盟商+会员ID+门店
            query.eq("CLIENT", modelClient.getClient());
            query.eq("GSMBC_MEMBER_ID", member.getGsmbMemberId());
            query.eq("GSMBC_ORG_ID", modelClient.getGsmbBrId());
            Integer count = sdMemberCardMapper.selectCount(query);
            if (count > 0) {
                return ResultUtil.error(ResultEnum.M0001);
            } else {
                // 清空之前绑定关系
                SdMemberCard cardOld = new SdMemberCard();
                cardOld.setGsmbcOpenId("");
                sdMemberCardMapper.update(cardOld,
                        new QueryWrapper<SdMemberCard>().eq("CLIENT", modelClient.getClient())
                                .eq("GSMBC_ORG_ID", modelClient.getGsmbBrId())
                                .eq("GSMBC_OPEN_ID", modelClient.getGsmbOpenId())
                );

                //会员id
                card.setGsmbcMemberId(member.getGsmbMemberId());
                //会员卡号
                String cardNumber = sdMemberCardMapper.getUserCardNumber(numMap).getGsmbcCardId();
                card.setGsmbcCardId(cardNumber);
                // 激活状态
                cardStatus(card);
                sdMemberCardMapper.insert(card);

                operationService.memberSync(card.getClient(), card.getGsmbcMemberId(), card.getGsmbcCardId());

                // 电子券 生成
                addElectron(modelClient.getClient(), cardNumber);

                //生成token
                String token = UserUtils.createMemberToken(modelClient.getClient(), cardNumber, modelClient.getGsmbOpenId(), modelClient.getGsmbBrId(), member.getGsmbMemberId(), UserUtils.PLATFORM_2);
                log.setMemberId(member.getGsmbMemberId());
                log.setMemberCard(cardNumber);
                log.setToken(token);
                memberLoginLogMapper.insert(log);
                //删除redis里面的验证码
                redisClient.del(modelClient.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_REGISTER);
                map.put("token", token);
                return ResultUtil.success(map);
            }
        }

        //会员信息表
        SdMemberBasic basic = new SdMemberBasic();
        String userId = UUIDUtil.getUUID();
        //加盟商
        basic.setClient(modelClient.getClient());
        //会员id
        basic.setGsmbMemberId(userId);
        //会员姓名
        basic.setGsmbName(modelClient.getGsmbName());
        //手机
        basic.setGsmbMobile(modelClient.getGsmbMobile());
        //性别
        basic.setGsmbSex(modelClient.getGsmbSex());
        //生日
        basic.setGsmbBirth(modelClient.getGsmbBirth());
        //身份证
        basic.setGsmbCredentials(modelClient.getGsmbCredentials());
        //年龄
        Integer year = Integer.valueOf(DateUtils.getCurrentDateStrYYMMDD().substring(0, 4)) - Integer.valueOf(modelClient.getGsmbBirth().substring(0, 4));
        basic.setGsmbAge(year.toString());
        //是否接受本司信息
        basic.setGsmbContactAllowed("1");
        //会员状态
        basic.setGsmbStatus(CommonEnum.UserStatus.STATUS_ACTIVITY.getCode());
        sdMemberBasicMapper.insert(basic);

        // 清空之前绑定关系
        SdMemberCard cardOld = new SdMemberCard();
        cardOld.setGsmbcOpenId("");
        sdMemberCardMapper.update(cardOld,
                new QueryWrapper<SdMemberCard>().eq("CLIENT", modelClient.getClient())
                        .eq("GSMBC_ORG_ID", modelClient.getGsmbBrId())
                        .eq("GSMBC_OPEN_ID", modelClient.getGsmbOpenId())
        );

        //会员卡号
        String cardNumber = sdMemberCardMapper.getUserCardNumber(numMap).getGsmbcCardId();
        card.setGsmbcCardId(cardNumber);
        //会员id
        card.setGsmbcMemberId(userId);
        // 激活状态
        cardStatus(card);
        sdMemberCardMapper.insert(card);
        operationService.memberSync(card.getClient(), card.getGsmbcMemberId(), card.getGsmbcCardId());
        // 电子券 生成
        addElectron(modelClient.getClient(), cardNumber);

        //生成token
        String token = UserUtils.createMemberToken(modelClient.getClient(), cardNumber, modelClient.getGsmbOpenId(), modelClient.getGsmbBrId(), basic.getGsmbMemberId(), UserUtils.PLATFORM_2);
        log.setMemberId(userId);
        log.setMemberCard(cardNumber);
        log.setToken(token);
        memberLoginLogMapper.insert(log);

        map.put("token", token);
        return ResultUtil.success(map);
    }

    /**
     * 生成电子券
     *
     * @param client
     * @param cardNumber
     */
    @Override
    public void addElectron(String client, String cardNumber) {
        // 电子券 生成
        List<ElectronReserved> electronReservedList = sdElectronChangeMapper.selectElectronReserved(client);
        // 电子券号
        Map<String, Integer> gsecIdMap = new HashMap<>();
        for (int i = 0; !CollectionUtils.isEmpty(electronReservedList) && i < electronReservedList.size(); i++) {
            // 赠送电子券
            ElectronReserved electronReserved = electronReservedList.get(i);
            // 面值
            BigDecimal gsebAmt = electronReserved.getGsebAmt();
            // 电子券活动号
            String gsebId = StringUtils.left(electronReserved.getGsebId(), 4);
            // 电子券描述
            String gsebName = electronReserved.getGsebName();
            // 发送数量
            int gseasQty = NumberUtils.toInt(electronReserved.getGseasQty(), 0);
            // 电子券号
            Integer gsecId = gsecIdMap.get(gsebId);
            if (gsecId == null) {
                gsecId = sdElectronChangeMapper.selectGsecId(client, gsebId);
            }
            if (gsecId == null) {
                gsecId = 0;
            }
            while (redisClient.exists(Constants.GAIA_SD_ELECTRON_CHANGE_GSEC_ID + client + gsebId + gsecId)) {
                gsecId++;
            }
            redisClient.set(Constants.GAIA_SD_ELECTRON_CHANGE_GSEC_ID + client + gsebId + gsecId, String.valueOf(gsecId), 1800);
            for (int r = 0; r < gseasQty; r++) {
                // 电子券异动表
                SdElectronChange sdElectronChange = new SdElectronChange();
                // 加盟商
                sdElectronChange.setClient(client);
                // 会员卡号
                sdElectronChange.setGsecMemberId(cardNumber);
                // 电子券号
                gsecId++;
                String gsedCode = StringUtils.leftPad(String.valueOf(gsecId), 10, "0");
                sdElectronChange.setGsecId(gsebId + gsedCode);
                // 电子券活动号
                sdElectronChange.setGsebId(gsebId);
                // 是否使用
                sdElectronChange.setGsecStatus("N");
                // 电子券金额
                sdElectronChange.setGsecAmt(gsebAmt);
                // 途径
                sdElectronChange.setGsecFlag("1");
                // 创建日期
                sdElectronChange.setGsecCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                // 电子券描述
                sdElectronChange.setGsebName(gsebName);
                sdElectronChangeMapper.insert(sdElectronChange);
            }
            gsecIdMap.put(gsebId, gsecId);
        }
    }

    /**
     * 公众号会员登录
     */
    @Override
    public Result Login(MemberUserLoginDto loginDto) {
        //查看是否有这个会员 根据加盟商，openId,门店
        QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
        //验证 加盟商+openId+门店
        query.eq("CLIENT", loginDto.getClient());
        query.eq("GSMBC_ORG_ID", loginDto.getGsmbBrId());
        query.eq("GSMBC_OPEN_ID", loginDto.getGsmbOpenId());
        SdMemberCard card = sdMemberCardMapper.selectOne(query);
        if (null != card) {
            // 判断是否企微关注绑过
            if ((card.getGsmbcWorkwechatBindStatus() == null || card.getGsmbcWorkwechatBindStatus() == 0) &&
                    StrUtil.isAllNotBlank(loginDto.getGwcId(), loginDto.getMemberId())) {
                // 企微关注绑定添加
                WorkwechatCusUserBind bind = new WorkwechatCusUserBind();
                bind.setClient(loginDto.getClient());
                bind.setGwcId(loginDto.getGwcId());
                bind.setGwcubUserId(loginDto.getUserId());
                bind.setGwcubWwUserId(loginDto.getWwUserId());
                bind.setGwcubWwMemberId(loginDto.getMemberId());
                bind.setGwcubMemberId(card.getGsmbcMemberId());
                // 开卡门店
                bind.setGwcubBrId(card.getGsmbcOpenCard());
                // 门店|连锁
                bind.setGwcubOrgId(card.getGsmbcOrgId());
                // 会员卡号
                bind.setGwcubCardId(card.getGsmbcCardId());
                // 所属组织类型，单体：1，连锁：2
                bind.setGwcubOrgType(StrUtil.equals(card.getGsmbcOpenCard(), card.getGsmbcOrgId()) ? 1 : 2);
                bind.setGwcubBindTime(new Date());
                workwechatCusUserBindMapper.insert(bind);

                // 已绑企业微信
                SdMemberCard cd = new SdMemberCard();
                cd.setGsmbcWorkwechatBindStatus(1);
                sdMemberCardMapper.update(cd, new QueryWrapper<SdMemberCard>()
                        .eq("CLIENT", loginDto.getClient())
                        .eq("GSMBC_MEMBER_ID", card.getGsmbcMemberId())
                        .eq("GSMBC_BR_ID", card.getGsmbcBrId()));
            }

            Map<Object, Object> map = new HashMap<>();
            //绑定过
            //当有会员时,根据会员卡号，门店，加盟商，openId 会员id 生成token
            String token = UserUtils.createMemberToken(loginDto.getClient(), card.getGsmbcCardId(), loginDto.getGsmbOpenId(), loginDto.getGsmbBrId(), card.getGsmbcMemberId(), UserUtils.PLATFORM_2);
            //塞当前登录人的信息到redis里面
            map.put("token", token);
            //登录日志表
            MemberLoginLog log = new MemberLoginLog();
            log.setId(UUIDUtil.getUUID());
            log.setClient(loginDto.getClient());
            log.setStoreCode(loginDto.getGsmbBrId());
            log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
            log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            log.setMemberId(card.getGsmbcMemberId());
            log.setMemberCard(card.getGsmbcCardId());
            log.setPlatform("1");
            log.setOpenId(loginDto.getGsmbOpenId());
            log.setToken(token);
            memberLoginLogMapper.insert(log);
            return ResultUtil.success(map);
        }
        return ResultUtil.success();
    }

    /**
     * 获取当前登录人
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getUserInfo() {
        Map<Object, String> tokenMap = getToken();
        String user = tokenMap.get("currentUser");
        TokenUserDto userInfo = JsonUtils.jsonToBean(user, TokenUserDto.class);
        return ResultUtil.success(userInfo);
    }


    /**
     * 公众号会员绑定
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result bindUserInfo(MemberBindUserDto bindUserDto) {

        //判断验正码是否正确
        String redisVerification = redisClient.get(bindUserDto.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_BIND);
        if (!bindUserDto.getVerificationCode().equals(redisVerification)) {
            return ResultUtil.error(ResultEnum.M0002);
        }

        //验证当前加盟商+手机号 在表中是否存在
        QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", bindUserDto.getClient());
        queryWrapper.eq("GSMB_MOBILE", bindUserDto.getGsmbMobile());
        SdMemberBasic member = sdMemberBasicMapper.selectOne(queryWrapper);
        if (null == member) {
            return ResultUtil.error(ResultEnum.M0003);
        }

        QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
        //验证 加盟商+会员ID+门店
        query.eq("CLIENT", bindUserDto.getClient());
        query.eq("GSMBC_MEMBER_ID", member.getGsmbMemberId());
        query.eq("GSMBC_ORG_ID", bindUserDto.getGsmbBrId());
        SdMemberCard card = sdMemberCardMapper.selectOne(query);
        if (null == card) {
            return ResultUtil.error(ResultEnum.M0003);
        }

        // 清空之前绑定关系
        SdMemberCard cardOld = new SdMemberCard();
        cardOld.setGsmbcOpenId("");
        sdMemberCardMapper.update(cardOld,
                new QueryWrapper<SdMemberCard>().eq("CLIENT", bindUserDto.getClient())
                        .eq("GSMBC_ORG_ID", bindUserDto.getGsmbBrId())
                        .eq("GSMBC_OPEN_ID", bindUserDto.getGsmbOpenId())
        );

        //返回信息
        Map<Object, Object> map = new HashMap<>();
        //生成token
        String token = UserUtils.createMemberToken(bindUserDto.getClient(), card.getGsmbcCardId(), bindUserDto.getGsmbOpenId(), bindUserDto.getGsmbBrId(), member.getGsmbMemberId(), UserUtils.PLATFORM_2);

        //绑定会员
        card.setGsmbcOpenId(bindUserDto.getGsmbOpenId());
        sdMemberCardMapper.update(card, query);

        //登录日志表
        MemberLoginLog log = new MemberLoginLog();
        log.setId(UUIDUtil.getUUID());
        log.setClient(bindUserDto.getClient());
        log.setStoreCode(bindUserDto.getGsmbBrId());
        log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
        log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
        log.setMemberId(card.getGsmbcMemberId());
        log.setMemberCard(card.getGsmbcCardId());
        log.setPlatform("1");
        log.setToken(token);
        log.setOpenId(bindUserDto.getGsmbOpenId());
        memberLoginLogMapper.insert(log);
        //删除redis里面的验证码
        redisClient.del(bindUserDto.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_BIND);
        map.put("token", token);
        return ResultUtil.success(map);
    }


    /**
     * 获取当前会员的积分记录
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getPointList(Pageable page) {
        Map<Object, String> tokenMap = getToken();
        String token = tokenMap.get("token");
        String user = tokenMap.get("currentUser");
        TokenUserDto userInfo = JsonUtils.jsonToBean(user, TokenUserDto.class);
//        if (StringUtils.isEmpty(user)) {
//            userInfo = JsonUtils.jsonToBean(redisClient.get(token), TokenUserDto.class);
//        } else {
//            userInfo = JsonUtils.jsonToBean(user, TokenUserDto.class);
//        }

        Map<Object, Object> map = new HashMap<>();

        QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
        query.eq("CLIENT", userInfo.getClient());
        query.eq("GSMBC_MEMBER_ID", userInfo.getGsmbMemberId());
        query.eq("GSMBC_CARD_ID", userInfo.getGsmbcCardId());
        query.eq("GSMBC_ORG_ID", userInfo.getGsmbcBrId());
        SdMemberCard card = sdMemberCardMapper.selectOne(query);

        QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sale.CLIENT", userInfo.getClient());
        queryWrapper.eq("sale.GSSH_HYK_NO", userInfo.getGsmbcCardId());
        Page<PointRecordDto> pageInfo = new Page<PointRecordDto>();
        pageInfo.setCurrent(page.getPageNum());
        pageInfo.setSize(page.getPageSize());

        IPage<PointRecordDto> pointList = sdMemberBasicMapper.selectPageList(pageInfo, queryWrapper);


        map.put("pointList", pointList);
        map.put("integral", null != card ? card.getGsmbcIntegral() : 0);
        map.put("token", token);
        return ResultUtil.success(map);
    }


    /**
     * 获取当前会员的余额使用记录
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getMoneyRecordList(Pageable page) {
        Map<Object, String> tokenMap = getToken();
        String token = tokenMap.get("token");
        String user = tokenMap.get("currentUser");
        TokenUserDto userInfo = JsonUtils.jsonToBean(user, TokenUserDto.class);

        QueryWrapper<SdRechargeCard> query = new QueryWrapper<>();
        query.eq("CLIENT", userInfo.getClient());
        query.eq("GSRC_ID", userInfo.getGsmbcCardId());
        query.eq("GSRC_STATUS", "1");
        SdRechargeCard card = sdRechargeCardMapper.selectOne(query);

        Map<Object, Object> map = new HashMap<>();
        QueryWrapper<SdMemberCard> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("a.CLIENT", userInfo.getClient());
        queryWrapper.eq("a.CARDNUMBER", userInfo.getGsmbcCardId());
        Page<PointRecordDto> pageInfo = new Page<PointRecordDto>();
        pageInfo.setCurrent(page.getPageNum());
        pageInfo.setSize(page.getPageSize());
        IPage<PointRecordDto> pointList = sdMemberBasicMapper.selectMoneyPageList(pageInfo, queryWrapper);
        map.put("moneyList", pointList);
        map.put("balance", null != card ? card.getGsrcAmt() : 0);
        map.put("token", token);
        return ResultUtil.success(map);
    }

    /**
     * 获取当前会员的优惠券
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getCouponRecordList(PageDto page) {
        Map<Object, String> tokenMap = getToken();
        String token = tokenMap.get("token");
        String user = tokenMap.get("currentUser");
        TokenUserDto userInfo = JsonUtils.jsonToBean(user, TokenUserDto.class);
        Page<PointRecordDto> pageInfo = new Page<PointRecordDto>();
        pageInfo.setCurrent(page.getPageNum());
        pageInfo.setSize(page.getPageSize());
        Map<Object, Object> map = new HashMap<>();
        IPage<PointRecordDto> pointList = sdMemberBasicMapper.selectCouponPageList(pageInfo, userInfo.getClient(), userInfo.getGsmbcCardId(), page.getType());
        map.put("couponList", pointList);
        map.put("token", token);
        return ResultUtil.success(map);
    }

    /**
     * 获取门店
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getStoreList(String client, String stoCode) {
        Map<Object, Object> map = new HashMap<>();
        //判断是否是连锁总店
        QueryWrapper<Compadm> query = new QueryWrapper<>();
        query.eq("CLIENT", client);
        query.eq("COMPADM_ID", stoCode);
        Integer Count = compadmMapper.selectCount(query);
        QueryWrapper<StoreData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", client);
        if (Count > 0) {
            queryWrapper.eq("STO_ATTRIBUTE", 2);
            queryWrapper.eq("STO_CHAIN_HEAD", stoCode);

        } else {
            queryWrapper.eq("STO_CODE", stoCode);
        }
        List<StoreData> storeList = storeDataMapper.selectList(queryWrapper);
        map.put("storeList", storeList);
        return ResultUtil.success(map);
    }

    /**
     * 获取公众号的配置
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result getConfigList(ConfigDto configDto) {
        // 获取公众号APPID
        QueryWrapper<OfficialAccounts> query = new QueryWrapper<>();
        query.eq("CLIENT", configDto.getClient());
        query.eq("GOA_CODE", configDto.getStoCode());
        query.eq("GOA_OBJECT_PARAM", "WECHAT_OFFICIAL_ACCOUNTS_APPID");
        OfficialAccounts accounts = officialAccountsMapper.selectOne(query);
        return ResultUtil.success(accounts);
    }

    @Override
    public Result cusRegister(MemberUserDto modelClient) {
        //判断验正码是否正确
        String redisVerification = redisClient.get(modelClient.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_REGISTER);
        if (!modelClient.getVerificationCode().equals(redisVerification)) {
            return ResultUtil.error(ResultEnum.M0002);
        } else {
            //删除redis里面的验证码
            redisClient.del(modelClient.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_REGISTER);
        }

        //验证当前加盟商+手机号 在表中是否存在
        QueryWrapper<SdMemberBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("CLIENT", modelClient.getClient());
        queryWrapper.eq("GSMB_MOBILE", modelClient.getGsmbMobile());
        SdMemberBasic member = sdMemberBasicMapper.selectOne(queryWrapper);

        //会员卡信息表
        SdMemberCard card = new SdMemberCard();
        //加盟商
        card.setClient(modelClient.getClient());
        // 所属组织
        card.setGsmbcOrgId(modelClient.getGsmbBrId());
        //渠道
        card.setGsmbcChannel("1");
        //卡类型
        card.setGsmbcClassId(CommonEnum.CardType.CARD_TYPE_ORDINARY.getCode());
        //当前积分
        card.setGsmbcIntegral("0");
        //最后积分日期
        card.setGsmbcIntegralLastdate(DateUtils.getCurrentDateStrYYMMDD());
        //新卡创建日期
        card.setGsmbcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        //类型
        card.setGsmbcType(CommonEnum.UserCardType.STATUS_ACTIVITY.getCode());
        //openID
        card.setGsmbcOpenId(modelClient.getGsmbOpenId());
        //卡状态
        card.setGsmbcStatus(CommonEnum.UserStatus.STATUS_ACTIVITY.getCode());
        // 开卡门店
        if (StringUtils.isNotBlank(modelClient.getOpenStore()) && modelClient.getOpenStore().toUpperCase().equals("NULL")) {
            modelClient.setOpenStore(null);
        }
        card.setGsmbcOpenCard(modelClient.getOpenStore());
        //门店
        if (StringUtils.isNotBlank(modelClient.getOpenStore())) {
            card.setGsmbcBrId(modelClient.getOpenStore());
        } else {
            QueryWrapper<StoreData> storeDataQueryWrapper = new QueryWrapper<>();
            storeDataQueryWrapper.eq("CLIENT", modelClient.getClient());
            storeDataQueryWrapper.eq("STO_CODE", modelClient.getGsmbBrId());
            StoreData storeData = storeDataMapper.selectOne(storeDataQueryWrapper);
            if (storeData != null) {
                card.setGsmbcBrId(modelClient.getGsmbBrId());
            } else {
                storeData = storeDataMapper.getMinStore(modelClient.getClient(), modelClient.getGsmbBrId());
                if (storeData != null) {
                    card.setGsmbcBrId(storeData.getStoCode());
                }
            }
        }
        //登录日志表
        MemberLoginLog log = new MemberLoginLog();
        log.setId(UUIDUtil.getUUID());
        log.setClient(modelClient.getClient());
        log.setStoreCode(modelClient.getGsmbBrId());
        log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
        log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
        log.setPlatform("1");
        log.setOpenId(modelClient.getGsmbOpenId());
        //返回信息
        Map<Object, Object> map = new HashMap<>();
        Map<Object, Object> numMap = new HashMap<>();
        numMap.put("client", modelClient.getClient());

        if (null != member) {
            QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
            //验证 加盟商+会员ID+门店
            query.eq("CLIENT", modelClient.getClient());
            query.eq("GSMBC_MEMBER_ID", member.getGsmbMemberId());
            query.eq("GSMBC_ORG_ID", modelClient.getGsmbBrId());
            Integer count = sdMemberCardMapper.selectCount(query);
            if (count > 0) {
                return ResultUtil.error(ResultEnum.M0001);
            } else {
                // 清空之前绑定关系
                SdMemberCard cardOld = new SdMemberCard();
                cardOld.setGsmbcOpenId("");
                sdMemberCardMapper.update(cardOld,
                        new QueryWrapper<SdMemberCard>().eq("CLIENT", modelClient.getClient())
                                .eq("GSMBC_ORG_ID", modelClient.getGsmbBrId())
                                .eq("GSMBC_OPEN_ID", modelClient.getGsmbOpenId())
                );

                //会员id
                card.setGsmbcMemberId(member.getGsmbMemberId());
                //会员卡号
                String cardNumber = sdMemberCardMapper.getUserCardNumber(numMap).getGsmbcCardId();
                card.setGsmbcCardId(cardNumber);
                // 激活状态
                cardStatus(card);
                sdMemberCardMapper.insert(card);


                operationService.memberSync(card.getClient(), card.getGsmbcMemberId(), card.getGsmbcCardId());

                // 电子券 生成
                addElectron(modelClient.getClient(), cardNumber);

                //生成token
                String token = UserUtils.createMemberToken(modelClient.getClient(), cardNumber, modelClient.getGsmbOpenId(), modelClient.getGsmbBrId(), member.getGsmbMemberId(), UserUtils.PLATFORM_2);
                log.setMemberId(member.getGsmbMemberId());
                log.setMemberCard(cardNumber);
                log.setToken(token);
                memberLoginLogMapper.insert(log);
                //删除redis里面的验证码
                redisClient.del(modelClient.getGsmbMobile() + Constants.SMSBusinessType.BUSINESS_REGISTER);
                map.put("token", token);
                return ResultUtil.success(map);
            }
        }

        //会员信息表
        SdMemberBasic basic = new SdMemberBasic();
        String userId = UUIDUtil.getUUID();
        //加盟商
        basic.setClient(modelClient.getClient());
        //会员id
        basic.setGsmbMemberId(userId);
        //会员姓名
        basic.setGsmbName(modelClient.getGsmbName());
        //手机
        basic.setGsmbMobile(modelClient.getGsmbMobile());
        //性别
        basic.setGsmbSex(modelClient.getGsmbSex());
        //生日
        basic.setGsmbBirth(modelClient.getGsmbBirth());
        //身份证
        basic.setGsmbCredentials(modelClient.getGsmbCredentials());
        //年龄
        Integer year = Integer.valueOf(DateUtils.getCurrentDateStrYYMMDD().substring(0, 4)) - Integer.valueOf(modelClient.getGsmbBirth().substring(0, 4));
        basic.setGsmbAge(year.toString());
        //是否接受本司信息
        basic.setGsmbContactAllowed("1");
        //会员状态
        basic.setGsmbStatus(CommonEnum.UserStatus.STATUS_ACTIVITY.getCode());
        sdMemberBasicMapper.insert(basic);

        // 清空之前绑定关系
        SdMemberCard cardOld = new SdMemberCard();
        cardOld.setGsmbcOpenId("");
        sdMemberCardMapper.update(cardOld,
                new QueryWrapper<SdMemberCard>().eq("CLIENT", modelClient.getClient())
                        .eq("GSMBC_ORG_ID", modelClient.getGsmbBrId())
                        .eq("GSMBC_OPEN_ID", modelClient.getGsmbOpenId())
        );

        //会员卡号
        String cardNumber = sdMemberCardMapper.getUserCardNumber(numMap).getGsmbcCardId();
        card.setGsmbcCardId(cardNumber);
        //会员id
        card.setGsmbcMemberId(userId);
        // 已綁企微
        card.setGsmbcWorkwechatBindStatus(1);
        // 激活状态
        cardStatus(card);
        sdMemberCardMapper.insert(card);

        // 企微关注绑定添加
        WorkwechatCusUserBind bind = new WorkwechatCusUserBind();
        bind.setClient(modelClient.getClient());
        bind.setGwcId(modelClient.getGwcId());
        bind.setGwcubUserId(modelClient.getUserId());
        bind.setGwcubWwUserId(modelClient.getWwUserId());
        bind.setGwcubWwMemberId(modelClient.getMemberId());
        bind.setGwcubMemberId(card.getGsmbcMemberId());
        // 开卡门店
        bind.setGwcubBrId(card.getGsmbcOpenCard());
        // 门店|连锁
        bind.setGwcubOrgId(card.getGsmbcOrgId());
        // 会员卡号
        bind.setGwcubCardId(card.getGsmbcCardId());
        // 所属组织类型，单体：1，连锁：2
        bind.setGwcubOrgType(StrUtil.equals(card.getGsmbcOpenCard(), card.getGsmbcOrgId()) ? 1 : 2);
        bind.setGwcubBindTime(new Date());
        workwechatCusUserBindMapper.insert(bind);

        operationService.memberSync(card.getClient(), card.getGsmbcMemberId(), card.getGsmbcCardId());
        // 电子券 生成
        addElectron(modelClient.getClient(), cardNumber);

        //生成token
        String token = UserUtils.createMemberToken(modelClient.getClient(), cardNumber, modelClient.getGsmbOpenId(), modelClient.getGsmbBrId(), basic.getGsmbMemberId(), UserUtils.PLATFORM_2);
        log.setMemberId(userId);
        log.setMemberCard(cardNumber);
        log.setToken(token);
        memberLoginLogMapper.insert(log);

        map.put("token", token);
        return ResultUtil.success(map);
    }


    /**
     * 登录凭证不存在则到登录日志表取用户信息，从会员卡信息里面，重新生成登录凭证，插入登录日志
     *
     * @return
     */
    private Map getToken() {
        Map<Object, Object> map = new HashMap<>();
        String token = UserUtils.getTokenCode();
        String userStr;
        try {
            userStr = redisClient.get(token);
        } catch (Exception e) {
            userStr = null;
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(userStr)) {
            //当redis里面的凭证不存在 ，去查一遍并重新塞值给redis
            QueryWrapper<MemberLoginLog> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("TOKEN", token);
            queryWrapper.orderByDesc("USER_CRE_DATE", "USER_CRE_TIME");
            List<MemberLoginLog> memberLoginLogList = memberLoginLogMapper.selectList(queryWrapper);
            if (CollectionUtils.isEmpty(memberLoginLogList)) {
                throw new CustomResultException(ResultEnum.E0007);
            }
            MemberLoginLog log = memberLoginLogList.get(0);
            QueryWrapper<SdMemberCard> query = new QueryWrapper<>();
            query.eq("CLIENT", log.getClient());
            query.eq("GSMBC_MEMBER_ID", log.getMemberId());
            query.eq("GSMBC_CARD_ID", log.getMemberCard());
            query.eq("GSMBC_ORG_ID", log.getStoreCode());
            query.eq("GSMBC_OPEN_ID", log.getOpenId());
            SdMemberCard card = sdMemberCardMapper.selectOne(query);
            if (null == card) {
                throw new CustomResultException(ResultEnum.E0007);
            }
            token = UserUtils.createMemberToken(card.getClient(), card.getGsmbcCardId(), card.getGsmbcOpenId(), card.getGsmbcOrgId(), card.getGsmbcMemberId(), UserUtils.PLATFORM_2);
            //重新插入日志表
            log.setId(UUIDUtil.getUUID());
            log.setClient(card.getClient());
            log.setStoreCode(card.getGsmbcOrgId());
            log.setPlatform("1");
            log.setLogintime(DateUtils.getCurrentDateStrYYMMDD());
            log.setUserCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            log.setUserCreDate(DateUtils.getCurrentDateStrYYMMDD());
            log.setMemberId(card.getGsmbcMemberId());
            log.setMemberCard(card.getGsmbcCardId());
            log.setOpenId(card.getGsmbcOpenId());
            log.setToken(token);
            memberLoginLogMapper.insert(log);

            Map<Object, Object> dataMap = new HashMap<>();
            dataMap.put("client", log.getClient());
            dataMap.put("gsmbMemberId", log.getMemberId());
            dataMap.put("gsmbcCardId", log.getMemberCard());
            List<TokenUserDto> userDtoList = sdMemberBasicMapper.selectMemberInfo(dataMap);
            TokenUserDto userInfo = userDtoList.get(0);
            Integer number = sdMemberBasicMapper.selectUnUseCouponNumber(userInfo.getClient(), userInfo.getGsmbcCardId());
            userInfo.setCoupon(number);
            userInfo.setToken(token);
            userInfo.setBalance("0");
            redisClient.set(token, JSONObject.toJSONString(userInfo));
            map.put("token", token);
            map.put("currentUser", JSONObject.toJSONString(userInfo));
            return map;
        } else {
            map.put("token", token);
            map.put("currentUser", userStr);
            return map;
        }
    }

    /**
     * 0-已激活（缺省）；1-待激活转正（预备会员）
     *
     * @param card
     */
    private void cardStatus(SdMemberCard card) {
        if (card == null || StringUtils.isBlank(card.getClient())) {
            return;
        }
        card.setGsmbcState("0");
        ClSystemPara clSystemPara = clSystemParaMapper.selectOne(new LambdaQueryWrapper<ClSystemPara>()
                .eq(ClSystemPara::getClient, card.getClient())
                .eq(ClSystemPara::getGcspId, "MEM_ONLINE_REGIST_MODE")
        );
        if (clSystemPara == null) {
            return;
        }
        // 0-免费注册并立即生效（缺省）；
        if ("0".equals(clSystemPara.getGcspPara1())) {
            card.setGsmbcState("0");
        }
        // 1-收费注册需线下转正（仁和模式）；
        if ("1".equals(clSystemPara.getGcspPara1())) {
            card.setGsmbcState("1");
        }
        // 2-收费注册线上支付后激活（字段GCSP_PARA2  可配置收费金额）
        if ("2".equals(clSystemPara.getGcspPara1())) {
            card.setGsmbcState("2");
            // TODO 生成订单 暂不实现 2021年12月3日 现场讨论暂不实现 2 的逻辑
        }
    }
}
