package com.gov.mobile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gov.mobile.dto.workWeixin.WorkwechatConfDto;
import com.gov.mobile.entity.WorkwechatConf;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity generator.domain.WorkwechatConf
 */
public interface WorkwechatConfMapper extends BaseMapper<WorkwechatConf> {

    IPage<WorkwechatConfDto> workwechatConfList(Page<WorkwechatConfDto> page, @Param("client") String client,
                                                @Param("gwcName") String gwcName, @Param("gwcId") String gwcId);

    void updateByOldId(WorkwechatConfDto workwechatConf);

    /**
     * 获取配置信息
     *
     * @return
     */
    List<WorkwechatConf> selectConfigList();
}




