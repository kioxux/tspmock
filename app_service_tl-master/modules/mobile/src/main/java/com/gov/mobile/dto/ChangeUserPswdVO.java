package com.gov.mobile.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChangeUserPswdVO {

    @NotBlank(message = "旧密码不能为空")
    private String oldUserPswd;

    @NotBlank(message = "新密码不能为空")
    private String newUserPswd;
}
