package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-06-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_IMAGES")
@ApiModel(value="Images对象", description="")
public class Images extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID")
    @TableField("ID")
    private String id;

    @ApiModelProperty(value = "路径")
    @TableField("PATH")
    private String path;

    @ApiModelProperty(value = "文件名")
    @TableField("FILENAME")
    private String filename;

    @ApiModelProperty(value = "排序")
    @TableField("SORT")
    private Integer sort;


}
