package com.gov.mobile.service;

import com.gov.mobile.entity.OfficialAccounts;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-08-03
 */
public interface IOfficialAccountsService extends SuperService<OfficialAccounts> {

}
