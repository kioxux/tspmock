package com.gov.mobile.mapper;

import com.gov.mobile.entity.SdRechargeCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2020-07-31
 */
public interface SdRechargeCardMapper extends BaseMapper<SdRechargeCard> {

}
