package com.gov.mobile.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gov.mybatis.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-07-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("GAIA_DC_DATA")
@ApiModel(value="DcData对象", description="")
public class DcData extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("CLIENT")
    private String client;

    @ApiModelProperty(value = "DC编码")
    @TableField("DC_CODE")
    private String dcCode;

    @ApiModelProperty(value = "DC名称")
    @TableField("DC_NAME")
    private String dcName;

    @ApiModelProperty(value = "助记码")
    @TableField("DC_PYM")
    private String dcPym;

    @ApiModelProperty(value = "DC简称")
    @TableField("DC_SHORT_NAME")
    private String dcShortName;

    @ApiModelProperty(value = "DC地址")
    @TableField("DC_ADD")
    private String dcAdd;

    @ApiModelProperty(value = "DC电话")
    @TableField("DC_TEL")
    private String dcTel;

    @ApiModelProperty(value = "DC状态 0-正常，1-停用")
    @TableField("DC_STATUS")
    private String dcStatus;

    @ApiModelProperty(value = "虚拟仓标记")
    @TableField("DC_INVENT")
    private String dcInvent;

    @ApiModelProperty(value = "是否有批发资质")
    @TableField("DC_WHOLESALE")
    private String dcWholesale;

    @ApiModelProperty(value = "连锁总部")
    @TableField("DC_CHAIN_HEAD")
    private String dcChainHead;

    @ApiModelProperty(value = "配送平均天数")
    @TableField("DC_DELIVERY_DAYS")
    private String dcDeliveryDays;

    @ApiModelProperty(value = "纳税主体")
    @TableField("DC_TAX_SUBJECT")
    private String dcTaxSubject;

    @ApiModelProperty(value = "20-连锁 30-批发公司")
    @TableField("DC_TYPE")
    private String dcType;

    @ApiModelProperty(value = "部门负责人ID")
    @TableField("DC_HEAD_ID")
    private String dcHeadId;

    @ApiModelProperty(value = "部门负责人姓名")
    @TableField("DC_HEAD_NAM")
    private String dcHeadNam;

    @ApiModelProperty(value = "创建日期")
    @TableField("DC_CRE_DATE")
    private String dcCreDate;

    @ApiModelProperty(value = "创建时间")
    @TableField("DC_CRE_TIME")
    private String dcCreTime;

    @ApiModelProperty(value = "创建人账号")
    @TableField("DC_CRE_ID")
    private String dcCreId;

    @ApiModelProperty(value = "修改日期")
    @TableField("DC_MODI_DATE")
    private String dcModiDate;

    @ApiModelProperty(value = "修改时间")
    @TableField("DC_MODI_TIME")
    private String dcModiTime;

    @ApiModelProperty(value = "修改人账号")
    @TableField("DC_MODI_ID")
    private String dcModiId;

    @ApiModelProperty(value = "统一社会信用代码")
    @TableField("DC_NO")
    private String dcNo;

    @ApiModelProperty(value = "法人/负责人")
    @TableField("DC_LEGAL_PERSON")
    private String dcLegalPerson;

    @ApiModelProperty(value = "质量负责人")
    @TableField("DC_QUA")
    private String dcQua;


}
