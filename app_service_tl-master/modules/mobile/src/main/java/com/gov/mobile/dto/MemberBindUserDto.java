package com.gov.mobile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "用户绑定")
public class MemberBindUserDto {

    @NotBlank(message = "加盟商不能为空")
    @ApiModelProperty(value = "加盟商id")
    private String client;

    @NotBlank(message = "openID不能为空")
    @ApiModelProperty(value = "openID")
    private String gsmbOpenId;

    @NotBlank(message = "所属门店不能为空")
    @ApiModelProperty(value = "所属门店")
    private String gsmbBrId;

    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号")
    private String gsmbMobile;

    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value = "验证码")
    private String verificationCode;

}
