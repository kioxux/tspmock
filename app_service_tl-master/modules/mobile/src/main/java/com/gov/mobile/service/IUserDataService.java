package com.gov.mobile.service;

import com.gov.common.response.Result;
import com.gov.mobile.dto.*;
import com.gov.mobile.entity.UserData;
import com.gov.mybatis.SuperService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author sy
 * @since 2020-06-17
 */
public interface IUserDataService extends SuperService<UserData> {

    /**
     * 获取token （手机号+密码）
     */
    Result getTokenByTelPasswd(UserLoginModel userLoginModel);

    /**
     * 获取token （加盟商+用户编号）
     */
    Result getTokenByClient(UserLoginModelClient modelClient);

    /**
     * 获取token（手机号+验证码）
     */
    Result getTokenByTelVerificationCode(VerificationToToken verificationToToken);

    /**
     * 检查手机号（已注册，且在职）
     */
    boolean checkTel(String tel);

    /**
     * 获取用户
     */
    Result getUser();

    /**
     * 修改密码
     */
    Result changeUserPswd(ChangeUserPswdVO changeUserPswdVO);

    /**
     * 检测密码
     */
    Result checkUserPswd(CheckUserPswdVO checkUserPswdVO);

    /**
     * 获取当前用户
     */
    UserLoginModelClient getCurrentUser();

    /**
     * 退出
     */
    Result logout();

    /**
     * 根据加盟商和员工编号获取用户
     */
    UserData getUserByClientAndUserId(String client, String userId);

    /**
     * 根据加盟商和员工手机号获取用户
     */
    UserData getUserByClientAndPhone(String client, String phone);

    /**
     * 获取userID
     * @return
     */
    int selectMaxId(String client);

    void insert(UserData userData);
}
