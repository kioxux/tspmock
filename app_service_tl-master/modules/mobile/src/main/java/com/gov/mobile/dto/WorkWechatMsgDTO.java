package com.gov.mobile.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class WorkWechatMsgDTO {

    private Integer id;

    /**
     * 任务名称
     */
    @NotBlank(message = "任务名称不能为空")
    private String gwmTheme;

    /**
     * 任务说明
     */
    private String gwmRemark;

    /**
     * 任务开始时间
     */
    @NotNull(message = "任务开始时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gwmStartDate;

    /**
     * 任务结束时间
     */
    @NotNull(message = "任务结束时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gwmEndDate;

    /**
     * 任务类型（1-企微关注员工，2-离职继承后员工）
     */
    private String gwmType;

    /**
     * 活动开始时间
     */
    @NotNull(message = "活动开始时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gwmActStartDate;

    /**
     * 活动结束时间
     */
    @NotNull(message = "活动结束时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gwmActEndDate;

    /**
     * 内容
     */
    private String gwmContent;

    /**
     * 推送类型（1-立即推送，2-定时推送）
     */
    private String gwmSendType;

    /**
     * 发送状态（0-待发送，1-发送成功，2-发送失败）
     */
    private String gwmSendStatus;

    /**
     * 预计发送时间
     */
    @NotNull(message = "推送时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gwmEstSendTime;

    /**
     * 消息类型（1-图片，2-图文，3-小程序）
     */
    @NotBlank(message = "推送方式不能为空")
    private String gwmMsgType;

    /**
     * 附件数组json（包括图片，小程序，视频，语音）
     */
    private String gwmAttachement;

    /**
     * 存cos路径键值对
     */
    private String gwmPaths;

    /**
     * 实际发送时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gwmActSendTime;

    /**
     * 创建人
     */
    private String gwmCreateBy;

    /**
     * 创建时间
     */
    private Date gwmCreateTime;

    @NotNull(message = "请选择最少一个会员")
    @Size(min = 0, message = "请选择最少一个会员")
    private List<WorkwechatMsgUserDTO> userList;


}
