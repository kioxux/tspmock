package com.gov.mobile.service;

import com.gov.mobile.entity.SdRechargeCard;
import com.gov.mybatis.SuperService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sy
 * @since 2020-07-31
 */
public interface ISdRechargeCardService extends SuperService<SdRechargeCard> {

}
