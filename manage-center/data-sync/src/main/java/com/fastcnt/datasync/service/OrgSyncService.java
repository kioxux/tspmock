package com.fastcnt.datasync.service;

import com.fastcnt.datasync.domain.TBcDepartment;
import com.fastcnt.datasync.domain.TBcManageUnit;
import com.fastcnt.datasync.domain.TBcOrganizationUnit;
import com.fastcnt.datasync.dto.OrganizationDto;
import com.fastcnt.datasync.feign.UcPullService;
import com.fastcnt.datasync.repository.TBcDepartmentRepository;
import com.fastcnt.datasync.repository.TBcManageUnitRepository;
import com.fastcnt.datasync.repository.TBcOrganizationUnitRepository;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class OrgSyncService {
    private static final Logger log = LoggerFactory.getLogger(OrgSyncService.class);

    @Resource
    TBcDepartmentRepository tBcDepartmentRepository;

    @Resource
    TBcManageUnitRepository tBcManageUnitRepository;

    @Resource
    TBcOrganizationUnitRepository tBcOrganizationUnitRepository;

    @Resource
    UcPullService ucPullService;

    @Value("#{'${org.manage.level}'.split(',')}")
    List<Integer> orgList;

    private static final String MANAGE = "m_";

    private static final String ORG = "o_";

    private static final String DEPT = "d_";

    public List<OrganizationDto> initOrgList() {
        List<OrganizationDto> list = new ArrayList<>();

        TBcManageUnit tBcManageUnit = this.tBcManageUnitRepository.findById(Integer.valueOf(1)).orElse(null);
        if (tBcManageUnit != null) {
            list.add(new OrganizationDto("m_" + tBcManageUnit.getFid(), tBcManageUnit.getfName(), null, null, tBcManageUnit.getfState(), "118502", tBcManageUnit.getfLevel()));
        }


        List<TBcManageUnit> allManageData = this.tBcManageUnitRepository.findAllManageData(this.orgList);
        if (allManageData != null && allManageData.size() > 0) {


            List<TBcOrganizationUnit> allByFMuIdData = this.tBcOrganizationUnitRepository.findAllByFMuIdData((List<Integer>) allManageData.stream().map(TBcManageUnit::getFid).collect(Collectors.toList()));
            allManageData.forEach(m -> list.add(new OrganizationDto("m_" + m.getFid(), m.getfName(), "m_" + m.getfParentId(), null, m.getfState(), "118502", m.getfLevel())));


            allByFMuIdData.forEach(m -> list.add(new OrganizationDto("o_" + m.getFid(), m.getfName(), "m_" + m.getfManageUnitId(), null, m.getfState(), (m.getfManageUnitId().intValue() != 2) ? "118503" : "118502", Integer.valueOf((m.gettBcManageUnit() == null) ? 1 : (m.gettBcManageUnit().getfLevel().intValue() + m.getfLevel().intValue())))));


            List<TBcDepartment> deptList = this.tBcDepartmentRepository.findAllByID(Arrays.asList(new Integer[]{Integer.valueOf(2), Integer.valueOf(1258)}));
            deptList.forEach(d -> list.add(new OrganizationDto("d_" + d.getFid(), d.getfName(), (d.getfParentId().intValue() == 0) ? ("o_" + d.getfOrganizationId()) : ("d_" + d.getfParentId()), null, d.getfState(), "118501", Integer.valueOf(((d.getOrganizationUnit() == null) ? 1 : d.getOrganizationUnit().getfLevel().intValue()) + ((d.getDepartment() == null) ? 2 : d.getDepartment().getfLevel().intValue()) + d.getfLevel().intValue()))));
        }


        return list;
    }


    public List<OrganizationDto> getUpdateOrgList(Integer day) {
        List<OrganizationDto> list = new ArrayList<>();

        TBcManageUnit tBcManageUnit = this.tBcManageUnitRepository.findById(Integer.valueOf(1)).orElse(null);
        if (tBcManageUnit != null) {
            list.add(new OrganizationDto("m_" + tBcManageUnit.getFid(), tBcManageUnit.getfName(), null, null, tBcManageUnit.getfState(), "118502", tBcManageUnit.getfLevel()));
        }


        List<TBcManageUnit> allManageData = this.tBcManageUnitRepository.findAllManageData(this.orgList);


        List<TBcManageUnit> dayManageData = this.tBcManageUnitRepository.findDayManageData(this.orgList, day);
        dayManageData.forEach(m -> list.add(new OrganizationDto("m_" + m.getFid(), m.getfName(), "m_" + m.getfParentId(), null, m.getfState(), "118502", m.getfLevel())));


        if (allManageData != null && allManageData.size() > 0) {


            List<TBcOrganizationUnit> dayData = this.tBcOrganizationUnitRepository.findDayByFMuIdData((List<Integer>) allManageData.stream().map(TBcManageUnit::getFid).collect(Collectors.toList()), day);

            dayData.forEach(m -> list.add(new OrganizationDto("o_" + m.getFid(), m.getfName(), "m_" + m.getfManageUnitId(), null, m.getfState(), (m.getfManageUnitId().intValue() != 2) ? "118503" : "118502", Integer.valueOf((m.gettBcManageUnit() == null) ? 1 : (m.gettBcManageUnit().getfLevel().intValue() + m.getfLevel().intValue())))));
        }


        List<TBcDepartment> deptList = this.tBcDepartmentRepository.findDayByID(Arrays.asList(new Integer[]{Integer.valueOf(2), Integer.valueOf(1258)}), day);
        deptList.forEach(d -> list.add(new OrganizationDto("d_" + d.getFid(), d.getfName(), (d.getfParentId().intValue() == 0) ? ("o_" + d.getfOrganizationId()) : ("d_" + d.getfParentId()), null, d.getfState(), "118501", Integer.valueOf(((d.getOrganizationUnit() == null) ? 1 : d.getOrganizationUnit().getfLevel().intValue()) + ((d.getDepartment() == null) ? 2 : d.getDepartment().getfLevel().intValue()) + d.getfLevel().intValue()))));


        return list;
    }

    public JsonNode pushInitOrgList() {
        log.info("=================初始化组织架构数据开始");
        JsonNode jsonNode = this.ucPullService.pushOrg(initOrgList());
        log.info("推送结果：{}", jsonNode.toString());
        log.info("=================初始化组织架构数据结束");
        return jsonNode;
    }

    public JsonNode pushUpdateOrgList(Integer day) {
        log.info("=================更新组织架构数据开始");
        JsonNode jsonNode = this.ucPullService.pushOrg(getUpdateOrgList(day));
        log.info("推送结果：{}", jsonNode.toString());
        log.info("=================更新组织架构数据结束");
        return jsonNode;
    }
}
