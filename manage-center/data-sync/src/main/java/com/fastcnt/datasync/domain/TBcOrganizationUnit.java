package com.fastcnt.datasync.domain;

import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.NotFound;

import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Basic;
import javax.persistence.Id;
import java.sql.Timestamp;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Table(name = "t_BC_OrganizationUnit", schema = "dbo", catalog = "zhifu360_OA")
public class TBcOrganizationUnit implements Serializable {
    private static final long serialVersionUID = -9167626124905370711L;
    private int fid;
    private Integer fManageUnitId;
    private String fNumber;
    private String fFullNumber;
    private String fName;
    private String fShortName;
    private String fAddress;
    private String fPhone;
    private String fFax;
    private String fZipCode;
    private Integer fArea;
    private Integer fCity;
    private Timestamp fStartTime;
    private Timestamp fStopTime;
    private String fCapital;
    private String fRegistNo;
    private String fCorporation;
    private String fTaxNo;
    private String fTrade;
    private Timestamp fFoundingTime;
    private String fValidityPeriod;
    private Integer fIsVirtual;
    private String fMemo;
    private Integer fType1;
    private Integer fType2;
    private Integer fType3;
    private Integer fType4;
    private Integer fType5;
    private Integer fParentId;
    private Integer fLevel;
    private Integer fState;
    private Integer fCreateId;
    private Timestamp fCreateTime;
    private Timestamp fModifyTime;
    private Integer iStoreType;
    private String fCitryLevel;
    private String sStoreNo;
    private boolean bIsAllowH8;
    private boolean bIsAllowH8S;
    private boolean bIsAllowH8M;
    private TBcManageUnit tBcManageUnit;

    @Id
    @Basic
    @Column(name = "FID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFid() {
        return this.fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    @Basic
    @Column(name = "FManageUnitID", nullable = true)
    public Integer getfManageUnitId() {
        return this.fManageUnitId;
    }

    public void setfManageUnitId(Integer fManageUnitId) {
        this.fManageUnitId = fManageUnitId;
    }

    @Basic
    @Column(name = "FNumber", nullable = true, length = 80)
    public String getfNumber() {
        return this.fNumber;
    }

    public void setfNumber(String fNumber) {
        this.fNumber = fNumber;
    }

    @Basic
    @Column(name = "FFullNumber", nullable = true, length = 200)
    public String getfFullNumber() {
        return this.fFullNumber;
    }

    public void setfFullNumber(String fFullNumber) {
        this.fFullNumber = fFullNumber;
    }

    @Basic
    @Column(name = "FName", nullable = true, length = 80)
    public String getfName() {
        return this.fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "FShortName", nullable = true, length = 80)
    public String getfShortName() {
        return this.fShortName;
    }

    public void setfShortName(String fShortName) {
        this.fShortName = fShortName;
    }

    @Basic
    @Column(name = "FAddress", nullable = true, length = 200)
    public String getfAddress() {
        return this.fAddress;
    }

    public void setfAddress(String fAddress) {
        this.fAddress = fAddress;
    }

    @Basic
    @Column(name = "FPhone", nullable = true, length = 200)
    public String getfPhone() {
        return this.fPhone;
    }

    public void setfPhone(String fPhone) {
        this.fPhone = fPhone;
    }

    @Basic
    @Column(name = "FFax", nullable = true, length = 200)
    public String getfFax() {
        return this.fFax;
    }

    public void setfFax(String fFax) {
        this.fFax = fFax;
    }

    @Basic
    @Column(name = "FZipCode", nullable = true, length = 200)
    public String getfZipCode() {
        return this.fZipCode;
    }

    public void setfZipCode(String fZipCode) {
        this.fZipCode = fZipCode;
    }

    @Basic
    @Column(name = "FArea", nullable = true)
    public Integer getfArea() {
        return this.fArea;
    }

    public void setfArea(Integer fArea) {
        this.fArea = fArea;
    }

    @Basic
    @Column(name = "FCity", nullable = true)
    public Integer getfCity() {
        return this.fCity;
    }

    public void setfCity(Integer fCity) {
        this.fCity = fCity;
    }

    @Basic
    @Column(name = "FStartTime", nullable = true)
    public Timestamp getfStartTime() {
        return this.fStartTime;
    }

    public void setfStartTime(Timestamp fStartTime) {
        this.fStartTime = fStartTime;
    }

    @Basic
    @Column(name = "FStopTime", nullable = true)
    public Timestamp getfStopTime() {
        return this.fStopTime;
    }

    public void setfStopTime(Timestamp fStopTime) {
        this.fStopTime = fStopTime;
    }

    @Basic
    @Column(name = "FCapital", nullable = true, length = 200)
    public String getfCapital() {
        return this.fCapital;
    }

    public void setfCapital(String fCapital) {
        this.fCapital = fCapital;
    }

    @Basic
    @Column(name = "FRegistNo", nullable = true, length = 200)
    public String getfRegistNo() {
        return this.fRegistNo;
    }

    public void setfRegistNo(String fRegistNo) {
        this.fRegistNo = fRegistNo;
    }

    @Basic
    @Column(name = "FCorporation", nullable = true, length = 200)
    public String getfCorporation() {
        return this.fCorporation;
    }

    public void setfCorporation(String fCorporation) {
        this.fCorporation = fCorporation;
    }

    @Basic
    @Column(name = "FTaxNo", nullable = true, length = 200)
    public String getfTaxNo() {
        return this.fTaxNo;
    }

    public void setfTaxNo(String fTaxNo) {
        this.fTaxNo = fTaxNo;
    }

    @Basic
    @Column(name = "FTrade", nullable = true, length = 200)
    public String getfTrade() {
        return this.fTrade;
    }

    public void setfTrade(String fTrade) {
        this.fTrade = fTrade;
    }

    @Basic
    @Column(name = "FFoundingTime", nullable = true)
    public Timestamp getfFoundingTime() {
        return this.fFoundingTime;
    }

    public void setfFoundingTime(Timestamp fFoundingTime) {
        this.fFoundingTime = fFoundingTime;
    }

    @Basic
    @Column(name = "FValidityPeriod", nullable = true, length = 200)
    public String getfValidityPeriod() {
        return this.fValidityPeriod;
    }

    public void setfValidityPeriod(String fValidityPeriod) {
        this.fValidityPeriod = fValidityPeriod;
    }

    @Basic
    @Column(name = "FIsVirtual", nullable = true)
    public Integer getfIsVirtual() {
        return this.fIsVirtual;
    }

    public void setfIsVirtual(Integer fIsVirtual) {
        this.fIsVirtual = fIsVirtual;
    }

    @Basic
    @Column(name = "FMemo", nullable = true, length = 200)
    public String getfMemo() {
        return this.fMemo;
    }

    public void setfMemo(String fMemo) {
        this.fMemo = fMemo;
    }

    @Basic
    @Column(name = "FType1", nullable = true)
    public Integer getfType1() {
        return this.fType1;
    }

    public void setfType1(Integer fType1) {
        this.fType1 = fType1;
    }

    @Basic
    @Column(name = "FType2", nullable = true)
    public Integer getfType2() {
        return this.fType2;
    }

    public void setfType2(Integer fType2) {
        this.fType2 = fType2;
    }

    @Basic
    @Column(name = "FType3", nullable = true)
    public Integer getfType3() {
        return this.fType3;
    }

    public void setfType3(Integer fType3) {
        this.fType3 = fType3;
    }

    @Basic
    @Column(name = "FType4", nullable = true)
    public Integer getfType4() {
        return this.fType4;
    }

    public void setfType4(Integer fType4) {
        this.fType4 = fType4;
    }

    @Basic
    @Column(name = "FType5", nullable = true)
    public Integer getfType5() {
        return this.fType5;
    }

    public void setfType5(Integer fType5) {
        this.fType5 = fType5;
    }

    @Basic
    @Column(name = "FParentID", nullable = true)
    public Integer getfParentId() {
        return this.fParentId;
    }

    public void setfParentId(Integer fParentId) {
        this.fParentId = fParentId;
    }

    @Basic
    @Column(name = "FLevel", nullable = true)
    public Integer getfLevel() {
        return this.fLevel;
    }

    public void setfLevel(Integer fLevel) {
        this.fLevel = fLevel;
    }

    @Basic
    @Column(name = "FState", nullable = true)
    public Integer getfState() {
        return this.fState;
    }

    public void setfState(Integer fState) {
        this.fState = fState;
    }

    @Basic
    @Column(name = "FCreateID", nullable = true)
    public Integer getfCreateId() {
        return this.fCreateId;
    }

    public void setfCreateId(Integer fCreateId) {
        this.fCreateId = fCreateId;
    }

    @Basic
    @Column(name = "FCreateTime", nullable = true)
    public Timestamp getfCreateTime() {
        return this.fCreateTime;
    }

    public void setfCreateTime(Timestamp fCreateTime) {
        this.fCreateTime = fCreateTime;
    }

    @Basic
    @Column(name = "FModifyTime", nullable = true)
    public Timestamp getfModifyTime() {
        return this.fModifyTime;
    }

    public void setfModifyTime(Timestamp fModifyTime) {
        this.fModifyTime = fModifyTime;
    }

    @Basic
    @Column(name = "iStoreType", nullable = true)
    public Integer getiStoreType() {
        return this.iStoreType;
    }

    public void setiStoreType(Integer iStoreType) {
        this.iStoreType = iStoreType;
    }

    @Basic
    @Column(name = "FCitryLevel", nullable = false, length = 50)
    public String getfCitryLevel() {
        return this.fCitryLevel;
    }

    public void setfCitryLevel(String fCitryLevel) {
        this.fCitryLevel = fCitryLevel;
    }

    @Basic
    @Column(name = "sStoreNo", nullable = false, length = 50)
    public String getsStoreNo() {
        return this.sStoreNo;
    }

    public void setsStoreNo(String sStoreNo) {
        this.sStoreNo = sStoreNo;
    }

    @Basic
    @Column(name = "bIsAllowH8", nullable = false)
    public boolean isbIsAllowH8() {
        return this.bIsAllowH8;
    }

    public void setbIsAllowH8(boolean bIsAllowH8) {
        this.bIsAllowH8 = bIsAllowH8;
    }

    @Basic
    @Column(name = "bIsAllowH8S", nullable = false)
    public boolean isbIsAllowH8S() {
        return this.bIsAllowH8S;
    }

    public void setbIsAllowH8S(boolean bIsAllowH8S) {
        this.bIsAllowH8S = bIsAllowH8S;
    }

    @Basic
    @Column(name = "bIsAllowH8M", nullable = false)
    public boolean isbIsAllowH8M() {
        return this.bIsAllowH8M;
    }

    public void setbIsAllowH8M(boolean bIsAllowH8M) {
        this.bIsAllowH8M = bIsAllowH8M;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH})
    @JoinColumn(name = "FManageUnitID", referencedColumnName = "FID", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    public TBcManageUnit gettBcManageUnit() {
        return this.tBcManageUnit;
    }

    public void settBcManageUnit(TBcManageUnit tBcManageUnit) {
        this.tBcManageUnit = tBcManageUnit;
    }
}
