package com.fastcnt.datasync.domain;

import javax.persistence.Basic;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Table(name = "t_BC_ManageUnit", schema = "dbo", catalog = "zhifu360_OA")
public class TBcManageUnit implements Serializable {
    private static final long serialVersionUID = 383211105508942747L;
    private int fid;
    private String fNumber;
    private String fFullNumber;
    private String fName;
    private Integer fParentId;
    private Integer fLevel;
    private Integer fState;

    @Id
    @Column(name = "FID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFid() {
        return this.fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    @Basic
    @Column(name = "FNumber", nullable = true, length = 80)
    public String getfNumber() {
        return this.fNumber;
    }

    public void setfNumber(String fNumber) {
        this.fNumber = fNumber;
    }

    @Basic
    @Column(name = "FFullNumber", nullable = true, length = 200)
    public String getfFullNumber() {
        return this.fFullNumber;
    }

    public void setfFullNumber(String fFullNumber) {
        this.fFullNumber = fFullNumber;
    }

    @Basic
    @Column(name = "FName", nullable = true, length = 80)
    public String getfName() {
        return this.fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "FParentID", nullable = true)
    public Integer getfParentId() {
        return this.fParentId;
    }

    public void setfParentId(Integer fParentId) {
        this.fParentId = fParentId;
    }

    @Basic
    @Column(name = "FLevel", nullable = true)
    public Integer getfLevel() {
        return this.fLevel;
    }

    public void setfLevel(Integer fLevel) {
        this.fLevel = fLevel;
    }

    @Basic
    @Column(name = "FState", nullable = true)
    public Integer getfState() {
        return this.fState;
    }

    public void setfState(Integer fState) {
        this.fState = fState;
    }
}
