package com.fastcnt.datasync.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_RS_EmpData", schema = "dbo", catalog = "zhifu360_OA")
public class TRsEmpData implements Serializable {
    private static final long serialVersionUID = 4542690224795081748L;

    private int fid;

    private Integer fPositiveId;

    private Integer fOrganizationId;

    private Integer fDepartmentId;

    private String fPostName;

    private String fName;

    private String fSex;

    private Integer fPosition;

    private Integer fCardType;

    private String fCardId;

    private Timestamp fBirthday;

    private String fHuJi;

    private Integer fMinZu;

    private Integer fHeight;

    private String fHealth;

    private String fXueXing;

    private Integer fWeight;

    private Integer fMarital;

    private Integer fhjType;

    private Integer fZhengZhi;

    private String feMail;

    private String fPhone;

    private String fqq;

    private Integer fXueLi;

    private String fbySchool;

    private String fProfessional;

    private Timestamp fbyDate;

    private String fWaiYuLevel;

    private String fHobby;

    private String fhjAddress;

    private String fAddress;

    private Timestamp fWorkTime;

    private Timestamp fEntryDate;

    private String fDangAnAddress;

    private Integer fJinJiContactGx;

    private String fJinJiContact;

    private String fJinJiContactPhone;

    private String fjtNumber;

    private String fGongJiJinNumber;

    private String fZipCode;

    private String fBank;

    private String fBankNumber;

    private Integer fKaoQinType;

    private String fZhuanZheng;

    private Timestamp fZhuanZhengDate;

    private String fBeiJingInfo;

    private BigDecimal fNewMoney;

    private BigDecimal fNewBuTie;

    private BigDecimal fSumMoney;

    private BigDecimal fJiXiaoMoney;

    private Integer fBanZuZhiBiao;

    private Integer fTingFa;

    private Integer fState;

    private Integer fCheckerId;

    private Timestamp fCheckTime;

    private Integer fMakerId;

    private Timestamp fMakerDate;

    private String fPreset1;

    private String fPreset2;

    private String fPreset3;

    private String fPreset4;

    private String fPreset5;

    private String fPreset6;

    private Integer fPreset7;

    private Integer fPreset8;

    private Integer fPreset9;

    private Integer fPreset10;

    private Integer fPreset11;

    private Integer fPreset12;

    private BigDecimal fPreset13;

    private BigDecimal fPreset14;

    private BigDecimal fPreset15;

    private BigDecimal fPreset16;

    private Timestamp fPreset17;

    private Timestamp fPreset18;

    private Timestamp fPreset19;

    private Timestamp fPreset20;

    private String fBank2;

    private String fBankNumber2;

    @Id
    @Column(name = "FID", nullable = false)
    public int getFid() {
        return this.fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    @Basic
    @Column(name = "FPositiveID", nullable = true)
    public Integer getfPositiveId() {
        return this.fPositiveId;
    }

    public void setfPositiveId(Integer fPositiveId) {
        this.fPositiveId = fPositiveId;
    }

    @Basic
    @Column(name = "FOrganizationID", nullable = true)
    public Integer getfOrganizationId() {
        return this.fOrganizationId;
    }

    public void setfOrganizationId(Integer fOrganizationId) {
        this.fOrganizationId = fOrganizationId;
    }

    @Basic
    @Column(name = "FDepartmentID", nullable = true)
    public Integer getfDepartmentId() {
        return this.fDepartmentId;
    }

    public void setfDepartmentId(Integer fDepartmentId) {
        this.fDepartmentId = fDepartmentId;
    }

    @Basic
    @Column(name = "FPostName", nullable = true, length = 50)
    public String getfPostName() {
        return this.fPostName;
    }

    public void setfPostName(String fPostName) {
        this.fPostName = fPostName;
    }

    @Basic
    @Column(name = "FName", nullable = true, length = 50)
    public String getfName() {
        return this.fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "FSex", nullable = true, length = 50)
    public String getfSex() {
        return this.fSex;
    }

    public void setfSex(String fSex) {
        this.fSex = fSex;
    }

    @Basic
    @Column(name = "FPosition", nullable = true)
    public Integer getfPosition() {
        return this.fPosition;
    }

    public void setfPosition(Integer fPosition) {
        this.fPosition = fPosition;
    }

    @Basic
    @Column(name = "FCardType", nullable = true)
    public Integer getfCardType() {
        return this.fCardType;
    }

    public void setfCardType(Integer fCardType) {
        this.fCardType = fCardType;
    }

    @Basic
    @Column(name = "FCardID", nullable = true, length = 50)
    public String getfCardId() {
        return this.fCardId;
    }

    public void setfCardId(String fCardId) {
        this.fCardId = fCardId;
    }

    @Basic
    @Column(name = "FBirthday", nullable = true)
    public Timestamp getfBirthday() {
        return this.fBirthday;
    }

    public void setfBirthday(Timestamp fBirthday) {
        this.fBirthday = fBirthday;
    }

    @Basic
    @Column(name = "FHuJi", nullable = true, length = 100)
    public String getfHuJi() {
        return this.fHuJi;
    }

    public void setfHuJi(String fHuJi) {
        this.fHuJi = fHuJi;
    }

    @Basic
    @Column(name = "FMinZu", nullable = true)
    public Integer getfMinZu() {
        return this.fMinZu;
    }

    public void setfMinZu(Integer fMinZu) {
        this.fMinZu = fMinZu;
    }

    @Basic
    @Column(name = "FHeight", nullable = true)
    public Integer getfHeight() {
        return this.fHeight;
    }

    public void setfHeight(Integer fHeight) {
        this.fHeight = fHeight;
    }

    @Basic
    @Column(name = "FHealth", nullable = true, length = 200)
    public String getfHealth() {
        return this.fHealth;
    }

    public void setfHealth(String fHealth) {
        this.fHealth = fHealth;
    }

    @Basic
    @Column(name = "FXueXing", nullable = true, length = 200)
    public String getfXueXing() {
        return this.fXueXing;
    }

    public void setfXueXing(String fXueXing) {
        this.fXueXing = fXueXing;
    }

    @Basic
    @Column(name = "FWeight", nullable = true)
    public Integer getfWeight() {
        return this.fWeight;
    }

    public void setfWeight(Integer fWeight) {
        this.fWeight = fWeight;
    }

    @Basic
    @Column(name = "FMarital", nullable = true)
    public Integer getfMarital() {
        return this.fMarital;
    }

    public void setfMarital(Integer fMarital) {
        this.fMarital = fMarital;
    }

    @Basic
    @Column(name = "FHJType", nullable = true)
    public Integer getFhjType() {
        return this.fhjType;
    }

    public void setFhjType(Integer fhjType) {
        this.fhjType = fhjType;
    }

    @Basic
    @Column(name = "FZhengZhi", nullable = true)
    public Integer getfZhengZhi() {
        return this.fZhengZhi;
    }

    public void setfZhengZhi(Integer fZhengZhi) {
        this.fZhengZhi = fZhengZhi;
    }

    @Basic
    @Column(name = "FEMail", nullable = true, length = 200)
    public String getFeMail() {
        return this.feMail;
    }

    public void setFeMail(String feMail) {
        this.feMail = feMail;
    }

    @Basic
    @Column(name = "FPhone", nullable = true, length = 50)
    public String getfPhone() {
        return this.fPhone;
    }

    public void setfPhone(String fPhone) {
        this.fPhone = fPhone;
    }

    @Basic
    @Column(name = "FQQ", nullable = true, length = 50)
    public String getFqq() {
        return this.fqq;
    }

    public void setFqq(String fqq) {
        this.fqq = fqq;
    }

    @Basic
    @Column(name = "FXueLi", nullable = true)
    public Integer getfXueLi() {
        return this.fXueLi;
    }

    public void setfXueLi(Integer fXueLi) {
        this.fXueLi = fXueLi;
    }

    @Basic
    @Column(name = "FBYSchool", nullable = true, length = 50)
    public String getFbySchool() {
        return this.fbySchool;
    }

    public void setFbySchool(String fbySchool) {
        this.fbySchool = fbySchool;
    }

    @Basic
    @Column(name = "FProfessional", nullable = true, length = 50)
    public String getfProfessional() {
        return this.fProfessional;
    }

    public void setfProfessional(String fProfessional) {
        this.fProfessional = fProfessional;
    }

    @Basic
    @Column(name = "FBYDate", nullable = true)
    public Timestamp getFbyDate() {
        return this.fbyDate;
    }

    public void setFbyDate(Timestamp fbyDate) {
        this.fbyDate = fbyDate;
    }

    @Basic
    @Column(name = "FWaiYuLevel", nullable = true, length = 50)
    public String getfWaiYuLevel() {
        return this.fWaiYuLevel;
    }

    public void setfWaiYuLevel(String fWaiYuLevel) {
        this.fWaiYuLevel = fWaiYuLevel;
    }

    @Basic
    @Column(name = "FHobby", nullable = true, length = 50)
    public String getfHobby() {
        return this.fHobby;
    }

    public void setfHobby(String fHobby) {
        this.fHobby = fHobby;
    }

    @Basic
    @Column(name = "FHJAddress", nullable = true, length = 200)
    public String getFhjAddress() {
        return this.fhjAddress;
    }

    public void setFhjAddress(String fhjAddress) {
        this.fhjAddress = fhjAddress;
    }

    @Basic
    @Column(name = "FAddress", nullable = true, length = 200)
    public String getfAddress() {
        return this.fAddress;
    }

    public void setfAddress(String fAddress) {
        this.fAddress = fAddress;
    }

    @Basic
    @Column(name = "FWorkTime", nullable = true)
    public Timestamp getfWorkTime() {
        return this.fWorkTime;
    }

    public void setfWorkTime(Timestamp fWorkTime) {
        this.fWorkTime = fWorkTime;
    }

    @Basic
    @Column(name = "FEntryDate", nullable = true)
    public Timestamp getfEntryDate() {
        return this.fEntryDate;
    }

    public void setfEntryDate(Timestamp fEntryDate) {
        this.fEntryDate = fEntryDate;
    }

    @Basic
    @Column(name = "FDangAnAddress", nullable = true, length = 50)
    public String getfDangAnAddress() {
        return this.fDangAnAddress;
    }

    public void setfDangAnAddress(String fDangAnAddress) {
        this.fDangAnAddress = fDangAnAddress;
    }

    @Basic
    @Column(name = "FJinJiContactGX", nullable = true)
    public Integer getfJinJiContactGx() {
        return this.fJinJiContactGx;
    }

    public void setfJinJiContactGx(Integer fJinJiContactGx) {
        this.fJinJiContactGx = fJinJiContactGx;
    }

    @Basic
    @Column(name = "FJinJiContact", nullable = true, length = 50)
    public String getfJinJiContact() {
        return this.fJinJiContact;
    }

    public void setfJinJiContact(String fJinJiContact) {
        this.fJinJiContact = fJinJiContact;
    }

    @Basic
    @Column(name = "FJinJiContactPhone", nullable = true, length = 50)
    public String getfJinJiContactPhone() {
        return this.fJinJiContactPhone;
    }

    public void setfJinJiContactPhone(String fJinJiContactPhone) {
        this.fJinJiContactPhone = fJinJiContactPhone;
    }

    @Basic
    @Column(name = "FJTNumber", nullable = true, length = 50)
    public String getFjtNumber() {
        return this.fjtNumber;
    }

    public void setFjtNumber(String fjtNumber) {
        this.fjtNumber = fjtNumber;
    }

    @Basic
    @Column(name = "FGongJiJinNumber", nullable = true, length = 50)
    public String getfGongJiJinNumber() {
        return this.fGongJiJinNumber;
    }

    public void setfGongJiJinNumber(String fGongJiJinNumber) {
        this.fGongJiJinNumber = fGongJiJinNumber;
    }

    @Basic
    @Column(name = "FZipCode", nullable = true, length = 20)
    public String getfZipCode() {
        return this.fZipCode;
    }

    public void setfZipCode(String fZipCode) {
        this.fZipCode = fZipCode;
    }

    @Basic
    @Column(name = "FBank", nullable = true, length = 50)
    public String getfBank() {
        return this.fBank;
    }

    public void setfBank(String fBank) {
        this.fBank = fBank;
    }

    @Basic
    @Column(name = "FBankNumber", nullable = true, length = 50)
    public String getfBankNumber() {
        return this.fBankNumber;
    }

    public void setfBankNumber(String fBankNumber) {
        this.fBankNumber = fBankNumber;
    }

    @Basic
    @Column(name = "FKaoQinType", nullable = true)
    public Integer getfKaoQinType() {
        return this.fKaoQinType;
    }

    public void setfKaoQinType(Integer fKaoQinType) {
        this.fKaoQinType = fKaoQinType;
    }

    @Basic
    @Column(name = "FZhuanZheng", nullable = true, length = 50)
    public String getfZhuanZheng() {
        return this.fZhuanZheng;
    }

    public void setfZhuanZheng(String fZhuanZheng) {
        this.fZhuanZheng = fZhuanZheng;
    }

    @Basic
    @Column(name = "FZhuanZhengDate", nullable = true)
    public Timestamp getfZhuanZhengDate() {
        return this.fZhuanZhengDate;
    }

    public void setfZhuanZhengDate(Timestamp fZhuanZhengDate) {
        this.fZhuanZhengDate = fZhuanZhengDate;
    }

    @Basic
    @Column(name = "FBeiJingInfo", nullable = true, length = 200)
    public String getfBeiJingInfo() {
        return this.fBeiJingInfo;
    }

    public void setfBeiJingInfo(String fBeiJingInfo) {
        this.fBeiJingInfo = fBeiJingInfo;
    }

    @Basic
    @Column(name = "FNewMoney", nullable = true, precision = 2)
    public BigDecimal getfNewMoney() {
        return this.fNewMoney;
    }

    public void setfNewMoney(BigDecimal fNewMoney) {
        this.fNewMoney = fNewMoney;
    }

    @Basic
    @Column(name = "FNewBuTie", nullable = true, precision = 2)
    public BigDecimal getfNewBuTie() {
        return this.fNewBuTie;
    }

    public void setfNewBuTie(BigDecimal fNewBuTie) {
        this.fNewBuTie = fNewBuTie;
    }

    @Basic
    @Column(name = "FSumMoney", nullable = true, precision = 2)
    public BigDecimal getfSumMoney() {
        return this.fSumMoney;
    }

    public void setfSumMoney(BigDecimal fSumMoney) {
        this.fSumMoney = fSumMoney;
    }

    @Basic
    @Column(name = "FJiXiaoMoney", nullable = true, precision = 2)
    public BigDecimal getfJiXiaoMoney() {
        return this.fJiXiaoMoney;
    }

    public void setfJiXiaoMoney(BigDecimal fJiXiaoMoney) {
        this.fJiXiaoMoney = fJiXiaoMoney;
    }

    @Basic
    @Column(name = "FBanZuZhiBiao", nullable = true)
    public Integer getfBanZuZhiBiao() {
        return this.fBanZuZhiBiao;
    }

    public void setfBanZuZhiBiao(Integer fBanZuZhiBiao) {
        this.fBanZuZhiBiao = fBanZuZhiBiao;
    }

    @Basic
    @Column(name = "FTingFa", nullable = true)
    public Integer getfTingFa() {
        return this.fTingFa;
    }

    public void setfTingFa(Integer fTingFa) {
        this.fTingFa = fTingFa;
    }

    @Basic
    @Column(name = "FState", nullable = true)
    public Integer getfState() {
        return this.fState;
    }

    public void setfState(Integer fState) {
        this.fState = fState;
    }

    @Basic
    @Column(name = "FCheckerID", nullable = true)
    public Integer getfCheckerId() {
        return this.fCheckerId;
    }

    public void setfCheckerId(Integer fCheckerId) {
        this.fCheckerId = fCheckerId;
    }

    @Basic
    @Column(name = "FCheckTime", nullable = true)
    public Timestamp getfCheckTime() {
        return this.fCheckTime;
    }

    public void setfCheckTime(Timestamp fCheckTime) {
        this.fCheckTime = fCheckTime;
    }

    @Basic
    @Column(name = "FMakerID", nullable = true)
    public Integer getfMakerId() {
        return this.fMakerId;
    }

    public void setfMakerId(Integer fMakerId) {
        this.fMakerId = fMakerId;
    }

    @Basic
    @Column(name = "FMakerDate", nullable = true)
    public Timestamp getfMakerDate() {
        return this.fMakerDate;
    }

    public void setfMakerDate(Timestamp fMakerDate) {
        this.fMakerDate = fMakerDate;
    }

    @Basic
    @Column(name = "FPreset1", nullable = true, length = 200)
    public String getfPreset1() {
        return this.fPreset1;
    }

    public void setfPreset1(String fPreset1) {
        this.fPreset1 = fPreset1;
    }

    @Basic
    @Column(name = "FPreset2", nullable = true, length = 200)
    public String getfPreset2() {
        return this.fPreset2;
    }

    public void setfPreset2(String fPreset2) {
        this.fPreset2 = fPreset2;
    }

    @Basic
    @Column(name = "FPreset3", nullable = true, length = 200)
    public String getfPreset3() {
        return this.fPreset3;
    }

    public void setfPreset3(String fPreset3) {
        this.fPreset3 = fPreset3;
    }

    @Basic
    @Column(name = "FPreset4", nullable = true, length = 200)
    public String getfPreset4() {
        return this.fPreset4;
    }

    public void setfPreset4(String fPreset4) {
        this.fPreset4 = fPreset4;
    }

    @Basic
    @Column(name = "FPreset5", nullable = true, length = 200)
    public String getfPreset5() {
        return this.fPreset5;
    }

    public void setfPreset5(String fPreset5) {
        this.fPreset5 = fPreset5;
    }

    @Basic
    @Column(name = "FPreset6", nullable = true, length = 200)
    public String getfPreset6() {
        return this.fPreset6;
    }

    public void setfPreset6(String fPreset6) {
        this.fPreset6 = fPreset6;
    }

    @Basic
    @Column(name = "FPreset7", nullable = true)
    public Integer getfPreset7() {
        return this.fPreset7;
    }

    public void setfPreset7(Integer fPreset7) {
        this.fPreset7 = fPreset7;
    }

    @Basic
    @Column(name = "FPreset8", nullable = true)
    public Integer getfPreset8() {
        return this.fPreset8;
    }

    public void setfPreset8(Integer fPreset8) {
        this.fPreset8 = fPreset8;
    }

    @Basic
    @Column(name = "FPreset9", nullable = true)
    public Integer getfPreset9() {
        return this.fPreset9;
    }

    public void setfPreset9(Integer fPreset9) {
        this.fPreset9 = fPreset9;
    }

    @Basic
    @Column(name = "FPreset10", nullable = true)
    public Integer getfPreset10() {
        return this.fPreset10;
    }

    public void setfPreset10(Integer fPreset10) {
        this.fPreset10 = fPreset10;
    }

    @Basic
    @Column(name = "FPreset11", nullable = true)
    public Integer getfPreset11() {
        return this.fPreset11;
    }

    public void setfPreset11(Integer fPreset11) {
        this.fPreset11 = fPreset11;
    }

    @Basic
    @Column(name = "FPreset12", nullable = true)
    public Integer getfPreset12() {
        return this.fPreset12;
    }

    public void setfPreset12(Integer fPreset12) {
        this.fPreset12 = fPreset12;
    }

    @Basic
    @Column(name = "FPreset13", nullable = true, precision = 2)
    public BigDecimal getfPreset13() {
        return this.fPreset13;
    }

    public void setfPreset13(BigDecimal fPreset13) {
        this.fPreset13 = fPreset13;
    }

    @Basic
    @Column(name = "FPreset14", nullable = true, precision = 2)
    public BigDecimal getfPreset14() {
        return this.fPreset14;
    }

    public void setfPreset14(BigDecimal fPreset14) {
        this.fPreset14 = fPreset14;
    }

    @Basic
    @Column(name = "FPreset15", nullable = true, precision = 2)
    public BigDecimal getfPreset15() {
        return this.fPreset15;
    }

    public void setfPreset15(BigDecimal fPreset15) {
        this.fPreset15 = fPreset15;
    }

    @Basic
    @Column(name = "FPreset16", nullable = true, precision = 2)
    public BigDecimal getfPreset16() {
        return this.fPreset16;
    }

    public void setfPreset16(BigDecimal fPreset16) {
        this.fPreset16 = fPreset16;
    }

    @Basic
    @Column(name = "FPreset17", nullable = true)
    public Timestamp getfPreset17() {
        return this.fPreset17;
    }

    public void setfPreset17(Timestamp fPreset17) {
        this.fPreset17 = fPreset17;
    }

    @Basic
    @Column(name = "FPreset18", nullable = true)
    public Timestamp getfPreset18() {
        return this.fPreset18;
    }

    public void setfPreset18(Timestamp fPreset18) {
        this.fPreset18 = fPreset18;
    }

    @Basic
    @Column(name = "FPreset19", nullable = true)
    public Timestamp getfPreset19() {
        return this.fPreset19;
    }

    public void setfPreset19(Timestamp fPreset19) {
        this.fPreset19 = fPreset19;
    }

    @Basic
    @Column(name = "FPreset20", nullable = true)
    public Timestamp getfPreset20() {
        return this.fPreset20;
    }

    public void setfPreset20(Timestamp fPreset20) {
        this.fPreset20 = fPreset20;
    }

    @Basic
    @Column(name = "FBank2", nullable = true, length = 200)
    public String getfBank2() {
        return this.fBank2;
    }

    public void setfBank2(String fBank2) {
        this.fBank2 = fBank2;
    }

    @Basic
    @Column(name = "FBankNumber2", nullable = true, length = 200)
    public String getfBankNumber2() {
        return this.fBankNumber2;
    }

    public void setfBankNumber2(String fBankNumber2) {
        this.fBankNumber2 = fBankNumber2;
    }
}
