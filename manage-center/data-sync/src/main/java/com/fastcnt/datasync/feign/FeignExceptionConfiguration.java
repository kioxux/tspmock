package com.fastcnt.datasync.feign;

import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

public class FeignExceptionConfiguration {
    private static final Logger log = LoggerFactory.getLogger(FeignExceptionConfiguration.class);

    @Bean
    public ErrorDecoder errorDecoder() {
        return new ServeErrorDecoder();
    }




    public class ServeErrorDecoder
            implements ErrorDecoder
    {
        public Exception decode(String methodKey, Response response) {
            try {
                String message = Util.toString(response.body().asReader());
                FeignExceptionConfiguration.log.error("服务器返回：" + message);
                return new RuntimeException(message);
            } catch (IOException iOException) {

                return decode(methodKey, response);
            }
        }
    }
}
