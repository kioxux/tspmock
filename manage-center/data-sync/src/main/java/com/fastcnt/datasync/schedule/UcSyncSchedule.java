package com.fastcnt.datasync.schedule;

import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import com.xxl.job.core.biz.model.ReturnT;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import com.fastcnt.datasync.service.OrgSyncService;

import javax.annotation.Resource;

import com.fastcnt.datasync.service.UserSyncService;
import org.springframework.stereotype.Service;

@Service
public class UcSyncSchedule {
    @Resource
    UserSyncService userSyncService;
    @Resource
    OrgSyncService orgSyncService;
    protected Logger logger;

    public UcSyncSchedule() {
        this.logger = LoggerFactory.getLogger((Class) this.getClass());
    }

    @XxlJob("syncJobHandler")
    public ReturnT<String> syncJobHandler(String param) throws Exception {
        this.logger.info("===========同步数据开始==========" + param);
        XxlJobLogger.log("同步用户数据", new Object[0]);
        this.userSyncService.pushUserList(Integer.valueOf(Integer.parseInt(param)));
        XxlJobLogger.log("同步组织数据", new Object[0]);
        this.orgSyncService.pushUpdateOrgList(Integer.valueOf(Integer.parseInt(param)));
        this.logger.info("===========同步数据结束==========");
        return (ReturnT<String>) ReturnT.SUCCESS;
    }

    @XxlJob("syncEmpDateHandler")
    public ReturnT<String> syncEmpDateHandler(String param) throws Exception {
        this.logger.info("===========同步在职员工数据开始==========" + param);
        XxlJobLogger.log("同步在职员工数据", new Object[0]);
        this.userSyncService.pushWorkingUserList(Integer.valueOf(Integer.parseInt(param)));
        this.logger.info("===========同步在职员工数据结束==========");
        return (ReturnT<String>) ReturnT.SUCCESS;
    }
}
