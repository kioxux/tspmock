package com.fastcnt.datasync.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.GetMapping;
import com.fasterxml.jackson.databind.JsonNode;
import com.fastcnt.datasync.service.OrgSyncService;

import javax.annotation.Resource;

import com.fastcnt.datasync.service.UserSyncService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UcSyncController {
    @Resource
    UserSyncService userSyncService;
    @Resource
    OrgSyncService orgSyncService;

    @GetMapping({"/initOrgData"})
    @ResponseBody
    public String initOrgData() {
        final JsonNode jsonNode = this.orgSyncService.pushInitOrgList();
        return jsonNode.toString();
    }

    @GetMapping({"/updateOrgData/{day}"})
    @ResponseBody
    public String updateOrgData(@PathVariable("day") Integer day) {
        final JsonNode jsonNode = this.orgSyncService.pushUpdateOrgList(day);
        return jsonNode.toString();
    }

    @GetMapping({"/updateUserData/{day}"})
    @ResponseBody
    public String updateUserData(@PathVariable("day") Integer day) {
        final JsonNode jsonNode = this.userSyncService.pushUserList(day);
        return jsonNode.toString();
    }
}
