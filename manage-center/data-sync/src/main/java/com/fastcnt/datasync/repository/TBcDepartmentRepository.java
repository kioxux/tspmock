package com.fastcnt.datasync.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.fastcnt.datasync.domain.TBcDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TBcDepartmentRepository extends JpaRepository<TBcDepartment, Integer>, JpaSpecificationExecutor<TBcDepartment> {
    @Query(value = "with depart_temp (FID,FNumber,FFullNumber,FOrganizationID,FParentID,FName,FState,FLevel) as ( select FID,FNumber,FFullNumber,FOrganizationID,FParentID,FName,FState,FLevel from t_BC_Department where FOrganizationID IN (?1)  union all select a.FID,a.FNumber,a.FFullNumber,a.FParentID,a.FOrganizationID,a.FName,a.FState,a.FLevel from t_BC_Department a inner join depart_temp on a.[FParentID] = depart_temp.[FID] ) select * from depart_temp;", nativeQuery = true)
    List<TBcDepartment> findAllfOId(@Param("ids") List<Integer> ids);

    @Query(value = "with depart_temp (FID,FNumber,FFullNumber,FOrganizationID,FParentID,FName,FState,FLevel) as ( select FID,FNumber,FFullNumber,FOrganizationID,FParentID,FName,FState,FLevel from t_BC_Department where FOrganizationID IN (?1) union all select a.FID,a.FNumber,a.FFullNumber,a.FParentID,a.FOrganizationID,a.FName,a.FState,a.FLevel from t_BC_Department a inner join depart_temp on a.[FParentID] = depart_temp.[FID] ) select * from depart_temp;", nativeQuery = true)
    List<TBcDepartment> findAllByID(@Param("ids") List<Integer> ids);

    @Query(value = "with depart_temp (FID,FNumber,FFullNumber,FOrganizationID,FParentID,FName,FState,FLevel,FCreateTime,FModifyTime,FStopTime) as ( select FID,FNumber,FFullNumber,FOrganizationID,FParentID,FName,FState,FLevel,FCreateTime,FModifyTime,FStopTime from t_BC_Department where FOrganizationID IN (?1) union all select a.FID,a.FNumber,a.FFullNumber,a.FParentID,a.FOrganizationID,a.FName,a.FState,a.FLevel,a.FCreateTime,a.FModifyTime,a.FStopTime from t_BC_Department a inner join depart_temp on a.[FParentID] = depart_temp.[FID] ) select * from depart_temp WHERE DateDiff(dd,FCreateTime,getdate()) = ?2 OR DateDiff(dd,FModifyTime,getdate()) = ?2 OR DateDiff(dd,FStopTime,getdate()) = ?2", nativeQuery = true)
    List<TBcDepartment> findDayByID(@Param("ids") List<Integer> ids, final Integer day);
}
