package com.fastcnt.datasync.domain;

import javax.persistence.Basic;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Table(name = "t_BC_Organization", schema = "dbo", catalog = "zhifu360_OA")
public class TBcOrganization implements Serializable {
    private static final long serialVersionUID = 524799486061739475L;
    private int fid;
    private int fItemId;
    private String fNumber;
    private String fName;
    private String fAddress1;
    private String fAddress2;
    private Integer fRegionId;
    private Integer fTrade;
    private String fContact;
    private String fPhone;
    private String fFax;
    private String fPostalCode;
    private String fEmail;
    private String fBank;
    private String fAccount;
    private String fTaxNum;
    private int fIsCreditMgr;
    private Integer fSaleMode;
    private Double fValueAddRate;
    private String fCountry;
    private String fProvince;
    private String fCity;
    private String fHomePage;
    private String fCorperate;
    private int fCarryingAos;
    private int fTypeId;
    private Integer fSaleId;
    private String fSort;
    private String fMobilePhone;
    private String fCarrier;
    private Timestamp fFileDate;
    private String fInvoiceType;
    private String fMemo;
    private Integer fCyId;
    private Integer fSetId;
    private String farAccountId;
    private String fPreArAcctId;
    private String fOtherArAcctId;
    private String fPayTaxAcctId;
    private String fapAccountId;
    private String fPreApAcctId;
    private String fOtherApAcctId;
    private String fFavorPolicy;
    private int fDepartment;
    private int fEmployee;
    private Timestamp fLastTradeDate;
    private BigDecimal fLastTradeAmount;
    private Timestamp fLastReceiveDate;
    private BigDecimal fLastRpAmount;
    private BigDecimal fMaxDealAmount;
    private double fMinForeReceiveRate;
    private double fMinReserverate;
    private String fNameEn;
    private String fAddrEn;
    private String fciqCode;
    private Integer fRegion;
    private int fState;
    private Timestamp fCreateTime;
    private Timestamp fModifyTime;
    private Integer fOrganizationId;

    @Id
    @Column(name = "FID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFid() {
        return this.fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    @Basic
    @Column(name = "FItemID", nullable = false)
    public int getfItemId() {
        return this.fItemId;
    }

    public void setfItemId(int fItemId) {
        this.fItemId = fItemId;
    }

    @Basic
    @Column(name = "FNumber", nullable = true, length = 255)
    public String getfNumber() {
        return this.fNumber;
    }

    public void setfNumber(String fNumber) {
        this.fNumber = fNumber;
    }

    @Basic
    @Column(name = "FName", nullable = true, length = 80)
    public String getfName() {
        return this.fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "FAddress1", nullable = true, length = 255)
    public String getfAddress1() {
        return this.fAddress1;
    }

    public void setfAddress1(String fAddress1) {
        this.fAddress1 = fAddress1;
    }

    @Basic
    @Column(name = "FAddress2", nullable = true, length = 255)
    public String getfAddress2() {
        return this.fAddress2;
    }

    public void setfAddress2(String fAddress2) {
        this.fAddress2 = fAddress2;
    }

    @Basic
    @Column(name = "FRegionID", nullable = true)
    public Integer getfRegionId() {
        return this.fRegionId;
    }

    public void setfRegionId(Integer fRegionId) {
        this.fRegionId = fRegionId;
    }

    @Basic
    @Column(name = "FTrade", nullable = true)
    public Integer getfTrade() {
        return this.fTrade;
    }

    public void setfTrade(Integer fTrade) {
        this.fTrade = fTrade;
    }

    @Basic
    @Column(name = "FContact", nullable = true, length = 50)
    public String getfContact() {
        return this.fContact;
    }

    public void setfContact(String fContact) {
        this.fContact = fContact;
    }

    @Basic
    @Column(name = "FPhone", nullable = true, length = 40)
    public String getfPhone() {
        return this.fPhone;
    }

    public void setfPhone(String fPhone) {
        this.fPhone = fPhone;
    }

    @Basic
    @Column(name = "FFax", nullable = true, length = 40)
    public String getfFax() {
        return this.fFax;
    }

    public void setfFax(String fFax) {
        this.fFax = fFax;
    }

    @Basic
    @Column(name = "FPostalCode", nullable = true, length = 20)
    public String getfPostalCode() {
        return this.fPostalCode;
    }

    public void setfPostalCode(String fPostalCode) {
        this.fPostalCode = fPostalCode;
    }

    @Basic
    @Column(name = "FEmail", nullable = true, length = 40)
    public String getfEmail() {
        return this.fEmail;
    }

    public void setfEmail(String fEmail) {
        this.fEmail = fEmail;
    }

    @Basic
    @Column(name = "FBank", nullable = true, length = 255)
    public String getfBank() {
        return this.fBank;
    }

    public void setfBank(String fBank) {
        this.fBank = fBank;
    }

    @Basic
    @Column(name = "FAccount", nullable = true, length = 80)
    public String getfAccount() {
        return this.fAccount;
    }

    public void setfAccount(String fAccount) {
        this.fAccount = fAccount;
    }

    @Basic
    @Column(name = "FTaxNum", nullable = true, length = 50)
    public String getfTaxNum() {
        return this.fTaxNum;
    }

    public void setfTaxNum(String fTaxNum) {
        this.fTaxNum = fTaxNum;
    }

    @Basic
    @Column(name = "FIsCreditMgr", nullable = false)
    public int getfIsCreditMgr() {
        return this.fIsCreditMgr;
    }

    public void setfIsCreditMgr(int fIsCreditMgr) {
        this.fIsCreditMgr = fIsCreditMgr;
    }

    @Basic
    @Column(name = "FSaleMode", nullable = true)
    public Integer getfSaleMode() {
        return this.fSaleMode;
    }

    public void setfSaleMode(Integer fSaleMode) {
        this.fSaleMode = fSaleMode;
    }

    @Basic
    @Column(name = "FValueAddRate", nullable = true, precision = 0)
    public Double getfValueAddRate() {
        return this.fValueAddRate;
    }

    public void setfValueAddRate(Double fValueAddRate) {
        this.fValueAddRate = fValueAddRate;
    }

    @Basic
    @Column(name = "FCountry", nullable = true, length = 80)
    public String getfCountry() {
        return this.fCountry;
    }

    public void setfCountry(String fCountry) {
        this.fCountry = fCountry;
    }

    @Basic
    @Column(name = "FProvince", nullable = true, length = 80)
    public String getfProvince() {
        return this.fProvince;
    }

    public void setfProvince(String fProvince) {
        this.fProvince = fProvince;
    }

    @Basic
    @Column(name = "FCity", nullable = true, length = 80)
    public String getfCity() {
        return this.fCity;
    }

    public void setfCity(String fCity) {
        this.fCity = fCity;
    }

    @Basic
    @Column(name = "FHomePage", nullable = true, length = 80)
    public String getfHomePage() {
        return this.fHomePage;
    }

    public void setfHomePage(String fHomePage) {
        this.fHomePage = fHomePage;
    }

    @Basic
    @Column(name = "FCorperate", nullable = true, length = 80)
    public String getfCorperate() {
        return this.fCorperate;
    }

    public void setfCorperate(String fCorperate) {
        this.fCorperate = fCorperate;
    }

    @Basic
    @Column(name = "FCarryingAOS", nullable = false)
    public int getfCarryingAos() {
        return this.fCarryingAos;
    }

    public void setfCarryingAos(int fCarryingAos) {
        this.fCarryingAos = fCarryingAos;
    }

    @Basic
    @Column(name = "FTypeID", nullable = false)
    public int getfTypeId() {
        return this.fTypeId;
    }

    public void setfTypeId(int fTypeId) {
        this.fTypeId = fTypeId;
    }

    @Basic
    @Column(name = "FSaleID", nullable = true)
    public Integer getfSaleId() {
        return this.fSaleId;
    }

    public void setfSaleId(Integer fSaleId) {
        this.fSaleId = fSaleId;
    }

    @Basic
    @Column(name = "FSort", nullable = true, length = 80)
    public String getfSort() {
        return this.fSort;
    }

    public void setfSort(String fSort) {
        this.fSort = fSort;
    }

    @Basic
    @Column(name = "FMobilePhone", nullable = true, length = 50)
    public String getfMobilePhone() {
        return this.fMobilePhone;
    }

    public void setfMobilePhone(String fMobilePhone) {
        this.fMobilePhone = fMobilePhone;
    }

    @Basic
    @Column(name = "FCarrier", nullable = true, length = 255)
    public String getfCarrier() {
        return this.fCarrier;
    }

    public void setfCarrier(String fCarrier) {
        this.fCarrier = fCarrier;
    }

    @Basic
    @Column(name = "FFileDate", nullable = true)
    public Timestamp getfFileDate() {
        return this.fFileDate;
    }

    public void setfFileDate(Timestamp fFileDate) {
        this.fFileDate = fFileDate;
    }

    @Basic
    @Column(name = "FInvoiceType", nullable = true, length = 80)
    public String getfInvoiceType() {
        return this.fInvoiceType;
    }

    public void setfInvoiceType(String fInvoiceType) {
        this.fInvoiceType = fInvoiceType;
    }

    @Basic
    @Column(name = "FMemo", nullable = true, length = 255)
    public String getfMemo() {
        return this.fMemo;
    }

    public void setfMemo(String fMemo) {
        this.fMemo = fMemo;
    }

    @Basic
    @Column(name = "FCyID", nullable = true)
    public Integer getfCyId() {
        return this.fCyId;
    }

    public void setfCyId(Integer fCyId) {
        this.fCyId = fCyId;
    }

    @Basic
    @Column(name = "FSetID", nullable = true)
    public Integer getfSetId() {
        return this.fSetId;
    }

    public void setfSetId(Integer fSetId) {
        this.fSetId = fSetId;
    }

    @Basic
    @Column(name = "FARAccountID", nullable = true, length = 50)
    public String getFarAccountId() {
        return this.farAccountId;
    }

    public void setFarAccountId(String farAccountId) {
        this.farAccountId = farAccountId;
    }

    @Basic
    @Column(name = "FPreARAcctID", nullable = true, length = 50)
    public String getfPreArAcctId() {
        return this.fPreArAcctId;
    }

    public void setfPreArAcctId(String fPreArAcctId) {
        this.fPreArAcctId = fPreArAcctId;
    }

    @Basic
    @Column(name = "FOtherARAcctID", nullable = true, length = 50)
    public String getfOtherArAcctId() {
        return this.fOtherArAcctId;
    }

    public void setfOtherArAcctId(String fOtherArAcctId) {
        this.fOtherArAcctId = fOtherArAcctId;
    }

    @Basic
    @Column(name = "FPayTaxAcctID", nullable = true, length = 50)
    public String getfPayTaxAcctId() {
        return this.fPayTaxAcctId;
    }

    public void setfPayTaxAcctId(String fPayTaxAcctId) {
        this.fPayTaxAcctId = fPayTaxAcctId;
    }

    @Basic
    @Column(name = "FAPAccountID", nullable = true, length = 50)
    public String getFapAccountId() {
        return this.fapAccountId;
    }

    public void setFapAccountId(String fapAccountId) {
        this.fapAccountId = fapAccountId;
    }

    @Basic
    @Column(name = "FPreAPAcctID", nullable = true, length = 50)
    public String getfPreApAcctId() {
        return this.fPreApAcctId;
    }

    public void setfPreApAcctId(String fPreApAcctId) {
        this.fPreApAcctId = fPreApAcctId;
    }

    @Basic
    @Column(name = "FOtherAPAcctID", nullable = true, length = 50)
    public String getfOtherApAcctId() {
        return this.fOtherApAcctId;
    }

    public void setfOtherApAcctId(String fOtherApAcctId) {
        this.fOtherApAcctId = fOtherApAcctId;
    }

    @Basic
    @Column(name = "FFavorPolicy", nullable = true, length = 255)
    public String getfFavorPolicy() {
        return this.fFavorPolicy;
    }

    public void setfFavorPolicy(String fFavorPolicy) {
        this.fFavorPolicy = fFavorPolicy;
    }

    @Basic
    @Column(name = "FDepartment", nullable = false)
    public int getfDepartment() {
        return this.fDepartment;
    }

    public void setfDepartment(int fDepartment) {
        this.fDepartment = fDepartment;
    }

    @Basic
    @Column(name = "FEmployee", nullable = false)
    public int getfEmployee() {
        return this.fEmployee;
    }

    public void setfEmployee(int fEmployee) {
        this.fEmployee = fEmployee;
    }

    @Basic
    @Column(name = "FLastTradeDate", nullable = true)
    public Timestamp getfLastTradeDate() {
        return this.fLastTradeDate;
    }

    public void setfLastTradeDate(Timestamp fLastTradeDate) {
        this.fLastTradeDate = fLastTradeDate;
    }

    @Basic
    @Column(name = "FLastTradeAmount", nullable = false, precision = 2)
    public BigDecimal getfLastTradeAmount() {
        return this.fLastTradeAmount;
    }

    public void setfLastTradeAmount(BigDecimal fLastTradeAmount) {
        this.fLastTradeAmount = fLastTradeAmount;
    }

    @Basic
    @Column(name = "FLastReceiveDate", nullable = true)
    public Timestamp getfLastReceiveDate() {
        return this.fLastReceiveDate;
    }

    public void setfLastReceiveDate(Timestamp fLastReceiveDate) {
        this.fLastReceiveDate = fLastReceiveDate;
    }

    @Basic
    @Column(name = "FLastRPAmount", nullable = false, precision = 2)
    public BigDecimal getfLastRpAmount() {
        return this.fLastRpAmount;
    }

    public void setfLastRpAmount(BigDecimal fLastRpAmount) {
        this.fLastRpAmount = fLastRpAmount;
    }

    @Basic
    @Column(name = "FMaxDealAmount", nullable = false, precision = 2)
    public BigDecimal getfMaxDealAmount() {
        return this.fMaxDealAmount;
    }

    public void setfMaxDealAmount(BigDecimal fMaxDealAmount) {
        this.fMaxDealAmount = fMaxDealAmount;
    }

    @Basic
    @Column(name = "FMinForeReceiveRate", nullable = false, precision = 0)
    public double getfMinForeReceiveRate() {
        return this.fMinForeReceiveRate;
    }

    public void setfMinForeReceiveRate(double fMinForeReceiveRate) {
        this.fMinForeReceiveRate = fMinForeReceiveRate;
    }

    @Basic
    @Column(name = "FMinReserverate", nullable = false, precision = 0)
    public double getfMinReserverate() {
        return this.fMinReserverate;
    }

    public void setfMinReserverate(double fMinReserverate) {
        this.fMinReserverate = fMinReserverate;
    }

    @Basic
    @Column(name = "FNameEN", nullable = true, length = 255)
    public String getfNameEn() {
        return this.fNameEn;
    }

    public void setfNameEn(String fNameEn) {
        this.fNameEn = fNameEn;
    }

    @Basic
    @Column(name = "FAddrEn", nullable = true, length = 255)
    public String getfAddrEn() {
        return this.fAddrEn;
    }

    public void setfAddrEn(String fAddrEn) {
        this.fAddrEn = fAddrEn;
    }

    @Basic
    @Column(name = "FCIQCode", nullable = true, length = 255)
    public String getFciqCode() {
        return this.fciqCode;
    }

    public void setFciqCode(String fciqCode) {
        this.fciqCode = fciqCode;
    }

    @Basic
    @Column(name = "FRegion", nullable = true)
    public Integer getfRegion() {
        return this.fRegion;
    }

    public void setfRegion(Integer fRegion) {
        this.fRegion = fRegion;
    }

    @Basic
    @Column(name = "FState", nullable = false)
    public int getfState() {
        return this.fState;
    }

    public void setfState(int fState) {
        this.fState = fState;
    }

    @Basic
    @Column(name = "FCreateTime", nullable = true)
    public Timestamp getfCreateTime() {
        return this.fCreateTime;
    }

    public void setfCreateTime(Timestamp fCreateTime) {
        this.fCreateTime = fCreateTime;
    }

    @Basic
    @Column(name = "FModifyTime", nullable = false)
    public Timestamp getfModifyTime() {
        return this.fModifyTime;
    }

    public void setfModifyTime(Timestamp fModifyTime) {
        this.fModifyTime = fModifyTime;
    }

    @Basic
    @Column(name = "FOrganizationID", nullable = true)
    public Integer getfOrganizationId() {
        return this.fOrganizationId;
    }

    public void setfOrganizationId(Integer fOrganizationId) {
        this.fOrganizationId = fOrganizationId;
    }
}
