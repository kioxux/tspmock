package com.fastcnt.datasync.service;

import com.alibaba.druid.util.StringUtils;
import com.fastcnt.datasync.domain.TComUser;
import com.fastcnt.datasync.domain.TRsEmpData;
import com.fastcnt.datasync.dto.UserInfoDto;
import com.fastcnt.datasync.feign.UcPullService;
import com.fastcnt.datasync.repository.TComUserRepository;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class UserSyncService {
    private static final Logger log = LoggerFactory.getLogger(UserSyncService.class);


    @Resource
    TComUserRepository tComUserRepository;


    @Resource
    UcPullService ucPullService;

    private static final String ORG = "o_";

    private static final String DEPT = "d_";


    public List<UserInfoDto> getUpdateUserList(Integer day) {
        List<UserInfoDto> list = new ArrayList<>();
        List<TComUser> thisDayData = this.tComUserRepository.findThisDayData(day);
        thisDayData.forEach(u -> {
            StringBuffer deptId = new StringBuffer();

            if (Objects.equals(Integer.valueOf(2), u.getFOrganizationID()) || Objects.equals(Integer.valueOf(1258), u.getFOrganizationID())) {
                deptId.append("d_").append(u.getFDepartmentID());
            } else {
                deptId.append("o_").append(u.getFOrganizationID());
            }
            String idCard = null;
            String position = null;
            String isOnJob = null;
            String phone = null;
            Iterator<TRsEmpData> it = u.getTRsEmpData().iterator();
            while (it.hasNext()) {
                TRsEmpData empData = it.next();
                idCard = empData.getfCardId();
                position = empData.getfPostName();
                isOnJob = empData.getfHobby();
                phone = empData.getfPhone();
            }
            list.add(new UserInfoDto(u.getFNumber(), u.getFName(), deptId.toString(), u.getFSex(), u.getFEmail(), StringUtils.isEmpty(phone) ? u.getFPhone() : phone, idCard, (u.getUpUser() != null) ? u.getUpUser().getFNumber() : null, u.getIUseState(), position, isOnJob));
        });
        return list;
    }


    public List<UserInfoDto> getWorkingUserList(Integer status) {
        List<UserInfoDto> list = new ArrayList<>();
        List<TComUser> thisDayData = this.tComUserRepository.findWorkingData(status);
        thisDayData.forEach(u -> {
            StringBuffer deptId = new StringBuffer();

            if (Objects.equals(Integer.valueOf(2), u.getFOrganizationID()) || Objects.equals(Integer.valueOf(1258), u.getFOrganizationID())) {
                deptId.append("d_").append(u.getFDepartmentID());
            } else {
                deptId.append("o_").append(u.getFOrganizationID());
            }
            String idCard = null;
            String position = null;
            String isOnJob = null;
            String phone = null;
            Iterator<TRsEmpData> it = u.getTRsEmpData().iterator();
            while (it.hasNext()) {
                TRsEmpData empData = it.next();
                idCard = empData.getfCardId();
                position = empData.getfPostName();
                isOnJob = empData.getfHobby();
                phone = empData.getfPhone();
            }
            list.add(new UserInfoDto(u.getFNumber(), u.getFName(), deptId.toString(), u.getFSex(), u.getFEmail(), StringUtils.isEmpty(phone) ? u.getFPhone() : phone, idCard, (u.getUpUser() != null) ? u.getUpUser().getFNumber() : null, u.getIUseState(), position, isOnJob));
        });
        return list;
    }

    public JsonNode pushUserList(Integer day) {
        log.info("=================更新员工数据开始");
        JsonNode jsonNode = this.ucPullService.pushUser(getUpdateUserList(day));
        log.info("推送结果：{}", jsonNode.toString());
        log.info("=================更新员工数据结束");
        return jsonNode;
    }

    public JsonNode pushWorkingUserList(Integer status) {
        log.info("=================更新在职员工数据开始");
        JsonNode jsonNode = this.ucPullService.pushUser(getWorkingUserList(status));
        log.info("推送在职员工数据结果：{}", jsonNode.toString());
        log.info("=================更新在职员工数据结束");
        return jsonNode;
    }
}
