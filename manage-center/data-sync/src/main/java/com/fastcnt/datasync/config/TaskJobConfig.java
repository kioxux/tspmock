package com.fastcnt.datasync.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class TaskJobConfig {
    private Logger logger = LoggerFactory.getLogger(TaskJobConfig.class);

    @Value("${task.job.admin.addresses}")
    private String adminAddresses;

    @Value("${task.job.accessToken}")
    private String accessToken;

    @Value("${task.job.executor.appname}")
    private String appname;

    @Value("${task.job.executor.address}")
    private String address;

    @Value("${task.job.executor.ip}")
    private String ip;

    @Value("${task.job.executor.port}")
    private int port;

    @Value("${task.job.executor.logpath}")
    private String logPath;

    @Value("${task.job.executor.logretentiondays}")
    private int logRetentionDays;


    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(this.adminAddresses);
        xxlJobSpringExecutor.setAppname(this.appname);
        xxlJobSpringExecutor.setAddress(this.address);
        xxlJobSpringExecutor.setIp(this.ip);
        xxlJobSpringExecutor.setPort(this.port);
        xxlJobSpringExecutor.setAccessToken(this.accessToken);
        xxlJobSpringExecutor.setLogPath(this.logPath);
        xxlJobSpringExecutor.setLogRetentionDays(this.logRetentionDays);
        return xxlJobSpringExecutor;
    }
}
