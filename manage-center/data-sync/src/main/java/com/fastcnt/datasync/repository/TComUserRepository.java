package com.fastcnt.datasync.repository;

import org.springframework.data.jpa.repository.Query;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.fastcnt.datasync.domain.TComUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TComUserRepository extends JpaRepository<TComUser, Integer>, JpaSpecificationExecutor<TComUser> {
    @Query("select user from TComUser user left join TRsEmpData emp on user.fNumber = emp.fjtNumber  where DateDiff(dd,user.fCreateTime,getdate()) = ?1 OR DateDiff(dd,user.fModifyTime,getdate()) = ?1 OR DateDiff(dd,emp.fPreset17,getdate()) = ?1")
    List<TComUser> findThisDayData(Integer day);

    @Query("select user from TComUser user left join TRsEmpData emp on user.fNumber = emp.fjtNumber where iUseState =?1")
    List<TComUser> findWorkingData(Integer iUseState);
}
