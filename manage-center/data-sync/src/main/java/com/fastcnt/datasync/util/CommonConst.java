package com.fastcnt.datasync.util;

public class CommonConst {
    public static final String ORG_TYPE_BRANCHES = "118501";
    public static final String ORG_TYPE_HEADQUARTERS = "118502";
    public static final String ORG_TYPE_STORE = "118503";
    public static final String ORG_TYPE_TEAM = "118504";
}
