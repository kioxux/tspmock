package com.fastcnt.datasync.dto;

import java.io.Serializable;

public class OrganizationDto implements Serializable {
    private static final long serialVersionUID = 4686481127561354114L;
    private String orgCode;
    private String orgName;
    private String upCode;

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    private String orgPath;
    private Integer status;
    private String orgType;
    private Integer levels;

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setUpCode(String upCode) {
        this.upCode = upCode;
    }

    public void setOrgPath(String orgPath) {
        this.orgPath = orgPath;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public void setLevels(Integer levels) {
        this.levels = levels;
    }

    public OrganizationDto() {
    }

    public String getOrgCode() {
        return this.orgCode;
    }

    public String getOrgName() {
        return this.orgName;
    }

    public String getUpCode() {
        return this.upCode;
    }

    public String getOrgPath() {
        return this.orgPath;
    }

    public Integer getStatus() {
        return this.status;
    }

    public String getOrgType() {
        return this.orgType;
    }

    public Integer getLevels() {
        return this.levels;
    }

    public String toString() {
        return "OrganizationDto(orgCode=" + getOrgCode() + ", orgName=" + getOrgName() + ", upCode=" + getUpCode() + ", orgPath=" + getOrgPath() + ", status=" + getStatus() + ", orgType=" + getOrgType() + ", levels=" + getLevels() + ")";
    }

    public OrganizationDto(String orgCode, String orgName, String upCode, String orgPath, Integer status, String orgType, Integer levels) {
        this.orgCode = orgCode;
        this.orgName = orgName;
        this.upCode = upCode;
        this.orgPath = orgPath;
        this.status = status;
        this.orgType = orgType;
        this.levels = levels;
    }
}
