package com.fastcnt.datasync.domain;

import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.NotFound;

import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Basic;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Table(name = "t_BC_Department", schema = "dbo", catalog = "zhifu360_OA")
public class TBcDepartment implements Serializable {
    private static final long serialVersionUID = 336240309023762193L;
    private int fid;
    private String fNumber;
    private String fFullNumber;
    private String fName;
    private Integer fOrganizationId;
    private Integer fParentId;
    private Integer fLevel;
    private Integer fState;
    private TBcOrganizationUnit organizationUnit;
    private TBcDepartment department;

    @Id
    @Column(name = "FID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getFid() {
        return this.fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    @Basic
    @Column(name = "FNumber")
    public String getfNumber() {
        return this.fNumber;
    }

    public void setfNumber(String fNumber) {
        this.fNumber = fNumber;
    }

    @Basic
    @Column(name = "FFullNumber")
    public String getfFullNumber() {
        return this.fFullNumber;
    }

    public void setfFullNumber(String fFullNumber) {
        this.fFullNumber = fFullNumber;
    }

    @Basic
    @Column(name = "FName")
    public String getfName() {
        return this.fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "FOrganizationID")
    public Integer getfOrganizationId() {
        return this.fOrganizationId;
    }

    public void setfOrganizationId(Integer fOrganizationId) {
        this.fOrganizationId = fOrganizationId;
    }

    @Basic
    @Column(name = "FParentID")
    public Integer getfParentId() {
        return this.fParentId;
    }

    public void setfParentId(Integer fParentId) {
        this.fParentId = fParentId;
    }

    @Basic
    @Column(name = "FLevel")
    public Integer getfLevel() {
        return this.fLevel;
    }

    public void setfLevel(Integer fLevel) {
        this.fLevel = fLevel;
    }

    @Basic
    @Column(name = "FState")
    public Integer getfState() {
        return this.fState;
    }

    public void setfState(Integer fState) {
        this.fState = fState;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH})
    @JoinColumn(name = "FOrganizationID", referencedColumnName = "FID", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    public TBcOrganizationUnit getOrganizationUnit() {
        return this.organizationUnit;
    }

    public void setOrganizationUnit(TBcOrganizationUnit organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH})
    @JoinColumn(name = "FParentID", referencedColumnName = "FID", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    public TBcDepartment getDepartment() {
        return this.department;
    }

    public void setDepartment(TBcDepartment department) {
        this.department = department;
    }
}
