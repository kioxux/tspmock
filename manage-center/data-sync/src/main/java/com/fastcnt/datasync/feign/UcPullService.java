package com.fastcnt.datasync.feign;

import com.fastcnt.datasync.dto.OrganizationDto;
import org.springframework.web.bind.annotation.PostMapping;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.web.bind.annotation.RequestBody;
import com.fastcnt.datasync.dto.UserInfoDto;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "${client.feign.uc}", configuration = {FeignExceptionConfiguration.class}, path = "/none/resource")
public interface UcPullService {
    @PostMapping({"/syncUserInfo"})
    JsonNode pushUser(@RequestBody List<UserInfoDto> info);

    @PostMapping({"/syncOrgInfo"})
    JsonNode pushOrg(@RequestBody List<OrganizationDto> info);
}
