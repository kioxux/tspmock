package com.fastcnt.datasync.repository;

import org.springframework.data.jpa.repository.Query;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.fastcnt.datasync.domain.TBcManageUnit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TBcManageUnitRepository extends JpaRepository<TBcManageUnit, Integer>, JpaSpecificationExecutor<TBcManageUnit> {
    @Query(value = "with mu_temp (FID,FNumber,FFullNumber,FParentID,FName,FState,FLevel) as (select FID,FNumber,FFullNumber,FParentID,FName,FState,FLevel from t_BC_ManageUnit where FID IN (?1)union all select a.FID,a.FNumber,a.FFullNumber,a.FParentID,a.FName,a.FState,a.FLevel from t_BC_ManageUnit a inner join mu_temp on a.[FParentID] = mu_temp.[FID] ) select * from mu_temp", nativeQuery = true)
    List<TBcManageUnit> findAllManageData(List<Integer> ids);

    @Query(value = "with mu_temp (FID,FNumber,FFullNumber,FParentID,FName,FState,FLevel,FCreateTime,FModifyTime,FStopTime) as (select FID,FNumber,FFullNumber,FParentID,FName,FState,FLevel,FCreateTime,FModifyTime,FStopTime from t_BC_ManageUnit where FID IN (?1)union all select a.FID,a.FNumber,a.FFullNumber,a.FParentID,a.FName,a.FState,a.FLevel,a.FCreateTime,a.FModifyTime,a.FStopTime from t_BC_ManageUnit a inner join mu_temp on a.[FParentID] = mu_temp.[FID] ) select * from mu_temp WHERE DateDiff(dd,FCreateTime,getdate()) = ?2 OR DateDiff(dd,FModifyTime,getdate()) = ?2 OR DateDiff(dd,FStopTime,getdate()) = ?2", nativeQuery = true)
    List<TBcManageUnit> findDayManageData(List<Integer> ids, final Integer day);
}
