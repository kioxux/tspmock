package com.fastcnt.datasync.domain;

import javax.persistence.OneToMany;
import java.util.Set;

import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.NotFound;

import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;
import java.util.Date;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Basic;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Table(name = "t_Com_User", catalog = "dbo")
public class TComUser implements Serializable {
    private static final long serialVersionUID = 8759294522246661268L;
    @Id
    @Basic(optional = false)
    @Column(name = "FID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer fid;
    @Column(name = "FNumber")
    private String fNumber;
    @Column(name = "FFullNumber")
    private String fFullNumber;
    @Column(name = "FName")
    private String fName;
    @Column(name = "FPassword")
    private String fPassword;
    @Column(name = "FIMEI")
    private String fimei;
    @Column(name = "FMAC")
    private String fmac;
    @Column(name = "FBirthday")
    private Date fBirthday;
    @Column(name = "FOrganizationID")
    private Integer fOrganizationID;
    @Column(name = "FDepartmentID")
    private Integer fDepartmentID;
    @Column(name = "FSex")
    private String fSex;
    @Column(name = "FNation")
    private String fNation;
    @Column(name = "FRecruitment")
    private String fRecruitment;
    @Column(name = "FBlood")
    private String fBlood;
    @Column(name = "FMarriage")
    private String fMarriage;
    @Column(name = "FPolitical")
    private String fPolitical;
    @Column(name = "FInPartyDay")
    private Date fInPartyDay;
    @Column(name = "FAccountNature")
    private String fAccountNature;
    @Column(name = "FAccountLocation")
    private String fAccountLocation;
    @Column(name = "FInWorkDay")
    private Date fInWorkDay;
    @Column(name = "FPhone")
    private String fPhone;
    @Column(name = "FEmail")
    private String fEmail;
    @Column(name = "FOfficeTel")
    private String fOfficeTel;
    @Column(name = "FOfficeAddress")
    private String fOfficeAddress;
    @Column(name = "FTel")
    private String fTel;
    @Column(name = "FAddress")
    private String fAddress;
    @Column(name = "FSocialCard")
    private String fSocialCard;
    @Column(name = "FProvidentFundAccount")
    private String fProvidentFundAccount;
    @Column(name = "FEntryDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fEntryDate;
    @Column(name = "FLevelDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fLevelDate;
    @Column(name = "FIsControl")
    private Integer fIsControl;
    @Column(name = "FIsOperation")
    private Integer fIsOperation;
    @Column(name = "FCfmMessage")
    private Integer fCfmMessage;
    @Column(name = "FCfmIMEI")
    private Integer fCfmIMEI;
    @Column(name = "FCfmMac")
    private Integer fCfmMac;
    @Column(name = "FParentID")
    private Integer fParentID;
    @Column(name = "FFullID")
    private String fFullID;
    @Column(name = "FState")
    private Integer fState;
    @Column(name = "FCreateID")
    private Integer fCreateID;
    @Column(name = "FCreateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fCreateTime;
    @Column(name = "FModifyTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fModifyTime;
    @Column(name = "FStartTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fStartTime;
    @Column(name = "FStopTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fStopTime;
    @Column(name = "FPreset1")
    private String fPreset1;
    @Column(name = "FPreset2")
    private String fPreset2;
    @Column(name = "FPreset3")
    private String fPreset3;
    @Column(name = "FPreset4")
    private String fPreset4;
    @Column(name = "FPreset5")
    private String fPreset5;
    @Column(name = "FPreset6")
    private Integer fPreset6;
    @Column(name = "FPreset7")
    private Integer fPreset7;
    @Column(name = "FPreset8")
    private Integer fPreset8;
    @Column(name = "FPreset9")
    private Integer fPreset9;
    @Column(name = "FPreset10")
    private Integer fPreset10;
    @Column(name = "groupid ")
    private Integer groupid;
    @Column(name = "sOldPassword")
    private String sOldPassword;
    @Column(name = "iUseState")
    private Integer iUseState;
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH})
    @JoinColumn(name = "FParentID", referencedColumnName = "FID", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private TComUser upUser;
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH})
    @JoinColumn(name = "FJTNumber", referencedColumnName = "FNumber", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private Set<TRsEmpData> tRsEmpData;

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public void setFNumber(String fNumber) {
        this.fNumber = fNumber;
    }

    public void setFFullNumber(String fFullNumber) {
        this.fFullNumber = fFullNumber;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public void setFPassword(String fPassword) {
        this.fPassword = fPassword;
    }

    public void setFimei(String fimei) {
        this.fimei = fimei;
    }

    public void setFmac(String fmac) {
        this.fmac = fmac;
    }

    public void setFBirthday(Date fBirthday) {
        this.fBirthday = fBirthday;
    }

    public void setFOrganizationID(Integer fOrganizationID) {
        this.fOrganizationID = fOrganizationID;
    }

    public void setFDepartmentID(Integer fDepartmentID) {
        this.fDepartmentID = fDepartmentID;
    }

    public void setFSex(String fSex) {
        this.fSex = fSex;
    }

    public void setFNation(String fNation) {
        this.fNation = fNation;
    }

    public void setFRecruitment(String fRecruitment) {
        this.fRecruitment = fRecruitment;
    }

    public void setFBlood(String fBlood) {
        this.fBlood = fBlood;
    }

    public void setFMarriage(String fMarriage) {
        this.fMarriage = fMarriage;
    }

    public void setFPolitical(String fPolitical) {
        this.fPolitical = fPolitical;
    }

    public void setFInPartyDay(Date fInPartyDay) {
        this.fInPartyDay = fInPartyDay;
    }

    public void setFAccountNature(String fAccountNature) {
        this.fAccountNature = fAccountNature;
    }

    public void setFAccountLocation(String fAccountLocation) {
        this.fAccountLocation = fAccountLocation;
    }

    public void setFInWorkDay(Date fInWorkDay) {
        this.fInWorkDay = fInWorkDay;
    }

    public void setFPhone(String fPhone) {
        this.fPhone = fPhone;
    }

    public void setFEmail(String fEmail) {
        this.fEmail = fEmail;
    }

    public void setFOfficeTel(String fOfficeTel) {
        this.fOfficeTel = fOfficeTel;
    }

    public void setFOfficeAddress(String fOfficeAddress) {
        this.fOfficeAddress = fOfficeAddress;
    }

    public void setFTel(String fTel) {
        this.fTel = fTel;
    }

    public void setFAddress(String fAddress) {
        this.fAddress = fAddress;
    }

    public void setFSocialCard(String fSocialCard) {
        this.fSocialCard = fSocialCard;
    }

    public void setFProvidentFundAccount(String fProvidentFundAccount) {
        this.fProvidentFundAccount = fProvidentFundAccount;
    }

    public void setFEntryDate(Date fEntryDate) {
        this.fEntryDate = fEntryDate;
    }

    public void setFLevelDate(Date fLevelDate) {
        this.fLevelDate = fLevelDate;
    }

    public void setFIsControl(Integer fIsControl) {
        this.fIsControl = fIsControl;
    }

    public void setFIsOperation(Integer fIsOperation) {
        this.fIsOperation = fIsOperation;
    }

    public void setFCfmMessage(Integer fCfmMessage) {
        this.fCfmMessage = fCfmMessage;
    }

    public void setFCfmIMEI(Integer fCfmIMEI) {
        this.fCfmIMEI = fCfmIMEI;
    }

    public void setFCfmMac(Integer fCfmMac) {
        this.fCfmMac = fCfmMac;
    }

    public void setFParentID(Integer fParentID) {
        this.fParentID = fParentID;
    }

    public void setFFullID(String fFullID) {
        this.fFullID = fFullID;
    }

    public void setFState(Integer fState) {
        this.fState = fState;
    }

    public void setFCreateID(Integer fCreateID) {
        this.fCreateID = fCreateID;
    }

    public void setFCreateTime(Date fCreateTime) {
        this.fCreateTime = fCreateTime;
    }

    public void setFModifyTime(Date fModifyTime) {
        this.fModifyTime = fModifyTime;
    }

    public void setFStartTime(Date fStartTime) {
        this.fStartTime = fStartTime;
    }

    public void setFStopTime(Date fStopTime) {
        this.fStopTime = fStopTime;
    }

    public void setFPreset1(String fPreset1) {
        this.fPreset1 = fPreset1;
    }

    public void setFPreset2(String fPreset2) {
        this.fPreset2 = fPreset2;
    }

    public void setFPreset3(String fPreset3) {
        this.fPreset3 = fPreset3;
    }

    public void setFPreset4(String fPreset4) {
        this.fPreset4 = fPreset4;
    }

    public void setFPreset5(String fPreset5) {
        this.fPreset5 = fPreset5;
    }

    public void setFPreset6(Integer fPreset6) {
        this.fPreset6 = fPreset6;
    }

    public void setFPreset7(Integer fPreset7) {
        this.fPreset7 = fPreset7;
    }

    public void setFPreset8(Integer fPreset8) {
        this.fPreset8 = fPreset8;
    }

    public void setFPreset9(Integer fPreset9) {
        this.fPreset9 = fPreset9;
    }

    public void setFPreset10(Integer fPreset10) {
        this.fPreset10 = fPreset10;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public void setSOldPassword(String sOldPassword) {
        this.sOldPassword = sOldPassword;
    }

    public void setIUseState(Integer iUseState) {
        this.iUseState = iUseState;
    }

    public void setUpUser(TComUser upUser) {
        this.upUser = upUser;
    }

    public void setTRsEmpData(Set<TRsEmpData> tRsEmpData) {
        this.tRsEmpData = tRsEmpData;
    }

    public String toString() {
        return "TComUser(fid=" + getFid() + ", fNumber=" + getFNumber() + ", fFullNumber=" + getFFullNumber() + ", fName=" + getFName() + ", fPassword=" + getFPassword() + ", fimei=" + getFimei() + ", fmac=" + getFmac() + ", fBirthday=" + getFBirthday() + ", fOrganizationID=" + getFOrganizationID() + ", fDepartmentID=" + getFDepartmentID() + ", fSex=" + getFSex() + ", fNation=" + getFNation() + ", fRecruitment=" + getFRecruitment() + ", fBlood=" + getFBlood() + ", fMarriage=" + getFMarriage() + ", fPolitical=" + getFPolitical() + ", fInPartyDay=" + getFInPartyDay() + ", fAccountNature=" + getFAccountNature() + ", fAccountLocation=" + getFAccountLocation() + ", fInWorkDay=" + getFInWorkDay() + ", fPhone=" + getFPhone() + ", fEmail=" + getFEmail() + ", fOfficeTel=" + getFOfficeTel() + ", fOfficeAddress=" + getFOfficeAddress() + ", fTel=" + getFTel() + ", fAddress=" + getFAddress() + ", fSocialCard=" + getFSocialCard() + ", fProvidentFundAccount=" + getFProvidentFundAccount() + ", fEntryDate=" + getFEntryDate() + ", fLevelDate=" + getFLevelDate() + ", fIsControl=" + getFIsControl() + ", fIsOperation=" + getFIsOperation() + ", fCfmMessage=" + getFCfmMessage() + ", fCfmIMEI=" + getFCfmIMEI() + ", fCfmMac=" + getFCfmMac() + ", fParentID=" + getFParentID() + ", fFullID=" + getFFullID() + ", fState=" + getFState() + ", fCreateID=" + getFCreateID() + ", fCreateTime=" + getFCreateTime() + ", fModifyTime=" + getFModifyTime() + ", fStartTime=" + getFStartTime() + ", fStopTime=" + getFStopTime() + ", fPreset1=" + getFPreset1() + ", fPreset2=" + getFPreset2() + ", fPreset3=" + getFPreset3() + ", fPreset4=" + getFPreset4() + ", fPreset5=" + getFPreset5() + ", fPreset6=" + getFPreset6() + ", fPreset7=" + getFPreset7() + ", fPreset8=" + getFPreset8() + ", fPreset9=" + getFPreset9() + ", fPreset10=" + getFPreset10() + ", groupid=" + getGroupid() + ", sOldPassword=" + getSOldPassword() + ", iUseState=" + getIUseState() + ", upUser=" + getUpUser() + ", tRsEmpData=" + getTRsEmpData() + ")";
    }


    public Integer getFid() {
        return this.fid;
    }


    public String getFNumber() {
        return this.fNumber;
    }


    public String getFFullNumber() {
        return this.fFullNumber;
    }


    public String getFName() {
        return this.fName;
    }


    public String getFPassword() {
        return this.fPassword;
    }


    public String getFimei() {
        return this.fimei;
    }


    public String getFmac() {
        return this.fmac;
    }


    public Date getFBirthday() {
        return this.fBirthday;
    }


    public Integer getFOrganizationID() {
        return this.fOrganizationID;
    }


    public Integer getFDepartmentID() {
        return this.fDepartmentID;
    }


    public String getFSex() {
        return this.fSex;
    }


    public String getFNation() {
        return this.fNation;
    }


    public String getFRecruitment() {
        return this.fRecruitment;
    }


    public String getFBlood() {
        return this.fBlood;
    }


    public String getFMarriage() {
        return this.fMarriage;
    }


    public String getFPolitical() {
        return this.fPolitical;
    }


    public Date getFInPartyDay() {
        return this.fInPartyDay;
    }


    public String getFAccountNature() {
        return this.fAccountNature;
    }


    public String getFAccountLocation() {
        return this.fAccountLocation;
    }


    public Date getFInWorkDay() {
        return this.fInWorkDay;
    }


    public String getFPhone() {
        return this.fPhone;
    }


    public String getFEmail() {
        return this.fEmail;
    }


    public String getFOfficeTel() {
        return this.fOfficeTel;
    }


    public String getFOfficeAddress() {
        return this.fOfficeAddress;
    }


    public String getFTel() {
        return this.fTel;
    }


    public String getFAddress() {
        return this.fAddress;
    }


    public String getFSocialCard() {
        return this.fSocialCard;
    }


    public String getFProvidentFundAccount() {
        return this.fProvidentFundAccount;
    }


    public Date getFEntryDate() {
        return this.fEntryDate;
    }


    public Date getFLevelDate() {
        return this.fLevelDate;
    }


    public Integer getFIsControl() {
        return this.fIsControl;
    }


    public Integer getFIsOperation() {
        return this.fIsOperation;
    }


    public Integer getFCfmMessage() {
        return this.fCfmMessage;
    }


    public Integer getFCfmIMEI() {
        return this.fCfmIMEI;
    }


    public Integer getFCfmMac() {
        return this.fCfmMac;
    }


    public Integer getFParentID() {
        return this.fParentID;
    }


    public String getFFullID() {
        return this.fFullID;
    }


    public Integer getFState() {
        return this.fState;
    }


    public Integer getFCreateID() {
        return this.fCreateID;
    }


    public Date getFCreateTime() {
        return this.fCreateTime;
    }


    public Date getFModifyTime() {
        return this.fModifyTime;
    }


    public Date getFStartTime() {
        return this.fStartTime;
    }


    public Date getFStopTime() {
        return this.fStopTime;
    }


    public String getFPreset1() {
        return this.fPreset1;
    }


    public String getFPreset2() {
        return this.fPreset2;
    }


    public String getFPreset3() {
        return this.fPreset3;
    }


    public String getFPreset4() {
        return this.fPreset4;
    }


    public String getFPreset5() {
        return this.fPreset5;
    }


    public Integer getFPreset6() {
        return this.fPreset6;
    }


    public Integer getFPreset7() {
        return this.fPreset7;
    }


    public Integer getFPreset8() {
        return this.fPreset8;
    }


    public Integer getFPreset9() {
        return this.fPreset9;
    }


    public Integer getFPreset10() {
        return this.fPreset10;
    }


    public Integer getGroupid() {
        return this.groupid;
    }


    public String getSOldPassword() {
        return this.sOldPassword;
    }


    public Integer getIUseState() {
        return this.iUseState;
    }


    public TComUser getUpUser() {
        return this.upUser;
    }


    public Set<TRsEmpData> getTRsEmpData() {
        return this.tRsEmpData;
    }
}
