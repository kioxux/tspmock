package com.fastcnt.datasync.repository;

import org.springframework.data.jpa.repository.Query;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.fastcnt.datasync.domain.TBcOrganizationUnit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TBcOrganizationUnitRepository extends JpaRepository<TBcOrganizationUnit, Integer>, JpaSpecificationExecutor<TBcOrganizationUnit> {
    @Query("select org from TBcOrganizationUnit org where fManageUnitId in (?1)")
    List<TBcOrganizationUnit> findAllByFMuIdData(List<Integer> ids);

    @Query("select org from TBcOrganizationUnit org where fManageUnitId in (?1) and DateDiff(dd,fCreateTime,getdate()) = ?2 OR DateDiff(dd,fModifyTime,getdate()) = ?2 OR DateDiff(dd,FStopTime,getdate()) = ?2")
    List<TBcOrganizationUnit> findDayByFMuIdData(List<Integer> ids, final Integer day);
}
