package com.fastcnt.datasync.dto;

import java.io.Serializable;

public class UserInfoDto implements Serializable {
    private static final long serialVersionUID = -1500532228949443080L;
    private String userCode;
    private String userName;
    private String deptId;
    private String sex;
    private String email;

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    private String phoneNo;
    private String idCard;
    private String upUserCode;
    private Integer status;
    private String position;
    private String isOnJob;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public void setUpUserCode(String upUserCode) {
        this.upUserCode = upUserCode;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setIsOnJob(String isOnJob) {
        this.isOnJob = isOnJob;
    }

    public String getUserCode() {
        return this.userCode;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getDeptId() {
        return this.deptId;
    }

    public String getSex() {
        return this.sex;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPhoneNo() {
        return this.phoneNo;
    }

    public String getIdCard() {
        return this.idCard;
    }

    public String getUpUserCode() {
        return this.upUserCode;
    }

    public Integer getStatus() {
        return this.status;
    }

    public String getPosition() {
        return this.position;
    }

    public String getIsOnJob() {
        return this.isOnJob;
    }

    public UserInfoDto(String userCode, String userName, String deptId, String sex, String email, String phoneNo, String idCard, String upUserCode, Integer status, String position, String isOnJob) {
        this.userCode = userCode;
        this.userName = userName;
        this.deptId = deptId;
        this.sex = sex;
        this.email = email;
        this.phoneNo = phoneNo;
        this.idCard = idCard;
        this.upUserCode = upUserCode;
        this.status = status;
        this.position = position;
        this.isOnJob = isOnJob;
    }

    public UserInfoDto() {
    }

    public String toString() {
        return "UserInfoDto(userCode=" + getUserCode() + ", userName=" + getUserName() + ", deptId=" + getDeptId() + ", sex=" + getSex() + ", email=" + getEmail() + ", phoneNo=" + getPhoneNo() + ", idCard=" + getIdCard() + ", upUserCode=" + getUpUserCode() + ", status=" + getStatus() + ", position=" + getPosition() + ", isOnJob=" + getIsOnJob() + ")";
    }
}
