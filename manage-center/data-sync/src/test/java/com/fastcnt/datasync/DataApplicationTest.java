package com.fastcnt.datasync;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DataApplicationTest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    void test() {
        logger.info("test");
    }
}
