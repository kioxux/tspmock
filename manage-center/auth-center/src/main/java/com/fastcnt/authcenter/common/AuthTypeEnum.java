package com.fastcnt.authcenter.common;

public enum AuthTypeEnum {
    OWN(1),
    ALL(2);
    private int index;

    AuthTypeEnum(int idx) {
        this.index = idx;
    }

    public int getIndex() {
        return this.index;
    }
}
