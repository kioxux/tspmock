package com.fastcnt.authcenter.domain;

import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "user_info", catalog = "base")
public class UserInfo implements Serializable {
    private static final long serialVersionUID = -2702299410883596422L;

    @Id
    @Column(name = "user_id")
    @GeneratedValue(generator = "JDBC")
    private Integer userId;
    @Column(name = "comp_code")
    private String compCode;
    @Column(name = "user_code")
    private String userCode;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "up_user_id")
    private Integer upUserId;
    @Column(name = "org_code")
    private String orgCode;
    @Column(name = "biz_code")
    private String bizCode;
    @Column(name = "biz_hall")
    private String bizHall;
    @Column(name = "`password`")
    private String password;
    @Column(name = "is_enabled")
    private Integer isEnabled;
    @Column(name = "is_system")
    private Integer isSystem;
    @Column(name = "is_acc_non_locked")
    private Integer isAccNonLocked;
    @Column(name = "is_pwd_non_expired")
    private Integer isPwdNonExpired;
    @Column(name = "is_acc_non_expired")
    private Integer isAccNonExpired;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "latest_update_time")
    private Date latestUpdateTime;
    @Column(name = "latest_access_time")
    private Date latestAccessTime;
    @Column(name = "attempt_times")
    private Integer attemptTimes;
    @Column(name = "phone")
    private String phone;
    @Column(name = "cellphone")
    private String cellphone;
    @Column(name = "email")
    private String email;
    @Column(name = "update_password_time")
    private Date updatePasswordTime;
    @Column(name = "call_num")
    private String callNum;
    @Column(name = "aisle")
    private String aisle;
    @Column(name = "`position`")
    private String position;
    @Column(name = "team")
    private String team;

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUpUserId(Integer upUserId) {
        this.upUserId = upUserId;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public void setBizCode(String bizCode) {
        this.bizCode = bizCode;
    }

    public void setBizHall(String bizHall) {
        this.bizHall = bizHall;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIsEnabled(Integer isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setIsSystem(Integer isSystem) {
        this.isSystem = isSystem;
    }

    public void setIsAccNonLocked(Integer isAccNonLocked) {
        this.isAccNonLocked = isAccNonLocked;
    }

    public void setIsPwdNonExpired(Integer isPwdNonExpired) {
        this.isPwdNonExpired = isPwdNonExpired;
    }

    public void setIsAccNonExpired(Integer isAccNonExpired) {
        this.isAccNonExpired = isAccNonExpired;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setLatestUpdateTime(Date latestUpdateTime) {
        this.latestUpdateTime = latestUpdateTime;
    }

    public void setLatestAccessTime(Date latestAccessTime) {
        this.latestAccessTime = latestAccessTime;
    }

    public void setAttemptTimes(Integer attemptTimes) {
        this.attemptTimes = attemptTimes;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUpdatePasswordTime(Date updatePasswordTime) {
        this.updatePasswordTime = updatePasswordTime;
    }

    public void setCallNum(String callNum) {
        this.callNum = callNum;
    }

    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public Integer getUserId() {
        return this.userId;
    }


    public String getCompCode() {
        return this.compCode;
    }


    public String getUserCode() {
        return this.userCode;
    }


    public String getUserName() {
        return this.userName;
    }


    public Integer getUpUserId() {
        return this.upUserId;
    }


    public String getOrgCode() {
        return this.orgCode;
    }


    public String getBizCode() {
        return this.bizCode;
    }


    public String getBizHall() {
        return this.bizHall;
    }


    public String getPassword() {
        return this.password;
    }


    public Integer getIsEnabled() {
        return this.isEnabled;
    }


    public Integer getIsSystem() {
        return this.isSystem;
    }


    public Integer getIsAccNonLocked() {
        return this.isAccNonLocked;
    }


    public Integer getIsPwdNonExpired() {
        return this.isPwdNonExpired;
    }


    public Integer getIsAccNonExpired() {
        return this.isAccNonExpired;
    }


    public Date getCreateTime() {
        return this.createTime;
    }


    public Date getLatestUpdateTime() {
        return this.latestUpdateTime;
    }


    public Date getLatestAccessTime() {
        return this.latestAccessTime;
    }


    public Integer getAttemptTimes() {
        return this.attemptTimes;
    }


    public String getPhone() {
        return this.phone;
    }


    public String getCellphone() {
        return this.cellphone;
    }


    public String getEmail() {
        return this.email;
    }


    public Date getUpdatePasswordTime() {
        return this.updatePasswordTime;
    }


    public String getCallNum() {
        return this.callNum;
    }


    public String getAisle() {
        return this.aisle;
    }


    public String getPosition() {
        return this.position;
    }


    public String getTeam() {
        return this.team;
    }


    public UserInfo() {
    }


    public UserInfo(String userCode) {
        this.userCode = userCode;
    }

    public UserInfo(Integer userId, Date latestAccessTime) {
        this.latestAccessTime = latestAccessTime;
        this.userId = userId;
    }

    public String toString() {
        return "UserInfo(userId=" + getUserId() + ", compCode=" + getCompCode() + ", userCode=" + getUserCode() + ", userName=" + getUserName() + ", upUserId=" + getUpUserId() + ", orgCode=" + getOrgCode() + ", bizCode=" + getBizCode() + ", bizHall=" + getBizHall() + ", password=" + getPassword() + ", isEnabled=" + getIsEnabled() + ", isSystem=" + getIsSystem() + ", isAccNonLocked=" + getIsAccNonLocked() + ", isPwdNonExpired=" + getIsPwdNonExpired() + ", isAccNonExpired=" + getIsAccNonExpired() + ", createTime=" + getCreateTime() + ", latestUpdateTime=" + getLatestUpdateTime() + ", latestAccessTime=" + getLatestAccessTime() + ", attemptTimes=" + getAttemptTimes() + ", phone=" + getPhone() + ", cellphone=" + getCellphone() + ", email=" + getEmail() + ", updatePasswordTime=" + getUpdatePasswordTime() + ", callNum=" + getCallNum() + ", aisle=" + getAisle() + ", position=" + getPosition() + ", team=" + getTeam() + ")";
    }
}

