package com.fastcnt.authcenter.common;

public enum UserTypeEnum {
    Agent(1),
    Custom(2);

    UserTypeEnum(int idx) {
        this.index = idx;
    }

    private int index;

    public int getIndex() {
        return this.index;
    }
}
