package com.fastcnt.authcenter.dto;

public class AuthInfoDTO {
    private String userName;
    private String client_id;

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClient_id() {
        return this.client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }
}
