package com.fastcnt.authcenter.controller;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    private final Logger logger = LoggerFactory.getLogger(getClass());


    @RequestMapping(value = {"index.html"}, method = {RequestMethod.GET})
    public String index(HttpServletRequest request) {
        return "index";
    }

    @GetMapping({"/user"})
    public Principal userInfo(Principal principal) {
        return principal;
    }
}
