package com.fastcnt.authcenter.mapper;

import com.fastcnt.authcenter.domain.UserRole;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.MyMapper;

public interface UserRoleMapper extends MyMapper<UserRole> {
    List<Integer> findByUserId(@Param("userId") Integer paramInteger, @Param("compCode") String paramString);
}
