package com.fastcnt.authcenter.common;

public enum StatusEnum {
    Enable(1),
    Disable(2);

    StatusEnum(int idx) {
        this.index = idx;
    }

    private int index;

    public int getIndex() {
        return this.index;
    }
}
