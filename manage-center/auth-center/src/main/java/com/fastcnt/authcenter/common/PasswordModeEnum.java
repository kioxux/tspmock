package com.fastcnt.authcenter.common;

public enum PasswordModeEnum {
    Old(0),
    Common(1);
    private int index;

    PasswordModeEnum(int idx) {
        this.index = idx;
    }

    public int getIndex() {
        return this.index;
    }
}
