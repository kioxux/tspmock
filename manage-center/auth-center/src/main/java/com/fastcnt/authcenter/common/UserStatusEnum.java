package com.fastcnt.authcenter.common;

public enum UserStatusEnum {
    NoEnty(0),
    OnJob(1),
    Suspend(2),
    LeaveJob(3);

    private int index;

    UserStatusEnum(int idx) {
        this.index = idx;
    }

    public int getIndex() {
        return this.index;
    }
}
