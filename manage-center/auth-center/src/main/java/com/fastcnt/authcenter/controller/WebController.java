package com.fastcnt.authcenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.fastcnt.authcenter.dto.ApolloResultDTO;
import com.fastcnt.authcenter.dto.AuthInfoDTO;
import com.fastcnt.authcenter.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {
    @RequestMapping(value = {"/removeToken"}, method = {RequestMethod.POST})
    @ResponseBody
    public ApolloResultDTO logout(@RequestBody JSONObject jsonObject) {
        String accessToken = jsonObject.getString("accessToken");

        this.loginService.logout(accessToken);

        ApolloResultDTO resultDto = new ApolloResultDTO();

        resultDto.setResult(0);

        return resultDto;
    }

    @Autowired
    LoginService loginService;

    @RequestMapping(value = {"/getUserInfo"}, method = {RequestMethod.GET})
    @ResponseBody
    public ApolloResultDTO getUserAuthInfo(String accessToken) {
        ApolloResultDTO resultDTO = new ApolloResultDTO();

        AuthInfoDTO authInfoDTO = this.loginService.findAuthInfo(accessToken);

        if (authInfoDTO != null) {
            resultDTO.setResult(0);
            resultDTO.setData(authInfoDTO);
        } else {

            resultDTO.setResult(99999);
        }

        return resultDTO;
    }
}
