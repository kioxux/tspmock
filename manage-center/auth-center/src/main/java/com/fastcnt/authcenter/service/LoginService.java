package com.fastcnt.authcenter.service;

import com.fastcnt.authcenter.dto.AuthInfoDTO;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;


@Service
public class LoginService {
    @Autowired
    private TokenStore tokenStore;

    public void logout(String accessToken) {
        OAuth2AccessToken oAuth2AccessToken = this.tokenStore.readAccessToken(accessToken);

        if (oAuth2AccessToken == null) {
            return;
        }

        if (oAuth2AccessToken.getRefreshToken() != null) {
            this.tokenStore.removeRefreshToken(oAuth2AccessToken.getRefreshToken());
        }

        this.tokenStore.removeAccessToken(oAuth2AccessToken);
    }


    public AuthInfoDTO findAuthInfo(String accessToken) {
        OAuth2Authentication oAuth2Authentication = this.tokenStore.readAuthentication(accessToken);
        if (oAuth2Authentication == null) {
            return null;
        }
        AuthInfoDTO dto = new AuthInfoDTO();

        dto.setUserName(oAuth2Authentication.getUserAuthentication().getName());
        Object detail = oAuth2Authentication.getUserAuthentication().getDetails();
        if (detail != null && detail instanceof LinkedHashMap) {
            LinkedHashMap detailHashMap = (LinkedHashMap) detail;
            if (detailHashMap.containsKey("client_id")) {
                String clientId = detailHashMap.get("client_id").toString();
                dto.setClient_id(clientId);
            }
        }
        return dto;
    }
}
