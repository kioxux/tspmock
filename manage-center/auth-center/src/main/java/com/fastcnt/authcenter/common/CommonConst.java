package com.fastcnt.authcenter.common;


public class CommonConst {
    public static String USER_CLIENT_ID = "system_user";


    public static String PASSWORD_GRANT_TYPE = "password";


    public static String HASH_SET_KEY = "Dl34(93^3e$t.oTY";


    public static String NEW_HASH_SET_KEY = "GgJ)j0OMm4VjOnKp";


    public static String USER_ROLES_REDIS_KEY = "UserRoles_";


    public static Integer ADMIN_TYPE = Integer.valueOf(1);


    public static int UPDATE_RESULT_SUCCESS = 1;

    public static String WECHATUSER_CLIENT_ID = "wechatuser";
    public static String QQUSER_CLIENT_ID = "qquser";

    public static String SAAS_AGENT_CLIENT_ID = "saas_agent";
    public static String DSITE_AGENT_CLIENT_ID = "dsite_agent";
    public static String SAAS_BASEUSER_CLIENT_ID = "saas_user";

    public static String SAAS_CODE_LOGIN_SOURCE = "qrcode";

    public static String HOUSE_USER_CLIENT_ID = "house_user";
}
