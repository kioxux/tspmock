package com.fastcnt.authcenter.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "rolemenu_permission")
public class RolemenuPermission implements Serializable {
    private static final long serialVersionUID = 7335531599798844539L;

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;
    @Column(name = "role_code")
    private Integer roleCode;
    @Column(name = "menu_code")
    private String menuCode;
    @Column(name = "`status`")
    private Integer status;
    @Column(name = "menu_id")
    private Integer menuId;
    @Column(name = "system_id")
    private Integer systemId;
    @Column(name = "is_deleted")
    private Integer isDeleted;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setRoleCode(Integer roleCode) {
        this.roleCode = roleCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getId() {
        return this.id;
    }


    public Integer getRoleCode() {
        return this.roleCode;
    }


    public String getMenuCode() {
        return this.menuCode;
    }

    public Integer getStatus() {
        return this.status;
    }


    public Integer getMenuId() {
        return this.menuId;
    }


    public Integer getSystemId() {
        return this.systemId;
    }


    public Integer getIsDeleted() {
        return this.isDeleted;
    }

    public String toString() {
        return "RolemenuPermission(id=" + getId() + ", roleCode=" + getRoleCode() + ", menuCode=" + getMenuCode() + ", status=" + getStatus() + ", menuId=" + getMenuId() + ", systemId=" + getSystemId() + ", isDeleted=" + getIsDeleted() + ")";
    }
}

