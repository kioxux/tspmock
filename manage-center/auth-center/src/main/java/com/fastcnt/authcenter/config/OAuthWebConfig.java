package com.fastcnt.authcenter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.SecurityBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsUtils;


@Configuration
@EnableWebSecurity
public class OAuthWebConfig
        extends WebSecurityConfigurerAdapter {
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    protected void configure(HttpSecurity http) throws Exception {
        ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl) ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl) ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl) http.authorizeRequests()
                .antMatchers(new String[]{"/login", "/loginSystem"})).permitAll()
                .requestMatchers(new RequestMatcher[]{CorsUtils::isPreFlightRequest})).permitAll()
                .antMatchers(new String[]{"/api/**"})).authenticated();

        http.csrf().disable();
    }


    public void configure(WebSecurity web) throws Exception {
        ((WebSecurity.IgnoredRequestConfigurer) web.ignoring()
                .antMatchers(new String[]{"/favor.ico"})).requestMatchers(new RequestMatcher[]{CorsUtils::isPreFlightRequest});
    }
}
