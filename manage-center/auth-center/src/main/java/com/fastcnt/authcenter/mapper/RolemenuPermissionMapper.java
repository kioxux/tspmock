package com.fastcnt.authcenter.mapper;

import com.fastcnt.authcenter.domain.RolemenuPermission;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.MyMapper;

public interface RolemenuPermissionMapper extends MyMapper<RolemenuPermission> {
    List<String> findByRoleId(@Param("roleId") List<Integer> paramList);
}
