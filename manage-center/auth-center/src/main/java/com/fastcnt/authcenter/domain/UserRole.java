package com.fastcnt.authcenter.domain;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "user_role")
public class UserRole implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "comp_code")
    private String compCode;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "role_id")
    private Integer roleId;
    @Column(name = "`status`")
    private Integer status;

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCompCode() {
        return this.compCode;
    }


    public Integer getUserId() {
        return this.userId;
    }


    public Integer getRoleId() {
        return this.roleId;
    }


    public Integer getStatus() {
        return this.status;
    }

    public String toString() {
        return "UserRole(compCode=" + getCompCode() + ", userId=" + getUserId() + ", roleId=" + getRoleId() + ", status=" + getStatus() + ")";
    }
}

