package com.fastcnt.authcenter.service;

import com.fastcnt.authcenter.service.login.ThorAgentLoginService;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class SsoAuthProvider
        implements AuthenticationProvider {
    private static final Logger log = LoggerFactory.getLogger(SsoAuthProvider.class);

    @Autowired
    ThorAgentLoginService thorAgentLoginService;

    @Autowired
    private HttpServletRequest request;


    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info("自定义provider调用");
        Object details = authentication.getDetails();
        if (!(details instanceof LinkedHashMap) || !((LinkedHashMap) details).containsKey("grant_type") || !((LinkedHashMap) details).containsKey("client_id")) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            return (Authentication) new UsernamePasswordAuthenticationToken(authentication.getName(), authentication.getCredentials(), authorities);
        }

        String source = this.request.getParameter("source");
        Authentication auth = this.thorAgentLoginService.auth(authentication, source);

        return auth;
    }


    public boolean supports(Class<?> aClass) {
        return true;
    }
}
