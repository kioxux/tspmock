package com.fastcnt.authcenter.service.login;

import com.fastcnt.authcenter.common.CommonConst;
import com.fastcnt.authcenter.common.UserStatusEnum;
import com.fastcnt.authcenter.domain.UserInfo;
import com.fastcnt.authcenter.mapper.RolemenuPermissionMapper;
import com.fastcnt.authcenter.mapper.UserInfoMapper;
import com.fastcnt.authcenter.mapper.UserRoleMapper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class ThorAgentLoginService {
    private static final Logger log = LoggerFactory.getLogger(ThorAgentLoginService.class);

    @Resource
    UserInfoMapper userInfoMapper;

    @Resource
    UserRoleMapper userRoleMapper;
    @Resource
    RolemenuPermissionMapper rolemenuPermissionMapper;
    @Resource
    BCryptPasswordEncoder passwordEncoder;
    @Value("${application.source}")
    List<String> sources;

    public Authentication auth(Authentication authentication, String source) throws AuthenticationException {
        UserInfo userInfo = null;
        List<GrantedAuthority> authorities = new ArrayList<>();

        String userName = authentication.getPrincipal().toString();
        String passWord = null;
        try {
            passWord = new String(Base64.getDecoder().decode(authentication.getCredentials().toString()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UsernameNotFoundException("密码不能为空");
        }


        if (Objects.nonNull(this.sources) && !this.sources.contains(source)) {
            userInfo = (UserInfo) this.userInfoMapper.selectOne(new UserInfo(userName));
            if (userInfo == null) {
                throw new UsernameNotFoundException("用户不存在");
            }

            if (userInfo.getIsEnabled().intValue() == UserStatusEnum.NoEnty.getIndex()) {
                throw new DisabledException("当前用户不可用");
            }
            if (!this.passwordEncoder.matches(passWord, userInfo.getPassword())) {
                throw new BadCredentialsException("密码不正确");
            }
        } else {
            log.warn("======用户：{},通过{}免密登录", userName, source);
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_" + source);
            authorities.add(simpleGrantedAuthority);
            return (Authentication) new UsernamePasswordAuthenticationToken(userName, passWord, authorities);
        }

        this.userInfoMapper.updateByPrimaryKeySelective(new UserInfo(userInfo.getUserId(), new Date()));

        if (userInfo.getIsSystem() == CommonConst.ADMIN_TYPE) {
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_admin");
            authorities.add(simpleGrantedAuthority);
            return (Authentication) new UsernamePasswordAuthenticationToken(userInfo.getUserId(), userInfo.getPassword(), authorities);
        }

        List<Integer> userRoles = this.userRoleMapper.findByUserId(userInfo.getUserId(), userInfo.getCompCode());
        if (Objects.nonNull(userRoles) && userRoles.size() > 0) {

            List<String> roleAuthList = this.rolemenuPermissionMapper.findByRoleId(userRoles);
            if (Objects.nonNull(roleAuthList) && roleAuthList.size() > 0) {
                Set<String> roleSet = new HashSet<>(roleAuthList);

                roleSet.forEach(role -> {
                    SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_" + role);
                    authorities.add(simpleGrantedAuthority);
                });
            }
        }
        return (Authentication) new UsernamePasswordAuthenticationToken(userInfo.getUserId(), userInfo.getPassword(), authorities);
    }
}
