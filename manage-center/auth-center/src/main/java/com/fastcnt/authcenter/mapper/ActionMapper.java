package com.fastcnt.authcenter.mapper;

import com.fastcnt.authcenter.domain.Action;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface ActionMapper {
    List<Action> findByIdList(@Param("idList") List<Integer> paramList);
}
