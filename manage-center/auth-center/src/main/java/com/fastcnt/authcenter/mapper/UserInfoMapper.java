package com.fastcnt.authcenter.mapper;

import com.fastcnt.authcenter.domain.UserInfo;
import tk.mybatis.mapper.MyMapper;

public interface UserInfoMapper extends MyMapper<UserInfo>
{
}
