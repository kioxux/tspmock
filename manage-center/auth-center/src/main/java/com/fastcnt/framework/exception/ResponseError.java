package com.fastcnt.framework.exception;

import java.sql.Timestamp;


public class ResponseError {
    private String status;
    private String error;
    private String message;
    private String exception;
    private String timestamp;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getException() {
        return this.exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    public ResponseError() {
    }

    public ResponseError(String status, String error, String message, String exception) {
        this.status = status;
        this.message = message;
        this.error = error;
        this.exception = exception;
        this.timestamp = String.valueOf((new Timestamp(System.currentTimeMillis())).getTime());
    }

    public static ResponseError build(Type errorType, String message) {
        ResponseError error = new ResponseError();
        error.status = errorType.getStatus();
        error.error = errorType.getError();
        error.message = message;
        error.timestamp = String.valueOf((new Timestamp(System.currentTimeMillis())).getTime());
        return error;
    }

    public enum Type {
        BAD_REQUEST_ERROR("error.badrequest", "Bad request error"),
        INTERNAL_SERVER_ERROR("error.internalserver", "Unexpected server error"),
        VALIDATION_ERROR("error.validation", "Found validation issues");

        private String status;
        private String error;

        Type(String status, String error) {
            this.status = status;
            this.error = error;
        }

        public String getStatus() {
            return this.status;
        }

        public String getError() {
            return this.error;
        }
    }
}
