package com.fastcnt.authcenter;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AuthApplicationTest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    void demo1() {
        logger.info("demo1");
    }
}
