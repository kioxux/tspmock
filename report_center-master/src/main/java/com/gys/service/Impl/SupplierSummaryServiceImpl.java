package com.gys.service.Impl;

import com.gys.common.data.PageInfo;
import com.gys.entity.SupplierSaleMan;
import com.gys.mapper.GaiaSdSaleDMapper;
import com.gys.mapper.SupplierSummaryMapper;
import com.gys.report.entity.*;
import com.gys.service.SupplierSummaryService;
import com.gys.util.CommonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SupplierSummaryServiceImpl implements SupplierSummaryService {

    @Autowired
    SupplierSummaryMapper supplierSummaryMapper;

    @Autowired
    GaiaSdSaleDMapper gaiaSdSaleDMapper;


    @Override
    public List<SupplierSaleMan> getSaleManList(String client ,String supplierCode) {
        return supplierSummaryMapper.querySaleManList(client, supplierCode);
    }

    @Override
    public PageInfo getSupplierSummaryList(SupplierSummaryInData inData) {
        if (inData.getWithSaleMan().equals("1")){
            if (ObjectUtils.isEmpty(inData.getGssCodeList())){
                // 如果业务员code列表为空且开启了业务员按钮,则获取所以业务员
                inData.setGssCodeList(supplierSummaryMapper.querySaleManCodeList(inData.getClient(), inData.getSupplierCode()));
            }
        }else {
            inData.setGssCodeList(null);
        }
        List<ProductSalesBySupplierWithSaleManOutData> productSupplierData = new ArrayList<>();
        ProductSalesBySupplierInData productSalesBySupplierInData = new ProductSalesBySupplierInData();
        BeanUtils.copyProperties(inData,productSalesBySupplierInData);
        PageInfo pageInfo;
        List<SalespersonsSalesDetailsOutData> saleList = gaiaSdSaleDMapper.getProductSalesBySupplier(productSalesBySupplierInData);
        List<ProductSalesBySupplierWithSaleManOutData>  salespersonsSalesDetailsOutData = new ArrayList<ProductSalesBySupplierWithSaleManOutData>();
        if (saleList.size() > 0) {
            productSupplierData = supplierSummaryMapper.getProductSalesBySupplier(inData);
            if (productSupplierData.size() > 0) {
                //根据供应商业务员汇总 获取该商品所有的销售信息
                salespersonsSalesDetailsOutData = summaryWithSupplier(saleList, productSupplierData);
            } else {
                return new PageInfo();
            }
        } else {
            return new PageInfo();
        }
        ProductSalesBySupplierOutTotal supplierOutTotal = new ProductSalesBySupplierOutTotal();
        // 集合列的数据汇总
        for (ProductSalesBySupplierWithSaleManOutData supplierOutData : salespersonsSalesDetailsOutData) {
            supplierOutTotal.setAmountReceivable(CommonUtil.stripTrailingZeros(supplierOutData.getAmountReceivable()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmountReceivable())));
            supplierOutTotal.setAmt(CommonUtil.stripTrailingZeros(supplierOutData.getAmt()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmt())));
            supplierOutTotal.setIncludeTaxSale(CommonUtil.stripTrailingZeros(supplierOutData.getIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getIncludeTaxSale())));
            supplierOutTotal.setGrossProfitMargin(CommonUtil.stripTrailingZeros(supplierOutData.getGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getGrossProfitMargin())));
            supplierOutTotal.setQty(CommonUtil.stripTrailingZeros(supplierOutData.getQty()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getQty())));

        }
        if (supplierOutTotal.getAmt().compareTo(BigDecimal.ZERO) != 0) {
            DecimalFormat df = new DecimalFormat("0.00%");
            supplierOutTotal.setGrossProfitRate(df.format(CommonUtil.stripTrailingZeros(supplierOutTotal.getGrossProfitMargin()).divide(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmt()), BigDecimal.ROUND_HALF_EVEN)));
        }

        pageInfo = new PageInfo(salespersonsSalesDetailsOutData, supplierOutTotal);
        return pageInfo;
    }

    private List<ProductSalesBySupplierWithSaleManOutData> summaryWithSupplier(List<SalespersonsSalesDetailsOutData> saleList,List<ProductSalesBySupplierWithSaleManOutData> productSupplierData){
        ArrayList<ProductSalesBySupplierWithSaleManOutData> productSalesBySupplierWithSaleManOutData = new ArrayList<>();
        for (SalespersonsSalesDetailsOutData saleSum : saleList) {
            //根据商品汇总 获取该商品所有的销售数量
            BigDecimal sumQty = CommonUtil.stripTrailingZeros(saleSum.getQty());
            for (ProductSalesBySupplierWithSaleManOutData supplierOutData : productSupplierData) {
                //用百分比乘各个数据,处理数据将null替换为0
                supplierOutData.setAmountReceivable(ObjectUtils.isEmpty(supplierOutData.getAmountReceivable()) ? BigDecimal.ZERO : supplierOutData.getAmountReceivable());
                supplierOutData.setAmt(ObjectUtils.isEmpty(supplierOutData.getAmt()) ? BigDecimal.ZERO : supplierOutData.getAmt());
                supplierOutData.setIncludeTaxSale(ObjectUtils.isEmpty(supplierOutData.getIncludeTaxSale()) ? BigDecimal.ZERO : supplierOutData.getIncludeTaxSale());
                supplierOutData.setGrossProfitMargin(ObjectUtils.isEmpty(supplierOutData.getGrossProfitMargin()) ? BigDecimal.ZERO : supplierOutData.getGrossProfitMargin());
                if (saleSum.getSelfCode().equals(supplierOutData.getProCode()) && saleSum.getStoCode().equals(supplierOutData.getStoCode())) {
                    //根据批次汇总 获取该商品所有的异动数量
                    // 批次==供应商
                    BigDecimal supQty = CommonUtil.stripTrailingZeros(supplierOutData.getQty());
                    //算出该加盟商销售数量占总销量的百分比
                    BigDecimal rateQty = BigDecimal.ONE;
                    if (sumQty.compareTo(BigDecimal.ZERO) != 0) {
                        rateQty = supQty.divide(sumQty, 10, BigDecimal.ROUND_HALF_EVEN);
                    }
                    supplierOutData.setAmountReceivable(CommonUtil.stripTrailingZeros(saleSum.getAmountReceivable()).multiply(rateQty));//应收金额
                    supplierOutData.setAmt(CommonUtil.stripTrailingZeros(saleSum.getAmt()).multiply(rateQty));//实收金额
                    supplierOutData.setIncludeTaxSale(CommonUtil.stripTrailingZeros(saleSum.getIncludeTaxSale()).multiply(rateQty));//成本额
                    supplierOutData.setGrossProfitMargin(CommonUtil.stripTrailingZeros(saleSum.getGrossProfitMargin()).multiply(rateQty));//毛利额
                    //在错误的情况下   实收金额可能是0  NND
                    if (supplierOutData.getAmt().compareTo(BigDecimal.ZERO) != 0) {
                        supplierOutData.setGrossProfitRate(supplierOutData.getGrossProfitMargin().
                                multiply(new BigDecimal(100)).
                                divide(supplierOutData.getAmt(), 2, BigDecimal.ROUND_HALF_EVEN)
                        );//毛利率=毛利额/实收金额
                    } else {
                        supplierOutData.setGrossProfitRate(BigDecimal.ZERO);
                    }
                }
            }
        }
        // 根据业务员编码分组对值进行累加
        Map<String, List<ProductSalesBySupplierWithSaleManOutData>> collect = productSupplierData.stream().collect(Collectors.groupingBy(item -> item.getSupplierCode() + '_' + item.getGssCode()));
        for (String s : collect.keySet()) {
            ProductSalesBySupplierWithSaleManOutData productSalesBySupplierWithSaleManOutData1 = new ProductSalesBySupplierWithSaleManOutData();
            // 初始化值为0
            productSalesBySupplierWithSaleManOutData1.setQty(BigDecimal.ZERO);
            productSalesBySupplierWithSaleManOutData1.setAmountReceivable(BigDecimal.ZERO);
            productSalesBySupplierWithSaleManOutData1.setAmt(BigDecimal.ZERO);
            productSalesBySupplierWithSaleManOutData1.setIncludeTaxSale(BigDecimal.ZERO);
            productSalesBySupplierWithSaleManOutData1.setGrossProfitMargin(BigDecimal.ZERO);
            // 累加数据
            List<ProductSalesBySupplierWithSaleManOutData> productSalesBySupplierWithSaleManOutData2 = collect.get(s);
            productSalesBySupplierWithSaleManOutData1.setSupplierCode(productSalesBySupplierWithSaleManOutData2.get(0).getSupplierCode());  // 设置供应商编码
            productSalesBySupplierWithSaleManOutData1.setSupplierName(productSalesBySupplierWithSaleManOutData2.get(0).getSupplierName());  // 设置供应商名称
            productSalesBySupplierWithSaleManOutData1.setGssCode(productSalesBySupplierWithSaleManOutData2.get(0).getGssCode());  // 设置业务员编码
            productSalesBySupplierWithSaleManOutData1.setGssName(productSalesBySupplierWithSaleManOutData2.get(0).getGssName());  // 设置业务员名称

            for (ProductSalesBySupplierWithSaleManOutData salesBySupplierWithSaleManOutData : productSalesBySupplierWithSaleManOutData2) {
                productSalesBySupplierWithSaleManOutData1.setQty(productSalesBySupplierWithSaleManOutData1.getQty().add(salesBySupplierWithSaleManOutData.getQty()));   // 数量
                productSalesBySupplierWithSaleManOutData1.setAmountReceivable(productSalesBySupplierWithSaleManOutData1.getAmountReceivable().add(salesBySupplierWithSaleManOutData.getAmountReceivable()));    // 应收金额
                productSalesBySupplierWithSaleManOutData1.setAmt(productSalesBySupplierWithSaleManOutData1.getAmt().add(salesBySupplierWithSaleManOutData.getAmt()));    // 实收金额
                productSalesBySupplierWithSaleManOutData1.setIncludeTaxSale(productSalesBySupplierWithSaleManOutData1.getIncludeTaxSale().add(salesBySupplierWithSaleManOutData.getIncludeTaxSale()));    // 成本额
                productSalesBySupplierWithSaleManOutData1.setGrossProfitMargin(productSalesBySupplierWithSaleManOutData1.getGrossProfitMargin().add(salesBySupplierWithSaleManOutData.getGrossProfitMargin()));  //毛利额
            }
            //在错误的情况下   实收金额可能是0  NND
            if (productSalesBySupplierWithSaleManOutData1.getAmt().compareTo(BigDecimal.ZERO) != 0) {
                productSalesBySupplierWithSaleManOutData1.setGrossProfitRate(productSalesBySupplierWithSaleManOutData1.getGrossProfitMargin().
                        multiply(new BigDecimal(100)).
                        divide(productSalesBySupplierWithSaleManOutData1.getAmt(), 2, BigDecimal.ROUND_HALF_EVEN)
                );//毛利率=毛利额/实收金额
            } else {
                productSalesBySupplierWithSaleManOutData1.setGrossProfitRate(BigDecimal.ZERO);
            }
            productSalesBySupplierWithSaleManOutData.add(productSalesBySupplierWithSaleManOutData1);
        }
        return productSalesBySupplierWithSaleManOutData;
    }
}
