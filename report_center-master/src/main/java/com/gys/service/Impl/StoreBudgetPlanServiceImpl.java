package com.gys.service.Impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.StoreAttributeEnum;
import com.gys.common.enums.StoreDTPEnum;
import com.gys.common.enums.StoreMedicalEnum;
import com.gys.common.enums.StoreTaxClassEnum;
import com.gys.common.exception.BusinessException;
import com.gys.entity.data.StoreBudgetPlanInData;
import com.gys.mapper.GaiaSdStoresGroupMapper;
import com.gys.mapper.StoreBudgetPlanMapper;
import com.gys.report.entity.GaiaStoreCategoryType;
import com.gys.report.entity.StoreBudgetPlanByStoCodeOutData;
import com.gys.report.entity.StoreBudgetPlanOutData;
import com.gys.report.entity.StoreDiscountSummaryData;
import com.gys.service.StoreBudgetPlanService;
import com.gys.util.ValidateUtil;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StoreBudgetPlanServiceImpl implements StoreBudgetPlanService {
    @Autowired
    private StoreBudgetPlanMapper storeBudgetPlanMapper;
    @Resource
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;

    @Override
    public PageInfo findStoreBudgePlanByDate(StoreBudgetPlanInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        List<Map<String, Object>> outData = storeBudgetPlanMapper.findStoreBudgePlanByDate(inData);

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {

            // 集合列的数据汇总
            // Map<String, Object> outSto = storeBudgetPlanMapper.findStoreBudgePlanByTotal(inData);
            // Long stoCount = (Long) outSto.get("stoCount");

            Map<String, Object> outTotal = new HashMap<>();
            for (Map<String, Object> out : outData) {
                if (outTotal.containsKey("amountReceivable")) {
                    BigDecimal amountReceivable = new BigDecimal(outTotal.get("amountReceivable").toString()).add(new BigDecimal(out.get("amountReceivable").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("amountReceivable", amountReceivable);
                } else {
                    outTotal.put("amountReceivable", out.get("amountReceivable").toString());
                }
                if (outTotal.containsKey("amt")) {
                    BigDecimal amt = new BigDecimal(outTotal.get("amt").toString()).add(new BigDecimal(out.get("amt").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("amt", amt);
                } else {
                    outTotal.put("amt", out.get("amt").toString());
                }
                if (outTotal.containsKey("movPrices")) {
                    BigDecimal movPrices = new BigDecimal(outTotal.get("movPrices").toString()).add(new BigDecimal(out.get("movPrices").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("movPrices", movPrices);
                } else {
                    outTotal.put("movPrices", out.get("movPrices").toString());
                }
                if (outTotal.containsKey("grossProfitMargin")) {
                    BigDecimal grossProfitMargin = new BigDecimal(outTotal.get("grossProfitMargin").toString()).add(new BigDecimal(out.get("grossProfitMargin").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("grossProfitMargin", grossProfitMargin);
                } else {
                    outTotal.put("grossProfitMargin", out.get("grossProfitMargin").toString());
                }
                if (outTotal.containsKey("discountAmt")) {
                    BigDecimal discountAmt = new BigDecimal(outTotal.get("discountAmt").toString()).add(new BigDecimal(out.get("discountAmt").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("discountAmt", discountAmt);
                } else {
                    outTotal.put("discountAmt", out.get("discountAmt").toString());
                }
            }
            List<StoreBudgetPlanOutData> list = JSON.parseArray(JSON.toJSONString(outData), StoreBudgetPlanOutData.class);
            pageInfo = new PageInfo(list, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public void exportStoreBudgePlanByDateDetails(StoreBudgetPlanInData inData, HttpServletResponse response) {
        //调用查询接口 查询数据 然后导出
        PageInfo result = findStoreBudgePlanByDate(inData);
        if (Objects.isNull(result) && CollUtil.isEmpty(result.getList())) {
            throw new BusinessException("提示：导出数据为空");
        }
        List<Map<String, Object>> storeList = (List<Map<String, Object>>) result.getList();
        Map<String, Object> outTotal = (Map<String, Object>) result.getListNum();
        if (ValidateUtil.isEmpty(storeList)) {
            throw new BusinessException("提示：导出数据为空");
        }
        CsvFileInfo fileInfo = new CsvFileInfo(new byte[0], 0, "日期销售折扣汇总表");
        List<StoreBudgetPlanOutData> list = JSON.parseArray(JSON.toJSONString(storeList), StoreBudgetPlanOutData.class);

        CsvClient.endHandle(response, list, fileInfo, () -> {
            if (ObjectUtil.isEmpty(fileInfo.getFileContent())) {
                throw new BusinessException("导出数据不能为空！");
            }
            byte[] bytes = ("\r\n合计,,," + outTotal.get("movPrices") + "," + outTotal.get("amt") + "," + outTotal.get("amountReceivable") + "," + outTotal.get("grossProfitMargin") + "," + "," + outTotal.get("discountAmt")).getBytes(StandardCharsets.UTF_8);
            byte[] all = ArrayUtils.addAll(fileInfo.getFileContent(), bytes);
            fileInfo.setFileContent(all);
            fileInfo.setFileSize(all.length);
        });
    }

    @Override
    public PageInfo findStoreBudgePlanByStoCodeAndDate(StoreBudgetPlanInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        List<Map<String, Object>> outData = storeBudgetPlanMapper.findStoreBudgePlanByStoCodeAndDate(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            Map<String, Object> outTotal = new HashMap<>();
            for (Map<String, Object> out : outData) {
                if (outTotal.containsKey("amountReceivable")) {
                    BigDecimal amountReceivable = new BigDecimal(outTotal.get("amountReceivable").toString()).add(new BigDecimal(out.get("amountReceivable").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("amountReceivable", amountReceivable);
                } else {
                    outTotal.put("amountReceivable", out.get("amountReceivable").toString());
                }
                if (outTotal.containsKey("amt")) {
                    BigDecimal amt = new BigDecimal(outTotal.get("amt").toString()).add(new BigDecimal(out.get("amt").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("amt", amt);
                } else {
                    outTotal.put("amt", out.get("amt").toString());
                }
                if (outTotal.containsKey("movPrices")) {
                    BigDecimal movPrices = new BigDecimal(outTotal.get("movPrices").toString()).add(new BigDecimal(out.get("movPrices").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("movPrices", movPrices);
                } else {
                    outTotal.put("movPrices", out.get("movPrices").toString());
                }
                if (outTotal.containsKey("grossProfitMargin")) {
                    BigDecimal grossProfitMargin = new BigDecimal(outTotal.get("grossProfitMargin").toString()).add(new BigDecimal(out.get("grossProfitMargin").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("grossProfitMargin", grossProfitMargin);
                } else {
                    outTotal.put("grossProfitMargin", out.get("grossProfitMargin").toString());
                }
                if (outTotal.containsKey("discountAmt")) {
                    BigDecimal discountAmt = new BigDecimal(outTotal.get("discountAmt").toString()).add(new BigDecimal(out.get("discountAmt").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("discountAmt", discountAmt);
                } else {
                    outTotal.put("discountAmt", out.get("discountAmt").toString());
                }
            }
            List<StoreBudgetPlanByStoCodeOutData> list = JSON.parseArray(JSON.toJSONString(outData), StoreBudgetPlanByStoCodeOutData.class);
            pageInfo = new PageInfo(list, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public void exportStoreBudgePlanByStoCodeAndDateDetails(StoreBudgetPlanInData inData, HttpServletResponse response) {
        //调用查询接口 查询数据 然后导出
        PageInfo result = findStoreBudgePlanByStoCodeAndDate(inData);
        if (Objects.isNull(result) && CollUtil.isEmpty(result.getList())) {
            throw new BusinessException("提示：导出数据为空");
        }
        List<Map<String, Object>> storeList = (List<Map<String, Object>>) result.getList();
        Map<String, Object> outTotal = (Map<String, Object>) result.getListNum();
        if (ValidateUtil.isEmpty(storeList)) {
            throw new BusinessException("提示：导出数据为空");
        }
        CsvFileInfo fileInfo = new CsvFileInfo(new byte[0], 0, "门店日期销售折扣表");
        List<StoreBudgetPlanByStoCodeOutData> list = JSON.parseArray(JSON.toJSONString(storeList), StoreBudgetPlanByStoCodeOutData.class);

        CsvClient.endHandle(response, list, fileInfo, () -> {
            if (ObjectUtil.isEmpty(fileInfo.getFileContent())) {
                throw new BusinessException("导出数据不能为空！");
            }
            byte[] bytes = ("\r\n合计,,,," + outTotal.get("movPrices") + "," + outTotal.get("amt") + "," + outTotal.get("amountReceivable") + "," + outTotal.get("grossProfitMargin") + "," + "," + outTotal.get("discountAmt")).getBytes(StandardCharsets.UTF_8);
            byte[] all = ArrayUtils.addAll(fileInfo.getFileContent(), bytes);
            fileInfo.setFileContent(all);
            fileInfo.setFileSize(all.length);
        });
    }

    @Override
    public PageInfo selectStoreDiscountSummary(StoreBudgetPlanInData budgetPlanInData) {
        if (ObjectUtil.isEmpty(budgetPlanInData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(budgetPlanInData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        String gssgId = budgetPlanInData.getGssgId();
        if (StringUtils.isNotBlank(gssgId)) {
            budgetPlanInData.setGssgIds(Arrays.asList(gssgId.split(StrUtil.COMMA)));
        }
        Set<String> stoGssgTypeSet = new HashSet<>();
        String stoGssgType = budgetPlanInData.getStoGssgType();
        boolean noChooseFlag = true;
        if (stoGssgType != null) {
            List<GaiaStoreCategoryType> stoGssgTypes = new ArrayList<>();
            for (String s : stoGssgType.split(StrUtil.COMMA)) {
                String[] str = s.split(StrUtil.UNDERLINE);
                GaiaStoreCategoryType gaiaStoreCategoryType = new GaiaStoreCategoryType();
                gaiaStoreCategoryType.setGssgType(str[0]);
                gaiaStoreCategoryType.setGssgId(str[1]);
                stoGssgTypes.add(gaiaStoreCategoryType);
                stoGssgTypeSet.add(str[0]);
            }
            budgetPlanInData.setStoGssgTypes(stoGssgTypes);
            noChooseFlag = false;
        }
        String stoAttribute = budgetPlanInData.getStoAttribute();
        if (stoAttribute != null) {
            noChooseFlag = false;
            budgetPlanInData.setStoAttributes(Arrays.asList(stoAttribute.split(StrUtil.COMMA)));
        }
        String stoIfMedical = budgetPlanInData.getStoIfMedical();
        if (stoIfMedical != null) {
            noChooseFlag = false;
            budgetPlanInData.setStoIfMedicals(Arrays.asList(stoIfMedical.split(StrUtil.COMMA)));
        }
        String stoTaxClass = budgetPlanInData.getStoTaxClass();
        if (stoTaxClass != null) {
            noChooseFlag = false;
            budgetPlanInData.setStoTaxClasss(Arrays.asList(stoTaxClass.split(StrUtil.COMMA)));
        }
        String stoIfDtp = budgetPlanInData.getStoIfDtp();
        if (stoIfDtp != null) {
            noChooseFlag = false;
            budgetPlanInData.setStoIfDtps(Arrays.asList(stoIfDtp.split(StrUtil.COMMA)));
        }
        List<Map<String, Object>> outData = storeBudgetPlanMapper.selectStoreDiscountSummary(budgetPlanInData);
        List<GaiaStoreCategoryType> storeCategoryByClient = gaiaSdStoresGroupMapper.selectStoreCategoryByClient(budgetPlanInData.getClient());
        Map<String, Object> outTotal = new HashMap<>();
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {

            for (Map<String, Object> item : outData) {
                String brId = MapUtil.getStr(item, "brId");
                // 转换
                if (stoAttribute != null || noChooseFlag) {
                    item.put("stoAttribute", StoreAttributeEnum.getName(MapUtil.getStr(item, "stoAttribute")));
                }
                if (stoIfMedical != null || noChooseFlag) {
                    item.put("stoIfMedical", StoreMedicalEnum.getName(MapUtil.getStr(item, "stoIfMedical")));
                }
                if (stoIfDtp != null || noChooseFlag) {
                    item.put("stoIfDtp", StoreDTPEnum.getName(MapUtil.getStr(item, "stoIfDtp")));
                }
                if (stoTaxClass != null || noChooseFlag) {
                    item.put("stoTaxClass", StoreTaxClassEnum.getName(MapUtil.getStr(item, "stoTaxClass")));
                }
                List<GaiaStoreCategoryType> collect = storeCategoryByClient.stream().filter(t -> t.getGssgBrId().equals(brId)).collect(Collectors.toList());

                Set<String> tmpStoGssgTypeSet = new HashSet<>(stoGssgTypeSet);


                for (GaiaStoreCategoryType gaiaStoreCategoryType : collect) {
                    String field = null;
                    boolean flag = false;
                    String gssgType = gaiaStoreCategoryType.getGssgType();
                    if (noChooseFlag) {
                        if (gssgType.contains("DX0001")) {
                            field = "shopType";
                        } else if (gssgType.contains("DX0002")) {
                            field = "storeEfficiencyLevel";
                        } else if (gssgType.contains("DX0003")) {
                            field = "directManaged";
                        } else if (gssgType.contains("DX0004")) {
                            field = "managementArea";
                        }
                        if (StringUtils.isNotBlank(field)) {
                            item.put(field, gaiaStoreCategoryType.getGssgIdName());
                        }
                    } else {
                        if (!stoGssgTypeSet.isEmpty()) {
                            if (tmpStoGssgTypeSet.contains("DX0001") && "DX0001".equals(gssgType)) {
                                field = "shopType";
                                flag = true;
                                tmpStoGssgTypeSet.remove("DX0001");
                            } else if (tmpStoGssgTypeSet.contains("DX0002") && "DX0002".equals(gssgType)) {
                                field = "storeEfficiencyLevel";
                                flag = true;
                                tmpStoGssgTypeSet.remove("DX0002");
                            } else if (tmpStoGssgTypeSet.contains("DX0003") && "DX0003".equals(gssgType)) {
                                field = "directManaged";
                                flag = true;
                                tmpStoGssgTypeSet.remove("DX0003");
                            } else if (tmpStoGssgTypeSet.contains("DX0004") && "DX0004".equals(gssgType)) {
                                field = "managementArea";
                                flag = true;
                                tmpStoGssgTypeSet.remove("DX0004");
                            }
                        }
                        if (flag) {
                            item.put(field, gaiaStoreCategoryType.getGssgIdName());
                        }
                    }
                }

                if (outTotal.containsKey("amountReceivable")) {
                    BigDecimal amountReceivable = new BigDecimal(outTotal.get("amountReceivable").toString()).add(new BigDecimal(item.get("amountReceivable").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("amountReceivable", amountReceivable);
                } else {
                    outTotal.put("amountReceivable", item.get("amountReceivable").toString());
                }
                if (outTotal.containsKey("amt")) {
                    BigDecimal amt = new BigDecimal(outTotal.get("amt").toString()).add(new BigDecimal(item.get("amt").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("amt", amt);
                } else {
                    outTotal.put("amt", item.get("amt").toString());
                }
                if (outTotal.containsKey("movPrices")) {
                    BigDecimal movPrices = new BigDecimal(outTotal.get("movPrices").toString()).add(new BigDecimal(item.get("movPrices").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("movPrices", movPrices);
                } else {
                    outTotal.put("movPrices", item.get("movPrices").toString());
                }
                if (outTotal.containsKey("grossProfitMargin")) {
                    BigDecimal grossProfitMargin = new BigDecimal(outTotal.get("grossProfitMargin").toString()).add(new BigDecimal(item.get("grossProfitMargin").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("grossProfitMargin", grossProfitMargin);
                } else {
                    outTotal.put("grossProfitMargin", item.get("grossProfitMargin").toString());
                }
                if (outTotal.containsKey("discountAmt")) {
                    BigDecimal discountAmt = new BigDecimal(outTotal.get("discountAmt").toString()).add(new BigDecimal(item.get("discountAmt").toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
                    outTotal.put("discountAmt", discountAmt);
                } else {
                    outTotal.put("discountAmt", item.get("discountAmt").toString());
                }

            }
            List<StoreDiscountSummaryData> list = JSON.parseArray(JSON.toJSONString(outData), StoreDiscountSummaryData.class);
            pageInfo = new PageInfo(list, outTotal);

        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public void exportStoreDiscountSummary(StoreBudgetPlanInData inData, HttpServletResponse response) {
        //调用查询接口 查询数据 然后导出
        PageInfo result = selectStoreDiscountSummary(inData);
        if (Objects.isNull(result) && CollUtil.isEmpty(result.getList())) {
            throw new BusinessException("提示：导出数据为空");
        }
        List<Map<String, Object>> storeList = (List<Map<String, Object>>) result.getList();
        Map<String, Object> outTotal = (Map<String, Object>) result.getListNum();
        if (ValidateUtil.isEmpty(storeList)) {
            throw new BusinessException("提示：导出数据为空");
        }
        CsvFileInfo fileInfo = new CsvFileInfo(new byte[0], 0, "门店折扣销售汇总表");
        List<StoreDiscountSummaryData> list = JSON.parseArray(JSON.toJSONString(storeList), StoreDiscountSummaryData.class);

        CsvClient.endHandle(response, list, fileInfo, () -> {
            if (ObjectUtil.isEmpty(fileInfo.getFileContent())) {
                throw new BusinessException("导出数据不能为空！");
            }
            byte[] bytes = ("\r\n合计,,,," + outTotal.get("movPrices") + "," + outTotal.get("amt") + "," + outTotal.get("amountReceivable") + "," + outTotal.get("grossProfitMargin") + "," + "," + ","+ outTotal.get("discountAmt")).getBytes(StandardCharsets.UTF_8);
            byte[] all = ArrayUtils.addAll(fileInfo.getFileContent(), bytes);
            fileInfo.setFileContent(all);
            fileInfo.setFileSize(all.length);
        });
    }
}
