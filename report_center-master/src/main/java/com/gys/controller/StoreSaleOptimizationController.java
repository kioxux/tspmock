package com.gys.controller;

import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.entity.renhe.StoreSaleSummaryInData;
import com.gys.report.entity.StoreSaleDateInData;
import com.gys.service.RenHeSaleReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Slf4j
@RestController
@Api(tags = "门店销售管理-山西仁和专供")
@RequestMapping({"/storeSaleReport"})
public class StoreSaleOptimizationController extends BaseController {

    @Autowired
    private RenHeSaleReportService renHeSaleReportService;

    @ApiOperation(value = "门店销售汇总查询")
    @PostMapping("/storeSaleSummary")
    public JsonResult selectStoreSaleSummaryByBrId(HttpServletRequest request, @Valid @RequestBody StoreSaleSummaryInData summaryInData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        summaryInData.setClient(userInfo.getClient());
        return JsonResult.success(renHeSaleReportService.selectStoreSaleSummaryByBrId(summaryInData), "返回成功");
    }

    @ApiOperation(value = "门店日期销售查询")
    @PostMapping({"/storeSaleByDate"})
    public JsonResult selectStoreSaleByDate(HttpServletRequest request,@RequestBody StoreSaleDateInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(renHeSaleReportService.selectStoreSaleByDate(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "日期销售汇总查询")
    @PostMapping("/storeSaleSummaryByDate")
    public JsonResult selectSalesSummaryByDate(HttpServletRequest request, @Valid @RequestBody com.gys.entity.data.salesSummary.StoreSaleDateInData summaryData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        summaryData.setClient(userInfo.getClient());
        return JsonResult.success(renHeSaleReportService.selectStoreSalesSummaryByDate(summaryData),"返回成功");
    }
}
