package com.gys.mapper;

import com.gys.common.base.BaseMapper;
import com.gys.entity.GaiaSdStoresGroup;
import com.gys.report.entity.GaiaStoreCategoryType;

import java.util.List;

public interface GaiaSdStoresGroupMapper extends BaseMapper<GaiaSdStoresGroup> {
    List<GaiaStoreCategoryType> selectStoreCategoryByClient(String clientId);
}