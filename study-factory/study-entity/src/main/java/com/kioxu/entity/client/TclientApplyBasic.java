package com.kioxu.entity.client;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.kioxu.entity.BaseModel;
import org.hibernate.annotations.Proxy;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Proxy(lazy=false)
@Entity
@Table(name = "tclient_apply_basic", catalog = "client", schema = "")
@XmlRootElement
@NamedQueries({})
public class TclientApplyBasic extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "veriface_score")
    private String verifaceScore;//人脸识别分数

    @Column(name = "user_id")
    private Integer userId;//客户id(登录客户id)

    @Column(name = "order_number")
    private String orderNumber;//申请单号

    @Column(name = "sales_id")
    private Integer salesId;//业务员id

    @Column(name = "sales")
    private String sales;//业务员

    @Column(name = "sales_manager_id")
    private String salesManagerId;//团队经理id

    @Column(name = "sales_manager")
    private String salesManager;//团队经理

    @Column(name = "store_id")
    private String storeId;//门店id

    @Column(name = "store")
    private String store;//门店

    @Column(name = "audit_amount")
    private BigDecimal auditAmount;//审批金额

    @Column(name = "lease_type")
    private String leaseType;//租赁类型 正租；回租

    @Column(name = "financing_period")
    private Integer financingPeriod;//融资期限

    @Column(name = "product_id")
    private Integer productId;//产品方案id

    @Column(name = "product_name")
    private String productName;//产品名称

    @Column(name = "client_name")
    private String clientName;//客户姓名

    @Column(name = "id_card")
    private String idCard;//客户证件号

    @Column(name = "apply_type")
    private String applyType;//申请类型 code=2132

    @Column(name = "apply_status")
    private String applyStatus;//申请状态 code=3001

    @Column(name = "remark")
    private String remark;//申请备注

    @Column(name = "flow_id")
    private String flowId;//流程id

    @Column(name = "apply_channel")
    private String applyChannel;//申请渠道

    @Column(name = "loan_channel")
    private String loanChannel;//放款渠道

    @Column(name = "client_source")
    private String clientSource;//客户来源

    @Column(name = "gps_install_type")
    private String gpsInstallType;//gps安装方式

    @Column(name = "financing_amount")
    private BigDecimal financingAmount;//融资项目金额(申请金额)

    @Column(name = "car_nature")
    private String carNature;//车辆性质code=3002

    @Column(name = "recording_personnel")
    private String recordingPersonnel;//录单人员

    @Column(name = "recording_personnel_id")
    private String recordingPersonnelId;//录单人员id

    @Column(name = "zf_trans_serial_no")
    private String zfTransSerialNo;//录单人员id

    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "recording_time")
    @Temporal(TemporalType.DATE)
    private Date recordingTime;//录单时间

    @Column(name = "sales_card_type")
    private String salesCardType;//推荐业务员证件类型code=1007

    @Column(name = "sales_id_card")
    private String salesIdCard;//推荐业务员身份证号

    @Column(name = "sales_name")
    private String salesName;//推荐业务员姓名

    @Column(name = "sales_mobile")
    private String salesMobile;//推荐业务员手机号

    @Column(name = "sales_province")
    private String salesProvince;//推荐业务员所在省

    @Column(name = "sales_city")
    private String salesCity;//推荐业务员所在城市

    @Column(name = "sales_store")
    private String salesStore;//推荐业务员所属门店

    @Column(name = "zl_sales")
    private String zlSales;//仲利业务员

    @Column(name = "comp_code")
    private String compCode;//公司代码 code=1001

    @Column(name = "usage_of_loan")
    private String usageOfLoan;//借款用途

    @Column(name = "business_type")
    private String businessType;//业务类型 首贷，续贷

    @Column(name = "is_unilateral")
    private Integer isUnilateral;//家人是否知晓此处借款 code=valid

    @Column(name = "loan_situation")
    private String loanSituation;//借款情况

    @Column(name = "location")
    private String location;// 所在地区

    @Column(name = "mobile")
    private String mobile;//客户手机号

    @Column(name = "is_select_product")
    private Boolean isSelectProduct;

    @Column(name = "is_identification")
    private Boolean isIdentification;

    @Column(name = "is_complete_basic")
    private Boolean isCompleteBasic;

    @Column(name = "is_complete_company")
    private Boolean isCompleteCompany;

    @Column(name = "is_complete_car")
    private Boolean isCompleteCar;

    @Column(name = "is_complete_contact")
    private Boolean isCompleteContact;

    @Column(name = "is_complete_work")
    private Boolean isCompleteWork;

    @Column(name = "is_complete_file")
    private Boolean isCompleteFile;

    @Column(name = "is_ocr")
    private Boolean isOcr;

    @Column(name = "is_complete_pre_apply")
    private Boolean isCompletePreApply = true; // 是否通过预申请（长安产品）

    @Column(name = "is_complete_sign")
    private Boolean isCompleteSign;

    @Column(name = "repaying_source")
    private String repayingSource;

    @Column(name = "audit_remark")
    private String auditRemark;

    @Column(name = "family_information")
    private String familyInformation; // 家庭情况

    @Column(name = "living_information")
    private String livingInformation; // 居住情况

    @Column(name = "loan_purpose")
    private String loanPurpose; // 贷款用途

    @Column(name = "use_of_funds")
    private String useOfFunds; // 资金用途

    @Column(name = "family_members")
    private String familyMembers; // 家庭成员

    @Column(name = "financing_gap")
    private String financingGap; // 资金缺口

    @Column(name = "suggested_loan_amount")
    private String suggestedLoanAmount;

    @Column(name = "check_id_card")
    private Integer checkIdCard;

    @Column(name = "surface_review_report")
    private String surfaceReviewReport;

    @Column(name = "bank_no")
    private String bankNo;

    @Column(name = "explain_remark")
    private String explainRemark;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time", updatable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;//创建时间

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "update_time", insertable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;//更新时间

    @Column(name = "person_info_auth")
    private Boolean personInfoAuth;

    @Column(name = "local")
    private Boolean local;

    @Column(name = "local_car")
    private Boolean localCar;

    @Column(name = "local_house")
    private Boolean localHouse;

    @Column(name = "have_local_household")
    private Boolean haveLocalHousehold;

    @Column(name = "local_work")
    private Boolean localWork;

    @Column(name = "local_operate")
    private Boolean localOperate;

    @Column(name = "income_to_card")
    private Boolean incomeToCard;

    @Column(name = "have_social_security")
    private Boolean haveSocialSecurity;

    @Column(name = "housewife")
    private Boolean housewife;

    @Column(name = "builder")
    private Boolean builder;

    @Column(name = "farmer")
    private Boolean farmer;

    @Column(name = "wechat_bussiness")
    private Boolean wechatBussiness;

    @Column(name = "is_other_worker")
    private String isOtherWorker;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "zf_identify")
    private String zfIdentify;

    @Column(name = "train")
    private Integer train;

    @Column(name = "audit_status")
    private String auditStatus;

    @Column(name = "face_examine")
    private String faceExamine;

    @Column(name = "face_examine_id")
    private String faceExamineid;

    @Column(name = "sign_certifier")
    private String signCertifier;

    @Column(name = "sign_certifier_id")
    private String signCertifierId;

    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "input_order_date", insertable=false)
    @Temporal(TemporalType.DATE)
    private Date inputOrderDate;//进件时间

    @JSONField(format = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "loan_date", insertable=false)
    @Temporal(TemporalType.DATE)
    private Date loanDate;//放款时间

    @Column(name = "is_family_trans")
    private Integer isFamilyTrans;

    @Column(name = "pretrial_loan_status")
    private Integer pretrialLoanStatus;

    @Column(name = "reject_item")
    private String rejectItem;

    @Column(name = "charterer")
    private String charterer;

    @Column(name = "before_org_type")
    private String beforeOrgType;

    @Column(name = "bank_child_name")
    private String bankChildName;

    @Column(name = "wf_status")
    private Boolean wfStatus;

    @Column(name = "mortgage_contact")
    private String mortgageContact;//抵押联系人

    @Column(name = "mortgage_contact_tel")
    private String mortgageContactTel;//抵押联系人电话

    @Column(name = "is_crm")
    private Boolean isCrm;//是否是crm进件

    @Column(name = "business_num")
    private String businessNum;//业务编码

    @Column(name = "source")
    private String source;// 订单来源

    @Transient
    private String capital;

    @Transient
    private String showResult;

    @Transient
    private String zfChildStatus;

    @Transient
    private String ourResults;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "pretrial_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pretrialTime;//预审时间

    @Column(name = "approval_model")
    private String approvalModel;//审批模式

    @Column(name = "is_complete_auto_rule")
    private Boolean isCompleteAutoRule;//是否执行完自动审批规则除资方审批规则外

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getZfTransSerialNo() {
        return zfTransSerialNo;
    }

    public void setZfTransSerialNo(String zfTransSerialNo) {
        this.zfTransSerialNo = zfTransSerialNo;
    }

    public Boolean getWfStatus() {
        return wfStatus;
    }

    public void setWfStatus(Boolean wfStatus) {
        this.wfStatus = wfStatus;
    }

    public String getBankChildName() {
        return bankChildName;
    }

    public void setBankChildName(String bankChildName) {
        this.bankChildName = bankChildName;
    }

    public String getCharterer() {
        return charterer;
    }

    public void setCharterer(String charterer) {
        this.charterer = charterer;
    }

    public String getBeforeOrgType() {
        return beforeOrgType;
    }

    public void setBeforeOrgType(String beforeOrgType) {
        this.beforeOrgType = beforeOrgType;
    }

    public String getRejectItem() {
        return rejectItem;
    }

    public void setRejectItem(String rejectItem) {
        this.rejectItem = rejectItem;
    }

    public Integer getPretrialLoanStatus() {
        return pretrialLoanStatus;
    }

    public void setPretrialLoanStatus(Integer pretrialLoanStatus) {
        this.pretrialLoanStatus = pretrialLoanStatus;
    }

    public Integer getIsFamilyTrans() {
        return isFamilyTrans;
    }

    public void setIsFamilyTrans(Integer isFamilyTrans) {
        this.isFamilyTrans = isFamilyTrans;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Date getInputOrderDate() {
        return inputOrderDate;
    }

    public void setInputOrderDate(Date inputOrderDate) {
        this.inputOrderDate = inputOrderDate;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public Integer getTrain() {
        return train;
    }

    public void setTrain(Integer train) {
        this.train = train;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVerifaceScore() {
        return verifaceScore;
    }

    public void setVerifaceScore(String verifaceScore) {
        this.verifaceScore = verifaceScore;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getSalesId() {
        return salesId;
    }

    public void setSalesId(Integer salesId) {
        this.salesId = salesId;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getSalesManagerId() {
        return salesManagerId;
    }

    public void setSalesManagerId(String salesManagerId) {
        this.salesManagerId = salesManagerId;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public BigDecimal getAuditAmount() {
        return auditAmount;
    }

    public void setAuditAmount(BigDecimal auditAmount) {
        this.auditAmount = auditAmount;
    }

    public String getLeaseType() {
        return leaseType;
    }

    public void setLeaseType(String leaseType) {
        this.leaseType = leaseType;
    }

    public Integer getFinancingPeriod() {
        return financingPeriod;
    }

    public void setFinancingPeriod(Integer financingPeriod) {
        this.financingPeriod = financingPeriod;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getApplyType() {
        return applyType;
    }

    public void setApplyType(String applyType) {
        this.applyType = applyType;
    }

    public String getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getApplyChannel() {
        return applyChannel;
    }

    public void setApplyChannel(String applyChannel) {
        this.applyChannel = applyChannel;
    }

    public String getLoanChannel() {
        return loanChannel;
    }

    public void setLoanChannel(String loanChannel) {
        this.loanChannel = loanChannel;
    }

    public String getClientSource() {
        return clientSource;
    }

    public void setClientSource(String clientSource) {
        this.clientSource = clientSource;
    }

    public String getGpsInstallType() {
        return gpsInstallType;
    }

    public void setGpsInstallType(String gpsInstallType) {
        this.gpsInstallType = gpsInstallType;
    }

    public BigDecimal getFinancingAmount() {
        return financingAmount;
    }

    public void setFinancingAmount(BigDecimal financingAmount) {
        this.financingAmount = financingAmount;
    }

    public String getCarNature() {
        return carNature;
    }

    public void setCarNature(String carNature) {
        this.carNature = carNature;
    }

    public String getRecordingPersonnel() {
        return recordingPersonnel;
    }

    public void setRecordingPersonnel(String recordingPersonnel) {
        this.recordingPersonnel = recordingPersonnel;
    }

    public String getRecordingPersonnelId() {
        return recordingPersonnelId;
    }

    public void setRecordingPersonnelId(String recordingPersonnelId) {
        this.recordingPersonnelId = recordingPersonnelId;
    }

    public Date getRecordingTime() {
        return recordingTime;
    }

    public void setRecordingTime(Date recordingTime) {
        this.recordingTime = recordingTime;
    }

    public String getSalesCardType() {
        return salesCardType;
    }

    public void setSalesCardType(String salesCardType) {
        this.salesCardType = salesCardType;
    }

    public String getSalesIdCard() {
        return salesIdCard;
    }

    public void setSalesIdCard(String salesIdCard) {
        this.salesIdCard = salesIdCard;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getSalesMobile() {
        return salesMobile;
    }

    public void setSalesMobile(String salesMobile) {
        this.salesMobile = salesMobile;
    }

    public String getSalesProvince() {
        return salesProvince;
    }

    public void setSalesProvince(String salesProvince) {
        this.salesProvince = salesProvince;
    }

    public String getSalesCity() {
        return salesCity;
    }

    public void setSalesCity(String salesCity) {
        this.salesCity = salesCity;
    }

    public String getSalesStore() {
        return salesStore;
    }

    public void setSalesStore(String salesStore) {
        this.salesStore = salesStore;
    }

    public String getZlSales() {
        return zlSales;
    }

    public void setZlSales(String zlSales) {
        this.zlSales = zlSales;
    }

    public String getCompCode() {
        return compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getUsageOfLoan() {
        return usageOfLoan;
    }

    public void setUsageOfLoan(String usageOfLoan) {
        this.usageOfLoan = usageOfLoan;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Integer getIsUnilateral() {
        return isUnilateral;
    }

    public void setIsUnilateral(Integer isUnilateral) {
        this.isUnilateral = isUnilateral;
    }

    public String getLoanSituation() {
        return loanSituation;
    }

    public void setLoanSituation(String loanSituation) {
        this.loanSituation = loanSituation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getSelectProduct() {
        return isSelectProduct;
    }

    public void setSelectProduct(Boolean selectProduct) {
        isSelectProduct = selectProduct;
    }

    public Boolean getIdentification() {
        return isIdentification;
    }

    public void setIdentification(Boolean identification) {
        isIdentification = identification;
    }

    public Boolean getCompleteBasic() {
        return isCompleteBasic;
    }

    public void setCompleteBasic(Boolean completeBasic) {
        isCompleteBasic = completeBasic;
    }

    public Boolean getCompleteCompany() {
        return isCompleteCompany;
    }

    public void setCompleteCompany(Boolean completeCompany) {
        isCompleteCompany = completeCompany;
    }

    public Boolean getCompleteCar() {
        return isCompleteCar;
    }

    public void setCompleteCar(Boolean completeCar) {
        isCompleteCar = completeCar;
    }

    public Boolean getCompleteContact() {
        return isCompleteContact;
    }

    public void setCompleteContact(Boolean completeContact) {
        isCompleteContact = completeContact;
    }

    public Boolean getCompleteWork() {
        return isCompleteWork;
    }

    public void setCompleteWork(Boolean completeWork) {
        isCompleteWork = completeWork;
    }

    public Boolean getCompleteFile() {
        return isCompleteFile;
    }

    public void setCompleteFile(Boolean completeFile) {
        isCompleteFile = completeFile;
    }

    public Boolean getOcr() {
        return isOcr;
    }

    public void setOcr(Boolean ocr) {
        isOcr = ocr;
    }

    public String getRepayingSource() {
        return repayingSource;
    }

    public void setRepayingSource(String repayingSource) {
        this.repayingSource = repayingSource;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public String getFamilyInformation() {
        return familyInformation;
    }

    public void setFamilyInformation(String familyInformation) {
        this.familyInformation = familyInformation;
    }

    public String getLivingInformation() {
        return livingInformation;
    }

    public void setLivingInformation(String livingInformation) {
        this.livingInformation = livingInformation;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }

    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public String getUseOfFunds() {
        return useOfFunds;
    }

    public void setUseOfFunds(String useOfFunds) {
        this.useOfFunds = useOfFunds;
    }

    public String getFamilyMembers() {
        return familyMembers;
    }

    public void setFamilyMembers(String familyMembers) {
        this.familyMembers = familyMembers;
    }

    public String getFinancingGap() {
        return financingGap;
    }

    public void setFinancingGap(String financingGap) {
        this.financingGap = financingGap;
    }

    public String getSuggestedLoanAmount() {
        return suggestedLoanAmount;
    }

    public void setSuggestedLoanAmount(String suggestedLoanAmount) {
        this.suggestedLoanAmount = suggestedLoanAmount;
    }

    public Integer getCheckIdCard() {
        return checkIdCard;
    }

    public void setCheckIdCard(Integer checkIdCard) {
        this.checkIdCard = checkIdCard;
    }

    public String getSurfaceReviewReport() {
        return surfaceReviewReport;
    }

    public void setSurfaceReviewReport(String surfaceReviewReport) {
        this.surfaceReviewReport = surfaceReviewReport;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getPersonInfoAuth() {
        return personInfoAuth;
    }

    public void setPersonInfoAuth(Boolean personInfoAuth) {
        this.personInfoAuth = personInfoAuth;
    }

    public Boolean getLocal() {
        return local;
    }

    public void setLocal(Boolean local) {
        this.local = local;
    }

    public Boolean getLocalCar() {
        return localCar;
    }

    public void setLocalCar(Boolean localCar) {
        this.localCar = localCar;
    }

    public Boolean getLocalHouse() {
        return localHouse;
    }

    public void setLocalHouse(Boolean localHouse) {
        this.localHouse = localHouse;
    }

    public Boolean getHaveLocalHousehold() {
        return haveLocalHousehold;
    }

    public void setHaveLocalHousehold(Boolean haveLocalHousehold) {
        this.haveLocalHousehold = haveLocalHousehold;
    }

    public Boolean getLocalWork() {
        return localWork;
    }

    public void setLocalWork(Boolean localWork) {
        this.localWork = localWork;
    }

    public Boolean getLocalOperate() {
        return localOperate;
    }

    public void setLocalOperate(Boolean localOperate) {
        this.localOperate = localOperate;
    }

    public Boolean getIncomeToCard() {
        return incomeToCard;
    }

    public void setIncomeToCard(Boolean incomeToCard) {
        this.incomeToCard = incomeToCard;
    }

    public Boolean getHaveSocialSecurity() {
        return haveSocialSecurity;
    }

    public void setHaveSocialSecurity(Boolean haveSocialSecurity) {
        this.haveSocialSecurity = haveSocialSecurity;
    }

    public Boolean getHousewife() {
        return housewife;
    }

    public void setHousewife(Boolean housewife) {
        this.housewife = housewife;
    }

    public Boolean getBuilder() {
        return builder;
    }

    public void setBuilder(Boolean builder) {
        this.builder = builder;
    }

    public Boolean getFarmer() {
        return farmer;
    }

    public void setFarmer(Boolean farmer) {
        this.farmer = farmer;
    }

    public Boolean getWechatBussiness() {
        return wechatBussiness;
    }

    public void setWechatBussiness(Boolean wechatBussiness) {
        this.wechatBussiness = wechatBussiness;
    }

    public String getIsOtherWorker() {
        return isOtherWorker;
    }

    public void setIsOtherWorker(String isOtherWorker) {
        this.isOtherWorker = isOtherWorker;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getZfIdentify() {
        return zfIdentify;
    }

    public void setZfIdentify(String zfIdentify) {
        this.zfIdentify = zfIdentify;
    }

    public String getFaceExamine() {
        return faceExamine;
    }

    public void setFaceExamine(String faceExamine) {
        this.faceExamine = faceExamine;
    }

    public String getFaceExamineid() {
        return faceExamineid;
    }

    public void setFaceExamineid(String faceExamineid) {
        this.faceExamineid = faceExamineid;
    }

    public String getSignCertifier() {
        return signCertifier;
    }

    public void setSignCertifier(String signCertifier) {
        this.signCertifier = signCertifier;
    }

    public String getSignCertifierId() {
        return signCertifierId;
    }

    public void setSignCertifierId(String signCertifierId) {
        this.signCertifierId = signCertifierId;
    }

    public Boolean getCompletePreApply() {
        return isCompletePreApply;
    }

    public void setCompletePreApply(Boolean completePreApply) {
        isCompletePreApply = completePreApply;
    }

    public Boolean getCompleteSign() {
        return isCompleteSign;
    }

    public void setCompleteSign(Boolean completeSign) {
        isCompleteSign = completeSign;
    }

    public Boolean getIsCrm() {
        return isCrm;
    }

    public void setIsCrm(Boolean isCrm) {
        this.isCrm = isCrm;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getShowResult() {
        return showResult;
    }

    public void setShowResult(String showResult) {
        this.showResult = showResult;
    }

    public String getMortgageContact() {
        return mortgageContact;
    }

    public void setMortgageContact(String mortgageContact) {
        this.mortgageContact = mortgageContact;
    }

    public String getMortgageContactTel() {
        return mortgageContactTel;
    }

    public void setMortgageContactTel(String mortgageContactTel) {
        this.mortgageContactTel = mortgageContactTel;
    }

    public String getExplainRemark() {
        return explainRemark;
    }

    public void setExplainRemark(String explainRemark) {
        this.explainRemark = explainRemark;
    }

    public String getZfChildStatus() {
        return zfChildStatus;
    }

    public void setZfChildStatus(String zfChildStatus) {
        this.zfChildStatus = zfChildStatus;
    }

    public String getOurResults() {
        return ourResults;
    }

    public void setOurResults(String ourResults) {
        this.ourResults = ourResults;
    }
    public Date getPretrialTime() {
        return pretrialTime;
    }

    public void setPretrialTime(Date pretrialTime) {
        this.pretrialTime = pretrialTime;
    }

    public String getBusinessNum() {
        return businessNum;
    }

    public void setBusinessNum(String businessNum) {
        this.businessNum = businessNum;
    }

    public String getApprovalModel() {
        return approvalModel;
    }

    public void setApprovalModel(String approvalModel) {
        this.approvalModel = approvalModel;
    }

    public Boolean getIsCompleteAutoRule() {
        return isCompleteAutoRule;
    }

    public void setIsCompleteAutoRule(Boolean completeAutoRule) {
        isCompleteAutoRule = completeAutoRule;
    }
}
