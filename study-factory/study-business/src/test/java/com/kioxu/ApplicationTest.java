package com.kioxu;

import com.alibaba.fastjson.JSON;
import com.kioxu.business.repository.TclientApplyBasicRepository;
import com.kioxu.business.service.MyService;
import com.kioxu.entity.client.TclientApplyBasic;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class ApplicationTest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private MyService myService;

    @Resource
    private TclientApplyBasicRepository tclientApplyBasicRepository;

    @Test
    void getMessage() {
        String message = myService.getMessage();
        logger.info("message: {}", message);
    }

    @Test
    void demo1() {
        TclientApplyBasic basic = tclientApplyBasicRepository.findByOrderNumber("DD2024051307235216");
        logger.info("basic: {}", JSON.toJSONString(basic));
    }
}