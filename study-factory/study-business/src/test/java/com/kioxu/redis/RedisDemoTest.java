package com.kioxu.redis;

import com.kioxu.business.util.JedisUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RedisDemoTest {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    void demo2() {
        JedisUtil.setKeyObject("test", "testValue", 60);
        Object test = JedisUtil.getKeyObjectNoReset("test");
        logger.info("test: {}", test);
        JedisUtil.delKeyStr("test");
    }
}
