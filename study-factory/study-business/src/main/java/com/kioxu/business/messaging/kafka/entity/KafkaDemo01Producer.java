package com.kioxu.business.messaging.kafka.entity;

import com.kioxu.business.messaging.kafka.message.KafkaDemo01Message;
import com.kioxu.business.messaging.kafka.message.KafkaMessage;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

@Component
public class KafkaDemo01Producer {
    @Resource
    private KafkaTemplate<Object, Object> kafkaTemplate;

    public SendResult syncSend(Integer id) throws ExecutionException, InterruptedException {
        // 创建 Demo01Message 消息
        KafkaDemo01Message message = new KafkaDemo01Message();
        message.setId(id);
        KafkaMessage kafkaMessage = new KafkaMessage();
        kafkaMessage.setId(1);
        kafkaMessage.setAge(20);
        kafkaMessage.setName("Jack");
        kafkaMessage.setSex("man");
        // 同步发送消息
        return kafkaTemplate.send(KafkaDemo01Message.TOPIC, kafkaMessage).get();
    }

    public ListenableFuture<SendResult<Object, Object>> asyncSend(Integer id) {
        // 创建 Demo01Message 消息
        KafkaDemo01Message message = new KafkaDemo01Message();
        message.setId(id);
        KafkaMessage kafkaMessage = new KafkaMessage();
        kafkaMessage.setId(1);
        kafkaMessage.setAge(20);
        kafkaMessage.setName("Jack");
        kafkaMessage.setSex("man");
        // 异步发送消息
        return kafkaTemplate.send(KafkaDemo01Message.TOPIC, kafkaMessage);
    }
}
