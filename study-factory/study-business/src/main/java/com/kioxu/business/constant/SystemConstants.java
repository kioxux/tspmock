package com.kioxu.business.constant;

/**
 * 系统代码
 */
public class SystemConstants {
	/**
	 * 错误码 - 异常
	 */
	public static final int ERROR_CODE_EXCEPTION = 0;
	/**
	 * 错误码 - 正常
	 */
	public static final int ERROR_CODE_OK = 1;
	/**
	 * 错误码 - 短信验证码错误次数超出异常
	 */
	public static final int ERROR_CODE_SMS_OUT_OF_BOUNDS = 2;
	/**
	 * 错误码 - 需强制更新
	 */
	public static final int ERROR_CODE_FORCED_UPDATE = 3;
	/**
	 * 错误码 - 接口已被停用
	 */
	public static final int ERROR_CODE_INTERFACE_DISABLED = 4;
	/**
	 * 错误码 - 未登陆
	 */
	public static final int ERROR_CODE_NOT_LOGGED_IN = 5;

	/**
	 * 错误码 - 此账号在其他设备登陆
	 */
	public static final int ERROR_CODE_KICKED_OUT = 6;

	/**
	 * 登陆超时时间 - 7天
	 */
	public static final int TOKEN_TIME = 60 * 60 * 24 * 7;

	/**
	 * 状态 - 启用
	 */
	public static final Boolean STATUS_ENABLE = true;

	/**
	 * 状态 - 禁用
	 */
	public static final Boolean STATUS_DISABLE = false;
}
