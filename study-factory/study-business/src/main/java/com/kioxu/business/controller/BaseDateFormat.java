package com.kioxu.business.controller;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BaseDateFormat extends SimpleDateFormat {
    String[] formats;

    public BaseDateFormat(String... formats) {
        super(formats[0], Locale.getDefault(Locale.Category.FORMAT));
        this.formats = formats;
    }

    @Override
    public Date parse(String source) throws ParseException {
        return DateUtils.parseDate(source, formats);
    }
}

