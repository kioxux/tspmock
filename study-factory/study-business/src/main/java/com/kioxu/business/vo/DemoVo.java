package com.kioxu.business.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DemoVo {
    /**
     * name
     */
    @JsonProperty("NAME_CODE")
    private String name;
    /**
     * age
     */
    @JsonProperty("AGE_CODE")
    private Integer age;
    /**
     * sex
     */
    @JsonProperty("sex")
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}