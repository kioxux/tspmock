package com.kioxu.business.exception;

import com.kioxu.business.constant.SystemConstants;

public class BusinessException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 8490949669644747709L;

    private int errorCode;
    private Object[] args;
    private BusinessException linkedException;
    private String propertyPath;


    public BusinessException(int errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public BusinessException(int errorCode, Object[] args){
        super();
        this.errorCode = errorCode;
        this.args = args;
    }

    public BusinessException(int errorCode, String message, String propertyPath){
        super(message);
        this.errorCode = errorCode;
        this.propertyPath = propertyPath;
    }

    public BusinessException(){
        super();
        errorCode = SystemConstants.ERROR_CODE_EXCEPTION;
    }

    public BusinessException(int errorCode,String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public BusinessException(String message) {
        super(message);
        errorCode = SystemConstants.ERROR_CODE_EXCEPTION;
    }

    public BusinessException(Throwable cause) {
        super(cause);
        errorCode = SystemConstants.ERROR_CODE_EXCEPTION;
    }

    public String getPropertyPath() {
        return propertyPath;
    }

    public void setPropertyPath(String propertyPath) {
        this.propertyPath = propertyPath;
    }

    public int getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Object[] getArgs() {
        return args;
    }
    public void setArgs(Object[] args) {
        this.args = args;
    }

    public BusinessException getLinkedException() {
        return linkedException;
    }

    public void setLinkedException(BusinessException linkedException) {
        this.linkedException = linkedException;
    }
}
