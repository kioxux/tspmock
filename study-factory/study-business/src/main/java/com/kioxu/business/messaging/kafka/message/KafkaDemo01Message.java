package com.kioxu.business.messaging.kafka.message;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class KafkaDemo01Message {
    public static final String TOPIC = "DEMO_01";

    /**
     * 编号
     */
    private Integer id;
}
