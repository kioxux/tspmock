package com.kioxu.business.inf;

import org.apache.tomcat.jni.User;

public interface UserInterface {
    void saveUser(User user);

    User getUser(long id);
}
