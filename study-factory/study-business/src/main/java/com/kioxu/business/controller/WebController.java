package com.kioxu.business.controller;

import com.kioxu.business.demo.DemoService;
import com.kioxu.business.vo.Result;
import com.kioxu.business.exception.SystemErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

@RestController
@RequestMapping("/web")
@Slf4j
public class WebController extends BaseController {
    @Autowired
    ThreadPoolExecutor threadPoolExecutor;
    @Autowired
    DemoService demoService;

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public Result test() {
        System.out.println("成功");
        System.out.println("当前线程：" + Thread.currentThread().getName());
        CompletableFuture.runAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getName());
        }, threadPoolExecutor);
        System.out.println("结束");
        Result result = Result.success();
        return result;
    }

    @RequestMapping(value = "/demo", method = RequestMethod.POST)
    public Result demo(@RequestBody Map param) {
        Result result;
        try {
            Map<String, Object> res = demoService.exampleOne(param);
            result = Result.success(res);
        } catch (Exception ex) {
            log.error("demo测试异常:" + ex.getMessage(), ex);
            result = Result.fail(SystemErrorType.SYSTEM_ERROR, ex.getMessage());
        }
        return result;
    }
}
