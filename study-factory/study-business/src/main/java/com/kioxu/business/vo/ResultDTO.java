package com.kioxu.business.vo;

import com.kioxu.business.constant.ResultResource;

/**
 * Created by Administrator on 2017/6/5 0005.
 */
public class ResultDTO<T> {
    private T data;

    private int result;

    private String message;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    /**
     * 扩展方法
     * @return
     */
    public static ResultDTO ok(){
        ResultDTO resultDto = new ResultDTO();
        resultDto.setResult(ResultResource.CODE_SUCCESS);
        resultDto.setMessage(ResultResource.MESSAGE_SUCCESS);
        return resultDto;
    }
    /**
     * 扩展方法
     * @param object
     * @return
     */
    public static ResultDTO ok(Object object){
        ResultDTO resultDto = new ResultDTO();
        resultDto.setResult(ResultResource.CODE_SUCCESS);
        resultDto.setMessage(ResultResource.MESSAGE_SUCCESS);
        resultDto.setData(object);
        return resultDto;
    }

    /**
     * 扩展方法
     * @return
     */
    public static ResultDTO codeParamError(){
        ResultDTO resultDto = new ResultDTO();
        resultDto.setResult(ResultResource.CODE_PARAM_ERROR);
        resultDto.setMessage(ResultResource.MESSAGE_PARAM_ERROR);
        return resultDto;
    }

    /**
     * 扩展异常方法
     * @return
     */
    public static ResultDTO error(int CODE,String message){
        ResultDTO resultDto = new ResultDTO();
        resultDto.setResult(CODE);
        resultDto.setMessage(message);
        return resultDto;
    }

}
