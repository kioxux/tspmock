package com.kioxu.business.vo;

import com.kioxu.business.constant.Constants;
import com.kioxu.business.constant.SystemConstants;
import com.kioxu.business.util.BeanUtils;

import javax.persistence.Id;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

public class OutVo<T> implements Serializable {
    private int respCode = 0;// 响应code 0 失败 1 成功 5 在其他设备已登陆
    private String respMsg;// app显示消息
    private String errorMsg;// 错误信息
    private String token;
    private T data;
    private String lastRowId;
    private String propertyPath;// 异常属性路径

    public OutVo<T> setSuccess(){
        this.respCode = SystemConstants.ERROR_CODE_OK;
        this.respMsg = Constants.SUCCESS;
        return this;
    }

    public OutVo<T> setSuccess(T data){
        this.data = data;
        return setSuccess();
    }

    public OutVo<T> setError(int respCode, String respMsg){
        this.respCode = respCode;
        this.respMsg = respMsg;
        return this;
    }

    public OutVo<T> setError(String errorMsg){
        this.respMsg = Constants.SYSTEMEXCEPTION;
        this.errorMsg = errorMsg;
        return this;
    }

    public int getRespCode() {
        return respCode;
    }

    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public T getData() {
        return data;
    }

    /**
     * 如果值是list时自动会查找最后行id值注入lastRowId字段
     *
     * @param data 数据
     */
    public void setData(T data) {
        // 初始化最后行的id
        initLastRowId(data);
        this.data = data;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getLastRowId() {
        return lastRowId;
    }

    public void setLastRowId(String lastRowId) {
        this.lastRowId = lastRowId;
    }

    public String getPropertyPath() {
        return propertyPath;
    }

    public void setPropertyPath(String propertyPath) {
        this.propertyPath = propertyPath;
    }

    private void initLastRowId(Object obj) {
        if(!(obj instanceof List)) {
            return;
        }
        List list = (List)obj;
        if(list == null || list.isEmpty()) {
            return;
        }

        try {
            Object o = list.get(list.size()-1);
            // 查询主键id
            Field[] fds = o.getClass().getDeclaredFields();
            for (int i = 0; i < fds.length; i++) {
                Field d = fds[i];
                Id id = d.getAnnotation(Id.class);
                if(id != null) {
                    d.setAccessible(true);
                    Object val = d.get(o);
                    if(val != null) {
                        lastRowId = val.toString();
                    }
                    return;
                }
            }
            // 获取值
            Object val = BeanUtils.getPropertyValue(o, "id");
            if(val == null) {
                return;
            }

            lastRowId = val.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
