//package com.kioxu.business.messaging.rabbitmq.entity;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
////@Component
////@RabbitListener(queues = RabbitDemo01Message.QUEUE)
//public class RabbitDemo01Consumer {
//    private Logger logger = LoggerFactory.getLogger(getClass());
//
//    @RabbitHandler
//    public void onMessage(RabbitDemo01Message message) {
//        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
//    }
//
////    @RabbitHandler(isDefault = true)
////    public void onMessage(org.springframework.amqp.core.Message message) {
////        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
////    }
//}
