package com.kioxu.business.vo;

import lombok.Data;

@Data
public class User {
    private String name;
    private int age;
}
