package com.kioxu.business.demo;

import cn.hutool.core.util.IdcardUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

//@SpringBootApplication
@Slf4j
public class LearnApplication implements CommandLineRunner {
    public static void main(String[] args) {
        new SpringApplication(LearnApplication.class).run(args);
    }

//    @Override
//    public void run(String... args) throws Exception {
//        JSONObject resultObject = JSON.parseObject("{\"result\":\"success\",\"code\":\"1000\",\"msg\":\"操作成功\",\"data\":\n" +
//                "{\"path\":\"a1e2231415cb411de18f7278gg1g3c71d6\",\"transactionId\":\"978a30ea2b46e1f298\n" +
//                "6602474a3ae251\",\"viewPdfUrl\":\"https://FDDServer:Port/api//viewdocs.action?\n" +
//                "app_id=xxxxxx&timestamp=20210901144453&v=2.0&msg_digest=DM2Qzg2MUODM1Q0NEMzdkFI1\n" +
//                "MzQTA2Q0EhGNzOEREQUI0MCRTIwEQQ==&transaction_id=9252b4a783ae981a30ea6646e1f20247\n" +
//                "&send_app_id=null\",\"downloadUrl\":\"https://FDDServer:Port/api//getdocs.action?\n" +
//                "app_id=xxxxxx&timestamp=20210901144453&v=2.0&msg_digest=DM2Qzg2MUODM1Q0NEMzdkFI1\n" +
//                "MzQTA2Q0EhGNzOEREQUI0MCRTIwEQQ==&transaction_id=9252b4a783ae981a30ea6646e1f20247\n" +
//                "&send_app_id=null\",\"uuid\":null}}");
//        System.out.println("hello：" + resultObject);
//    }


//    @Override
//    public void run(String... args) throws Exception {
//
//        // 定义一个Java对象
//        User user = new User("John", 30);
//
//        // 将Java对象转换成Map对象
//        Map<String, Object> userMap = BeanUtil.beanToMap(user);
//        System.out.println(userMap);
//    }

//    @Override
//    public void run(String... args) throws Exception {
//        Files.deleteIfExists(Paths.get("E:\\cs.txt"));
//        File file = new File("E:\\cs.txt");
//        FileOutputStream output = new FileOutputStream("E:\\cs.pdf");
//        PdfReader pdfReader = new PdfReader("E:\\cs.pdf");
//        PdfStamper pdfStamper = new PdfStamper(pdfReader, output);
//        System.out.println(file.getName());
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        List<String> list = new ArrayList<>();
//        list.add("hello");
//        list.add("world");
//        list.add(null);
//        list.forEach(e -> {
//            System.out.println(e);
//        });
//        System.out.println(StringUtils.isNotBlank(null));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        if (1==1) {
//            throw new RuntimeException("异常");
//        }
//        try {
////            throw new RuntimeException("异常");
//        } finally {
//            System.out.println("ok");
//        }
//        if (1==1) {
//            throw new RuntimeException("异常");
//        }
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println(JSON.parseArray(null));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        log.info("开始执行------");
//        try {
//            log.info("代码执行中------");
//        } finally {
//            log.info("finally执行------");
//        }
//        log.info("业务执行完毕------");
//        throw new RuntimeException("异常了");
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        DemoClass demoClass = new DemoClass();
//        demoClass.setName("1323");
//        System.out.println(optNotBlankStrOpt(demoClass.getName()).isPresent());
//    }
//
//    @Data
//    @NoArgsConstructor
//    class DemoClass {
//        String name;
//        Integer age;
//    }
//
//    public Optional<String> optNotBlankStrOpt(Object obj) {
//        return Optional.ofNullable(obj).map(String::valueOf).filter(StringUtils::isNotBlank).filter(e -> !"undefined".equals(e)).filter(e -> !"null".equals(e));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        // 创建文档对象并设置页面大小
//        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
//
//        // 创建PdfWriter对象，将文档对象写入到磁盘文件
//        PdfWriter.getInstance(document, new FileOutputStream("周爱华还款试算表.pdf"));
//        Font font = FontFactory.getFont("SimSun", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//
//        // 打开文档对象
//        document.open();
//
//        // 创建标题
//        Paragraph title = new Paragraph("还款试算表");
//        title.setAlignment(Paragraph.ALIGN_CENTER);
//        document.add(title);
//
//        // 添加客户信息
//        Paragraph customerInfo = new Paragraph();
//        customerInfo.setFont(font);
//        customerInfo.add("客户姓名： 周爱华 ");
//        customerInfo.add("证件号码： 321028197107163410 ");
//        document.add(customerInfo);
//
//        // 添加贷款期限和打印时间信息
//        Paragraph loanInfo = new Paragraph();
//        loanInfo.setFont(font);
//        loanInfo.add("贷款期限： 36个月 ");
//        loanInfo.add("打印时间： " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//        document.add(loanInfo);
//
//        // 创建还款表格
//        PdfPTable table = new PdfPTable(3);
//        PdfPCell cell = new PdfPCell();
//
//        // 添加表头行
//        cell.setPhrase(new Paragraph("期号", font));
//        table.addCell(cell);
//        cell.setPhrase(new Paragraph("到期日", font));
//        table.addCell(cell);
//        cell.setPhrase(new Paragraph("期供金额", font));
//        table.addCell(cell);
//
//        // 添加数据行
//        for (int i = 0; i <= 36; i++) {
//            table.addCell(String.valueOf(i));
//            table.addCell("2023-05-06");
//            table.addCell(i == 0 ? "0" : "4550.4");
//        }
//
//        // 添加表格
//        document.add(table);
//
//        // 添加注意事项
//        Paragraph notice = new Paragraph();
//        notice.setFont(font);
//        notice.add("注意：\n");
//        notice.add("1.此还款试算表为查询当日计算生成所得的试算表，还款金额与实际略有差异,请以放款后放款日生成的还款计划表为准！\n");
//        notice.add("2.在等额本金的还款方式项下,每期还款金额均不一致。在等额本息还款方式项下,首期与末期还款周期不足月还款金额低于其他期还款金额、首期与末期还款周期足月则还款金额与其他期还款金额一致、如首期与末期还款周期超过足月还款金额高于其他期还款金额。\n");
//        document.add(notice);
//
//        // 关闭文档对象
//        document.close();
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        // 创建 PDF 文件
//        Document document = new Document();
//        PdfWriter.getInstance(document, new FileOutputStream("周爱华还款表.pdf"));
//
//        // 打开文档
//        document.open();
//        // 设置中文字体
//        BaseFont bfChinese = BaseFont.createFont("c://windows//fonts//simsun.ttc,1", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//        Font font = new Font(bfChinese, 12, Font.NORMAL);
//        Font titleFont = new Font(bfChinese, 30, Font.BOLD);
//
//        String empty = "                                 ";
//
//        // 添加标题
//        Paragraph title = new Paragraph("还款试算表", titleFont);
//        title.setAlignment(Element.ALIGN_CENTER);
//        title.setSpacingAfter(20f);
//        document.add(title);
//
//        // 添加客户信息
//        Paragraph customerInfo = new Paragraph();
//        customerInfo.add(new Chunk("客户姓名： ", font));
//        customerInfo.add(new Chunk("周爱华"+empty, font));
//        customerInfo.add(new Chunk(" 证件号码： ", font));
//        customerInfo.add(new Chunk("321028197107163410", font));
//        customerInfo.setAlignment(Element.ALIGN_JUSTIFIED);
//        document.add(customerInfo);
//
//        // 添加贷款信息
//        Paragraph loanInfo = new Paragraph();
//        loanInfo.add(new Chunk("贷款期限： ", font));
//        loanInfo.add(new Chunk("36个月"+empty, font));
//        loanInfo.add(new Chunk(" 打印时间： ", font));
//        loanInfo.add(new Chunk(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), font));
//        loanInfo.setAlignment(Element.ALIGN_JUSTIFIED);
//        loanInfo.setSpacingAfter(20f);
//        document.add(loanInfo);
//
//        // 添加表格
//        PdfPTable table = new PdfPTable(3);
//        table.setWidthPercentage(100);
//        table.setSpacingBefore(10f);
//        table.setSpacingAfter(10f);
//
//        // 添加表头
//        Stream.of("期号", "到期日", "期供金额").forEach(columnTitle -> {
//            PdfPCell header = new PdfPCell();
//            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
//            header.setPhrase(new Phrase(columnTitle, font));
//            header.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(header);
//        });
//
//        // 添加表格数据
//        for (int i = 0; i <= 36; i++) {
//            PdfPCell contentIssue = new PdfPCell(new Phrase(String.valueOf(i)));
//            contentIssue.setHorizontalAlignment(Element.ALIGN_CENTER);
//            PdfPCell contentExpiryDate = new PdfPCell(new Phrase("2023-05-06"));
//            contentExpiryDate.setHorizontalAlignment(Element.ALIGN_CENTER);
//            PdfPCell contentPaymentAmount = new PdfPCell(new Phrase(i == 0 ? "0" : "4550.4"));
//            contentPaymentAmount.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(contentIssue);
//            table.addCell(contentExpiryDate);
//            table.addCell(contentPaymentAmount);
//        }
//        document.add(table);
//
//        // 添加注意事项
//        Paragraph note = new Paragraph();
//        note.setSpacingBefore(10f);
//        note.add(new Chunk("注意：", font));
//        note.add(new Chunk("\n 1. 此还款试算表为查询当日计算生成所得的试算表，还款金额与实际略有差异，请以放款后放款日生成的还款计划表为准！",
//                font));
//        note.add(new Chunk("\n 2. 在等额本金的还款方式项下，每期还款金额均不一致。在等额本息还款方式项下，首期与末期还款周期不足月还款金额低于其他期还款金额，首期与末期还款周期足月则还款金额与其他期还款金额一致，如首期与末期还款周期超过足月还款金额高于其他期还款金额。",
//                font));
//        document.add(note);
//
//        // 关闭文档对象
//        document.close();
//        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
//        response.reset();
//        response.setContentType("application/pdf");
//        response.setContentLength(fileContent.length);
//        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
//        response.getOutputStream().write(fileContent);
//        response.getOutputStream().flush();
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println(new BigDecimal(0.0));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        Map result = new HashMap();
//        Map org = Optional.ofNullable(createMap("null")).orElse(new HashMap());
//        Map result = new HashMap();
//        if (false) {
//            result = createMap("null");
//        }
//        result.put("id", 1);
//        result = (Map) SerializationUtils.clone((Serializable) org);
//        System.out.println(JSON.toJSONString(result));
//    }
//
//    Map createMap(String param) {
//        if (StringUtils.isNotBlank(param)) {
//            return JSON.parseObject("{\"dateSchedual\": \"2023-03-20\",\"residualPrincipal\": 95700.0,\"billId\": \"0\",\"principalReceivable\": 0.0,\"feeGuaranteeReceivable\": 0.0,\"interestReceivable\": 0.0}", new TypeReference<Map>(){});
//        } else {
//            return null;
//        }
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        User user = new User();
//        user.setAge(20);
//        user.setName("目标");
//        User res = new User();
//        res.setId(10);
//        BeanUtils.copyProperties(user, res);
//        System.out.println(JSON.toJSONString(user));
//        System.out.println(JSON.toJSONString(res));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        User user = new User();
//        user.setStatus(user.getStatus() | 256);
//        user.setStatus(user.getStatus() | 4);
//        System.out.println(JSON.toJSONString(user));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        BigDecimal divide = BigDecimal.valueOf(10).divide(BigDecimal.valueOf(0), 0, BigDecimal.ROUND_DOWN);
//        System.out.println(divide);
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        List<User> list = Arrays.asList(new User(1,18,"one1",1), new User(2,20,"two",2), new User(3,20,"one2",1));
////        List<User> list = new ArrayList<>();
//        boolean allStartWithA = list.stream().anyMatch(str -> str.getStatus() > 1);
//        System.out.println(allStartWithA);
//    }

//    @Data
//    @NoArgsConstructor
//    @AllArgsConstructor
//    class User {
//        private int id;
//        private Integer age;
//        private String name;
//        private Integer status = 0;
//    }

//    @Override
//    public void run(String... args) throws Exception {
//        Timestamp timestamp = new Timestamp(new Date("2023-02-14 10:10:10").getTime());
//        System.out.println(timestamp);
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        String pdfFilePath = "E:\\工作资料\\文档说明\\陈素青.pdf";
//        String outputDirectory = "E:\\工作资料\\文档说明\\";
//
//        try (PDDocument document = PDDocument.load(new File(pdfFilePath))) {
//            int pageNum = 0;
//            for (PDPage page : document.getPages()) {
//                pageNum++;
//                PDResources resources = page.getResources();
//                for (COSName cosName : resources.getXObjectNames()) {
//                    PDXObject xObject = resources.getXObject(cosName);
//                    if (xObject instanceof PDImageXObject) {
//                        PDImageXObject image = (PDImageXObject) xObject;
//                        BufferedImage bufferedImage = image.getImage();
//                        String outputFilePath = outputDirectory + "signature_page_" + pageNum + ".png";
//                        ImageIO.write(bufferedImage, "png", new File(outputFilePath));
//                        System.out.println("Signature image extracted and saved: " + outputFilePath);
//                    }
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        String pdfFilePath = "E:\\工作资料\\文档说明\\陈素青.pdf";
//        String outputImagePath = "E:\\工作资料\\文档说明\\image.png";
//
//        try (PDDocument document = PDDocument.load(new File(pdfFilePath))) {
//            PDFRenderer pdfRenderer = new PDFRenderer(document);
//            int pageNumber = 7; // Assuming the signature is on the first page
//
//            // Extract the signature image from the first page
//            BufferedImage image = pdfRenderer.renderImageWithDPI(pageNumber, 300);
//            ImageIO.write(image, "PNG", new File(outputImagePath));
//
//            System.out.println("Signature image extracted and saved successfully.");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        try {
//            // 加载PDF文档
//            PDDocument document = PDDocument.load(new File("E:\\工作资料\\文档说明\\陈素青.pdf"));
//
//            // 创建PDFRenderer实例
//            PDFRenderer renderer = new PDFRenderer(document);
//
//            // 获取第一页
//            PDPage page = document.getPage(7);
//
//            // 获取页面上的所有注释（签章）
//            for (PDAnnotation annotation : page.getAnnotations()) {
//                if (annotation.getSubtype().equals("Widget")) {
//                    // 提取签章位置信息
//                    float x = annotation.getRectangle().getLowerLeftX();
//                    float y = annotation.getRectangle().getLowerLeftY();
//                    float width = annotation.getRectangle().getWidth();
//                    float height = annotation.getRectangle().getHeight();
//
//                    // 创建一个与签章大小相同的图像
//                    BufferedImage image = renderer.renderImage(7);
//                    BufferedImage signatureImage = image.getSubimage((int) x, (int) y, (int) width, (int) height);
//
//                    // 保存签章图像为文件
//                    File outputFile = new File("E:\\工作资料\\文档说明\\signature.png");
//                    ImageIO.write(signatureImage, "png", outputFile);
//                }
//            }
//
//            // 关闭PDF文档
//            document.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        String str = "";
//        System.out.println(StringUtils.trimToEmpty(str));
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        long time = System.currentTimeMillis();
//        String param = JSON.toJSONString(new HashMap<String, Object>(){
//            {
//                put("orderId", 110246);
//            }
//        });
//        String sign = DigestUtils.sha512Hex(param + "L7uveSrVTfAgCx0L5SOkvnC6sViB56Aa" + time);
//        System.out.println("param------"+param);
//        System.out.println("time------"+time);
//        System.out.println("sign------"+sign);
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        Demo demo = new Demo();
//        demo.setId(2);
//        demo.setName("hello");
//        System.out.println(Objects.equals(1, demo.getId()));
//        if (Objects.equals(1, demo.getId())) {
//            if (StringUtils.isNotBlank(demo.getName())) demo.setName(null);
//        }
//        System.out.println(JSON.toJSONString(demo));
//    }
//
//    @Data
//    @NoArgsConstructor
//    @AllArgsConstructor
//    class Demo {
//        private Integer id;
//        private String name;
//    }


//    @Override
//    public void run(String... args) throws Exception {
//        List<Integer> originalList = new ArrayList<>();
//        ThreadConfig threadConfig = new ThreadConfig();
//        ThreadPoolExecutor threadPoolExecutor = threadConfig.threadPoolExecutor();
//        originalList.add(1);
//        originalList.add(2);
//        originalList.add(3);
//        originalList.add(4);
//        originalList.add(5);
//        originalList.add(6);
//        originalList.add(7);
//        originalList.add(8);
//        originalList.add(9);
//        originalList.add(10);
//
//        int batchSize = 3; // 切分的批次大小
//
//        // 计算切分次数
//        int totalBatches = (int) Math.ceil((double) originalList.size() / batchSize);
//
//        CompletableFuture<Void>[] futures = new CompletableFuture[totalBatches];;
//        for (int i = 0; i < totalBatches; i++) {
//            // 计算起始索引和结束索引
//            int startIndex = i * batchSize;
//            int endIndex = Math.min(startIndex + batchSize, originalList.size());
//
//            // 切分子列表
//            List<Integer> sublist = originalList.subList(startIndex, endIndex);
//
//            // 对子列表进行操作
//            System.out.println(Thread.currentThread().getName() +"------------"+ "Batch " + (i + 1) + ": " + sublist);
////            threadPoolExecutor.execute(new Runnable() {
////                @Override
////                public void run() {
////                    fun(sublist);
////                }
////            });
//            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
//                fun(sublist);
//            }, threadPoolExecutor);
//            futures[i] = future;
//        }
//        CompletableFuture.allOf(futures).get();
//        System.out.println(Thread.currentThread().getName() + "已完成");
//    }

//    public void fun(List<Integer> list) {
//        System.out.println(Thread.currentThread().getName()+"--------"+JSON.toJSONString(list));
//    }


    @Override
    public void run(String... args) throws Exception {
        String ID_18 = "830000197503020051";
        String ID_15 = "150102880730303";

        //是否有效
        boolean valid = IdcardUtil.isValidCard(ID_18);
        boolean valid15 = IdcardUtil.isValidCard(ID_15);
        System.out.println(valid);
        System.out.println(valid15);

        //省份
        String province = IdcardUtil.getProvinceByIdCard(ID_18);
        System.out.println(province);
    }
}
