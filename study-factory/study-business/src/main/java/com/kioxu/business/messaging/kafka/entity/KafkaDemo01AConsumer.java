package com.kioxu.business.messaging.kafka.entity;

import com.kioxu.business.messaging.kafka.message.KafkaDemo01Message;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

//@Component
public class KafkaDemo01AConsumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @KafkaListener(topics = KafkaDemo01Message.TOPIC, groupId = "demo01-A-consumer-group-" + KafkaDemo01Message.TOPIC)
    public void onMessage(ConsumerRecord<Integer, String> record) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), record);
    }
}
