package com.kioxu.business.util;

import com.kioxu.business.annotation.CryptIntercept;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * api使用加解密类(只允许api使用)
 *
 * @author hujp
 * @date 2019/09/23
 *
 */
public class CryptUtils {
    /**
     * 日志写入对象
     */
    protected static Logger logger = LoggerFactory.getLogger(CryptUtils.class);

    /**
     * AES加密解密
     */
    private static final String AESKEY = "o172qGNEnRPB5oK5z2uoTMDzrvSJYx3U";

    /**
     * 加密算法
     */
    private static final String ALGORITHM = "AES";

    /**
     * algorithm/mode/padding
     */
    private static final String ALGORITHM_STR = "AES/CBC/PKCS5Padding";
    private static final byte[] IV = "0102030405060708".getBytes();

    /**
     * 加密对象
     */
    private static Cipher encryptCipher;

    /**
     * 解密对象
     */
    private static Cipher decodeCipher;

    private static Charset defaultCharset = Charset.forName("UTF-8");

    static {
        try {
            IvParameterSpec sp = new IvParameterSpec(IV);

            SecretKeySpec key = new SecretKeySpec(AESKEY.getBytes(), ALGORITHM);
            encryptCipher = Cipher.getInstance(ALGORITHM_STR);
            encryptCipher.init(Cipher.ENCRYPT_MODE, key, sp);
            // -------加密对象初始化完成------

            // -------解密对象初始化---------
            decodeCipher = Cipher.getInstance(ALGORITHM_STR);
            decodeCipher.init(Cipher.DECRYPT_MODE, key, sp);
        } catch (Exception e) {
            logger.error("错误：初始化加密、解密失败！", e);
            e.printStackTrace();
        }
    }

    /**
     * 先app解密（数据传输要求）后将字符串加密（存储要求） - base64编码<br>
     * <li><b>应用场景：</b>需要将前端传过来的数据加密存储到数据库</li>
     *
     * @param encryptString
     * @return
     */
    public static String encrypt(String encryptString) {
        // 将app传输的数据进行解密
        String yStr = baseDecrypt(encryptString);
        // 系统加密
        return CryptDESUtil.encryptDES(yStr);
    }

    /**
     * 先字符串解密（存储要求）后将app加密（数据传输要求）- hex编码方式 <br>
     * <li><b>应用场景：</b>需要将数据数据的数据解密后再加密传输给客户端</li>
     *
     * @param decryptString
     * @return
     */
    public static String decrypt(String decryptString) {
        // 系统解码
        String mStr = CryptDESUtil.decode(decryptString);
        // app 加密
        return baseEncrypt(mStr);
    }

    /**
     * app 加密 (hex)<br>
     * <li><b>应用场景：</b>客户端与服务端交互的数据传输加密</li>
     *
     * @param encryptString
     *            需要加密串
     * @return 加密串
     */
    public static String baseEncrypt(String encryptString, Charset charset) {
        try {
            byte[] encryptedData = encryptCipher.doFinal(encryptString.getBytes(charset));
            return Hex.encodeHexString(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("警告：app hex 加密失败！", e);
        }

        return encryptString;
    }

    /**
     * app 加密 (hex)<br>
     * <li><b>应用场景：</b>客户端与服务端交互的数据传输加密</li>
     *
     * @param encryptString
     *            需要加密串
     * @return 加密串
     */
    public static String baseEncrypt(String encryptString) {
        return baseEncrypt(encryptString, StandardCharsets.UTF_8);
    }

    /**
     * 针对api与app（客户端）交互内容加密 （hex展示形式）
     *
     * @param decryptString
     *            需要加密串
     * @return
     */
    public static String baseDecrypt(String decryptString) {
        return baseDecrypt(decryptString, defaultCharset);
    }

    /**
     * 针对api与app（客户端）交互内容解密 （hex展示形式）
     *
     * @param decryptString
     *            需要解密串
     * @param charset
     *            字符编码
     * @return
     */
    public static String baseDecrypt(String decryptString, Charset charset) {
        try {
            byte[] byteMi = Hex.decodeHex(decryptString.toCharArray());

            byte decryptedData[] = decodeCipher.doFinal(byteMi);
            return new String(decryptedData, charset);
        } catch (Exception e) {
            logger.warn("警告：app hex  解密失败！");
        }

        return decryptString;
    }

    /**
     * 解密对象（标识 @CryptIntercept 标签进行解密）
     *
     * @param obj 需要解密对象
     */
    public static void decryptBean(Object obj, Class supClass) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass(), supClass);
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                // 过滤class属性
                if (!key.equals("class")) {
                    // 得到property对应的getter方法
                    propDec(obj, property, true);
                }
            }
        } catch (Exception e) {
            logger.error("内省期发生异常！");
        }
    }

    /**
     * 加密对象（标识 @CryptIntercept 标签进行加密）
     *
     * @param obj 需要加密对象
     */
    public static void encryptBean(Object obj) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                // 过滤class属性
                if (!key.equals("class")) {
                    // 得到property对应的getter方法
                    propDec(obj, property, false);
                }
            }
        } catch (Exception e) {
            logger.error("内省期发生异常！");
        }
    }

    private static void propDec(Object obj, PropertyDescriptor property, boolean isDec) {
        try {
            Method getter = property.getReadMethod();
            if(getter == null) {
                return;
            }

            Field field = null;
            try {
                field = getter.getDeclaringClass().getDeclaredField(property.getName());
            } catch (Exception e) {
                logger.debug(String.format("属性【%s】不存在！", property.getName()));
            }

            boolean b = false;
            if(field != null && field.getAnnotation(CryptIntercept.class) != null) {
                b = true;
            }

            if(getter.getAnnotation(CryptIntercept.class) != null) {
                b = true;
            }

            // 未查找到标签则不操作
            if(!b) {
                return;
            }

            Object value = getter.invoke(obj);
            if(value == null || "".equals(value.toString())) {
                return;
            }

            if(isDec) {
                // 解密
                value = baseDecrypt(value.toString(), Charset.forName("UTF-8"));
            } else {
                value = baseEncrypt(value.toString());
            }
            Method setter = property.getWriteMethod();
            setter.invoke(obj, value);
        } catch (Exception e) {
            logger.warn(String.format("字段【%s】解密失败！", property.getName()));
        }
    }

    public static void main(String[] args) {
        String str = baseEncrypt("张三李四王五赵六hhh111!!!!");
        System.out.println(str);
        System.out.println(baseDecrypt("8b71aca58c00b114371da4edb271d74cc14ae29adbe7eb9f0ad2c426ab2f6e95a3d111ddc0cd6b7895388f96672a7ee5"));
    }
}

