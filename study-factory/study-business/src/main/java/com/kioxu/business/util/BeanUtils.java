package com.kioxu.business.util;

import com.kioxu.business.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class BeanUtils {
    private static Logger logger = LoggerFactory.getLogger(BeanUtils.class);

    /**
     * 异常
     * @param isError 是否抛异常
     * @param massage 异常内容
     * @throws BusinessException
     */
    private static void fail(boolean isError, String massage) throws BusinessException {
        if(isError) {
            throw new BusinessException(1, massage);
        }
    }

    /**
     * Bean转Map
     * @param obj 转换对象
     * @param supClass 截止到哪个父类
     * @return
     * @throws BusinessException
     */
    public static Map<String, Object> convertBeanToMap(Object obj, Class supClass, boolean isFilterNull) {
        Map<String, Object> result = new HashMap<>();
        try {
            PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(obj, supClass);
            for (PropertyDescriptor property : propertyDescriptors) {
                // 得到property对应的getter方法
                Object val = getValue(obj, property);
                if(isFilterNull) {
                    // 判断
                    if(val == null || (val instanceof String && "".equals(((String) val).trim()))) {
                        continue;
                    }
                }
                result.put(property.getName(), val);
            }
        } catch (Exception e) {
            logger.debug("属性处理判断异常！");
        }

        return result;
    }

    private static Object getValue(Object obj, PropertyDescriptor property) {
        try {
            Method m = property.getReadMethod();
            if(m != null) {
                return m.invoke(obj);
            }
        } catch (Exception e) {
        }

        return null;
    }

    private static PropertyDescriptor[] getPropertyDescriptors(Object obj, Class supClass) throws IntrospectionException {
        BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass(), supClass);
        return beanInfo.getPropertyDescriptors();
    }

    /**
     * 获得属性值
     *
     * @param obj 对象
     * @param name 属性名
     * @return
     */
    public static Object getPropertyValue(Object obj, String name) {
        try {
            PropertyDescriptor[] propertys = getPropertyDescriptors(obj, Object.class);
            for (int i = 0; i < propertys.length; i++) {
                PropertyDescriptor p = propertys[i];
                if(p.getName().equals(name)) {
                    return p.getReadMethod().invoke(obj);
                }
            }
        } catch (Exception e) {
        }

        return null;
    }

}
