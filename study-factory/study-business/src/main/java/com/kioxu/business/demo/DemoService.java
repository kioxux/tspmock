package com.kioxu.business.demo;

import java.util.Map;

public interface DemoService {
    Map<String, Object> demo(Map param);

    Map<String, Object> exampleOne(Map param);
}
