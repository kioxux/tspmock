//package com.kioxu.business.messaging.rabbitmq.entity;
//
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class RabbitDemo02Producer {
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//    public void syncSend(Integer id, String routingKey) {
//        // 创建 Demo02Message 消息
//        RabbitDemo02Message message = new RabbitDemo02Message();
//        message.setId(id);
//        // 同步发送消息
//        rabbitTemplate.convertAndSend(RabbitDemo02Message.EXCHANGE, routingKey, message);
//    }
//}
