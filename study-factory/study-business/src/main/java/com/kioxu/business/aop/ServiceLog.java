package com.kioxu.business.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Aspect
@Slf4j
public class ServiceLog {
    @Before("serviceLog()")
    public void before(JoinPoint joinPoint) {
        try {
            Object[] obj = joinPoint.getArgs();
            String params = "";
            for (Object o : obj) {
                if (o instanceof List) {
                    o = ((List) o).size();
                }
                params = params + "|" + o;
            }
            log.info("service before:" + (joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName() + "()"
                    + " params:" + params));
        } catch (Exception e) {
            //记录本地异常日志
            log.error("==前置通知异常==");
            log.error("异常信息:{}", e.getMessage());
        }
    }

    @Pointcut("execution(* com.kioxu..service..*.*(..))")
    public void serviceLog() {
    }

    @After("serviceLog()")
    public void after(JoinPoint joinPoint) {
        Object[] obj = joinPoint.getArgs();
        String params = "";
        for (Object o : obj) {
            if (o instanceof List) {
                o = ((List) o).size();
            }
            params = params + "|" + o;
        }
        log.info(" service after:" + (joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName() + "()"
                + " params:" + params));
    }
}
