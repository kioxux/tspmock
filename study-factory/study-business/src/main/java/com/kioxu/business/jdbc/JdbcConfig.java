package com.kioxu.business.jdbc;

import com.sun.istack.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JdbcConfig {
    @Bean
    public MyJdbcTemplate MyJdbcTemplate(@Nullable DataSource dataSource){
        if(dataSource==null){
            return new MyJdbcTemplate();
        }else{
            return new MyJdbcTemplate(dataSource);
        }
    }
}

