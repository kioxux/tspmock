package com.kioxu.business.service;

import org.springframework.stereotype.Service;

@Service
public class MyService {

    public String getMessage() {
        return "Hello, Spring!";
    }
}

