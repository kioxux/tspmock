package com.kioxu.business.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.nio.charset.Charset;
import java.security.SecureRandom;

/**
 * 提供一个已DES模式的加密和解密工具类
 *
 * @author
 *
 */
public class CryptDESUtil {

    /**
     * 加密密钥
     */
    private static final String privateKey = "Fangjinsuo123";

    /**
     * 加密对象
     */
    private static Cipher encryptCipher;

    /**
     * 解密对象
     */
    private static Cipher decodeCipher;

    static {
        try {
            // -------加密对象----------
            // 生成一个可信任的随机数源
            SecureRandom sr = new SecureRandom();
            // 从原始密钥数据创建DESKeySpec对象
            DESKeySpec dks = new DESKeySpec(privateKey.getBytes());
            // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(dks);
            // Cipher对象实际完成加密操作
            encryptCipher = Cipher.getInstance("DES");
            // 用密钥初始化Cipher对象
            encryptCipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
            // -------加密对象初始化完成------

            // -------解密对象初始化---------
            // 生成一个可信任的随机数源
            SecureRandom sr1 = new SecureRandom();
            // 从原始密钥数据创建DESKeySpec对象
            DESKeySpec dks1 = new DESKeySpec(privateKey.getBytes());
            // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
            SecretKeyFactory keyFactory1 = SecretKeyFactory.getInstance("DES");
            SecretKey securekey1 = keyFactory1.generateSecret(dks1);
            // Cipher对象实际完成解密操作
            decodeCipher = Cipher.getInstance("DES");
            // 用密钥初始化Cipher对象
            decodeCipher.init(Cipher.DECRYPT_MODE, securekey1, sr1);
            // -------解密对象初始化完成------
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将字符加密
     *
     * @param val
     *            需要加密的串
     * @return 如果加密失败则返回原始字符串
     */
    public static String encryptDES(String val) {
        // 查看是否已经加密
        try {
            //进行解密操作 -- 如果能解密开则ok，无需加密
            baseDecode(val);

            return val;
        } catch (Exception e1) {
            //异常情况则判断为需进行加密
            try {
                return baseEncryptDES(val);
            } catch (Exception e) {
                return val;
            }
        }
    }

    /**
     * 原始加密方法(请使用{@link CryptDESUtil#encryptDES(String)}方法)
     *
     * @param val
     * @return
     * @throws Exception
     */
    public static String baseEncryptDES(String val) throws Exception {
        byte[] bys = encryptCipher.doFinal(val.getBytes(Charset.forName("UTF-8")));
        return Base64.encode(bys);
    }

    /**
     * 将字符解密
     *
     * @param val
     *            需要解密的串
     * @return
     */
    public static String decode(String val) {
        try {
            return baseDecode(val);
        } catch (Exception e) {
            return val;
        }
    }

    /**
     * 原始解密方法
     *
     * @param val
     * @return
     * @throws Exception 解密失败
     */
    private static String baseDecode(String val) throws Exception {
        byte[] bys = decodeCipher.doFinal(Base64.decode(val));
        return new String(bys, Charset.forName("UTF-8"));
    }

    /**
     * 1. 先解密
     * 2. 判断最后一位是否包含小写的x
     * 3. 如果包含则转为大写
     * 4. 加密返回
     * @param val
     * @return
     */
    public static String formatCardNo(String val){
        if(val==null||"".equals(val)){return val;}
        try {
            String cardNo = decode(val);
            if(cardNo.endsWith("x")){
                cardNo = cardNo.toUpperCase();
                return baseEncryptDES(cardNo);
            }
        } catch (Exception e) {
        }
        return val;
    }

    public static void main(String[] args) throws Exception {
        String name = "VjdUf1sPVs5snN1IPnhlpYYVzrZRMIyX";
        String pp= decode(name);
        System.out.println(">>>>>>>>>>>>>>>>>>:"+pp);

        //System.out.println("==="+baseDecode("6nYqc3dyYDsJSeGG0/8/T3vBceelqnXk"));
        String jm = baseEncryptDES("13050319890706003X");
        String jm2 = baseEncryptDES("13050319890706003x");

        System.out.println(jm);
        System.out.println(jm2);
        String aa = "42028119900819575x";
        System.out.println(aa.endsWith("x"));
        System.out.println(aa.endsWith("X"));
//
//		String jmm = encryptDES("2222222");
//		System.out.println(jmm);
//		System.out.println(baseDecode("bNq8A5ibkczF3cFemVV6ZEBlp7IUK+1q"));
//		System.out.println(baseDecode("L4AdrgM8i4NHJ2Ol7KGdyQ=="));
//		System.out.println(baseEncrypt("15021369282"));
//		Map<String,Object> map =  new HashMap<String,Object>(0);
//		map.put("eee","ee");
//		System.out.println(map.toString());

//		System.out.println(jmm);
    }

}
