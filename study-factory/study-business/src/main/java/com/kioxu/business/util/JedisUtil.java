package com.kioxu.business.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kioxu.business.constant.SystemConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class JedisUtil implements InitializingBean {

	protected final static Logger logger = LoggerFactory.getLogger(JedisUtil.class);

	private static StringRedisTemplate template;
	private static RedisTemplate<String, Object> redisTemplate;
	/**
	 * APItoken统一有效期
	 */
	public static Integer sessionTimeOut = 172800;//默认时间2天
	/**
	 * 如果提示完被其他人登陆后的30分钟内调用接口会继续提示
	 */
	public static final Integer warm_time_out = 1800; // 30分钟

	public static final String LOGIN_KEY = "login_";
	public static final String LOGIN_STAT_KEY = "login_stat_";

	@Autowired
	public void setTemplate(StringRedisTemplate template) {
		JedisUtil.template = template;
	}

	@Autowired
	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		JedisUtil.redisTemplate = redisTemplate;
	}

	/**
	 * redis 字符串数据存入
	 * 
	 * @param key
	 *            存入key值
	 * @param val
	 *            存入 value 值
	 * @param timeOut
	 *            超时时间 单位 秒
	 */
	public static void setKeyVal(String key, String val, int timeOut) {
		try {
			template.opsForValue().set(key, val, timeOut, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("set keyVal 设值错误:" + key + "++" + val + "====" + e.getMessage(), e);
		}
	}

	/**
	 * 获取 redis 存入的字符串值（每次获取后重新设置过期时间）
	 * 
	 * @param key
	 * @return
	 */
	public static String getKeyVal(String key) {
		String val = getKeyValNoReset(key);
		if(val != null)
			template.expire(key, sessionTimeOut, TimeUnit.SECONDS);

		return val;
	}
	
	/**
	 * 获取 redis 存入的字符串值
	 * 
	 * @param key
	 * @return
	 */
	public static String getKeyValNoReset(String key) {
		try {
			return template.opsForValue().get(key);
		} catch (Exception e) {
			logger.error("getKeyVal 取值错误:" + key + "====" + e.getMessage(), e);
		}
		return null;
	}

	/**
	 *
	 * @param key
	 *            存入的key
	 * @param val
	 *            存入的 object 对象
	 * @param timeOut
	 *            key 生存周期。单位为秒
	 */
	public static void setKeyObject(String key, Object val, Integer timeOut) {
		// 从池中获取一个Jedis对象
		int time = sessionTimeOut;
		try {
			if (timeOut != 0) {
				time = timeOut;
			}
			redisTemplate.opsForValue().set(key, val, time, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("setKeyObject 错误:" + key + "====" + e.getMessage(), e);
		}
	}
	
	/**
	 * 获取key Object值（每次获取后重新设置过期时间）
	 * 
	 * @param key
	 * @return
	 */
	public static Object getKeyObject(String key) {
		Object o = getKeyObjectNoReset(key);
		if(o != null) {
			redisTemplate.expire(key, sessionTimeOut, TimeUnit.SECONDS);
		}

		return o;
	}
	
	/**
	 * 获取key Object值
	 * 
	 * @param key
	 * @return
	 */
	public static Object getKeyObjectNoReset(String key) {
		try {
			Object o = redisTemplate.opsForValue().get(key);
			return o;
		} catch (Exception e) {
			logger.error("getKeyObject 错误:" + key + "====" + e.getMessage(), e);
		}
		return null;
	}
	
	public static void setKeyObjectJSON(String key, Object val, Integer timeOut) {
		int time = sessionTimeOut;
		try {
			if (timeOut != 0) {
				time = timeOut;
			}
			String v = template.opsForValue().get(key);
			if(v != null)
				template.expire(key, time, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("setKeyObject 错误:" + key + "====" + e.getMessage(), e);
		}
	}
	
	public static JSONObject getKeyObjectJSON(String key) {
		try {
			String val = template.opsForValue().get(key);
			if (val != null) {
				template.expire(key, sessionTimeOut, TimeUnit.SECONDS);
				return JSONObject.parseObject(val);
			}
		} catch (Exception e) {
			logger.error("getKeyObject 错误:" + key + "====" + e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 刷新用户登陆状态，重复登陆把之前登陆的账号信息删除。存入新的登陆账号信息 刷新登陆的信息 刷新 过期的信息 登陆过期为6， 登陆被挤掉为5
	 * 
	 * @param mobile
	 */
	public static void refreshLoginStatus(String mobile, String token) {
		// 从池中获取一个Jedis对象
		try {
			/**
			 * 1、先查询出是否已登陆的 token
			 * 2、如果存在则删除之前的 token
			 * 3、把已登陆的账号存入 重新登陆的 缓存集中 并删除存在的 token
			 * 
			 */
			template.opsForValue().set(LOGIN_STAT_KEY + token, SystemConstants.ERROR_CODE_OK + "", sessionTimeOut, TimeUnit.SECONDS);
			logger.info("=================设置登陆有效时间：" + sessionTimeOut);

			String oldToken = template.opsForValue().get(LOGIN_KEY + mobile);
			if(oldToken != null){
				// 删除老token
				template.delete(oldToken);

				// 删除老key
				template.opsForValue().set(LOGIN_STAT_KEY + oldToken, SystemConstants.ERROR_CODE_KICKED_OUT+"", sessionTimeOut, TimeUnit.SECONDS);
			}

			// 手机号对应token关系
			template.opsForValue().set(LOGIN_KEY + mobile, token, sessionTimeOut, TimeUnit.SECONDS);
			// token对应手机号关系
			template.opsForValue().set(LOGIN_KEY + token, mobile, sessionTimeOut, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("刷新用户登陆状态 错误:" + mobile + "====" + e.getMessage(), e);
		}
	}

	/**
	 * 获得 该 token 的状态。是因为在另一台设备重新登陆了还是token 过期 
	 * 6 为过期 5 为重新登陆
	 * 
	 * @param key
	 * @return
	 */
	public static int getTokenStatus(String key) {
		int ret = 6;
		try {
			String val = template.opsForValue().get(LOGIN_STAT_KEY + key);
			if(val != null){
				ret = Integer.parseInt(val);
				template.expire(LOGIN_STAT_KEY + key, warm_time_out, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			logger.error("刷新用户登陆状态 错误:" + key + "====" + e.getMessage(), e);
		}
		return ret;
	}

	public static void setTimeOut(String key) {
		try {
			// 设置过期时间
			template.expire(key, sessionTimeOut, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("getKeyObject 错误:" + key + "====" + e.getMessage(), e);
		}
	}

	/**
	 * 设置超时时间
	 * 
	 * @param key
	 */
	public static void setTimeOut(String key, int timeOut) {
		try {
			template.expire(key, timeOut, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("getKeyObject 错误:" + key + "====" + e.getMessage(), e);
		}
	}

	/**
	 * 删除已存在的 token
	 * 
	 * @param key
	 */
	public static void delTokenKey(String key) {
		// 从池中获取一个Jedis对象
		try {
			template.delete(key);
		} catch (Exception e) {
			logger.error("delKey 错误:" + key + "====" + e);
		}
	}

	/**
	 *  判断对象是否存在
	**/
	public static boolean isExists(String key) {
		try {
			boolean count = template.hasKey(key);
			if (count) {
				template.expire(key, sessionTimeOut, TimeUnit.SECONDS);
			}
			return count;
		} catch (Exception e) {
			logger.error("isExists 错误:" + key + "====" + e.getMessage(), e);
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * API登陆使用 - 判断 token 是否存在(并且刷新token时间)
	 * 
	 * @param key
	 * @return
	 */
	public static boolean isTokenExists(String key) {
		try {
			boolean count = template.hasKey(key);
			if (count) {
				template.expire(key, sessionTimeOut, TimeUnit.SECONDS);// 设置用户信息
			}
			// 登陆状态
			String mobile = template.opsForValue().get(LOGIN_KEY + key);
			if(mobile != null) {
				template.expire(LOGIN_KEY + key, sessionTimeOut, TimeUnit.SECONDS);// 刷新token对应手机号
				template.expire(LOGIN_KEY + mobile, sessionTimeOut, TimeUnit.SECONDS);// 刷新手机号对应token
			} else {
				// 如果token对应手机号不存在则也删除token
				template.delete(key);
			}
			return count;
		} catch (Exception e) {
			logger.error("isExists 错误:" + key + "====" + e.getMessage(), e);
		}
		return false;
	}

	/**
	 * 查询出缓存对象还有多少时间销毁
	 * @param key
	 * @return
	 */
	public static Long getKeyPttl(String key) {
		try {
			logger.info("getKeyPttl 调用:" + key + "====");
			if (template.hasKey(key)) {
				return template.getExpire(key);
			}
			return null;
		} catch (Exception e) {
			logger.error("isExists 错误:" + key + "====" + e.getMessage(), e);
		}
		return null;
	}

	/***
	 * 手动删除key
	 * @param key
	 * @return
	 */
	public static void delKeyStr(String key) {
		try {
			logger.info("delKeyStr 调用:" + key + "====");
			template.delete(key);
		} catch (Exception e) {
			logger.info("delKeyStr 错误:" + key + "====" + e.getMessage(), e);
		}
	}
	
	/**
	 * 查询出所有的key
	 */
	@SuppressWarnings("rawtypes")
	public static Map<String, String> getKeyList() {
		Map<String, String> map = new HashMap<String, String>();
		try {
			Set keys = template.keys("*");// 列出所有的key，查找特定的key如：redis.keys("foo")
			Iterator t1 = keys.iterator();
			while (t1.hasNext()) {
				Object obj1 = t1.next();
				Object objVal = getKeyObjectNoReset((String) obj1);
				if (objVal == null) {
					objVal = getKeyVal((String) obj1);
					map.put((String) obj1, (String) objVal);
				} else {
					if (objVal instanceof java.util.List) {
						map.put((String) obj1, JSON.toJSONString(objVal));
					} else {
						map.put((String) obj1, JSON.toJSONString(objVal));
					}
				}
			}
			return map;
		} catch (Exception e) {
			logger.error("getKeyList 错误:====" + e.getMessage(), e);
		}
		return null;
	}

	
	public static void delSystemCache(String key){  
		try {
			Set<String> set = template.keys(key + "*");
			if(set == null)
				return;
	        Iterator<String> it = set.iterator();  
	        while(it.hasNext()){
	            String keyStr = it.next();  
				logger.info("清除已存在缓存，key：" + keyStr + "====");
				template.delete(keyStr);
	        }
		} catch (Exception e) {
			logger.error(" 启动时清除 cache 开头的缓存失败:"+ e.getMessage(), e);
		}
    }  

	/**
	 * notes 设置jedis的值
	 * @param key
	 * @param field
	 * @param val
	 */
	public static void hset(String key, String field, String val) {
		hset(key, field, val, sessionTimeOut);
	}

	/**
	 * notes 设置jedis的值
	 * @param key
	 * @param field
	 * @param val
	 */
	public static void hset(String key, String field, String val, int timeOut) {
		try {
			template.opsForHash().put(key, field, val);
			// 设置过期时间
			template.expire(key, timeOut, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("set keyVal 设值错误:" + key + "++field设值错误:" + field + "++"+ val + "====" + e.getMessage(), e);
		}
	}

	/**
	 * notes 获取jedis的值
	 * @param key
	 * @param field
	 */
	public static String hget(String key, String field) {
		try {
			return ObjTools.obj2String(template.opsForHash().get(key,field));
		} catch (Exception e) {
			logger.error("获取redis值错误！key:"+key + "fileld:"+ field);
		}
		return null;
	}

	/**
	 * notes 删除
	 * @param key
	 * @param field
	 */
	public static void hdel(String key, String... field) {
		// 从池中获取一个Jedis对象
		try {
			template.boundHashOps(key).delete((Object[]) field);
		} catch (Exception e) {
			logger.error("删除redis值错误！key:"+key + "fileld:"+ field);
		}
	}

	/**
	 * 自增数
	 *
	 * @param key key
	 * @param timeOut 超时时间
	 * @return
	 */
	public static long incr(String key, int timeOut) {
		try {
			long i = template.boundValueOps(key).increment();
			if(timeOut > 0) {
				// 设置过期时间
				template.expire(key, timeOut, TimeUnit.SECONDS);
			}
			return i;
		} catch (Exception e) {
			logger.error("set incr 设值错误:" + key + "====" + e.getMessage(), e);
		}
		return 0l;
	}

	/**
	 * 自增数
	 *
	 * @param key key
	 * @param num 自增维度
	 * @param timeOut 超时时间
	 * @return
	 */
	public static long incrBy(String key,int num, int timeOut) {
		try {
			long i = template.boundValueOps(key).increment(num);
			if(timeOut > 0) {
				// 设置过期时间
				template.expire(key, timeOut, TimeUnit.SECONDS);
			}
			return i;
		} catch (Exception e) {
			logger.error("set incr 设值错误:" + key + "====" + e.getMessage(), e);
		}
		return 0l;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		//清除短信模板
		JedisUtil.delSystemCache("cache");
	}
}
