package com.kioxu.business.demo;

import cn.hutool.core.util.IdcardUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.kioxu.business.inf.ConvertMapper;
import com.kioxu.business.vo.User;
import com.kioxu.business.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DemoServiceImpl implements DemoService {
    @Autowired
    RedisTemplate redisTemplate;
    @Resource
    private ThreadPoolExecutor threadPoolExecutor;

    @Override
    public Map<String, Object> demo(Map param) {
        String ID_18 = "830000197503020051";
        String ID_15 = "150102880730303";

        //是否有效
        boolean valid = IdcardUtil.isValidCard(ID_18);
        boolean valid15 = IdcardUtil.isValidCard(ID_15);
        log.info("ID_18: ", valid);
        log.info("ID_15: ", valid15);

        //省份
        String province = IdcardUtil.getProvinceByIdCard(ID_18);
        log.info("province: ", province);
        Map<String, Object> result = new HashMap<>();
        result.put("province", province);
        return result;
    }

    @Override
    public Map<String, Object> exampleOne(Map param) {
        String type = StringUtils.defaultString((String) param.get("type"), "default");
        switch (type) {
            case "demo1":
                demo1(param);
                break;
            case "demo2":
                demo2(param);
                break;
            case "demo3":
                demo3(param);
                break;
            default:
                break;
        }
        return null;
    }

    public void demo1(Map param) {
        Object obj = redisTemplate.opsForValue().get("DISTRICT_MAP");
        if (obj == null) {
            redisTemplate.opsForValue().set("DISTRICT_MAP", param, 60, TimeUnit.SECONDS);
        }
        System.out.println(JSON.toJSONString(obj));
    }

    public void demo2(Map param) {
        Date date = new Date(1684512000000L);
        System.out.println(date);
    }

    public void demo3(Map param) {
        Map de = null;
        System.out.println(Objects.isNull(de));
        System.out.println(Objects.nonNull(de));
    }

//    public static void main(String[] args) {
//        System.out.println(StringUtils.containsAny("智慧车9H", "智慧车9"));
//    }

    public static void main5(String[] args) {
        log.info("开始执行");
        ListeningExecutorService service = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));

        ListenableFuture<Integer> future = service.submit(() -> {
            // 模拟长时间运算
            Thread.sleep(10000);
            return 1;
        });

        Futures.addCallback(future, new FutureCallback<Integer>() {
            @Override
            public void onSuccess(Integer result) {
                log.info("运算结果: " + result);
            }

            @Override
            public void onFailure(Throwable t) {
                log.info("出错, 原因: " + t.getMessage());
            }
        }, service);
    }

    public static void main4(String[] args) {
        // 创建一个 StopWatch 对象
        Stopwatch stopwatch = Stopwatch.createStarted();
        System.out.println(Thread.currentThread().getName() + "------开始");
        CompletableFuture<Void> one = CompletableFuture.runAsync(() -> {
            performTask1();
        });
        CompletableFuture<Void> two = CompletableFuture.runAsync(() -> {
            performTask2();
        });
        CompletableFuture.allOf(one, two).join();
        stopwatch.stop();
        System.out.println("经过的时间（秒）: " + stopwatch.elapsed(TimeUnit.SECONDS));
        System.out.println(Thread.currentThread().getName() + "------结束");
    }

    public static void main3(String[] args) {
        String str1 = "abc";
        String str2 = "def";
        String str3 = "ghi";

        boolean result1 = StringUtils.equalsAny(str1, "abc", "def");  // result1为true
        boolean result2 = StringUtils.equalsAny(str2, "abc", "def");  // result2为false
        boolean result3 = StringUtils.equalsAny(str3, "abc", "def");  // result3为false
        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);
    }

    public static void main2(String[] args) {
        //System.out.println(StringUtils.equalsAny("D", "B", "C", "D"));
        //Boolean[] boolArray = {true, true, true};
        //boolean andResult = BooleanUtils.and(boolArray); // false (true && false && true)
        //System.out.println(andResult);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Task1");
        User user = new User();
        user.setName("李四");
        user.setAge(20);
        ConvertMapper mapper = Mappers.getMapper(ConvertMapper.class);
        UserVo userVo = mapper.userToUserVo(user);
        userVo.setName("新的");
        System.out.println(JSON.toJSONString(user));
        System.out.println(JSON.toJSONString(userVo));
        stopWatch.stop();

        stopWatch.start("Task2");
        UserVo userVo2 = new UserVo();
        BeanUtils.copyProperties(user, userVo2);
        userVo2.setName("新的2");
        System.out.println(JSON.toJSONString(user));
        System.out.println(JSON.toJSONString(userVo2));
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }

    public static void deal(User user) {
        user.setName("lisi");
    }

    public static void main1(String[] args) {
        // 创建一个 StopWatch 对象
        StopWatch stopWatch = new StopWatch();

        // 开始计时
        stopWatch.start("Task1");
        // 执行你的代码块1
        performTask1();
        stopWatch.stop();

        // 开始计时
        stopWatch.start("Task2");
        // 执行你的代码块2
        performTask2();
        stopWatch.stop();

        // 输出计时结果
        System.out.println(stopWatch.prettyPrint());
    }

    private static void performTask1() {
        // 模拟一个耗时的操作
        try {
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName() + "------performTask1");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void performTask2() {
        // 模拟另一个耗时的操作
        try {
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + "------performTask2");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{0,0,1,1,1,2,2,3,3,4};
        int[] nums2 = new int[]{2, 5, 6};
//        merge(nums1, 3, nums2, 3);
//        System.out.println(removeElement(nums1, 2));
        System.out.println(removeDuplicates(nums1));
        System.out.println(Arrays.toString(nums1));
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int p1 = m - 1;
        int p2 = n - 1;
        int p = m + n - 1;

        // 从后向前遍历nums1和nums2
        while (p1 >= 0 && p2 >= 0) {
            // 比较nums1[p1]和nums2[p2]，将较大的值放到nums1[p]中
            if (nums1[p1] > nums2[p2]) {
                nums1[p] = nums1[p1];
                p1--;
            } else {
                nums1[p] = nums2[p2];
                p2--;
            }
            p--;
        }

        // 如果nums2中还有元素未被合并，直接复制到nums1的前面
        while (p2 >= 0) {
            nums1[p] = nums2[p2];
            p2--;
            p--;
        }
    }

    public static int removeElement(int[] nums, int val) {
        int slowIndex = 0;
        for (int fastIndex = 0; fastIndex < nums.length; fastIndex++) {
            if (nums[fastIndex] != val) {
                nums[slowIndex] = nums[fastIndex];
                slowIndex++;
            }
        }
        return slowIndex;
    }

    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) return 0;
        int k = 1; // nums[0]自身就是唯一的，所以从1开始
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }
}
