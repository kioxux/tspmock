package com.kioxu.business.constant;

import java.util.Random;

public abstract class Constants {

    //公司编码
    public static final int COMPANY_CODE = 1001;

    public static final String SUCCESS = "success";
    public static final String SYSTEMEXCEPTION = "系统异常";
    public static final String FAIL = "fail";
    public static final String NOFILETYPE = "nofiletype";

    public static final String MSG = "msg";
    public static final String STATUS_TRUE = "1";
    public static final String STATUS_FALSE = "0";
    public static final String RS_TRUE = "true";
    public static final String RS_FALSE = "false";
    public static final String UTF_8 = "UTF-8";
    public static final String GBK = "GBK";
    public static final String EXCEL_CSV = ".csv";
    public static final String EXCEL_XLS = ".xls";
    public static final String EXCEL_XLSX = ".xlsx";
    public static final String ISO_8859_1 = "ISO_8859_1";
    final public static String RANDOMVALUE_KEY="validaterandomkey";
    final public static Random RANDOM = new Random();

    public static final String DF_YMDHMS = "yyyy-MM-dd HH:mm:ss";

    //提交方式(系统日志表)
    public static final String LOGIN_FORM_WEB = "WEB";

    // -- 状态：使用中|已禁用
    public static final String CHOOSEOPTION_CATEGORY_CODE_STATUS = "status";

    //登陆状态
    public static final String LOGIN_SUCCESS ="SUCCESS";//登录成功
    public static final String LOGIN_FAILURE_BAD_CREDENTIAL="FAILURE_BAD_CREDENTIAL";//用户名密码错误
    public static final String LOGIN_FAILURE_CREDENTIAL_EXPIRED="FAILURE_CREDENTIAL_EXPIRED";//密码过期
    public static final String LOGIN_FAILURE_USER_DISABLED="FAILURE_USER_DISABLED";//用户被禁用
    public static final String LOGIN_FAILURE_USER_EXPIRED="FAILURE_USER_EXPIRED";//用户被禁用
    public static final String LOGIN_FAILURE_USER_LOCKED="FAILURE_USER_LOCKED";//用户名过期
    public static final String LOGIN_FAILURE_OTHERS="FAILURE_OTHERS";//用户被锁定
    public static final String LOGOUT_SUCCESS="LOOUT_SUCCESS";//退出系统
    public static final String SESSION_TIMEOUT="SESSION_TIMEOUT";//页面过期
    public static final String IS_VALID="100111";//有效记录
    public static final String IS_NOT_VALID="100112";//无效记录
    public static final String CLIENT_TYPE="1";//无效记录


    public static final String PHONE_DIAL_DATA="1054";//电话拨打情况
    public static final String PHONE_DIAL_GENERAL="1052";//电话拨打情况选项


    /*应收账款、其他应收款明细标识*/
    public static final int RECEIVABLE_TYPE=1;
    public static final int RECEIVABLE_TYPEA=2;
    /*主要供应商商和销售*/
    public static final int PARTNER_TYPE_SUPPLIER=1;
    public static final int PARTNER_TYPE_SALER=2;
    // 产品选择范围
    public static final String PRODUCT_SELECT_RANGE_KEY = "productSelectionRange";

    public static int REPAY_REMAIN_DIFF_DAY = 3;   // 逐月还  提前3天通知
    public static int DAYREPAY_BELL_HOUROFDAY = 8;   // 逐日还    每天 8:30 时 通知
    public static int DAYREPAY_BELL_MINUTE = 30;

    public static final int CREDIT_ASSESSMENT_PER=1;
    public static final int CREDIT_ASSESSMENT_COM=2;

    public static final String DEBT_COMPANY_CODE="1001";//小额贷款公司编号
    public static final String TASK_URL_COLLECTION="M-BUSINESS-MANAGE-AFTER-COLLECTION-ADD";//催收代表任务url
    public static final String ROLE_CODE_COLLECTOR="0111";//角色Id--催收人员
    public static final String TASK_URL_COLLECT_ID="20";//taskurl 里的id。
    public static final int ROLE_ID_COLLECTOR=21;

    public static final String TASK_URL_EXTEND_SIGN_ID="25";//展期签约
    public static final String TASK_URL_EXTEND_APPLY_ID="25";//展期申请
    public static final int ROLE_ID_SIGNNER=17;//角色Id--签约人员
    public static final String ROLE_CODE_RESEARCHER="0104";//角色Id--调查员

    public static final String PLEASE_TO_CHOOSE="请选择";

    public static final int CREDIT_COMP_CARD_TYPE=110211;//公司组织机构代码

    //征信结果状态
    public static final String CREDIT_RESULT_STATUS_NULL="无征信记录";
    public static final String CREDIT_RESULT_STATUS_EFFECTIVE="征信在有效期内";
    public static final String CREDIT_RESULT_STATUS_INVALID="征信已失效";

    public static final int UserRole_STATUS=1;//userRole里的状态标识

    //系统配置模块
    public static final String  SYSTEM_CONFIG_KEY_EXIST="exist";//同一公司下配置表中的Key已经存在


    //进件来源
    public static final String INTO_STATUS_MH="门户";
    public static final String INTO_STATUS_WMH="微门户";
    public static final String INTO_STATUS_DHYX="电话营销";
    public static final String INTO_STATUS_QTQD="其他渠道";

    //证件类型
    public static final String CARD_TYPE_SFZ="身份证";
    public static final String CARD_TYPE_HZ="护照";
    public static final String CARD_TYPE_ORG_CODE="营业执照";

    //产品名称
    public static final String NO_STANDARD_PRODUCT="非标准化产品";

    //加收费用项
    public static final String FEE_JSLI="加收利息";
    public static final String FEE_JSFWF="加收服务费";

    //公司默认每月还款日
    public static final int LOAN_REPAY_DATE=10;


    //征信期限（天）
    public static final String CREDIT_INVES_PERIOD="period";
    //罚息类型
    public static final String FINE_TYPE="interestFineType";
    //罚息利率
    public static final String FINE_INTEREST="interestFine";
    //挪用罚息利率
    public static final String FINE_EM_INTEREST="interesTembezzle";
    //每天生成罚息的时间
    public static final String FINE_TIME="interestFineComputeTime";

    //套账信息
    public static final String COMP_ACCOUNT_INFO_CODE="A";
    public static final String COMP_ACCOUNT_INFO_NAME="表内";

    //财务指标
    public static final String INDEX_QM="qm";
    public static final String INDEX_QC="qc";

    //打印路径
    public static final String PRINT_LEND="M-PRINT-LEND-VOUCHER";//放款凭据
    public static final String PRINT_DUNBASIC="M-PRINT-DUN";//逾期催收
    public static final String PRINT_REPAYPLAN="M-PRINT-REPAYPLAN";//还款计划

    //支付渠道编号
    public static final String CHANNEL_CODE_YL="yinlian";//银联
    public static final String CHANNEL_CODE_TL="tonglian";//通联
    public static final String CHANNEL_CODE_WXZ="-1";//未选择支付渠道


    public static final String COMPANY_RESOURCE = "/config/company_data_init.properties";


    //客户类型
    public static final int CLIENT_TYPE_PERSON = 100311;	//个人客户

    //客户证件类型
    public static final int CERTIFICATE_TYPE = 100711;	//身份证

    //客户财力证明类别
    public static final int CLIENT_ANCIALPROOF_FILETYPE = 1199;
    public static final int CLIENT_ANCIALPROOF_SOURCETYPE = 119901;

    //客户房产账单
    public static final int CLIENT_ESTATE_FILETYPE = 1198;
    public static final int CLIENT_WATER_SOURCETYPE = 119801;
    public static final int CLIENT_TAX_SOURCETYPE = 119802;
    public static final int CLIENT_BANK_SOURCETYPE = 119803;
    public static final int CLIENT_CREDIT_SOURCETYPE = 119804;
    public static final int CLIENT_SINGLE_SOURCETYPE = 119805;
    public static final int CLIENT_ESTATE_SOURCETYPE = 119806;
    public static final int CLIENT_MOBILE_SOURCETYPE = 119807;
    public static final int CLIENT_POSTMARK_SOURCETYPE = 119808;

    //客户关系
    public static final int CLIENT_RELATIONS = 1020;
    public static final int CLIENT_RELATIONS_PERSONAL = 102011;

    //身份证类型
    public static final int CLIENT_CARD_FILETYPE = 1200;
    public static final int CLIENT_CARD_SOURCETYPE_A  = 120001;
    public static final int CLIENT_CARD_SOURCETYPE_B  = 120002;
    public static final int CLIENT_CARD_SOURCETYPE_C  = 120003;
    //央行征信图片
    public static final int CLIENT_CARD_SOURCETYPE_D  = 120004;
    //工作证明图片
    public static final int CLIENT_CARD_SOURCETYPE_F  = 120005;
    //授信默认额度
    public static final String CLIENT_CONFIG_KEY = "creditLines";
    //在线客服key
    public static final String ONLINE_SERVICE_KEY = "onlineServiceUrl";
    //罚息参数
    public static final String INTEREST_FINE = "interestFine";
    //授信默认天数
    public static final String CREDIT_TIME = "creditTime";
    //系统初筛失败默认天数
    public static final String SYSTEM_PRIMARY = "systemPrimary";
    //芝麻请求过期天数
    public static final String ZM_DATE = "zm_date";
    //芝麻请求过期天数
    public static final String PBCCRC_DATE = "pbccrc_date";
    //手机运营商爬取过期天数
    public static final String JIENA_DATE = "jiena_date";
    //公积金过期天数
    public static final String GJJ_TIMEOUT_DAY = "gjj_timeout_day";
    //公积金过期天数
    public static final String JD_TIMEOUT_DAY = "jd_timeout_day";

    //认证状态 0 为认证成功，1 未认证，2 认证失败 3认证中
    public static final int AUTH_CODE_SUCCESS = 0;
    public static final int AUTH_CODE_NO = 1;
    public static final int AUTH_CODE_OVERDUR = 2;
    public static final int AUTH_CODE_IN = 3;
    public static final int AUTH_CODE_FAILURE = 4;

    /**  还款方式
     *  109102	REPAYMENT_MODE_ZYXXHB	逐月先息后本
     *  109108	REPAYMENT_MODE_ZYDBDX	追月等本等息
     */
    public static final int REPAYMENT_MODE_ZYXXHB=109101;	//逐月先息后本
    public static final int REPAYMENT_MODE_ZYDBDX=109102;//追月等本等息
    public static final int REPAYMENT_MODE_YCXHBFX=109103;//一次性还本付息
    /**
     * 等额本息
     */
    public static final int REPAYMENT_MODE_DEBX=109104;
    /** 利率单位
     * 105811	INTERESTRATE_UNITS_YEAR		年
     * 105812	INTERESTRATE_UNITS_MONTH	月
     */
    public static final int INTERESTRATE_UNITS_YEAR=105811;		//年
    public static final int INTERESTRATE_UNITS_MONTH=105812;	//月
    public static final int INTERESTRATE_UNITS_WEEK=105813;	//月
    public static final int INTERESTRATE_UNITS_DAY=105814;	//t天

    /** 手续费费用模式
     * 109201	COST_MODEL_COLUMN_CARGE	比例收取
     * 109202	COST_MODEL_FIXED_CHARGE		固定费用
     * 109203	COST_MODEL_DONT_CHARGE		不收到
     */
    public static final int COST_MODEL_COLUMN_CHARGE = 109201;		//比例收取
    public static final int COST_MODEL_FIXED_CHARGE = 109202;	//固定费用
    public static final int COST_MODEL_DONT_CHARGE = 109203;	//不收到

    /**	手续费收费周期
     * 109301	COST_CYCLE		按期收费
     * 109302	COST_DISPOSABLE	一次性收费
     */
    public static final int COST_CYCLE = 109301;		//按期收费
    public static final int COST_DISPOSABLE = 109302;	//一次性收费



    /**提现申请表申请状态
     * 118801	LOAN_APPLICATION 	待放款
     */
    public static final int LOAN_APPLICATION=118801;	//待放款

    /** 用户授信状态
     * 120201	CLIENT_CREDIT_STAT_USING	正在授信
     * 120202	CLIENT_CREDIT_STAT_FURTHER_INFORMATION	补充资料
     * 120203	CLIENT_CREDIT_STAT_UNUSED	已授信未使用
     * 120204	CLIENT_CREDIT_STAT_HASBEENUSED	已授信已使用
     * 120205	CLIENT_CREDIT_STAT_OVERDUEUSER	授信过期用户
     * 120206	CLIENT_CREDIT_STAT_WITHDRAWAL	提现中
     * 120207	CLIENT_CREDIT_STAT_FAILURE		授信失败
     * 120208	CLIENT_CREDIT_STAT_PRIMARY		授信失败
     * 120209	CLIENT_WITHDRAWAL_STAT_FAILURE	提现失败状态Withdrawal of failure
     */
    public static final int CLIENT_CREDIT_STAT_NULL 				= 0;	//未提交申请
    public static final int CLIENT_CREDIT_STAT_NO 				= 120200;	//授信未提交
    public static final int CLIENT_CREDIT_STAT_USING 				= 120201;	//正在授信
    public static final int CLIENT_CREDIT_STAT_FURTHER_INFORMATION	= 120202;	//补充资料
    public static final int CLIENT_CREDIT_STAT_UNUSED 				= 120203;	//已授信未使用
    public static final int CLIENT_CREDIT_STAT_HASBEENUSED 		= 120204;	//已授信已使用
    public static final int CLIENT_CREDIT_STAT_OVERDUEUSER			= 120205;	//授信过期用户
    public static final int CLIENT_CREDIT_STAT_WITHDRAWAL  			= 120206;	//提现中
    public static final int CLIENT_CREDIT_STAT_FAILURE 				= 120207;	//授信失败
    public static final int CLIENT_CREDIT_STAT_PRIMARY 				= 120208;	//授信系统初筛失败
    public static final int CLIENT_WITHDRAWAL_STAT_FAILURE 		= 120209;	//提现失败状态
    public static final int CLIENT_CREDIT_STAT_CANCEL 		= 120210;	//取消借款

    /** 项目编码类别
     * 103007	CODE_INFO_CREDIT	客户授信申请编码
     * 103008	CODE_INFO_LOAN		放贷申请编码
     * 103004	CODE_INFO_CLIENT	客户编码类型
     */
    public static final int CODE_INFO_CREDIT  = 103007;	//客户授信申请编码
    public static final int CODE_INFO_LOAN  = 103008;	//放贷申请编码
    public static final int CODE_INFO_CLIENT = 103004;	//客户编码类型


    /**
     * 提现审核状态
     * 125001	提现审核状态	待审核
     125002	提现审核状态	审批通过
     125003	提现审核状态	审核拒绝
     */
    public static final int WITHDRAWAL_STAT_AUDIT  = 125001;	//待审核
    public static final int WITHDRAWAL_STAT_SUCCESS  = 125002;	//审批通过
    public static final int WITHDRAWAL_STAT_FAILURE = 125003;	//审核拒绝
    public static final int WITHDRAWAL_STAT_CANCLE = 125004;	//审核取消
    public static final int WITHDRAWAL_STAT_ROLLBACK=125005;    //待审核-通过被撤回


    /** 授信审核流程状态
     *
     */
    public static final int CREDIT_AUDIT_REJECT  = 118701;	//系统拒绝
    public static final int CREDIT_AUDIT_STAY  = 118702;	//待初审
    public static final int CREDIT_AUDIT_ING  = 118703;	//初审中
    public static final int CREDIT_AUDIT_PATCH  = 118704;	//初审补件中
    public static final int CREDIT_AUDIT_TRIAL_REJECT  = 118705;	//初审直接拒绝
    public static final int CREDIT_AUDIT_JUDGMENT  = 118706;	//待终审
    public static final int CREDIT_AUDIT_JUDGMENT_ING  = 118707;	//终审中
    public static final int CREDIT_AUDIT_BACK_TRIAL  = 118708;	//退回初审
    public static final int CREDIT_AUDIT_FINAL_PASS  = 118709;	//终审通过
    public static final int CREDIT_AUDIT_REFUSED_REJECT  = 118710;	//终审拒绝
    public static final int CREDIT_AUDIT_SYSTEM_REVIEW  = 118711;	//系统待审核

    /** 放款状态 */
    public static final int APPLY_FOR_LENDING  = 118801;	//待放款
    public static final int APPLY_LENDINGG  = 118802;	//放款中
    public static final int APPLY_LOAN_SUCCESS  = 118803;	//放款成功
    public static final int APPLY_LOAN_FAILURE  = 118804;	//放款失败
    public static final int APPLY_LOAN_IN_PAYMENT  = 118805;	//还款中
    public static final int APPLY_LOAN_OVERDUE  = 118806;	//已逾期
    public static final int APPLY_LOAN_CLOSED_ACCOUNT  = 118807;	//已结清
    public static final int APPLY_LOAN_CANCEL  = 118809;	//取消

    //实名认证类型 进行什么认证
    public static final String CLIENT_AUTH_TYPE_REALNAME  = "120101";	//实名认证
    public static final String CLIENT_AUTH_TYPE_ACCUMULATION  = "120102";	//客户公积金认证
    public static final String CLIENT_AUTH_TYPE_YH  = "120103";	//央行征信报告
    public static final String CLIENT_AUTH_TYPE_ZCX  = "120104";	//中诚信认证
    public static final String CLIENT_AUTH_TYPE_ZMXY  = "120105";	//芝麻信息认证
    public static final String CLIENT_AUTH_TYPE_YYS  = "120106";	//手机运营商认证
    public static final String CLIENT_AUTH_TYPE_JD  = "120107";	//京东认证
    public static final String CLIENT_AUTH_TYPE_TB  = "120109";	//淘宝认证
    public static final String CLIENT_AUTH_TYPE_ALIPAY  = "120110";	//支付宝认证
    public static final String CLIENT_AUTH_TYPE_LOANUSEAGE  = "120111";	//借款用途


    //实名认证的证件类型
    public static final int CLIENT_AUTH_TYPE_REALNAME_CARD  = 100711;	//身份证

    //客户认证状态
    public static final int CLIENT_AUTH_CERTIFICATION_SUCCEED  = 0;	//认证成功
    public static final int CLIENT_AUTH_CERTIFICATION_UNAUTHORIZED  = 1;	//未认证
    public static final int CLIENT_AUTH_CERTIFICATION_NOTPASS  = 2;	//认证失败


    //客户上传文件审核状态
    public static final int CLIENT_FILE_STAT_PASS  = 0;	//审核通过
    public static final int CLIENT_FILE_STAT_NOT_PASS  = 1;	//审核未通过
    public static final int CLIENT_FILE_STAT_NOT_CHECK  = 2;	//未审核


    //客户上传文件是否有效
    public static final int CLIENT_FILE_VALID  = 0;	//有效
    public static final int CLIENT_FILE_VALID_NOT  = 1;	//完效，已软删除

    //APP 个人信息其他信息里的账号类别 手机运营商账号
    public static final int CLIENT_OTHER_ACCOUNT_MOBILE = 120301;	//手机运营商账号
    public static final int CLIENT_OTHER_ACCOUNT_HOUSING = 120302;	//住房公积金账号
    public static final int CLIENT_OTHER_ACCOUNT_JD = 120303;		//京东账号
    public static final int CLIENT_OTHER_ACCOUNT_TAOBAO = 120304;	//淘宝账号

    //客户资产类型
    public static final int CLIENT_ASSET_HOUSE = 109712; //房产

    //数据是否有效
    public static final int EFFECTIVE = 100111;	//是
    public static final int INVALID = 100112;	//否

    //客户的放款申请状态
    public static final int CLIENT_LEND_STATUS_OVER = 118807;	//放款完结清

    /**
     * 产品费用详细R
     * 109034	PRODUCT_COST_POUNDAGE 手续费
     * 109035	PRODUCT_COST_MONTHLY_FEE	月管理费
     */
    public static final int PRODUCT_COST_POUNDAGE = 109034;
    public static final int PRODUCT_COST_MONTHLY_FEE = 109035;

    /**
     * 修改记录的字段
     */
    //客户基本信息
    public static final String CLIENT_CARD_A = "client_card_a";	//身份证正面
    public static final String CLIENT_CARD_B = "client_card_b";	//身份证反面
    public static final String CLIENT_CARD_C = "client_card_c";	//身份证手持
    public static final String CLIENT_ADDRESS = "client_address";	//客户详细 地址
    public static final String CLIENT_APPLYCREDIT = "client_apply_credit";	//客户授信申请额度
    public static final String CLIENT_NAME = "client_name";	//客户授信申请额度
    public static final String CLIENT_CARDNO = "client_cardNo";	//客户授信申请额度

    //客户工作信息
    public static final String CLIENT_WORK_COMPANY = "client_work_company";	//单位名称
    public static final String CLIENT_WORK_INDUSTRY = "client_work_industry";	//从事行业
    public static final String CLIENT_WORK_JOB = "client_work_job";	//工作岗位
    public static final String CLIENT_WORK_TEL = "client_work_tel";	//单位电话
    public static final String CLIENT_WORK_MAIL = "client_work_mail";	//公司邮件
    public static final String CLIENT_WORK_MONTHLY_INCOME = "client_work_monthly_income";	//月收入
    public static final String CLIENT_WORK_ADDRESS = "client_work_address";	//工作详细 地址
    public static final String CLIENT_WORK_STATUS= "client_work_status";	//工作状态

    //联系人信息
    public static final String CLIENT_LINK_ONE = "client_link_1";	//联系人关系
    public static final String CLIENT_LINK_TWO = "client_link_2";	//联系人关系
    public static final String CLIENT_LINK_THREE = "client_link_3";	//联系人关系

    //房产账单信息
    public static final String CLIENT_HOUSE = "client_house";	//房产账单 水费图片
    public static final String CLIENT_FILE_BILL_WATER = "client_file_bill_water";	//房产账单 水费图片
    public static final String CLIENT_FILE_BILL_TAX = "client_file_bill_tax";	//房产账单 税单、地税发票
    public static final String CLIENT_FILE_BILL_BANK = "client_file_bill_bank";	//房产账单 月度银行账单
    public static final String CLIENT_FILE_BILL_CREDIT  = "client_file_bill_credit";	//房产账单 月度信用卡账单
    public static final String CLIENT_FILE_BILL_SINGLE  = "client_file_bill_single";	//房产账单 电费单/水费单/燃气单费单
    public static final String CLIENT_FILE_BILL_MOBILE  = "client_file_bill_mobile";	//房产账单 移动电话服务账单/网络服务账单/有线电视账单
    public static final String CLIENT_FILE_BILL_POSTMARK   = "client_file_bill_postmark";	//租赁合同/有邮戳的信函
    public static final String CLIENT_FILE_ESTATE = "client_file_estate";	//房产图片
    public static final String CLIENT_FILE_FINANCIAL = "client_file_financial";	//财力证明图片
    public static final String CLIENT_ACCOUNT_JD = "client_account_jd";
    public static final String CLIENT_ACCOUNT_TB = "client_account_tb";
    public static final String CLIENT_ACCOUNT_MOBILE = "client_account_mobile";
    public static final String CLIENT_OTHER_BANK = "client_other_bank";
    public static final String CLIENT_HOUSE_ADDRESS = "house_address";	//客户房产地址修改
    public static final String CLIENT_HOUSE_BILL_TYPE = "house_bill_type";	//客户房产账单类型修改

    /** -------------------------------------  */

    //更多 手机。淘宝。京东账号修改信息

    public static final String CLIENT_MODIFY_TYPE_USER = "120601";
    public static final String CLIENT_MODIFY_TYPE_WORK = "120602";
    public static final String CLIENT_MODIFY_TYPE_LINK = "120603";
    public static final String CLIENT_MODIFY_TYPE_BILL = "120604";
    public static final String CLIENT_MODIFY_TYPE_PROVE = "120605";
    public static final String CLIENT_MODIFY_TYPE_ACCOUNT = "120606";

    /**
     * sha512 签名加后缀参数
     */
    public static final String SHA512_SALT = "L7uveSrVTfAgCx0L5SOkvnC6sViB56Aa";

    /**
     * 一天有多少毫秒数
     */
    public static final Integer DAYMSEC = 86400000;

    /**
     * app 用户佣金提现
     */
    public final static String TRANS_TYPE = "124401";
    /**
     * app 用户佣金提现状态
     */
    public final static String TRANS_STATUS = "2";

    /**
     * app 客户来源
     * 对应的字段项为主动申请(CS001)、邀请码推荐(CS002).
     */
    public final static String CUSTOMER_SOURCE_SQ = "124801";
    public final static String CUSTOMER_SOURCE_TJ = "124802";

    /**
     *  129901	基本信息
     129902	工作信息
     129903	联系人信息
     129904	房产信息
     129905	更多信息
     129906	完成申请
     129907	其他
     129908	征信信息
     */
    public final static Integer CLIENT_BASIC_NODE = 129901;
    public final static Integer CLIENT_WORK_NODE = 129902;
    public final static Integer CLIENT_CONTACT_NODE = 129903;
    public final static Integer CLIENT_HOUSE_NODE = 129904;
    public final static Integer CLIENT_MORE_NODE = 129905;
    public final static Integer CLIENT_COMPLETE_NODE = 129906;
    public final static Integer CLIENT_CREDIT_NODE = 129908;

    /**
     * 工作状态 （自雇、受薪 ）
     * 124601	自雇人士
     * 124602	受薪人士
     */
    public final static Integer WORK_STATUS = 1901;
    public final static Integer WORK_STATUS_SOHO = 190101;
    public final static Integer WORK_STATUS_SALARIED = 190102;

    /**
     * 线下还款审核状态
     */
    public final static String OFFLINE_APPROVAL_STATUS   = "126001";

    /** 还款日志类型 **/
    public final static Integer OFFLINE_PAYMENT   = 126101;
    public final static Integer ONLINE_PAYMENT   = 126102;

    /**
     * 还款操作记录状态
     * 1 为还款成功， 0 为失败，2为还款中
     */
    public final static int OPER_STATE_SUCCESS = 1;
    public final static int OPER_STATE_FAIL = 0;
    public final static int OPER_STATE_IN = 2;


    /**
     * 首页产品状态
     * PENDING_AUDIT	待审核
     * UNDER_REVIEW	审核中
     * WITHDRAWALS	提现
     * REPAYMENT_IN 还款中
     * **/
    /**
     public final static String PENDING_AUDIT = "待审核";
     public final static String UNDER_REVIEW = "审核中";
     public final static String PENDING_WITHDRAWALS = "可提现";
     public final static String WITHDRAWALS_IN = "提现中";
     public final static String REJECT = "拒绝";
     public final static String REPAYMENT_IN  = "还款中";
     chenggong
     daitijiao
     daitixian
     jujue
     shenhetongguo
     shenhezhong
     shibai
     tixianzhong

     **/

    public final static String PENDING_AUDIT = "120200";
    public final static String UNDER_REVIEW = "审核中";
    public final static String PENDING_WITHDRAWALS = "可提现";
    public final static String WITHDRAWALS_IN = "chenggong";
    public final static String REJECT = "拒绝";
    public final static String REPAYMENT_IN  = "还款中";

    /**
     * 首页广告json缓存数据
     */
    public final static String ADVERTS_CACHE = "adverts";

    /**
     * 账单状态
     * */
    public final static int ORDER_STATE_S = 109403;

    public final static int ORDER_STATE_F = 109404;

    //申请进度
    /**
     * 未提交申请
     * */
    public final static int APPLY_PROGRESS_NO = 0;
    /**
     * 提交申请
     * */
    public final static String APPLY_PROGRESS_1 = "提交申请";
    /**
     * 申请审核
     * */
    public final static String APPLY_PROGRESS_2 = "申请审核";
    /**
     * 提现
     * */
    public final static String APPLY_PROGRESS_3 = "提现申请";
    /**
     * 申请成功
     * */
    public final static String APPLY_PROGRESS_4 = "借款成功";

    //客户完善资料选项
    public final static Integer CLIENT_PERFECT_OPTIONS = 1262;
    //运营商选项
    public final static Integer CLIENT_PERFECT_OPTIONS_OPERATOR = 126201;
    //其他影像选项
    public final static Integer CLIENT_PERFECT_OPTIONS_IMAGE = 126202;
    //房产选项
    public final static Integer CLIENT_PERFECT_OPTIONS_HOUSE = 126203;

    //客户上传央行报告图片
    public final static Integer CLIENT_CREDIT_REPORTING_TYPE = 1263;
    public final static Integer CLIENT_CREDIT_REPORTING = 126301;

    //客户上传影像资料图片
    public final static Integer CLIENT_IMAGE_DATA_TYPE = 1264;
    public final static Integer CLIENT_IMAGE_DATA = 126401;
    //营业执照
    public final static Integer CLIENT_IMAGE_DATA_LICENCE =126402;

    //客户上传工作证明图片
    public final static Integer CLIENT_IMAGE_WORK_TYPE = 1265;
    public final static Integer CLIENT_IMAGE_WORK = 126501;

    /**
     * 借款用途图片  大类
     */
    public final static Integer CLIENT_IMAGE_LOAN_USEAGE_TYPE=1266;
    /**
     * 借款用途图片 小类
     */
    public final static Integer CLIENT_IMAGE_LOAN_USEAGE=126601;

    /**
     * 字典：借款用途-个人
     */
    public final static Integer LOAN_USEAGE_PERSON=2141;
    /**
     * 字典：借款用途-企业
     */
    public final static Integer LOAN_USEAGE_COMPANY=2142;

    /**图片类型-字典表()**/
    public final static Integer IMAGES_TYPE = 1915;//图片大类代码
    public final static Integer IMAGES_TYPE_STATUS_Y = 1;//图片状态有效
    /**banner图**/
    public final static Integer IMAGES_TYPE_A = 191501;
    /**app广告图**/
    public final static Integer IMAGES_TYPE_B = 191502;
    /**活动图**/
    public final static Integer IMAGES_TYPE_C = 191503;
    /** 申请进度Banner图**/
    public final static Integer IMAGES_TYPE_D = 191504;
    /**产品类型**/
    /**线上**/
    public final static Integer PRODECT_ONLINE = 191801;
    /**线下**/
    public final static Integer PRODECT_OFFLINE = 191802;

    /**到账方式**/
    /**到手金额**/
    public final static String ARRIVAL_A = "192101";
    /**合同金额**/
    public final static String ARRIVAL_B = "192102";

    public static final int PUSH_CRM_FLAG_INIT=0;

    /**渠道code**/
    /**固金所**/
    public final static Integer CHANNEL_A = 210001;
    public final static Integer CHANNEL_B = 210002;
    /**渠道code**/
    /**交易成功*/
    public final static  String JX_SUCCESS="0000";
    /**交易失败*/
    public final static  String JX_FALSE="0001";
    /**交易处理中*/
    public final static  String JX_HANDLE="0002";
    /**操作处理中*/
    public final static  String CZ_SUCCESS="0004";

    /**业务员权限**/
    /**所有权限**/
    public final static String AUTH_TYPE_A = "120901";
    /**CRM入口**/
    public final static String AUTH_TYPE_B = "120902";
    /**特殊流程**/
    public final static String AUTH_TYPE_C = "120903";
    /**普通用户**/
    public final static String AUTH_TYPE_D = "120904";

    /**存管渠道提现状态**/
    /**提现中**/
    public final static String WITHDRAW_STATUS_IN = "212001";
    /**提现成功**/
    public final static String WITHDRAW_STATUS_S = "212002";
    /**提现失败**/
    public final static String WITHDRAW_STATUS_F = "212003";

    /**产品code**/
    /**房信贷**/
    public final static String FXD = "HOUSCREDIT";
    /**闪电花**/
    public final static String SDH = "PAYDAYLOAN";

    //授权项============

    //(192213:移动运营商,192212:芝麻信用,192211:征信报告, 192214工作证明:192215: 更多信息, 192216 : 房产信息,192217:公积金)
    /**移动运营商**/
    public final  static String AUTH_YD="192213";
    /**芝麻信用**/
    public final  static String AUTH_ZM="192212";
    /**征信报告**/
    public final  static String AUTH_ZX="192211";
    /**工作证明**/
    public final  static String AUTH_GZ="192214";
    /**更多信息**/
    public final  static String AUTH_GD="192215";
    /**房产信息**/
    public final  static String AUTH_FC="192216";
    /**公积金**/
    public final  static String AUTH_GJJ="192217";
    /**京东**/
    public final  static String AUTH_JD="192218";
    /**营业执照**/
    public final  static String AUTH_YYZZ="192219";
    /**淘宝**/
    public final  static String AUTH_TB="192220";
    /**支付宝**/
    public final  static String AUTH_ALIPAY="192221";

    /**
     * 借款用途
     */
    public final static String AUTH_LOANUSEAGE="192222";


    //帐单生成状态====================
    /**帐单未生成**/
    public final static Integer BILL_NOT_GENERATE=0;
    /**帐单已生成**/
    public final static Integer BILL_GENERATED=1;
    //数据魔盒返回码
    //**成功
    public final static String MO_HE_SUCCESS="00";
    //**待发短信
    public final static String MO_HE_WAIT_MSG="01";
    //**失败
    public final static String MO_HE_FAIL="02";
    public final static String MO_HE_IMAG="03";
    public final static String MO_HE_SMS_IMAG="04";

    //放款人对象========================
    public final static String LENDER_PERSON="212201";
    public final static String LENDER_MERCHANT="212202";

    /**
     * 房金所（平台）
     */
    public final static int ACCOUNT_TYPE_FJS = 118402;
    /**
     * 专业房贷人
     */
    public final static int ACCOUNT_TYPE_FDR = 118401;

    /**
     * 结果返回成功
     */
    public static final String RESULT_SUCCESS = "00";

    /**
     * 需填写短信验证码
     */
    public static final String RESULT_SMSCODE = "01";

    /**
     * 调用失败
     */
    public static final String RESULT_FAIL = "02";

    /**
     * 需填写图片验证码
     */
    public static final String RESULT_INMAGECODE = "03";

    public static final String PLAT_PLAN_ACCOUNT_OPER_FLAG_YHTH="191904";
    //个人账户收入类型(191501:收入,191502:支出)
    public static final String CLIENT_FUND_FLOW_OT_INCOME="191501";
    public static final String CLIENT_FUND_FLOW_OT_PAY="191502";

    public static final String  AUDIT_REFUSED_REASON_TYPE_CODE_USER = "1141";

    /**精选产品**/
    public final static Integer PRODECT_ESSENCE = 1;

    /**获取下载url**/
    public final static String DOWNlOAD_APP_URL_KEY = "downloadAppKey";

    /**应用标识**/
    /**房速贷主应用 **/
    public final static String FSD_MAPP = "FSD_MAPP";
    /**51秒借款**/
    public final static String FSD_51APP = "FSD_51APP";
    /**车主贷H5**/
    public final static String FSD_CAROENER = "FSD_CAROENER";
    /**房速贷微信服务号**/
    public final static String FSD_WECHAT = "FSD_WECHAT";
    /**51闪电花**/
    public final static String FSD_LIGHTNINGAPP = "FSD_LIGHTNINGAPP";
    /**用户类型**/
    /**个人用户**/
    public final static String  USER_TYPE_PENSON = "213210";
    /**企业用户 **/
    public final static String USER_TYPE_ENTERPRISE = "213220";
    /**银行卡绑定状态--成功**/
    public final static String BANK_BINDING_S = "119101";
    /**银行卡绑定状态--进行中**/
    public final static String BANK_BINDING_R = "119100";
    /**银行卡绑定状态--失败**/
    public final static String BANK_BINDING_F = "119102";

    /**
     * 案件审核大类 - 审批附件类
     */
    public final static String AUDIT_TYPE_FILE_CATEGORY = "118905";
    /**
     * 附件分类 - 默认附件大分类
     */
    public final static String CATEGORY_SUBCLASS_DEFAULT = "100699";
    /**
     * 附件分类 - 默认附件小分类
     */
    public final static String CATEGORY_CATEGORY_DEFAULT = "1006";

    /**
     * 性别 - 女
     */
    public final static String SEX_FEMALE = "100612";
    /**
     * 性别 - 男
     */
    public final static String SEX_MAN = "100611";

    /**
     * 默认短信签名
     */
    public final static String SMS_DEFAULT_SIGN = "【中犇租赁】";

    /**
     * 签约主体-中犇
     */
    public final static String PAY_SIGN_SUBJECT_ZB = "zb";

}
