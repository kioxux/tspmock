package com.kioxu.business.annotation;

/**
 * 加密类型(set的时候进行解密，get的时候进行加密)
 *
 * @author fangjinsuo.com
 *
 */
public enum CryptTypes {
    /**
     * 调用set设置值时进行加密
     */
    SET,
    /**
     * 调用get获得值得时候进行解密
     */
    GET
}

