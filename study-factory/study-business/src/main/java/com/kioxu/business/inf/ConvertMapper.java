package com.kioxu.business.inf;

import com.kioxu.business.vo.User;
import com.kioxu.business.vo.UserVo;
import org.mapstruct.Mapper;

@Mapper
public interface ConvertMapper {
    UserVo userToUserVo(User user);
}
