package com.kioxu.business.vo;

import lombok.Data;

@Data
public class UserVo {
    private String name;
    private int age;
}
