package com.kioxu.business.vo;

import lombok.Data;

/**
 * 返回的用户信息
 */
@Data
public class UserInfoVo {
    /**
     * 上级领导人Id
     */
    private  String upUserId;
    /**
     * 上级领导人编码
     */
    private  String upUserCode;
    /**
     * 上级领导人姓名
     */
    private String upUserName;
    /**
     * 部门编码
     */
    private String orgCode;
    /**
     * 部门名称
     */
    private String orgName;


    /**
     * 手机号
     */
    private String telePhone;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 用户姓名
     */
    private  String userName;
    /**
     * 用户id
     */
    private  String userId;
    /**
     * 用户编码
     */
    private  String userCode;
    /**
     * 公司编码
     */
    private  String compCode;

    /**
     * 公司名称
     */
    private  String companyName;





}
