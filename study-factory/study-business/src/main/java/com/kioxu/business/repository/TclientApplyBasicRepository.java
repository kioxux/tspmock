package com.kioxu.business.repository;

import com.kioxu.entity.client.TclientApplyBasic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface TclientApplyBasicRepository extends JpaRepository<TclientApplyBasic, Integer>, JpaSpecificationExecutor<TclientApplyBasic> {
    Integer countByOrderNumber(String orderNumber);
	TclientApplyBasic findByBusinessNum(String businessNum);
    TclientApplyBasic findByOrderNumber(String orderNumber);
	List<TclientApplyBasic> findByApplyStatus(String status);

	@Query(value = "update TclientApplyBasic t set t.applyStatus = ?1, t.updateTime = CURRENT_TIMESTAMP where t.id = ?2")
	@Modifying(clearAutomatically = true)
	int updateApplyStatus(String newStatus, Integer orderId);

	@Query(value = "update TclientApplyBasic t set t.pretrialLoanStatus = ?1, t.updateTime = CURRENT_TIMESTAMP where t.id = ?2")
	@Modifying(clearAutomatically = true)
	int updatePretrialLoanStatus(Integer PretrialLoanStatus, Integer orderId);

	int countByIdCardAndApplyStatusInAndTrain(String idCard, List<String> status, Integer train);

	int countByIdCardAndApplyStatusInAndTrainAndProductId(String idCard, List<String> status, Integer train, Integer productId);

	@Query(value = "select t.id from client.tclient_apply_basic t where t.client_name =?1 and t.id_card =?2 and t.mobile =?3", nativeQuery = true)
	List<Integer> findOrderIds(String clientName, String idCard, String mobile);
	TclientApplyBasic findFirstByClientNameAndIdCardAndMobileAndApplyStatusInOrderByCreateTimeDesc(String clientName, String idCard, String mobile, List<String> applyStatusList);
	TclientApplyBasic findFirstByClientNameAndIdCardAndTrainOrderByCreateTimeDesc(String clientName, String idCard, Integer train);
	List<TclientApplyBasic> findByIdCardAndUserId(String idCard, Integer userId);

	int countByApplyStatusAndIdCard(String status, String idCard);

	int countBySalesIdAndApplyStatusIn(Integer sales, List<String> status);

	int countBySalesId(Integer sales);

	@Query(value = "select * from client.tclient_apply_basic t where t.apply_status != '300115' and t.apply_status != '300113' and t.apply_status != '300114' " +
			"and if(:#{#dto.orderNumber} is not null && :#{#dto.orderNumber} !='', (t.order_number = :#{#dto.orderNumber}), 1=1) " +
			"and if(:#{#dto.clientName} is not null && :#{#dto.clientName} !='', (t.client_name = :#{#dto.clientName}), 1=1) " +
			"and if(:#{#dto.idCard} is not null && :#{#dto.idCard} !='', (t.id_card = :#{#dto.idCard}), 1=1) " +
			"and if(:#{#dto.mobile} is not null && :#{#dto.mobile} !='', (t.mobile = :#{#dto.mobile}), 1=1) " +
			"and if(:#{#dto.bankNo} is not null && :#{#dto.bankNo} !='', (t.bank_no = :#{#dto.bankNo}), 1=1) " +
			"order by t.id desc", nativeQuery = true)
	List<TclientApplyBasic> queryByBasicList(@Param("dto") TclientApplyBasic dto);

	@Query(value = "SELECT a.* FROM client.tclient_apply_basic a, client.tclient_order_product_relationship pr " +
			"WHERE a.id = pr.order_id AND pr.capital = ?3 AND pr.zb_child_status = '1' " +
			"and a.apply_status in (?1) AND a.is_identification = 1 " +
			"AND a.is_ocr = 1 AND a.train = ?2 AND a.pretrial_loan_status IS NOT NULL", nativeQuery = true)
	List<TclientApplyBasic> queryPreApplyBasic(List<String> status, int train, String capital);

	@Query(value = "SELECT a.* FROM client.tclient_apply_basic a, client.tclient_order_product_relationship pr " +
			"WHERE a.id = pr.order_id AND pr.capital = ?3 AND pr.zb_child_status = '1' " +
			"and a.apply_status in (?1) AND a.is_identification = 1 " +
			"AND a.is_ocr = 1 AND a.train = ?2 AND a.pretrial_loan_status IS NOT NULL and locate(?4,a.order_number) > 0", nativeQuery = true)
	List<TclientApplyBasic> queryFlowPreApplyBasic(List<String> status, int train, String capital, String condition);

	@Query(value = "SELECT a.* FROM client.tclient_apply_basic a " +
			"WHERE a.apply_status in (?1) AND a.is_identification = 1 " +
			"AND a.is_ocr = 1 AND a.train = ?2 AND a.pretrial_loan_status = 0", nativeQuery = true)
	List<TclientApplyBasic> queryCreditAuthorizationBasic(List<String> status, int train);

	@Query(value = "update TclientApplyBasic t set t.productId = ?1, t.productName = ?2 , t.updateTime = CURRENT_TIMESTAMP where t.id = ?3")
	@Modifying(clearAutomatically = true)
	int updateProductIdAndProductName(Integer productId, String productName, Integer orderId);


	@Query(value = "SELECT t.product_id,ROW_NUMBER() over(ORDER BY t.num desc) idx  \n" +
			"from (\n" +
			"SELECT\n" +
			"\tproduct_id,count(*) num\n" +
			"\tfrom client.tclient_apply_basic \n" +
			"\tWHERE product_id is not null \n" +
			"\tand DATEDIFF(now(),recording_time)<15\n" +
			"\tGROUP BY product_id\n" +
			")t limit 10",nativeQuery = true)
	List<Map<String,Object>> listTop10Product();

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('clientName', group_concat(distinct client_name)) as duplicateContent from client.tclient_apply_basic " +
			"where id_card=?1 and client_name is not null and client_name !='' and client_name !=?2 group by id_card", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleOne(String idCard, String clientName);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tab.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('address',group_concat(distinct tab.address)," +
			"'livingAddress',group_concat(distinct tab.living_address),'livingProvince',group_concat(distinct (select privince_name from base.province where id=tab.living_province))," +
			"'livingCity', group_concat(distinct (select city_name from base.city where id=tab.living_city)),'livingCounty',group_concat(distinct (select dist_name from base.district where id=tab.living_county))) as duplicateContent from (" +
			"select tb.order_number,tc.id_card,tc.address as address,null as living_address,null as living_province,null as living_city,null as living_county " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 and tc.address is not null and tc.address != '' " +
			"where tc.address !=?2 " +
			"union " +
			"select tb.order_number,tc.id_card,null as address,tad.living_address,null as living_province,null as living_city,null as living_county " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_address is not null and tad.living_address != '' " +
			"where tad.living_address !=?3 " +
			"union " +
			"select tb.order_number,tc.id_card,null as address,null as living_address,tad.living_province,null as living_city,null as living_county " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_province is not null and tad.living_province != '' " +
			"where tad.living_province !=?4 " +
			"union " +
			"select tb.order_number,tc.id_card,null as address,null as living_address,null as living_province,tad.living_city as living_city,null as living_county " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_city is not null and tad.living_city != '' " +
			"where tad.living_city !=?5 " +
			"union " +
			"select tb.order_number,tc.id_card,null as address,null as living_address,null as living_province,null as living_city,tad.living_county as living_county " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.living_county is not null and tad.living_county != '' " +
			"where tad.living_county !=?6) tab group by tab.id_card", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleTwo(String idCard, String address, String livingAddress, String livingProvince, String livingCity, String livingCounty);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tab.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('companyAddress',group_concat(distinct tab.company_address)," +
			"'companyPrivince',group_concat(distinct (select privince_name from base.province where id=tab.company_privince)),'companyCity',group_concat(distinct (select city_name from base.city where id=tab.company_city))," +
			"'companyArea',group_concat(distinct (select dist_name from base.district where id=tab.company_area))) as duplicateContent from (" +
			"select tb.order_number,tc.id_card,taw.company_address,null as company_privince,null as company_city,null as company_area " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_address is not null and taw.company_address != '' " +
			"where taw.company_address !=?2 " +
			"union " +
			"select tb.order_number,tc.id_card,null as company_address,taw.company_privince,null as company_city,null as company_area " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_privince is not null and taw.company_privince != '' " +
			"where taw.company_privince !=?3 " +
			"union " +
			"select tb.order_number,tc.id_card,null as company_address,null as company_privince,taw.company_city,null as company_area " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_city is not null and taw.company_city != '' " +
			"where taw.company_city !=?4 " +
			"union " +
			"select tb.order_number,tc.id_card,null as company_address,null as company_privince,null as company_city,taw.company_area " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id and taw.company_area is not null and taw.company_area != '' " +
			"where taw.company_area !=?5) tab group by tab.id_card", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleThree(String idCard, String companyAddress, String companyPrivince, String companyCity, String companyArea);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tab.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('bMobile',group_concat(distinct tab.b_mobile)," +
			"'cMobile',group_concat(distinct tab.c_mobile),'livingPhone',group_concat(distinct tab.living_phone)) as duplicateContent from (" +
			"select order_number,id_card,mobile as b_mobile,null as c_mobile,null as living_phone from client.tclient_apply_basic where id_card=?1 and create_time >= date_sub(curdate(), interval 6 month) and mobile is not null and mobile !='' and mobile != ?2 " +
			"union " +
			"select tb.order_number,tc.id_card,null as b_mobile,tc.mobile as c_mobile,null as living_phone from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 and tc.create_time >= date_sub(curdate(), interval 6 month) and tc.mobile is not null and tc.mobile !='' " +
			"where tc.mobile != ?2 " +
			"union " +
			"select tb.order_number,tc.id_card,null as b_mobile,null as c_mobile,tad.living_phone from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id and tc.id_card=?1 " +
			"left join client.tclient_apply_cust_address tad on tad.cust_id=tc.id and tad.create_time >= date_sub(curdate(), interval 6 month) and tad.living_phone is not null and tad.living_phone !='' " +
			"where tad.living_phone != ?2) tab group by tab.id_card", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleFour(String idCard, String mobile);

	@Query(value = "select tb.order_number as orderNumbers,json_object('mobile',group_concat(distinct tac.mobile),'sameValue', tac.name) as duplicateContent " +
			"from client.tclient_apply_contacts tac " +
			"join client.tclient_apply_basic tb on tac.order_id=tb.id and tb.order_number =?1 group by tac.name having count(distinct tac.mobile) > 1", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleFive(String orderNumber);

	@Query(value = "select tb.order_number as orderNumbers,json_object('name',group_concat(distinct tac.name),'sameValue', tac.mobile) as duplicateContent " +
			"from client.tclient_apply_contacts tac " +
			"join client.tclient_apply_basic tb on tac.order_id=tb.id and tb.order_number =?1 group by tac.mobile having count(distinct tac.name) > 1", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleSix(String orderNumber);

	@Query(value = "select tb.order_number as orderNumbers,json_object('name',group_concat(distinct tac.name)) as duplicateContent " +
			"from client.tclient_apply_contacts tac " +
			"join client.tclient_apply_basic tb on tac.order_id=tb.id and tb.order_number =?1 " +
			"where tac.name is not null and tac.name !='' and tac.name != tb.client_name and tac.mobile=tb.mobile group by tac.mobile", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleSeven(String orderNumber);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tb.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('name',tac.name) as duplicateContent " +
			"from client.tclient_apply_contacts tac " +
			"join client.tclient_apply_basic tb on tac.order_id=tb.id where tac.name = ?2 and tb.order_number !=?1 group by tac.name", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleEight(String orderNumber, String clientName);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tb.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('mobile',tac.mobile) as duplicateContent " +
			"from client.tclient_apply_contacts tac " +
			"join client.tclient_apply_basic tb on tac.order_id=tb.id where tac.mobile = ?2 and tb.order_number !=?1 group by tac.mobile", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleNine(String orderNumber, String mobile);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tb.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('org',taw.org) as duplicateContent " +
			"from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id " +
			"where tb.order_number !=?1 and taw.org =?2 group by taw.org ", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleTen(String orderNumber, String org);

	@Query(value = "select ?4 as orderNumbers,json_object('cardNo',card_no,'name',name,'mobile',mobile) as duplicateContent " +
			"from client.client_blacklist " +
			"where card_no=?1 and name=?2 and mobile=?3 and current_status='1' limit 1", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleEleven(String idCard, String clientName, String mobile, String orderNumber);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tb.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('vin',tr.vin) as duplicateContent " +
			"from client.tclient_apply_car tr " +
			"join client.tclient_apply_basic tb on tr.order_id=tb.id " +
			"where tb.id_card !=?1 and tr.vin =?2 group by tr.vin", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleTwelve(String idCard, String vin);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tb.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('address', tc.address) as duplicateContent " +
			"from client.tclient_apply_cust tc join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"where tc.id_card !=?1 and tc.address =?2 group by tc.address", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleThirteen(String idCard, String address);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tab.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers,json_object('mobile',group_concat(distinct tab.mobile), " +
			"'livingAddress',group_concat(distinct tab.living_address),'org',group_concat(distinct tab.org), " +
			"'companyAddress',group_concat(distinct tab.company_address),'companyMobile', " +
			"group_concat(distinct tab.company_mobile),'contactInfo',replace(replace(replace(json_unquote(json_arrayagg(tab.contactInfo)),'null,',''),' ',''),',null','')) as duplicateContent from ( " +
			"select tc.id_card,tb.order_number,tc.mobile,null as living_address,null as org,null as company_address, " +
			"null as company_mobile,null as contactInfo from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"where tc.id_card=?1 and tc.mobile !=?2 " +
			"union " +
			"select tc.id_card,tb.order_number,null as mobile,tad.living_address,null as org,null as company_address, " +
			"null as company_mobile,null as contactInfo from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"left join client.tclient_apply_cust_address tad on tad.cust_id=tc.id " +
			"where tc.id_card=?1 and tad.living_address !=?3 " +
			"union " +
			"select tc.id_card,tb.order_number,null as mobile,null as living_address,taw.org,null as company_address, " +
			"null as company_mobile,null as contactInfo from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id " +
			"where tc.id_card=?1 and taw.org !=?4 " +
			"union " +
			"select tc.id_card,tb.order_number,null as mobile,null as living_address,null as org,taw.company_address, " +
			"null as company_mobile,null as contactInfo from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id " +
			"where tc.id_card=?1 and taw.company_address !=?5 " +
			"union " +
			"select tc.id_card,tb.order_number,null as mobile,null as living_address,null as org,null as company_address, " +
			"taw.mobile as company_mobile,null as contactInfo from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"left join client.tclient_apply_cust_work taw on taw.cust_id=tc.id " +
			"where tc.id_card=?1 and taw.mobile is not null and taw.mobile !=?6 " +
			"union " +
			"select tc.id_card,tb.order_number,null as mobile,null as living_address,null as org,null as company_address, " +
			"null as company_mobile,json_object('sameValue',tac.name,'mobile',tac.mobile) as contactInfo from client.tclient_apply_cust tc " +
			"join client.tclient_apply_basic tb on tc.order_id=tb.id " +
			"left join client.tclient_apply_contacts tac on tac.order_id=tb.id " +
			"where tc.id_card=?1 and concat(tac.name,'-',tac.mobile) not in (?7)) tab group by tab.id_card", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleFourteen(String idCard, String mobile, String livingAddress, String org, String companyAddress, String companyMobile, List<String> contact);

	@Query(value = "select replace(replace(replace(replace(json_unquote(json_arrayagg(tb.order_number)), '[', ''), ']', ''),'\"',''),' ','') as orderNumbers, " +
			"json_object('sameValue',group_concat(distinct tac.name),'mobile',tac.mobile) as duplicateContent " +
			"from client.tclient_apply_contacts tac " +
			"join client.tclient_apply_basic tb on tac.order_id=tb.id " +
			"join client.tclient_apply_cust tc on tc.order_id=tb.id " +
			"where tc.id_card !=?1 and tac.mobile=?2 and tac.create_time between date_sub(curdate(), interval 1 month) and date_add(curdate(), interval 1 month) " +
			"group by tac.mobile having count(distinct tb.order_number) > 1", nativeQuery = true)
	Map<String, Object> findDifferentInfoByRuleFifteen(String idCard, String contactMobile);

	@Query(value = "update client.tclient_apply_basic t set t.explain_remark =?2, t.update_time = CURRENT_TIMESTAMP where t.order_number = ?1", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	void saveExplainRemarkr(String orderNumber, String explainRemark);

	@Query(value = "select 'all' as applyStatus,count(*) as num from client.tclient_apply_basic " +
			"union " +
			"select apply_status as applyStatus,count(*) as num from client.tclient_apply_basic group by apply_status", nativeQuery = true)
	List<Map<String, Object>> queryApplyStatusList();

	@Query(value = "select b.store,b.sales,b.client_source as clientSource,pmi.product_name as productName,b.apply_type as applyType,b.client_name as clientName,'无' as formerName,cust.id_card as idCard,cust.mobile,cust.sex,cust.nation,cast(cust.age as char) as age,'有' as isDriving,ifnull(cust.wechat_no,cust.mobile) as wechatNo,cust.marital_status as maritalStatus,ifnull(cust.alipay_no,cust.mobile) as alipayNo,cust.education,cust.address,adr.living_address as livingAddress,adr.current_living_years as currentLivingYears,adr.property_nature as propertyNature,cwk.org,cwk.duty,cwk.company_type as companyType,cwk.department,cwk.career,cwk.industry,cwk.company_address as companyAddress,cwk.mobile as companyMobile,cwk.work_year as workYear,if(cwk.is_social_security_fund,'是','否') as isSocialSecurityFund from client.tclient_apply_basic b left join base.product_max_info pmi on b.product_id=pmi.id left join client.tclient_apply_cust cust on b.id = cust.order_id left join client.tclient_apply_cust_address adr on cust.id=adr.cust_id left join client.tclient_apply_cust_work cwk on cust.id = cwk.cust_id where b.id = ?1", nativeQuery = true)
	Map<String, String> queryApplySignContent(Integer orderId);

	@Query(value = "update TclientApplyBasic t set t.isCompleteSign = false , t.updateTime = CURRENT_TIMESTAMP where t.id = ?1")
	@Modifying(clearAutomatically = true)
	int updateCompleteSign(Integer orderId);

	@Query(value = "select b.order_number from client.tclient_apply_basic b " +
			"join client.tclient_order_product_relationship rp on rp.order_id=b.id " +
			"join client.client_credit cc on b.order_number=cc.credit_code " +
			"join workflow.task_list tl on cc.id=tl.project_id " +
			"where b.apply_status=?1 and b.train=?2 and rp.capital in (?3) and rp.zb_child_status='1' and tl.task_code='CLF10010001' and tl.submit_time is null group by b.order_number", nativeQuery = true)
	List<String> queryAutoInterviewReject(String status, int train, List<String> capitals);

	@Query(value = "select ifnull( ui.user_name, cast( tl.task_agent AS CHAR ) ) audit_name," +
			"tu.task_name," +
			"cc.security_level," +
			"tl.apply_money," +
			"tor.task_oper_name," +
			"arr.sutype_name refuse_name," +
			"case tl.add_material when '2' then '信息更改' when '4' then '退回补件' when '5' then '退回实地' when '6' then '退回实地补件' else tl.add_material end add_material," +
			"tl.oper_reason_remark," +
			"cc.create_time apply_time," +
			"tl.get_time," +
			"tl.submit_time," +
			"tl.other_remark " +
			"from workflow.task_list tl " +
			"inner join client.client_credit cc on tl.project_id = cc.id " +
			"left join base.user_info ui on tl.task_agent = ui.user_id " +
			"left join workflow.task_url tu on tl.task_code = tu.task_code " +
			"left join workflow.task_oper_reason tor on tor.task_oper_code = tl.oper_type " +
			"left join client.audit_refused_reason arr on arr.sutype_code = tl.oper_reason and arr.audit_type = '118901' " +
			"where tl.project_id = ?1 order by tl.create_time desc,tl.task_id desc", nativeQuery = true)
	List<Map<String, Object>> queryApprovalInfo(Integer projectId);

	@Query(value = "select t.id, t.order_number as orderNumber, t.client_name as clientName, t.apply_status as applyStatus,t.id_card as idCard, t.mobile, t.create_time as createTime, t.train, c.product_name as productName from client.tclient_apply_basic t left join base.product_max_info c on t.product_id = c.id where" +
			" t.id_card = ?1 and t.mobile = ?2 and t.is_select_product = 1 ORDER BY t.create_time DESC", nativeQuery = true)
	List<Map<String, Object>> getOrderList(String idCard, String mobile);
}
