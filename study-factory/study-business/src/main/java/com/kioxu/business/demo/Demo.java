package com.kioxu.business.demo;

import cn.hutool.core.util.IdcardUtil;
import org.apache.commons.lang3.StringUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Thread)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@Threads(2)
public class Demo {
    @Param({"330123199001011234", "330123199601011234"})
    public String idCard;

    public static void main1(String[] args) {
        ClassTest test = new ClassTest();
        test.fun(test.str, test.ch);
        System.out.print(test.str+" and ");
        System.out.print(test.ch);
        // hello and dbc
    }

    public static void main2(String[] args) {
        Integer a = 127;
        Integer b = 127;
        System.out.println(a == b);
        // true
    }

    public static void main3(String[] args) {
        Sub s = new Sub();
        System.out.println(s.count);
        s.display();
        Base b = s;
        System.out.println(b.count);
        b.display();
        // 20
        // 20
        // 10
        // 20
        /**
         * 这段代码演示了Java中的继承、多态以及字段的隐藏。Sub类继承自Base类，并且两个类都有一个名为count的字段。在Java中，字段不是多态的，所以Sub类的count字段隐藏了Base类的count字段。
         * 当你创建Sub类的实例并直接访问count字段时，你访问的是Sub类中定义的count。但是，如果你将Sub类的实例赋值给Base类的引用，然后通过那个引用访问count字段，你访问的是Base类中定义的count，因为字段的访问是基于引用类型的，而不是实例的实际类型。
         * 另一方面，方法是多态的。如果子类覆盖了父类的方法，那么无论是通过子类的引用还是父类的引用调用该方法，都会调用子类中的实现。
         */
    }

    public static void main4(String[] args) {
        System.out.println("result2: "+test2());
        // result1: 2
        // result2: 1
        /**
         * 在Java中，finally块总是在try块之后执行，无论try块中发生了什么。但是，如果try块中有return语句，那么finally块中的修改不会影响try块中return语句的返回值。这是因为return语句返回的是在执行return语句时变量的值，即使finally块中对变量进行了修改，也不会影响已经确定要返回的值。
         */
    }

    public static void main5(String[] args) {
        Map<String, Object> result = new HashMap<>();
//        result.put("code", 0);
        System.out.println("0".equals(String.valueOf(result.get("code"))));
        System.out.println(StringUtils.equals("0", String.valueOf(result.get("code"))));
    }

    public static void main6(String[] args) {
        String tmp = "AD20231242432";
        System.out.println(StringUtils.startsWithAny(tmp, "AD", "DD"));
        String msg = "系统车牌号与车本不一致，请退回贷款申请修改；车本首页不清晰，请提供清晰照片";
        String[] searchStrings = {"请退回", "退回2", "自行退回"};

        boolean containsAny = Arrays.stream(searchStrings).anyMatch(searchString -> StringUtils.contains(msg, searchString));
        System.out.println(containsAny);
    }

    public static void main7(String[] args) throws Exception {
//        System.out.println(DateUtil.format(DateUtil.parse("20240311"), "yyyy-MM-dd"));
//        checkValidity("长期");
//        test2();
        Options opts = new OptionsBuilder()
                .include(Demo.class.getSimpleName())
                .resultFormat(ResultFormatType.JSON)
                .param("idCard", "330123199001011234")
                .build();
        new Runner(opts).run();
    }

    public static void main8(String[] args) {
        Queue queue = new ArrayDeque();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        System.out.println(queue);
        System.out.println("-----------");
        Deque stack = new ArrayDeque();
        stack.addFirst(1);
        stack.addFirst(2);
        stack.addFirst(3);
        System.out.println(stack);
    }

    public static void main9(String[] args) {
        List list = Arrays.asList("1","2","3","4","5","6");
        for (Object tmp : list) {
            if (Arrays.asList("1", "2").contains(tmp)) {
                return;
            }
            System.out.println("hello world");
        }
        System.out.println("out");
    }

    public static void main10(String[] args) {
        try {
            int[] arr = {1, 2, 3};
            System.out.println(arr[3]);
        } catch (Exception e) {
            System.out.println(e);
            throw new RuntimeException(e);
        }
        System.out.println("hello world");
    }

    public static void moveZeros(int[] nums) {
        int dix = 0;
        for (int num : nums) {
            if (num != 0) {
                nums[dix++] = num;
            }
        }
        while (dix < nums.length) {
            nums[dix++] = 0;
        }
    }

    public static int[][] matrixReshape(int[][] nums, int r, int c) {
        int m = nums.length;
        int n = nums[0].length;
        if (m * n != r * c) {
            return nums;
        }
        int[][] res = new int[r][c];
        int index = 0;
        for (int i=0; i<r; i++) {
            for (int j=0; j<c; j++) {
                res[i][j] = nums[index/n][index%n];
                index++;
            }
        }
        return res;
    }

    @Benchmark
    public int getAge() {
        return IdcardUtil.getAgeByIdCard(idCard);
    }

    public static void checkValidity(String expiryDateStr) {
        LocalDate today = LocalDate.now();
        // 假设“长期”有效期表示至少有效到当前日期之后的某个时间点
        if ("长期".equals(expiryDateStr)) {
            System.out.println("身份证有效。");
            return;
        }

        // 尝试解析日期，支持多种格式
        LocalDate expiryDate = null;
        try {
            expiryDate = LocalDate.parse(expiryDateStr, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            try {
                expiryDate = LocalDate.parse(expiryDateStr, DateTimeFormatter.ofPattern("yyyyMMdd"));
            } catch (DateTimeParseException e2) {
                System.err.println("无法解析的日期格式: " + expiryDateStr);
                return;
            }
        }

        // 检查日期是否在有效期内
        if (expiryDate != null && !expiryDate.isBefore(today)) {
            System.out.println("身份证有效。");
        } else {
            System.out.println("身份证已过期。");
        }
    }

    private static int test2() {
        int x = 1;
        try {
            return x;
        } finally {
            x = 2;
            System.out.println("result1: " + x);
        }
    }

    /**
     * Employee表包含所有员工，他们的经理也属于员工，每个员工都有一个id，此外还有一列对应员工的经理id。
     * ID NAME SALARY MANAGER_ID
     * 1 TOM   7000.00 3
     * 2 JACK  4000.00 4
     * 3 LUCY  6000.00
     * 4 LILY  9000.00
     *
     * 1.编写一个SQL查询，该查询可以获取收入超过他们经理的员工的姓名
     * SELECT e.NAME
     * FROM Employee e
     * JOIN Employee m ON e.MANAGER_ID = m.ID
     * WHERE e.SALARY > m.SALARY;
     *
     * 2.编写一个SQL查询，获取 Employee 表中第二高的薪水（Salary）
     * SELECT MAX(SALARY) AS SecondHighestSalary
     * FROM Employee
     * WHERE SALARY < (SELECT MAX(SALARY) FROM Employee);
     */
    public static class Base {
        int count = 10;
        public void display() {
            System.out.println(this.count);
        }
    }

    public static class Sub extends Base {
        int count = 20;
        public void display() {
            System.out.println(this.count);
        }
    }

    static class ClassTest {
        String str = new String("hello");
        char[] ch = {'a', 'b', 'c'};
        public void fun(String str, char[] ch) {
            str = "world";
            ch[0] = 'd';
        }
    }
}
