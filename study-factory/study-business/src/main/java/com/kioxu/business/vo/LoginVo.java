package com.kioxu.business.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 登录请求信息
 */
@Data
public class LoginVo {
    private String username;
    private String password;
}
