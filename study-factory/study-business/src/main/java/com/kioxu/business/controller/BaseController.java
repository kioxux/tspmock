package com.kioxu.business.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kioxu.business.exception.BusinessException;
import com.kioxu.business.util.CryptUtils;
import com.kioxu.business.util.StreamUtil;
import com.kioxu.business.vo.OutVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 基本控制类
 *
 * @author hujp
 * @date 2019/9/16
 *
 */
public abstract class BaseController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Validator validator;

    /**
     * 打印vo对象
     *
     * @param out
     */
    public void outVoWrite(OutVo<?> out) {
        String jsonObject = JSON.toJSONString(out);
        logger.info("Response:object json：" + jsonObject);
    }

    /**
     * 从流中读取数据转为Json对象，同时关闭流
     *
     * @param request
     *            请求对象
     * @return
     */
    public JSONObject readAndCloseInputStream(HttpServletRequest request) {
        try {
            String dataStr = StreamUtil.readLine(request.getInputStream(), Charset.forName("UTF-8"));
            return JSONObject.parseObject(dataStr);
        } catch (Exception ex) {
            logger.error("readAndCloseInputStream:error:{}", ex);
            return null;
        }
    }

    /**
     * 验证加密是否正确
     *
     * @param str
     * @return
     */
    public boolean checkEncrypt(String str) {
        try {
            CryptUtils.baseEncrypt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getToken(HttpServletRequest request) {
        return request.getHeader("token");
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(Date.class,
                new CustomDateEditor(new BaseDateFormat("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "yyyy年MM月dd日", "yyyy年MM月dd日 HH时mm分ss秒"), false));
    }
    /**
     * 处理Controller抛出的异常
     * @param e 异常实例
     * @return Controller层的返回值
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class, HttpMediaTypeNotSupportedException.class})
    @ResponseBody
    public OutVo<String> expHandler(Exception e){
        MethodArgumentNotValidException c = (MethodArgumentNotValidException) e;
        List<ObjectError> errors =c.getBindingResult().getAllErrors();
        StringBuffer errorMsg = new StringBuffer();
        StringBuffer propertyPath = new StringBuffer();
        errors.stream().forEach(x -> {
            if(x instanceof FieldError) {
                FieldError fe = (FieldError) x;
                errorMsg.append(fe.getDefaultMessage()).append(";");
                propertyPath.append(fe.getField()).append(";");
            } else {
                errorMsg.append(x.getDefaultMessage()).append(";");
                propertyPath.append(x.getObjectName()).append(";");
            }
        });

        // 删除尾部
        delEndChar(errorMsg, ';');
        delEndChar(propertyPath, ';');

        OutVo<String> out = new OutVo<>();
        out.setRespCode(0);
        out.setRespMsg(errorMsg.toString());
        out.setPropertyPath(propertyPath.toString());

        return out;
    }

    /**
     * 校验参数必填(javax.validation.Validator)
     *
     */
    public <T> void validator(T vo) {
        validator(vo, "");
    }

    public <T> void validator(T vo, String info) {
        Set<ConstraintViolation<T>> set = validator.validate(vo, Default.class);
        if(set == null || set.size() == 0) {
            return;
        }

        StringBuffer sb = new StringBuffer();
        StringBuffer pp = new StringBuffer();
        Iterator<ConstraintViolation<T>> iter = set.iterator();
        while (iter.hasNext()) {
            ConstraintViolation<T> t = iter.next();
            sb.append(t.getMessage()).append(";");

            pp.append(String.format("%s%s", info, t.getPropertyPath().toString())).append(";");
        }
        // 删除尾部
        delEndChar(sb, ';');
        delEndChar(pp, ';');

        throw new BusinessException(0, sb.toString(), pp.toString());
    }

    protected void delEndChar(StringBuffer sb, char c) {
        if(sb == null) {
            return;
        }

        if(sb.charAt(sb.length()-1) == c) {
            sb.deleteCharAt(sb.length() - 1);
        }
    }

    public <T> void validator(List<T> vos) {
        if(vos == null || vos.isEmpty()) {
            return;
        }

        for (int i = 0; i < vos.size(); i++) {
            validator(vos.get(i), "data["+i+"].");
        }
    }
}
