package com.kioxu.business.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 注解需要加密或者解密的字段（目前用在读取数据查询时候数据库数据是加密的，需要解密出来的情况和表单查询条件需要加密的情况）
 *
 * @author hujp
 *
 */
@Target({ METHOD, FIELD })
@Retention(RUNTIME)
public @interface CryptIntercept {
    CryptTypes[] mode() default { CryptTypes.GET, CryptTypes.SET };
}

