package com.kioxu.business.controller;

import com.alibaba.fastjson.JSONObject;
import com.kioxu.business.constant.ResultResource;
import com.kioxu.business.feign.OAuth2Service;
import com.kioxu.business.vo.LoginSusDTO;
import com.kioxu.business.vo.LoginVo;
import com.kioxu.business.vo.ResultDTO;
import com.kioxu.business.vo.UserInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;
import java.util.Objects;

@RestController
@Slf4j
public class LoginController {
    @Autowired
    OAuth2Service oAuthService;
    @Value("${security.oauth2.client.client-id}")
    private String clientId;
    @Value("${security.oauth2.client.client-secret}")
    private String clientSecret;
    @Value("${security.oauth2.client.grant-type}")
    private String grantType;

    /**
     * 系统-登录
     *
     * @return
     */
    @PostMapping("/login/loginSystem")
    @ResponseBody
    public ResultDTO loginSystem(@RequestBody LoginVo loginVo) {
        if(Objects.isNull(loginVo) ||Objects.isNull(loginVo.getPassword()) || Objects.isNull(loginVo.getPassword())){
            return ResultDTO.error(ResultResource.CODE_LOGIN_NULL, ResultResource.MESSAGE_LOGIN_NULL);
        }
        try {
            JSONObject responseJson = oAuthService.getOAuthToken(grantType, loginVo.getUsername(),
                    Base64.getEncoder().encodeToString(loginVo.getPassword().getBytes("UTF-8")) , clientId, clientSecret, "");
            log.info("oauth:" + responseJson.toJSONString());
            String accessToken = responseJson.getString("access_token");
            if (accessToken != null && accessToken.length() > 0) {
                LoginSusDTO bean = new LoginSusDTO();
                UserInfoVo userInfo= new UserInfoVo();
                if(userInfo!=null){
                    bean.setUserName(userInfo.getUserName());
                    bean.setCompCode(userInfo.getCompCode());
                    bean.setUserId(userInfo.getUserId());
                }
                bean.setAccessToken(accessToken);
                bean.setRefreshToken(responseJson.getString("refresh_token"));
                bean.setTokenType(responseJson.getString("token_type"));
                return ResultDTO.ok(bean);
            } else {
                return ResultDTO.error(ResultResource.CODE_LOGIN_ERROR, ResultResource.MESSAGE_LOGIN_ERROR);
            }
        } catch (Exception e) {
            log.error("-------------->登录系统操作失败", e);
            if (e instanceof RuntimeException) {
                if (e.getMessage().contains(ResultResource.MESSAGE_PASSWORD_ERROR)) {
                    return ResultDTO.error(ResultResource.CODE_PASSWORD_ERROR, ResultResource.MESSAGE_PASSWORD_ERROR);
                } else if (e.getMessage().contains(ResultResource.MESSAGE_NOT_EXIST_USER)) {
                    return ResultDTO.error(ResultResource.CODE_NOT_EXIST_USER, ResultResource.MESSAGE_NOT_EXIST_USER);
                } else if (e.getMessage().contains(ResultResource.MESSAGE_USER_DISABLE)) {
                    return ResultDTO.error(ResultResource.CODE_USER_DISABLE, ResultResource.MESSAGE_USER_DISABLE);
                } else {
                    return ResultDTO.error(ResultResource.CODE_LOGIN_ERROR, ResultResource.MESSAGE_LOGIN_ERROR);
                }
            } else {
                return ResultDTO.error(ResultResource.CODE_LOGIN_ERROR, ResultResource.MESSAGE_LOGIN_ERROR);
            }
        }
    }
}
