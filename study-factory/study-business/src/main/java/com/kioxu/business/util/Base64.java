package com.kioxu.business.util;

import java.nio.charset.StandardCharsets;

public class Base64 {
    private static final char[] legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
            .toCharArray();

    /**
     * data[]进行编码
     *
     * @param data
     * @return
     */
    public static String encode(byte[] data) {
        return new String(java.util.Base64.getEncoder().encode(data), StandardCharsets.UTF_8).replaceAll("[\\s*\\t\\n\\r]", "");
    }

    /**
     * Decodes the given Base64 encoded String to a new byte array. The byte
     * array holding the decoded data is returned.
     */

    public static byte[] decode(String s) {
        return java.util.Base64.getDecoder().decode(s.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * 数据进行解码
     * @param str
     * @return
     */
    public static String decodeStr(String str){
        if(str == null){
            return null;
        }
        try{
            byte[] dataByte = decode(str);
            String data = new String(dataByte, StandardCharsets.UTF_8);
            return data;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 数据进行编码
     */
    public static String encodeStr(String str){
        if(str == null){
            return null;
        }
        str = new String(str.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        byte[] data = str.getBytes(StandardCharsets.UTF_8);
        return encode(data);
    }

    public static void main(String args[]){
        String ss = "44CQ5oi/6YCf6LS344CR5oKo5q2j5Zyo5L2/55So5oi/6YCf6LS35LiA6ZSu 55m75b2V77yM6aqM6K+B56CBNTQ5MTI077yM6K+35Yu/5ZGK6K+J5Lu75L2V 5Lq644CCM+WIhumSn+WGheacieaViOOAgg==";
        System.out.println(decodeStr(ss));
    }

}