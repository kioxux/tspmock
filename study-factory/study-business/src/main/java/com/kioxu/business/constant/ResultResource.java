package com.kioxu.business.constant;

/**
 * Created by Administrator on 2017/6/9 0009.
 */
public class  ResultResource {
    public static int CODE_SYSTEM_ERROR = -1;
    public static String MESSAGE_SYSTEM_ERROR = "系统异常";

    /**
     * 错误码 - 正常
     */
    public static int ERROR_CODE_OK = 1;

    /**
     * 错误码 - 此账号在其他设备登陆
     */
    public static final int ERROR_CODE_KICKED_OUT = 6;

    // SUCCESS
    public static int CODE_SUCCESS = 0;
    public static String MESSAGE_SUCCESS = "成功";

    public static int CODE_LOGIN_NULL = 900000;
    public static String MESSAGE_LOGIN_NULL = "用户名或密码不能为空";

    // 登录失败
    public static int CODE_LOGIN_ERROR = 900001;
    public static String MESSAGE_LOGIN_ERROR = "登录失败";

    // 参数异常
    public static int CODE_PARAM_ERROR = 900002;
    public static String MESSAGE_PARAM_ERROR = "参数异常";

    // 该用户名已存在
    public static int CODE_USERNAME_EXIST_ERROR = 900003;
    public static String MESSAGE_USERNAME_EXIST_ERROR = "该手机号已存在";

    // 有下属的用户或者部门
    public static int CODE_HAS_SUB_OBJECT = 900004;
    public static String MESSAGE_HAS_SUB_OBJECT = "该用户名已存在";

    // 该短缩名已存在
    public static int CODE_EXIST_SAME_SHORTNAME = 900005;
    public static String MESSAGE_EXIST_SAME_SHORTNAME = "该公司简称已存在";

    // 该公司名已存在
    public static int CODE_EXIST_SAME_NAME = 900006;
    public static String MESSAGE_EXIST_SAME_NAME = "该公司名已存在";

    // 其他错误
    public static int CODE_OTHER_ERROR = 999999;
    public static String MESSAGE_OTHER_ERROR = "其他错误";

    public static  int CODE_ERROR_NUM = 1;
    public static  String MESSAGE_CHECK_ERROR = "该手机不是业务员";
    public static  String MESSAGE_CHECK_SUCCESS = "该手机是业务员";

    public static int CODE_NOT_EXIST_USER = 900007;
    public static String MESSAGE_NOT_EXIST_USER = "用户不存在";

    public static int CODE_USER_DISABLE = 900008;
    public static String MESSAGE_USER_DISABLE = "当前用户不可用";

    public static String MESSAGE_UNLOGIN = "还未扫码成功";

    public static int CODE_PASSWORD_ERROR = 900009;
    public static String MESSAGE_PASSWORD_ERROR = "密码不正确";

    public static int CODE_USER_HAS_COMPANY = 900010;
    public static String MESSAGE_USER_HAS_COMPANY = "用户已经加入公司，不能再申请加入";

    public static int CODE_EXIST_SAME_ACTIONNAME = 900011;
    public static String MESSAGE_EXIST_SAME_ACTIONNAME = "已经存在相同的方法名";

    public static int CODE_EXIST_SAME_MODULENAME = 900012;
    public static String MESSAGE_EXIST_SAME_MODULENAME = "已经存在相同的模块名";

    public static int CODE_EXIST_SUB_USER_DEPARTMENT = 900013;
    public static String MESSAGE_EXIST_SUB_USER_DEPARTMENT = "存在下属的部门和用户，不能删除该部门";

    public static int CODE_ERROR_EDITPASSWORD = 91000;
    public static String MESSAGE_ERROR_EDITPASSWORD = "修改密码出错";

    public  static int CODE_ERROR_GETUSERINFO = 91001;
    public  static String MESSAGE_ERROR_GETUSERINFO = "getUserInfo失败";
    //-----------------------------贷后 异常 码表--------------------------------------------

    public  static int CODE_ERROR_CITY_USER = 70001;
    public  static String MESSAGE_ERROR_CITY_USER = "该城市下家访员被门店使用";

    public static int CODE_EXIST_INVALID_GRANT = 900014;
    public static String MESSAGE_EXIST_INVALID_GRANT = "无效的授权";

    public static int CODE_EXIST_INVALID_TOKEN = 900015;
    public static String MESSAGE_EXIST_INVALID_TOKEN = "无效的凭据";

}
