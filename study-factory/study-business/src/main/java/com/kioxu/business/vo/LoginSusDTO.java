package com.kioxu.business.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 登录返回信息
 */
@Data
public class LoginSusDTO {
    private String userName;

    private String accessToken;

    private String refreshToken;

    private String compCode;

    private String tokenType;

    private String userId;
}
