package com.kioxu.business.feign;

import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@Slf4j
public class FeignExceptionConfiguration {
    @Bean
    public ErrorDecoder errorDecoder() {
        return new UserErrorDecoder();
    }
    /**
     * 重新实现feign的异常处理，捕捉restful接口返回的json格式的异常信息
     *
     */
    public class UserErrorDecoder implements ErrorDecoder {
        @Override
        public Exception decode(String methodKey, Response response) {
            try {
                // 这里直接拿到我们抛出的异常信息
                String message = Util.toString(response.body().asReader());
                log.error("服务器返回："+ message);
                return new RuntimeException(message);
            } catch (IOException ignored) {
            }
            return decode(methodKey, response);
        }
    }
}