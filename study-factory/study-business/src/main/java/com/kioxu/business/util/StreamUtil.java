package com.kioxu.business.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;

public class StreamUtil {

    /**
     * 流复制操作
     *
     * @param in 输入流
     * @param out 输出流
     * @throws IOException
     */
    public static void copyStream(InputStream in,OutputStream out) throws IOException{
        try {
            // 创建一个缓冲区
            byte buffer[] = new byte[1024];
            // 判断输入流中的数据是否已经读完的标识
            int len = 0;
            // 循环将输入流读入到缓冲区当中，(len=in.read(buffer))>0就表示in里面还有数据
            while ((len = in.read(buffer)) > 0) {
                // 使用FileOutputStream输出流将缓冲区的数据写入到指定的目录(savePath + "\\"
                // + filename)当中
                out.write(buffer, 0, len);
            }
        } finally{
            // 关闭输入流
            StreamUtil.close(in);
            // 关闭输出流
            StreamUtil.close(out);
        }
    }
    /**
     * 字节保存文件
     *
     * @param bytes
     * @param file
     * @throws IOException
     */
    public static void saveFile(byte[] bytes,File file) throws IOException{
        FileOutputStream out = null;
        BufferedOutputStream stream = null;
        try {
            out = new FileOutputStream(file);
            stream = new BufferedOutputStream(out);
            stream.write(bytes);
        } finally {
            close(stream);
            close(out);
        }
    }

    /**
     * 读取刘转换成字符串
     *
     * @param input
     * @return
     * @throws IOException
     */
    public static String toString(InputStream input) throws IOException {
        if (input == null) {
            return "";
        }
        BufferedReader reader = null;
        InputStreamReader read = null;
        StringBuilder sb = new StringBuilder();
        try {
            read = new InputStreamReader(input);
            reader = new BufferedReader(read);
            String str = null;
            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }
        } finally {
            close(read);
            close(reader);
        }
        return sb.toString();
    }

    /**
     * 读取流中字符串
     *
     * @param input 输入流
     * @return 字符串
     * @throws IOException
     */
    public static String readLine(InputStream input,Charset charset) throws IOException{
        StringBuilder sb = new StringBuilder();

        InputStreamReader read = null;
        BufferedReader br = null;
        try {
            read = new InputStreamReader(input, charset);
            br = new BufferedReader(read);
            String str = null;
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } finally {
            close(br);
            close(read);
            close(input);
        }
        return sb.toString();
    }

    /**
     * 读取流字节
     *
     * @param input 输入流
     * @return
     * @throws IOException
     */
    public static byte[] readByte(InputStream input) throws IOException{
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            // 创建一个缓冲区
            byte buffer[] = new byte[1024];
            // 判断输入流中的数据是否已经读完的标识
            int len = 0;
            // 循环将输入流读入到缓冲区当中，(len=in.read(buffer))>0就表示in里面还有数据
            while ((len = input.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            return out.toByteArray();
        } finally{
            // 关闭输入流
            StreamUtil.close(input);
            StreamUtil.close(out);
        }

    }

    /**
     * 将文字写入文本文件
     *
     * @param str 字符
     * @param file 文件
     * @throws IOException
     */
    public static void writeFile(String str, File file) throws IOException{
        FileOutputStream output = null;
        BufferedWriter write = null;
        OutputStreamWriter out = null;
        try {
            output = new FileOutputStream(file);
            out = new OutputStreamWriter(output);
            write = new BufferedWriter(out);
            write.write(str);
        } finally {
            close(write);
            close(out);
            close(output);
        }
    }

    public static void close(InputStream input){
        if(input != null){
            try {
                input.close();
            } catch (IOException e) {
                try {
                    input.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public static void close(Writer w){
        if(w != null){
            try {
                w.flush();
                w.close();
            } catch (IOException e) {
                try {
                    w.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public static void close(Reader read){
        if(read != null){
            try {
                read.close();
            } catch (IOException e) {
                try {
                    read.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public static void close(OutputStream out){
        if(out != null){
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * 读取流字节(读取指定长度的流)
     *
     * @param input 输入流
     * @param len 读取的长度
     * @return
     * @throws IOException
     */
    public static byte[] readByte(InputStream input, int len) throws IOException{
        try {
            // 创建一个缓冲区
            byte buffer[] = new byte[len];
            // 循环将输入流读入到缓冲区当中，(len=in.read(buffer))>0就表示in里面还有数据
            int l = input.read(buffer);
            if(len > l) {
                byte[] bu = new byte[l];
                System.arraycopy(buffer, 0, bu, 0, l);
                buffer = bu;
            }

            return buffer;
        } finally{
            // 关闭输入流
            StreamUtil.close(input);
        }

    }

}
