package com.kioxu.business.messaging.kafka.message;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class KafkaMessage {
    private String name;
    private String sex;
    private int age;
    private int id;
}
