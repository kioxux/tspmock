package com.kioxu.business.jdbc;

import com.fastcnt.usercenter.handler.DataPermissionHandler;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlProvider;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MyJdbcTemplate extends JdbcTemplate {
    public MyJdbcTemplate(){
        super();
    }

    public MyJdbcTemplate(DataSource dataSource){
        super(dataSource);
    }

    public MyJdbcTemplate(DataSource dataSource, boolean lazyInit){
        super(dataSource, lazyInit);
    }

    @Override
    @Nullable
    public <T> T query(String sql, final ResultSetExtractor<T> rse) throws DataAccessException {
        Assert.notNull(sql, "SQL must not be null");
        Assert.notNull(rse, "ResultSetExtractor must not be null");

        // 数据权限处理
        sql = DataPermissionHandler.processSql(sql);
        /**
         * Callback to execute the query.
         */
        String finalSql = sql;
        class QueryStatementCallback implements StatementCallback<T>, SqlProvider {
            @Override
            @Nullable
            public T doInStatement(Statement stmt) throws SQLException {
                ResultSet rs = null;
                try {
                    rs = stmt.executeQuery(finalSql);
                    return rse.extractData(rs);
                }
                finally {
                    JdbcUtils.closeResultSet(rs);
                }
            }
            @Override
            public String getSql() {
                return finalSql;
            }
        }

        return execute(new QueryStatementCallback());
    }
}
