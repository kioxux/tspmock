//package com.kioxu.business.messaging.rabbitmq.entity;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
////@Component
////@RabbitListener(queues = RabbitDemo02Message.QUEUE)
//public class RabbitDemo02Consumer {
//    private Logger logger = LoggerFactory.getLogger(getClass());
//
//    @RabbitHandler
//    public void onMessage(RabbitDemo02Message message) {
//        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
//    }
//}
