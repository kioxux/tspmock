package com.kioxu.business.messaging.kafka.entity;

import com.kioxu.business.messaging.kafka.message.KafkaDemo01Message;
import com.kioxu.business.messaging.kafka.message.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

//@Component
public class KafkaDemo01Consumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @KafkaListener(topics = KafkaDemo01Message.TOPIC, groupId = "demo01-consumer-group-" + KafkaDemo01Message.TOPIC)
    public void onMessage(KafkaMessage message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
}
