docker run -p 24000:9000 --name portainer \--restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true -d portainer/portainer-ce

docker run -p 24001:5432 -v /senscloud/data/postgres/data:/var/lib/postgresql/data --name postgres -e POSTGRES_PASSWORD=pg123456 -e TZ=PRC -d postgres

docker run -d --name=syncthing --hostname=syncthing -e PUID=0 -e PGID=0 -e TZ=Europe/London -p 24002:8384 -p 22000:22000/tcp -p 22000:22000/udp -p 21027:21027/udp -v /senscloud/data/syncthing/config:/config -v /senscloud/data/syncthing/data1:/data1 -v /senscloud/data/syncthing/data2:/data2 --restart unless-stopped lscr.io/linuxserver/syncthing:latest
	
docker run -p 86:80 --restart=always --name nginx -v /data/nginx/html:/usr/share/nginx/html -v /data/nginx/logs:/var/log/nginx -v /data/nginx/conf:/etc/nginx -d nginx	
	
docker run --name pgadmin \
    -p 24003:80 \
    -e "PGADMIN_DEFAULT_EMAIL=cloud_system@senscloud.cn" \
    -e "PGADMIN_DEFAULT_PASSWORD=qad2fms1cqlvsT91hNI" \
    -v /senscloud/soft/pgadmin/data:/var/lib/pgadmin \
    -v /senscloud/soft/pgadmin/logs:/var/log/pgadmin \
    -d dpage/pgadmin4 	

docker build -f cisDockerFile -t cis:0.1 .
docker run -p 24004:8888 -v /senscloud/web/cis:/cis --name senscloud --restart=always -d cis:0.1

docker build -f syncDockerFile -t sync:0.1 .
docker run -p 24005:8087 -v /senscloud/web/syncservice:/sync --name syncservice --restart=always -d sync:0.1

docker build -f scadaDockerFile -t scada:0.1 .
docker run -p 24006:8800 -v /senscloud/web/scada:/scada --name scada --restart=always -d scada:0.1

docker run --name=registry -p 5000:5000 -v /data/registry:/var/lib/registry -d registry

-v /senscloud/data/postgres/usr:/usr/lib/postgresql/14/bin

docker system df 查看docker所占的硬盘大小

docker run -d -p 8088:9000 \--restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true portainer/portainer-ce
docker run -d  --name portainer -p 8088:9000 -v /var/run/docker.sock:/var/run/docker.sock -v /data/portainer/data:/data -v /data/portainer/public:/public portainer/portainer:1.20.2 
docker run -d -p 8088:9000 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock  -v /data/portainer/data:/data     portainer/portainer
docker run -p 42397:5432 -v /data/postgres/data/:/var/lib/postgresql/data --name postgres -e POSTGRES_PASSWORD=123456 -d postgres

# 用于清理磁盘，删除关闭的容器、无用的数据卷和网络，以及无tag的镜像。
docker system prune
# 可以将没有容器使用 Docker 镜像都删掉。注意，这两个命令会把你暂时关闭的容器，以及暂时没有用到的Docker镜像都删掉了
docker system prune -a

删除未启动成功的容器
docker rm $(docker ps -a|grep Created|awk '{print $1}')
或者
docker rm $(docker ps -qf status=created)

删除退出状态的容器
docker rm $(docker ps -a|grep Exited|awk '{print $1}')
或者
docker rm $(docker ps -qf status=exited)

删除所有未运行的容器
docker rm $(docker ps -a -q) #正在运行的删除不了，所有未运行的都被删除了
或者
docker container prune #Docker 1.13版本以后，可以使用 docker containers prune 命令，删除孤立的容器


随便启动一个 nginx 实例，只是为了复制出配置
docker run -p 80:80 --name nginx -d nginx:1.10
将容器内的配置文件拷贝到当前目录：docker container cp nginx:/etc/nginx .
:后面是容器内的目录，.是当前路径下
别忘了后面的点

docker cp :用于容器与主机之间的数据拷贝
1、从主机往容器中拷贝
eg：将主机/www/runoob目录拷贝到容器96f7f14e99ab的/www目录下。
docker cp /www/runoob 96f7f14e99ab:/www/

2、将容器中文件拷往主机
eg：将容器96f7f14e99ab的/www目录拷贝到主机的/tmp目录中。
docker cp  96f7f14e99ab:/www /tmp/

eg:将主机/www/runoob目录拷贝到容器96f7f14e99ab中，目录重命名为www。
docker cp /www/runoob 96f7f14e99ab:/www

FROM # 基础镜像，当前新镜像是基于哪个镜像的
MAINTAINER # 镜像维护者的姓名混合邮箱地址
RUN # 容器构建时需要运行的命令
EXPOSE # 当前容器对外保留出的端口
WORKDIR # 指定在创建容器后，终端默认登录的进来工作目录，一个落脚点
ENV # 用来在构建镜像过程中设置环境变量
ADD # 将宿主机目录下的文件拷贝进镜像且ADD命令会自动处理URL和解压tar压缩包
COPY # 类似ADD，拷贝文件和目录到镜像中！
VOLUME # 容器数据卷，用于数据保存和持久化工作
CMD # 指定一个容器启动时要运行的命令，dockerFile中可以有多个CMD指令，但只有最
后一个生效！
ENTRYPOINT # 指定一个容器启动时要运行的命令！和CMD一样
ONBUILD # 当构建一个被继承的DockerFile时运行命令，父镜像在被子镜像继承后，父镜像的
ONBUILD被触发

-------------------------------------------------------Dockerfile文件示例--------------------------------------------------------------
FROM openjdk:8-jre-alpine
ENV TZ=Asia/Shanghai
RUN apk --no-cache add curl
RUN apk --no-cache add vim
RUN apk update && apk upgrade && apk add ca-certificates && update-ca-certificates \
    && apk add --update tzdata && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone \
    && rm -rf /var/cache/apk/*
WORKDIR /data
RUN curl -O https://arthas.aliyun.com/arthas-boot.jar
COPY afterloan-business.jar .
CMD java -Xms1G -Xmx1G -Djava.security.egd=file:/dev/./urandom -Duser.timezone=Asia/shanghai -jar afterloan-business.jar
----------------------------------------------------------------------------------------------------------------------------------------

-v /data/postgres/data/:/var/lib/postgresql/data #:后面的是要挂载的目标地址

docker run -p 3306:3306 -v /data/mysql/conf:/etc/mysql/conf.d -v /data/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=senscloud123 --name mysql -d mysql:8.0.30

docker network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 mynet 创建指定的网络

docker network ls //查看网络列表

docker volume create mongo-data //创建一个数据卷

docker volume ls //查看数据卷列表

docker build -f myJavaDockerFile -t myjava:0.1 .
docker run -p 8080:8888 --name senscloud -d --net mynet myjava:0.1  指定网络要启动的镜像放最后面	

docker run -p 80:80 --name nginx -v nginx:/etc/nginx:rw -v /data/dist:/usr/share/nginx/html -it nginx

docker run -i -t debian /bin/bash

docker exec -it c8530dbbe3b4 /bin/bash
docker exec -it c8530dbbe3b4 bash  进入容器
按下 Ctrl + P + Q 让容器后台运行

echo "Starting backup out ...";docker exec -i postgres bash <<EOF ./var/lib/postgresql/data/backup/backup.sh;
\\q EOF echo "Finish backup out ..."

docker inspect a233a54ea4db 查看容器详细
docker logs -f -t 
docker logs --tail 100 <container_name_or_id> 要查看Docker容器的最新日志信息
docker logs --tail 100 -f <container_name_or_id>

docker save -o redis.tar redis:5.0.2 image1 image2 image3...Docker打包镜像
docker load -i 压缩包名+后缀    解压加载镜像

docker 私有仓库搭建 参考资料：https://blog.csdn.net/Canger_/article/details/125490983
docker tag ubuntu:latest 127.0.0.1:5000/ubuntu:latest
docker tag [镜像id] [新镜像名称]:[新镜像标签] 重命名镜像
docker push 127.0.0.1:5000/ubuntu:latest
curl 127.0.0.1:5000/v2/_catalog
curl 127.0.0.1:5000/v2/cis/tags/list

阿里云CentOs 最小化安装，出现一些命令无法自动补全的情况
#安装bash-completion
yum install -y bash-completion // apt-get install -y bash-completion
#刷新文件
source /usr/share/bash-completion/completions/docker
source /usr/share/bash-completion/bash_completion

WORKDIR /app

cat /etc/group|grep docker 
sudo groupadd docker 
sudo gpasswd -a root docker 

编辑 /etc/docker/daemon.json,增加如下配置
{"insecure-registries":["仓库ip:port"]}
systemctl restart docker.service  

docker-ce docker-ce-cli
./nginx -s reload

docker update --restart=always 容器名或容器ID

Linux中启动Docker容器报错：Error response from daemon: driver failed programming external connectivity
解决方法：输入指令  systemctl restart docker     重启docker服务及可重新生成自定义链DOCKER

docker update --restart=always 容器名或容器ID

Docker容器没有vi/vim
apt-get update
apt-get install vim

删除容器
docker rm -f 
删除镜像
docker rmi -f

docker ps [OPTIONS] 其他参数

-a :显示所有的容器，包括未运行的。
-f :根据条件过滤显示的内容。//docker ps -f 'name=nginx'
–format :指定返回值的模板文件。
-l :显示最近创建的容器。
-n :列出最近创建的n个容器。
--no-trunc :不截断输出。
-q :静默模式，只显示容器编号。
-s :显示总的文件大小。

sudo yum install python3 python3-pip
pip3 install runlike
使用 runlike 生成 docker run 命令：
runlike <container_id_or_name>

如何在docker容器中安装yum等软件
//先更新一波
apt update
//vim
apt install vim
//wget
apt install wget
//yum
apt install yum
//ifconfig
apt install net-tools
//ping
apt install iputils-ping


查看docker-compose.yml所在位置
locate docker-compose.yml