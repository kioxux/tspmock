package com.kioxu.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface CherryAnnotation {
    String name();
    int age() default 18;
    int[] score();
}
