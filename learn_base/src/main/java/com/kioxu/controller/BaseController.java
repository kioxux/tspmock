package com.kioxu.controller;

import com.kioxu.common.data.JsonResult;
import com.kioxu.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/base")
public class BaseController {
    @Autowired
    private BaseService baseService;

    @PostMapping("learn")
    public JsonResult baseLearn() {
        return baseService.baseLearn();
    }
}
