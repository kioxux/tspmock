package com.kioxu.common.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class JsonResult<T> implements Serializable {
    private static final String ERROR_MESSAGE = "服务器出现了一些小状况，正在修复中";
    private static final long serialVersionUID = -2370241787217704182L;
    private Integer code;
    private Object data;
    private String message;

    public JsonResult(Integer code, Object data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public static JsonResult success(Object data, String desc) {
        return new JsonResult(0, data, desc);
    }

    public static JsonResult success(Integer code, Object data, String desc) {
        return new JsonResult(code, data, desc);
    }

    public static JsonResult fail(Integer code, String desc) {
        return new JsonResult(code, null, desc);
    }

    public static JsonResult process(Integer code, Object data, String desc) {
        return new JsonResult(code, data, desc);
    }

    public static JsonResult error() {
        return new JsonResult(500, null, ERROR_MESSAGE);
    }
}
