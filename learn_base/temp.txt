ALTER TABLE `material`.`material_tag` 
ADD INDEX `index_material_id`(`material_id`) USING BTREE;


---------------------------------------------Jenkins 构建脚本 material-service ---------------------------------------------------
==构建环境
勾选上 1. Add timestamps to the Console Output
	   2. Color ANSI Console Output 并在ANSI color map下选择xterm
==Build
Root POM上填写pom.xml
Goals and options上填写 clean install -am -DskipTests=true 
含义：综合来说，clean install -am -DskipTests=true 是一个Maven构建命令，用于清理先前的构建文件，构建项目并将构建产物安装到本地仓库中，同时跳过测试阶段。-am参数用于确保构建包括当前模块以及其依赖的模块。	   
==执行 shell
#!/bin/bash
source /etc/profile

##set color##
echoRed() { echo $'\e[0;31m'"$1"$'\e[0m'; }
echoGreen() { echo $'\e[0;32m'"$1"$'\e[0m'; }
echoYellow() { echo $'\e[0;33m'"$1"$'\e[0m'; }
##set color##

# 设置变量
project="material-service"
version="dev-"`date +%Y%m%d-%H%M%S`
harbor_user="admin"
harbor_pwd="Harbor12345"
harbor_host="10.7.0.62:3380"
echo ----------------------------------------------

# 进入target目录
cd  $WORKSPACE/target

# 创建docker镜像
mv ../docker/Dockerfile ./

# 编译镜像
echoGreen "开始构建本次镜像！"
docker build -t $harbor_host/others/$project:$version .
[ $? != 0 ] && echoRed "请注意，打镜像时出错，故而退出！" && exit 1

# 登录远程Harbor仓库
echoGreen "开始登录远程Harbor仓库！"
docker login -u $harbor_user -p $harbor_pwd $harbor_host
[ $? != 0 ] && echoRed "请注意，登录Harbor时出错，故而退出！" && exit 1

# 上传到docker私服
echoGreen "开始push新镜像到私服！"
docker push $harbor_host/others/$project:$version
[ $? != 0 ] && echoRed "请注意，在执行push上传时出错，故而退出！" && exit 1
docker rmi $harbor_host/others/$project:$version

# 更新镜像
echoGreen "开始将新镜像部署到 K8S！"
rancher kubectl set image deployment/$project $project=10.7.0.62:3380/others/$project:$version -n default
[ $? != 0 ] && echoRed "请注意，在执行镜像更新时出错，故而退出！" && exit 1
echoGreen "部署完成！"


---------------------------------------------Jenkins 构建脚本 cschool-admin ---------------------------------------------------
==构建环境
勾选上 1. Add timestamps to the Console Output
	   2. Color ANSI Console Output 并在ANSI color map下选择xterm
==Build
Root POM上填写pom.xml
Goals and options上填写 clean install -pl cschool-admin -am -DskipTests=true
含义：综合来说，该命令的作用是清理项目并构建、安装项目中名为cschool-admin的模块，同时构建所有依赖cschool-admin模块的其他模块，并跳过单元测试。
==执行 shell
#!/bin/bash
source /etc/profile

##set color##
echoRed() { echo $'\e[0;31m'"$1"$'\e[0m'; }
echoGreen() { echo $'\e[0;32m'"$1"$'\e[0m'; }
echoYellow() { echo $'\e[0;33m'"$1"$'\e[0m'; }
##set color##

# 设置变量
project="cschool-admin"
version="dev-"`date +%Y%m%d-%H%M%S`
harbor_user="admin"
harbor_pwd="Harbor12345"
harbor_host="10.7.0.62:3380"
echo ----------------------------------------------

# 进入target目录
cd  $WORKSPACE/cschool-admin/target

# 创建docker镜像
mv ../docker/Dockerfile ./

# 编译镜像
echoGreen "开始构建本次镜像！"
docker build -t $harbor_host/others/$project:$version .
[ $? != 0 ] && echoRed "请注意，打镜像时出错，故而退出！" && exit 1

# 登录远程Harbor仓库
echoGreen "开始登录远程Harbor仓库！"
docker login -u $harbor_user -p $harbor_pwd $harbor_host
[ $? != 0 ] && echoRed "请注意，登录Harbor时出错，故而退出！" && exit 1

# 上传到docker私服
echoGreen "开始push新镜像到私服！"
docker push $harbor_host/others/$project:$version
[ $? != 0 ] && echoRed "请注意，在执行push上传时出错，故而退出！" && exit 1
docker rmi $harbor_host/others/$project:$version

# 更新镜像
echoGreen "开始将新镜像部署到 K8S！"
rancher kubectl set image deployment/$project $project=10.7.0.62:3380/others/$project:$version -n default
[ $? != 0 ] && echoRed "请注意，在执行镜像更新时出错，故而退出！" && exit 1
echoGreen "部署完成！"



 
---------------------------------------------------------------------------------------------------

# 日志配置
logging:
  level:
    com.cpower: info
    com.cpower.cschool.mapper: debug
    org.springframework: info
# token配置
token:
  # 令牌自定义标识
  header: Authorization
  # 令牌密钥
  secret: abcdefghijklmnopqrstuvwxyz
  # 令牌有效期（默认30分钟）
  expireTime: 30

# MyBatis配置
mybatis:
  # 搜索指定包别名
  typeAliasesPackage: com.cpower.**.domain
  # 配置mapper的扫描，找到所有的mapper.xml映射文件
  mapperLocations: classpath*:mapper/**/*Mapper.xml
  # 加载全局的配置文件
  configLocation: classpath:mybatis/mybatis-config.xml
  configuration:
    log-impl: org.apache.ibatis.logging.slf4j.Slf4jImpl
# PageHelper分页插件
pagehelper:
  helperDialect: mysql
  supportMethodsArguments: true
  params: count=countSql
# 防止XSS攻击
xss:
  # 过滤开关
  enabled: true
  # 排除链接（多个用逗号分隔）
  excludes: /system/notice
  # 匹配链接
  urlPatterns: /system/*,/monitor/*,/tool/*,/dev-api/*	



-----------------------------------------------------------蓝海接入分析-----------------------------------------------------------  
汽车金融
【授信流程】
影像文件上传>>授信申请>>授信结果通知 / 授信结果查证>>额度信息查询（可选）
【合同签订流程】
影像文件上传>>合同签订申请
【放款流程】
影像文件上传>>支用申请试算（可选）>>支用申请>>支用结果通知 / 支用结果查证>>还款计划查询
【还款流程】
还款试算>>还款登记>>代偿申请>>还款结果通知 / 还款结果查证>>还款计划查询（可选）
【贷后补件流程】
影像文件上传>>贷后补件申请>>贷后补件结果通知 / 贷后补件结果查证


INSERT INTO `base`.`system_dictionary`(`type_code`, `subtype_code`, `type_name`, `subtype_name`, `status`, `sort_no`) VALUES ('loanChannel', 'AIBANK_CM', '放款渠道', '百信银行（车抵贷）', 1, 1);
INSERT INTO `base`.`system_dictionary`(`type_code`, `subtype_code`, `type_name`, `subtype_name`, `status`, `sort_no`) VALUES ('loanChannel', 'BOBANK', '放款渠道', '蓝海银行', 1, 2);
INSERT INTO `base`.`system_dictionary`(`type_code`, `subtype_code`, `type_name`, `subtype_name`, `status`, `sort_no`) VALUES ('loanChannel', 'SYJZ_CM', '放款渠道', '苏银金租（车抵贷）', 1, 3);
INSERT INTO `base`.`system_dictionary`(`type_code`, `subtype_code`, `type_name`, `subtype_name`, `status`, `sort_no`) VALUES ('loanChannel', 'WXZL', '放款渠道', '皖新租赁（车抵贷）', 1, 4);


file_subtype_code=100106 婚姻证明材料 不必传
<#if basic.maritalStatus?default("") == '214501' || basic.maritalStatus?default("") == '214506' || basic.maritalStatus?default("") == '214503' || basic.maritalStatus?default("") == '214504'>1</#if>

file_subtype_code=100612 商业险保单/交强险保单 在请款阶段 Aging=2

file_subtype_code=100204 营业执照（贷款金额大于30万需上传） 在申请阶段必传 Aging=0

#百度
baidu:
  api_key: 5nNZGYScZcmds2KT69ZGOG6j
  secret_key: pYe7TTZuEy9hBKM0LET9edZ32YC6h5de
  token_url: https://aip.baidubce.com/oauth/2.0/token
  ocr:
    id_ocr_url: https://aip.baidubce.com/rest/2.0/ocr/v1/idcard
    vehicle_ocr_url: https://aip.baidubce.com/rest/2.0/ocr/v1/vehicle_license
	
baiduai:
  app_id: '32246529'
  api_key: 'HBE664Qu8hQIPT4QR9QR8EnK'
  secret_key: 'FdwgPO37VHSkdplnaQhFdUxwliBQSkGN'	
  
Path filePath = Paths.get("D:\\360MoveData\\Users\\Admin\\Desktop\\file.txt");
            String url = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);
            HashMap map1 = new HashMap();
            map1.put("code", "0000");
            map1.put("message", "查询成功");
            map1.put("data", new HashMap<String, Object>(){{put("CONTRACT_INFOS", Arrays.asList(new HashMap<String, String>(){{put("CONTRACT_URL", url);}}));}});
			String responseData = JSON.toJSONString(map1);  
			

public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException {
        File data = new File("D:\\360MoveData\\Users\\Admin\\Desktop\\nm.xlsx");
        File data2 = new File("D:\\360MoveData\\Users\\Admin\\Desktop\\nn.xlsx");
        List<TclientApplyFile> file = ExcelReaderUtil.readExcel2Bean(new FileInputStream(data), TclientApplyFile.class);
//        Map<String, List<TclientApplyFile>> map = file.stream().collect(Collectors.groupingBy(TclientApplyFile::getAttachmentSubclass));
        List<String> specifiedSubclasses = Arrays.asList(ChanganFileTypeEnum.SUBTYPE20.getSubType(), ChanganFileTypeEnum.SUBTYPE19.getSubType());

        Map<String, List<TclientApplyFile>> map = file.stream().collect(Collectors.groupingBy(f ->specifiedSubclasses.contains(f.getAttachmentSubclass()) ? ChanganFileTypeEnum.SUBTYPE20.getSubType() : f.getAttachmentSubclass()));
        List<TclientProductFileVo> needList = ExcelReaderUtil.readExcel2Bean(new FileInputStream(data2), TclientProductFileVo.class);
        List<ImageCodeEnum> enumList = new ArrayList<>();
        needList.forEach(n -> enumList.addAll(ImageCodeEnum.getBySubType(n.getFileSubtypeCode())));
        enumList.add(ImageCodeEnum.VEHICLE_INSPECTION_SHEET);
        for (ImageCodeEnum imageCodeEnum : enumList) {
            String subType = imageCodeEnum.getSubType();
            if (StringUtils.isNotBlank(subType)) {
                if (CollectionUtils.isEmpty(map.get(subType))) {
                    logger.warn(imageCodeEnum.getDesc() + "文件无保存记录");
                    continue;
                }
                List<TclientApplyFile> files = map.get(subType);
                for (int i = 0; i < files.size(); i++) {
                    TclientApplyFile tclientApplyFile = files.get(i);
                    Integer id = tclientApplyFile.getId();
                    String filePath = tclientApplyFile.getFilePath();
                    String fileName;
                    if (StringUtils.equals(subType, ChanganFileTypeEnum.SUBTYPE2.getSubType())) {
                        fileName = getFileName("cs", imageCodeEnum, "1", i, filePath);
                    } else if (StringUtils.equals(imageCodeEnum.getSubType(), ChanganFileTypeEnum.SUBTYPE3.getSubType())) {
                        fileName = getFileName("cs", imageCodeEnum, "2", i, filePath);
                    } else {
                        fileName = getFileName("cs", imageCodeEnum, "", i, filePath);
                    }
                    System.out.println(fileName);
                }
            }
        }
    }
    private static String getFileName(String clientName, ImageCodeEnum imageCodeEnum, String num, int index, String filePath) {
        return StringUtils.joinWith("_", imageCodeEnum.getValue(), clientName, num, (index + 1) + "." + StringUtils.defaultIfBlank(StringUtils.substringAfter(filePath,"."), "jpg"));
    }			
	
<#if car.mortgageSituation?default("") == '305003'>1</#if>
				
https://wx.madadai.com?gzhUserCode=ZHIFU75128865&gzhUserName=CAXS75128865&gzhType=01&gzhSource=ZHIFU&redirectUrl=https://ai.mr-zhi.com/sign/success/index.html

GPS 1 派单接口     2 查询接口    3 订单状态订单详情 

select b.order_number from client.tclient_apply_basic b join client.client_credit cc on b.order_number=cc.credit_code join workflow.task_list tl on cc.id=tl.project_id join operating.app_user au on b.sales_id=au.id join base.user_info u on au.account=u.cellphone where b.apply_status='300120' and b.train=0 and tl.task_code='CLF10010001' and tl.submit_time is null and u.is_enabled=1 and u.org_code in (567015795545079808,581770923258937344,583951625815588864) group by b.order_number	


车务的
@New Journey😀 
测试环境配置：
userAccount：180
token：045117b0e0a11a242b9765e79cbf113f
sign：cd81462754e7ac2c8f457d6df05efeaf
门店名称：智富门店
抵押权人名称：智富门店抵押权人
业务类型：代办抵押、解除抵押			


{
	"msg": null,
	"code": null,
	"data": {
		"orderNo": "GD20240112036019",
		"applyNo": "DD2023112307235032",
		"customerName": "智富",
		"storeName": "智富门店",
		"bankAccountType": "COMPANY",
		"mortgageeName": "智富门店抵押权人",
		"clientName": "郑瑞建",
		"clientPhone": "13970903109",
		"appointTime": null,
		"region": {
			"level1Code": "320000",
			"level1Name": "江苏省",
			"level2Code": "320500",
			"level2Name": "苏州市",
			"level3Code": "320508",
			"level3Name": "姑苏区"
		},
		"detailAddress": null,
		"expressNumber": "1111",
		"dataReturnType": "EXPRESS",
		"receiveName": "郑瑞建",
		"receivePhone": "13970903109",
		"receiveAddress": null,
		"receiveRegion": null,
		"remark": null,
		"ownerName": "郑瑞建",
		"carNo": "苏03S04E",
		"carVin": "LFV3A23H5K3500835",
		"idCard": "420101197508113711",
		"carBrand": "2019款 大众CC 330TSI 魅颜版 国VI",
		"ownerPhone": "13970903109",
		"status": "FINISH",
		"serviceType": "代办抵押",
		"constructionList": [
			{
				"itemName": "登记证书首页",
				"itemType": "ONEPIC",
				"requireFlag": "YES",
				"remark": "",
				"content": "https://tygps-sit.oss-cn-hangzhou.aliyuncs.com/carFormality/workOrder/recoverMaterialsConfirm/25058/f4e5f7b29c184954831d5bfd1a377f00.jpg"
			},
			{
				"itemName": "抵押登记页",
				"itemType": "ONEPIC",
				"requireFlag": "YES",
				"remark": "",
				"content": "https://tygps-sit.oss-cn-hangzhou.aliyuncs.com/carFormality/workOrder/recoverMaterialsConfirm/25058/548fbe0f81d44cd3a5057d4ddd735c1a.jpg"
			},
			{
				"itemName": "首页与登记页一起拍照（折叠）",
				"itemType": "ONEPIC",
				"requireFlag": "YES",
				"remark": "",
				"content": "https://tygps-sit.oss-cn-hangzhou.aliyuncs.com/carFormality/workOrder/recoverMaterialsConfirm/25058/b6a6d1bed58b45be9b454b35d64375d5.png"
			},
			{
				"itemName": "其他照片",
				"itemType": "MULTIPLEPIC",
				"requireFlag": "NO",
				"remark": "其他照片",
				"content": ""
			}
		]
	},
	"success": true
}

select t.* from (select b.*,m.capital,m.review_loan from client.tclient_apply_order b left join base.product_max_info m on m.id = b.product_id where 1=1
							and b.status in ('303701','303704') 
							or (b.examine_status ='30370802' or b.examine_status is null) 
						) t where 1=1
						ORDER BY t.create_time desc;
						
select t.* from (select b.*,m.capital,m.review_loan from client.tclient_apply_order b left join base.product_max_info m on m.id = b.product_id where 1=1
							and b.status in ('303701','303704') 
							or (b.examine_status ='30370802') 
						) t where 1=1
						ORDER BY t.create_time desc;
						

时间复杂度和空间复杂度是衡量算法性能的两个重要指标，它们分别代表了算法执行的时间和占用的空间随输入数据规模增长的变化趋势。

时间复杂度
时间复杂度是指执行算法所需要的计算工作量。它通常以输入数据的规模\(n\)为变量，来表达算法执行时间的增长率。常见的时间复杂度包括：
常数时间 O(1)：无论数据规模如何，算法所需时间都是固定的。
线性时间 O(n)：算法执行时间与输入数据规模成正比。
对数时间 O(\log n)：当输入数据翻倍时，执行时间只增加一个常量。
线性对数时间 O(n\log n)：执行时间与\(n\log n\)成正比。
二次时间 O(n^2)：执行时间与输入数据规模的平方成正比，常见于简单排序算法如冒泡排序。
指数时间 O(2^n)：执行时间随数据规模指数增长，常见于一些递归算法。

空间复杂度
空间复杂度是指执行算法所需要的最大存储空间。它也通常以输入数据的规模\(n\)为变量，来表达算法占用空间的增长率。例如：
常数空间 O(1)：算法执行所需的存储空间大小固定，与输入数据的规模无关。
线性空间 O(n)：算法执行所需的存储空间与输入数据的规模成正比。
如何理解
时间复杂度关注点在于随着输入数据规模的增加，算法执行的时间如何增长。这有助于我们评估算法在处理大量数据时的性能表现。
空间复杂度关注点在于算法执行过程中需要占用多少内存空间，这对于内存资源有限的场景尤为重要。
理解这两个概念，可以帮助我们在设计和选择算法时，权衡算法的执行效率和资源消耗，从而选择最适合当前问题和环境的算法

要确定一个算法的时间复杂度和空间复杂度，通常需要通过分析算法的结构和执行步骤来实现。下面是一些基本的分析方法和思路：
确定时间复杂度
识别最内层语句的执行次数：首先找到算法中执行次数最多的语句，这通常是算法时间复杂度的决定因素。
循环分析：分析循环结构，确定循环的次数。对于嵌套循环，需要乘以每层循环的次数。
顺序结构：顺序结构的时间复杂度是所有语句时间复杂度的和，但总复杂度由最大的那个决定。
条件分支：对于条件分支结构，时间复杂度取决于执行次数最多的分支。
递归算法：对于递归算法，可以通过递归方程来分析时间复杂度，这通常需要一些数学技巧。

确定空间复杂度
变量占用：分析算法中声明的所有变量和数据结构所占用的空间。
递归算法：对于递归算法，需要考虑递归调用栈的最大深度，因为每次递归调用都会消耗一定的栈空间。
动态分配：分析算法中动态分配的数据结构（如动态数组、链表等）的大小，以及这些数据结构的最大可能规模。
辅助空间：考虑算法执行过程中需要的辅助空间，比如排序算法中的临时数组。

/ 一个整数除法操作，结果只保留整数部分。
% 取余操作符，index % n的结果是index除以n的余数

CLASSPATH .;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar
JAVA_HOME D:\tools\java\jdk
D:\tools\java\jdk\bin
D:\tools\java\jdk\jre\bin

%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin

SM2P1
待签名的明文==>{"third_trade_no":"ZJCG20190428202900159104","partner_id":"20170003","trade_code":"queryBindCardRel"}
签名公钥==>MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEnEBtDxqi3zmYkeh0j+rFAWM+XrDPvw/tDJmn6ovmUM3o3Pe0CvfKPG02WmbJ9FO4C5SAjMAJXgx0ENRA0g7Eog==
签名私钥==>MIGIAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBG4wbAIBAQIhAIKwuySJcenaeqxwmiVD+K/4EA8wVx3TpxAsBKhRc+VnoUQDQgAEnEBtDxqi3zmYkeh0j+rFAWM+XrDPvw/tDJmn6ovmUM3o3Pe0CvfKPG02WmbJ9FO4C5SAjMAJXgx0ENRA0g7Eog==

SM2
2024-06-14 15:16:00.792 [main] INFO  c.f.c.i.z.u.client.test.partner.GenerateSM2KeyTest:33 - SM2公钥：MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEdq8FBiQhPdOwPMjj9d8iSlOUZfewtlfWO9sB+r9cUzayWZMULfnlN1Ka/+GermYJ4F779afxolyMbCbhpmkTUw==
2024-06-14 15:16:00.794 [main] INFO  c.f.c.i.z.u.client.test.partner.GenerateSM2KeyTest:34 - SM2私钥：MIGIAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBG4wbAIBAQIhAPjLGb3qou150f/CjDdSebcVIxiVtZgGBiILUcD/Mj0/oUQDQgAEdq8FBiQhPdOwPMjj9d8iSlOUZfewtlfWO9sB+r9cUzayWZMULfnlN1Ka/+GermYJ4F779afxolyMbCbhpmkTUw==


yfph_pub (PKCS#8 format): MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhtX5csXMRL4S0coVB+h1a4lVYOEVzYZu/oQgfShFyFYfguo+41V5Ckq+W41xKPD1GCmfBIvqa920FcpVqTrv/P9RiEu4t3QxOf3XaEoR/8hvZ50BlMAE5xotVCKVcX1p1rqGyMCrtMwqfqCiqsTI0PcRKN+CNArzIuKm4PFXbc3O6O4/FJBc1lMqaRUHwwddPMaFFokak305LAJBb7vdTEcUhgQ8W/MQWNRr4K/I1RmZYr8xYkjbclelnGgTFEITGhVHNY3TiR+We2R5lBKz6aOr1WBGwd9SB/Pq7QAIhmeSk0PkdcJk49QT+LR/nyXNK/f5fBUtzgrJlMrn1mgZawIDAQAB
yfph_pri (PKCS#8 format): MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCG1flyxcxEvhLRyhUH6HVriVVg4RXNhm7+hCB9KEXIVh+C6j7jVXkKSr5bjXEo8PUYKZ8Ei+pr3bQVylWpOu/8/1GIS7i3dDE5/ddoShH/yG9nnQGUwATnGi1UIpVxfWnWuobIwKu0zCp+oKKqxMjQ9xEo34I0CvMi4qbg8Vdtzc7o7j8UkFzWUyppFQfDB108xoUWiRqTfTksAkFvu91MRxSGBDxb8xBY1Gvgr8jVGZlivzFiSNtyV6WcaBMUQhMaFUc1jdOJH5Z7ZHmUErPpo6vVYEbB31IH8+rtAAiGZ5KTQ+R1wmTj1BP4tH+fJc0r9/l8FS3OCsmUyufWaBlrAgMBAAECggEAXrqXvQ2//A+5sxARhHPJgtqCVETuh21KL5/uH2Aa3qu7CcO+mQKkvhlS8YlGlifrWaVsJcqS0GN7MM/tLK7OimVqB6KDTMfiCccP/iID0TslIZtJ0BKznxViATkm2wvsPsLfDM1oFOZXeI0sm2MTXtQlhxy4Ig4LUVgBPAnyjZ49PM6/uyQelpAh7Q2pL2/3Zrh7t+92ZfwxCYAEkxMD2Tva8IA2TeLCJqQwZeep2bN2ApxWM+jexU41TK9dERSugfDEkL3LVphyQTgMy9CsxWst5NSQv7+SByVkgORt7srqGuIaHv+bNogDy9Js4mzFfRW3jUQFeXHHSixBTtspwQKBgQDpZ+qnyi0ysJPWClXi93ubeJMZKcuBs09NVdFOnf4KMbSHTxTPCzXX/lkYh9be3XH2AFQa2uBPwnruVtF87B3Pe7o3HUEnneI1waaiA/xYYYOJitr5KhpleCOrnvX2jWeeI+QJqhw5Zc4rowO/2S51dllOrOZhqL8qOcpUHgpcCwKBgQCT417waanOYvs/R6h/FtsFfTvA7Izx9LK44X0l7psY4AbwRRnyEN2DNK1h2NmJu4hZGLwo4ZYAJtdWBDzVIIrh1L9zp8YYrSFF0/pErGV2VgkbsPj/kqnR+a9eBbnhBsz25jaQ4UpLgZBETzp7yrGxVnhTzPd7dMH/LViM8SA0IQKBgQDZzLmAVz3z9igN7FkW0T+L5WjGa2TWo98VWEZXaUDcfGNsfBP73AcjhdnX9TJBhzzHhKj24lW8t8sEywjy3FY4/cjXkznwYPjslscKmlOImX4oA+Wv6mT1WLVYeZ4lsvAE9pLdndp2iRRpTdW62UvsGkLeEqNscA3qtkCiW+Ho3wKBgB5L7D0wSFtBp9sU4Q34LrJ3L3CchVtkF+EMQMkukpOMAZ/jaCdRETA58HMlK5ut0Yb/LDzrygEJ8/9pKtU2r30ISph0/H49qZ1WieTbtIWDpqYHoVsqPqQHlMecQLI65G6IRxFzNfc1+cokuQR3R+PepRP135VpG/0c2xE0rLZBAoGAH8eDi36JN3wNL8wEoTbM/di4g99X3K57qjHofpUTzbqtWceqJX8tDfB1wLCv09wR54D8a5wGcmB8BReZz2zH99vxdN+J01BzMOfFcTDgd+RKA0UMa2xAQ+l4kAbqxHCvqAKrMY8lnOGHi9Sjfe8m2vCRAVKt+owJldmlXcRwIbY=
app_pub (PKCS#8 format): MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwFRwRrYCI8XF76WNWvcAIZnk2q9epeMwHYEAYHeA/lu1ENEONBE8QAKbU29OkCWE20JSZKaQCVpnzovBVFtb1j/R9XEvFFJGlRwvFqaIBi3Hj/SaerEXMed8bLKTsEuNMhvOwpRe1C7h9iiqRdzAqVd+FfsGYlMbZ0KTP1xim9Kg6QDazCru+jHZYlKthWnNmCJFuE9ZYwFMIvz0oYQDuW69ATCfowpDV0tgp0Fnp1hYpp8OjZ+TRdTNTwQqUJ9zTcPUWtPHDfcj4nBWcBMiSvN9y1eJwuUcNLW7nzHDJLdo+/HUmX8AMUw2Vxmm/XicAHfHZ0NhaBHEsxVKEWMoMwIDAQAB
app_pri (PKCS#8 format): MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDAVHBGtgIjxcXvpY1a9wAhmeTar16l4zAdgQBgd4D+W7UQ0Q40ETxAAptTb06QJYTbQlJkppAJWmfOi8FUW1vWP9H1cS8UUkaVHC8WpogGLceP9Jp6sRcx53xsspOwS40yG87ClF7ULuH2KKpF3MCpV34V+wZiUxtnQpM/XGKb0qDpANrMKu76MdliUq2Fac2YIkW4T1ljAUwi/PShhAO5br0BMJ+jCkNXS2CnQWenWFimnw6Nn5NF1M1PBCpQn3NNw9Ra08cN9yPicFZwEyJK833LV4nC5Rw0tbufMcMkt2j78dSZfwAxTDZXGab9eJwAd8dnQ2FoEcSzFUoRYygzAgMBAAECggEBALTNbDL9HSvV20p7HlR6SLRNwNdX7ykRVwUVZW5KSYhUW/XwIWMPJM2j5bLcu0VFEA0y3tPhxRwV0epYdSlBLSdPHaiXe0OmSNYaBaZMbSkp1iXdM2/NlNmEHPLuOWqMy7Jkc811v2PGowNOnStJ6BAJwO0W65x5NhSvgE8bIVQLXedNNUNr/Y4jEk+EiTlGNSnIi0sHCqz6JU6Ja+KnFkuu03Y7P9Lmclsmw9/Zj/h6sZ1JEIPEGkjV8bpZeYkVjfOrRqHU3km/ZP9ibNcTRl0V3XscpQEsqmF4O/2WugwRFFDybCv1ScLfAFRNMwCgw6DOtjOWrv7+O/jY13nzarkCgYEA8zrb85g7UjqKZnY9l/SjDbiNyADQyR0fwmTiwM2HaYNsR/zmr/HWS+ll+ZyQF/QGz9Nqsrr8gBcBJl6vikqq6j1Zq9EKlxRaKynoj8twrvzOD51mdmvplu5vgT3YioQfpNCBVoG8j4XNNv9Da/i5NznS54YSzhSyYgEkBeDmkpUCgYEAym10eCwdYeBwwo4Lu1w7SiSf6uwee7Z42H50HJmY8pKzMPIHGm2d77xGnTpH3mhiZFJDw1J4ZoMl5gW6rI2aQSHDCJFAMnHiODr6ZsE43TAAz+TkYXzyNNdNUKHeOTn9vyZppOwC45Yjn3jjubmTglaNC1E3JYqfs099MVAiJacCgYEA51H7DViiUjnKlletjG7lA4+Zh7T+28YPBlncLXFKb1bGDFDBNGPTYq6+hP4ZLC6wq8Ztmz2qFkfKmCtE10jb85oVRgqw/64jcxmQS/E3YbScZM1VPRq0NQ7ejRO8dT/iuGR0HUmVgwnFeaCbBnyt6Y/cvMQN7mBQplRfZhfWMWECgYAxtEaQ62QbkFyfqPlAfBkiz3Nk361sexWZMxOXoi9oAJV3QzZzSs8o+nXsTU8wUTk3oOt6nllp8nHv1SBNFkf2DjAxfljhCg2d+P1bFb68sPjCE1Xwihl+2A7n6ZcDPjYMJYZzNHBRKaB9m9Vu2R5bNp1TVhHQagVkpfP7k82OqQKBgQClOEVNHq6IOfEIbxJybD4CRHs8uZWymguL2ESinUU2v6R+DCXWHaucHE6/4a5A0XNll+/dX2a1oItiz07ssUZftL5boUW1I7UVKth8D5TXWSRKOuHEXFKIUVTrfOGByQM0ng2evncLHsdAQ/Ezx0VJjXV5lKh3eD1v9z8wFRRTbg==


[Unit]
Description=The nginx HTTP and reverse proxy server
After=network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/data/nginx/logs/nginx.pid
ExecStartPre=/usr/bin/rm -f /data/nginx/logs/nginx.pid
ExecStartPre=/data/nginx/sbin/nginx -t
ExecStart=/data/nginx/sbin/nginx
ExecReload=/data/nginx/sbin/nginx -s reload
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target


ownership,releaseDate,registrationAuthority,priorMortgageNum2y
补充材料的，加了，在车辆借款订单查看列表里，有个补充材料按钮，点进去，上传你要补充的材料，再点一下保存按钮

<#if basic.guarCustType?default("") != '306704'>1</#if>

"header": {"orderNo": "1727166812813","assetCode": "zb001","opCode": "sk001","timestamp": "2024-09-26 10:50:38 964"},
	
生产环境 产品方案编号：8af7956c-dc91-4a10-ab31-f4ac2db1fb11

http://ouya.mr-zhi.com:81/service/index.html/#/
http://service.mr-zhi.com/service/index.html/#/


@中犇 陈烨强 @徐鑫 
【河北金租系统1月23日22点投产内容提前通知】
  各位领导好：
    因我司风控要求，针对车主业务，对进件字段、进件资料、请款资料进行了调整，我司计划于2025年1月23日晚上正式上线，届时接口将有必传校验，请各位领导根据内部安排进行协调。
    更新内容：
    一、进件接口、进件重提接口新增必传：
   1.行驶里程（万公里）--用于自动化审批，不传输接口不影响进件
   2.上牌时间--用于自动化审批，不传输接口不影响进件
   3.驾照到期日--用于自动化审批，不传输接口不影响进件
   4.客户月收入（元）、燃料种类、单位名称、单位省、单位市、单位县、单位详细地址
   5.担保人与承租人关系(有担保人时需推送)
    二、乘用车（车主）进件、请款资料清单调整

具体请见接口文档：修订记录、乘用车（车主）进件、请款资料清单。