package com.gys.business.controller.app;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.gys.business.controller.app.form.OutStockForm;
import com.gys.business.controller.app.form.PushForm;
import com.gys.business.controller.app.vo.DcPushVO;
import com.gys.business.controller.app.vo.ProductInfoVO;
import com.gys.business.mapper.GaiaProductBusinessMapper;
import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.business.service.CommonService;
import com.gys.business.service.CompanyOutOfStockService;
import com.gys.business.service.appservice.MemberService;
import com.gys.business.service.data.*;
import com.gys.common.enums.SerialCodeTypeEnum;
import com.gys.util.CommonUtil;
import com.gys.util.DateUtil;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

//@ContextConfiguration(locations = { "classpath:spring*.xml", "classpath:struts.xml", "classpath:spring-hibernate.xml" })
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MemberControllerTest {
    @Autowired
    private MemberService memberService;
    @Test
    void queryMember() {

        GetQueryMemberInData inData = new GetQueryMemberInData();
        inData.setClientId("10000005");
        inData.setBrId("10000");
        inData.setPhoneOrCardId("18816290140");
        GetQueryMemberOutData aa = this.memberService.queryMember(inData);
        System.out.println(aa);
    }

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Test
    public void getProList() {
        String client = "10000005";
        Map<String, Integer> proMap = new HashMap<>();
        proMap.put("10140782", 1);
        List<GaiaProductBusiness> list = gaiaProductBusinessMapper.getListByProCode(client, proMap);
        System.out.println(list.size());
    }

    @Test
    public void getMonthData() {
        OutStockForm form = new OutStockForm();
        form.setDcCodeList(Lists.newArrayList("105"));
        form.setClient("10000005");
        form.setStartDate(DateUtil.formatDate(new Date()));
        form.setEndDate(DateUtil.formatDate(DateUtils.addDays(new Date(), -90)));
        List<ProductInfoVO> outOfStockList = gaiaProductBusinessMapper.getOutOfStockList(form);
        System.out.println(outOfStockList.size());
    }

    @Test
    public void getCompanyData() {
        OutOfStockInData inData = new OutOfStockInData();
        inData.setClientId("10000005");
        inData.setQueryDate(CommonUtil.getyyyyMMdd());
        List<ProductRank> rankList = new ArrayList<>();
        ProductRank productRank = new ProductRank();
        productRank.setProSelfCode("0101001");
        rankList.add(productRank);
        inData.setRankList(rankList);
        CompanyOutOfStockInfoOutData info = gaiaProductBusinessMapper.getCompanyDcOutOfStockInfo(inData);
        System.out.println(JSON.toJSONString(info));

        List<CompanyOutOfStockDetailOutData> list = gaiaProductBusinessMapper.getCompanyDcOutOfStockList(inData);
        System.out.println(JSON.toJSONString(list));
    }

    @Resource
    private CompanyOutOfStockService companyOutOfStockService;

    @Test
    public void push() {
        PushForm pushForm = new PushForm();
        pushForm.setClient("10000005");
        pushForm.setUserId("1106");
        pushForm.setProSelfCodeList(Lists.newArrayList("01000000001", "01000000002", "01000000002"));
        pushForm.setStartDate(DateUtil.formatDate(DateUtils.addDays(new Date(), -1)));
        pushForm.setEndDate(DateUtil.formatDate(new Date()));
        DcPushVO push = companyOutOfStockService.push(pushForm);
        System.out.println(JSON.toJSONString(push));
    }

    @Resource
    private CommonService commonService;

    @Test
    public void testSerialCode() {
        String client = "10000013";
        String storeId = "10001";
        for (int i = 0; i < 100; i++) {
            System.out.println(commonService.getSerialCode(client, SerialCodeTypeEnum.INTEGRAL_EXCHANGE));
            System.out.println(commonService.getSdSerialCode(client, storeId, SerialCodeTypeEnum.INTEGRAL_EXCHANGE));
        }
    }
}