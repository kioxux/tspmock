package com.test;


import com.gys.GysOperationApplication;
import com.gys.business.controller.ZjnbYbController;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ZjnbYbService;
import com.gys.business.service.data.*;
import com.gys.business.service.impl.*;
import com.gys.common.kylin.TestData;
import com.gys.feign.StoreService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SalesSummaryServiceImplTest {


    @Autowired
    private SalesSummaryServiceImpl salesSummaryService;

    @Autowired
    private PointsDetailsQueryServiceImpl detailsQueryService;

    @Autowired
    private ReceiptServiceImpl receiptService;

    @Autowired
    private SalesReceiptsImpl receipts;

    @Autowired
    private Al_Pl_AnalyseImpl analyse;

    @Autowired
    private ZjnbYbService zjnbYbService;
    @Autowired
    private StoreService storeService;



    @Test
    public void testOne() {
//        List<GaiaDataSynchronized> all = this.service.updateContentAll("1610711714734");
//        for (GaiaDataSynchronized aSynchronized : all) {
//            System.out.println(aSynchronized);
//        }
    }


//    @Test
//    public void afterSplitePrice() {
//        GaiaRetailPriceInData inData = new GaiaRetailPriceInData();
//        inData.setClient("21020006");
//        inData.setBrId("1000000001");
//        inData.setGsppProId("0205033");
//
//        String price = receipts.afterSplitePrice(inData);
//        System.out.println(price);
//
//    }
//
//
//    @Test
//    public void findReceipt() {
//        ReceiptInData inData = new ReceiptInData();
//        inData.setSaleBillNo("1909010010020000001");
//        inData.setClient("21020006");
//        inData.setBrId("1000000001");
//        ReceiptOutData outData = receiptService.findReceipt(inData);
//        log.info("findReceipt{}", outData);
//        List<ReceiptPayOutData> receipt = receiptService.findTypeByOrderNumber(inData);
//        log.info("findReceipt{}", receipt);
//    }
//
//
//    @Test
//    public void findSalesSummary() {
//        SalesSummaryData salesSummaryData = new SalesSummaryData();
//        salesSummaryData.setStartDate("20200401");
//        salesSummaryData.setEndDate("20200401");
//        salesSummaryData.setClient("21020001");
//        salesSummaryData.setStoreCode("1000001001");
//
//        List<GaiaSalesSummary> salesSummary = salesSummaryService.findSalesSummary(salesSummaryData);
//        System.out.println(salesSummary);
//    }
//
//    @Test
//    public void findUserByClientAndDepId() {
//        List<GaiaUser> userByClientAndDepId = salesSummaryService.findUserByClientAndDepId("21020001", "1000001001");
//        System.out.println(userByClientAndDepId);
//
//    }
//
//    @Test
//    public void findCategoriesByClientAndDepId() {
//        List<GaiaCategories> categoriesByClientAndDepId = salesSummaryService.findCategoriesByClientAndDepId("21020001", "1000001001");
//        System.out.println(categoriesByClientAndDepId);
//    }
//
//    @Test
//    public void selectPointsDetailsQueryByNameAndId() {
//        PointsDetailsQueryInData pointsDetailsQueryInData = new PointsDetailsQueryInData();
//        pointsDetailsQueryInData.setClient("21020001");
//        pointsDetailsQueryInData.setBrId("1000001001");
////        pointsDetailsQueryInData.setGsmbCardId("");
////        pointsDetailsQueryInData.setGsmbName("王");
//        pointsDetailsQueryInData.setStartDate("20200715");
//        pointsDetailsQueryInData.setEndDate("20200715");
////        pointsDetailsQueryInData.setPageNum(0);
////        pointsDetailsQueryInData.setPageSize(0);
//
//        List<PointsDetailsQueryOutData> queryOutData = detailsQueryService.selectPointsDetailsQueryByNameAndId(pointsDetailsQueryInData);
//        log.info("selectPointsDetailsQueryByNameAndId{}", queryOutData);
//        System.out.println(queryOutData);
//    }
//
//    @Test
//    public void execSH() {
//        GetWfApproveInData inData = new GetWfApproveInData();
//        inData.setClientId("21020006");
//        inData.setCreateUser("10000047");
//        inData.setWfDefineCode("GAIA_WF_007");
//        inData.setWfOrder("SYS2020000167");
//        inData.setWfStatus("3");
//        String res = storeService.approval(inData);
//        log.info("test2{}", res);
//    }
//
//
//    @Test
//    public void execAlPro() {
//        List<TestData> tdList = analyse.getDistinctIds();
//        log.info("test{}", tdList);
//
//        List<GaiaAlPlQjmd> qjmdList = analyse.getSaleSummaryByArea("JSSZ");
//        log.info("test1{}", qjmdList);
//        HashMap<String, Object> item = new HashMap<>();
//        List<String> compList = new ArrayList<>();
//        compList.add("A0111");
//        compList.add("A0112");
//        compList.add("A0113");
//        item.put("aplArea", "JSSZ");
//        item.put("compList", compList);
//        qjmdList = analyse.getSaleInfoByComp(item);
//        log.info("test2{}", qjmdList);
//        //todo 获取上次抓取数据的行号。数据模型数据。
//
//
//        //todo 检索地区销量前100的成分
//        //todo 检索成分价格带，取每个价格带，销量高于平价数的商品。
//        //todo 循环每个成分，每个价格带，对应商品的状态。
//        //todo      无销售，有库存，提高关联推荐率，提示增加促销活动。
//        //todo      无销售，无库存，建议补货
//        //todo      有销售，无库存，建议补货
//        //todo      有销售，有库存，库存量比较，是否满足建议补货数量。
//        //todo 插入品类分析结论表。
//
//    }


}
//
//
//权重参数：
//        成分：地区，日期，xx，xx
//        商品：
//        数据采样参数：
//        区域采样数量：
//        门店采样数量：
//
//        从上次记录采样结束点{Y}开始，依据成分重要登记，反向排序，采样{X}。循环，主补门店缺品：
//        第N个成分，高于平均销量价格带，对应门店数据：
//
//
//
//        门店主打品种，采用自动补货的推荐即可达成。
