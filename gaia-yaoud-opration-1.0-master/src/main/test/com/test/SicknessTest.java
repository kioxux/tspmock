package com.test;

import com.gys.GysOperationApplication;
import com.gys.business.mapper.SicknessMapper;
import com.gys.util.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SicknessTest {

    @Resource
    private SicknessMapper sicknessMapper;

    @Test
    public void sicknessTest() {

        String s = null;
        try {
            s = ExcelUtils.readExcel("C:\\Users\\86176\\Desktop\\疾病分类（二级）V20210830.xlsx", null);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> excellList = Arrays.stream(s.split("\r\n")).collect(Collectors.toList());
        List<Map<String,String>> importList=new ArrayList<>();
        excellList.forEach(excellItem->{
            List<String> dataList = Arrays.stream(excellItem.split(" ")).collect(Collectors.toList());
            Map<String,String> dataMap=new HashMap<>();
            dataMap.put("SIO_SICKNESS_TAG",dataList.get(0));
            dataMap.put("SIO_SICKNESS_NAME",dataList.get(1));
            dataMap.put("SIO_SICKNESS_CODING",dataList.get(2));
            dataMap.put("SIO_SICKNESS_CODING_NAME",dataList.get(3));
            dataMap.put("SIO_ELEMENT_ID",dataList.get(4));
            dataMap.put("SIO_ELEMENT_NAME",dataList.get(5));
            importList.add(dataMap);
        });
        importList.forEach(importData->{
            saveTag(importData);
            saveCoding(importData);
            saveElement(importData);
            saveTagCoding(importData);
            saveElementCoding(importData);
        });

    }

    public void saveTag(Map<String,String> importData) {
        Integer count=sicknessMapper.findTag(importData);
        if (count == 0) {
        sicknessMapper.saveTag(importData);
        }
    }
    public void saveCoding(Map<String,String> importData) {
        Integer count=sicknessMapper.findCoding(importData);
        if (count == 0) {
        sicknessMapper.saveCoding(importData);
        }
    }
    public void saveElement(Map<String,String> importData) {
        Integer count=sicknessMapper.findElement(importData);
        if (count == 0) {
        sicknessMapper.saveElement(importData);
        }
    }
    public void saveTagCoding(Map<String,String> importData) {
        Integer count=sicknessMapper.findTagCoding(importData);
        if (count == 0) {
        sicknessMapper.saveTagCoding(importData);
        }
    }
    public void saveElementCoding(Map<String,String> importData) {
        Integer count=sicknessMapper.findElementCoding(importData);
        if (count == 0) {
        sicknessMapper.saveElementCoding(importData);
        }
    }
}
