package com.test;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import com.google.common.collect.Maps;
import com.gys.GysOperationApplication;
import com.gys.common.config.RabbitConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RabbitMqTest {
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Test
    public void produceMsgTest(){
        String msgId = UUID.randomUUID().toString();
        String msg = "test11111111111";
        String msgDate = DateUtil.now();

        Map<String, Object> msgMap = Maps.newHashMap();
        msgMap.put("msgId", msgId);
        msgMap.put("msg", msg);
        msgMap.put("msgDate", msgDate);
        rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TEST, RabbitConfig.ROUTING_KEY_TEST, msgMap);
    }
}
