package com.test;

import com.google.common.collect.Lists;
import com.gys.GysOperationApplication;
import com.gys.business.mapper.GaiaSdMessageMapper;
import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.GaiaSdStockMapper;
import com.gys.business.mapper.NewDistributionPlanMapper;
import com.gys.business.mapper.entity.GaiaSdStock;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.service.GaiaSdMessageService;
import com.gys.business.service.StockService;
import com.gys.business.service.data.StockInData;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockServiceTest {
    @Resource
    private StockService stockService;

    @Resource
    private GaiaSdStockMapper stockMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;
    @Resource
    private NewDistributionPlanMapper newDistributionPlanMapper;

    @Test
    public void distributionPlan() {
        NewDistributionPlan distributionPlan = new NewDistributionPlan();
        distributionPlan.setPlanCode("ndp202105310001");
        distributionPlan.setClient("100001");
        distributionPlan.setProSelfCode("10001");
        newDistributionPlanMapper.add(distributionPlan);
    }

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Test
    public void testVoucher() {
        String voucherId = gaiaSdMessageMapper.selectNextVoucherId("10000013", "company");
        System.out.println(voucherId);
    }

    @Test
    public void testThread() {
        System.out.println("run main task");
        threadPoolTaskExecutor.execute(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("run other task");
        });
        System.out.println("main task over");
    }

    @Test
//    @Transactional
    @Rollback
    public void consumeShopStockTest(){
        String proId = "2003735", clientId = "21020006", brId = "1000000001", empId = "0000000", voucherId = "0000000", serial = "0000000";
        BigDecimal consumeNum  = new BigDecimal(10);
        List<String> batchNoList = Lists.newArrayList("2004091", "5555");

        Example example = new Example(GaiaSdStock.class);
        example.createCriteria().andEqualTo("clientId", clientId).andEqualTo("gssBrId", brId).andEqualTo("gssProId", proId);
        GaiaSdStock sdStock = stockMapper.selectOneByExample(example);
        System.out.println("商品{" + proId + "}目前总库存为： " + sdStock.getGssQty());
        Iterator<String> batchNoIt = batchNoList.iterator();
        BigDecimal owingStock = BigDecimal.ZERO;
        while (batchNoIt.hasNext()) {
            String batchNo = batchNoIt.next();
            StockInData stockInData = new StockInData();
            stockInData.setClientId(clientId);
            stockInData.setGssBrId(brId);
            stockInData.setGssProId(proId);
            stockInData.setEmpId(empId);
            stockInData.setBatchNo(batchNo);
            stockInData.setVoucherId(voucherId);
            stockInData.setSerial(serial);
            stockInData.setNum(consumeNum);
            Object[] stockObj = stockService.consumeShopStock(stockInData, owingStock, batchNoIt.hasNext());
            owingStock = (BigDecimal) stockObj[0];

            example = new Example(GaiaSdStockBatch.class);
            example.createCriteria().andEqualTo("clientId", clientId).andEqualTo("gssbBrId", brId).andEqualTo("gssbProId", proId).andEqualTo("gssbBatchNo", batchNo);
            List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(example);
            for(GaiaSdStockBatch stockBatch : stockBatchList){
                System.out.println("批次{" + stockBatch.getGssbBatch() + "}目前库存为： " + stockBatch.getGssbQty());
            }

            consumeNum = new BigDecimal(12);
        }
    }

}
