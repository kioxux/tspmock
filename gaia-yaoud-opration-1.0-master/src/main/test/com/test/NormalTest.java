package com.test;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.junit.Test;
import sun.security.util.Length;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;


public class NormalTest {

    @Test
    public void testBigDecimal() {
//        LocalDate data= LocalDate.now();
//        System.out.println(data);
//        DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("yyyyMMdd");
//
//        String strDate3 = dtf3.format(data);
//        System.out.println(strDate3);
//        String reportDateStr = "2020-09-01";
//        String replace = reportDateStr.replace("-", "");
//        System.out.println(replace);


//    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
//        Class<?> aClass = Class.forName("");
//        aClass.newInstance();

//        DateTime dateTime = new DateTime(DateTime.now().toString(), DatePattern.NORM_DATE_FORMAT);
//        System.out.println(dateTime.toString());
//        String riq = "2020-10-31";
//        LocalDate parse = LocalDate.parse(riq);
//        System.out.println(parse);
////        LocalDate date = LocalDate.now();
//        String ni = "2020-11-01";
//        LocalDate date = LocalDate.parse(ni);
//        System.out.println(date);
//        if (date.isBefore(parse)){
//            System.out.println("小于");
//        }else {
//            System.out.println("大于");
//        }
//
//
//        BigDecimal negate = new BigDecimal("11").negate();
//        System.out.println(negate);
        List<String> list1 = new ArrayList<String>();
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("5");
        list1.add("6");

        List<String> list2 = new ArrayList<String>();
        list2.add("2");
        list2.add("3");
        list2.add("7");
        list2.add("8");

        // 交集
        List<String> intersection = list1.stream().filter(item -> list2.contains(item)).collect(toList());
        System.out.println("---交集 intersection---");
        intersection.parallelStream().forEach(System.out :: println);

        // 差集 (list1 - list2)
        List<String> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(toList());
        System.out.println("---差集 reduce1 (list1 - list2)---");
        reduce1.parallelStream().forEach(System.out :: println);

        // 差集 (list2 - list1)
        List<String> reduce2 = list2.stream().filter(item -> !list1.contains(item)).collect(toList());
        System.out.println("---差集 reduce2 (list2 - list1)---");
        reduce2.parallelStream().forEach(System.out :: println);

        // 并集
        List<String> listAll = list1.parallelStream().collect(toList());
        List<String> listAll2 = list2.parallelStream().collect(toList());
        listAll.addAll(listAll2);
        System.out.println("---并集 listAll---");
        listAll.parallelStream().forEachOrdered(System.out :: println);

        // 去重并集
        List<String> listAllDistinct = listAll.stream().distinct().collect(toList());
        System.out.println("---得到去重并集 listAllDistinct---");
        listAllDistinct.parallelStream().forEachOrdered(System.out :: println);

        System.out.println("---原来的List1---");
        list1.parallelStream().forEachOrdered(System.out :: println);
        System.out.println("---原来的List2---");
        list2.parallelStream().forEachOrdered(System.out :: println);
    }

    @Test
    public void testMap(){
        System.out.println("200G , 200G:" + String.valueOf(isMatch("200G","200G")));
        System.out.println("1*200G , 1*0.2KG:" + String.valueOf(isMatch("1*200G","1*0.2KG")));
        System.out.println("200G*10s , 0.2KG*10s:" + String.valueOf(isMatch("200G*10s","0.2KG*10s")));
        System.out.println("200G*10s , 10s*0.2KG:" + String.valueOf(isMatch("200G*10s","10s*0.2KG")));
        System.out.println("500MG*36s , 0.5g*12s*3p:" + String.valueOf(isMatch("500MG*36片","0.5g*12s*3p")));
        System.out.println("0.5g*12s*3p , 0.5g*6s*6p:" + String.valueOf(isMatch("0.5g*12s*3p","0.5g*6s*6p")));
    }

    private boolean isMatch(String item1 ,String item2){
        boolean result=false;
        item1=item1.toUpperCase();
        item2=item2.toUpperCase();
        if (item1.equals(item2)){
            result=true;
        }
        if (!result){
            item1=computerMathChar(computerMathChar(item1,'M',"0.001"),'K',"1000");
            item2=computerMathChar(computerMathChar(item2,'M',"0.001"),'K',"1000");
            if (item1.equals(item2)){
                result=true;
            }
        }
        if (!result) {
            List<String> item1List= new ArrayList<>(Arrays.asList(item1.split("\\*")));
            List<String> item2List=new ArrayList<>(Arrays.asList(item2.split("\\*")));
            if (item1List.size()==item2List.size()){
                boolean isMatchAll=true;
                for(String it1:item1List){
                    boolean isMatch=item2List.contains(it1);
                    if (!isMatch){
                        isMatchAll=false;
                        break;
                    }
                }
                if (isMatchAll){
                    result=true;
                }
            }else{
                List<String> matchedList=new ArrayList<>();
                for(String it1:item1List){
                    if (item2List.contains(it1)){
                        matchedList.add(it1);
                    }
                }
                for (String matched:matchedList){
                    item1List.remove(matched);
                    item2List.remove(matched);
                }
                if (ExecListVal(item1List).equals(ExecListVal(item2List))){
                    result=true;
                }
            }
        }
        return result;
    }

    private String computerMathChar(String item,char c,String replaceString){
        String result=item;
        int iPos= result.indexOf(c);
        if (iPos>=0){
            String left=result.substring(0,iPos);
            String right=result.substring(iPos+1);
            String comput=getExecString(left);
            left=left.substring(0,left.length()- comput.length() );
            String mix=new BigDecimal(comput).multiply(new BigDecimal(replaceString)).toString();
            mix=rvZeroAndDot(mix);
//            if (mix.endsWith(".0")){
//                mix=mix.substring(0,mix.length()-2);
//            }
            result=left+mix+right;
        }
        return result;
    }

    private char[] itemCharList=new char[]{'1','2','3','4','5','6','7','8','9','0','.'};
    private String getExecString(String item){
        String result=item;
        for(int i=item.length()-1;i>=0;i--){
            boolean isContain=false;
            char a=item.charAt(i);
            for (char con:itemCharList){
               if (a==con){
                   isContain=true;
                   break;
               }
            }
            if (!isContain){
                result=result.substring(i+1);
                break;
            }
        }
        return result;
    }

    private String ExecListVal(List<String> itemList){
        BigDecimal result=new BigDecimal("1");
        for (String item :itemList){
            result=result.multiply(new BigDecimal(TrimNumberic(item)));
        }
        String strResult=rvZeroAndDot(result.toString());
        return strResult;
    }

    private String TrimNumberic(String item){
        String result="";
        for(char a :item.toCharArray()){
            for (char con:itemCharList){
                if (a==con){
                    result=result+a;
                    break;
                }
            }

        }
        if (result.equals("")){
            result="1";
        }
        return result;
    }

    private String rvZeroAndDot(String s){
        if (s.isEmpty()) {
            return null;
        }
        if(s.indexOf(".") > 0){
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }

}
