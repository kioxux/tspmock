package com.test;


import cn.hutool.json.JSONObject;
import com.gys.GysOperationApplication;
import com.gys.business.mapper.GaiaSdSaleDMapper;
import com.gys.business.service.AcceptWareHouseService;
import com.gys.business.service.NewProductDistributionPlanService;
import com.gys.business.service.ReportFormsService;
import com.gys.business.service.data.ReportForms.StoreSaleDateInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.NewProductDistributionNoPlanDto;
import com.gys.util.GuoYaoUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SqlTest {

    @Resource
    private ReportFormsService reportFormsService;
    @Resource
    private GaiaSdSaleDMapper saleDMapper;
    @Resource
    private NewProductDistributionPlanService newProductDistributionPlanService;

    @Resource
    private AcceptWareHouseService acceptWareHouseService;

    @Test
//    @Transactional
    @Rollback
    public void ts1() {
        StoreSaleDateInData inData=new StoreSaleDateInData();
        inData.setStartDate("2021-05-01");
        inData.setEndDate("2021-06-01");
        inData.setDateType("3");
        List<Map<String, Object>> maps = saleDMapper.selectStoreSaleByDate(inData);
        System.out.println(maps);

    }

    @Test
    @Rollback
    public void noPlanTest() {
        NewProductDistributionNoPlanDto dto = new NewProductDistributionNoPlanDto();
        JsonResult jsonResult = newProductDistributionPlanService.noPlan(dto);
        System.out.println(jsonResult.getData());
    }

    @Test
    public void test(){
        ArrayList<JSONObject> jsonObjects = new ArrayList<>();
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.set("billno","10000016-PD2021000374-A1000003");
        jsonObjects.add(jsonObject1);

        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.set("billno","10000016-PD2021000374-A100001");
        jsonObjects.add(jsonObject2);

        JSONObject jsonObject3 = new JSONObject();
        jsonObject3.set("billno","10000016-PD2021000374-A100002");
        jsonObjects.add(jsonObject3);
        List<JSONObject> orderDetail = GuoYaoUtil.getOrderStatus(jsonObjects);
        System.out.println(orderDetail);
    }

    @Test
    public void findGoodsDetails(){
        List<JSONObject> goodsDetail = GuoYaoUtil.findGoodsDetail("2020-04-01", "2020-05-01", "");
        System.out.println(goodsDetail);
    }

    @Test
    public void manage(){
        acceptWareHouseService.manageGuoYaoOrder();
    }
}
