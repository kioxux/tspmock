package com.test;

import com.alibaba.fastjson.JSON;
import com.gys.GysOperationApplication;
import com.gys.business.controller.ZjnbYbController;
import com.gys.business.service.ZjnbYbService;
import com.gys.business.service.data.zjnb.FeesSettleConfirmVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class test {

    @Autowired
    private ZjnbYbService zjnbYbService;

    @Test
    public void ss() {
        String json = "{\n" +
                "    \"cardData1Dto\": {\n" +
                "        \"cardData\": \"92002528T00049999330299\",\n" +
                "        \"cardType\": \"21\"\n" +
                "    },\n" +
                "    \"clinicNum\": \"A8470002110296942107\",\n" +
                "    \"detailBillCode\": \"B8470002110292477417\",\n" +
                "    \"detailSerialCode\": \"C0002021102948730309\",\n" +
                "    \"medicalChargePayDto\": {\n" +
                "        \"cashPayA\": 0.00,\n" +
                "        \"cashPayB\": 0.00,\n" +
                "        \"cashPayC\": 0.00,\n" +
                "        \"cashPayD\": 0.00,\n" +
                "        \"familyAcctPay\": 0.00,\n" +
                "        \"fulAmtOwnPayAmt\": 0.00,\n" +
                "        \"mafPay\": 0.00,\n" +
                "        \"medFeeSumAmt\": 28.10,\n" +
                "        \"othPay\": 0.00,\n" +
                "        \"poolPay\": 0.00,\n" +
                "        \"psnAcctHisPay\": 0.00,\n" +
                "        \"psnAcctPay\": 28.10,\n" +
                "        \"publicFundPay\": 0.00,\n" +
                "        \"salvaPay\": 0.00,\n" +
                "        \"selfPayAmt\": 0.00,\n" +
                "        \"socialSalvaPay\": 0.00\n" +
                "    },\n" +
                "    \"medicalChargeTotalVo\": {\n" +
                "        \"familyAcctAft\": 0.00,\n" +
                "        \"familyAcctBef\": 0.00,\n" +
                "        \"hosStartTotAft\": 0.00,\n" +
                "        \"hosStartTotBef\": 0.00,\n" +
                "        \"hosTopAft\": 0.00,\n" +
                "        \"hosTopBef\": 0.00,\n" +
                "        \"hosTotAft\": 0.00,\n" +
                "        \"hosTotBef\": 0.00,\n" +
                "        \"hosYearTotAft\": 0.00,\n" +
                "        \"hosYearTotBef\": 0.00,\n" +
                "        \"mafPayAft\": 0.00,\n" +
                "        \"mafPayBef\": 0.00,\n" +
                "        \"othTotAft\": 0.00,\n" +
                "        \"othTotBef\": 0.00,\n" +
                "        \"outPayTotAft\": 0.00,\n" +
                "        \"outPayTotBef\": 0.00,\n" +
                "        \"outTopAft\": 0.00,\n" +
                "        \"outTopBef\": 0.00,\n" +
                "        \"outYearTotAft\": 0.00,\n" +
                "        \"outYearTotBef\": 0.00,\n" +
                "        \"psnAcctBalAft\": 4971.90,\n" +
                "        \"psnAcctBalBef\": 5000.00,\n" +
                "        \"psnAcctHisBalAft\": 5000.00,\n" +
                "        \"psnAcctHisBalBef\": 5000.00,\n" +
                "        \"speTopAft\": 0.00,\n" +
                "        \"speTopBef\": 0.00,\n" +
                "        \"speYearTotAft\": 0.00,\n" +
                "        \"speYearTotBef\": 0.00\n" +
                "    },\n" +
                "    \"personalInfoVo\": {\n" +
                "        \"accountInfo\": {\n" +
                "            \"accountStatus\": \"0000\",\n" +
                "            \"dispensingMark\": \"9\",\n" +
                "            \"monitoringTag\": \"9\",\n" +
                "            \"nonLocalSign\": \"0\",\n" +
                "            \"systemRetention\": \"\",\n" +
                "            \"unitInsuranceNo\": \"999997\"\n" +
                "        },\n" +
                "        \"areaCode\": \"01\",\n" +
                "        \"birthday\": \"21101501\",\n" +
                "        \"id\": \"5346T0112\",\n" +
                "        \"name\": \"定点机构（本地）13\",\n" +
                "        \"otherInfo\": {\n" +
                "            \"authorizationID\": \"\",\n" +
                "            \"isCheckPwd\": \"0\",\n" +
                "            \"loadReductionMark\": \"0\",\n" +
                "            \"objectMark\": \"\",\n" +
                "            \"systemRetention\": \"\",\n" +
                "            \"thirdPartyWithholding\": \"\"\n" +
                "        },\n" +
                "        \"personalType\": \"00\",\n" +
                "        \"registerInfo\": {\n" +
                "            \"inpatientNumber\": \"0\",\n" +
                "            \"organization\": \"\",\n" +
                "            \"regTime\": \"\",\n" +
                "            \"visitNumber\": \"\"\n" +
                "        },\n" +
                "        \"sex\": \"0\",\n" +
                "        \"subPersonalType\": \"0020\",\n" +
                "        \"treatmentCategory\": \"000\"\n" +
                "    }}";
        FeesSettleConfirmVo feesSettleConfirmVo = JSON.parseObject(json,FeesSettleConfirmVo.class);
        ZjnbYbController controller = new ZjnbYbController();
        Map<String,Object> map = new HashMap();
        map.put("client","client");
        map.put("stoCode","stoCode");
        map.put("selfPayTotal",13212131);
        map.put("amountPayTotal",56);
        map.put("selfPayBeforeTotal",99);
        map.put("feesSettleConfirmVo",feesSettleConfirmVo);
        map.put("receiptTime","2021101");

        zjnbYbService.insertSaleInfo(map);
    }
}
