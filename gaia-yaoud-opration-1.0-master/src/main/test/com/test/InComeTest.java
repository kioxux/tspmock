package com.test;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.gys.GysOperationApplication;
import com.gys.business.mapper.GaiaSdIncomeStatementHMapper;
import com.gys.business.mapper.GaiaSdPhysicalCountDiffMapper;
import com.gys.business.mapper.GaiaSdPhysicalHeaderMapper;
import com.gys.business.mapper.entity.GaiaSdIncomeStatementH;
import com.gys.business.mapper.entity.GaiaSdPhysicalHeader;
import com.gys.business.service.IncomeStatementService;
import com.gys.business.service.data.GetPhysicalCountInData;
import com.gys.business.service.data.GetWfNewPhysicalWorkflowInData;
import com.gys.business.service.data.PhysicalNewReportOutData;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {GysOperationApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InComeTest {
    @Resource
    private IncomeStatementService incomeStatementService;

    @Resource
    private GaiaSdIncomeStatementHMapper incomeStatementHMapper;

    @Resource
    private GaiaSdPhysicalHeaderMapper physicalHeaderMapper;

    @Resource
    private GaiaSdPhysicalCountDiffMapper physicalCountDiffMapper;

    @Test
//    @Transactional
    @Rollback
    public void generateNewIncomeStatementWorkflowTest(){
        Example example = new Example(GaiaSdIncomeStatementH.class);
        example.createCriteria().andEqualTo("clientId", "10000013").andEqualTo("gsishBrId", "10020");
        GaiaSdIncomeStatementH incomeStatement = incomeStatementHMapper.selectOneByExample(example);

        String pcVoucherId = incomeStatement.getGsishPcVoucherId();
        String clientId = incomeStatement.getClientId();
        String brId = incomeStatement.getGsishBrId();
        String voucherId = incomeStatement.getGsishPcVoucherId();
        GetPhysicalCountInData physicalCountInData = new GetPhysicalCountInData();
        GetWfNewPhysicalWorkflowInData newPhysicalWorkflowInData = new GetWfNewPhysicalWorkflowInData();

        //品项 盘点总数
        BigDecimal itemInventoryTotal = BigDecimal.ZERO;
        //品项 盘盈数
        BigDecimal itemInventoryProfit = BigDecimal.ZERO;
        //品项 盘亏数
        BigDecimal itemInventoryLosses = BigDecimal.ZERO;
        //品项 差异合计(绝对值
        BigDecimal itemAbsoluteDiff = BigDecimal.ZERO;
        //品项 绝对差异率
        BigDecimal itemAbsoluteDiffRatio = BigDecimal.ZERO;
        //品项 差异合计(相对值)
        BigDecimal itemRelativeDiff = BigDecimal.ZERO;
        //品项 相对差异率
        BigDecimal itemRelativeDiffRatio = BigDecimal.ZERO;
        //批号行数 盘点总数
        int batchInventoryTotal = 0;
        //批号行数 盘盈数
        int batchInventoryProfit = 0;
        //批号行数 盘亏数
        int batchInventoryLosses = 0;
        //批号行数 差异合计(绝对值)
        int batchAbsoluteDiff = 0;
        //批号行数 绝对差异率
        BigDecimal batchAbsoluteDiffRatio = BigDecimal.ZERO;
        //批号行数 差异合计(相对值)
        int batchRelativeDiff = 0;
        //批号行数 相对差异率
        BigDecimal batchRelativeDiffRatio = BigDecimal.ZERO;
        //数量 盘点总数
        BigDecimal numInventoryTotal = BigDecimal.ZERO;
        //数量 盘盈数
        BigDecimal numInventoryProfit = BigDecimal.ZERO;
        //数量 盘亏数
        BigDecimal numInventoryLosses = BigDecimal.ZERO;
        //数量 差异合计(绝对值)
        BigDecimal numAbsoluteDiff = BigDecimal.ZERO;
        //数量 绝对差异率
        BigDecimal numAbsoluteDiffRatio = BigDecimal.ZERO;
        //数量 差异合计(相对值)
        BigDecimal numRelativeDiff = BigDecimal.ZERO;
        //数量 相对差异率
        BigDecimal numRelativeDiffRatio = BigDecimal.ZERO;
        //成本额 盘点总数
        BigDecimal costInventoryTotal = BigDecimal.ZERO;
        //成本额 盘盈数
        BigDecimal costInventoryProfit = BigDecimal.ZERO;
        //成本额 盘亏数
        BigDecimal costInventoryLosses = BigDecimal.ZERO;
        //成本额 差异合计(绝对值)
        BigDecimal costAbsoluteDiff = BigDecimal.ZERO;
        //成本额 绝对差异率
        BigDecimal costAbsoluteDiffRatio = BigDecimal.ZERO;
        //成本额 差异合计(相对值)
        BigDecimal costRelativeDiff = BigDecimal.ZERO;
        //成本额 相对差异率
        BigDecimal costRelativeDiffRatio = BigDecimal.ZERO;
        //零售额 盘点总数
        BigDecimal retailInventoryTotal = BigDecimal.ZERO;
        //零售额 盘盈数
        BigDecimal retailInventoryProfit = BigDecimal.ZERO;
        //零售额 盘亏数
        BigDecimal retailInventoryLosses = BigDecimal.ZERO;
        //零售额 差异合计(绝对值)
        BigDecimal retailAbsoluteDiff = BigDecimal.ZERO;
        //零售额 绝对差异率
        BigDecimal retailAbsoluteDiffRatio = BigDecimal.ZERO;
        //零售额 差异合计(相对值)
        BigDecimal retailRelativeDiff = BigDecimal.ZERO;
        //零售额 相对差异率
        BigDecimal retailRelativeDiffRatio = BigDecimal.ZERO;

        Example physicalHeaderExample = new Example(GaiaSdPhysicalHeader.class);
        physicalHeaderExample.createCriteria().andEqualTo("client", clientId) .andEqualTo("gsphBrId", brId) .andEqualTo("gsphVoucherId", voucherId);
        GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(physicalHeaderExample);
        Table<String, String, Integer> productIdTable = HashBasedTable.create();
        String isFull = physicalHeader.getGsphFlag1();
        String isShowStock = physicalHeader.getGsphFlag2();
        String isBatch = physicalHeader.getGsphFlag3();
        StringBuffer inventoryTypeSb = new StringBuffer();
        inventoryTypeSb.append(isFull.equals("0") ? "部分盘点" : "全盘");
        inventoryTypeSb.append("、");
        inventoryTypeSb.append(isShowStock.equals("0") ? "明盘" : "盲盘");
        inventoryTypeSb.append("、");
        inventoryTypeSb.append(isBatch.equals("0") ? "数量盘点" : "批号盘点");


        physicalCountInData.setClientId(clientId);
        physicalCountInData.setGspcBrId(brId);
        physicalCountInData.setGspcVoucherId(pcVoucherId);
//            Example physicalCountDiffExample = new Example(GaiaSdPhysicalCountDiff.class);
//            physicalCountDiffExample.createCriteria().andEqualTo("clientId", clientId)
//                                                     .andEqualTo("gspcdBrId", brId)
//                                                     .andEqualTo("gspcVoucherId", voucherId);
        List<PhysicalNewReportOutData> physicalNewReportOutDataList = physicalCountDiffMapper.fetchPhysicalNewReport(physicalCountInData);
        for(PhysicalNewReportOutData physicalNewReportOutData : physicalNewReportOutDataList){
            //盘点数量
            BigDecimal pcFirstQty = physicalNewReportOutData.getPcFirstQty();
            //盘点差异
            BigDecimal diffSecondQty = physicalNewReportOutData.getDiffSecondQty() == null ? BigDecimal.ZERO : physicalNewReportOutData.getDiffSecondQty();
            String proId = physicalNewReportOutData.getProId();
            BigDecimal movPrice = physicalNewReportOutData.getMovPrice() == null ? BigDecimal.ZERO : physicalNewReportOutData.getMovPrice();
            BigDecimal movAmt = physicalNewReportOutData.getMovAmt() == null ? BigDecimal.ZERO : physicalNewReportOutData.getMovAmt();
            BigDecimal priceNormal = physicalNewReportOutData.getPriceNormal() == null ? BigDecimal.ZERO : physicalNewReportOutData.getPriceNormal();
            numInventoryTotal = numInventoryTotal.add(pcFirstQty.abs());
            if(diffSecondQty.compareTo(BigDecimal.ZERO) > 0){
                numInventoryProfit = numInventoryProfit.add(diffSecondQty);
                costInventoryProfit = costInventoryProfit.add(diffSecondQty.multiply(movPrice));
                retailInventoryProfit = retailInventoryProfit.add(diffSecondQty.multiply(priceNormal));

                if(productIdTable.get("profit", proId) == null){
                    productIdTable.put("profit", proId, 1);
                }else {
                    productIdTable.put("profit", proId, productIdTable.get("profit", proId) + 1);
                }
                batchInventoryProfit++;
            }else if(diffSecondQty.compareTo(BigDecimal.ZERO) < 0){
                numInventoryLosses = numInventoryLosses.add(diffSecondQty);
                costInventoryLosses = costInventoryLosses.add(diffSecondQty.multiply(movPrice));
                retailInventoryLosses = retailInventoryLosses.add(diffSecondQty.multiply(priceNormal));

                if(productIdTable.get("losses", proId) == null){
                    productIdTable.put("losses", proId, 1);
                }else {
                    productIdTable.put("losses", proId, productIdTable.get("losses", proId) + 1);
                }

                batchInventoryLosses++;
            }else {
                if(productIdTable.get("empty", proId) == null){
                    productIdTable.put("empty", proId, 1);
                }else {
                    productIdTable.put("empty", proId, productIdTable.get("empty", proId) + 1);
                }
            }

            //品项 按数量盘点
            if(StringUtils.equals("0", isBatch)){
                if(diffSecondQty.compareTo(BigDecimal.ZERO) > 0){
                    itemInventoryProfit = itemInventoryProfit.add(BigDecimal.ONE);

                }else if(diffSecondQty.compareTo(BigDecimal.ZERO) < 0){
                    itemInventoryLosses = itemInventoryLosses.add(BigDecimal.ONE);
                }
                itemInventoryTotal = itemInventoryTotal.add(BigDecimal.ONE);
            }

            //成本额
            costInventoryTotal = costInventoryTotal.add(pcFirstQty.multiply(movPrice));

            //零售额
            retailInventoryTotal = retailInventoryTotal.add(pcFirstQty.multiply(priceNormal));
        }

        //按批号盘点
        if(StringUtils.equals("1", isBatch)){
            Map<String ,Integer> itemInventoryProfitMap = productIdTable.row("profit");
            Map<String ,Integer> itemInventoryLossesMap = productIdTable.row("losses");
            Map<String ,Integer> itemInventoryEmptyMap = productIdTable.row("empty");
            newPhysicalWorkflowInData.setItemInventoryProfit(new BigDecimal(itemInventoryProfitMap.size()));
            newPhysicalWorkflowInData.setItemInventoryLosses(new BigDecimal(itemInventoryLossesMap.size()));
            itemInventoryTotal = newPhysicalWorkflowInData.getItemInventoryProfit().abs().add(newPhysicalWorkflowInData.getItemInventoryLosses().abs()).add(new BigDecimal(itemInventoryEmptyMap.size()));

            //批号行数
            double listSize = physicalNewReportOutDataList.size();
            newPhysicalWorkflowInData.setBatchInventoryTotal(NumberUtil.decimalFormat("#", physicalNewReportOutDataList.size()));
            newPhysicalWorkflowInData.setBatchInventoryProfit(batchInventoryProfit);
            newPhysicalWorkflowInData.setBatchInventoryLosses(batchInventoryLosses);
            newPhysicalWorkflowInData.setBatchAbsoluteDiff(batchInventoryProfit + batchInventoryLosses);
            newPhysicalWorkflowInData.setBatchAbsoluteDiffRatio(NumberUtil.decimalFormat("#.##%", newPhysicalWorkflowInData.getBatchAbsoluteDiff() / listSize));
            newPhysicalWorkflowInData.setBatchRelativeDiff(batchInventoryProfit - batchInventoryLosses);
            newPhysicalWorkflowInData.setBatchRelativeDiffRatio(NumberUtil.decimalFormat("#.##%", newPhysicalWorkflowInData.getBatchRelativeDiff() / listSize));
        }else{
            newPhysicalWorkflowInData.setItemInventoryProfit(itemInventoryProfit);
            newPhysicalWorkflowInData.setItemInventoryLosses(itemInventoryLosses);
        }

        //品项
        newPhysicalWorkflowInData.setItemInventoryTotal(itemInventoryTotal);
        newPhysicalWorkflowInData.setItemAbsoluteDiff(newPhysicalWorkflowInData.getItemInventoryProfit().add(newPhysicalWorkflowInData.getItemInventoryLosses().abs()));
        newPhysicalWorkflowInData.setItemAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getItemAbsoluteDiff(), itemInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setItemRelativeDiff(newPhysicalWorkflowInData.getItemInventoryProfit().subtract(newPhysicalWorkflowInData.getItemInventoryLosses().abs()));
        newPhysicalWorkflowInData.setItemRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getItemRelativeDiff(), itemInventoryTotal, "#.##%"));

        //数量
        newPhysicalWorkflowInData.setNumInventoryTotal(NumberUtil.decimalFormat("#.####",numInventoryTotal));
        newPhysicalWorkflowInData.setNumInventoryProfit(numInventoryProfit);
        newPhysicalWorkflowInData.setNumInventoryLosses(numInventoryLosses.abs());
        newPhysicalWorkflowInData.setNumAbsoluteDiff(numInventoryProfit.add(numInventoryLosses.abs()));
        newPhysicalWorkflowInData.setNumAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getNumAbsoluteDiff(), numInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setNumRelativeDiff(numInventoryProfit.subtract(numInventoryLosses.abs()));
        newPhysicalWorkflowInData.setNumRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getNumRelativeDiff(), numInventoryTotal, "#.##%"));

        //成本额
        newPhysicalWorkflowInData.setCostInventoryTotal(NumberUtil.decimalFormat("#.##", costInventoryTotal));
        newPhysicalWorkflowInData.setCostInventoryProfit(NumberUtil.decimalFormat("#.##", costInventoryProfit));
        newPhysicalWorkflowInData.setCostInventoryLosses(NumberUtil.decimalFormat("#.##", costInventoryLosses.abs()));
        newPhysicalWorkflowInData.setCostAbsoluteDiff(new BigDecimal(NumberUtil.decimalFormat("#.##", costInventoryProfit.add(costInventoryLosses.abs()))));
        newPhysicalWorkflowInData.setCostRelativeDiff(new BigDecimal(NumberUtil.decimalFormat("#.##", costInventoryProfit.subtract(costInventoryLosses.abs()))));
        newPhysicalWorkflowInData.setCostAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getCostAbsoluteDiff(), costInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setCostRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getCostRelativeDiff(), costInventoryTotal, "#.##%"));

        //零售额
        newPhysicalWorkflowInData.setRetailInventoryTotal(NumberUtil.decimalFormat("#.##", retailInventoryTotal.abs()));
        newPhysicalWorkflowInData.setRetailInventoryProfit(NumberUtil.decimalFormat("#.##", retailInventoryProfit.abs()));
        newPhysicalWorkflowInData.setRetailInventoryLosses(NumberUtil.decimalFormat("#.##", retailInventoryLosses.abs()));
        newPhysicalWorkflowInData.setRetailAbsoluteDiff(new BigDecimal(NumberUtil.decimalFormat("#.##", retailInventoryProfit.add(retailInventoryLosses.abs()))));
        newPhysicalWorkflowInData.setRetailAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getRetailAbsoluteDiff(), retailInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setRetailRelativeDiff(retailInventoryProfit.subtract(retailInventoryLosses.abs()));
        newPhysicalWorkflowInData.setRetailRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getRetailRelativeDiff(), retailInventoryTotal, "#.##%"));

        newPhysicalWorkflowInData.setInventoryDate(CommonUtil.parseyyyyMMdd(incomeStatement.getGsishDate()));
        newPhysicalWorkflowInData.setVoucherId(voucherId);
        newPhysicalWorkflowInData.setIsBatch(isBatch);
        newPhysicalWorkflowInData.setInventoryType(inventoryTypeSb.toString());

        JSON.toJSON(newPhysicalWorkflowInData);
    }


}
