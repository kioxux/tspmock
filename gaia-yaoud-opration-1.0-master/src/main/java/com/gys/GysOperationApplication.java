package com.gys;

import cn.hutool.core.collection.ListUtil;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableSwagger2Doc
@SpringBootApplication
@MapperScan({"com.gys.business.mapper","com.gys.business.mapper.takeAway"})
@EnableCaching
@EnableEurekaClient
@EnableFeignClients
@EnableScheduling
public class GysOperationApplication { 
    public static void main(String[] args) {
        SpringApplication.run(GysOperationApplication.class, args);
    }
}
