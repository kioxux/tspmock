package com.gys.util;


import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
public class DateUtil {

    /**
     * 默认日期格式
     */
    public static String DEFAULT_FORMAT = "yyyyMMdd";

    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


//    /**
//     * 测试主方法
//     * @param args
//     */
//    public static void main(String[] args) {
//        System.out.println(getYearFirst(2021));
//        System.out.println(getYearLast(2023));
//        System.out.println(getYearMonthFirst("202102"));
//
//        System.out.println(getYearMonthLast("202106"));
//
////        System.out.println(formatDate(getCurrYearFirst()));
////        System.out.println(formatDate(getCurrYearLast()));

//    }


    /**
     * 格式化日期
     *
     * @param date 日期对象
     * @return String 日期字符串
     */
    public static String formatDate(Date date) {
        SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
        String sDate = f.format(date);
        return sDate;
    }

    /**
     * 判断日期是否是过去日期
     * @param str
     * @return
     */
    public static boolean isPastDate(String str){
        boolean flag = false;
        Date nowDate = new Date();
        Date pastDate = null;
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.CHINA);
        //在日期字符串非空时执行
        if (str != null && !"".equals(str)) {
            try {
                //将字符串转为日期格式，如果此处字符串为非合法日期就会抛出异常。
                pastDate = sdf.parse(str);
                //调用Date里面的before方法来做判断
                flag = pastDate.before(nowDate);
                if (flag) {
                    System.out.println("该日期早于今日");
                }else {
                    System.out.println("该日期晚于今日");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("日期参数不可为空");
        }
        return flag;
    }

    //判断选择的日期是否是本月
    public static boolean isThisMonth(Date time, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String param = sdf.format(time);//参数时间
        String now = sdf.format(new Date());//当前时间
        if (param.equals(now)) {
            return true;
        }
        return false;
    }

    /**
     * 功能描述: 获取上一个月
     * @param localDate
     * @return java.lang.String
     * @author wangQc
     * @date 2021/8/31 3:36 下午
     */
    public static LocalDate getPreMonth(LocalDate localDate) {
        LocalDate preMonth;
        if(localDate.getMonthValue() == 1){
            preMonth = LocalDate.of(localDate.getYear() -1, 12, 1);
        } else {
            preMonth = LocalDate.of(localDate.getYear(), localDate.getMonthValue() - 1, 1);
        }
        return preMonth;
    }


    /**
     * 格式化日期
     *
     * @param date 日期对象
     * @return String 日期字符串
     */
    public static String formatDate(LocalDate date) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
        return dateTimeFormatter.format(date);
    }
    public static String formatLocalDate(LocalDate date) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return dateTimeFormatter.format(date);
    }

    /**
     * 获取当年的第一天
     * @return
     */
//    public static Date getCurrYearFirst(){
//        Calendar currCal=Calendar.getInstance();
//        int currentYear = currCal.get(Calendar.YEAR);
//        return getYearFirst(currentYear);
//    }

    /**
     * 获取当年的最后一天
     *
     * @return
     */
//    public static Date getCurrYearLast(){
//        Calendar currCal=Calendar.getInstance();
//        int currentYear = currCal.get(Calendar.YEAR);
//        return getYearLast(currentYear);
//    }
    public static String addDay(Date date, String format, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String format1 = sdf.format(calendar.getTime());
        return format1;
    }

    /**
     * 获取某年第一天日期
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearFirst(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        Date currYearFirst = calendar.getTime();
        SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
        return f.format(currYearFirst);
    }

    /**
     * 获取某年最后一天日期
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearLast(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();
        SimpleDateFormat f = new SimpleDateFormat(DEFAULT_FORMAT);
        return f.format(currYearLast);
    }

    /**
     * 获取某年第一天日期
     *
     * @param yearMonth 年月
     * @return Date
     */
    public static String getYearMonthFirst(String yearMonth) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        try {

            Date date = null;
            date = sdf.parse(yearMonth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);// 设为本月第一天
            return sdf2.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new BusinessException("起始日期异常！");

    }

    /**
     * 获取某年某月最后一天日期
     *
     * @param yearMonth 年月
     * @return Date
     */
    public static String getYearMonthLast(String yearMonth) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        try {

            Date date = null;
            date = sdf.parse(yearMonth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);// 设为本月第一天
            calendar.add(Calendar.MONTH, 1);// 月份加一
            calendar.add(Calendar.DATE, -1);// 天数加 -1 = 上一个月的最后一天
            return sdf2.format(calendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new BusinessException("结束日期异常！");
    }

    /**
     * 当前日期
     *
     * @return
     */
    public static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(new Date());
    }
    public static String getCurrentDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }


    /**
     * 格式化时间   字符串转为Date类型
     *
     * @param dateStr
     * @param pattern
     * @return
     * @throws Exception
     */
    public static Date stringToDate(String dateStr, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new BusinessException("时间格式转换异常");
        }
        return date;
    }


    public static Date getMiddleTime(Date start, Date end) {
        Long startL = start.getTime();
        Long endL = end.getTime();
        long l = new Double(startL + (endL - startL) * Math.random()).longValue();
        return new Date(l);
    }


    /**
     * 格式化时间   Date类型转为字符串
     *
     * @param date
     * @param pattern
     * @return
     * @throws Exception
     */
    public static String dateToString(Date date, String pattern) {
        String dateStr = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            dateStr = sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("时间格式转换异常");
        }
        return dateStr;
    }


    public static String getTimeRange(String begin, String end) {
        Date startDate = stringToDate(begin, "HH:mm:ss");
        Date endDate = stringToDate(end, "HH:mm:ss");
        Date middleTime = getMiddleTime(startDate, endDate);

        String rangeTime = dateToString(middleTime, "HHmmss");
        return rangeTime;
    }

    public static String formatDateStr(String strDate, String pattern) {
        String format = "";
        try {
            Date date = (Date) new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);//设置日期格式
            format = dateFormat.format(date);
        } catch (ParseException e) {
            log.error("日期格式转换异常：{}",e.getMessage(), e);
        }
        return format;
    }

    public static String tranceDateStrByPattern(String strDate, String fromPattern,String toPattern) {
        String format = "";
        try {
            Date date = (Date) new SimpleDateFormat(fromPattern).parse(strDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat(toPattern);//设置日期格式
            format = dateFormat.format(date);
        } catch (ParseException e) {
            log.error("日期格式转换异常：{}",e.getMessage(), e);
        }
        return format;
    }



    /**
     * 当前时间字符串（yyyy-MM-dd）
     *
     * @return
     */
    public static String getCurrentDateStr() {
        return LocalDate.now().format(DATE_FORMATTER);
    }

    /**
     * 当前时间字符串（HH:mm:ss）
     *
     * @return
     */
    public static String getCurrentTimeStr() {
        return LocalTime.now().format(TIME_FORMATTER);
    }

    /**
     * 自定义格式获取当前时间(yyyy-MM-dd)
     *
     * @param pattern 格式
     * @return
     */
    public static String getCurrentDateStr(String pattern) {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param pattern
     * @return
     */
    public static String getCurrentDateTimeStr(String pattern) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * @param pattern
     * @return
     */
    public static String getCurrentTimeStr(String pattern) {
        return LocalTime.now().format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取上月
     * 描述:<描述函数实现的功能>.
     * @param date
     * @return
     */
    public static String getLastMonth(String date) {
        String lastMonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("yyyyMM");
        int year = Integer.parseInt(date.substring(0, 4));
        String monthsString = date.substring(4, 6);
        int month;
        if ("0".equals(monthsString.substring(0, 1))) {
            month = Integer.parseInt(monthsString.substring(1, 2));
        } else {
            month = Integer.parseInt(monthsString.substring(0, 2));
        }
        cal.set(year,month-2,Calendar.DATE);
        lastMonth = dft.format(cal.getTime());
        return lastMonth;
    }

    /**
     * 获取下月
     * 描述:<描述函数实现的功能>.
     * @param date
     * @return
     */
    public static String getNextMonth(String date) {
        String lastMonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("yyyyMM");
        int year = Integer.parseInt(date.substring(0, 4));
        String monthsString = date.substring(4, 6);
        int month;
        if ("0".equals(monthsString.substring(0, 1))) {
            month = Integer.parseInt(monthsString.substring(1, 2));
        } else {
            month = Integer.parseInt(monthsString.substring(0, 2));
        }
        cal.set(year, month - 1, Calendar.DATE);
        cal.add(cal.MONTH, 1);
        lastMonth = dft.format(cal.getTime());
        return lastMonth;
    }


    /**
     * 获取季度的第一天
     *
     * @param now
     * @return
     */
    public static Date getQuarterlyFirstDay(Date now) {
        int month = now.getMonth();
        if (month == 0 || month == 1 || month == 2) {
            now.setMonth(0);
            now.setDate(1);
            return now;
        } else if (month == 3 || month == 4 || month == 5) {
            now.setMonth(3);
            now.setDate(1);
            return now;
        } else if (month == 6 || month == 7 || month == 8) {
            now.setMonth(6);
            now.setDate(1);
            return now;
        } else {
            now.setMonth(9);
            now.setDate(1);
            return now;
        }
    }

    /**
     * 获取单月第一天
     * @param now
     * @return
     */
    public static Date getSingleMonthFirstDay(Date now) {
        int month = now.getMonth();
        if (month == 0 || month == 1) {
            now.setMonth(0);
            now.setDate(1);
            return now;
        } else if (month == 3 || month == 2) {
            now.setMonth(3);
            now.setDate(1);
            return now;
        } else if (month == 4 || month == 5) {
            now.setMonth(4);
            now.setDate(1);
            return now;
        } else if (month == 6 || month == 7) {
            now.setMonth(6);
            now.setDate(1);
            return now;
        } else if (month == 8 || month == 9) {
            now.setMonth(8);
            now.setDate(1);
            return now;
        } else {
            now.setMonth(10);
            now.setDate(1);
            return now;
        }
    }

    /**
     * 本月第一天
     *
     * @param date
     * @return
     */
    public static Date getThisMonthFirstDay(Date date) {
        Calendar cal_1 = Calendar.getInstance();//获取当前日期

        cal_1.add(Calendar.MONTH, 0);

        cal_1.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天

        Date firstDay = cal_1.getTime();

        return firstDay;
    }

    /**
     * 本周一
     *
     * @param date
     * @return
     */
    public static Date getThisWeekMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 获得当前日期是一个星期的第几天
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        return cal.getTime();
    }

    /**
     * 获取输入日期是周几
     *
     * @param date
     * @return
     */
    public static int getWhichDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 获得当前日期是一个星期的第几天
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        return dayWeek;
    }

    public static Date moveTime(Date date, int day) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, day);//把日期往后增加一天.整数往后推,负数往前移动
        return calendar.getTime();  //这个时间就是日期往后推一天的结果
    }

    //判断相差几天
    public static int getBetweenDay(Date date1, Date date2) {
        Calendar d1 = new GregorianCalendar();
        d1.setTime(date1);
        Calendar d2 = new GregorianCalendar();
        d2.setTime(date2);
        int days = d2.get(Calendar.DAY_OF_YEAR)- d1.get(Calendar.DAY_OF_YEAR);
        System.out.println("days="+days);
        int y2 = d2.get(Calendar.YEAR);
        if (d1.get(Calendar.YEAR) != y2) {
            do {
                days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);
                d1.add(Calendar.YEAR, 1);
            } while (d1.get(Calendar.YEAR) != y2);
        }
        return days;
    }


    public static String getLastDay(int year, int month){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        LocalDate now = LocalDate.now();
        // 获取Calendar类的实例
        Calendar c = Calendar.getInstance();
        // 设置年份
        c.set(Calendar.YEAR,year);
        // 设置月份，因为月份从0开始，所以用month - 1
        c.set(Calendar.MONTH, month - 1);
        int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 将获取的最大日期数设置为Calendar实例的日期数
        c.set(Calendar.DAY_OF_MONTH, lastDay);
        String format1 = format.format(c.getTime());
         return format1;
    }

    //获取前一天日期
    public static Date getYesterday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        return date;
    }

    public static String getDate(int year, int month){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        LocalDate now = LocalDate.now();
        // 获取Calendar类的实例
        Calendar c = Calendar.getInstance();
        // 设置年份
        c.set(Calendar.YEAR,year);
        // 设置月份，因为月份从0开始，所以用month - 1
        c.set(Calendar.MONTH, month - 2);
        int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 将获取的最大日期数设置为Calendar实例的日期数
        c.set(Calendar.DAY_OF_MONTH, lastDay);
        String format1 = format.format(c.getTime());
        return format1;
    }


    public static long getDiffByDate(String beginDateStr, String endDateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date beginDate = null;
        Date endDate = null;
        try {
            beginDate = sdf.parse(beginDateStr);
            endDate = sdf.parse(endDateStr);
        } catch (ParseException e) {
            log.error("日期格式转换异常：{}",e.getMessage(), e);
            throw new BusinessException("日期计算错误");
        }
        long diffDay = (endDate.getTime() - beginDate.getTime() + 1000000) / ( 60 * 60 * 24 * 1000);
        return diffDay;
    }

    public static List<String> getDateList(String beginDate, String endDate) throws ParseException {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(beginDate));
        List<String> dates = new ArrayList<>();
        for (long d = cal.getTimeInMillis(); d <= sdf.parse(endDate).getTime(); d = get_D_Plaus_1(cal)) {
            System.out.println(sdf.format(d));
            dates.add(DateUtil.dates(sdf.format(d)));
        }
        return dates;
    }
    public static long get_D_Plaus_1(Calendar c) {
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + 1);

        return c.getTimeInMillis();

    }
    public static String dateConvert(String str) {
        String dateString;
        try {
            Date parse = new SimpleDateFormat("yyyyMMdd").parse(str);
            dateString = new SimpleDateFormat("yyyy-MM-dd").format(parse);
        } catch (ParseException e) {
            dateString = null;
        }
        return dateString;
    }
    public static String dates(String str) {
        String dateString;
        try {
            Date parse = new SimpleDateFormat("yyyy-MM-dd").parse(str);
            dateString = new SimpleDateFormat("yyyyMMdd").format(parse);
        } catch (ParseException e) {
            dateString = null;
        }
        return dateString;
    }

}
