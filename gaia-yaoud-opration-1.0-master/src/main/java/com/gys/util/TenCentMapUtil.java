package com.gys.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
@Slf4j
public class TenCentMapUtil {


        // key
        private static final String KEY = "HSMBZ-BFFWF-CZLJZ-NAZLZ-DWZWQ-LZF55";

        /**
         * @Description: 通过经纬度获取位置
         * @Param: [log, lat]
         * @return: java.lang.String
         * 响应结果:https://lbs.qq.com/service/webService/webServiceGuide/webServiceGeocoder
         * status	number	是	状态码，0为正常，其它为异常，详细请参阅状态码说明
         * message	string	是	状态说明
         * result	object	是	地址解析结果
         * title	string	是	最终用于坐标解析的地址或地点名称
         * （如果需要获得结构化及标准化地址请参阅 智能地址解析）
         * location	string	是	解析到的坐标（GCJ02坐标系）
         * lat	number	是	纬度
         * lng	number	是	经度
         * address_components	object	是	解析后的地址部件
         * province	string	是	省
         * city	string	是	市
         * district	string	是	区，可能为空字串
         * street	string	是	街道/道路，可能为空字串
         * （要获取行政区划的街道级别请参阅 智能地址解析）
         * street_number	string	是	门牌，可能为空字串
         * ad_info	object	是	行政区划信息
         * adcode			行政区划代码，规则详见：行政区划代码说明
         * similarity	number	是	即将下线，由reliability代替
         * deviation	number	是	即将下线，由level代替
         * reliability	number	是	可信度参考：值范围 1 <低可信> - 10 <高可信>
         * 我们根据用户输入地址的准确程度，在解析过程中，将解析结果的可信度(质量)，由低到高，分为1 - 10级，该值>=7时，解析结果较为准确，<7时，会存各类不可靠因素，开发者可根据自己的实际使用场景，对于解析质量的实际要求，进行参考。
         * level	number	否	解析精度级别，分为11个级别，一般>=9即可采用（定位到点，精度较高） 也可根据实际业务需求自行调整，完整取值表见下文。
         */
        public static Map<String, Object> getAddress(String lng, String lat) {

            Map<String, Object> resultMap = new HashMap<String, Object>();

            // 参数解释：lng：经度，lat：维度。KEY：腾讯地图key，get_poi：返回状态。1返回，0不返回
            String urlString = "http://apis.map.qq.com/ws/geocoder/v1/?location=" + lat + "," + lng + "&key=" + KEY + "&get_poi=1";
            String result = "";
            try {
                URL url = new URL(urlString);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                // 腾讯地图使用GET
                conn.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line;
                // 获取地址解析结果
                while ((line = in.readLine()) != null) {
                    result += line + "\n";
                }
                in.close();
            } catch (Exception e) {
                e.getMessage();
            }

            // 转JSON格式
            JSONObject jsonObject = JSONObject.parseObject(result).getJSONObject("result");
            // 获取地址（行政区划信息） 包含有国籍，省份，城市
            JSONObject adInfo = jsonObject.getJSONObject("ad_info");
            resultMap.put("nation", adInfo.get("nation"));
            resultMap.put("nationCode", adInfo.get("nation_code"));
            resultMap.put("province", adInfo.get("province"));
            resultMap.put("provinceCode", adInfo.get("adcode"));
            resultMap.put("city", adInfo.get("city"));
            resultMap.put("cityCode", adInfo.get("city_code"));
            return resultMap;
        }

        public static void main(String[] args) {

            // 测试
//            String lng = "111.546112";//经度
//            String lat = "24.378622";//维度
//            Map<String, Object> map = getLocation(lng, lat);
//            System.out.println(map);
//            System.out.println("国   籍：" + map.get("nation"));
//            System.out.println("国家代码：" + map.get("nationCode"));
//            System.out.println("省   份：" + map.get("province"));
//            System.out.println("省份代码：" + map.get("provinceCode"));
//            System.out.println("城   市：" + map.get("city"));
//            System.out.println("城市代码：" + map.get("cityCode"));

            String address="武汉市";
            Map<String, Object> map = getLngAndLat(address);
            System.out.println(map);

        }

    public static Map<String, Object> getLngAndLat(String address) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        String urlStr = "https://apis.map.qq.com/ws/geocoder/v1/?address=" + address + "&key="+KEY;
        //请求的url
        URL url = null;
        //请求的输入流
        BufferedReader in = null;
        //输入流的缓冲
        StringBuffer sb = new StringBuffer();
        try {
            url = new URL(urlStr);
            in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            String str = null;
            //一行一行进行读入
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
        } catch (Exception ex) {

        } finally {
            try {
                if (in != null) {
                    in.close(); //关闭流
                }
            } catch (IOException ex) {

            }
        }
        String result = sb.toString();
        log.info("<result: %s>"+ result);
        Integer status = JSONObject.parseObject(result).getInteger("status");
        if (status != 0) {
            return null;
        }
        String r = JSONObject.parseObject(result).getString("result");
        String location = JSONObject.parseObject(r)!=null?JSONObject.parseObject(r).getString("location"):"";
        String lng = JSONObject.parseObject(location)!=null?JSONObject.parseObject(location).getString("lng"):"";
        String lat = JSONObject.parseObject(location)!=null?JSONObject.parseObject(location).getString("lat"):"";
        Map<String, Object> map = new HashMap<>();
        map.put("lng", lng);
        map.put("lat", lat);
        return map;

    }


}
