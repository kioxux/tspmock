package com.gys.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;

import java.io.Writer;

/**
 * @desc:  xml/实体转换工具类
 * @author: ryan
 * @createTime: 2018/6/14-10:30
 */
public class XmlUtils {

    /**
     * xml序列化（将对象转为xml）
     * @param object
     */
    public static String toXml(Object object) {
        xStream.autodetectAnnotations(true);
        return xStream.toXML(object);
    }

    /**
     * xml反序列化（将xml转为对象）
     */
    @SuppressWarnings("unchecked")
    public static <T> T fromXml(String xml,Class<T> tClass) {
        deserialize_xStream.processAnnotations(tClass);
        deserialize_xStream.ignoreUnknownElements();
        return (T) deserialize_xStream.fromXML(xml);
    }

    /**
     * 注意：并发情况下不要直接使用工具类静态成员变量
     * 扩展xStream支持name中带有'_',如<test_name></test_name>
     */
    public static XStream underline_xStream = new XStream(
            new DomDriver("UTF-8",new XmlFriendlyNameCoder("-_","_"))
    );

    /**
     * 注意：并发情况下不要直接使用工具类静态成员变量
     */
    private static XStream deserialize_xStream = new XStream(new DomDriver());

    /**
     * 注意：并发情况下不要直接使用工具类静态成员变量
     * 扩展xStream，使其支持CDATA块[xml序列化使用]
     */
    private static XStream xStream = new XStream(new XppDriver() {
        @Override
        public HierarchicalStreamWriter createWriter(Writer out) {
            return new PrettyPrintWriter(out) {
                boolean cdata = true;
                @SuppressWarnings("unchecked")
                @Override
                public void startNode(String name, Class clazz) {
                    super.startNode(name, clazz);
                }
                @Override
                protected void writeText(QuickWriter writer, String text) {
                    if (cdata) {
                        writer.write("<![CDATA[");
                        writer.write(text);
                        writer.write("]]>");
                    } else {
                        writer.write(text);
                    }
                }
            };
        }
    });

}
