package com.gys.util.thirdPay;

import com.gys.common.data.ResultVO;

import java.util.Map;

public interface ThirdPayInterface {
    /**
     * 支付
     * @return
     */
    ResultVO<Map<String, Object>> pay(Map<String, Object> params);
    /**
     * 查询订单
     * @return
     */
    ResultVO<Map<String, Object>> query(Map<String, Object> params);
    /**
     * 退货
     * @return
     */
    ResultVO<Map<String, Object>> refundBack(Map<String, Object> params);
    /**
     * 撤销
     * @return
     */
    ResultVO<Map<String, Object>> revokePay(Map<String, Object> params);

}
