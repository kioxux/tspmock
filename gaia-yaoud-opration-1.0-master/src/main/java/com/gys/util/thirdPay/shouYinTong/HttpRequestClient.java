package com.gys.util.thirdPay.shouYinTong;


import com.gys.common.http.HttpJson;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Slf4j
public class HttpRequestClient {

    /**
     *
     * 发送http请求
     *
     * @param requestUrl 请求的url
     * @param paramMap 请求中所需要传的参数
     * @return
     * @throws
     */
    public static HttpJson httpRequest(String requestUrl, Map<String, String> paramMap) {
        HttpJson httpJson = new HttpJson();
        StringBuffer buffer = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setRequestMethod("POST");
            httpUrlConn.setConnectTimeout(30000);
            httpUrlConn.setReadTimeout(30000);
            httpUrlConn.connect();
            OutputStream outputStream = httpUrlConn.getOutputStream();
            StringBuffer params = new StringBuffer();
            if (paramMap != null) {
                Set<String> keySet = paramMap.keySet();
                Iterator<String> iterator = keySet.iterator();
                int i = 0;
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    if (i > 0) {
                        params.append("&");
                    }
                    params.append(key + "=" + paramMap.get(key));
                    i++;
                }
            }
            outputStream.write(params.toString().getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();

            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            buffer = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }

            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (SocketTimeoutException timeoutException) {
            httpJson.setSuccess(false);
            httpJson.setMsg("连接收银通网络超时,请查看网络~!");
        } catch (NoRouteToHostException e) {
            httpJson.setSuccess(false);
            httpJson.setMsg("当前处于无网络状态~!");
        } catch (Exception e) {
            httpJson.setSuccess(false);
            httpJson.setMsg("接口请求失败!");
            log.error("接口请求失败!{}",e.getMessage(), e);
        }
//        JSONObject resultObj = JSONObject.parseObject(buffer.toString());
        if(buffer==null){
            httpJson.setSuccess(false);
            httpJson.setMsg("收银通服务器无响应！");
        }else{
            httpJson.setSuccess(true);
            httpJson.setMsg(buffer.toString());
            log.info(buffer.toString());
        }
        return httpJson;
    }


    public static String httpRequest(String requestUrl, String paramMap) {
        StringBuffer buffer = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setRequestMethod("POST");
            httpUrlConn.setConnectTimeout(30000);
            httpUrlConn.setReadTimeout(30000);
            httpUrlConn.connect();
            OutputStream outputStream = httpUrlConn.getOutputStream();
            outputStream.write(paramMap.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();

            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            buffer = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }

            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    /**
     *
     * 发送http请求
     *
     * @param requestUrl 请求的url
     * @param
     * @return
     * @throws
     */
    public static String httpRequestXml(String requestUrl, byte[] byteStr) {
        StringBuffer buffer = null;
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setRequestMethod("POST");
            httpUrlConn.setConnectTimeout(30000);
            httpUrlConn.setRequestProperty("Content-Type", "text/xml");
            httpUrlConn.setReadTimeout(30000);
            httpUrlConn.connect();
            OutputStream outputStream = httpUrlConn.getOutputStream();
            outputStream.write(byteStr);
            outputStream.flush();
            outputStream.close();

            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            buffer = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }

            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

}
