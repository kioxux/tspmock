package com.gys.util.thirdPay.shouYinTong;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.http.HttpJson;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class BizUtil {

//    public static String host = "http://csmbk.jnbank.cn:8110";


//    public static void main(String[] args) throws Exception {
//        unifiedMicrosPay("N677","81010344730","93599C30DBDA5EA7743FC48CFAD76061","SD2102000100015","134722643916342399","000000000001","订单备注信息");// 被扫 付款码
//        query("N677","801001147398","93599C30DBDA5EA7743FC48CFAD76061","2107131PNMSH3XZLC985");
//        revokeMicrosPay("N677","801001147398","93599C30DBDA5EA7743FC48CFAD76061","2107131PNMR361700985","2107131PNMR361700985","");
//        Map<String, Object> payMaps = (Map<String, Object>) JSON.parse(a.getMsg());

//        refundBack("N677","801001147398","93599C30DBDA5EA7743FC48CFAD76061","2107131PNMSH3XZLC985","20210713212341","000000000001","JN200017153","20210713");
//    }

    /**
     * 被扫交易接口
     * @param channel 渠道编号
     * @param merId 商户号
     * @param key 预分配加密要素
     * @param outerNumber 订单编号
     * @param time yyyyMMddHHmmss
     * @param authCode 用户授权码/被扫交易支付码
     * @param orderAmount 金额以分为单位 000000000001
     * @param note 订单备注
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    public static HttpJson unifiedMicrosPay(String host,String channel, String merId, String key, String outerNumber, String time, String authCode, String orderAmount, String note) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Map<String, Object> order = new HashMap<>();
        order.put("channel", channel);
        order.put("outerNumber", outerNumber);// 最好加上几位随机数保证订单编号不重复
        order.put("outerDtTm", time);//new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
        order.put("merId", merId);
        order.put("transType", "01");// 固定值
        order.put("orderAmount", orderAmount);// 金额以分为单位 000000000001
        order.put("authCode", authCode);//用户授权码 被扫交易支付码
        order.put("payType", "15");// 15:微信被扫 20：支付宝被扫 , 如果分不清微信或者支付的付款码，默认传入"15"
        order.put("currencyCode", "156");// 固定值
        if(StrUtil.isNotEmpty(note)){
            order.put("note", URLEncoder.encode(note, "GBK"));// 非必传，如果传请采用该种方式编码
        }
        order.put("deviceInfo", "123456");// 非必传，终端设备号
        order.put("sign", signMd5(order, key));// MD5加密
        log.info(JSONObject.toJSON(order).toString());
        /************************ Base64加密 ****************************************/
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("params", signBase64(order));
        String url = host+"/PayWeb/pay/unified/unifiedMicrosPay.do";
        HttpJson resultStr = HttpRequestClient.httpRequest(url, paramMap);
        log.info("unifiedMicrosPay resultStr:" + resultStr.getMsg());
        return resultStr;
    }

    /**
     * 查询订单状态
     * @param channel 渠道编号
     * @param merId 商户号
     * @param key 预分配加密要素
     * @param orderNumber 商户订单号
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static HttpJson query(String host,String channel,String merId,String key,String orderNumber) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Map<String, Object> input = new HashMap<>();
        input.put("channel", channel);
        input.put("orderNumber", orderNumber);
        input.put("currencyType", "156");
        input.put("merId", merId);
        input.put("sign", signMd5(input, key));// MD5加密
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("params", signBase64(input));
        String url = host+"/PayWeb/query/queryOrder.do";
        HttpJson resultStr = HttpRequestClient.httpRequest(url, paramMap);
        log.info("query resultStr:" + resultStr.getMsg());
        return resultStr;
    }

    /**
     * 退货
     * @param channel 渠道编号
     * @param merId 商户号
     * @param key 预分配加密要素
     * @param outerNumber 商户订单号
     * @param outerDtTm 订单时间
     * @param orderAmount 交易金额
     * @param initOrderNumber 原平台流水号
     * @param initOrderTime 原平台交易日期
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static HttpJson refundBack(String host,String channel,String merId,String key,String outerNumber,String outerDtTm,String orderAmount,String initOrderNumber,String initOrderTime) throws Exception {
        HashMap<String, Object> input = new HashMap<>();
        input.put("channel", channel); //渠道编号
        input.put("outerNumber", outerNumber);//商户订单号
        input.put("outerDtTm", outerDtTm);//订单时间 20170330123300
        input.put("orderAmount", orderAmount);//交易金额 退货金额,000000000001 代表0.01 元
        input.put("merId", merId);//商户号
        input.put("initOrderNumber", initOrderNumber);//原平台流水号 原消费交易的收单消费流水号：JN100334036
        input.put("initOrderTime", initOrderTime);//原平台交易日期 退货需要原平台交易日期20171205
        input.put("currencyCode", "156");//币种 默认：156：人民币
//        input.put("sign", signMd5(input, key));// MD5加密
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("params", SignUtil.signBase64(Rsa256Sign.getSignMap(input,   "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCY5A1HuentN6XZ1i4kdssh3GYDzPZZ7bMp1A0kwRkmdYZy1XtCbBeT/MQb48WloKdg2/3qKLP5ojSIwGiRn4s4G7TE0vnzfaEsUyy4rJwBvPaueLyUJgaSlmarrumH44jF0dFxnrNrFFUsTwDWROjnbHfAsUU8r6EdK8kp/X8Z/OszH/QeLfxKR4Jn0DQ+HWqd1Fh0PE8Xa6bc7ZTrKO8rwb7Hl89GyXo4VxdZUleMu9u5AAvgn6Y7JmW4kmN3ND3CJfrw7nCV+LCvdYfkBlrkl16hMDZ8AjoizYA6NK4Ccx9AnsMkYaUrugErm+LAueywqgn0g7qaAMUTpqv2CN3HAgMBAAECggEBAJFzg97U0+NFqZbP2B4Alcek8bhz6XosVOJaRhbNTav06u+cgSLzyoqTUc5mpsWnTRUstSdriazXz7q5scG4Uq4STatiO0CWKBsPFeCq9dABgX0CiAU5F8+lHg/+21TX9mqb7fF11aLtOQpqLpWqARkJa/P5sbDGNTSXT02PSLfjF42LpZA3bECyCw7kC6BaBBJeaaMPdBwPDs1vxbG+RjWA/y32gmJDfPALWCCnFmXx+YaWXAAc6RhPeEz6lOiRYUGwTdYfzpRM9kSj6CQ5GfC+IeU4rcsKw6Hs7S9sdvi9Ca6quzE9MkMOhl0A8u3BOJgQjeifP2iSzJhx1eeU2eECgYEA2ZDbxs0fdcyaxkj3gjoUXgnzTvXa9Fc8IMNcdXnQDAYWUDFyHAICSItlGF1XMIZ63hPgxNgzXeKugWESfMFOe+COEmGgP+fm+6fOXORQQikbtyArVz/VaccnOclG7SQm7C9degb2iRU+HhS74EThshuhomc2cRpv4iZsDPtXKCUCgYEAs+ZYyvARvhXh5nDv4CpYfXEANa84eoyqykpA2J3SyTbzboZsML6SBD+1X+K1NO5nn0EZ0Gi2eyPLX1KLTt/qviPwkRvWnvdfAQy8STO0VJLZkm9JepcctRucOjQvq1TlGLEXVMrevH/bUh39SzsRaEji/8Ok3ARcZAWf/xHoBHsCgYEAsz4l4vZRifKEPQIWcAK7jvIxswbYWd+Y2QE6pkoyPgWx0mW6NhrtmKjHJDiaQY85oMbNsj7QqvO7gznfSkrUBzjss3Yc2SmMYh5Q5LTw7mEBxXBKZLphb6E0zuXlejADpSgsogEkkbGfmJbKZi05qEwdcTgJYUs/jBjNgjB8EMUCgYANynrEE+NITyUiJUY+SQC/nQoHE4eg3We6WjbeZDrkcfH3ZxakJffLm5ar4xVBGYWYD8WK1HwotHXCuPE0+rVkHh0zAo9RYGVID1HfyrboRNmpAB8B5tOsjpFLZPTCjZ+CZREEzwiEKskFYocoXT7YX74HHVhmBW+Pu16ojxbaQQKBgHshqTQj3xemLCSoSnCKQabeA240alrlk/BCI+ZCYh2//FUL2bWdL1L/4U0iXkcFsweh91bwFW4AKsY9Q+CJhjwABNcFUua3mT4iL1dp+fzbgYdHDsU57qTv8dQOuG3inqr+pbsnDzY8UCVR+Qw4CIAiTp6gjuk554dMCdCqp7k0")));
        String url = host+"/PayWeb/refund/refundBack.do";
        log.info("refundBack paramMap:" + input);
        HttpJson resultStr = HttpRequestClient.httpRequest(url, paramMap);
        log.info("refundBack resultStr:" + resultStr.getMsg());
        return resultStr;
    }

    /**
     * 撤销
     * @param channel 渠道编号
     * @param merId 商户号
     * @param key 预分配加密要素
     * @param outerNumber 商户订单号
     * @param oldOuterNumber 原订单编号
     * @param txnSeqId 内部订单号
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static HttpJson revokeMicrosPay(String host,String channel,String merId,String key,String outerNumber,String oldOuterNumber,String txnSeqId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Map<String, Object> input = new HashMap<>();
        input.put("channel", channel); //渠道编号
        input.put("outerNumber", outerNumber);//商户订单号
        input.put("oldOuterNumber", oldOuterNumber);//原订单编号
        input.put("txnSeqId", txnSeqId);//内部订单号
        input.put("merId", merId);//商户号
        input.put("sign", signMd5(input, key));// MD5加密
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("params", signBase64(input));
        String url = host+"/PayWeb/pay/unified/revokeMicrosPay.do";
        HttpJson resultStr = HttpRequestClient.httpRequest(url, paramMap);
        log.info("revokeMicrosPay resultStr:" + resultStr.getMsg());
        return resultStr;
    }


    private static String signMd5(Map<String, Object> order, String key)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {
        /**************************
         * sign值获取排序md5加密************************************8
         */
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : order.entrySet()) {
            list.add(entry.getKey() + "=" + entry.getValue() + "&");
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "key=" + key;
        log.info("Sign Before MD5:" + result);
        result = string2MD5(result).toUpperCase();
        log.info("Sign Result:" + result);
        return result;
    }

    /***
     * MD5加码 生成32位md5码
     *
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String string2MD5(String inStr) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String resultString = null;
        try {
            resultString = inStr;
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteArrayToHexString(md.digest(resultString.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    protected static String signBase64(Map<String, Object> order) throws UnsupportedEncodingException {
        /************************ Base64加密 ****************************************/
//        JSONObject json = JSON.parseObject();
        return Base64.getEncoder().encodeToString(JSON.toJSONString(order).getBytes(StandardCharsets.UTF_8));
    }

    public static String byteArrayToHexString(byte[] b) {
        StringBuilder resultSb = new StringBuilder();
        for (byte aB : b) {
            resultSb.append(byteToHexString(aB));
        }
        return resultSb.toString();
    }

    /**
     * 转换byte到16进制
     *
     * @param b 要转换的byte
     * @return 16进制格式
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    private final static String[] hexDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f"};

    public static void refund(String channel, String outerDtTm, String initOrderNumber, String initOrderTime, String merId, String amount) throws Exception {
        HashMap<String, Object> input = new HashMap<String, Object>();
        input.put("channel", channel);
        input.put("outerNumber", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        input.put("outerDtTm", outerDtTm);
        input.put("orderAmount", amount);
        input.put("initOrderNumber", initOrderNumber);
        input.put("initOrderTime", initOrderTime);
        input.put("currencyCode", "156");
        input.put("merId", merId);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("params", SignUtil.signBase64(Rsa256Sign.getSignMap(input,   "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCY5A1HuentN6XZ1i4kdssh3GYDzPZZ7bMp1A0kwRkmdYZy1XtCbBeT/MQb48WloKdg2/3qKLP5ojSIwGiRn4s4G7TE0vnzfaEsUyy4rJwBvPaueLyUJgaSlmarrumH44jF0dFxnrNrFFUsTwDWROjnbHfAsUU8r6EdK8kp/X8Z/OszH/QeLfxKR4Jn0DQ+HWqd1Fh0PE8Xa6bc7ZTrKO8rwb7Hl89GyXo4VxdZUleMu9u5AAvgn6Y7JmW4kmN3ND3CJfrw7nCV+LCvdYfkBlrkl16hMDZ8AjoizYA6NK4Ccx9AnsMkYaUrugErm+LAueywqgn0g7qaAMUTpqv2CN3HAgMBAAECggEBAJFzg97U0+NFqZbP2B4Alcek8bhz6XosVOJaRhbNTav06u+cgSLzyoqTUc5mpsWnTRUstSdriazXz7q5scG4Uq4STatiO0CWKBsPFeCq9dABgX0CiAU5F8+lHg/+21TX9mqb7fF11aLtOQpqLpWqARkJa/P5sbDGNTSXT02PSLfjF42LpZA3bECyCw7kC6BaBBJeaaMPdBwPDs1vxbG+RjWA/y32gmJDfPALWCCnFmXx+YaWXAAc6RhPeEz6lOiRYUGwTdYfzpRM9kSj6CQ5GfC+IeU4rcsKw6Hs7S9sdvi9Ca6quzE9MkMOhl0A8u3BOJgQjeifP2iSzJhx1eeU2eECgYEA2ZDbxs0fdcyaxkj3gjoUXgnzTvXa9Fc8IMNcdXnQDAYWUDFyHAICSItlGF1XMIZ63hPgxNgzXeKugWESfMFOe+COEmGgP+fm+6fOXORQQikbtyArVz/VaccnOclG7SQm7C9degb2iRU+HhS74EThshuhomc2cRpv4iZsDPtXKCUCgYEAs+ZYyvARvhXh5nDv4CpYfXEANa84eoyqykpA2J3SyTbzboZsML6SBD+1X+K1NO5nn0EZ0Gi2eyPLX1KLTt/qviPwkRvWnvdfAQy8STO0VJLZkm9JepcctRucOjQvq1TlGLEXVMrevH/bUh39SzsRaEji/8Ok3ARcZAWf/xHoBHsCgYEAsz4l4vZRifKEPQIWcAK7jvIxswbYWd+Y2QE6pkoyPgWx0mW6NhrtmKjHJDiaQY85oMbNsj7QqvO7gznfSkrUBzjss3Yc2SmMYh5Q5LTw7mEBxXBKZLphb6E0zuXlejADpSgsogEkkbGfmJbKZi05qEwdcTgJYUs/jBjNgjB8EMUCgYANynrEE+NITyUiJUY+SQC/nQoHE4eg3We6WjbeZDrkcfH3ZxakJffLm5ar4xVBGYWYD8WK1HwotHXCuPE0+rVkHh0zAo9RYGVID1HfyrboRNmpAB8B5tOsjpFLZPTCjZ+CZREEzwiEKskFYocoXT7YX74HHVhmBW+Pu16ojxbaQQKBgHshqTQj3xemLCSoSnCKQabeA240alrlk/BCI+ZCYh2//FUL2bWdL1L/4U0iXkcFsweh91bwFW4AKsY9Q+CJhjwABNcFUua3mT4iL1dp+fzbgYdHDsU57qTv8dQOuG3inqr+pbsnDzY8UCVR+Qw4CIAiTp6gjuk554dMCdCqp7k0")));
        String url = "http://paytest.jnbank.cn/PayWeb/refund/refundBack.do";
        HttpJson resultStr = HttpRequestClient.httpRequest(url, paramMap);
        System.out.println("返回"+resultStr);
    }
}


