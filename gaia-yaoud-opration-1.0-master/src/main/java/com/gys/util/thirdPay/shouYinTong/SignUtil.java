package com.gys.util.thirdPay.shouYinTong;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;


public class SignUtil {

    public static String getSign(Map<String, Object> order, String key) throws UnsupportedEncodingException {
        order.put("sign", signMd5(order, key));
        return signBase64(order);
    }

    protected static String signMd5(Map<String, Object> order, String key) {
        /**************************sign值获取排序md5加密************************************8*/
        ArrayList<String> list = new ArrayList<String>();
        for (Map.Entry<String, Object> entry : order.entrySet()) {
            if (entry.getValue() != "") {
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        System.err.println(result);
        if (!StrUtil.isEmpty(key)) {
            result += "key=" + key;
        }
        System.err.println(result);
        result = string2MD5(result).toUpperCase();
        return result;
    }

    /***
     * MD5加码 生成32位md5码
     */
    public static String string2MD5(String inStr) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];
        for (int i = 0; i < charArray.length; i++)
            byteArray[i] = (byte) charArray[i];
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    public static String signBase64(Map<String, Object> order) {
        /************************Base64加密****************************************/
        return Base64.getEncoder().encodeToString(JSON.toJSONString(order).getBytes(StandardCharsets.UTF_8));
    }
}
