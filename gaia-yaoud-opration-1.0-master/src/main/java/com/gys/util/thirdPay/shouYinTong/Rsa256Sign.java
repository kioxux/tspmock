package com.gys.util.thirdPay.shouYinTong;

import org.springframework.util.StringUtils;

import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

public class Rsa256Sign {

    public static HashMap<String, Object> getSignMap(HashMap<String, Object> params, String privateKey) throws Exception {
        return getSignMap(params, privateKey, "UTF-8");
    }

    public static HashMap<String, Object> getSignMap(HashMap<String, Object> params, String privateKey, String charset) throws Exception {
        String sortParams = getSortContent(params);

        String sign = rsa256Sign(sortParams, privateKey, charset);
        params.put("sign", sign);
        return params;
    }

    public static boolean rsa256Check(Map<String, Object> params, String publicKey) throws Exception {
        return rsa256Check(params, publicKey, "UTF-8");
    }

    public static boolean rsa256Check(Map<String, Object> params, String publicKey, String charset) throws Exception {
        String sign = (String) params.get("sign");
        String content = getSignCheckContent(params);
        return rsa256CheckContent(content, sign, publicKey, charset);
    }

    public static boolean rsa256CheckContent(String content, String sign, String publicKey, String charset) throws Exception {
        PublicKey pubKey = getPublicKeyFromX509("RSA", new ByteArrayInputStream(publicKey.getBytes()));
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initVerify(pubKey);
        if (StringUtils.isEmpty(charset)) {
            signature.update(content.getBytes());
        } else {
            signature.update(content.getBytes(charset));
        }

        return signature.verify(Base64.getDecoder().decode(sign.getBytes()));
    }

    public static PublicKey getPublicKeyFromX509(String algorithm, InputStream ins) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        StringWriter writer = new StringWriter();
        io(new InputStreamReader(ins), writer);
        byte[] encodedKey = writer.toString().getBytes();
        encodedKey = Base64.getDecoder().decode(encodedKey);
        return keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
    }

    public static String getSortContent(Map<String, Object> params) {
        ArrayList<String> list = new ArrayList<String>();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            if (!StringUtils.isEmpty(String.valueOf( entry.getValue()))) {
                list.add("&" + entry.getKey() + "=" + entry.getValue());
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String rs = sb.toString().replaceFirst("&", "");
        return rs;
    }

    public static String getSignCheckContent(Map<String, Object> params) {
        if (params == null) {
            return null;
        } else {
            params.remove("sign");
            return getSortContent(params);
        }
    }

    public static String rsa256Sign(String content, String privateKey, String charset) throws Exception {
        PrivateKey priKey = getPrivateKeyFromPKCS8("RSA", new ByteArrayInputStream(privateKey.getBytes()));
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initSign(priKey);
        if (StringUtils.isEmpty(charset)) {
            signature.update(content.getBytes());
        } else {
            signature.update(content.getBytes(charset));
        }

        byte[] signed = signature.sign();
        return new String(Base64.getEncoder().encode(signed));
    }

    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm, InputStream ins) throws Exception {
        if (ins != null && !StringUtils.isEmpty(algorithm)) {
            KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
            byte[] encodedKey = readText(ins).getBytes();
            encodedKey = Base64.getDecoder().decode(encodedKey);
            return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedKey));
        } else {
            return null;
        }
    }

    public static String readText(InputStream in) throws IOException {
        return readText(in, (String) null, -1);
    }

    public static String readText(InputStream in, String encoding, int bufferSize) throws IOException {
        Reader reader = encoding == null ? new InputStreamReader(in) : new InputStreamReader(in, encoding);
        return readText(reader, bufferSize);
    }

    public static String readText(Reader reader, int bufferSize) throws IOException {
        StringWriter writer = new StringWriter();
        io((Reader) reader, (Writer) writer, bufferSize);
        return writer.toString();
    }
    public static void io(Reader in, Writer out) throws IOException {
        io((Reader)in, (Writer)out, -1);
    }

    public static void io(Reader in, Writer out, int bufferSize) throws IOException {
        if (bufferSize == -1) {
            bufferSize = 4096;
        }

        char[] buffer = new char[bufferSize];

        int amount;
        while ((amount = in.read(buffer)) >= 0) {
            out.write(buffer, 0, amount);
        }

    }

    public static void main(String[] args) throws Exception {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("merId", "1212");
        params.put("sdsd", 1212);
        params = getSignMap(params, "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCY5A1HuentN6XZ1i4kdssh3GYDzPZZ7bMp1A0kwRkmdYZy1XtCbBeT/MQb48WloKdg2/3qKLP5ojSIwGiRn4s4G7TE0vnzfaEsUyy4rJwBvPaueLyUJgaSlmarrumH44jF0dFxnrNrFFUsTwDWROjnbHfAsUU8r6EdK8kp/X8Z/OszH/QeLfxKR4Jn0DQ+HWqd1Fh0PE8Xa6bc7ZTrKO8rwb7Hl89GyXo4VxdZUleMu9u5AAvgn6Y7JmW4kmN3ND3CJfrw7nCV+LCvdYfkBlrkl16hMDZ8AjoizYA6NK4Ccx9AnsMkYaUrugErm+LAueywqgn0g7qaAMUTpqv2CN3HAgMBAAECggEBAJFzg97U0+NFqZbP2B4Alcek8bhz6XosVOJaRhbNTav06u+cgSLzyoqTUc5mpsWnTRUstSdriazXz7q5scG4Uq4STatiO0CWKBsPFeCq9dABgX0CiAU5F8+lHg/+21TX9mqb7fF11aLtOQpqLpWqARkJa/P5sbDGNTSXT02PSLfjF42LpZA3bECyCw7kC6BaBBJeaaMPdBwPDs1vxbG+RjWA/y32gmJDfPALWCCnFmXx+YaWXAAc6RhPeEz6lOiRYUGwTdYfzpRM9kSj6CQ5GfC+IeU4rcsKw6Hs7S9sdvi9Ca6quzE9MkMOhl0A8u3BOJgQjeifP2iSzJhx1eeU2eECgYEA2ZDbxs0fdcyaxkj3gjoUXgnzTvXa9Fc8IMNcdXnQDAYWUDFyHAICSItlGF1XMIZ63hPgxNgzXeKugWESfMFOe+COEmGgP+fm+6fOXORQQikbtyArVz/VaccnOclG7SQm7C9degb2iRU+HhS74EThshuhomc2cRpv4iZsDPtXKCUCgYEAs+ZYyvARvhXh5nDv4CpYfXEANa84eoyqykpA2J3SyTbzboZsML6SBD+1X+K1NO5nn0EZ0Gi2eyPLX1KLTt/qviPwkRvWnvdfAQy8STO0VJLZkm9JepcctRucOjQvq1TlGLEXVMrevH/bUh39SzsRaEji/8Ok3ARcZAWf/xHoBHsCgYEAsz4l4vZRifKEPQIWcAK7jvIxswbYWd+Y2QE6pkoyPgWx0mW6NhrtmKjHJDiaQY85oMbNsj7QqvO7gznfSkrUBzjss3Yc2SmMYh5Q5LTw7mEBxXBKZLphb6E0zuXlejADpSgsogEkkbGfmJbKZi05qEwdcTgJYUs/jBjNgjB8EMUCgYANynrEE+NITyUiJUY+SQC/nQoHE4eg3We6WjbeZDrkcfH3ZxakJffLm5ar4xVBGYWYD8WK1HwotHXCuPE0+rVkHh0zAo9RYGVID1HfyrboRNmpAB8B5tOsjpFLZPTCjZ+CZREEzwiEKskFYocoXT7YX74HHVhmBW+Pu16ojxbaQQKBgHshqTQj3xemLCSoSnCKQabeA240alrlk/BCI+ZCYh2//FUL2bWdL1L/4U0iXkcFsweh91bwFW4AKsY9Q+CJhjwABNcFUua3mT4iL1dp+fzbgYdHDsU57qTv8dQOuG3inqr+pbsnDzY8UCVR+Qw4CIAiTp6gjuk554dMCdCqp7k0");
        System.err.println(rsa256Check(params, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmOQNR7np7Tel2dYuJHbLIdxmA8z2We2zKdQNJMEZJnWGctV7QmwXk/zEG+PFpaCnYNv96iiz+aI0iMBokZ+LOBu0xNL5832hLFMsuKycAbz2rni8lCYGkpZmq67ph+OIxdHRcZ6zaxRVLE8A1kTo52x3wLFFPK+hHSvJKf1/GfzrMx/0Hi38SkeCZ9A0Ph1qndRYdDxPF2um3O2U6yjvK8G+x5fPRsl6OFcXWVJXjLvbuQAL4J+mOyZluJJjdzQ9wiX68O5wlfiwr3WH5AZa5JdeoTA2fAI6Is2AOjSuAnMfQJ7DJGGlK7oBK5viwLnssKoJ9IO6mgDFE6ar9gjdxwIDAQAB"));
    }
}
