package com.gys.util.thirdPay.shouYinTong;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaSdSytOperationMapper;
import com.gys.business.mapper.entity.GaiaSdSytOperation;
import com.gys.common.data.ResultVO;
import com.gys.common.exception.BusinessException;
import com.gys.common.http.HttpJson;
import com.gys.util.CommonUtil;
import com.gys.util.thirdPay.ThirdPayInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SytBusiness implements ThirdPayInterface {

    @Value("${thirdPay.shouyintong}")
    private String host;

    @Autowired
    private GaiaSdSytOperationMapper sytOperationMapper;

    @Override
    public ResultVO<Map<String, Object>> pay(Map<String, Object> params) {
        ResultVO<Map<String, Object>> res = new ResultVO<>();
        res.setSuccess(false);
        //支付请求
        try {
            String channel = (String) params.get("channel");
            String merId = (String) params.get("merId");
            String key = (String) params.get("key");
            String outerNumber = (String) params.get("outerNumber");
            String time = (String) params.get("time");
            String authCode = (String) params.get("authCode");
            String orderAmount = String.format("%012d", Integer.parseInt(CommonUtil.bigDecimalTo2fStr(new BigDecimal((String) params.get("orderAmount"))).replace(".","")));
            String note = (String) params.get("note");
            HttpJson httpJson = BizUtil.unifiedMicrosPay(host,channel, merId, key, outerNumber, time, authCode, orderAmount, note);
            if (httpJson.isSuccess()) {
                Map<String, Object> payMaps = (Map<String, Object>) JSON.parse(httpJson.getMsg());
                //只有当 respCode 为 0000，并且txnSta 为 02，才表示该笔交易支付成功
                if ("0000".equals(payMaps.get("respCode"))) { //成功
                    GaiaSdSytOperation sytOperation = mapToBean(payMaps);
                    sytOperation.setClient((String) params.get("client"));
                    sytOperation.setBrId((String) params.get("brId"));
                    sytOperation.setOperation("支付");
                    sytOperation.setResult("0");
                    if ("02".equals(payMaps.get("txnSta"))) { //支付成功
                        res.setSuccess(true);
                        res.setObj(payMaps);
                    } else if ("01".equals(payMaps.get("txnSta"))) {//支付中
                        res.setSuccess(true);
                        res.setObj(payMaps);
                    } else {//03支付失败
                        res.setSuccess(false);
                        res.setMsg("支付失败：" + payMaps.get("respMsg"));
                        sytOperation.setResultContent((String)payMaps.get("respMsg"));
                    }
                    Date date = new Date();
                    sytOperation.setCreateDate(date);
                    sytOperation.setUpdateDate(date);
                    sytOperation.setCreateEmp((String) params.get("userId"));
                    sytOperation.setUpdateEmp((String) params.get("userId"));
                    sytOperationMapper.insert(sytOperation);
                } else {
                    res.setSuccess(false);
                    res.setMsg("接口失败：" + payMaps.get("respMsg"));
                }
            } else {
                res.setSuccess(false);
                res.setMsg(httpJson.getMsg());
            }
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            res.setMsg(e.getMessage());
        }
        return res;
    }

    @Override
    public ResultVO<Map<String, Object>> query(Map<String, Object> params) {
        ResultVO<Map<String, Object>> res = new ResultVO<>();
        res.setSuccess(false);
        try {
            String channel = (String) params.get("channel");
            String merId = (String) params.get("merId");
            String key = (String) params.get("key");
            String orderNumber = (String) params.get("orderNumber");//订单号
            HttpJson httpJson = BizUtil.query(host,channel, merId, key, orderNumber);
            if (httpJson.isSuccess()) {
                Map<String, Object> queryMaps = (Map<String, Object>) JSON.parse(httpJson.getMsg());
                //只有当 respCode 为 0000，并且txnSta 为 02，才表示该笔交易支付成功
                if ("0000".equals(queryMaps.get("respCode"))) { //成功
                    res.setSuccess(true);
                    res.setObj(queryMaps);

                    Example example = new Example(GaiaSdSytOperation.class);
                    example.createCriteria().andEqualTo("client",params.get("client")).andEqualTo("brId",params.get("brId")).andEqualTo("outerNumber",orderNumber);
                    GaiaSdSytOperation sytOperation = sytOperationMapper.selectOneByExample(example);

                    if ("02".equals(queryMaps.get("txnSta"))) { //交易成功
                        res.setSuccess(true);
                        res.setObj(queryMaps);
                        sytOperation.setTxnSta("02");
                        sytOperation.setRealPayAmount(StrUtil.isEmpty((String)queryMaps.get("realPayAmount"))?null:new BigDecimal((String)queryMaps.get("realPayAmount")));
                        sytOperation.setDiscountAmount(StrUtil.isEmpty((String)queryMaps.get("discountAmount"))?null:new BigDecimal((String)queryMaps.get("discountAmount")));
                    } else if ("01".equals(queryMaps.get("txnSta"))) { //待支付
                        sytOperation.setTxnSta("01");
                        res.setSuccess(false);
                        res.setObj(queryMaps);
                        res.setMsg("支付等待处理中；");
                    } else {
                        sytOperation.setTxnSta(String.valueOf(queryMaps.get("txnSta")));
                        res.setSuccess(false);
                        res.setMsg("支付失败：" + queryMaps.get("respMsg"));
                        res.setObj(queryMaps);
                        sytOperation.setTxnSta("03");
                        sytOperation.setResultContent((String)queryMaps.get("respMsg"));
                    }
                    sytOperationMapper.updateByExample(sytOperation,example);
                } else {
                    res.setSuccess(false);
                    res.setMsg("接口失败：" + queryMaps.get("respMsg"));
                }
            } else {
                res.setSuccess(false);
                res.setMsg(httpJson.getMsg());
            }
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            res.setMsg(e.getMessage());
        }
        return res;
    }

    @Override
    public ResultVO<Map<String, Object>> refundBack(Map<String, Object> params) {
        ResultVO<Map<String, Object>> res = new ResultVO<>();
        res.setSuccess(false);
        try {
            ResultVO<Map<String, Object>> queryVo = query(params);
            if(!queryVo.isSuccess()) {
                throw new BusinessException(queryVo.getMsg());
            }
            String channel = (String) params.get("channel");
            String merId = (String) params.get("merId");
            String key = (String) params.get("key");
            String outerNumber = (String) params.get("outerNumber");//订单号
            Date date = new Date();
            String outerDtTm = new SimpleDateFormat("yyyyMMddHHmmss").format(date);//订单时间
            String orderAmount = String.format("%012d", Integer.parseInt(CommonUtil.bigDecimalTo2fStr(new BigDecimal((String) params.get("orderAmount"))).replace(".","")));//退货金额
            String initOrderNumber = (String)queryVo.getObj().get("txnSeqId");//流水号  txnSeqId
            String initOrderTime = (String) queryVo.getObj().get("txnDate");// 销售日期 txnDate
            HttpJson httpJson = BizUtil.refundBack(host,channel, merId, key, outerNumber, outerDtTm, orderAmount, initOrderNumber, initOrderTime);
            if (httpJson.isSuccess()) {
                Map<String, Object> backMaps = (Map<String, Object>) JSON.parse(httpJson.getMsg());
                //只有当 respCode 为 0000，表示该笔交易退款成功
                if ("0000".equals(backMaps.get("respCode"))) { //成功
                    res.setSuccess(true);
                    res.setObj(backMaps);
                    Map<String,Object> backParams = new HashMap<>();
                    backParams.put("channel",channel);
                    backParams.put("merId",merId);
                    backParams.put("key",key);
                    backParams.put("orderNumber",outerNumber);
                    ResultVO<Map<String, Object>> queryRefundVo = query(backParams);

                    GaiaSdSytOperation sytOperation = new GaiaSdSytOperation();
                    sytOperation.setClient((String)params.get("client"));
                    sytOperation.setBrId((String)params.get("brId"));
                    sytOperation.setChannel(channel);
                    sytOperation.setMerId(merId);
                    sytOperation.setTransType("34");
                    sytOperation.setOuterNumber(outerNumber);
                    sytOperation.setOuterDtTm(outerDtTm);
                    sytOperation.setOrderAmount(new BigDecimal((String)params.get("orderAmount")));
                    sytOperation.setOperation("退货");
                    sytOperation.setResult("1");
                    sytOperation.setInitOrderNumber(initOrderNumber);

                    if(queryVo.isSuccess()) {
//                        GaiaSdSytOperation sytOperation2 = mapToBean(queryVo.getObj());
                        sytOperation.setMchtName((String)queryRefundVo.getObj().get("mchtName"));
                        sytOperation.setTxnSeqId((String)queryRefundVo.getObj().get("txnSeqId"));
                        sytOperation.setPayType((String)queryRefundVo.getObj().get("payType"));
                        sytOperation.setTxnSta((String)queryRefundVo.getObj().get("txnSta"));
                        sytOperation.setCurrencyCode((String)queryRefundVo.getObj().get("currencyCode"));
                        sytOperation.setTxnDate((String)queryRefundVo.getObj().get("txnDate"));
                        sytOperation.setTxnTime((String)queryRefundVo.getObj().get("txnTime"));
                    }
                    sytOperation.setCreateDate(date);
                    sytOperation.setUpdateDate(date);
                    sytOperation.setCreateEmp((String) params.get("userId"));
                    sytOperation.setUpdateEmp((String) params.get("userId"));
                    sytOperationMapper.insert(sytOperation);
                } else {
                    res.setSuccess(false);
                    res.setMsg("接口失败：" + backMaps.get("respMsg"));
                }
            } else {
                res.setSuccess(false);
                res.setMsg(httpJson.getMsg());
            }
        } catch (Exception e) {
            res.setMsg(e.getMessage());
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public ResultVO<Map<String, Object>> revokePay(Map<String, Object> params) {
        ResultVO<Map<String, Object>> res = new ResultVO<>();
        res.setSuccess(false);
        try {
            String channel = (String) params.get("channel");
            String merId = (String) params.get("merId");
            String key = (String) params.get("key");
            String outerNumber = (String) params.get("outerNumber");
            String oldOuterNumber = (String) params.get("oldOuterNumber");
            String txnSeqId = (String) params.get("txnSeqId");
            HttpJson httpJson = BizUtil.revokeMicrosPay(host,channel, merId, key, outerNumber, oldOuterNumber, txnSeqId);
            if (httpJson.isSuccess()) {
                Map<String, Object> backMaps = (Map<String, Object>) JSON.parse(httpJson.getMsg());
                Example example = new Example(GaiaSdSytOperation.class);
                example.createCriteria().andEqualTo("client",params.get("client")).andEqualTo("brId",params.get("brId")).andEqualTo("outerNumber",outerNumber);
                GaiaSdSytOperation sytOperation = sytOperationMapper.selectOneByExample(example);
                sytOperation.setOperation("撤单");
                //只有当 respCode 为 0000，并且txnSta 为 02，才表示该笔交易支付成功
                if ("0000".equals(backMaps.get("respCode"))) { //成功
                    if ("02".equals(backMaps.get("txnSta"))) { //成功
                        res.setSuccess(true);
                        res.setObj(backMaps);
                        sytOperation.setTxnSta("02");
                        sytOperation.setResult("1");
                    } else {
                        res.setSuccess(false);
                        res.setMsg("撤单失败：" + backMaps.get("respMsg"));
                        sytOperation.setTxnSta((String)backMaps.get("txnSta"));
                        sytOperation.setResult("0");
                        sytOperation.setResultContent((String)backMaps.get("respMsg"));
                    }
                } else {
                    res.setSuccess(false);
                    res.setMsg("撤单失败：" + backMaps.get("respMsg"));
                    sytOperation.setTxnSta((String)backMaps.get("txnSta"));
                    sytOperation.setResult("0");
                    sytOperation.setResultContent((String)backMaps.get("respMsg"));
                }
                Date date = new Date();
                sytOperation.setUpdateDate(date);
                sytOperation.setUpdateEmp((String) params.get("userId"));
                sytOperationMapper.updateByExample(sytOperation,example);
            } else {
                res.setSuccess(false);
                res.setMsg(httpJson.getMsg());
            }
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            res.setMsg(e.getMessage());
        }
        return res;
    }


    private GaiaSdSytOperation mapToBean(Map<String,Object> payMaps){
        GaiaSdSytOperation sytOperation = new GaiaSdSytOperation();
        sytOperation.setClient((String) payMaps.get("client"));
        sytOperation.setBrId((String) payMaps.get("brId"));
        sytOperation.setChannel((String) payMaps.get("channel"));
        sytOperation.setMerId((String) payMaps.get("merId"));
        sytOperation.setMchtName((String) payMaps.get("mchtName"));
        sytOperation.setTxnSeqId((String) payMaps.get("txnSeqId"));
        sytOperation.setTxnDate((String) payMaps.get("txnDate"));
        sytOperation.setTxnTime((String) payMaps.get("txnTime"));
        sytOperation.setTransType((String) payMaps.get("transType"));
        sytOperation.setPayType((String) payMaps.get("payType"));
        sytOperation.setOuterNumber((String) payMaps.get("outerNumber"));
        sytOperation.setOuterDtTm((String) payMaps.get("outerDtTm"));
        sytOperation.setCurrencyCode((String) payMaps.get("currencyCode"));
        BigDecimal divideDecimal = new BigDecimal("100");
        sytOperation.setOrderAmount(new BigDecimal(((String) payMaps.get("orderAmount")).replaceAll("^(0+)","")).divide(divideDecimal, 2,BigDecimal.ROUND_HALF_UP));
        sytOperation.setRealPayAmount(StrUtil.isEmpty((String)payMaps.get("realPayAmount"))?null:new BigDecimal((String) payMaps.get("realPayAmount")));
        sytOperation.setDiscountAmount(StrUtil.isEmpty((String)payMaps.get("discountAmount"))?null:new BigDecimal((String) payMaps.get("discountAmount")));
        sytOperation.setTxnSta((String) payMaps.get("txnSta"));
        sytOperation.setUpdateDate(new Date());
        return sytOperation;
    }
}
