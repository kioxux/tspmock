package com.gys.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.gys.business.service.data.RecipelInfoInputData;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * @author xiaoyuan
 */
@SuppressWarnings("all")
public class Util {
    public static String getExceptionInfo(Exception ex) {
        String sOut = "";
        sOut = sOut + ex.getMessage() + "\r\n";
        StackTraceElement[] trace = ex.getStackTrace();
        StackTraceElement[] var3 = trace;
        int var4 = trace.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            StackTraceElement s = var3[var5];
            sOut = sOut + "\tat " + s + "\r\n";
        }

        return sOut;
    }

    /**
     * 不够位数的在前面补0，保留code的长度位数字
     *
     * @param code
     * @return
     */
    public static String autoGenericCode(String code) {
        String result = "";
        // 保留code的位数
        result = String.format("%0" + code.length() + "d", Integer.parseInt(code) + 1);

        return result;
    }

    /**
     * 金额正则判断
     *
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        // 判断小数点后4位的数字的正则表达式
        Pattern pattern = compile(UtilMessage.AMOUNT_REGULAR2);
        Matcher match = pattern.matcher(str);
        return match.matches();
    }

    /**
     * 数量正则判断
     *
     * @param str
     * @return
     */
    public static boolean quantityJudgment(String str) {
        //判断数量正则
        Pattern pattern = compile(UtilMessage.NUMBER_REGULAR);
        Matcher match = pattern.matcher(str);
        return match.matches();
    }

    /**
     * 金额正则判断2
     *
     * @param str
     * @return
     */
    public static boolean isNumber2(String str) {
        // 判断小数点后4位的数字的正则表达式
        Pattern pattern = compile(UtilMessage.AMOUNT_REGULAR);
        Matcher match = pattern.matcher(str);
        return match.matches();
    }

    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime   当前时间
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     * @author fanjz
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime() || nowTime.getTime() == endTime.getTime()) {
            return true;
        }
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);
        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 获取文件后缀名
     */
    public static String getTheSuffix(String name) {
        String suffix = name.substring(name.lastIndexOf("_") + 1);
        return suffix;
    }

    /**
     * 判断是否为数字，包含负数情况
     *
     * @param str
     * @return
     */
    public static boolean isNumbers(String str) {
        Boolean flag = false;
        String tmp;
        if (StringUtils.isNotBlank(str)) {
            if (str.startsWith("-")) {
                tmp = str.substring(1);
            } else {
                tmp = str;
            }
            flag = tmp.matches("^[0.0-9.0]+$");
        }
        return flag;
    }

    /**
     * 判断日期
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    public static boolean belongCalendar(Date beginTime, Date endTime) {
        if (DateUtil.isSameDay(beginTime, endTime)) {
            return true;
        }
        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        if (begin.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断时间
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    public static boolean belongCalendarTime(Date beginTime, Date endTime) {
        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        if (begin.before(end)) {
            return true;
        } else {
            return false;
        }
    }


    public static Map<String, Object> jsonToMap(String str_json) {
        Map<String, Object> res = null;
        try {
            Gson gson = new Gson();
            res = gson.fromJson(str_json, new TypeToken<Map<String, Object>>() {
            }.getType());
        } catch (JsonSyntaxException e) {
        }
        return res;
    }

    public static boolean compTime(String s1, String s2) {
        try {
            if (s1.indexOf(":") < 0 || s1.indexOf(":") < 0) {
                return false;
            } else {
                String[] array1 = s1.split(":");
                int total1 = Integer.valueOf(array1[0]) * 3600 + Integer.valueOf(array1[1]) * 60 + Integer.valueOf(array1[2]);
                String[] array2 = s2.split(":");
                int total2 = Integer.valueOf(array2[0]) * 3600 + Integer.valueOf(array2[1]) * 60 + Integer.valueOf(array2[2]);
                return total1 - total2 > 0 ? true : false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 手机号验证
     *
     * @param phone
     * @return
     */
    public static boolean checkPhone(String phone) {
        Pattern p = compile("^1[0-9]{10}$");
        Matcher m = p.matcher(phone);
        return m.matches();
    }

    /**
     * 身份号验证
     *
     * @param IDNumber
     * @return
     */
    public static boolean isIDNumber(String IDNumber) {
        if (IDNumber == null || "".equals(IDNumber)) {
            return false;
        }
        String regularExpression = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
        boolean matches = IDNumber.matches(regularExpression);
        if (matches) {
            if (IDNumber.length() == 18) {
                try {
                    char[] charArray = IDNumber.toCharArray();
                    int[] idCardWi = new int[]{7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
                    String[] idCardY = new String[]{"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
                    int sum = 0;
                    for (int i = 0; i < idCardWi.length; ++i) {
                        int current = Integer.parseInt(String.valueOf(charArray[i]));
                        int count = current * idCardWi[i];
                        sum += count;
                    }
                    char idCardLast = charArray[17];
                    int idCardMod = sum % 11;
                    if (idCardY[idCardMod].toUpperCase().equals(String.valueOf(idCardLast).toUpperCase())) {
                        return true;
                    }
                    System.out.println("身份证最后一位:" + String.valueOf(idCardLast).toUpperCase() + "错误,正确的应该是:" + idCardY[idCardMod].toUpperCase());
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return false;
        }
        return matches;
    }

    /**
     * 判断该对象属性是否null
     * @param obj
     * @return
     * @throws Exception
     */
    public static boolean isAllFieldNull(Object obj) {
        Class stuCla = (Class) obj.getClass(); // 得到类对象
        Field[] fs = stuCla.getDeclaredFields();//得到属性集合
        boolean flag = false;
        for (Field f : fs) {//遍历属性
            f.setAccessible(true);// 设置属性是可以访问的(私有的也可以)
            Type type = f.getGenericType();//拿到 当前属性 类型
            if ("long".equals(type.toString()) || "boolean".equals(type.toString())) {//过滤点 long 对象和boolean 类型
                continue;
            }
            Object o = null;// 得到此属性的值
            try {
                o = f.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (o != null) {//只要有1个属性不为空 直接返回
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static BigDecimal divide(BigDecimal lastYearSalesAmt, BigDecimal salesAmt2) {
        lastYearSalesAmt = ObjectUtil.isNotEmpty(lastYearSalesAmt) ? lastYearSalesAmt : BigDecimal.ZERO;
        salesAmt2 = ObjectUtil.isNotEmpty(salesAmt2) ? salesAmt2 : BigDecimal.ZERO;
        return salesAmt2.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : lastYearSalesAmt.divide(salesAmt2, 4, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal checkNull(Double num) {
        BigDecimal bigDecimal = ObjectUtil.isNotEmpty(num) ? new BigDecimal(num.toString()) : BigDecimal.ZERO;
        return bigDecimal;
    }

    public static BigDecimal checkNull(BigDecimal num) {
        BigDecimal bigDecimal = ObjectUtil.isNotEmpty(num) ? num : BigDecimal.ZERO;
        return bigDecimal;
    }
//    public static void main(String[] args) throws Exception {
////        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
////        String beginTime = "10:38:05";
////        String endTime = "09:37:25";
////
////        boolean b = compTime(endTime, beginTime);
////        System.out.println(b);
//
////        TableMap<String, Integer> tableMap = new TableMap<>(8);
////        tableMap.put("1111",1111);
////        tableMap.put("1111",2222);
////        tableMap.put("2222",3333);
////        tableMap.put("1111",4444);
////        tableMap.put("1111",55555);
////        System.out.println(tableMap);
////        Integer integer = tableMap.get("2222");
////        System.out.println(integer);
////        BigDecimal AA  = null;
////
////        BigDecimal decimal = StrUtil.isBlank(StrUtil.toString(AA)) ? BigDecimal.ZERO : new BigDecimal("1111");
////        System.out.println(decimal);
////        String aa = null;
////        String anElse = Optional.ofNullable(aa).orElse("有值");//三木
//////        assertEquals
////        System.out.println(anElse);
//
////        System.out.println(IdcardUtil.isValidCard("230402199301210211"));
//
//        RecipelInfoInputData infoInputData = new RecipelInfoInputData();
//        System.out.println(infoInputData.toString());
////        infoInputData.setGssrBrId("111");
////        System.out.println(ObjectUtil.length(infoInputData));
////        if (ObjectUtil.isNotNull(infoInputData)) {
////            System.out.println("不是空的");
////        }
////        boolean allFieldNull = Util.isAllFieldNull(infoInputData);
////        System.out.println(allFieldNull);
//    }
}
