package com.gys.util;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.response.Result;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Set;

/**
 * @author li_haixia@gov-info.cn
 * @desc 易慧-云南监管平台util
 * @date 2021/11/11 15:36
 */
@Component
public class YunnanSuperviseUtil {
    private static final Logger logger = LoggerFactory.getLogger("易慧-云南执业药师远程监管第三方api");
    /**
     * 私钥
     */
    private static String str_privateKey="";
    @Value("${yihuiYunNanYJ.str_privateKey}")
    private void  setStr_privateKey(String value){YunnanSuperviseUtil.str_privateKey=value;}

    /**
     * 机构编码
     */
    public static String instNo="";
    @Value("${yihuiYunNanYJ.instNo}")
    public void setInstNo(String value){YunnanSuperviseUtil.instNo=value;}
    /**
     * 机构编码
     */
    public static String cameraIp="";
    @Value("${yihuiYunNanYJ.cameraIp}")
    public void setCameraIp(String value){YunnanSuperviseUtil.cameraIp=value;}
    /**
     * 执业药师签到URL
     */
    private static String pharmacist_login_url="";
    @Value("${yihuiYunNanYJ.pharmacist_login_url}")
    private void setPharmacist_login_url(String value){YunnanSuperviseUtil.pharmacist_login_url=value;}
    /**
     * 执业药师签退URL
     */
    private static String pharmacist_logout_url="";
    @Value("${yihuiYunNanYJ.pharmacist_logout_url}")
    private void setPharmacist_logout_url(String value){YunnanSuperviseUtil.pharmacist_logout_url=value;}
    /**
     * 处方信息URL
     */
    private static String prescription_url="";
    @Value("${yihuiYunNanYJ.prescription_url}")
    private void setPrescription_url(String value){YunnanSuperviseUtil.prescription_url=value;}
    /**
     * 处方药销售明细URL
     */
    private static String drugstore_salesDetail_url="";
    @Value("${yihuiYunNanYJ.drugstore_salesDetail_url}")
    private void setDrugstore_salesDetail_url(String value){YunnanSuperviseUtil.drugstore_salesDetail_url=value;}


    private static String schema = "SUPERVISION-V2-SHA256-RSA1024";

    /**
     * 获取http头 Authorization 信息
     * @param body
     * @return
     * @throws Exception
     */
    private static String getToken(String body) throws Exception {
        String signature = sign(body.getBytes(StandardCharsets.UTF_8));
        return schema + " " + instNo + "," + signature;
    }

    private static String sign(byte[] message) throws Exception {
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(getPrivateKey(str_privateKey));
        sign.update(message);
        return Base64.getEncoder().encodeToString(sign.sign());
    }

    private static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }


    /**
     * 药师签到
     * @param jsonObject
     * @return
     */
    public static Result login(String jsonObject){
        String result_str = sendHttp(jsonObject, pharmacist_login_url);
        logger.info("<药师签到>结果打印{}", result_str);
        Result result = JSONUtil.toBean(result_str, Result.class);
        return result;
    }

    /**
     * 药师签退
     * @param jsonObject
     * @return
     */
    public static Result logout(String jsonObject){
        String result_str = sendHttp(jsonObject, pharmacist_logout_url);
        logger.info("<药师签退>结果打印{}", result_str);
        Result result = JSONUtil.toBean(result_str, Result.class);
        return result;
    }

    /**
     * 处方信息
     * @param jsonObject
     * @return
     */
    public static Result prescription(String jsonObject){
        String result_str = sendHttp(jsonObject, prescription_url);
        logger.info("<处方信息>结果打印{}", result_str);
        Result result = JSONUtil.toBean(result_str, Result.class);
        return result;
    }

    /**
     * 处方药销售明细
     * @param jsonObject
     * @return
     */
    public static Result salesDetail(String jsonObject){
        String result_str = sendHttp(jsonObject, drugstore_salesDetail_url);
        logger.info("<处方药销售明细>结果打印{}", result_str);
        Result result = JSONUtil.toBean(result_str, Result.class);
        return result;
    }

    private static String sendHttp(String jsonObject, String url){
        logger.info("<请求参数>{}",jsonObject.toString());
        return HttpRequest.post(url)
                //设置请求头
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                //传输参数
                .body(jsonObject.toString())
                .execute()
                .body();
    }
}
