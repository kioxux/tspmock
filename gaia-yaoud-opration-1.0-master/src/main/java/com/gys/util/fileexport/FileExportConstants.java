package com.gys.util.fileexport;

/**
 * @author wu mao yin
 * @Title:
 * @date 2021/11/1513:11
 */
public interface FileExportConstants {

    Integer PAGE_SIZE = 200000;

}
