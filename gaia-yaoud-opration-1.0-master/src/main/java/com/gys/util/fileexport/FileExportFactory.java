package com.gys.util.fileexport;

import lombok.experimental.UtilityClass;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

/**
 * @author wu mao yin
 * tips: 暂未整理 easyExcel 后期整合
 * <examples>
 *      List<UserTest> list = new ArrayList<>();
 *         for (int i = 0; i < 320000; i++) {
 *             UserTest userTest = new UserTest()
 *                     .setUserName("ceshi" + i + "asdasd")
 *                     .setSex(i % 2 == 0 ? "1" : "2")
 *                     .setAge(ThreadLocalRandom.current().nextInt(33))
 *                     .setBirth("2000-10-01 12:11:12")
 *                     .setAddress("苏州市,园区" + i);
 *             list.add(userTest);
 *         }
 *         try {
 *             AbstractFileExport exportFactory = FileExportFactory.createExportFactory(FileExportType.XLSX);
 *             exportFactory.exportFile(response, UserTest.class, list, null, "发生的范德萨发生");
 *         } catch (Exception e) {
 *             e.printStackTrace();
 *         }
 * </examples>
 * @date 2021/11/1510:40
 */
@UtilityClass
public class FileExportFactory {

    public static AbstractFileExport createExportFactory(FileExportType fileExportType) {
        return createExportFactory(fileExportType, "sheet");
    }

    public static AbstractFileExport createExportFactory(FileExportType fileExportType, String sheetName) {
        return createExportFactory(fileExportType, sheetName, null);
    }

    public static AbstractFileExport createExportFactory(FileExportType fileExportType, String sheetName, XSSFCellStyle cellStyle) {
        AbstractFileExport abstractFileExport = null;
        if (FileExportType.CSV.equals(fileExportType)) {
            abstractFileExport = new CsvFileExport();
        } else if (FileExportType.XLS.equals(fileExportType) || FileExportType.XLSX.equals(fileExportType)) {
            abstractFileExport = new ExcelFileExport(fileExportType, sheetName, cellStyle);
        }
        if (abstractFileExport == null) {
            abstractFileExport = new ExcelFileExport(FileExportType.XLSX, sheetName, cellStyle);
        }
        return abstractFileExport;
    }

}
