package com.gys.util.fileexport;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.collections4.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/11/1510:28
 */
public abstract class AbstractFileExport {

    protected void initResponse(@NotNull HttpServletResponse response,
                                @NotNull FileExportType fileExportType,
                                @NotNull String fileName) throws UnsupportedEncodingException {
        response.reset();
        response.setContentType(fileExportType.getContentType());
        response.setCharacterEncoding("UTF-8");
        fileName = URLEncoder.encode(fileName + fileExportType.getSuffix(), "UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
    }

    /**
     * 对导出字段根据注解中 index 属性排序
     *
     * @param clazz    clazz
     * @param fieldSet 需要导出的字段
     * @return List<Field>
     */
    protected List<Field> sortField(Class<?> clazz, List<String> fieldSet) {
        List<Field> declaredFields = Arrays.asList(clazz.getDeclaredFields());
        if (CollectionUtils.isNotEmpty(fieldSet)) {
            declaredFields = declaredFields.stream()
                    .filter(item -> fieldSet.contains(item.getName()))
                    .collect(Collectors.toList());
        }

        declaredFields = declaredFields.stream()
                .filter(item -> item.isAnnotationPresent(FieldTitle.class))
                .sorted((o1, o2) -> {
                    FieldTitle annotation1 = o1.getAnnotation(FieldTitle.class);
                    int index1 = annotation1.index();
                    FieldTitle annotation2 = o2.getAnnotation(FieldTitle.class);
                    int index2 = annotation2.index();
                    return Integer.compare(index1, index2);
                }).collect(Collectors.toList());
        return declaredFields;
    }

    /**
     * 具体导出实现
     *
     * @param response response
     * @param clazz    clazz
     * @param list     list
     * @param fieldSet fieldSet
     * @param fileName fileName
     * @throws Exception Exception
     */
    public abstract void exportFile(HttpServletResponse response, Class<?> clazz,
                                    List<?> list, List<String> fieldSet, String fileName) throws Exception;

    /**
     * 具体导出实现
     *
     * @param response response
     * @param titles    titles
     * @param list     list
     * @param fieldSet fieldSet
     * @param fileName fileName
     * @throws Exception Exception
     */
    public abstract void exportFile(HttpServletResponse response, String[] titles,
                                    List<?> list, List<String> fieldSet, String fileName) throws Exception;


    protected String parseVal(Object obj) {
        String value = StrUtil.EMPTY;
        if (Objects.isNull(obj)) {
            return value;
        }
        if (obj instanceof Integer) {
            value = String.valueOf(obj);
        } else if (obj instanceof String) {
            value = (String) obj;
        } else if (obj instanceof Double) {
            value = String.valueOf(obj);
        } else if (obj instanceof Float) {
            value = String.valueOf(obj);
        } else if (obj instanceof Long) {
            value = String.valueOf(obj);
        } else if (obj instanceof Boolean) {
            value = String.valueOf(obj);
        } else if (obj instanceof BigDecimal) {
            value = obj.toString();
        }
        return value;
    }

}
