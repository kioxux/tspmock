package com.gys.util.fileexport;

import cn.hutool.core.util.StrUtil;
import com.gys.common.enums.BaseEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wu mao yin
 * @Title: 导出excel, 只支持简单格式导出，需要合并单元格自行实现
 * @date 2021/11/15 10:43
 */
public class ExcelFileExport extends AbstractFileExport {

    private final FileExportType fileExportType;

    private final String sheetName;

    private final XSSFCellStyle cellStyle;

    public ExcelFileExport(FileExportType fileExportType, String sheetName, XSSFCellStyle cellStyle) {
        this.fileExportType = fileExportType;
        this.sheetName = sheetName;
        this.cellStyle = cellStyle;
    }

    @Override
    public void exportFile(HttpServletResponse response, Class<?> clazz, List<?> list, List<String> fieldSet, String fileName) throws IOException, IllegalAccessException {
        initResponse(response, fileExportType, fileName);
        // 创建工作簿
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);

        // 写入标题
        writeTitle(clazz, sheet, fieldSet);

        writeData(response, workbook, list, sheet, fieldSet);
    }

    /**
     * 写入数据
     *
     * @param response response
     * @param workbook workbook
     * @param list     list
     * @param sheet    sheet
     * @param fieldSet fieldSet
     * @throws IllegalAccessException IllegalAccessException
     * @throws IOException            IOException
     */
    private void writeData(HttpServletResponse response, XSSFWorkbook workbook, List<?> list,
                           XSSFSheet sheet, List<String> fieldSet) throws IllegalAccessException, IOException {
        if (CollectionUtils.isNotEmpty(list)) {
            int pageSize = FileExportConstants.PAGE_SIZE;
            AtomicInteger lineNo = new AtomicInteger(1);
            // 如果一次性加载数据超过设置阈值，则分页写入
            if (list.size() > pageSize) {
                int count = list.size();
                // 页数
                int pageCount = count % pageSize == 0 ? count / pageSize : (count / pageSize) + 1;
                int pageNum = 1;
                for (int i = 0; i < pageCount; i++) {
                    int fromIndex = (pageNum - 1) * pageSize;
                    int toIndex = pageNum != pageCount ? fromIndex + pageSize : count;
                    pageWrite(list.subList(fromIndex, toIndex), sheet, lineNo, fieldSet);
                    pageNum++;
                }
            } else {
                pageWrite(list, sheet, lineNo, fieldSet);
            }
            writeAndClose(workbook, response);
        }
    }

    @Override
    public void exportFile(HttpServletResponse response, String[] titles, List<?> list, List<String> fieldSet, String fileName) throws IOException, IllegalAccessException {
        initResponse(response, fileExportType, fileName);
        // 创建工作簿
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);

        // 写入标题
        writeTitle(titles, sheet);

        writeData(response, workbook, list, sheet, fieldSet);
    }

    /**
     * 分页写入
     *
     * @param pageList pageList
     * @param sheet    sheet
     * @param lineNo   lineNo
     * @param fieldSet fieldSet
     * @throws IllegalAccessException IllegalAccessException
     */
    private void pageWrite(List<?> pageList, XSSFSheet sheet, AtomicInteger lineNo, List<String> fieldSet) throws IllegalAccessException {
        for (int i = 0; i < pageList.size(); i++) {
            Object obj = pageList.get(i);
            XSSFRow row = sheet.createRow(lineNo.getAndIncrement());
            List<Field> declaredFields = sortField(obj.getClass(), fieldSet);
            for (int j = 0; j < declaredFields.size(); j++) {
                Field field = declaredFields.get(j);
                XSSFCell cell = row.createCell(j);
                field.setAccessible(true);
                Object objVal = Optional.ofNullable(field.get(obj)).orElse(StrUtil.EMPTY);
                String val = parseVal(objVal);
                FieldTitle annotation = field.getAnnotation(FieldTitle.class);
                Class<? extends BaseEnum> aClass = annotation.dictType();
                if (aClass.isEnum()) {
                    String finalVal = val;
                    val = Arrays.stream(aClass.getEnumConstants())
                            .filter(item -> item.getType().equals(finalVal))
                            .map(BaseEnum::getName).findFirst().orElse(StrUtil.EMPTY);
                }
                cell.setCellValue(val);
                if (cellStyle != null) {
                    cell.setCellStyle(cellStyle);
                }
            }
        }
    }

    /**
     * 写入 excel 标题
     *
     * @param clazz    clazz
     * @param sheet    sheet
     * @param fieldSet fieldSet
     */
    private void writeTitle(Class<?> clazz, XSSFSheet sheet, List<String> fieldSet) {
        List<Field> fields = sortField(clazz, fieldSet);
        // 创建第一行
        XSSFRow row = sheet.createRow(0);
        for (int i = 0; i < fields.size(); i++) {
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(fields.get(i).getAnnotation(FieldTitle.class).title());
            if (cellStyle != null) {
                cell.setCellStyle(cellStyle);
            }
        }
    }

    /**
     * 写入 excel 标题
     *
     * @param titles titles
     * @param sheet  sheet
     */
    private void writeTitle(String[] titles, XSSFSheet sheet) {
        // 创建第一行
        XSSFRow row = sheet.createRow(0);
        for (int i = 0; i < titles.length; i++) {
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(titles[i]);
            if (cellStyle != null) {
                cell.setCellStyle(cellStyle);
            }
        }
    }

    /**
     * 关闭流
     *
     * @param response response
     */
    private void writeAndClose(XSSFWorkbook workbook, HttpServletResponse response) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);

        IOUtils.closeQuietly(outputStream);
    }

}
