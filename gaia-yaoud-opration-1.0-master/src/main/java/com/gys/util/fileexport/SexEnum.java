package com.gys.util.fileexport;

import com.gys.common.enums.BaseEnum;

/***
 * @desc: 门店管理区域
 * @author: ryan
 * @createTime: 2021/6/10 14:36
 **/
public enum SexEnum implements BaseEnum {

    PARK("1", "男"),
    URBAN_AREA("2", "女"),

    ;

    public final String type;
    public final String name;

    SexEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (SexEnum value : SexEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
