package com.gys.util.fileexport;

import com.gys.common.enums.BaseEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义导出Excel数据注解
 *
 * @author somewhere
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FieldTitle {

    /**
     * 导出到Excel中的名字.
     */
    String title() default "";

    /**
     * 下标，指定导出文件中顺序
     */
    int index() default 0;

    /**
     * 如果是字典类型，请设置字典的type值
     */
    Class<? extends BaseEnum> dictType() default BaseEnum.class;

}
