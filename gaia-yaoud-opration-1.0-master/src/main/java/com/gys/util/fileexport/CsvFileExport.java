package com.gys.util.fileexport;

import cn.hutool.core.util.StrUtil;
import com.gys.common.enums.BaseEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * @author wu mao yin
 * @Title: csv 文件导出
 * @date 2021/11/15 10:40
 */
public class CsvFileExport extends AbstractFileExport {

    /**
     * 支持小数据量 csv 格式文件导出
     *
     * @param response 返回前端响应文件流
     * @param clazz    映射实体
     * @param list     数据集
     * @param fieldSet 需要导出的字段
     * @param fileName 文件名
     * @throws IOException IOException
     */
    @Override
    public void exportFile(HttpServletResponse response, Class<?> clazz, List<?> list, List<String> fieldSet, String fileName)
            throws IOException, IllegalAccessException {

        initResponse(response, FileExportType.CSV, fileName);

        PrintWriter writer = createPrintWriter(response);

        // 写入标题
        writeTitle(clazz, writer, fieldSet);

        writeData(list, fieldSet, writer);

    }

    /**
     * 写入数据
     *
     * @param list     list
     * @param fieldSet fieldSet
     * @param writer   writer
     * @throws IllegalAccessException IllegalAccessException
     */
    private void writeData(List<?> list, List<String> fieldSet, PrintWriter writer) throws IllegalAccessException {
        if (CollectionUtils.isNotEmpty(list)) {
            int pageSize = FileExportConstants.PAGE_SIZE;
            // 如果一次性加载数据超过设置阈值，则分页写入
            if (list.size() > pageSize) {
                int count = list.size();
                // 页数
                int pageCount = count % pageSize == 0 ? count / pageSize : (count / pageSize) + 1;
                int pageNum = 1;
                for (int i = 0; i < pageCount; i++) {
                    int fromIndex = (pageNum - 1) * pageSize;
                    int toIndex = pageNum != pageCount ? fromIndex + pageSize : count;
                    pageWrite(list.subList(fromIndex, toIndex), writer, fieldSet);
                    pageNum++;
                }
            } else {
                pageWrite(list, writer, fieldSet);
            }
            close(writer);
        }
    }

    @Override
    public void exportFile(HttpServletResponse response, String[] titles, List<?> list, List<String> fieldSet, String fileName) throws Exception {
        initResponse(response, FileExportType.CSV, fileName);
        PrintWriter writer = createPrintWriter(response);

        // 写入标题
        writeTitle(titles, writer);

        writeData(list, fieldSet, writer);
    }

    public void writeTitle(String[] titles, PrintWriter writer) {
        StringJoiner stringJoiner = new StringJoiner(StrUtil.COMMA);
        for (String title : titles) {
            stringJoiner.add(title);
        }
        stringJoiner.add(StrUtil.CRLF);
        // 避免乱码
        writer.write(new String(new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF}));
        writer.write(stringJoiner.toString());
    }

    /**
     * 写入 csv 文件标题
     *
     * @param clazz    clazz
     * @param writer   writer
     * @param fieldSet fieldSet
     */
    private void writeTitle(Class<?> clazz, PrintWriter writer, List<String> fieldSet) {
        List<Field> fields = sortField(clazz, fieldSet);
        StringJoiner stringJoiner = new StringJoiner(StrUtil.COMMA);
        for (Field field : fields) {
            stringJoiner.add(field.getAnnotation(FieldTitle.class).title());
        }
        stringJoiner.add(StrUtil.CRLF);
        // 避免乱码
        writer.write(new String(new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF}));
        writer.write(stringJoiner.toString());
    }

    /**
     * 分页写入数据到 csv 文件
     *
     * @param pageList 分页数据集
     * @param writer   写入流
     * @param fieldSet 导出的字段
     * @throws IllegalAccessException IllegalAccessException
     */
    private void pageWrite(List<?> pageList, PrintWriter writer, List<String> fieldSet) throws IllegalAccessException {
        // 逐行写入数据
        for (Object obj : pageList) {
            List<Field> declaredFields = sortField(obj.getClass(), fieldSet);
            StringJoiner stringJoiner = new StringJoiner(StrUtil.COMMA);
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                Object objVal = Optional.ofNullable(declaredField.get(obj)).orElse(StrUtil.EMPTY);
                String val = parseVal(objVal);
                FieldTitle annotation = declaredField.getAnnotation(FieldTitle.class);
                Class<? extends BaseEnum> aClass = annotation.dictType();
                if (aClass.isEnum()) {
                    String finalVal = val;
                    val = Arrays.stream(aClass.getEnumConstants())
                            .filter(item -> item.getType().equals(finalVal))
                            .map(BaseEnum::getName).findFirst().orElse(StrUtil.EMPTY);
                }
                // 解析值包含 ","
                if (val.contains(StrUtil.COMMA)) {
                    val = "\"" + val + "\"";
                }
                stringJoiner.add(val);
            }

            stringJoiner.add(StrUtil.CRLF);
            writer.print(stringJoiner);
        }
        writer.flush();
    }

    /**
     * 关闭流
     *
     * @param writer writer
     */
    private void close(PrintWriter writer) {
        IOUtils.closeQuietly(writer);
    }

    /**
     * @param response 创建写入流
     * @return PrintWriter
     * @throws IOException
     */
    private PrintWriter createPrintWriter(HttpServletResponse response) throws IOException {
        return new PrintWriter(response.getOutputStream());
    }

}
