package com.gys.util.fileexport;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @date 2021/11/1515:57
 */
@Data
@Accessors(chain = true)
public class UserTest implements Serializable {

    @FieldTitle(title = "姓名", index = 1)
    private String userName;

    @FieldTitle(title = "年龄", index = 2)
    private Integer age;

    @FieldTitle(title = "性别", index = 4, dictType = SexEnum.class)
    private String sex;

    @FieldTitle(title = "地址", index = 3)
    private String address;

    @FieldTitle(title = "出生日期", index = 5)
    private String birth;

}
