package com.gys.util.fileexport;

/**
 * @author wu mao yin
 * @Title: 文件导出类型
 * @Package
 * @date 2021/11/1510:26
 */
public enum FileExportType {

    XLS("application/vnd.ms-excel", ".xls"),

    CSV("application/csv;charset=UTF-8", ".csv"),

    XLSX("application/vnd.ms-excel", ".xlsx"),

    ;

    private final String contentType;
    private final String suffix;

    FileExportType(String contentType, String suffix) {
        this.contentType = contentType;
        this.suffix = suffix;
    }

    public String getContentType() {
        return contentType;
    }

    public String getSuffix() {
        return suffix;
    }

}
