package com.gys.util;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Excel操作工具类
 *
 * @author ChaiXY
 */
public class ExcelUtils {

    public static final String OFFICE_EXCEL_XLS = "xls";
    public static final String OFFICE_EXCEL_XLSX = "xlsx";
    /**
     * 转换null
     *
     * @param obj
     * @return
     */
    public static String tranColumn(Object obj) {
        return (obj == null) ? "" : obj.toString();
    }
    /**
     * 自适应列宽
     *
     * @param sheet
     * @param allSize
     */
    public static void setSizeColumnLength(Sheet sheet, int allSize) {
        for (int columnNum = 0; columnNum <= allSize; columnNum++) {
            int columnWidth = sheet.getColumnWidth(columnNum) / 256;
            for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
                Row currentRow;
                //当前行未被使用过
                if (sheet.getRow(rowNum) == null) {
                    currentRow = sheet.createRow(rowNum);
                } else {
                    currentRow = sheet.getRow(rowNum);
                }

                if (currentRow.getCell(columnNum) != null) {
                    Cell currentCell = currentRow.getCell(columnNum);
                    if (currentCell.getCellType() == CellType.STRING) {
                        int length = currentCell.getStringCellValue().getBytes().length;
                        if (length > 40) {
                            columnWidth = 40;
                            break;
                        }
                        if (columnWidth < length) {
                            columnWidth = length;
                        }
                    }
                }
            }
            sheet.setColumnWidth(columnNum, columnWidth * 256);
        }
    }
    /**
     * 读取指定Sheet也的内容
     *
     * @param filepath filepath 文件全路径
     * @param sheetNo  sheet序号,从0开始,如果读取全文sheetNo设置null
     */
    public static String readExcel(String filepath, Integer sheetNo)
            throws EncryptedDocumentException, InvalidFormatException, IOException {
        StringBuilder sb = new StringBuilder();
        Workbook workbook = getWorkbook(filepath);
        if (workbook != null) {
            if (sheetNo == null) {
                int numberOfSheets = workbook.getNumberOfSheets();
                for (int i = 0; i < numberOfSheets; i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    if (sheet == null) {
                        continue;
                    }
                    sb.append(readExcelSheet(sheet));
                }
            } else {
                Sheet sheet = workbook.getSheetAt(sheetNo);
                if (sheet != null) {
                    sb.append(readExcelSheet(sheet));
                }
            }
        }
        return sb.toString();
    }

    /**
     * 根据文件路径获取Workbook对象
     *
     * @param filepath 文件全路径
     */
    public static Workbook getWorkbook(String filepath)
            throws EncryptedDocumentException, InvalidFormatException, IOException {
        InputStream is = null;
        Workbook wb = null;
        if (StringUtils.isBlank(filepath)) {
            throw new IllegalArgumentException("文件路径不能为空");
        } else {
            String suffiex = getSuffiex(filepath);
            if (StringUtils.isBlank(suffiex)) {
                throw new IllegalArgumentException("文件后缀不能为空");
            }
            if (OFFICE_EXCEL_XLS.equals(suffiex) || OFFICE_EXCEL_XLSX.equals(suffiex)) {
                try {
                    is = new FileInputStream(filepath);
                    wb = WorkbookFactory.create(is);
                } finally {
                    if (is != null) {
                        is.close();
                    }
                    if (wb != null) {
                        wb.close();
                    }
                }
            } else {
                throw new IllegalArgumentException("该文件非Excel文件");
            }
        }
        return wb;
    }

    /**
     * 获取后缀
     *
     * @param filepath filepath 文件全路径
     */
    private static String getSuffiex(String filepath) {
        if (StringUtils.isBlank(filepath)) {
            return "";
        }
        int index = filepath.lastIndexOf(".");
        if (index == -1) {
            return "";
        }
        return filepath.substring(index + 1, filepath.length());
    }

    private static String readExcelSheet(Sheet sheet) {
        StringBuilder sb = new StringBuilder();

        if (sheet != null) {
            int rowNos = sheet.getLastRowNum();// 得到excel的总记录条数
            for (int i = 0; i <= rowNos; i++) {// 遍历行
                Row row = sheet.getRow(i);
                if (row != null) {
                    int columNos = row.getLastCellNum();// 表头总共的列数
                    for (int j = 0; j < columNos; j++) {
                        Cell cell = row.getCell(j);
                        if (cell != null) {
                            cell.setCellType(CellType.STRING);
                            CellAddress cellAddress = new CellAddress(3, 0);
                            if (cell.getAddress().compareTo(cellAddress) >= 0) {
                                sb.append(cell.getStringCellValue() + " ");
                            }
                            // System.out.print(cell.getStringCellValue() + " ");
                        }
                    }
                    if (i > 2) {
                        sb.append("\r\n");
                    }
                }
            }
        }

        return sb.toString();
    }

    /**
     * 读取指定Sheet页的表头
     *
     * @param filepath filepath 文件全路径
     * @param sheetNo  sheet序号,从0开始,必填
     */
    public static Row readTitle(String filepath, int sheetNo)
            throws IOException, EncryptedDocumentException, InvalidFormatException {
        Row returnRow = null;
        Workbook workbook = getWorkbook(filepath);
        if (workbook != null) {
            Sheet sheet = workbook.getSheetAt(sheetNo);
            returnRow = readTitle(sheet);
        }
        return returnRow;
    }

    /**
     * 读取指定Sheet页的表头
     */
    public static Row readTitle(Sheet sheet) throws IOException {
        Row returnRow = null;
        int totalRow = sheet.getLastRowNum();// 得到excel的总记录条数
        for (int i = 0; i < totalRow; i++) {// 遍历行
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            returnRow = sheet.getRow(0);
            break;
        }
        return returnRow;
    }

    /**
     * Java文件操作 获取文件扩展名
     */
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }

    /**
     * MultipartFile 转 File
     *
     * @param file
     * @throws Exception
     */
    public static File multipartFileToFile(MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }


    public static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 导出数据
     * @param heads
     * @param data
     * @return
     */
    public static HSSFWorkbook exportExcel(List<String[]> heads, List<List<List<String>>> data, List<String> sheetName) {
        if (CollectionUtils.isEmpty(heads) || CollectionUtils.isEmpty(data) || CollectionUtils.isEmpty(sheetName)) {
            return null;
        }
        if (heads.size() != data.size() || heads.size() != sheetName.size()) {
            return null;
        }
        // excel 对象
        HSSFWorkbook workbook = new HSSFWorkbook();
        for (int i = 0; i < sheetName.size(); i++) {
            HSSFSheet sheet = workbook.createSheet(sheetName.get(i));
            // 头部写入
            setHead(workbook, sheet, heads.get(i));
            // 内容写入
            setBody(sheet, data.get(i));
        }
        return workbook;
    }

    /**
     * 导出模板excel
     * @param heads
     * @param
     * @return
     */
    public static HSSFWorkbook exportExcelByTemplate(List<String[]> heads,List<String> sheetName) {
        if (CollectionUtils.isEmpty(heads) || CollectionUtils.isEmpty(sheetName)) {
            return null;
        }
        /*if (heads.size() != sheetName.size()) {
            return null;
        }*/
        // excel 对象
        HSSFWorkbook workbook = new HSSFWorkbook();
        for (int i = 0; i < sheetName.size(); i++) {
            HSSFSheet sheet = workbook.createSheet(sheetName.get(i));
            // 头部写入
            setHead(workbook, sheet, heads.get(i));
        }
        return workbook;
    }


    /**
     * 数据写入
     * @param sheet
     * @param datas
     */
    private static void setBody(HSSFSheet sheet, List<List<String>> datas) {
        if (CollectionUtils.isEmpty(datas)) {
            return;
        }
        // 数据行写入
        for (int i = 0; i < datas.size(); i++) {
            // 行数据
            List<String> data = datas.get(i);
            HSSFRow row = sheet.getRow(i + 1);
            if (row == null) {
                row = sheet.createRow(i + 1);
            }
            // 循环列数据
            for (int c = 0; c < data.size(); c++) {
                HSSFCell cell = row.getCell(c);
                if (cell == null) {
                    cell = row.createCell(c);
                }
                if (StringUtils.isNotBlank(data.get(c))) {
                    cell.setCellValue(data.get(c));
                }
            }
        }
    }

    /**
     * 导出数据
     * @param heads
     * @param data
     * @return
     */
    public static HSSFWorkbook exportExcel2(List<String[]> heads, List<List<List<Object>>> data, List<String> sheetName) {
        if (CollectionUtils.isEmpty(heads) || CollectionUtils.isEmpty(data) || CollectionUtils.isEmpty(sheetName)) {
            return null;
        }
        if (heads.size() != data.size() || heads.size() != sheetName.size()) {
            return null;
        }
        // excel 对象
        HSSFWorkbook workbook = new HSSFWorkbook();
        for (int i = 0; i < sheetName.size(); i++) {
            HSSFSheet sheet = workbook.createSheet(sheetName.get(i));
            // 头部写入
            setHead(workbook, sheet, heads.get(i));
            // 内容写入
            setBody2(sheet, data.get(i));
        }
        return workbook;
    }

    /**
     * 数据写入
     * @param sheet
     * @param datas
     */
    private static void setBody2(HSSFSheet sheet, List<List<Object>> datas) {
        if (CollectionUtils.isEmpty(datas)) {
            return;
        }
        // 数据行写入
        for (int i = 0; i < datas.size(); i++) {
            // 行数据
            List<Object> data = datas.get(i);
            HSSFRow row = sheet.getRow(i + 1);
            if (row == null) {
                row = sheet.createRow(i + 1);
            }
            // 循环列数据
            for (int c = 0; c < data.size(); c++) {
                HSSFCell cell = row.getCell(c);
                if (cell == null) {
                    cell = row.createCell(c);
                }
                if (data.get(c) != null) {
                    Object param = data.get(c);
                    if (param instanceof Integer) {
                        int value = ((Integer) param).intValue();
                        cell.setCellValue(value);
                    } else if (param instanceof String) {
                        String value = (String) param;
                        cell.setCellValue(value);
                    } else if (param instanceof Double) {
                        double value = ((Double) param).doubleValue();
                        cell.setCellValue(value);
                    } else if (param instanceof Float) {
                        float value = ((Float) param).floatValue();
                        cell.setCellValue(value);
                    } else if (param instanceof Long) {
                        long value = ((Long) param).longValue();
                        cell.setCellValue(value);
                    } else if (param instanceof Boolean) {
                        boolean value = ((Boolean) param).booleanValue();
                        cell.setCellValue(value);
                    } else if (param instanceof Date) {
                        Date value = (Date) param;
                        cell.setCellValue(value);
                    } else if (param instanceof BigDecimal) {
                        double doubleVal = (new BigDecimal(param.toString())).doubleValue();
                        cell.setCellValue(doubleVal);
                    } else {
                        cell.setCellValue(param.toString());
                    }
                }
            }
        }
    }

    /**
     * 表头写入
     * @param sheet
     * @param heads
     */
    private static void setHead(HSSFWorkbook workbook, HSSFSheet sheet, String[] heads) {
        HSSFCellStyle hssfCellStyle = workbook.createCellStyle();
        hssfCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        HSSFRow row = sheet.getRow(0);
        if (row == null) {
            row = sheet.createRow(0);
        }
        for (int i = 0; i < heads.length; i++) {
            String head = heads[i];
            HSSFCell cell = row.getCell(i);
            if (cell == null) {
                cell = row.createCell(i);
            }
            cell.setCellValue(head);
            cell.setCellStyle(hssfCellStyle);
        }
    }

    /**
     * 转换null
     *
     * @param obj
     * @return
     */
    public static String tran(Object obj) {
        return (obj == null) ? "" : obj.toString();
    }

    /**
     * 自适应列宽
     *
     * @param sheet
     * @param allSize
     */
    public static void setSizeColumn(Sheet sheet, int allSize) {
        for (int columnNum = 0; columnNum <= allSize; columnNum++) {
            int columnWidth = sheet.getColumnWidth(columnNum) / 256;
            for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
                Row currentRow;
                //当前行未被使用过
                if (sheet.getRow(rowNum) == null) {
                    currentRow = sheet.createRow(rowNum);
                } else {
                    currentRow = sheet.getRow(rowNum);
                }

                if (currentRow.getCell(columnNum) != null) {
                    Cell currentCell = currentRow.getCell(columnNum);
                    if (currentCell.getCellType() == CellType.STRING) {
                        int length = currentCell.getStringCellValue().getBytes().length;
                        if (length > 30) {
                            columnWidth = 30;
                            break;
                        }
                        if (columnWidth < length) {
                            columnWidth = length;
                        }
                    }
                }
            }
            sheet.setColumnWidth(columnNum, columnWidth * 256);
        }
    }


}
