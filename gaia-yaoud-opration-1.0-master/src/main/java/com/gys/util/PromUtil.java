package com.gys.util;

import java.util.Arrays;
import java.util.List;

public class PromUtil {
    public final static List<String> list = Arrays.asList("拆零", "积分换购","改价","促销");
    public final static List<String> list2 = Arrays.asList("拆零", "积分换购","改价");

    /**
     * 判断商品状态是否可以参与促销计算，排除("拆零", "积分换购","改价","促销")
     * @param status
     * @return
     */
    public static boolean isJoinActivity(String status) {
        if(status == null){
            return true;
        }else {
            return !list.contains(status);
        }
    }

    /**
     * 判断商品状态是否可以参与促销计算2，排除("拆零", "积分换购","改价"）
     * @param status
     * @return
     */
    public static boolean isJoinActivity2(String status) {
        if(status == null){
            return true;
        }else {
            return !list2.contains(status);
        }
    }
}
