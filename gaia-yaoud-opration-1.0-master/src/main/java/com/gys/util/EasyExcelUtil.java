package com.gys.util;

import cn.hutool.core.date.DateTime;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.fastjson.JSON;
import com.gys.common.exception.BusinessException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EasyExcelUtil  {
    public static <T> void simpleWrite(String execlName, Class execlClass, List<T> data) {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        ExcelWriter excelWriter = null;
        try {

            // 写法2
            String fileName = execlName+new DateTime() + ".xlsx";
            // 这里 需要指定写用哪个class去写
            excelWriter = EasyExcel.write(execlName, execlClass).build();
            WriteSheet writeSheet = EasyExcel.writerSheet(execlName).build();
            excelWriter.write(data, writeSheet);
        } catch (Exception e) {
            throw new BusinessException("导出失败！");
        } finally {
            // 千万别忘记finish 会帮忙关闭流
            if (excelWriter != null) {
                excelWriter.finish();
            }
        }
    }
}