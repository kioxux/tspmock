//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.service.data.upload.UpLoadDto;
import com.gys.common.exception.BusinessException;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.model.GeneratePresignedUrlRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.region.Region;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

import static com.gys.business.bosen.constant.UpLoadConstant.SOURCE_PRESCRIPTION;

public class CosUtil {
    private static final Logger log = LoggerFactory.getLogger(CosUtil.class);
    private static CosUtil cosUtil = null;
    private static String secretId;
    private static String secretKey;
    private static String regionName;
    private static String bucketName;

    public CosUtil() {
    }

    public static CosUtil getInstance(String id, String key, String region, String bucket) {
        if (!StrUtil.isBlank(id) && !StrUtil.isBlank(key) && !StrUtil.isBlank(region) && !StrUtil.isBlank(bucket)) {
            if (cosUtil == null) {
                cosUtil = new CosUtil();
            }

            secretId = id;
            secretKey = key;
            regionName = region;
            bucketName = bucket;
            return cosUtil;
        } else {
            throw new BusinessException("提示：未配置对象存储参数");
        }
    }

    public String upload(MultipartFile file) {
        String key = DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + File.separator + file.getOriginalFilename();

        try {
            COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
            Region region = new Region(regionName);
            ClientConfig clientConfig = new ClientConfig(region);
            COSClient cosClient = new COSClient(cred, clientConfig);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            InputStream inputStream = file.getInputStream();
            objectMetadata.setContentLength((long)inputStream.available());
            cosClient.putObject(bucketName, key, inputStream, objectMetadata);
            cosClient.shutdown();
            return key;
        } catch (Exception var9) {
            log.error("上传文件错误---", var9);
            throw new BusinessException("提示：上传失败！");
        }
    }
    public String uploadLocal(File file) {
        String key = DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + File.separator + file.getName();

        try {
            COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
            Region region = new Region(regionName);
            ClientConfig clientConfig = new ClientConfig(region);
            COSClient cosClient = new COSClient(cred, clientConfig);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            InputStream inputStream = new FileInputStream(file);
            objectMetadata.setContentLength((long)inputStream.available());
            cosClient.putObject(bucketName, key, inputStream, objectMetadata);
            cosClient.shutdown();
            return key;
        } catch (Exception var9) {
            log.error("上传文件错误---", var9);
            throw new BusinessException("提示：上传失败！");
        }
    }


    public String getFileUrl(String key) {
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(regionName);
        ClientConfig clientConfig = new ClientConfig(region);
        COSClient cosClient = new COSClient(cred, clientConfig);
        GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);
        Date expirationDate = new Date(System.currentTimeMillis() + 1800000L);
        req.setExpiration(expirationDate);
        URL url = cosClient.generatePresignedUrl(req);
        cosClient.shutdown();
        return url.toString();
    }

    /**
     *
     * @param key
     * @param expirationDate  失效日期
     * @return
     */
    public String getFileUrl(String key,Date expirationDate) {
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(regionName);
        ClientConfig clientConfig = new ClientConfig(region);
        COSClient cosClient = new COSClient(cred, clientConfig);
        GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);
        req.setExpiration(expirationDate);
        URL url = cosClient.generatePresignedUrl(req);
        cosClient.shutdown();
        return url.toString();
    }

    public String upload(UpLoadDto upLoadDto) {
        if(StringUtils.isNotBlank(upLoadDto.getSource())&&SOURCE_PRESCRIPTION.equals(upLoadDto.getSource())){
            String key = DateUtil.format(new Date(), "yyyyMMddHHmmssSSS")+upLoadDto.getFile().getOriginalFilename();
            String path = upLoadDto.getPath();
            StringBuilder stringBuilder=new StringBuilder();
            String totalKey = stringBuilder.append(path).append(key).toString();
            try {
                COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
                Region region = new Region(regionName);
                ClientConfig clientConfig = new ClientConfig(region);
                COSClient cosClient = new COSClient(cred, clientConfig);
                ObjectMetadata objectMetadata = new ObjectMetadata();
                InputStream inputStream = upLoadDto.getFile().getInputStream();
                objectMetadata.setContentLength((long)inputStream.available());
                cosClient.putObject(bucketName,totalKey , inputStream, objectMetadata);
                cosClient.shutdown();
                return totalKey;
            } catch (Exception var9) {
                log.error("上传文件错误---", var9);
                throw new BusinessException("提示：上传失败！");
            }

        }
        return null;
    }
}
