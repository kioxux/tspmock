package com.gys.util;

import com.google.common.collect.Lists;
import com.gys.business.service.data.ProductTitleSettings;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 11:11 2021/8/20
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
public class ProductTitleUtil {
    public static final String DEPOT_PRICE_SHOW = "DEPOTPRICE_SHOW";//显示配送价
    public static final String DEPOT_STOCK_SHOW = "DEPOTSTOCK_SHOW";//显示仓库库存

    public static ProductTitleSettings PRODUCT = new ProductTitleSettings("proCode", null, "商品编码", null, null);
    public static ProductTitleSettings PROCOMMONNAME = new ProductTitleSettings("proCommonName", 140, "通用名", null, null);
    public static ProductTitleSettings NAME = new ProductTitleSettings("name", 140, "商品名称", null, null);
    public static ProductTitleSettings SPECS = new ProductTitleSettings("specs", null, "规格", null, null);
    public static ProductTitleSettings UNIT = new ProductTitleSettings("unit", 50, "单位", null, null);
    public static ProductTitleSettings PROFACTORYNAME = new ProductTitleSettings("proFactoryName", 160, "生产企业", null, null);
    public static ProductTitleSettings IFMED = new ProductTitleSettings("ifMed", null, "是否医保", null, null);
    public static ProductTitleSettings PRICENORMAL = new ProductTitleSettings("priceNormal", null, "零售价", null, "right");
    public static ProductTitleSettings INVENTORY = new ProductTitleSettings("inventory", null, "本地库存", null, "right");
    public static ProductTitleSettings UNITPRICE = new ProductTitleSettings("unitPrice", null, "进货价", null, "right");
    public static ProductTitleSettings ADDRATE = new ProductTitleSettings("addRate", null, "加点率", null, "right");
    public static ProductTitleSettings ADDAMT = new ProductTitleSettings("addAmt", null, "配送价", null, "right");
    public static ProductTitleSettings KYSL = new ProductTitleSettings("kysl", null, "仓库可用数量", null, "right");
    public static ProductTitleSettings PYM = new ProductTitleSettings("pym", null, "助记码", null, null);
    public static ProductTitleSettings FORM = new ProductTitleSettings("form", null, "剂型", null, null);
    public static ProductTitleSettings PROPLACE = new ProductTitleSettings("proPlace", null, "产地", null, null);

    private static final List<ProductTitleSettings> ALLTITLE = Lists.newArrayList(
            PRODUCT,
            PROCOMMONNAME,
            NAME,
            SPECS,
            UNIT,
            PROFACTORYNAME,
            IFMED,
            PRICENORMAL,
            INVENTORY,
            UNITPRICE,
            ADDRATE,
            ADDAMT,
            KYSL,
            PYM,
            FORM,
            PROPLACE
    );

    public static List<ProductTitleSettings> getTitle(String type) {
        List<ProductTitleSettings> titleSettings = new ArrayList<>(ALLTITLE);
        switch (type) {
            case "1":
                titleSettings.removeAll(Lists.newArrayList(
                        new ProductTitleSettings("unitPrice", null, "进货价", null, "right"),
                        new ProductTitleSettings("addRate", null, "加点率", null, "right")
                ));
                break;
            case "2":
            case "3":
                titleSettings.removeAll(Lists.newArrayList(
                        new ProductTitleSettings("inventory", null, "本地库存", null, "right")
                ));
                break;
        }
        return titleSettings;
    }
}
