package com.gys.util.yaomeng;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/11/4 15:26
 */
@Data
public class ReturnRecipelInfo {

    private String no;//处方单号
    private String memberName;//患者姓名
    private Integer memberAge;//患者年龄
    private Integer memberSex;//性别0女1男
    private String memberTel;//患者电话
    private String memberIdCard;//身份证
    private String memberAllergy;//过敏史
    private String memberBirthday;//生日
    private String memberAddress;//患者地址
    private String doctorName;//医生姓名
    private String signature;//医生签名
    private List<String> diagnosisList;
    private List<YaomengDrugDto> drugList;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime saleTime;//处方日期
    private String orderId;//销售单号

}
