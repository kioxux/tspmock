package com.gys.util.yaomeng;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/11/3 18:00
 */
@Data
public class YaomengSpeedSimpleBean {

    private String name;//患者姓名
    private Integer age;//患者年龄
    private Integer sex;//患者性别  0.女  1.男
    private String enterpriseCode;//企业编码
    private String customerURL;//门店编码
    private String birthday;//生日
    private String idCard;// 身份证
    private List<YaomengDiseaseDto> diagnosis;//病症信息
    private String orderId;//订单id  传我们的销售单
    private Long doctorId;//医生id
    private String imgs; //上传的图片附件信息，url格式，最多五张,英文逗号隔开   非必填
    private List<YaomengErpDrugBean> erpDrugs;
    private List<YaomengChatBean> chats;
    private String address;//地址  非必填
    private String machineCode;//机器编码  非必填
}
