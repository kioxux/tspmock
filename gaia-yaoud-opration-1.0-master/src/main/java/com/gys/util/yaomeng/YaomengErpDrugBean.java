package com.gys.util.yaomeng;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZhangDong
 * @date 2021/11/3 18:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YaomengErpDrugBean {

    private String erpNo;//erp药品编号
    private Long drugId;//药品id
    private Integer number;//购买数量
}
