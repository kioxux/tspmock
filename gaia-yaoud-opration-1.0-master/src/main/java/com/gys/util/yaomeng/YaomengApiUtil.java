package com.gys.util.yaomeng;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import com.gys.business.service.CommonService;
import com.gys.common.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 药盟
 *
 * @author ZhangDong
 * @date 2021/11/3 11:03
 */
public class YaomengApiUtil {

    @Resource
    private CommonService commonService;


    private static final Logger log = LoggerFactory.getLogger(YaomengApiUtil.class);
    private static final String TEST_SERVEL_URL = "https://cf.sdlhzs.com/";
    private static final String PRD_SERVEL_URL = "https://cfapi.yaomengwang.cn/";

    private static final String SERVEL_URL = PRD_SERVEL_URL;

    //code=enterpriseCode  相当于我们的客户编号，就是咱们药德，customerurl就是咱们药德下面的客户，一个客户一个customerurl，这边客户是我们系统加盟商的概念
    //那其实上线之后，code和enterpriseCode就不会变了，可以写死
    private static final String code = "yaode";
    private static final String enterpriseCode = "yaode";

    private static final String BASIC_DRUG_QUERY = "docking/erp/drug/list";
    private static final String DRUG_ADD = "docking/erp/drug/add";
    private static final String DRUG_DISEASE = "docking/" + code + "/drug/disease";//传平台标识  code
    private static final String CHECK_LIMIT = "docking/erp/check/limit";
    private static final String DIALOGUE_QUESTION = "docking/dialogue/question/diseases";
    private static final String SPEED_SIMPLE = "docking/" + code + "/speed/simple";
    private static final String GET_PRESCRIPTION_FILE_64 = "docking/prescription/getPrescriptionFile/base64";
    private static final String GET_PRESCRIPTION_FILE = "docking/prescription/getPrescriptionFile";
    private static final String GET_DOCTOR = "docking/" + code + "/doctor/speedAllocateDoctor";

    private static RestTemplate restTemplate;

    static {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10000);
        requestFactory.setReadTimeout(5000);
        restTemplate = new RestTemplate(requestFactory);
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                List<MediaType> mediaTypeList = new ArrayList<>(converter.getSupportedMediaTypes());
                mediaTypeList.add(MediaType.APPLICATION_JSON);
                ((MappingJackson2HttpMessageConverter) converter).setSupportedMediaTypes(mediaTypeList);
            }
        }
    }

    public static void main(String[] args) {
        //药品查询
//        List<YaomengDrugDto> list = drugList("yd001", null, "通脉颗粒", null, null);
        List<YaomengDrugDto> list = drugList("yd001", "10103906", null, null, null);
        System.out.println(JSON.toJSONString(list));

        //药品对码
//        List<YaomengDrugAddBean> list = Lists.newArrayList();
//        list.add(new YaomengDrugAddBean("10154391", 93661L));
//        list.add(new YaomengDrugAddBean("10103906", 134555L));
//        list.add(new YaomengDrugAddBean("01000220089", 106172L));
//        drugAdd("yd001", list);

        //获取病症信息
//        drugDisease("yd001",151863L, "01090000022");
//        drugDisease("yd001",106172L, "01000220089");

        //配伍禁忌限制校验
//        YaomengCheckLimitBean bean = new YaomengCheckLimitBean(Lists.newArrayList(158701L), 1, 0, "320924199312216886");
//        checkLimit(TEST_SERVEL_URL,"yd001",  bean);

        //获取医生信息
//        getDoctor("yd001");

        //话术查询
//        dialogueQuestion("yd001", Lists.newArrayList(148L));
//        dialogueQuestion("yd001", Lists.newArrayList(151L));

        //提交图文开方
//        YaomengSpeedSimpleBean bean = new YaomengSpeedSimpleBean();
//        bean.setName("马壮壮");
//        bean.setAge(24);
//        bean.setBirthday("1997-03-29");
//        bean.setIdCard("321281199712262263");
//        List<YaomengDiseaseDto> diagnosis = Lists.newArrayList();
//        diagnosis.add(new YaomengDiseaseDto(148L, "尿路感染"));
//        diagnosis.add(new YaomengDiseaseDto(151L, "前列腺炎"));
//        bean.setDiagnosis(diagnosis);
//        bean.setOrderId("SD2111041QMEJ8AJNGW0");
//        bean.setDoctorId(412L);
////        bean.setImgs("http://cf.storage.yaomengwang.cn/FvDfOD2SKAQeBQiKbUb4me1B5uAV");
//        List<YaomengErpDrugBean> erpDrugs = Lists.newArrayList();
//        erpDrugs.add(new YaomengErpDrugBean("010900000220", 151863L, 1));
//        bean.setErpDrugs(erpDrugs);
//        List<YaomengChatBean> chats = Lists.newArrayList();
//        chats.add(new YaomengChatBean("您描述的患有尿路感染、前列腺炎是否在线下医院确诊过?", 1, true));
//        chats.add(new YaomengChatBean("是的，我确定", 0, true));
//        chats.add(new YaomengChatBean("您确定使用过阿莫西林胶囊，使用后并无不适症状？", 1, true));
//        chats.add(new YaomengChatBean("是的，我确定", 0, true));
//        chats.add(new YaomengChatBean("请问有无肝、肾功能异常？", 1, true));
//        chats.add(new YaomengChatBean("没有", 0, true));
//        chats.add(new YaomengChatBean("请问是否在备孕/怀孕/哺乳期？", 1, true));
//        chats.add(new YaomengChatBean("没有", 0, false));
//        bean.setChats(chats);
//        speedSimple("yd001", bean);

        //获取处方图片
//        getPrescriptionFile("CF657848693475988803995","http://cf.storage.yaomengwang.cn/FvDfOD2SKAQeBQiKbUb4me1B5uAV");


    }

    /**
     * 药品基础数据查询接口
     * 在查询未对码药品的时候，用名称查询，已经对完码的用erpNo查询就行了，俩必须有填一个
     * 只传erpNo其他非必要参数不传，只会查已对码商品，如该商品还未对码则返回空集
     * 返回状态0：成功
     * 返回状态1：失败
     *
     * @param customerURL  门店编号 必填
     * @param erpNo        药品对码编号（精准查询）
     * @param drugName     药品名称（精准查询）
     * @param isCorrespond 是否对码（1 查询对码的数据 ，0查询没有对码的数据）
     * @param state        对码数据状态（1 启用 ，0 停用）
     */
    public static List<YaomengDrugDto> drugList(String customerURL, String erpNo, String drugName, Integer isCorrespond, Integer state) {
        customerURL = "yd001";
        JSONObject sign = new JSONObject();
        sign.put("customerURL", customerURL);
        sign.put("isCorrespond", isCorrespond);
        sign.put("erpNo", erpNo);
        sign.put("drugName", drugName);
        sign.put("state", state);

        String str = sign.toJSONString();
        String signStr = DesUtils.encrypt(str, DesUtils.KEY);
        JSONObject queryJson = new JSONObject();
        queryJson.put("code", code);
        queryJson.put("sign", signStr);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.postForEntity(SERVEL_URL + BASIC_DRUG_QUERY, queryJson, YaomengResponse.class);
        log.info("药盟药品基础数据查询接口，inData:{}, result:{}", str, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<List> body = responseEntity.getBody();
            if (body.getCode() == 0) {
                List<YaomengDrugDto> list = body.getData();
                return list;
            }
        }
        return null;
    }

    /**
     * 药品对码接口
     * 批量添加药品对码数据， 最多支持1000条，禁止重复添加
     * 返回状态0：全部成功
     * 返回状态1：全部失败
     * 返回状态2：部分失败-data  eg: {"body":{"code":2,"data":[{"drugId":151863,"erpNo":"01090000022","msg":"此药品已对码"}],"msg":"成功1条 ,失败1条"}    --data里是失败的
     *
     * @param customerURL 门店编号 必填
     * @param list        erpNo       药品对码编号  必填
     * @param list        drugId      药品基础数据查询接口中的drugId  必填
     */
    public static void drugAdd(String customerURL, List<YaomengDrugAddBean> list) {
        customerURL = "yd001";
        JSONObject sign = new JSONObject();
        sign.put("customerURL", customerURL);
        sign.put("data", list);

        String str = sign.toJSONString();
        String signStr = DesUtils.encrypt(str, DesUtils.KEY);
        JSONObject queryJson = new JSONObject();
        queryJson.put("code", code);
        queryJson.put("sign", signStr);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.postForEntity(SERVEL_URL + DRUG_ADD, queryJson, YaomengResponse.class);
        log.info("药盟药品对码接口，inData:{}, result:{}", str, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<List> body = responseEntity.getBody();
            if (body.getCode() == 0) {
                //成功
                return;
            } else if (body.getCode() == 1) {
                //全部失败
                JSONObject res =JSON.parseObject(body.getData().get(0).toString());
                throw new BusinessException("药盟系统提示：" + res.get("msg").toString());
            } else {
                List<LinkedHashMap<String,Object>> resultList = body.getData();
                log.error("药盟药品对码接口，部分失败, inData:{}, 失败数据:{}", str, JSON.toJSONString(resultList));//全部失败
                String msg = resultList.get(0).get("msg").toString();
                throw new BusinessException("药盟系统提示：" + msg);
            }
        }
        return;
    }

    /**
     * 获取病症信息
     * 返回状态1：成功
     * 返回状态非1：失败
     *
     * @param customerUrl    门店编号
     * @param drugId         药盟药品id
     * @param erpNo          erp编号
     */
    public static List<YaomengDiseaseDto> drugDisease(String yaomengServerUrl, String customerUrl, Long drugId, String erpNo) {
        customerUrl = "yd001";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("enterpriseCode", enterpriseCode);
        httpHeaders.add("customerUrl", customerUrl);
        String url = String.format(yaomengServerUrl + DRUG_DISEASE + "?drugId=%s&erpNo=%s", drugId, erpNo);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, YaomengResponse.class);
        log.info("药盟获取病症信息，customerUrl:{},enterpriseCode:{},drugId:{},erpNo:{}, result:{}",
                customerUrl, enterpriseCode, drugId, erpNo, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<Map> body = responseEntity.getBody();
            if (body.getCode() == 1) {
                YaomengDrugDiseaseDto dto = new YaomengDrugDiseaseDto();
                BeanUtil.fillBeanWithMap(body.getData(), dto, true);
                List<YaomengDiseaseDto> list = dto.getDiseases();
                return list;
            }
        }
        return null;
    }

    /**
     * 配伍禁忌限制校验
     * 维护完成患者信息，选择完药品病症后，进行校验，校验成功后才可以进行医生患者聊天对话
     * 返回状态1：成功
     * 返回状态非1：失败
     *
     * @param customerUrl    门店编号
     * @param bean
     * @return  返回不为空即为有禁忌
     */
    public static String checkLimit(String yaomengServerUrl, String customerUrl, YaomengCheckLimitBean bean) {
        customerUrl = "yd001";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("enterpriseCode", enterpriseCode);
        httpHeaders.add("customerUrl", customerUrl);
        List<Long> drugId = bean.getDrugId();
        String drugIdStr = Joiner.on(",").join(drugId);
        String url = String.format(yaomengServerUrl + CHECK_LIMIT + "?drugId=%s&sex=%s&age=%s&idCard=%s", drugIdStr, bean.getSex(), bean.getAge(), bean.getIdCard());
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, YaomengResponse.class);
        log.info("药盟配伍禁忌限制校验，url:{}, result:{}", url, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<String> body = responseEntity.getBody();
            if (body.getCode() == 1) {
                return null;
            } else {
                return body.getMsg();
            }
        }
        return "网路异常请重试";
    }

    /**
     * 获取医生信息
     * 获取医生信息接口，没有医生在线或未分配医生则返回失败
     * 返回状态1：成功
     * 返回状态非1：失败
     * @param customerUrl 门店编号
     * @return
     */
    public static YaomengDoctorDto getDoctor(String yaomengServerUrl, String customerUrl) {
        customerUrl = "yd001";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("enterpriseCode", enterpriseCode);
        httpHeaders.add("customerUrl", customerUrl);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.exchange(yaomengServerUrl + GET_DOCTOR, HttpMethod.GET, httpEntity, YaomengResponse.class);
        log.info("药盟获取医生信息，customerUrl:{}, result:{}", customerUrl, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<Map> body = responseEntity.getBody();
            if (body.getCode() == 1) {
                YaomengDoctorDto dto = new YaomengDoctorDto();
                BeanUtil.fillBeanWithMap(body.getData(), dto, true);
                return dto;
            }
        }
        return null;
    }

    /**
     * 话术查询
     * 返回状态0：成功
     * 返回状态1：失败
     *
     * @param customerUrl 门店编号
     * @param ids         病症id
     * @return
     */
    public static List<YaomengDialogueQuestionDto> dialogueQuestion(String yaomengServerUrl, String customerUrl, List<Long> ids) {
        customerUrl = "yd001";
        JSONObject sign = new JSONObject();
        sign.put("customerURL", customerUrl);
        sign.put("ids", ids);

        String str = sign.toJSONString();
        String signStr = DesUtils.encrypt(str, DesUtils.KEY);
        JSONObject queryJson = new JSONObject();
        queryJson.put("code", code);
        queryJson.put("sign", signStr);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.postForEntity(yaomengServerUrl + DIALOGUE_QUESTION, queryJson, YaomengResponse.class);
        log.info("药盟话术查询，ids:{}, customerUrl:{}, result:{}", JSON.toJSONString(ids), customerUrl, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<List> body = responseEntity.getBody();
            if (body.getCode() == 0) {
                List<YaomengDialogueQuestionDto> list = body.getData();
                return list;
            }
        }
        return null;
    }

    /**
     * 提交图文开方
     * 患者维护完成个人信息，选择药品病症，选择完成医生信息提交到此接口，接口返回成功则代表处方开具完成
     * 返回状态0：成功
     * 返回状态非0：失败  msg eg:  签名错误;阿莫西林胶囊最大开方量为5;所选病症数量不能大于5
     *
     * @param customerUrl
     * @param bean
     * @return
     */
    public static YaomengSpeedSimpleRep speedSimple(String yaomengServerUrl, String customerUrl, YaomengSpeedSimpleBean bean) {
        customerUrl = "yd001";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("enterpriseCode", enterpriseCode);
        httpHeaders.add("customerUrl", customerUrl);
        bean.setCustomerURL(customerUrl);
        bean.setEnterpriseCode(enterpriseCode);
        String signStr = DesUtils.encrypt(JSON.toJSONString(bean), DesUtils.KEY);
        JSONObject queryJson = new JSONObject();
        queryJson.put("code", code);
        queryJson.put("sign", signStr);
        HttpEntity httpEntity = new HttpEntity<>(queryJson);

        ResponseEntity<YaomengResponse> responseEntity = restTemplate.postForEntity(yaomengServerUrl + SPEED_SIMPLE, httpEntity, YaomengResponse.class);
        log.info("药盟提交图文开方，inData:{}, result:{}", JSON.toJSONString(bean), JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<Map> body = responseEntity.getBody();
            if (body.getCode() == 0) {
                YaomengSpeedSimpleRep rep = new YaomengSpeedSimpleRep();
                BeanUtil.fillBeanWithMap(body.getData(), rep, true);
                return rep;
            } else {
                String msg = body.getMsg();
                YaomengSpeedSimpleRep rep = new YaomengSpeedSimpleRep();
                rep.setErrorMsgTemp(msg);
                return rep;
            }
        }
        return null;
    }

    /**
     * 获取处方图片   返回的base64
     * year字段用于查询历史处方信息，历史处方为当前时间为15-31号，上个月及之前处方需要拼上年份，比如2021。1-15号处方，上个月无需传年份，上上月需要传年份。
     * 执业药师，调配人，核发人，支持签名图片url或者文字
     * @param no 处方号
     * @param pharmacist 执业药师
     * @return
     */
    public static String getPrescriptionFileBase64(String no, String pharmacist) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("enterpriseCode", enterpriseCode);
        httpHeaders.add("no", no);
        //todo year 字段判断 加到no后
        String url = String.format(SERVEL_URL + GET_PRESCRIPTION_FILE_64 + "?no=%s", no);
        if (StrUtil.isNotEmpty(pharmacist)) {
            url += "&pharmacist=" + pharmacist;
        }
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, YaomengResponse.class);
        log.info("获取处方图片Base64，url:{}, result:{}", url, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<String> body = responseEntity.getBody();
            if (body.getCode() == 0) {
                String dto = body.getData();
                return dto;
            }
        }
        return null;
    }

    /**
     * 获取处方图片  返回的文件流
     * year字段用于查询历史处方信息，历史处方为当前时间为15-31号，上个月及之前处方需要拼上年份，比如2021。1-15号处方，上个月无需传年份，上上月需要传年份。
     *
     * @param no 处方号
     * @param pharmacist 执业药师
     * @return
     */
    public static String getPrescriptionFile(String no, String pharmacist) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("enterpriseCode", enterpriseCode);
        httpHeaders.add("no", no);
        //todo year 字段判断 加到no后
        String url = String.format(SERVEL_URL + GET_PRESCRIPTION_FILE + "?no=%s", no);
        if (StrUtil.isNotEmpty(pharmacist)) {
            url += "&pharmacist=" + pharmacist;
        }
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<YaomengResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, YaomengResponse.class);
        log.info("获取处方图片，url:{}, result:{}", url, JSON.toJSONString(responseEntity));
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            YaomengResponse<String> body = responseEntity.getBody();
            if (body.getCode() == 0) {
                String dto = body.getData();
                return dto;
            }
        }
        return null;
    }

    /**
     * 获取处方图片url
     *
     * @param no         处方号
     * @param pharmacist 执业药师
     * @return
     */
    public static String getPrescriptionFileUrl(String yaomengServerUrl, String no, String pharmacist) {
        String url = String.format(yaomengServerUrl + GET_PRESCRIPTION_FILE + "?no=%s", no);
        if (StrUtil.isNotEmpty(pharmacist)) {
            url += "&enterpriseCode=" + enterpriseCode + "&pharmacist=" + Base64.encode(pharmacist);
        }
        log.info("药盟处方图片url = " + url);
        return url;
    }

    //药盟下载处方图片
    public static Map<String, Object> downLoadFile(String httpUrl) {
        log.info("药盟开始下载处方图片, url:" + httpUrl);
        Map<String, Object> map = new HashMap<>(2);
        URL url = null;
        InputStream inputStream = null;
        FileOutputStream fos = null;
        int random = ThreadLocalRandom.current().nextInt(10000, 99999);
        String fileTempName = "yaomeng" + random + ".jpg";
        map.put("fileName", fileTempName);
        try {
            url = new URL(httpUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置超时间为3秒
            conn.setConnectTimeout(3 * 1000);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

            //得到输入流
            inputStream = conn.getInputStream();
            //获取自己数组
            byte[] getData = readInputStream(inputStream);

            //文件保存位置
            File saveDir = new File(fileTempName);
            if (!saveDir.exists()) {
                saveDir.createNewFile();
            }
            fos = new FileOutputStream(saveDir);
            fos.write(getData);
            map.put("file", saveDir);
        } catch (IOException e) {
            log.error("药盟下载处方图片失败, url:" + httpUrl + "，error:" + e);
            return null;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        log.info("药盟结束下载处方图片, url:" + httpUrl + "，保存的图片名:" + fileTempName);
        return map;
    }

    /**
     * 从输入流中获取字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

}
