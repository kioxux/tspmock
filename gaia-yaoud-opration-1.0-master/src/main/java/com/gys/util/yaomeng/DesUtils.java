package com.gys.util.yaomeng;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author ZhangDong
 * @date 2021/11/2 15:47
 */
public class DesUtils {
    private static final String KEY_ALGORITHM = "DESede";
    private static final String DEFAULT_CIPHER_ALGORITHM = "DESede/ECB/PKCS5Padding";// 默认的加密算法
    public static final String KEY = "6QTEUMWJ0D";//固定不变的key


    public static void main(String[] args) {
        JSONObject sign = new JSONObject();
        //门店编号（必填）
        sign.put("customerURL", "yd001");
        // 是否已经对码 1 是 0否 （选填 ）
        sign.put("isCorrespond", 0);
        // erp编号 （用来查询已经对码的编号）
//        sign.put("erpNo", "");
        //药品名称（用来查询药品名称）
        sign.put("drugName", "阿莫西林克拉维酸钾分散片");

//        sign.put("state", "");

        String str = sign.toJSONString();
        //加密参数，固定值（药盟提供），必填
        String dTxfUYwqwJvT = encrypt(str, KEY);
        JSONObject json = new JSONObject();
        json.put("code", "yaode");
        json.put("sign", dTxfUYwqwJvT);
        System.out.println(json);

    }

    /**
     * DESede 加密操作
     *
     * @param content 待加密内容
     * @param key     加密密钥
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String key) {
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            // 创建密码器
            byte[] byteContent = content.getBytes("utf-8");
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(key));
            // 初始化为加密模式的密码器
            byte[] result = cipher.doFinal(byteContent);// 加密
            return new String(Base64Utils.encode(result));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    /**
     * }
     * 生成加密秘钥
     *
     * @return
     */
    private static SecretKeySpec getSecretKey(final String key) {
        KeyGenerator kg;
        try {
            kg = KeyGenerator.getInstance(KEY_ALGORITHM);
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(key.getBytes());
            kg.init(secureRandom);
            //生成一个密钥
            SecretKey secretKey = kg.generateKey();
            return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
