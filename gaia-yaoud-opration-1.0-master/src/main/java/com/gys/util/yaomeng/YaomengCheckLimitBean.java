package com.gys.util.yaomeng;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/11/3 16:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YaomengCheckLimitBean {

    private List<Long> drugId;//药盟药品id   必填
    private int sex;//性别 0.女 1.男   非必填
    private int age;//年龄  非必填
    private String idCard;//身份证号  必填

    public YaomengCheckLimitBean(List<Long> drugId, String idCard) {
        this.drugId = drugId;
        this.idCard = idCard;
    }
}
