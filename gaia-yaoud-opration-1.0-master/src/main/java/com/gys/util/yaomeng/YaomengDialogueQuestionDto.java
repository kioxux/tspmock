package com.gys.util.yaomeng;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author ZhangDong
 * @date 2021/11/3 17:41
 */
@Data
public class YaomengDialogueQuestionDto {

    private Long id;//唯一id
    private String word;//话术内容
    private Long dialogueId;//话术id
    private String yesText;// 肯定话术
    private String noText;//否定话术
    private Integer defaultAnswer;//默认  肯定/否定 0否定 1肯定
    private String dialogText;//弹框话术
    private Integer defaultDialog;//默认弹框 0否定 1肯定 2不弹框
    private Integer sexLimit;//性别限制 0女1男2不限
    private String ageLimit;//年龄限制
    private String placeholder;//占位符
    private Integer sort;//显示排序
    private LocalDateTime createDate; //创建时间
    private LocalDateTime updateDate;//修改时间

}
