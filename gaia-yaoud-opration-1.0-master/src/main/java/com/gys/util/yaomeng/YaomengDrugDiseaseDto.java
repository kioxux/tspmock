package com.gys.util.yaomeng;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/11/3 15:32
 */
@Data
public class YaomengDrugDiseaseDto {

    private List<YaomengDiseaseDto> diseases;
}
