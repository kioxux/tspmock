package com.gys.util.yaomeng;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZhangDong
 * @date 2021/11/3 14:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YaomengDrugAddBean {

    private String erpNo;//药德商品编码
    private Long drugId;//药盟商品编码

}
