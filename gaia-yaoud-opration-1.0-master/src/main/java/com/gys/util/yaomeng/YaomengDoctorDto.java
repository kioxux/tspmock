package com.gys.util.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/4 13:51
 */
@Data
public class YaomengDoctorDto {

    private String img;//医生头像
    private String goodAt;//医生擅长
    private String name;//医生名字
    private Integer autoCreateTime;//处方自动开方时间（0.手动开方，其他数字代表自动开方秒数，比如35,60等）
    private Long id;//医生id
    private String title;//医生职称
    private String depart;//科室
    private String uuid;//医生uuid（不常用，一般调用其他接口传入医生id）

}
