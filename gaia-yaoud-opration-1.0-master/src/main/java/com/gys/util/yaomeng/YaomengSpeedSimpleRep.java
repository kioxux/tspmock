package com.gys.util.yaomeng;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author ZhangDong
 * @date 2021/11/3 18:28
 */
@Data
public class YaomengSpeedSimpleRep {

    private int id;//处方id
    private String uuid;
    private LocalDateTime createDate;
    private String no;//处方单号
    private Long memberId;
    private String memberName;
    private Integer memberSex;
    private String memberAge;
    private String memberBirthday;
    private String memberTel;
    private String memberIdCard;
    private Object memberAllergy;
    private String memberAddress;
    private String saleTime;
    private String applicationTime;
    private int state;
    private int typeId;
    private String typeName;
    private int amount;
    private int doctorId;
    private String doctorName;
    private int doctorSectionId;
    private String doctorSectionName;
    private String doctorAddress;
    private Object signature;
    private int companyId;
    private String companyName;
    private int storeId;
    private String storeName;
    private String remark;
    private Long pharmacistId;
    private String pharmacistName;
    private String pharmacistSignature;
    private String pharmacistTime;
    private String templatName;
    private String description;
    private int fromState;
    private int showSeal;
    private int showDoctorAddr;
    private int doctorAutoAudit;
    private Object pharmacistAutoAudit;
    private Object dispatcherName;
    private Object dispatcherSignature;
    private String issuerName;
    private Object issuerSignature;
    private Object auditType;
    private Object pharmacistFingerprint;
    private int printStatus;

    private String errorMsgTemp;//传递开方返回的错误信息
}
