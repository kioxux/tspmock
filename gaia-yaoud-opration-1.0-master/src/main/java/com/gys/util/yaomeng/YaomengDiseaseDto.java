package com.gys.util.yaomeng;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZhangDong
 * @date 2021/11/3 15:33
 */
@Data
@NoArgsConstructor
public class YaomengDiseaseDto {

    private String _id;
    private Long id;//病症id
    private String uuid;
    private String name;//病症名称
    private Long pid;
    private Integer isShow;
    private String simpleCode;
    private Integer sexLimit;
    private Object trunkNameIds;

    public YaomengDiseaseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
