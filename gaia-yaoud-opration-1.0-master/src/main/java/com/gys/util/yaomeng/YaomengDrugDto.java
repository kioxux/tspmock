package com.gys.util.yaomeng;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/11/3 13:45
 */
@Data
public class YaomengDrugDto {

    private String unit;//药品单位  eg:盒
    private String drugName;//药品名称  eg:阿莫西林克拉维酸钾分散片
    private Long drugId;//药品id 唯一  eg:151771
    private String approvalNumber;//国药准字  eg:国药准字H20041621
    private String spec;//规格  eg:0.3125g*12片
    private String proSelfCode;//商品自编码

    private Integer number;//数量  回传处方信息接口用到
    private String erpNo;//   回传处方信息接口用到
    private List<YaomengDiseaseDto> yaomengDiseaseDtoList;
}
