package com.gys.util.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/3 13:32
 */
@Data
public class YaomengResponse<T> {

    private Byte code;
    private T data;
    private String msg;
    private Object page;
}
