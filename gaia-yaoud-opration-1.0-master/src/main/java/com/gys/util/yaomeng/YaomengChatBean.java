package com.gys.util.yaomeng;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZhangDong
 * @date 2021/11/3 18:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YaomengChatBean {

    private String msg;
    private Integer isMember;// 1.医生  0.患者
    private Boolean nextShow;
}
