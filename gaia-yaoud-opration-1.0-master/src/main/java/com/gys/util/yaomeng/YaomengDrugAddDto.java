package com.gys.util.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/3 14:13
 */
@Data
public class YaomengDrugAddDto {

    private Long drugId;
    private String erpNo;
    private String msg;

}
