package com.gys.util.csv;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.StringJoiner;

/**
 * @author wu mao yin
 * @Description: CSV 导出工具类
 * @date 2021/11/1 9:38
 */
@Slf4j
public class CsvUtil {

    /**
     * 初始化 CSV 标题
     *
     * @param writer writer
     * @param titles 标题集合
     */
    private static void initCsv(PrintWriter writer, String[] titles) {
        // 写入标题头
        StringJoiner stringJoiner = new StringJoiner(StrUtil.COMMA);
        for (String title : titles) {
            stringJoiner.add(title);
        }
        stringJoiner.add("\r\n");
        writer.write(new String(new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF}));
        writer.write(stringJoiner.toString());
    }

    /**
     * 导出Excel到浏览器
     *
     * @param response              response
     * @param eachWriteRowCount     总记录数
     * @param fileName              文件名称
     * @param titles                标题
     * @param writeCsvDataDelegated 向EXCEL写数据/处理格式的委托类 自行实现
     * @throws Exception
     */
    public static void exportCsvToWebsite(HttpServletResponse response, Integer eachWriteRowCount, String fileName,
                                          String[] titles, WriteCsvDataDelegated writeCsvDataDelegated) throws Exception {
        response.reset();
        response.setContentType("application/csv;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".csv", "UTF-8"));
        // 初始化 csv
        PrintWriter writer = new PrintWriter(response.getOutputStream());
        CsvUtil.initCsv(writer, titles);

        writeCsvDataDelegated.writeExcelData(writer, 1, eachWriteRowCount);

        closeWrite(writer);
    }

    private static void closeWrite(PrintWriter writer) {
        if (writer != null) {
            writer.close();
        }
    }

}