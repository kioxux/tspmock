package com.gys.util.csv;

import org.apache.poi.xssf.streaming.SXSSFSheet;

import java.io.PrintWriter;

/**
 * @author wu mao yin
 * @Description: EXCEL写数据委托类
 * @date 2021/11/1 9:38
 */
public interface WriteCsvDataDelegated {

    /**
     * EXCEL写数据委托类  针对不同的情况自行实现
     *
     * @param writer      writer
     * @param currentPage 分批查询开始页
     * @param pageSize    分批查询数据量
     * @throws Exception
     */
    void writeExcelData(PrintWriter writer, Integer currentPage, Integer pageSize) throws Exception;


}