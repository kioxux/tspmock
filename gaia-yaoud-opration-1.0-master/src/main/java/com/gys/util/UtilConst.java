//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.util;

public class UtilConst {
    public static final Integer CODE_0 = 0;
    public static final Integer CODE_401 = 401;
    public static final Integer CODE_500 = 500;
    public static final Integer CODE_1001 = 1001;
    public static final Integer CODE_200 = 200;
    public static final String LOGIN_TOKEN = "X-Token";
    public static final String STR_NUMBER_0 = "0";
    public static final String STR_NUMBER_1 = "1";
    public static final String STR_NUMBER_2 = "2";
    public static final String STR_NUMBER_3 = "3";
    public static final String STR_NUMBER_4 = "4";
    public static final String STR_NUMBER_5 = "5";
    public static final String STR_NUMBER_6 = "6";
    public static final String STR_NUMBER_10 = "10";
    public static final String STR_NUMBER_20 = "20";
    public static final String STR_NUMBER_30 = "30";
    public static final String STR_NUMBER_40 = "40";
    public static final Long TOKEN_EXPIRE = 86400L;
    public static final Long PHONE_CODE_EXPIRE = 300L;
    public static final String RECHARGE_CARD_LOSE_STATUS = "1";
    public static final String RECHARGE_CARD_UNLOSE_STATUS = "2";
    public static final String RECHARGE_CARD_NORMAL = "1";
    public static final String RECHARGE_CARD_AMT_NOT_ENOUGH = "2";
    public static final String WOMAN_0 = "0";
    public static final String MAN_1 = "1";
    public static final String WOMAN = "女";
    public static final String MAN = "男";
    public static final String ABB_PURE_DATE_PATTERN = "yyMMdd";
    public static final String YEAR_DATE_PATTERN = "yyyy";
    public static final String DECIMAL_FORMAT = "##########.##########";
    public static final String MEMBER_STATUS_NEW = "1";
    public static final String MEMBER_STATUS_OLD = "2";
    public static final String GSCHSTATUS_1 = "已清斗";
    public static final String GSCHSTATUS_0 = "未清斗";
    public static final String GSIHSTATUS_1 = "已装斗";
    public static final String GSIHSTATUS_0 = "未装斗";
    public static final String WF_CODE_AUTH_NOTICE = "GAIA_WF_001";
    public static final String WF_CODE_DELEGATE_NOTICE = "GAIA_WF_002";
    public static final String WF_CODE_SAVE_ORG_NOTICE = "GAIA_WF_003";
    public static final String WF_CODE_CATEGORY_ANALYSIS = "GAIA_WF_004";
    public static final String WF_CODE_RETURN_COMPADM = "GAIA_WF_005";
    public static final String WF_CODE_LOSS_COMPADM = "GAIA_WF_006";
    public static final String WF_CODE_LOSS_SINGLE = "GAIA_WF_007";
    public static final String WF_CODE_DPGJ_COMPADM = "GAIA_WF_008";
    public static final String WF_CODE_DPGJ_SINGLE = "GAIA_WF_009";
    public static final String WF_CODE_DPZL_COMPADM = "GAIA_WF_010";
    public static final String WF_CODE_DPZL_SINGLE = "GAIA_WF_011";
    public static final String WF_CODE_ZDZL_COMPADM = "GAIA_WF_012";
    public static final String WF_CODE_ZDZL_SINGLE = "GAIA_WF_013";
    public static final String WF_CODE_PRODUCT_GSP = "GAIA_WF_014";
    public static final String WF_CODE_SUPPLIER_GSP = "GAIA_WF_015";
    public static final String WF_CODE_CUSTOMER_GSP = "GAIA_WF_016";
    public static final String WF_CODE_PURCHASE_ORDER = "GAIA_WF_017";
    public static final String WF_CODE_PURCHASE_PAY = "GAIA_WF_018";
    public static final String WF_CODE_SPBS = "GAIA_WF_019";
    public static final String WF_CODE_ZZY = "GAIA_WF_020";
    public static final String WF_CODE_SPYH = "GAIA_WF_021";
    public static final String WF_CODE_CKBSBY = "GAIA_WF_022";
    public static final String WF_CODE_PHTZ = "GAIA_WF_023";
    public static final String WF_CODE_RKSH = "GAIA_WF_024";
    public static final String WF_CODE_RKYS = "GAIA_WF_025";
    public static final String WF_CODE_KCTZ = "GAIA_WF_026";
    public static final String PROM_SINGLE = "PROM_SINGLE";
    public static final String PROM_SERIES = "PROM_SERIES";
    public static final String PROM_GIFT = "PROM_GIFT";
    public static final String PROM_COUPON = "PROM_COUPON";
    public static final String PROM_HY = "PROM_HY";
    public static final String PROM_COMBIN = "PROM_COMBIN";
    public static final String REPEAT1 = "repeat1";
    public static final String REPEAT2 = "repeat2";
    public static final String REPEAT3 = "repeat3";
    public static final String ALLPRO = "assign1";
    public static final String SELEPRO = "assign2";
    public static final String GSPHPART_1 = "1";
    public static final String GSPHPART_2 = "2";
    public static final String GSPHPART_3 = "3";
    public static final String RECHARGE_CARD_PASSWORD = "123456";
    public static final int SALE_0 = 0;
    public static final int SALE_1 = 1;
    public static final int SALE_2 = 2;
    public static final int SALE_3 = 3;
    public static final int SALE_4 = 4;
    public static final String EXCLUSION1 = "exclusion1";
    public static final String EXCLUSION2 = "exclusion2";
    public static final String EXCLUSION3 = "exclusion3";
    public static final String EXCLUSION4 = "exclusion4";
    public static final String STATUS_1 = "正常";
    public static final String STATUS_2 = "会员卡折扣";
    public static final String STATUS_3 = "拆零";
    public static final String STATUS_4 = "促销";
    public static final String STATUS_5 = "会员价";
    public static final String STATUS_6 = "会员日折扣";
    public static final String STATUS_7 = "会员日特价";
    public static final String STATUS_8 = "改价";
    public static final String PARA_COS_SECRET_ID = "TENCENT_COS_SECRET_ID";
    public static final String PARA_COS_SECRET_KEY = "TENCENT_COS_SECRET_KEY";
    public static final String PARA_COS_REGION_NAME = "TENCENT_COS_REGION_NAME";
    public static final String PARA_COS_BUCKET_NAME = "TENCENT_COS_BUCKET_NAME";
    public static final Integer CODE_202 = 202;
    public static final String GYS = "GYS";

    public UtilConst() {
    }
}
