package com.gys.util;

import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @Author ：gyx
 * @Date ：Created in 16:24 2021/12/28
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
public class NullUtil {

    private static final NumberFormat numberFormat = DecimalFormat.getInstance();

    public static <V> V defaultNull(V t,V value) {
        return ObjectUtils.isEmpty(t) ? value : t;
    }

    public static String decimalFormatting(BigDecimal value) {
        DecimalFormat numberDecimalFormat;
        numberDecimalFormat=(DecimalFormat)numberFormat;
        numberDecimalFormat.applyPattern("0.00");
        return numberDecimalFormat.format(value);
    }

    public static void main(String[] args) {
        BigDecimal nullVar=new BigDecimal(0);
        String bigDecimal = decimalFormatting(defaultNull(nullVar, BigDecimal.ZERO));
        System.out.println(bigDecimal);
    }
}
