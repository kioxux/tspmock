//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.util;

import com.google.gson.Gson;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class SignUtil {
    public SignUtil() {
    }

    public static String getSign(Map<String, Object> data) {
        TreeMap<String, Object> arr = new TreeMap();
        Gson gson = new Gson();
        arr.put("body", gson.toJson(data.get("body")));
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", data.get("secret"));
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();

        String strSign;
        while (it.hasNext()) {
            strSign = it.next().toString();
            strSignTmp.append(strSign + "=" + arr.get(strSign) + "&");
        }

        strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        String sign = getMD5(strSign.toString());
        return sign;
    }

    public static String getSign(Map<String, Object> data, Map<String, Object> config) {
        TreeMap<String, Object> arr = new TreeMap();
        arr.put("body", data.get("body"));
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", config.get("secret"));
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();

        String strSign;
        while (it.hasNext()) {
            strSign = it.next().toString();
            strSignTmp.append(strSign + "=" + arr.get(strSign) + "&");
        }

        strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        String sign = getMD5(strSign.toString());
        return sign;
    }

    public static boolean checkSign(Map<String, Object> data, Map<String, Object> config) {
        String signFrom = getSign(data, config);
        String sign = data.get("sign").toString();
        return signFrom.equals(sign);
    }

    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);

            String hashtext;
            for (hashtext = number.toString(16); hashtext.length() < 32; hashtext = "0" + hashtext) {
            }

            return hashtext.toUpperCase();
        } catch (NoSuchAlgorithmException var5) {
            throw new RuntimeException(var5);
        }
    }
}
