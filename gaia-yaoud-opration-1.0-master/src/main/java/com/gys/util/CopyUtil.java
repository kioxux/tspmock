package com.gys.util;

import cn.hutool.core.collection.CollUtil;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 复制方法
 * @author xiaoyuan
 */
public class CopyUtil {

    /**
     * 集合复制
     * @param source
     * @param clazz
     * @param <T>
     * @return
     * 案例 : List<GaiaSdPayMethodOutData> outData = CopyUtil.copyList(paymentMethods, GaiaSdPayMethodOutData.class);
     */
    public static <T> List<T> copyList(List source, Class<T> clazz) {
        List<T> target = new ArrayList<>();
        if (CollUtil.isNotEmpty(source)){
            if (CollUtil.isNotEmpty(source)){
                for (Object c: source) {
                    T obj = copy(c, clazz);
                    target.add(obj);
                }
            }
        }
        return target;
    }

    /**
     * 单个复制
     * @param source
     * @param clazz
     * @param <T>
     * @return
     * 案例 :  Resource resource = CopyUtil.copy(resourceDto, Resource.class);
     */
    public static <T> T copy(Object source, Class<T> clazz) {
        if (source == null) {
            return null;
        }
        T obj = null;
        try {
            obj = clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BeanUtils.copyProperties(source, obj);
        return obj;
    }
}