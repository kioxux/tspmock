package com.gys.util;

import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import com.gys.common.response.ResultEnum;
import com.gys.common.response.ResultUtil;
import com.gys.common.data.Tencent;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.auth.COSSigner;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.Download;
import com.qcloud.cos.transfer.TransferManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.09
 */
@Slf4j
@Component
public class CosUtils {

    @Resource
    Tencent tencent;

    public Result uploadFile(ByteArrayOutputStream bos, String fileName) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(tencent.getCosSecretId(), tencent.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(tencent.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);

            byte[] barray = bos.toByteArray();
            int length = barray.length;

            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);

            // 保存目录
            String key = tencent.getExportPath() + fileName;

            PutObjectRequest putObjectRequest = new PutObjectRequest(tencent.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return ResultUtil.success(this.urlAuth(key));
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 文件签名
     *
     * @param path
     * @return
     */
    public String urlAuth(String path) {
        if (StringUtils.isBlank(path)) {
            return null;
        }
        String secretId = tencent.getCosSecretId();
        String secretKey = tencent.getCosSecretKey();
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(tencent.getCosRegion());
        ClientConfig clientConfig = new ClientConfig(region);
        // 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // 存储桶的命名格式为 BucketName-APPID，此处填写的存储桶名称必须为此格式
        String bucketName = tencent.getCosBucket();
        String key = path.startsWith("/") ? path : "/" + path;
        GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);
        // 设置签名过期时间(可选), 若未进行设置, 则默认使用 ClientConfig 中的签名过期时间(1小时)
        // 这里设置签名在半个小时后过期
        Date expirationDate = new Date(System.currentTimeMillis() + 60L * 60L * 1000L * 24L);
        req.setExpiration(expirationDate);
        URL url = cosClient.generatePresignedUrl(req);
        log.info(url.toString());
        if (url.toString().toLowerCase().startsWith("http:")) {
            return url.toString().replace("http:", "https:");
        }
        return url.toString();
    }


    public JsonResult uploadFileNew(ByteArrayOutputStream bos, String fileName) {
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(tencent.getCosSecretId(), tencent.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(tencent.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);

            byte[] barray = bos.toByteArray();
            int length = barray.length;

            // 获取文件流
            InputStream byteArrayInputStream = new ByteArrayInputStream(barray);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
            objectMetadata.setContentLength(length);

            // 保存目录
            String key = tencent.getExportPath() + fileName;

            PutObjectRequest putObjectRequest = new PutObjectRequest(tencent.getCosBucket(), key, byteArrayInputStream, objectMetadata);
            // 设置 Content type, 默认是 application/octet-stream
            putObjectRequest.setMetadata(objectMetadata);
            // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard_IA);
            PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

            String eTag = putObjectResult.getETag();
            System.out.println(eTag);
            // 关闭客户端
            cosClient.shutdown();
            return JsonResult.success(this.urlAuth(key));
        } catch (Exception e) {
            return JsonResult.error("导出失败！");
        }
    }


    /**
     * @Author jiht
     * @Description 下载图片
     * @Date 2022/1/16 21:07
     * @Param [key, localDownFile]
     * @Return void
     **/
    public void download(String key,File localDownFile) {
        TransferManager transferManager = null;
        try {
            // 1 初始化秘钥信息
            COSCredentials cred = new BasicCOSCredentials(tencent.getCosSecretId(), tencent.getCosSecretKey());
            // 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
            ClientConfig clientConfig = new ClientConfig(new Region(tencent.getCosRegion()));
            // 3 生成cos客户端
            COSClient cosClient = new COSClient(cred, clientConfig);
            // 指定要上传到 COS 上的路径
            ExecutorService threadPool = Executors.newFixedThreadPool(32);
            // 传入一个 threadpool, 若不传入线程池, 默认 TransferManager 中会生成一个单线程的线程池。
            transferManager = new TransferManager(cosClient, threadPool);
            //下载到本地指定路径
            GetObjectRequest getObjectRequest = new GetObjectRequest(tencent.getCosBucket(), tencent.getExportPath() + key);
            // 下载文件
            Download download = transferManager.download(getObjectRequest, localDownFile);
            // 等待传输结束（如果想同步的等待上传结束，则调用 waitForCompletion）
            download.waitForCompletion();
            System.out.println("下载成功");
        } catch (Throwable tb) {
            System.out.println("下载失败");
            tb.printStackTrace();
        } finally {
            // 关闭 TransferManger
            transferManager.shutdownNow();
        }
    }
}
