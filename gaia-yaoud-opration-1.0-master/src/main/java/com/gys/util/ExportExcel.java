package com.gys.util;

import com.gys.common.base.ExportExcelInterface;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collection;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/4/16 14:35
 * @Version 1.0.0
 **/
public class ExportExcel<T> implements ExportExcelInterface<T> {

    @Override
    public void exportExcel(String[] headers, Collection<T> dataset, OutputStream out, String pattern) throws Exception {
        SXSSFWorkbook workbook = new SXSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        SXSSFSheet sheet = workbook.createSheet("sheet1");
        //默认宽度
        sheet.setDefaultColumnWidth(30);

//        //创建一行并在其中放入一些单元格。行是基于0的。
//        Row row = sheet.createRow(0);
//        //创建一个单元格并在其中放一个值
//        Cell cell = row.createCell(0);
//        cell.setCellValue(1);
//        //或者写在一行上。
//        row.createCell(2).setCellValue(2);
//        row.createCell(3).setCellValue(true);
//        //我们将第二个单元格样式设置为日期(和时间)。重要的是
//        ////从工作簿中创建一个新的单元格样式，否则你会结束
//        ////修改内建的样式，不仅影响这个单元格，而且影响其他单元格。
//        CellStyle cellStyle = workbook.createCellStyle();
//        cellStyle.setDataFormat(
//                createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
        try (OutputStream fileOut = new FileOutputStream("workbook.xls")) {
            workbook.write(fileOut);
        }

    }

    @Override
    public void exportExcel2(String title, String[] headers1, String[] headers2, Collection<T> dataset, OutputStream out, String pattern) throws Exception {

    }

    @Override
    public void exportExcelInitStyle(String[] headers, Collection<T> dataset, OutputStream out, String pattern) throws Exception {

    }
}
