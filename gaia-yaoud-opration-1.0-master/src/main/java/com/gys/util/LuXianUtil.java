package com.gys.util;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.gys.common.exception.BusinessException;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 17:42 2021/8/11
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Component
public class LuXianUtil {

    private static final Logger logger = LoggerFactory.getLogger("泸县药监对接接口");
    private static String USERNAME;
    @Value("${luxian.username}")
    private void setUSERNAME(String USERNAME) {
        LuXianUtil.USERNAME = USERNAME;
    }
    private static String PASSWORD;
    @Value("${luxian.password}")
    private void setPASSWORD(String PASSWORD) {
        LuXianUtil.PASSWORD = PASSWORD;
    }
    private static String URL;
    @Value("${luxian.URL}")
    private void setURL(String url) {
        URL = url;
    }
    private static final String LUXIAN_ACCESSTOKEN = "LUXIAN_ACCESSTOKEN";
    private static RedisTemplate redisTemplate;
    @Autowired
    public void setRedisTemplate(RedisTemplate redisTemplate) {
        LuXianUtil.redisTemplate = redisTemplate;
    }

    private static String getAccessToken() {
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("loginName", USERNAME);
        jsonObject.set("password", PASSWORD);
        jsonObject.set("type", "enterprise");
        logger.info("<获取 accessToken>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject,URL+"/accessToken/issue");
        logger.info("<获取 accessToken>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && !resultObj.get("id").toString().isEmpty()) {
            return resultObj.get("id", String.class);
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    private static String renewalToken(){
        String accesstoken="";
        if(redisTemplate.opsForHash().hasKey(LUXIAN_ACCESSTOKEN,"token")){
            accesstoken = (String) redisTemplate.opsForHash().get(LUXIAN_ACCESSTOKEN,"token");
        }

        if (StringUtils.isEmpty(accesstoken)){
            accesstoken = getAccessToken();
            HashMap<String, String> tokenMap = new HashMap<>();
            tokenMap.put("token",accesstoken);
            redisTemplate.opsForHash().putAll(LUXIAN_ACCESSTOKEN,tokenMap);
            redisTemplate.expire(LUXIAN_ACCESSTOKEN,50, TimeUnit.MINUTES);
        }
        return accesstoken;
    }


    /**
     * 推送单据
     * @param jsonObject
     * @return
     */
    public static JSONObject sendOrder(JSONObject jsonObject) throws IOException {
        //写入json文件
        String fileName= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))+"inboundRecord.json";
        File file=FileUtil.createNewFile("/temp/"+"luxian/"+ fileName);
        writeString(file,jsonObject.toString());
        logger.info("<推送单据>参数打印{}", jsonObject);


        //发送post请求
        String postResult=sendHttpFile(file,URL+"/data/receive");
        logger.info("<推送单据>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && !resultObj.containsKey("errorCode")) {
            //推送成功，删除file
            file.delete();

            JSONObject returnObj=new JSONObject();
            returnObj.set("code",0);
            returnObj.set("id",resultObj.get("id"));
            return returnObj;
        }
        throw new BusinessException(resultObj.get("errorMessage",String.class));
    }

    public static JSONObject getStatus(String id) {
        //发送get请求
        String getResult = sendGet(URL+"/data/check?id="+id);
        logger.info("<订单状态>结果打印{}", getResult);
        JSONObject resultObj = JSONUtil.toBean(getResult, JSONObject.class);
        if (resultObj != null && !resultObj.containsKey("errorCode")) {
            JSONObject returnObj=new JSONObject();
            returnObj.set("status",resultObj.get("status"));
            returnObj.set("result",resultObj.get("result"));
            return returnObj;
        }
        throw new BusinessException(resultObj.get("errorMessage",String.class));
    }

    private static String sendHttp(JSONObject jsonObject,String url){
        return HttpRequest.post(url)
                //设置请求头
                .header("Content-Type", "application/json")
                //传输参数
                .body(jsonObject.toString())
                .execute()
                .body();
    }

//    public static void main(String[] args) throws IOException {
//
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.set("inboundDocNumber","测试222222");
//        jsonObject.set("commodityCode","LK6935727400076");
//        jsonObject.set("packingSpec","6g*10袋22");
//        jsonObject.set("lotNumber","20111111");
//        jsonObject.set("enterpriseLocationCode","10000");
//        jsonObject.set("inboundDate","2021-11-01");
//        jsonObject.set("type","1");
//        jsonObject.set("companyName","江苏平光信谊(焦作)中药有限公司");
//        jsonObject.set("transcator","测试");
//        jsonObject.set("conclusion","通过");
//        jsonObject.set("expirationDate","2022-11-01");
//        jsonObject.set("productionDate","2021-01-01");
//        jsonObject.set("quantity","5");
//        jsonObject.set("unit","盒");
//        jsonObject.set("reason","");
//        jsonObject.set("relativeDocNumber","");
//        jsonObject.set("producer","");
//        jsonObject.set("producingArea","");
//        jsonObject.set("remarks","");
//        jsonObject.set("processType","1");
//
//
//        //写入json文件
//        String fileName= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))+"inboundRecord.json";
//        File file=FileUtil.createNewFile("/temp/"+"luxian/"+ fileName);
//        writeString(file,jsonObject.toString());
//
//
//        String ss=sendHttpFile(file,"http://223.85.160.105:31100/data/receive");
//
//    }

    public static void writeString(File file,String content){

        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(content);
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            try {
                //关闭流释放资源
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static String sendHttpFile(File file, String url) throws IOException {

        HttpPost httpPost = new HttpPost(url);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        builder.addBinaryBody("data",file);
        HttpEntity multipart = builder.build();

        httpPost.setEntity(multipart);
        httpPost.addHeader("accessToken",renewalToken());
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = client.execute(httpPost);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            String result = EntityUtils.toString(entity, "UTF-8");
            return result;
        }
       return null;
    }

    private static String sendGet(String url){
        return HttpRequest.get(url)
                //设置请求头
                .header("Content-Type", "application/json")
                .header("accessToken",renewalToken())
                .execute()
                .body();
    }
}
