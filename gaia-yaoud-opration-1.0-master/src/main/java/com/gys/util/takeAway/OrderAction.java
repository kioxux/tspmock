package com.gys.util.takeAway;

import org.apache.commons.collections.map.HashedMap;

import javax.annotation.PostConstruct;
import java.util.Map;

public class OrderAction {
    private static Map<Integer,String> orderAction;
    public static String getOrderActionById(Integer id) {
        if (id > 4)
            return "无此订单动作";
        else
            return orderAction.get(id);
    }
    public static String getOrderActionById(String id) {
        try {
            int iid = Integer.valueOf(id);
            return getOrderActionById(iid);
        } catch (Exception ex) {
            return "订单动作异常！";
        }
    }

    @PostConstruct
    public void init() {
        orderAction=new HashedMap();
        orderAction.put(0,"无动作");
        orderAction.put(1,"新订单下发");
        orderAction.put(2,"用户催单");
        orderAction.put(3,"门店申请退单");
        orderAction.put(4,"用户申请退单");
        orderAction.put(5,"用户申请退款");
        orderAction.put(6,"同意用户退款申请");
        orderAction.put(7,"驳回用户退款申请");
    }
}
