package com.gys.util.takeAway.mt;

public class MTConfig {
    public final static String MT_CMD_MEDICINE_STOCK = "MT_CMD_MEDICINE_STOCK";
    public final static String MT_CMD_MEDICINE_SAVE = "MT_CMD_MEDICINE_SAVE";
    public final static String MT_CMD_MEDICINE_UPDATE = "MT_CMD_MEDICINE_UPDATE";

    //getAccessToken
    public static final String MT_ACCESS_TOKEN_GET = "https://waimaiopen.meituan.com/api/v1/oauth/authorize";
    //refreshToken
    public static final String MT_ACCESS_TOKEN_REFRESH = "https://waimaiopen.meituan.com/api/v1/oauth/token";

}
