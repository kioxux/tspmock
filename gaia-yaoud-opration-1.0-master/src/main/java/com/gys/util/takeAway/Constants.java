package com.gys.util.takeAway;

public class Constants {

    public final static String USER_TOKEN = "USER_TOKEN";

    public final static int BUFFER_SIZE = 1024 * 1024 * 20;

    public final static String ERROR_CODE_1001 = "1001";

    public final static String PLATFORMS_MT = "MT";//美团

    public final static String PLATFORMS_EB = "EB";//饿百

    public final static String PLATFORMS_JD = "JD";//京东到家

    public final static String PLATFORMS_KY = "KY";//快药

    public final static String PLATFORMS_YL = "YL";//药联

    public final static String PLATFORMS_DDKY = "DDKY";//叮当快药

    public final static String PLATFORMS_JGG = "JGG";//九宫格(遂宁汇通)

    public final static String PLATFORMS_MINI_MALL = "WSC";//微商城

}
