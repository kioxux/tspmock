package com.gys.util.takeAway.mt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/7/9 17:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MtRefreshTokenBean {

    private String app_id;
    private String grant_type;
    private String refresh_token;
    private String sig;
}
