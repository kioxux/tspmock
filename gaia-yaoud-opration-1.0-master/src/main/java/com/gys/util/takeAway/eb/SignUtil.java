package com.gys.util.takeAway.eb;

import com.google.common.base.Joiner;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class SignUtil {

    /**
     * 根据参数获取签名
     * @return String
     */
    public static String getSign(Map<String, String> data) {
        TreeMap<String, Object> arr = new TreeMap<String, Object>();
        arr.put("body", data.get("body"));//JSON.toJSONString(data.get("body"), SerializerFeature.BrowserCompatible)
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", data.get("secret"));
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            strSignTmp.append(key + "=" + arr.get(key) + "&");
        }
        String strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        String sign = getMD5(strSign.toString());
        return sign;
    }
    /**
     * 根据参数获取签名
     *
     * @param
     * @return String
     */
    public static String getSign(Map<String, Object> data, String source,String secret) {
        Map<String, Object> arr = new HashMap<>();
        arr.put("body", data.get("body"));
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", secret);
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        //sort排序后的参数
        Map<String, Object> paramsMap = MapSortUtil.sortMapByKey(arr);
        //用&拼接成字符串
        String bodySign = Joiner.on("&").withKeyValueSeparator("=").join(paramsMap);
        //md5工具类
        return getMD5(bodySign);
    }

    /**
     * 校验签名是否正确
     *
     * @param
     * @return boolean
     */
    public static boolean checkSign(Map<String, Object> data, String source,String secret) {
        String signFrom = getSign(data, source,secret);
        String sign = data.get("sign").toString();
        if (signFrom.equals(sign)) {
            return true;
        } else {
            System.out.println(data);
            return false;
        }
    }

    /**
     * 获取MD5
     *
     * @param
     * @return String
     */
    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext.toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
//        String aa = getMD5("body={\"name\":\"测试店铺\",\"shop_id\":\"12345\"}&cmd=order.get&encrypt=aes&secret=test123&source=37641616&ticket=69CB08F4-DBF1-4781-9DF4-BEFB66D48F07&timestamp=1590993036&version=3");
        String aa =getMD5("body={\"platform_shop_id\":\"32267798003\",\"order_id\":\"2146527022225409151\"}&cmd=order.create&encrypt=&secret=2j6QA5pITJQ&source=33873511&ticket=8E3C6B78-C9C9-4655-A0EA-9916A4765C96&timestamp=1616307252&version=3");
        System.out.println(aa);
        //0649777B663943FF7F7B0C80D69BC0DE
    }
}
