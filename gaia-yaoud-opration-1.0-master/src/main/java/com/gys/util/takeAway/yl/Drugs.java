package com.gys.util.takeAway.yl;

import lombok.Data;

import java.io.Serializable;

@Data
public class Drugs implements Serializable {
    private static final long serialVersionUID = 5124371061528883906L;
    /**
     * 门店ID | 必须
     */
    private String id;

    /**
     * 商品编码 | 必须 |
     */
    private String goods_no;
    /**
     * 数据创建时间 | 必须 |
     */
    private String create_time;
    /**
     * 数据最新更新时间（判断是否需要同步更新） | 必须 |
     */
    private String update_time;

    /**
     * 商品名称 | 必须 |
     */
    private String name;

    /**
     *  商品助记码 | String | 非必须 |
     */
    private String memory_code;

    /**
     *  商品条码（商品的条形码） | 必须 |
     */
    private String code;

    /**
     *  国药准字Z51033213 |批准文号 | 必须（非药或者中药可以为空） |
     */
    private String number;

    /**
     * 售价 | 必须 |
     */
    private String price;

    /**
     * 优惠价
     */
    private String member_price;

    /**
     * 包装 | 必须 |
     */
    private String pack;

    /**
     * | 生成厂商 | 必须 |
     */
    private String manufacturer;

    /**
     *  ["1","2","4"] |商品所属分类ID数组 | 必须 |
     */
    private String[] category_id;

    /**
     *  ["1","2"] | 商品标签数组，1:DTP，2:低毛商品 | 根据业务 |
     */
    private String[] tags;

    /**
     * 36粒/盒 |规格 | 必须 |
     */
    private String form;

    /**
     * 所属价格组(默认为门店id) | 必须 |
     */
    private String group_id;

    /**
     * 状态，新增为1，注销为0 | 必须 |
     */
    private String status;

    /**
     *  省 | 必须
     */
    private String province;

    /**
     * 市 | 必须 |
     */
    private String city;

    /**
     *  营业时间（24小时制）| 必须 |
     */
    private String  business_time;
}
