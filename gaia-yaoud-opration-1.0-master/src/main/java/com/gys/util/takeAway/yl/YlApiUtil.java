package com.gys.util.takeAway.yl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.common.data.yaolian.*;
import com.gys.common.data.yaolian.common.RequestHead;
import com.gys.common.data.yaolian.common.YlResponseData;
import com.gys.util.UtilConst;
import com.gys.util.takeAway.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/7 16:21
 */
public class YlApiUtil {
    private static final Logger logger = LoggerFactory.getLogger(YlApiUtil.class);

    public static final String SERVER_URL_TEST = "http://data.push.backend.turboradio.cn/api/partner/data";
    public static final String SERVER_URL_PRD = "http://data.push.backend.uniondrug.cn/api/partner/data";
    //正式环境和测试环境，都是用的下面两个
    public static final String COOPERATION = "hebeibosen";//改为配置化
    public static final String TOKEN = "uniondrug";
    public static final String NONCE = "94092762BDA12EF580DA1B773738DE5E1";

    /**
     * 订单数据录入接口
     */
    public static boolean orderDataInput(YlOrderInputBean bean, String cooperation) {
        RequestHead requestHead = getRequestHead(cooperation);
        bean.setRequestHead(requestHead);
        bean.setChannel("1");

        /*List<YlOrderBean> orders = Lists.newArrayList();
        YlOrderBean ylOrderBean = new YlOrderBean();
        ylOrderBean.setId("210820126255466016799");
        ylOrderBean.setStore_id("10000005GYS1000");
        ylOrderBean.setCreate_time("2021-09-08 13:43:09");
        ylOrderBean.setUpdate_time("2021-09-08 17:43:09");
        ylOrderBean.setMember_id("");
        ylOrderBean.setMember_name("");
        ylOrderBean.setMember_mobile("");
        ylOrderBean.setTotal_price("8");
        List<YlOrderDetailBean> retailDetail = Lists.newArrayList();
        YlOrderDetailBean detailBean = new YlOrderDetailBean();
        detailBean.setDrug_id("01000220028");
        detailBean.setCommon_name("复方硫酸双阱屈嗪片");
        detailBean.setAmount("1");
        detailBean.setNumber("12314");//todo 批准文号
        detailBean.setForm("20片*3板");
        detailBean.setPack("盒");
        detailBean.setPrice("90");
        detailBean.setUnit_price("90");
        detailBean.setCode("6942817000115");
        detailBean.setBatch_number("P20200000001");
        detailBean.setExpirydate("20200911");
        retailDetail.add(detailBean);
        ylOrderBean.setRetailDetail(retailDetail);
        orders.add(ylOrderBean);
        bean.setOrders(orders);*/
        logger.info("药联-订单录入接口,inData:{}", JSON.toJSONString(bean));

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YlResponseData> responseEntity = restTemplate.postForEntity(SERVER_URL_PRD, httpEntity, YlResponseData.class);
        //(errno=0, error=, dataType=OBJECT, data=null)
        YlResponseData response = responseEntity.getBody();
        logger.info("药联-订单录入接口,inData:{}，result:{}", JSON.toJSONString(bean), JSON.toJSONString(response));
        return "0".equals(response.getErrno());
    }

    private static RequestHead getRequestHead(String cooperation) {
        RequestHead requestHead = new RequestHead();
        requestHead.setCooperation(cooperation);
        requestHead.setNonce(NONCE);
        String timestamp = getTimeStamp();
        requestHead.setTimestamp(timestamp);
        String sign = getSign(timestamp, NONCE);
        requestHead.setSign(sign);
        return requestHead;
    }

    /**
     * 商品价格&amp;库存实时查询接口
     */
    public static void medicineQuery() {
        HttpUtils.doPost("", "", Maps.newHashMap());
    }

    /**
     * 库存数据录入接口
     * syncStock 是否是异动更新
     */
    public static boolean stockUpdate(String cooperation, YlStockInputBean bean, boolean syncStock) {
        RequestHead requestHead = getRequestHead(cooperation);
        bean.setRequestHead(requestHead);
        bean.setChannel("1");

        if (syncStock) {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime before = now.minusMinutes(1);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String beforeTime = before.format(dateTimeFormatter);
            String nowTime = now.format(dateTimeFormatter);
            List<YlStockData> stocks = bean.getStocks();
            for (YlStockData stock : stocks) {
                stock.setCreate_time(beforeTime);
                stock.setUpdate_time(nowTime);
            }
        }
        logger.info("药联-库存上传接口,inData:{}", JSON.toJSONString(bean));

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YlResponseData> responseEntity = restTemplate.postForEntity(SERVER_URL_PRD, httpEntity, YlResponseData.class);
        YlResponseData response = responseEntity.getBody();
        logger.info("药联-库存上传接口,inData:{}，result:{}", JSON.toJSONString(bean), JSON.toJSONString(response));
        return "0".equals(response.getErrno());
    }

    /**
     * 商品类目录入接口
     */
    public static boolean medicineCategoryInput(String cooperation, List<String> categoryNameList) {
        YlMedicineCategory bean = new YlMedicineCategory();
        RequestHead requestHead = getRequestHead(cooperation);
        bean.setRequestHead(requestHead);
        List<YlMedicineCategoryDetail> categorys = new ArrayList<>(categoryNameList.size());
        for (int i = 0; i < categoryNameList.size(); i++) {
            String categoryName = categoryNameList.get(i);
            YlMedicineCategoryDetail detail = new YlMedicineCategoryDetail();
            detail.setCategory_id(String.valueOf(i + 1));
            detail.setParent_id("0");
            detail.setCategory_name(categoryName);
            detail.setStatus("1");
//            detail.setLevels("0");
//            detail.setCategory_alias("RX");
            detail.setSort("1");
            categorys.add(detail);
        }
        bean.setCategorys(categorys);
        logger.info("药联-商品类目录入接口,inData:{}", JSON.toJSONString(bean));

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YlResponseData> responseEntity = restTemplate.postForEntity(SERVER_URL_PRD, httpEntity, YlResponseData.class);
        YlResponseData response = responseEntity.getBody();
        logger.info("药联-商品类目录入接口,inData:{}，result:{}", JSON.toJSONString(bean), JSON.toJSONString(response));
        return "0".equals(response.getErrno());
    }

    /**
     * 商品数据录入接口
     */
    public static void commodityEntry() {
        JsonRootBean bean = new JsonRootBean();
        RequestHead requestHead = new RequestHead();
        requestHead.setCooperation("hebeibosen");
        String nonce = "94092762BDA12EF580DA1B773738DE5E1";
        requestHead.setNonce(nonce);
        String timestamp = getTimeStamp();
        requestHead.setTimestamp(timestamp);
        requestHead.setSign(getSign(timestamp, nonce));
        requestHead.setTradeDate(DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN));

        bean.setRequestHead(requestHead);
        bean.setChannel("1");
        List<Drugs> categorys = Lists.newArrayList();
        Drugs detail = new Drugs();
        detail.setId("01000220028");//商品内码
        detail.setGoods_no("01000220028");//商品编码
        detail.setCreate_time("2021-09-08 15:21:21");
        detail.setUpdate_time("2021-09-08 15:21:21");
        detail.setName("复方硫酸双阱屈嗪片");//名称
        detail.setMemory_code("FFLSSJQQP");//助记码
        detail.setCode("6942817000115");//69
        detail.setNumber("国药准字H32026237");//国药
        detail.setPrice("90");
        detail.setMember_price("90");
        detail.setPack("盒");
        detail.setManufacturer("常州制药厂有限公司");
        detail.setCategory_id(new String[]{"1"});
        detail.setTags(new String[]{"1"});
        detail.setForm("20片*3板");
        detail.setGroup_id("10000005&GYS10000");
        detail.setStatus("1");
        detail.setProvince("北京省");
        detail.setCity("北京市");
        detail.setBusiness_time("07:00-20:00");

        categorys.add(detail);
        bean.setDrugs(categorys);
        System.out.println("data：" + JSON.toJSONString(bean));
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YlResponseData> responseEntity = restTemplate.postForEntity(SERVER_URL_PRD, httpEntity, YlResponseData.class);
        YlResponseData response = responseEntity.getBody();
        System.out.println(response);
    }


    public static void main(String[] args) {
        String storeId = "10000005" + UtilConst.GYS + "10000";
        YlStockInputBean bean = new YlStockInputBean();
        List<YlStockData> stocks = new ArrayList<>(1);
        YlStockData stockData = new YlStockData();
        stockData.setDrug_id("01000000005");
        stockData.setQuantity("7.0000");
        stockData.setStore_id(storeId);
        stocks.add(stockData);
        bean.setStocks(stocks);
        stockUpdate("", bean, true);
//        medicineCategoryInput();
//        commodityEntry();
//        medicineCategoryInput();
//        stockUpdate();
//        orderDataInput();
    }


    public static String getTimeStamp() {
        String timestamp = String.valueOf(new Date().getTime() / 1000);
        return timestamp;
    }


    //nonce 说是全部写94092762BDA12EF580DA1B773738DE5E1
    public static String getSign(String timestamp, String nonce) {
        String[] data = {nonce, timestamp, TOKEN};
        Arrays.sort(data);
        String sign_origin = "";
        for (int i = 0; i < data.length; i++) {
            sign_origin += data[i];
        }
        String sign = getSha1(sign_origin);
        System.out.println(sign);
        return sign;
    }

    public static String getSha1(String str) {
        if (null == str || 0 == str.length()) {
            return null;
        }
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
            mdTemp.update(str.getBytes("UTF-8"));

            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] buf = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 门店数据录入
     * @param storeDataList
     * @return
     */
    public static YlResponseData storeInitByYaolian(String cooperation, List<GaiaStoreData> storeDataList){
        YlStore ylStore = new YlStore();
        List<Store> stores = new ArrayList<>();

//        Store store = new Store();
//        store.setId("10000005&GYS10000");
//        store.setNumber("10000");
//        store.setName("苏州二寿堂药房有限公司");
//        store.setAddress("苏州市相城区轨道交通2号线蠡口站1F-01");
//        store.setPhone("123");
//        store.setGroup_id("10000005&GYS10000");
//        store.setStatus("1");
//        store.setCreate_time(String.valueOf(new DateTime()));
//        store.setUpdate_time(String.valueOf(new DateTime()));
//        store.setLongitude("");
//        store.setLatitude("31.298982");
//        store.setProvince("北京市");
//        store.setCity("北京市");
//        store.setArea("朝阳区");
//        store.setBusiness_time("07:00-20:00");
//        stores.add(store);
        storeDataList.forEach(item->{
            Store store = new Store();
            store.setId(item.getClient()+ UtilConst.GYS+item.getStoCode());
            store.setNumber(item.getStoCode());
            store.setName(item.getStoName());
            store.setAddress(item.getStoAdd());
            store.setPhone(item.getStoTelephone());
            store.setGroup_id(item.getClient()+UtilConst.GYS+item.getStoCode());
            store.setStatus("1");
            store.setCreate_time(String.valueOf(new DateTime()));
            store.setUpdate_time(String.valueOf(new DateTime()));
            store.setLongitude(item.getLongitude());
            store.setLatitude(item.getLatitude());
            store.setProvince(item.getPorvinceCn());
            store.setCity(item.getCityCn());
            store.setArea(item.getDistrictCn());
            store.setBusiness_time(item.getGsstDailyOpenDate()+"-"+ item.getGsstDailyCloseDate());
            stores.add(store);
        });

        ylStore.setStores(stores);
        RequestHead requestHead = getRequestHead(cooperation);
        String tradeDate = String.valueOf(new DateTime());
        requestHead.setTradeDate(tradeDate);
        ylStore.setRequestHead(requestHead);
        ylStore.setChannel("1");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        HttpEntity httpEntity = new HttpEntity(ylStore, headers);
        logger.info("药联-门店数据录入，inData:{}", JSON.toJSONString(ylStore));
        ResponseEntity<YlResponseData> responseEntity = restTemplate.postForEntity(SERVER_URL_PRD, httpEntity, YlResponseData.class);
        YlResponseData response = responseEntity.getBody();
        logger.info("药联-门店数据录入，inData:{}, result:{}", JSON.toJSONString(ylStore), JSON.toJSONString(response));
        return response;
    }

}
