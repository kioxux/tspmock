package com.gys.util.takeAway.mt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.pts.PtsApiUtil;
import com.sankuai.meituan.shangou.open.sdk.domain.SystemParam;
import com.sankuai.meituan.shangou.open.sdk.request.*;
import com.sankuai.meituan.shangou.open.sdk.response.SgOpenResponse;
import lombok.Data;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 美团外卖-接口封装类
 *
 * @author Mirai.lee
 * @date 2018.11.20
 */
public class MtApiUtil  {

    private static Logger logger = LoggerFactory.getLogger(MtApiUtil.class);

    /**
     * 美团订单API：设订单为商家已收到
     *
     * @param appId       美团分配商家的app_id
     * @param appSecret   门店的secret
     * @param orderId     美团平台订单ID
     */
    public static boolean orderReceived(String appId, String appSecret, String orderId) {
        boolean ret = false;
        try {
            SystemParam sysPram = new SystemParam(appId, appSecret);
            OrderPoiReceivedRequest orderPoiReceivedRequest = new OrderPoiReceivedRequest(sysPram);
            orderPoiReceivedRequest.setOrder_id(orderId);
            SgOpenResponse sgOpenResponse = orderPoiReceivedRequest.doRequest();
            //发起请求时的sig，用来联系美团员工排查问题时使用
            String requestSig = sgOpenResponse.getRequestSig();
            //请求返回的结果，按照官网的接口文档自行解析即可
            String result = sgOpenResponse.getRequestResult();
            ret = validApiResult(result);
            logger.info("美团订单API：商家收到订单! 订单ID：{}, 执行结果：{}", orderId, result);
        } catch (Exception e) {
            logger.error("美团订单API：商家收到订单! 订单ID：{}, 执行异常：{}", orderId, e.getMessage());
        }
        return ret;
    }

    public static void main(String[] args) {
        //测试库存更新
        //List<MtMedicineData> medicineData = Lists.newArrayList(new MtMedicineData("20160227", "cre8oqqi", "2"));//2
        //medicineData.add(new MtMedicineData("A002", "6515_2694697", "72"));//72
        //medicineStock("5085", "d05e53d5975ef34b38208cd42fbd4c57", "cre8oqqi", medicineData, null);

        //测试门店商品列表查询
//        medicineList("6515","6074eb73b3bec029bb9baf62d04905dc", "6515_2694697",4,2);

//        String s = "https://waimaiopen.meituan.com/api/v1/order/getOrderDaySeq?app_id=0000&app_poi_code=31号测试店&timestamp=1389751221a00ba58f000001aa697ab000006d52d";
//        String s1 = org.apache.commons.codec.digest.DigestUtils.md5Hex(s.getBytes());
//        String temp = DigestUtils.md5DigestAsHex(s.getBytes());
//        System.out.println(temp);
//        System.out.println(s1);

        //测试获取accessToken
        //{"status":0,"state":null,"message":"","access_token":"token_gZBhvL1whXvi8aGchudCRg","expires_in":2592000,"refresh_token":"refresh_token_kczbRSfDF9Um_fo2iYFKyA","re_expires_in":15552000}
        //{"status":0,"state":null,"message":"","access_token":"token_l83yI0d205DDXYXZSmlLpQ","expires_in":2590654,"refresh_token":"refresh_token_mxGuAWgHKUkTCafoM2pHFg","re_expires_in":15550654}
//        TokenResponse accessToken = getAccessToken("6515", "6515_2694697", "6074eb73b3bec029bb9baf62d04905dc");
//        System.out.println(accessToken);

        //确认订单
//        String result = orderConfirm("6515", "6074eb73b3bec029bb9baf62d04905dc", "26946974037301817", "token_l83yI0d205DDXYXZSmlLpQ");
//        System.out.println(result);

        //{"status":0,"state":null,"message":"","access_token":"token_l7PLcNzPY2oqf3DPIFdGvQ","expires_in":2592000,"refresh_token":"refresh_token_nSo1fzEkcEODYFxaFqdKPw","re_expires_in":15552000}
        //{"status":0,"state":null,"message":"","access_token":"token_hzFm9KGWt4euY6SLsRZLmw","expires_in":2592000,"refresh_token":"refresh_token_v-_0IrbaJia5VSK4PEdJ9A","re_expires_in":15552000}
        //{"status":0,"state":null,"message":"","access_token":"token_nZ7jb9YK9_NJIPYaONNJsw","expires_in":2592000,"refresh_token":"refresh_token_nFCPy8-llbAajqx_8YRNsw","re_expires_in":15552000}
        //{"status":0,"state":null,"message":"","access_token":"token_mcbr7gIgZcxi7ApKsiRFGw","expires_in":2592000,"refresh_token":"refresh_token_pwrzmU9yCZMOnN8WC4tAXA","re_expires_in":15552000}
        //{"status":0,"state":null,"message":"","access_token":"token_l83yI0d205DDXYXZSmlLpQ","expires_in":2592000,"refresh_token":"refresh_token_mxGuAWgHKUkTCafoM2pHFg","re_expires_in":15552000}
        //String s = refreshToken("6515", "6515_2694697", "6074eb73b3bec029bb9baf62d04905dc", "refresh_token_pwrzmU9yCZMOnN8WC4tAXA");
        //System.out.println(s);

        //测试订单取消
        //orderCancel("5085", "d05e53d5975ef34b38208cd42fbd4c57", "103925032239520992", "系统取消，超时未确认", "1001", null);

        //商品列表查询
        //medicineList("5085", "d05e53d5975ef34b38208cd42fbd4c57", "cre8oqqi", 0, 200, null);

        List<Map<String, String>> batchPullPhoneNumber = getBatchPullPhoneNumber("5085", "d05e53d5975ef34b38208cd42fbd4c57");
        System.out.println(String.format("result:%s", JSON.toJSONString(batchPullPhoneNumber)));

    }

    /**
     * 获取授权token，调部分接口需要传此token
     * token 30天失效; refresh_token 180天失效
     */
    public static TokenResponse getAccessToken(String appId, String appPoiCode, String appSecret) {
        logger.info("美团API，开始获取accessToken, appId:{}, appPoiCode:{}", appId, appPoiCode);
        long timeStamp = getTimeStamp();
        String url = String.format(MTConfig.MT_ACCESS_TOKEN_GET + "?app_id=%s&app_poi_code=%s&response_type=token&timestamp=%s%s", appId, appPoiCode, timeStamp, appSecret);
        String sig = DigestUtils.md5DigestAsHex(url.getBytes());
        String allUrl = String.format(MTConfig.MT_ACCESS_TOKEN_GET + "?app_id=%s&app_poi_code=%s&response_type=token&timestamp=%s&sig=%s", appId, appPoiCode, timeStamp, sig);
        RestTemplate restTemplate = new RestTemplate();
        //{"status":0,"state":null,"message":"","access_token":"token_sk-3BTfvduGtcidW9D9I1Q","expires_in":2592000,"refresh_token":"refresh_token_sVAIT8D7hDSDzru_fVRCzA","re_expires_in":15552000}
        //可反复调用，有效期内返回的token相同
        TokenResponse response = restTemplate.getForObject(allUrl, TokenResponse.class);
        logger.info("美团API，结束获取accessToken, appId:{}, appPoiCode:{}, result:{}", appId, appPoiCode, response);
        return response;
    }

    public static TokenResponse refreshToken(String appId, String appPoiCode, String appSecret, String refreshToken) {
        logger.info("美团API，开始刷新accessToken, appId:{}, appPoiCode:{}, refreshToken:{}", appId, appPoiCode, refreshToken);
        long timeStamp = getTimeStamp();
        String url = String.format(MTConfig.MT_ACCESS_TOKEN_REFRESH + "?app_id=%s&grant_type=refresh_token&refresh_token=%s&timestamp=%s%s", appId, refreshToken, timeStamp, appSecret);
        String sig = DigestUtils.md5DigestAsHex(url.getBytes());
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("app_id", appId);
        map.add("grant_type", "refresh_token");
        map.add("refresh_token", refreshToken);
        map.add("sig", sig);
        map.add("timestamp", String.valueOf(timeStamp));
        RestTemplate restTemplate = new RestTemplate();
        //可反复调用，有效期内返回的token相同
        ResponseEntity<TokenResponse> responseEntity = restTemplate.postForEntity(MTConfig.MT_ACCESS_TOKEN_REFRESH, map, TokenResponse.class);
        TokenResponse response = null;
        if (responseEntity != null) {
            //报错示例：{"status":778,"state":null,"message":"failed to check:值不应为空,nonblank param app_id is required!","access_token":"","expires_in":0,"refresh_token":"","re_expires_in":0}
            //报错示例2：{"status":769,"state":null,"message":"failed to check refresh token:refresh token不存在或已经过期","access_token":"","expires_in":0,"refresh_token":"","re_expires_in":0}
            response = responseEntity.getBody();
        }
        logger.info("美团API，结束刷新accessToken, appId:{}, appPoiCode:{}, result:{}", appId, appPoiCode, responseEntity);
        return response;
    }

    @Data
    public static class TokenResponse {
        private Integer status;
        private Object state;
        private String message;
        private String access_token;
        private long expires_in;//token失效时间
        private String refresh_token;
        private long re_expires_in;//refresh_token失效时间
    }

    /**
     * 美团订单API：商家确认订单
     *
     * @param appId     美团分配商家的app_id
     * @param appSecret 门店的secret
     * @param orderId   美团平台订单ID
     */
    public static String orderConfirm(String appId, String appSecret, String orderId, String accessToken) {
        String result;
        String requestSig = null;
        try {
            SystemParam sysPram = new SystemParam(appId, appSecret);
            sysPram.setAppPoiAccessToken(accessToken);
            OrderConfirmRequest orderConfirmRequest = new OrderConfirmRequest(sysPram);
            orderConfirmRequest.setOrder_id(orderId);
            SgOpenResponse sgOpenResponse = orderConfirmRequest.doRequest();
            //发起请求时的sig，用来联系美团员工排查问题时使用
            requestSig = sgOpenResponse.getRequestSig();
            //请求返回的结果，按照官网的接口文档自行解析即可
            String requestResult = sgOpenResponse.getRequestResult();
            result = validApiResult(requestResult) ? "ok" : "error";
            logger.info("美团订单API：商家确认订单! 平台订单ID：{}，requestSig:{}, 执行结果：{}", orderId, requestSig, requestResult);
        } catch (Exception e) {
            logger.error("美团订单API：商家确认订单! 平台订单ID：{}，requestSig:{},  执行异常：{}", orderId, requestSig, e.getMessage());
            result = "ERROR" + e.getMessage();
        }
        return result;
    }

    /**
     * 接口返回参数校验
     *
     * @param result
     * @return
     */
    private static boolean validApiResult(String result) {
        Map<String, Object> resultMap = (Map<String, Object>) JSONObject.parse(result);
        String data = String.valueOf(resultMap.get("data"));
        if ("ok".equals(data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 美团订单API：商家取消订单
     *
     * @param appId      美团分配商家的app_id
     * @param appSecret  门店的secret
     * @param orderId    美团平台订单ID
     * @param reason     取消原因
     * @param reasonCode 规范化取消原因code
     *                   {
     *                   "data":"ok",
     *                   "msg":"订单取消成功,取消配送单成功"
     *                   }
     */
    public static String orderCancel(String appId, String appSecret, String orderId, String reason, String reasonCode, String accessToken) {
        String result;
        String requestSig = null;
        try {
            SystemParam systemParam = new SystemParam(appId, appSecret);
            systemParam.setAppPoiAccessToken(accessToken);
            OrderCancelRequest orderCancelRequest = new OrderCancelRequest(systemParam);
            orderCancelRequest.setOrder_id(orderId);
            orderCancelRequest.setReason(reason);
            orderCancelRequest.setReason_code(Integer.valueOf(reasonCode));
            SgOpenResponse sgOpenResponse = orderCancelRequest.doRequest();
            //发起请求时的sig，用来联系美团员工排查问题时使用
            requestSig = sgOpenResponse.getRequestSig();
            //请求返回的结果，按照官网的接口文档自行解析即可
            //{"data":"ok","msg":"取消订单成功"}
            //{"data":"ok","msg":"订单已经取消过了"}
            String requestResult = sgOpenResponse.getRequestResult();
            result = validApiResult(requestResult) ? "ok" : "error";
            logger.info("美团订单API：商家取消订单! 订单ID:{}, requestSig:{}, 执行结果:{}", orderId, requestSig, requestResult);
        } catch (Exception e) {
            logger.error("美团订单API：商家取消订单! 订单ID:{}, requestSig:{}, 执行异常:{}", orderId, requestSig, e.getMessage());
            result = "ERROR" + e.getMessage();
        }
        return result;
    }
//
//    /**
//     * 美团订单API：订单确认退款请求
//     * @param appId     美团分配商家的app_id
//     * @param appSecret 门店的secret
//     * @param orderId   美团平台订单ID
//     * @param reason    确认退款详情
//     */
//    public static String orderRefundAgree(String appId, String appSecret, String orderId, String reason) {
////		boolean ret = false;
//        String result;
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            result = APIFactory.getOrderAPI().orderRefundAgree(sysPram, Long.valueOf(orderId), reason);
////			ret = validApiResult(result);
//            logger.info("美团订单API：订单确认退款请求! 订单ID：" + orderId + " 执行结果：" + result);
//        } catch (ApiOpException e) {
//            logger.error("美团订单API：订单确认退款请求! 订单ID：" + orderId + " 执行异常：" + e.getMsg());
//            result=e.getMsg();
//        } catch (ApiSysException e) {
//            logger.error("美团订单API：订单确认退款请求! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
//            result=e.getMessage();
//        }
////		return ret;
//        return result;
//    }
//
//    /**
//     * 美团订单API：驳回订单退款申请
//     * @param appId     美团分配商家的app_id
//     * @param appSecret 门店的secret
//     * @param orderId   美团平台订单ID
//     * @param reason    取消原因
//     */
//    public static String orderRefundReject(String appId, String appSecret, String orderId, String reason) {
////		boolean ret = false;
//        String result;
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            result = APIFactory.getOrderAPI().orderRefundReject(sysPram, Long.valueOf(orderId), reason);
////			ret = validApiResult(result);
//            logger.info("美团订单API：驳回订单退款申请! 订单ID：" + orderId + " 执行结果：" + result);
//        } catch (ApiOpException e) {
//            logger.error("美团订单API：驳回订单退款申请! 订单ID：" + orderId + " 执行异常：" + e.getMsg());
//            result=e.getMsg();
//        } catch (ApiSysException e) {
//            logger.error("美团订单API：驳回订单退款申请! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
//            result=e.getMessage();
//        }
////		return ret;
//        return result;
//    }

    //美团批量库存更新最多200组
    public static final int STOCK_UPDATE_MAX_COUNT = 200;

    /**
     * 美团药品API：更新药品库存
     *
     * @param appId        美团分配商家的app_id
     * @param appSecret    门店的secret
     * @param appPoiCode   APP方门店id  public_code
     * @param medicineData 药品库存数据
     * app_id不存在情况存在于：      美团用饿百的APP_ID和APP_SECRET才会报app_id不存在
     */
    public static String medicineStock(String appId, String appSecret, String appPoiCode, List<MtMedicineData> medicineData, String accessToken) {
        String result = "";
        String requestSig = "";
        try {
            SystemParam sysPram = new SystemParam(appId, appSecret);
            sysPram.setAppPoiAccessToken(accessToken);
            MedicineStockRequest request = new MedicineStockRequest(sysPram);
            request.setApp_poi_code(appPoiCode);
            String medicineDataString = JSON.toJSONString(medicineData);
            request.setMedicine_data(medicineDataString);
            SgOpenResponse sgOpenResponse = request.doRequest();
            //发起请求时的sig，用来联系美团员工排查问题时使用
            requestSig = sgOpenResponse.getRequestSig();
            //请求返回的结果，按照官网的接口文档自行解析即可
            //success:  {"msg":"线上门店批量更新药品库存成功","data":"ok"}
            //fail:     {"data":"ng","error":{"msg":"[{\"app_medicine_code\":\"[\\\"A001-1\\\"]\",\"error_msg\":\"不存在此药品\"}]","code":1}}  存在部分成功
            String requestResult = sgOpenResponse.getRequestResult();
            String resultData = getResultData(requestResult);
            logger.info("美团药品API,更新药品库存! 门店ID:{},sysPram:{},inData:{},执行结果:{}", appPoiCode, JSON.toJSONString(sysPram), medicineDataString, requestResult);
            return resultData;
        } catch (Exception e) {
            //如果 request.setApp_poi_code(appPoiCode);  参数appPoiCode门店不存在，则会报错进catch
            result = "ER:" + e.getMessage();
            logger.error("美团药品API：更新药品库存! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage() + "请求sig：" + requestSig);
        }
        return result;
    }

    private static String getResultData(String requestResult) {
        JSONObject jsonObject = JSON.parseObject(requestResult);
        return jsonObject.getString("data");
    }

    /**
     * 查询门店药品列表
     *
     * @param appId
     * @param appSecret
     * @param appPoiCode  需要传public_code
     * @param offset     分页查询偏移量，表示从第几页开始查询，需根据公式得到：页码(舍弃小数)=offset/limit+1。例如：offset=23，limit=5，
     *                   根据公式计算结果表示从第5页开始查询，且每页5个商品，即本次请求结果将展示门店内第21～第25条商品数据。(2)【limit】字段有值时，此字段必填。
     * @param limit      分页每页展示药品数量，须为大于0的整数，最多支持200。
     */
    public static String medicineList(String appId, String appSecret, String appPoiCode, int offset, int limit, String accessToken) {
        String requestSig = null;
        try {
            SystemParam systemParam = new SystemParam(appId, appSecret);
            systemParam.setAppPoiAccessToken(accessToken);
            //组建请求参数,如有其它参数请补充完整
            MedicineListRequest medicineListRequest = new MedicineListRequest(systemParam);
            medicineListRequest.setApp_poi_code(appPoiCode);
            medicineListRequest.setOffset(offset);
            medicineListRequest.setLimit(limit);
            //发起请求
            SgOpenResponse sgOpenResponse = medicineListRequest.doRequest();
            //发起请求时的sig，用来联系美团员工排查问题时使用
            requestSig = sgOpenResponse.getRequestSig();
            //请求返回的结果，按照官网的接口文档自行解析即可
            //error eg:{"data":"ng","result_code":4,"error":{"msg":"failed to check:值不应为空,param accessToken is required!","code":778}}
            //success eg:{"extra_info":{"total_count":3605},"data":[]}
            String requestResult = sgOpenResponse.getRequestResult();
            logger.info("美团药品API, 查询门店药品列表,result:{}, appPoiCode:{}, requestSig:{}", requestResult, appPoiCode, requestSig);
            return requestResult;
        } catch (Exception e) {
            logger.error("美团药品API, 查询门店药品列表,error:{}, appPoiCode:{}, requestSig:{}", e.getMessage(), appPoiCode, requestSig);
            return "error";
        }
    }

    private static long getTimeStamp() {
        long l = System.currentTimeMillis() / 1000;
        return l;
    }


//
//    /**
//     * 下发美团配送订单(接入美团配送选接) 商家确认订单，准备到配送阶段了
//     * @param appId     美团分配商家的app_id
//     * @param appSecret 门店的secret
//     * @param orderId   平台订单ID
//     * @Author: Li.Deman
//     * @Date: 2018/11/23
//     */
//    public static void orderLogisticsPush(String appId, String appSecret, String orderId) {
//        try {
//            SystemParam sysParam = new SystemParam(appId, appSecret);
//            String result = APIFactory.getOrderAPI().orderLogisticsPush(sysParam, Long.valueOf(orderId));
//            logger.info("美团订单API：下发美团配送订单! 订单ID：" + orderId + " 执行结果：" + result);
//        } catch (ApiOpException e) {
//            logger.error("美团订单API：下发美团配送订单! 订单ID：" + orderId + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            logger.error("美团订单API：下发美团配送订单! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
//        }
//    }
//
//    /**
//     * @Description: 更新美团药品信息，已存在的更新，全新的则新增
//     * @Param: [appId, appSecret, appPoiCode, isExist, medicine_data]
//     * @return: java.lang.String
//     * @Author: Qin.Qing
//     * @Date: 2018/11/26
//     */
//    public static String medicineInfoUpdate(String appId, String appSecret, String appPoiCode, Boolean isExist, MedicineParam medicine_data) {
//        String result="";
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            if (isExist) {
//                if (MTFreqUtil.checkFreq(MTConfig.MT_CMD_MEDICINE_UPDATE)) {
//                    result = APIFactory.getMedicineAPI().medicineUpdate(sysPram, medicine_data);
//                }
//            } else {
//                if (MTFreqUtil.checkFreq(MTConfig.MT_CMD_MEDICINE_SAVE)) {
//                    result = APIFactory.getMedicineAPI().medicineSave(sysPram, medicine_data);
//                }
//            }
//            logger.info("美团药品API：更新药品信息! 门店ID：" + appPoiCode + " 执行结果：" + result);
//        } catch (ApiOpException e) {
//            result = "ER:" + e.getMsg();
//            logger.error("美团药品API：更新药品信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            result = "ER:" + e.getMessage();
//            logger.error("美团药品API：更新药品信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage());
//        }
//        return result;
//    }
//
//    /**
//     * @Description: 获取平台已有药品清单
//     * @Param: [appId, appSecret, appPoiCode]
//     * @return: java.util.List<com.sankuai.meituan.waimai.opensdk.vo.MedicineParam>
//     * @Author: Qin.Qing
//     * @Date: 2018/11/26
//     */
//    public static List<MedicineParam> getmedicineList(String appId, String appSecret, String appPoiCode) {
//        List<MedicineParam> result = new ArrayList<>();
//        try {
//            int getOffset=0;
//            int getLimit=200;
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            //result = APIFactory.getMedicineAPI().medicineList(sysPram, appPoiCode);//美团框架调整，更新SDK版本位1.0.18版，采用分页式查询药品清单。2019年7月4日后，将不在支持此接口。
//            while (true) {
//                List<MedicineParam> tempList = APIFactory.getMedicineAPI().medicineListByPage(sysPram, appPoiCode, getOffset, getLimit);
//                if (tempList.size()<=0){
//                    break;
//                }else{
//                    getOffset+=getLimit;
//                    result.addAll(tempList);
//                }
//            }
////            logger.info("美团药品API：查询门店药品清单信息! 门店ID：" + appPoiCode + " 执行结果：返回结果条目" + result.size());
//            return result;
//        } catch (ApiOpException e) {
//            logger.error("美团药品API：查询门店药品清单信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            logger.error("美团药品API：查询门店药品清单信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage());
//        }
//        return result;
//    }
//
//    /**
//     * @Description: 更新药品分类信息
//     * @Param: [appId, appSecret, appPoiCode, isExist, cat]
//     * @return: void
//     * @Author: Qin.Qing
//     * @Date: 2018/11/26
//     */
//    public static String medicineCatUpdate(String appId, String appSecret, String appPoiCode, Boolean isExist, MedicineCatParam cat) {
//        String result;
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            if (isExist) {
//                result = APIFactory.getMedicineAPI().medicineCatUpdate(sysPram, cat);
//            } else {
//                result = APIFactory.getMedicineAPI().medicineCatSave(sysPram, cat);
//            }
//            logger.info("美团药品API：更新药品信息! 门店ID：" + appPoiCode + " 执行结果：" + result);
//        } catch (ApiOpException e) {
//            result ="ER:"+e.getMsg();
//            logger.error("美团药品API：更新药品信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            result="ER:"+ e.getMessage();
//            logger.error("美团药品API：更新药品信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage());
//        }
//        return result;
//    }
//
//    /**
//     * @Description: 获取已存在药品分类信息
//     * @Param: [appId, appSecret, appPoiCode]
//     * @return: java.util.List<com.sankuai.meituan.waimai.opensdk.vo.MedicineCatParam>
//     * @Author: Qin.Qing
//     * @Date: 2018/11/26
//     */
//    public static List<MedicineCatParam> getmedicineCatList(String appId, String appSecret, String appPoiCode) {
//        List<MedicineCatParam> result = null;
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            result = APIFactory.getMedicineAPI().medicineCatList(sysPram, appPoiCode);
//            logger.info("美团药品API：查询门店药品分类清单信息! 门店ID：" + appPoiCode + " 执行结果：返回结果条目" + result.size());
//            return result;
//        } catch (ApiOpException e) {
//            logger.error("美团药品API：查询门店药品分类清单信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            logger.error("美团药品API：查询门店药品分类清单信息! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage());
//        }
//        return result;
//    }
//
//    /**
//    * @Description: 获取已在线门店信息
//    * @Param: [appId, appSecret]
//    * @return: java.util.List<java.lang.String>
//    * @Author: Qin.Qing
//    * @Date: 2018/11/27
//    */
//    public static List<String> getshopList(String appId, String appSecret) {
//        List<String> result=new ArrayList();
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            String strresult = APIFactory.getPoiAPI().poiGetIds(sysPram);
//            result= JSON.parseArray(strresult,String.class);
//            logger.info("美团药品API：查询门店药品分类清单信息! 门店ID：" + "" + " 执行结果：返回结果条目" + result.size());
//            return result;
//        } catch (ApiOpException e) {
//            logger.error("美团药品API：查询门店药品分类清单信息! 门店ID：" + "" + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            logger.error("美团药品API：查询门店药品分类清单信息! 门店ID：" + "" + " 执行异常：" + e.getMessage());
//        }
//        return result;
//    }
//
//    public static String medicineCodeUpdate(String appId, String appSecret, String appPoiCode, List<MedicineCodeUpdateParam> medicineCodeUpdateParams){
//        String result="";
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            result=APIFactory.getMedicineAPI().medicineCodeUpdate(sysPram,appPoiCode,medicineCodeUpdateParams);
//        }catch (ApiOpException e) {
//            logger.error("美团药品API：更新药品SAP编码信息! 门店ID：" + "" + " 执行异常：" + e.getMsg());
//        } catch (ApiSysException e) {
//            logger.error("美团药品API：更新药品SAP编码信息! 门店ID：" + "" + " 执行异常：" + e.getMessage());
//        }
//        return  result;
//    }
//
//    /**
//     * @description：商家确认已完成出餐
//     * @param appId
//     * @param appSecret
//     * @param orderId
//     * @author：Li.Deamn
//     * @date：2018/11/30
//     */
//    public static String preparationMealComplete(String appId, String appSecret, String orderId){
//        String result;
//        try {
//            SystemParam sysParam = new SystemParam(appId, appSecret);
//            result=APIFactory.getOrderAPI().orderPreparationMealComplete(sysParam, orderId);
//            logger.info("美团订单API：商家确认已完成出餐! 订单ID：" + orderId + " 执行结果：" + result);
//        }catch (ApiOpException e){
//            logger.error("美团订单API：商家确认已完成出餐! 订单ID：" + orderId + " 执行异常：" + e.getMsg());
//            result=e.getMsg();
//        }catch (ApiSysException e){
//            logger.error("美团订单API：商家确认已完成出餐! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
//            result=e.getMessage();
//        }
//        return result;
//    }
//

    /**
     * 拉取用户真实手机号
     * 如果拉取结果为空，则代表当前无受影响订单或故障已处理完毕
     * @param appId
     * @param appSecret
     * @return
     */
    public static List<Map<String, String>> getBatchPullPhoneNumber(String appId, String appSecret) {
        List<Map<String, String>> result = new ArrayList<>();
        try {
            int offset = 0;
            while (true) {
                SystemParam sysPram = new SystemParam(appId, appSecret);
                OrderBatchPullPhoneNumberRequest request = new OrderBatchPullPhoneNumberRequest(sysPram);
//                request.setApp_poi_code(shopId);
                request.setOffset(offset);
                request.setLimit(1000);
                SgOpenResponse sgOpenResponse = request.doRequest();
                //发起请求时的sig，用来联系美团员工排查问题时使用
                String requestSig = sgOpenResponse.getRequestSig();
                //请求返回的结果，按照官网的接口文档自行解析即可
                String requestResult = sgOpenResponse.getRequestResult();
                JSONObject jsonObject = JSONObject.parseObject(requestResult);
                String data = jsonObject.getString("data");
                JSONArray jsonArray = JSON.parseArray(data);
                for (Object item : jsonArray) {
                    result.add((Map<String, String>) item);
                }
                offset++;
                if (jsonArray.size() < 1000) break;
            }
            logger.info("美团药品API：查询用户真实电话号码! 执行结果：返回结果条目" + result.size());
            return result;
        } catch (Exception e) {
            logger.error("美团药品API：查询用户真实电话号码! 执行异常：" + e.getMessage());
        }
        return result;
    }

//    /**
//     * 门店营业和休息2种状态更新
//     * @param appId
//     * @param appSecret
//     * @param appPoiCode
//     * @param open_level
//     * @return
//     * @author：Li.Deman
//     * @date：2018/12/25
//     */
//    public static String shopOpenUpdate(String appId, String appSecret, String appPoiCode, String open_level) {
//        String result;
//        SystemParam sysPram = new SystemParam(appId, appSecret);
//        //关闭
//        if(open_level.equals("0")){
//            try{
//                result=	APIFactory.getPoiAPI().poiClose(sysPram,appPoiCode);
//                logger.info("美团门店API：门店设置为休息状态! 门店ID：" + appPoiCode + " 执行结果：" + result);
//            }catch (ApiOpException e) {
//                result = "ER:" + e.getMsg();
//                logger.error("美团门店API：门店设置为休息状态! 门店ID：" + appPoiCode + " 执行异常：" + e.getMsg());
//            } catch (ApiSysException e) {
//                result = "ER:" + e.getExceptionEnum().getMsg();
//                logger.error("美团门店API：门店设置为休息状态! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage());
//            }
//        }else {
//            //开启
//            try{
//                result=	APIFactory.getPoiAPI().poiOpen(sysPram,appPoiCode);
//                logger.info("美团门店API：门店设置为营业状态! 门店ID：" + appPoiCode + " 执行结果：" + result);
//            }catch (ApiOpException e) {
//                result = "ER:" + e.getMsg();
//                logger.error("美团门店API：门店设置为营业状态! 门店ID：" + appPoiCode + " 执行异常：" + e.getMsg());
//            } catch (ApiSysException e) {
//                result = "ER:" + e.getExceptionEnum().getMsg();
//                logger.error("美团门店API：门店设置为营业状态! 门店ID：" + appPoiCode + " 执行异常：" + e.getMessage());
//            }
//        }
//        return result;
//    }
//
//    /**
//    * @Description: 批量更新药品主数据信息
//    * @Param: [appId, appSecret, appPoiCode, medicineParams]
//    * @return: java.lang.String
//    * @Author: Qin.Qing
//    * @Date: 2019/04/03
//    */
//    public static String medicineInfobatchUpdate(String appId, String appSecret, String appPoiCode, List<MedicineParam> medicineParams) {
//        String result;
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            result= APIFactory.getMedicineAPI().medicineBatchUpdate(sysPram,appPoiCode,medicineParams);
//            logger.info("美团门店API：批量更新门店：" + appPoiCode + "商品数据，执行结果：" + result);
//        }catch (ApiOpException e){
//            logger.error("美团订单API：批量更新门店：" + appPoiCode + "商品数据，执行异常：" + e.getMsg());
//            result=e.getMsg();
//        }catch (ApiSysException e){
//            logger.error("美团订单API：批量更新门店：" + appPoiCode + "商品数据， 执行异常：" + e.getMessage());
//            result=e.getMessage();
//        }
//        return result;
//    }
//
//    /**
//     * @Description: 批量创建药品主数据信息
//     * @Param: [appId, appSecret, appPoiCode, medicineParams]
//     * @return: java.lang.String
//     * @Author: Qin.Qing
//     * @Date: 2019/04/03
//     */
//    public static String medicineInfobatchCreate(String appId, String appSecret, String appPoiCode, List<MedicineParam> medicineParams) {
//        String result;
//        try {
//            SystemParam sysPram = new SystemParam(appId, appSecret);
//            result= APIFactory.getMedicineAPI().medicineBatchSave(sysPram,appPoiCode,medicineParams);
//            logger.info("美团门店API：批量创建门店：" + appPoiCode + "商品数据，执行结果：" + result);
//        }catch (ApiOpException e){
//            logger.error("美团订单API：批量创建门店：" + appPoiCode + "商品数据，执行异常：" + e.getMsg());
//            result=e.getMsg();
//        }catch (ApiSysException e){
//            logger.error("美团订单API：批量创建门店：" + appPoiCode + "商品数据， 执行异常：" + e.getMessage());
//            result=e.getMessage();
//        }
//        return result;
//    }
//
//    /**
//     * 接口返回参数校验
//     *
//     * @param result
//     * @return
//     */
//    private static boolean validApiResult(String result){
//        if("ok".equals(result)){
//            return true;
//        }else{
//            return false;
//        }
//    }

    /**
     * 参数转换
     *
     * @param param
     * @return BaseOrderInfo
     */
    public static BaseOrderInfo convertToBaseOrder(Map<String, Object> param, String client) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        try {
            BeanUtils.populate(baseOrderInfo, param);
            Object[] detailArray = JSONObject.parseArray
                    (String.valueOf(param.get("detail"))).toArray();
            List<BaseOrderProInfo> proList = new ArrayList<BaseOrderProInfo>();
            Map<String, Object> proMap = null;
            BaseOrderProInfo baseOrderProInfo = null;
            // 内部订单ID
            String orderId = String.valueOf(param.get("order_id"));
            //order_tag_list	set<Integer>	是 订单信息，8代表处方药，23代表医保药
            String orderTagList = String.valueOf(param.get("order_tag_list"));
            if (orderTagList != null && orderTagList.contains("8")) {
                baseOrderInfo.setPrescriptionOrNot("1");
            } else {
                baseOrderInfo.setPrescriptionOrNot("0");
            }
            // 门店编码
            String storeCode = String.valueOf(param.get("app_poi_code"));
            //日期转换：美团传过来的是unix-timestamp时间，我们要转换为yyyy-mm-dd这种日期
            String create_time = TimeStamp2Date(String.valueOf(param.get("ctime")));
            baseOrderInfo.setCreate_time(create_time);
            String update_time = TimeStamp2Date(String.valueOf(param.get("utime")));
            baseOrderInfo.setUpdate_time(update_time);
            baseOrderInfo.setShop_no(storeCode);
            //  用户实际支付金额（扣除了所有优惠金额后）
            String total = String.valueOf(param.get("total"));
            baseOrderInfo.setCustomer_pay(total);
            // 是否开票（0：不需要发票1：需要发票）是否需要发票 1 是 2 否
            baseOrderInfo.setIs_invoiced(String.valueOf(param.get("has_invoiced")));
            //"2".equals(String.valueOf(param.get("has_invoiced")))?"0":"1");
            // 支付类型（0：在线支付1：货到付款）付款类型 1 下线 2 在线
            baseOrderInfo.setPay_type("2".equals(String.valueOf(param.get("pay_type"))) ? "0" : "1");
            baseOrderInfo.setStatus("1");
            //后去商品优惠信息
            String order_info = "";
            List<Map> extras = (List<Map>) JSONObject.parse(String.valueOf(param.get("extras")));
            if (extras != null) {
                List<String> doInsert = new ArrayList<>();
                for (Map m : extras) {
                    String itemRemark = String.valueOf(m.get("remark"));
                    if (!doInsert.contains(itemRemark)) {
                        order_info = order_info + itemRemark + "；";
                        doInsert.add(itemRemark);
                    }
                }
            }
            baseOrderInfo.setOrder_info(order_info);
            baseOrderInfo.setCaution(PtsApiUtil.filterOffUtf8Mb4(baseOrderInfo.getCaution()).trim());
            baseOrderInfo.setDay_seq(String.valueOf(param.get("day_seq")));
            //总应收欠款
            String total_price = String.format("%.2f", Double.valueOf(String.valueOf(((Map) JSONObject.parse(
                    String.valueOf(param.get("poi_receive_detail")))).get("wmPoiReceiveCent"))) / 100);
            baseOrderInfo.setTotal_price(total_price);
            String skuBenefitDetail = (String) param.get("sku_benefit_detail");
            if (skuBenefitDetail == null) {
                baseOrderInfo.setOrder_info("0.00");
            } else {
                baseOrderInfo.setOrder_info(getBenefit(skuBenefitDetail));
            }

            // 获取订单明细并记录在途库存信息
            Integer rowindex = 1;
            for (int i = 0; i < detailArray.length; i++) {
                proMap = (Map) JSON.parse(detailArray[i].toString());
                baseOrderProInfo = new BaseOrderProInfo();
                BeanUtils.populate(baseOrderProInfo, proMap);
                baseOrderProInfo.setOrder_id(orderId);
                //baseOrderProInfo.setMedicine_code(String.valueOf(proMap.get("app_food_code")));
                baseOrderProInfo.setMedicine_code(String.valueOf(proMap.get("sku_id")));
                Object foodName = proMap.get("food_name");
                baseOrderProInfo.setPlatformProductName(foodName == null ? null : String.valueOf(foodName));
                baseOrderProInfo.setDiscount(String.valueOf(proMap.get("food_discount")));
                baseOrderProInfo.setSequence(String.valueOf(rowindex++));
                baseOrderProInfo.setShop_no(storeCode);
                baseOrderProInfo.setStock_lock(baseOrderProInfo.getQuantity());
                proList.add(baseOrderProInfo);
            }
            //仁和的入机金额需要定制化
            if ("10000368".equals(client)) {
                Object[] extrasArray = JSONObject.parseArray
                        (String.valueOf(param.get("extras"))).toArray();
                Map<String, Object> tempMap = null;
                //商家活动支出
                BigDecimal reduceFeeTotal = BigDecimal.ZERO;
                for (int i = 0; i < extrasArray.length; i++) {
                    tempMap = (Map) JSON.parse(extrasArray[i].toString());
                    String reduceFee = String.valueOf(tempMap.get("reduce_fee"));
                    reduceFeeTotal = reduceFeeTotal.add(new BigDecimal(reduceFee));
                }
                BigDecimal totalIn = BigDecimal.ZERO;
                for (int i = 0; i < detailArray.length; i++) {
                    tempMap = (Map) JSON.parse(detailArray[i].toString());
                    String originalPrice = String.valueOf(tempMap.get("original_price"));
                    String quantity = String.valueOf(tempMap.get("quantity"));
                    totalIn = totalIn.add(new BigDecimal(originalPrice).multiply(new BigDecimal(quantity)));
                }
                totalIn = totalIn.subtract(reduceFeeTotal);
                //配送方式
                String logisticsCode = String.valueOf(param.get("logistics_code"));
                if ("0000".equals(logisticsCode)) {
                    String shippingFee = String.valueOf(param.get("shipping_fee"));
                    totalIn = totalIn.add(new BigDecimal(shippingFee));
                }
                baseOrderInfo.setTotal_price(totalIn.toString());
            }
            // 添加订单商品明细数据
            baseOrderInfo.setItems(proList);
            baseOrderInfo.setResult("ok");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            baseOrderInfo.setResult("ERROR" + e.getMessage());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            baseOrderInfo.setResult("ERROR" + e.getMessage());
        }
        return baseOrderInfo;
    }

    /**
     * 获取优惠总额
     * 比如满减优惠，它会将优惠额分摊到每个商品
     */
    private static String getBenefit(String benefitDetail) {
        JSONArray jsonArray = JSONArray.parseArray(benefitDetail);
        BigDecimal totalPrice = new BigDecimal(0.00);
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            BigDecimal totalReducePrice = jsonObject.getBigDecimal("totalReducePrice");
            totalPrice = totalPrice.add(totalReducePrice);
        }
        totalPrice = totalPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
        return totalPrice.toString();
    }

    /**
     * 参数转换。仅支持部分退款
     *
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2018/12/26
     */
    public static BaseOrderInfo convertToBaseOrderForRefund(Map<String, Object> param) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        try {
            BeanUtils.populate(baseOrderInfo, param);
            Object[] detailArray = JSONObject.parseArray
                    (String.valueOf(param.get("food"))).toArray();
            List<BaseOrderProInfo> proList = new ArrayList<BaseOrderProInfo>();
            Map<String, Object> proMap = null;
            BaseOrderProInfo baseOrderProInfo = null;
            String money = String.valueOf(param.get("money"));
            baseOrderInfo.setRefund_money(money);//暂时移除测试，金额变更，直接变更total_price字段。采用订单金额变更接口更新。该方案，暂时只适用于美团。
            //baseOrderInfo.setRefund_money("0");
            String reason = String.valueOf(param.get("reason"));
            baseOrderInfo.setReason(reason);
            for (int i = 0; i < detailArray.length; i++) {
                proMap = (Map) JSON.parse(detailArray[i].toString());
                baseOrderProInfo = new BaseOrderProInfo();
                BeanUtils.populate(baseOrderProInfo, proMap);
                baseOrderProInfo.setMedicine_code(String.valueOf(proMap.get("app_food_code")));
                baseOrderProInfo.setRefund_qty(String.valueOf(proMap.get("count")));
                baseOrderProInfo.setRefund_price(String.valueOf(proMap.get("refund_price")));
                baseOrderProInfo.setFoodPrice(String.valueOf(proMap.get("food_price")));
                proList.add(baseOrderProInfo);
            }
            // 添加订单商品明细数据
            baseOrderInfo.setItems(proList);
            baseOrderInfo.setResult("ok");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            baseOrderInfo.setResult("ERROR" + e.getMessage());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            baseOrderInfo.setResult("ERROR" + e.getMessage());
        }
        return baseOrderInfo;
    }

    /**
     * Convert Unix timestamp to normal date style
     *
     * @param timestampString
     * @return
     * @author：Li.Deman
     * @date：2018/12/6
     */
    public static String TimeStamp2Date(String timestampString) {
        Long timestamp = Long.parseLong(timestampString) * 1000;
        String date = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(timestamp));
        return date;
    }

}
