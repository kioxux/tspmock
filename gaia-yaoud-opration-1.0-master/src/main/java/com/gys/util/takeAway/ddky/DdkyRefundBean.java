package com.gys.util.takeAway.ddky;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/10/15 14:07
 */
@Data
public class DdkyRefundBean {

    private String orderID;//订单id
    private Long isRefundFreight;//是否退运费: 0不退 1 退；拒收单入库，默认传1即可
    private String createdBy;//创建人

}
