package com.gys.util.takeAway.ky;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.gys.common.config.takeAway.KyConfig;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.HttpUtils;
import com.gys.util.takeAway.UuidUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 快药外卖-接口封装类
 *
 * @author zhangdong
 * @date 2021/5/7 14:16
 */
public class KyApiUtil {
    private static Logger logger = LoggerFactory.getLogger(KyApiUtil.class);

    /**
     * 修改单个商品库存
     */
    public static String stockUpdate(String appId, String secret, String shopNo, String skuId, String stockNum) {
        Map<String, String> paramMap = new HashMap<>();
        String timeStamp = getTimeStamp();
        paramMap.put("timestamp", timeStamp);
        paramMap.put("app_id", appId);
        String sign = SignUtil.getKySign(appId, secret, timeStamp, KyConfig.KY_API_VERSION);
        paramMap.put("signature", sign);
        paramMap.put("version", KyConfig.KY_API_VERSION);
        paramMap.put("shop_no", shopNo);
        paramMap.put("merchant_sku", skuId);
        paramMap.put("stock", stockNum);
        String response = HttpUtils.doPost(KyConfig.KY_API_SERVER, KyConfig.KY_API_VERSION + KyConfig.KY_STOCK_UPDATE, paramMap);
        logger.info("快药库存更新, 门店id:{}, 商品id:{}, 库存:{}, 执行结果:{}", shopNo, skuId, stockNum, response);
        return response;
    }

//    public static void main(String[] args) {
//        String result = "";
//        List<String> kyResult = Lists.newArrayList();
//        String response = stockUpdate("205832875460190941", "c11761966b91b62570e99b3a745dd2e4b2a3bf9bf13906e9fc14c4af2272517a",
//                "cre6yl39", "6030044", "2");
//        Map revMap = (Map) JSON.parse(response);
//        boolean success = "0".equals(String.valueOf(revMap.get("code"))) && "200".equals(String.valueOf(revMap.get("status")));
//        if (!success) {
//            kyResult.add(String.valueOf(revMap.get("message")));
//        }
//        String response1 = stockUpdate("205832875460190941", "c11761966b91b62570e99b3a745dd2e4b2a3bf9bf13906e9fc14c4af2272517a",
//                "cre6yl39", "6030044", "2");
//        Map revMap1 = (Map) JSON.parse(response1);
//        boolean success1 = "0".equals(String.valueOf(revMap1.get("code"))) && "200".equals(String.valueOf(revMap1.get("status")));
//        if (!success1) {
//            kyResult.add(String.valueOf(revMap1.get("message")));
//        }
//        if (CollUtil.isNotEmpty(kyResult)) {
//            kyResult = kyResult.stream().distinct().collect(Collectors.toList());
//            result += Joiner.on(";").join(kyResult);
//        }
//        System.out.println(result);
//    }

    private static String getTimeStamp() {
        long currentMis = System.currentTimeMillis() / 1000;
        return String.valueOf(currentMis);
    }

    public static BaseOrderInfo convertOrder(Map<String, Object> paramMap, String client, String stoCode) {
        Map<String, Object> paramBody = (Map<String, Object>) JSONObject.parse(StringEscapeUtils.unescapeJava((String) paramMap.get("body")));
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        String orderId = UuidUtil.getUUID("ky-");
        baseOrderInfo.setNotice_message("new");
        baseOrderInfo.setClient(client);
        baseOrderInfo.setStoCode(stoCode);
        baseOrderInfo.setPlatforms(Constants.PLATFORMS_KY);
        baseOrderInfo.setOrder_id(orderId);
        baseOrderInfo.setPlatforms_order_id(String.valueOf(paramBody.get("order_no")));
        baseOrderInfo.setShop_no(String.valueOf(paramBody.get("shop_no")));
        baseOrderInfo.setRecipient_address("湖东街29号");//todo 收件人地址 无
        baseOrderInfo.setRecipient_name("张***");//todo 收件人姓名 无
        baseOrderInfo.setRecipient_phone("18962614636");//todo 收件人电话 无
        baseOrderInfo.setShipping_fee(String.valueOf(paramBody.get("shipping_fee")));
        baseOrderInfo.setTotal_price(String.valueOf(paramBody.get("product_fee")));
        baseOrderInfo.setOriginal_price("0.1");//todo 原价 无
        baseOrderInfo.setCustomer_pay("0.1");//todo 用户实付金额 无
        baseOrderInfo.setCaution("啦啦啦啦");//todo 备注 无
        baseOrderInfo.setStatus("1");//todo 订单状态 无     订单完成、用户催单何时推送
        baseOrderInfo.setIs_invoiced("0");//todo 是否开票 无
        baseOrderInfo.setDelivery_time("0");//todo 延时配送 无
        baseOrderInfo.setPay_type("0");//todo 支付类型 无
        Object discountDetail = paramMap.get("discount_detail");
        BigDecimal discount = new BigDecimal(0);
        if (discountDetail != null) {
            List<Map<String, Object>> discountDetailList = (List<Map<String, Object>>) JSONObject.parse(StringEscapeUtils.unescapeJava((String) discountDetail));
            if (CollUtil.isNotEmpty(discountDetailList)) {
                for (Map<String, Object> map : discountDetailList) {
                    BigDecimal fee = BigDecimal.valueOf(Double.valueOf(String.valueOf(map.get("fee"))));
                    discount.add(fee);
                }
            }
        }
        discount = discount.setScale(2, RoundingMode.HALF_DOWN);
        //优惠金额
        baseOrderInfo.setOrder_info(discount.toString());
        baseOrderInfo.setCreate_time("20210510");//todo 创建时间 无
        baseOrderInfo.setDay_seq("1");//todo 外卖员取货码 无

        List<BaseOrderProInfo> items = Lists.newArrayList();
        Object products = paramBody.get("products");
        if (products != null) {
            JSONArray jsonArray = (JSONArray) products;
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject)jsonArray.get(i);
                String sku = String.valueOf(jsonObject.get("sku"));
                String price = String.valueOf(jsonObject.get("price"));
                String number = String.valueOf(jsonObject.get("number"));
                BaseOrderProInfo proInfo = new BaseOrderProInfo();
                proInfo.setClient(client);
                proInfo.setStoCode(stoCode);
                proInfo.setOrder_id(orderId);
                proInfo.setMedicine_code(sku);
                proInfo.setQuantity(number);
                proInfo.setSequence(String.valueOf(i + 1));
                proInfo.setBox_num("0");//todo 包装数量 无
                proInfo.setBox_price("0");//todo 包装价格 无
                proInfo.setUnit("");
                proInfo.setDiscount("1");
                proInfo.setStock_lock(proInfo.getQuantity());
                proInfo.setPrice(price);
                items.add(proInfo);
            }
        }
        baseOrderInfo.setItems(items);
        return baseOrderInfo;
    }
}
