package com.gys.util.takeAway;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class HttpUtils {

    private static final int socketTimeOut = 50000;
    private static final int connectTimeOut = 30000;
    private static final int connectRequestTimeOut = 30000;
    private static final int maxTotal = 200;
    private static final int defaultMaxPerRoute = 200;
    private static final int maxPerRoute = 200;
    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    public static String doGet(String host, String path, Map<String, String> param) throws Exception {
        String result = "";
        CloseableHttpClient httpClient = wrapClient(host, path);
        StringBuilder sbUrl = new StringBuilder();
        if (!StringUtils.isBlank(host)) {
            sbUrl.append(host);
        }
        if (!StringUtils.isBlank(path)) {
            sbUrl.append(path);
        }
        StringBuilder sbQuery = new StringBuilder();
        for (Map.Entry<String, String> query : param.entrySet()) {
            if (0 < sbQuery.length()) {
                sbQuery.append("&");
            }
            if (StringUtils.isBlank(query.getKey()) && !StringUtils.isBlank(query.getValue())) {
                sbQuery.append(query.getValue());
            }
            if (!StringUtils.isBlank(query.getKey())) {
                sbQuery.append(query.getKey());
                if (!StringUtils.isBlank(query.getValue())) {
                    sbQuery.append("=");
                    sbQuery.append(URLEncoder.encode(query.getValue(), "utf-8"));
                }
            }
        }
        if (0 < sbQuery.length()) {
            sbUrl.append("?").append(sbQuery);
        }
        HttpGet request = new HttpGet(sbUrl.toString());
        request.setConfig(setTimeOutConfig(request.getConfig()));
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
            result = "ERROR:" + ex.getMessage();
        } finally {
            try {
                request.releaseConnection();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                result = "ERROR:" + ex.getMessage();
            }
        }
        return result;
    }

    public static String doPost(String host, String path, Map<String, String> param) {
        String result = "";
        CloseableHttpClient httpClient = wrapClient(host, path);
        StringBuilder sbUrl = new StringBuilder();
        if (!StringUtils.isBlank(host)) {
            sbUrl.append(host);
        }
        if (!StringUtils.isBlank(path)) {
            sbUrl.append(path);
        }
        HttpPost request = new HttpPost(sbUrl.toString());
        request.setConfig(setTimeOutConfig(request.getConfig()));
        CloseableHttpResponse response = null;
        try {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            for (String key : param.keySet()) {
                nameValuePairList.add(new BasicNameValuePair(key, param.get(key)));
            }
            UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(nameValuePairList, "UTF-8");
            uefEntity.setContentType("application/x-www-form-urlencoded; charset=UTF-8");
            request.setEntity(uefEntity);
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
            result = "ERROR:" + ex.getMessage();
        } finally {
            try {
                request.releaseConnection();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                result = "ERROR:" + ex.getMessage();
            }
        }
        return result;
    }

    public static String doPost(String host, String path,String param){
        String result = "";
        CloseableHttpClient httpClient = wrapClient(host, path);
        StringBuilder sbUrl = new StringBuilder();
        if (!StringUtils.isBlank(host)) {
            sbUrl.append(host);
        }
        if (!StringUtils.isBlank(path)) {
            sbUrl.append(path);
        }
        HttpPost request = new HttpPost(sbUrl.toString());
        request.setConfig(setTimeOutConfig(request.getConfig()));
        request.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        CloseableHttpResponse response = null;
        try {
//            HttpEntity httpEntity= EntityBuilder.create()
//                    .setText(param)
//                    .setContentEncoding("UTF-8")
//                    .setContentType(ContentType.APPLICATION_FORM_URLENCODED)
//                    .build();
//            request.setEntity(httpEntity);
            StringEntity se = new StringEntity( param,"UTF-8");
            se.setContentType("application/x-www-form-urlencoded; charset=UTF-8");

            request.setEntity(se);
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
            result = "ERROR:" + ex.getMessage();
        } finally {
            logger.info("Post Message to:" + sbUrl.toString() + "\n" + param + "\n Result:" + result);
            try {
                request.releaseConnection();
                if (response != null) {
                    response.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                result = "ERROR:" + ex.getMessage();
            }
        }
        return result;
    }

    private static CloseableHttpClient wrapClient(String host, String path) {
        if (host != null && host.startsWith("https://")) {
            return sslClient();
        } else if (StringUtils.isBlank(host) && path != null && path.startsWith("https://")) {
            return sslClient();
        } else {
            return HttpClientBuilder.create().build();
        }
    }

    private static CloseableHttpClient sslClient() {
        try {
            // 在调用SSL之前需要重写验证方法，取消检测SSL
            X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String str) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String str) {
                }
            };
            SSLContext ctx = SSLContext.getInstance(SSLConnectionSocketFactory.TLS);
            ctx.init(null, new TrustManager[]{trustManager}, null);
            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(ctx, NoopHostnameVerifier.INSTANCE);
            // 创建Registry
            RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT)
                    .setExpectContinueEnabled(Boolean.TRUE).setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
                    .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC)).build();
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE)
                    .register("https", socketFactory).build();
            // 创建ConnectionManager，添加Connection配置信息
            PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            connectionManager.setMaxTotal(maxTotal);
            connectionManager.setDefaultMaxPerRoute(defaultMaxPerRoute);
            CloseableHttpClient closeableHttpClient = HttpClients.custom().setConnectionManager(connectionManager)
                    .setDefaultRequestConfig(requestConfig).build();
            return closeableHttpClient;
        } catch (KeyManagementException ex) {
            throw new RuntimeException(ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static RequestConfig setTimeOutConfig(RequestConfig requestConfig) {
        if (requestConfig == null) {
            return RequestConfig.custom()
                    .setConnectionRequestTimeout(connectRequestTimeOut)
                    .setConnectTimeout(connectTimeOut)
                    .setSocketTimeout(socketTimeOut)
                    .build();
        } else {
            return RequestConfig.copy(requestConfig)
                    .setConnectionRequestTimeout(connectRequestTimeOut)
                    .setConnectTimeout(connectTimeOut)
                    .setSocketTimeout(socketTimeOut)
                    .build();
        }
    }

    public static JSONObject getJson(HttpResponse httpResponse) throws IOException {
        HttpEntity entity = httpResponse.getEntity();
        String resp = EntityUtils.toString(entity, "UTF-8");
        EntityUtils.consume(entity);
        return JSON.parseObject(resp);
    }

}
