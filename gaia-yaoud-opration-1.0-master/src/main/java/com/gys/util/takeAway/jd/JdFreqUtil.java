package com.gys.util.takeAway.jd;

import com.gys.common.config.takeAway.JdConfig;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JdFreqUtil {
    private static Map<String, Timestamp> accessTimeMap=new HashMap<>();
    private static Map<String,Double> accessFreq=new HashMap<>();
    public static boolean checkFreq(String cmd){
        boolean result=false;
        try {
            if (accessTimeMap.get(cmd)!=null){
                if(accessFreq.get(cmd)!=null) {
                    while (true) {
                        Timestamp currentTimestamp = new Timestamp((new Date()).getTime());
                        if ((currentTimestamp.getTime() - accessTimeMap.get(cmd).getTime()) >= ((int) (1000.0 / accessFreq.get(cmd)) + 100)) {
                            accessTimeMap.put(cmd,new Timestamp((new Date()).getTime()));
                            break;
                        } else {
                            Thread.sleep(50);
                        }
                    }
                }
            }else{
                accessTimeMap.put(cmd,new Timestamp((new Date()).getTime()));
            }
            result=true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    static {
        accessFreq.put(JdConfig.JD_CMD_STOCK_UPDATE,300.0/60);
        accessFreq.put(JdConfig.JD_CMD_SHOP_INFO,300.0/60);
        accessFreq.put(JdConfig.JD_CMD_SKU_LIST,200.0/60);
        accessFreq.put(JdConfig.JD_CMD_SKU_SHOP_LIST,300.0/60);
        accessFreq.put(JdConfig.JD_CMD_SHOP_ConfigOpen,300.0/60);
        accessFreq.put(JdConfig.JD_CMD_SKU_PRICE,300.0/60);
        accessFreq.put(JdConfig.JD_CMD_CATEGORY_LIST,200.0/60);
        accessFreq.put(JdConfig.JD_CMD_CATEGORY_ADD,200.0/60);
        accessFreq.put(JdConfig.JD_CMD_SKU_CREATE,5.0/60);
    }
}
