package com.gys.util.takeAway.yl;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/7 16:19
 */
@Data
public class YlOrderDetailBean {

    private String drug_id;// | String | 001321 | 商品id，和连锁商品保持一致 | 是|
    private String common_name;// | String | 阿奇霉素 | 商品通用名 | 是|
    private String amount;// | String | 2 | 数量 | 是|
    private String price;// | String | 32.2 | 商品原价 | 是|
    private String settlediscountrate;// | String | 88 | 结算扣率| 根据业务 |
    private String code;// | String | 6920356200227 | 商品条形码 | 是|
    private String activity_type;// | String | 1 | 1:使用优惠价购买0:未使用到优惠价 | 根据业务 |

    private String number;//批准文号（非药品类可以为空）
    private String form;//规格
    private String pack;//包装
    private String unit_price;//门店零售单价
    private String batch_number;//批次号  --陈周：传批号
    private String expirydate;//商品有效期

}
