package com.gys.util.takeAway;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class UuidUtil {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    /**
     * 外卖生成订单号
     * @param prefix 前缀 eg: eb-
     * @return
     */
    public static String getUUID(String prefix) {
        return prefix + dateTimeFormatter.format(LocalDateTime.now()) + String.format("%05d", ThreadLocalRandom.current().nextInt(0, 99999));
//        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddwhhmmssSSS");
//        return sign + sf.format(new Date());
    }

    public static String getUUID() {
        return UUID.randomUUID().toString().toUpperCase();
    }

}
