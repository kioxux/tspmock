package com.gys.util.takeAway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtil {
    private static final Logger logger = LoggerFactory.getLogger(PropertyUtil.class);

    private Properties property = new Properties();

    private final static String resources_root = "src/main/resources/";

    public static PropertyUtil getInstance(String fileName) {
        PropertyUtil temp = new PropertyUtil();
        if (!temp.load(new File(resources_root + fileName)))
            return null;
        return temp;
    }

    private boolean load(File file) {
        if (file == null)
            return false;

        if (!file.exists())
            return false;

        FileInputStream fin;
        try {
            fin = new FileInputStream(file);
            property.load(fin);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static String getProperty(String key) {
        InputStream is = null;
        final Properties properties = new Properties();
        final Resource resource = new ClassPathResource("config.properties");
        try {
            is = resource.getInputStream();
            properties.load(is);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        if (key == null)
            return null;

        return properties.getProperty(key);
    }
}
