package com.gys.util.takeAway.mt;

import com.sankuai.meituan.shangou.open.sdk.exception.SgOpenException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 美团签名验证
 *
 * @author：Li.Deman
 * @date：2019/1/23
 */
public class SignUtil {
    private static Logger logger = Logger.getLogger(SignUtil.class);
    //public final static String SELF_HOST_SERVER = PropertyUtil.getProperty("self_host_server");
//    public final static String SELF_HOST_SERVER = "http://je38890793.qicp.vip:80/";
    public final static String SELF_HOST_SERVER = "https://app.yaoud.com/operation/";

    /**
     * 校验签名是否正确
     *
     * @param data
     * @param methodName
     * @param secret
     * @return
     */
    public static boolean checkSign(Map<String, Object> data, String methodName, String secret) {
        Map<String, String> systemParamsMap = new HashMap<>();
        Map<String, String> applicationParamsMap = new HashMap<>();
        systemParamsMap.put("appSecret", secret);
        for (String key : data.keySet()) {
            if (key.equals("sig")) {
                continue;
            }
            applicationParamsMap.put(key, (String) data.get(key));
        }
        String signFrom = "";
        String urlForGenSig = genUrlForGenSig(methodName, systemParamsMap, applicationParamsMap);
        //urlForGenSig=urlForGenSig.replaceAll("\\+"," ");
        try {
            signFrom = genSig(urlForGenSig).toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
            signFrom = String.valueOf(data.get("sig")).toUpperCase();
        }
        String sign = String.valueOf(data.get("sig")).toUpperCase();
        if (signFrom.equals(sign)) {
            return true;
        } else {
            return false;
        }
    }

    public static String genSig(String baseUrl) throws IOException, SgOpenException {
        String str = null;
        MessageDigest md = null;

        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException var4) {
            throw new SgOpenException(var4);
        }

        str = byte2hex(md.digest(baseUrl.getBytes("utf-8")));
        return str;
    }

    private static String byte2hex(byte[] b) {
        StringBuffer buf = new StringBuffer();

        for(int offset = 0; offset < b.length; ++offset) {
            int i = b[offset];
            if (i < 0) {
                i += 256;
            }

            if (i < 16) {
                buf.append("0");
            }

            buf.append(Integer.toHexString(i));
        }

        return buf.toString();
    }

    public static String genUrlForGenSig(String methodName, Map<String, String> systemParamsMap, Map<String, String> applicationParamsMap) {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.putAll(systemParamsMap);
        if (applicationParamsMap != null) {
            paramMap.putAll(applicationParamsMap);
        }

        String str = concatParams(paramMap);
        String basedUrl = genUrlPrefix(methodName) + "?" + str + (String) systemParamsMap.get("appSecret");
        return basedUrl;
    }

    private static String concatParams(Map<String, String> params2) {
        Object[] key_arr = params2.keySet().toArray();
        Arrays.sort(key_arr);
        String str = "";
        Object[] var3 = key_arr;
        int var4 = key_arr.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            Object key = var3[var5];
            if (!key.equals("appSecret")) {
                String val = (String) params2.get(key);
                str = str + "&" + key + "=" + val;
            }
        }

        return str.replaceFirst("&", "");
    }

    private static String genUrlPrefix(String methodName) {
        return SELF_HOST_SERVER + "mt/push/" + methodName;
    }

}
