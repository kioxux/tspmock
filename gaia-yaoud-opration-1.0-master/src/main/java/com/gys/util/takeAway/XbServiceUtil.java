package com.gys.util.takeAway;

import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.BaseServicePTS;
import com.gys.business.service.takeAway.BaseServiceWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class XbServiceUtil {
    private BaseService baseService;
    private BaseServiceWeb baseServiceWeb;
    private BaseServicePTS baseServicePTS;
    public static BaseService service;
    public static BaseServiceWeb serviceWeb;
    public static BaseServicePTS servicePTS;
    private static XbServiceUtil xbServiceUtil;

    @Autowired
    public void setBaseService(BaseService baseService) {
        this.baseService = baseService;
    }

    @Autowired
    public void setBaseServiceWeb(BaseServiceWeb baseServiceWeb) {
        this.baseServiceWeb = baseServiceWeb;
    }

    @Autowired
    public void setBaseServicePTS(BaseServicePTS baseServicePTS) {
        this.baseServicePTS = baseServicePTS;
    }

    @PostConstruct
    public void init() {
        xbServiceUtil=this;
        xbServiceUtil.baseService=this.baseService;
        xbServiceUtil.baseServiceWeb=this.baseServiceWeb;
        xbServiceUtil.baseServicePTS=this.baseServicePTS;
        service=this.baseService;
        serviceWeb=this.baseServiceWeb;
        servicePTS=this.baseServicePTS;
    }
}
