package com.gys.util.takeAway.yl;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/7 16:12
 */
@Data
public class YlOrderBean {

    //                   | 名称 | 类型 | 示例值 | 描述 | 是否必须 |
    private String id;// | String | 2019712601021479499848 | 交易流水号(即药联订单号) | 是 |
    private String create_time;// | String | 2019-07-12 21:21:21 | 药联下单时间 | 是|
    private String store_id;// | String | 223 | 门店内码 | 是|
    private String member_id;// | String | 234244 | 会员ID（需要配合会员数据录入接口实现） | 根据业务|
    private String member_name;// | String | 王 | 会员姓名（需要开通新零售业务） | 根据业务|
    private String member_mobile;// | String | 18510000406 | 会员手机号（需要开通新零售业务） | 根据业务|
    private String assistant_mobile;// | String | 13583271199 | 店员手机号 | 是|
    private String assistant_number;// | String | 4756 | 店员工号 | 是|
    private String total_price;// | String | 64.4 | 订单总价 | 是 |
    private String free_price;// | String | 60 | 药联直付金额 | 是 |
    private String sale_price;// | String | 4.4 | 顾客自付金额 | 是|
    private String isSuper;// | String | 1 | 超级会员日订单标示 | 根据业务 |
    private String is_dtp;// | String | 1 | 是否为dtp订单(1:是 0:否) | 是 |
    private String is_prescription;// | String | 1 | 订单是否有处方单标示，1是存在处方单，0是没有 | 是 |

    private String update_time;
    private String card_number;
    private String ud_card;
    private String order_no;

    private List<YlOrderDetailBean> retailDetail;

}
