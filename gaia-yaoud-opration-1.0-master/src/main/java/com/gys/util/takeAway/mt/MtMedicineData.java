package com.gys.util.takeAway.mt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/6/25 16:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MtMedicineData {

    private String app_medicine_code;//商品编码
    private String app_poi_code;//门店编码
    private String stock;//库存
}
