package com.gys.util.takeAway.pts;

public class PtsMedicineInfo {
    public String getMedicine_code() {
        return medicine_code;
    }
    public void setMedicine_code(String medicine_code) {
        this.medicine_code = medicine_code;
    }

    public String getSequence() {
        return sequence;
    }
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getQuantity() {
        return quantity;
    }
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBox_num() {
        return box_num;
    }
    public void setBox_num(String box_num) {
        this.box_num = box_num;
    }

    public String getBox_price() {
        return box_price;
    }
    public void setBox_price(String box_price) {
        this.box_price = box_price;
    }

    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDiscount() {
        return discount;
    }
    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getRefund_qty() {
        return refund_qty;
    }
    public void setRefund_qty(String refund_qty) {
        this.refund_qty = refund_qty;
    }

    public String getRefund_price() {
        return refund_price;
    }
    public void setRefund_price(String refund_price) {
        this.refund_price = refund_price;
    }

    public String getOrder_id() {
        return order_id;
    }
    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    private String medicine_code;
    private String sequence;
    private String quantity;
    private String box_num;
    private String box_price;
    private String unit;
    private String discount;
    private String refund_qty;
    private String refund_price;
    private String order_id;
    private String price;
}
