package com.gys.util.takeAway.ddky;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/10/14 14:18
 */
@Data
public class DdkyStockUpdateDetailBean {

    private Long goodsId;//线下门店系统货品ID
    private Long invQty;//商品可销库存
    private String price;//商品价格
}
