package com.gys.util.takeAway.ddky;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/10/14 14:20
 */
@Data
public class DdkyResponse {

    /**
     * success result eg:
     * {
     * "msg":"success",
     * "code":"0"
     * }
     * error result eg:
     * {
     * "code":"90004",
     * "msg":"无商品映射信息
     * }
     */
    private String msg;
    private String code;
}
