package com.gys.util.takeAway.pts;

import java.util.ArrayList;

public class PtsOrderInfo {
    public String getPlatforms() {
        return platforms;
    }
    public void setPlatforms(String platforms) {
        this.platforms = platforms;
    }

    public String getOrder_id() {
        return order_id;
    }
    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPlatforms_order_id() {
        return platforms_order_id;
    }
    public void setPlatforms_order_id(String platforms_order_id) {
        this.platforms_order_id = platforms_order_id;
    }

    public String getShop_no() {
        return shop_no;
    }
    public void setShop_no(String shop_no) {
        this.shop_no = shop_no;
    }

    public String getRecipient_address() {
        return recipient_address;
    }
    public void setRecipient_address(String recipient_address) {
        this.recipient_address = recipient_address;
    }

    public String getRecipient_name() {
        return recipient_name;
    }
    public void setRecipient_name(String recipient_name) {
        this.recipient_name = recipient_name;
    }

    public String getRecipient_phone() {
        return recipient_phone;
    }
    public void setRecipient_phone(String recipient_phone) {
        this.recipient_phone = recipient_phone;
    }

    public String getShipping_fee() {
        return shipping_fee;
    }
    public void setShipping_fee(String shipping_fee) {
        this.shipping_fee = shipping_fee;
    }

    public String getTotal_price() {
        return total_price;
    }
    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getOriginal_price() {
        return original_price;
    }
    public void setOriginal_price(String original_price) {
        this.original_price = original_price;
    }

    public String getCustomer_pay() {
        return customer_pay;
    }
    public void setCustomer_pay(String customer_pay) {
        this.customer_pay = customer_pay;
    }

    public String getCaution() {
        return caution;
    }
    public void setCaution(String caution) {
        this.caution = caution;
    }

    public String getShipper_name() {
        return shipper_name;
    }
    public void setShipper_name(String shipper_name) {
        this.shipper_name = shipper_name;
    }

    public String getShipper_phone() {
        return shipper_phone;
    }
    public void setShipper_phone(String shipper_phone) {
        this.shipper_phone = shipper_phone;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_invoiced() {
        return is_invoiced;
    }
    public void setIs_invoiced(String is_invoiced) {
        this.is_invoiced = is_invoiced;
    }

    public String getInvoice_title() {
        return invoice_title;
    }
    public void setInvoice_title(String invoice_title) {
        this.invoice_title = invoice_title;
    }

    public String getTaxpayer_id() {
        return taxpayer_id;
    }
    public void setTaxpayer_id(String taxpayer_id) {
        this.taxpayer_id = taxpayer_id;
    }

    public String getDelivery_time() {
        return delivery_time;
    }
    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getPay_type() {
        return pay_type;
    }
    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getOrder_info() {
        return order_info;
    }
    public void setOrder_info(String order_info) {
        this.order_info = order_info;
    }

    public String getCreate_time() {
        return create_time;
    }
    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }
    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public ArrayList<PtsMedicineInfo> getItems() {
        return items;
    }
    public void setItems(ArrayList<PtsMedicineInfo> items) {
        this.items = items;
    }

    public String getDay_seq() {
        return day_seq;
    }
    public void setDay_seq(String day_seq) {
        this.day_seq = day_seq;
    }

    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

    public String getRefund_money() {
        return refund_money;
    }
    public void setRefund_money(String refund_money) {
        this.refund_money = refund_money;
    }

    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason_code() {
        return reason_code;
    }
    public void setReason_code(String reason) {
        this.reason_code = reason_code;
    }


    public String getNotice_message() {
        return notice_message;
    }

    public void setNotice_message(String notice_message) {
        this.notice_message = notice_message;
    }

    private String platforms;
    private String order_id;
    private String platforms_order_id;
    private String shop_no;
    private String recipient_address;
    private String recipient_name;
    private String recipient_phone;
    private String shipping_fee;
    private String total_price;
    private String original_price;
    private String customer_pay;
    private String caution;
    private String shipper_name;
    private String shipper_phone;
    private String status;
    private String action;
    private String is_invoiced;
    private String invoice_title;
    private String taxpayer_id;
    private String delivery_time;
    private String pay_type;
    private String order_info;
    private String create_time;
    private String update_time;
    private ArrayList<PtsMedicineInfo> items;
    private String day_seq;
    public PtsOrderInfo(){
        items=new ArrayList<>();
    }
    private String refund_money;
    private String reason;
    private String reason_code;
    private String notice_message;
}
