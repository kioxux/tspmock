package com.gys.util.takeAway.yl;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/8 10:21
 */
@Data
public class YlReturnOrderBean {

    private List<YlReturnOrderDetailBean> refund;
}
