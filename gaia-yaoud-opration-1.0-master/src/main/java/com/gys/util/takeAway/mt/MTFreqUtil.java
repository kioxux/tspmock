package com.gys.util.takeAway.mt;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MTFreqUtil {
    private static Map<String, Timestamp> accessTimeMap=new HashMap<>();
    private static Map<String,Double> accessFreq=new HashMap<>();
    public static boolean checkFreq(String cmd){
        boolean result=false;
        try {
            if (accessTimeMap.get(cmd)!=null){
                if(accessFreq.get(cmd)!=null) {
                    while (true) {
                        Timestamp currentTimestamp = new Timestamp((new Date()).getTime());
                        if ((currentTimestamp.getTime() - accessTimeMap.get(cmd).getTime()) >= ((int) (1000.0 / accessFreq.get(cmd)) + 100)) {
                            accessTimeMap.put(cmd,new Timestamp((new Date()).getTime()));
                            break;
                        } else {
                            Thread.sleep(50);
                        }
                    }
                }
            }else{
                accessTimeMap.put(cmd,new Timestamp((new Date()).getTime()));
            }
            result=true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    static {
        accessFreq.put(MTConfig.MT_CMD_MEDICINE_STOCK,20.0);
        accessFreq.put(MTConfig.MT_CMD_MEDICINE_SAVE,50.0);
        accessFreq.put(MTConfig.MT_CMD_MEDICINE_UPDATE,50.0);
    }
}
