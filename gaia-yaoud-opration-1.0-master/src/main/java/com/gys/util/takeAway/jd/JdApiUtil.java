package com.gys.util.takeAway.jd;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.config.takeAway.JdConfig;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.HttpUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class JdApiUtil {
    private static Logger logger = LoggerFactory.getLogger(JdApiUtil.class);

    /**
     * 京东到家订单API：获取订单详情
     *
     * @param app_key    应用的app_key
     * @param app_secret
     * @param token
     * @param orderId
     * @return
     */
    public static BaseOrderInfo orderDetail(String app_key, String app_secret, String token, String orderId) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("orderId", orderId);
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_LIST, params);
            logger.debug("rev:" + rev);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                Map<String, Object> resultMap = (Map<String, Object>) JSON.parse(nullToEmpty(dataMap.get("result")));
                if (resultMap != null) {
                    List<Map<String, Object>> orderMapList = (List<Map<String, Object>>) JSON.parse(nullToEmpty(resultMap.get("resultList")));
                    baseOrderInfo = convertToBaseOrder(orderMapList.get(0));
                    logger.info("京东订单API：查询订单详情信息! 订单ID：" + orderId + " 执行结果：OK");
                } else {
                    logger.info("京东订单API：查询订单详情信息! 订单ID：" + orderId + " 执行异常：NG" + nullToEmpty(revObj.get("msg")));
                }
            } else {
                logger.info("京东订单API：查询订单详情信息! 订单ID：" + orderId + " 执行结果：NG" + nullToEmpty(revObj.get("msg")));
            }

        } catch (Exception e) {
            logger.error("京东订单API：查询订单详情信息! 订单ID：" + orderId + " 执行异常：ERROR" + e.getMessage());
        }

        return baseOrderInfo;
    }

    public static void main(String[] args) {
//        BaseOrderInfo info = orderDetail("025ffcc0534a4dd08a44cecde332120f", "a32628422c764bb1ac7f0c6db8a7bac1",
//                "df383432-1d40-45a5-a69c-f7045f0625c3", "2117534349000052");
//        System.out.println(JSON.toJSONString(info));

        //订单确认
//        orderConfirm("025ffcc0534a4dd08a44cecde332120f", "a32628422c764bb1ac7f0c6db8a7bac1",
//                "df383432-1d40-45a5-a69c-f7045f0625c3", "2117534349000052");

        //商品查询
//        List<Map<String, Object>> maps = getmedicineList("025ffcc0534a4dd08a44cecde332120f", "a32628422c764bb1ac7f0c6db8a7bac1",
//                "df383432-1d40-45a5-a69c-f7045f0625c3", "cre8oqq9");
//        System.out.println(JSON.toJSONString(maps));

        //商品库存更新
//        List<Map<String, Object>> medicineInfoList = new ArrayList<>();
//        Map<String, Object> medicineInfo = new HashMap<>();
//        medicineInfo.put("outSkuId", "11603023");
//        medicineInfo.put("stockQty", "6");//6
//        medicineInfoList.add(medicineInfo);
//        Map<String, Object> medicineInfo1 = new HashMap<>();
//        medicineInfo1.put("outSkuId", "40902022");
//        medicineInfo1.put("stockQty", "13");//13
//        medicineInfoList.add(medicineInfo1);
//        Map<String, Object> inputparam = new HashMap<>();
//        inputparam.put("outStationNo", "cre8oqqe");//cre8oqq9
//        inputparam.put("userPin", "service");
//        inputparam.put("skuStockList", medicineInfoList);
//        String rev = JdApiUtil.medicineStock("025ffcc0534a4dd08a44cecde332120f", "a32628422c764bb1ac7f0c6db8a7bac1",
//                "df383432-1d40-45a5-a69c-f7045f0625c3", "cre8oqqe", inputparam);
//        System.out.println(rev);

        //verifyUpdateToken("df383432-1d40-45a5-a69c-f7045f0625c3",
//                "df383432-1d40-45a5-a69c-f7045f0625c3", "025ffcc0534a4dd08a44cecde332120f", "a32628422c764bb1ac7f0c6db8a7bac1");

        //查询售后单详情
        orderAfs("025ffcc0534a4dd08a44cecde332120f", "a32628422c764bb1ac7f0c6db8a7bac1",
                "df383432-1d40-45a5-a69c-f7045f0625c3","2345556");

    }

    /**
     * 京东到家订单API：获取订单应结金额
     *
     * @param app_key    应用的app_key
     * @param app_secret
     * @param token
     * @param orderId
     * @return
     */
    public static BaseOrderInfo orderAmount(String app_key, String app_secret, String token, String orderId) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("orderId", orderId);
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            //{"code":"0","msg":"操作成功, UUID[beda43ba8bf9415fa27c356bd783b022]",
            // "data":"{\"msg\":\"成功\",\"result\":{\"platDeliveryFreight\":null,\"orderCashOnDeliveryMoney\":null,\"venderOrderGoodsDiscountMoney\":null,\"orderId\":2117965474000132,\"packageMoney\":null,\"venderDeliveryFreight\":null,\"goodsCommission\":173,\"distanceFreightMoney\":null,\"venderSkuGoodsDiscountMoney\":null,\"packageCommission\":null,\"userActualPaidGoodsMoney\":2270,\"orgCode\":\"329648\",\"packageFeeCommission\":null,\"allAfterSalePackageMoney\":null,\"stationNo\":\"11851771\",\"venderFreightDiscountMoney\":null,\"platOrderGoodsDiscountMoney\":200,\"venderPaidTips\":null,\"billTime\":1627551549694,\"guaranteedCommission\":227,\"allAfterSaleFreight\":null,\"orderGiftCardMoney\":null,\"platSkuGoodsDiscountMoney\":null,\"settlementAmount\":2070,\"platFreightDiscountMoney\":null,\"freightCommission\":null},\"code\":\"0\",\"success\":true,\"detail\":null}","success":true}
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_ACCOUNT, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                Map<String, Object> resultMap = (Map<String, Object>) JSON.parse(nullToEmpty(dataMap.get("result")));
                if (nullToEmpty(dataMap.get("code")).equals("0") && resultMap != null) {
                    baseOrderInfo.setResult("ok");
                    baseOrderInfo.setTotal_price(centToDollarForString(resultMap.get("settlementAmount")));
                    logger.info("京东订单API：获取订单应结金额! 订单ID：" + orderId + " 执行结果：OK");
                } else {
                    baseOrderInfo.setResult("error" + nullToEmpty(dataMap.get("msg")));
                    logger.info("京东订单API：获取订单应结金额! 订单ID：" + orderId + " 执行结果：NG" + nullToEmpty(dataMap.get("msg")));
                }
            } else {
                logger.info("京东订单API：获取订单应结金额! 订单ID：" + orderId + " 执行结果：NG" + nullToEmpty(revObj.get("msg")));
            }

        } catch (Exception e) {
            logger.error("京东订单API：获取订单应结金额! 订单ID：" + orderId + " 执行异常：ERROR" + e.getMessage());
        }

        return baseOrderInfo;
    }

    /**
     * 京东到家订单API：商家确认订单
     * 应用场景:
     * 若门店接单模式为“手动接单”模式，商家需要调此接口，“确认接单”或“取消接单”
     *
     * @param app_key    应用的app_key
     * @param app_secret
     * @param token
     * @param orderId
     * @return
     */
    public static String orderConfirm(String app_key, String app_secret, String token, String orderId) {
        String result = "";
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("orderId", orderId);
            param.put("isAgreed", true);
            param.put("operator", "qyjkyf");
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_CONFIRM, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                if (nullToEmpty(dataMap.get("code")).equals("0")) {
                    result = "ok";
                    logger.info("京东订单API：商家确认订单! 订单ID：" + orderId + " 执行结果：OK");
                } else {
                    result = "error" + nullToEmpty(dataMap.get("msg"));
                    logger.info("京东订单API：商家确认订单! 订单ID：" + orderId + " 执行结果：NG");
                }
            } else {
                result = "error" + nullToEmpty(revObj.get("msg"));
                logger.info("京东订单API：商家确认订单! 订单ID：" + orderId + " 执行结果：NG");
            }
        } catch (Exception e) {
            logger.error("京东订单API：商家确认订单! 订单ID：" + orderId + " 执行异常：ERROR" + e.getMessage());
            result = "error" + e.getMessage();
        }

        return result;
    }

    /**
     * 京东到家订单API：商家取消订单
     *
     * @param app_key    应用的app_key
     * @param app_secret
     * @param token
     * @param orderId
     * @return
     */
    public static String orderCancel(String app_key, String app_secret, String token, String orderId) {
        String result = "";
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("orderId", orderId);
            param.put("isAgreed", false);
            param.put("operator", "qyjkyf");
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_CONFIRM, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                if (nullToEmpty(dataMap.get("code")).equals("0")) {
                    result = "ok";
                    logger.info("京东订单API：商家取消订单! 订单ID：" + orderId + " 执行结果：OK");
                } else {
                    result = "error" + nullToEmpty(dataMap.get("msg"));
                    logger.info("京东订单API：商家取消订单! 订单ID：" + orderId + " 执行结果：NG");
                }
            } else {
                result = "error" + nullToEmpty(revObj.get("msg"));
                logger.info("京东订单API：商家取消订单! 订单ID：" + orderId + " 执行结果：NG");
            }
        } catch (Exception e) {
            logger.error("京东订单API：商家取消订单! 订单ID：" + orderId + " 执行异常：ERROR" + e.getMessage());
            result = "error" + e.getMessage();
        }

        return result;
    }

    /**
     * token更新确认
     * @param oldToken
     * @param newToken
     * @param appKey
     * @param appSecret
     * @return
     */
    public static boolean verifyUpdateToken(String oldToken, String newToken, String appKey, String appSecret) {
        Map<String, Object> param = new HashMap<>();
        param.put("oldToken", oldToken);
        param.put("newToken", newToken);
        param.put("appKey", appKey);
        Map<String, String> params = buildParam(appKey, appSecret, oldToken, param);
        String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_API_VERIFY_UPDATE_TOKEN, params);
        Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
        //{"code":"0","msg":"操作成功, UUID[cb4b8bc82c6e48d484a37616c52dd673]","data":"{\"msg\":\"tokne更新信息不存在\",\"code\":\"3002\",\"data\":null,\"detail\":null}","success":true}{"code":"0","msg":"操作成功, UUID[cb4b8bc82c6e48d484a37616c52dd673]","data":"{\"msg\":\"tokne更新信息不存在\",\"code\":\"3002\",\"data\":null,\"detail\":null}","success":true}
        if (nullToEmpty(revObj.get("code")).equals("0")) {
            Object data = revObj.get("data");
            if (data != null) {
                Map<String, Object> dataResult = (Map<String, Object>) JSON.parse((String) data);
                //编码,0:表示成功；3001:表示参数不能为空，3002:表示token更新信息不存在
                if ("0".equals(dataResult.get("code"))) {
                    logger.info("京东到家API，token更新确认，成功, oldToken:{},newToken:{}, appKey:{}", oldToken, newToken, appKey);
                    return true;
                }
            }
        }
        logger.info("京东到家API，token更新确认，失败, oldToken:{},newToken:{},appKey:{}, result:{}", oldToken, newToken, appKey, rev);
        return false;
    }

    /**
     * 京东到家订单API：查询售后单详情
     *
     * @param app_key         应用的app_key
     * @param app_secret
     * @param token
     * @param afsServiceOrder 售后单号
     * @return
     */
    public static Map<String, Object> orderAfs(String app_key, String app_secret, String token, String afsServiceOrder) {
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("afsServiceOrder", afsServiceOrder);
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_AFS, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                Map<String, Object> resultMap = (Map<String, Object>) JSON.parse(nullToEmpty(dataMap.get("result")));
                if (nullToEmpty(dataMap.get("code")).equals("0") && resultMap != null) {
                    result.put("result", "ok");
                    result.put("afsServiceState", nullToEmpty(resultMap.get("afsServiceState")));
                    result.put("orderId", nullToEmpty(resultMap.get("orderId")));
                    result.put("reason", nullToEmpty(resultMap.get("questionDesc")));
                    result.put("reason_code", nullToEmpty(resultMap.get("questionTypeCid")));
                    //这边信息不全，还得补充（Li.Deman）
                    //应退商品金额
                    String cashMoney = nullToEmpty(resultMap.get("cashMoney"));
                    //应退订单餐盒费
                    String mealBoxMoney = nullToEmpty(resultMap.get("mealBoxMoney"));
                    //应退订单运费金额
                    String orderFreightMoney = nullToEmpty(resultMap.get("orderFreightMoney"));
                    //应退包装费金额
                    String packagingMoney = nullToEmpty(resultMap.get("packagingMoney"));
                    //应退到家积分金额
                    String platformIntegralDeductMoney = nullToEmpty(resultMap.get("platformIntegralDeductMoney"));
                    //用户售后实际退款总金额totalMoney=cashMoney+mealBoxMoney+orderFreightMoney+packagingMoney+platformIntegralDeductMoney
                    String totalMoney = addForTwoString(addForTwoString(addForTwoString(addForTwoString(cashMoney, mealBoxMoney), orderFreightMoney), packagingMoney), platformIntegralDeductMoney);
                    result.put("refund_money", centToDollarForString(totalMoney));
                    //退款商品明细（说明无论是部分还是全部退款，都是同一个接口）
                    List<Map<String, Object>> refundList = (List<Map<String, Object>>) JSON.parse(nullToEmpty(resultMap.get("afsDetailList")));
                    //只抓对自己有用的信息：medicine_code+refund_qty+refund_price
                    List<Map<String, Object>> items = new ArrayList<>();
                    Map<String, Object> proMap = null;
                    for (int i = 0; i < refundList.size(); i++) {
                        proMap = refundList.get(i);
                        Map<String, Object> item = new HashMap<>();
                        item.put("medicine_code", nullToEmpty(proMap.get("skuIdIsv")));
                        item.put("refund_qty", nullToEmpty(proMap.get("skuCount")));
                        //应退商品金额
                        item.put("refund_price", centToDollarForString(nullToEmpty(proMap.get("afsMoney"))));
                        items.add(item);
                    }
                    result.put("items", items);
                    logger.info("京东订单API：查询售后单详情! 售后单号：" + afsServiceOrder + " 执行结果：OK");
                } else {
                    result.put("result", "error" + nullToEmpty(dataMap.get("msg")));
                    logger.info("京东订单API：查询售后单详情! 售后单号：" + afsServiceOrder + " 执行结果：NG" + nullToEmpty(dataMap.get("msg")));
                }
            } else {
                result.put("result", "error" + nullToEmpty(revObj.get("msg")));
                logger.info("京东订单API：查询售后单详情! 售后单号：" + afsServiceOrder + " 执行结果：" + rev);
            }
        } catch (Exception e) {
            logger.error("京东订单API：查询售后单详情! 售后单号：" + afsServiceOrder + " 执行异常：ERROR" + e.getMessage());
            result.put("result", "error" + e.getMessage());
        }
        return result;
    }

    /**
     * 京东到家订单API：同意退款
     *
     * @param app_key      应用的app_key
     * @param app_secret
     * @param token
     * @param serviceOrder 售后单号
     * @return
     */
    public static String agreeRefund(String app_key, String app_secret, String token, String serviceOrder) {
        String result = "";
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("serviceOrder", serviceOrder);
            param.put("approveType", 1);
            param.put("rejectReason", "");
            param.put("optPin", "qyjkyf");
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_AFSAPPROVE, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                if (nullToEmpty(dataMap.get("code")).equals("0")) {
                    result = "ok";
                    logger.info("京东订单API：商家同意退款! 售后单号：" + serviceOrder + " 执行结果：OK");
                } else {
                    result = "error" + nullToEmpty(dataMap.get("msg"));
                    logger.info("京东订单API：商家同意退款! 售后单号：" + serviceOrder + " 执行结果：NG");
                }
            } else {
                result = "error" + nullToEmpty(revObj.get("msg"));
                logger.info("京东订单API：商家同意退款! 售后单号：" + serviceOrder + " 执行结果：NG");
            }
        } catch (Exception e) {
            logger.error("京东订单API：商家同意退款! 售后单号：" + serviceOrder + " 执行异常：ERROR" + e.getMessage());
        }

        return result;
    }


    /**
     * 京东到家订单API：拒绝退款
     *
     * @param app_key      应用的app_key
     * @param app_secret
     * @param token
     * @param serviceOrder 售后单号
     * @param rejectReason 拒绝原因
     * @return
     */
    public static String disagreeRefund(String app_key, String app_secret, String token, String serviceOrder, String rejectReason) {
        String result = "";
        try {
            if (StringUtils.isBlank(rejectReason)) {
                rejectReason = "门店反馈不便退单，请联系门店电话处理，给您带来困扰和不便表示抱歉！";
            }
            Map<String, Object> param = new HashMap<>();
            param.put("serviceOrder", serviceOrder);
            param.put("approveType", 3);
            param.put("rejectReason", rejectReason);
            param.put("optPin", "qyjkyf");
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_AFSAPPROVE, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                if (nullToEmpty(dataMap.get("code")).equals("0")) {
                    result = "ok";
                    logger.info("京东订单API：商家拒绝退款! 售后单号：" + serviceOrder + " 执行结果：OK");
                } else {
                    result = "error" + nullToEmpty(dataMap.get("msg"));
                    logger.info("京东订单API：商家拒绝退款! 售后单号：" + serviceOrder + " 执行结果：NG");
                }
            } else {
                result = "error" + nullToEmpty(revObj.get("msg"));
                logger.info("京东订单API：商家拒绝退款! 售后单号：" + serviceOrder + " 执行结果：NG");
            }
        } catch (Exception e) {
            logger.error("京东订单API：商家拒绝退款! 售后单号：" + serviceOrder + " 执行异常：ERROR" + e.getMessage());
        }

        return result;
    }

    /**
     * 京东到家订单API：商家确认用户取消申请  --商家审核用户取消申请接口
     * 应用场景：
     * 用户发起“订单取消申请”，商家需调此接口进行审核（同意或驳回）
     * 注：用户申请取消订单后，商家需在15分钟内审核，超时系统默认同意申请
     * @param app_key    应用的app_key
     * @param app_secret
     * @param token
     * @param orderId
     * @return
     */
    public static String orderCancelConfirm(String app_key, String app_secret, String token, String orderId) {
        String result = "";
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("orderId", orderId);
            param.put("isAgreed", true);
            param.put("operator", "qyjkyf");
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_CANCEL_CONFIRM, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                if (nullToEmpty(dataMap.get("code")).equals("0")) {
                    result = "ok";
                    logger.info("京东订单API：商家确认用户取消申请! 订单ID：" + orderId + " 执行结果：OK");
                } else {
                    result = "error" + nullToEmpty(dataMap.get("msg"));
                    logger.info("京东订单API：商家确认用户取消申请! 订单ID：" + orderId + " 执行结果：NG");
                }
            } else {
                result = "error" + nullToEmpty(revObj.get("msg"));
                logger.info("京东订单API：商家确认用户取消申请! 订单ID：" + orderId + " 执行结果：NG");
            }
        } catch (Exception e) {
            logger.error("京东订单API：商家确认用户取消申请! 订单ID：" + orderId + " 执行异常：ERROR" + e.getMessage());
        }

        return result;
    }

    /**
     * 京东到家订单API：呼叫京东配送
     *
     * @param app_key    应用的app_key
     * @param app_secret
     * @param token
     * @param orderId
     * @return
     */
    public static String callDelivery(String app_key, String app_secret, String token, String orderId) {
        String result = "";
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("orderId", orderId);
            param.put("operator", "qyjkyf");
            Map<String, String> params = buildParam(app_key, app_secret, token, param);
            String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_ORDER_DELIVERY, params);
            Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                if (nullToEmpty(dataMap.get("code")).equals("0")) {
                    result = "ok";
                    logger.info("京东订单API：呼叫京东配送! 订单ID：" + orderId + " 执行结果：OK");
                } else {
                    result = "error" + nullToEmpty(dataMap.get("msg"));
                    logger.info("京东订单API：呼叫京东配送! 订单ID：" + orderId + " 执行结果：NG");
                }
            } else {
                result = "error" + nullToEmpty(revObj.get("msg"));
                logger.info("京东订单API：呼叫京东配送! 订单ID：" + orderId + " 执行结果：NG");
            }
        } catch (Exception e) {
            logger.error("京东订单API：呼叫京东配送! 订单ID：" + orderId + " 执行异常：ERROR" + e.getMessage());
            result = "error" + e.getMessage();
        }

        return result;
    }


    /**
     * @Description: 获取门店对应商品清单
     * 结果示例：[{"msg":"成功","stockout":0,"code":0,"lockQty":0,"usableQty":0,"vendibility":0,"orderQty":0,"currentQty":0,"outSkuId":"40203008","skuId":2041143517}]
     * 实测：2754条数据，查询时间29454ms，可怕哦
     * @Param: [source, secret, shop_no]
     * @return: java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2019/1/3
     */
    public static List<Map<String, Object>> getmedicineList(String app_key, String app_secret, String token, String shop_no) {
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            int pageindex = 0;
            while (true) {
                pageindex++;
                Map<String, Object> param = new HashMap<>();
                param.put("pageNo", pageindex);
                param.put("pageSize", 50);//不填写分页信息默认最多返回50条数据。
                param.put("isFilterDel", "0");
                Map<String, String> params = buildParam(app_key, app_secret, token, param);
                if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SKU_LIST)) {
                    String rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SKU_LIST, params);
                    Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
                    if (nullToEmpty(revObj.get("code")).equals("0")) {
                        Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                        Object tempResult = dataMap.get("result");
                        if (tempResult == null || "".equals(tempResult)) {
                            return result;
                        }
                        List<Map<String, Object>> itemList = (List<Map<String, Object>>) JSON.parse(nullToEmpty(((Map<String, Object>) dataMap.get("result")).get("result")));
                        if (itemList != null) {
                            Map<String, Object> paramShop = new HashMap<>();
                            List<Map<String, Object>> outSkuIdList = new ArrayList<>();
                            paramShop.put("outStationNo", shop_no);
                            paramShop.put("skuIds", outSkuIdList);
                            for (Map<String, Object> item : itemList) {
                                if ((!StringUtils.isBlank(nullToEmpty(item.get("outSkuId")))) && (!nullToEmpty(item.get("outSkuId")).equals("null"))) {
                                    Map<String, Object> outSku = new HashMap<>();
                                    outSku.put("outSkuId", nullToEmpty(item.get("outSkuId")));
                                    outSkuIdList.add(outSku);
                                }
                            }
                            Map<String, String> paramsShop = buildParam(app_key, app_secret, token, paramShop);
                            if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SKU_SHOP_LIST)) {
                                String revShop = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SKU_SHOP_LIST, paramsShop);
                                Map<String, Object> revShopObj = (Map<String, Object>) JSON.parse(revShop);
                                if (nullToEmpty(revShopObj.get("code")).equals("0")) {
                                    Map<String, Object> dataShopMap = (Map<String, Object>) JSON.parse(nullToEmpty(revShopObj.get("data")));
                                    List<Map<String, Object>> itemsList = (List<Map<String, Object>>) dataShopMap.get("data");
                                    if (itemsList != null) {
                                        for (Map<String, Object> items : itemsList) {
                                            if (nullToEmpty(items.get("code")).equals("0")) {
                                                result.add(items);
                                            }
                                        }
                                    }
                                } else {
                                    logger.info("京东药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行异常：" + nullToEmpty(revShopObj.get("msg")));
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        logger.info("京东药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行异常：" + nullToEmpty(revObj.get("msg")));
                        //break;//这样无法区分是否报错
                        return null;
                    }
                }
            }
            logger.info("京东药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行结果：返回结果条目" + result.size());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("京东药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行异常：" + e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 返回京东到家在线门店清单
     * @Param: [app_key, app_secret, token]
     * @return: java.util.List<java.lang.String>
     * @Author: Qin.Qing
     * @Date: 2019/1/3
     */
    public static List<String> getshopList(String app_key, String app_secret, String token) {
        List<String> result = new ArrayList();
        try {
            String rev = "";
            if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SHOP_LIST)) {
                Map<String, String> params = buildParam(app_key, app_secret, token, null);
                rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SHOP_LIST, params);
                Map<String, Object> revObj = (Map<String, Object>) JSON.parse(rev);
                if (nullToEmpty(revObj.get("code")).equals("0")) {
                    Map<String, Object> dataMap = (Map<String, Object>) JSON.parse(nullToEmpty(revObj.get("data")));
                    List<String> shopList = (List<String>) dataMap.get("result");
                    for (String shop : shopList) {
                        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SHOP_INFO)) {
                            Map<String, Object> shopInfo = new HashMap<>();
                            shopInfo.put("StoreNo", shop);
                            Map<String, String> param = buildParam(app_key, app_secret, token, shopInfo);
                            String revinfo = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SHOP_INFO, param);
                            Map<String, Object> revInfoObj = (Map<String, Object>) JSON.parse(revinfo);
                            if (nullToEmpty(revInfoObj.get("code")).equals("0")) {
                                Map<String, Object> InfoMap = (Map<String, Object>) JSON.parse(nullToEmpty(revInfoObj.get("data")));
                                result.add(nullToEmpty(((Map) InfoMap.get("result")).get("outSystemId")));
                            } else {
                                logger.info("京东药品API：查询门店清单信息!" + " 执行异常：" + nullToEmpty(revInfoObj.get("msg")));
                            }
                        }
                        updateStoreConfig4Open(app_key, app_secret, token, shop, "N");
                    }
                } else {
                    logger.info("京东药品API：查询门店清单信息!" + " 执行异常：" + nullToEmpty(revObj.get("msg")));
                }
            }
            logger.info("京东药品API：查询门店清单信息!" + " 执行结果：返回结果条目" + result.size());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("京东药品API：查询门店清单信息!" + " 执行异常：" + e.getMessage());
        }
        return result;
    }


    /**
     * @Description: 配置门店进入手动接单模式
     * @Param: [app_key, app_secret, token, shopId, isAutoOrder]
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2019/01/25
     */
    public static String updateStoreConfig4Open(String app_key, String app_secret, String token, String shopId, String isAutoOrder) {
        Map<String, Object> param = new HashMap<>();
        param.put("stationNo", shopId);
        param.put("isAutoOrder", "Y".equals(isAutoOrder) ? 0 : 1);
        Map<String, String> params = buildParam(app_key, app_secret, token, param);
        String result = "";
        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SHOP_ConfigOpen)) {
            result = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SHOP_ConfigOpen, params);
        }
        logger.info("京东药品API：配置门店进入手动接单模式! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }

    //库存更新每次最多修改50条
    public static final int STOCK_UPDATE_MAX_COUNT = 50;

    /**
     * @Description: 更新京东库存
     * 根据商家商品编码和商家门店编码批量修改现货库存接口
     * 应用场景
     * 根据“商家门店编码”或“到家门店编码”（二者填其一）和“商家商品编码”批量修改商品现货库存。
     * 注：每次最多修改50条
     * @Param: [app_key, app_secret, token, shopId, param]
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2019/1/3
     */
    public static String medicineStock(String app_key, String app_secret, String token, String shopId, Map<String, Object> param) {
        Map<String, String> params = buildParam(app_key, app_secret, token, param);
        String result = "";
        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_STOCK_UPDATE)) {
            result = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_STOCK_UPDATE, params);
        }
        /**
         * success 1:
         * {"code":"0","msg":"操作成功, UUID[570c73dd7e9b47ae9db0258a250a7f45]",
         *    "data":"{\"ret\":true,\"data\":[{\"msg\":\"更新成功\",\"code\":0,\"outSkuId\":\"11603023\"}],\"retCode\":\"0\",\"retMsg\":\"成功\"}","success":true}
         * success 2:
         * {"code":"0","msg":"操作成功, UUID[a16fd1cd3eea434b856ebd53f507a8c0]",
         *    "data":"{\"ret\":true,\"data\":[{\"msg\":\"更新成功\",\"code\":0,\"outSkuId\":\"11603023\"},{\"msg\":\"更新成功\",\"code\":0,\"outSkuId\":\"11603021\"}],
         *    \"retCode\":\"0\",\"retMsg\":\"成功\"}","success":true}
         * fail 1:
         * {"code":"0","msg":"操作成功, UUID[05dc79e720e64d1a9500988a8bbf62af]",
         *    "data":"{\"ret\":false,\"data\":null,\"retCode\":\"23\",\"retMsg\":\"未获取到到家门店编号\"}","success":true}
         * fail 2:
         * {"code":"0","msg":"操作成功, UUID[0458090901ff4edfb0911c69988e1bd2]",
         *    "data":"{\"ret\":true,\"data\":[{\"msg\":\"未查询到到家商品编码\",\"code\":1,\"outSkuId\":\"11603023---\"}],\"retCode\":\"0\",\"retMsg\":\"成功\"}","success":true}
         * 并没有明显字段去判断成功与否，写的是真的烂，只能照京东文档里,用最外层的code: 0表示成功，其他均为失败。
         */
        logger.info("京东到家药品API：更新药品库存! 门店ID:{}, appSecret:{}, param:{}, result:{}", shopId, app_secret, params, result);
        return result;
    }

    //todo medicine_Create
    public static String medicine_Create(String app_key, String app_secret, String token, String shopId, List<Map<String, Object>> list) {
        Map<String, Object> param = new HashMap<>();
        param.put("batchSkuRequestList", list);
        Map<String, String> params = buildParam(app_key, app_secret, token, param);
        String result = "";
        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SKU_CREATE)) {
            result = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SKU_CREATE, params);
        }
        logger.info("京东药品API：创建药品! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }

    //todo medicine_Price_Update
    public static String medicine_Price_Update(String app_key, String app_secret, String token, String shopId, Map<String, Object> param) {
        Map<String, String> params = buildParam(app_key, app_secret, token, param);
        String result = "";
        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_SKU_PRICE)) {
            result = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_SKU_PRICE, params);
        }
        logger.info("京东药品API：更新药品价钱! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }

    //todo medicine_Category_Create
    public static String medicine_Category_Create(String app_key, String app_secret, String token, String shopId, String category_name, String category_seq) {
        Map<String, Object> param = new HashMap<>();
        param.put("pid", 0);
        param.put("shopCategoryName", category_name);
        Map<String, String> params = buildParam(app_key, app_secret, token, param);
        String result = "";
        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_CATEGORY_ADD)) {
            result = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_CATEGORY_ADD, params);
            Map<String, Object> revObj = JSON.parseObject(result);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = JSON.parseObject(nullToEmpty(revObj.get("data")));
                result = "ok:" + nullToEmpty(((Map<String, Object>) dataMap.get("result")).get("id"));
            }
        }
        logger.info("京东药品API：创建药品分类! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }

    //todo get_Medicine_Category_List
    public static List<Map<String, Object>> get_Medicine_Category_List(String app_key, String app_secret, String token, String shopId) {
        List<Map<String, Object>> result = new ArrayList<>();
        Map<String, Object> param = new HashMap<>();
        List<String> fields = new ArrayList<>();
        fields.add("ID");
        fields.add("PID");
        fields.add("SHOP_CATEGORY_NAME");
        fields.add("SORT");
        param.put("fields", fields);
        Map<String, String> params = buildParam(app_key, app_secret, token, param);
        String rev = "";
        if (JdFreqUtil.checkFreq(JdConfig.JD_CMD_CATEGORY_LIST)) {
            rev = HttpUtils.doPost(JdConfig.JD_API_SERVER, JdConfig.JD_CMD_CATEGORY_LIST, params);
            Map<String, Object> revObj = JSON.parseObject(rev);
            if (nullToEmpty(revObj.get("code")).equals("0")) {
                Map<String, Object> dataMap = JSON.parseObject(nullToEmpty(revObj.get("data")));
                result = (List<Map<String, Object>>) dataMap.get("result");
            }
        }
        logger.info("京东药品API：获取药品分类清单! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }

    public static Map<String, String> buildParam(String app_key, String app_secret, String token, Map<String, Object> param) {
        Map<String, String> result = new HashMap<>();
        result.put("token", token);
        result.put("app_key", app_key);
        result.put("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        result.put("format", "json");
        result.put("v", "1.0");
        if (param != null) {
            result.put("jd_param_json", JSON.toJSONString(param));
        }
        String sign = SignUtil.getSignByMD5(result, app_secret);
        result.put("sign", sign);
        return result;
    }

    /**
     * 接口返回错误码：
     * 错误码	描述
     * 0	    操作成功
     * -1	    操作失败
     * -10000	业务重试
     * 10005	必填项参数未填
     * 10013	无效Token令牌
     * 10014	无效Sign签名
     * 10015	API参数异常
     * 10018	不存在的方法名
     */
    public static String apiReturnMsg(String code, String msg, Map<String, Object> data) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", code);
        result.put("msg", msg);
        result.put("data", JSON.toJSONString(data));
        return JSON.toJSONString(result);
    }

    /**
     * 参数转换
     *
     * @param orderMap
     * @return BaseOrderInfo
     */
    public static BaseOrderInfo convertToBaseOrder(Map<String, Object> orderMap) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        Map<String, Object> orderInvoiceMap = (Map<String, Object>) JSONObject.parse(nullToEmpty(orderMap.get("orderInvoice")));
        List<Map<String, Object>> prosMap = (List<Map<String, Object>>) JSON.parse(nullToEmpty(orderMap.get("product")));
        try {
            BeanUtils.populate(baseOrderInfo, orderMap);
            List<BaseOrderProInfo> proList = new ArrayList<BaseOrderProInfo>();
            Map<String, Object> proMap = null;
            BaseOrderProInfo baseOrderProInfo = null;
            // 内部订单ID
            String orderId = nullToEmpty(orderMap.get("orderId"));
            baseOrderInfo.setPlatforms_order_id(orderId);
            // 门店编码(合作方门店ID)
            String storeCode = nullToEmpty(orderMap.get("deliveryStationNoIsv"));
            baseOrderInfo.setShop_no(storeCode);
            /*			private String order_info; // 优惠信息*/
            //订单级别优惠商品金额：(不含单品促销类优惠金额及运费相关优惠金额)，等于OrderDiscountlist表中，除优惠类型7，8，12，15，16，18，外的优惠金额discountPrice累加和
            baseOrderInfo.setOrder_info(centToDollarForString(orderMap.get("orderDiscountMoney")));
            baseOrderInfo.setCreate_time(formateDate(orderMap.get("orderStartTime")));
            baseOrderInfo.setUpdate_time(formateDate(orderMap.get("orderStatusTime")));
            baseOrderInfo.setRecipient_address(nullToEmpty(orderMap.get("buyerFullAddress")));
            baseOrderInfo.setRecipient_name(nullToEmpty(orderMap.get("buyerFullName")));
            baseOrderInfo.setRecipient_phone(nullToEmpty(orderMap.get("buyerTelephone")));
            baseOrderInfo.setShipping_fee(centToDollarForString(orderMap.get("orderFreightMoney")));
            //baseOrderInfo.setTotal_price(centToDollarForString(orderMap.get("shop_fee")));
            //京东到家销售价:=skuJdPrice*商品下单数量skuCount
            baseOrderInfo.setOriginal_price(centToDollarForString(orderMap.get("orderTotalMoney")));
            baseOrderInfo.setCustomer_pay(centToDollarForString(orderMap.get("orderBuyerPayableMoney")));
            baseOrderInfo.setCaution(nullToEmpty(orderMap.get("orderBuyerRemark")));
            baseOrderInfo.setDay_seq(nullToEmpty(orderMap.get("orderNum")));
            // 订单时效类型(0:无时效;2:自定义时间;1:次日达;27:七小时达;24:六小时达;21:五小时达;18:四小时达;15:三小时达;12:两小时达;9:一小时达;6:半小时达;)
            //以上类型描述无效，京东技术文档不规范，采用orderPurchaseTime和orderPreStartDeliveryTime时间差是否小于1小时，区分是否为预订单。
            //如果确实是预订单，则采用orderPreStartDeliveryTime时间作为预订单的预计送达时间。
            String delivery_time = "0";
            Long orderPurchaseTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(formateDate(orderMap.get("orderPurchaseTime"))).getTime();
            Long orderPreStartDeliveryTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(formateDate(orderMap.get("orderPreStartDeliveryTime"))).getTime();
            if ((orderPreStartDeliveryTime - orderPurchaseTime) >= (1000 * 60 * 70)) {
                delivery_time = String.valueOf((int) (orderPreStartDeliveryTime / 1000));
            }
            //baseOrderInfo.setDelivery_time(nullToEmpty(orderMap.get("orderAgingType")));
            baseOrderInfo.setDelivery_time(delivery_time);
            // 是否开票（0：不需要发票1：需要发票）是否需要发票 1 是 2 否
            if (orderInvoiceMap != null) {
                baseOrderInfo.setIs_invoiced("1");
                baseOrderInfo.setInvoice_title(nullToEmpty(orderInvoiceMap.get("invoiceTitle")));
                baseOrderInfo.setTaxpayer_id(nullToEmpty(orderInvoiceMap.get("invoiceDutyNo")));
            } else {
                baseOrderInfo.setIs_invoiced("0");
            }
            // 支付类型（0：在线支付1：货到付款）付款类型 1下线 4 在线
            baseOrderInfo.setPay_type("4".equals(nullToEmpty(orderMap.get("orderPayType"))) ? "0" : "1");
            // 获取订单明细并记录在途库存信息
            for (int i = 0; i < prosMap.size(); i++) {
                proMap = prosMap.get(i);
                baseOrderProInfo = new BaseOrderProInfo();
                BeanUtils.populate(baseOrderProInfo, proMap);
                baseOrderProInfo.setMedicine_code(nullToEmpty(proMap.get("skuIdIsv")));
                baseOrderProInfo.setQuantity(nullToEmpty(proMap.get("skuCount")));
                baseOrderProInfo.setSequence(String.valueOf(i + 1));
                baseOrderProInfo.setShop_no(storeCode);
                baseOrderProInfo.setStock_lock(baseOrderProInfo.getQuantity());
                baseOrderProInfo.setPrice(centToDollarForString(proMap.get("skuJdPrice")));
                proList.add(baseOrderProInfo);
            }
            baseOrderInfo.setItems(proList);
            baseOrderInfo.setResult("ok");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return baseOrderInfo;
    }


    /**
     * 金额转换方法：分转换成元
     *
     * @param t
     * @return
     */
    public static String centToDollarForString(Object t) {
        if (t == null) {
            return "0";
        } else {
            BigDecimal amount = getBigDecimal(t);
            amount = amount.divide(new BigDecimal(100));
            return amount.toString();
        }
    }

    private static BigDecimal getBigDecimal(Object t) {
        BigDecimal amount;
        if (t instanceof Integer) {
            amount = new BigDecimal(t.toString());
        } else if (t instanceof Long) {
            amount = new BigDecimal(t.toString());
        } else if (t instanceof String) {
            amount = new BigDecimal(t.toString());
        } else {
            throw new RuntimeException(String.format("不支持的数据类型,%s", t.getClass()));
        }
        return amount;
    }

    /**
     * 字符串转换函数
     *
     * @param param 参数
     * @return String
     */
    private static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }

    }

    /**
     * 字符串转换函数
     *
     * @param param 参数
     * @return String
     */
    public static String formateDate(Object param) {
        if (param == null) {
            return "";
        } else {
            String res = String.valueOf(param);
            res = res.replaceAll("-", "/");
            if (res.length() > 19) {
                return res.substring(0, 19);
            } else {
                return res;
            }
        }

    }

    /**
     * 两个字符串相加，返回结果还是字符串
     *
     * @param param1
     * @param param2
     * @return
     * @author：Li.Deman
     * @date：2019/1/14
     */
    private static String addForTwoString(String param1, String param2) {
        String result = null;
        if (param1.equals("")) {
            if (param2.equals("")) {
                result = "0";
            } else {
                result = param2;
            }
        } else {
            if (param2.equals("")) {
                result = param1;
            } else {
                double temp = Double.valueOf(param1) + Double.valueOf(param2);
                result = String.valueOf(temp);
            }
        }
        return result;
    }
}
