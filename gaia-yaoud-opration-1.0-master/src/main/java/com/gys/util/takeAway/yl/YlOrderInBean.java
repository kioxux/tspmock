package com.gys.util.takeAway.yl;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/13 10:18
 */
@Data
public class YlOrderInBean {

    private List<YlOrderBean> orders;
}
