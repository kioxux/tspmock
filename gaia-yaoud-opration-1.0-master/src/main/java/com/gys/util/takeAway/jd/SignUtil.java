package com.gys.util.takeAway.jd;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignUtil {
    private static Logger logger = LoggerFactory.getLogger(SignUtil.class);

    /**
     * 拼接所有业务参数
     * concatParams </br>
     * @param params2
     * @return </br>
     * String
     */
    private static String concatParams(Map<String, String> params2) {
        Object[] key_arr = params2.keySet().toArray();
        Arrays.sort(key_arr);
        StringBuilder sb = new StringBuilder();
        for (Object key : key_arr) {
            String val = params2.get(key);
            sb.append(key).append(val);
        }
        return sb.toString();
    }

    /**
     * 拼接所有业务参数
     * @param params2
     * @return
     * @author：Li.Deman
     * @date：2019/1/24
     */
    private static String concatParamsByMap(Map<String, Object> params2){
        //定义的数组，应该去掉sign参数
        Map<String,String> applicationParamsMap=new HashMap<>();
        for(String key:params2.keySet()){
            if (key.equals("sign")){
                continue;
            }
            applicationParamsMap.put(key,(String) params2.get(key));
        }
        return concatParams(applicationParamsMap);
    }

    /**
     * 拼接对象属性
     * getProperty </br>
     * @param entityName
     * @return
     * @throws Exception </br>
     * Map<String,String>
     */
    public static Map<String,String> getProperty(Object entityName) throws Exception {
        Map<String,String> mapResult=new HashMap<String, String>();
        Class<? extends Object> c = entityName.getClass();
        Field field[] = c.getDeclaredFields();
        for (Field f : field) {
            Object v = invokeMethod(entityName, f.getName(), null);
            if (v == null||"serialVersionUID".equals(f.getName())||"sign".equals(f.getName())){
                continue;
            }
            mapResult.put(f.getName(), v.toString());
        }
        return mapResult;
    }

    /**
     * 获取属性值
     * invokeMethod </br>
     * @param owner
     * @param methodName
     * @param args
     * @return
     * @throws Exception </br>
     * Object
     */
    private static Object invokeMethod(Object owner, String methodName, Object[] args) throws Exception {
        Class<? extends Object> ownerClass = owner.getClass();
        methodName = methodName.substring(0, 1).toUpperCase() + methodName.substring(1);
        Method method = null;
        try {
            method = ownerClass.getMethod("get" + methodName);
        }catch (Exception e) {
            return " can't find 'get" + methodName + "' method";
        }
        return method.invoke(owner);
    }

    /**
     *
     * getSign 获取MD5签名
     * @param param 参数对象
     * @param appSecret 应用secret
     * @return </br>
     * String
     */
    public static String getSignByMD5(Object param,String appSecret ) throws Exception{
        String sysStr = concatParams(getProperty(param));
        StringBuilder resultStr = new StringBuilder("");
        resultStr.append(appSecret).append(sysStr).append(appSecret);
        return MD5Util.getMD5String(resultStr.toString()).toUpperCase();
    }

    /**
    * @Description:获取MD5签名
    * @Param: [param, appSecret]
    * @return: java.lang.String
    * @Author: Qin.Qing
    * @Date: 2019/1/2
    */
    public static String getSignByMD5(Map<String,String> param,String appSecret ){
        String sysStr = concatParams(param);
        StringBuilder resultStr = new StringBuilder();
        resultStr.append(appSecret).append(sysStr).append(appSecret);
        return MD5Util.getMD5String(resultStr.toString()).toUpperCase();
    }

    /**
     * 获取MD5签名
     * @param param
     * @param appSecret
     * @return
     * @author：Li.Deman
     * @date：2019/1/24
     */
    public static String getSignByMD5ForObject(Map<String,Object> param,String appSecret){
        String sysStr = concatParamsByMap(param);
        StringBuilder resultStr = new StringBuilder();
        resultStr.append(appSecret).append(sysStr).append(appSecret);
        //去掉+，改成空格
        //return MD5Util.getMD5String(resultStr.toString().replaceAll("\\+"," ")).toUpperCase();
        return MD5Util.getMD5String(resultStr.toString()).toUpperCase();
    }

    /**
     *
     * getSign 获取SHA512签名
     * @param param 参数对象
     * @param appSecret 应用secret
     * @return </br>
     * String
     */
    public static String getSignBySHA512(Object param,String appSecret ) throws Exception{
        String sysStr = concatParams(getProperty(param));
        StringBuilder resultStr = new StringBuilder("");
        resultStr.append(appSecret).append(sysStr).append(appSecret);
        return DigestUtils.sha512Hex(resultStr.toString()).toUpperCase();
    }

    /**
     * 校验签名是否正确
     * @param param
     * @param appSecret
     * @return
     * @author：Li.Deman
     * @date：2019/1/24
     */
    public static boolean checkSign(Map<String,Object> param,String appSecret){
        logger.info("param:"+ JSON.toJSONString(param));
        String sign = String.valueOf(param.get("sign")).toUpperCase();
        String signFrom=getSignByMD5ForObject(param,appSecret);
        if (signFrom.equals(sign)) {
            return true;
        } else {
            return false;
        }
    }
}
