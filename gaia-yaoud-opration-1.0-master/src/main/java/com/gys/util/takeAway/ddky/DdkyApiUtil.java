package com.gys.util.takeAway.ddky;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 叮当快药API
 *
 * @author ZhangDong
 * @date 2021/10/13 17:52
 */
public class DdkyApiUtil {
    private static final Logger log = LoggerFactory.getLogger(DdkyApiUtil.class);

    private static final String SERVER_TEST_URL = "http://61.135.14.103:8090/api/rest.htm";//测试环境
    private static final String SERVER_PRD_PRE_URL = "http://devkyopen.ddky.com/api/rest.htm";//预发布环境  不用管
    private static final String SERVER_PRD_URL = "http://kyopen.ddky.com/api/rest.htm";//生产环境
    private static final String SERVER_URL = SERVER_PRD_URL;

    private static final String STOCK_UPDATE_METHOD = "goods.sync.qty";
    private static final String ORDER_REFUND_METHOD = "order.rejection.in";
    private static final String GOODS_SYNC_METHOD = "goods.sync.all";
    public static final int SYNC_GOODS_MAX = 100;

    private static String testCompanyId = "47";
    private static String testSecretKey = "90d3400ed8d5950e2ec1933eb2f6b386";
    private static Long testShopId = 10001L;

    private static RestTemplate restTemplate;

    static {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(15000);
        requestFactory.setReadTimeout(30000);
        restTemplate = new RestTemplate(requestFactory);
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                List<MediaType> mediaTypeList = new ArrayList<>(converter.getSupportedMediaTypes());
                mediaTypeList.add(MediaType.TEXT_HTML);
                mediaTypeList.add(MediaType.APPLICATION_JSON);
                ((MappingJackson2HttpMessageConverter) converter).setSupportedMediaTypes(mediaTypeList);
            }
        }
    }


    public static void main(String[] args) throws UnsupportedEncodingException {
        //test 同步库存
        DdkyStockUpdateBean bean = new DdkyStockUpdateBean();
        bean.setShopId(testShopId);
        List<DdkyStockUpdateDetailBean> goodsList = new ArrayList<>();
        DdkyStockUpdateDetailBean detailBean = new DdkyStockUpdateDetailBean();
        detailBean.setGoodsId(100012222L);
        //detailBean.setGoodsId(112L);
        detailBean.setInvQty(400L);
        detailBean.setPrice("12.7");
        goodsList.add(detailBean);
        bean.setGoodsList(goodsList);
//        boolean b = stockUpdate(testCompanyId, testSecretKey, bean);

        //test 全量同步商品
//        boolean b = syncGoods(testCompanyId, testSecretKey, bean);
//        System.out.println(b);

        //退货
        DdkyRefundBean refundBean = new DdkyRefundBean();
        refundBean.setCreatedBy("test");
        refundBean.setOrderID("103104224641562");
        refundBean.setIsRefundFreight(1L);
        returnOrder(testCompanyId, testSecretKey, refundBean);
    }

    /**
     * 叮当快药，库存同步，inData:{"sign":"6dd6dea7ad704505222a41aaa722476a","companyId":"47","method":"goods.sync.qty","data":"{\"goodsList\":[],\"shopId\":10001}"}, result:{"body":{"code":"1000","msg":"companyId参数为空"},"headers":{"Pragma":["No-cache"],"Cache-Control":["no-cache"],"Expires":["Thu, 01 Jan 1970 00:00:00 GMT"],"Content-Type":["text/html;charset=utf-8"],"Transfer-Encoding":["chunked"],"Date":["Thu, 14 Oct 2021 07:49:17 GMT"],"Server":["WWW.DDKY.COM"]},"statusCode":"OK","statusCodeValue":200}
     */

    /**
     * 库存实时同步
     */
    public static boolean stockUpdate(String companyId, String secretKey, DdkyStockUpdateBean bean) {
        Map<String, String> map = new HashMap<>(4);
        map.put("companyId", companyId);
        String data = JSON.toJSONString(bean);
        map.put("data", data);
        map.put("method", STOCK_UPDATE_METHOD);
        String sign = SignUtil.generateSign(map, STOCK_UPDATE_METHOD, secretKey);
        String url = String.format(SERVER_URL + "?companyId=%s&method=%s&sign=%s&data={data}", companyId, STOCK_UPDATE_METHOD, sign);
        Map<String, Object> tempMap = new HashMap<>();
        tempMap.put("data", data);
        log.info("叮当快药，库存同步，inData:{}", url + data);
        ResponseEntity<DdkyResponse> responseEntity = restTemplate.getForEntity(url, DdkyResponse.class, tempMap);
        log.info("叮当快药，库存同步，inData:{}, result:{}", url + data, JSON.toJSONString(responseEntity));
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            DdkyResponse body = responseEntity.getBody();
            if ("0".equals(body.getCode())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 三方退货(拒收入库退货入库)
     */
    public static boolean returnOrder(String companyId, String secretKey, DdkyRefundBean bean) {
        Map<String, String> map = new HashMap<>(4);
        map.put("companyId", companyId);
        String data = JSON.toJSONString(bean);
        map.put("data", data);
        map.put("method", ORDER_REFUND_METHOD);
        String sign = SignUtil.generateSign(map, ORDER_REFUND_METHOD, secretKey);
        String url = String.format(SERVER_URL + "?companyId=%s&method=%s&sign=%s&data={data}", companyId, ORDER_REFUND_METHOD, sign);
        Map<String, Object> tempMap = new HashMap<>();
        tempMap.put("data", data);
        log.info("叮当快药，三方退货，inData:{}", url + data);
        ResponseEntity<DdkyResponse> responseEntity = restTemplate.getForEntity(url, DdkyResponse.class, tempMap);
        log.info("叮当快药，三方退货，inData:{}, result:{}", url + data, JSON.toJSONString(responseEntity));
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            DdkyResponse body = responseEntity.getBody();
            if ("0".equals(body.getCode())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 全量推送商品
     */
    public static boolean syncGoods(String companyId, String secretKey, DdkyStockUpdateBean bean) {
        Map<String, String> map = new HashMap<>(4);
        map.put("companyId", companyId);
        String data = JSON.toJSONString(bean);
        map.put("data", data);
        map.put("method", GOODS_SYNC_METHOD);
        String sign = SignUtil.generateSign(map, GOODS_SYNC_METHOD, secretKey);
        String url = String.format(SERVER_URL + "?companyId=%s&method=%s&sign=%s&data={data}", companyId, GOODS_SYNC_METHOD, sign);
        Map<String, Object> tempMap = new HashMap<>();
        tempMap.put("data", data);
        ResponseEntity<DdkyResponse> responseEntity = restTemplate.getForEntity(url, DdkyResponse.class, tempMap);
        /**
         * result: {"body":{"code":"0","msg":""},"headers":{"Pragma":["No-cache"],"Cache-Control":["no-cache"],"Expires":["Thu, 01 Jan 1970 00:00:00 GMT"],
         * "Content-Type":["text/html;charset=utf-8"],"Transfer-Encoding":["chunked"],"Date":["Tue, 19 Oct 2021 06:47:27 GMT"],
         * "Server":["WWW.DDKY.COM"]},"statusCode":"OK","statusCodeValue":200}
         */
        log.info("叮当快药，全量推送商品，inData:{}, result:{}", url + data, JSON.toJSONString(responseEntity));
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            DdkyResponse body = responseEntity.getBody();
            if ("0".equals(body.getCode())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 参数转换
     * 里面每一条明细的money是优惠分摊后的金额，加起来就是用户实际支付的金额，订单主体里面有个订单总优惠金额。总优惠金额+用户实际支付的金额就是这个订单优惠前的金额
     * price有优惠的时候也会分摊, money = price *数量
     * 即： 商品原价= money总和+总优惠；  用户支付金额=money总和； 门店的实际收入金额= 用户支付金额
     */
    public static BaseOrderInfo convertToBaseOrder(JSONObject dataJson) {
        BaseOrderInfo orderInfo = new BaseOrderInfo();
        orderInfo.setPlatforms_order_id(dataJson.getString("orderId"));
        orderInfo.setPlatforms(Constants.PLATFORMS_DDKY);
        orderInfo.setRecipient_name(dataJson.getString("userName"));
        orderInfo.setIs_invoiced("0");//是否支持开票 这些都是叮当系统上或者三方平台上面设置 跟线下系统没关系
        orderInfo.setDelivery_time("0");//延时配送 无
        orderInfo.setPay_type("0".equalsIgnoreCase(dataJson.getString("payType")) ? "1" : "0");//和我们系统是反的
        orderInfo.setOrder_info(dataJson.getString("discountPay"));
        orderInfo.setDay_seq("");//取货码 无
        orderInfo.setStatus(dataJson.getString("orderStatus"));
        BigDecimal discountPay = dataJson.getBigDecimal("discountPay");//总优惠金额
        orderInfo.setOrder_info(discountPay.toString());
        JSONArray orderItems = dataJson.getJSONArray("orderItems");
        List<BaseOrderProInfo> itemList = new ArrayList<>(orderItems.size());
        BigDecimal moneyTotalPrice = BigDecimal.ZERO;
        for (Object orderItem : orderItems) {
            JSONObject item = (JSONObject) orderItem;
            BaseOrderProInfo proInfo = new BaseOrderProInfo();
            proInfo.setMedicine_code(item.getString("goodsid"));
            proInfo.setQuantity(item.getString("goodsqty"));
            proInfo.setPrice(item.getString("price"));
            BigDecimal money = item.getBigDecimal("money");
            moneyTotalPrice = moneyTotalPrice.add(money);
            orderInfo.setCreate_time(item.getString("senda"));//创建时间得取商品明细里
            orderInfo.setRecipient_address(item.getString("address"));//收件人地址
            orderInfo.setStoCode(item.getString("placepointid"));//药店三方系统编码
            itemList.add(proInfo);
        }
        orderInfo.setOriginal_price(moneyTotalPrice.add(discountPay).toString());//原价
        orderInfo.setTotal_price(moneyTotalPrice.toString());
        orderInfo.setCustomer_pay(moneyTotalPrice.toString());
        orderInfo.setItems(itemList);
        return orderInfo;
    }

}
