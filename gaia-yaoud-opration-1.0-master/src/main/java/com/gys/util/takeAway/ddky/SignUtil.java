package com.gys.util.takeAway.ddky;

import java.security.MessageDigest;
import java.util.*;

/**
 * @author ZhangDong
 * @date 2021/10/13 17:56
 */
public class SignUtil {

    private static final String HEX_CHARS = "0123456789abcdef";

    public static void main(String[] args) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", "12");
        hashMap.put("ab", "12");
        hashMap.put("yk", "12");
        String s = generateSign(hashMap, "132", "1321");
        System.out.println(s);
    }

    public static String toHexString(byte[] b) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            sb.append(HEX_CHARS.charAt(b[i] >>> 4 & 0x0F));
            sb.append(HEX_CHARS.charAt(b[i] & 0x0F));
        }
        return sb.toString();
    }

    public static String generateSign(Map map, String methodName, String secretKey) {
        String str = getStrExcludeSign(map);
        String data = methodName + str + secretKey;
        String result = "";//加密
        try {
            result = toHexString(MessageDigest.getInstance("MD5").digest(data.getBytes("UTF-8")));
        } catch (Exception e) {
        }
        return result;
    }

    private static String getStrExcludeSign(Map map) {
        Set<String> keySet = map.keySet();
        //剔除sign放入参数集合
        List<String> list = new ArrayList();
        for (String key : keySet) {
            if ("sign".equalsIgnoreCase(key)) {
                continue;
            }
            list.add(key);
        }
        //参数集合升序排序
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object a, Object b) {
                return a.toString().toLowerCase()
                        .compareTo(b.toString().toLowerCase());
            }
        });
        //参数集合拼接成字符串
        StringBuffer sb = new StringBuffer();
        for (String key : list) {
            sb.append(key);
            Object obj = map.get(key);
            if (obj instanceof String[]) {
                String[] params = (String[]) map.get(key);
                for (int i = 0; i < params.length; i++) {
                    sb.append(params[i]);
                }
            } else {
                String param = String.valueOf(map.get(key));
                sb.append(param);
            }
        }
        return sb.toString();
    }
}
