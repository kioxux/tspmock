package com.gys.util.takeAway;

public class WebConofig {
    public final static String retCode = "status";
    public final static String retDesc = "desc";
    public final static String tokenFailCode = "1";
    public final static String tokenFailDesc = "token验证失败";
    public final static String userFailCode = "2";
    public final static String userFailDesc = "用户不存在";
    public final static String otherFailCode = "3";
    public static String otherFailDesc = "失败";
    public final static String existMyAsyncTaskCode = "4";
    public static String existMyAsyncTaskDesc = "当前用户还有未执行完的开关信息设置事务";
    public final static String successCode = "0";
    public final static String successDesc = "成功";
    public final static String beginLoadCode = "5";
    public final static String beginLoadDesc = "数据初始化";
}
