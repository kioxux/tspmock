package com.gys.util.takeAway.eb;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gys.common.config.takeAway.EbConfig;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.HttpUtils;
import com.gys.util.takeAway.UuidUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 饿百外卖-接口封装类
 * @author Mirai.lee
 * @date 2018.11.20
 */
public class EbApiUtil {
    private static Logger logger = LoggerFactory.getLogger(EbApiUtil.class);
    private static final int reTryGetOrderTime=30;

    /**
     * 中文转unicode,如果参数中有中文，需要转一下
     * @param gbString
     * @return
     */
    public static String gbEncoding(final String gbString) {
        char[] utfBytes = gbString.toCharArray();
        String unicodeBytes = "";
        for (int i = 0; i < utfBytes.length; i++) {
            String hexB = Integer.toHexString(utfBytes[i]);
            if (hexB.length() <= 2) {
                hexB = "00" + hexB;
            }
            unicodeBytes = unicodeBytes + "\\u" + hexB;
        }
        return unicodeBytes;
    }

    /**
     * 饿百订单API：商家确认订单
     * 饿了么：此接口主要用于订单接单，实现商家自动接单场景。注意：如订单下单后五分钟，商家未接单系统会自动取消订单。
     * @param source  饿百分配商家的source
     * @param secret  门店的secret
     * @param orderId 饿百平台订单ID
     */
    public static String orderConfirm(String source, String secret, String orderId) {
        String result="";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_CONFIRM);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp",String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        params.put("body", JSON.toJSONString(body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result = HttpUtils.doPost(EbConfig.EB_API_SERVER,"",params);
        logger.info("饿百订单API：商家确认订单! 订单ID：" + orderId + " 执行结果：" + result);
        return result;
    }

    /**
     * 饿百订单API：商品列表
     * @param source  饿百分配商家的source
     * @param secret  门店的secret
     * @param shopId  系统门店对外唯一编码
     */
    public static String skuList(String source, String secret, String shopId, Integer page, Integer pagesize) {
        String result="";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_SKU_LIST);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp",String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("shop_id", shopId);
        body.put("page", page);//页码,默认为1
        body.put("pagesize", pagesize);//获取数量,1-100,默认20
        params.put("body", JSON.toJSONString(body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result = HttpUtils.doPost(EbConfig.EB_API_SERVER,"",params);
        logger.info("饿百订单API：商品列表查询, shopId：{}, 执行结果：{}", shopId, result);
        return result;
    }

    /**
     * 饿百订单API：商家取消订单
     * 饿了么：此接口主要用于售中商家发起全单取消场景。注意：商家只能在骑手已取餐状态前发起全单取消，骑手已取餐后（包含骑手已取餐）不再支持商家发起全单取消（或部分退款）。
     * @param source  饿百分配商家的source
     * @param secret  门店的secret
     * @param orderId 饿百平台订单ID
     * @param reason  取消原因
     * @param type    规范化取消原因code
     */
    public static String orderCancel(String source, String secret, String orderId, String type, String reason) {
        String result="";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_CANCEL);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        //如果encrypt没有，直接用""，不要用null表示
        params.put("encrypt", "");
        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        body.put("type", Integer.parseInt(type));
        body.put("reason", reason);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result =HttpUtils.doPost(EbConfig.EB_API_SERVER,"",params);
        logger.info("饿百订单API：商家取消订单! 订单ID：" + orderId + " 执行结果：" + result);
        return result;
    }

    /**
     * 饿百订单API：查看订单详情
     * 注意：1. 如订单详情中down_flag为1时，标识该订单因网络或信息交互异常被降级，此时订单会被标记为“已降级”状态。此类订单不影响订单履约流程（无需拒单），
     * 重试查询获取订单完整数据即可。2. 尽可查询近三个月内的订单
     * @param source  饿百分配商家的source
     * @param secret  门店的secret
     * @param orderId 饿百平台订单ID
     */
    public static BaseOrderInfo orderDetail(String source, String secret, String orderId, String client) {
        BaseOrderInfo result=null;
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_GET);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示l

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        int trytime=0;
        while(trytime<reTryGetOrderTime) {
            trytime++;
            try {
                String rev = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
                logger.info("饿百订单API：查看订单详情! 订单ID：" + orderId + " 执行结果：" + rev);
                boolean ret = validApiResult(rev);
                if (ret) {
                    result= convertToBaseOrder(rev, client);
                    Map<String,Object> resultMap=JSON.parseObject(rev);
                    String down_flag=String.valueOf(((Map<String, Object>)((Map<String, Object>)((Map<String, Object>)resultMap.get("body")).get("data")).get("order")).get("down_flag"));
                    if (down_flag.equals("0")) {
                        break;
                    }
                }
                Thread.sleep(5000);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return result;
    }

    //饿百最多一次性更新100件商品
    public static final int STOCK_UPDATE_MAX = 100;

    /**
     * 饿百药品API：更新药品库存
     * @param source      饿百分配商家的source
     * @param secret      门店的secret
     * @param shopId      APP方门店id
     * @param custom_skuid_stocks 药品库存数据
     */
    public static String medicineStock(String source, String secret, String shopId, String custom_skuid_stocks) {
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_STOCK_UPDATE);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("shop_id", shopId);
        body.put("custom_sku_id", custom_skuid_stocks);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        String result="";
        if(EbFreqUtil.checkFreq(EbConfig.EB_CMD_STOCK_UPDATE)) {
            result = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
        }
        logger.info("饿百药品API：更新药品库存! 门店ID：" + shopId +"提交参数："+params+ " 执行结果：" + result);
        return result;
    }

    /**
     * @Description: 返回在线正常营业的门店编号
     * @Param: [source, secret]
     * @return: java.util.List<java.lang.String>
     * @Author: Qin.Qing
     * @Date: 2018/12/28
     */
    public static List<String> getshopList(String source, String secret){
        List<String> result=new ArrayList();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("cmd", EbConfig.EB_CMD_SHOP_LIST);
            params.put("version", EbConfig.EB_API_VERSION);
            params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
            params.put("ticket", UuidUtil.getUUID());
            params.put("source", source);
            params.put("secret", secret);
            params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示
            Map<String, Object> body = new HashMap<>();
            //body.put("sys_status", "6");
            params.put("body", JSON.toJSONString( body));
            String sign = SignUtil.getSign(params);
            params.put("sign", sign);
            params.remove("secret");
            String rev="";
            if(EbFreqUtil.checkFreq(EbConfig.EB_CMD_SHOP_LIST)) {
                rev = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
            }
            Map<String, Object> resultMap = (Map<String, Object>) JSONObject.parse(rev);
            if (String.valueOf(((Map)resultMap.get("body")).get("errno")).equals("0")) {
                if (((Map) resultMap.get("body")).get("data")!=null) {
                    List<Map> shoplist = (List<Map>) ((Map) resultMap.get("body")).get("data");
                    for(Map shopmap: shoplist){
                        result.add(String.valueOf(shopmap.get("shop_id")));
                    }
                }
            }
            logger.info("饿百药品API：查询门店清单信息!" + " 执行结果：返回结果条目" + result.size());
            return result;
        } catch (Exception e) {
            logger.error("饿百药品API：查询门店清单信息!" + " 执行异常：" + e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 获取门店对应商品清单
     * @Param: [source, secret, shop_no]
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2018/12/29
     */
    public static List<Map<String,Object>> getmedicineList(String source, String secret,String shop_no) {
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            int pageindex = 0;
            while (true) {
                pageindex++;
                Map<String, String> params = new HashMap<>();
                params.put("cmd", EbConfig.EB_CMD_SKU_LIST);
                params.put("version", EbConfig.EB_API_VERSION);
                params.put("timestamp", String.valueOf((int) (System.currentTimeMillis() / 1000)));//当前时间戳
                params.put("ticket", UuidUtil.getUUID());
                params.put("source", source);
                params.put("secret", secret);
                params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示
                Map<String, Object> body = new HashMap<>();
                body.put("shop_id", shop_no);
                body.put("page", pageindex);
                body.put("pagesize", 100);
                params.put("body", JSON.toJSONString(body));
                String sign = SignUtil.getSign(params);
                params.put("sign", sign);
                params.remove("secret");
                String rev="";
                if(EbFreqUtil.checkFreq(EbConfig.EB_CMD_SKU_LIST)) {
                    rev = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
                }
                Map<String, Object> resultMap = (Map<String, Object>) JSONObject.parse(rev);
                if (String.valueOf(((Map) resultMap.get("body")).get("errno")).equals("0")) {
                    if (((Map) resultMap.get("body")).get("data") != null) {
                        List<Map<String, Object>> medicinelist = (List<Map<String, Object>>) ((Map) ((Map) resultMap.get("body")).get("data")).get("list");
                        if (medicinelist.size() > 0) {
                            for (Map map : medicinelist) {
                                result.add(map);
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            logger.info("饿百药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行结果：返回结果条目" + result.size());
            return result;
        } catch (Exception e) {
            logger.error("饿百药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行异常：" + e.getMessage());
        }
        return result;
    }

    /**
     * 订单拣货完成
     */
    public static boolean pickComplete(String appId, String secret, String orderId) {
        String result = "";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_PICKCOMPLETE);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", appId);
        params.put("secret", secret);
        //如果encrypt没有，直接用""，不要用null表示
        params.put("encrypt", "");
        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
        logger.info("饿百订单API: 调用拣货完成，result:{}, orderNo:{}", result, orderId);
        return validApiResult(result);
    }

    /**
     * 获取订单配送信息
     * @param source
     * @param secret
     * @param orderId
     * @return
     * @Author: Li.Deman
     * @Date: 2019/1/8
     */
    public static Map<String,Object> getOrderDeliveryInfo(String source, String secret, String orderId){
        Map<String, Object> result = null;
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_DELIVERY_GET);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示
        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        int trytime=0;
        while(trytime<reTryGetOrderTime){
            trytime++;
            try {
                String rev = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
                logger.info("饿百订单API：获取订单配送信息! 订单ID：" + orderId + "，执行结果：" + rev);
                boolean ret = validApiResult(rev);
                if (ret) {
                    //解析rev，主要要的是data部分的信息
                    Map<String, Object> resultMap = (Map<String, Object>) JSONObject.parse(rev);
                    Map<String, Object> bodyMap = (Map<String, Object>) JSONObject.parse(String.valueOf(resultMap.get("body")));
                    Map<String, Object> dataMap = (Map<String, Object>) JSONObject.parse(String.valueOf(bodyMap.get("data")));
                    result=new HashMap<>();
                    //order_id:订单ID
                    result.put("order_id",String.valueOf(dataMap.get("order_id")));
                    //status:运单状态
                    result.put("status",String.valueOf(dataMap.get("status")));
                    //substatus:运单子状态
                    result.put("substatus",String.valueOf(dataMap.get("substatus")));
                    //name:配送员姓名
                    result.put("name",String.valueOf(dataMap.get("name")));
                    //phone:配送员手机号
                    result.put("phone",String.valueOf(dataMap.get("phone")));
                    //update_time:记录更新时间
                    result.put("update_time",String.valueOf(dataMap.get("update_time")));
                    break;
                }else{
                    Thread.sleep(1000);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 饿百订单API：同意用户退款申请
     * @param source
     * @param secret
     * @param orderId
     * @return
     * @Author: Li.Deman
     * @Date: 2019/1/9
     */
    public static String agreeRefund(String source, String secret, String orderId){
        String result="";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_AGREEREFUND);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        //如果encrypt没有，直接用""，不要用null表示
        params.put("encrypt", "");
        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result =HttpUtils.doPost(EbConfig.EB_API_SERVER,"",params);
        logger.info("饿百订单API：同意用户退款申请! 订单ID：" + orderId + " 执行结果：" + result);
        return result;
    }

    /**
     * 饿百订单API：拒绝用户退款申请
     * @param source
     * @param secret
     * @param orderId
     * @param refuse_reason
     * @return
     */
    public static String disagreeRefund(String source, String secret, String orderId,String refuse_reason){
        String result="";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_DISAGREEREFUND);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        //如果encrypt没有，直接用""，不要用null表示
        params.put("encrypt", "");
        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        body.put("refuse_reason", refuse_reason);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result =HttpUtils.doPost(EbConfig.EB_API_SERVER,"",params);
        logger.info("饿百订单API：同意用户退款申请! 订单ID：" + orderId + " 执行结果：" + result);
        return result;
    }

    /**
     * 饿百订单API：呼叫配送
     * @param source
     * @param secret
     * @param orderId
     * @return
     */
    public static String callDelivery(String source, String secret, String orderId){
        String result="";
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_ORDER_CALLDELIVERY);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        //如果encrypt没有，直接用""，不要用null表示
        params.put("encrypt", "");
        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("order_id", orderId);
        params.put("body", JSON.toJSONString( body));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        result =HttpUtils.doPost(EbConfig.EB_API_SERVER,"",params);
        logger.info("饿百订单API：呼叫配送! 订单ID：" + orderId + " 执行结果：" + result);
        return result;
    }


    /**
     * 饿百药品API：创建药品主数据
     * @param source      饿百分配商家的source
     * @param secret      门店的secret
     * @param shopId      APP方门店id
     * @param upc         药品国际编码
     * @param custom_sku_id 自定义商品编码
     * @param skuName     药品名称
     * @param category_id 药品分类
     * @param stock       库存数量
     * @param price       销售价格，单位：分
     */
    public static String medicine_Create(String source, String secret, String shopId, String upc,String custom_sku_id ,String skuName,String category_id,int stock,double price) {
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_SKU_CREATE);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("shop_id", shopId);
        body.put("upc", upc);
        body.put("name", skuName);
        body.put("status", 1);//商品状态 1上架，0下架
        body.put("category_id", category_id);
        body.put("left_num", stock);
        body.put("sale_price", price);
        body.put("market_price", price);
        body.put("custom_sku_id", custom_sku_id);

        params.put("body", JSON.toJSONString( body, SerializerFeature.BrowserCompatible));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        String result="";
        if(EbFreqUtil.checkFreq(EbConfig.EB_CMD_SKU_CREATE)) {
            result = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
        }
        logger.info("饿百药品API：创建药品! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }


    /**
     * 饿百药品API：更新药品主数据
     * @param source      饿百分配商家的source
     * @param secret      门店的secret
     * @param shopId      APP方门店id
     * @param upc         药品国际编码
     * @param custom_sku_id 自定义商品编码
     * @param skuName     药品名称
     * @param category_id 药品分类
     * @param stock       库存数量
     * @param price       销售价格，单位：分
     */
    public static String medicine_Update(String source, String secret, String shopId, String upc,String custom_sku_id ,String skuName,String category_id,int stock,double price) {
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_SKU_UPDATE);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf( (int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("shop_id", shopId);
        body.put("name", skuName);
        body.put("status", 1);//商品状态 1上架，0下架
        if (!StringUtils.isBlank(category_id)) {
            body.put("category_id", category_id);
        }
        body.put("left_num", stock);
        body.put("sale_price", price);
        body.put("market_price", price);
        body.put("custom_sku_id", custom_sku_id);

        params.put("body", JSON.toJSONString( body,SerializerFeature.BrowserCompatible));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        String result="";
        if(EbFreqUtil.checkFreq(EbConfig.EB_CMD_SKU_UPDATE)) {
            result = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
        }
        logger.info("饿百药品API：创建药品! 门店ID：" + shopId + " 执行结果：" + result);
        return result;
    }


    /**
     * @Description: 创建药品分类
     * @Param: [source, secret, shopId, name, rank]
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2019/04/16
     */
    public static String medicine_Category_Create(String source, String secret, String shopId, String name,int rank) {
        Map<String, String> params = new HashMap<>();
        params.put("cmd", EbConfig.EB_CMD_CATEGORY_CREATE);
        params.put("version", EbConfig.EB_API_VERSION);
        params.put("timestamp", String.valueOf((int) (System.currentTimeMillis() / 1000)));//当前时间戳
        params.put("ticket", UuidUtil.getUUID());
        params.put("source", source);
        params.put("secret", secret);
        params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示

        //在body中添加需要输入的参数
        Map<String, Object> body = new HashMap<>();
        body.put("shop_id", shopId);
        body.put("parent_category_id", 0);
        //body.put("name", JSON.toJSONString(name, SerializerFeature.BrowserCompatible));
        body.put("name", name);
        body.put("rank", rank);

        params.put("body", JSON.toJSONString(body,SerializerFeature.BrowserCompatible));
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        //body.put("name", name);
        params.remove("secret");
        String result = "";
        if (EbFreqUtil.checkFreq(EbConfig.EB_CMD_CATEGORY_CREATE)) {
            result = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
        }
        logger.info("饿百药品API：创建药品! 门店ID：" + shopId + " 执行结果：" + result);
        Map revmap = (Map) JSON.parse(result);
        Map revbody = (Map) revmap.get("body");
        if (String.valueOf(revbody.get("errno")).equals("0")) {
            result = "OK:" + String.valueOf(((Map<String, Object>) revbody.get("data")).get("category_id"));
        }
        return result;
    }


    /**
     * @Description: 获取药品自定义分类
     * @Param: [source, secret, shop_no]
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2019/04/16
     */
    public static List<Map<String,Object>> get_Medicine_Category_List(String source, String secret,String shop_no) {
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("cmd", EbConfig.EB_CMD_CATEGORY_GET);
            params.put("version", EbConfig.EB_API_VERSION);
            params.put("timestamp", String.valueOf((int) (System.currentTimeMillis() / 1000)));//当前时间戳
            params.put("ticket", UuidUtil.getUUID());
            params.put("source", source);
            params.put("secret", secret);
            params.put("encrypt", "");//如果encrypt没有，直接用""，不要用null表示
            Map<String, Object> body = new HashMap<>();
            body.put("shop_id", shop_no);
            params.put("body", JSON.toJSONString(body));
            String sign = SignUtil.getSign(params);
            params.put("sign", sign);
            params.remove("secret");
            String rev = "";
            if (EbFreqUtil.checkFreq(EbConfig.EB_CMD_CATEGORY_GET)) {
                rev = HttpUtils.doPost(EbConfig.EB_API_SERVER, "", params);
            }
            Map<String, Object> resultMap = JSON.parseObject(rev);
            if (String.valueOf(((Map) resultMap.get("body")).get("errno")).equals("0")) {
                if (((Map) resultMap.get("body")).get("data") != null) {
                    List<Map<String, Object>> categoryList = (List<Map<String, Object>>) ((Map) ((Map) resultMap.get("body")).get("data")).get("categorys");
                    if (categoryList.size() > 0) {
                        for (Map map : categoryList) {
                            result.add(map);
                        }
                    }
                }
            }
            logger.info("饿百药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行结果：返回结果条目" + result.size());
            return result;
        } catch (Exception e) {
            logger.error("饿百药品API：查询门店药品清单信息! 门店ID：" + shop_no + " 执行异常：" + e.getMessage());
        }
        return result;
    }


    /**
     * 接口返回参数校验
     * @param result
     * @return
     */
    private static boolean validApiResult(String result) {
        Map<String, Object> resultMap = (Map<String, Object>) JSONObject.parse(result);
        Map<String, Object> bodyMap = (Map<String, Object>) JSONObject.parse(String.valueOf(resultMap.get("body")));
        if ("success".equals(String.valueOf(bodyMap.get("error")))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 参数转换
     * @param result
     * @return BaseOrderInfo
     */
    public static BaseOrderInfo convertToBaseOrder(String result, String client) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        Map<String, Object> resultMap = (Map<String, Object>) JSONObject.parse(result);
        Map<String, Object> bodyMap = (Map<String, Object>) resultMap.get("body");
        Map<String, Object> dataMap = (Map<String, Object>) bodyMap.get("data");
        Map<String, Object> shopMap = (Map<String, Object>) dataMap.get("shop");
        Map<String, Object> orderMap = (Map<String, Object>) dataMap.get("order");
        Map<String, Object> userMap = (Map<String, Object>) dataMap.get("user");
        List<Map<String, Object>> prosMap = (List<Map<String, Object>>)((List<Map<String, Object>>)dataMap.get("products")).get(0);
        List<Map<String, Object>> discountMap=(List<Map<String, Object>>)dataMap.get("discount");
        try {
            BeanUtils.populate(baseOrderInfo, orderMap);
            List<BaseOrderProInfo> proList = new ArrayList<BaseOrderProInfo>();
            Map<String, Object> proMap = null;
            BaseOrderProInfo baseOrderProInfo = null;
            // 内部订单ID
            String orderId = String.valueOf(orderMap.get("order_id"));
            //is_prescription Integer	是否处方药订单，枚举值： 0 不是 ，1 是
            String prescriptionOrNot = String.valueOf(orderMap.get("is_prescription"));
            baseOrderInfo.setPrescriptionOrNot(prescriptionOrNot);
            // 门店编码(合作方门店ID)
            String storeCode = String.valueOf(shopMap.get("id"));
            baseOrderInfo.setShop_no(storeCode);
            /*			private String order_info; // 优惠信息*/
            baseOrderInfo.setRecipient_address(String.valueOf(userMap.get("address")));
            baseOrderInfo.setRecipient_name(String.valueOf(userMap.get("name")));
            baseOrderInfo.setRecipient_phone(String.valueOf(userMap.get("phone")));
            baseOrderInfo.setShipping_fee(centToDollarForString(orderMap.get("send_fee")));
            baseOrderInfo.setTotal_price(centToDollarForString(orderMap.get("shop_fee")));
            baseOrderInfo.setOriginal_price(centToDollarForString(orderMap.get("total_fee")));
            baseOrderInfo.setCustomer_pay(centToDollarForString(orderMap.get("user_fee")));
            baseOrderInfo.setCaution(String.valueOf(orderMap.get("remark")));
            // 是否开票（0：不需要发票1：需要发票）是否需要发票 1 是 2 否
            baseOrderInfo.setIs_invoiced("2".equals(String.valueOf(orderMap.get("need_invoice"))) ? "0" : "1");
            baseOrderInfo.setInvoice_title (String.valueOf(orderMap.get("invoice_title")));
            baseOrderInfo.setTaxpayer_id(String.valueOf(orderMap.get("taxer_id")));
            // 支付类型（0：在线支付1：货到付款）付款类型 1 下线 2 在线
            baseOrderInfo.setPay_type("2".equals(String.valueOf(orderMap.get("pay_type"))) ? "0" : "1");
            //骑士手机号
            baseOrderInfo.setShipper_phone(String.valueOf(orderMap.get("delivery_phone")));
            //预约订单
//            long send_time= Long.valueOf(String.valueOf(orderMap.get("send_time")));
//            if (send_time>10) {
//                baseOrderInfo.setDelivery_time(String.valueOf( send_time));
//            }else{
//                baseOrderInfo.setDelivery_time("0");
//            }
            String delivery_time="0";
            Long orderPurchaseTime=Long.valueOf( String.valueOf(orderMap.get("create_time")));
            Long orderPreStartDeliveryTime=Long.valueOf(String.valueOf(orderMap.get("send_time")));
            if ((orderPreStartDeliveryTime-orderPurchaseTime)>=(60*70)) {
                delivery_time = String.valueOf(orderPreStartDeliveryTime );
            }
            baseOrderInfo.setDelivery_time(delivery_time);

            //仁和的入机金额需要定制化
            if ("10000368".equals(client)) {
                BigDecimal totalIn = BigDecimal.ZERO;
                for (int i = 0; i < prosMap.size(); i++) {
                    proMap = prosMap.get(i);
                    String productFee = centToDollarForString(proMap.get("product_fee"));
                    if (StrUtil.isNotEmpty(productFee)) {
                        totalIn = totalIn.add(new BigDecimal(productFee));
                    }
                }
                //商户承担金额
                BigDecimal reduceFee = BigDecimal.ZERO;
                if (discountMap != null) {
                    for (Map<String, Object> tempMap : discountMap) {
                        String shopRate = centToDollarForString(tempMap.get("shop_rate"));
                        if (StrUtil.isNotEmpty(shopRate)) {
                            reduceFee = reduceFee.add(new BigDecimal(shopRate));
                        }
                    }
                }
                totalIn = totalIn.subtract(reduceFee);
                baseOrderInfo.setTotal_price(totalIn.toString());
            }

            //订单优惠信息order_info：data-discount
            //此订单优惠信息存储无意义，改存优惠总金额
//            String order_info="";
//            if (discountMap!=null) {
//                for (Map m : discountMap) {
//                    order_info = order_info + m.get("desc") + "；";
//                }
//            }

            if (discountMap != null) {
                int totalFee = 0;
                for (Map<String, Object> tempMap : discountMap) {
                    //单个商品优惠总金额，单位分
                    Integer fee = (Integer) tempMap.get("fee");
                    if (fee != null) {
                        totalFee += fee;
                    }
                }
                BigDecimal feePrice = new BigDecimal(0.01).multiply(new BigDecimal(totalFee));
                feePrice = feePrice.setScale(2, BigDecimal.ROUND_HALF_UP);
                baseOrderInfo.setOrder_info(feePrice.toString());
            } else {
                baseOrderInfo.setOrder_info("0");
            }

            //订单状态status：5、订单确认； 7、骑士已接单开始取餐（此时可通过订单详情接口获取骑士手机号); 8、骑士已取餐正在配送; 9、订单完成; 10、订单取消; 15、订单退款；
            int status=Integer.valueOf(String.valueOf(orderMap.get("status")));
            int newStatus=0;
            switch (status){
                case 5:
                    newStatus=1;
                    break;
                case 7:
                    newStatus=2;
                    break;
                case 8:
                    newStatus=2;
                    break;
                case 9:
                    newStatus=3;
                    break;
                case 10:
                    newStatus=4;
                    break;
                case 15:
                    newStatus=4;
                    break;
                default:
                    newStatus=1;
                    break;
            }
            baseOrderInfo.setStatus(String.valueOf( newStatus));
            baseOrderInfo.setDay_seq(String.valueOf(orderMap.get("order_index")));
            baseOrderInfo.setCreate_time(getDateStringFormTimestamp(Long.valueOf( String.valueOf(orderMap.get("create_time")))));
            // 获取订单明细并记录在途库存信息
            for (int i = 0; i < prosMap.size(); i++) {
                proMap = prosMap.get(i);
                baseOrderProInfo = new BaseOrderProInfo();
                BeanUtils.populate(baseOrderProInfo, proMap);
                baseOrderProInfo.setOrder_id(orderId);
                baseOrderProInfo.setMedicine_code(String.valueOf(proMap.get("custom_sku_id")));
                Object productName = proMap.get("product_name");
                baseOrderProInfo.setPlatformProductName(productName == null ? null : String.valueOf(productName));
                baseOrderProInfo.setQuantity(String.valueOf(proMap.get("product_amount")));
                baseOrderProInfo.setBox_num(String.valueOf(proMap.get("package_amount")));
                baseOrderProInfo.setBox_price(centToDollarForString(proMap.get("package_price")));
                baseOrderProInfo.setUnit("");
                baseOrderProInfo.setDiscount("1");
                baseOrderProInfo.setSequence(String.valueOf(i+1));
                baseOrderProInfo.setShop_no(storeCode);
                baseOrderProInfo.setStock_lock(baseOrderProInfo.getQuantity());
                baseOrderProInfo.setPrice(centToDollarForString(proMap.get("product_price")));
                proList.add(baseOrderProInfo);
            }
            baseOrderInfo.setItems(proList);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return baseOrderInfo;
    }

    /**
     * 金额转换方法：分转换成元
     * @param t
     * @return
     */
    private static String centToDollarForString(Object t){
        if (t == null) {
            return "0";
        } else {
            BigDecimal amount = getBigDecimal(t);
            amount = amount.divide(new BigDecimal(100));
            return amount.toString();
        }
    }

    /**
     *
     * @param t
     * @return
     */
    private static BigDecimal getBigDecimal(Object t) {
        BigDecimal amount;
        if(t instanceof Integer){
            amount = new BigDecimal(t.toString());
        }
        else if(t instanceof Long){
            amount = new BigDecimal(t.toString());
        }
        else if(t instanceof String){
            amount=new BigDecimal(t.toString());
        }
        else{
            throw new RuntimeException(String.format("不支持的数据类型,%s",t.getClass()));
        }
        return amount;
    }

    /**
     *  饿百推送API-返回参数封装
     * @param source
     * @param secret
     * @param cmd
     * @return
     */
    public static String apiReturnMsg(String source,String secret, String cmd,String ticket, Map<String, Object> paramBody) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("body", JSON.toJSONString( paramBody));
        result.put("cmd", "resp." + cmd);
        result.put("source",source);
        result.put("ticket",ticket);
        result.put("timestamp",String.valueOf((int) (System.currentTimeMillis() / 1000)));
        result.put("version",EbConfig.EB_API_VERSION);
        String sign = SignUtil.getSign(result, source,secret);
        result.put("sign", sign);
        result.put("body", paramBody);
        return JSON.toJSONString( result);
    }

    /**
     *
     * @param timestamp
     * @return
     */
    public static String getDateStringFormTimestamp(Long timestamp){
        return new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(timestamp*1000));
    }
}
