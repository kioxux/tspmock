package com.gys.util.takeAway.jgg;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/10/26 13:44
 */
@Data
public class JggReturnOrderItem {

    private String proId;//商品编码
    private String returnQty;//退货数量
}
