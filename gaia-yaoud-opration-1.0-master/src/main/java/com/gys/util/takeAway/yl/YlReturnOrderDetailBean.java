package com.gys.util.takeAway.yl;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/8 10:22
 */
@Data
public class YlReturnOrderDetailBean {

    private String id;//药联订单号
    private String create_time;//药联退单时间
    private String store_id;// 门店内码
    private String assistant_mobile;//店员手机号
    private String assistant_number;//店员工号
}
