package com.gys.util.takeAway.ky;

import org.springframework.util.DigestUtils;

import java.util.Base64;

/**
 * 快药-签名工具类
 * @author zhangdong
 * @date 2021/5/7 14:44
 */
public class SignUtil {

    /**
     * 快药签名校验
     */
    public static boolean kYcheckSign(String appId, String secret, String timestamp, String version, String signature) {
        String result = getKySign(appId, secret, timestamp, version);
        return result.equals(signature);
    }

    /**
     * 快药签名获取
     */
    public static String getKySign(String appId, String secret, String timestamp, String version) {
        String temp = appId + secret + timestamp + version;
        String md5 = DigestUtils.md5DigestAsHex(temp.getBytes());
        Base64.Encoder encoder = Base64.getEncoder();
        String result = encoder.encodeToString(md5.getBytes());
        return result;
    }
}
