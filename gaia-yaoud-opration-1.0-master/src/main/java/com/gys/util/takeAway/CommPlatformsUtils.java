package com.gys.util.takeAway;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.common.data.yaolian.YlStockData;
import com.gys.common.data.yaolian.YlStockInputBean;
import com.gys.util.UtilConst;
import com.gys.util.takeAway.ddky.DdkyApiUtil;
import com.gys.util.takeAway.ddky.DdkyStockUpdateBean;
import com.gys.util.takeAway.ddky.DdkyStockUpdateDetailBean;
import com.gys.util.takeAway.eb.EbApiUtil;
import com.gys.util.takeAway.jd.JdApiUtil;
import com.gys.util.takeAway.ky.KyApiUtil;
import com.gys.util.takeAway.mt.MtApiUtil;
import com.gys.util.takeAway.mt.MtMedicineData;
import com.gys.util.takeAway.yl.YlApiUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommPlatformsUtils {
    private static final Logger logger = LoggerFactory.getLogger(CommPlatformsUtils.class);

    /**
     * @Description: 设订单为商家已收到
     * @Param: [orderId]订单号
     * @return: boolean
     * @Author: Qin.Qing
     * @Date: 2018/12/25
     */
    public static String orderReceived(String orderId) {
        String result = "";
        try {

        } catch (Exception e) {
            logger.error("商家收到订单! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result="ERROR:"+e.getMessage();
        }
        return result;
    }

    /**
     * @Description: 商家确认订单
     * @Param: orderId] 订单号
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2018/12/26
     */
    public static String orderConfirm(String platform,String shop_no,String orderId,Map<String,String> stoData) {
        String result="";
        try {
            platform=platform.toUpperCase();
            String rev="";
            switch (platform){
                case "EB":
                    rev = EbApiUtil.orderConfirm(stoData.get("source"), stoData.get("secret"), orderId);
                    Map revmap = (Map) JSON.parse(rev);
                    Map revbody = (Map) revmap.get("body");
                    if (String.valueOf(revbody.get("errno")).equals("0")) {
                        result ="ok";
                    }else {
                        result ="ERROR:" + revbody.get("error") + "\n";
                    }
                    break;
                default:
                    logger.error("商家确认订单! 平台订单ID：" + orderId + " 执行异常：订单平台为空!" );
                    result="商家确认订单! 平台订单ID：" + orderId + " 订单平台为空!";
                    break;
            }
        } catch (Exception e) {
            logger.error("商家确认订单! 平台订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result="ERROR"+e.getMessage();
        }
        return result;
    }

    /**
     * 商家取消订单
     * @param platform    平台
     * @param shop_no    门店
     * @param orderId    平台订单ID
     * @param reason     取消原因
     * @param reasonCode 规范化取消原因code
     */
    public static String orderCancel(String platform, String shop_no, String orderId, String reason, String reasonCode,Map<String,String> stoData) {
        String result="";
        try {
            platform=platform.toUpperCase();
            String rev="";
            switch (platform){
                case "EB":
                    rev = EbApiUtil.orderCancel(stoData.get("source"),
                            stoData.get("secret"),
                            orderId, reasonCode, reason);
                    Map revmap = (Map) JSON.parse(rev);
                    Map revbody = (Map) revmap.get("body");
                    if (String.valueOf(revbody.get("errno")).equals("0")) {
                        result ="ok";
                    }else {
                        result ="ERROR:" +String.valueOf(revbody.get("error")) + "\n";
                    }
                    break;
                default:
                    logger.error("商家取消订单! 平台订单ID：" + orderId + " 执行异常：订单平台为空!" );
                    result="商家取消订单! 平台订单ID：" + orderId + " 订单平台为空!";
                    break;
            }
        } catch (Exception e) {
            logger.error("商家取消订单! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result="ERROR"+e.getMessage();
        }
        return result;
    }

    /**
     * 订单确认退款请求
     * @param platform   平台
     * @param shop_no    门店
     * @param orderId   平台订单ID
     * @param reason    确认退款详情
     * @param afsServiceOrder    售后单号
     */
    public static String orderRefundAgree(String platform,String shop_no, String orderId, String reason,String afsServiceOrder,Map<String,String> stoData) {
        String result="";
        try {
            platform=platform.toUpperCase();
            String rev="";
            if (StringUtils.isBlank(reason)){
                reason="门店确认可以退款！";
            }
            switch (platform){
                case "EB":
                    rev = EbApiUtil.agreeRefund(stoData.get("source"),
                            stoData.get("secret"),
                            orderId);
                    Map revmap = (Map) JSON.parse(rev);
                    Map revbody = (Map) revmap.get("body");
                    if (String.valueOf(revbody.get("errno")).equals("0")) {
                        result ="ok";
                    }else {
                        result ="ERROR:" +String.valueOf(revbody.get("error")) + "\n";
                    }
                    break;
                default:
                    logger.error("订单确认退款请求! 平台订单ID：" + orderId + " 执行异常：订单平台为空!" );
                    result="订单确认退款请求! 平台订单ID：" + orderId + " 订单平台为空!";
                    break;
            }
        } catch (Exception e) {
            logger.error("订单确认退款请求! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result="ERROR"+e.getMessage();
        }
        return result;
    }

    /**
     * 驳回订单退款申请
     * @param platform   平台
     * @param shop_no   门店
     * @param orderId   平台订单ID
     * @param reason    取消原因
     * @param afsServiceOrder    售后单号
     */
    public static String orderRefundReject(String platform,String shop_no,String orderId, String reason,String afsServiceOrder,Map<String,String> stoData) {
        String result="";
        try {
            platform=platform.toUpperCase();
            String rev="";
            if (StringUtils.isBlank(reason)){
                reason="门店确认不可以退款！";
            }
            switch (platform){
                case "EB":
                    rev = EbApiUtil.disagreeRefund(stoData.get("source"),
                            stoData.get("secret"),
                            orderId, reason);
                    Map revmap = (Map) JSON.parse(rev);
                    Map revbody = (Map) revmap.get("body");
                    if (String.valueOf(revbody.get("errno")).equals("0")) {
                        result ="ok";
                    }else {
                        result ="ERROR:" +String.valueOf(revbody.get("error")) + "\n";
                    }
                    break;
                default:
                    logger.error("驳回订单退款申请! 平台订单ID：" + orderId + " 执行异常：订单平台为空!" );
                    result="驳回订单退款申请! 平台订单ID：" + orderId + " 订单平台为空!";
                    break;
            }
        } catch (Exception e) {
            logger.error("驳回订单退款申请! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result="ERROR"+e.getMessage();
        }
        return result;
    }

    /**
     * 更新药品库存
     * @param shop_no    APP方门店id
     * @param medicine_data 药品库存数据
     * @param accessToken  MT、JD使用参数
     * @param cooperation  组织编码，药联用到
     */
    public static String medicineStock(String shop_no, List<Map<String, Object>> medicine_data, Map<String,String> stoData,
                                       String accessToken, GaiaTAccount account, String cooperation) {
        String result="";
        try {
            //当一家门店对接多个外卖平台时就会有问题
//            for(String platforms: SwitchConfig.platformsList) {
//                if (!SwitchConfig.checkStockSwitch(shop_no, platforms)) {
//                    logger.info("外卖平台:{}, 门店：[" + shop_no + "]库存更新开关未打开！", platforms);
//                    continue;
//                }
            String platforms = stoData.get("platform");
                String rev = "";
                List<Map<String, Object>> checkList=new ArrayList<>();
                for (Map<String, Object> m : medicine_data) {
                    if (!StringUtils.isBlank(nullToEmpty(m.get("stock")))) {
                        int itemCount = new BigDecimal(String.valueOf(m.get("stock"))).toBigInteger().intValue();
                        //int itemCount = Integer.valueOf(String.valueOf(m.get("stock")));
                        if (itemCount < 0) {
                            m.put("stock", "0");
                        }
                        checkList.add(m);
                    }
                }
                logger.info("更新库存，[门店:"+shop_no+"]输入产品数量："+String.valueOf( medicine_data.size())+"输入产品数量："+String.valueOf( checkList.size()));
                medicine_data=checkList;
                switch (platforms) {
                    case Constants.PLATFORMS_EB:
                        StringBuilder upcsb = new StringBuilder();
                        for (Map<String, Object> m : medicine_data) {
                            //饿百库存可以传小数，只是它后台会做向下取整操作
                            upcsb.append(String.valueOf(m.get("app_medicine_code"))).append(":").append(String.valueOf(m.get("stock"))).append(";");
                        }
                        String streb = upcsb.toString();
                        if (!streb.equals("")) {
                            streb = streb.substring(0, streb.length() - 1);
                        }
                        rev = EbApiUtil.medicineStock(stoData.get("source"), stoData.get("secret"), shop_no, streb);
                        Map revmap = (Map) JSON.parse(rev);
                        Map revbody = (Map) revmap.get("body");
                        if (!String.valueOf(revbody.get("errno")).equals("0")) {
                            result += String.valueOf(revbody.get("error")) + "\n";
                        }
                        break;
                    case Constants.PLATFORMS_KY:
                        //快药目前只支持单条商品更新库存
                        List<String> kyResult = Lists.newArrayList();
                        logger.info("快药外卖开始同步库存, medicineData:{}", medicine_data);
                        for (Map<String, Object> m : medicine_data) {
                            String response = KyApiUtil.stockUpdate(stoData.get("source"), stoData.get("secret"), shop_no,
                                    String.valueOf(m.get("app_medicine_code")), String.valueOf(m.get("stock")));
                            Map revMap = (Map) JSON.parse(response);
                            boolean success = "0".equals(String.valueOf(revMap.get("code"))) && "200".equals(String.valueOf(revMap.get("status")));
                            if (!success) {
                                kyResult.add(String.valueOf(revMap.get("message")));
                            }
                        }
                        if (CollUtil.isNotEmpty(kyResult)) {
                            kyResult = kyResult.stream().distinct().collect(Collectors.toList());
                            result += Joiner.on(";").join(kyResult);
                        }
                        break;
                    case Constants.PLATFORMS_MT:
                        //美团闪购一次最多更新200条
                        List<String> mtResult = Lists.newArrayList();
                        int tempSize = medicine_data.size();
                        if (tempSize > MtApiUtil.STOCK_UPDATE_MAX_COUNT) {
                            int total = tempSize / MtApiUtil.STOCK_UPDATE_MAX_COUNT;
                            int left = tempSize % MtApiUtil.STOCK_UPDATE_MAX_COUNT;
                            if (left > 0) {
                                total++;
                            }
                            for (int i = 0; i < total; i++) {
                                List<Map<String, Object>> tempList;
                                if (i == total - 1 && left > 0) {
                                    tempList = medicine_data.subList(MtApiUtil.STOCK_UPDATE_MAX_COUNT * i, MtApiUtil.STOCK_UPDATE_MAX_COUNT * i + left);
                                } else {
                                    tempList = medicine_data.subList(MtApiUtil.STOCK_UPDATE_MAX_COUNT * i, MtApiUtil.STOCK_UPDATE_MAX_COUNT * (i + 1));
                                }
                                mtStockSyncDetail(tempList, shop_no, stoData.get("source"), stoData.get("secret"), mtResult, accessToken);
                            }
                        } else {
                            mtStockSyncDetail(medicine_data, shop_no, stoData.get("source"), stoData.get("secret"), mtResult, accessToken);
                        }
                        if (CollUtil.isNotEmpty(mtResult)) {
                            mtResult = mtResult.stream().distinct().collect(Collectors.toList());
                            result += Joiner.on(";").join(mtResult);
                        }
                        break;
                    case Constants.PLATFORMS_JD:
                        //京东到家一次最多更新50条
                        List<String> jdResult = Lists.newArrayList();
                        int jdTempSize = medicine_data.size();
                        if (jdTempSize > JdApiUtil.STOCK_UPDATE_MAX_COUNT) {
                            int total = jdTempSize / JdApiUtil.STOCK_UPDATE_MAX_COUNT;
                            int left = jdTempSize % JdApiUtil.STOCK_UPDATE_MAX_COUNT;
                            if (left > 0) {
                                total++;
                            }
                            for (int i = 0; i < total; i++) {
                                List<Map<String, Object>> tempList;
                                if (i == total - 1 && left > 0) {
                                    tempList = medicine_data.subList(JdApiUtil.STOCK_UPDATE_MAX_COUNT * i, JdApiUtil.STOCK_UPDATE_MAX_COUNT * i + left);
                                } else {
                                    tempList = medicine_data.subList(JdApiUtil.STOCK_UPDATE_MAX_COUNT * i, JdApiUtil.STOCK_UPDATE_MAX_COUNT * (i + 1));
                                }
                                jdStockSyncDetail(tempList, shop_no, stoData.get("source"), stoData.get("secret"), jdResult, accessToken);
                            }
                        } else {
                            jdStockSyncDetail(medicine_data, shop_no, stoData.get("source"), stoData.get("secret"), jdResult, accessToken);
                        }
                        if (CollUtil.isNotEmpty(jdResult)) {
                            jdResult = jdResult.stream().distinct().collect(Collectors.toList());
                            result += Joiner.on(";").join(jdResult);
                        }
                        break;
                    case Constants.PLATFORMS_YL:
                        String storeId = account.getClient() + UtilConst.GYS + account.getStoCode();
                        YlStockInputBean bean = new YlStockInputBean();
                        List<YlStockData> stocks = new ArrayList<>(medicine_data.size());
                        for (Map<String, Object> tempMap : medicine_data) {
                            YlStockData stockData = new YlStockData();
                            stockData.setDrug_id(String.valueOf(tempMap.get("app_medicine_code")));
                            stockData.setQuantity(String.valueOf(tempMap.get("stock")));
                            stockData.setStore_id(storeId);
                            stocks.add(stockData);
                        }
                        bean.setStocks(stocks);
                        boolean success = YlApiUtil.stockUpdate(cooperation, bean, true);
                        if (!success) {
                            result += "药联同步失败";
                        }
                        break;
                    case Constants.PLATFORMS_DDKY:
                        String appSecret = account.getAppSecret();
                        String companyId = account.getCompanyId();
                        DdkyStockUpdateBean ddkyStockUpdateBean = new DdkyStockUpdateBean();
                        ddkyStockUpdateBean.setShopId(Long.valueOf(account.getPublicCode()));
                        List<DdkyStockUpdateDetailBean> goodsList = new ArrayList<>();
                        for (Map<String, Object> tempMap : medicine_data) {
                            DdkyStockUpdateDetailBean detailBean = new DdkyStockUpdateDetailBean();
                            detailBean.setGoodsId(Long.valueOf(String.valueOf(tempMap.get("app_medicine_code"))));
                            detailBean.setInvQty(Long.valueOf(String.valueOf(tempMap.get("stock"))));
                            goodsList.add(detailBean);
                        }
                        ddkyStockUpdateBean.setGoodsList(goodsList);
                        boolean ddkySuccess = DdkyApiUtil.stockUpdate(companyId, appSecret, ddkyStockUpdateBean);
                        if (!ddkySuccess) {
                            result += "订单快药同步失败";
                        }
                        break;
                }
//            }
        } catch (Exception e) {
            logger.error("更新药品库存! 门店ID：" + shop_no + " 执行异常：" + e.getMessage());
            result="ERROR"+e.getMessage();
        }
        if (result.equals("")) result="ok";
        return result;
    }

    private static void jdStockSyncDetail(List<Map<String, Object>> tempList, String shop_no, String source, String secret, List<String> jdResult, String accessToken) {
        Map<String, Object> paramMap = getJdStockBean(tempList, shop_no);
        if (paramMap == null) {
            return;
        }
        String rev = JdApiUtil.medicineStock(source, secret, accessToken, shop_no, paramMap);
        Map revmap = (Map) JSON.parse(rev);
        if (!String.valueOf(revmap.get("code")).equals("0")) {
            jdResult.add(String.valueOf(revmap.get("msg")));
        }
    }

    private static Map<String, Object> getJdStockBean(List<Map<String, Object>> tempList, String shopNo) {
        if (CollUtil.isEmpty(tempList)) {
            return null;
        }
        Map<String, Object> inputparam = new HashMap<>();
        inputparam.put("outStationNo", shopNo);
        inputparam.put("userPin", "service");
        List<Map<String, Object>> medicineInfoList = new ArrayList<>();
        for (Map<String, Object> map : tempList) {
            Map<String, Object> medicineInfo = new HashMap<>();
            medicineInfo.put("outSkuId", String.valueOf(map.get("app_medicine_code")));
            medicineInfo.put("stockQty", String.valueOf(map.get("stock")));
            medicineInfoList.add(medicineInfo);
        }
        inputparam.put("skuStockList", medicineInfoList);
        return inputparam;
    }

    private static void mtStockSyncDetail(List<Map<String, Object>> tempList, String shop_no, String source, String secret, List<String> mtResult, String accessToken) {
        List<MtMedicineData> list = new ArrayList<>(tempList.size());
        for (Map<String, Object> map : tempList) {
            list.add(new MtMedicineData(String.valueOf(map.get("app_medicine_code")), shop_no, String.valueOf(map.get("stock"))));
        }
        if (CollUtil.isNotEmpty(list)) {
            String rev = MtApiUtil.medicineStock(source, secret, shop_no, list, accessToken);
            if (!"ok".equalsIgnoreCase(rev)) {
                mtResult.add(rev);
            }
        }
    }

    /**
     * 下发商家确认订单，准备到配送阶段了
     * @param platform   平台
     * @param shop_no   门店
     * @param orderId   平台订单ID
     * @Author: Li.Deman
     * @Date: 2018/11/23
     */
    public static String orderLogisticsPush(String platform,String shop_no,String orderId,Map<String,String> stoData) {
        String result="";
        try {
            platform=platform.toUpperCase();
            String rev="";
            switch (platform){
                case "EB":
                    rev = EbApiUtil.callDelivery(stoData.get("source"),
                            stoData.get("secret"),
                            orderId);
                    Map revmap = (Map) JSON.parse(rev);
                    Map revbody = (Map) revmap.get("body");
                    if (String.valueOf(revbody.get("errno")).equals("0")) {
                        result ="ok";
                    }else {
                        result ="ERROR:" +String.valueOf(revbody.get("error")) + "\n";
                    }
                    break;
                default:
                    logger.error("下发配送订单! 平台订单ID：" + orderId + " 执行异常：订单平台为空!" );
                    result="下发配送订单! 平台订单ID：" + orderId + " 订单平台为空!";
                    break;
            }
        } catch (Exception e) {
            logger.error("下发配送订单! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result = "ERROR" + e.getMessage();
        }
        return result;
    }

    /**
     * @description：商家确认已完成出餐
     * @param orderId
     * @author：Li.Deamn
     * @date：2018/11/30
     */
    public static String preparationMealComplete(String orderId){
        String result="";
        try {

        }catch (Exception e){
            logger.error("商家确认已完成出餐! 订单ID：" + orderId + " 执行异常：" + e.getMessage());
            result = "ERROR" + e.getMessage();
        }
        return result;
    }

    /**
     * 门店营业和休息2种状态更新
     * @param shop_no
     * @param open_level
     * @return
     * @author：Li.Deman
     * @date：2018/12/25
     */
    public static String shopOpenUpdate(String shop_no, String open_level) {
        String result = "";
        try {
            if (open_level.equals("0")) { //闭店

            } else {//开店


            }
        } catch (Exception e) {
            logger.error("门店设置状态! 门店ID：" + shop_no + " 执行异常：" + e.getMessage());
            result = "ERROR" + e.getMessage();
        }
        return result;
    }

    /**
     * Convert Unix timestamp to normal date style
     * @param timestampString
     * @return
     * @author：Li.Deman
     * @date：2018/12/6
     */
    public static String TimeStamp2Date(String timestampString){
        Long timestamp = Long.parseLong(timestampString)*1000;
        String date = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(timestamp));
        return date;
    }

    public static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }
    }
}
