package com.gys.util.takeAway.jgg;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/10/26 13:42
 */
@Data
public class JggReturnOrder {

    @NotEmpty(message = "平台订单号不能为空")
    private String platformOrderId;//平台订单号
    @NotNull(message = "无退单商品详情")
    private List<JggReturnOrderItem> returnItems;

}
