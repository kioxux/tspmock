package com.gys.util.takeAway.pts;

import com.alibaba.fastjson.JSON;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.HttpUtils;
import com.gys.util.takeAway.XbServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PtsApiUtil {
    private static String PtsUrl = "http://172.19.1.93:8090/api/admin/interface/xb.json";
    private static final Logger logger = LoggerFactory.getLogger(PtsApiUtil.class);

    /**
     * @Description: 推送新订单给PTS
     * @Param: [order_info]
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2018/11/30
     */
    public static String pushNewOrder(Map<String, Object> order_info) {
        String result = "ERROR";
        try {
            PtsOrderInfo ptsOrderInfo = new PtsOrderInfo();
            ArrayList<PtsMedicineInfo> ptsMedicineInfos = new ArrayList<>();
            //todo 根据map信息，填充ptsorderinfo--已完成，待验证。
            ptsOrderInfo.setCaution(nullToEmpty(order_info.get("caution")));
            ptsOrderInfo.setCreate_time(nullToEmpty(order_info.get("create_time")));
            ptsOrderInfo.setCustomer_pay(nullToEmpty(order_info.get("customer_pay")));
            ptsOrderInfo.setDelivery_time(nullToEmpty(order_info.get("delivery_time")));
            ptsOrderInfo.setInvoice_title(nullToEmpty(order_info.get("invoice_title")));
            ptsOrderInfo.setIs_invoiced(nullToEmpty(order_info.get("is_invoiced")));
            ptsOrderInfo.setOrder_id(nullToEmpty(order_info.get("order_id")));
            ptsOrderInfo.setOrder_info(nullToEmpty(order_info.get("order_info")));
            ptsOrderInfo.setOriginal_price(nullToEmpty(order_info.get("original_price")));
            ptsOrderInfo.setPay_type(nullToEmpty(order_info.get("pay_type")));
            ptsOrderInfo.setPlatforms(nullToEmpty(order_info.get("platforms")));
            ptsOrderInfo.setPlatforms_order_id(nullToEmpty(order_info.get("platforms_order_id")));
            ptsOrderInfo.setRecipient_address(trimUnSupportChar(nullToEmpty(order_info.get("recipient_address"))));
            ptsOrderInfo.setRecipient_name(nullToEmpty(order_info.get("recipient_name")));
            ptsOrderInfo.setRecipient_phone(nullToEmpty(order_info.get("recipient_phone")));
            ptsOrderInfo.setShipping_fee(nullToEmpty(order_info.get("shipping_fee")));
            ptsOrderInfo.setShop_no(nullToEmpty(order_info.get("shop_no")));
//            //先以S999测试
//            ptsOrderInfo.setShop_no("S999");
            ptsOrderInfo.setStatus(nullToEmpty(order_info.get("status")));
            ptsOrderInfo.setTaxpayer_id(nullToEmpty(order_info.get("taxpayer_id")));
            ptsOrderInfo.setTotal_price(nullToEmpty(order_info.get("total_price")));
            ptsOrderInfo.setUpdate_time(nullToEmpty(order_info.get("update_time")));
            ptsOrderInfo.setDay_seq(nullToEmpty(order_info.get("day_seq")));
            ptsOrderInfo.setItems(ptsMedicineInfos);
            ptsOrderInfo.setAction(filterOffUtf8Mb4(nullToEmpty(order_info.get("action"))));
            ptsOrderInfo.setNotice_message("new");
            List<Map> maps = (List<Map>) order_info.get("detail");
            if (maps != null) {
                for (Map m : maps) {
                    PtsMedicineInfo ptsMedicineInfo = new PtsMedicineInfo();
                    ptsMedicineInfo.setBox_num(nullToEmpty(m.get("box_num")));
                    ptsMedicineInfo.setBox_price(nullToEmpty(m.get("box_price")));
                    ptsMedicineInfo.setDiscount(nullToEmpty(m.get("discount")));
                    ptsMedicineInfo.setMedicine_code(nullToEmpty(m.get("medicine_code")));
                    ptsMedicineInfo.setQuantity(nullToEmpty(m.get("quantity")));
                    ptsMedicineInfo.setSequence(nullToEmpty(m.get("sequence")));
                    ptsMedicineInfo.setUnit(nullToEmpty(m.get("unit")));
                    ptsMedicineInfo.setPrice(nullToEmpty(m.get("price")));
                    ptsMedicineInfos.add(ptsMedicineInfo);
                }
            }
//            String jsonstr = JSON.toJSONString(ptsOrderInfo);
//            String param = "{\"TXCODE\":\"00050001\",\"DATA\":[" + jsonstr + "]}";
//            result = HttpUtil.sendSimplePostRequest(PtsUrl, param);
            List<PtsOrderInfo> data = new ArrayList<>();
            Map<String, Object> pushMap = new HashMap<>();
            data.add(ptsOrderInfo);
            pushMap.put("TXCODE", "00050001");
            pushMap.put("DATA", data);
            result = HttpUtils.doPost(PtsUrl, "", JSON.toJSONString(pushMap));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            result = "{\"RETURNCODE\":\"F\",\"MESSAGE\":\"" + ex.getMessage() + "\",\"DATA\":[]}";
        }
        return result;
    }

    /**
     * @Description: 推送新订单状态给PTS
     * @Param: [order_info]
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2018/11/30
     */
    public static String pushOrderStatusChange(Map<String, Object> order_info) {
        String result = "ERROR";
        try {
            PtsOrderStatus ptsOrderStatus = new PtsOrderStatus();
            //todo 根据map信息，ptsOrderStatus--已完成，待测试
            ptsOrderStatus.setOrder_id(nullToEmpty(order_info.get("order_id")));
            ptsOrderStatus.setReason(nullToEmpty(order_info.get("reason")));
            ptsOrderStatus.setReason_code(nullToEmpty(order_info.get("reason_code")));
            ptsOrderStatus.setShipper_name(nullToEmpty(order_info.get("shipper_name")));
            ptsOrderStatus.setShipper_phone(nullToEmpty(order_info.get("shipper_phone")));
            ptsOrderStatus.setShop_no(nullToEmpty(order_info.get("shop_no")));
            ptsOrderStatus.setStatus(nullToEmpty(order_info.get("status")));
            ptsOrderStatus.setAction(nullToEmpty(order_info.get("action")));
            ptsOrderStatus.setNotice_message(nullToEmpty(order_info.get("notice_message")));
//            String jsonstr = JSON.toJSONString(ptsOrderStatus);
//            String param = "{\"TXCODE\":\"00050002\",\"DATA\":[" + jsonstr + "]}";
//            result = HttpUtil.sendSimplePostRequest(PtsUrl, param);
            List<PtsOrderStatus> data = new ArrayList<>();
            Map<String, Object> pushMap = new HashMap<>();
            data.add(ptsOrderStatus);
            pushMap.put("TXCODE", "00050002");
            pushMap.put("DATA", data);
            result = HttpUtils.doPost(PtsUrl, "", JSON.toJSONString(pushMap));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            result = "{\"RETURNCODE\":\"F\",\"MESSAGE\":\"" + ex.getMessage() + "\",\"DATA\":[]}";
        }
        return result;
    }

    public static String pushDeliveryOrder(Map<String, Object> order_info){
        String result = "ERROR";
        try {
            String order_id=nullToEmpty(order_info.get("order_id"));
            Map<String,Object> selectMap=new HashMap<>();
            selectMap.put("platforms_order_id", "");
            selectMap.put("order_id", order_id);
            selectMap.put("platforms", "");
            Map<String, Object> retMap = XbServiceUtil.service.selectOrderInfoByMap(selectMap);
            order_info.put("reason",nullToEmpty(retMap.get("reason")));
            order_info.put("reason_code",nullToEmpty(retMap.get("reason_code")));
            order_info.put("shipper_name",nullToEmpty(retMap.get("shipper_name")));
            order_info.put("shipper_phone",nullToEmpty(retMap.get("shipper_phone")));
            order_info.put("status",nullToEmpty(retMap.get("status")));
            if (("1".equals(nullToEmpty(retMap.get("status"))))||("2".equals(nullToEmpty(retMap.get("status"))))) {
                result=pushOrderStatusChange(order_info);
            }else{
                result="OK:订单状态不为1,2，已不需要推送！";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            result = "{\"RETURNCODE\":\"F\",\"MESSAGE\":\"" + ex.getMessage() + "\",\"DATA\":[]}";
        }
        return result;
    }

    /**
     * @param order_info
     * @return
     * @Description: 推送退款信息给PTS
     * 支持部分退款和全额退款
     * @Author: Li.Deman
     * @Date: 2018/12/26
     */
    public static String pushRefundOrder(Map<String, Object> order_info) {
        String result;
        try {
            PtsOrderInfo ptsOrderInfo = new PtsOrderInfo();
            ArrayList<PtsMedicineInfo> ptsMedicineInfos = new ArrayList<>();
            ptsOrderInfo.setOrder_id(nullToEmpty(order_info.get("order_id")));
            ptsOrderInfo.setPlatforms_order_id(nullToEmpty(order_info.get("platforms_order_id")));
            ptsOrderInfo.setShop_no(nullToEmpty(order_info.get("shop_no")));
            ptsOrderInfo.setReason(nullToEmpty(order_info.get("reason")));
            ptsOrderInfo.setRefund_money(nullToEmpty(order_info.get("refund_money")));
            ptsOrderInfo.setAction(nullToEmpty(order_info.get("action")));
            ptsOrderInfo.setStatus(nullToEmpty(order_info.get("status")));
            ptsOrderInfo.setNotice_message("refund");
            if (order_info.get("detail") != null) {
                if (order_info.get("detail") instanceof List) {
                    List<Map> maps = (List<Map>) order_info.get("detail");
                    if (maps != null) {
                        for (Map m : maps) {
                            PtsMedicineInfo ptsMedicineInfo = new PtsMedicineInfo();
                            ptsMedicineInfo.setOrder_id(nullToEmpty(m.get("order_id")));
                            ptsMedicineInfo.setMedicine_code(nullToEmpty(m.get("medicine_code")));
                            ptsMedicineInfo.setRefund_qty(nullToEmpty(m.get("refund_qty")));
                            ptsMedicineInfo.setRefund_price(nullToEmpty(m.get("refund_price")));
                            ptsMedicineInfo.setSequence(nullToEmpty(m.get("sequence")));
                            ptsMedicineInfos.add(ptsMedicineInfo);
                        }
                        ptsOrderInfo.setItems(ptsMedicineInfos);
                    }
                }
            }
            //String jsonstr = JSON.toJSONString(ptsOrderInfo);
            //String param = "{\"TXCODE\":\"00050002\",\"DATA\":[" + jsonstr + "]}";
            //result = HttpUtil.sendSimplePostRequest(PtsUrl, param);
            List<PtsOrderInfo> data = new ArrayList<>();
            Map<String, Object> pushMap = new HashMap<>();
            data.add(ptsOrderInfo);
            pushMap.put("TXCODE", "00050003");
            pushMap.put("DATA", data);
            result = HttpUtils.doPost(PtsUrl, "", JSON.toJSONString(pushMap));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            result = "{\"RETURNCODE\":\"F\",\"MESSAGE\":\"" + e.getMessage() + "\",\"DATA\":[]}";
        }
        return result;
    }

    public static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }

    }

    public static String trimUnSupportChar(String inStr){
        return inStr.replace("'"," ");
    }

    /**
     * 过滤掉超过3个字节的UTF8字符
     *
     * @param text
     * @return
     */
    public static String filterOffUtf8Mb4(String text) {
        String result = text;
        try {
            byte[] bytes = text.getBytes("utf-8");
            ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
            int i = 0;
            while (i < bytes.length) {
                short b = bytes[i];
                if (b > 0) {
                    buffer.put(bytes[i++]);
                    continue;
                }
                b += 256; // 去掉符号位

                if (((b >> 5) ^ 0x6) == 0) {
                    buffer.put(bytes, i, 2);
                    i += 2;
                } else if (((b >> 4) ^ 0xE) == 0) {
                    buffer.put(bytes, i, 3);
                    i += 3;
                } else if (((b >> 3) ^ 0x1E) == 0) {
                    i += 4;
                } else if (((b >> 2) ^ 0x3E) == 0) {
                    i += 5;
                } else if (((b >> 1) ^ 0x7E) == 0) {
                    i += 6;
                } else {
                    buffer.put(bytes[i++]);
                }
            }
            buffer.flip();
            result = new String(buffer.array(), "utf-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * 将药德系统订单对象转换为PTS需要的map对象
     * @param baseOrderInfo
     * @return
     * @author：Li.Deman
     * @date：2018/11/30
     */
    public static Map<String,Object>  getOrderInfoToPTSByMap(BaseOrderInfo baseOrderInfo){
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try{
            resultMap.put("caution",baseOrderInfo.getCaution());
            resultMap.put("create_time",baseOrderInfo.getCreate_time());
            resultMap.put("customer_pay",baseOrderInfo.getCustomer_pay());
            resultMap.put("delivery_time",baseOrderInfo.getDelivery_time());
            resultMap.put("invoice_title",baseOrderInfo.getInvoice_title());
            resultMap.put("is_invoiced",baseOrderInfo.getIs_invoiced());
            resultMap.put("order_id",baseOrderInfo.getOrder_id());
            resultMap.put("order_info",baseOrderInfo.getOrder_info());
            resultMap.put("original_price",baseOrderInfo.getOriginal_price());
            resultMap.put("pay_type",baseOrderInfo.getPay_type());
            resultMap.put("platforms",baseOrderInfo.getPlatforms());
            resultMap.put("platforms_order_id",baseOrderInfo.getPlatforms_order_id());
            resultMap.put("recipient_address",baseOrderInfo.getRecipient_address());
            resultMap.put("recipient_name",baseOrderInfo.getRecipient_name());
            resultMap.put("recipient_phone",baseOrderInfo.getRecipient_phone());
            resultMap.put("shipping_fee",baseOrderInfo.getShipping_fee());
            resultMap.put("shop_no",baseOrderInfo.getShop_no());
            resultMap.put("status",baseOrderInfo.getStatus());
            resultMap.put("taxpayer_id",baseOrderInfo.getTaxpayer_id());
            resultMap.put("total_price",baseOrderInfo.getTotal_price());
            resultMap.put("update_time",baseOrderInfo.getUpdate_time());
            resultMap.put("day_seq",baseOrderInfo.getDay_seq());
            resultMap.put("notice_message",baseOrderInfo.getNotice_message());
            List<Map<String, Object>> detail = new ArrayList<Map<String, Object>>();
            Integer rowindex=1;
            for (BaseOrderProInfo item:baseOrderInfo.getItems()){
                Map<String, Object> itemMap = new HashMap<String, Object>();
                itemMap.put("box_num",item.getBox_num());
                itemMap.put("box_price",item.getBox_price());
                itemMap.put("discount",item.getDiscount());
                itemMap.put("medicine_code",item.getMedicine_code());
                itemMap.put("quantity",item.getQuantity());
                itemMap.put("sequence",rowindex++);
                itemMap.put("unit",item.getUnit());
                itemMap.put("price",item.getPrice());
                detail.add(itemMap);
            }
            resultMap.put("detail",detail);
            resultMap.put("result","ok");
        }catch (Exception e){
            resultMap.put("result",e.getMessage());
        }
        return resultMap;
    }

    /**
     * 将药德系统订单对象转换为PTS需要的map对象
     * 支持部分退款和全额退款
     * @param baseOrderInfo
     * @return
     * @author：Li.Deman
     * @date：2018/12/27
     */
    public static Map<String,Object> getOrderInfoToPTSByMapForRefund(BaseOrderInfo baseOrderInfo){
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try{
            resultMap.put("order_id",baseOrderInfo.getOrder_id());
            resultMap.put("platforms_order_id",baseOrderInfo.getPlatforms_order_id());
            resultMap.put("shop_no",baseOrderInfo.getShop_no());
            resultMap.put("reason",baseOrderInfo.getReason());
            resultMap.put("refund_money",baseOrderInfo.getRefund_money());
            resultMap.put("action",baseOrderInfo.getAction());
            resultMap.put("status",baseOrderInfo.getStatus());
            List<Map<String, Object>> detail = new ArrayList<Map<String, Object>>();
            for (BaseOrderProInfo item:baseOrderInfo.getItems()){
                Map<String, Object> itemMap = new HashMap<String, Object>();
                itemMap.put("order_id",item.getOrder_id());
                itemMap.put("medicine_code",item.getMedicine_code());
                itemMap.put("refund_qty",item.getRefund_qty());
                itemMap.put("refund_price",item.getRefund_price());
                itemMap.put("sequence",item.getSequence());
                detail.add(itemMap);
            }
            resultMap.put("detail",detail);
            resultMap.put("result","ok");
        }catch (Exception e){
            resultMap.put("result","ERROR:" + e.getMessage());
        }
        return resultMap;
    }
}
