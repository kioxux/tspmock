package com.gys.util.takeAway.ddky;

/**
 * @author ZhangDong
 * @date 2021/10/18 13:21
 */
public enum DdkyOrderStatus {

    ORDER_CREATED("5","创建订单"),
    ORDER_CHECKOUT("6","订单出库"),
    ORDER_DELIVERED("7","订单妥投"),
    ORDER_REJECT("8","订单拒收"),
    ORDER_CANCEL("10","订单取消"),
    ORDER_SHIPPING("51", "订单运费");


    private String status;
    private String desp;

    DdkyOrderStatus(String status, String desp) {
        this.status = status;
        this.desp = desp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

}
