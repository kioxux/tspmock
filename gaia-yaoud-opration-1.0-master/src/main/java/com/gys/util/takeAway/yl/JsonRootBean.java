package com.gys.util.takeAway.yl;

import com.gys.common.data.yaolian.common.RequestHead;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class JsonRootBean implements Serializable {
    private static final long serialVersionUID = 8908094883797859493L;

    /**
     * 请求头
     */
    private RequestHead requestHead;

    /**
     *  1 | 1、普通（默认为1）| 必须 |
     */
    private String channel;

    /**
     * 集合参数
     */
    private List<Drugs> drugs;
}
