package com.gys.util.takeAway.ddky;

import lombok.Data;

import java.util.List;

/**
 * 库存批量更新
 * @author ZhangDong
 * @date 2021/10/14 14:17
 */
@Data
public class DdkyStockUpdateBean {

    private Long shopId;
    private List<DdkyStockUpdateDetailBean> goodsList;
}
