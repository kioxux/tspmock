package com.gys.util.takeAway;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class OrderStatus {
    private static Map<Integer,String> orderStatus;
    public static String getOrderStatusById(Integer id) {
        if (id > 4)
            return "无此订单状态";
        else
            return orderStatus.get(id);
    }
    public static String getOrderStatusById(String id) {
        try{
            int iid = Integer.valueOf(id);
            return getOrderStatusById(iid);
        }catch (Exception ex){
            return "订单状态异常！";
        }
    }
    @PostConstruct
    public void init() {
        orderStatus=new HashedMap();
        orderStatus.put(0,"新订单");
        orderStatus.put(1,"已确认订单");
        orderStatus.put(2,"已配送订单");
        orderStatus.put(3,"已完成订单");
        orderStatus.put(4,"已取消订单");
    }
}
