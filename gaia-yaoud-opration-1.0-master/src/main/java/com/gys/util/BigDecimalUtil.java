package com.gys.util;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 * @author wu mao yin
 * @Title: 数值计算工具类
 * @date 2021/12/19:45
 */
@UtilityClass
public class BigDecimalUtil {

    public final static BigDecimal ZERO = BigDecimal.ZERO;

    public final static BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);

    public final static RoundingMode HALF_UP = RoundingMode.HALF_UP;

    /**
     * 两数相除， 默认保留2位小数
     *
     * @param arg1 arg1
     * @param arg2 arg2
     * @return BigDecimal
     */
    public static BigDecimal divide(BigDecimal arg1, BigDecimal arg2) {
        return divide(arg1, arg2, 2);
    }

    /**
     * 两数相除， 可执行保留小数位
     *
     * @param arg1  arg1
     * @param arg2  arg2
     * @param scale scale
     * @return BigDecimal
     */
    public static BigDecimal divide(BigDecimal arg1, BigDecimal arg2, int scale) {
        if (arg1 == null) {
            return ZERO;
        }
        if (arg2 == null || arg2.compareTo(ZERO) == 0) {
            return ZERO;
        }

        return arg1.divide(arg2, scale, HALF_UP);
    }

    /**
     * 两数相除， 可执行保留小数位，计算百分比/占比
     *
     * @param arg1  arg1
     * @param arg2  arg2
     * @param scale scale
     * @return BigDecimal
     */
    public static BigDecimal divideWithPercent(BigDecimal arg1, BigDecimal arg2, int scale) {
        return divide(arg1, arg2, scale).multiply(ONE_HUNDRED);
    }

    /**
     * 格式化小数位
     *
     * @param arg   arg
     * @param scale scale
     * @return BigDecimal
     */
    public static BigDecimal format(BigDecimal arg, int scale) {
        if (arg == null) {
            return ZERO;
        }
        return arg.setScale(scale, HALF_UP);
    }
    /**
     * Object转BigDecimal类型
     *
     * @param value 传入Object值
     * @return 转成的BigDecimal类型数据
     */
    public static BigDecimal ToBigDecimal(Object value) {
        BigDecimal bigDec = null;
        if (value != null) {
            if (value instanceof BigDecimal) {
                bigDec = (BigDecimal) value;
            } else if (value instanceof String) {
                bigDec = new BigDecimal((String) value);
            } else if (value instanceof BigInteger) {
                bigDec = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                bigDec = new BigDecimal(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Can Not make [" + value + "] into a BigDecimal.");
            }
        }
        return bigDec;
    }

}
