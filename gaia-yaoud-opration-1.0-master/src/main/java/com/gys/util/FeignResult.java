package com.gys.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/18
 */
@Data
public class FeignResult<T> implements Serializable {
    private Integer code;

    private String message;

    private boolean success;

    private Object data;

    private String msg;
}
