//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class InputDataFormat {
    public InputDataFormat() {
    }

    public static String makeInputStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while((line = reader.readLine()) != null) {
                sb.append(line);
            }

            String var4 = sb.toString();
            return var4;
        } catch (IOException var14) {
            var14.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException var13) {
                var13.printStackTrace();
            }

        }

        return null;
    }

    public static Map<String, Object> formData2Dic(String formData) {
        Map<String, Object> result = new HashMap();
        if (formData != null && formData.trim().length() != 0) {
            String[] items = formData.split("&");
            Arrays.stream(items).forEach((item) -> {
                String[] keyAndVal = item.split("=");
                if (keyAndVal.length == 2) {
                    try {
                        String key = URLDecoder.decode(keyAndVal[0], "utf8");
                        Object val = URLDecoder.decode(keyAndVal[1], "utf8");
                        result.put(key, val);
                    } catch (UnsupportedEncodingException var5) {
                        System.out.println(var5.getMessage());
                    }
                }

            });
            return result;
        } else {
            return result;
        }
    }
}
