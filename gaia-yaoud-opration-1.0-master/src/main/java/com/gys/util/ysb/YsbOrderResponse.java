package com.gys.util.ysb;

import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/6/7 16:23
 */
@Data
public class YsbOrderResponse<T> {

    private String code;
    private String message;
    private T data;
}