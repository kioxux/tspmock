package com.gys.util.ysb;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/6/7 15:56
 */
@Data
@NoArgsConstructor
public class YsbOrderUpdateBean {

    private String orderId;
    private String orderCode = "";
    private Integer status;

    public YsbOrderUpdateBean(String orderId, String orderCode, Integer status) {
        this.orderId = orderId;
        this.orderCode = orderCode;
        this.status = status;
    }

}
