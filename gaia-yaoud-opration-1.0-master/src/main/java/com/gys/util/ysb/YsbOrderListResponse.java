package com.gys.util.ysb;

import lombok.Data;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/7 12:04
 */
@Data
public class YsbOrderListResponse<T> {

    private String code;
    private String message;
    private List<T> data;
}
