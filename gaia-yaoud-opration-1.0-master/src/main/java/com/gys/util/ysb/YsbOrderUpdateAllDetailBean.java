package com.gys.util.ysb;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhangdong
 * @date 2021/6/20 17:56
 */
@Data
public class YsbOrderUpdateAllDetailBean {

    private String drugCode;//药品编码
    private BigDecimal amount;//数量
    private BigDecimal price;//单价
    private String batchNum;//批号
    private String validity;//有效期
    private String prodDate;//生产日期
}
