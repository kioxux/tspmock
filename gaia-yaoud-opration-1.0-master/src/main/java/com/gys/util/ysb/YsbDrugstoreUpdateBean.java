package com.gys.util.ysb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/6/22 13:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YsbDrugstoreUpdateBean {

    private String controlType = "";    //是	管控方式(字符串）
    private String number;    //是	药店编码(字符串）
    private String storeTitle;    //是	药店名称(字符串）
    private String busiScope = "";    //是	经营范围(字符串）以某中分隔符分开
    private String certNo = "";    //是	证照号(字符串）
    private String invalidDate = "";    //是	失效日期（时间磋格式）
    private String address;    //	地址
    private String drugstoreBranchId;    //否	药师帮平台的药店ID

    public YsbDrugstoreUpdateBean(String number, String storeTitle) {
        this.number = number;
        this.storeTitle = storeTitle;
    }
}
