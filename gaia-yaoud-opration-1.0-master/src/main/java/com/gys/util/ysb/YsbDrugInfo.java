package com.gys.util.ysb;

import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/6/7 11:59
 */
@Data
public class YsbDrugInfo {

    private String drugId;//	药品在药师帮系统的编号
    private String drugCode;//	药品在供应商系统的编号（字符串）
    private String drugName;//	药品名称（仅用于核对，配货单上的药品名称请以开单系统药品资料为准）
    private String price;//	单价（开启推送折后价后，这个价格是折后单价）
    private String originalPrice;//	折前原单价 (开启推送折后价后，这个价格是折前单价）
    private String drugInvoiceType;//	发票类型（0-缺省，1-含票）
    private String newQty;//	数量（新）
    private String newAmount;//	总金额（新）（开启推送折后价后，这个价格是折后金额）
    private String deliverAlone;//	不同药品类别（与后台设置开单组合有关）
    private String deliverType;//	订单活动类型
    private String serialNo;//	药品序号（从0开始的排序）
    private String drugRemark;//	药品备注（存放药品促销政策等内容）
    private String feeRatio;//	商品首推费率
}
