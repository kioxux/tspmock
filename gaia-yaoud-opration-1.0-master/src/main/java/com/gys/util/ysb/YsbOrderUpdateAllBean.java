package com.gys.util.ysb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/20 17:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YsbOrderUpdateAllBean {

    private String orderId;//药师帮订单号
    private String orderCode;//批发销售订单
    private Integer status;//状态
    private List<YsbOrderUpdateAllDetailBean> orderDetails;//订单明细列表
}
