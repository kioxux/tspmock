package com.gys.util.ysb;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhangdong
 * @date 2021/6/22 16:04
 */
@Data
public class YsbGoodlInfo {

    /**
     * 地点名称
     */
    private String dcName;

    /**
     * 地点编号
     */
    private String dcCode;
    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产企业
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 中包装量
     */
    private String proMidPackage;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 库存地点
     */
    private String batLocationCode;
    /**
     * 批次
     */
    private String batBatch;
    /**
     * 库存数量
     */
    private BigDecimal batNormalQty;
    /**
     * 库存金额
     */
    private BigDecimal batBatchCost;
    /**
     * 生产批号
     */
    private String batBatchNo;
    /**
     * 生产日期
     */
    private String batProductDate;
    /**
     * 有效期
     */
    private String batExpiryDate;
    /**
     * 采购供应商编码
     */
    private String batSupplierCode;
    /**
     * 采购供应商名
     */
    private String batSupplierName;
    /**
     * 采购价格
     */
    private BigDecimal batPoPrice;
    /**
     * 采购订单
     */
    private String batPoId;
    /**
     * 采购订单行
     */
    private String batPoLineno;
    /**
     * 采购税率编码
     */
    private String poRate;
    /**
     * 采购税率值
     */
    private String taxCodeValue;

    private String proCommonname;
    /**
     * 商品单价
     */
    private BigDecimal soPrice;

    /**
     * 建议补货量
     */
    private BigDecimal gsrdProposeQty;

    /**
     * 货位号
     */
    private String gsrdHwh;

    /**
     * 行号
     */
    private String gsrdSerial;

    /**
     * 补货单号
     */
    private String gsrdVoucherId;

}
