package com.gys.util.ysb;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.gys.business.service.data.yaoshibang.GaiaSoHeader;
import com.gys.business.service.data.yaoshibang.SoItem;
import com.gys.common.config.yaoshibang.YsbConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 药师帮外卖-接口封装类
 *
 * @author zhangdong
 * @date 2021/6/7 11:08
 */
@Slf4j
public class YsbApiUtil {

    private static RestTemplate restTemplate;

    static {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10000);
        requestFactory.setReadTimeout(10000);
        restTemplate = new RestTemplate(requestFactory);
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                List<MediaType> mediaTypeList = new ArrayList<>(converter.getSupportedMediaTypes());
                mediaTypeList.add(MediaType.APPLICATION_OCTET_STREAM);
                mediaTypeList.add(MediaType.APPLICATION_JSON);
                ((MappingJackson2HttpMessageConverter) converter).setSupportedMediaTypes(mediaTypeList);
            }
        }
    }

    /**
     * 2.1 订单查询
     */
    public static List<YsbOrderDto> orderQuery(String appId, String authCode) {
        log.info("药师帮API,订单查询开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        YsbQueryBean bean = new YsbQueryBean(appId, authCode);
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YsbOrderListResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + YsbConfig.ORDER_QUERY, httpEntity, YsbOrderListResponse.class);
        YsbOrderListResponse response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            List list = response.getData();
            log.info("药师帮API,订单查询,appId:{},authCode:{},result list:{}", appId, authCode, JSON.toJSONString(list));
            if (CollUtil.isNotEmpty(list)) {
                List<YsbOrderDto> dtoList = (List<YsbOrderDto>) list.stream().map(r -> {
                    YsbOrderDto ysbOrderDto = new YsbOrderDto();
                    try {
                        BeanUtils.populate(ysbOrderDto, (Map<String, ? extends Object>) r);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //订单明细信息
                    List<LinkedHashMap> drugInfo = (List<LinkedHashMap>) ((LinkedHashMap) r).get("drugInfo");
                    if (CollUtil.isNotEmpty(drugInfo)) {
                        List<YsbDrugInfo> drugInfoList = drugInfo.stream().map(v -> {
                            YsbDrugInfo ysbDrugInfo = new YsbDrugInfo();
                            try {
                                BeanUtils.populate(ysbDrugInfo, v);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return ysbDrugInfo;
                        }).collect(Collectors.toList());
                        ysbOrderDto.setDrugInfo(drugInfoList);
                    }
                    return ysbOrderDto;
                }).collect(Collectors.toList());
                return dtoList;
            } else {
                log.info("药师帮API,订单查询,result list is empty");
            }
        } else {
            log.error("药师帮API,订单查询fail,response:" + JSON.toJSONString(response));
        }
        return null;
    }

    /**
     * 调用“修改状态”接口
     * success message:  更改订单状态信息成功,总共请求1个，0个未改变。不处理非该供应商的订单:[]
     *
     * @param orderIdList
     */
    public static boolean updateOrder(List<String> orderIdList, Integer status, String orderCode, String appId, String authCode) {
        if (CollUtil.isEmpty(orderIdList)) {
            return false;
        }
        log.info("药师帮API,修改订单状态开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        YsbQueryBean bean = new YsbQueryBean(appId, authCode);
        List<YsbOrderUpdateBean> updateBeanList = orderIdList.stream().map(r -> new YsbOrderUpdateBean(r, orderCode, status)).collect(Collectors.toList());
        bean.setData(updateBeanList);
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YsbOrderResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + YsbConfig.ORDER_UPDATE, httpEntity, YsbOrderResponse.class);
        YsbOrderResponse response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            log.info("药师帮API,修改订单状态success,orderIdList:" + JSON.toJSONString(orderIdList));
            return true;
        } else {
            log.info("药师帮API,修改订单状态fail,orderIdList:{},response:{}", JSON.toJSONString(orderIdList), JSON.toJSONString(response));
        }
        return false;
    }

    /**
     * 调用“修改状态”接口
     * erp开了销售单后，使用2.4 updateOrderStatus上传一次数据回来，所有参数都要上传
     * success message:  YsbOrderResponse(code=200, message=更改订单状态信息成功,总共请求1个，0个未改变。不处理非该供应商的订单:[], data={successCount=1, total=1, cachedOrderIds=[], errorData=[]})
     */
    public static boolean updateOrderAll(GaiaSoHeader header, String orderId, String orderCode, Integer status, String appId, String authCode) {
        log.info("药师帮API,修改订单状态all开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        YsbQueryBean bean = new YsbQueryBean(appId, authCode);
        YsbOrderUpdateAllBean allBean = new YsbOrderUpdateAllBean();
        allBean.setOrderId(orderId);
        allBean.setOrderCode(orderCode);
        allBean.setStatus(status);
        List<SoItem> soItemList = header.getSoItemList();
        List<YsbOrderUpdateAllDetailBean> detailBeanList = new ArrayList<>(soItemList.size());
        for (SoItem soItem : soItemList) {
            YsbOrderUpdateAllDetailBean detailBean = new YsbOrderUpdateAllDetailBean();
            detailBean.setBatchNum(soItem.getSoBatchNo());
            detailBean.setDrugCode(soItem.getSoProCode());
            detailBean.setPrice(soItem.getSoPrice());
            detailBean.setAmount(soItem.getSoQty());
            detailBean.setValidity(soItem.getBatExpiryDate());
            detailBeanList.add(detailBean);
        }
        allBean.setOrderDetails(detailBeanList);
        bean.setData(Lists.newArrayList(allBean));
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YsbOrderResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + YsbConfig.ORDER_UPDATE, httpEntity, YsbOrderResponse.class);
        YsbOrderResponse response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            log.info("药师帮API,修改订单状态all success,orderId:{}", orderId);
            return true;
        } else {
            log.info("药师帮API,修改订单状态all fail,orderId:{},response:{}", orderId, JSON.toJSONString(response));
        }
        return false;
    }

    /**
     * 上传药店信息
     * number和storeTitle这两个给实际的值就行，其他参数给空字符就可以;
     * drugstoreBranchId这个是要传平台客户id，如果erp里没有就给0 , 可以不传
     */
    public static boolean updateDrugstore(String appId, String authCode, List<YsbDrugstoreUpdateBean> list) {
        log.info("药师帮API,上传药店信息开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        YsbQueryBean bean = new YsbQueryBean(appId, authCode);
        bean.setData(list);
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YsbOrderResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + YsbConfig.UPDATE_DRUGSTORE, httpEntity, YsbOrderResponse.class);
        YsbOrderResponse response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            log.info("药师帮API,上传药店信息 success,inData:{}", JSON.toJSONString(bean));
            return true;
        } else {
            log.info("药师帮API,上传药店信息 fail,inData:{},response:{}", JSON.toJSONString(bean), JSON.toJSONString(response));
        }
        return false;
    }


    /**
     * 上传药品价格
     * @param appId
     * @param authCode
     * @param list
     * @return
     */
    public static boolean updatePrice(String appId, String authCode, List<LinkedHashMap<String, Object>> list) {
        log.info("药师帮API,上传药品价格信息开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        YsbQueryBean bean = new YsbQueryBean(appId, authCode);
        bean.setData(list);
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YsbOrderResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + YsbConfig.UPDATE_Price, httpEntity, YsbOrderResponse.class);
        YsbOrderResponse response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            log.info("药师帮API,上传药品价格 success,inData:{}", JSON.toJSONString(bean));
            return true;
        } else {
            log.info("药师帮API,上传药品价格 fail,inData:{},response:{}", JSON.toJSONString(bean), JSON.toJSONString(response));
        }
        return false;
    }

    /**
     * 上传药品价格
     * @param appId
     * @param authCode
     * @param list
     * @return
     */
    public static boolean updateStock(String appId, String authCode, List<LinkedHashMap<String, Object>> list) {
        log.info("药师帮API,上传药品库存信息开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        YsbQueryBean bean = new YsbQueryBean(appId, authCode);
        bean.setData(list);
        HttpEntity httpEntity = new HttpEntity(bean, headers);
        ResponseEntity<YsbOrderResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + YsbConfig.UPDATE_Stock, httpEntity, YsbOrderResponse.class);
        YsbOrderResponse response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            log.info("药师帮API,上传药品库存 success,inData:{}", JSON.toJSONString(bean));
            return true;
        } else {
            log.info("药师帮API,上传药品库存 fail,inData:{},response:{}", JSON.toJSONString(bean), JSON.toJSONString(response));
        }
        return false;
    }


    /**
     * 获取药店信息--药店其实也就是客户
     *
     * @param isAll 是否全部获取  1-全部获取（一般用于第一次获取数据）
     *              0-增量获取（首单在3天内或3天内药店客户有修改过资料）
     */
    public static List<YsbDrugstoreDto> getDrugstoreInfo(String appId, String authCode, Integer isAll) {
        long start = System.currentTimeMillis();

        log.info("药师帮API,修改订单状态all开始==========");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        Map<String, Object> map = new HashMap<>(4);
        map.put("appId", appId);
        map.put("authCode", authCode);
        map.put("isAll", isAll);
        HttpEntity httpEntity = new HttpEntity(map, headers);
        ResponseEntity<YsbOrderResponse> responseEntity = restTemplate.postForEntity(YsbConfig.BASE_URL + "/provider/getDrugstoreInfo", httpEntity, YsbOrderResponse.class);
        YsbOrderResponse<List> response = responseEntity.getBody();
        if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && String.valueOf(HttpStatus.OK.value()).equals(response.getCode())) {
            List list = response.getData();
            log.info("药师帮API,获取药店信息 success,appId:{},authCode:{}, result:{}", appId, authCode, JSON.toJSONString(list));
            if (CollUtil.isNotEmpty(list)) {
                List<YsbDrugstoreDto> dtoList = (List<YsbDrugstoreDto>) list.stream().map(r -> {
                    YsbDrugstoreDto ysbDrugstoreDto = new YsbDrugstoreDto();
                    try {
                        BeanUtils.populate(ysbDrugstoreDto, (Map<String, ? extends Object>) r);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return ysbDrugstoreDto;
                }).collect(Collectors.toList());
                return dtoList;
            } else {
                log.info("药师帮API,获取药店信息,appId:{},authCode:{},result list is empty", appId, authCode);
            }
        } else {
            log.error("药师帮API,获取药店信息fail,appId:{},authCode:{},response:", appId, authCode, JSON.toJSONString(response));
        }
        System.err.println("耗时："+(System.currentTimeMillis()-start)+"ms");
        return null;
    }


    /**
     * [{"orderId":39731762,"orderTime":"2020-09-25 17:55:03","newPayTime":"2020-09-25 17:55:34","customerId":"测试药店编码","drugstoreName":"肇州县仁和堂药房","drugstoreBranchId":1408138,"totalCost":1032.2,"couponPayProv":20.64,"deliverFee":0,"subPay":0,"invoiceType":2,"payTime":1601027734,"drugInfo":[{"drugId":263391,"wholesaleId":7354398,"drugCode":"1011004","drugName":"康迈欣 盐酸头孢他美酯片","appName":"盐酸头孢他美酯片 181.3mg*4s*2板","price":7.5,"amount":50.0,"count":375.0,"drugInvoiceType":0,"newQty":50.0,"newAmount":375.0,"serialNo":0,"deliverAlone":0,"wholesaleType":1,"originalPrice":7.5,"subPay":0.0,"refundAmount":0.0,"drugRemark":"","batchId":"20190207","prodDate":"2019-12-13","pack":"181.3mg*4s*2板","deliverType":0,"provDrugFactor":1.0,"needReport":0,"unit":"盒","feeRatio":0.0,"fixedPrice":7.5},{"drugId":8086,"wholesaleId":5513717,"drugCode":"1012017","drugName":"波依定 非洛地平缓释片","appName":"非洛地平缓释片(波依定) 5mg*10s","price":29.2,"amount":10.0,"count":292.0,"drugInvoiceType":0,"newQty":10.0,"newAmount":292.0,"serialNo":1,"deliverAlone":0,"wholesaleType":1,"originalPrice":29.2,"subPay":0.0,"refundAmount":0.0,"drugRemark":"","batchId":"1808A16,1908A10","prodDate":"2020-03-01","pack":"5mg*10s","deliverType":0,"provDrugFactor":1.0,"needReport":0,"unit":"盒","feeRatio":0.0,"fixedPrice":0.0},{"drugId":61596,"wholesaleId":7351788,"drugCode":"1012028","drugName":"金毓婷 左炔诺孕酮片","appName":"左炔诺孕酮片 1.5mg*1片","price":14.3,"amount":14.0,"count":200.2,"drugInvoiceType":0,"newQty":14.0,"newAmount":200.2,"serialNo":2,"deliverAlone":0,"wholesaleType":1,"originalPrice":14.3,"subPay":0.0,"refundAmount":0.0,"drugRemark":"","batchId":"43190307","prodDate":"2019-07-04","pack":"1.5mg*1片","deliverType":0,"provDrugFactor":1.0,"needReport":0,"unit":"盒","feeRatio":0.0,"fixedPrice":0.0},{"drugId":109250,"wholesaleId":5513854,"drugCode":"1013020","drugName":"西乐葆 塞来昔布胶囊","appName":"塞来昔布胶囊 0.2g*6粒","price":33.0,"amount":5.0,"count":165.0,"drugInvoiceType":0,"newQty":5.0,"newAmount":165.0,"serialNo":3,"deliverAlone":0,"wholesaleType":1,"originalPrice":33.0,"subPay":0.0,"refundAmount":0.0,"drugRemark":"","batchId":"DC9515","prodDate":"2019-08-14","pack":"0.2g*6粒","deliverType":0,"provDrugFactor":1.0,"needReport":0,"unit":"盒","feeRatio":0.0,"fixedPrice":2.2}],"address":"黑龙江省大庆市肇州县南瑞商服楼由北向南数20门","remark":"[超长留言请到YSB后台查看]39731762药师帮已付款旗舰店自营[药师帮首单，三证可能需要随货配送],留言","cnDrug":0,"orderCnt":1,"orderSerial":4,"shipInfo":"盖兆玲  13704671468","firstOrder":"需提供三证","providerName":"对接测试店铺","drugProviderName":"对接测试店铺","recvName":"盖兆玲","recvPhone":"13704671468","taxNo":"92230621MA1926JB53","regAddress":"黑龙江省大庆市肇州县南瑞商服楼由北向南数20门","regPhone":"13704671468","bankName":"肇州农商行园区分理处","bankCardNo":"560230122000002489","exportTimes":0,"checkState":0,"provinceName":"黑龙江省","cityName":"大庆市","districtName":"肇州县","streetName":"肇州镇","streetAreaCode":230621100,"sn":"2120092518699658","refundCouponPayProv":0.0,"processStatus":0,"heyeDeliverAlone":0,"orderType":1,"channel":"iOS","drugProviderId":2710,"originalRemark":"随货纸质版资质，增值税专用发票。","processNote":"","drugReport":"","partnerProviderId":0,"isLyh":0,"deliverType":0,"isEPayment":0,"couponRatio":0,"isEInvoice":1}]
     *
     * @param args
     */
    public static void main(String[] args) {
        //orderQuery("YIYATONG", "557F51F074C54E6E808333A367644CE6","http://test.ysbang.cn/misc-ysb");
        //orderQuery("YIYATONG", "557F51F074C54E6E808333A367644CE9", "http://test.ysbang.cn/misc-ysb");

        //测试正式环境订单查询
//        orderQuery("ENDRTLHHGV", "e81c2ce176f196d6739de1d15e3ad6a2");
//        orderQuery("OBTQTYM", "bb9c8aa34d85518bc005b4d907691d5b");
        List<YsbDrugstoreDto> list = getDrugstoreInfo("ENDRTLHHGV", "e81c2ce176f196d6739de1d15e3ad6a2", 1);
//        System.out.println(JSON.toJSONString(list));

        //updateOrder("YIYATONG", "557F51F074C54E6E808333A367644CE9", "http://test.ysbang.cn/misc-ysb")
        //"39731762", "39733327", "39744238"
        //updateOrder(Lists.newArrayList("39731762"), 0, "", "YIYATONG", "557F51F074C54E6E808333A367644CE6", "http://test.ysbang.cn/misc-ysb");

        //测试上传药店
//        String s = "[{\"busiScope\":\"\",\"certNo\":\"\",\"controlType\":\"\",\"invalidDate\":\"\",\"number\":\"00001111\",\"storeTitle\":\"台州经济开发区天客隆诊所\"}]";
//        List<YsbDrugstoreUpdateBean> list = JSON.parseArray(s, YsbDrugstoreUpdateBean.class);
//        updateDrugstore("ENDRTLHHGV", "e81c2ce176f196d6739de1d15e3ad6a2",  list);
    }
}
