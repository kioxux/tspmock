package com.gys.util.ysb;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/7 11:55
 */
@Data
@NoArgsConstructor
public class YsbOrderDto {

    private String orderId;//	订单编码（数字；唯一）
    private String orderTime;//	下单时间（格式 “2015-06-26 12:06:00”）
    private String customerId;//	客户（药店）在供应商系统的编号（字符串）
    private String drugstoreName;//	客户（药店）名称（仅用于核对，配货单上的药店名称请以开单系统开户资料为准）
    private String totalCost;//	订单总金额
    private String invoiceType;//	发票类型（0 缺省，1 普通发票，2 专用发票）
    private String newPayTime;//	付款时间（格式 “2015-06-26 12:06:00”）
    private String couponPayProv;//	优惠金额 (非满立减部分的其他优惠）
    private List<YsbDrugInfo> drugInfo;//	订单明细信息
    private String shipInfo;//	收货人信息（姓名和电话）（小贺 138123456789）（拼接一起的值，中间间隔两个空格）
    private String provinceName;//	省份（广东省）
    private String cityName;//	城市（广州市）
    private String districtName;//	区或者县（海珠区）
    private String address;//	配送地址（仅用于核对，配货单上的地址请以开单系统开户资料为准）
    private String taxNo;//	税务登记证号
    private String regAddress;//	注册地址
    private String regPhone;//	注册电话
    private String bankName;//	开户行
    private String bankCardNo;//	开户行账号
    private String remark;//	订单备注（格式为订单编码 + 订单类型（爆款、自营）+ 客户留言）（需写入到开单系统的备注中）
    private String originalRemark;//	客户留言信息和资质需求信息(拼接的信息，数据会很长）
    private String isEPayment;//	是否电汇，1-是，0-否
    private String couponRatio;//	平台优惠券费率(保留6位小数，四舍五入)

    private String subPay;//	满立减金额
    private String recvPhone;//	收件电话
    private String recvName;//	收件人
    private String deliverFee;//	运费
    private String providerName;//	配送商商家名称
    private String drugProviderName;//	委托商商家名称（如果没有委托商家 显示”待指定”）
    private String isLyh;//	是否邻药汇订单（1-是 0-否）
    private String deliverType;//	订单活动类型（0-普通单 2-限量秒杀, 7-药慧拼（拼团） 8-批购包邮）

    private String client;

}
