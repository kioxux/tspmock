package com.gys.util.ysb;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/12/6 13:42
 */
@Data
public class YsbDrugstoreDto {

    private String drugstoreId;//药店ID
    private String customerId;//药店编码
    private String drugstoreName;//药店名称
    private String provinceName;//省份
    private String cityName;//城市
    private String districtName;//区县
    private String regAdress;//注册地址
    private String drugStoreType;//药店类型0-零售单体 1-第三终端 2-连锁直营 3-连锁加盟 4-连锁总部 5-商业公司
    private String invoiceType;//开票类型 0-无要求 1-普票 2-专票
    private String taxNo;//税号
    private String bankName;//开户行
    private String bankCardNo;//银行账户
    private String busiScope;//商品经营范围（用,分隔）  1-中成药  2-化学药制剂  3-抗生素制剂  4-生物制品   5-中药饮片   6-医疗器械（二类）   7-医疗器械（三类）   8-食品   9-医疗器械（一类）   10-日用品   11-化妆品   12消毒产品   13-全球购   14-保健品
    private String otcScope;//处方药经营范围（用,分隔）   1-处方药  2-甲类非处方  3-乙类非处方  4-其它非药
    private String busicardNo;//营业执照编号
    private String busicardIssue;//营业执照营业执照发证日期，格式为：2015-12-30
    private String busicardValid;//营业执照有效期，格式为：2015-12-30，或者“长期”
    private String proxyOpername;//法人代表
    private String drugBusitcardNo;//药品经营许可证编号
    private String drugBusiIssue;//药品许可证发证日期，格式为：2015-12-30
    private String drugBusiValidity;//药品许可证有效期，格式为：2015-12-30
    private String qualityManager;//质量负责人
    private String gspcardNo;//GSP证书编号
    private String gspIssue;//GSP发证日期，格式为：2015-12-30
    private String gspValidity;//GSP有效期，格式为：2015-12-30
    private String hisOrgcardNo;//医疗机构执业许可证编号
    private String hisOrgcardIssue;//医疗机构执业许可证发证日期，格式为：2015-12-30
    private String hisOrgcardValid;//医疗机构执业许可证有效日期，格式为：2015-12-30
    private String hisOrgcardUrl;//医疗机构执业许可证图片地址
    private String foodBusitcardNo;//食品流通许可证编号
    private String hiseqBusitcardNo;//医疗器械经营企业许可证编号
    private String selIdentityNo;//采购员身份证编号（采购人员身份证扫描件上身份证）
    private String sellerName;//采购员姓名（采购人员身份证扫描件上姓名）
    private String hiseqRecord;//医疗器械备案凭证编号
    private String busicardUrl;//营业执照图片地址
    private String drugBusiUrl;//药品经营许可证图片地址
    private String selIdentityUrl;//采购人身份证图片地址
    private String purchasePaperUrl;//采购委托书图片地址
    private String gspUrl;//gsp证书图片地址
    private String hiseqRecordUrl;//二级器械备案凭证图片地址
    private String hiseqBusitcardUrl;//医疗器械经营许可证图片地址
    private String foodBusiUrl;//食品经营许可证图片地址
    private String invoiceInfoUrl;//开票信息图片地址
    private String changeRecordUrl;//变更记录图片地址
    private String areaCode;//地区编码
    private String mtimeTimestamp;//药店信息最近修改时间
    private String firstOrderTime;//首单时间
    private String salesmanPhone;//在职店员电话号码
    private String salesmanName;//在职店员姓名
    private String lastPaperUpdateTime;//药店证件最近修改时间
    private String updateFlag;//药店更新标志（true-有更新，false-无更新，更新内容：药店名称、药店地址、营业执照、药品经营许可证、医疗机构执业许可证、开票信息、变更记录）
    private String newHiseqRecordUrl;//二级器械备案凭证图片地址（与hiseqRecordUrl数据一样，取其中一个就行）
    private String busicardName;//营业执照药店执照名称
    private String busicardAddress;//营业执照药店注册地址
    private String drugBusiProxyOpername;//药品经营许可证法人代表
    private String drugBusiPrincipal;//药品经营许可证主要负责人
    private String drugBusiName;//药品经营许可证药店许可证名称
    private String purchasePaperValid;//采购委托书有效期
    private String purchasePaperNo;//采购委托书证件编号
    private String selIdentityIssue;//采购人身份证发证日期
    private String selIdentityValid;//采购人身份证有效期
    private String sellerPhone;//采购人身份证电话号码
    private String foodBusiIssue;//食品经营许可证发证日期
    private String foodBusiValid;//食品经营许可证有效期
    private String foodBusiNo;//食品经营许可证证件编号
    private String invoiceInfoName;//开票信息开户户名
    private String invoiceInfoAddress;//开票信息开票地址
    private String invoiceInfoPhone;//开票信息开票电话
    private String hisOrgcardProxyOpername;//医疗机构执业许可证法人代表
    private String hisOrgcardPrincipal;//医疗机构执业许可证主要负责人
    private String hiseqBusitcardIssue;//医疗器械经营许可证发证日期
    private String hiseqBusitcardValid;//医疗器械经营许可证有效期
    private int bePapersSyn;//资质是否共享，0-否，1-是

}
