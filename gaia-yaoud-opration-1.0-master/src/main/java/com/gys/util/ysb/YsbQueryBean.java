package com.gys.util.ysb;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/6/7 11:20
 */
@Data
@NoArgsConstructor
public class YsbQueryBean {

    private String appId;
    private String authCode;
    private Object data;

    public YsbQueryBean(String appId, String authCode) {
        this.appId = appId;
        this.authCode = authCode;
    }
}
