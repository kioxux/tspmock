package com.gys.util;

import java.util.Random;
import java.util.UUID;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/23 19:39
 */
public class SerialNoUtils {

    private static final Random random = new Random();

    public static String generate(String prefix, String suffix) {
        String serialNo = UUID.randomUUID().toString().replace("-", "");
        return String.format("%s%s%s%s", prefix, suffix, random.nextInt(1000), serialNo.substring(0, 19)).toUpperCase();
    }

}
