package com.gys.util;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.gys.common.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 17:42 2021/8/11
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Component
public class GuoYaoUtil {

    private static final Logger logger = LoggerFactory.getLogger("国控第三方接口");
    private static String USERNAME;
    @Value("${guoyao.username}")
    private void setUSERNAME(String USERNAME) {
        GuoYaoUtil.USERNAME = USERNAME;
    }
    private static String PASSWORD;
    @Value("${guoyao.password}")
    private void setPASSWORD(String PASSWORD) {
        GuoYaoUtil.PASSWORD = PASSWORD;
    }
    private static String GET_ACCESS_TOKEN_URL;
    @Value("${guoyao.GET_ACCESS_TOKEN_URL}")
    private void setGetAccessTokenUrl(String getAccessTokenUrl) {
        GET_ACCESS_TOKEN_URL = getAccessTokenUrl;
    }
    private static String FIND_GOODS_DETAIL_URL;
    @Value("${guoyao.FIND_GOODS_DETAIL_URL}")
    private void setFindGoodsDetailUrl(String findGoodsDetailUrl) {
        FIND_GOODS_DETAIL_URL = findGoodsDetailUrl;
    }
    private static String SEND_ORDER_URL;
    @Value("${guoyao.SEND_ORDER_URL}")
    private void setSendOrderUrl(String sendOrderUrl) {
        SEND_ORDER_URL = sendOrderUrl;
    }
    private static String GET_ORDER_STATUS_URL;
    @Value("${guoyao.GET_ORDER_STATUS_URL}")
    private void setGetOrderStatusUrl(String getOrderStatusUrl) {
        GET_ORDER_STATUS_URL = getOrderStatusUrl;
    }
    private static String GET_ORDER_DETAIL_URL;
    @Value("${guoyao.GET_ORDER_DETAIL_URL}")
    private void setGetOrderDetailUrl(String getOrderDetailUrl) {
        GET_ORDER_DETAIL_URL = getOrderDetailUrl;
    }
    private static String APPLY_ORDER_RETURN_URL;
    @Value("${guoyao.APPLY_ORDER_RETURN_URL}")
    private void setApplyOrderReturnUrl(String applyOrderReturnUrl) {
        APPLY_ORDER_RETURN_URL = applyOrderReturnUrl;
    }

    private static final String GUOYAO_ACCESSTOKEN = "GUOYAO_ACCESSTOKEN";


    private static RedisTemplate redisTemplate;
    @Autowired
    public void setRedisTemplate(RedisTemplate redisTemplate) {
        GuoYaoUtil.redisTemplate = redisTemplate;
    }

    private static String getAccessToken() {
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("userName", USERNAME);
        jsonObject.set("password", PASSWORD);
        logger.info("<获取 accessToken>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject, GET_ACCESS_TOKEN_URL);
        logger.info("<获取 accessToken>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && resultObj.get("rtn", Integer.class) == 1) {
            return resultObj.get("accessToken", String.class);
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    private static String renewalToken(){
        String accesstoken = (String) redisTemplate.opsForHash().get(GUOYAO_ACCESSTOKEN,"token");
        if (StringUtils.isEmpty(accesstoken)){
            accesstoken = getAccessToken();
            HashMap<String, String> tokenMap = new HashMap<>();
            tokenMap.put("token",accesstoken);
            redisTemplate.opsForHash().putAll(GUOYAO_ACCESSTOKEN,tokenMap);
            redisTemplate.expire(GUOYAO_ACCESSTOKEN,30,TimeUnit.MINUTES);
        }
        return accesstoken;
    }

    /**
     * 查询商品明细
     * @param createDateBegin
     * @param createDateEnd
     * @param goods
     * @return
     */
    public static List<JSONObject> findGoodsDetail(String createDateBegin,String createDateEnd,String goods) {
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("accessToken", renewalToken());
        jsonObject.set("CreatedateBegin", createDateBegin);
        jsonObject.set("CreatedateEnd", createDateEnd);
        jsonObject.set("Goods", goods);
        logger.info("<获取商品信息>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject, FIND_GOODS_DETAIL_URL);
        logger.info("<获取商品信息>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && resultObj.get("rtn", Integer.class) == 1) {
            return resultObj.get("info", List.class);
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    /**
     * 推送单据
     * @param info
     * @return
     */
    public static boolean sendOrder(List<JSONObject> info) {
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("data",info);
        jsonObject.set("accessToken",renewalToken());
        logger.info("<发送订单>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject, SEND_ORDER_URL);
        logger.info("<发送订单>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && resultObj.get("rtn", Integer.class) == 1) {
            return true;
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    public static List<JSONObject> getOrderStatus(List<JSONObject> info) {
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("accessToken",renewalToken());
        jsonObject.set("data",info);
        logger.info("<订单状态>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject, GET_ORDER_STATUS_URL);
        logger.info("<订单状态>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && resultObj.get("rtn", Integer.class) == 1) {
            return resultObj.get("info", List.class);
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    public static List<JSONObject> getOrderDetail(List<JSONObject> info) {
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("accessToken",renewalToken());
        jsonObject.set("data",info);
        logger.info("<订单详情>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject, GET_ORDER_DETAIL_URL);
        logger.info("<订单详情>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && resultObj.get("rtn", Integer.class) == 1) {
            return resultObj.get("info", List.class);
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    public static boolean ApplyOrderReturn(List<JSONObject> info) {
        renewalToken();
        //设置请求参数
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.set("accessToken",renewalToken());
        jsonObject.set("data",info);
        logger.info("<退单>参数打印{}", jsonObject);
        //发送post请求
        String postResult = sendHttp(jsonObject, APPLY_ORDER_RETURN_URL);
        logger.info("<退单>结果打印{}", postResult);
        JSONObject resultObj = JSONUtil.toBean(postResult, JSONObject.class);
        if (resultObj != null && resultObj.get("rtn", Integer.class) == 1) {
            return true;
        }
        throw new BusinessException(resultObj.get("message",String.class));
    }

    private static String sendHttp(JSONObject jsonObject,String url){
        return HttpRequest.post(url)
                //设置请求头
                .header("Content-Type", "application/json")
                //传输参数
                .body(jsonObject.toString())
                .execute()
                .body();
    }
}
