package com.gys.util;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaMenu;
import com.gys.business.service.data.Menu.MenuTree;
import com.gys.business.service.data.MenuConfig.MenuConfigOutput;
import org.springframework.beans.BeanUtils;

import java.util.*;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/3/15 15:44
 * @Version 1.0.0
 **/
public class MenuTreeUtil {
    /**
     * 类名称：MenuTreeUtil
     * 类描述：递归构造树型结构
     */

    public static Map<String, Object> mapArray = new LinkedHashMap<String, Object>();
    public List<GaiaMenu> menuCommon;
    public List<MenuConfigOutput> menuConfigCommon;
    public List<Object> list = new ArrayList<Object>();

    public List<Object> menuList(List<GaiaMenu> menu) {
        this.menuCommon = menu;
        for (GaiaMenu x : menu) {
            Map<String, Object> mapArr = new LinkedHashMap<String, Object>();
            if (x.getParentId() == 0) {
                mapArr.put("id", x.getId());
                mapArr.put("code", x.getCode());
                mapArr.put("name", x.getName());
                mapArr.put("title", x.getTitle());
                mapArr.put("path", x.getPath());
                mapArr.put("pagePath", x.getPagePath());
                mapArr.put("icon", x.getIcon());
                mapArr.put("type", x.getType());
                mapArr.put("parentId", x.getParentId());
                mapArr.put("parentIdList", x.getParentIdList());
                mapArr.put("keepAlive", x.getKeepAlive());
                mapArr.put("detaisPage", x.getDetaisPage());
                mapArr.put("status", x.getStatus());
                mapArr.put("seq", x.getSeq());
                mapArr.put("lists", menuChild(x.getId()));
                list.add(mapArr);
            }
        }
        return list;
    }

    public List<?> menuChild(Long id) {
        List<Object> lists = new ArrayList<Object>();
        for (GaiaMenu a : menuCommon) {
            Map<String, Object> childArray = new LinkedHashMap<String, Object>();
            if (id == a.getParentId()) {
                childArray.put("id", a.getId());
                childArray.put("code", a.getCode());
                childArray.put("name", a.getName());
                childArray.put("title", a.getTitle());
                childArray.put("path", a.getPath());
                childArray.put("pagePath", a.getPagePath());
                childArray.put("icon", a.getIcon());
                childArray.put("type", a.getType());
                childArray.put("parentId", a.getParentId());
                childArray.put("parentIdList", a.getParentIdList());
                childArray.put("keepAlive", a.getKeepAlive());
                childArray.put("detaisPage", a.getDetaisPage());
                childArray.put("status", a.getStatus());
                childArray.put("seq", a.getSeq());
                childArray.put("lists", menuChild(a.getId()));
                lists.add(childArray);
            }
        }
        return lists;
    }

    public List<GaiaMenu> menuTreeToMenu (List<MenuTree> menuTrees,String userId){
        List<GaiaMenu> menus = new ArrayList<>();

        for (int i=0;i<menuTrees.size();i++){
            GaiaMenu menu = new GaiaMenu();
            MenuTree menuTree = new MenuTree();
            menuTree = menuTrees.get(i);
            BeanUtils.copyProperties(menuTree,menu);
            menu.setSeq(i);
            menu.setUpdator(userId);
            menu.setUpdateDate(new Date());
            menus.add(menu);
            if(ObjectUtil.isNotEmpty(menuTree.getLists())){
                //查询下一级菜单
                menus.addAll(menuTreeToMenu(menuTree.getLists(),null));
            }
        }

        return menus;
    }

    public List<Object> menuConfigList(List<MenuConfigOutput> menu) {
        this.menuConfigCommon = menu;
        for (MenuConfigOutput x : menu) {
            Map<String, Object> mapArr = new LinkedHashMap<String, Object>();
            if (x.getParentId() == 0) {
                mapArr.put("id", x.getId());
                mapArr.put("title", x.getTitle());
                mapArr.put("parentId", x.getParentId());
                mapArr.put("menuState", x.getMenuState());
                mapArr.put("auths", x.getAuths());
                mapArr.put("lists", menuConfigChild(x.getId()));
                list.add(mapArr);
            }
        }
        return list;
    }

    public List<?> menuConfigChild(Long id) {
        List<Object> lists = new ArrayList<Object>();
        for (MenuConfigOutput a : menuConfigCommon) {
            Map<String, Object> childArray = new LinkedHashMap<String, Object>();
            if (id == a.getParentId()) {
                childArray.put("id", a.getId());
                childArray.put("title", a.getTitle());
                childArray.put("parentId", a.getParentId());
                childArray.put("menuState", a.getMenuState());
                childArray.put("auths", a.getAuths());
                childArray.put("lists", menuConfigChild(a.getId()));
                lists.add(childArray);
            }
        }
        return lists;
    }
}