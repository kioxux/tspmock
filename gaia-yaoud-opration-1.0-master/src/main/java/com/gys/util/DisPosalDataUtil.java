package com.gys.util;

import cn.hutool.core.util.ObjectUtil;
import com.gys.common.annotation.NumberFormatRemark;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author ：gyx
 * @Date ：Created in 15:34 2021/12/28
 * @Description：根据注解@NumberFormatRemark来进行非空判断、格式化操作
 * @Modified By：gyx
 * @Version:
 */
public class DisPosalDataUtil {

    public static <T> T disposalData(Class<T> clazz,T data){
        try {
            Field[] declaredFields = clazz.getDeclaredFields();
            List<Field> fieldList = Arrays.stream(declaredFields)
                    .filter(field -> ObjectUtil.isNotEmpty(field.getAnnotation(NumberFormatRemark.class)))
                    .collect(Collectors.toList());
            for (Field field : fieldList) {
                field.setAccessible(true);
                Class<?> type = field.getType();
                Object value = field.get(data);
                if (value instanceof BigDecimal){
                    BigDecimal var=(BigDecimal) value;
                    var=new BigDecimal(NullUtil.decimalFormatting(var));
                    field.set(data,var);
                }else if (ObjectUtil.isEmpty(value) && type==BigDecimal.class){
                    BigDecimal var=BigDecimal.ZERO;
                    var=new BigDecimal(NullUtil.decimalFormatting(var));
                    field.set(data,var);
                }else if(value instanceof String){
                    String var=(String)value;
                    var = NullUtil.decimalFormatting(new BigDecimal(var));
                    field.set(data,var);
                }
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
