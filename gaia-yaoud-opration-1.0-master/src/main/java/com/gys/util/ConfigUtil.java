package com.gys.util;

import com.gys.business.service.takeAway.BaseService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ConfigUtil {
    private BaseService baseService;

    public BaseService getBaseService() {
        return baseService;
    }

    @Autowired
    public void setBaseService(BaseService baseService) {
        this.baseService = baseService;
    }

    @Deprecated
    public static Map<String, Map<String, Map<String, String>>> getSubCompany() {
        return subCompany;
    }

    public static Map<String, Map<String, String>> getSubCompany(String inSubCompany) {
        Map<String, Map<String, String>> result=subCompany.get(inSubCompany);
        if (result==null){
            configUtil.init();
            result=subCompany.get(inSubCompany);
        }
        return result;
    }

    public static ConfigUtil configUtil;

    private static Map<String, Map<String, Map<String, String>>> subCompany;

    private static Map<String, Map<String, Map<String, String>>> shop;

    @Deprecated
    public static Map<String, Map<String, Map<String, String>>> getshop() {
        return shop;
    }

    public static Map<String, Map<String, String>> getshop(String shop_no) {
        Map<String, Map<String, String>> result=shop.get(shop_no);
        if (result==null){
            configUtil.init();
            result=shop.get(shop_no);
        }
        return result;
    }

    private static Map<String, String> appSecretForJd;

    @Deprecated
    public static Map<String, String> getAppSecretForJd() {
        return appSecretForJd;
    }

    public static String getAppSecretForJd(String app_key) {
        String result=appSecretForJd.get(app_key);
        if (result==null){
            configUtil.init();
            result=appSecretForJd.get(app_key);
        }
        return result;
    }

    public static String getSecretForEB(String source) {
        String result="";
        if (!StringUtils.isBlank(source)) {
            for (String companyCode : subCompany.keySet()) {
                if (source.equals(subCompany.get(companyCode).get("EB").get("source"))) {
                    result = subCompany.get(companyCode).get("EB").get("secret");
                    break;
                }
            }
            if (result.equals("")){
                configUtil.init();
                for (String companyCode : subCompany.keySet()) {
                    if (source.equals(subCompany.get(companyCode).get("EB").get("source"))) {
                        result = subCompany.get(companyCode).get("EB").get("secret");
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 获取美团app_id对应app_secret
     * @param app_id
     * @return
     * @author：Li.Deman
     * @date：2018/1/23
     */
    public static String getSecretForMT(String app_id) {
        String result="";
        if (!StringUtils.isBlank(app_id)) {
            for (String companyCode : subCompany.keySet()) {
                if (app_id.equals(subCompany.get(companyCode).get("MT").get("app_id"))) {
                    result = subCompany.get(companyCode).get("MT").get("app_secret");
                    break;
                }
            }
            if (result.equals("")){
                configUtil.init();
                for (String companyCode : subCompany.keySet()) {
                    if (app_id.equals(subCompany.get(companyCode).get("MT").get("app_id"))) {
                        result = subCompany.get(companyCode).get("MT").get("app_secret");
                        break;
                    }
                }
            }
        }
        return result;
    }

    public void init() {
        configUtil = this;
        configUtil.baseService = this.baseService;
        subCompany = new HashMap<>();
        shop=new HashMap<>();
        appSecretForJd=new HashMap<>();
        List<Map<String, Object>> loadrows = baseService.loadConfig();
        Map<String, Map<String, String>> platform = new HashMap<>();
        Map<String, String> config = new HashMap<>();
        String lastSubCompany = "";
        String lastPlatform = "";
        for (Map<String, Object> row : loadrows) {
            if (lastSubCompany == "") lastSubCompany = String.valueOf(row.get("subcompany_code"));
            if (lastPlatform == "") lastPlatform = String.valueOf(row.get("type"));
            if (!lastSubCompany.equals(String.valueOf(row.get("subcompany_code")))) {
                platform.put(lastPlatform, config);
                config = new HashMap<>();
                lastPlatform = String.valueOf(row.get("type"));
                subCompany.put(lastSubCompany, platform);
                platform = new HashMap<>();
                lastSubCompany = String.valueOf(row.get("subcompany_code"));
            }
            if (!lastPlatform.equals(String.valueOf(row.get("type")))) {
                platform.put(lastPlatform, config);
                config = new HashMap<>();
                lastPlatform = String.valueOf(row.get("type"));
            }
            config.put(String.valueOf(row.get("parameter")), String.valueOf(row.get("value")));
        }
        platform.put(lastPlatform, config);
        subCompany.put(lastSubCompany, platform);

        Map<String,Object> loadMap=new HashMap<>();
        List<Map<String, Object>> loadshops = baseService.loadShop(loadMap);
        for (Map<String, Object> sp : loadshops){
            shop.put(String.valueOf(sp.get("shop_no")),subCompany.get(String.valueOf(sp.get("subcompany_code"))));
        }

        //改造，数据库查询出来再过滤
        List<Map<String, Object>> AppSecretsByIndex = baseService.loadAppSecretForJdByIndex();
        //parameter = 'app_key' parameter = 'app_secret'
        for(Map<String, Object> param : AppSecretsByIndex){
            //同样分部代码，取app_key和app_secret
            List<Map<String, Object>> tempList = AppSecretsByIndex.stream().filter((Map<String, Object> a)->
                    String.valueOf(param.get("subcompany_code")).equals(String.valueOf(a.get("subcompany_code")))
                            ).collect(Collectors.toList());
            String temp_app_key="";
            String temp_app_secret="";
            for(Map<String, Object> paramTemp : tempList){
                //获取该分部的app_key
                if (String.valueOf(paramTemp.get("parameter")).equals("app_key")){
                    temp_app_key=String.valueOf(paramTemp.get("value"));
                }
                //获取该分部的app_secret
                if (String.valueOf(paramTemp.get("parameter")).equals("app_secret")){
                    temp_app_secret=String.valueOf(paramTemp.get("value"));
                }
            }
            //这个分部的app_key和app_secret组合一下
            appSecretForJd.put(temp_app_key, temp_app_secret);
        }

        List<Map<String, Object>> AppSecrets = baseService.loadAppSecretForJd();
        for(Map<String, Object> param : AppSecrets){
            appSecretForJd.put(String.valueOf(param.get("app_key")), String.valueOf(param.get("app_secret")));
        }
    }

}
