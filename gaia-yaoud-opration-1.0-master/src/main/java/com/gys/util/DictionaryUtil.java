package com.gys.util;

import com.gys.business.mapper.GaiaSdNewStoreDistributionRecordHMapper;
import com.gys.business.service.data.GaiaDictionary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author SunJiaNan
 * @date 20210716
 * 字典表工具类
 */
@Component
@Slf4j
public class DictionaryUtil {

    @Autowired
    private GaiaSdNewStoreDistributionRecordHMapper newStoreDistributionRecordHMapper;

    private static GaiaSdNewStoreDistributionRecordHMapper staticNewStoreDistributionRecordHMapper;

    @PostConstruct
    public void init() {
        staticNewStoreDistributionRecordHMapper = newStoreDistributionRecordHMapper;
    }

    /**
     * 声明全局变量Map,存储表GAIA_DICTIONARY的数据  将Type+Code作为key  将name作为value
     */
    private static Map<String, String> CACHE_MAP = new ConcurrentHashMap<String, String>(100);


    /**
     * @param type 字典类型
     * @param code 字典code
     * @return
     */
    public String getValueByTypeCode(String type, String code) {
        if (ValidateUtil.isEmpty(type) || ValidateUtil.isEmpty(code)) {
            return null;
        }

        String typeCodeStr = type + code;
        String value = CACHE_MAP.get(typeCodeStr);
        //如果不为空 则直接返回value
        if(ValidateUtil.isNotEmpty(value)) {
            log.info("字典表中存在此数据, TYPE：" + type + ", CODE：" + code + ", NAME：" + value);
            return value;
        }
        //如果获取失败，则去数据库中查询全部数据
        List<GaiaDictionary> dictionaryList = staticNewStoreDistributionRecordHMapper.getAllDictionary();
        if(ValidateUtil.isNotEmpty(dictionaryList)) {
            //转为map重新赋值给CACHE_MAP  将Type+Code作为key  将name作为value
            CACHE_MAP = dictionaryList.stream().collect(Collectors.toMap(GaiaDictionary::getTypeCodeStr, GaiaDictionary::getName));
            log.info("重新加载字典表, 共：" + CACHE_MAP.size() + "条数据");
        }
        return CACHE_MAP.get(typeCodeStr);
    }

    /**
     * @Author jiht
     * @Description 根据编码差码表
     * @Date 2022/1/14 15:17
     * @Param [type]
     * @Return java.util.List<com.gys.business.service.data.GaiaDictionary>
     **/
    public List<GaiaDictionary> geGaiaDictionaryByType(String type) {
        List<GaiaDictionary> dictionaryList = staticNewStoreDistributionRecordHMapper.getDictionaryByType(type);
        return dictionaryList;
    }

}
