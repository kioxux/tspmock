package com.gys.util.yaoka;

/**
 * 药卡支付环境配置
 * @author 胡鑫鑫
 */

public enum QueryEnv {
    TEST("TEST", "测试环境", "https://openapi.test.pajk.cn/api/v1/",true),
    PROD("PROD", "生产环境", "https://openapi.jk.cn/api/v1/",true);


    private String url;
    private String code;
    private String msg;
    private boolean printFlag;

    private QueryEnv(String code, String msg, String url, boolean printFlag) {
        this.code = code;
        this.msg = msg;
        this.url = url;
        this.printFlag = printFlag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isPrintFlag() {
        return printFlag;
    }

    public void setPrintFlag(boolean printFlag) {
        this.printFlag = printFlag;
    }

    public static QueryEnv getQueryEnv(String code){
            for (QueryEnv queryEnv : QueryEnv.values()) {
            if (!queryEnv.getCode().equals(code)) {
                continue;
            }
            return queryEnv;
        }
        return null;

    }
}
