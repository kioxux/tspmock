package com.gys.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gys.common.webSocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * 贰佰平台签名
 */
public class SignErBaiUtil {
    private static final Logger log = LoggerFactory.getLogger(SignErBaiUtil.class);
    /**
     * 根据参数获取签名
     * @return String
     */
    public static String getSign(Map<String, String> data) {
        TreeMap<String, Object> arr = new TreeMap<String, Object>();
        arr.put("body", data.get("body"));//JSON.toJSONString(data.get("body"), SerializerFeature.BrowserCompatible)
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", data.get("secret"));
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            strSignTmp.append(key + "=" + arr.get(key) + "&");
        }
        String strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        log.debug(strSign);
        String sign = getMD5(strSign.toString());
        log.debug(sign);
        return sign;
    }

    /**
     * 根据参数获取签名
     * @return String
     */
    public static String getSign1(Map<String, Object> data) {
        TreeMap<String, Object> arr = new TreeMap<String, Object>();
        arr.put("body", data.get("body"));//JSON.toJSONString(data.get("body"), SerializerFeature.BrowserCompatible)
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", data.get("secret"));
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            strSignTmp.append(key + "=" + arr.get(key) + "&");
        }
        String strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        log.debug(strSign);
        String sign = getMD5(strSign.toString());
        log.debug(sign);
        return sign;
    }
    /**
     * 根据参数获取签名
     *
     * @param
     * @return String
     */
    public static String getSign(Map<String, Object> data, String source,String secret) {
        TreeMap<String, Object> arr = new TreeMap<String, Object>();
        arr.put("body", data.get("body"));
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", secret);
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            strSignTmp.append(key + "=" + arr.get(key) + "&");
        }
        String strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        log.debug(strSign);
        String sign = getMD5(strSign.toString());
        log.debug(sign);
        return sign;
    }

    /**
     * 根据参数获取签名
     *
     * @param
     * @return String
     */
    public static String getSign2(Map<String, Object> data, String source,String secret) {
        TreeMap<String, Object> arr = new TreeMap<String, Object>();
        arr.put("body", data.get("body"));
        arr.put("cmd", data.get("cmd"));
        arr.put("encrypt", "");
        arr.put("secret", secret);
        arr.put("source", data.get("source"));
        arr.put("ticket", data.get("ticket"));
        arr.put("timestamp", data.get("timestamp"));
        arr.put("version", data.get("version"));
        StringBuilder strSignTmp = new StringBuilder("");
        Iterator it = arr.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            if("body".equals(key)){
                strSignTmp.append(key + "=" + JSONObject.toJSONString(arr.get(key),SerializerFeature.WriteMapNullValue) + "&");
            }else{
                strSignTmp.append(key + "=" + arr.get(key) + "&");
            }
        }
        String strSign = strSignTmp.toString().substring(0, strSignTmp.length() - 1);
        log.debug("strSign:"+strSign);
        String sign = getMD5(strSign.toString());
        log.debug("sign:"+sign);
        return sign;
    }

    /**
     * 校验签名是否正确
     *
     * @param
     * @return boolean
     */
    public static boolean checkSign(Map<String, Object> data, String source,String secret) {
        String signFrom = getSign(data, source,secret);
        String sign = data.get("sign").toString();
        if (signFrom.equals(sign)) {
            return true;
        } else {
            System.out.println(data);
            return false;
        }
    }

    /**
     * 校验签名是否正确
     *
     * @param
     * @return boolean
     */
    public static boolean checkSign2(Map<String, Object> data, String source,String secret) {
        String signFrom = getSign2(data, source,secret);
        String sign = data.get("sign").toString();
        if (signFrom.equals(sign)) {
            return true;
        } else {
            System.out.println(data);
            return false;
        }
    }

    /**
     * 获取MD5
     *
     * @param
     * @return String
     */
    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext.toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 使用 Map按key进行排序
     * @param map
     * @return
     */
    public static Map<String, String> sortMapByKey(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }

        Map<String, String> sortMap = new TreeMap<String, String>(
                new MapKeyComparator());

        sortMap.putAll(map);

        return sortMap;
    }
}
