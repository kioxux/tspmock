package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 生产类别
 * @date 2021/11/1 13:17
 */
public enum ProProduceClassEnum implements BaseEnum {

    ACCESSORIES("1", "辅料"),
    CHEMICAL("2", "化学药品"),
    BIOLOGICAL_PRODUCT("3", "生物制品"),
    TRADITIONAL_CHINESE_MEDICINE("3", "中药"),
    INSTRUMENT("3", "器械"),

    ;

    public final String type;
    public final String name;

    ProProduceClassEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
