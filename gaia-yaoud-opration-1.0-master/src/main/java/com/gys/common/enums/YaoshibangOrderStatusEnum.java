package com.gys.common.enums;

/**
 * @author zhangdong
 * @date 2021/6/17 10:00
 */
public enum YaoshibangOrderStatusEnum {

    //1-正在开单2-拣货完成，待配送 3-订单交与拣货,分捡中4正在配送 5客户已签收，配送完成（药师帮定义的状态）6 客户不存在 7 客户首营审批中 8 客户首营成功 9 客户首营审批失败 10 创建成功
    OPENING_ORDER("1", "正在开单"),
    DIEKING_NEED_DELIVERY("2", "拣货完成，待配送"),
    DIEKING("3", "订单交与拣货,分捡中"),
    DELIVERING("4", "正在配送"),
    CUSTOMER_SIGN("5", "客户已签收，配送完成"),
    CUSTOMER_NO_EXIST("6", "客户不存在"),
    FIRST_CAMP_AUDITING("7", "客户首营审批中"),
    FIRST_CAMP_SUCCESS("8", "客户首营成功"),
    FIRST_CAMP_FAIL("9", "客户首营审批失败"),
    CREATE_SUCCESS("10", "创建成功");

    private String status;
    private String statusRemark;

    YaoshibangOrderStatusEnum(String status, String statusRemark) {
        this.status = status;
        this.statusRemark = statusRemark;
    }

    public static String getStatusRemark(String status) {
        YaoshibangOrderStatusEnum[] values = YaoshibangOrderStatusEnum.values();
        for (YaoshibangOrderStatusEnum value : values) {
            if (value.status.equals(status)) {
                return value.statusRemark;
            }
        }
        return null;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusRemark() {
        return statusRemark;
    }

    public void setStatusRemark(String statusRemark) {
        this.statusRemark = statusRemark;
    }}
