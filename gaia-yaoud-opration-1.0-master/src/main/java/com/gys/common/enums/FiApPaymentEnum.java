package com.gys.common.enums;

/**
 * @Description: 门店补货参数枚举
 * @Author: wangQc
 * @Date: 2019/12/31
 * @Time: 18:12
 */

public enum FiApPaymentEnum {
    PAYED("6666", "期间付款"),
    PERIOD_BEGIN("8888", "期初金额"),
    BILL("9999", "期间发生额");

    private String code;

    private String value;

    private FiApPaymentEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    /**
     * 根据状态值 查询状态名称
     *
     * @param code
     * @return
     */
    public static String getValueByCode(String code) {
        for (FiApPaymentEnum sdReplenishConfigEnum : FiApPaymentEnum.values()) {
            if (sdReplenishConfigEnum.getCode().equals(code) && !(sdReplenishConfigEnum.getCode().equals("8888"))) {
                return sdReplenishConfigEnum.getValue();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
