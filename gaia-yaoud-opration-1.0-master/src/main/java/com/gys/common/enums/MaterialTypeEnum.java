package com.gys.common.enums;

/**
 * 物料凭证类型
 * @author chenhao
 */
public enum MaterialTypeEnum {
    CAI_GOU("采购", "CD"),
    TUI_CHANG("退厂", "GD"),
    XIAO_SHOU("销售", "XD"),
    XIAO_TUI("销退", "ED"),
    PEI_SONG("配送", "PD"),
    TUI_KU("退库", "TD"),
    LING_SHOU("零售", "LS"),
    ZHONG_YAO_DAI_JIAN("中药代煎", "LX"),
    HU_DIAO_TUI_KU("互调退库", "MD"),
    HU_DIAO_PEI_SONG("互调配送", "ND"),
    BAO_SUN("报损", "BD"),
    ZI_YONG("自用", "ZD"),
    SUN_YI("损溢", "SY"),
    YI_KU("移库", "ZT");
    private String name;

    private String code;

    MaterialTypeEnum(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
