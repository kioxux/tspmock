package com.gys.common.enums;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.StringUtils;

/**
 * 报损报溢类型
 */
public enum IncomeStatementTypeEnum {
    PAN_DIAN_DAO_RU("1", "损溢单", "报损报溢", "报损报溢", "GAIA_WF_007", "GAIA_WF_006", ""),
    MEN_DIAN_LING_YONG("3", "门店领用", "门店领用单", "门店领用单", "GAIA_WF_038", "GAIA_WF_037", "SYLY"),
    MEN_DIAN_BAO_SUN("4", "门店报损", "门店报损单", "门店报损单", "GAIA_WF_036", "GAIA_WF_035", "SYBS"),
    MEN_DIAN_BAO_YI("6", "门店报溢", "门店报溢单", "门店报溢单", "GAIA_WF_050", "GAIA_WF_051", "SYBY");

    public static String getTypeName(String type){
        for(IncomeStatementTypeEnum typeEnum : IncomeStatementTypeEnum.values()){
            if(StringUtils.equalsIgnoreCase(typeEnum.getType(), type)){
                return typeEnum.getName();
            }
        }
        return "";
    }

    /**
     * 获取单号规则
     * @param type
     * @return
     */
    public static String getOrderNo(String type){
        for(IncomeStatementTypeEnum typeEnum : IncomeStatementTypeEnum.values()){
            if(StringUtils.equalsIgnoreCase(typeEnum.getType(), type)){
                return typeEnum.getOrderNo() + DateUtil.thisYear();
            }
        }
        return "";
    }

    /**
     * 获取单号标题
     * @param type
     * @return
     */
    public static String getTitle(String type){
        for(IncomeStatementTypeEnum typeEnum : IncomeStatementTypeEnum.values()){
            if(StringUtils.equalsIgnoreCase(typeEnum.getType(), type)){
                return typeEnum.getTitle();
            }
        }
        return "";
    }

    /**
     * 获取工作流编号
     * @param type
     * @param isChain true:连锁, false:单体
     * @return
     */
    public static String getWfCode(String type, boolean isChain){
        for(IncomeStatementTypeEnum typeEnum : IncomeStatementTypeEnum.values()){
            if(StringUtils.equalsIgnoreCase(typeEnum.getType(), type)){
                if(isChain){
                    return typeEnum.getChainWfCode();
                }
                return typeEnum.getSingleWfCode();
            }
        }
        return "";
    }

    private String type;

    private String name;

    private String title;

    private String description;

    @ApiModelProperty(value = "单体店工作流编号")
    private String singleWfCode;

    @ApiModelProperty(value = "连锁店工作流编号")
    private String chainWfCode;

    @ApiModelProperty(value = "单号规则")
    private String orderNo;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSingleWfCode() {
        return singleWfCode;
    }

    public void setSingleWfCode(String singleWfCode) {
        this.singleWfCode = singleWfCode;
    }

    public String getChainWfCode() {
        return chainWfCode;
    }

    public void setChainWfCode(String chainWfCode) {
        this.chainWfCode = chainWfCode;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    IncomeStatementTypeEnum(String type, String name, String title, String description, String singleWfCode, String chainWfCode, String orderNo) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.description = description;
        this.singleWfCode = singleWfCode;
        this.chainWfCode = chainWfCode;
        this.orderNo = orderNo;
    }
}
