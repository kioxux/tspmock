package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Description: 消息类型
 * @date 2021/10/20 17:40
 */
public enum MessageTypeEnum {

    PAID(1, "付费"),
    COMPANY_MAINTENANCE(2, "公司维护"),
    PRECISION_MARKETING(3, "精准营销"),
    TRAINING(4, "培训"),
    BUSINESS_MONTHLY_REPORT(5, "经营月报"),
    PROMOTION_REPORT(6, "促销报告"),

    ;

    private final Integer type;
    private final String name;

    MessageTypeEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (MessageTypeEnum value : MessageTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    public static Integer matchType(Integer type) {
        for (MessageTypeEnum value : MessageTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value.type;
            }
        }
        return null;
    }

    public Integer getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

}
