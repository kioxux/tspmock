package com.gys.common.enums;

import java.util.*;

public class EnumUtil {
    private final static ArrayList cashier = new ArrayList(
            Arrays.asList( "GAIA_SD_MDDR","SD_01","GAIA_SD_MDSY","GAIA_SD_MDYS","GAIA_SD_ZLGL"));
    private final static ArrayList assistant =new ArrayList(
            Arrays.asList( "GAIA_SD_MDDR","SD_01","GAIA_SD_MDSY","GAIA_SD_MDYS","GAIA_SD_ZLGL"));
    private final static ArrayList doctor = new ArrayList(Arrays.asList( "GAIA_SD_MDDR"));

    public enum MenuFlow {
        LOGISTICS_MANAGEMENT("GAIA_WM_01", "1"),
        STORE_MANAGEMENT("GAIA_SD_01", "2"),
        PRODUCT_MANAGEMENT("GAIA_MM_01", "3"),
        PURCHASE_MANAGEMENT("GAIA_MM_02", "3"),
        WHOLESALE_MANAGEMENT("GAIA_MM_03", "3"),
        FINANCE_MANAGEMENT("GAIA_FI_01", "4"),
        HOME_PAGE("homePage", "5");
        private String code;
        private String message;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }
        MenuFlow(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public static String getValue(String code) {
            for (MenuFlow ele : values()) {
                if (ele.getCode().equals(code)) return  ele.getMessage();
            }
            return null;


        }
    }
    public enum ProHeader {
        PRO_SELF_CODE("proCode", "PRO_SELF_CODE"),
        PRO_COMMONNAME("proCommonName", "PRO_COMMONNAME"),
        PRO_NAME("name", "PRO_NAME"),
        PRO_PYM("pym", "PRO_PYM"),
        PRO_SPECS("specs", "PRO_SPECS"),
        PRO_UNIT("unit", "PRO_UNIT"),
        PRO_FORM("form", "PRO_FORM"),
        PRO_PLACE("proPlace", "PRO_PLACE"),
        PRO_FACTORY_NAME("proFactoryName", "PRO_FACTORY_NAME");


        private String code;
        private String message;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }
        ProHeader(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public static String getValue(String code) {
            for (ProHeader ele : values()) {
                if (ele.getCode().equals(code)) return  ele.getMessage();
            }
            return null;


        }
    }

    public enum ProThirdlyHeader1 {
        PRO_SELF_CODE("proCode", "商品编码"),
        PRO_COMMONNAME("proCommonName", "通用名称"),
        PRO_NAME("name", "商品名称"),
        PRO_PYM("pym", "助记码"),
        PRO_SPECS("specs", "规格"),
        PRO_UNIT("unit", "计量单位"),
        PRO_FORM("form", "剂型"),
        PRO_PLACE("proPlace", "产地"),
        PRO_FACTORY_NAME("proFactoryName", "生产企业"),
        PRO_KYSL("kysl", "总部数量"),
        PRO_PRICE_NORMAL("priceNormal", "零售价"),
        PRO_ADD_AMT("addAmt", "配送价");

        private String code;
        private String message;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }
        ProThirdlyHeader1(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public static String getValue(String code) {
            for (ProThirdlyHeader1 ele : values()) {
                if (ele.getCode().equals(code)) return  ele.getMessage();
            }
            return null;
        }

        public static List getList(){
            List list = new ArrayList();
            Map<String,String> map = new HashMap<>();

            for (ProThirdlyHeader1 ele : values()) {
                map = new HashMap<>();
                map.put(ele.code,ele.message);
                list.add(map);

            }

            return list;
        }
    }

    public enum ProThirdlyHeader2 {
        PRO_SELF_CODE("proCode", "商品编码"),
        PRO_COMMONNAME("proCommonName", "通用名称"),
        PRO_NAME("name", "商品名称"),
        PRO_PYM("pym", "助记码"),
        PRO_SPECS("specs", "规格"),
        PRO_UNIT("unit", "计量单位"),
        PRO_FORM("form", "剂型"),
        PRO_PLACE("proPlace", "产地"),
        PRO_FACTORY_NAME("proFactoryName", "生产企业"),
        PRO_KYSL("kysl", "总部数量"),
        PRO_PRICE_NORMAL("priceNormal", "零售价"),
        UNIT_PRICE("unitPrice","成本价"),
        ADDRATE("addRate","加点额"),
        PRO_ADD_AMT("addAmt", "配送价");

        private String code;
        private String message;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }
        ProThirdlyHeader2(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public static String getValue(String code) {
            for (ProThirdlyHeader2 ele : values()) {
                if (ele.getCode().equals(code)) return  ele.getMessage();
            }
            return null;
        }

        public static List getList(){
            List list = new ArrayList();
            Map<String,String> map = new HashMap<>();

            for (ProThirdlyHeader2 ele : values()) {
                map = new HashMap<>();
                map.put(ele.code,ele.message);
                list.add(map);

            }

            return list;
        }
    }

    public enum StaffEnum {
        CASHIER("cashier", "收银员",cashier),
        ASSISTANT("assistant", "营业员",assistant),
        DOCTOE("doctor", "商品名称",doctor);

//        CASHIER("cashier", "收银员", new ArrayList(
//                Arrays.asList( "GAIA_SD_MDDR","SD_01","GAIA_SD_MDSY","GAIA_SD_MDYS","GAIA_SD_ZLGL"))),
//        ASSISTANT("assistant", "营业员",new ArrayList(
//                Arrays.asList( "GAIA_SD_MDDR","SD_01","GAIA_SD_MDSY","GAIA_SD_MDYS","GAIA_SD_ZLGL"))),
//        DOCTOE("doctor", "商品名称",new ArrayList(
//                Arrays.asList( "GAIA_SD_MDDR","SD_01","GAIA_SD_MDSY","GAIA_SD_MDYS","GAIA_SD_ZLGL")));
        private String code;
        private String message;
        private ArrayList groupId;

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }

        public ArrayList getGroupId() {
            return groupId;
        }

        public void setGroupId(ArrayList groupId) {
            this.groupId = groupId;
        }

        StaffEnum(String code, String message,ArrayList groupId) {
            this.code = code;
            this.message = message;
            this.groupId = groupId;
        }

        public static ArrayList getValue(String code) {
            for (StaffEnum ele : values()) {
                if (ele.getCode().equals(code)) return  ele.getGroupId();
            }
            return null;
        }

        public static List getList(){
            List list = new ArrayList();
            Map<String,String> map = new HashMap<>();

            for (StaffEnum ele : values()) {
                map = new HashMap<>();
                map.put(ele.code,ele.message);
                list.add(map);

            }

            return list;
        }
    }
}
