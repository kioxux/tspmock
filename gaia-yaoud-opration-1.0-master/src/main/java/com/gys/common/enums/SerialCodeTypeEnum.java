package com.gys.common.enums;

import com.alibaba.excel.util.DateUtils;

import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/29 19:05
 */
public enum SerialCodeTypeEnum {

    INTEGRAL_EXCHANGE("INTEGRAL_EXCHANGE", DateTypeEnum.MONTH, "积分换购流水号生成规则、按照月份重置"),
    SAFETY_HEALTH("SAFETY_HEALTH", DateTypeEnum.YEAR, "安全卫生检查流水号生成规则、按照年份重置"),;
    public final String type;
    public final DateTypeEnum dateTypeEnum;
    public final String name;

    SerialCodeTypeEnum(String type, DateTypeEnum dateTypeEnum, String name) {
        this.type = type;
        this.dateTypeEnum = dateTypeEnum;
        this.name = name;
    }

    public String getCodePrefix() {
        Date date = new Date();
        switch (this.dateTypeEnum) {
            case YEAR:
            case MONTH:
            case DAY:
            case HOUR:
            case MINUTE:
            case SECOND:
                return DateUtils.format(date, this.dateTypeEnum.format);
            default:
                return "";
        }
    }

    public enum DateTypeEnum {
        YEAR(1, "yyyy", "年（每年重置流水号）"),
        MONTH(2, "yyyyMM", "月（每月重置流水号）"),
        DAY(3, "yyyyMMdd", "日（每日重置流水号）"),
        HOUR(4, "yyyyMMddHH", "小时（每小时重置流水号）"),
        MINUTE(5, "yyyyMMddHHmm", "分钟（每分钟重置流水号）"),
        SECOND(6, "yyyyMMddHHmmss", "秒（每秒重置流水号）"),;

        public final Integer type;
        public final String format;
        public final String name;

        DateTypeEnum(Integer type, String format, String name) {
            this.type = type;
            this.format = format;
            this.name = name;
        }
    }

}
