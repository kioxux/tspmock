package com.gys.common.enums;


import com.gys.common.annotation.StoreInfoRemark;

/**
 * @author wu mao yin
 * @Description: 纳税属性
 * @date 2021/10/20 17:40
 */
@StoreInfoRemark(value = "纳税属性", label = "stoTaxClass")
public enum StoreTaxClassEnum implements BaseEnum {

    GENERAL_TAXPAYER("1", "一般纳税人"),
    SMALL_TAXPAYER("2", "小规模纳税人"),
    INDIVIDUAL_INDUSTRIAL_AND_COMMERCIAL_HOUSEHOLDS("3", "个体工商户"),

    ;

    public final String type;
    public final String name;

    StoreTaxClassEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (StoreTaxClassEnum value : StoreTaxClassEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
