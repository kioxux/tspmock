package com.gys.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 店门调剂建议明细-单据状态
 */
public enum StoOutSuggestionEnum {
    ZERO(0,"未推送"),
    ONE(1,"已推送"),
    TWO(2,"待确认"),
    THREE(3,"已确认"),
    FOUR(4,"已失效");
    private Integer code;
    private String type;

    StoOutSuggestionEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "StoOUtSuggestionEnum{" +
                "code=" + code +
                ", type='" + type + '\'' +
                '}';
    }

    public static List toJson(){
        List<Map> list = new ArrayList<>();
        for (StoOutSuggestionEnum e : StoOutSuggestionEnum.values()){
            Map<String ,Object> map = new HashMap<>(8);
            map.put("typeCode",e.getCode());
            map.put("type",e.getType());
            list.add(map);
       }
        return list;
    }
}

