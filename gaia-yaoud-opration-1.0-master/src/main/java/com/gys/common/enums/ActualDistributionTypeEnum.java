package com.gys.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ActualDistributionTypeEnum {
    ONE(1,"门店属性"),
    TWO(2,"铺货店型"),
    THREE(3,"门店"),
    FOUR(4,"店效级别");
    private Integer typeCode;
    private String type;

    ActualDistributionTypeEnum(Integer typeCode, String type) {
        this.typeCode = typeCode;
        this.type = type;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(Integer typeCode) {
        this.typeCode = typeCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static List toJson(){
        List<Map> list = new ArrayList<>();
        for(ActualDistributionTypeEnum e : ActualDistributionTypeEnum.values()){
            Map<String , Object> map = new HashMap<>(2);
            map.put("typeCode" ,e.getTypeCode());
            map.put("type",e.getType());
            list.add(map);
        }
        return list;
    }
}
