package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 批准文号分类
 * @date 2021/11/1 13:17
 */
public enum ProRegisterClassEnum implements BaseEnum {

    DOMESTIC_CHEMICALS("1", "国产化学药品"),
    IMPORTED_CHEMICALS("2", "进口化学药品"),
    DOMESTIC_EQUIPMENT("3", "国产器械"),
    IMPORTED_EQUIPMENT("4", "进口器械"),
    CHINESE_HERBAL_MEDICINE("5", "中药饮片"),
    SPECIAL_COSMETICS("6", "特殊化妆品"),
    DISINFECTION_SUPPLIES("7", "消毒用品"),
    HEALTHY_FOOD("8", "保健食品"),
    QS_PRODUCTS("9", "QS商品"),
    OTHER("10", "其它"),
    DOMESTICALLY_PRODUCED_CHINESE_MEDICINES("11", "国产中成药品"),
    IMPORTED_CHINESE_MEDICINES("12", "进口中成药品"),
    BIOLOGICS("13", "生物制剂"),
    ACCESSORIES("14", "辅料"),

    ;

    public final String type;
    public final String name;

    ProRegisterClassEnum( String type,String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
