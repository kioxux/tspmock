package com.gys.common.enums;


import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

// 会员卡类型

public enum MemberCardStatusEnum {

    MEMBER_CARD_STATUS_USABLE("0", "可用"),
    MEMBER_CARD_STATUS_LOSS("2", "挂失"),
    MEMBER_CARD_STATUS_CANCEL("3", "注销");

    public final String code;
    public final String desc;

    MemberCardStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ArrayList getStatusList(){
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        for (MemberCardStatusEnum value : MemberCardStatusEnum.values()) {
            HashMap<String, String> statusMap = new HashMap<>();
            statusMap.put(value.code,value.desc);
            arrayList.add(statusMap);
        }
        return arrayList;
    }

    public String getDescByCode(String code) {
        for (MemberCardStatusEnum value : MemberCardStatusEnum.values()) {
            if (StringUtils.equals(value.code,code)){
                return value.desc;
            }
        }
        return "";
    }


}
