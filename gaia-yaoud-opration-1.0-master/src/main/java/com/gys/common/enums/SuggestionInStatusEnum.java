package com.gys.common.enums;

/**
 * @desc: 调剂调入状态枚举
 * @author: Ryan
 * @createTime: 2021/11/1 23:17
 */
public enum SuggestionInStatusEnum {

    WAIT_CONFIRM(0, "待确认"),
    CONFIRMED(1, "已确认"),
    INVALID(2, "失效"),;

    public final Integer status;
    public final String message;

    SuggestionInStatusEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }
}
