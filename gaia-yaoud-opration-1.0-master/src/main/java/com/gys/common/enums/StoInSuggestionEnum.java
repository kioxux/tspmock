package com.gys.common.enums;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 调剂建议调入门店--状态类型
 */
public enum StoInSuggestionEnum {

    ZERO(0,"待处理"),
    ONE(1,"已确认"),
    TWO(2,"已失效"),
    THREE(3,"已完成");
    private Integer code;
    private String type;

    StoInSuggestionEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "StoInSuggestionEnum{" +
                "code=" + code +
                ", type='" + type + '\'' +
                '}';
    }

    public static List toJson(){
        List<Map> list = new ArrayList<>();
        for (StoInSuggestionEnum e : StoInSuggestionEnum.values()){
            Map<String ,Object> map = new HashMap<>(8);
            map.put("typeCode",e.getCode());
            map.put("type",e.getType());
            list.add(map);
        }
        return list;
    }
}
