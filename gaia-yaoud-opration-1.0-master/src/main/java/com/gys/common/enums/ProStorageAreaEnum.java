package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 商品仓储分区
 * @date 2021/11/1 13:17
 */
public enum ProStorageAreaEnum implements BaseEnum {

    ORAL_MEDICINE("1", "内服药品"),
    TOPICAL_DRUGS("2", "外用药品"),
    CHINESE_MEDICINE_DECOCTION_AREA("3", "中药饮片区"),
    HARDCOVER_CHINESE_MEDICINE_DISTRICT("4", "精装中药区"),
    INJECTION("5", "针剂"),
    CLASS_II_PSYCHOTROPIC_DRUGS("6", "二类精神药品"),
    ANESTHETIC_DRUGS("7", "含麻药品"),
    REFRIGERATED_GOODS("8", "冷藏商品"),
    NON_MEDICINAL_FOR_EXTERNAL_USE("9", "外用非药"),
    MEDICAL_INSTRUMENTS("10", "医疗器械"),
    FOOD("11", "食品"),
    HEALTHY_FOOD("12", "保健食品"),
    FLAMMABLE_COMMODITY_AREA("13", "易燃商品区"),
    OTHER("14", "其他"),

    ;

    public final String type;
    public final String name;

    ProStorageAreaEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
