package com.gys.common.enums;

/**
 * @desc: 加盟商参数枚举
 * @author: Ryan
 * @createTime: 2021/12/13 17:10
 */
public enum ClientParamsEnum {

    WTPS_PRICE_VALID_CLOSE("WTPS_PRICE_VALID_CLOSE", "是否关闭委托配送请货审核价格必填校验"),
    WTPSDD_NOT_MATCH_CODE_NOTICE("WTPSDD_NOT_MATCH_CODE_NOTICE", "是否开启单据未对码提醒");
    public final String id;
    public final String name;

    ClientParamsEnum(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
