package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 商品定位
 * @date 2021/11/3015:35
 */
public enum StorePositionEnum {

    H("H", "H-核心商品"),
    Z("Z", "Z-正常商品"),
    X("X", "X-新商品"),
    B("B", "B-补充商品"),
    D("D", "D-订购商品"),
    T("T", "T-淘汰商品"),

    ;

    private final String type;

    private final String name;

    StorePositionEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

}
