package com.gys.common.enums;

import com.gys.common.annotation.StoreInfoRemark;

/***
 * @desc: 门店管理区域
 * @author: ryan
 * @createTime: 2021/6/10 14:36
 **/
public enum StoreManagementEnum implements BaseEnum {

    PARK("1", "园区"),
    URBAN_AREA("2", "市区"),

    ;

    public final String type;
    public final String name;

    StoreManagementEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (StoreManagementEnum value : StoreManagementEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
