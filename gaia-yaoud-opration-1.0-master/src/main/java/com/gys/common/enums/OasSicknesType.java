package com.gys.common.enums;

import lombok.Getter;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 14:30
 **/
@Getter
public enum OasSicknesType {

    OasSicknesLargeType(0,"大类疾病"),
    OasSicknesMiddleType(1,"中类疾病"),
    OasSicknesSmalleType(2,"成分明细");
    private Integer type;
    private String name;

    OasSicknesType(Integer name, String code) {
        this.type = name;
        this.name = code;
    }
    public static String getName(Integer type){
        String name ="";
        if(OasSicknesLargeType.getType().equals(type)){
            name=OasSicknesLargeType.getName();
        }else if(OasSicknesMiddleType.getType().equals(type)){
            name=OasSicknesMiddleType.getName();
        }else if(OasSicknesSmalleType.getType().equals(type)){
            name=OasSicknesSmalleType.getName();
        }
        return name;
    }
}
