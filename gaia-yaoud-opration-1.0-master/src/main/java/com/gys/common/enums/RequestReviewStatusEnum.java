package com.gys.common.enums;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/12/23 20:40
 */
public enum RequestReviewStatusEnum {

    WAIT_REVIEW("0", "未审核"),
    REVIEWED("1", "已审核"),
    REJECTED("2", "已拒绝"),;

    public final String type;
    public final String name;

    RequestReviewStatusEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }
}
