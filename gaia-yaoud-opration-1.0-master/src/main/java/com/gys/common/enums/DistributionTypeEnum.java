package com.gys.common.enums;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/8 12:19
 **/
public enum DistributionTypeEnum {

    ONCE_CALL(1, ""),
    MANY_CALL(2, ""),;

    public final Integer type;
    public final String message;

    DistributionTypeEnum(Integer type, String message) {
        this.type = type;
        this.message = message;
    }
}
