package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/10/2017:27
 */
public interface BaseEnum {

    String getType();

    String getName();

    default String getNameByType(String type){
        return "";
    }

}
