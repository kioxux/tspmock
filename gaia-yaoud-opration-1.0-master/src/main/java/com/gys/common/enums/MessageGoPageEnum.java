package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Description: 消息类型页面跳转
 * @date 2021/10/20 17:40
 */
public enum MessageGoPageEnum {

    NO_JUMP(0, "不跳转"),
    JUMP(1, "跳转"),

    ;

    private final Integer type;
    private final String name;

    MessageGoPageEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (MessageGoPageEnum value : MessageGoPageEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    public static Integer matchType(Integer type) {
        for (MessageGoPageEnum value : MessageGoPageEnum.values()) {
            if (value.type.equals(type)) {
                return value.type;
            }
        }
        return null;
    }

    public Integer getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

}
