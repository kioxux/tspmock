package com.gys.common.enums;

/**
 * @author zhangdong
 * @date 2021/6/17 22:08
 */
public enum YaoshibangInvoiceTypeEnum {

    //发票类型（0 缺省，1 普通发票，2 专用发票）

    DEFAULT("0", "无"),
    NORMAL("1", "普通发票"),
    SPECIAL("2", "专用发票");
    private String index;
    private String type;

    YaoshibangInvoiceTypeEnum(String index, String type) {
        this.index = index;
        this.type = type;
    }

    public static String getType(String index) {
        YaoshibangInvoiceTypeEnum[] values = YaoshibangInvoiceTypeEnum.values();
        for (YaoshibangInvoiceTypeEnum value : values) {
            if (value.index.equals(index)) {
                return value.type;
            }
        }
        return null;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }}
