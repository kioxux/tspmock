package com.gys.common.enums;

/**
 * @author zhangdong
 * @date 2021/6/18 20:17
 */
public enum YaoshibangDeliverTypeEnum {

    //订单活动类型（0-普通单 2-限量秒杀, 7-药慧拼（拼团） 8-批购包邮）
    NORMAL("0","普通单"),
    LIMIT_SECOND_KILL("2","限量秒杀"),
    GROUP_WORK("7","药慧拼（拼团）"),
    FREE_SHIPPING("8","批购包邮");

    private String index;
    private String type;

    YaoshibangDeliverTypeEnum(String index, String type) {
        this.index = index;
        this.type = type;
    }

    public static String getType(String index) {
        YaoshibangDeliverTypeEnum[] values = YaoshibangDeliverTypeEnum.values();
        for (YaoshibangDeliverTypeEnum value : values) {
            if (value.index.equals(index)) {
                return value.type;
            }
        }
        return null;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }}



