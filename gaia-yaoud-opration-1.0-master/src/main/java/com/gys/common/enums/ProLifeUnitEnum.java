package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 保质期单位
 * @date 2021/11/1 13:17
 */
public enum ProLifeUnitEnum implements BaseEnum {

    DAY("1", "天"),
    MONTH("2", "月"),
    YEAR("3", "年"),

    ;

    public final String type;
    public final String name;

    ProLifeUnitEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
