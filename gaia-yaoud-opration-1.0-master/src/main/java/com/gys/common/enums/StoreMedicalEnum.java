package com.gys.common.enums;


import com.gys.common.annotation.StoreInfoRemark;

/**
 * @author wu mao yin
 * @Description: 是否医保店
 * @date 2021/10/20 17:40
 */
@StoreInfoRemark(value = "是否医保店", label = "stoIfMedical")
public enum StoreMedicalEnum implements BaseEnum {

    YES("1", "是"),
    NO("2", "否"),

    ;

    public final String type;
    public final String name;

    StoreMedicalEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (StoreMedicalEnum value : StoreMedicalEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
