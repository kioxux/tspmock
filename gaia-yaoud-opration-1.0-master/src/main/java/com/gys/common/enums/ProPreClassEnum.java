package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 处方类别
 * @date 2021/11/1 13:17
 */
public enum ProPreClassEnum implements BaseEnum {

    PRESCRIPTION("1", "处方药"),
    CLASS_A_OTC("2", "甲类OTC"),
    CLASS_B_OTC("3", "乙类OTC"),
    DOUBLE_SPAN_PRESCRIPTION("4", "双跨处方"),
    MONORAIL_PRESCRIPTION("5", "单轨处方"),
    DUAL_TRACK_PRESCRIPTION("6", "双轨处方"),

    ;

    public final String type;
    public final String name;

    ProPreClassEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
