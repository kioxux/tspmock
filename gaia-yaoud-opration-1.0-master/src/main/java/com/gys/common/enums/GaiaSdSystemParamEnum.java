package com.gys.common.enums;

/**
 * @Author ：gyx
 * @Date ：Created in 9:45 2021/11/5
 * @Description：系统参数枚举
 * @Modified By：gyx
 * @Version:
 */

public enum GaiaSdSystemParamEnum {

    STORE_MAINTEN("MDYH_WARN","门店养护消息提醒");

    GaiaSdSystemParamEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public final String code;

    public final String name;


}
