package com.gys.common.enums;

/***
 * @desc: 消息枚举类
 * @author: ryan
 * @createTime: 2021/6/16 10:49
 **/
public enum SdMessageTypeEnum {
    ENT_NEW_PRODUCT_EVALUATION("GAIA_MM_010313", "commodityEvaluation", "门店级新品评估"),

    ENTRUSTED_DELIVERY_THIRD_PARTY("GAIA_MM_010110", "entrustedDeliveryThirdParty", "委托配送商品编码未匹配"),

    RETURN_SUPPLIER_APPROVAL("GAIA_MM_020210", "shippingManagement_returnSupplierApproval", "退货通知单审核"),

    COMMODITY_INVENTORY_ADJUSTMENT("GAIA_TK_0101", "commodityInventoryAdjustment", "商品调库"),

    STORE_OUT_SUGGESTION_CONFIRM("GAIA_WM_010506", "storeAdjustmentFunction", "门店调剂调出确认消息"),

    STORE_IN_SUGGESTION_CONFIRM("GAIA_WM_010506", "storeAdjustmentFunction", "门店调剂调入确认消息"),

    LICENSE_QUALIFICATION("GAIA_SD_0222","earlyWarningOfLicenseValidityPeriod","证照资质消息预警"),

    CONSIGNMENT_OF_CLEARANCE_WAS_REJECTED("GAIA_SD_0102","","委托配送请货被拒绝"),

    STORE_PRODUCT_MAINTEN("GAIA_WM_0109","storeMaintenList","门店商品养护"),

    RECEIPT_ARE_NOT_MATCHED("GAIA_MM_010117", "entrustedistributionommodity", "单据未对码消息提示");

    public final String code;
    public final String page;
    public final String remark;

    SdMessageTypeEnum(String code, String page, String remark) {
        this.code = code;
        this.page = page;
        this.remark = remark;
    }
}
