package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Description: 消息类型
 * @date 2021/10/20 17:40
 */
public enum MessageShowTypeEnum {

    PAID(1, "消息"),
    COMPANY_MAINTENANCE(2, "推送"),
    PRECISION_MARKETING(3, "消息+推送"),

    ;

    private final Integer type;
    private final String name;

    MessageShowTypeEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (MessageShowTypeEnum value : MessageShowTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    public static Integer matchType(Integer type) {
        for (MessageShowTypeEnum value : MessageShowTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value.type;
            }
        }
        return null;
    }

    public Integer getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

}
