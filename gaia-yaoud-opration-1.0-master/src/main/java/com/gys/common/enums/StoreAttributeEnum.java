package com.gys.common.enums;

import com.gys.common.annotation.StoreInfoRemark;

/***
 * @desc: 门店属性枚举类
 * @author: ryan
 * @createTime: 2021/6/10 14:36
 **/
@StoreInfoRemark(value = "门店属性", label = "stoAttribute")
public enum StoreAttributeEnum implements BaseEnum {

    SINGLE("1", "单体店"),
    LINKAGE("2", "连锁店"),
    JOIN("3", "加盟店"),
    OUTPATIENTS("4", "门诊");

    public final String type;
    public final String name;

    StoreAttributeEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (StoreAttributeEnum value : StoreAttributeEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
