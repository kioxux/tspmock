package com.gys.common.enums;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/8/6 13:37
 **/
public enum WarnTypeEnum {

    SUPPLIER("1", "供应商"),
    CLIENT("2", "客户"),
    COMMODITY("3", "商品"),
    REPORT_WARN_DAYS("4", "报表预警天数");

    public final String type;
    public final String message;

    WarnTypeEnum(String type, String message) {
        this.type = type;
        this.message = message;
    }
}
