package com.gys.common.enums;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 14:30
 **/
public enum CheckOrderDetailEnum {

    REQUEST_ORDER("1", ""),
    THIRD_PARTY_ORDER("2", ""),;

    public final String type;
    public final String name;

    CheckOrderDetailEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }
}
