package com.gys.common.enums;

import cn.hutool.core.util.CharUtil;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wu mao yin
 * @Title: 会员卡类型
 * @date 2021/12/815:15
 */
public enum MemberCardTypeEnum implements BaseEnum {

    DIAMOND_CARD("1", "钻石卡"),
    PLATINUM_CARD("2", "白金卡"),
    GOLD_CARD("3", "金卡"),
    SILVER_CARD("4", "银卡"),
    REGULAR_CARD("5", "普通卡"),


    ;

    MemberCardTypeEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    private final String type;

    private final String name;

    public static String getNameByTypes(List<String> types) {
        Map<String, String> map = Arrays.stream(MemberCardTypeEnum.values())
                .collect(Collectors.toMap(MemberCardTypeEnum::getType, MemberCardTypeEnum::getName));
        if (CollectionUtils.isNotEmpty(types)) {
            List<String> names = new ArrayList<>();
            for (String type : types) {
                names.add(map.get(type));
            }
            return Joiner.on(CharUtil.COMMA).join(names);
        }
        return null;
    }

    public static List<String> getTypes() {
        List<String> types = new ArrayList<>();
        for (MemberCardTypeEnum value : MemberCardTypeEnum.values()) {
            types.add(value.getType());
        }
        return types;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
