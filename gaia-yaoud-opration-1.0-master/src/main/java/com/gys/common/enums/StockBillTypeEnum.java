package com.gys.common.enums;

//单据类型
public enum StockBillTypeEnum {
    YAN_SHOU("验收", "YS"),
    SHOU_HUO("收货", "SH"),
    TUI_KU_GY("退库GY", "TKGY"),
    TUI_KU_WT("退库WT", "TKWT"),
    TIAO_JI("调剂", "TJ"),
    SUN_YI("损益", "SYS"),
    LING_YONG("领用", "SYLY"),
    BAO_SUN("报损", "SYBS"),
    PIHAO_TIAOZHENG("批号调整", "PHTZ"),
    PIHAO_YANGHU("批号养护", "PHYH"),
    MEN_DIAN_BAO_YI("报溢", "SYBY"),
    BU_HE_GE("不合格品", "SDP");


    private String name;
    private String code;

    StockBillTypeEnum(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
