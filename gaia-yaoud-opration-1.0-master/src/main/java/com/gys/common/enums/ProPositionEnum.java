package com.gys.common.enums;

/***
 * @desc: 商品定位枚举
 * @author: ryan
 * @createTime: 2021/6/8 9:36
 **/
public enum ProPositionEnum {
    H("H", ""),
    X("X", "新品"),;

    public final String status;
    public final String message;

    ProPositionEnum(String status, String message) {
        this.status = status;
        this.message = message;
    }
}
