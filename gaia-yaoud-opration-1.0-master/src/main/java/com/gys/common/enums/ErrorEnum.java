package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 错误编码
 * @date 2021/10/21 10:09
 */
public enum ErrorEnum {

    SYSTEM_ERROR("500", "服务器出现了一些小状况，正在修复中！"),

    ;

    public final String code;
    public final String name;

    ErrorEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return null;
    }

    public String getName() {
        return null;
    }
}
