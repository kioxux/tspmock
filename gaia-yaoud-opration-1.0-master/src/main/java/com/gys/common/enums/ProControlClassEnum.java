package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 品牌区分
 * @date 2021/11/1 13:17
 */
public enum ProControlClassEnum implements BaseEnum {

    TOXIC_DRUGS("1", "毒性药品"),
    NARCOTIC_DRUGS("2", "麻醉药品"),
    CLASS_I_PSYCHOTROPIC_DRUGS("3", "一类精神药品"),
    CLASS_II_PSYCHOTROPIC_DRUGS("4", "二类精神药品"),
    PRECURSOR_DRUGS_EPHEDRINE("5", "易制毒药品（麻黄碱）"),
    RADIOPHARMACEUTICAL("6", "放射性药品"),
    BIOLOGICAL_PRODUCTS_INCLUDING_INSULIN("7", "生物制品（含胰岛素）"),
    STIMULANTS_EXCEPT_INSULIN("8", "兴奋剂（除胰岛素）"),
    FIRST_CLASS_DEVICE("9", "第一类器械"),
    SEC_CLASS_DEVICE("10", "第二类器械"),
    THIRD_CLASS_DEVICE("11", "第三类器械"),
    OTHER_CONTROLS("12", "其它管制"),

    ;

    public final String type;
    public final String name;

    ProControlClassEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
