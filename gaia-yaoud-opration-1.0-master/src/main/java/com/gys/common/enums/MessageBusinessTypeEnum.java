package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Description: 消息业务类型
 * @date 2021/10/20 17:40
 */
public enum MessageBusinessTypeEnum {

    WORK(1, "工作"),
    MESSAGE(2, "消息"),

    ;

    private final Integer type;
    private final String name;

    MessageBusinessTypeEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getName(String type) {
        for (MessageBusinessTypeEnum value : MessageBusinessTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

    public static Integer matchType(Integer type) {
        for (MessageBusinessTypeEnum value : MessageBusinessTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value.type;
            }
        }
        return null;
    }

    public Integer getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

}
