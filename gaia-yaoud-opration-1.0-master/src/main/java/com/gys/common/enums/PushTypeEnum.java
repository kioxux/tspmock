package com.gys.common.enums;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/8/6 13:37
 **/
public enum PushTypeEnum {

    BUSINESS_MASTER(1, "商采负责人"),
    OPERATION_MASTER(2, "营运负责人");

    public final Integer type;
    public final String message;

    PushTypeEnum(Integer type, String message) {
        this.type = type;
        this.message = message;
    }
}
