package com.gys.common.enums;

// 远程审方审核状态
public enum ReviewCheckStatusEnum {

    STATUS_NOT_APPROVED("0", "未审核"),
    STATUS_REVIEWED("1", "已审核"),
    STATUS_REJECTED("2", "已驳回");

    private final String code;
    private final String desc;

    // 根据code返回desc
    public String getDescByCode(String code) {
        for (ReviewCheckStatusEnum reviewCheckStatus : ReviewCheckStatusEnum.values()) {
            if (reviewCheckStatus.getCode().equals(code)) {
                return reviewCheckStatus.getDesc();
            }
        }
        return null;
    }



    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    private ReviewCheckStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
