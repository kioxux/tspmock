package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 品牌区分
 * @date 2021/11/1 13:17
 */
public enum ProBrandClassEnum implements BaseEnum {

    NATIONAL_BRAND("1", "全国品牌"),
    PROVINCIAL_BRAND("2", "省份品牌"),
    REGIONAL_BRAND("3", "地区品牌"),
    LOCAL_BRAND("4", "本地品牌"),
    OTHER("5", "其它"),

    ;

    public final String type;
    public final String name;

    ProBrandClassEnum(String type, String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
