package com.gys.common.enums;

/**
 * @author wu mao yin
 * @Title: 贮存条件
 * @date 2021/11/1 13:17
 */
public enum ProStorageConditionEnum implements BaseEnum {

    ROOM_TEMPERATURE("1", "常温"),
    COOL("2", "阴凉"),
    REFRIGERATION("3", "冷藏"),

    ;

    public final String type;
    public final String name;

    ProStorageConditionEnum(String type,String name) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNameByType(String type) {
        for (StoreDTPEnum value : StoreDTPEnum.values()) {
            if (value.type.equals(type)) {
                return value.name;
            }
        }
        return "";
    }

}
