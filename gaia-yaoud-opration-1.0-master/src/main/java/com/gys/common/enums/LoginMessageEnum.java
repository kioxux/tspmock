package com.gys.common.enums;

public enum LoginMessageEnum {
    GUI_LEI("参数归类","LOGIN_MESSAGE"),
    XIAOQI_YUJING_STO("门店效期预警","EXPIRYDATE_WARNING_STO"),
//    XIAOQI_PAGE_CODE("效期预警信息","GAIA_SD_0214"),
//    XIAOQI_PAGE_URL("效期预警路由","validityAlertqQuery"),
    XIAOQI_PAGE_CODE("效期库存查询","GAIA_MM_0105"),//2021年5月12号，陈周需求更改页面路由指向
    XIAOQI_PAGE_URL("库存查询路由","reportQuery"),
    XIAOQI_PAGE_PRO_CODE("商品实时库存查询","GAIA_RP_0103"),
    XIAOQI_PAGE_PRO_URL("商品预警路由","reportQueryByIndex"),
    XIAOQI_YUJING_PRO("商品效期预警","EXPIRYDATE_WARNING_PRO"),
    CANGKU_XIAOQI_PAGE_CODE("仓库有","GAIA_WM_010322"),
    CANGKU_XIAOQI_PAGE_URL("库存预警路由", "insidemgt/commodityWarning"),
    STORE_DISTRIBUTION2_CODE("门店铺货（新）", "GAIA_MM_010311"),
    STORE_DISTRIBUTION2_PAGE("门店铺货（新）", "storeDistribution2"),
    NEW_DISTRIBUTION_PLAN_CODE("新品铺货", "GAIA_MM_010311"),
    NEW_DISTRIBUTION_PLAN_PAGE("新品铺货", "newProductDistributionPlan");
    private String name;

    private String code;

    LoginMessageEnum(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
