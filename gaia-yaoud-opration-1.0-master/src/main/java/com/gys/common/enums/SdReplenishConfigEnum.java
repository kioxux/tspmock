package com.gys.common.enums;

/**
 * @Description: 门店补货参数枚举
 * @Author: wangQc
 * @Date: 2019/12/31
 * @Time: 18:12
 */
public enum SdReplenishConfigEnum {
    UN_CONTROL("0", "不控制"),
    RATE("1", "百分比"),
    QTY("2", "数量");

    private String code;

    private String value;

    private SdReplenishConfigEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    /**
     * 根据状态值 查询状态名称
     *
     * @param code
     * @return
     */
    public static String getValueByCode(String code) {
        for (SdReplenishConfigEnum sdReplenishConfigEnum : SdReplenishConfigEnum.values()) {
            if (sdReplenishConfigEnum.getCode().equals(code)) {
                return sdReplenishConfigEnum.getValue();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
