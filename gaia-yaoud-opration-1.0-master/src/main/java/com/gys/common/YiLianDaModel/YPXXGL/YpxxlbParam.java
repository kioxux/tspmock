package com.gys.common.YiLianDaModel.YPXXGL;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class YpxxlbParam {
    //ypxxlb 药品信息集合
    @Valid
    @NotNull(message = "药品信息集合不能为空")
    private List<Ypxxlb> ypxxlb;
}
