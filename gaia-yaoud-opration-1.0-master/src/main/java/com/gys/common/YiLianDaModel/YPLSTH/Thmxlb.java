package com.gys.common.YiLianDaModel.YPLSTH;

import lombok.Data;

@Data
public class Thmxlb {
    //药品编号  部分商品冲销是必填
    private String ypbh00;
    //冲销药品数量  部分商品冲销是必填
    private String ypsl00;
    //库存编码  部分商品冲销是必填
    private String stockcode
;}
