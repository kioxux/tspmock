package com.gys.common.YiLianDaModel.YPTHXX;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CkmxlbParam {
    //单据退回日期
    @NotNull(message = "单据退回日期不能为空")
    private String ckrq00;
    //退货单据号
    @NotNull(message = "退货单据号不能为空")
    private String ckdjh0;
    //供应商编码
    @NotNull(message = "供应商编码不能为空")
    private String gysbh0;
    //供应商名称
    @NotNull(message = "供应商名称不能为空")
    private String gysmc0;
    @Valid
    @NotNull
    private List<Ckmxlb> ckmxlb;
}
