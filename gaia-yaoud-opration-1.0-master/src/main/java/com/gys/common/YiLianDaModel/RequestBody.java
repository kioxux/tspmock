package com.gys.common.YiLianDaModel;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@Data
public class RequestBody<T>{
    //接口方法
    @NotNull(message = " 接口方法不能为空")
    private String method;
    //请求内容是否完成 TRUE 时表示返回内容完整
    @NotNull(message = "状态不能为空")
    private String request;
    //入参实体
    @Valid
    private T param;

//    public RequestBody(@NotNull(message = " 接口方法不能为空") String method, @NotNull(message = "状态不能为空") String request, T param) {
//        this.method = method;
//        this.request = request;
//        this.param = param;
//    }
}
