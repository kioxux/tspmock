package com.gys.common.YiLianDaModel.YPJHXX;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RkmxlbParam{
    //入库验收日期
    @NotNull(message = "入库验收日期不能为空")
    private String rkysrq;
    //验收单据号
    @NotNull(message = "验收单据号不能为空")
    private String ysdjh0;
    //供应商编码
    @NotNull(message = "供应商编码不能为空")
    private String gysbh0;
    //供应商名称
    @NotNull(message = "供应商名称不能为空")
    private String gysmc0;
    //入库明细集合
    @NotNull(message = "入库明细集合不能为空")
    private List<Rkmxlb> rkmxlb;
}
