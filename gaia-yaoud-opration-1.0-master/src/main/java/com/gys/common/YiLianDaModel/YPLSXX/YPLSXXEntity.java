package com.gys.common.YiLianDaModel.YPLSXX;

import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class YPLSXXEntity {
    //单据流水
    @NotNull(message = "单据流水不能为空")
    private String djlsh0;
    //门诊流水号
    @NotNull(message = "门诊流水号不能为空")
    private String mzlsh0;
    //保险号
    @NotNull(message = "保险号不能为空")
    private String id0000;
    //卡号
    @NotNull(message = "卡号不能为空")
    private String cardno;
    //姓名
    @NotNull(message = "姓名不能为空")
    private String xming0;
    //性别
    @NotNull(message = "性别不能为空")
    private String xbie00;
    //年龄
    @NotNull(message = "年龄不能为空")
    private String brnl00;
    //挂号科室
    @NotNull(message = "挂号科室不能为空")
    private String ghksmc;
    //病情编码
    private String bqbm00;
    //是否特殊门诊 Y 或 N
    @NotNull(message = "挂号科室不能为空")
    private String sftsmz;
    //是否特殊病种 Y 或 N
    @NotNull(message = "是否特殊病种不能为空")
    private String sftsbz;
    //账户支付额
    @NotNull(message = "账户支付额不能为空")
    private String zhzfe0;
    //现金支付额
    @NotNull(message = "现金支付额不能为空")
    private String grzfe0;
    //基金支付额
    @NotNull(message = "基金支付额不能为空")
    private String jjzfe0;
    //公务员补助
    @NotNull(message = "公务员补助不能为空")
    private String gwybz0;
    //总金额
    @NotNull(message = "总金额不能为空")
    private String bcbxf0;
    //收费日期 如：20160101
    @NotNull(message = "收费日期不能为空")
    private String sfrq00;
    //收费时间 如：0809
    @NotNull(message = "收费时间不能为空")
    private String sfsj00;
    //收费人
    @NotNull(message = "收费人不能为空")
    private String sfrxm0;
    //个人账户余额
    @NotNull(message = "个人账户余额不能为空")
    private String grzhye;
    //处方项目数
    @NotNull(message = "处方项目数不能为空")
    private String cfxms0;
}
