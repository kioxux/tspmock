package com.gys.common.YiLianDaModel;

public enum  YLDEunm {

    YDSFSK("YDSFSK"),
    YPLSXX("YPLSXX"),
    YPLSTH("YPLSTH"),
    YPXXGL("YPXXGL"),
    GYSXXGL("GYSXXGL"),
    YPJHXX("YPJHXX"),
    YPTHXX("YPTHXX"),
    YPKCXX("YPKCXX"),
    YPSYXX("YPSYXX"),
    YPKCPD("YPKCPD");

    private String code;



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    YLDEunm(String code) {
        this.code = code;
    }

//    public static String getValue(String code) {
//        for (MenuFlow ele : values()) {
//            if (ele.getCode().equals(code)) return  ele.getMessage();
//        }
//        return null;
//
//
//    }
//    public static Class getNameByRole(String code) {
//        for (YLDEunm value : YLDEunm.values()) {
//            if (value.getCode().equals(code)) {
//                return value.getDeclaringClass();
//            }
//        }
//        return null;
//    }
}
