package com.gys.common.YiLianDaModel.YPLSXX;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 销售单据
 * param 入参实体
 */
@Data
public class LsmxlbParam {

    //零售时间  如：20180101120101
    @NotNull(message = "零售时间号不能为空")
    private String lsrq00;
    //零售单据号
    @NotNull(message = "零售单据号不能为空")
    private String lsdjh0;
    //是否医保  1-是 0-否
    @Size(max = 1, min = 0)
    @NotNull(message = "是否医保不能为空")
    private String sfyb00;
    //卡号   医保销售时，该字段必填
    private String cardno;
    //个人编号  医保销售时，该字段必填
    private String id0000;
    //姓名   医保销售时，该字段必填
    private String xming0;
    //总金额
    @NotNull(message = "总金额不能为空")
    private String bcbxf0;
    //微信支付额
    private String wxzfe0;
    //银联支付金额
    private String ylzfje;
    //支付宝支付额
    private String zfbzfe;
    //lsmxlb 零售明细集合
    @Valid
    @NotNull(message = "零售明细集合不能为空")
    private List<Lsmxlb> lsmxlb;


}