package com.gys.common.YiLianDaModel.YPLSXX;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 销售单据
 * lsmxlb 零售明细集合
 */
@Data
public class Lsmxlb {
    //商品编码
    @NotNull(message = "商品编码不能为空")
    private String ypbh00;
    //医保编码
    private String ybbh00;
    //通用名称
    @NotNull(message = "通用名称不能为空")
    private String bzmc00;
    //商品名
    @NotNull(message = "商品名不能为空")
    private String ypmc00;
    //规格
    @NotNull(message = "规格不能为空")
    private String ypgg00;
    //单位
    @NotNull(message = "单位不能为空")
    private String dwmc00;
    //剂型
    @NotNull(message = "剂型不能为空")
    private String xh0000;
    //生产厂家
    @NotNull(message = "生产厂家不能为空")
    private String ypcjmc;
    //销售单价
    @NotNull(message = "销售单价不能为空")
    private String sjlsj0;
    //销售数量
    @NotNull(message = "销售数量不能为空")
    private String ypsl00;
    //库存编码
    @NotNull(message = "库存编码不能为空")
    private String stockcode ;
}