package com.gys.common.YiLianDaModel.GYSXXGL;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class GysxxlbParam {
    //gysxxlb 供应商信息集合
    @Valid
    @NotNull(message = "供应商信息集合不能为空")
    private List<Gysxxlb> gysxxlb;

}
