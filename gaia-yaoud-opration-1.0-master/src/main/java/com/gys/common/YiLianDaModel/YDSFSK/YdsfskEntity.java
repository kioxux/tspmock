package com.gys.common.YiLianDaModel.YDSFSK;

import lombok.Data;

@Data
public class YdsfskEntity {
    //卡号
    private String cardno;
    //保险号
    private String id0000;
    //姓名
    private String xming0;
    //性别 男或女
    private String xbie00;
    //年龄
    private String brnl00;
    //个人账户余额
    private String grzhye;
    //工作状态名称
    private String gzztmc;
    //单位名称
    private String dwmc00;
    //地区名称
    private String dqmc00;
    //分中心名称
    private String fzxmc0;
    //卡状态名称
    private String icztmc;
    //挂号科室
    private String ghksmc;
    //门诊流水号
    private String mzlsh0;
    //不能挂号原因
    private String bnghyy;
    //是否有效
    private String valid0;
}
