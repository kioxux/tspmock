package com.gys.common.YiLianDaModel.YPXXGL;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Ypxxlb {

    //药品编号
    @NotNull(message = "药品编号不能为空")
    private String ypbh00;
    //药品名称
    @NotNull(message = "药品名称不能为空")
    private String ypmc00;
    //通用名称
    @NotNull(message = "通用名称不能为空")
    private String bzmc00;
    //剂型
    @NotNull(message = "剂型不能为空")
    private String xh0000;
    //规格
    @NotNull(message = "规格不能为空")
    private String ypgg00;
    //生产厂家
    @NotNull(message = "生产厂家不能为空")
    private String ypcjmc;
    //单位
    @NotNull(message = "单位不能为空")
    private String dwmc00;
    //零售价
    @NotNull(message = "零售价不能为空")
    private String xmdj00;
    //医保编码
    private String ybbh00;
}
