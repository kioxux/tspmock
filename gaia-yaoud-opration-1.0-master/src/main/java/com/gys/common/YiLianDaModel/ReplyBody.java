package com.gys.common.YiLianDaModel;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class ReplyBody<T> {
    //接口方法
    @NotNull(message = " 接口方法不能为空")
    private String method;
    //返回内容是否完成
    @NotNull(message = " reply不能为空")
    private boolean reply;
    //请求结果
    private String success;
    @Valid
    private T entity;
    private String error;
}
