package com.gys.common.YiLianDaModel.YPKCPD;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class KcmxlbParam {
    //上传库存时间
    @NotNull(message = "上传库存时间不能为空")
    private String sckcrq;
    //盘点单据号
    @NotNull(message = "盘点单据号")
    private String pddjh0;
    @Valid
    @NotNull
    private List<Kcmxlb> kcmxlb;
}
