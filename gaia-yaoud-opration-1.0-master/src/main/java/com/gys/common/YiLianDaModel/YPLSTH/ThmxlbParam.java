package com.gys.common.YiLianDaModel.YPLSTH;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ThmxlbParam {
    //冲销时间日期 如：20180101120101
    @NotNull(message = "冲销时间日期不能为空")
    private String cxsj00;
    //冲销单据号
    @NotNull(message = "冲销单据号不能为空")
    private String cxdjh0;
    //原零售单据号
    @NotNull(message = "原零售单据号不能为空")
    private String lsdjh0;
    // 冲销单据类型 默认 1（医保销售冲销只能为 1） 1- 整单冲销 2- 部分商品冲销
    private String djlx00;
    //冲销现金支付额  部分商品冲销时是必填
    private String grzfe0;
    //冲销支付宝支付额  部分商品冲销时是必填
    private String zfbzfe;
    //冲销微信支付额 部分商品冲销时是必填
    private String wxzfe0;
    //冲销银联支付额 部分商品冲销时是必填
    private String ylzfe0;
    //thmxlb 冲销明细集合
    @NotNull(message = "冲销明细集合不能为空")
    private List<Thmxlb> thmxlb;
}
