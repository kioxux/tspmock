package com.gys.common.YiLianDaModel.YPSYXX;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class SymxlbParam {
    //损溢时间
    @NotNull(message = "损溢时间不能为空")
    private String sysj00;
    //损溢单据号
    @NotNull(message = "损溢单据号不能为空")
    private String sydjh0;
    //损溢类型 损:01,溢：02
    @NotNull(message = "损溢类型不能为空")
    private String sylx00;
    @Valid
    @NotNull
    private List<Symxlb> symxlb;
}
