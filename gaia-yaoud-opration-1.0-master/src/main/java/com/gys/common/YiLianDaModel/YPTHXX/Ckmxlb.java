package com.gys.common.YiLianDaModel.YPTHXX;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Ckmxlb {
    //药品编号
    @NotNull(message = "药品编号不能为空")
    private String ypbh00;
    //药品名称
    @NotNull(message = "药品名称不能为空")
    private String ypmc00;
    //通用名称
    @NotNull(message = "通用名称不能为空")
    private String bzmc00;
    //剂型
    @NotNull(message = "剂型不能为空")
    private String xh0000;
    //规格
    @NotNull(message = "规格不能为空")
    private String ypgg00;
    //生产厂家
    @NotNull(message = "生产厂家不能为空")
    private String ypcjmc;
    //单位
    @NotNull(message = "单位不能为空")
    private String dwmc00;
    //退货数量
    @NotNull(message = "退货数量不能为空")
    private String sl0000;
    //药品批号
    @NotNull(message = "药品批号不能为空")
    private String ypph00;
    //生产日期
    @NotNull(message = "生产日期不能为空")
    private String scrq00;
    //有效日期
    @NotNull(message = "有效日期不能为空")
    private String yxrq00;
    //进价
    @NotNull(message = "进价不能为空")
    private String ypjj00;
    //入库编号
    @NotNull(message = "入库编号不能为空")
    private String stockcode;
    //退货原因
    @NotNull(message = "退货原因不能为空")
    private String thyy00;
}
