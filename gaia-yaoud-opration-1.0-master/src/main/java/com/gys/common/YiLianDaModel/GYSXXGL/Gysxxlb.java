package com.gys.common.YiLianDaModel.GYSXXGL;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Gysxxlb {
    //供应商编号
    @NotNull(message = "供应商编号不能为空")
    private String gysbh0;
    //供应商名称
    @NotNull(message = "供应商名称不能为空")
    private String gysmc0;
    //地址
    private String gysdz0;
    //联系方式
    private String lxfs00;
    //仓库地址
    private String ckdz00;
    //联系人
    private String lxr000;
}
