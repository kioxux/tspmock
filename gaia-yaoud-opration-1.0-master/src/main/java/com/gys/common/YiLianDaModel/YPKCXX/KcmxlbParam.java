package com.gys.common.YiLianDaModel.YPKCXX;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class KcmxlbParam {
    //上传库存时间
    @NotNull(message = "上传库存时间不能为空")
    private String sckcrq;
    //库存单据号
    @NotNull(message = "库存单据号")
    private String kcdjh0;
    @Valid
    @NotNull
    private List<Kcmxlb> kcmxlb;
}
