//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.gys.business.service.data.GetElemeOutData;
import com.gys.common.data.Result;
import com.gys.common.webSocket.WebSocket;
import com.gys.util.InputDataFormat;
import com.gys.util.SignUtil;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;

public class ElemeHandler implements HttpHandler {
    @Resource
    private WebSocket webSocket;

    public ElemeHandler() {
    }

    public void handle(HttpExchange exchange) {
        Object var2 = null;

        try {
            String postString = InputDataFormat.makeInputStreamToString(exchange.getRequestBody());
            Map<String, Object> postInfo = InputDataFormat.formData2Dic(postString);
            Map<String, Object> config = new HashMap();
            config.put("secret", "e885dc3ff6f75105");
            boolean flag = SignUtil.checkSign(postInfo, config);
            Result res = new Result(postInfo);
            String appId;
            GetElemeOutData outData;
            if ("order.create".equals(postInfo.get("cmd"))) {
                appId = postInfo.get("source").toString();
                outData = new GetElemeOutData();
                outData.setParams(JSON.toJSONString(postInfo.get("body")));
                outData.setSource("2");
                outData.setType("orderCreate");
                this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
                res.setCreateResult(config, flag);
            } else if ("order.status.push".equals(postInfo.get("cmd"))) {
                appId = postInfo.get("source").toString();
                outData = new GetElemeOutData();
                outData.setParams(JSON.toJSONString(postInfo.get("body")));
                outData.setSource("2");
                outData.setType("orderStatusPush");
                this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
                res.setPushStatusResult(config, flag);
            } else if ("order.user.cancel".equals(postInfo.get("cmd"))) {
                appId = postInfo.get("source").toString();
                outData = new GetElemeOutData();
                outData.setParams(JSON.toJSONString(postInfo.get("body")));
                outData.setSource("2");
                outData.setType("orderUserCancel");
                this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
                res.setCancelResult(config, flag);
            } else if ("order.partrefund.push".equals(postInfo.get("cmd"))) {
                appId = postInfo.get("source").toString();
                outData = new GetElemeOutData();
                outData.setParams(JSON.toJSONString(postInfo.get("body")));
                outData.setSource("2");
                outData.setType("orderPartrefundPush");
                this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
                res.setPartRefundPushResult(config, flag);
            }

            Gson gson = new Gson();
            String result = gson.toJson(res.getResult());
            exchange.sendResponseHeaders(200, 0L);
            OutputStream os = exchange.getResponseBody();
            os.write(result.toString().getBytes());
            os.close();
        } catch (IOException var11) {
            System.out.println(var11.getMessage());
        } catch (Exception var12) {
            System.out.println(var12.getMessage());
        }

    }
}
