package com.gys.common.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class InsertProductCodeImportResponse {
    @ApiModelProperty(value = "商品编码集合")
    private List<String> productCodes;
}
