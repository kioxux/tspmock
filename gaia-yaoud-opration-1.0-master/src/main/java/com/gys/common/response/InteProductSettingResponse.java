package com.gys.common.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class InteProductSettingResponse {

    @ApiModelProperty
    private String result;
}
