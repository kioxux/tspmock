package com.gys.common.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 在自定义异常的错误码和信息时，如果过多，没有统一管理，则会出现重复。
 * 使用枚举统一管理code和message：
 */
@Getter
@AllArgsConstructor
public enum ResultEnum {
    SUCCESS("0", "执行成功"),
    /***
     * 所有接口成功都返回，如非成功的请在下面自定义
     */
    E0155("0155", "导出失败"),
    E0156("0156", "导出数据为空"),

    UNKNOWN_ERROR("9999", "系统异常，请稍后再试");

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
