package com.gys.common.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectStoreListResponse {
    @ApiModelProperty(value = "门店编码")
    private String stoCode;

    @ApiModelProperty(value = "门店简称")
    private String  stoName;
}
