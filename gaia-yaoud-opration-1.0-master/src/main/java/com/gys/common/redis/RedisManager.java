//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.redis;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class RedisManager {
    @Autowired
    private RedisTemplate redisTemplate;

    public RedisManager() {
    }

    public Boolean setNx(String key, Object value, long second) {
        return redisTemplate.opsForValue().setIfAbsent(key, value, second, TimeUnit.SECONDS);
    }

    public void set(String key, Object value) {
        ValueOperations<String, Object> operation = this.redisTemplate.opsForValue();
        operation.set(key, value);
    }

    public void set(String key, Object value, Long second) {
        ValueOperations<String, Object> operation = this.redisTemplate.opsForValue();
        operation.set(key, value, second, TimeUnit.SECONDS);
    }

    public Object get(String key) {
        ValueOperations<String, Object> operation = this.redisTemplate.opsForValue();
        return operation.get(key);
    }

    public void setHash(String key, Map map) {
        HashOperations operation = this.redisTemplate.opsForHash();
        operation.putAll(key, map);
    }

    public void setHash(String key, Map map, Long hour) {
        HashOperations operation = this.redisTemplate.opsForHash();
        operation.putAll(key, map);
        this.redisTemplate.expire(key, hour, TimeUnit.HOURS);
    }

    public Object getHash(String key) {
        HashOperations operation = this.redisTemplate.opsForHash();
        return operation.entries(key);
    }

    /**
     * 获取以某一前缀开头的所有map
     * return:  key  redis缓存key; value  Map
     */
    public Map<String, Object> getHashByKeyPrefix(String prefix) {
        Set<String> keys = redisTemplate.keys(prefix + "*");
        Map<String, Object> map = new HashMap<>(keys.size());
        for (String key : keys) {
            map.put(key, getHash(key));
        }
        return map;
    }

    /**
     * 加锁  记得解锁，不然资源无法释放
     * @param key 锁key
     * @param timeout 获取锁的最大等待时间, 单位: ms
     */
    public boolean tryLock(String key, long timeout) {
        Long start = System.currentTimeMillis();
        //自旋，在一定时间内获取锁，超时则返回错误
        for (;;) {
            //Set命令返回OK，则证明获取锁成功
            Boolean ret = redisTemplate.opsForValue().setIfAbsent(key, "exist");
            if (ret) {
                return true;
            }
            //否则循环等待，在timeout时间内仍未获取到锁，则获取失败
            long end = System.currentTimeMillis() - start;
            if (end >= timeout) {
                return false;
            }
        }
    }

    /**
     * 解锁
     * @param key
     */
    public void unLock(String key) {
        redisTemplate.delete(key);
    }

    public void deleteHashKey(String busType, String key) {
        this.redisTemplate.opsForHash().delete(busType, key);
    }

    public void delete(String key) {
        this.redisTemplate.delete(key);
    }

    public Boolean hasKey(String key) {
        return this.redisTemplate.hasKey(key);
    }

    public Boolean containsKey(String prefix) {
        return CollectionUtils.isNotEmpty(this.redisTemplate.keys(prefix + "*"));
    }

    /**
     * @Description: 获得锁 可用于线程抢占执行
     * @Author: wangQc
     * @Date: 2020/4/3
     * @Time: 11:54
     */
    public boolean getLock(String lockId, long second) {
        Boolean success = this.redisTemplate.opsForValue().setIfAbsent(lockId, "lock", second, TimeUnit.SECONDS);
        return success != null && success;
    }

    /**
     * redis自增 原子性
     * @param key key
     * @param step 自增值
     * @return 自增后的值
     */
    public Long increment(String key, long step) {
        return redisTemplate.opsForValue().increment(key, step);
    }
}
