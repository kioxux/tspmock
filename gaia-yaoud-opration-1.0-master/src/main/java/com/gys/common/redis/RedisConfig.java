package com.gys.common.redis;
import java.math.BigDecimal;
import com.google.common.collect.Lists;
import com.gys.business.mapper.entity.*;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.data.mib.PayInfoData;
import com.gys.business.service.data.mib.SetlInfoData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.ArrayList;
import java.util.List;

@Configuration
@AutoConfigureAfter({RedisAutoConfiguration.class})
public class RedisConfig {
//    public static void main(String[] args) {
//        PayInfoData payInfoData = new PayInfoData();
//        MibDrugInfo info = new MibDrugInfo();
//        info.setCzDate("12");
//        info.setCzFlag("3");
//        info.setCzUser("4");
//        info.setFailMsg("2");
//        info.setPsnName("4");
//        info.setClient("5");
//        info.setBrId("3");
//        info.setBillNo("5");
//        info.setMsgId("5");
//        info.setPsnNo("6");
//        info.setMdtrtCertType("1");
//        info.setMdtrtCertNo("2");
//        info.setBegntime("23");
//        info.setMedfeeSumamt("44");
//        info.setInvono("5");
//        info.setInsutype("2");
//        info.setDiseCodg("2");
//        info.setDiseName("3");
//        info.setAcctUsedFlag("3");
//        info.setMedType("3");
//        info.setOmsgId("4");
//        info.setOinfNo("4");
//        info.setRighting("4");
//        List<MibDrugDetail> de = new ArrayList<>();
//        MibDrugDetail d = new MibDrugDetail();
//        d.setClient("2");
//        d.setBrId("3");
//        d.setBillNo("4");
//        d.setMsgId("5");
//        d.setFeedetlSn("5");
//        d.setRxno("5");
//        d.setRxCircFlag("3");
//        d.setFeeOcurTime("4");
//        d.setMedListCodg("5");
//        d.setMedinsListCodg("3");
//        d.setDetItemFeeSumamt("1");
//        d.setCnt("2");
//        d.setPric("3");
//        d.setSinDosDscr("4");
//        d.setUsedFrquDscr("");
//        d.setPrdDays("");
//        d.setMedcWayDscr("");
//        d.setBilgDrCodg("");
//        d.setBilgDrName("");
//        d.setTcmdrugUsedWay("");
//        de.add(d);
//
//        payInfoData.setMibDrugInfo(info);
//        payInfoData.setMibDrugDetails(de);
//        System.out.println("aaaaa:  "+ JSON.toJSONString(payInfoData));
//
//        SetlInfoData  cc= new SetlInfoData();
//        MibSetlInfo dd = new MibSetlInfo();
//        dd.setOinfNo("123");
//        dd.setClient("423");
//        dd.setBrId("4123");
//        dd.setBillNo("4124");
//        dd.setMsgId("qq");
//        dd.setSetlId("qq");
//        dd.setMdtrtId("qq");
//        dd.setPsnNo("qq");
//        dd.setPsnName("qq");
//        dd.setPsnCertType("qq");
//        dd.setCertno("qq");
//        dd.setGend("qq");
//        dd.setNaty("qq");
//        dd.setBrdy("qq");
//        dd.setAge("qq");
//        dd.setInsutype("qq");
//        dd.setPsnType("qq");
//        dd.setCvlservFlag("qq");
//        dd.setSetlTime("qq");
//        dd.setMdtrtCertType("qq");
//        dd.setMedType("qq");
//        dd.setMedfeeSumamt("qq");
//        dd.setFulamtOwnpayAmt("qq");
//        dd.setOverlmtSelfpay("qq");
//        dd.setPreselfpayAmt("qq");
//        dd.setInscpScpAmt("qq");
//        dd.setActPayDedc("qq");
//        dd.setHifpPay("qq");
//        dd.setPoolPropSelfpay("qq");
//        dd.setCvlservPay("qq");
//        dd.setHifesPay("qq");
//        dd.setHifmiPay("qq");
//        dd.setHifobPay("qq");
//        dd.setMafPay("qq");
//        dd.setOthPay("qq");
//        dd.setFundPaySumamt("qq");
//        dd.setPsnPartAmt("qq");
//        dd.setAcctPay("qq");
//        dd.setPsnCashPay("qq");
//        dd.setCashPayamt("qq");
//        dd.setBalc("qq");
//        dd.setAcctMulaidPay("qq");
//        dd.setMedinsSetlId("qq");
//        dd.setClrOptins("qq");
//        dd.setClrWay("qq");
//        dd.setClrType("qq");
//        dd.setPsnPay("qq");
//        dd.setState("");
//        dd.setCzFlag("");
//        dd.setEmpName("");
//        dd.setSetlIdReturn("");
//        dd.setCzDate("");
//        cc.setMibSetlInfo(dd);
//
//        List<MibSetlDetail> eee = new ArrayList<>();
//        MibSetlDetail ff = new MibSetlDetail();
//        ff.setClient("qq");
//        ff.setBrId("qq");
//        ff.setBillNo("qq");
//        ff.setMsgId("qq");
//        ff.setFundPayType("qq");
//        ff.setInscpScpAmt("qq");
//        ff.setCrtPaybLmtAmt("qq");
//        ff.setFundPayamt("qq");
//        ff.setFundPayTypeName("qq");
//        ff.setSetlProcInfo("");
//eee.add(ff);
//        List<MibDetlcutInfo> dddd = new ArrayList<>();
//        MibDetlcutInfo hhhh = new MibDetlcutInfo();
//
//        hhhh.setClient("qq");
//        hhhh.setBrId("qq");
//        hhhh.setBillNo("qq");
//        hhhh.setMsgId("qq");
//        hhhh.setFeedetlSn("qq");
//        hhhh.setDetItemFeeSumamt(new BigDecimal("0"));
//        hhhh.setCnt(new BigDecimal("0"));
//        hhhh.setPric(new BigDecimal("0"));
//        hhhh.setPricUplmtAmt(new BigDecimal("0"));
//        hhhh.setSelfpayProp(new BigDecimal("0"));
//        hhhh.setFulamtOwnpayAmt(new BigDecimal("0"));
//        hhhh.setOverlmtAmt(new BigDecimal("0"));
//        hhhh.setPreselfpayAmt(new BigDecimal("0"));
//        hhhh.setInscpScpAmt(new BigDecimal("0"));
//        hhhh.setChrgitmLv("qq");
//        hhhh.setMedChrgitmType("qq");
//        hhhh.setBasMednFlag("qq");
//        hhhh.setHiNegoDrugFlag("qq");
//        hhhh.setChldMedcFlag("qq");
//        hhhh.setListSpItemFlag("qq");
//        hhhh.setDrtReimFlag("");
//        hhhh.setMemo("");
//
//        dddd.add(hhhh);
//
//
//        cc.setMibSetlDetails(eee);
//        cc.setMibDetlcutInfos(dddd);
//        System.out.println("aaaaa:  "+JSON.toJSONString(cc));
//
//    }
    public RedisConfig() {
    }

    @Bean
    public RedisTemplate<String, Object> redisCacheTemplate(LettuceConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate();
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

    @Autowired
    public void setRedisTemplate(RedisTemplate redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
    }
}
