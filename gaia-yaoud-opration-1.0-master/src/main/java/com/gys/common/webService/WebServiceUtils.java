package com.gys.common.webService;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;


public class WebServiceUtils {
    /**
     * WebService请求发送方法
     *
     * @param clientBean 客户端请求发送实体类(包含一些请求参数、webservice接口地址等)
     * @return 通常返回XML格式的文本
     * <p>
     * 使用示例:
     * List<ClientField> clientFields = new ArrayList<>();
     * // 设置请求参数
     * ClientField field = new ClientField("strCardno", String.class, xh);
     * clientFields.add(field);
     * <p>
     * ClientBean clientBean = new ClientBean(
     * "http://202.192.41.136:8090/Service.asmx",
     * "ilasIII OpenAPI", "ilasIII OpenAPI/GetRdrFin",
     * "GetRdrFin", clientFields);
     * //返回XML
     * String returnXml = NHWebServiceUtils.sendWebServiceRequest(clientBean);
     */
    public static String sendWebServiceRequest(ClientBean clientBean) {
        try {
            Object[] paramValues;
            //实例化访问对象
            Service service = new Service();
            //实例化调用对象
            Call call = (Call) service.createCall();
            //在调用对象中添加WebService地址
            call.setTargetEndpointAddress(new java.net.URL(clientBean.getEndpointUrl()));
            paramValues = new Object[clientBean.getClientFields().size()];
            int i = 0;
            //设置入参，第一个参数是命名空间以及参数名，这两个参数是采用一个Qname变量打包传入的，第二个参数是入参的类型（字符或者数字）第三个参数是入参种类
            for (ClientField field : clientBean.getClientFields()) {
                QName qn = new QName(clientBean.getTargetNamespace(), field.getParamName());
                if (field.getParamType() == String.class) {
                    call.addParameter(qn, XMLType.XSD_STRING, ParameterMode.IN);
                } else if (field.getParamType() == int.class) {
                    call.addParameter(qn, XMLType.XSD_INT, ParameterMode.IN);
                } else if (field.getParamType() == Integer.class) {
                    call.addParameter(qn, XMLType.XSD_INTEGER, ParameterMode.IN);
                } else if (field.getParamType() == Double.class) {
                    call.addParameter(qn, XMLType.XSD_DOUBLE, ParameterMode.IN);
                } else if (field.getParamType() == Float.class) {
                    call.addParameter(qn, XMLType.XSD_FLOAT, ParameterMode.IN);
                } else if (field.getParamType() == Long.class) {
                    call.addParameter(qn, XMLType.XSD_LONG, ParameterMode.IN);
                } else {
                    throw new Exception("请确定传入参数类型是否正确，或者该参数类型还没有被注册。");
                }
                //获取参数值
                paramValues[i] = field.getParamValue();
                i++;
            }
            //设置返回值格式（字符串或者组装对象）
            call.setReturnType(XMLType.XSD_STRING);
            //是否是SOAPAction这里需要看WebService是否要求如下格式，如果没有要求可以不添加此设置
            call.setUseSOAPAction(true);
            //如果前面要求是SOAPAction的话需要添加下面设置，指定访问那个命名空间上的那个方法
            call.setSOAPActionURI(clientBean.getActionUrl());
            //在调用对象中添加WebService对应的命名空间，以及将要调用的函数名
            call.setOperationName(new QName(clientBean.getTargetNamespace(), clientBean.getMethodName()));
            String ret;
            //调用目标方法,设置参数值将返回值转换为String类型
            ret = (String) call.invoke(paramValues);
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
