package com.gys.common.annotation;

import com.gys.common.enums.BaseEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wu mao yin
 * @Title: 解析枚举值
 * @Package
 * @date 2021/11/114:00
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParseEnumVal {

    Class<? extends BaseEnum> enumVal();

}
