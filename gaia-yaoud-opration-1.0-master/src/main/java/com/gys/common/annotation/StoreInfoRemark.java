package com.gys.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wu mao yin
 * @Description: 门店信息标注
 * @date 2021/10/20 18:19
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface StoreInfoRemark {

    String value() default "";

    String label() default "";

}
