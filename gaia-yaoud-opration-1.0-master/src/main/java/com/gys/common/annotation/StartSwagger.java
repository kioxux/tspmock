package com.gys.common.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * swagger 启动扫描注解  配合 @ApiOperation 注解一起使用 实现对单一接口扫描
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface StartSwagger {

}
