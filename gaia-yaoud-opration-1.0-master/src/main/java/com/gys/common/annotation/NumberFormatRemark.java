package com.gys.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author ：gyx
 * @Date ：Created in 17:40 2021/12/28
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NumberFormatRemark {
}
