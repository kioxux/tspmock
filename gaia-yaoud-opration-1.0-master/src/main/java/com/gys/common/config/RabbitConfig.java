package com.gys.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 陈浩
 */
@Configuration
@Slf4j
public class RabbitConfig {
    public static final String EXCHANGE_TEST = "exchange_test";

    public static final String EXCHANGE_TOPIC = "Data_Synchronized";

    public static final String QUEUE_TEST = "queue_test";

    public static final String ROUTING_KEY_TEST = "routing_key_test";

    public static final String QUEUE_STORE_DATA = "queue_store_data";

    public static final String ERROR_QUEUE_STORE_DATA = "error_queue_store_data";

    @Bean
    public Queue TestDirectQueue() {
        Map<String,Object> arg = new HashMap<>();
        arg.put("contentType","text/plain");
        arg.put("contentEncoding","UTF-8");
        return new Queue(QUEUE_TEST,true,false,false,arg);
    }

    @Bean
    public Queue StoreDataDirectQueue() {
        Map<String,Object> arg =new HashMap<>();
        arg.put("contentType","text/plain");
        arg.put("contentEncoding","UTF-8");
        return new Queue(QUEUE_STORE_DATA,true,false,false,arg);
    }

    @Bean
    public Queue ErrorStoreDataDirectQueue() {
        Map<String,Object> arg =new HashMap<>();
        arg.put("contentType","text/plain");
        arg.put("contentEncoding","UTF-8");
        return new Queue(ERROR_QUEUE_STORE_DATA,true,false,false,arg);
    }

    @Bean
    public DirectExchange TestDirectExchange() {
        return new DirectExchange(EXCHANGE_TEST,true,false);
    }


//    @Bean
//    public TopicExchange StoreDataTopicExchange() {
//        return new TopicExchange(EXCHANGE_STORE_DATA,true,false);
//    }

    @Bean
    public Binding bindingDirect() {
        return BindingBuilder.bind(this.TestDirectQueue()).to(this.TestDirectExchange()).with(ROUTING_KEY_TEST);
    }

    /**
     * {@link org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration}
     *  会自动识别
     * @return mq 消息序列化工具
     */

    @Bean
    public RabbitTemplate jacksonRabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

}
