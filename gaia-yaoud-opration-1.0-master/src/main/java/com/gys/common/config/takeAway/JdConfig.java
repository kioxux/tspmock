package com.gys.common.config.takeAway;


public class JdConfig {
    //https://openapi.jddj.com/djapi/stock/queryStockCenter
    //public final static String JD_API_SERVER = PropertyUtil.getProperty("jd_api_server");
    public final static String JD_API_SERVER = "https://openapi.jddj.com/djapi/";
    //测试mock地址
    public final static String JD_API_SERVER_TEST = "https://openapi.jddj.com/mockapi/";
    public final static String JD_CMD_STOCK_UPDATE = "stock/batchUpdateCurrentQtys";
    public final static String JD_CMD_SHOP_LIST = "store/getStationsByVenderId";
    public final static String JD_CMD_SHOP_INFO = "storeapi/getStoreInfoByStationNo";
    public final static String JD_CMD_SKU_LIST = "pms/querySkuInfos";
    public final static String JD_CMD_SKU_SHOP_LIST = "stock/queryStockCenter";
    public final static String JD_CMD_ORDER_LIST = "order/es/query";
    public final static String JD_CMD_ORDER_CONFIRM = "ocs/orderAcceptOperate";
    public final static String JD_CMD_ORDER_DELIVERY = "bm/open/api/order/OrderJDZBDelivery";
    public final static String JD_CMD_ORDER_CANCEL_CONFIRM = "ocs/orderCancelOperate";
    public final static String JD_CMD_ORDER_ACCOUNT = "bill/orderShoudSettlementService";
    public final static String JD_CMD_ORDER_AFS = "afs/getAfsService";
    public final static String JD_CMD_ORDER_AFSAPPROVE = "afs/afsOpenApprove";
    public final static String JD_CMD_SHOP_ConfigOpen = "store/updateStoreConfig4Open";

    public final static String JD_CMD_SKU_CREATE = "PmsSkuMainService/batchAddSku";
    public final static String JD_CMD_SKU_PRICE = "venderprice/updateStationPrice";

    public final static String JD_CMD_CATEGORY_LIST = "pms/queryCategoriesByOrgCode";
    public final static String JD_CMD_CATEGORY_ADD = "pms/addShopCategory";

    public final static String JD_API_VERIFY_UPDATE_TOKEN = "ApplicationService/verificationUpdateToken";
    public final static String JD_WMS_GET_STOCK_LIST = "wms/getStockListForMerchant";

}
