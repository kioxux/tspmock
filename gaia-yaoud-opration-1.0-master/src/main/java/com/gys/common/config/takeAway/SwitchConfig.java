package com.gys.common.config.takeAway;

import com.gys.business.service.takeAway.BaseService;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class SwitchConfig {
    public static Map<String, Object> switchHash = new HashedMap();
    public static List<String> platformsList=new ArrayList<>();
    public static int timeValiedRange = 30000;
    private BaseService baseService;
    private static BaseService service;
    private static SwitchConfig switchConfig;

    @Autowired
    public void setBaseService(BaseService baseService) {
        this.baseService = baseService;
    }

    @PostConstruct
    public void init() {
        switchConfig=this;
        switchConfig.baseService=this.baseService;
        service=this.baseService;
    }

    public static boolean checkMedicineSwitch(String shop_no, String platforms) {
        boolean result = false;
        Map<String, Object> map = selectSwitchInfo(service, shop_no, platforms);
        if (map!=null) {
            String val = String.valueOf(map.get("medicine_on_service"));
            if (val.equals("1")) {
                result = true;
            }
        }
        return result;
    }

    public static boolean checkOrderSwitch(String shop_no, String platforms) {
        boolean result = false;
        Map<String, Object> map = selectSwitchInfo(service, shop_no, platforms);
        if (map!=null) {
            String val = String.valueOf(map.get("order_on_service") == null ? map.get("ORDER_ON_SERVICE") : "");
            //todo  T_SWITCH_INFO和 T_ACCOUNT表需要整理
            if (val.equals("1")) {
                result = true;
            }
        }
        return result;
    }

    public static boolean checkStockSwitch(String shop_no, String platforms) {
        boolean result = false;
        Map<String, Object> map = selectSwitchInfo(service, shop_no, platforms);
        if (map!=null) {
            String val = String.valueOf(map.get("stock_on_service") == null ? map.get("STOCK_ON_SERVICE") : "");
            if (val.equals("1")) {
                result = true;
            }
        }
        return result;
    }

    public static boolean checkActSwitch(String shop_no, String platforms) {
        boolean result = false;
        Map<String, Object> map = selectSwitchInfo(service, shop_no, platforms);
        if (map!=null) {
            String val = String.valueOf(map.get("activity_on_service"));
            if (val.equals("1")) {
                result = true;
            }
        }
        return result;
    }

    private static Map<String, Object> selectSwitchInfo(BaseService service, String shop_no, String platforms) {
        Map<String, Object> result = null;
        boolean needExec = true;
        String plat_shop = platforms + ":" + shop_no;
        if (switchHash.get(plat_shop) != null) {
            result = (Map<String, Object>) switchHash.get(plat_shop);
            Timestamp lastTimestamp = (Timestamp) result.get("last_check_time");
            Timestamp currentTimestamp = new Timestamp((new Date()).getTime());
            if ((currentTimestamp.getTime() - lastTimestamp.getTime()) < timeValiedRange) {
                needExec = false;
            }
        }

        if (needExec) {
            Map<String, Object> param = new HashedMap();
            param.put("shop_no", shop_no);
            param.put("platforms", platforms);
            List<Map<String, Object>> list = service.selectSwitchInfo(param);
            if (list != null) {
                if (list.size() > 0) {
                    result = list.get(0);
                    Timestamp timestamp = new Timestamp((new Date()).getTime());
                    result.put("last_check_time", timestamp);
                    switchHash.put(plat_shop, result);
                }
            }
        }
        return result;
    }

    static {
        platformsList.add("MT");
        platformsList.add("EB");
        platformsList.add("JD");
        platformsList.add("KY");
    }
}
