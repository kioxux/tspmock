package com.gys.common.config.takeAway;

/**
 * 快药外卖配置
 * @author zhangdong
 * @date 2021/5/7 14:19
 */
public class KyConfig {

    /*** 接口地址*/
    public final static String KY_API_SERVER = "https://store-api.lyky.cn/";
    /*** 版本，默认是v1*/
    public final static String KY_API_VERSION = "v1";
    /*** 修改单个商品库存*/
    public final static String KY_STOCK_UPDATE = "/shop-product/update-stock-one";



}
