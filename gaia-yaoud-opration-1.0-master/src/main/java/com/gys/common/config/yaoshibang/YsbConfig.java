package com.gys.common.config.yaoshibang;

/**
 * @author zhangdong
 * @date 2021/6/22 19:43
 */
public class YsbConfig {

    //正式环境
    public static final String BASE_URL = "http://yst-api.ysbang.cn/misc-ysb";
    //测试环境
    //public static final String BASE_URL = "http://test.ysbang.cn/misc-ysb";
    /**
     * 订单查询
     */
    public static final String ORDER_QUERY = "/provider/getOrder";
    /**
     * 修改订单状态
     */
    public static final String ORDER_UPDATE = "/provider/updateOrderStatus";
    /**
     * 上传药店信息
     */
    public static final String UPDATE_DRUGSTORE = "/provider/updateDrugstore";
    /**
     * 上传药品价格
     */
    public static final String UPDATE_Price = "/provider/updatePrice";
    /**
     * 上传药品库存
     */
    public static final String UPDATE_Stock = "/provider/updateStock";



}
