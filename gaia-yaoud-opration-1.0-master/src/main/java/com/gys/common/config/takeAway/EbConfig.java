package com.gys.common.config.takeAway;

public class EbConfig {
    public final static String EB_API_SERVER = "https://api-be.ele.me";

    public final static String EB_API_VERSION = "3";

    public final static String EB_CMD_ORDER_CREATE_PUSH = "order.create";

    public final static String EB_CMD_ORDER_STATUS_PUSH = "order.status.push";

    public final static String EB_CMD_ORDER_DELIVERYSTATUS_PUSH = "order.deliveryStatus.push";

    public final static String EB_CMD_ORDER_REMIND_PUSH = "order.remind.push";

    /** 用户申请订单取消/退款 */
    public final static String EB_CMD_ORDER_USER_CANCEL = "order.user.cancel";

    public final static String EB_CMD_ORDER_PARTREFUND_PUSH = "order.partrefund.push";

    public final static String EB_CMD_ORDER_CONFIRM = "order.confirm";

    public final static String EB_CMD_ORDER_CANCEL = "order.cancel";

    public final static String EB_CMD_ORDER_GET = "order.get";

    public final static String EB_CMD_STOCK_UPDATE = "sku.stock.update.batch";

    public final static String EB_CMD_CATEGORY_GET="sku.shop.category.get";

    public final static String EB_CMD_CATEGORY_CREATE="sku.shop.category.create";

    public final static String EB_CMD_SKU_CREATE ="sku.create";

    public final static String EB_CMD_SKU_UPDATE ="sku.update";

    public final static String EB_CMD_SKU_LIST ="sku.list";

    public final static String EB_CMD_SHOP_LIST="shop.list";

    public final static String EB_CMD_DELIVERY_GET="order.delivery.get";

    public final static String EB_CMD_ORDER_AGREEREFUND="order.agreerefund";

    public final static String EB_CMD_ORDER_DISAGREEREFUND="order.disagreerefund";

    public final static String EB_CMD_ORDER_CALLDELIVERY="order.callDelivery";

    public final static String EB_CMD_ORDER_PICKCOMPLETE="order.pickcomplete";


//	1	超出配送范围
//2	商家已打烊
//3	商品已售完
//4	菜品价格发生变化
//5	顾客要求取消
//6	顾客重复订单
//7	商家太忙无法及时备货
//8	顾客信息错误
//9	假订单
//53	API商户系统向门店推送订单失败
//-1	自定义输入
}
