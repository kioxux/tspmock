package com.gys.common.interceptor;

import cn.hutool.core.util.ObjectUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangdong
 * @date 2021/6/29 17:00
 */
//@Configuration
public class FeignHeaderInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if(ObjectUtil.isNotEmpty(requestAttributes)){
            HttpServletRequest request = requestAttributes.getRequest();
            String token = request.getHeader("X-Token");
            requestTemplate.header("X-Token", token);
        }
    }
}
