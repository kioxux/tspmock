package com.gys.common.exception;

import com.gys.common.data.JsonResult;
import com.gys.util.UtilConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class ExceptionAdvice {
    @ExceptionHandler({RuntimeException.class})
    public JsonResult handleException(HttpServletRequest request, Exception e) throws Exception{
        log.error("handleException: {}",e.getMessage(), e);

        if (e instanceof NumberFormatException) {
            return new JsonResult(UtilConst.CODE_500, null, "数据格式化错误");
        }
        return JsonResult.error();
    }

    @ExceptionHandler({com.gys.common.exception.BusinessException.class})
    public JsonResult handleException(com.gys.common.exception.BusinessException e) {
        return JsonResult.fail(UtilConst.CODE_1001, e.getMessage());
    }

    /**
     * 参数效验统一拦截
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public JsonResult notValidExceptionHandle(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        Objects.requireNonNull(bindingResult.getFieldError());
        return JsonResult.error(UtilConst.CODE_1001,bindingResult.getFieldError().getDefaultMessage());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public JsonResult missingServletRequestParameter(MissingServletRequestParameterException e) {

        return JsonResult.error(UtilConst.CODE_1001, e.getParameterName().concat("不能为空"));
    }
}
