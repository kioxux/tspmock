package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;

/**
 * 新增商品信息入参
 */
@Data
public class GoodsInfoInput extends BaseInput implements Serializable {

    private String startDate;
    private String endDate;
    private String client;

    private String brId;
    /**
     * 1: 新增  0 ：编辑
     */
    private String flag;

    /**
     * 货号
     */
    private String yp_number;
    /**
     * 调用方法
     */
    private String method;
    /**
     * 药品编码   2.8新增商品返回mis系统唯一主键值
     */
    private String ypbm;
    /**
     * 药品名称
     */
    private String ypmc;
    /**
     * 拼音码
     */
    private String pym;
    /**
     * 化学名称
     */
    private String hxmc;
    /**
     * 化学拼音码
     */
    private String hxpym;
    /**
     * 剂型
     */
    private String jx;
    /**
     * 规格
     */
    private String gg;
    /**
     * 单位
     */
    private String dw;
    /**
     * 生产企业
     */
    private String scqy;
    /**
     * 是否中药
     */
    private String sfcy;
    /**
     * 是否特殊商品
     */
    private String yp_special;
    /**
     * 是否处方药
     */
    private String sfcfy;
    /**
     * 结算方式
     */
    private String yp_sup_settle;
    /**
     * 药品分类
     */
    private String ypfl;
    /**
     * 药品类别
     */
    private String yplb;
    /**
     * 批准文号
     */
    private String yp_appr_num;
    /**
     * 销售价格
     */
    private String xsjg;
    /**
     * 税率
     */
    private String yp_tax;
    /**
     * 企业编码
     */
    private String porgid;
    /**
     * 新增时间
     */
    private String aae036;
    /**
     * 操作人
     */
    private String aae011;
    /**
     * 条形码
     */
    private String txm;
    /**
     *最近进价
     */
    private String yp_lastprice;
    /**
     * 首营供应商
     */
    private String supplier;
    /**
     * 供货价
     */
    private String yp_deli_price;
    /**
     * 销售单位
     */
    private String yp_sale_unit;
    /**
     * 拆零数量
     */
    private String yp_dismounted;
    /**
     * 拆零单位
     */
    private String yp_dis_unit;
    /**
     * 拆零单价
     */
    private String yp_dis_pricee;
    /**
     * 养护标志
     */
    private String yp_maintenance;
    /**
     * 最近供应商
     */
    private String yp_supplier_name;
    /**
     * 会员价
     */
    private String yp_mem_price;

}
