package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;

/**
 * 已对码商品上传、修改入参
 */
@Data
public class GoodsCodeInput extends BaseInput implements Serializable {

    /**
     * 商品编码
     */
    private String ypbm;

    /**
     * 医保药品编码
     */
    private String id869;

    /**
     * 医保通用项目名称
     */
    private String yka003;

    /**
     * 生产厂家
     */
    private String ykd039;
}
