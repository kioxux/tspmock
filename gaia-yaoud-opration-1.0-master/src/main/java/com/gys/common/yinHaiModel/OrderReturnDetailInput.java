package com.gys.common.yinHaiModel;

import lombok.Data;

import java.io.Serializable;

/**
 * 零售退单明细实体
 */
@Data
public class OrderReturnDetailInput implements Serializable {

    /**
     * 商品编码
     */
    private String ypbm;

    /**
     * 药品批号
     */
    private String sl;

    /**
     * 单品数量
     */
    private String ypph;
}
