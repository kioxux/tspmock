package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 新增门店调拨信息入参
 */
@Data
public class TransforStockInfoInput extends BaseInput implements Serializable {
    private String startDate;
    private String endDate;
    private String client;

    private String brId;


    /**
     * 转库自定义单号
     */
    private String tgGivenNo;
    /**
     * 转出仓库点
     */
    private String tgGivenFrom;
    /**
     * 转入仓库点
     */
    private String tgGivenTo;
    /**
     * 转库单据总金额
     */
    private BigDecimal tgMoney;
    /**
     * 转库总数量
     */
    private BigDecimal tgAmount;
    /**
     * 操作员编码
     */
    private String aae011;
    /**
     * 操作时间
     */
    private String aae036;
    /**
     * 出库审核人编码
     */
    private String tgOutCheck1;
    /**
     * 出库审核时间
     */
    private String tgOutCheckTime1;
    /**
     * 调拨出库双人审核人编码
     */
    private String tgOutCheck2;
    /**
     * 调拨出库双人审核时间
     */
    private String tgOutCheckTime2;
    /**
     * 调拨入库审核人编码
     */
    private String tgInCheck1;
    /**
     * 调拨入库审核时间
     */
    private String tgInCheckTime1;
    /**
     * 调拨入库双人审核人编码
     */
    private String tgInCheck2;
    /**
     * 调拨入库双人审核时间
     */
    private String tgInCheckTime2;

    List<TransforStockDetailnput> details;
}
