package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 采购修正入参
 */
@Data
public class OrderModifyInput extends BaseInput implements Serializable {

    /**
     * 药品编码
     */
    private String ypbm;

    /**
     * 入库单号
     */
    private String shcd;

    /**
     * 原药品批号
     */
    private String ypph;

    /**
     * 调整后的药品批号
     */
    private String yd_ypph;

    /**
     * 原生产日期
     */
    private Date scrq;

    /**
     * 调整后的生产日期
     */
    private Date yd_scrq;

    /**
     * 原有效期
     */
    private Date yxqz;

    /**
     * 调整后的原有效期
     */
    private Date yd_yxqz;

    /**
     * 备注
     */
    private String yc_remark;

    /**
     * 操作员编码
     */
    private String aae011;
}
