package com.gys.common.yinHaiModel;


import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 报损或报溢制单入参
 */
@Data
public class InventoryManagerOrderInput extends BaseInput implements Serializable {

    /**
     * 制单机构编码
     */
    private String imOrgid;

    /**
     * 制单机构名称
     */
    private String kfmc;

    /**
     * 损益类型
     */
    private String imType;

    /**
     * 单据总金额
     */
    private BigDecimal imMoney;

    /**
     * 损溢单据审核人员编码
     */
    private String imChecker;

    /**
     * 损溢单据审核时间
     */
    private Date imChecktime;

    /**
     * 损溢单据备注信息
     */
    private String imRemark;

    /**
     * 操作员编码
     */
    private String aae011;

    /**
     * 操作时间
     */
    private String aae036;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 明细列表
     */
    private List<InventoryManagerOrderDetailInput> detail;
}
