package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 新增采购入库信息入参
 */
@Data
public class StockInfoInput extends BaseInput implements Serializable {
    /**
     * 随货单号
     */
    private String shdh;
    /**
     * 入库日期
     */
    private String rkrq;
    /**
     * 供应商代码
     */
    private String wldwdm;
    /**
     * 供应商名称
     */
    private String supplier;
    /**
     * 金额合计
     */
    private String jehj;
    /**
     * 操作人
     */
    private String aae011;
    /**
     * 操作时间
     */
    private String aae036;
    /**
     * 收货人
     */
    private String shr;
    /**
     * 收货时间
     */
    private String shsj;
    /**
     * 验收人
     */
    private String shYsrOne;
    /**
     * 验收时间
     */
    private String shYsdateOne;
    /**
     * 验收人2
     */
    private String shYsrTwo;
    /**
     * 验收时间2
     */
    private String shYsdateTwo;
    /**
     * 收货类型
     */
    private String shlx;
    /**
     * 收货状态
     */
    private String shStatus;

    List<StockDetailInfoInput> details;
}
