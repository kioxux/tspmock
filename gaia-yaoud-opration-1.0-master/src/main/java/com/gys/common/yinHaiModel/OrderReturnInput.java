package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 零售退单入参
 */
@Data
public class OrderReturnInput extends BaseInput implements Serializable {

    /**
     * 销售单号
     */
    private String xsdh;

    /**
     * 退单操作员
     */
    private String aae011;

    /**
     * 销售单号
     */
    private List<OrderReturnDetailInput> detail;


}
