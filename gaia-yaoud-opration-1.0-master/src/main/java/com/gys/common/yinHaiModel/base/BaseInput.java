package com.gys.common.yinHaiModel.base;

import lombok.Data;

@Data
public class BaseInput {

    /**
     * 调用方法 method
     */
    private String method;

    /**
     * 企业编码 porgid
     */
    private String porgid;

    /**
     * 门店编码 orgid
     */
    private String orgid;
}
