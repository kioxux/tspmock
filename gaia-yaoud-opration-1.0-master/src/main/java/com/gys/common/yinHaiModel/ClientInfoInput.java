package com.gys.common.yinHaiModel;


import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;

/**
 * 供应商信息
 */
@Data
public class ClientInfoInput extends BaseInput implements Serializable {

    private String startDate;
    private String endDate;
    private String client;

    private String brId;

    /**
     * 供货商名称
     */
    private String wldwmc;
    /**
     * 单位类别
     */
    private String wldwlb;
    /**
     * 操作员
     */
    private String czy;
    /**
     * 新增时间
     */
    private String aae036;
    /**
     * 拼音码
     */
    private String pym;
    /**
     * 企业编码
     */
    private String porgid;
    /**
     *企业地址
     */
    private String qydz;
    /**
     * 仓库地址
     */
    private String ckdz;
    /**
     * 往来单位代码
     */
    private String wldwdm;
    /**
     * 调用方法
     */
    private String method;
}
