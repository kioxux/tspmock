package com.gys.common.yinHaiModel;


import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;

/**
 * 新增采购入库明细信息入参
 */
@Data
public class StockDetailInfoInput extends BaseInput implements Serializable {
    private String startDate;
    private String endDate;
    private String client;

    private String brId;

    private String shdh;
    private String supplier;
    private String jehjH;
    private String aae011;
    private String aae036;
    private String shr;
    private String shsj;
    private String shYsrOne;
    private String shYsdateOne;
    private String shlx;
    private String shStatus;

    /**
     * 入库日期
     */
    private String rkrq;
    /**
     * 药品编码
     */
    private String ypbm;
    /**
     * 药品批号
     */
    private String ypph;
    /**
     * 生产企业
     */
    private String wldwdm;
    /**
     * 生产日期
     */
    private String scrq;
    /**
     * 有效期至
     */
    private String yxqz;
    /**
     * 进价
     */
    private String hsjg;
    /**
     * 数量
     */
    private String sl;
    /**
     * 金额合计
     */
    private String jehj;
    /**
     * 应收金额
     */
    private String ysjg;
    /**
     * 拒收数量
     */
    private String ypRefuse;
    /**
     * 是否特殊商品
     */
    private String ypSpecial;
}
