package com.gys.common.yinHaiModel;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 采购退货入参明细
 */
@Data
public class CancelStockDetailInput implements Serializable {

    /**
     * 商品编码
     */
    private String ypbm;

    /**
     * 商品批号
     */
    private String ypph;

    /**
     * 商品单位
     */
    private String dw;

    /**
     * 含税单价
     */
    private BigDecimal thPrice;

    /**
     * 单品数量
     */
    private BigDecimal sl;

    /**
     * 单品总金额
     */
    private BigDecimal thTotal;

    /**
     * 是否管理特殊药品
     */
    private String ypSpecial;

    /**
     * 退货单号
     */
    private String returnOrderNo;
}
