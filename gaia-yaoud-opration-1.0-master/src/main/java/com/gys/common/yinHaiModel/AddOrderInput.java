package com.gys.common.yinHaiModel;


import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 零售下单入参
 */
@Data
public class AddOrderInput extends BaseInput implements Serializable {

    /**
     * 实收金额
     */
    private BigDecimal sfReceivable;

    /**
     * 操作员编码
     */
    private String aae011;

    /**
     * 销售点编码
     */
    private String posId;

    /**
     * 支付方式
     */
    private String sfPay;

    /**
     * 备注
     */
    private String remark;

    /**
     * 订单号
     */
    private String billNo;

    /**
     * 零售下单列表
     */
    private List<AddOrderDetailInput> detail;
}
