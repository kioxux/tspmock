package com.gys.common.yinHaiModel;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 报损或报溢制单入参明细列表
 */
@Data
public class InventoryManagerOrderDetailInput implements Serializable {

    /**
     * 商品编码
     */
    private String ypbm;

    /**
     * 商品名称
     */
    private String ypmc;

    /**
     * 商品货号
     */
    private String ypNumber;

    /**
     * 生产企业名称
     */
    private String scqyName;

    /**
     * 商品规格
     */
    private String gg;

    /**
     * 商品单位
     */
    private String dw;

    /**
     * 商品剂型
     */
    private String jx;

    /**
     * 商品批号
     */
    private String ypph;

    /**
     * 损溢原因
     */
    private String mdReason;

    /**
     * 损溢数量
     */
    private BigDecimal mdAmount;

    /**
     * 损溢明细信息行号
     */
    private String arrayindex;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 损益标志  1报损；2报溢
     */
    private String flag;

    /**
     * 损益金额 如果损益标志位1时 此金额为报损   如果损益标志位2时 此金额为报溢
     */
    private BigDecimal money;
}
