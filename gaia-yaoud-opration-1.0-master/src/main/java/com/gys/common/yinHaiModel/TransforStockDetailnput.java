package com.gys.common.yinHaiModel;

import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 新增门店调拨信息入参
 */
@Data
public class TransforStockDetailnput extends BaseInput implements Serializable {
    /**
     * 商品编码
     */
    private String ypbm;
    /**
     * 商品批号
     */
    private String ypph;
    /**
     * 生产日期
     */
    private String scrq;
    /**
     * 有效期至
     */
    private String yxqz;
    /**
     * 含税单价
     */
    private BigDecimal hsjg;
    /**
     * 单品数量
     */
    private BigDecimal tdAmount;
    /**
     * 单品总金额
     */
    private BigDecimal tdTotal;
    /**
     * 是否特殊
     */
    private String ypSpecial;
    private String tgGivenNo;
    private String tgGivenFrom;
    private String tgGivenTo;
    private String aae011;
    private String aae036;
    private String tgOutCheck1;
    private String tgOutCheckTime1;
    private String tgInCheck1;
    private String tgInCheckTime1;
}
