package com.gys.common.yinHaiModel;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 零售下单入参明细列表
 */
@Data
public class AddOrderDetailInput implements Serializable {

    /**
     * 商品编码
     */
    private String ypbm;

    /**
     * 商品名称
     */
    private String ypmc;

    /**
     * 商品规格
     */
    private String gg;

    /**
     * 商品单位
     */
    private String dw;

    /**
     * 商品数量
     */
    private BigDecimal sl;

    /**
     * 单品单价
     */
    private BigDecimal price;

    /**
     * 药品批号
     */
    private String ypph;

    /**
     * 商品剂型
     */
    private String Jx;

    /**
     * 销售单号
     */
    private String billNo;
}
