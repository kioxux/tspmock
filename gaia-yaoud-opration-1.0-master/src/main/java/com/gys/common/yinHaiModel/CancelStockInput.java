package com.gys.common.yinHaiModel;


import com.gys.common.yinHaiModel.base.BaseInput;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 采购退货入参
 */
@Data
public class CancelStockInput extends BaseInput implements Serializable {

    /**
     * 退货时间
     */
    private Date thDate;

    /**
     * 往来单代编码
     */
    private String wldwdm;

    /**
     * 往来单位名称
     */
    private String supplier;

    /**
     * 退货金额合计
     */
    private BigDecimal thAmount;

    /**
     * 退货总数量
     */
    private BigDecimal thQuanlity;

    /**
     * 操作员编码
     */
    private String aae011;

    /**
     * 操作时间
     */
    private Date aae036;

    /**
     * 退货审核人员编码
     */
    private String thVerifierOne;

    /**
     * 退货审核时间
     */
    private Date thDateOne;

    /**
     * 退货双人复核人员编码
     */
    private String thVerifierTwo;

    /**
     * 退货双人复核验收时间
     */
    private Date thDateTwo;

    /**
     * 退货审核状态
     */
    private String thStatus;

    /**
     * 退货单号
     */
    private String returnOrderNo;

    /**
     *
     */
    private List<CancelStockDetailInput> detail;
}
