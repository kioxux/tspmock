package com.gys.common.vo;

import com.gys.business.mapper.entity.MemberClass;
import lombok.Data;

import java.util.List;

@Data
public class MemberClassVo {

   private List<MemberClass> list;
}
