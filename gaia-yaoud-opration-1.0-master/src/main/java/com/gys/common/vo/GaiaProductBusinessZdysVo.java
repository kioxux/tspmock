package com.gys.common.vo;

import lombok.Data;

import java.util.List;

/**
 * @author csm
 * @date 2021/12/21 - 18:25
 */
@Data
public class GaiaProductBusinessZdysVo {
    private List<String> zdy1;
    private List<String>  zdy2;
    private List<String>  zdy3;
    private List<String>  zdy4;
    private List<String>  zdy5;
}
