package com.gys.common.vo;



import com.gys.business.mapper.entity.GaiaClientBenefitLevelInfo;
import lombok.Data;

/**
 * @author csm
 * @date 2021/12/18 - 17:10
 */
@Data
public class GaiaClientBenefitLevelInfoVo extends GaiaClientBenefitLevelInfo  {

    /**
     * 加盟商名称
     */
    private String francName;


}
