package com.gys.common.constant;

/**
 * @author wu mao yin
 * @Title: 会员管理常量定义
 * @date 2021/12/816:45
 */
public interface MemberManagerConstant {

    String POINT_RULE_CODE_PREFIX = "DXGZ";

    String POINT_RULE_CONTENT_PREFIX = "DXGZNR";

}
