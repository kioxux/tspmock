package com.gys.common.constant;

import com.gys.common.data.app.salereport.ProBigClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 商品定位统计常量
 * @Package
 * @date 2021/11/3017:46
 */
public class ProPositionConstant {

    public static List<ProBigClass> CATEGORY = new ArrayList<ProBigClass>() {{
        add(new ProBigClass("", "全品", null));
        add(new ProBigClass("1", "处方药", Collections.singletonList("1")));
        add(new ProBigClass("2", "OTC", Collections.singletonList("2")));
        add(new ProBigClass("3", "药品小计", Arrays.asList("1", "2")));
        add(new ProBigClass("4", "中药", Collections.singletonList("3")));
        add(new ProBigClass("5", "非药品", Arrays.asList("4", "5", "6", "7", "8", "9", "H")));
    }};

}
