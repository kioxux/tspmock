//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.base;

import java.io.OutputStream;
import java.util.Collection;

public interface ExportExcelInterface<T> {
    void exportExcel(String[] headers, Collection<T> dataset, OutputStream out, String pattern) throws Exception;

    void exportExcel2(String title, String[] headers1, String[] headers2, Collection<T> dataset, OutputStream out, String pattern) throws Exception;

    void exportExcelInitStyle(String[] headers, Collection<T> dataset, OutputStream out, String pattern) throws Exception;
}
