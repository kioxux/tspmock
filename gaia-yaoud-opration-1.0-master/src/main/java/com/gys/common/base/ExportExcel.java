//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.base;

import cn.hutool.core.date.DateUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.hssf.util.HSSFColor.BLUE;
//import org.apache.poi.hssf.util.HSSFColor.LIGHT_YELLOW;
//import org.apache.poi.hssf.util.HSSFColor.SKY_BLUE;
//import org.apache.poi.hssf.util.HSSFColor.VIOLET;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.util.HtmlUtils;

public class ExportExcel<T> implements ExportExcelInterface<T> {
    private static Pattern NUMBER_PATTERN = Pattern.compile("^//d+(//.//d+)?$");

    public ExportExcel() {
    }

    public void exportExcel(String[] headers, Collection<T> dataset, OutputStream out, String pattern) throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        sheet.setDefaultColumnWidth(15);
        XSSFCellStyle style = workbook.createCellStyle();
        //chenhao TODO
//        style.setFillForegroundColor(SKY_BLUE.index);
//        style.setFillPattern((short)1);
//        style.setBorderBottom((short)1);
//        style.setBorderLeft((short)1);
//        style.setBorderRight((short)1);
//        style.setBorderTop((short)1);
//        style.setAlignment((short)2);
        XSSFFont font = workbook.createFont();
        //chenhao TODO
//        font.setColor(VIOLET.index);
        font.setFontHeightInPoints((short)12);
//        font.setBoldweight((short)700);
        style.setFont(font);
        XSSFCellStyle style2 = workbook.createCellStyle();
        //chenhao TODO
//        style2.setFillForegroundColor(LIGHT_YELLOW.index);
//        style2.setFillPattern((short)1);
//        style2.setBorderBottom((short)1);
//        style2.setBorderLeft((short)1);
//        style2.setBorderRight((short)1);
//        style2.setBorderTop((short)1);
//        style2.setAlignment((short)2);
//        style2.setVerticalAlignment((short)1);
        XSSFFont font2 = workbook.createFont();
//        font2.setBoldweight((short)400);
        style2.setFont(font2);
        XSSFRow row = sheet.createRow(0);

        int index;
        for(index = 0; index < headers.length; ++index) {
            XSSFCell cell = row.createCell(index);
            cell.setCellStyle(style);
            XSSFRichTextString text = new XSSFRichTextString(headers[index]);
            cell.setCellValue(text);
        }

        index = 0;
        Iterator<T> it = dataset.iterator();
        XSSFFont font3 = workbook.createFont();
        //chenhao TODO
//        font3.setColor(BLUE.index);

        while(it.hasNext()) {
            ++index;
            row = sheet.createRow(index);
            T t = it.next();
            Field[] fields = t.getClass().getDeclaredFields();

            for(int i = 0; i < fields.length; ++i) {
                XSSFCell cell = row.createCell(i);
                cell.setCellStyle(style2);
                Field field = fields[i];
                String fieldName = field.getName();
                String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                Class tCls = t.getClass();
                Method getMethod = tCls.getMethod(getMethodName);
                Object value = getMethod.invoke(t);
                String textValue = null;
                if (value instanceof Date) {
                    Date date = (Date)value;
                    textValue = DateUtil.format(date, "yyyy-MM-dd");
                } else if (value == null) {
                    textValue = "";
                } else {
                    textValue = HtmlUtils.htmlUnescape(value.toString());
                }

                if (textValue != null) {
                    Matcher matcher = NUMBER_PATTERN.matcher(textValue);
                    if (matcher.matches()) {
                        cell.setCellValue(Double.parseDouble(textValue));
                    } else {
                        XSSFRichTextString richString = new XSSFRichTextString(textValue);
                        richString.applyFont(font3);
                        cell.setCellValue(richString);
                    }
                }
            }
        }

        workbook.write(out);
    }

    public void exportExcel2(String title, String[] headers1, String[] headers2, Collection<T> dataset, OutputStream out, String pattern) throws Exception {
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
        HSSFSheet sheet = hssfWorkbook.createSheet(title);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 2, 0, 10);
        sheet.addMergedRegion(cellRangeAddress);
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        cell.setCellValue(title);
        HSSFCellStyle style = hssfWorkbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        HSSFFont font = hssfWorkbook.createFont();
        font.setFontHeightInPoints((short)20);
        style.setFont(font);
        cell.setCellStyle(style);
        HSSFCellStyle style2 = hssfWorkbook.createCellStyle();
        style2.setAlignment(HorizontalAlignment.CENTER);
        style2.setVerticalAlignment(VerticalAlignment.CENTER);
        style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style2.setBorderBottom(BorderStyle.THIN);
        style2.setBorderTop(BorderStyle.THIN);
        style2.setBorderLeft(BorderStyle.THIN);
        style2.setBorderRight(BorderStyle.THIN);
        HSSFCellStyle style3 = hssfWorkbook.createCellStyle();
        style3.setAlignment(HorizontalAlignment.CENTER);
        style3.setVerticalAlignment(VerticalAlignment.CENTER);
        style3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style3.setBorderBottom(BorderStyle.THIN);
        style3.setBorderTop(BorderStyle.THIN);
        style3.setBorderLeft(BorderStyle.THIN);
        style3.setBorderRight(BorderStyle.THIN);
        HSSFFont font2 = hssfWorkbook.createFont();
        font2.setFontHeightInPoints((short)12);
        style2.setFont(font2);
        HSSFRow row3 = sheet.createRow(6);

        for(int i = 0; i < headers1.length; ++i) {
            sheet.setColumnWidth(i + 2, sheet.getColumnWidth(i + 2) * 15 / 7);
            HSSFCell titleCell = row3.createCell(i + 2);
            titleCell.setCellStyle(style2);
            HSSFRichTextString text = new HSSFRichTextString(headers1[i]);
            titleCell.setCellValue(text);
        }

        HSSFRow row5 = sheet.createRow(11);

        for(int i = 0; i < headers2.length; ++i) {
            HSSFCell titleCell = row5.createCell(i + 2);
            titleCell.setCellStyle(style2);
            HSSFRichTextString text = new HSSFRichTextString(headers2[i]);
            titleCell.setCellValue(text);
        }

        Iterator<T> it = dataset.iterator();

        HSSFRow row6;
        while(it.hasNext()) {
            T t = it.next();
            HSSFRow row4 = sheet.createRow(7);
            row6 = sheet.createRow(12);
            Field[] fields = t.getClass().getDeclaredFields();

            for(int i = 0; i < fields.length; ++i) {
                HSSFCell valueCell;
                Field field;
                String fieldName;
                String getMethodName;
                Class tCls;
                Method getMethod;
                Object value;
                String textValue;
                Date date;
                HSSFRichTextString richString;
                Matcher matcher;
                if (i <= 6) {
                    valueCell = row4.createCell(i + 2);
                    valueCell.setCellStyle(style3);
                    field = fields[i];
                    fieldName = field.getName();
                    getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    tCls = t.getClass();
                    getMethod = tCls.getMethod(getMethodName);
                    value = getMethod.invoke(t);
                    textValue = null;
                    if (value instanceof Date) {
                        date = (Date)value;
                        textValue = DateUtil.format(date, "yyyy-MM-dd");
                    } else if (value == null) {
                        textValue = "";
                    } else {
                        textValue = HtmlUtils.htmlUnescape(value.toString());
                    }

                    if (textValue != null) {
                        matcher = NUMBER_PATTERN.matcher(textValue);
                        if (matcher.matches()) {
                            valueCell.setCellValue(Double.parseDouble(textValue));
                        } else {
                            richString = new HSSFRichTextString(textValue);
                            richString.applyFont(font2);
                            valueCell.setCellValue(richString);
                        }
                    }
                } else {
                    valueCell = row6.createCell(i - 5);
                    valueCell.setCellStyle(style3);
                    field = fields[i];
                    fieldName = field.getName();
                    getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    tCls = t.getClass();
                    getMethod = tCls.getMethod(getMethodName);
                    value = getMethod.invoke(t);
                    textValue = null;
                    if (value instanceof Date) {
                        date = (Date)value;
                        textValue = DateUtil.format(date, "yyyy-MM-dd");
                    } else if (value == null) {
                        textValue = "";
                    } else {
                        textValue = HtmlUtils.htmlUnescape(value.toString());
                    }

                    if (textValue != null) {
                        matcher = NUMBER_PATTERN.matcher(textValue);
                        if (matcher.matches()) {
                            valueCell.setCellValue(Double.parseDouble(textValue));
                        } else {
                            richString = new HSSFRichTextString(textValue);
                            richString.applyFont(font2);
                            valueCell.setCellValue(richString);
                        }
                    }
                }
            }
        }

        row6 = sheet.createRow(14);
        HSSFCell signCell = row6.createCell(2);
        signCell.setCellValue("药师签名：");
        signCell.setCellStyle(style2);
        row6 = sheet.createRow(15);
        HSSFCell dateCell = row6.createCell(2);
        dateCell.setCellValue("打印日期：");
        dateCell.setCellStyle(style2);
        HSSFCell valueCell = row6.createCell(3);
        valueCell.setCellValue(DateUtil.format(new Date(), "yyyyMMdd"));
        valueCell.setCellStyle(style2);
        hssfWorkbook.write(out);
    }

    public void exportExcelInitStyle(String[] headers, Collection<T> dataset, OutputStream out, String pattern) throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        sheet.setDefaultColumnWidth(15);
        XSSFCellStyle style = workbook.createCellStyle();
        //chenhao TODO
//        style.setFillForegroundColor(SKY_BLUE.index);
//        style.setFillPattern((short)1);
//        style.setBorderBottom((short)1);
//        style.setBorderLeft((short)1);
//        style.setBorderRight((short)1);
//        style.setBorderTop((short)1);
//        style.setAlignment((short)2);
        XSSFFont font = workbook.createFont();
        //chenhao TODO
//        font.setColor(VIOLET.index);
        font.setFontHeightInPoints((short)12);
//        font.setBoldweight((short)700);
        style.setFont(font);
        XSSFCellStyle style2 = workbook.createCellStyle();
        //chenhao TODO
//        style2.setFillForegroundColor(LIGHT_YELLOW.index);
//        style2.setFillPattern((short)1);
//        style2.setBorderBottom((short)1);
//        style2.setBorderLeft((short)1);
//        style2.setBorderRight((short)1);
//        style2.setBorderTop((short)1);
//        style2.setAlignment((short)2);
//        style2.setVerticalAlignment((short)1);
        XSSFFont font2 = workbook.createFont();
//        font2.setBoldweight((short)400);
//        style2.setFont(font2);
        CellRangeAddress callRangeAddress = new CellRangeAddress(0, 0, 0, 36);
        CellRangeAddress callRangeAddress1 = new CellRangeAddress(1, 3, 0, 0);
        CellRangeAddress callRangeAddress2 = new CellRangeAddress(1, 3, 1, 1);
        CellRangeAddress callRangeAddress3 = new CellRangeAddress(1, 3, 2, 2);
        CellRangeAddress callRangeAddress4 = new CellRangeAddress(1, 3, 3, 3);
        CellRangeAddress callRangeAddress5 = new CellRangeAddress(1, 3, 4, 4);
        CellRangeAddress callRangeAddress6 = new CellRangeAddress(1, 3, 5, 5);
        CellRangeAddress callRangeAddress7 = new CellRangeAddress(1, 3, 6, 6);
        CellRangeAddress callRangeAddress8 = new CellRangeAddress(1, 3, 7, 7);
        CellRangeAddress callRangeAddress9 = new CellRangeAddress(1, 3, 8, 8);
        CellRangeAddress callRangeAddress10 = new CellRangeAddress(1, 3, 9, 9);
        CellRangeAddress callRangeAddress11 = new CellRangeAddress(1, 3, 10, 10);
        CellRangeAddress callRangeAddress12 = new CellRangeAddress(1, 3, 11, 11);
        CellRangeAddress callRangeAddress13 = new CellRangeAddress(1, 3, 12, 12);
        CellRangeAddress callRangeAddress14 = new CellRangeAddress(1, 3, 13, 13);
        CellRangeAddress callRangeAddress15 = new CellRangeAddress(1, 3, 14, 14);
        CellRangeAddress callRangeAddress16 = new CellRangeAddress(1, 3, 15, 15);
        CellRangeAddress callRangeAddress17 = new CellRangeAddress(1, 3, 16, 16);
        CellRangeAddress callRangeAddress18 = new CellRangeAddress(1, 1, 17, 21);
        CellRangeAddress callRangeAddress19 = new CellRangeAddress(2, 2, 17, 18);
        CellRangeAddress callRangeAddress20 = new CellRangeAddress(2, 2, 19, 20);
        CellRangeAddress callRangeAddress21 = new CellRangeAddress(1, 3, 22, 22);
        CellRangeAddress callRangeAddress22 = new CellRangeAddress(1, 3, 23, 23);
        CellRangeAddress callRangeAddress23 = new CellRangeAddress(1, 3, 24, 24);
        CellRangeAddress callRangeAddress24 = new CellRangeAddress(1, 3, 25, 25);
        CellRangeAddress callRangeAddress25 = new CellRangeAddress(1, 3, 26, 26);
        CellRangeAddress callRangeAddress26 = new CellRangeAddress(1, 3, 27, 27);
        CellRangeAddress callRangeAddress27 = new CellRangeAddress(1, 2, 28, 29);
        CellRangeAddress callRangeAddress28 = new CellRangeAddress(1, 2, 30, 31);
        CellRangeAddress callRangeAddress29 = new CellRangeAddress(1, 3, 32, 32);
        CellRangeAddress callRangeAddress30 = new CellRangeAddress(1, 3, 33, 33);
        CellRangeAddress callRangeAddress31 = new CellRangeAddress(1, 3, 34, 34);
        CellRangeAddress callRangeAddress32 = new CellRangeAddress(1, 3, 35, 35);
        XSSFRow rowHead = sheet.createRow(0);
        XSSFCell cellHead = rowHead.createCell(0);
        cellHead.setCellStyle(style);
        cellHead.setCellValue("促销商品海报");
        XSSFRow row = sheet.createRow(1);

        for(int i = 0; i < headers.length; ++i) {
            XSSFCell cell = row.createCell(i);
            cell.setCellStyle(style);
            XSSFRichTextString text = new XSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }

        XSSFRow rowTwo = sheet.createRow(2);
        String[] titleTwo = new String[]{"版一ABC", "版二DE", "版三F"};

        for(int i = 0; i < titleTwo.length * 2; i += 2) {
            XSSFCell cell2 = rowTwo.createCell(i + 17);
            cell2.setCellStyle(style);
            cell2.setCellValue(titleTwo[i / 2]);
        }

        XSSFRow rowThree = sheet.createRow(3);
        String[] titleThree = new String[]{"室内", "室外", "室内", "室外", "伍缘"};

        for(int i = 0; i < titleThree.length; ++i) {
            XSSFCell cell2 = rowThree.createCell(i + 17);
            cell2.setCellStyle(style);
            cell2.setCellValue(titleThree[i]);
        }

        String[] titlePS = new String[]{"直送", "配送", "直送", "配送"};

        int index;
        for(index = 0; index < titlePS.length; ++index) {
            XSSFCell cell2 = rowThree.createCell(index + 28);
            cell2.setCellStyle(style);
            cell2.setCellValue(titlePS[index]);
        }

        sheet.addMergedRegion(callRangeAddress);
        sheet.addMergedRegion(callRangeAddress1);
        sheet.addMergedRegion(callRangeAddress2);
        sheet.addMergedRegion(callRangeAddress3);
        sheet.addMergedRegion(callRangeAddress4);
        sheet.addMergedRegion(callRangeAddress5);
        sheet.addMergedRegion(callRangeAddress6);
        sheet.addMergedRegion(callRangeAddress7);
        sheet.addMergedRegion(callRangeAddress8);
        sheet.addMergedRegion(callRangeAddress9);
        sheet.addMergedRegion(callRangeAddress10);
        sheet.addMergedRegion(callRangeAddress11);
        sheet.addMergedRegion(callRangeAddress12);
        sheet.addMergedRegion(callRangeAddress13);
        sheet.addMergedRegion(callRangeAddress14);
        sheet.addMergedRegion(callRangeAddress15);
        sheet.addMergedRegion(callRangeAddress16);
        sheet.addMergedRegion(callRangeAddress17);
        sheet.addMergedRegion(callRangeAddress18);
        sheet.addMergedRegion(callRangeAddress19);
        sheet.addMergedRegion(callRangeAddress20);
        sheet.addMergedRegion(callRangeAddress21);
        sheet.addMergedRegion(callRangeAddress22);
        sheet.addMergedRegion(callRangeAddress23);
        sheet.addMergedRegion(callRangeAddress24);
        sheet.addMergedRegion(callRangeAddress25);
        sheet.addMergedRegion(callRangeAddress26);
        sheet.addMergedRegion(callRangeAddress27);
        sheet.addMergedRegion(callRangeAddress28);
        sheet.addMergedRegion(callRangeAddress29);
        sheet.addMergedRegion(callRangeAddress30);
        sheet.addMergedRegion(callRangeAddress31);
        sheet.addMergedRegion(callRangeAddress32);
        index = 3;
        Iterator<T> it = dataset.iterator();
        XSSFFont font3 = workbook.createFont();
        //chenhao TODO
//        font3.setColor(BLUE.index);

        while(it.hasNext()) {
            ++index;
            row = sheet.createRow(index);
            T t = it.next();
            Field[] fields = t.getClass().getDeclaredFields();

            for(int i = 0; i < fields.length; ++i) {
                XSSFCell cell = row.createCell(i);
                cell.setCellStyle(style2);
                Field field = fields[i];
                String fieldName = field.getName();
                String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                Class tCls = t.getClass();
                Method getMethod = tCls.getMethod(getMethodName);
                Object value = getMethod.invoke(t);
                String textValue = null;
                if (value instanceof Date) {
                    Date date = (Date)value;
                    textValue = DateUtil.format(date, "yyyy-MM-dd");
                } else if (value == null) {
                    textValue = "";
                } else {
                    textValue = HtmlUtils.htmlUnescape(value.toString());
                }

                if (textValue != null) {
                    Matcher matcher = NUMBER_PATTERN.matcher(textValue);
                    if (matcher.matches()) {
                        cell.setCellValue(Double.parseDouble(textValue));
                    } else {
                        XSSFRichTextString richString = new XSSFRichTextString(textValue);
                        richString.applyFont(font3);
                        cell.setCellValue(richString);
                    }
                }
            }
        }

        workbook.write(out);
    }

    public static void exportModelFile(HttpServletResponse res, String fileName, String modelPath) {
        res.setHeader("content-type", "text/plain");
        res.setHeader("content-type", "application/x-msdownload;");
        res.setContentType("text/plain; charset=utf-8");
        res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        byte[] buff = new byte[1024];
        BufferedInputStream bis = null;
        ServletOutputStream os = null;

        try {
            os = res.getOutputStream();
            ClassPathResource resource = new ClassPathResource(modelPath);
//            bis = new BufferedInputStream(new FileInputStream(new File(modelPath)));
            bis = new BufferedInputStream(resource.getInputStream());
            for(int i = bis.read(buff); i != -1; i = bis.read(buff)) {
                os.write(buff, 0, buff.length);
                os.flush();
            }
        } catch (IOException var15) {
            var15.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException var14) {
                    var14.printStackTrace();
                }
            }

        }

    }

    public static void exportFile(HttpServletResponse res, String fileName, String filePath) {
        res.setHeader("content-type", "text/plain");
        res.setHeader("content-type", "application/x-msdownload;");
        res.setContentType("text/plain; charset=utf-8");
        res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        byte[] buff = new byte[1024];
        BufferedInputStream bis = null;
        ServletOutputStream os = null;

        try {
            ClassPathResource resource = new ClassPathResource("modelFile/" + filePath);
            os = res.getOutputStream();
            bis = new BufferedInputStream(resource.getInputStream());

            for(int i = bis.read(buff); i != -1; i = bis.read(buff)) {
                os.write(buff, 0, buff.length);
                os.flush();
            }
        } catch (IOException var16) {
            var16.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException var15) {
                    var15.printStackTrace();
                }
            }

        }

    }
}
