//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.base;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.redis.RedisManager;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;


public class BaseController {
    @Autowired
    private RedisManager redisManager;

    public BaseController() {
    }

    @InitBinder
    public void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }

    public GetLoginOutData getLoginUser(HttpServletRequest request) {
        String userInfo = (String)this.redisManager.get(request.getHeader("X-Token"));
        JSONObject jsonObject = JSON.parseObject(userInfo);
        GetLoginOutData data = JSONObject.toJavaObject(jsonObject, GetLoginOutData.class);
        return data;
    }

    public GetLoginOutData getLoginUser(String token) {
        String userInfo = (String)this.redisManager.get(token);
        JSONObject jsonObject = JSON.parseObject(userInfo);
        GetLoginOutData data = JSONObject.toJavaObject(jsonObject, GetLoginOutData.class);
        return data;
    }

    protected void exportExcel(HttpServletResponse response, List<?> list, String[] header, ExportExcelInterface export) throws Exception {
        Date dt = new Date();
        String fileName = (new Long(dt.getTime())).toString();
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        ServletOutputStream out = response.getOutputStream();

        try {
            export.exportExcel(header, list, out, "yyyy-MM-dd");
            out.flush();
        } catch (Exception var12) {
            throw var12;
        } finally {
            out.close();
        }

    }

//    private String getCellValue(Cell cell) {
//        String firstCell = "";
//        if (cell == null) {
//            firstCell = "";
//        } else if (cell.getCellType() == 0) {
//            if (HSSFDateUtil.isCellDateFormatted(cell)) {
//                SimpleDateFormat sdf = null;
//                if (cell.getCellStyle().getDataFormat() == HSSFDataFormat.getBuiltinFormat("h:mm")) {
//                    sdf = new SimpleDateFormat("HH:mm");
//                } else {
//                    sdf = new SimpleDateFormat("yyyy-MM-dd");
//                }
//
//                Date date = cell.getDateCellValue();
//                firstCell = sdf.format(date);
//            } else {
//                DecimalFormat df = new DecimalFormat("0");
//                firstCell = df.format(cell.getNumericCellValue());
//            }
//        } else if (cell.getCellType() == 1) {
//            firstCell = cell.getStringCellValue();
//        }
//
//        return firstCell;
//    }

    public <T> List<T> importExcel(MultipartFile uploadExcel, Class<T> t, Integer sheetNo, Integer firstRowNum) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
        Workbook workbook = null;
        String fileName = uploadExcel.getOriginalFilename();
        String fileEnd = fileName.substring(fileName.indexOf(".") + 1);
        Sheet sheet = null;
        if ("xls".equals(fileEnd)) {
            workbook = new HSSFWorkbook(uploadExcel.getInputStream());
            sheet = (HSSFSheet)workbook.getSheetAt(sheetNo);
        } else {
            workbook = new XSSFWorkbook(uploadExcel.getInputStream());
            sheet = (XSSFSheet)workbook.getSheetAt(sheetNo);
        }

        List<T> list = new ArrayList();

        for(int i = firstRowNum; i <= ((Sheet)sheet).getLastRowNum(); ++i) {
            Row row = ((Sheet)sheet).getRow(i);
            if (row != null) {
                T instance = t.newInstance();
                Field[] fields = instance.getClass().getDeclaredFields();
                int count = 0;
                int length = fields.length;

                for(int k = 0; k < length; ++k) {
                    Method setMethod = instance.getClass().getMethod("set" + StringUtils.capitalize(fields[k].getName()), String.class);
                    if (k == length - 1) {
                        String index = i + 1 + "";
                        setMethod.invoke(instance, index);
                    } else {
                        Cell cell = row.getCell(k);
                        if (cell == null) {
                            setMethod.invoke(instance, "");
                            ++count;
                        } else {
                            cell.setCellType(CellType.STRING);
                            if (StrUtil.isBlank(cell.getStringCellValue())) {
                                ++count;
                            }

                            setMethod.invoke(instance, cell.getStringCellValue());
                        }
                    }
                }

                if (count != length - 1) {
                    list.add(instance);
                }
            }
        }

        return list;
    }

    /**
     * 创建操作日志map对象
     * @param operatetype
     * @param platform_type
     * @param operatename
     * @param remark
     * @return
     * @author：Li.Deman
     * @date：2018/12/14
     */
    public Map<String, Object> createOperateLogMap(String operatetype,
                                                   String platform_type,
                                                   String operatename,
                                                   String remark){
        //定义接口调用操作日志map对象
        Map<String, Object> logMap = new HashMap<String, Object>();
        //操作类型
        logMap.put("operatetype", operatetype);
        //平台类型
        logMap.put("platform_type", platform_type);
        //接口名称
        logMap.put("operatename", operatename);
        //接口说明
        logMap.put("remark", remark);
        //默认成功
        logMap.put("IsOk", "1");
        //默认reason要有这个key，一开始为空
        logMap.put("reason", null);
        return logMap;
    }

    /**
     * 设置操作日志map对象信息
     * @param paramMap
     * @param IsOk
     * @param reason
     * @author：Li.Deman
     * @date：2018/12/14
     */
    public void  setOperateLogInfo(Map<String, Object> paramMap,
                                   String IsOk,String reason){
        //失败
        paramMap.put("IsOk", IsOk);
        //失败原因
        paramMap.put("reason", reason);
    }

    /**
     * HTTP请求参数获取
     *
     * @param request
     * @return
     */
    public  Map<String, Object> getParameterMap(HttpServletRequest request) {

        Map<String, String[]> properties = request.getParameterMap();
        Map<String, Object> returnMap = new HashMap<String, Object>();
        try {
            Iterator<?> entries = properties.entrySet().iterator();
            Map.Entry entry;
            String name = "";
            String value = "";
            while (entries.hasNext()) {
                entry = (Map.Entry) entries.next();
                name = (String) entry.getKey();
                Object valueObj = entry.getValue();
                if (null == valueObj) {
                    value = "";
                } else if (valueObj instanceof String[]) {
                    String[] values = (String[]) valueObj;
                    for (int i = 0; i < values.length; i++) {
                        value = values[i] + ",";
                    }
                    value = replacer(value.substring(0, value.length() - 1));
                    //value = value.substring(0, value.length() - 1);
                } else if (isJson(valueObj.toString())) {
                    Map<String, Object> map = JSONObject.parseObject(URLDecoder.decode(valueObj.toString(), "utf-8"));
                    returnMap.putAll(map);
                } else {
                    value = replacer(valueObj.toString());
                }
                returnMap.put(name, value);

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return returnMap;
    }

    public String replacer(String inputStr){
        String data=inputStr;
        try {
            data = data.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
            //data = data.replaceAll("\\+", "%2B");
            data = URLDecoder.decode(data, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return data;
    }


    /**
     * 字符串转换函数
     *
     * @param param 参数
     * @return String
     */
    public static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }

    }

    public static Map<String,Object> MapKey2LowCase(Map<String,Object> map){
        Map<String,Object> result=new HashMap<>();
        for(String key:map.keySet()){
            result.put(key.toLowerCase(),map.get(key));
        }
        return result;
    }

    /**
     * @Description: 过滤字符串最后的一些字符
     * @Param: [input, trimstring] 输入字符串，需要过滤的字符串
     * @return: java.lang.String 返回字符串
     * @Author: Qin.Qing
     * @Date: 2018/11/20
     */
    public static String trimEnd(String input,String trimstring){
        String rightstring=input.substring(input.length()-trimstring.length(),input.length());
        if (trimstring.equals(rightstring))
        {
            return input.substring(0,input.length()-trimstring.length());
        }else{
            return input;
        }
    }

    /**
     *
     * @param content
     * @return
     */
    public static boolean isJson(String content) {
        try {
            JSONObject jsonStr = JSONObject.parseObject(content);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 将药德系统订单对象转换为PTS需要的map对象
     * @param baseOrderInfo
     * @return
     * @author：Li.Deman
     * @date：2018/11/30
     */
    public Map<String,Object> getOrderInfoToPTSByMap(BaseOrderInfo baseOrderInfo){
        Map<String, Object> resultMap = new HashMap<>();
        try{
            resultMap.put("client",baseOrderInfo.getClient());
            resultMap.put("stoCode",baseOrderInfo.getStoCode());
            resultMap.put("caution",baseOrderInfo.getCaution());
            resultMap.put("create_time",baseOrderInfo.getCreate_time());
            resultMap.put("customer_pay",baseOrderInfo.getCustomer_pay());
            resultMap.put("delivery_time",baseOrderInfo.getDelivery_time());
            resultMap.put("invoice_title",baseOrderInfo.getInvoice_title());
            resultMap.put("is_invoiced",baseOrderInfo.getIs_invoiced());
            resultMap.put("order_id",baseOrderInfo.getOrder_id());
            resultMap.put("order_info",baseOrderInfo.getOrder_info());
            resultMap.put("original_price",baseOrderInfo.getOriginal_price());
            resultMap.put("pay_type",baseOrderInfo.getPay_type());
            resultMap.put("platforms",baseOrderInfo.getPlatforms());
            resultMap.put("platforms_order_id",baseOrderInfo.getPlatforms_order_id());
            resultMap.put("recipient_address",baseOrderInfo.getRecipient_address());
            resultMap.put("recipient_name",baseOrderInfo.getRecipient_name());
            resultMap.put("recipient_phone",baseOrderInfo.getRecipient_phone());
            resultMap.put("shipping_fee",baseOrderInfo.getShipping_fee());
            resultMap.put("shop_no",baseOrderInfo.getShop_no());
            resultMap.put("status",baseOrderInfo.getStatus());
            resultMap.put("taxpayer_id",baseOrderInfo.getTaxpayer_id());
            resultMap.put("total_price",baseOrderInfo.getTotal_price());
            resultMap.put("update_time",baseOrderInfo.getUpdate_time());
            resultMap.put("day_seq",baseOrderInfo.getDay_seq());
            resultMap.put("notice_message",baseOrderInfo.getNotice_message());
            List<Map<String, Object>> detail = new ArrayList<>();
            Integer rowindex=1;
            for (BaseOrderProInfo item:baseOrderInfo.getItems()){
                Map<String, Object> itemMap = new HashMap<>();
                itemMap.put("client",baseOrderInfo.getClient());
                itemMap.put("stoCode",baseOrderInfo.getStoCode());
                itemMap.put("box_num",item.getBox_num());
                itemMap.put("box_price",item.getBox_price());
                itemMap.put("discount",item.getDiscount());
                itemMap.put("medicine_code",item.getMedicine_code());
                itemMap.put("quantity",item.getQuantity());
                itemMap.put("sequence",rowindex++);
                itemMap.put("unit",item.getUnit());
                itemMap.put("price",item.getPrice());
                detail.add(itemMap);
            }
            resultMap.put("detail",detail);
            resultMap.put("result","ok");
        }catch (Exception e){
            resultMap.put("result","ERROR" + e.getMessage());
        }
        return resultMap;
    }

    /**
     * 将药德系统订单对象转换为PTS需要的map对象
     * 支持部分退款和全额退款
     * @param baseOrderInfo
     * @return
     * @author：Li.Deman
     * @date：2018/12/27
     */
    public Map<String,Object> getOrderInfoToPTSByMapForRefund(BaseOrderInfo baseOrderInfo){
        Map<String, Object> resultMap = new HashMap<>();
        try{
            resultMap.put("order_id",baseOrderInfo.getOrder_id());
            resultMap.put("platforms_order_id",baseOrderInfo.getPlatforms_order_id());
            resultMap.put("shop_no",baseOrderInfo.getShop_no());
            resultMap.put("reason",baseOrderInfo.getReason());
            resultMap.put("refund_money",baseOrderInfo.getRefund_money());
            resultMap.put("action",baseOrderInfo.getAction());
            resultMap.put("status",baseOrderInfo.getStatus());
            resultMap.put("notice_message",baseOrderInfo.getNotice_message());
            List<Map<String, Object>> detail = new ArrayList<>();
            for (BaseOrderProInfo item:baseOrderInfo.getItems()){
                Map<String, Object> itemMap = new HashMap<>();
                itemMap.put("order_id",item.getOrder_id());
                itemMap.put("medicine_code",item.getMedicine_code());
                itemMap.put("refund_qty",item.getRefund_qty());
                itemMap.put("refund_price",item.getRefund_price());
                itemMap.put("sequence",item.getSequence());
                detail.add(itemMap);
            }
            resultMap.put("detail",detail);
            resultMap.put("result","ok");
        }catch (Exception e){
            resultMap.put("result","ERROR:" + e.getMessage());
        }
        return resultMap;
    }
}
