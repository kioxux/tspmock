//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.pay;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.common.exception.BusinessException;
import com.gys.common.http.HttpJson;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BsaoCPay {
    private static final Logger log = LoggerFactory.getLogger(BsaoCPay.class);
    private static BsaoCPay bsaoCPay = null;
    private static String APP_ID;
    private static String APP_KEY;
    private static String MERCHANT_CODE;
    private static String TERMINAL_CODE;
    private static String authorization;

    public BsaoCPay() {
    }

    public static BsaoCPay getInstance(String appId, String appKey, String merchantCode, String terminalCode) {
        if (!StrUtil.isBlank(appId) && !StrUtil.isBlank(appKey) && !StrUtil.isBlank(merchantCode) && !StrUtil.isBlank(terminalCode)) {
            if (bsaoCPay == null) {
                bsaoCPay = new BsaoCPay();
            }

            APP_ID = appId;
            APP_KEY = appKey;
            MERCHANT_CODE = merchantCode;
            TERMINAL_CODE = terminalCode;
            return bsaoCPay;
        } else {
            throw new BusinessException("请配置支付渠道参数");
        }
    }

    public HttpJson payV2(String url, String payCode, Long amount, String orderNo, String merchantRemark) {
        HttpJson httpJson = new HttpJson();
        JSONObject body = new JSONObject();
        body.put("merchantCode", MERCHANT_CODE);
        body.put("terminalCode", TERMINAL_CODE);
        body.put("transactionAmount", amount);
        body.put("transactionCurrencyCode", "156");
        body.put("merchantOrderId", orderNo);
        body.put("merchantRemark", merchantRemark);
        body.put("payMode", "CODE_SCAN");
        body.put("payCode", payCode);

        try {
            String send = send(url, body.toString());
            System.out.println("返回结果:\n" + send);
            if (StrUtil.isBlank(send)) {
                httpJson.setMsg("支付异常");
            } else {
                JSONObject jsonObject = JSON.parseObject(send);
                if ("00".equals(jsonObject.getString("errCode"))) {
                    httpJson.setSuccess(true);
                } else {
                    httpJson.setMsg(jsonObject.getString("errInfo"));
                }
            }
        } catch (Exception var10) {
            log.error("支付异常{}",var10.getMessage(), var10);
            httpJson.setMsg("支付异常");
        }

        return httpJson;
    }

    private static String send(String url, String entity) throws Exception {
        authorization = getOpenBodySig(APP_ID, APP_KEY, entity);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(entity, "UTF-8");
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity1 = response.getEntity();
        String resStr = null;
        if (entity1 != null) {
            resStr = EntityUtils.toString(entity1, "UTF-8");
        }

        httpClient.close();
        response.close();
        return resStr;
    }

    private static String getOpenBodySig(String appId, String appKey, String body) throws Exception {
        String timestamp = (new SimpleDateFormat("yyyyMMddHHmmss")).format(new Date());
        String nonce = UUID.randomUUID().toString().replace("-", "");
        byte[] data = body.getBytes("UTF-8");
        System.out.println("data:\n" + body);
        InputStream is = new ByteArrayInputStream(data);
        String bodyDigest = testSHA256(is);
        System.out.println("bodyDigest:\n" + bodyDigest);
        String str1_C = appId + timestamp + nonce + bodyDigest;
        System.out.println("str1_C:" + str1_C);
        byte[] localSignature = hmacSHA256(str1_C.getBytes(), appKey.getBytes());
        String localSignatureStr = Base64.encodeBase64String(localSignature);
        System.out.println("Authorization:\nOPEN-BODY-SIG AppId=\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"\n");
        return "OPEN-BODY-SIG AppId=\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"";
    }

    private static String testSHA256(InputStream is) {
        try {
            return DigestUtils.sha256Hex(is);
        } catch (IOException var2) {
            var2.printStackTrace();
            return null;
        }
    }

    private static byte[] hmacSHA256(byte[] data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data);
    }
}
