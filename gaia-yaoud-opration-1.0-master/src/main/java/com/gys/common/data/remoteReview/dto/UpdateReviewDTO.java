package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel("上传审方DTO")
public class UpdateReviewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("远程审方单号")
    private String gsrahVoucherId;

    // 患者信息
    @ApiModelProperty("顾客姓名")
    private String gsrahCustName;

    @ApiModelProperty("顾客性别")
    private String gsrahCustSex;

    @ApiModelProperty("顾客手机")
    private String gsrahCustMobile;

    @ApiModelProperty("顾客年龄")
    private String gsrahCustAge;

    @ApiModelProperty("顾客身份证")
    private String gsrahCustIdcard;

    // 登记信息
    @ApiModelProperty("上传日期")
    private String gsrahUploadDate;

    @ApiModelProperty("上传时间")
    private String gsrahUploadTime;

    @ApiModelProperty("上传人员")
    private String gsrahUploadEmp;

    @ApiModelProperty("上传状态")
    private String gsrahUploadFlag;

    @ApiModelProperty("审方结论")
    private String gsrahResult;

    // 商品信息
    @ApiModelProperty("商品信息列表")
    private ArrayList<UpadteDrugsDTO> drugsList;

    @ApiModelProperty("处方图片地址")
    private String gsrahRecipelPicAddress;

    public UpdateReviewDTO() {
    }
}