package com.gys.common.data.storeorder;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author wu mao yin
 * @Title: 末次供应商价格
 * @date 2021/12/316:33
 */
@Data
public class LastSupplierPrice implements Serializable {

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "末次供应商")
    private String lastSupplier;

    @ApiModelProperty("末次进价")
    private BigDecimal lastPurchasePrice;

}
