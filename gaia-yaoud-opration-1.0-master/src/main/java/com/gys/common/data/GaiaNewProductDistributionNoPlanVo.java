package com.gys.common.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 16:51 2021/8/5
 * @Description：系统级新品铺货计划统计报表-无计划明细统计
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
@CsvRow
public class GaiaNewProductDistributionNoPlanVo {

    @CsvCell(title = "新品入库时间",index = 1,fieldNo = 1)
    private String newProductEnterStockTime;  //新品入库时间

    @CsvCell(title = "新品铺货计划单号",index = 2,fieldNo = 1)
    private String newProductDH;

    @CsvCell(title = "是否有铺货计划",index = 3,fieldNo = 1)
    private String isPuhuoPlan;

    @CsvCell(title = "客户",index = 4,fieldNo = 1)
    private String cusCode;

    @CsvCell(title = "客户名称",index = 5,fieldNo = 1)
    private String cusName;

    @CsvCell(title = "商品编码",index = 6,fieldNo = 1)
    private String proCode;

    @CsvCell(title = "商品描述",index = 7,fieldNo = 1)
    private String proDesc;

    @CsvCell(title = "规格",index = 8,fieldNo = 1)
    private String proSpecs;

    @CsvCell(title = "生产厂家",index = 9,fieldNo = 1)
    private String proSCCJ;

    @CsvCell(title = "单位",index = 10,fieldNo = 1)
    private String proUnit;

    @CsvCell(title = "入库数量",index = 11,fieldNo = 1)
    private String enterStockQty;           //入库数量

    @CsvCell(title = "实际铺货数量",index = 12,fieldNo = 1)
    private String realPuhuoQty;            //实际铺货数量

    @CsvCell(title = "新品入库提示查看(是/否)",index = 13,fieldNo = 1)
    private String isNewProductAndPuhuoRead;    //是否新品入库铺货查看

    @CsvCell(title = "新品入库铺货(是/否)",index = 14,fieldNo = 1)
    private String isNewProductAndPuhuo;        //是不是新品入库并且铺货

    @CsvCell(title = "新品入库铺货时间",index = 15,fieldNo = 1)
    private String newProEnterStockAndPuhuoDate;    //新品入库铺货时间

    private String isWeekNewProductAndPuhuoRead;  //周新品入库提示查看

    private String isWeekNewProductAndPuhuo;       //周新品入库铺货

    private String weekNewProEnterStockAndPuhuoDate;  //周新品入库铺货时间
}
