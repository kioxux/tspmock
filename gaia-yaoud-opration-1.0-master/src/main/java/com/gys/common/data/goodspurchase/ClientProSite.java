package com.gys.common.data.goodspurchase;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/12/211:20
 */
@Data
public class ClientProSite implements Serializable {

    private String client;

    private String proSite;

}
