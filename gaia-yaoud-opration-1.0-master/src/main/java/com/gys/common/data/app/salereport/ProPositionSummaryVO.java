package com.gys.common.data.app.salereport;

import com.gys.business.service.data.AppStoreSaleOutData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author wu mao yin
 * @Title: 商品定位统计
 * @Package
 * @date 2021/12/19:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProPositionSummaryVO extends AppStoreSaleOutData {

    @ApiModelProperty(value = "毛利额")
    private BigDecimal profitAmt;

    @ApiModelProperty(value = "销售占比")
    private BigDecimal percentageOfSales;

    @ApiModelProperty(value = "商品定位", hidden = true)
    private String proPosition;

    @ApiModelProperty(value = "商品定位名称")
    private String proPositionName;

}
