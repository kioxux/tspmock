package com.gys.common.data.remoteReview.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("审核处方信息入参")
public class ApproveReviewDTO {

    @ApiModelProperty("远程审方单号")
    private String gsrahVoucherId;

    @ApiModelProperty("药师名称")
    private String gsrahPharmacistName;

    @ApiModelProperty("药师编号")
    private String gsrahPharmacistId;

    List<ApproveReviewDetailDTO> detailDTOList;
}
