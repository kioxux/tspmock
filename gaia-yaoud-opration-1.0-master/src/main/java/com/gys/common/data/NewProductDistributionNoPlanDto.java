package com.gys.common.data;

import lombok.Data;

import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:17 2021/8/5
 * @Description：系统级新品铺货计划统计报表，无计划明细统计
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class NewProductDistributionNoPlanDto {

    private List<String> prov;

    private List<String> city;

    private List<String> client;

    private List<String> productCodeList;

    private String newProEnterStockFlag;

    private String weekNewProEnterStockFlag;

    private String startDate;

    private String endDate;

    private Integer[] maxArr;

    //新品入库铺货开始时间 yyyyMMdd
    private String distributionStartDate;

    //新品入库铺货结束数据 yyyyMMdd
    private String distributionEndDate;
}
