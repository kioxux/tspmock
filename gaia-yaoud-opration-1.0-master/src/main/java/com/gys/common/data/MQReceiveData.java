package com.gys.common.data;

import lombok.Data;

@Data
public class MQReceiveData {
    private String type;
    private String cmd;
    private Object data;
}
