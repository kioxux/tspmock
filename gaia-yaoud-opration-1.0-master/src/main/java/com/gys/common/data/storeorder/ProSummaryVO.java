package com.gys.common.data.storeorder;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author wu mao yin
 * @Title: 商品汇总清单返回
 * @Package
 * @date 2021/12/315:50
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProSummaryVO extends StoreOrderProData {

    @ApiModelProperty("补货门店数")
    @ExcelProperty("补货门店数")
    private Integer replenishmentStoreNum;

    @ApiModelProperty("差异量")
    @ExcelProperty("差异量")
    private Integer diffNum;

    @ApiModelProperty("零售价")
    @ExcelProperty("零售价")
    private BigDecimal retailPrice;

    @ApiModelProperty("30天销量")
    @ExcelProperty("30天销量")
    private Integer salesNumWithThirtyDay;

}
