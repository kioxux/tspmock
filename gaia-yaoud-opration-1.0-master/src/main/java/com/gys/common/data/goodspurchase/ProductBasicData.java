package com.gys.common.data.goodspurchase;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductBasicData implements Serializable {

    private static final long serialVersionUID = -4300872673365866644L;

    private String proCode;

    private String proCommonName;

    private String proDepict;

    private String proPym;

    private String proName;

    private String proSpecs;

    private String proUnit;

    private String proForm;

    private String proPartform;

    private String proMinDose;

    private String proTotalDose;

    private String proBarcode;

    private String proBarcode2;

    private String proRegisterClass;

    private String proRegisterNo;

    private String proRegisterDate;

    private String proRegisterExDate;

    private String proClass;

    private String proClassName;

    private String proCompClass;

    private String proCompClassName;

    private String proPresClass;

    private String proFactoryCode;

    private String proFactoryName;

    private String proMark;

    private String proBrand;

    private String proBrandClass;

    private String proLife;

    private String proLifeUnit;

    private String proHolder;

    private String proInputTax;

    private String proOutputTax;

    private String proBasicCode;

    private String proTaxClass;

    private String proControlClass;

    private String proProduceClass;

    private String proStorageCondition;

    private String proStorageArea;

    private String proLong;

    private String proWide;

    private String proHigh;

    private String proMidPackage;

    private String proBigPackage;

    private String proElectronicCode;

    private String proQsCode;

    private String proMaxSales;

    private String proInstructionCode;

    private String proInstruction;

    private String proMedProDCT;

    private String proMedProDCTCode;

    private String proCountry;

    private String proPlace;

    private String proStatus;

    private String proTakeDays;

    private String proUsage;

    private String proContraindication;

    private String proMedListNum;

    private String proMedListName;

    private String proMedListForm;

    private String proNoticeThings;

    private String updateClient;

    private String updater;

    private String updateTime;

}
