package com.gys.common.data.yaolian;

import com.gys.common.data.yaolian.common.RequestHead;
import lombok.Data;

import java.util.List;

@Data
public class YlStockInputBean {
    private RequestHead requestHead;
    private String channel;//1:普通
    private List<YlStockData> stocks;
}
