package com.gys.common.data;

import lombok.Data;

import java.io.Serializable;

/**
 * GAIA_SD_INTEGRAL_EXCHANGE_RECORD
 * @author 
 */
@Data
public class GaiaSdIntegralExchangeRecord implements Serializable {
    private Long id;

    private String client;

    private String stoCode;

    /**
     * 会员卡号
     */
    private String cardNum;

    /**
     * 商品编码
     */
    private String proId;

    /**
     * 购买数量
     */
    private String num;

    /**
     * 购买日期
     */
    private String date;

    /**
     * 积分设置表字段
     */
    private String voucherId;

    /**
     * 销售单号
     */
    private String saleBillNo;

    private static final long serialVersionUID = 1L;
}