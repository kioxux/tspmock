package com.gys.common.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @Title: 基础下拉列表实体
 * @date 2021/12/315:57
 */
@Data
public class BaseListSelect implements Serializable {

    private String id;

    private String name;

}
