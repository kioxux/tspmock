package com.gys.common.data;

import lombok.Data;

import java.util.*;

@Data
public class ProThirdlyHeaderData {
//    private String proCode = "商品编码";
//    private String proCommonName = "通用名称";
//    private String name = "商品名称";
//    private String pym = "助记码";
//    private String specs = "规格";
//    private String unit = "计量单位";
//    private String form = "剂型";
//    private String proPlace = "产地";
//    private String proFactoryName = "生产企业";
//    private String kysl = "总部数量";
//    private String priceNormal = "零售价";
//    private String addAmt = "配送价";
//
//    private String unitPrice;
//    private String addRate;
//
//    public ProThirdlyHeaderData(String proCode, String proCommonName, String name, String pym, String specs, String unit, String form, String proPlace, String proFactoryName, String kysl, String priceNormal, String addAmt, String unitPrice, String addRate) {
//        this.proCode = proCode;
//        this.proCommonName = proCommonName;
//        this.name = name;
//        this.pym = pym;
//        this.specs = specs;
//        this.unit = unit;
//        this.form = form;
//        this.proPlace = proPlace;
//        this.proFactoryName = proFactoryName;
//        this.kysl = kysl;
//        this.priceNormal = priceNormal;
//        this.addAmt = addAmt;
//        this.unitPrice = unitPrice;
//        this.addRate = addRate;
//    }
//
//    public ProThirdlyHeaderData(String proCode, String proCommonName, String name, String pym, String specs, String unit, String form, String proPlace, String proFactoryName, String kysl, String priceNormal, String addAmt) {
//        this.proCode = proCode;
//        this.proCommonName = proCommonName;
//        this.name = name;
//        this.pym = pym;
//        this.specs = specs;
//        this.unit = unit;
//        this.form = form;
//        this.proPlace = proPlace;
//        this.proFactoryName = proFactoryName;
//        this.kysl = kysl;
//        this.priceNormal = priceNormal;
//        this.addAmt = addAmt;
//    }
    /**
     * 商品查询 3.0  表格表头 顺序不能随便动
     * 门店端使用
     */
    public final static LinkedHashMap<String, String> PRODUCT_HEADER_STORE = new LinkedHashMap<String, String>() {
        {
            //商品编码、通用名、商品名称、规格、单位、生产企业、零售价（取门店零售价）、配送价、仓库库存，助记码、剂型、产地
            //顺序不能随便动
            put("proCode", "商品编码");
            put("proCommonName", "通用名称");
            put("name", "商品名称");
            put("specs", "规格");
            put("unit", "计量单位");
            put("proFactoryName", "生产企业");
            put("priceNormal", "零售价");
            put("addAmt", "配送价");
            put("kysl", "仓库库存");
            put("pym", "助记码");
            put("form", "剂型");
            put("proPlace", "产地");
        }
    };

//    public final static List<Map<String,String>> PRODUCT_HEADER_STORE = new ArrayList(){
//        {
//             this.add(new HashMap<>);
//             this.add(Arrays.asList("proCommonName", "通用名称"));
//             this.add(Arrays.asList("name", "商品名称"));
//             this.add(Arrays.asList("pym", "助记码"));
//             this.add(Arrays.asList("specs", "规格"));
//             this.add(Arrays.asList("unit", "计量单位"));
//             this.add(Arrays.asList("form", "剂型"));
//             this.add(Arrays.asList("proPlace", "产地"));
//             this.add(Arrays.asList("proFactoryName", "生产企业"));
//             this.add(Arrays.asList("kysl", "总部数量"));
//             this.add(Arrays.asList("priceNormal", "零售价"));
//             this.add(Arrays.asList("addAmt", "配送价"));
//        }
//    };
    /**
     * 商品查询 3.0  表格表头 顺序不能随便动
     * 后台端使用
     */
    public final static LinkedHashMap<String, String> PRODUCT_HEADER_CLIENT = new LinkedHashMap<String, String>() {
        {
            // 商品编码、通用名、商品名称、规格、单位、生产企业、零售价（取总部零售价）、进货（成本价)、加点率、配送价、仓库库存，助记码、剂型、产地
            put("proCode", "商品编码");
            put("proCommonName", "通用名称");
            put("name", "商品名称");
            put("specs", "规格");
            put("unit", "计量单位");
            put("proFactoryName", "生产企业");
            put("priceNormal", "零售价");
            put("unitPrice", "成本价");
            put("addRate", "加点率");
            put("addAmt", "配送价");
            put("kysl", "仓库库存");
            put("pym", "助记码");
            put("form", "剂型");
            put("proPlace", "产地");
        }
    };
}
