package com.gys.common.data.yaolian;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2022/1/18 11:15
 */
@Data
public class YlCategoryInputBean {

    private String client;
    private List<String> categoryNameList;//类目集合
}
