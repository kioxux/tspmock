package com.gys.common.data.yaoka.preCalc;

import lombok.Data;

import java.util.List;
/**
 * @author 胡鑫鑫
 */
@Data
public class PaymentDecisionV2 {
    /**
     * 支付方案的id
     */
    private String planId;
    /**
     * 方案详情
     */
    private List<DecisionItemV2> items;
}
