package com.gys.common.data.suggestion;

import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/1 12:05
 */
@Data
public class SuggestionInVO {
    /**门店调剂建议单号*/
    private String billCode;
    /**门店调剂建议时间*/
    private String billDate;
    /**门店调剂建议失效时间*/
    private String invalidDate;
    /**
     * 调出门店code
     */
    private String outStoCode;
    /**
     * 调出门店名称
     */
    private String outStoName;
    /**门店调剂品项数量*/
    private Integer itemsQty;
    /**单据状态：0-待确认 1-已确认 2-失效*/
    private Integer status;
}
