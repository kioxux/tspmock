package com.gys.common.data.yaolian;

import lombok.Data;

import java.util.Date;

/**
 * 门店数据
 * | 名称 | 类型 | 示例值 | 描述| 是否必须 |
 * @author 胡鑫鑫
 */
@Data
public class Store {
    /**
     * | id | String| 123 | 门店ID | 必须 |
     */
    private String id;
    /**
     * | number | String | 123 | 门店编码 | 必须 |
     */
    private String number;

    /**
     * | name | String | 1号店 | 门店名称 | 必须 |
     */
    private String name;
    /**
     *| address | String | 北京朝阳区永安路123号 | 门店地址 | 必须 |
     */
    private String address;
    /**
     *| phone | String | 51033213 | 门店联系电话 | 非必须 |
     */
    private String phone;
    /**
     *| group_id | String | 123 | 所属价格组(默认为门店id) | 必须 |
     */
    private String group_id;
    /**
     *| status | String | 1 | 状态，新增为1，注销为0 | 必须 |
     */
    private String status;
    /**
     *| create_time | String | 2014-12-21 21:21:21 | 创建时间 | 必须 |
     */
    private String create_time;

    /**
     *| update_time | String | 2014-12-21 21:21:21 | 数据最新更新时间（判断是否需要同步更新）| 必须 |
     *
     */
    private String update_time;

    /**
     *| longitude | String | 121.44577 | 门店经度 | 非必须 |
     */
    private String longitude;

    /**
     * | latitude | String | 31.298982 | 门店纬度 | 非必须 |
     */
    private String latitude;

    /**
     *| province | String | 北京市 | 省 | 必须 |
     */
    private String province;

    /**
     *| city | String | 北京市 | 市 | 必须 |
     */
    private String city;

    /**
     * | area | String | 朝阳区 |  区| 必须 |
     */
    private String area;

    /**
     * | business_time | String | 07:00-20:00 | 营业时间（24小时制）| 必须 |
     */
    private String business_time;

}
