package com.gys.common.data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/28 16:05
 **/
@Data
public class CategoryModelForm {
    private String endDate;
    private String analyseDay;
    private String isProfit;
    private String dimension;
    private List<String> clientIds;
    private List<String> provinces;
    private List<String> citise;
    private List<String> storeIds;
    private String storeType;
    private Integer limitNum;

    public CategoryModelForm() {
        this.analyseDay = "30";
        this.citise = new ArrayList<>();
        this.provinces = new ArrayList<>();
        this.storeIds = new ArrayList<>();
        this.storeType = "";
    }
}
