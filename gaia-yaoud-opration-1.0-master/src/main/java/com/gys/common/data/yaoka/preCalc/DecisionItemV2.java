package com.gys.common.data.yaoka.preCalc;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 胡鑫鑫
 */
@Data
public class DecisionItemV2 {
    /**
     * 金额
     */
    private long amount;
    /**
     * 药诊卡卡号。当type=1或2时，才有值
     */
    private String cardNo;
    /**
     * id
     */
    private long  id;
    /**
     * token
     */
    private String token;
    /**
     * 状态
     * 不能作为药卡是否可用的判断标准，计算卡可用金额只需看amount。会出现status !=0，amount > 0的情况，属于可用一部分药卡金额
     * 0-正常
     * 1- 比例卡和非比例卡不可共用
     * 2- 比例卡和非比例卡不可共用,一笔订单只可使用一张比例卡
     * 3-限额
     * 4-卡号（XXXX）为限额卡，每XX（天/周/月/季度/年）只能使用XX元，当前额度已经用完
     * 5-额度已够，无需使用
     */
    private Integer status;
    /**
     * 类型 代码取值：1-现有药诊卡扣款；2-非固定面额卡（在线充值）；3-药店线下扣款
     */
    private Integer type;
    /**
     * 状态描述 汉字描述部分
     * 0-正常
     * 1- 比例卡和非比例卡不可共用
     * 2- 比例卡和非比例卡不可共用,一笔订单只可使用一张比例卡
     * 3-限额
     * 4-卡号（XXXX）为限额卡，每XX（天/周/月/季度/年）只能使用XX元，当前额度已经用完
     * 5-额度已够，无需使用
     */
    private String statusDesc;
}
