package com.gys.common.data.yaomeng;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * GAIA_PRESCRIBE_INFO
 * @author 
 */
@Data
public class GaiaPrescribeInfo {
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单
     */
    private String preBillNo;

    /**
     * 门店
     */
    private String preBrId;

    /**
     * 收银人员
     */
    private String preEmp;

    /**
     * 生成日期
     */
    private LocalDate preDate;

    /**
     * 生成时间
     */
    private LocalTime preTime;

    /**
     * 客户姓名
     */
    private String preCustName;

    /**
     * 性别
     */
    private String preCustSex;

    /**
     * 年龄
     */
    private String preCustAge;

    /**
     * 生日
     */
    private String preCustBirthday;

    /**
     * 医生编号
     */
    private Long preDoctorId;

    /**
     * 身份证号
     */
    private String preCustIdcard;

    /**
     * 手机号码
     */
    private String preCustMobile;

    /**
     * 病症名称
     */
    private String preIllName;

    /**
     * 病症ID
     */
    private Long preIllId;

    /**
     * 创建日期
     */
    private LocalDate preCreDate;

    /**
     * 创建时间
     */
    private LocalTime preCreTime;

    /**
     * 创建人账号
     */
    private String preCreId;

    /**
     * 修改日期
     */
    private LocalDate preModiDate;

    /**
     * 修改时间
     */
    private LocalTime preModiTime;

    /**
     * 修改人账号
     */
    private String preModiId;

}