package com.gys.common.data.remoteReview;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author chenxingyu
 * @description 远程审方主表
 * @date 2021-11-03
 */
@Data
@ApiModel("远程审方主表")
public class GaiaSdRemoteAuditH implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("远程审方单号")
    private String gsrahVoucherId;

    @ApiModelProperty("店号")
    private String gsrahBrId;

    @ApiModelProperty("店名")
    private String gsrahBrName;

    @ApiModelProperty("销售单号")
    private String gsrahBillNo;

    @ApiModelProperty("上传日期")
    private String gsrahUploadDate;

    @ApiModelProperty("上传时间")
    private String gsrahUploadTime;

    @ApiModelProperty("上传人员")
    private String gsrahUploadEmp;

    @ApiModelProperty("上传状态")
    private String gsrahUploadFlag;

    @ApiModelProperty("审方日期")
    private String gsrahDate;

    @ApiModelProperty("审方时间")
    private String gsrahTime;

    @ApiModelProperty("审方结论")
    private String gsrahResult;

    @ApiModelProperty("药师编号")
    private String gsrahPharmacistId;

    @ApiModelProperty("药师名称")
    private String gsrahPharmacistName;

    @ApiModelProperty("顾客姓名")
    private String gsrahCustName;

    @ApiModelProperty("顾客性别")
    private String gsrahCustSex;

    @ApiModelProperty("顾客年龄")
    private String gsrahCustAge;

    @ApiModelProperty("顾客身份证")
    private String gsrahCustIdcard;

    @ApiModelProperty("顾客手机")
    private String gsrahCustMobile;

    @ApiModelProperty("状态")
    private String gsrahStatus;

    @ApiModelProperty("处方图片地址")
    private String gsrahRecipelPicAddress;

    public GaiaSdRemoteAuditH() {
    }
}