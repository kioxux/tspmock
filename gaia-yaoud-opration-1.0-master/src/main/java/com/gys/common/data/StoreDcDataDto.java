package com.gys.common.data;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/12/23 14:29
 */
@Data
public class StoreDcDataDto {

    private String dcCode;//配送中心
    private String dcWtdc;//委托配送中心
}
