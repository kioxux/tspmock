package com.gys.common.data.wechatTemplate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 查询微信模板返回对象
 * @author cxy
 * @date 2022-01-06
 */
@Data
@ApiModel("查询微信模板返回对象")
public class GaiaMouldMaintainDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("微信模板id")
    private String mouldId;

    @ApiModelProperty("模板编码")
    private String mouldCode;

    @ApiModelProperty("模板名称")
    private String mouldName;

    @ApiModelProperty("模板标题")
    private String mouldTitle;

    @ApiModelProperty("标题内容")
    private String titleContent;

    @ApiModelProperty("模板结尾")
    private String mouldEnding;

    @ApiModelProperty("结尾内容")
    private String endingContent;

    @ApiModelProperty("状态（0：停用，1：启用）")
    private String status;

    @ApiModelProperty("示例内容")
    private String example;

    @ApiModelProperty("详细内容")
    private ArrayList<GaiaMouldMaintainD> gaiaMouldMaintainDS;

}
