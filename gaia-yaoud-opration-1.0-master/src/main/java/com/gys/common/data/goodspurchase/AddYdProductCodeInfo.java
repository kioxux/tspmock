package com.gys.common.data.goodspurchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @Title: 新增药德商品编码信息
 * @Package
 * @date 2021/10/2811:02
 */
@Data
public class AddYdProductCodeInfo implements Serializable {

    @ApiModelProperty("客户")
    private String client;

    @ApiModelProperty("地点")
    private String proSite;

    @ApiModelProperty("客户商品编码")
    private String proCode;

    @ApiModelProperty("药德商品编码")
    private String ydProCode;

    @ApiModelProperty("药德商品信息")
    private ProductBasicData gaiaProductBasic;

}
