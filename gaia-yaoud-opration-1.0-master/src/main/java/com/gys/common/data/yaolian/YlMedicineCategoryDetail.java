package com.gys.common.data.yaolian;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/8 13:53
 */
@Data
public class YlMedicineCategoryDetail {

    //                           | 示例值 | 描述| 是否必须 |
    private String category_id;//| 1 | 类目ID | 是 |
    private String parent_id;  //| 0 | 上级ID | 是 |
    private String category_name;//| 处方药 | 分类名称 | 是 |
    private String category_alias;//| RX | 分类别名 | 非必须 |
    private String levels;//| 1 | 类目层级 | 非必须 |
    private String status;//| 1 | 是否启用 0不启用 1启用 | 是 |
    private String sort;//| 1 | 分类顺序 | 是 |
}
