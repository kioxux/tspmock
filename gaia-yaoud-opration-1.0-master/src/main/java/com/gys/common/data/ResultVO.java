package com.gys.common.data;

import lombok.Data;

@Data
public class ResultVO<T> {
    private Boolean success;
    private String msg;
    private T obj;
    public boolean isSuccess() {
        return this.success;
    }
}