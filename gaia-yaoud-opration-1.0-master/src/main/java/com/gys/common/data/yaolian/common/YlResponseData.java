package com.gys.common.data.yaolian.common;

import lombok.Data;

/**
 * 返回参数
 * @author 胡鑫鑫
 */
@Data
public class YlResponseData {
    /**
     错误代码 0 成功， 1 失败（失败只会传errno,error）
     */
    private String errno;
    /**
     * 失败原因
     */
    private String error;
    /**
     * 数据类型 OBJECT
     */
    private String dataType;
    /**\
     * 数据
     */
    private Object data;
}
