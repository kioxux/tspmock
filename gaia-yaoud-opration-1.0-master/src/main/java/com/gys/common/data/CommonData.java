package com.gys.common.data;

import lombok.Data;

@Data
public class CommonData {
    private String content;
}
