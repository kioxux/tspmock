package com.gys.common.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 明细清单表(GaiaBillOfAr)实体类
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:11
 */
@Data
@CsvRow("批发应收管理")
public class GaiaBillOfArOutData implements Serializable {
    private static final long serialVersionUID = -38101044084295338L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 回款单号
     */
    private String billCode;
    /**
     * 仓库编码
     */
    @CsvCell(title = "仓库编码", index = 1, fieldNo = 1)
    private String dcCode;
    @CsvCell(title = "仓库名称", index = 2, fieldNo = 1)
    private String dcName;
    /**
     * 应收方式编码
     */
    @CsvCell(title = "应收方式编码", index = 5, fieldNo = 1)
    private String arMethodId;

    @CsvCell(title = "应收方式", index = 6, fieldNo = 1)
    private String arMethodName;
    /**
     * 发生金额
     */
    @CsvCell(title = "结算金额", index = 8, fieldNo = 1)
    private BigDecimal arAmt;
    /**
     * 核销金额
     */
    private BigDecimal hxAmt;
    /**
     * 备注
     */
    @CsvCell(title = "备注", index = 9, fieldNo = 1)
    private String remark;
    /**
     * 发生日期
     */
    @CsvCell(title = "发生日期", index = 7, fieldNo = 1)
    private String billDateStr;

    private Date billDate;
    /**
     * 客户自编码
     */
    @CsvCell(title = "客户编码", index = 3, fieldNo = 1)
    private String cusSelfCode;
    /**
     * 客户名称
     */
    @CsvCell(title = "客户名称", index = 4, fieldNo = 1)
    private String cusName;
    /**
     * 核销状态 0-未核销 1-核销中 2-已核销
     */
    private Integer status;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    @ApiModelProperty(value = "开单员编码")
    private String soDnByCode;

    @ApiModelProperty(value = "开单员姓名")
    private String soDnByName;

    @ApiModelProperty(value = "销售员编码")
    private String gssCode;

    @ApiModelProperty(value = "销售员")
    private String gssName;



    @CsvCell(title = "销售员", index = 10, fieldNo = 1)
    @ApiModelProperty(value = "销售员编码-销售员")
    private String gssNameStr;



}
