//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.data;

import java.io.Serializable;

public class MeituanResult implements Serializable {
    private Object data;

    public MeituanResult(Object data) {
        this.data = data;
    }

    public static MeituanResult success(Object data) {
        return new MeituanResult(data);
    }

    public MeituanResult() {
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
