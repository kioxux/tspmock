package com.gys.common.data.yaomeng;

import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/11/12 13:50
 */
@Data
public class RecipelQuestionInfoBean {

    /**
     * 销售单号
     */
    private String preBillNo;
    /**
     * 收银人员
     */
    private String preEmp;
    /**
     * 客户姓名
     */
    private String preCustName;

    /**
     * 性别
     */
    private String preCustSex;

    /**
     * 年龄
     */
    private String preCustAge;

    /**
     * 身份证号
     */
    private String preCustIdcard;

    /**
     * 手机号码
     */
    private String preCustMobile;
    /**
     * 生日
     */
    private String preCustBirthday;

    /**
     * 医生编号
     */
    private Long preDoctorId;

    /**
     * 病症集合
     */
    private List<YaomengDiseaseBean> diseaseList;

    /**
     * 问答集合
     */
    private List<YaomengQuestionBean> questionList;
}
