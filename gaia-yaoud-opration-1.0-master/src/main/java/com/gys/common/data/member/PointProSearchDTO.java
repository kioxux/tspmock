package com.gys.common.data.member;

import com.gys.business.service.data.FuzzyQueryOfProductGroupInData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wu mao yin
 * @Title: 积分商品搜索
 * @Package
 * @date 2021/12/1017:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PointProSearchDTO extends FuzzyQueryOfProductGroupInData {

    private String voucherId;

    @ApiModelProperty("null: ")
    private Integer flag;

}
