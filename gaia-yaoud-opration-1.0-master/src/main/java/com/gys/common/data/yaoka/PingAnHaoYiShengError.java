package com.gys.common.data.yaoka;

/**
 * 平安好医生 药卡异常信息
 * @author 胡鑫鑫
 */
public enum PingAnHaoYiShengError {
    /**
     *公共错误码
     */
    ILLEGAL_ARGUMENT("ILLEGAL_ARGUMENT","丢失参数"),
    SERVICE_IS_NOT_EXIST("SERVICE_IS_NOT_EXIST","服务不存在"),
    SERVICE_ACCESS_DENIED("SERVICE_ACCESS_DENIED","服务访问被拒绝"),
    SERVICE_IS_DISABLED("SERVICE_IS_DISABLED","服务不是公开的"),
    SYSTEM_ERROR("SYSTEM_ERROR","系统异常"),
    INVALID_PARAMS("INVALID_PARAMS","参数未传值"),
    PARAM_TYPE_CONVERT_ERROR("PARAM_TYPE_CONVERT_ERROR","参数转换类型异常"),
    SERVICE_UNABLE("SERVICE_UNABLE","服务访问失败"),
    OVERFLOW_PARTNER("OVERFLOW_PARTNER","合作伙伴访问次数过多"),
    OVERFLOW_IP("OVERFLOW_IP","访问次数过多"),
    FAILED_TO_FIND_PARTNER("FAILED_TO_FIND_PARTNER","找不到合作伙伴"),
    PARTNER_ACCESS_DENIED("PARTNER_ACCESS_DENIED","合作伙伴无权限调用API"),
    PARTNER_ACCESS_NEED_ENCRYPTED_OR_ACCESS_DENIED("PARTNER_ACCESS_NEED_ENCRYPTED_OR_ACCESS_DENIED","访问未加密"),
    PARTNER_ACCESS_DENIED_IP("PARTNER_ACCESS_DENIED_IP","IP未加入白名单"),
    FAILED_TO_DECODE_UNENCRPTED_DATA("FAILED_TO_DECODE_UNENCRPTED_DATA","解码失败: 数据不是可解码的格式"),
    FAILED_TO_DECODE("FAILED_TO_DECODE","解码失败"),
    FAILED_TO_DECODE_MISSING_PARAM("","缺少参数"),
    FAILED_TO_DECODE_MISSING_VALUE("","缺少值"),
    API_ACCESS_CONCURRENT_OVERFLOW("","并发访问量过高"),
    API_ACCESS_PERIOD_OVERFLOW("","定期访问量过大"),
    ERROE_10("10","参数错误"),
    ERROR_10001("10001","请求频率过高"),
    ERROR_10002("10002","余额不足"),
    ERROR_10003("10003","门店未开通支付功能"),
    ERROR_10004("10004","付款码已过期"),
    ERROR_10006("10006","一笔交易不允许跨多个用户的账户"),
    ERROR_10008("10008","账户已冻结，卡号{0}"),
    ERROR_10009("10009","卡已过期，请使用其他卡"),
    ERROR_10010("10010","不支持此来源:{0}"),
    ERROR_10011("10011","卡未起效，请使用其他卡"),
    ERROR_10013("10013","付款码错误！"),
    ERROR_10019("10019","该药诊权益不支持在该门店使用，请使用其他药诊权益"),
    ERROR_10023("10023","比例卡只能单独使用"),
    ERROR_10024("10024","超过扣款比例上限({0}%)"),
    ERROR_10025("10025","付款码已使用，请勿重复使用"),
    ERROR_10049("10049","请勿短时间内重复扫码");



    //        Error_10("10","参数错误");
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    PingAnHaoYiShengError(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getName(String code) {
        for (PingAnHaoYiShengError form : PingAnHaoYiShengError.values()) {
            if (!form.getCode().equals(code)) {
                continue;
            }
            return form.name;
        }
        return null;
    }
}

