package com.gys.common.data.yaomeng;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * 处方问诊信息详情
 * @author 
 */
@Data
public class GaiaPrescribeDetail {
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单
     */
    private String preBillNo;

    /**
     * 门店
     */
    private String preBrId;

    /**
     * 收银人员
     */
    private String preEmp;

    /**
     * 生成日期
     */
    private LocalDate preDate;

    /**
     * 生成时间
     */
    private LocalTime preTime;

    /**
     * 问题
     */
    private String preQuestion;

    /**
     * 答案
     */
    private String preAnswer;

    /**
     * 创建日期
     */
    private LocalDate preCreDate;

    /**
     * 创建时间
     */
    private LocalTime preCreTime;

    /**
     * 创建人账号
     */
    private String preCreId;

    /**
     * 修改日期
     */
    private LocalDate preModiDate;

    /**
     * 修改时间
     */
    private LocalTime preModiTime;

    /**
     * 修改人账号
     */
    private String preModiId;

}