package com.gys.common.data.storeorder;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author wu mao yin
 * @Title: 门店请货商品数据
 * @date 2021/12/315:29
 */
@Data
public class StoreOrderProData implements Serializable {

    @ApiModelProperty("商品编码")
    @ExcelProperty("商品编码")
    private String proCode;

    @ApiModelProperty("商品名称")
    @ExcelProperty("商品名称")
    private String proName;

    @ApiModelProperty("商品描述")
    @ExcelProperty("商品描述")
    private String proDepict;

    @ApiModelProperty("商品规格")
    @ExcelProperty("商品规格")
    private String proSpecs;

    @ApiModelProperty("商品生产厂家")
    @ExcelProperty("商品生产厂家")
    private String proFactoryName;

    @ApiModelProperty("商品单位")
    @ExcelProperty("商品单位")
    private String proUnit;

    @ApiModelProperty("需求量")
    @ExcelProperty("需求量")
    private Integer demandNum;

    @ApiModelProperty("开单量")
    @ExcelProperty("开单量")
    private Integer billingNum;

    @ApiModelProperty("门店库存")
    @ExcelProperty("门店库存")
    private Integer storeInventory;

    @ApiModelProperty("零售价")
    @ExcelProperty("零售价")
    private BigDecimal retailPrice;

    @ApiModelProperty("末次供应商")
    @ExcelProperty("末次供应商")
    private String lastSupplier;

    @ApiModelProperty("末次进价")
    @ExcelProperty("末次进价")
    private BigDecimal lastPurchasePrice;

}
