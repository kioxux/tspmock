package com.gys.common.data.suggestion;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/29 14:47
 */
@Data
public class SuggestionDetailVO {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 调出门店编码
     */
    private String outStoCode;
    /**
     * 调出门店名称
     */
    private String outStoName;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 商品规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 商品单位
     */
    private String proUnit;
    /**
     * 月均销量
     */
    private BigDecimal averageSalesQty;
    /**
     * 库存数量
     */
    private BigDecimal inventoryQty;
    /**
     * 建议调出量
     */
    private BigDecimal suggestionOutQty;
    /**
     * 确认调出量
     */
    private BigDecimal confirmQty;
    /**
     * 最终确认量
     */
    private BigDecimal finalQty;
    /**
     * 商品批号
     */
    private String batchNo;
    /**
     * 商品效期
     */
    private String validDay;
    /**
     * 商品成分分类名称
     */
    private String proCompClass;
    /**
     * 商品分类名称
     */
    private String proClass;
    private String proBigClassCode;
    private String proBigClassName;
    private String proClassName;
    /**
     * 更新时间
     */
    private String updateTime;
}
