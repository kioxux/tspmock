package com.gys.common.data.suggestion;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/2 10:04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class InDetailSaveForm extends SuggestionForm {
    /**
     * 调入商品列表
     */
    private List<InProductForm> productList;

    @Data
    public static class InProductForm {
        /**
         * 商品编码
         */
        @NotNull(message = "商品编码不能为空")
        private String proSelfCode;
        /**
         * 商品批号
         */
        @NotNull(message = "商品批号不能为空")
        private String batchNo;
        /**
         * 确认调入量
         */
        @NotNull(message = "确认调入量不能为空")
        private BigDecimal confirmQty;
    }
}
