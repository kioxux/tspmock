package com.gys.common.data.yaoka;

import lombok.Data;

@Data
public class YaokaResponse {
    private Boolean status;
    private String obj;
    private String msg;
}
