package com.gys.common.data.wechatTemplate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * @description  微信模板维护明细表
 * @author cxy
 * @date 2022-01-06
 */
@Data
@ApiModel(" 微信模板维护明细表")
public class GaiaMouldMaintainD implements Serializable, Comparable<GaiaMouldMaintainD>  {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("模板维护主表id")
    private String mouldCode;

    @ApiModelProperty("行号")
    private Integer lineNo;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("是否删除（0：未删除，1：已删除）")
    private String delFlag;

    public GaiaMouldMaintainD() {}

    @Override
    public int compareTo(@NotNull GaiaMouldMaintainD compareNaintainD) {
        int compareNo=((GaiaMouldMaintainD)compareNaintainD).getLineNo();
        /* 正序排列 */
        return this.lineNo-compareNo;
    }

}