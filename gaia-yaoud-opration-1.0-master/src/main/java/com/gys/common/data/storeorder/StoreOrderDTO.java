package com.gys.common.data.storeorder;

import com.gys.common.data.PageDomain;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wu mao yin
 * @Title: 门店请货数据查询入参
 * @date 2021/12/315:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StoreOrderDTO extends PageDomain {

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("连锁公司")
    private String chainCompId;

    @ApiModelProperty("门店")
    private List<String> stoCodes;

    @ApiModelProperty("请货单号")
    private String orderNos;

    @ApiModelProperty("需求类型")
    private String demandType;

    @ApiModelProperty("商品编号")
    private String proCodes;

    @ApiModelProperty("开始时间")
    private String startDate;

    @ApiModelProperty("结束时间")
    private String endDate;

    @ApiModelProperty("开单状态")
    private String billStatus;

    @ApiModelProperty(value = "请货单号集合", hidden = true)
    private List<String> orderNoList;

    @ApiModelProperty(value = "商品编号集合", hidden = true)
    private List<String> proCodeList;

}
