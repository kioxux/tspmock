package com.gys.common.data.yaolian;

import lombok.Data;

@Data
public class YlStockData {
    /**
     *  门店id
     */
    private String store_id;
    /**
     * 药品id
     */
    private String drug_id;
    /**
     * 药品库存（商品上下架时，非必须）
     */
    private String quantity;
    /**
     * 数据创建时间
     */
    private String create_time;
    /**
     * 数据最新更新时间（判断是否需要同步更新）
     */
    private String update_time;
}
