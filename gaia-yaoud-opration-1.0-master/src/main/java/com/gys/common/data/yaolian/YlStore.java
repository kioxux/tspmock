package com.gys.common.data.yaolian;

import com.gys.common.data.yaolian.common.YlRequestData;
import lombok.Data;

import java.util.List;
@Data
public class YlStore extends YlRequestData {
//    private RequestHead requestHead;
//    private String channel;
    private List<Store> stores;
}
