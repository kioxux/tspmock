package com.gys.common.data.app.salereport;

import com.gys.business.service.data.SelectData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/11/3018:36
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProBigClass extends SelectData {

    private List<String> proBigClasses;

    public ProBigClass(String id, String name, List<String> proBigClasses) {
        super(id, name);
        this.proBigClasses = proBigClasses;

    }

}
