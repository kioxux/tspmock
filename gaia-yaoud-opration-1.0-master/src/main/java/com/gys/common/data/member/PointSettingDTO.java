package com.gys.common.data.member;

import com.gys.common.data.PageDomain;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 积分规则设置
 * @date 2021/12/815:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PointSettingDTO extends PageDomain {

    @ApiModelProperty("规则代号")
    private Long id;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("用户")
    private String userId;

    @ApiModelProperty("规则代号")
    private String gsicsPlanId;

    @ApiModelProperty("规则描述")
    private String gsicsPlanName;

    @ApiModelProperty("抵现类型 0-单次固定，1-最优")
    private Integer gsicsType;

    @ApiModelProperty("抵用上限 0-无，1-有")
    private Integer gsicsAmtLimitFlag;

    @ApiModelProperty("抵用上限-比例")
    private BigDecimal gsicsLimitRate;

    @ApiModelProperty("抵用上限-金额")
    private BigDecimal gsicsAmtOffsetMax;

    @ApiModelProperty("是否积分 0-否，1-是")
    private Integer gsicsIntegralAddSet;

    @ApiModelProperty("会员卡类型, 多个逗号分割")
    private String gsicsMemberCardType;

    @ApiModelProperty("规则内容代号")
    private String gsicsCode;

    @ApiModelProperty("执行内容")
    private List<ExecuteContent> executeContent;

}
