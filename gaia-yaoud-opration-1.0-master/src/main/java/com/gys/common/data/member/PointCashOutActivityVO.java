package com.gys.common.data.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.List;

/**
 * 积分抵现活动
 *
 * @author wu mao yin
 * @date 2021/12/9 15:15
 */
@Data
public class PointCashOutActivityVO implements Serializable {

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("积分抵现活动单号")
    private String gsicVoucherId;

    @ApiModelProperty("积分抵现活动描述")
    private String gsicName;

    @ApiModelProperty("积分抵现规则代号")
    private String gsicPlanId;

    @ApiModelProperty("开始日期")
    private String gsicStartDate;

    @ApiModelProperty("结束日期")
    private String gsicEndDate;

    @ApiModelProperty("频率类型 1:日期，2:星期")
    private Integer gsicFrequencyType;

    @ApiModelProperty("日期频率 多个逗号分割")
    private String gsicDateFrequency;

    @ApiModelProperty("星期频率 多个逗号分割")
    private String gsicWeekFrequency;

    @ApiModelProperty("开始时间")
    private String gsicStartTime;

    @ApiModelProperty("结束时间")
    private String gsicEndTime;

    @ApiModelProperty("时段 格式 ['000000~011111', '020000~031111']")
    private List<String> timeIntervals;

    @ApiModelProperty("抵用商品范围 0-所有，1-部分")
    private Integer gsicProRange;

    @ApiModelProperty("单据状态 0-保存，1-审核")
    private Integer gsicStatus;

    private static final long serialVersionUID = 1L;

}