package com.gys.common.data.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 积分规则设置列表
 * @date 2021/12/815:33
 */
@Data
public class PointSettingVO implements Serializable {

    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("规则代号")
    private String gsicsPlanId;

    @ApiModelProperty("规则描述")
    private String gsicsPlanName;

    @ApiModelProperty("抵现类型 0-单次固定，1-最优")
    private Integer gsicsType;

    @ApiModelProperty("抵用上限 0-无，1-有")
    private Integer gsicsAmtLimitFlag;

    @ApiModelProperty("抵用上限-比例")
    private BigDecimal gsicsLimitRate;

    @ApiModelProperty("抵用上限-金额")
    private BigDecimal gsicsAmtOffsetMax;

    @ApiModelProperty("是否积分 0-否，1-是")
    private Integer gsicsIntegralAddSet;

    @ApiModelProperty("所有会员 true:是，false否")
    private Boolean allMemberCardType;

    @ApiModelProperty("会员卡类型")
    private String gsicsMemberCardType;

    @ApiModelProperty("会员卡类型名称")
    private String gsicsMemberCardTypeName;

    @ApiModelProperty("规则内容代号")
    private String gsicsCode;

    @ApiModelProperty("执行内容")
    private List<ExecuteContent> executeContent;

}
