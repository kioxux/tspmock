package com.gys.common.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 9:15 2021/8/10
 * @Description：无计划新品查询商品编码入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class NewProductDistributionNoPlanProductCodeDto {
    @NotEmpty(message = "客户必选")
    private List<String> clientCodeList;

    private String content;
    private List<String> queryStringList;
    @NotNull(message = "分页参数必填")
    private Integer pageSize;
    @NotNull(message = "分页参数必填")
    private Integer pageNum;
}
