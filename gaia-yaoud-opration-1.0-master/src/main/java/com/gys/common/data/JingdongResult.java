//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.data;

import java.io.Serializable;

public class JingdongResult implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final String ERROR_MESSAGE = "服务器出现了一些小状况，正在修复中！";
    private String code;
    private Object data;
    private String msg;

    public JingdongResult(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static JingdongResult success(Object data, String desc) {
        return new JingdongResult("0", desc, data);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
