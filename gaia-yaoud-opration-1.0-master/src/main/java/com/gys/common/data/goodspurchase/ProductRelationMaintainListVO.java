package com.gys.common.data.goodspurchase;

import com.alibaba.excel.annotation.ExcelProperty;
import com.gys.common.annotation.ParseEnumVal;
import com.gys.common.data.PageDomain;
import com.gys.common.enums.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @Title: 客户商品关系维护列表
 * @Package
 * @date 2021/10/289:11
 */
@Data
public class ProductRelationMaintainListVO implements Serializable {

    @ApiModelProperty("客户")
    private String client;

    @ApiModelProperty("客户地点")
    private String proSite;

    @ApiModelProperty("门店")
    private String stoCode;

    @ApiModelProperty("客户商品自编码")
    private String proSelfCode;

    @ApiModelProperty("客户商品描述")
    private String proDepict;

    @ApiModelProperty("客户商品规格")
    private String proSpecs;

    @ApiModelProperty("客户商品国际条形码1")
    private String proBarCode;

    @ApiModelProperty("客户商品生产企业")
    private String proFactoryName;

    @ApiModelProperty("客户商品计量单位")
    private String proUnit;

    @ApiModelProperty("客户商品批准文号")
    private String proRegisterNo;

    @ApiModelProperty("药德商品编码")
    private String ydProCode;

    @ApiModelProperty("客户通用名称")
    private String proCommonName;

    @ApiModelProperty("药德通用名称")
    private String ydProCommonName;

    @ApiModelProperty("药德商品描述")
    private String ydProDepict;

    @ApiModelProperty("药德助记码")
    private String ydProPym;

    @ApiModelProperty("药德商品名")
    private String ydProName;

    @ApiModelProperty("药德规格")
    private String ydProSpecs;

    @ApiModelProperty("药德计量单位")
    private String ydProUnit;

    @ApiModelProperty("药德剂型")
    private String ydProForm;

    @ApiModelProperty("药德细分剂型")
    private String ydProPartForm;

    @ApiModelProperty("药德最小剂量（以mg/ml计算）")
    private String ydProMinDose;

    @ApiModelProperty("药德总剂量（以mg/ml计算）")
    private String ydProTotalDose;

    @ApiModelProperty("药德国际条形码1")
    private String ydProBarCode;

    @ApiModelProperty("药德国际条形码2")
    private String ydProBarCode2;

    @ParseEnumVal(enumVal = ProRegisterClassEnum.class)
    @ApiModelProperty("药德批准文号分类")
    private String ydProRegisterClass;

    @ApiModelProperty("药德批准文号")
    private String ydProRegisterNo;

    @ApiModelProperty("药德批准文号批准日期")
    private String ydProRegisterDate;

    @ApiModelProperty("药德批准文号失效日期")
    private String ydProRegisterExDate;

    @ApiModelProperty("药德商品分类")
    private String ydProClass;

    @ApiModelProperty("药德商品分类描述")
    private String ydProClassName;

    @ApiModelProperty("药德成分分类")
    private String ydProCompClass;

    @ApiModelProperty("药德成分分类描述")
    private String ydProCompClassName;

    @ParseEnumVal(enumVal = ProPreClassEnum.class)
    @ApiModelProperty("药德处方类别")
    private String ydProPresClass;

    @ApiModelProperty("药德生产企业代码")
    private String ydProFactoryCode;

    @ApiModelProperty("药德生产企业")
    private String ydProFactoryName;

    @ApiModelProperty("药德商标")
    private String ydProMark;

    @ApiModelProperty("药德品牌标识名")
    private String ydProBrand;

    @ParseEnumVal(enumVal = ProBrandClassEnum.class)
    @ApiModelProperty("药德品牌区分")
    private String ydProBrandClass;

    @ApiModelProperty("药德保质期")
    private String ydProLife;

    @ParseEnumVal(enumVal = ProLifeUnitEnum.class)
    @ApiModelProperty("药德保质期单位")
    private String ydProLifeUnit;

    @ApiModelProperty("药德上市许可持有人")
    private String ydProHolder;

    @ApiModelProperty("药德进项税率")
    private String ydProInputTax;

    @ApiModelProperty("药德销项税率")
    private String ydProOutputTax;

    @ApiModelProperty("药德药品本位码")
    private String ydProBasicCode;

    @ApiModelProperty("药德税务分类编码")
    private String ydProTaxClass;

    @ParseEnumVal(enumVal = ProControlClassEnum.class)
    @ApiModelProperty("药德管制特殊分类")
    private String ydProControlClass;

    @ParseEnumVal(enumVal = ProProduceClassEnum.class)
    @ApiModelProperty("药德生产类别")
    private String ydProProduceClass;

    @ParseEnumVal(enumVal = ProStorageConditionEnum.class)
    @ApiModelProperty("药德贮存条件")
    private String ydProStorageCondition;

    @ParseEnumVal(enumVal = ProStorageAreaEnum.class)
    @ApiModelProperty("药德商品仓储分区")
    private String ydProStorageArea;

    @ApiModelProperty("药德长（以MM计算）")
    private String ydProLong;

    @ApiModelProperty("药德宽（以MM计算）")
    private String ydProWide;

    @ApiModelProperty("药德高（以MM计算）")
    private String ydProHigh;

    @ApiModelProperty("药德中包装量")
    private String ydProMidPackage;

    @ApiModelProperty("药德大包装量")
    private String ydProBigPackage;

    @ApiModelProperty("药德启用电子监管码")
    private String ydProElectronicCode;

    @ApiModelProperty("药德生产经营许可证号")
    private String ydProQsCode;

    @ApiModelProperty("药德最大销售量")
    private String ydProMaxSales;

    @ApiModelProperty("药德说明书代码")
    private String ydProInstructionCode;

    @ApiModelProperty("药德说明书内容")
    private String ydProInstruction;

    @ApiModelProperty("药德国家医保品种")
    private String ydProMedProdct;

    @ApiModelProperty("药德国家医保品种编码")
    private String ydProMedProdctCode;

    @ApiModelProperty("药德生产国家")
    private String ydProCountry;

    @ApiModelProperty("药德产地")
    private String ydProPlace;

    @ApiModelProperty("药德状态")
    private String ydProStatus;

    @ApiModelProperty("药德可服用天数")
    private String ydProTakeDays;

    @ApiModelProperty("药德用法用量")
    private String ydProUsage;

    @ApiModelProperty("药德禁忌说明")
    private String ydProContraindication;

    @ApiModelProperty("药德国家医保目录编号")
    private String ydProMedListNum;

    @ApiModelProperty("药德国家医保目录名称")
    private String ydProMedListName;

    @ApiModelProperty("药德国家医保医保目录剂型")
    private String ydProMedListForm;

    @ApiModelProperty("药德注意事项")
    private String ydProNoticeThings;

    @ApiModelProperty("更新人")
    private String updater;

    @ApiModelProperty("更新时间")
    private String updateTime;

}
