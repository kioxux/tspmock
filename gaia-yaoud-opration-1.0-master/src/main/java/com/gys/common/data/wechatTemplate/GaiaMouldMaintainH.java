package com.gys.common.data.wechatTemplate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 微信模板维护主表
 * @author cxy
 * @date 2022-01-06
 */
@Data
@ApiModel("微信模板维护主表")
public class GaiaMouldMaintainH implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("微信模板id")
    private String mouldId;

    @ApiModelProperty("模板编码")
    private String mouldCode;

    @ApiModelProperty("模板名称")
    private String mouldName;

    @ApiModelProperty("模板标题")
    private String mouldTitle;

    @ApiModelProperty("标题内容")
    private String titleContent;

    @ApiModelProperty("标题结尾")
    private String mouldEnding;

    @ApiModelProperty("结尾内容")
    private String endingContent;

    @ApiModelProperty("示例内容")
    private String example;

    @ApiModelProperty("状态（0：停用，1：启用）")
    private String status;

    @ApiModelProperty("创建日期")
    private String createDate;

    @ApiModelProperty("更新日期")
    private String updateDate;

    @ApiModelProperty("创建加盟商")
    private String createClient;

    @ApiModelProperty("是否删除（0：未删除，1：已删除）")
    private String delFlag;

}