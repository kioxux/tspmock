package com.gys.common.data;

import lombok.Data;

@Data
public class SpecialClass {
    private String clientId;
    private String stoCode;
    private String bigClass;
    private String stockCode;
    private String stockType;
}
