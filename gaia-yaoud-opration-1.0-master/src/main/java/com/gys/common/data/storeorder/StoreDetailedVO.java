package com.gys.common.data.storeorder;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author wu mao yin
 * @Title: 门店明细清单返回
 * @Package
 * @date 2021/12/315:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StoreDetailedVO extends StoreOrderProData {

    @ApiModelProperty("门店编码")
    @ExcelProperty("门店编码")
    private String stoCode;

    @ApiModelProperty("门店名称")
    @ExcelProperty("门店名称")
    private String stoName;

    @ApiModelProperty("出库价")
    @ExcelProperty("出库价")
    private BigDecimal outgoingPrice;

    @ApiModelProperty("金额")
    @ExcelProperty("金额")
    private BigDecimal amount;

    @ApiModelProperty("补货单号")
    @ExcelProperty("补货单号")
    private String replenishmentNo;

    @ApiModelProperty("行号")
    @ExcelProperty("行号")
    private String serialNo;

    @ApiModelProperty("订单日期")
    @ExcelProperty("订单日期")
    private String orderDate;

}
