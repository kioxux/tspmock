package com.gys.common.data.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/12 13:54
 */
@Data
public class YaomengDiseaseBean {

    private Long preIllId;//病症Id
    private String preIllName;//病症名称
}
