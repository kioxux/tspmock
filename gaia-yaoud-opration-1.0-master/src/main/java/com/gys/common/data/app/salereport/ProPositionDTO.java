package com.gys.common.data.app.salereport;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 商品定位统计入参
 * @date 2021/11/3016:34
 */
@Data
public class ProPositionDTO implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "日期维度 1:上月,2:上周")
    private Integer dateDimension;

    @ApiModelProperty(value = "商品分类")
    private String bigClass;

    @ApiModelProperty(value = "商品分类集合", hidden = true)
    private List<String> bigClasses;

}

