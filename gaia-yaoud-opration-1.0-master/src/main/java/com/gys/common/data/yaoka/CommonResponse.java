package com.gys.common.data.yaoka;

import lombok.Data;

import java.util.List;

/**
 * @author 胡鑫鑫
 */
@Data
public class CommonResponse<T> {
    private Boolean success;
    private String errorCode;
    private T model;
    private String errorMsg;
}
