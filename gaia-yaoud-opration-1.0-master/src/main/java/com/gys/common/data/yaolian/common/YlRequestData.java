package com.gys.common.data.yaolian.common;

import com.gys.common.data.yaolian.common.RequestHead;
import lombok.Data;

/**
 * 入参
 * @author 胡鑫鑫
 */
@Data
public class YlRequestData {
    /**
     *
     */
    private RequestHead requestHead;
    /**
     * | channel | String | 1 | 1、普通（默认为1）| 必须 |
     */
    private String channel;
}
