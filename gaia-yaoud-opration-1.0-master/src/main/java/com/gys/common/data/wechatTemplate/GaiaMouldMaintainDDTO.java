package com.gys.common.data.wechatTemplate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description  微信模板维护明细表
 * @author cxy
 * @date 2022-01-06
 */
@Data
@ApiModel(" 微信模板维护明细表")
public class GaiaMouldMaintainDDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("内容")
    private String content;

    public GaiaMouldMaintainDDTO() {}
}