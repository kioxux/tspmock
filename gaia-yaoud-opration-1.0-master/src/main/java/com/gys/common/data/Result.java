//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.common.data;

import com.google.gson.Gson;
import com.gys.util.SignUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Result {
    Gson gson = new Gson();
    private Map<String, Object> result;

    public Result(Map<String, Object> result) {
        this.result = result;
    }

    public Map<String, Object> getResult() {
        return this.result;
    }

    public void setCreateResult(Map<String, Object> config, boolean flag) {
        Map<String, Object> body = new HashMap();
        HashMap source_order_id;
        Random rand;
        if (flag) {
            body.put("errno", "0");
            body.put("error", "success");
            source_order_id = new HashMap();
            rand = new Random(2147483647L);
            source_order_id.put("source_order_id", rand.nextInt(2147483647));
            body.put("data", source_order_id);
        } else {
            body.put("errno", "-1");
            body.put("error", "check sign failed!");
            source_order_id = new HashMap();
            rand = new Random(2147483647L);
            source_order_id.put("source_order_id", rand.nextInt(2147483647));
            body.put("data", source_order_id);
        }

        this.result.put("body", this.gson.toJson(body));
        this.result.put("cmd", "resp.order.create");
        String sign = SignUtil.getSign(this.result, config);
        this.result.put("body", body);
        this.result.put("sign", sign);
    }

    public void setPushStatusResult(Map<String, Object> config, boolean flag) {
        Map<String, Object> body = new HashMap();
        if (flag) {
            body.put("errno", "0");
            body.put("error", "success");
        } else {
            body.put("errno", "-1");
            body.put("error", "check sign failed!");
        }

        this.result.put("body", this.gson.toJson(body));
        this.result.put("cmd", "resp.order.status.push");
        String sign = SignUtil.getSign(this.result, config);
        this.result.put("sign", sign);
        this.result.put("body", body);
    }

    public void setCancelResult(Map<String, Object> config, boolean flag) {
        Map<String, Object> body = new HashMap();
        if (flag) {
            body.put("errno", "0");
            body.put("error", "success");
        } else {
            body.put("errno", "-1");
            body.put("error", "check sign failed!");
        }

        this.result.put("body", this.gson.toJson(body));
        this.result.put("cmd", "resp.order.user.cancel");
        String sign = SignUtil.getSign(this.result, config);
        this.result.put("sign", sign);
        this.result.put("body", body);
    }

    public void setPartRefundPushResult(Map<String, Object> config, boolean flag) {
        Map<String, Object> body = new HashMap();
        if (flag) {
            body.put("errno", "0");
            body.put("error", "success");
        } else {
            body.put("errno", "-1");
            body.put("error", "check sign failed!");
        }

        this.result.put("body", this.gson.toJson(body));
        this.result.put("cmd", "resp.order.partrefund.push");
        String sign = SignUtil.getSign(this.result, config);
        this.result.put("sign", sign);
        this.result.put("body", body);
    }
}
