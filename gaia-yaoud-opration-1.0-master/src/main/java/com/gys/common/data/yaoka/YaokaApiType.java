package com.gys.common.data.yaoka;

/**
 * 药卡支付环境配置
 * @author 胡鑫鑫
 */

public enum YaokaApiType {
    PRE_CALC("preCalc", "9c195789802c9054c4027bea3823fe26"),
    POLL_READY_PLAN("pollReadyPlan", "70e34466e3da1623a03e60a688d8065e"),
    PAY("pay", "8408863aee7fe0104e95669d1fe72fe3"),
    UNDO_PAY("undoPay", "0558752788129844801c40cb8c16b73c");

    private String apiName;
    private String apiId;

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    YaokaApiType(String apiName, String apiId) {
        this.apiName = apiName;
        this.apiId = apiId;
    }
//    public static YaokaApiType getQueryEnv(String code){
//            for (YaokaApiType queryEnv : YaokaApiType.values()) {
//            if (!queryEnv.getCode().equals(code)) {
//                continue;
//            }
//            return queryEnv;
//        }
//        return null;
//
//    }
}
