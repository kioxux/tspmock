package com.gys.common.data;

import lombok.Data;

@Data
public class PtsOrderProInfo {
    private String quantity;
    private String box_num;
    private String box_price;
    private String unit;
    private String discount;
    private String order_id;
    private String medicine_code;
    private String sequence;
    private String client;
    private String sto_code;
    private String price;
    private String refund_qty;
    private String refund_price;
}
