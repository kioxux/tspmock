package com.gys.common.data.member;

import com.gys.common.data.PageDomain;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.List;

/**
 * 积分抵现活动
 *
 * @author wu mao yin
 * @date 2021/12/9 15:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PointCashOutActivitySearchDTO extends PageDomain {

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("开始日期")
    private String gsicStartDate;

    @ApiModelProperty("结束日期")
    private String gsicEndDate;

    @ApiModelProperty("门店")
    private List<String> stoCodes;

    @ApiModelProperty("积分抵现活动单号")
    private String gsicVoucherId;

    @ApiModelProperty("积分抵现活动名称")
    private String gsicName;

    @ApiModelProperty("积分抵现规则代号")
    private String gsicPlanId;

    @ApiModelProperty("单据状态 0-保存，1-审核")
    private Integer gsicStatus;

    @ApiModelProperty("会员卡类型")
    private List<String> memberCardTypes;

    @ApiModelProperty("会员卡类型")
    private String memberCardType;

    private static final long serialVersionUID = 1L;

}