package com.gys.common.data;

import com.gys.business.mapper.entity.GaiaSdMemberBasic;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/5/14 10:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberAddDto {

    private GaiaSdMemberBasic basic;
    private GaiaSdMemberCardData card;

}
