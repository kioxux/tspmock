package com.gys.common.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wu mao yin
 * @Title:
 * @Package
 * @date 2021/10/2017:15
 */
@Data
public class StoreCategoryDropDown implements Serializable {

    private String id;

    private String name;

    private String label;

    @ApiModelProperty("子节点")
    private List<StoreCategoryDropDown> childNode;

}
