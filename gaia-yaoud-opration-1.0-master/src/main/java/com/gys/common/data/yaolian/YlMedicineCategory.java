package com.gys.common.data.yaolian;

import com.gys.common.data.yaolian.common.RequestHead;
import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/8 13:51
 */
@Data
public class YlMedicineCategory {

    private RequestHead requestHead;
    private List<YlMedicineCategoryDetail> categorys;

}
