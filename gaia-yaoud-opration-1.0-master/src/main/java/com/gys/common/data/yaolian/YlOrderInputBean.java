package com.gys.common.data.yaolian;

import com.gys.common.data.yaolian.common.RequestHead;
import com.gys.util.takeAway.yl.YlOrderBean;
import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/8 18:18
 */
@Data
public class YlOrderInputBean {

    private RequestHead requestHead;
    private String channel;//1:普通
    private List<YlOrderBean> orders;
}
