package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Data
@ApiOperation("返回审核状态")
public class ReviewCheckStatus {
    @ApiModelProperty("状态码")
    private String code;

    @ApiModelProperty("状态描述")
    private String desc;
}
