package com.gys.common.data.yaolian;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/8 16:21
 */
@Data
public class YlMedicineQueryBean {

    private String store_id;//门店ID  必须
    private String goods_id;//药品ID  非必须
    private String code;//门店名称	非必须

}
