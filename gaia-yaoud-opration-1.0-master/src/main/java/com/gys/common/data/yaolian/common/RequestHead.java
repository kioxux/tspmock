package com.gys.common.data.yaolian.common;

import lombok.Data;

import java.util.Date;

/**
 * @author 胡鑫鑫
 */
@Data
public class RequestHead {
    /**
     *| cooperation | String | yaolian | 组织代码 | 是 |
     */
    private String cooperation;
    /**
     *
     */
    private String nonce;
    /**
     * Sign验证代码
     */
    private String sign;
    private String timestamp;
    private String tradeDate;
}
