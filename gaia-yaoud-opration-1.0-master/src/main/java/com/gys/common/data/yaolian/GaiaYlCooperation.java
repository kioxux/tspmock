package com.gys.common.data.yaolian;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * GAIA_YL_COOPERATION
 * @author 
 */
@Data
public class GaiaYlCooperation implements Serializable {
    private Long id;

    private String client;

    /**
     * 组织代码
     */
    private String cooperation;

    private Date createTime;

    private static final long serialVersionUID = 1L;
}