package com.gys.common.data;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;

public class PtsReturnMessage extends PtsReturnCommonMessage {


    @JSONField(name ="DATA")
    public ArrayList<String> getDATA() {
        return DATA;
    }

    public void setDATA(ArrayList<String> DATA) {
        this.DATA = DATA;
    }

    private ArrayList<String> DATA;
    public  PtsReturnMessage()
    {
        DATA=new ArrayList<>();
    }

}
