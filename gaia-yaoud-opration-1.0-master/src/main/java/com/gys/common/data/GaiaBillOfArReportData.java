package com.gys.common.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 明细清单表(GaiaBillOfAr)实体类
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:11
 */
@Data
@CsvRow("批发应收汇总")
public class GaiaBillOfArReportData implements Serializable {
    private static final long serialVersionUID = -38101044084295338L;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 客户编码
     */
    private String cusSelfCode;
    private String cusName;
    /**
     * 仓库编码
     */
    private String dcCode;
    private String dcName;
    /**
     * 某某年之前余额
     */
    private BigDecimal zqye;
    /**
     * 本期应收增加
     */
    @CsvCell(title = "本期应收增加",index = 6,fieldNo = 1)
    private BigDecimal bqys;
    /**
     * 本期回款
     */
    @CsvCell(title = "本期回款",index = 7,fieldNo = 1)
    private BigDecimal bqhk;
    /**
     * 当前余额
     */
    @CsvCell(title = "当前余额",index = 11,fieldNo = 1)
    private BigDecimal dqye;
    /**
     * 前一月余额
     */
    private BigDecimal one;
    /**
     * 前三月余额
     */
    private BigDecimal three;
    /**
     * 前六个月
     */
    private BigDecimal six;
    /**
     * 一月内
     */
    @CsvCell(title = "1个月内",index = 12,fieldNo = 1)
    private BigDecimal oneMonth;
    /**
     * 3月内
     */
    @CsvCell(title = "3个月内",index = 13,fieldNo = 1)
    private BigDecimal threeMonth;
    /**
     * 6月内
     */
    @CsvCell(title = "6个月内",index = 14,fieldNo = 1)
    private BigDecimal sixMonth;
    /**
     * 6月以上
     */
    @CsvCell(title = "6个月以上",index = 15,fieldNo = 1)
    private BigDecimal overSixMonth;

    /**
     * 期初金额
    */
    @CsvCell(title = "期初余额",index = 5,fieldNo = 1)
    private BigDecimal openingAmount;

    /**
     *应收调整
     */
    @CsvCell(title = "应收调整",index = 9,fieldNo = 1)
    private BigDecimal adjustment;
    /**
     * 应收-其他
     */
    @CsvCell(title = "应收-其他",index = 10,fieldNo = 1)
    private BigDecimal other;
    /**
     *销售员编号
     */
    private String gwsCode;
    /**
     *销售员名字
     */
    private String gwsName;
    /**
     *销售员编号名字
     */
    @CsvCell(title = "销售员",index = 8,fieldNo = 1)
    private String gwsCodeByName;
    private String startTime;
    private String endTime;
    /**
     *仓库编码名称集合
     */
    @CsvCell(title = "仓库编码",index = 1,fieldNo = 1)
    private String dcCodeByDcName;
    /**
     *客户编码名称集合
     */
    @CsvCell(title = "客户编码",index = 3,fieldNo = 1)
    private String cusSelfCodeByName;
}
