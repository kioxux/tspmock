package com.gys.common.data;

import lombok.Data;

@Data
public class SupInfo {
    private String proCode;
    private String poPrice;
    private String supCode;
    private String supName;
}
