package com.gys.common.data.newProductsOrder;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("新品订购查询返回对象")
@Data
public class NewProductsOrderInfoDTO {

    @ApiModelProperty("门店编号")
    @ExcelProperty(value = "门店编号",index = 0)
    private String gsphBrId;

    @ApiModelProperty("门店简称")
    @ExcelProperty(value = "门店简称",index = 1)
    private String gsradBrName;

    @ApiModelProperty("订购日期")
    @ExcelProperty(value = "订购日期",index = 2)
    private String gsphDate;

    @ApiModelProperty("订购时间")
    @ExcelProperty(value = "订购时间",index = 3)
    private String gsphTime;

    @ApiModelProperty("顾客姓名")
    @ExcelProperty(value = "顾客姓名",index = 4)
    private String gsphCostoerName;

    @ApiModelProperty("顾客手机")
    @ExcelProperty(value = "顾客手机",index = 5)
    private String gsphCostoerMobile;

    @ApiModelProperty("订购金额")
    @ExcelProperty(value = "订购金额",index = 6)
    private String gsphAmt;

    @ApiModelProperty("录入员工")
    @ExcelProperty(value = "录入员工",index = 7)
    private String gsphEmp;

    @ApiModelProperty("订购单号")
    @ExcelProperty(value = "订购单号",index = 8)
    private String gsphVoucherId;

    @ApiModelProperty("商品名称")
    @ExcelProperty(value = "商品名称",index = 9)
    private String gspdProName;

    @ApiModelProperty("通用名")
    @ExcelProperty(value = "通用名",index = 10)
    private String gspdProCommonname;

    @ApiModelProperty("规格")
    @ExcelProperty(value = "规格",index = 11)
    private String gspdProSpecs;

    @ApiModelProperty("厂家")
    @ExcelProperty(value = "厂家",index = 12)
    private String gspdProFactoryName;

    @ApiModelProperty("单位")
    @ExcelProperty(value = "单位",index = 13)
    private String gspdProUnit;

    @ApiModelProperty("条形码")
    @ExcelProperty(value = "条形码",index = 14)
    private String gspdProBarcode;

    @ApiModelProperty("批准文号")
    @ExcelProperty(value = "批准文号",index = 15)
    private String gspdProRegisterNo;

    @ApiModelProperty("订购数量")
    @ExcelProperty(value = "订购数量",index = 16)
    private String gspdQty;

    @ApiModelProperty("参考零售价")
    @ExcelProperty(value = "参考零售价",index = 17)
    private String gspdProLsj;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注",index = 18)
    private String remark;
}
