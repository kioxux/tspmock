package com.gys.common.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 9:40 2021/8/10
 * @Description：无计划报表查询省和市入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class NewProductDistributionNoPlanProvAndCityDto {

    @NotBlank(message = "省份编码不允许为空")
    private String provCode;

}
