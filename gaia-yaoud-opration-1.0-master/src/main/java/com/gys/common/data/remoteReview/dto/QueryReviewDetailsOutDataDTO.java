package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description 查询远程审方列表-处方明细返回对象
 * @author chenxingyu
 * @date 2021-11-04
 */
@Data
@ApiModel("查询远程审方列表-处方明细返回对象")
public class QueryReviewDetailsOutDataDTO {

    @ApiModelProperty("商品编码")
    private String gsradProId;

    @ApiModelProperty("商品名称")
    private String proName;

    @ApiModelProperty("规格")
    private String proSpecs;

    @ApiModelProperty("单位")
    private String proUnit;

    @ApiModelProperty("批号")
    private String gsradBatchNo;

    @ApiModelProperty("数量")
    private BigDecimal gsradQty;

    @ApiModelProperty("零售价")
    private String gsppPriceNormal;

    @ApiModelProperty("厂家")
    private String proFactoryName;

    @ApiModelProperty("产地")
    private String proPlace;

    @ApiModelProperty("剂型")
    private String proForm;

    @ApiModelProperty("批准文号")
    private String proRegisterNo;

}
