package com.gys.common.data;

import lombok.Data;

/**
 * @Author ：gyx
 * @Date ：Created in 13:16 2021/11/26
 * @Description：模糊查询入参
 * @Modified By：gyx
 * @Version:
 */
@Data
public class FuzzyQueryForConditionInData {

    private String client;

    private String startDate;

    private String endDate;

    private String queryString;


}
