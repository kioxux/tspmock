package com.gys.common.data;

import com.gys.business.mapper.entity.*;
import lombok.Data;

import java.util.List;

/**
 * @author 胡鑫鑫
 */
@Data
public class DataSubServer {
    private List<GaiaSdSaleHLocal> saleHList;
    private List<GaiaSdSaleDLocal> saleDList;
    private List<GaiaSdSalePayMsgLocal> salePayMsgList;
    private List<GaiaSdBatchChangeLocal> batchChangeList;
}
