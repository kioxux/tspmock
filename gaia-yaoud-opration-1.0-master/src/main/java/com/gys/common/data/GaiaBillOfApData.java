package com.gys.common.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GaiaBillOfApData {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "地点")
    private String dcCode;

    @ApiModelProperty(value = "供应商编码")
    private String supSelfCode;

    @ApiModelProperty(value = "结算金额")
    private String amountOfPayment;
}
