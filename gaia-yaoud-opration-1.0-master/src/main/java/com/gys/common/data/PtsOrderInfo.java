package com.gys.common.data;

import lombok.Data;

import java.util.List;

@Data
public class PtsOrderInfo extends PtsReturnCommonMessage {
    private String platforms;
    private String order_id;
    private String platforms_order_id;
    private String shop_no;
    private String recipient_address;
    private String recipient_name;
    private String recipient_phone;
    private String shipping_fee;
    private String total_price;
    private String original_price;
    private String caution;
    private String shipper_name;
    private String shipper_phone;
    private String customer_pay;
    private String status;
    private String is_invoiced;
    private String invoice_title;
    private String taxpayer_id;
    private String delivery_time;
    private String pay_type;
    private String order_info;
    private String create_time;
    private String update_time;
    private String reason_code;
    private String reason;
    private String day_seq;
    private List<PtsOrderProInfo> items;
    //后补充：方便方法执行结果的判断
    private String result;
    private String client;
    private String sto_code;
}
