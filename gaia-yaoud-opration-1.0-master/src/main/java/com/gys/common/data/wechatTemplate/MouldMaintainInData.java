package com.gys.common.data.wechatTemplate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MouldMaintainInData {

    @ApiModelProperty("模板编码")
    private String mouldCode;

    @ApiModelProperty("模板名称")
    private String mouldName;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "页码")
    private Integer pageNum;

    @ApiModelProperty(value = "页大小")
    private Integer pageSize;
}
