package com.gys.common.data;

import lombok.Data;

@Data
public class PtsOrderStatus {
    private String shop_no;
    private String order_id;
    private String status;
    private String action;
    private String reason;
    private String reason_code;
    private String shipper_name;
    private String shipper_phone;
    private String notice_message;
}
