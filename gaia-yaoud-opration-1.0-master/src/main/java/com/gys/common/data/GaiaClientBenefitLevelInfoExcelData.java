package com.gys.common.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author csm
 * @date 2021/12/20 - 12:49
 */
@Data
@CsvRow("必备商品铺货范围")
public class GaiaClientBenefitLevelInfoExcelData {
    @ApiModelProperty(value= "序号")
    private Integer index;
    /**
     * 客户加盟ID
     */
    @ApiModelProperty(value= "客户ID")
    private String client;

    /**
     * 加盟商名称
     */
    @ApiModelProperty(value= "客户名称")
    private String francName;

    /**
     * 店效级别 0-无 1-超高 2-高 3-中 4-低
     */
    @ApiModelProperty(value="DX0002-店效效级别")
    private String storeLevel;

    /**
     * 综合排名上限值
     */
    @ApiModelProperty(value="综合排名上限值")
    private Integer comprehensiveRank;

    /**
     * 合格品项项数
     */
    @ApiModelProperty(value="合格品项项数")
    private Integer stockQualifiedCount;

    /**
     * 铺货品项数上限值
     */
    @ApiModelProperty(value="铺货品项数上限值")
    private Integer stepValue;

    /**
     * 更新者ID
     */
    @ApiModelProperty(value="修改人")
    private String updateUser;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-DD")
    @ApiModelProperty(value= "修改日期")
    private Date updateTime;


}
