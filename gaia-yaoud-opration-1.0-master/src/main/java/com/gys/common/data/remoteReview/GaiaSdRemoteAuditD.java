package com.gys.common.data.remoteReview;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 远程审方明细
 * @author chenxingyu
 * @date 2021-11-03
 */
@Data
public class GaiaSdRemoteAuditD implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("远程审方单号")
    private String gsradVoucherId;

    @ApiModelProperty("店号")
    private String gsradBrId;

    @ApiModelProperty("行号")
    private String gsradSerial;

    @ApiModelProperty("商品编码")
    private String gsradProId;

    @ApiModelProperty("批号")
    private String gsradBatchNo;

    @ApiModelProperty("数量")
    private BigDecimal gsradQty;

    public GaiaSdRemoteAuditD() {}
}