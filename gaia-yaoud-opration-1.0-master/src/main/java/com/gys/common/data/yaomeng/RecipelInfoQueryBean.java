package com.gys.common.data.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/15 9:43
 */
@Data
public class RecipelInfoQueryBean {

    private String client;
    private String stoCode;
    private String saleBillNo;//销售单号
    private String saleDate;//销售日期
    private String recipelDate;//开方日期
    private String status;//状态

}
