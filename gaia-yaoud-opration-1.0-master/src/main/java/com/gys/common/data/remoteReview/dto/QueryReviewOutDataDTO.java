package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Set;

/**
 * @description 查询远程审方列表返回对象
 * @author chenxingyu
 * @date 2021-11-04
 */
@Data
@ApiModel("查询远程审方列表返回对象")
public class QueryReviewOutDataDTO {

    @ApiModelProperty("销售单号")
    private String gssrSaleBillNo;

    @ApiModelProperty("审方日期")
    private String gssrCheckDate;

    @ApiModelProperty("审方时间")
    private String gssrCheckTime;

    @ApiModelProperty("药师编号")
    private String gssrPharmacistId;

    @ApiModelProperty("药师名称")
    private String gssrPharmacistName;

    @ApiModelProperty("店名")
    private String gssrBrName;

    @ApiModelProperty("顾客姓名")
    private String gssrCustName;

    @ApiModelProperty("顾客性别")
    private String gssrCustSex;

    @ApiModelProperty("顾客年龄")
    private String gssrCustAge;

    @ApiModelProperty("状态")
    private String gssrCheckStatus;

    @ApiModelProperty("审方结论")
    private String checkResult;

    @ApiModelProperty("处方图片地址")
    private String gssrRecipelPicAddress;

    @ApiModelProperty("上传日期")
    private String gssrDateTime;

    // 处方明细列表
    @ApiModelProperty("商品信息列表")
    private ArrayList<QueryReviewDetailsOutDataDTO> detailsList;


}
