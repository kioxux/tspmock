package com.gys.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author ZhangDong
 * @date 2021/11/23 10:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeideProIdDto {

    private String proId;//商品编码
    private BigDecimal qty;//库存
    private BigDecimal price;//价格

    public WeideProIdDto(String proId) {
        this.proId = proId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeideProIdDto that = (WeideProIdDto) o;
        return Objects.equals(proId, that.proId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(proId);
    }
}
