package com.gys.common.data;

import lombok.Data;

@Data
public class BaseOrderProInfo {
    private String client;
    private String stoCode;
    private String order_id;	// 系统订单ID
    private String medicine_code; // 药品编码
    private String quantity; // 数量
    private String sequence; // 订单内药品行号
    private String box_num; // 包装数量
    private String box_price; // 包装价格
    private String unit; // 单位
    private String discount; // 折扣信息
    private String shop_no;
    private String stock_lock;
    private String refund_qty;
    private String refund_price;
    private String price;//单价
    //部分退货时针对第二件半价这种依次字段决定到底退的哪个
    private String foodPrice;
    //商品在外卖平台上的名称
    private String platformProductName;
}
