package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author chenxingyu
 * @description 查询远程审方列表入参
 * @date 2021-11-04
 */
@Data
@ApiModel("查询远程审方列表入参")
public class QueryReviewParamDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商品编码")
    private String gsradProId;

    @ApiModelProperty("审方日期-开始时间")
    private String gsrahDateStart;

    @ApiModelProperty("审方日期-结束时间")
    private String gsrahDateEnd;

    @ApiModelProperty("药师名称")
    private String gsrahPharmacistName;

    @ApiModelProperty("顾客姓名")
    private String gsrahCustName;

    @ApiModelProperty("状态")
    private String gsrahStatus;

    @ApiModelProperty("登记店号批量查询")
    private String[] storeArr;

    @ApiModelProperty("登记开始时间")
    private String gssrDateStart;

    @ApiModelProperty("登记结束时间")
    private String gssrDateEnd;
}
