package com.gys.common.data.member;

import com.gys.common.data.PageDomain;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 积分规则设置
 * @date 2021/12/815:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PointSettingSearchDTO extends PageDomain {

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("规则代号")
    private String gsicsPlanId;

    @ApiModelProperty("规则描述")
    private String gsicsPlanName;

    @ApiModelProperty("抵现类型 0-单次固定，1-最优")
    private Integer gsicsType;

    @ApiModelProperty("会员卡类型, 多个逗号分割")
    private String gsicsMemberCardType;

    @ApiModelProperty(value = "会员卡类型", hidden = true)
    private List<String> memberCardTypes;
}
