package com.gys.common.data.yaoka.pollReadyPlan;

import lombok.Data;

import java.util.List;

@Data
public class PaymentDecision {
    /**
     * 支付方案的id
     */
    private long planId;
    /**
     * 方案详情
     */
    private List<DecisionItem> items;
}
