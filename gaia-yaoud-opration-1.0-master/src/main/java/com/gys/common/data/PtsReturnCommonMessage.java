package com.gys.common.data;

import lombok.Data;

@Data
public class PtsReturnCommonMessage {
    private String MESSAGE;
    private String RETURNCODE;
}
