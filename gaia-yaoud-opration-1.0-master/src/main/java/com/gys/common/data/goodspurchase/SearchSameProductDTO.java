package com.gys.common.data.goodspurchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jinwencheng
 * @version 1.0
 * @Description 查找符合条件的商品信息需要的参数
 * @createTime 2021-12-30 11:15:00
 */
@Data
public class SearchSameProductDTO {

    @ApiModelProperty("国际条形码1")
    private String proBarCode;

    @ApiModelProperty("国际条形码2")
    private String proBarCode2;

    @ApiModelProperty("批准文号")
    private String proRegisterNo;

}
