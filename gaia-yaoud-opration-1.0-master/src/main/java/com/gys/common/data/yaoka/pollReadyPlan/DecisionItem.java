package com.gys.common.data.yaoka.pollReadyPlan;

import lombok.Data;

/**
 * @author 胡鑫鑫
 */
@Data
public class DecisionItem {
    /**
     *
     */
    private long id;
    /**
     * 类型 代码取值：1-现有药诊卡扣款；2-非固定面额卡（在线充值）；3-药店线下扣款
     */
    private Integer type;
    /**
     * 药诊卡卡号。当type=1或2时，才有值
     */
    private String cardNo;
    /**
     * 金额
     */
    private long amount;
}
