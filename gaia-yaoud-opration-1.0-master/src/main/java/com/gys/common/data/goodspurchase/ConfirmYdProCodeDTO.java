package com.gys.common.data.goodspurchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jinwencheng
 * @version 1.0
 * @Description 确认药德商品编码需要的参数
 * @createTime 2021-12-30 11:15:00
 */
@Data
public class ConfirmYdProCodeDTO {

    @ApiModelProperty("客户")
    private String client;

    @ApiModelProperty("地点")
    private String proSite;

    @ApiModelProperty("客户商品编码")
    private String proCode;

    @ApiModelProperty("药德商品编码")
    private String ydProCode;

    private String updateClient;

    private String updater;

    private String updateTime;
}
