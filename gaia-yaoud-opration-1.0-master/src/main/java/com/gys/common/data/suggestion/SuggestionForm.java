package com.gys.common.data.suggestion;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/10/29 14:38
 */
@Data
public class SuggestionForm {
    /**
     * 加盟商
     */
    protected String client;
    /**
     * 调剂建议单号（调出）
     */
    @NotNull(message = "调剂建议单号不能为空")
    protected String billCode;
    /**
     * 门店编码
     */
    protected String stoCode;
    /**
     * 操作员工ID
     */
    protected String userId;
    /**
     * 操作员工名称
     */
    private String userName;
}
