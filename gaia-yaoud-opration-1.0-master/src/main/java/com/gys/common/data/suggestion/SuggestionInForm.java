package com.gys.common.data.suggestion;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/1 11:30
 */
@Data
public class SuggestionInForm {
    /**
     * 加盟商
     */
    protected String client;
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 调剂建议单号（调出）
     */
    protected String billCode;
    /**
     * 起始日期：yyyy-MM-hh
     */
    private String startDate;
    private String startTime;
    /**
     * 结束日期：yyyy-MM-hh
     */
    private String endDate;
    private String endTime;
    /**
     * 调出单据号
     */
    private String outBillCode;
    /**
     * 单据状态：0-待处理 1-已处理 2-失效
     */
    @Max(value = 2, message = "请输入正确的单据状态", groups = SuggestionListValidator.class)
    @Min(value = 0, message = "请输入正确的单据状态", groups = SuggestionListValidator.class)
    private Integer status;
    /**
     * 分页参数：当前页码，默认1
     */
    private Integer pageNum;
    /**
     * 分页参数：分页大小，默认10
     */
    private Integer pageSize;
}
