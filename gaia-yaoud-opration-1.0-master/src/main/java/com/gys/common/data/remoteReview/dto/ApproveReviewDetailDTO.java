package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("审核处方明细入参")
public class ApproveReviewDetailDTO {

    @ApiModelProperty("商品编码")
    private String gsradProId;

    @ApiModelProperty("数量")
    private BigDecimal gsradQty;
}
