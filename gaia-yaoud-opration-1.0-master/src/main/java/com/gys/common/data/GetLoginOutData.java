package com.gys.common.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetLoginOutData implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "")
    private String userId;
    @ApiModelProperty(value = "")
    private String loginAccount;
    @ApiModelProperty(value = "")
    private String client;
    @ApiModelProperty(value = "")
    private String loginName;
    @ApiModelProperty(value = "")
    private String userSex;
    @ApiModelProperty(value = "")
    private String userTel;
    @ApiModelProperty(value = "")
    private String userIdc;
    @ApiModelProperty(value = "")
    private String userYsId;
    @ApiModelProperty(value = "")
    private String userYsZyfw;
    @ApiModelProperty(value = "")
    private String userYsZylb;
    @ApiModelProperty(value = "")
    private String depId;
    @ApiModelProperty(value = "")
    private String depName;
    @ApiModelProperty(value = "")
    private String userAddr;
    @ApiModelProperty(value = "")
    private String userSta;
    @ApiModelProperty(value = "")
    private String token;
    @ApiModelProperty(value = "连锁编码")
    private String dcCode;
    @ApiModelProperty(value = "")
    private List<GetLoginFrancOutData> franchiseeList;
}
