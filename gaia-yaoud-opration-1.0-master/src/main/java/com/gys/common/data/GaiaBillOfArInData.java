package com.gys.common.data;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 明细清单表(GaiaBillOfAr)实体类
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:11
 */
@Data
public class GaiaBillOfArInData extends BaseRowModel implements Serializable {
    private static final long serialVersionUID = -38101044084295338L;

    /*@ApiModelProperty(value = "抬头备注")
    private String remark;*/

    @ApiModelProperty(value = "开单员集合")
    private List<String>  soDnByCodeList;

   /* @ApiModelProperty(value = "销售员")
    private String gssCode;*/

    /**
     * 销售员编号集合
     */
    private List<String> gwsCodeList;

    @ApiModelProperty(value = "开单员")
    private  String  soDnByCode;

   /* @ApiModelProperty(value = "销售员")
    private String gssCode;*/

    /**
     * 销售员编号
     */
    private  String  gwsCode;




    /**
     * 销售员姓名
     */
    private String gwsName;

    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 回款单号
     */
    private String billCode;
    /**
     * 仓库编码
     */
    @ExcelProperty(index = 0)
    private String dcCode;
    /**
     * 应收方式编码
     */
    @ExcelProperty(index = 2)
    private String arMethodId;
    private List<String> arMethodIdList;
    private List<String> cusSelfCodeList;
    private List<String> dcCodeList;
    private List<String> stoCodeList;
    /**
     * 发生金额
     */
    private BigDecimal arAmt;
    @ExcelProperty(index = 3)
    private String amt;
    /**
     * 核销金额
     */
    private BigDecimal hxAmt;
    /**
     * 备注
     */
    @ExcelProperty(index = 5)
    private String remark;
    /**
     * 发生日期
     */
    @ExcelProperty(index = 4)
    private String billDateStr;
    private String keywords;
    private Date billDate;
    private Date startTime;
    private Date endTime;
    /**
     * 客户自编码
     */
    @ExcelProperty(index = 1)
    private String cusSelfCode;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 核销状态 0-未核销 1-核销中 2-已核销
     */
    private Integer status;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;
    private String matDnId;
    private String matPoId;

    List<GaiaBillOfArDetailOutData> billList;

    private List<String> statusList;

    private String isSales;
}
