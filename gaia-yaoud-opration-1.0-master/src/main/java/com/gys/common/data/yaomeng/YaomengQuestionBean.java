package com.gys.common.data.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/12 13:56
 */
@Data
public class YaomengQuestionBean {

    /**
     * 问题
     */
    private String preQuestion;

    /**
     * 答案
     */
    private String preAnswer;

}
