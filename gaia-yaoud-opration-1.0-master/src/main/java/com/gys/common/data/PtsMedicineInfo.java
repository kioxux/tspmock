package com.gys.common.data;

import lombok.Data;

@Data
public class PtsMedicineInfo {
    private String medicine_code;
    private String sequence;
    private String quantity;
    private String box_num;
    private String box_price;
    private String unit;
    private String discount;
    private String refund_qty;
    private String refund_price;
    private String order_id;
    private String price;
}
