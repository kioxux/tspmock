package com.gys.common.data;

import lombok.Data;

import java.util.List;

/**
 * @description 订单信息
 * @author Mirai.Li
 */
@Data
public class BaseOrderInfo {
    private String client;
    private String stoCode;
    private String platforms; // 外卖平台
    private String order_id; // 系统订单ID
    private String platforms_order_id; // 外卖平台订单号
    private String shop_no; // 门店编码
    private String recipient_address; // 收件人地址
    private String recipient_name; // 收件人姓名
    private String recipient_phone; // 收件人电话
    private String shipping_fee; // 门店配送费
    private String total_price; // 总价（门店实际收入）
    private String original_price; // 原价
    private String customer_pay; // 用户实付金额
    private String caution; // 备注
    private String shipper_name; // 配送员姓名
    private String shipper_phone; // 配送员电话
    private String status; // 订单状态（0:新订单[正常订单]1:已确认订单2:已配送订单3:已完成订单4:用户申请退单5:门店申请退单6:用户催单）
    private String action;
    private String is_invoiced; // 是否开票（0：不需要发票1：需要发票）
    private String invoice_title; // 发票抬头
    private String taxpayer_id; // 纳税人识别号
    private String delivery_time; // 延时配送（延迟多久送出，默认0，立刻送出）
    private String pay_type; // 支付类型（0：在线支付1：货到付款）
    private String order_info; // 优惠信息
    private String create_time; // 创建时间
    private String update_time; // 更新时间
    private String reason_code; // 退单原因代码
    private String reason; // 退单原因
    private List<BaseOrderProInfo> items;// 订单商品明细
    private String day_seq;//外卖员取货码
    //自定义一个属性，用来接收转换结果
    private String result;
    private String refund_money;
    private String notice_message;
    //售后单号
    private String afsServiceOrder;
    //EB 是否处方药订单，枚举值： 0 不是 ，1 是; MT 1 是
    private String prescriptionOrNot;

}
