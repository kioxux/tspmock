package com.gys.common.data.remoteReview.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("商品信息DTO")
public class UpadteDrugsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商品编码")
    private String gsradProId;

    @ApiModelProperty("名称")
    private String proName;

    @ApiModelProperty("规格")
    private String proSpecs;

    @ApiModelProperty("单位")
    private String proUnit;

    @ApiModelProperty("行号")
    private String gsradSerial;

    @ApiModelProperty("批号")
    private String gsradBatchNo;

    @ApiModelProperty("数量")
    private BigDecimal gsradQty;
}
