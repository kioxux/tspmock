package com.gys.common.data.yaomeng;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/11/15 9:48
 */
@Data
public class RecipelInfoQueryDto {

    private String saleBillNo;
    private String saleDate;
    private String recipelId;
    private String custName;
    private String custSex;
    private String custAge;
    private String custIdCard;
    private String recipelPicture;
    private String status;

}
