package com.gys.common.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 明细清单表(GaiaBillOfAr)实体类
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:11
 */
@Data
@CsvRow("批发应收明细")
public class GaiaBillOfArDetailOutData implements Serializable {
    private static final long serialVersionUID = -38101044084295338L;

    @ApiModelProperty(value = "开单员编码")
    private String soDnByCode;

    @ApiModelProperty(value = "开单员姓名")
    private String soDnByName;

    @ApiModelProperty(value = "销售员编码")
    private String gssCode;

    @ApiModelProperty(value = "销售员")
    private String gssName;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 仓库编码
     */
    @CsvCell(title = "仓库编码", index = 1, fieldNo = 1)
    private String dcCode;
    @CsvCell(title = "仓库名称", index = 2, fieldNo = 1)
    private String dcName;
    /**
     * 客户自编码
     */
    @CsvCell(title = "客户编码", index = 3, fieldNo = 1)
    private String cusSelfCode;
    @CsvCell(title = "客户名称", index = 4, fieldNo = 1)
    private String cusName;
    /**
     * 发生日期
     */
    @CsvCell(title = "发生日期", index = 5, fieldNo = 1)
    private String billDateStr;

    private Date billDate;
    /**
     * 单据类型
     */
    private String matType;
    /**
     * 业务订单号
     */
    private String matPoId;
    /**
     * 业务单据号
     */
    @CsvCell(title = "业务单号", index = 6, fieldNo = 1)
    private String matDnId;
    @CsvCell(title = "抬头备注", index = 9, fieldNo = 1)
    private String remark;

    private int hxStatus;

    /**
     * 单据发生金额
     */
    @CsvCell(title = "发生金额", index = 7, fieldNo = 1)
    private BigDecimal addTotalAmount;
    /**
     * 已核销金额
     */
    @CsvCell(title = "已核销金额", index = 8, fieldNo = 1)
    private BigDecimal hasArAmt;
    private BigDecimal arAmt;

    @CsvCell(title = "销售员", index = 11, fieldNo = 1)
    @ApiModelProperty(value = "销售员编码-销售员")
    private String gssNameStr;


    @CsvCell(title = "开单员", index = 10, fieldNo = 1)
    @ApiModelProperty(value = "开单员编码-开单员名称")
    private String soDnByNameStr;
}
