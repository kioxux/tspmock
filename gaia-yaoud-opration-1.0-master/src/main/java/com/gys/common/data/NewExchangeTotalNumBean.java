package com.gys.common.data;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/12/10 15:06
 */
@Data
public class NewExchangeTotalNumBean {

    private String client;
    private String stoCode;
    private String cardNum;//会员卡号
    private String proId;//商品编码
    private String days;//几天内
    private String voucherId;

    private String startDate;//起始时间--查询用到
}
