package com.gys.common.data;

import lombok.Data;

import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 9:12 2021/8/10
 * @Description：新品铺货无计划报表查询加盟商列表入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class NewProductDistributionNoPlanClientDto {

    private List<String> prov;

    private List<String> city;
}
