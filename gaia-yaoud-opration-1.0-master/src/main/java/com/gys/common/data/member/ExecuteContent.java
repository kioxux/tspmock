package com.gys.common.data.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wu mao yin
 * @date 2021/12/9 13:11
 */
@Data
public class ExecuteContent {

    private Long id;

    @ApiModelProperty("序号")
    private Integer serial;

    @ApiModelProperty("积分")
    private BigDecimal points;

    @ApiModelProperty("现金")
    private BigDecimal cash;

    @ApiModelProperty("比例")
    private BigDecimal proportion;

}