package com.gys.common.data.yaoka.preCalc;

import lombok.Data;

/**
 * @author 胡鑫鑫
 */
@Data
public class PreCalcResp {
    /**
     * 支付流程类型。1-单卡支付流程(ERP调用好医生付款接口，不足部分走药店线下支付)；2-整单支付流程（ERP等待用户选择支付方案）
     */
    private Integer type;
    /**
     * 当type=1时，才有值。这种情况直接返回ready状态的支付方案给ERP，ERP就不需要轮询了
     */
    private PaymentDecisionV2 decision;
}
