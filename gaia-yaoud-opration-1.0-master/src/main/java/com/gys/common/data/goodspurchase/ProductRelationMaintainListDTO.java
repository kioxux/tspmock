package com.gys.common.data.goodspurchase;

import com.gys.common.data.PageDomain;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author wu mao yin
 * @Title: 客户商品关系维护列表入参
 * @Package
 * @date 2021/10/289:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductRelationMaintainListDTO extends PageDomain {

    @ApiModelProperty("客户（加盟商）")
    private String clients;

    @ApiModelProperty("客户地点")
    private String proSites;

    @ApiModelProperty("客户商品自编码")
    private List<String> proSelfCodes;

    @ApiModelProperty("客户通用名称")
    private String proCommonName;

    @ApiModelProperty("药德商品编码")
    private List<String> proCodes;

    @ApiModelProperty("药德通用名称")
    private String ydProCommonName;

    @ApiModelProperty("客药德商品国际条形码1")
    private String ydProBarCode;

    @ApiModelProperty("药德商品批准文号")
    private String ydProRegisterNo;

    @ApiModelProperty("导出字段集合")
    private LinkedList<String> fieldSet;

}
