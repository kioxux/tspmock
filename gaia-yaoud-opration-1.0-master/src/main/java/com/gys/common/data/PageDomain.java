package com.gys.common.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @Title: 分页参数
 * @Package
 * @date 2021/10/289:13
 */
@Data
public class PageDomain implements Serializable {

    @ApiModelProperty("页码")
    private Integer pageNum = 1;

    @ApiModelProperty("每页条数")
    private Integer pageSize = 100;

}
