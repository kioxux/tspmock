package com.gys.common.rabbit.receiver;

import com.gys.common.config.RabbitConfig;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TestReceiver {
    @RabbitListener(queues = RabbitConfig.QUEUE_TEST)
    @RabbitHandler
    public void process(byte[] testMessage) {
        System.out.println("TestReceiver消费者收到消息: " + new String(testMessage));
    }
}
