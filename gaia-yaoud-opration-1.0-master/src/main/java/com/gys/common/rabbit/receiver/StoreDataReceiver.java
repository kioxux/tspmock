package com.gys.common.rabbit.receiver;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaParamDetailMapper;
import com.gys.business.mapper.entity.ParamDetailData;
import com.gys.business.service.PayService;
import com.gys.business.service.data.GetPayInData;
import com.gys.business.service.data.MQSaleOrderInData;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.MQReceiveData;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class StoreDataReceiver {
    @Autowired
    private PayService payService;
    @Autowired
    private GaiaParamDetailMapper paramDetailMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = RabbitConfig.QUEUE_STORE_DATA)
    @RabbitHandler
    public void process(byte[] text, Channel channel, Message message) throws IOException {
        String receiverText = new String(text, StandardCharsets.UTF_8);
        try {
            log.info("StoreDataReceiver: " + receiverText);
            MQSaleOrderInData mqSaleOrderInData = JSON.parseObject(receiverText, MQSaleOrderInData.class);
            if (ObjectUtil.isNotEmpty(mqSaleOrderInData)) {
                //支付结算
                Map<String, Object> resultMap = payService.receiveSaleOrder(mqSaleOrderInData);

                //推送门店同步数据
                MQReceiveData mqReceiveData = new MQReceiveData();
                mqReceiveData.setType("DataSync");
                mqReceiveData.setCmd("receiveSaleOrder");
                mqReceiveData.setData(mqSaleOrderInData.getSaleH().getGsshBillNo());
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, mqSaleOrderInData.getSaleH().getClientId() + "." + mqSaleOrderInData.getSaleH().getGsshBrId(), JSON.toJSON(mqReceiveData));

                if("6".equals(mqSaleOrderInData.getSaleH().getGsshBillType())){ //his门诊单
                    payService.hisPayCallback(mqSaleOrderInData.getSaleH(),mqSaleOrderInData.getSaleDList());
                }
                //物料凭证
                payService.insertMaterialDoc((List<MaterialDocRequestDto>) resultMap.get("materialList"));
                //送电子券
                //查询参数
                Example example = new Example(ParamDetailData.class);
                example.createCriteria().andEqualTo("clientId", mqSaleOrderInData.getClient()).andEqualTo("gsspBrId", mqSaleOrderInData.getBrId()).andEqualTo("gsspId", "SEND_ELEC_AFTER_USE");
                ParamDetailData paramDetailData = (ParamDetailData)this.paramDetailMapper.selectOneByExample(example);
                Example example1 = new Example(ParamDetailData.class);
                example1.createCriteria().andEqualTo("clientId", mqSaleOrderInData.getClient()).andEqualTo("gsspBrId", mqSaleOrderInData.getBrId()).andEqualTo("gsspId", "PAY_IFELEC");
                //支付完成后赠送电子券是否可选  0/NULL 必须赠送  1 可选择
                ParamDetailData payIfelec = (ParamDetailData)this.paramDetailMapper.selectOneByExample(example1);
                //电子券用券后是否可送券；0/null：是；1为否
                if(paramDetailData != null && "1".equals(paramDetailData.getGsspPara())) {
                    if(CollectionUtil.isEmpty(mqSaleOrderInData.getSelectElectronList())){
                        if (payIfelec != null && "1".equals(payIfelec.getGsspPara())) {
                            if (CollectionUtil.isNotEmpty(mqSaleOrderInData.getSendElectronList())) {
                                payService.insertSendElectronList(mqSaleOrderInData.getClient(),mqSaleOrderInData.getBrId(),mqSaleOrderInData.getSaleH().getGsshBillNo(),mqSaleOrderInData.getSendElectronList());
                            }
                        } else {
                            GetPayInData payInData = new GetPayInData();
                            payInData.setClientId(mqSaleOrderInData.getClient());
                            payInData.setStoreCode(mqSaleOrderInData.getBrId());
                            payInData.setBillNo(mqSaleOrderInData.getSaleH().getGsshBillNo());
                            payService.giveElectrons(payInData);
                        }
                    }
                }else{
                    if (payIfelec != null && "1".equals(payIfelec.getGsspPara())) {
                        if (CollectionUtil.isNotEmpty(mqSaleOrderInData.getSendElectronList())) {
                            payService.insertSendElectronList(mqSaleOrderInData.getClient(), mqSaleOrderInData.getBrId(), mqSaleOrderInData.getSaleH().getGsshBillNo(), mqSaleOrderInData.getSendElectronList());
                        }
                    } else {
                        GetPayInData payInData = new GetPayInData();
                        payInData.setClientId(mqSaleOrderInData.getClient());
                        payInData.setStoreCode(mqSaleOrderInData.getBrId());
                        payInData.setBillNo(mqSaleOrderInData.getSaleH().getGsshBillNo());
                        payService.giveElectrons(payInData);
                    }
                }

            } else {
                log.error("消息内容解析失败，推送至失败队列...");
                this.rabbitTemplate.convertAndSend(RabbitConfig.ERROR_QUEUE_STORE_DATA, receiverText);
            }
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            if (message.getMessageProperties().getRedelivered()) {
                log.error("消息已重复处理失败,拒绝再次接收,推送至失败队列...");
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), false); // 拒绝消息
                this.rabbitTemplate.convertAndSend(RabbitConfig.ERROR_QUEUE_STORE_DATA, receiverText);
            } else {
                log.error("消息即将再次返回队列处理...");
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            }
        }
    }
}
