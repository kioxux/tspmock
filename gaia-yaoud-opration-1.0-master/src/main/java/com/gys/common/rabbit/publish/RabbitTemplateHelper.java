package com.gys.common.rabbit.publish;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * @author shentao
 */
@Component
public class RabbitTemplateHelper {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 事务提交后发送MQ
     * @param queue
     * @param message
     * @param <T>
     */
    public <T> void convertAndSend(String queue, T message) {
        // 是否开启事务判断
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void afterCommit() {
                    rabbitTemplate.convertAndSend(queue, message);
                }
            });
        } else {
            rabbitTemplate.convertAndSend(queue, message);
        }
    }

    /**
     * 事务提交后发送MQ
     * @param exchangeTopic 交换机
     * @param routingKey 路由
     * @param message 消息
     * @param <T> 泛型
     */
    public <T> void convertAndSend(String exchangeTopic, String routingKey, T message) {
        // 是否开启事务判断
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void afterCommit() {
                    rabbitTemplate.convertAndSend(exchangeTopic,routingKey, message);
                }
            });
        } else {
            rabbitTemplate.convertAndSend(exchangeTopic,routingKey, message);
        }
    }
}

