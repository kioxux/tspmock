package com.gys.common.request;

import com.gys.common.response.SelectStoreListResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class InsertProductSettingReq {


    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "商品信息")
    private List<InsertProductSetting> settingReqList;

    @Data
    public class InsertProductSetting {


        @ApiModelProperty(value = "加盟商")
        private String client;

        @ApiModelProperty(value = "商品编码")
        private String proId;


        @ApiModelProperty(value = "是否积分0：否，1：是")
        private String ifInte;


        @ApiModelProperty(value = "积分倍率")
        private String inteRate;


        @ApiModelProperty(value = "0：否，1：是")
        private String ifAlltime;


        @ApiModelProperty(value = "起始日期")
        private String beginDate;


        @ApiModelProperty(value = "结束日期")
        private String endDate;


        @ApiModelProperty(value = "星期频率")
        private String[] weekFrequency;

        @ApiModelProperty(value = "星期频率")
        private String weekFrequencyStr;

        @ApiModelProperty(value = "日期频率")
        private String[] dateFrequency;

        @ApiModelProperty(value = "日期频率")
        private String dateFrequencyStr;

        @ApiModelProperty(value = "是否所有门店 0：否，1：是")
        private String ifAllsto;

        @ApiModelProperty(value = "时间拼接")
        private String[] inTimeArr;

        @ApiModelProperty(value = "门店编码集合")
        private String[] sites;

        @ApiModelProperty(value = "门店集合")
        private List<InsertProductStortReq> storeList ;
    }
}
