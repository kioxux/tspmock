package com.gys.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SelectInteProductListReq {

    @ApiModelProperty(value = "当前页码")
    private Integer pageNum = 1;

    @ApiModelProperty(value = "每一页数量")
    private Integer pageSize = 10;

    @ApiModelProperty(value = "商品编码")
    private String[] proIds;

    @ApiModelProperty(value = "是否所有门店 0：否，1：是")
    private String ifAllsto;

    @ApiModelProperty(value = "时间数组")
    private String[] inTimeArr;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "是否一直生效0：否，1：是")
    private String ifAlltime;


    @ApiModelProperty(value = "是否积分0：否，1：是")
    private String ifInte;


    @ApiModelProperty(value = "积分倍率")
    private String inteRate;


    @ApiModelProperty(value = "起始日期")
    private String beginDate;


    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "门店集合")
    private String[] storeList;

}
