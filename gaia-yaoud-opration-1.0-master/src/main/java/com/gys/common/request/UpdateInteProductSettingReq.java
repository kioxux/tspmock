package com.gys.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UpdateInteProductSettingReq {

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "通用名称")
    private String proCommonname;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "生产国家")
    private String proCountry;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "是否一直生效中文名称")
    private String ifAlltimeName;

    @ApiModelProperty(value = "是否积分中文名称")
    private String ifInteName;

    @ApiModelProperty(value = "是否所有门店中文")
    private String ifAllstoName;


    @ApiModelProperty(value = "零售价")
    private String price;


    @ApiModelProperty(value = "是否积分0：否，1：是")
    private String ifInte;


    @ApiModelProperty(value = "加盟商")
    private String client;


    @ApiModelProperty(value = "商品编码")
    private String proId;


    @ApiModelProperty(value = "积分倍率")
    private String inteRate;


    @ApiModelProperty(value = "是否一直生效0：否，1：是")
    private String ifAlltime;


    @ApiModelProperty(value = "起始日期")
    private String beginDate;


    @ApiModelProperty(value = "结束日期")
    private String endDate;


    @ApiModelProperty(value = "星期频率")
    private String[] weekFrequency;
    @ApiModelProperty(value = "星期频率")
    private String weekFrequencyStr;


    @ApiModelProperty(value = "日期频率")
    private String[] dateFrequency;
    @ApiModelProperty(value = "日期频率")
    private String dateFrequencyStr;


    @ApiModelProperty(value = "是否所有门店 0：否，1：是")
    private String ifAllsto;

    @ApiModelProperty(value = "日期数组")
    private String[] inTimeArr;

    @ApiModelProperty(value = "门店编码集合")
    private String[] sites;

    @ApiModelProperty(value = "多个门店集合")
    private List<InsertProductStortReq> productStortList;

}
