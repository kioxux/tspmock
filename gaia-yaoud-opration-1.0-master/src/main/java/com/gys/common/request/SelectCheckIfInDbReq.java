package com.gys.common.request;

import lombok.Data;

import java.util.List;

@Data
public class SelectCheckIfInDbReq {
    List<InsertProductStortReq> settingReqList;
    private String client;
}
