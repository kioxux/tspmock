package com.gys.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class InsertProductStortReq {

    //private String id;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "商品编码")
    private String proId;

    @ApiModelProperty(value = "门店编码")
    private String brId;

    @ApiModelProperty(value = "门店简称")
    private String stoName;
}
