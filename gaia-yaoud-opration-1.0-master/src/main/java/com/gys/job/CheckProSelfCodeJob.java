package com.gys.job;

import com.gys.business.service.AcceptService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/***
 * @desc:
 * @author: XiaoZY
 * @createTime: 2021/7/1 16:53
 **/
@Slf4j
@Component
public class CheckProSelfCodeJob {
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private AcceptService acceptService;

    @XxlJob("checkProSelfCode")
    public ReturnT<String> checkProSelfCode(String param) {
        XxlJobHelper.log("定时任务开始----检查委托平日送未对码提醒----");
        threadPoolTaskExecutor.execute(() -> acceptService.checkProSelfCode());
        XxlJobHelper.log("定时任务结束----检查委托平日送未对码提醒----");
        return ReturnT.SUCCESS;
    }

}
