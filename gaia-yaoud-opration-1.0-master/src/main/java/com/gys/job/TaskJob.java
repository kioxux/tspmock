package com.gys.job;

import com.gys.business.service.*;
import com.gys.business.service.saleTask.SaleTaskSystemService;
import com.gys.business.service.yaoshibang.YaoshibangService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.12
 */
@Component
public class TaskJob {
    @Resource
    private GaiaSdMessageService gaiaSdMessageService;
    @Autowired
    private YaoshibangService yaoshibangService;
    @Resource
    private PercentagePlanV2Service planV2Service;

    @Autowired
    private PercentagePlanV5Service planV5Service;

    @Resource
    private SaleTaskSystemService saleTaskSystemService;
    @Resource
    private ComeAndGoManageService comeAndGoManageService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private IMessageTemplateService messageTemplateService;
    /**
     * 登录消息提醒每日任务更新
     */
    @XxlJob("loginMessageUpdateHandler")
    public ReturnT<String> loginMessageUpdateHandler(String param){
        XxlJobHelper.log("登录消息提醒每日任务更新 定时任务开始 ");
        gaiaSdMessageService.timerUpdateMessage();
        XxlJobHelper.log("登录消息提醒每日任务更新 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 药师帮订单同步
     */
    @XxlJob("yaoshibangOrderSyncHandler")
    public ReturnT<String> yaoshibangOrderSyncHandler(String param) {
        System.err.println("============yaoShiBangNewOrderHandler=============");
        yaoshibangService.orderSync();
        return ReturnT.SUCCESS;
    }

    /**
     * 药师帮药品价格同步
     */
    @XxlJob("yaoshibangPriceSyncHandler")
    public ReturnT<String> yaoshibangPriceSyncHandler(String param) {
        System.err.println("============yaoshibangPriceSyncHandler=============");
        yaoshibangService.priceSync();
        return ReturnT.SUCCESS;
    }


    /**
     * 药师帮药品库存同步
     */
    @XxlJob("yaoshibangStockSyncHandler")
    public ReturnT<String> yaoshibangStockSyncHandler(String param) {
        System.err.println("============yaoshibangStockSyncHandler=============");
        yaoshibangService.stockSync();
        return ReturnT.SUCCESS;
    }

    @XxlJob("timerStopPlanHandler")
    public ReturnT<String> timerStopPlanHandler(String param){
        XxlJobHelper.log("提成方案停用每日任务更新 定时任务开始 ");
        planV5Service.timerStopPlan();
        XxlJobHelper.log("提成方案停用每日任务更新 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    @XxlJob("timerPushSalePlanHandler")
    public ReturnT<String> timerPushSalePlan(String param) {
        XxlJobHelper.log("月度任务定时推送 定时任务开始 ");
        saleTaskSystemService.timerPushSalePlan();
        XxlJobHelper.log("月度任务定时推送 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    @XxlJob("computeComeAndGoRestAmtJobHandler")
    public ReturnT<String> computeComeAndGoRestAmtJobHandler(String param) {
        XxlJobHelper.log("计算往来管理加盟商余额每日任务更新 定时任务开始 ");
        threadPoolTaskExecutor.execute(() -> comeAndGoManageService.computeComeAndGoResAmt());
        XxlJobHelper.log("计算往来管理加盟商余额每日任务更新 定时任务结束 ");
        return ReturnT.SUCCESS;
    }


    @XxlJob("messageTempleEffectTimeJobHandler")
    public ReturnT<String> messageTempleEffectTimeJobHandler() {
        XxlJobHelper.log("消息管理处定时生效处理开始 ");
        messageTemplateService.runEffect();
        XxlJobHelper.log("消息管理处定时生效处理结束");
        return ReturnT.SUCCESS;
    }
}
