package com.gys.job;

import cn.hutool.core.date.DateUtil;
import com.gys.business.service.DiseaseService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Component
@RestController
public class MemberLabelJob {

    @Autowired
    private DiseaseService diseaseService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;


    @XxlJob("memberLabelJob")
    public ReturnT<String> memberLabelJob(String startDate,String endDate,Boolean delete){
        XxlJobHelper.log("<会员卡贴标签开始> {}", DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        threadPoolTaskExecutor.execute(() ->diseaseService.setMemberLabel(startDate,endDate,delete));
        XxlJobHelper.log("<会员卡贴标签结束> {}", DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        return ReturnT.SUCCESS;
    }
}
