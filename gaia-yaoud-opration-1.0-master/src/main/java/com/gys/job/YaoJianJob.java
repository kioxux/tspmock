package com.gys.job;

import com.alibaba.fastjson.JSON;
import com.gys.business.yaojian.service.EIMSService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/23 16:11
 */
@Slf4j
@Component
public class YaoJianJob {
    @Resource
    private EIMSService eimsService;

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @XxlJob("luxianCheckStatusJob")
    public ReturnT<String> checkStatusJob() {
        long startTime = System.currentTimeMillis();
        XxlJobHelper.log("<<定时任务><泸县处理结果查询>> {}",new Date());
        threadPoolTaskExecutor.execute(() -> eimsService.check());
        XxlJobHelper.log("<<定时任务><泸县处理结果查询>> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }

    @XxlJob("luxianBatchInsertInData")
    public ReturnT<String> batchInsertInData() {

        long startTime = System.currentTimeMillis();
        String xxlData = XxlJobHelper.getJobParam();
        String client="";
        Map<String, List<String>> xxlParamMap = new HashMap<>();
        if (StringUtils.isNotEmpty(xxlData)) {
            xxlParamMap = JSON.parseObject(xxlData, (Type) Map.class);
            client=xxlParamMap.get("client").toString();
        }
        if(StringUtils.isNotEmpty(client)){return ReturnT.SUCCESS;}

        XxlJobHelper.log("<<定时任务><批量添加前一天的入库数据>> {}",new Date());
        String finalClient = client;
        threadPoolTaskExecutor.execute(() -> eimsService.batchInsertInData(finalClient));
        XxlJobHelper.log("<<定时任务><批量添加前一天的入库数据>> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }


    @XxlJob("luxianBatchInsertStockData")
    public ReturnT<String> batchInsertStockData() {

        long startTime = System.currentTimeMillis();
        String xxlData = XxlJobHelper.getJobParam();
        String client="";
        Map<String, List<String>> xxlParamMap = new HashMap<>();
        if (StringUtils.isNotEmpty(xxlData)) {
            xxlParamMap = JSON.parseObject(xxlData, (Type) Map.class);
            client=xxlParamMap.get("client").toString();
        }
        if(StringUtils.isNotEmpty(client)){return ReturnT.SUCCESS;}
        XxlJobHelper.log("<<定时任务><批量添加库存数据>> {}",new Date());
        String finalClient = client;
        threadPoolTaskExecutor.execute(() -> eimsService.batchInsertStockData(finalClient));
        XxlJobHelper.log("<<定时任务><批量添加库存数据>> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }


    @XxlJob("luxianBatchUpdateStockData")
    public ReturnT<String> batchUpdateStockData() {

        long startTime = System.currentTimeMillis();
        String xxlData = XxlJobHelper.getJobParam();
        String client="";
        Map<String, List<String>> xxlParamMap = new HashMap<>();
        if (StringUtils.isNotEmpty(xxlData)) {
            xxlParamMap = JSON.parseObject(xxlData, (Type) Map.class);
            client=xxlParamMap.get("client").toString();
        }
        if(StringUtils.isNotEmpty(client)){return ReturnT.SUCCESS;}
        XxlJobHelper.log("<<定时任务><批量更新库存信息，库存数量>> {}",new Date());
        String finalClient = client;
        threadPoolTaskExecutor.execute(() -> eimsService.batchUpdateStockData(finalClient));
        XxlJobHelper.log("<<定时任务><批量更新库存信息，库存数量>> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }


    @XxlJob("luxianBatchInsertOutData")
    public ReturnT<String> batchInsertOutData() {

        long startTime = System.currentTimeMillis();
        String xxlData = XxlJobHelper.getJobParam();
        String client="";
        Map<String, List<String>> xxlParamMap = new HashMap<>();
        if (StringUtils.isNotEmpty(xxlData)) {
            xxlParamMap = JSON.parseObject(xxlData, (Type) Map.class);
            client=xxlParamMap.get("client").toString();
        }
        if(StringUtils.isNotEmpty(client)){return ReturnT.SUCCESS;}
        XxlJobHelper.log("<<定时任务><批量添加前一天的出库数据>> {}",new Date());
        String finalClient = client;
        threadPoolTaskExecutor.execute(() -> eimsService.batchInsertOutData(finalClient));
        XxlJobHelper.log("<<定时任务><批量添加前一天的出库数据>> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }
}
