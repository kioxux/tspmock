package com.gys.job;

import com.gys.business.service.AcceptWareHouseService;
import com.gys.business.service.IGaiaBillOfApService;
import com.gys.business.service.IGaiaFiApBalanceService;
import com.gys.business.service.impl.GaiaBillOfApServiceImpl;
import com.gys.common.data.JsonResult;
import com.gys.common.redis.RedisManager;
import com.gys.util.DateUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;


@Component
public class SupplierBillOfApJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    RedisManager redisManager;
    @Autowired
    private IGaiaBillOfApService gaiaBillOfApService;
    @Autowired
    private IGaiaFiApBalanceService fiApBalanceService;


    /**
     * 功能描述: 每月结算供应商应付期末余额
     *
     * @return com.gys.common.data.JsonResult
     * @author wangQc
     * @date 2021/9/2 3:24 下午
     */
    @XxlJob("supplierApMonthlySettlement")
    public ReturnT<String> supplierApMonthlySettlement() {
        XxlJobHelper.log("=======开始统计上一天的供应商应付定时任务===========");
        //抢占锁
        boolean possession = redisManager.getLock("fi_ap_balance_monthly_settlement_lock", 1);
        if (possession) {
            XxlJobHelper.log("每月结算供应商应付期末余额===执行业务逻辑");
            LocalDate preMonth = DateUtil.getPreMonth(LocalDate.now());
            fiApBalanceService.monthlySettlement(preMonth.minusDays(1));
            XxlJobHelper.log("每月结算供应商应付期末余额===执行结束");
        }
        return ReturnT.SUCCESS;
    }


    /**
     * 功能描述: 统计上一天的供应商应付
     * @param
     * @return com.xxl.job.core.biz.model.ReturnT<java.lang.String>
     * @author wangQc
     * @date 2021/9/2 3:50 下午
     */
    @XxlJob("supplierBillOfApDailyInsert")
    public ReturnT<String> dailyInsert() throws ParseException {
        XxlJobHelper.log("=======开始统计上一天的供应商应付定时任务===========");
        //抢占锁
        boolean possession = redisManager.getLock("bill_of_ap_daily_insert_lock", 1);
        if (possession) {
            XxlJobHelper.log("统计上一天的供应商应付===执行业务逻辑");
            gaiaBillOfApService.dailyInsert(LocalDate.now().minusDays(1));
            XxlJobHelper.log("统计上一天的供应商应付===执行结束");
        }
        return ReturnT.SUCCESS;
    }
    @XxlJob("updateEndingBalance")
    public ReturnT<String> updateEndingBalance() {
        XxlJobHelper.log("=======修改供应商余额===========");
        //抢占锁
        boolean possession = redisManager.getLock("bill_of_ap_daily_insert_lock", 1);
        if (possession) {
            XxlJobHelper.log("修改供应商余额===执行业务逻辑");
            gaiaBillOfApService.updateEndingBalance();
            XxlJobHelper.log("修改供应商余额===执行结束");
        }
        return ReturnT.SUCCESS;
    }

}
