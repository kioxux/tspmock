package com.gys.job;

import com.gys.business.service.GaiaSynchronousAuditStockService;
import com.gys.business.service.NewDistributionPlanService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/***
 * @desc:
 * @author: XiaoZY
 * @createTime: 2021/7/1 16:53
 **/
@Slf4j
@Component
public class AuditStockJob {
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaSynchronousAuditStockService gaiaSynchronousAuditStockService;

    @XxlJob("insertStockInfoJob")
    public ReturnT<String> insertStockInfoJob(String param) {
        log.info("<定时任务><插入库存表对比信息><插入库存表对比信息任务调用成功>");
        threadPoolTaskExecutor.execute(()->gaiaSynchronousAuditStockService.insertStockInfo(null));
        return ReturnT.SUCCESS;
    }

}
