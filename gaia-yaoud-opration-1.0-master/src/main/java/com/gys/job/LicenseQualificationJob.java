package com.gys.job;

import cn.hutool.core.date.DateUtil;
import com.gys.business.service.LicenseQualificationService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
@Slf4j
public class LicenseQualificationJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    private LicenseQualificationService licenseQualificationService;

    @XxlJob("licenseQualificationJob")
    public ReturnT<String> licenseQualificationHandler(String param) {
        XxlJobHelper.log("<每天生成证照资质预警消息定时任务开始> {}", DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        threadPoolTaskExecutor.execute(() -> licenseQualificationService.dailyInsertToMessage());
        return ReturnT.SUCCESS;
    }
}
