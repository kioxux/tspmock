package com.gys.job;

import com.gys.business.service.StockMouthService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StockMonthJob {

    //    @Autowired
//    private StockMouthMapper stockMouthMapper;
//
//    @Autowired
//    private GaiaMaterialDocMapper materialDocMapper;


    @Autowired
    private StockMouthService stockMouthService;

    @XxlJob("stockMouthJob")
    public ReturnT<String> checkDistributionJob(String param) {
        log.info("<定时任务><月度库存><月度库存表推算-每月底24点前最后一分钟执行>");
        stockMouthService.caculateByCron();
        return ReturnT.SUCCESS;
    }

}
