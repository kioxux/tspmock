package com.gys.job;

import com.gys.business.service.GaiaSynchronousAuditStockService;
import com.gys.business.service.RequestOrderAutoCheckService;
import com.gys.business.service.RequestOrderCheckService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/***
 * @desc:委托配送请货自动审核
 **/
@Slf4j
@Component
public class AutoAuditRequestOrderJob {
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private RequestOrderAutoCheckService requestOrderAutoCheckService;

    @XxlJob("autoAuditRequestOrderJob")
    public void autoAuditRequestOrderJob() {
        String jobParam = XxlJobHelper.getJobParam();
        log.info(String.format("<定时任务><委托配送请货自动审核，param=%s>", jobParam));
        threadPoolTaskExecutor.execute(()-> requestOrderAutoCheckService.autoAuditRequestOrder());
        log.info("<定时任务><委托配送请货自动审核><调用结束>");
        XxlJobHelper.handleSuccess();
    }

}
