package com.gys.job;


import com.gys.business.service.GrossConstructionService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:28 2021/9/17
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Slf4j
@Component
public class GrossConstructionJob {

    @Autowired
    private GrossConstructionService grossConstructionService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;


    /**
     * 计算毛利结构
     */
    @XxlJob("grossConstructionJob")
    private void grossConstructionJob(){
        log.info("<定时任务><计算毛利结构><任务调用成功>");
        String xxlData = XxlJobHelper.getJobParam();
        threadPoolTaskExecutor.execute(() -> grossConstructionService.grossConstructionJob(xxlData));
        XxlJobHelper.handleSuccess("调用成功");
    }
}
