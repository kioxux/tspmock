package com.gys.job;

import com.gys.business.service.NewDistributionPlanService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/7 16:53
 **/
@Slf4j
@Component
public class DistributionPlanJob {
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private NewDistributionPlanService newDistributionPlanService;

    @XxlJob("checkDistributionJob")
    public ReturnT<String> checkDistributionJob(String param) {
        log.info("<定时任务><检查铺货><检查铺货差异任务调用成功>");
        threadPoolTaskExecutor.execute(()->newDistributionPlanService.execCheckDistribution());
        return ReturnT.SUCCESS;
    }

    @XxlJob("checkPlanExpire")
    public ReturnT<String> checkPlanExpire(String params) {
        log.info("<定时任务><铺货过期><检查铺货是否过期任务调用成功>");
        threadPoolTaskExecutor.execute(()->newDistributionPlanService.execPlanExpireJob());
        return ReturnT.SUCCESS;
    }
}
