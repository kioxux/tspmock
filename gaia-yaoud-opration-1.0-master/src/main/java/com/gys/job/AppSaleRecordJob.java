package com.gys.job;

import com.gys.business.service.AppSaleReportService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Date;

/***
 * @desc:
 * @author: SunJiaNan
 * @createTime: 2021/8/30 16:53
 **/
@Slf4j
@Component
public class AppSaleRecordJob {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    private AppSaleReportService appSaleReportService;

    /**
     * app销售报表定时任务
     */
    @XxlJob("batchInsertLastMonthSaleData")
    public ReturnT<String> batchInsertLastMonthSaleData() {
        long startTime = System.currentTimeMillis();
        XxlJobHelper.log("<app销售报表定时任务开始> {}",new Date());
        log.info("<app销售报表定时任务开始> {}",new Date());
        threadPoolTaskExecutor.execute(() -> appSaleReportService.initSaleData());
        log.info("<app销售报表定时任务结束> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        XxlJobHelper.log("<app销售报表定时任务结束> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }
}
