package com.gys.job;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.GAIAYaoshibangStoreService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 药师帮job
 * @date 2021/12/22 14:46
 */
@Slf4j
@Component
public class YaoshibangJob {
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GAIAYaoshibangStoreService gaiaYaoshibangStoreService;
    @XxlJob("yaoshibangStoreSynch")
    public ReturnT<String> yaoshibangStoreSynch(String param) {
        long startTime = System.currentTimeMillis();
        XxlJobHelper.log("<药师帮><同步门店job开始> {}",new Date());
        String xxlData = XxlJobHelper.getJobParam();
        Integer isAll=0;
        Map<String, List<String>> xxlParamMap = new HashMap<>();
        if (StringUtils.isNotEmpty(xxlData)) {
            xxlParamMap = JSON.parseObject(xxlData, (Type) Map.class);
            isAll=Integer.valueOf(xxlParamMap.get("isAll").toString());
        }
        Integer finalIsAll = isAll;
        threadPoolTaskExecutor.execute(() -> gaiaYaoshibangStoreService.synchYaoshibangStore(finalIsAll));

        XxlJobHelper.log("<药师帮><同步门店job结束> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }
}
