package com.gys.job;

import com.gys.business.service.GaiaCommodityInventoryHService;
import com.gys.business.service.impl.GaiaEntNewProductEvaluationHServiceImpl;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务，每周一定时检查生成商品评估单
 *
 * @author: XiaoZY
 * @date: 2021.06.08
 */
@Component
public class InsertInventoryJob {
    @Resource
    private GaiaCommodityInventoryHService gaiaCommodityInventoryHService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    /**
     * 登录消息提醒每日任务更新
     */
    @XxlJob("insertInventoryBill")
    public ReturnT<String> insertEntProductData(String param){
        XxlJobHelper.log("定时任务开始----调库单生成----");
        threadPoolTaskExecutor.execute(() -> gaiaCommodityInventoryHService.insertInventoryData(param));
        XxlJobHelper.log("定时任务结束----调库单生成----");
        return ReturnT.SUCCESS;
    }

    @XxlJob("updateInventoryBill")
    public ReturnT<String> updateEntProductData(String param){
        XxlJobHelper.log("定时任务开始----更新调库单状态----");
        threadPoolTaskExecutor.execute(() -> gaiaCommodityInventoryHService.updateInventoryData(param));
        XxlJobHelper.log("定时任务结束----更新调库单状态----");
        return ReturnT.SUCCESS;
    }
}
