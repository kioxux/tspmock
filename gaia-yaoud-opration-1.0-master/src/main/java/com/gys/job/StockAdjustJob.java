package com.gys.job;

import com.gys.business.mapper.GaiaCommodityInventoryHMapper;
import com.gys.business.service.GaiaCommodityInventoryHService;
import com.gys.business.service.GaiaStoreOutSuggestionHService;
import com.gys.business.service.StockMouthService;
import com.gys.business.service.impl.GaiaStoreOutSuggestionHServiceImpl;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务，门店调剂修改状态
 *
 * @author: XiaoZY
 * @date: 2021.11.04
 */
@Slf4j
@Component
public class StockAdjustJob {

    @Resource
    private GaiaStoreOutSuggestionHService gaiaStoreOutSuggestionHService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;


    @XxlJob("stockAdjustJob")
    public ReturnT<String> checkDistributionJob(String param) {
        log.info("<定时任务><门店调剂><每天晚上 1 点执行>");
        XxlJobHelper.log("定时任务开始----商品评估单据生成----");
        threadPoolTaskExecutor.execute(() -> gaiaStoreOutSuggestionHService.validBill());
        XxlJobHelper.log("定时任务结束----商品评估单据生成----");
        return ReturnT.SUCCESS;
    }

}
