package com.gys.job;

import com.gys.business.service.impl.GaiaEntNewProductEvaluationHServiceImpl;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务，每周一定时检查生成商品评估单
 *
 * @author: XiaoZY
 * @date: 2021.06.08
 */
@Component
public class InsertEntNewProductEvaluationJob {
    @Resource
    private GaiaEntNewProductEvaluationHServiceImpl gaiaEntNewProductEvaluationHService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    /**
     * 登录消息提醒每日任务更新
     */
    @XxlJob("insertEntProductData")
    public ReturnT<String> insertEntProductData(String param){
        XxlJobHelper.log("定时任务开始----公司级商品评估单据生成----");
        threadPoolTaskExecutor.execute(() -> gaiaEntNewProductEvaluationHService.insertEntProductData(param));
        XxlJobHelper.log("定时任务结束----门店级商品评估单据生成----");
        return ReturnT.SUCCESS;
    }

    @XxlJob("updateEntProductData")
    public ReturnT<String> updateEntProductData(String param){
        XxlJobHelper.log("定时任务开始----公司级商品评估单据状态编辑----");
        threadPoolTaskExecutor.execute(() -> gaiaEntNewProductEvaluationHService.updateEntProductData());
        XxlJobHelper.log("定时任务结束----公司级商品评估单据状态编辑----");
        return ReturnT.SUCCESS;
    }
}
