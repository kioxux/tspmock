package com.gys.job;

import com.gys.business.service.GaiaNewProductEvaluationHService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 定时任务，每周一定时检查生成商品评估单
 *
 * @author: XiaoZY
 * @date: 2021.06.08
 */
@Component
public class InsertGaiaNewProductEvaluationJob {
    @Resource
    private GaiaNewProductEvaluationHService gaiaNewProductEvaluationHService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    /**
     * 登录消息提醒每日任务更新
     */
    @XxlJob("insertGaiaNewProductEvaluationHandler")
    public ReturnT<String> loginMessageUpdateHandler(String param){
        XxlJobHelper.log("定时任务开始----商品评估单据生成----");
        threadPoolTaskExecutor.execute(() -> gaiaNewProductEvaluationHService.InsertGaiaNewProductEvaluation());
        XxlJobHelper.log("定时任务结束----商品评估单据生成----");
        return ReturnT.SUCCESS;
    }

    @XxlJob("updateBillStatus")
    public ReturnT<String> updateBillStatus(String param){
        XxlJobHelper.log("定时任务开始----商品评估单据状态编辑----");
        threadPoolTaskExecutor.execute(() -> gaiaNewProductEvaluationHService.updateBillStatus());
        XxlJobHelper.log("定时任务结束----商品评估单据状态编辑----");
        return ReturnT.SUCCESS;
    }
}
