package com.gys.job;

import cn.hutool.core.date.DateUtil;
import com.gys.business.service.IMemberActiveService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author ：gyx
 * @Date ：Created in 13:54 2021/11/19
 * @Description：会员活跃度表维护job
 * @Modified By：gyx
 * @Version:
 */
@Component
@Slf4j
public class MemberActiveJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    private IMemberActiveService memberActiveService;

    @XxlJob("memberActiveJobHandle")
    public ReturnT<String> memberActiveJobHandle(String param) {
        XxlJobHelper.log("<每天维护会员活跃度表定时任务开始> {}", DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        threadPoolTaskExecutor.execute(() -> memberActiveService.dailyMaintenMemberActive(param));
        return ReturnT.SUCCESS;
    }
}
