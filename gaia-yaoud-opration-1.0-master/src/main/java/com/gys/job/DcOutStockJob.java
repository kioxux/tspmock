package com.gys.job;

import com.gys.business.service.DcOutStockJobService;
import com.xxl.job.core.context.XxlJobContext;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @desc: 公司级缺断货统计任务
 * @author: Ryan
 * @createTime: 2021/8/22 17:12
 */
@Slf4j
@Component
public class DcOutStockJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private DcOutStockJobService dcOutStockJobService;

    @XxlJob("generateOutStockData")
    public void generateOutStockData() {
        log.info("<定时任务><公司级缺断货><任务调用成功>");
        threadPoolTaskExecutor.execute(() -> dcOutStockJobService.execJob());
        XxlJobHelper.handleResult(XxlJobContext.HANDLE_COCE_SUCCESS, "调用成功");
    }
}
