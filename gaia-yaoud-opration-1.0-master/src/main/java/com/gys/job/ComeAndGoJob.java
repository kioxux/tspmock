package com.gys.job;

import cn.hutool.core.date.DateUtil;
import com.gys.business.service.ComeAndGoManageService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @desc:
 * @author: guoyuxi
 * @createTime: 2021/8/25 13:38
 */
@Slf4j
@Component
public class ComeAndGoJob {

    @Resource
    private ComeAndGoManageService comeAndGoManageService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @XxlJob("comeAndGoJob")
    public ReturnT<String> ComeAndGoJobHandler(String param) {
        XxlJobHelper.log("<自动计算仓库往来数据定时任务开始> {}", DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        threadPoolTaskExecutor.execute(() -> comeAndGoManageService.autoComputeDistributionDataAndAddComeAndGoDetail());
        return ReturnT.SUCCESS;
    }
}
