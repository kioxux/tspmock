package com.gys.job;

import com.gys.business.service.AcceptWareHouseService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 9:45 2021/8/17
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Component
public class GuoYaoJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private AcceptWareHouseService acceptWareHouseService;

    @XxlJob("guoYaoOrder")
    public ReturnT<String> computeComeAndGoRestAmtJobHandler(String param) {
        long startTime = System.currentTimeMillis();
        XxlJobHelper.log("<国药冷链定时任务开始> {}",new Date());
        threadPoolTaskExecutor.execute(() -> acceptWareHouseService.manageGuoYaoOrder());

        XxlJobHelper.log("<国药冷链定时任务结束> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }

    @XxlJob("returningToSupplier")
    public ReturnT<String> returningToSupplierJobHandler(String param) {
        long startTime = System.currentTimeMillis();
        XxlJobHelper.log("<国药冷链定时任务开始> {}",new Date());
        threadPoolTaskExecutor.execute(() -> acceptWareHouseService.insertMessageForReturning());

        XxlJobHelper.log("<国药冷链定时任务结束> {} <耗时> {}",new Date(),System.currentTimeMillis() - startTime);
        return ReturnT.SUCCESS;
    }
}
