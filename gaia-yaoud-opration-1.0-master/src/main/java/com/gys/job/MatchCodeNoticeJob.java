package com.gys.job;

import com.gys.business.service.DsfPordWtproService;
import com.gys.business.service.RequestOrderAutoCheckService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/***
 * @desc:委托配送未对码提醒
 **/
@Slf4j
@Component
public class MatchCodeNoticeJob {
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private DsfPordWtproService dsfPordWtproService;

    @XxlJob("autoMatchCodeNotice")
    public void autoMatchCodeNoticeJob() {
        String jobParam = XxlJobHelper.getJobParam();
        log.info(String.format("<定时任务><委托配送商品未对码提醒，param=%s>", jobParam));
        threadPoolTaskExecutor.execute(()-> dsfPordWtproService.autoMatchCodeNotice());
        log.info("<定时任务><委托配送商品未对码提醒><调用结束>");
        XxlJobHelper.handleSuccess();
    }
}
