package com.gys.job;


import cn.hutool.core.date.DateUtil;
import com.gys.business.service.IStoreMaintenService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author ：gyx
 * @Date ：Created in 9:39 2021/11/5
 * @Description：门店养护每日消息提醒
 * @Modified By：gyx
 * @Version:
 */
@Component
@Slf4j
public class StoreMaintenJob {


    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    private IStoreMaintenService storeMaintenService;

    @XxlJob("storeMaintenJob")
    public ReturnT<String> storeMaintenJobHandler(String param) {
        XxlJobHelper.log("<每天生成门店养护消息定时任务开始> {}", DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        threadPoolTaskExecutor.execute(() -> storeMaintenService.dailyInsertStoreMaintenMessageJob());
        return ReturnT.SUCCESS;
    }
}
