package com.gys.business.cashbox.form;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:54
 */
@Data
public class AddCashBoxForm {
    private String client;
    private String userName;
    /**
     * 门店编码
     */
    @NotEmpty(message = "请选择门店")
    private List<String> storeIdList;
    /**
     * 机台编号
     */
    @NotEmpty(message = "机台编号不能为空")
    private String machineNo;
    /**
     * 初始备用金
     */
    @Min(value = 0, message = "初始备用金不能小于0")
    private BigDecimal initAmount;
}
