package com.gys.business.cashbox.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 12:33
 */
@Data
public class CashBoxVO {
    private String client;
    private String storeId;
    private String storeName;
    private String machineNo;
    private String machineName;
    private BigDecimal initAmount;
    private BigDecimal lastAmount;
    /**
     * 上次调整金额：需要携带正负号
     */
    private String changeAmount;
    private BigDecimal currentAmount;
    private String remark;
    private String createUser;
    private String createTime;
}
