package com.gys.business.cashbox.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 16:08
 */
@Data
public class CashBoxDetailVO {
    private String storeId;
    private String machineNo;
    private Integer type;
    private BigDecimal lastAmount;
    private BigDecimal changeAmount;
    private BigDecimal currentAmount;
    private BigDecimal initAmount;
    private String remark;
    private String createTime;
    private String createUser;
}
