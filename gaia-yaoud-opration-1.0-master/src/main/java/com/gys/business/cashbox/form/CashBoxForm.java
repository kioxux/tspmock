package com.gys.business.cashbox.form;

import lombok.Data;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 12:24
 */
@Data
public class CashBoxForm {
    private String client;
    private String storeId;
    private List<String> storeIdList;
    private String machineNo;
    private Integer pageNum;
    private Integer pageSize;
    private String startDate;
    private String endDate;
    private String operator;
}
