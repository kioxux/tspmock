package com.gys.business.cashbox.service;

import com.gys.business.cashbox.form.AddCashBoxForm;
import com.gys.business.cashbox.form.CashBoxForm;
import com.gys.business.cashbox.form.ChangeCashBoxForm;
import com.gys.business.cashbox.vo.CashBoxDetailVO;
import com.gys.business.cashbox.vo.CashBoxVO;
import com.gys.common.data.PageInfo;

import java.util.Map;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:06
 */
public interface CashBoxService {

    Map<String, Object> addCashBox(AddCashBoxForm addCashBoxForm);

    PageInfo<CashBoxVO> cashBoxList(CashBoxForm cashBoxForm);

    void batchChange(ChangeCashBoxForm changeCashBoxForm);

    PageInfo<CashBoxDetailVO> changeRecord(CashBoxForm cashBoxForm);
}
