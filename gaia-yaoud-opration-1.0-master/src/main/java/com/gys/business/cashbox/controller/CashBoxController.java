package com.gys.business.cashbox.controller;

import com.gys.business.cashbox.form.AddCashBoxForm;
import com.gys.business.cashbox.form.CashBoxForm;
import com.gys.business.cashbox.form.ChangeCashBoxForm;
import com.gys.business.cashbox.service.CashBoxService;
import com.gys.business.cashbox.vo.CashBoxVO;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.util.JsonUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

/**
 * @desc: 钱箱功能控制类
 * @author: Ryan
 * @createTime: 2022/1/27 02:03
 */
@RestController
@RequestMapping("cashBox")
public class CashBoxController extends BaseController {
    @Resource
    private CashBoxService cashBoxService;

    @PostMapping("add")
    public JsonResult add(HttpServletRequest request, @RequestBody @Valid AddCashBoxForm addCashBoxForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        addCashBoxForm.setClient(loginUser.getClient());
        addCashBoxForm.setUserName(String.format("%s-%s", loginUser.getLoginName(), loginUser.getUserId()));
        Map<String, Object> result = cashBoxService.addCashBox(addCashBoxForm);
        if (result != null && result.size() > 0) {
            String message = "%s，机台号：%s初始备用金已设置。";
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, Object> entry : result.entrySet()) {
                sb.append(entry.getKey()).append("-").append(entry.getValue()).append("、");
            }
            message = String.format(message, sb.substring(0, sb.length() - 1), addCashBoxForm.getMachineNo());
            return JsonResult.error(1001, message);
        }
        return JsonResult.success();
    }

    @PostMapping("list")
    public JsonResult list(HttpServletRequest request, @RequestBody CashBoxForm cashBoxForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        cashBoxForm.setClient(loginUser.getClient());
        if (cashBoxForm.getPageNum() == null) {
            cashBoxForm.setPageNum(1);
        }
        if (cashBoxForm.getPageSize() == null) {
            cashBoxForm.setPageSize(10);
        }
        PageInfo<CashBoxVO> page = cashBoxService.cashBoxList(cashBoxForm);
        return JsonResult.success(page);
    }

    @PostMapping("batchChange")
    public JsonResult batchChange(HttpServletRequest request, @RequestBody @Valid ChangeCashBoxForm changeCashBoxForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        changeCashBoxForm.setClient(loginUser.getClient());
        changeCashBoxForm.setUserName(String.format("%s-%s", loginUser.getLoginName(), loginUser.getUserId()));
        cashBoxService.batchChange(changeCashBoxForm);
        return JsonResult.success();
    }

    @PostMapping("changeRecord")
    public JsonResult changeRecord(HttpServletRequest request, @RequestBody CashBoxForm cashBoxForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        cashBoxForm.setClient(loginUser.getClient());
        if (cashBoxForm.getPageSize() == null) {
            cashBoxForm.setPageSize(10);
        }
        if (cashBoxForm.getPageNum() == null) {
            cashBoxForm.setPageNum(1);
        }
        PageInfo pageInfo = cashBoxService.changeRecord(cashBoxForm);
        return JsonResult.success(pageInfo);
    }

}
