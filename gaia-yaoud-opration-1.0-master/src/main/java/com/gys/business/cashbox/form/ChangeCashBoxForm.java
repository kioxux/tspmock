package com.gys.business.cashbox.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 14:34
 */
@Data
public class ChangeCashBoxForm {
    private String client;
    private Integer type;
    @NotEmpty(message = "调整备注不能为空")
    private String remark;
    private BigDecimal amount;
    private List<MachineForm> machineList;
    private String userName;

    @Data
    public static class MachineForm {
        private String storeId;
        private String machineNo;
    }
}
