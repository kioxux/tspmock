package com.gys.business.cashbox.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.cashbox.form.AddCashBoxForm;
import com.gys.business.cashbox.form.CashBoxForm;
import com.gys.business.cashbox.form.ChangeCashBoxForm;
import com.gys.business.cashbox.service.CashBoxService;
import com.gys.business.cashbox.vo.CashBoxDetailVO;
import com.gys.business.cashbox.vo.CashBoxVO;
import com.gys.business.mapper.CashBoxDetailMapper;
import com.gys.business.mapper.CashBoxMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.CashBox;
import com.gys.business.mapper.entity.CashBoxDetail;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.common.data.PageInfo;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:06
 */
@Slf4j
@Service("cashBoxService")
public class CashBoxServiceImpl implements CashBoxService {
    @Resource
    private CashBoxMapper cashBoxMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private CashBoxDetailMapper cashBoxDetailMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> addCashBox(AddCashBoxForm addCashBoxForm) {
        Map<String, Object> result = new HashMap<>();
        List<GaiaStoreData> storeList = gaiaStoreDataMapper.findList(addCashBoxForm.getClient());
        for (String storeId : addCashBoxForm.getStoreIdList()) {
            List<GaiaStoreData> matchStoreList = storeList.stream().filter(store -> store.getStoCode().equals(storeId)).collect(Collectors.toList());
            if (matchStoreList.size() == 0) {
                log.warn("<钱箱模块><新增钱箱><门店不存在：{}>", storeId);
                continue;
            }
            GaiaStoreData store = matchStoreList.get(0);
            CashBox cashBox = transformCashBox(addCashBoxForm, store);
            //校验机台编号是否存在
            CashBox cond = new CashBox();
            cond.setClient(addCashBoxForm.getClient());
            cond.setStoreId(storeId);
            cond.setMachineNo(addCashBoxForm.getMachineNo());
            CashBox unique = cashBoxMapper.getUnique(cond);
            if (unique != null) {
                result.put(storeId, StringUtil.isNotEmpty(store.getStoShortName()) ? store.getStoShortName() : store.getStoName());
                continue;
            }

            cashBoxMapper.add(cashBox);
        }
        return result;
    }

    @Override
    public PageInfo<CashBoxVO> cashBoxList(CashBoxForm cashBoxForm) {
        PageHelper.startPage(cashBoxForm);
        List<CashBox> cashBoxList = cashBoxMapper.findList(cashBoxForm);
        List<CashBoxVO> list = new ArrayList<>();
        fillCashBoxVo(list, cashBoxList);
        return new PageInfo<>(list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchChange(ChangeCashBoxForm changeCashBoxForm) {
        for (ChangeCashBoxForm.MachineForm machineForm : changeCashBoxForm.getMachineList()) {
            CashBox cond = new CashBox();
            cond.setClient(changeCashBoxForm.getClient());
            cond.setStoreId(machineForm.getStoreId());
            cond.setMachineNo(machineForm.getMachineNo());
            CashBox cashBox = cashBoxMapper.getUnique(cond);
            if (cashBox == null) {
                continue;
            }
            updateCashBoxAndChangeRecord(cashBox, changeCashBoxForm);
        }
    }

    @Override
    public PageInfo<CashBoxDetailVO> changeRecord(CashBoxForm cashBoxForm) {
        PageHelper.startPage(cashBoxForm);
        List<CashBoxDetailVO> list = cashBoxDetailMapper.getChangeRecord(cashBoxForm);
        return new PageInfo<>(list);
    }

    private void updateCashBoxAndChangeRecord(CashBox cashBox, ChangeCashBoxForm changeCashBoxForm) {
        //保存调整记录
        BigDecimal lastAmount = cashBox.getCurrentAmount();
        BigDecimal currentAmount = changeCashBoxForm.getType() == 1 ? cashBox.getCurrentAmount().add(changeCashBoxForm.getAmount())
                : cashBox.getCurrentAmount().subtract(changeCashBoxForm.getAmount());
        if (BigDecimal.ZERO.compareTo(currentAmount) > 0) {
            throw new BusinessException(String.format("门店：%s，机台编号：%s备用金不能小于0", cashBox.getStoreName(), cashBox.getMachineNo()));
        }
        CashBoxDetail cashBoxDetail = new CashBoxDetail();
        cashBoxDetail.setClient(cashBox.getClient());
        cashBoxDetail.setStoreId(cashBox.getStoreId());
        cashBoxDetail.setMachineNo(cashBox.getMachineNo());
        cashBoxDetail.setType(changeCashBoxForm.getType());
        cashBoxDetail.setLastAmount(cashBox.getCurrentAmount());
        cashBoxDetail.setChangeAmount(changeCashBoxForm.getAmount());
        cashBoxDetail.setCurrentAmount(currentAmount);
        cashBoxDetail.setCreateUser(changeCashBoxForm.getUserName());
        cashBoxDetail.setRemark(changeCashBoxForm.getRemark());
        cashBoxDetailMapper.add(cashBoxDetail);
        //更新机台备用金
        cashBox.setLastAmount(lastAmount);
        BigDecimal changeAmount = changeCashBoxForm.getType() == 1 ? changeCashBoxForm.getAmount() : changeCashBoxForm.getAmount().negate();
        cashBox.setChangeAmount(changeAmount);
        cashBox.setCurrentAmount(currentAmount);
        cashBox.setUpdateTime(new Date());
        cashBox.setUpdateUser(changeCashBoxForm.getUserName());
        cashBox.setRemark(cashBoxDetail.getRemark());
        cashBoxMapper.update(cashBox);
    }

    private void fillCashBoxVo(List<CashBoxVO> list, List<CashBox> cashBoxList) {
        if (cashBoxList != null && cashBoxList.size() > 0) {
            for (CashBox cashBox : cashBoxList) {
                CashBoxVO cashBoxVO = new CashBoxVO();
                BeanUtils.copyProperties(cashBox, cashBoxVO);
                //上次调整金额
                if (BigDecimal.ZERO.compareTo(cashBox.getChangeAmount()) < 0) {
                    cashBoxVO.setChangeAmount(String.format("+%s", cashBox.getChangeAmount()));
                } else {
                    cashBoxVO.setChangeAmount(cashBox.getChangeAmount().toString());
                }
                if (StringUtil.isNotEmpty(cashBox.getUpdateUser())) {
                    cashBoxVO.setCreateUser(cashBox.getUpdateUser());
                }
                if (cashBox.getUpdateTime().after(cashBox.getCreateTime())) {
                    cashBoxVO.setCreateTime(DateUtil.format(cashBox.getUpdateTime(), "yyyy-MM-dd HH:mm:ss"));
                } else {
                    cashBoxVO.setCreateTime(DateUtil.format(cashBox.getCreateTime(),"yyyy-MM-dd HH:mm:ss"));
                }
                list.add(cashBoxVO);
            }
        }
    }

    private CashBox transformCashBox(AddCashBoxForm addCashBoxForm, GaiaStoreData gaiaStoreData) {
        CashBox cashBox = new CashBox();
        BeanUtils.copyProperties(addCashBoxForm, cashBox);
        cashBox.setCurrentAmount(cashBox.getInitAmount());
        cashBox.setCreateUser(addCashBoxForm.getUserName());
        cashBox.setRemark("初始备用金");
        cashBox.setStoreId(gaiaStoreData.getStoCode());
        cashBox.setStoreName(StringUtil.isNotEmpty(gaiaStoreData.getStoShortName()) ? gaiaStoreData.getStoShortName() : gaiaStoreData.getStoName());
        return cashBox;
    }
}
