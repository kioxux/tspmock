package com.gys.business.bosen.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/22 23:39
 */
@Data
public class DownloadDataDTO {
    /**
     * 库存数据
     * */
    private Map<String, List<BsStockData>> stockMap;

    /**
     * 入库数据
     */
    private Map<String,List<BsInData>> inDataMap;

    /**
     * 出库数据
     */
    private Map<String,List<BsInData>> outDataMap;
}
