package com.gys.business.bosen.dto;

import lombok.Data;

import java.io.File;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 09:18
 */
@Data
public class BosenFile {
    /**文件名称*/
    private String fileName;
    /**文件实体*/
    private File file;
}
