package com.gys.business.bosen.dto;

import com.google.common.collect.Lists;
import com.gys.util.XmlUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 13:14
 */
@Data
@XStreamAlias("SIGNATURE")
public class BaseXmlData {

    @XStreamAsAttribute
    private String xmlns = "www.hebfdea.com";
    @XStreamAlias("OBJECT")
    private DataObject xmlData;

    public BaseXmlData(DataObject xmlData) {
        this.xmlData = xmlData;
    }

    public static void main(String[] args) throws IOException {
        BaseXmlData baseXmlData = new BaseXmlData(null);
        InitXmlData initXmlData = new InitXmlData();
        initXmlData.setEnvelopInfo(new EnvelopInfo());
        initXmlData.setDataInfoList(Lists.newArrayList(new InitDataInfo()));
        XStream underlineXStream = XmlUtils.underline_xStream;
        underlineXStream.autodetectAnnotations(true);
        File file = File.createTempFile("stock", ".xml");
        FileOutputStream fout = new FileOutputStream(file);
        DataObject dataObject = new DataObject(initXmlData);
        baseXmlData.setXmlData(dataObject);
        System.out.println(underlineXStream.toXML(baseXmlData));
        underlineXStream.toXML(baseXmlData, fout);
        System.out.println(fout);
        file.delete();
    }
}
