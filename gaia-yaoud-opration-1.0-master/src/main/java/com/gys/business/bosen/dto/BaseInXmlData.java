package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/24 00:04
 */
@Data
@XStreamAlias("SIGNATURE")
public class BaseInXmlData {
    @XStreamAsAttribute
    private String xmlns = "www.hebfdea.com";
    @XStreamAlias("OBJECT")
    private InDataObject xmlData;

    public BaseInXmlData(InDataObject xmlData) {
        this.xmlData = xmlData;
    }
}
