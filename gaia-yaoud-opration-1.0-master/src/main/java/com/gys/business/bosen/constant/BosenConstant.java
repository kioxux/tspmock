package com.gys.business.bosen.constant;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/22 18:39
 */
public class BosenConstant {

    /**库存文件名称模板*/
    public static final String FILE_INIT_TEMPLATE = "k_init_%s_%s#1.xml";
    /**入库文件名称模板*/
    public static final String FILE_IN_TEMPLATE = "k_in_%s_%s#1.xml";
    /**出库文件名称模板*/
    public static final String FILE_OUT_TEMPLATE = "k_out_%s_%s#1.xml";

    /**默认类型：1药品*/
    public static final String DEFAULT_SOFT_TYPE = "1";
    /**默认接收者：1300000000-河北药监局*/
    public static final String DEFAULT_RECEIVER_ID = "1300000000";
    /**默认版本*/
    public static final String DEFAULT_VERSION = "v1.0";

    /**博森门店映射关系键值*/
    public static final String KEY_BS_STORE = "key:bs:store:%s";

    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

    public static final String BS_CLIENT = "10000058";


}
