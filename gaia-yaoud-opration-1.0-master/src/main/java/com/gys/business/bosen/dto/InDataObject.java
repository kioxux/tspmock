package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 13:27
 */
@Data
@XStreamAlias("OBJECT")
public class InDataObject {
    @XStreamAlias("PACKAGE")
    private InXmlData data;

    public InDataObject(InXmlData data) {
        this.data = data;
    }
}
