package com.gys.business.bosen.constant;

/**
 * @desc: 河北药监上传文件消息类型
 * @author: Ryan
 * @createTime: 2021/9/23 09:21
 */
public enum MessageTypeEnum {

    IN("in", "入库"),
    OUT("out", "出库"),
    INIT("init", "库存");

    public final String type;
    public final String name;

    MessageTypeEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }
}
