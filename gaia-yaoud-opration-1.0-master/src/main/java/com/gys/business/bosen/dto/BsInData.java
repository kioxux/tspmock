package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 20:40
 */
@Data
@XStreamAlias("ROWINFO")
public class BsInData {
    @XStreamOmitField
    private String proSite;
    @XStreamOmitField
    private String proSelfCode;
    @XStreamOmitField
    private String client;
    @XStreamOmitField
    private String gssdDate;
    @XStreamAlias("I_ITEM_CODE")
    private String centerCode;
    @XStreamAlias("YPPH")
    private String batchNo;
    @XStreamAlias("VDATE")
    private String validDate;
    @XStreamAlias("PACKAGE")
    private String proSpecs;
    @XStreamAlias("UNIT")
    private String proUnit;
    @XStreamAlias("NUM")
    private BigDecimal num;
    @XStreamAlias("JZ_DATE")
    private String jzDate;
    @XStreamAlias("FPH")
    private String fph;
    @XStreamAlias("I_MER_CODE")
    private String supCreditCode;
    @XStreamAlias("MER_NAME")
    private String supName;
    @XStreamAlias("MERA_CODE")
    private String areaCode;
    @XStreamAlias("BILL_TYPE")
    private String billType;
}
