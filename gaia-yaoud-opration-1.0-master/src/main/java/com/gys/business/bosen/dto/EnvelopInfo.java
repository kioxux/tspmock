package com.gys.business.bosen.dto;

import com.gys.business.bosen.constant.BosenConstant;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 00:07
 */
@Data
public class EnvelopInfo {
    @XStreamAlias("VERSION")
    private String version;
    @XStreamAlias("MESSAGE_ID")
    private String messageId;
    @XStreamAlias("FILE_NAME")
    private String fileName;
    @XStreamAlias("MESSAGE_TYPE")
    private String messageType;
    @XStreamAlias("SENDER_ID")
    private String senderId;
    @XStreamAlias("SEND_TIME")
    private String sendTime;
    @XStreamAlias("RECEIVER_ID")
    private String receiverId;
    @XStreamAlias("SOFTTYPE")
    private String softType;
    @XStreamAlias("SENDERNAME")
    private String senderName;

    public EnvelopInfo() {
        this.version = BosenConstant.DEFAULT_VERSION;
        this.messageId = UUID.randomUUID().toString();
        this.sendTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.receiverId = BosenConstant.DEFAULT_RECEIVER_ID;
        this.softType = BosenConstant.DEFAULT_SOFT_TYPE;
    }
}
