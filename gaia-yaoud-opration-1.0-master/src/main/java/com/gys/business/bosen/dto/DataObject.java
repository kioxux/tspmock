package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 13:27
 */
@Data
@XStreamAlias("OBJECT")
public class DataObject {
    @XStreamAlias("PACKAGE")
    private InitXmlData data;

    public DataObject(InitXmlData data) {
        this.data = data;
    }
}
