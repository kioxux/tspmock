package com.gys.business.bosen.constant;

/**
 * @desc: 河北药监加盟商账号枚举类
 * @author: Ryan
 * @createTime: 2021/11/3 16:37
 */
public enum HBYJEnum {

    /**
     * 新接入河北药监程序的加盟商需要先完成：1-商品对码（pro_zdy3） 2-门店对码(wtsto) 3-供应商区域对码(area_code)
     */
    BS("10000058", "", "河北博森"),
    XA("10000088", "F92D7691FD05466CB92CE47A1EA9C28A", "河北雄安"),
    WST("10000351", "DA8A54AA33D842899FA682511BCD1211", "新疆万盛堂"),
    XJBZYK("10000378", "782F9C8B0EF24D858752E77FC98C2CA7", "新疆博州永康药业有限公司"),;

    public final String appId;
    public final String appKey;
    public final String message;

    HBYJEnum(String appId, String appKey, String message) {
        this.message = message;
        this.appId = appId;
        this.appKey = appKey;
    }

    public static HBYJEnum getEnum(String appId) {
        for (HBYJEnum value : HBYJEnum.values()) {
            if (value.appId.equals(appId)) {
                return value;
            }
        }
        return null;
    }
}
