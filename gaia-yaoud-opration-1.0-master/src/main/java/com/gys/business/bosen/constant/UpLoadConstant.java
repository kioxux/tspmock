package com.gys.business.bosen.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc:
 * @author: tzh
 * @createTime: 2021/9/22 18:39
 */
public class UpLoadConstant {

    /**
     * 基础路径  处方图片
     */
    public static final String BASE_PATH = "PRESCRIPTION_PICTURES";
    /**
     * 来源处方
     */
    public static final String SOURCE_PRESCRIPTION = "PRESCRIPTION";
    /**
     * 来源处方
     */
    public static final String PRODUCT_BASIC_IMAGE = "PRODUCT_BASIC_IMAGE";

    /**
     * 来源,路径
     */
    public static final Map<String, String> UPLOAD_CONFIG = new HashMap() {{
        put(SOURCE_PRESCRIPTION, "PRESCRIPTION_PICTURES");// 药德商品基础信息
        put(PRODUCT_BASIC_IMAGE, "PRODUCT_BASIC");// 药德商品基础信息
    }};
}
