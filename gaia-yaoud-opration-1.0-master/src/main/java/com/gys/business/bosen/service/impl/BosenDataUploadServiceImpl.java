package com.gys.business.bosen.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.gys.business.bosen.constant.BosenConstant;
import com.gys.business.bosen.constant.MessageTypeEnum;
import com.gys.business.bosen.dto.*;
import com.gys.business.bosen.form.DataForm;
import com.gys.business.bosen.form.InDataForm;
import com.gys.business.bosen.service.BosenDataUploadService;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.mapper.entity.GaiaSdWTSTO;
import com.gys.business.mapper.entity.WmKuCun;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.DateUtil;
import com.gys.util.FileUtil;
import com.gys.util.ZipUtil;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/22 15:42
 */
@Slf4j
@Service("bosenDataUploadService")
public class BosenDataUploadServiceImpl implements BosenDataUploadService {
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaWmKuCunMapper gaiaWmKuCunMapper;
    @Resource
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Resource
    private GaiaSdWTSTOMapper gaiaSdWTSTOMapper;
    @Resource
    private RedisManager redisManager;
    @Resource
    private GaiaSdBatchChangeMapper gaiaSdBatchChangeMapper;

    @Override
    public void downloadZipData(DataForm dataForm, HttpServletResponse response) {
        try {
            //查询数据：入库、出库、库存
            DownloadDataDTO dataDTO = fetchDataByClient(dataForm);
            //生成XML文件
            List<File> fileList = generateFileList(dataForm.getClient(), dataDTO);
            //压缩包文件
            String zipName = "data.zip";
            response.setContentType("APPLICATION/OCTET-STREAM");
            response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
            response.setCharacterEncoding("UTF-8");

            ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
            for (File file : fileList) {
                ZipUtil.doCompress(file, out);
                file.delete();
            }
            out.close();
        } catch (Exception e) {
            throw new BusinessException("生成博森上传数据异常", e);
        }
    }

    private List<File> generateFileList(String client, DownloadDataDTO dataDTO) {
        List<File> list = Lists.newArrayList();
        //库存文件
        Map<String, List<BsStockData>> stockMap = dataDTO.getStockMap();
        List<File> stockFileList = generateStockFileList(client, stockMap);
        list.addAll(stockFileList);

        //入库数据
        Map<String, List<BsInData>> inDataMap = dataDTO.getInDataMap();
        List<File> inFileList = generateInAndOutFileList(client, inDataMap, MessageTypeEnum.IN.type);
        list.addAll(inFileList);
        //出库数据
        Map<String, List<BsInData>> outDataMap = dataDTO.getOutDataMap();
        List<File> outFileList = generateInAndOutFileList(client, outDataMap, MessageTypeEnum.OUT.type);
        list.addAll(outFileList);
        return list;
    }

    private List<File> generateInAndOutFileList(String client, Map<String, List<BsInData>> inDataMap, String messageTypeEnum) {
        List<File> fileList = new ArrayList<>();
        List<InXmlData> xmlDataList = new ArrayList<>();
        try {
            for (Map.Entry<String, List<BsInData>> proSiteInData : inDataMap.entrySet()) {
                String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
                //控制dataList数据生成的文件大小在2M内
                List<BsInData> dataList = proSiteInData.getValue();
                List<List<BsInData>> fileDataList = new ArrayList<>();
                fileDataList.add(dataList);

                for (List<BsInData> list : fileDataList) {
                    InXmlData xmlData = new InXmlData();
                    //ENVELOP INFO
                    GaiaSdWTSTO dsfStore = getStoreCenterCode(client, proSiteInData.getKey());
                    if (dsfStore == null) {
                        continue;
                    }
                    String fileName = "";
                    if (MessageTypeEnum.IN.type.equals(messageTypeEnum)) {
                        fileName = String.format(BosenConstant.FILE_IN_TEMPLATE, dateTime, dsfStore.getStoDsfSite());
                    } else if (MessageTypeEnum.OUT.type.equals(messageTypeEnum)) {
                        fileName = String.format(BosenConstant.FILE_OUT_TEMPLATE, dateTime, dsfStore.getStoDsfSite());
                    }

                    EnvelopInfo envelopInfo = new EnvelopInfo();
                    envelopInfo.setFileName(fileName);
                    envelopInfo.setMessageType(messageTypeEnum);
                    envelopInfo.setSenderId(dsfStore.getStoDsfSite());
                    envelopInfo.setSenderName(dsfStore.getStoNm());
                    xmlData.setEnvelopInfo(envelopInfo);

                    //DATA INFO
                    List<BsInData> dataInfoList = Lists.newArrayList();
                    for (BsInData inData : list) {
                        if (StringUtil.isEmpty(inData.getCenterCode())) {
                            continue;
                        }
                        dataInfoList.add(inData);
                    }
                    xmlData.setDataInfoList(dataInfoList);
                    xmlDataList.add(xmlData);
                }
            }

            for (InXmlData xmlData : xmlDataList) {
                XStream xStream = new XStream(new DomDriver("UTF-8",new XmlFriendlyNameCoder("-_","_")));
                xStream.autodetectAnnotations(true);
                BaseInXmlData baseXmlData = new BaseInXmlData(new InDataObject(xmlData));
                String fileName = xmlData.getEnvelopInfo().getFileName();
                File file = FileUtil.createNewFile("/temp/" + fileName);
                FileOutputStream fos = new FileOutputStream(file, true);
                //写入xml声明
                fos.write(BosenConstant.XML_HEADER.getBytes(StandardCharsets.UTF_8));
                xStream.toXML(baseXmlData, fos);
                fileList.add(file);
                fos.close();
            }
        } catch (Exception e) {
            log.error("<河北博森><文件下载><生成入库数据异常{}>", e.getMessage(), e);
        }
        return fileList;
    }

    private List<File> generateStockFileList(String client, Map<String, List<BsStockData>> stockMap) {
        List<File> fileList = new ArrayList<>();
        try {
            //待上传文件列表
            List<InitXmlData> xmlDataList = new ArrayList<>();
            for (Map.Entry<String, List<BsStockData>> proSiteStock : stockMap.entrySet()) {
                String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
                //控制dataList数据生成的文件大小在2M内
                List<BsStockData> dataList = proSiteStock.getValue();
                List<List<BsStockData>> fileDataList = new ArrayList<>();
                fileDataList.add(dataList);

                for (List<BsStockData> list : fileDataList) {
                    InitXmlData xmlData = new InitXmlData();
                    //ENVELOP INFO
                    GaiaSdWTSTO dsfStore = getStoreCenterCode(client, proSiteStock.getKey());
                    if (dsfStore == null) {
                        continue;
                    }
                    String fileName = String.format(BosenConstant.FILE_INIT_TEMPLATE, dateTime, dsfStore.getStoDsfSite());
                    EnvelopInfo envelopInfo = new EnvelopInfo();
                    envelopInfo.setFileName(fileName);
                    envelopInfo.setMessageType(MessageTypeEnum.INIT.type);
                    envelopInfo.setSenderId(dsfStore.getStoDsfSite());
                    envelopInfo.setSenderName(dsfStore.getStoNm());
                    xmlData.setEnvelopInfo(envelopInfo);

                    //DATA INFO
                    List<InitDataInfo> dataInfoList = Lists.newArrayList();
                    for (BsStockData bsStockData : list) {
                        InitDataInfo dataInfo = new InitDataInfo();
                        if (StringUtil.isEmpty(bsStockData.getCenterCode())) {
                            continue;
                        }
                        BeanUtils.copyProperties(bsStockData, dataInfo);
                        dataInfoList.add(dataInfo);
                    }
                    xmlData.setDataInfoList(dataInfoList);
                    xmlDataList.add(xmlData);
                }
            }

            for (InitXmlData xmlData : xmlDataList) {
                //每个xml均要重新生成xStream实例
                XStream xStream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("-_", "_")));
                xStream.autodetectAnnotations(true);
                BaseXmlData baseXmlData = new BaseXmlData(new DataObject(xmlData));
                String fileName = xmlData.getEnvelopInfo().getFileName();
                File file = FileUtil.createNewFile("/temp/" + fileName);
                FileOutputStream fos = new FileOutputStream(file, true);
                //写入xml声明
                fos.write(BosenConstant.XML_HEADER.getBytes(StandardCharsets.UTF_8));
                xStream.toXML(baseXmlData, fos);
                fileList.add(file);
                fos.close();
            }
        } catch (Exception e) {
            log.error("<河北博森><数据下载><生成库存文件异常：{}>", e.getMessage(), e);
        }
        return fileList;
    }

    /**
     * 获取门店中心编码信息
     *
     * @param client    client
     * @param storeCode storeCode
     */
    private GaiaSdWTSTO getStoreCenterCode(String client, String storeCode) {
        String key = String.format(BosenConstant.KEY_BS_STORE, client);
        String storeResult = (String) redisManager.get(key);
        if (StringUtil.isNotEmpty(storeResult)) {
            List<GaiaSdWTSTO> list = JSON.parseArray(storeResult, GaiaSdWTSTO.class);
            List<GaiaSdWTSTO> collect = list.stream().filter(sto -> storeCode.equals(sto.getStoSite())).collect(Collectors.toList());
            return collect.size() == 0 ? null : collect.get(0);
        } else {
            GaiaSdWTSTO cond = new GaiaSdWTSTO();
            cond.setClient(client);
            List<GaiaSdWTSTO> list = gaiaSdWTSTOMapper.select(cond);
            redisManager.set(key, JSON.toJSONString(list));
            List<GaiaSdWTSTO> collect = list.stream().filter(sto -> storeCode.equals(sto.getStoSite())).collect(Collectors.toList());
            return collect.size() == 0 ? null : collect.get(0);
        }
    }

    private DownloadDataDTO fetchDataByClient(DataForm dataForm) {
        DownloadDataDTO dataDTO = new DownloadDataDTO();
        //配送中心列表
        GaiaDcData cond = new GaiaDcData();
        cond.setClient(dataForm.getClient());
        List<GaiaDcData> dcList = gaiaDcDataMapper.findList(cond);

        //1.库存数据<地点，库存列表>
        Map<String, List<BsStockData>> stockMap = fetchStockData(dataForm.getClient(), dcList);
        //如果传入门店，只要传入门店的数据
        if(!StringUtil.isEmpty(dataForm.getBrId())){
            if(stockMap.containsKey(dataForm.getBrId())){
                stockMap.entrySet().removeIf(d-> !d.getKey().equals(dataForm.getBrId()));
            }else{
                stockMap=new HashMap<>();
            }
        }
        dataDTO.setStockMap(stockMap);

        //2.入库数据<地点，入库列表>
        Map<String, List<BsInData>> inDataMap = fetchInData(dataForm, dcList);
        //如果传入门店，已经在上面那个方法里面过滤了
        dataDTO.setInDataMap(inDataMap);

        //3.出库数据<地点，出库列表>
        Map<String, List<BsInData>> outDataMap = fetchOutData(dataForm);
        //如果传入门店，只要传入门店的数据
        if(!StringUtil.isEmpty(dataForm.getBrId())){
            if(outDataMap.containsKey(dataForm.getBrId())){
                outDataMap.entrySet().removeIf(d-> !d.getKey().equals(dataForm.getBrId()));
            }else{
                outDataMap=new HashMap<>();
            }
        }
        dataDTO.setOutDataMap(outDataMap);
        return dataDTO;
    }

    private Map<String, List<BsInData>> fetchOutData(DataForm dataForm) {
        Map<String, List<BsInData>> outDataMap = new HashMap<>();
        Date currentDate = StringUtil.isNotEmpty(dataForm.getDate()) ?
                DateUtil.stringToDate(dataForm.getDate(), "yyyy-MM-dd") : new Date();
        String startTime = DateUtil.dateToString(DateUtils.addDays(currentDate, -1), "yyyy-MM-dd 22:00:01");
        String endTime = DateUtil.dateToString(currentDate, "yyyy-MM-dd 22:00:00");
        InDataForm cond = new InDataForm();
        cond.setClient(dataForm.getClient());
        cond.setStartTime(startTime);
        cond.setEndTime(endTime);
        List<BsInData> outDataList = gaiaWmKuCunMapper.getSaleData(cond);
        if (outDataList == null || outDataList.size() == 0) {
            return outDataMap;
        }
        //处理中药的批号和效期
        processBatchNoAndValidDay(outDataList);
        //转map
        Map<String, List<BsInData>> collect = outDataList.stream().collect(Collectors.groupingBy(BsInData::getProSite));
        outDataMap.putAll(collect);
        return outDataMap;
    }

    private void processBatchNoAndValidDay(List<BsInData> outDataList) {
        for (BsInData bsInData : outDataList) {
            if (StringUtil.isEmpty(bsInData.getCenterCode())) {
                continue;
            }
            if (StringUtil.isEmpty(bsInData.getBatchNo())) {
                //批次异动表中查询批号信息
                GaiaSdBatchChange cond = new GaiaSdBatchChange();
                cond.setClient(bsInData.getClient());
                cond.setGsbcDate(bsInData.getGssdDate());
                cond.setGsbcVoucherId(bsInData.getFph());
                cond.setGsbcProId(bsInData.getProSelfCode());
                cond.setGsbcBrId(bsInData.getProSite());
                GaiaSdBatchChange batchChange = gaiaSdBatchChangeMapper.getOneByVoucherId(cond);
                if (batchChange != null) {
                    bsInData.setBatchNo(batchChange.getGsbcBatchNo());
                }
            }
            if (StringUtil.isEmpty(bsInData.getValidDate())) {
                Date saleDate = DateUtil.stringToDate(bsInData.getJzDate(), "yyyy-MM-dd HH:mm:ss");
                Date date = DateUtils.addYears(saleDate, 1);
                bsInData.setValidDate(DateUtil.dateToString(date,"yyyy-MM-dd 00:00:00"));
            }
        }
    }

    private Map<String, List<BsInData>> fetchInData(DataForm dataForm, List<GaiaDcData> dcList) {
        Map<String, List<BsInData>> inDataMap = new HashMap<>();

        Date currentDate = StringUtil.isNotEmpty(dataForm.getDate()) ?
                DateUtil.stringToDate(dataForm.getDate(), "yyyy-MM-dd") : new Date();
        String startTime = DateUtil.dateToString(DateUtils.addDays(currentDate, -1), "yyyy-MM-dd 22:00:01");
        String endTime = DateUtil.dateToString(currentDate, "yyyy-MM-dd 22:00:00");
        if(!StringUtil.isEmpty(dataForm.getBrId())){
            //门店采购：雄安为单体店，需要上传各自门店的数据
            InDataForm cond = new InDataForm();
            cond.setClient(dataForm.getClient());
            cond.setProSite(dataForm.getBrId());
            cond.setStartTime(startTime);
            cond.setEndTime(endTime);
            //采购入库
            List<BsInData> dcInDataList = gaiaWmKuCunMapper.getStoreInData(cond);
            if (dcInDataList == null || dcInDataList.size() == 0) {
                dcInDataList = new ArrayList<>();
            }
            //采购退回
            List<BsInData> dcInTGDataList = gaiaWmKuCunMapper.getDcTGData(cond);
            if (dcInTGDataList != null) {
                dcInDataList.addAll(dcInTGDataList);
            }
            if (dcInDataList.size() > 0) {
                inDataMap.put(dataForm.getBrId(), dcInDataList);
            }
        }else {
            //配送中心入库数据
            for (GaiaDcData dcData : dcList) {
                InDataForm cond = new InDataForm();
                cond.setClient(dcData.getClient());
                cond.setProSite(dcData.getDcCode());
                cond.setStartTime(startTime);
                cond.setEndTime(endTime);
                //采购入库
                List<BsInData> dcInDataList = gaiaWmKuCunMapper.getDcInData(cond);
                //采购退回
                if (dcInDataList == null || dcInDataList.size() == 0) {
                    dcInDataList = new ArrayList<>();
                }
                List<BsInData> dcInTGDataList = gaiaWmKuCunMapper.getDcTGData(cond);
                if (dcInTGDataList != null) {
                    dcInDataList.addAll(dcInTGDataList);
                }
                if (dcInDataList.size() == 0) {
                    continue;
                }
                inDataMap.put(dcData.getDcCode(), dcInDataList);
            }
        }
        //门店采购:博森为连锁门店、无外采，无需上传采购记录
        return inDataMap;
    }

    private Map<String, List<BsStockData>> fetchStockData(String client, List<GaiaDcData> dcList) {
        Map<String, List<BsStockData>> stockMap = Maps.newHashMap();
        //配送中心库存
        for (GaiaDcData dcData : dcList) {
            WmKuCun cond = new WmKuCun();
            cond.setClient(dcData.getClient());
            cond.setProSite(dcData.getDcCode());
            List<BsStockData> currentStock = gaiaWmKuCunMapper.getCurrentStock(cond);
            if (currentStock == null || currentStock.size() == 0) {
                continue;
            }
            stockMap.put(dcData.getDcCode(), currentStock);
        }
        //门店库存
        List<BsStockData> storeStock = gaiaSdStockBatchMapper.getCurrentStock(client);
        //转成门店维度map
        Map<String, List<BsStockData>> collect = storeStock.stream().collect(Collectors.groupingBy(BsStockData::getProSite));
        stockMap.putAll(collect);
        return stockMap;
    }
}
