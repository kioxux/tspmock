package com.gys.business.bosen.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/22 13:48
 */
@Data
public class DataForm {
    /**
     * 加盟商
     */
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 指定执行日期：默认执行当前日期22：00至前一天22：00后的数据
     */
    private String date;
    /**
     * 账号KEY
     */
    private String key;
    /**
     * 门店id
     */
    private String brId;
}
