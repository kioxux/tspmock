package com.gys.business.bosen.service;

import com.gys.business.bosen.form.DataForm;

import javax.servlet.http.HttpServletResponse;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/18 09:48
 */
public interface BosenDataUploadService {

    void downloadZipData(DataForm dataForm, HttpServletResponse response);

}
