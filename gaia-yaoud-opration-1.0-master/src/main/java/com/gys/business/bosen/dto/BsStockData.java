package com.gys.business.bosen.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/22 19:26
 */
@Data
public class BsStockData {
    /**加盟商*/
    private String client;
    /**商品地点*/
    private String proSite;
    /**商品自编码*/
    private String proSelfCode;
    /**批次号*/
    private String pch;
    /**生产批号*/
    private String batchNo;
    /**库存数量*/
    private BigDecimal num;
    /**中心编码（河北药监局）*/
    private String centerCode;
    /**有效期：yyyy-mm-dd 00:00:00*/
    private String validDate;
    /**商品规格*/
    private String proSpecs;
    /**商品单位*/
    private String proUnit;
}
