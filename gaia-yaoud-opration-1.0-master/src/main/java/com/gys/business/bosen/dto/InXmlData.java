package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 23:48
 */
@Data
public class InXmlData {
    @XStreamAlias("ENVELOPINFO")
    private EnvelopInfo envelopInfo;
    @XStreamAlias("DATAINFO")
    private List<BsInData> dataInfoList;
}
