package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @desc: 库存数据XML实体
 * @author: Ryan
 * @createTime: 2021/9/23 09:40
 */
@Data
@XStreamAlias("ROWINFO")
public class InitDataInfo {
    @XStreamAlias("I_ITEM_CODE")
    private String centerCode;
    @XStreamAlias("BARCODE")
    private String barCode;
    @XStreamAlias("YPPH")
    private String batchNo;
    @XStreamAlias("VDATE")
    private String validDate;
    @XStreamAlias("PACKAGE")
    private String proSpecs;
    @XStreamAlias("UNIT")
    private String proUnit;
    @XStreamAlias("NUM")
    private BigDecimal num;
    @XStreamAlias("JZ_DATE")
    private String jzDate;

    public InitDataInfo() {
        this.barCode = "";
        this.jzDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
