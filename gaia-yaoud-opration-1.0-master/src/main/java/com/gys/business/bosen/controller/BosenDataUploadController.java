package com.gys.business.bosen.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.bosen.constant.BosenConstant;
import com.gys.business.bosen.constant.HBYJEnum;
import com.gys.business.bosen.form.DataForm;
import com.gys.business.bosen.service.BosenDataUploadService;
import com.gys.business.heartBeatWarn.service.HeartBeatService;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @desc: 河北博森数据上传
 * @author: Ryan
 * @createTime: 2021/9/17 20:20
 */
@Slf4j
@RestController
@RequestMapping("bosen")
public class BosenDataUploadController {
    @Resource
    private BosenDataUploadService bosenDataUploadService;
    @Resource
    private RedisManager redisManager;
    @Resource
    private HeartBeatService heartBeatService;


    /**
     * 下载药监追溯上传数据
     * 配送中心上传：采购记录+库存记录
     * 门店上传：采购记录+销售记录+库存记录
     * @param response response
     * @param dataForm 请求实体
     */
    @PostMapping("download")
    public void download(HttpServletRequest request, HttpServletResponse response, @RequestBody DataForm dataForm) {
        long startTime = System.currentTimeMillis();
        log.info("<河北博森><数据下载><请求参数：{}>", JSON.toJSONString(dataForm));
        validParams(dataForm);
        bosenDataUploadService.downloadZipData(dataForm, response);

        //添加监控
        Map<String,Object> inData=new HashMap<>();
        inData.put("client", dataForm.getClient());
        inData.put("site",dataForm.getBrId());
        inData.put("IP", CommonUtil.getIpAddr(request));
        inData.put("Name", "download");
        inData.put("Comment", "下载药监追溯上传数据");
        inData.put("WatchType", 2);
        inData.put("Comsume", System.currentTimeMillis()-startTime);
        heartBeatService.addServericeWatch(inData);
    }

    @GetMapping("downloadData")
    public void downloadData(HttpServletResponse response, @RequestParam("client") String client, @RequestParam("date") String date, @RequestParam("brId") String brId) {
        DataForm dataForm = new DataForm();
        dataForm.setClient(client);
        dataForm.setDate(date);
        dataForm.setBrId(brId);
        bosenDataUploadService.downloadZipData(dataForm, response);
    }

    @GetMapping("getRedisValue")
    public JsonResult getRedisValue(@Param("key") String key) {
        try {
            Object value = redisManager.get(key);
            return JsonResult.success(value);
        } catch (Exception e) {
            return JsonResult.error();
        }
    }

    private void validParams(DataForm dataForm) {
        if (BosenConstant.BS_CLIENT.equals(dataForm.getClient())) {
            dataForm.setKey("");
        }
        HBYJEnum hbyjEnum = HBYJEnum.getEnum(dataForm.getClient());
        if (hbyjEnum == null) {
            throw new BusinessException("当前加盟商未开放数据下载");
        }
        if (!hbyjEnum.appKey.equalsIgnoreCase(dataForm.getKey())) {
            throw new BusinessException("参数错误【key】");
        }
    }

}


