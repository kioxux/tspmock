package com.gys.business.bosen.form;

import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 20:39
 */
@Data
public class InDataForm {
    private String client;
    private String proSite;
    private String startTime;
    private String endTime;
}
