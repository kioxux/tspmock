package com.gys.business.bosen.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/23 09:47
 */
@Data
public class InitXmlData {
    @XStreamAlias("ENVELOPINFO")
    private EnvelopInfo envelopInfo;
    @XStreamAlias("DATAINFO")
    private List<InitDataInfo> dataInfoList;
}
