package com.gys.business.service;

import com.gys.business.service.data.GaiaSdMemberData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface SwitchPlatformService {

    /**
     * 添加数据
     * @param userInfo
     * @return
     */
    List<String> getTableName(GetLoginOutData userInfo);

    /**
     * 创建表结构
     * @param userInfo
     * @param vo
     */
    void createTableStructure(GetLoginOutData userInfo,GaiaSdMemberData vo);
}
