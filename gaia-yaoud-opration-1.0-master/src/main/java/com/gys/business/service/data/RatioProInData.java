package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class RatioProInData {
    @ApiModelProperty("加盟商")
    private String clientId;
    @ApiModelProperty("补货门店")
    private String brId;
    @ApiModelProperty("比价单号")
    private String pgId;
    @ApiModelProperty("操作人")
    private String userId;
    @ApiModelProperty("状态：0:需要抓取，1：抓取中，2;抓取完成")
    private String repStatus;
    private List<RatioProDetail> proDetail;
    @ApiModelProperty("连锁公司")
    private String stoChinaHead;
}
