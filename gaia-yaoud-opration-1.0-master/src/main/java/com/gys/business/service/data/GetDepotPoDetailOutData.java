//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "配送信息")
public class GetDepotPoDetailOutData {
    @ApiModelProperty(value = "采购凭证号")
    private String poId;
    @ApiModelProperty(value = "订单行号")
    private String poLineNo;
    @ApiModelProperty(value = "采购订单单价")
    private String poPrice;
    @ApiModelProperty(value = "税率")
    private String poRate;
}
