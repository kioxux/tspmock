package com.gys.business.service.data;

import cn.hutool.core.util.StrUtil;
import com.gys.util.CommonUtil;
import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@CsvRow("期末库存导出")
public class StockDiffRes implements Serializable {


    private static final long serialVersionUID = 8105501811896647375L;
//    @CsvCell(title = "加盟商code", index = 1, fieldNo = 1)
    @ApiModelProperty(value = "加盟商code")
    private String client;

    @CsvCell(title = "加盟商名称", index = 1, fieldNo = 1)
    @ApiModelProperty(value = "加盟商名称")
    private String francName;

//    @CsvCell(title = "地点（门店/仓库）code", index = 3, fieldNo = 1)
    @ApiModelProperty(value = "地点（门店/仓库）code")
    private String siteCode;

    @CsvCell(title = "地点（门店/仓库）名称", index = 2, fieldNo = 1)
    @ApiModelProperty(value = "地点（门店/仓库）名称")
    private String siteName;

    @CsvCell(title = "对比日期", index = 3, fieldNo = 1)
    @ApiModelProperty(value = "对比日期")
    private String gscCreateDate;

    @CsvCell(title = "对比时间", index = 4, fieldNo = 1)
    @ApiModelProperty(value = "对比时间")
    private String gscCreateTime;

    public String getGscCreateTime() {
        String res = "";
        if(StrUtil.isNotBlank(gscCreateTime)){
            res = CommonUtil.parseHHmmss(gscCreateTime);
        }
        return res;
    }

    @CsvCell(title = "商品编码", index = 5, fieldNo = 1)
    @ApiModelProperty(value = "商品编码")
    private String gsdProCode;

    @CsvCell(title = "商品名称", index = 6, fieldNo = 1)
    @ApiModelProperty(value = "商品名称")
    private String proName;

    @CsvCell(title = "商品规格", index = 7, fieldNo = 1)
    @ApiModelProperty(value = "商品规格")
    private String proSpecs;

    @CsvCell(title = "生产厂家", index = 8, fieldNo = 1)
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;

    @CsvCell(title = "单位", index = 9, fieldNo = 1)
    @ApiModelProperty(value = "单位")
    private String proUnit;

    @CsvCell(title = "批次", index = 10, fieldNo = 1)
    @ApiModelProperty(value = "批次")
    private String gsmBatch;

    @CsvCell(title = "推算数量", index = 11, fieldNo = 1)
    @ApiModelProperty(value = "推算数量")
    private String gsdCountQty;

    @CsvCell(title = "实时数量", index = 12, fieldNo = 1)
    @ApiModelProperty(value = "实时数量")
    private String gsdStockQty;

    @CsvCell(title = "差异数量", index = 13, fieldNo = 1)
    @ApiModelProperty(value = "差异数量")
    private String gsdDiffQty;

    @CsvCell(title = "推算金额", index = 14, fieldNo = 1)
    @ApiModelProperty(value = "推算金额")
    private String gsdCountAmt;

    @CsvCell(title = "实时金额", index = 15, fieldNo = 1)
    @ApiModelProperty(value = "实时金额")
    private String gsdStockAmt;

    @CsvCell(title = "差异金额", index = 16, fieldNo = 1)
    @ApiModelProperty(value = "差异金额")
    private String gsdDiffAmt;

//    @CsvCell(title = "比对任务号", index = 17, fieldNo = 1)
    @ApiModelProperty(value = "比对任务号")
    private String taskNo;

//    @CsvCell(title = "比对任务行号", index = 18, fieldNo = 1)
    @ApiModelProperty(value = "比对任务行号")
    private String taskItem;

//    @CsvCell(title = "带序号的对比任务号", index = 19, fieldNo = 1)
    @ApiModelProperty(value = "带序号的对比任务号")
    private String taskNoWithItem;

    @ApiModelProperty(value = "最后更新时间")
    private Date lastUpdateTime;

    public String getTaskNoWithItem() {
        return this.taskNo + this.taskItem;
    }
}
