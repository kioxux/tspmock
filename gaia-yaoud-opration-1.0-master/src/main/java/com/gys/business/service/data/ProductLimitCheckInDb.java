package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductLimitCheckInDb implements Serializable {

    private static final long serialVersionUID = 2748260953975464946L;

    //前端选择的商品编码合集
    private List<String> proIds;

}
