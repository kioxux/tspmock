package com.gys.business.service.data;

import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/28 19:07
 **/
@Data
public class ProductRank {
    private String client;
    private String proSelfCode;
    private Integer rankNum;
}
