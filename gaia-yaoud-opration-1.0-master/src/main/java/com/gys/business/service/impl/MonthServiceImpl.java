package com.gys.business.service.impl;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.druid.sql.visitor.functions.If;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.MonthService;
import com.gys.business.service.PayService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.CommonUtil;
import com.gys.util.UtilConst;
import com.microsoft.schemas.office.excel.STObjectType;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.collect.HppcMaps;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class MonthServiceImpl implements MonthService {

    @Resource
    private GaiaSaletaskHplanNewMapper gaiaSaletaskHplanNewMapper;

    @Resource
    private PayService payService;

    @Resource
    private GaiaSaletaskPlanClientStoNewMapper gaiaSaletaskPlanClientStoNewMapper;

    @Resource
    private GaiaSaletaskPlanStoMapper saletaskPlanStoMapper;

    @Resource
    private RedisManager redisManager;//redis


    @Override
    public List<MonthOutData> selectPage(MonthOutData monthOutData) {
        List<MonthOutData> outData = gaiaSaletaskHplanNewMapper.selectPage(monthOutData);
        return outData;
    }


    //新增
    @Override
    public String insertMonthlySalesPlan(GetLoginOutData userInfo, MonthOutData outData) {
        String missionMonth = outData.getMonthPlan();//获取任务月份
        //String ThismissionMonth = missionMonth.replace("-","");
        int count = gaiaSaletaskHplanNewMapper.staskTaskNameByName(outData.getStaskTaskName(), missionMonth,outData.getClient());
        if (count > 0) {
            throw new BusinessException("任务名称:" + outData.getStaskTaskName() + "已存在！");
        }
        List<GaiaSaletaskHplanNew> gaiaSaletaskHplanNews = new ArrayList<>();
        //创建月销售对象
        GaiaSaletaskHplanNew gaiaSaletaskHplanNew = new GaiaSaletaskHplanNew();
        //获取任务月份
        gaiaSaletaskHplanNew.setMonthPlan(missionMonth);
        //任务加盟商
        gaiaSaletaskHplanNew.setClient(outData.getClient());
        //获取任务id
        String taskId = gaiaSaletaskHplanNewMapper.selectNextVoucherIdS(userInfo.getClient());
        gaiaSaletaskHplanNew.setStaskTaskId(taskId);//任务id
        gaiaSaletaskHplanNew.setStaskTaskName(outData.getStaskTaskName());//任务名称
        //获取任务月份
        String year = missionMonth.substring(0, 4);//获取年
        String month = missionMonth.substring(4, 6);//获取月
        Integer yearS = 0; //转换年
        Integer monthS = 0;//转换月
        try {
            yearS = Integer.valueOf(year).intValue();
            monthS = Integer.valueOf(month).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        //获取当前月的第一天
        String firstDay = getFisrtDayOfMonth(yearS, monthS);
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearS, monthS);
        String thisLastDay = lastDay.replace("-", "");
        //起始日期
        gaiaSaletaskHplanNew.setStaskStartDate(thisFirstDay);
        //结束日期
        gaiaSaletaskHplanNew.setStaskEndDate(thisLastDay);
        gaiaSaletaskHplanNew.setYoyMonth(outData.getYoyMonth());//同比月份
        gaiaSaletaskHplanNew.setMomMonth(outData.getMomMonth());//环比月份
        Integer numberDay = getDaysByYearMonth(yearS, monthS);//获取期间天数
        gaiaSaletaskHplanNew.setMonthDays(numberDay);//期间天数

        String YoySaleAmt = outData.getYoySaleAmt();
        BigDecimal YoySaleAmtS = new BigDecimal(YoySaleAmt);
        gaiaSaletaskHplanNew.setYoySaleAmt(YoySaleAmtS);//营业额环比增长率

        String MomSaleAmt = outData.getMomSaleAmt();
        BigDecimal MomSaleAmtS = new BigDecimal(MomSaleAmt);
        gaiaSaletaskHplanNew.setMomSaleAmt(MomSaleAmtS);//营业额环比增长率

        String YoySaleGross = outData.getYoySaleGross();
        BigDecimal YoySaleGrossS = new BigDecimal(YoySaleGross);
        gaiaSaletaskHplanNew.setYoySaleGross(YoySaleGrossS);//毛利额同比增长率

        String MomSaleGross = outData.getMomSaleGross();
        BigDecimal MomSaleGrossS = new BigDecimal(MomSaleGross);
        gaiaSaletaskHplanNew.setMomSaleGross(MomSaleGrossS);//毛利额环比增长率

        String YoyMcardQty = outData.getYoyMcardQty();
        BigDecimal YoyMcardQtyS = new BigDecimal(YoyMcardQty);
        gaiaSaletaskHplanNew.setYoyMcardQty(YoyMcardQtyS);//会员卡同比增长率

        String MomMcardQty = outData.getMomMcardQty();
        BigDecimal MomMcardQtS = new BigDecimal(MomMcardQty);
        gaiaSaletaskHplanNew.setMomMcardQty(MomMcardQtS);//会员卡环比增长率

        gaiaSaletaskHplanNew.setStaskSplanSta("S");//任务状态(S已保存，P已下发)
        gaiaSaletaskHplanNew.setStaskCreId(userInfo.getUserId());//创建人id
        gaiaSaletaskHplanNew.setStaskCreDate(DateUtil.format(new Date(), "yyyyMMdd"));//创建日期
        gaiaSaletaskHplanNew.setStaskCreTime(DateUtil.format(new Date(), "HHmmss"));//创建时间
        gaiaSaletaskHplanNew.setStaskType("0");//删除 0 未删除 1已删除 添加
        gaiaSaletaskHplanNews.add(gaiaSaletaskHplanNew);
        this.gaiaSaletaskHplanNewMapper.insertMonthlySalesPlan(gaiaSaletaskHplanNews);
        return gaiaSaletaskHplanNew.getStaskTaskId();
    }

    @Override //删除任务
    public void deleteMonthlySalesPlan(MonthOutData outData) {
        this.gaiaSaletaskHplanNewMapper.deleteMonthlySalesPlan(outData);
    }


    //审核
    @Override
    public AuditData auditDate(GetLoginOutData userInfo,MonthOutData outData){
        outData.setClient(userInfo.getClient());
        AuditData audit = this.gaiaSaletaskHplanNewMapper.selectAudit(outData);
        return audit;
    }

    @Override
    public void audit(GetLoginOutData userInfo,MonthOutData outData){
        outData.setClient(userInfo.getClient());
        this.gaiaSaletaskHplanNewMapper.audit(outData);
    }

    //停用
    @Override
    public void stop(GetLoginOutData userInfo,MonthOutData outData){
        outData.setClient(userInfo.getClient());
        this.gaiaSaletaskHplanNewMapper.stop(outData);
    }

    //忽略
    @Override
    public void lose(GetLoginOutData userInfo,MonthOutData outData){
        outData.setClient(userInfo.getClient());
        this.gaiaSaletaskHplanNewMapper.lose(outData);
    }

    //用户删除门店
//    @Override
//    public void deleteStoreData(GaiaSaletaskPlanClientStoNew sto){
//        this.gaiaSaletaskHplanNewMapper.deleteStoreData(sto.getClient(),sto.getMonthPlan(),sto.getStoCode(),sto.getStaskTaskId());
//    }

    //查看
    @Override
    public Map<String, Object> viewSalesTask(MonthOutData outData){
        Map<String, Object> result = new HashMap<>();
//        GaiaSaletaskHplanNew gaiaSaletaskHplanNew = new GaiaSaletaskHplanNew();
        //创建月销售对象
        GaiaSaletaskHplanNew saletaskHplanNew = gaiaSaletaskHplanNewMapper.selectViewSalesTask(outData.getStaskTaskId(),outData.getClient());
        //验证查询返回对象数据是否为空
        if (ObjectUtil.isEmpty(saletaskHplanNew)){
            throw new BusinessException("提示：暂无数据！");
        }
        //list
        outData.setMonthPlan(saletaskHplanNew.getMonthPlan());
        List<MonthOutDataVo> monthOutDataVos = gaiaSaletaskHplanNewMapper.ViewSalesTaskPlan(outData);
        //判断返回数据是否为空
        if (ObjectUtil.isEmpty(monthOutDataVos)){
            throw new BusinessException("提示：暂无数据！");
        }
        result.put("SaleTaskPlan", saletaskHplanNew);
        result.put("SaleTaskPlanList", monthOutDataVos);
        return result;
    }

    //任务设置保存
    @Override
    public void  storeTaskSetting(GetLoginOutData userInfo, MonthOutData outData){
        GaiaSaletaskPlanClientStoNew stoNew = new GaiaSaletaskPlanClientStoNew();
        stoNew.setClient(outData.getClient());
        //获取任务ID
        stoNew.setStaskTaskId(outData.getStaskTaskId());
        //获取任务月份
//        stoNew.setMonthPlan(outData.getMonthPlan());//任务月份
        //获取门店编码
        stoNew.setStoCode(outData.getBrId());
        //营业额同比增长率
        String yoySaleAmt = outData.getYoySaleAmt();
        BigDecimal yoySaleAmt1 = new BigDecimal(yoySaleAmt);
        stoNew.setYoySaleAmt(String.valueOf(yoySaleAmt1));
        //营业额环比增长率
        String momSaleAmt = outData.getMomSaleAmt();
        BigDecimal momSaleAmt1 = new BigDecimal(momSaleAmt);
        stoNew.setMomSaleAmt(String.valueOf(momSaleAmt1));
        //毛利额同比增长率
        String yoySaleGross = outData.getYoySaleGross();
        BigDecimal yoySaleGross1 = new BigDecimal(yoySaleGross);
        stoNew.setYoySaleGross(String.valueOf(yoySaleGross1));
        //毛利额环比增长率
        String momSaleGross = outData.getMomSaleGross();
        BigDecimal momSaleGross1 = new BigDecimal(momSaleGross);
        stoNew.setMomSaleGross(String.valueOf(momSaleGross1));
        //会员卡同比增长率
        String yoyCardQty = outData.getYoyMcardQty();
        BigDecimal yoyCardQty1 = new BigDecimal(yoyCardQty);
        stoNew.setYoyMcardQty(String.valueOf(yoyCardQty1));
        //会员卡环比增长率
        String momMCardQty = outData.getMomMcardQty();
        BigDecimal momMCardQty1 = new BigDecimal(momMCardQty);
        stoNew.setMomMcardQty(String.valueOf(momMCardQty1));
        this.gaiaSaletaskPlanClientStoNewMapper.updateStoreTaskSetting(stoNew);
    }

    //门店数据导入查询
    @Override
    public List<MonthOutDataVo> exportIn(GetLoginOutData userInfo,List<GetStoreInData> getStoreInData){
        List<MonthOutDataVo> list = new ArrayList<>();
        String isNull = "";
        String str= "";
        for (GetStoreInData storeInData : getStoreInData) {
            if (ObjectUtil.isEmpty(storeInData.getBrId())){
                isNull = isNull+"，"+storeInData.getIndex();
            }
            int days = getDaysByYearMonth(Integer.parseInt(storeInData.getMonthPlan().substring(0,4)),Integer.parseInt(storeInData.getMonthPlan().substring(4,6)));
            if (storeInData.getMonthDays()>days){
                str = str +"，"+storeInData.getIndex();
            }
        }
        if(ObjectUtil.isNotEmpty(isNull)) {
            isNull = isNull.substring(1);
            throw new BusinessException("第" + isNull + "行的门店编码不能为空！");
        }
        if (ObjectUtil.isNotEmpty(str)){
            str = str.substring(1);
            throw new BusinessException("第" + str + "行的任务天数超出任务月份天数！");
        }
        String client = userInfo.getClient();
        for (GetStoreInData storeInData : getStoreInData) {
            storeInData.setClient(storeInData.getClient());
            MonthOutDataVo monthOutDataVo = this.gaiaSaletaskPlanClientStoNewMapper.selectStoreName(storeInData);
            if (ObjectUtil.isEmpty(monthOutDataVo)){
                throw new BusinessException("第"+storeInData.getIndex()+"行的门店不存在！");
            }else{
                monthOutDataVo.setClient(client);
                monthOutDataVo.setBrId(String.valueOf(storeInData.getBrId()));
                monthOutDataVo.setMissionDays(storeInData.getMonthDays());
                monthOutDataVo.setTurnoverDailyPlan(storeInData.getTurnoverDailyPlan());
                monthOutDataVo.setTurnoverDailyMonth(String.valueOf(new BigDecimal(storeInData.getMonthDays()*Double.valueOf(storeInData.getTurnoverDailyPlan())).setScale(2,BigDecimal.ROUND_HALF_UP)));
                monthOutDataVo.setGrossProfitDailyPlan(storeInData.getGrossProfitDailyPlan());
                monthOutDataVo.setGrossProfitDailyMonth(String.valueOf(new BigDecimal(storeInData.getMonthDays()*Double.valueOf(storeInData.getGrossProfitDailyPlan())).setScale(2,BigDecimal.ROUND_HALF_UP)));
                monthOutDataVo.setGrossMarginThisPeriod(String.valueOf(new BigDecimal(Double.valueOf(storeInData.getGrossProfitDailyPlan())/Double.valueOf(storeInData.getTurnoverDailyPlan())*100).setScale(2,BigDecimal.ROUND_HALF_UP)));
                monthOutDataVo.setMcardDailyPlan(storeInData.getMCardDailyPlan());
                monthOutDataVo.setMcardDailyMonth(String.valueOf(new BigDecimal(storeInData.getMonthDays()*Double.valueOf(storeInData.getMCardDailyPlan())).setScale(2,BigDecimal.ROUND_HALF_UP)));
                list.add(monthOutDataVo);
            }
        }
        return list;
    }

    @Override
    public void modifyMonthlySalesPlan(GetLoginOutData userInfo, MonthOutData outData) {
        String missionMonth = outData.getMonthPlan();//获取任务月份
        String ThismissionMonth = missionMonth.replace("-", "");

//        List<GaiaSaletaskHplanNew> gaiaSaletaskHplanNews = new ArrayList<>();
        GaiaSaletaskHplanNew gaiaSaletaskHplanNew = new GaiaSaletaskHplanNew();//创建月销售对象
        gaiaSaletaskHplanNew.setMonthPlan(ThismissionMonth);//任务月份
        //String taskId = gaiaSaletaskHplanClientNewMapper.selectNextVoucherIdS();//获取任务id
        gaiaSaletaskHplanNew.setStaskTaskId(outData.getStaskTaskId());//任务id
        gaiaSaletaskHplanNew.setStaskTaskName(outData.getStaskTaskName());//任务名称
        //获取任务月份
        String year = missionMonth.substring(0, 4);//获取年
        String month = missionMonth.substring(4, 6);//获取月
        Integer yearS = 0; //转换年
        Integer monthS = 0;//转换月
        try {
            yearS = Integer.valueOf(year).intValue();
            monthS = Integer.valueOf(month).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        String firstDay = getFisrtDayOfMonth(yearS, monthS);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearS, monthS);
        String thisLastDay = lastDay.replace("-", "");
        gaiaSaletaskHplanNew.setStaskStartDate(thisFirstDay);//起始日期
        gaiaSaletaskHplanNew.setStaskEndDate(thisLastDay);//结束日期
        gaiaSaletaskHplanNew.setYoyMonth(outData.getYoyMonth());//同比月份
        gaiaSaletaskHplanNew.setMomMonth(outData.getMomMonth());//环比月份
        Integer numberDay = getDaysByYearMonth(yearS, monthS);//获取期间天数
        gaiaSaletaskHplanNew.setMonthDays(numberDay);//期间天数
        String YoySaleAmt = outData.getYoySaleAmt();
        BigDecimal YoySaleAmtS = new BigDecimal(YoySaleAmt);
        gaiaSaletaskHplanNew.setYoySaleAmt(YoySaleAmtS);//营业额环比增长率

        String MomSaleAmt = outData.getMomSaleAmt();
        BigDecimal MomSaleAmtS = new BigDecimal(MomSaleAmt);
        gaiaSaletaskHplanNew.setMomSaleAmt(MomSaleAmtS);//营业额环比增长率

        String YoySaleGross = outData.getYoySaleGross();
        BigDecimal YoySaleGrossS = new BigDecimal(YoySaleGross);
        gaiaSaletaskHplanNew.setYoySaleGross(YoySaleGrossS);//毛利额同比增长率

        String MomSaleGross = outData.getMomSaleGross();
        BigDecimal MomSaleGrossS = new BigDecimal(MomSaleGross);
        gaiaSaletaskHplanNew.setMomSaleGross(MomSaleGrossS);//毛利额环比增长率

        String YoyMcardQty = outData.getYoyMcardQty();
        BigDecimal YoyMcardQtyS = new BigDecimal(YoyMcardQty);
        gaiaSaletaskHplanNew.setYoyMcardQty(YoyMcardQtyS);//会员卡同比增长率

        String MomMcardQty = outData.getMomMcardQty();
        BigDecimal MomMcardQtS = new BigDecimal(MomMcardQty);
        gaiaSaletaskHplanNew.setMomMcardQty(MomMcardQtS);//会员卡环比增长率
        gaiaSaletaskHplanNew.setStaskSplanSta("S");//任务状态(S已保存，P已下发)
        gaiaSaletaskHplanNew.setStaskCreId(userInfo.getUserId());//创建人id
        gaiaSaletaskHplanNew.setStaskCreDate(DateUtil.format(new Date(), "yyyyMMdd"));//创建日期
        gaiaSaletaskHplanNew.setStaskCreTime(DateUtil.format(new Date(), "HHmmss"));//创建时间
        gaiaSaletaskHplanNew.setStaskMdId(userInfo.getUserId());//修改人
        gaiaSaletaskHplanNew.setStaskMdDate(DateUtil.format(new Date(), "yyyyMMdd"));//修改日期
        gaiaSaletaskHplanNew.setStaskMdTime(DateUtil.format(new Date(), "HHmmss"));//修改时间
        gaiaSaletaskHplanNew.setStaskType("0");//删除 0 未删除 1已删除 添加
        this.gaiaSaletaskHplanNewMapper.modifyMonthlySalesPlan(gaiaSaletaskHplanNew);
    }






    @Override//用户修改保存
    public void saveStoreData(GetLoginOutData userInfo,MonthOutData outData) {
        String missionMonth = outData.getMonthPlan();//获取任务月份
        String ThismissionMonth = missionMonth.replace("-", "");
        //创建月销售对象：更新修改的销售任务信息
        GaiaSaletaskHplanNew gaiaSaletaskHplanNew = new GaiaSaletaskHplanNew();
        gaiaSaletaskHplanNew.setMonthPlan(ThismissionMonth);//任务月份
        //获取任务id
        gaiaSaletaskHplanNew.setStaskTaskId(outData.getStaskTaskId());//任务id
        gaiaSaletaskHplanNew.setStaskTaskName(outData.getStaskTaskName());//任务名称
        //获取任务月份
        String year = missionMonth.substring(0, 4);//获取年
        String month = missionMonth.substring(4, 6);//获取月
        Integer yearS = 0; //转换年
        Integer monthS = 0;//转换月
        try {
            yearS = Integer.valueOf(year).intValue();
            monthS = Integer.valueOf(month).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        String firstDay = getFisrtDayOfMonth(yearS, monthS);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearS, monthS);
        String thisLastDay = lastDay.replace("-", "");
        gaiaSaletaskHplanNew.setClient(userInfo.getClient());
        gaiaSaletaskHplanNew.setStaskStartDate(thisFirstDay);//起始日期
        gaiaSaletaskHplanNew.setStaskEndDate(thisLastDay);//结束日期
        gaiaSaletaskHplanNew.setYoyMonth(outData.getYoyMonth());//同比月份
        gaiaSaletaskHplanNew.setMomMonth(outData.getMomMonth());//环比月份
        Integer numberDay = getDaysByYearMonth(yearS, monthS);//获取期间天数
        gaiaSaletaskHplanNew.setMonthDays(numberDay);//期间天数
        String YoySaleAmt = outData.getYoySaleAmt();
        BigDecimal YoySaleAmtS = new BigDecimal(YoySaleAmt);
        gaiaSaletaskHplanNew.setYoySaleAmt(YoySaleAmtS);//营业额环比增长率
        String MomSaleAmt = outData.getMomSaleAmt();
        BigDecimal MomSaleAmtS = new BigDecimal(MomSaleAmt);
        gaiaSaletaskHplanNew.setMomSaleAmt(MomSaleAmtS);//营业额环比增长率

        String YoySaleGross = outData.getYoySaleGross();
        BigDecimal YoySaleGrossS = new BigDecimal(YoySaleGross);
        gaiaSaletaskHplanNew.setYoySaleGross(YoySaleGrossS);//毛利额同比增长率

        String MomSaleGross = outData.getMomSaleGross();
        BigDecimal MomSaleGrossS = new BigDecimal(MomSaleGross);
        gaiaSaletaskHplanNew.setMomSaleGross(MomSaleGrossS);//毛利额环比增长率

        String YoyMcardQty = outData.getYoyMcardQty();
        BigDecimal YoyMcardQtyS = new BigDecimal(YoyMcardQty);
        gaiaSaletaskHplanNew.setYoyMcardQty(YoyMcardQtyS);//会员卡同比增长率

        String MomMcardQty = outData.getMomMcardQty();
        BigDecimal MomMcardQtS = new BigDecimal(MomMcardQty);
        gaiaSaletaskHplanNew.setMomMcardQty(MomMcardQtS);//会员卡环比增长率
        gaiaSaletaskHplanNew.setStaskSplanSta("S");//任务状态(S已保存，P已下发)
//        gaiaSaletaskHplanNew.setStaskCreId(userInfo.getUserId());//创建人id
//        gaiaSaletaskHplanNew.setStaskCreDate(DateUtil.format(new Date(), "yyyyMMdd"));//创建日期
//        gaiaSaletaskHplanNew.setStaskCreTime(DateUtil.format(new Date(), "HHmmss"));//创建时间
        gaiaSaletaskHplanNew.setStaskMdId(userInfo.getUserId());//修改人
        gaiaSaletaskHplanNew.setStaskMdDate(CommonUtil.getyyyyMMdd());//修改日期
        gaiaSaletaskHplanNew.setStaskMdTime(CommonUtil.getHHmmss());//修改时间
        gaiaSaletaskHplanNew.setStaskType("0");//删除 0 未删除 1已删除 添加
//        gaiaSaletaskHplanNews.add(gaiaSaletaskHplanNew);
        //保存修改的任务
        this.gaiaSaletaskHplanNewMapper.modifyMonthlySalesPlan(gaiaSaletaskHplanNew);
        //删除门店数据
        this.gaiaSaletaskHplanNewMapper.deletePlanSto(outData.getStaskTaskId(),userInfo.getClient());
        //重新批量保存门店信息
        List<GaiaSaletaskPlanSto> list = new ArrayList<>();
        for (int i = 0; i < outData.getMonthOutDataVo().size(); i++) {
            GaiaSaletaskPlanSto gaiaSaletaskPlanClientStoNew = new GaiaSaletaskPlanSto();
            gaiaSaletaskPlanClientStoNew.setClient(userInfo.getClient());
            gaiaSaletaskPlanClientStoNew.setMonthPlan(outData.getMonthPlan());
            gaiaSaletaskPlanClientStoNew.setStoCode(outData.getMonthOutDataVo().get(i).getBrId());
            gaiaSaletaskPlanClientStoNew.setStaskTaskId(outData.getStaskTaskId());
            gaiaSaletaskPlanClientStoNew.setMonthDays(Integer.valueOf(String.valueOf(outData.getMonthOutDataVo().get(i).getMissionDays())));
            gaiaSaletaskPlanClientStoNew.setASaleAmt(new BigDecimal(outData.getMonthOutDataVo().get(i).getTurnoverDailyMonth()).divide(new BigDecimal(numberDay),2,BigDecimal.ROUND_HALF_UP));
            gaiaSaletaskPlanClientStoNew.setASaleGross(new BigDecimal(outData.getMonthOutDataVo().get(i).getGrossProfitDailyMonth()).divide(new BigDecimal(numberDay),2,BigDecimal.ROUND_HALF_UP));
            gaiaSaletaskPlanClientStoNew.setAMcardQty(new BigDecimal(outData.getMonthOutDataVo().get(i).getMcardDailyMonth()).divide(new BigDecimal(numberDay),2,BigDecimal.ROUND_HALF_UP).intValue());
            gaiaSaletaskPlanClientStoNew.setStaskCreId(userInfo.getUserId());  //创建人账号
            gaiaSaletaskPlanClientStoNew.setStaskCreDate(CommonUtil.getyyyyMMdd());  //创建日期
            gaiaSaletaskPlanClientStoNew.setStaskCreTime(CommonUtil.getHHmmss());  //创建时间
            gaiaSaletaskPlanClientStoNew.setStaskMdId(userInfo.getUserId());
            gaiaSaletaskPlanClientStoNew.setStaskMdDate(CommonUtil.getyyyyMMdd());
            gaiaSaletaskPlanClientStoNew.setStaskMdTime(CommonUtil.getHHmmss());
            gaiaSaletaskPlanClientStoNew.setStaskType("0");  //删除 0 未删除 1已删除
            gaiaSaletaskPlanClientStoNew.setStaskIsShow("1");
            gaiaSaletaskPlanClientStoNew.setASaleAmtSdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getTurnoverTSP())?"0.00":outData.getMonthOutDataVo().get(i).getTurnoverTSP()));
            gaiaSaletaskPlanClientStoNew.setASaleAmtPdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getTurnoverOP())?"0.00":outData.getMonthOutDataVo().get(i).getTurnoverOP()));
            gaiaSaletaskPlanClientStoNew.setASaleGrossSdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getGrossProfitTSP())?"0.00":outData.getMonthOutDataVo().get(i).getGrossProfitTSP()));
            gaiaSaletaskPlanClientStoNew.setASaleGrossPdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getGrossProfitOP())?"0.00":outData.getMonthOutDataVo().get(i).getGrossProfitOP()));
            gaiaSaletaskPlanClientStoNew.setASaleGrossRateSdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getGrossMarginTSP())?"0.00":outData.getMonthOutDataVo().get(i).getGrossMarginTSP()));
            gaiaSaletaskPlanClientStoNew.setASaleGrossRatePdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getGrossMarginOP())?"0.00":outData.getMonthOutDataVo().get(i).getGrossMarginOP()));
            gaiaSaletaskPlanClientStoNew.setAMcardQtySdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getMcardTSP())?"0.00":outData.getMonthOutDataVo().get(i).getMcardTSP()));
            gaiaSaletaskPlanClientStoNew.setAMcardQtyPdaily(new BigDecimal(ObjectUtil.isEmpty(outData.getMonthOutDataVo().get(i).getMcardOP())?"0.00":outData.getMonthOutDataVo().get(i).getMcardOP()));
            gaiaSaletaskPlanClientStoNew.setYoySaleAmt(outData.getMonthOutDataVo().get(i).getYoySaleAmt());
            gaiaSaletaskPlanClientStoNew.setMomSaleAmt(outData.getMonthOutDataVo().get(i).getMomSaleAmt());
            gaiaSaletaskPlanClientStoNew.setYoySaleGross(outData.getMonthOutDataVo().get(i).getYoySaleGross());
            gaiaSaletaskPlanClientStoNew.setMomSaleGross(outData.getMonthOutDataVo().get(i).getMomSaleGross());
            gaiaSaletaskPlanClientStoNew.setYoyMcardQty(outData.getMonthOutDataVo().get(i).getYoyMcardQty());
            gaiaSaletaskPlanClientStoNew.setMomMcardQty(outData.getMonthOutDataVo().get(i).getMomMcardQty());
            list.add(gaiaSaletaskPlanClientStoNew);
//            this.gaiaSaletaskPlanClientStoNewMapper.insert(gaiaSaletaskPlanClientStoNew);
        }
        if (ObjectUtil.isNotEmpty(list) && list.size() > 0){
            saletaskPlanStoMapper.insertList(list);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override //用户新增保存
    public void addedMonthlySalesTaskUsers(GetLoginOutData userInfo, MonthOutData outData) {
        if(ObjectUtil.isEmpty(outData.getYoySaleAmt()) && ObjectUtil.isEmpty(outData.getMomSaleAmt())){
            throw new BusinessException("营业额任务不能为空！");
        }
        if(ObjectUtil.isEmpty(outData.getYoySaleGross()) && ObjectUtil.isEmpty(outData.getMomSaleGross())){
            throw new BusinessException("毛利额任务不能为空！");
        }
        if(ObjectUtil.isEmpty(outData.getYoyMcardQty()) && ObjectUtil.isEmpty(outData.getMomMcardQty())){
            throw new BusinessException("会员卡任务不能为空！");
        }
//        Map<String, Object> map = new HashMap<>();
        String missionMonth = outData.getMonthPlan();//获取任务月份
        if(StringUtils.isNotEmpty(outData.getStaskTaskId())){
            saveStoreData(userInfo,outData);
        }else {
            int count = gaiaSaletaskHplanNewMapper.staskTaskNameByName(outData.getStaskTaskName(), missionMonth,userInfo.getClient());
            if (count > 0) {
                throw new BusinessException("任务名称:‘" + outData.getStaskTaskName() + "’已存在，请重新命名！");
            }
            //GAIA_SALETASK_PLAN_CLIENT_STO_NEW
            List<GaiaSaletaskPlanClientStoNew> gaiaSaletaskStoNew = new ArrayList<>();
            //GAIA_SALETASK_HPLAN_NEW
//        List<GaiaSaletaskHplanNew> saletaskHplanNews = new ArrayList<>();

            //生成任务id
            String taskId = gaiaSaletaskHplanNewMapper.selectNextVoucherIdS(userInfo.getClient());
            //获取默认值
            //outData.get

            int[] day = Conversion(outData.getMonthPlan());//转换int 类型
            int year = day[0];//年
            int month = day[1];//月
            String firstDay = getFisrtDayOfMonth(year, month);//获取当前月的第一天
            String thisFirstDay = firstDay.replace("-", "");
            String lastDay = getLastDayOfMonth(year, month);
            String thisLastDay = lastDay.replace("-", "");

//        String strArrS = taskId.substring(0, 10);
//        String strArr = taskId.substring(10);
//        String result = String.format("%0" + strArr.length() + "d", Integer.parseInt(strArr) + 1);
//        strArrS += result;


            GaiaSaletaskHplanNew gaiaSaletaskHplanNew = new GaiaSaletaskHplanNew();
            //客户ID
            gaiaSaletaskHplanNew.setClient(userInfo.getClient());
            //计划月份
            gaiaSaletaskHplanNew.setMonthPlan(outData.getMonthPlan());
            gaiaSaletaskHplanNew.setStaskTaskId(taskId);//任务ID
            gaiaSaletaskHplanNew.setStaskTaskName(outData.getStaskTaskName());//任务名称
            gaiaSaletaskHplanNew.setStaskStartDate(thisFirstDay);//起始日期
            gaiaSaletaskHplanNew.setStaskEndDate(thisLastDay);//结束日期
            gaiaSaletaskHplanNew.setYoyMonth(outData.getYoyMonth());//同比月份
            gaiaSaletaskHplanNew.setMomMonth(outData.getMomMonth());//环比月份

//        gaiaSaletaskHplanNew.setMonthDays(Integer.valueOf(outData.getMonthDays()));
            if (StringUtils.isNotEmpty(outData.getYoySaleAmt())) {
                gaiaSaletaskHplanNew.setYoySaleAmt(new BigDecimal(outData.getYoySaleAmt()));//营业额环比增长率
            }
            if (StringUtils.isNotEmpty(outData.getMomSaleAmt())) {
                gaiaSaletaskHplanNew.setMomSaleAmt(new BigDecimal(outData.getMomSaleAmt()));//营业额环比增长率
            }
            if (StringUtils.isNotEmpty(outData.getYoySaleGross())) {
                gaiaSaletaskHplanNew.setYoySaleGross(new BigDecimal(outData.getYoySaleGross()));//毛利额同比增长率
            }
            if (StringUtils.isNotEmpty(outData.getMomSaleGross())) {
                gaiaSaletaskHplanNew.setMomSaleGross(new BigDecimal(outData.getMomSaleGross()));//毛利额环比增长率
            }
            if (StringUtils.isNotEmpty(outData.getYoyMcardQty())) {
                gaiaSaletaskHplanNew.setYoyMcardQty(new BigDecimal(outData.getYoyMcardQty()));//会员卡同比增长率
            }
            if (StringUtils.isNotEmpty(outData.getMomMcardQty())) {
                gaiaSaletaskHplanNew.setMomMcardQty(new BigDecimal(outData.getMomMcardQty()));//会员卡环比增长率
            }

//        gaiaSaletaskHplanNew.setYoySaleAmt(new BigDecimal(outData.getYoySaleAmt()));//营业额同比增长率
//        gaiaSaletaskHplanNew.setMomSaleAmt(new BigDecimal(outData.getMomSaleAmt()));//营业额环比增长率
//        gaiaSaletaskHplanNew.setYoySaleGross(new BigDecimal(outData.getYoySaleGross()));//毛利额同比增长率
//        gaiaSaletaskHplanNew.setMomSaleGross(new BigDecimal(outData.getMomSaleGross()));//毛利额环比增长率
//        gaiaSaletaskHplanNew.setYoyMcardQty(new BigDecimal(outData.getYoyMcardQty()));//会员卡同比增长率
//        gaiaSaletaskHplanNew.setMomMcardQty(new BigDecimal(outData.getMomMcardQty()));//会员卡环比增长率
            gaiaSaletaskHplanNew.setStaskSplanSta("S");//任务状态
            //创建人
            gaiaSaletaskHplanNew.setStaskCreId(userInfo.getUserId());
            gaiaSaletaskHplanNew.setStaskCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            gaiaSaletaskHplanNew.setStaskCreTime(DateUtil.format(new Date(), "HHmmss"));
            //修改人
            gaiaSaletaskHplanNew.setStaskMdId(userInfo.getUserId());
            gaiaSaletaskHplanNew.setStaskMdDate(DateUtil.format(new Date(), "yyyyMMdd"));
            gaiaSaletaskHplanNew.setStaskMdTime(DateUtil.format(new Date(), "HHmmss"));
            gaiaSaletaskHplanNew.setStaskType("0");
//        saletaskHplanNews.add(gaiaSaletaskHplanNew);
            int days = getDaysByYearMonth(Integer.valueOf(outData.getMonthPlan().substring(0, 4)), Integer.valueOf(outData.getMonthPlan().substring(4)));
            gaiaSaletaskHplanNewMapper.insert(gaiaSaletaskHplanNew);
            if (ObjectUtil.isNotEmpty(outData.getMonthOutDataVo()) && outData.getMonthOutDataVo().size() > 0) {
                String msgTotal = "";
                for (int i = 0; i < outData.getMonthOutDataVo().size(); i++) {
                    String msg = "";
                    GaiaSaletaskPlanClientStoNew stoNew = new GaiaSaletaskPlanClientStoNew();
                    stoNew.setClient(userInfo.getClient());//客户ID
                    stoNew.setStoCode(outData.getMonthOutDataVo().get(i).getBrId());//门店编号\
                    stoNew.setMonthPlan(outData.getMonthPlan());//计划月份
                    stoNew.setStaskTaskId(taskId);//任务ID
                    stoNew.setMonthDays(days); //期间天数
                    if (StringUtils.isEmpty(outData.getMonthOutDataVo().get(i).getTurnoverDailyMonth())) {
                        msg += "第" + (i + 1) + "行，" + outData.getMonthOutDataVo().get(i).getBrName() + "营业额本期月计划未填写  ";
                    }
                    if (StringUtils.isEmpty(outData.getMonthOutDataVo().get(i).getGrossProfitDailyMonth())) {
                        msg += "毛利额本期月计划未填写  ";
                    }
                    if (StringUtils.isEmpty(outData.getMonthOutDataVo().get(i).getMcardDailyMonth())) {
                        msg += "会员卡本期月计划未填写  ";
                    }
                    if (StringUtils.isNotEmpty(msg)) {
                        msgTotal = msgTotal + msg;
                    } else {
                        //营业额本期日均计划
                        stoNew.setaSaleAmt(new BigDecimal(outData.getMonthOutDataVo().get(i).getTurnoverDailyMonth()).divide(new BigDecimal(days), 2, BigDecimal.ROUND_HALF_UP));
                        //毛利额本期日均计划
                        stoNew.setaSaleGross(new BigDecimal(outData.getMonthOutDataVo().get(i).getGrossProfitDailyMonth()).divide(new BigDecimal(days), 2, BigDecimal.ROUND_HALF_UP));
                        //会员卡本期日均计划
                        stoNew.setaMcardQty(new BigDecimal(outData.getMonthOutDataVo().get(i).getMcardDailyMonth()).divide(new BigDecimal(days), 2, BigDecimal.ROUND_HALF_UP).intValue());
                        //创建人账号
                        stoNew.setStaskCreId(userInfo.getUserId());
                        stoNew.setStaskCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
                        stoNew.setStaskCreTime(DateUtil.format(new Date(), "HHmmss"));
                        stoNew.setStaskType("0");  //删除 0 未删除 1已删除
                        //营业额同期日均
                        stoNew.setaSaleAmtSdaily(outData.getMonthOutDataVo().get(i).getTurnoverTSP());
                        //营业额环期日均
                        stoNew.setaSaleAmtPdaily(outData.getMonthOutDataVo().get(i).getTurnoverOP());
                        //毛利额同期日均
                        stoNew.setaSaleGrossSdaily(outData.getMonthOutDataVo().get(i).getGrossProfitTSP());
                        //毛利额环期日均
                        stoNew.setaSaleGrossPdaily(outData.getMonthOutDataVo().get(i).getGrossProfitOP());
                        String marginTSP = outData.getMonthOutDataVo().get(i).getGrossMarginTSP().split("%")[0];
                        if (marginTSP.equals("")){
                            marginTSP = "0.00";
                        }
                        //毛利率同期
                        stoNew.setaSaleGrossRateSdaily(marginTSP);
                        String marginOP = outData.getMonthOutDataVo().get(i).getGrossMarginOP().split("%")[0];
                        if (marginOP.equals("")){
                            marginOP = "0.00";
                        }
                        //毛利率环期
                        stoNew.setaSaleGrossRatePdaily(marginOP);
                        //会员卡同期日均
                        stoNew.setaMcardQtySdAily(outData.getMonthOutDataVo().get(i).getMcardTSP());
                        //会员卡环期日均
                        stoNew.setaMcardQtyPdAily(outData.getMonthOutDataVo().get(i).getMcardOP());
                        //营业额同比增长率s
                        stoNew.setYoySaleAmt(outData.getYoySaleAmt());
                        //营业额环比增长率
                        stoNew.setMomSaleAmt(outData.getMomSaleAmt());
                        //毛利额同比增长率
                        stoNew.setYoySaleGross(outData.getYoySaleGross());
                        //毛利额环比增长率
                        stoNew.setMomSaleGross(outData.getMomSaleGross());
                        //会员卡同比增长率
                        stoNew.setYoyMcardQty(outData.getYoyMcardQty());
                        //会员卡环比增长率
                        stoNew.setMomMcardQty(outData.getMomMcardQty());
                        gaiaSaletaskStoNew.add(stoNew);
                    }
                }
                if (StringUtils.isNotEmpty(msgTotal)) {
                    throw new BusinessException(msgTotal);
                } else {
                    if (ObjectUtil.isNotEmpty(gaiaSaletaskStoNew) && gaiaSaletaskStoNew.size() > 0) {
                        gaiaSaletaskPlanClientStoNewMapper.insertMonthlySalesPlan(gaiaSaletaskStoNew);
                    }
                }
            }else {
                //没有试算时，自动保存加盟商下门店
                List<GaiaSaletaskPlanSto> saletaskPlanStoList = this.saletaskPlanStoMapper.selectSaleStorePlanByClient(gaiaSaletaskHplanNew);
                if (ObjectUtil.isNotEmpty(saletaskPlanStoList) && saletaskPlanStoList.size() > 0){
                    List<GaiaSaletaskPlanSto> result = new ArrayList<>();
                    for (GaiaSaletaskPlanSto s : saletaskPlanStoList) {
                        s.setClient(userInfo.getClient());
                        s.setMonthPlan(outData.getMonthPlan());
                        s.setStaskCreDate(CommonUtil.getyyyyMMdd());
                        s.setYoySaleAmt(new BigDecimal(outData.getYoySaleAmt()));
                        s.setMomSaleAmt(new BigDecimal(outData.getMomSaleAmt()));
                        s.setYoySaleGross(new BigDecimal(outData.getYoySaleGross()));
                        s.setMomSaleGross(new BigDecimal(outData.getMomSaleGross()));
                        s.setYoyMcardQty(new BigDecimal(outData.getYoyMcardQty()));
                        s.setMomMcardQty(new BigDecimal(outData.getMomMcardQty()));
                        s.setStaskCreId("药德");
                        s.setStaskIsShow("0");
                        s.setStaskType("0");
                        s.setStaskTaskId(taskId);
                        if (ObjectUtil.isNotEmpty(s.getASaleAmt())
                                && ObjectUtil.isNotEmpty(s.getASaleGross())
                                && ObjectUtil.isNotEmpty(s.getAMcardQty())){
                            result.add(s);
                        }
                    }
                    if (ObjectUtil.isNotEmpty(result) && result.size() > 0) {
                        saletaskPlanStoMapper.insertList(result);
                    }
                }
            }
        }
        //添加 GAIA_SALETASK_PLAN_CLIENT_STO_NEW
    }

    @Override
    public String PushMonthlySalesPlanUser(GetLoginOutData userInfo, MonthOutData outData) {
        return null;
    }


    @Override //门店查询 用户
    public List<MonthOutData> getStoreBySaleTask(GaiaStoreData storeData) {
        return gaiaSaletaskHplanNewMapper.getStoreBySaleTask(storeData);
    }

    //设置
    @Override
    public MonthOutDataVo settingStore(MonthOutDataVo inData){
        MonthOutDataVo result = this.gaiaSaletaskHplanNewMapper.settingStore(inData);
        result.setMomSaleAmt(inData.getMomSaleAmt());
        result.setYoySaleAmt(inData.getYoySaleAmt());
        result.setMomSaleGross(inData.getMomSaleGross());
        result.setYoySaleGross(inData.getYoySaleGross());
        result.setMomMcardQty(inData.getMomMcardQty());
        result.setYoyMcardQty(inData.getYoyMcardQty());
        result.setMonthPlan(inData.getMonthPlan());
//            v.setStaskTaskId(gaiaSaletaskHplanNew.getStaskTaskId());
//        result.setStaskTaskName(gaiaSaletaskHplanNew.getStaskTaskName());
        return result;
    }


    @Override
    public List<MonthOutDataVo> target(GetLoginOutData userInfo,GaiaSaletaskHplanNew gaiaSaletaskHplanNew){
        gaiaSaletaskHplanNew.setClient(userInfo.getClient());
        List<MonthOutDataVo> list = this.gaiaSaletaskHplanNewMapper.targetTrial(gaiaSaletaskHplanNew);
        for (MonthOutDataVo v : list) {
            v.setMomSaleAmt(gaiaSaletaskHplanNew.getMomSaleAmt());
            v.setYoySaleAmt(gaiaSaletaskHplanNew.getYoySaleAmt());
            v.setMomSaleGross(gaiaSaletaskHplanNew.getMomSaleGross());
            v.setYoySaleGross(gaiaSaletaskHplanNew.getYoySaleGross());
            v.setMomMcardQty(gaiaSaletaskHplanNew.getMomMcardQty());
            v.setYoyMcardQty(gaiaSaletaskHplanNew.getYoyMcardQty());
            v.setMonthPlan(gaiaSaletaskHplanNew.getMonthPlan());
//            v.setStaskTaskId(gaiaSaletaskHplanNew.getStaskTaskId());
            v.setStaskTaskName(gaiaSaletaskHplanNew.getStaskTaskName());
        }
        return list;
    }

    @Override
    public Map<String, Object> targetTrial(GetLoginOutData userInfo, MonthOutData outData) {
        Map<String, Object> map = new HashMap<>();
        //List<CountVo> countVo = new ArrayList<>();//rf合计vo

        //创建 月销售任务维护 维护对象
        List<MonthOutDataVo> monthOutDataVos = new ArrayList<>();//创建对象 返回给前端的

        List<String> bigDecimalList = new ArrayList<>();//营业额同比 日均计划
        List<String> bigMomSaleGross = new ArrayList<>();//营业额环比 日均计划
        List<String> yearOnYearGrossProfit = new ArrayList<>();//毛利额同比 日均计划
        List<String> chainRatioGrossProfit = new ArrayList<>();//毛利额环比 日均计划
        //获取月份
        String monthPlan = outData.getMonthPlan();//用户输入的月份
        int[] day = Conversion(monthPlan);//转换int 类型
        int year = day[0];//年
        int month = day[1];//月
        int thisHeaven = getDaysByYearMonth(year, month);//获取天数完毕

        SalesSummaryData summaryData = new SalesSummaryData();
        summaryData.setClient(outData.getClient());
        GetPayInData inData = new GetPayInData();
        inData.setClientId(summaryData.getClient());
        inData.setType("1");
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(inData);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            summaryData.setPayTypeOutData(payTypeOutData);
        }
        //获取去年 日期
        String lastYear = previousYear(monthPlan);//去年日期
        //进行拆分 获取对相应的日期
        int[] lastYearS = Conversion(lastYear);//转换int 类型
        int yearLast = lastYearS[0];//年
        int monthLast = lastYearS[1];//月
        String firstDay = getFisrtDayOfMonth(yearLast, monthLast);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearLast, monthLast);
        String thisLastDay = lastDay.replace("-", "");
        summaryData.setStartDate(thisFirstDay);//开始日期  查询
        summaryData.setEndDate(thisLastDay);//结束日期
        List<MonthOutData> salesStoSummaries = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//去年
        List<String> dayGrossMarginLast = new ArrayList<>();
        List<String> dailyProfitAmtLast = new ArrayList<>();
        List<String> dailyPayAmtLast = new ArrayList<>();//去年营业额


        //获取今年开始
        //获取上月的日期
        String lastMonth = getFisrtDayOfMonth(monthPlan);//获取本年上月
        int[] thisYear = Conversion(lastMonth);
        int yearThis = thisYear[0];//年
        int monthThis = thisYear[1];//月
        String firstDayS = getFisrtDayOfMonth(yearThis, monthThis);//获取当前月的第一天

        //String thisFirstDayS = firstDayS.replace("-", "");
        String lastDayS = yesterday();//获取昨天的 日期
        //String thisLastDayS = lastDayS.replace("-", "");
        summaryData.setStartDate(firstDayS);//开始日期  查询
        summaryData.setEndDate(lastDayS);//结束日期
        List<MonthOutData> salesStoSummariesList = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//获取上月数据
        //今年开始
        List<String> dayGrossMargin = new ArrayList<>();
        List<String> dailyProfitAmt = new ArrayList<>();
        List<String> dailyPayAmt = new ArrayList<>();
        List<String>  brId =new ArrayList<>(); //今年
        List<String> brName =new ArrayList<>();
        List<String> client = new ArrayList<>();
        for (MonthOutData mo:salesStoSummariesList) {
            brId.add(mo.getBrId());
            brName.add(mo.getBrName());
            client.add(mo.getClient());
        }


        //新增 CollUtil.isNotEmpty(salesStoSummariesList)
        //System.out.println(outData.getMonthlyPlanVos().size());
        if (CollUtil.isNotEmpty(outData.getMonthlyPlanVos())){//如果不为空
            for (int j = 0; j <outData.getMonthlyPlanVos().size() ; j++) {
                if (outData.getMonthlyPlanVos().get(j).getHeaven()==null){
                    if (CollUtil.isNotEmpty(salesStoSummaries)){
                        //去年的
                        dayGrossMarginLast = salesStoSummaries.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率

                        dailyProfitAmtLast = salesStoSummaries.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额

                        dailyPayAmtLast = salesStoSummaries.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额
                        for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                            //营业额同比
                            String dailyPayAmtD = dailyPayAmtLast.get(i);//
                            //环比毛利额 日均
                            String userYoySale = outData.getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                            userYoySale = userYoySale.replace("%", "");
                            Float f = Float.valueOf(userYoySale) / 100;
                            BigDecimal decimal = new BigDecimal(f);
                            decimal = decimal.setScale(4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                            BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                            //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                            BigDecimal planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                            // planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                            // planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                            bigDecimalList.add(planForDailyGrowth.toString());//添加 营业额同比 日均计划

                            String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                            String momSaleGross = outData.getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                            momSaleGross = momSaleGross.replace("%", "");
                            Float fS = Float.valueOf(momSaleGross) / 100;
                            BigDecimal decimalS = new BigDecimal(fS);
                            decimalS = decimalS.setScale(4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal userMomSaleGross = decimalS.abs();//转换绝对值 转换用户输入的
                            //计算
                            BigDecimal bigDecimalS = new BigDecimal(dailyProfitAmtD);//转换
                            BigDecimal planMomSaleGross = bigDecimalS.multiply(userMomSaleGross);
                            planMomSaleGross = bigDecimalS.add(planMomSaleGross);
                            planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                            String str = planMomSaleGross.toString();
                            yearOnYearGrossProfit.add(str);//添加同比 毛利额
                        }
                    }
                    if (CollUtil.isNotEmpty(salesStoSummariesList)){
                        dayGrossMargin = salesStoSummariesList.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率
                        dailyProfitAmt = salesStoSummariesList.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额
                        dailyPayAmt = salesStoSummariesList.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额
                        System.out.println(dailyPayAmt);
                        for (int i = 0; i < dailyPayAmt.size(); i++) {
                            //momSaleAmt 营业额 环比字段
                            String dailyPayAmtD = dailyPayAmt.get(i);// 获取上期 环比日均营业额
                            //环比毛利额 日均
                            //String userYoySale = outData.getMomSaleAmt();//同比营业额 用户输入  yoySaleAmt

                            String userYoySale = outData.getMonthlyPlanVos().get(i).getMomSaleAmt();//同比营业额 用户输入  yoySaleAmt
                            System.out.println("用户输入的环比" + userYoySale);
                            userYoySale = userYoySale.replace("%", "");
                            float f = Float.valueOf(userYoySale) / 100;
                            BigDecimal decimal = new BigDecimal(f);
                            decimal = decimal.setScale(4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                            BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                            //同比增长日均计划  同比增长日均计划=同期日均×同比增长率；
                            BigDecimal planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                            System.out.println("结果" + planForDailyGrowth);
                            //添加到营业额 环比增长计划里面
                            planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                            planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                            bigMomSaleGross.add(planForDailyGrowth.toString());

                            //环比毛利额

                            String dailyProfitAmtD = dailyProfitAmt.get(i);//环比日均毛利额
                            String momSaleGross = outData.getMonthlyPlanVos().get(i).getMomSaleGross();//同比营业额 用户输入  yoySaleAmt
                            //String momSaleGross = outData.getMomSaleGross();//环比毛利额 用户输入的
                            System.out.println("环比毛利额" + momSaleGross);
                            momSaleGross = momSaleGross.replace("%", "");
                            Float f1 = Float.valueOf(momSaleGross) / 100;
                            BigDecimal decimal1 = new BigDecimal(f1);
                            decimal1 = decimal1.setScale(4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal userMomSaleGross = decimal1.abs();//转换绝对值 转换用户输入的
                            //计算
                            BigDecimal bigDecimals = new BigDecimal(dailyProfitAmtD);//转换
                            BigDecimal planMomSaleGross = bigDecimals.multiply(userMomSaleGross);
                            planMomSaleGross = bigDecimals.add(planMomSaleGross);
                            planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                            chainRatioGrossProfit.add(planMomSaleGross.toString());//添加环比 毛利额
                            //上期毛率
                        }
                    }
                }else {//获取用户输入的
                    MonthOutDataVo vo = new MonthOutDataVo();
                    //是新店--------------------------------------------------------------------------------
                    //获取用户输入的天数
                    String heaven = outData.getMonthlyPlanVos().get(j).getHeaven();
                    //string 转 BigDecimal stringConversionBigDecimal
                    BigDecimal heavenStr =stringConversionBigDecimal(heaven);//转换天数 成BigDecimal
                    heavenStr = heavenStr.abs();//绝对值
                    //获取 本期计划营业额

                    String thisPeriod = outData.getMonthlyPlanVos().get(j).getTurnoverPlan();
                    BigDecimal thisPeriodStr =stringConversionBigDecimal(thisPeriod);//转换本期日均计划 成BigDecimal
                    thisPeriodStr = thisPeriodStr.abs();//绝对值

                    //本期计划 毛利额 grossProfitPlay
                    String grossProfitPlay = outData.getMonthlyPlanVos().get(j).getGrossProfitPlay();
                    BigDecimal grossProfitPlayStr =stringConversionBigDecimal(grossProfitPlay);
                    grossProfitPlayStr = grossProfitPlayStr.abs();

                    //本期月计划 turnoverDailyMonth multiply() //营业额
                    BigDecimal turnoverDailyMonth = heavenStr.multiply(thisPeriodStr);
                    //本期月计划 毛利额
                    BigDecimal grossProfitDailyMonth = heavenStr.multiply(grossProfitPlayStr);
                    //本期 月毛利率
                    BigDecimal grossMarginThisPeriod = grossProfitDailyMonth.divide(turnoverDailyMonth,4,BigDecimal.ROUND_HALF_UP);
                    //获取毛利额 本期日均计划 参数
                    System.out.println("结束==============================================");
                    System.out.println(outData.getMonthlyPlanVos().get(j).getBrName().toString());
                    vo.setClient(userInfo.getClient());//加盟商
                    vo.setBrId(outData.getMonthlyPlanVos().get(j).getBrId());//地点
                    vo.setBrName(outData.getMonthlyPlanVos().get(j).getBrName());//门店简称
                    vo.setMissionDays(Integer.valueOf(heaven).intValue());//任务天数
                    vo.setTurnoverTSP("0");//营业额同期日均
                    vo.setTurnoverOP("0");//营业额上期日均
                    vo.setTurnoverYOYDAPlan("0");//营业额同比增长日均计划
                    vo.setTurnoverCRDAPlan("0");//营业额 环比增长日均计划
                    vo.setTurnoverDailyPlan(thisPeriodStr.toString());//营业额 本期日均计划
                    vo.setTurnoverDailyMonth(turnoverDailyMonth.toString());//营业额 本期月计划
                    //毛利额
                    vo.setGrossProfitTSP("0");//毛利额 同期日均
                    vo.setGrossProfitOP("0");//毛利额 上期日均
                    vo.setGrossProfitYOYDAPlan("0");//毛利额 同比增长日均计划
                    vo.setGrossProfitCRDAPlan("0");//毛利额 环比增长日均计划
                    vo.setGrossProfitDailyPlan(grossProfitPlayStr.toString());//毛利额 本期日均计划
                    vo.setGrossProfitDailyMonth(grossProfitDailyMonth.toString());//毛利额 本期月计划
                    //毛利率
                    vo.setGrossMarginTSP("0");
                    vo.setGrossMarginOP("0");
                    vo.setGrossMarginThisPeriod(grossMarginThisPeriod.toString());//毛利额/销售额
                    monthOutDataVos.add(vo);
                }
            }
        }else {
            if (CollUtil.isNotEmpty(salesStoSummaries)){
                //去年的
                dayGrossMarginLast = salesStoSummaries.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率

                dailyProfitAmtLast = salesStoSummaries.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额

                dailyPayAmtLast = salesStoSummaries.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额
                for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                    //营业额同比
                    String dailyPayAmtD = dailyPayAmtLast.get(i);//
                    //环比毛利额 日均
                    String userYoySale = outData.getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                    userYoySale = userYoySale.replace("%", "");
                    Float f = Float.valueOf(userYoySale) / 100;
                    BigDecimal decimal = new BigDecimal(f);
                    decimal = decimal.setScale(4, BigDecimal.ROUND_HALF_UP);
                    BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                    BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                    //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                    BigDecimal planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                    // planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                    // planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    bigDecimalList.add(planForDailyGrowth.toString());//添加 营业额同比 日均计划

                    String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                    String momSaleGross = outData.getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                    momSaleGross = momSaleGross.replace("%", "");
                    Float fS = Float.valueOf(momSaleGross) / 100;
                    BigDecimal decimalS = new BigDecimal(fS);
                    decimalS = decimalS.setScale(4, BigDecimal.ROUND_HALF_UP);
                    BigDecimal userMomSaleGross = decimalS.abs();//转换绝对值 转换用户输入的
                    //计算
                    BigDecimal bigDecimalS = new BigDecimal(dailyProfitAmtD);//转换
                    BigDecimal planMomSaleGross = bigDecimalS.multiply(userMomSaleGross);
                    planMomSaleGross = bigDecimalS.add(planMomSaleGross);
                    planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    String str = planMomSaleGross.toString();
                    yearOnYearGrossProfit.add(str);//添加同比 毛利额
                }
            /*else {
                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }

                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);
                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }*/


            }

            //今年开始
            if (CollUtil.isNotEmpty(salesStoSummariesList)){
                dayGrossMargin = salesStoSummariesList.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率
                dailyProfitAmt = salesStoSummariesList.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额
                dailyPayAmt = salesStoSummariesList.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额
                System.out.println(dailyPayAmt);
                for (int i = 0; i < dailyPayAmt.size(); i++) {
                    //momSaleAmt 营业额 环比字段
                    String dailyPayAmtD = dailyPayAmt.get(i);// 获取上期 环比日均营业额
                    //环比毛利额 日均
                    String userYoySale = outData.getMomSaleAmt();//同比营业额 用户输入  yoySaleAmt
                    System.out.println("用户输入的环比" + userYoySale);
                    //String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//同比营业额 用户输入  yoySaleAmt
                    userYoySale = userYoySale.replace("%", "");
                    float f = Float.valueOf(userYoySale) / 100;
                    BigDecimal decimal = new BigDecimal(f);
                    decimal = decimal.setScale(4, BigDecimal.ROUND_HALF_UP);
                    BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                    BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                    //同比增长日均计划  同比增长日均计划=同期日均×同比增长率；
                    BigDecimal planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                    System.out.println("结果" + planForDailyGrowth);
                    //添加到营业额 环比增长计划里面
                    planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                    planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    bigMomSaleGross.add(planForDailyGrowth.toString());

                    //环比毛利额

                    String dailyProfitAmtD = dailyProfitAmt.get(i);//环比日均毛利额
                    //String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoyMcardQty();//同比营业额 用户输入  yoySaleAmt
                    String momSaleGross = outData.getMomSaleGross();//环比毛利额 用户输入的
                    System.out.println("环比毛利额" + momSaleGross);
                    momSaleGross = momSaleGross.replace("%", "");
                    Float f1 = Float.valueOf(momSaleGross) / 100;
                    BigDecimal decimal1 = new BigDecimal(f1);
                    decimal1 = decimal1.setScale(4, BigDecimal.ROUND_HALF_UP);
                    BigDecimal userMomSaleGross = decimal1.abs();//转换绝对值 转换用户输入的
                    //计算
                    BigDecimal bigDecimals = new BigDecimal(dailyProfitAmtD);//转换
                    BigDecimal planMomSaleGross = bigDecimals.multiply(userMomSaleGross);
                    planMomSaleGross = bigDecimals.add(planMomSaleGross);
                    planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    chainRatioGrossProfit.add(planMomSaleGross.toString());//添加环比 毛利额
                    //上期毛率
                }
            /*else {
                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }

                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);
                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }
            }*/
            }

        }



        //返回给前端
        //monthOutDataVos
        List<String> thisPeriodTurnover = ComparisonMax(bigDecimalList,bigMomSaleGross);//本期日均计划 营业额
        System.out.println(thisPeriodTurnover);
        List<String> thisPeriodGrossProfit = ComparisonMax(yearOnYearGrossProfit,chainRatioGrossProfit);//本期日均计划 毛利额
        System.out.println(thisPeriodGrossProfit);

        for (int i = 0; i <thisPeriodTurnover.size() ; i++) {
            MonthOutDataVo vo = new MonthOutDataVo();
            vo.setClient(userInfo.getClient());
            vo.setMissionDays(thisHeaven);//本月天数
            vo.setClient(userInfo.getClient());//加盟商
            vo.setBrId(brId.get(i));//地点
            vo.setBrName(brName.get(i));//门店简称

            if (CollUtil.isNotEmpty(dailyPayAmtLast)){//去年营业额
                vo.setTurnoverTSP(dailyPayAmtLast.get(i));
            }else {
                vo.setTurnoverTSP("0");
            }

            if (CollUtil.isNotEmpty(dailyPayAmt)){ //今年营业额
                vo.setTurnoverOP(dailyPayAmt.get(i));
            }else {
                vo.setTurnoverOP("0");
            }

            if (CollUtil.isNotEmpty(bigDecimalList)){//去年同比增长计划
                vo.setTurnoverYOYDAPlan(bigDecimalList.get(i));
            }else {
                vo.setTurnoverYOYDAPlan("0");
            }

            if (CollUtil.isNotEmpty(bigMomSaleGross)){//去年环比增长计划
                vo.setTurnoverCRDAPlan(bigMomSaleGross.get(i));
            }else {
                vo.setTurnoverCRDAPlan("0");
            }

            if (CollUtil.isNotEmpty(thisPeriodTurnover)){//营业额本期 日均计划
                vo.setTurnoverDailyPlan(thisPeriodTurnover.get(i));
            }else {
                vo.setTurnoverDailyPlan("0");
            }
            if (CollUtil.isNotEmpty(thisPeriodTurnover)){//营业额 本期月计划
                BigDecimal bd = new BigDecimal(thisPeriodTurnover.get(i));
                //BigDecimal number = new BigDecimal(0);
                BigDecimal number =BigDecimal.valueOf((int)thisHeaven);//天数
                BigDecimal multiplyVal = number.multiply(bd);
                vo.setTurnoverDailyMonth(multiplyVal.toString());
            }else {
                vo.setTurnoverDailyMonth("0");
            }

            //毛利额
            if (CollUtil.isNotEmpty(dailyProfitAmtLast)){//去年毛利额
                vo.setGrossProfitTSP(dailyProfitAmtLast.get(i));
            }else {
                vo.setGrossProfitTSP("0");
            }

            if (CollUtil.isNotEmpty(dailyProfitAmt)){ //今年毛利额
                vo.setGrossProfitOP(dailyProfitAmt.get(i));
            }else {
                vo.setGrossProfitOP("0");
            }

            if (CollUtil.isNotEmpty(yearOnYearGrossProfit)){//去年同比增长计划
                vo.setGrossProfitYOYDAPlan(yearOnYearGrossProfit.get(i));
            }else {
                vo.setGrossProfitYOYDAPlan("0");
            }

            if (CollUtil.isNotEmpty(chainRatioGrossProfit)){//今年环比增长计划
                vo.setGrossProfitCRDAPlan(chainRatioGrossProfit.get(i));
            }else {
                vo.setGrossProfitCRDAPlan("0");
            }

            if (CollUtil.isNotEmpty(thisPeriodGrossProfit)){//毛利额本期 日均计划
                vo.setGrossProfitDailyPlan(thisPeriodGrossProfit.get(i));
            }else {
                vo.setGrossProfitDailyPlan("0");
            }

            if (CollUtil.isNotEmpty(thisPeriodGrossProfit)){//营业额 本期月计划
                BigDecimal bd = new BigDecimal(thisPeriodGrossProfit.get(i));
                //BigDecimal number = new BigDecimal(0);
                BigDecimal number =BigDecimal.valueOf((int)thisHeaven);//天数
                BigDecimal multiplyVal = number.multiply(bd);
                vo.setGrossProfitDailyMonth(multiplyVal.toString());
            }else {
                vo.setGrossProfitDailyMonth("0");
            }
            //毛利率
            if (CollUtil.isNotEmpty(dayGrossMarginLast)){//毛利率 同期
                vo.setGrossMarginTSP(dayGrossMarginLast.get(i));
            }else {
                vo.setGrossMarginTSP("0");
            }

            if (CollUtil.isNotEmpty(dayGrossMargin)){//毛利率 上期
                vo.setGrossMarginOP(dayGrossMargin.get(i));
            }else {
                vo.setGrossMarginOP("0");
            }

            if (CollUtil.isNotEmpty(thisPeriodTurnover)){//毛利率 本期计划
                String bq =thisPeriodGrossProfit.get(i);
                BigDecimal q = new BigDecimal(bq);
                String bd = thisPeriodTurnover.get(i);
                BigDecimal b = new BigDecimal(bd);
                if (b.compareTo(BigDecimal.ZERO)!=0){
                    BigDecimal divideVal = q.divide(b, 2, BigDecimal.ROUND_HALF_UP);
                    vo.setGrossMarginThisPeriod(divideVal.toString());//毛利率 本期计划 thisPeriodGrossProfit /  thisPeriodTurnover
                }else {
                    vo.setGrossMarginThisPeriod("0");
                }
            }else {
                vo.setGrossMarginThisPeriod("0");
            }
            monthOutDataVos.add(vo);
        }
        map.put("monthOutDataVo",monthOutDataVos);

       /* List<Integer> dayMissionDays =monthOutDataVos.stream().map(Book -> Book.getMissionDays()).collect(Collectors.toList());
        int maxCount =Collections.max(dayMissionDays);//合计任务天数*/

        List<String> turnoverTSP =monthOutDataVos.stream().map(Book -> Book.getTurnoverTSP()).collect(Collectors.toList());//营业额同期日均
        String turnoverTSPCount = count(turnoverTSP); //合计营业额 同期日均

        List<String> turnoverOP = monthOutDataVos.stream().map(Book -> Book.getTurnoverOP()).collect(Collectors.toList());//营业额上期日均
        String turnoverOPCount = count(turnoverOP); //合计营业额 上期日均

        List<String> turnoverYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverYOYDAPlan()).collect(Collectors.toList());//营业额 同比增长日均计划
        String turnoverYOYDAPlanCount = count(turnoverYOYDAPlan); //合计营业额 同比增长日均计划

        List<String> turnoverCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverCRDAPlan()).collect(Collectors.toList());//营业额 环比增长日均计划
        String turnoverCRDAPlanCount = count(turnoverCRDAPlan); //合计营业额 环比增长日均计划

        List<String> turnoverDailyPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyPlan()).collect(Collectors.toList());//营业额 本期日均计划
        String turnoverDailyPlanCount = count(turnoverDailyPlan); //合计营业额 本期日均计划

        List<String> turnoverDailyMonth = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyMonth()).collect(Collectors.toList());//营业额 本期月计划

        String turnoverDailyMonthCount = count(turnoverDailyMonth); //合计营业额 本期月计划

        List<String> grossProfitTSP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitTSP()).collect(Collectors.toList());//毛利额 同期日均
        String grossProfitTSPCount = count(grossProfitTSP); //合计毛利额 同期日均

        List<String> grossProfitOP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitOP()).collect(Collectors.toList());//毛利额 上期日均
        String grossProfitOPCount = count(grossProfitOP); //合计毛利额 上期日均

        List<String> grossProfitYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitYOYDAPlan()).collect(Collectors.toList());//毛利额 同比增长日均计划
        String grossProfitYOYDAPlanCount = count(grossProfitYOYDAPlan); //合计毛利额 同比增长日均计划

        List<String> grossProfitCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitCRDAPlan()).collect(Collectors.toList());//毛利额 环比增长日均计划
        String grossProfitCRDAPlanCount = count(grossProfitCRDAPlan); //合计毛利额 环比增长日均计划

        List<String> grossProfitDailyPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyPlan()).collect(Collectors.toList());//毛利额 本期日均计划
        String grossProfitDailyPlanCount = count(grossProfitDailyPlan); //合计毛利额 本期日均计划

        List<String> grossProfitDailyMonth = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyMonth()).collect(Collectors.toList());//毛利额 本期月计划
        String grossProfitDailyMonthCount = count(grossProfitDailyMonth); //合计毛利额 本期月计划

        List<String> grossMarginTSP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginTSP()).collect(Collectors.toList());//毛利率 同期
        String grossMarginTSPCount = count(grossMarginTSP); //合计毛利额 同期日均

        List<String> grossMarginOP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginOP()).collect(Collectors.toList());//毛利率 上期
        String grossMarginOPCount = count(grossMarginOP); //合计毛利额 同期日均

        List<String> grossMarginThisPeriod = monthOutDataVos.stream().map(Book -> Book.getGrossMarginThisPeriod()).collect(Collectors.toList());//毛利率 本期计划

        String grossMarginThisPeriodCount = count(grossMarginThisPeriod); //合计毛利额 同期日均
        //map.put("maxCount",maxCount);
        map.put("turnoverTSPCount",turnoverTSPCount);
        map.put("turnoverOPCount",turnoverOPCount);
        map.put("turnoverYOYDAPlanCount",turnoverYOYDAPlanCount);
        map.put("turnoverCRDAPlanCount",turnoverCRDAPlanCount);
        map.put("turnoverDailyPlanCount",turnoverDailyPlanCount);
        map.put("turnoverDailyMonthCount",turnoverDailyMonthCount);
        map.put("grossProfitTSPCount",grossProfitTSPCount);
        map.put("grossProfitOPCount",grossProfitOPCount);
        map.put("grossProfitYOYDAPlanCount",grossProfitYOYDAPlanCount);
        map.put("grossProfitCRDAPlanCount",grossProfitCRDAPlanCount);
        map.put("grossProfitDailyPlanCount",grossProfitDailyPlanCount);
        map.put("grossProfitDailyMonthCount",grossProfitDailyMonthCount);
        map.put("grossMarginTSPCount",grossMarginTSPCount);
        map.put("grossMarginOPCount",grossMarginOPCount);
        map.put("grossMarginThisPeriodCount",grossMarginThisPeriodCount);
        return map;
    }

    @Override
    public Map<String, Object> list(GetLoginOutData userInfo, MonthOutData outData) {
        Map<String, Object> map = new HashMap<>();
        //List<CountVo> countVo = new ArrayList<>();//合计vo

        //创建 月销售任务维护 维护对象
        List<MonthOutDataVo> monthOutDataVos = new ArrayList<>();//创建对象 返回给前端的

        List<String> bigDecimalList = new ArrayList<>();//营业额同比 日均计划
        List<String> bigMomSaleGross = new ArrayList<>();//营业额环比 日均计划
        List<String> yearOnYearGrossProfit = new ArrayList<>();//毛利额同比 日均计划
        List<String> chainRatioGrossProfit = new ArrayList<>();//毛利额环比 日均计划
        //获取月份
        String monthPlan = outData.getMonthPlan();//用户输入的月份
        int[] day = Conversion(monthPlan);//转换int 类型
        int year = day[0];//年
        int month = day[1];//月
        int thisHeaven = getDaysByYearMonth(year, month);//获取天数完毕

        SalesSummaryData summaryData = new SalesSummaryData();
        summaryData.setClient(outData.getClient());
        GetPayInData inData = new GetPayInData();
        inData.setClientId(summaryData.getClient());
        inData.setType("1");
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(inData);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            summaryData.setPayTypeOutData(payTypeOutData);
        }
        //获取去年 日期
        String lastYear = previousYear(monthPlan);//去年日期
        //进行拆分 获取对相应的日期
        int[] lastYearS = Conversion(lastYear);//转换int 类型
        int yearLast = lastYearS[0];//年
        int monthLast = lastYearS[1];//月
        String firstDay = getFisrtDayOfMonth(yearLast, monthLast);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearLast, monthLast);
        String thisLastDay = lastDay.replace("-", "");
        summaryData.setStartDate(thisFirstDay);//开始日期  查询
        summaryData.setEndDate(thisLastDay);//结束日期
        List<MonthOutData> salesStoSummaries = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//去年
        List<String> dayGrossMarginLast = new ArrayList<>();
        List<String> dailyProfitAmtLast = new ArrayList<>();
        List<String> dailyPayAmtLast = new ArrayList<>();//去年毛利额


        //获取今年开始
        //获取上月的日期
        String lastMonth = getFisrtDayOfMonth(monthPlan);//获取本年上月
        int[] thisYear = Conversion(lastMonth);
        int yearThis = thisYear[0];//年
        int monthThis = thisYear[1];//月
        String firstDayS = getFisrtDayOfMonth(yearThis, monthThis);//获取当前月的第一天

        //String thisFirstDayS = firstDayS.replace("-", "");
        String lastDayS = yesterday();//获取昨天的 日期

        //String thisLastDayS = lastDayS.replace("-", "");
        summaryData.setStartDate(firstDayS);//开始日期  查询
        summaryData.setEndDate(lastDayS);//结束日期
        List<MonthOutData> salesStoSummariesList = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//获取上月数据
        //今年开始
        List<String> dayGrossMargin = new ArrayList<>();
        List<String> dailyProfitAmt = new ArrayList<>();
        List<String> dailyPayAmt = new ArrayList<>();
        List<String>  brId =new ArrayList<>(); //今年
        List<String> brName =new ArrayList<>();
        List<String> client = new ArrayList<>();
        for (MonthOutData mo:salesStoSummariesList) {
            brId.add(mo.getBrId());
            brName.add(mo.getBrName());
            client.add(mo.getClient());
        }

        //获取 去年开始

        if (CollUtil.isNotEmpty(salesStoSummaries)){
            if (outData.getMonthlyPlanVos().size() == 0 || outData.getMonthlyPlanVos() == null){
                //去年的
                dayGrossMarginLast = salesStoSummaries.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率

                dailyProfitAmtLast = salesStoSummaries.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额

                dailyPayAmtLast = salesStoSummaries.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额

                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划 集合
                        //System.err.println(str);
                        //lastMonthThisIssue.add(planForDailyGrowth);
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }
                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }//结束

            }else {
                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }

                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);
                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }
            }
        }
        //去年 结束
        //今年开始
        if (CollUtil.isNotEmpty(salesStoSummariesList)){
            dayGrossMargin = salesStoSummariesList.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率
            dailyProfitAmt = salesStoSummariesList.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额
            dailyPayAmt = salesStoSummariesList.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额
            System.out.println(dailyPayAmt);

            BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
            if (dailyPayAmt.size() > 0 && dailyPayAmt != null) {
                MonthOutDataVo vo = new MonthOutDataVo();

                for (int i = 0; i < dailyPayAmt.size(); i++) {
                    String dailyPayAmtD = dailyPayAmt.get(i);// 获取上期 环比日均营业额
                    System.out.println(dailyPayAmtD);
                    //环比毛利额 日均
                    String userYoySale = outData.getMomSaleGross();//同比营业额 用户输入  yoySaleAmt
                    System.out.println("用户输入的环比"+userYoySale);
                    //String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//同比营业额 用户输入  yoySaleAmt
                    userYoySale = userYoySale.replace("%", "");
                    float f = Float.valueOf(userYoySale) / 100;
                    BigDecimal decimal = new BigDecimal(f);
                    decimal = decimal.setScale(4,BigDecimal.ROUND_HALF_UP);
                    BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                    BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                    //同比增长日均计划  同比增长日均计划=同期日均×同比增长率；
                    planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                    System.out.println("结果"+planForDailyGrowth);


                    //planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                    //planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    //String str = planForDailyGrowth.toString();
                    //bigMomSaleGross.add(str);//添加 同比增长日均计划
                    //lastMonthThisIssue.add(planForDailyGrowth);
                }//营业额同期增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
            }
            if (dailyProfitAmt.size() > 0 && dailyProfitAmt != null) {
                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                for (int i = 0; i < dailyProfitAmt.size(); i++) {
                    String dailyProfitAmtD = dailyProfitAmt.get(i);//环比日均毛利额
                    //String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoyMcardQty();//同比营业额 用户输入  yoySaleAmt
                    String momSaleGross = outData.getMomSaleAmt();//环比毛利额 用户输入的
                    System.out.println("环比毛利额"+momSaleGross);
                    momSaleGross = momSaleGross.replace("%", "");
                    Float f = Float.valueOf(momSaleGross) / 100;
                    BigDecimal decimal = new BigDecimal(f);

                    BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                    //计算
                    BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                    planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                    planMomSaleGross = bigDecimal.add(planMomSaleGross);
                    planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    String str = planMomSaleGross.toString();
                    chainRatioGrossProfit.add(str);//添加环比 毛利额
                }
            }
        }
        //判断 如果是新增的
        for (int i = 0; i <outData.getMonthlyPlanVos().size() ; i++) {

        }

        //创建vo类

        List<String> thisPeriodTurnover = ComparisonMax(bigDecimalList,bigMomSaleGross);//本期日均计划 营业额

        List<String> thisPeriodGrossProfit = ComparisonMax(yearOnYearGrossProfit,chainRatioGrossProfit);//本期日均计划 毛利额

        /*System.out.println(thisPeriodGrossProfit.size());
        for (int i = 0; i <thisPeriodTurnover.size() ; i++) {
            System.out.println(thisPeriodTurnover.get(i));
        }

        for (int i = 0; i <thisPeriodGrossProfit.size() ; i++) {
            System.out.println(thisPeriodGrossProfit.get(i));
        }*/


        for (int i = 0; i <thisPeriodTurnover.size() ; i++) {
            MonthOutDataVo vo = new MonthOutDataVo();
            vo.setMissionDays(thisHeaven);//天数
            vo.setBrId(brId.get(i));//地点
            vo.setBrName(brName.get(i));//门店简称
            vo.setClient(userInfo.getClient());
            //vo.setUuid(uuId.get(i));
            if (dailyPayAmtLast.size() >0 && dailyPayAmtLast != null){
                vo.setTurnoverTSP(dailyPayAmtLast.get(i));//营业额同期日均
            }else {
                vo.setTurnoverTSP("0");//营业额同期日均
            }
            if(dailyPayAmt.size() > 0 && dailyPayAmt !=null){
                vo.setTurnoverOP(dailyPayAmt.get(i));//营业额上期日均
            }else{
                vo.setTurnoverOP("0");//营业额上期日均
            }
            if (bigDecimalList.size() > 0 && bigDecimalList != null){
                vo.setTurnoverYOYDAPlan(bigDecimalList.get(i));//营业额 同比增长日均计划
            }else {
                vo.setTurnoverYOYDAPlan("0");//营业额 同比增长日均计划
            }
            if (bigMomSaleGross.size() >0 && bigMomSaleGross != null){
                vo.setTurnoverCRDAPlan(bigMomSaleGross.get(i));//营业额 环比增长日均计划
            }else {
                vo.setTurnoverCRDAPlan("0");//营业额 环比增长日均计划
            }
            if (thisPeriodTurnover.size() >0 && thisPeriodTurnover != null){
                vo.setTurnoverDailyPlan(thisPeriodTurnover.get(i));//营业额 本期日均计划
            }else {
                vo.setTurnoverDailyPlan("0");//营业额 本期日均计划
            }
            if (thisPeriodTurnover.size()>0 && thisPeriodTurnover != null){
                String months = thisPeriodTurnover.get(i);//月计划
                BigDecimal bd = new BigDecimal(months);
                BigDecimal number = new BigDecimal(0);
                number =BigDecimal.valueOf((int)thisHeaven);
                BigDecimal multiplyVal = number.multiply(bd);
                String monthlyPlan = String.valueOf(multiplyVal);
                vo.setTurnoverDailyMonth(monthlyPlan);//营业额 本期月计划
            }else {
                vo.setTurnoverDailyMonth("0");//营业额 本期月计划
            }


            //毛利额
            if (dailyProfitAmtLast.size() >0 && dailyProfitAmtLast !=null ){
                vo.setGrossProfitTSP(dailyProfitAmtLast.get(i));
            }else {
                vo.setGrossProfitTSP("0");
            }

            //vo.setGrossProfitTSP();//毛利额 同期日均
            if (dailyProfitAmt.size() > 0 && dailyProfitAmt!=null){
                vo.setGrossProfitOP(dailyProfitAmt.get(i));//毛利额 上期日均
            }else {
                vo.setGrossProfitOP("0");//毛利额 上期日均
            }
            if (yearOnYearGrossProfit.size() > 0 && yearOnYearGrossProfit != null){
                vo.setGrossProfitYOYDAPlan(yearOnYearGrossProfit.get(i));//毛利额 同比增长日均计划
            }else {
                vo.setGrossProfitYOYDAPlan("0");//毛利额 同比增长日均计划
            }


            if (chainRatioGrossProfit.size() > 0 && chainRatioGrossProfit !=null){
                vo.setGrossProfitCRDAPlan(chainRatioGrossProfit.get(i));//毛利额 环比增长日均计划
            }else {
                vo.setGrossProfitCRDAPlan("0");//毛利额 环比增长日均计划
            }
            if (thisPeriodGrossProfit.size() > 0 && thisPeriodGrossProfit != null){
                vo.setGrossProfitDailyPlan(thisPeriodGrossProfit.get(i));//毛利额 本期日均计划
            }else {
                vo.setGrossProfitDailyPlan("0");//毛利额 本期日均计划
            }

            if (thisPeriodGrossProfit.size() >0 && thisPeriodGrossProfit !=null ){
                String months = thisPeriodGrossProfit.get(i);//月计划
                BigDecimal bd = new BigDecimal(months);
                BigDecimal number = new BigDecimal(0);
                number =BigDecimal.valueOf((int)thisHeaven);
                BigDecimal multiplyVal = number.multiply(bd);
                String monthlyPlan = String.valueOf(multiplyVal);
                vo.setGrossProfitDailyMonth(monthlyPlan);//毛利额 本期月计划
            }else {
                vo.setGrossProfitDailyMonth("0");//毛利额 本期月计划
            }

            //毛利率
            if (dayGrossMarginLast.size() > 0 && dayGrossMarginLast !=null){
                vo.setGrossMarginTSP(dayGrossMarginLast.get(i));//毛利率 同期 dayGrossMarginLast
            }else {
                vo.setGrossMarginTSP("0");//毛利率 同期 dayGrossMarginLast
            }
            if (dayGrossMargin.size() > 0 && dayGrossMargin !=null){
                vo.setGrossMarginOP(dayGrossMargin.get(i));//毛利率 上期 dayGrossMargin
            }else {
                vo.setGrossMarginOP("0");//毛利率 上期 dayGrossMargin
            }
            if (thisPeriodTurnover.size() > 0 && thisPeriodTurnover !=null){
                String bq =thisPeriodGrossProfit.get(i);
                BigDecimal q = new BigDecimal(bq);
                String bd = thisPeriodTurnover.get(i);
                BigDecimal b = new BigDecimal(bd);
                if (b.compareTo(BigDecimal.ZERO)!=0){
                    BigDecimal divideVal = q.divide(b, 2, BigDecimal.ROUND_HALF_UP);
                    vo.setGrossMarginThisPeriod(divideVal.toString());//毛利率 本期计划 thisPeriodGrossProfit /  thisPeriodTurnover
                }else {
                    vo.setGrossMarginThisPeriod("0");
                }
            }else {
                vo.setGrossMarginThisPeriod("0");
            }

            monthOutDataVos.add(vo);
        }

        for (int i = 0; i <monthOutDataVos.size() ; i++) {
            System.out.println(monthOutDataVos.get(i).toString());
        }

        map.put("monthOutDataVo",monthOutDataVos);


        //合计算法

        //合计
        //List<String> list = new ArrayList<>();//合计list

       /* List<Integer> dayMissionDays =monthOutDataVos.stream().map(Book -> Book.getMissionDays()).collect(Collectors.toList());
        int maxCount =Collections.max(dayMissionDays);//合计任务天数

        List<String> turnoverTSP =monthOutDataVos.stream().map(Book -> Book.getTurnoverTSP()).collect(Collectors.toList());//营业额同期日均
        String turnoverTSPCount = count(turnoverTSP); //合计营业额 同期日均

        List<String> turnoverOP = monthOutDataVos.stream().map(Book -> Book.getTurnoverOP()).collect(Collectors.toList());//营业额上期日均
        String turnoverOPCount = count(turnoverOP); //合计营业额 上期日均

        List<String> turnoverYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverYOYDAPlan()).collect(Collectors.toList());//营业额 同比增长日均计划
        String turnoverYOYDAPlanCount = count(turnoverYOYDAPlan); //合计营业额 同比增长日均计划

        List<String> turnoverCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverCRDAPlan()).collect(Collectors.toList());//营业额 环比增长日均计划
        String turnoverCRDAPlanCount = count(turnoverCRDAPlan); //合计营业额 环比增长日均计划

        List<String> turnoverDailyPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyPlan()).collect(Collectors.toList());//营业额 本期日均计划
        String turnoverDailyPlanCount = count(turnoverDailyPlan); //合计营业额 本期日均计划

        List<String> turnoverDailyMonth = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyMonth()).collect(Collectors.toList());//营业额 本期月计划

        String turnoverDailyMonthCount = count(turnoverDailyMonth); //合计营业额 本期月计划

        List<String> grossProfitTSP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitTSP()).collect(Collectors.toList());//毛利额 同期日均
        String grossProfitTSPCount = count(grossProfitTSP); //合计毛利额 同期日均

        List<String> grossProfitOP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitOP()).collect(Collectors.toList());//毛利额 上期日均
        String grossProfitOPCount = count(grossProfitOP); //合计毛利额 上期日均

        List<String> grossProfitYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitYOYDAPlan()).collect(Collectors.toList());//毛利额 同比增长日均计划
        String grossProfitYOYDAPlanCount = count(grossProfitYOYDAPlan); //合计毛利额 同比增长日均计划

        List<String> grossProfitCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitCRDAPlan()).collect(Collectors.toList());//毛利额 环比增长日均计划
        String grossProfitCRDAPlanCount = count(grossProfitCRDAPlan); //合计毛利额 环比增长日均计划

        List<String> grossProfitDailyPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyPlan()).collect(Collectors.toList());//毛利额 本期日均计划
        String grossProfitDailyPlanCount = count(grossProfitDailyPlan); //合计毛利额 本期日均计划

        List<String> grossProfitDailyMonth = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyMonth()).collect(Collectors.toList());//毛利额 本期月计划
        String grossProfitDailyMonthCount = count(grossProfitDailyMonth); //合计毛利额 本期月计划

        List<String> grossMarginTSP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginTSP()).collect(Collectors.toList());//毛利率 同期
        String grossMarginTSPCount = count(grossMarginTSP); //合计毛利额 同期日均

        List<String> grossMarginOP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginOP()).collect(Collectors.toList());//毛利率 上期
        String grossMarginOPCount = count(grossMarginOP); //合计毛利额 同期日均

        List<String> grossMarginThisPeriod = monthOutDataVos.stream().map(Book -> Book.getGrossMarginThisPeriod()).collect(Collectors.toList());//毛利率 本期计划

        String grossMarginThisPeriodCount = count(grossMarginThisPeriod); //合计毛利额 同期日均
        map.put("maxCount",maxCount);
        map.put("turnoverTSPCount",turnoverTSPCount);
        map.put("turnoverOPCount",turnoverOPCount);
        map.put("turnoverYOYDAPlanCount",turnoverYOYDAPlanCount);
        map.put("turnoverCRDAPlanCount",turnoverCRDAPlanCount);
        map.put("turnoverDailyPlanCount",turnoverDailyPlanCount);
        map.put("turnoverDailyMonthCount",turnoverDailyMonthCount);
        map.put("grossProfitTSPCount",grossProfitTSPCount);
        map.put("grossProfitOPCount",grossProfitOPCount);
        map.put("grossProfitYOYDAPlanCount",grossProfitYOYDAPlanCount);
        map.put("grossProfitCRDAPlanCount",grossProfitCRDAPlanCount);
        map.put("grossProfitDailyPlanCount",grossProfitDailyPlanCount);
        map.put("grossProfitDailyMonthCount",grossProfitDailyMonthCount);
        map.put("grossMarginTSPCount",grossMarginTSPCount);
        map.put("grossMarginOPCount",grossMarginOPCount);
        map.put("grossMarginThisPeriodCount",grossMarginThisPeriodCount);*/
        return map;
    }



    public Map<String, Object> list2(GetLoginOutData userInfo, MonthOutData outData) {
        Map<String, Object> map = new HashMap<>();
        //List<CountVo> countVo = new ArrayList<>();//合计vo

        //创建 月销售任务维护 维护对象
        List<MonthOutDataVo> monthOutDataVos = new ArrayList<>();//创建对象 返回给前端的

        List<String> bigDecimalList = new ArrayList<>();//营业额同比 日均计划
        List<String> bigMomSaleGross = new ArrayList<>();//营业额环比 日均计划
        List<String> yearOnYearGrossProfit = new ArrayList<>();//毛利额同比 日均计划
        List<String> chainRatioGrossProfit = new ArrayList<>();//毛利额环比 日均计划
        /*if (StringUtils.isNotEmpty(outData.getBrId())) {//多个编码查询
            outData.setSiteArr(outData.getBrId().split("\\s+ |\\s+|,"));
        }*/
        //获取月份
        String strmo = outData.getMonthPlan();


       /* String monthPlan = null;
        for (int i = 0; i <outData.getMonthlyPlanVos().size(); i++) {
            //获取用户输入的
            monthPlan = outData.getMonthlyPlanVos().get(i).getMonthPlan();//用户输入的月份
        }*/
        //String[] s = outData.getSiteArr();//获取传过来的门店编码
        //获取用户输入的
        /* String monthPlan = outData.getMonthPlan();//用户输入的月份*/
        //任务天数 根据传过来的任务月份计算 天数
        int[] day = Conversion(strmo);//转换int 类型
        int year = day[0];//年
        int month = day[1];//月
        int thisHeaven = getDaysByYearMonth(year, month);//获取天数完毕
        SalesSummaryData summaryData = new SalesSummaryData();
        summaryData.setClient(outData.getClient());
        //summaryData.setSiteArr(s);
        GetPayInData inData = new GetPayInData();
        inData.setClientId(summaryData.getClient());
        inData.setType("1");
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(inData);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            summaryData.setPayTypeOutData(payTypeOutData);
        }
        //获取去年 日期
        String lastYear = previousYear(strmo);//去年日期
        //进行拆分 获取对相应的日期
        int[] lastYearS = Conversion(lastYear);//转换int 类型
        int yearLast = lastYearS[0];//年
        int monthLast = lastYearS[1];//月
        String firstDay = getFisrtDayOfMonth(yearLast, monthLast);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearLast, monthLast);
        String thisLastDay = lastDay.replace("-", "");
        summaryData.setStartDate(thisFirstDay);//开始日期  查询
        summaryData.setEndDate(thisLastDay);//结束日期
        List<MonthOutData> salesStoSummaries = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//去年
        List<String> dayGrossMarginLast = new ArrayList<>();
        List<String> dailyProfitAmtLast = new ArrayList<>();
        List<String> dailyPayAmtLast = new ArrayList<>();//去年毛利额

        //获取今年开始
        //获取上月的日期
        String lastMonth = getFisrtDayOfMonth(strmo);//获取本年上月
        int[] thisYear = Conversion(lastMonth);
        int yearThis = thisYear[0];//年
        int monthThis = thisYear[1];//月
        String firstDayS = getFisrtDayOfMonth(yearThis, monthThis);//获取当前月的第一天
        String thisFirstDayS = firstDayS.replace("-", "");
        String lastDayS = getLastDayOfMonth(yearThis, monthThis);
        String thisLastDayS = lastDayS.replace("-", "");
        summaryData.setStartDate(thisFirstDayS);//开始日期  查询
        summaryData.setEndDate(thisLastDayS);//结束日期
        List<MonthOutData> salesStoSummariesList = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//获取上月数据

        List<String>  brId =new ArrayList<>(); //今年
        List<String> brName =new ArrayList<>();
        List<String> client = new ArrayList<>();
        for (MonthOutData mo:salesStoSummariesList) {
            brId.add(mo.getBrId());
            brName.add(mo.getBrName());
            client.add(mo.getClient());
        }
        //今年开始
        List<String> dayGrossMargin = new ArrayList<>();
        List<String> dailyProfitAmt = new ArrayList<>();
        List<String> dailyPayAmt = new ArrayList<>();
        //新增逻辑


        //新增的店逻辑
        /*for (int j = 0; j <outData.getMonthlyPlanVos().size() ; j++) { //thisPeriod 日均计划*/

        /*if (outData.getMonthlyPlanVos().get(j).getHeaven() == null && outData.getMonthlyPlanVos().get(j).getThisPeriod() ==null ){*/

        if (CollUtil.isNotEmpty(salesStoSummaries)){
            if (outData.getMonthlyPlanVos().size() == 0 || outData.getMonthlyPlanVos() == null){
                //去年的
                dayGrossMarginLast = salesStoSummaries.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率

                dailyProfitAmtLast = salesStoSummaries.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额

                dailyPayAmtLast = salesStoSummaries.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额

                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划 集合
                        //System.err.println(str);
                        //lastMonthThisIssue.add(planForDailyGrowth);
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }
                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }//结束

            }else {
                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }

                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);
                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }
            }
        }

        //去年 结束

        if (CollUtil.isNotEmpty(salesStoSummariesList)){
            dayGrossMargin = salesStoSummariesList.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率
            dailyProfitAmt = salesStoSummariesList.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额
            dailyPayAmt = salesStoSummariesList.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额

            BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
            if (dailyProfitAmt.size() > 0 && dailyProfitAmt != null) {
                for (int i = 0; i < dailyPayAmt.size(); i++) {
                    String dailyPayAmtD = dailyProfitAmt.get(i);// 获取上期 环比日均营业额
                    //环比毛利额 日均
                    //String userYoySale = outData.getMomSaleAmt();//同比营业额 用户输入  yoySaleAmt
                    String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//同比营业额 用户输入  yoySaleAmt
                    userYoySale = userYoySale.replace("%", "");
                    Float f = Float.valueOf(userYoySale) / 100;
                    BigDecimal decimal = new BigDecimal(f);

                    BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                    BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                    //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                    planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                    planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                    planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    String str = planForDailyGrowth.toString();
                    bigMomSaleGross.add(str);//添加 同比增长日均计划
                    //lastMonthThisIssue.add(planForDailyGrowth);
                }//营业额同期增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
            }
            if (dailyPayAmt.size() > 0 && dailyPayAmt != null) {
                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                for (int i = 0; i < dailyPayAmt.size(); i++) {
                    String dailyProfitAmtD = dailyPayAmt.get(i);//环比日均毛利额
                    String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoyMcardQty();//同比营业额 用户输入  yoySaleAmt
                    //String momSaleGross = outData.getMomSaleGross();//环比毛利额 用户输入的
                    momSaleGross = momSaleGross.replace("%", "");
                    Float f = Float.valueOf(momSaleGross) / 100;
                    BigDecimal decimal = new BigDecimal(f);

                    BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                    //计算
                    BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                    planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                    planMomSaleGross = bigDecimal.add(planMomSaleGross);
                    planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    String str = planMomSaleGross.toString();
                    System.err.println("修改前:" + str);
                    chainRatioGrossProfit.add(str);//添加环比 毛利额
                }
            }
        }



        for (int j = 0; j <outData.getMonthlyPlanVos().size() ; j++) { //thisPeriod 日均计划*/ turnoverPlan
            if (outData.getMonthlyPlanVos().get(j).getHeaven() != null && outData.getMonthlyPlanVos().get(j).getHeaven() != "" &&
                    outData.getMonthlyPlanVos().get(j).getTurnoverPlan() !=null && outData.getMonthlyPlanVos().get(j).getTurnoverPlan() !="" &&
                    outData.getMonthlyPlanVos().get(j).getGrossProfitPlay() !=null && outData.getMonthlyPlanVos().get(j).getGrossProfitPlay() !=""){
                MonthOutDataVo vo = new MonthOutDataVo();
                //是新店--------------------------------------------------------------------------------
                //获取用户输入的天数
                String heaven = outData.getMonthlyPlanVos().get(j).getHeaven();
                //string 转 BigDecimal stringConversionBigDecimal
                BigDecimal heavenStr =stringConversionBigDecimal(heaven);//转换天数 成BigDecimal
                heavenStr = heavenStr.abs();//绝对值
                //获取 本期计划营业额

                String thisPeriod = outData.getMonthlyPlanVos().get(j).getTurnoverPlan();
                BigDecimal thisPeriodStr =stringConversionBigDecimal(thisPeriod);//转换本期日均计划 成BigDecimal
                thisPeriodStr = thisPeriodStr.abs();//绝对值

                //本期计划 毛利额 grossProfitPlay
                String grossProfitPlay = outData.getMonthlyPlanVos().get(j).getGrossProfitPlay();
                BigDecimal grossProfitPlayStr =stringConversionBigDecimal(grossProfitPlay);
                grossProfitPlayStr = grossProfitPlayStr.abs();

                //本期月计划 turnoverDailyMonth multiply() //营业额
                BigDecimal turnoverDailyMonth = heavenStr.multiply(thisPeriodStr);
                //本期月计划 毛利额
                BigDecimal grossProfitDailyMonth = heavenStr.multiply(grossProfitPlayStr);
                //本期 月毛利率
                BigDecimal grossMarginThisPeriod = grossProfitDailyMonth.divide(turnoverDailyMonth,4,BigDecimal.ROUND_HALF_UP);
                //获取毛利额 本期日均计划 参数
                System.out.println("结束==============================================");
                System.out.println(outData.getMonthlyPlanVos().get(j).getBrName().toString());
                vo.setClient(userInfo.getClient());//加盟商
                vo.setBrId(outData.getMonthlyPlanVos().get(j).getBrId());//地点
                vo.setBrName(outData.getMonthlyPlanVos().get(j).getBrName());//门店简称
                vo.setMissionDays(Integer.valueOf(heaven).intValue());//任务天数
                vo.setTurnoverTSP("0");//营业额同期日均
                vo.setTurnoverOP("0");//营业额上期日均
                vo.setTurnoverYOYDAPlan("0");//营业额同比增长日均计划
                vo.setTurnoverCRDAPlan("0");//营业额 环比增长日均计划
                vo.setTurnoverDailyPlan(thisPeriodStr.toString());//营业额 本期日均计划
                vo.setTurnoverDailyMonth(turnoverDailyMonth.toString());//营业额 本期月计划
                //毛利额
                vo.setGrossProfitTSP("0");//毛利额 同期日均
                vo.setGrossProfitOP("0");//毛利额 上期日均
                vo.setGrossProfitYOYDAPlan("0");//毛利额 同比增长日均计划
                vo.setGrossProfitCRDAPlan("0");//毛利额 环比增长日均计划
                vo.setGrossProfitDailyPlan(grossProfitPlayStr.toString());//毛利额 本期日均计划
                vo.setGrossProfitDailyMonth(grossProfitDailyMonth.toString());//毛利额 本期月计划
                //毛利率
                vo.setGrossMarginTSP("0");
                vo.setGrossMarginOP("0");
                vo.setGrossMarginThisPeriod(grossMarginThisPeriod.toString());//毛利额/销售额
                monthOutDataVos.add(vo);
            }
        }


        //创建vo类

        List<String> thisPeriodTurnover = ComparisonMax(bigDecimalList,bigMomSaleGross);//本期日均计划 营业额

        List<String> thisPeriodGrossProfit = ComparisonMax(yearOnYearGrossProfit,chainRatioGrossProfit);//本期日均计划 毛利额

        System.out.println(thisPeriodGrossProfit.size());
        for (int i = 0; i <thisPeriodTurnover.size() ; i++) {
            System.out.println(thisPeriodTurnover.get(i));
        }

        for (int i = 0; i <thisPeriodGrossProfit.size() ; i++) {
            System.out.println(thisPeriodGrossProfit.get(i));
        }


        for (int i = 0; i <thisPeriodTurnover.size() ; i++) {
            MonthOutDataVo vo = new MonthOutDataVo();
            vo.setMissionDays(thisHeaven);//天数
            vo.setBrId(brId.get(i));//地点
            vo.setBrName(brName.get(i));//门店简称
            vo.setClient(userInfo.getClient());
            //vo.setUuid(uuId.get(i));
            if (dailyPayAmtLast.size() >0 && dailyPayAmtLast != null){
                vo.setTurnoverTSP(dailyPayAmtLast.get(i));//营业额同期日均
            }else {
                vo.setTurnoverTSP("0");//营业额同期日均
            }
            if(dailyPayAmt.size() > 0 && dailyPayAmt !=null){
                vo.setTurnoverOP(dailyPayAmt.get(i));//营业额上期日均
            }else{
                vo.setTurnoverOP("0");//营业额上期日均
            }
            if (bigDecimalList.size() > 0 && bigDecimalList != null){
                vo.setTurnoverYOYDAPlan(bigDecimalList.get(i));//营业额 同比增长日均计划
            }else {
                vo.setTurnoverYOYDAPlan("0");//营业额 同比增长日均计划
            }
            if (bigMomSaleGross.size() >0 && bigMomSaleGross != null){
                vo.setTurnoverCRDAPlan(bigMomSaleGross.get(i));//营业额 环比增长日均计划
            }else {
                vo.setTurnoverCRDAPlan("0");//营业额 环比增长日均计划
            }
            if (thisPeriodTurnover.size() >0 && thisPeriodTurnover != null){
                vo.setTurnoverDailyPlan(thisPeriodTurnover.get(i));//营业额 本期日均计划
            }else {
                vo.setTurnoverDailyPlan("0");//营业额 本期日均计划
            }
            if (thisPeriodTurnover.size()>0 && thisPeriodTurnover != null){
                String months = thisPeriodTurnover.get(i);//月计划
                BigDecimal bd = new BigDecimal(months);
                BigDecimal number = new BigDecimal(0);
                number =BigDecimal.valueOf((int)thisHeaven);
                BigDecimal multiplyVal = number.multiply(bd);
                String monthlyPlan = String.valueOf(multiplyVal);
                vo.setTurnoverDailyMonth(monthlyPlan);//营业额 本期月计划
            }else {
                vo.setTurnoverDailyMonth("0");//营业额 本期月计划
            }


            //毛利额
            if (dailyProfitAmtLast.size() >0 && dailyProfitAmtLast !=null ){
                vo.setGrossProfitTSP(dailyProfitAmtLast.get(i));
            }else {
                vo.setGrossProfitTSP("0");
            }

            //vo.setGrossProfitTSP();//毛利额 同期日均
            if (dailyProfitAmt.size() > 0 && dailyProfitAmt!=null){
                vo.setGrossProfitOP(dailyProfitAmt.get(i));//毛利额 上期日均
            }else {
                vo.setGrossProfitOP("0");//毛利额 上期日均
            }
            if (yearOnYearGrossProfit.size() > 0 && yearOnYearGrossProfit != null){
                vo.setGrossProfitYOYDAPlan(yearOnYearGrossProfit.get(i));//毛利额 同比增长日均计划
            }else {
                vo.setGrossProfitYOYDAPlan("0");//毛利额 同比增长日均计划
            }


            if (chainRatioGrossProfit.size() > 0 && chainRatioGrossProfit !=null){
                vo.setGrossProfitCRDAPlan(chainRatioGrossProfit.get(i));//毛利额 环比增长日均计划
            }else {
                vo.setGrossProfitCRDAPlan("0");//毛利额 环比增长日均计划
            }
            if (thisPeriodGrossProfit.size() > 0 && thisPeriodGrossProfit != null){
                vo.setGrossProfitDailyPlan(thisPeriodGrossProfit.get(i));//毛利额 本期日均计划
            }else {
                vo.setGrossProfitDailyPlan("0");//毛利额 本期日均计划
            }

            if (thisPeriodGrossProfit.size() >0 && thisPeriodGrossProfit !=null ){
                String months = thisPeriodGrossProfit.get(i);//月计划
                BigDecimal bd = new BigDecimal(months);
                BigDecimal number = new BigDecimal(0);
                number =BigDecimal.valueOf((int)thisHeaven);
                BigDecimal multiplyVal = number.multiply(bd);
                String monthlyPlan = String.valueOf(multiplyVal);
                vo.setGrossProfitDailyMonth(monthlyPlan);//毛利额 本期月计划
            }else {
                vo.setGrossProfitDailyMonth("0");//毛利额 本期月计划
            }

            //毛利率
            if (dayGrossMarginLast.size() > 0 && dayGrossMarginLast !=null){
                vo.setGrossMarginTSP(dayGrossMarginLast.get(i));//毛利率 同期 dayGrossMarginLast
            }else {
                vo.setGrossMarginTSP("0");//毛利率 同期 dayGrossMarginLast
            }
            if (dayGrossMargin.size() > 0 && dayGrossMargin !=null){
                vo.setGrossMarginOP(dayGrossMargin.get(i));//毛利率 上期 dayGrossMargin
            }else {
                vo.setGrossMarginOP("0");//毛利率 上期 dayGrossMargin
            }
            if (thisPeriodTurnover.size() > 0 && thisPeriodTurnover !=null){
                String bq =thisPeriodGrossProfit.get(i);
                BigDecimal q = new BigDecimal(bq);
                String bd = thisPeriodTurnover.get(i);
                BigDecimal b = new BigDecimal(bd);
                if (b.compareTo(BigDecimal.ZERO)!=0){
                    BigDecimal divideVal = q.divide(b, 2, BigDecimal.ROUND_HALF_UP);
                    vo.setGrossMarginThisPeriod(divideVal.toString());//毛利率 本期计划 thisPeriodGrossProfit /  thisPeriodTurnover
                }else {
                    vo.setGrossMarginThisPeriod("0");
                }
            }else {
                vo.setGrossMarginThisPeriod("0");
            }

            monthOutDataVos.add(vo);
        }

        for (int i = 0; i <monthOutDataVos.size() ; i++) {
            System.out.println(monthOutDataVos.get(i).toString());
        }

        map.put("monthOutDataVo",monthOutDataVos);


        //合计算法

        //合计
        //List<String> list = new ArrayList<>();//合计list

       /* List<Integer> dayMissionDays =monthOutDataVos.stream().map(Book -> Book.getMissionDays()).collect(Collectors.toList());
        int maxCount =Collections.max(dayMissionDays);//合计任务天数

        List<String> turnoverTSP =monthOutDataVos.stream().map(Book -> Book.getTurnoverTSP()).collect(Collectors.toList());//营业额同期日均
        String turnoverTSPCount = count(turnoverTSP); //合计营业额 同期日均

        List<String> turnoverOP = monthOutDataVos.stream().map(Book -> Book.getTurnoverOP()).collect(Collectors.toList());//营业额上期日均
        String turnoverOPCount = count(turnoverOP); //合计营业额 上期日均

        List<String> turnoverYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverYOYDAPlan()).collect(Collectors.toList());//营业额 同比增长日均计划
        String turnoverYOYDAPlanCount = count(turnoverYOYDAPlan); //合计营业额 同比增长日均计划

        List<String> turnoverCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverCRDAPlan()).collect(Collectors.toList());//营业额 环比增长日均计划
        String turnoverCRDAPlanCount = count(turnoverCRDAPlan); //合计营业额 环比增长日均计划

        List<String> turnoverDailyPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyPlan()).collect(Collectors.toList());//营业额 本期日均计划
        String turnoverDailyPlanCount = count(turnoverDailyPlan); //合计营业额 本期日均计划

        List<String> turnoverDailyMonth = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyMonth()).collect(Collectors.toList());//营业额 本期月计划

        String turnoverDailyMonthCount = count(turnoverDailyMonth); //合计营业额 本期月计划

        List<String> grossProfitTSP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitTSP()).collect(Collectors.toList());//毛利额 同期日均
        String grossProfitTSPCount = count(grossProfitTSP); //合计毛利额 同期日均

        List<String> grossProfitOP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitOP()).collect(Collectors.toList());//毛利额 上期日均
        String grossProfitOPCount = count(grossProfitOP); //合计毛利额 上期日均

        List<String> grossProfitYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitYOYDAPlan()).collect(Collectors.toList());//毛利额 同比增长日均计划
        String grossProfitYOYDAPlanCount = count(grossProfitYOYDAPlan); //合计毛利额 同比增长日均计划

        List<String> grossProfitCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitCRDAPlan()).collect(Collectors.toList());//毛利额 环比增长日均计划
        String grossProfitCRDAPlanCount = count(grossProfitCRDAPlan); //合计毛利额 环比增长日均计划

        List<String> grossProfitDailyPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyPlan()).collect(Collectors.toList());//毛利额 本期日均计划
        String grossProfitDailyPlanCount = count(grossProfitDailyPlan); //合计毛利额 本期日均计划

        List<String> grossProfitDailyMonth = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyMonth()).collect(Collectors.toList());//毛利额 本期月计划
        String grossProfitDailyMonthCount = count(grossProfitDailyMonth); //合计毛利额 本期月计划

        List<String> grossMarginTSP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginTSP()).collect(Collectors.toList());//毛利率 同期
        String grossMarginTSPCount = count(grossMarginTSP); //合计毛利额 同期日均

        List<String> grossMarginOP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginOP()).collect(Collectors.toList());//毛利率 上期
        String grossMarginOPCount = count(grossMarginOP); //合计毛利额 同期日均

        List<String> grossMarginThisPeriod = monthOutDataVos.stream().map(Book -> Book.getGrossMarginThisPeriod()).collect(Collectors.toList());//毛利率 本期计划

        String grossMarginThisPeriodCount = count(grossMarginThisPeriod); //合计毛利额 同期日均
        map.put("maxCount",maxCount);
        map.put("turnoverTSPCount",turnoverTSPCount);
        map.put("turnoverOPCount",turnoverOPCount);
        map.put("turnoverYOYDAPlanCount",turnoverYOYDAPlanCount);
        map.put("turnoverCRDAPlanCount",turnoverCRDAPlanCount);
        map.put("turnoverDailyPlanCount",turnoverDailyPlanCount);
        map.put("turnoverDailyMonthCount",turnoverDailyMonthCount);
        map.put("grossProfitTSPCount",grossProfitTSPCount);
        map.put("grossProfitOPCount",grossProfitOPCount);
        map.put("grossProfitYOYDAPlanCount",grossProfitYOYDAPlanCount);
        map.put("grossProfitCRDAPlanCount",grossProfitCRDAPlanCount);
        map.put("grossProfitDailyPlanCount",grossProfitDailyPlanCount);
        map.put("grossProfitDailyMonthCount",grossProfitDailyMonthCount);
        map.put("grossMarginTSPCount",grossMarginTSPCount);
        map.put("grossMarginOPCount",grossMarginOPCount);
        map.put("grossMarginThisPeriodCount",grossMarginThisPeriodCount);*/
        return map;
    }

    @Override //用户查询
    public List<MonthOutData> monthlySalesTaskListUser(MonthOutData outData) {
        List<MonthOutData> monthOutData = gaiaSaletaskHplanNewMapper.monthlySalesTaskListUser(outData);
        return monthOutData;
    }

    //    插入行
    @Override
    public List<GaiaStoreData> insertRow(GetLoginOutData userInfo){
        return this.gaiaSaletaskHplanNewMapper.stoCodeList(userInfo.getClient());
    }

    //    删除行
    @Override
    public void deleteRow(GetLoginOutData userInfo,MonthOutData outData){
        outData.setClient(userInfo.getClient());
        this.gaiaSaletaskHplanNewMapper.deleteRow(outData);
    }


    public Map<String, Object> list1(GetLoginOutData userInfo, MonthOutData outData) {
        if (StringUtils.isNotEmpty(outData.getBrId())) {//多个编码查询
            outData.setSiteArr(outData.getBrId().split("\\s+ |\\s+|,"));
        }
        String[] s = outData.getSiteArr();
        Map<String, Object> map = new HashMap<>();
        List<String> bigDecimalList = new ArrayList<>();//营业额同比 日均计划
        List<String> bigMomSaleGross = new ArrayList<>();//营业额环比 日均计划
        List<String> yearOnYearGrossProfit = new ArrayList<>();//毛利额同比 日均计划
        List<String> chainRatioGrossProfit = new ArrayList<>();//毛利额环比 日均计划

        List<MonthOutDataVo> monthOutDataVos = new ArrayList<>();//创建对象 返回给前端的

        String monthPlan = null;
        for (int i = 0; i <outData.getMonthlyPlanVos().size(); i++) {
            //获取用户输入的
            monthPlan = outData.getMonthlyPlanVos().get(i).getMonthPlan();//用户输入的月份
        }

        //任务天数 根据传过来的任务月份计算 天数
        int[] day = Conversion(monthPlan);//转换int 类型
        int year = day[0];//年
        int month = day[1];//月
        int thisHeaven = getDaysByYearMonth(year, month);//获取天数完毕
        SalesSummaryData summaryData = new SalesSummaryData();
        summaryData.setClient(outData.getClient());
        summaryData.setSiteArr(s);
        GetPayInData inData = new GetPayInData();
        inData.setClientId(summaryData.getClient());
        inData.setType("1");
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(inData);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            summaryData.setPayTypeOutData(payTypeOutData);
        }
        //获取去年 日期
        String lastYear = previousYear(monthPlan);//去年日期
        //进行拆分 获取对相应的日期
        int[] lastYearS = Conversion(lastYear);//转换int 类型
        int yearLast = lastYearS[0];//年
        int monthLast = lastYearS[1];//月
        String firstDay = getFisrtDayOfMonth(yearLast, monthLast);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearLast, monthLast);
        String thisLastDay = lastDay.replace("-", "");
        summaryData.setStartDate(thisFirstDay);//开始日期  查询
        summaryData.setEndDate(thisLastDay);//结束日期
        List<MonthOutData> salesStoSummaries = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//去年
        List<String> dayGrossMarginLast = new ArrayList<>();
        List<String> dailyProfitAmtLast = new ArrayList<>();
        List<String> dailyPayAmtLast = new ArrayList<>();
        //获取今年开始
        //获取上月的日期
        String lastMonth = getFisrtDayOfMonth(monthPlan);//获取本年上月
        int[] thisYear = Conversion(lastMonth);
        int yearThis = thisYear[0];//年
        int monthThis = thisYear[1];//月
        String firstDayS = getFisrtDayOfMonth(yearThis, monthThis);//获取当前月的第一天
        String thisFirstDayS = firstDayS.replace("-", "");
        String lastDayS = getLastDayOfMonth(yearThis, monthThis);
        String thisLastDayS = lastDayS.replace("-", "");
        summaryData.setStartDate(thisFirstDayS);//开始日期  查询
        summaryData.setEndDate(thisLastDayS);//结束日期
        List<MonthOutData> salesStoSummariesList = gaiaSaletaskHplanNewMapper.listStoreSales(summaryData);//获取上月数据
        List<String>  brId =new ArrayList<>();
        List<String> brName =new ArrayList<>();
        List<String> client = new ArrayList<>();
        List<String> uuId = new ArrayList<>();
        for (MonthOutData mo:salesStoSummariesList) {
            brId.add(mo.getBrId());
            brName.add(mo.getBrName());
            client.add(mo.getClient());
        }


        if (CollUtil.isNotEmpty(salesStoSummaries)){
            if (outData.getMonthlyPlanVos().size() == 0 || outData.getMonthlyPlanVos() == null) {
                //去年的
                dayGrossMarginLast = salesStoSummaries.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率

                dailyProfitAmtLast = salesStoSummaries.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额

                dailyPayAmtLast = salesStoSummaries.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额

                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划
                        //System.err.println(str);
                        //lastMonthThisIssue.add(planForDailyGrowth);
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }

                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }//结束

            }else {

                // 营业额同比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyPayAmtLast.size() > 0 && dailyPayAmtLast != null) {
                    for (int i = 0; i < dailyPayAmtLast.size(); i++) {
                        String dailyPayAmtD = dailyPayAmtLast.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getMonthlyPlanVos().get(i).getYoySaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);
                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigDecimalList.add(str);//添加 同比增长日均计划
                    }//营业额环比增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                }

                //营业额本期月计划

                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                if (dailyProfitAmtLast.size() > 0 && dailyProfitAmtLast != null) {
                    for (int i = 0; i < dailyProfitAmtLast.size(); i++) {
                        String dailyProfitAmtD = dailyProfitAmtLast.get(i);//环比日均毛利额
                        String momSaleGross = outData.getMonthlyPlanVos().get(i).getYoySaleGross();//环比毛利额 用户输入的 momSaleAmt
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);
                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        yearOnYearGrossProfit.add(str);//添加环比 毛利额
                    }
                }
            }

        }/*else {
            bigDecimalList.add("0");//添加 同比增长日均计划
            yearOnYearGrossProfit.add("0");//添加同比 毛利额
        }*/

        //今年年开始
        List<String> dayGrossMargin = new ArrayList<>();
        List<String> dailyProfitAmt = new ArrayList<>();
        List<String> dailyPayAmt = new ArrayList<>();

        if (CollUtil.isNotEmpty(salesStoSummariesList)){
            if (outData.getMonthlyPlanVos().size() == 0 || outData.getMonthlyPlanVos() == null){
                dayGrossMargin = salesStoSummariesList.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率
                System.out.println("日均毛利率："+dayGrossMargin);
                dailyProfitAmt = salesStoSummariesList.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额
                System.out.println("日均毛利额："+dailyProfitAmt);
                dailyPayAmt = salesStoSummariesList.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额
                System.out.println("营业额："+dailyPayAmt);
                List<MonthOutData> list = new ArrayList<>();
                for (int i = 0; i <dayGrossMargin.size() ; i++) {
                    MonthOutData mo = new MonthOutData();
                    mo.setDayGrossMargin(dayGrossMargin.get(i));
                    mo.setDailyProfitAmt(dailyProfitAmt.get(i));
                    mo.setDailyPayAmt(dailyPayAmt.get(i));
                    list.add(mo);
                }
                System.out.println(list.size());
                Integer second = 0;
                String keyNameY = "";
                for (int i = 0; i <salesStoSummariesList.size() ; i++) {
                    keyNameY = monthPlan+salesStoSummariesList.get(i).getBrId();
                    System.out.println(keyNameY);
                    second = 30 * 60;//小时 分 秒
                    uuId.add(keyNameY);
                    redisManager.set(keyNameY, JSON.toJSON(list.get(i)).toString(),second.longValue());
                }
               /* System.err.println("1"+dayGrossMargin);
                System.err.println("2"+dailyProfitAmt);
                System.err.println("3"+dailyPayAmt);//去年  今年的*/
                // 营业额同期增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                if (dailyProfitAmt.size() > 0 && dailyProfitAmt != null) {
                    for (int i = 0; i < dailyPayAmt.size(); i++) {
                        String dailyPayAmtD = dailyProfitAmt.get(i);// 获取上期 环比日均营业额
                        //环比毛利额 日均
                        String userYoySale = outData.getMomSaleAmt();//同比营业额 用户输入  yoySaleAmt
                        userYoySale = userYoySale.replace("%", "");
                        Float f = Float.valueOf(userYoySale) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                        BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                        //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                        planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                        planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                        planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planForDailyGrowth.toString();
                        bigMomSaleGross.add(str);//添加 同比增长日均计划
                        //lastMonthThisIssue.add(planForDailyGrowth);
                    }//营业额同期增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑

                }else {
                    bigMomSaleGross.add("0");//添加 同比增长日均计划
                }
                //
                if (dailyPayAmt.size() > 0 && dailyPayAmt != null) {
                    //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                    BigDecimal planMomSaleGross = null; //
                    for (int i = 0; i < dailyPayAmt.size(); i++) {
                        String dailyProfitAmtD = dailyPayAmt.get(i);//环比日均毛利额
                        String momSaleGross = outData.getMomSaleGross();//环比毛利额 用户输入的
                        momSaleGross = momSaleGross.replace("%", "");
                        Float f = Float.valueOf(momSaleGross) / 100;
                        BigDecimal decimal = new BigDecimal(f);

                        BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                        //计算
                        BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                        planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                        planMomSaleGross = bigDecimal.add(planMomSaleGross);
                        planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                        String str = planMomSaleGross.toString();
                        System.err.println("修改前:" + str);
                        chainRatioGrossProfit.add(str);//添加环比 毛利额
                        //System.err.println(str);
                    }
                }else {
                    chainRatioGrossProfit.add("0");//添加环比 毛利额
                }
                //毛利额 环比增长日均计划结束

            }else{
                // String keyNames = "";
                for (int i = 0; i <outData.getMonthlyPlanVos().size() ; i++) {

                }
                /*Boolean f = redisManager.hasKey(keyNames);
                if (f){
                    redisManager.delete(keyNames);
                }*/

               /* dayGrossMargin = salesStoSummariesList.stream().map(book -> book.getDayGrossMargin()).collect(Collectors.toList());//日均毛利率

                dailyProfitAmt = salesStoSummariesList.stream().map(book -> book.getDailyProfitAmt()).collect(Collectors.toList());//日均毛利额

                dailyPayAmt = salesStoSummariesList.stream().map(book -> book.getDailyPayAmt()).collect(Collectors.toList());//营业额*/

                // 营业额同期增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planForDailyGrowth = null;  //momSaleAmt 营业额 环比字段
                //营业额里面的 环比
                //if (dailyProfitAmt.size() > 0 && dailyProfitAmt != null) {
                System.out.println("我是传过来的集合"+outData.getMonthlyPlanVos().size());
                for (int i = 0; i <outData.getMonthlyPlanVos().size() ; i++) {
                    String keyNames = monthPlan+outData.getMonthlyPlanVos().get(i).getBrId();
                    System.out.println(keyNames);
                    Object  obj =  redisManager.get(keyNames);
                    MonthOutData mo = JSONArray.parseObject(obj.toString(),MonthOutData.class);
                    String dailyPayAmtD = mo.getDailyPayAmt();// 获取上期 环比日均营业额
                    //String dailyPayAmtD = dailyProfitAmt.get(i);// 获取上期 环比日均营业额
                    //环比毛利额 日均
                    String userYoySale = outData.getMonthlyPlanVos().get(i).getMomSaleAmt();//同比营业额 用户输入  yoySaleAmt
                    userYoySale = userYoySale.replace("%", "");
                    Float f = Float.valueOf(userYoySale) / 100;
                    BigDecimal decimal = new BigDecimal(f);

                    BigDecimal userYoySaleAmt = decimal.abs(); //转换绝对值
                    BigDecimal bigDecimal = new BigDecimal(dailyPayAmtD);//转换
                    //同比增长日均计划 * 同比增长日均计划=同期日均×同比增长率；
                    planForDailyGrowth = bigDecimal.multiply(userYoySaleAmt);//同比增长日均计划
                    planForDailyGrowth = bigDecimal.add(planForDailyGrowth);

                    planForDailyGrowth = planForDailyGrowth.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    String str = planForDailyGrowth.toString();
                    bigMomSaleGross.add(str);//添加 同比增长日均计划
                    //lastMonthThisIssue.add(planForDailyGrowth);
                }
                //}//营业额同期增长日均计划结束 ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
                //毛利额 环比增长日均计划开始  ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
                BigDecimal planMomSaleGross = null; //
                for (int i = 0; i < outData.getMonthlyPlanVos().size(); i++) {
                    String keyNames = monthPlan+outData.getMonthlyPlanVos().get(i).getBrId();
                    System.out.println(keyNames);
                    Object  obj =  redisManager.get(keyNames);
                    MonthOutData mo = JSONArray.parseObject(obj.toString(),MonthOutData.class);
                    String dailyProfitAmtD = mo.getDailyProfitAmt();// 获取上期 环比日均营业额
                    //String dailyProfitAmtD = dailyPayAmt.get(i);//环比日均毛利额
                    String momSaleGross = outData.getMonthlyPlanVos().get(i).getMomSaleGross();//环比毛利额 用户输入的
                    momSaleGross = momSaleGross.replace("%", "");
                    Float f = Float.valueOf(momSaleGross) / 100;
                    BigDecimal decimal = new BigDecimal(f);

                    BigDecimal userMomSaleGross = decimal.abs();//转换绝对值 转换用户输入的
                    //计算
                    BigDecimal bigDecimal = new BigDecimal(dailyProfitAmtD);//转换
                    planMomSaleGross = bigDecimal.multiply(userMomSaleGross);
                    planMomSaleGross = bigDecimal.add(planMomSaleGross);
                    planMomSaleGross = planMomSaleGross.divide(new BigDecimal(1), 4, BigDecimal.ROUND_HALF_UP);
                    String str = planMomSaleGross.toString();
                    System.err.println("修改后:" + str);
                    chainRatioGrossProfit.add(str);//添加环比 毛利额
                }
            }
        }

        /*for (int i = 0; i <chainRatioGrossProfit.size() ; i++) {
            System.err.println(chainRatioGrossProfit.get(i));
        }*/
        //List<MonthOutDataVo> monthOutDataVos = new ArrayList<>();//创建对象 返回给前端的


        bigDecimalList.size();       //营业额同比 日均计划
        bigMomSaleGross.size();       //营业额环比 日均计划
        yearOnYearGrossProfit.size(); //毛利额同比 日均计划
        chainRatioGrossProfit.size();//毛利额环比 日均计划
        System.out.println("营业额同比 日均计划"+bigDecimalList.size() );
        System.out.println("营业额环比 日均计划"+bigMomSaleGross.size() );
        System.out.println("毛利额同比 日均计划"+yearOnYearGrossProfit.size());
        System.out.println("毛利额环比 日均计划"+chainRatioGrossProfit.size());

        List<String> thisPeriodTurnover = ComparisonMax(bigDecimalList,bigMomSaleGross);//本期日均计划 营业额

        for (int i = 0; i <thisPeriodTurnover.size() ; i++) {
            System.err.println("营业额："+thisPeriodTurnover.get(i).toString());
        }

        List<String> thisPeriodGrossProfit = ComparisonMax(yearOnYearGrossProfit,chainRatioGrossProfit);//本期日均计划 毛利额
        System.out.println(thisPeriodGrossProfit.size());

        for (int i = 0; i <thisPeriodGrossProfit.size() ; i++) {
            MonthOutDataVo vo = new MonthOutDataVo();
            vo.setMissionDays(thisHeaven);//天数
            vo.setBrId(brId.get(i));//地点
            vo.setBrName(brName.get(i));//门店简称
            vo.setClient(client.get(i));
            //vo.setUuid(uuId.get(i));
            if (dailyPayAmtLast.size() >0 && dailyPayAmtLast != null){
                vo.setTurnoverTSP(dailyPayAmtLast.get(i));//营业额同期日均
            }else {
                vo.setTurnoverTSP("0");//营业额同期日均
            }
            if(dailyPayAmt.size() > 0 && dailyPayAmt !=null){
                vo.setTurnoverOP(dailyPayAmt.get(i));//营业额上期日均
            }else{
                vo.setTurnoverOP("0");//营业额上期日均
            }
            if (bigDecimalList.size() > 0 && bigDecimalList != null){
                vo.setTurnoverYOYDAPlan(bigDecimalList.get(i));//营业额 同比增长日均计划
            }else {
                vo.setTurnoverYOYDAPlan("0");//营业额 同比增长日均计划
            }
            if (bigMomSaleGross.size() >0 && bigMomSaleGross != null){
                vo.setTurnoverCRDAPlan(bigMomSaleGross.get(i));//营业额 环比增长日均计划
            }else {
                vo.setTurnoverCRDAPlan("0");//营业额 环比增长日均计划
            }
            if (thisPeriodTurnover.size() >0 && thisPeriodTurnover != null){
                vo.setTurnoverDailyPlan(thisPeriodTurnover.get(i));//营业额 本期日均计划
            }else {
                vo.setTurnoverDailyPlan("0");//营业额 本期日均计划
            }
            if (thisPeriodTurnover.size()>0 && thisPeriodTurnover != null){
                String months = thisPeriodTurnover.get(i);//月计划
                BigDecimal bd = new BigDecimal(months);
                BigDecimal number = new BigDecimal(0);
                number =BigDecimal.valueOf((int)thisHeaven);
                BigDecimal multiplyVal = number.multiply(bd);
                String monthlyPlan = String.valueOf(multiplyVal);
                vo.setTurnoverDailyMonth(monthlyPlan);//营业额 本期月计划
            }else {
                vo.setTurnoverDailyMonth("0");//营业额 本期月计划
            }


            //毛利额
            if (dailyProfitAmtLast.size() >0 && dailyProfitAmtLast !=null ){
                vo.setGrossProfitTSP(dailyProfitAmtLast.get(i));
            }else {
                vo.setGrossProfitTSP("0");
            }

            //vo.setGrossProfitTSP();//毛利额 同期日均
            if (dailyProfitAmt.size() > 0 && dailyProfitAmt!=null){
                vo.setGrossProfitOP(dailyProfitAmt.get(i));//毛利额 上期日均
            }else {
                vo.setGrossProfitOP("0");//毛利额 上期日均
            }
            if (yearOnYearGrossProfit.size() > 0 && yearOnYearGrossProfit != null){
                vo.setGrossProfitYOYDAPlan(yearOnYearGrossProfit.get(i));//毛利额 同比增长日均计划
            }else {
                vo.setGrossProfitYOYDAPlan("0");//毛利额 同比增长日均计划
            }


            if (chainRatioGrossProfit.size() > 0 && chainRatioGrossProfit !=null){
                vo.setGrossProfitCRDAPlan(chainRatioGrossProfit.get(i));//毛利额 环比增长日均计划
            }else {
                vo.setGrossProfitCRDAPlan("0");//毛利额 环比增长日均计划
            }
            if (thisPeriodGrossProfit.size() > 0 && thisPeriodGrossProfit != null){
                vo.setGrossProfitDailyPlan(thisPeriodGrossProfit.get(i));//毛利额 本期日均计划
            }else {
                vo.setGrossProfitDailyPlan("0");//毛利额 本期日均计划
            }

            if (thisPeriodGrossProfit.size() >0 && thisPeriodGrossProfit !=null ){
                String months = thisPeriodGrossProfit.get(i);//月计划
                BigDecimal bd = new BigDecimal(months);
                BigDecimal number = new BigDecimal(0);
                number =BigDecimal.valueOf((int)thisHeaven);
                BigDecimal multiplyVal = number.multiply(bd);
                String monthlyPlan = String.valueOf(multiplyVal);
                vo.setGrossProfitDailyMonth(monthlyPlan);//毛利额 本期月计划
            }else {
                vo.setGrossProfitDailyMonth("0");//毛利额 本期月计划
            }

            //毛利率
            if (dayGrossMarginLast.size() > 0 && dayGrossMarginLast !=null){
                vo.setGrossMarginTSP(dayGrossMarginLast.get(i));//毛利率 同期 dayGrossMarginLast
            }else {
                vo.setGrossMarginTSP("0");//毛利率 同期 dayGrossMarginLast
            }
            if (dayGrossMargin.size() > 0 && dayGrossMargin !=null){
                vo.setGrossMarginOP(dayGrossMargin.get(i));//毛利率 上期 dayGrossMargin
            }else {
                vo.setGrossMarginOP("0");//毛利率 上期 dayGrossMargin
            }
            if (thisPeriodTurnover.size() > 0 && thisPeriodTurnover !=null){
                String bq =thisPeriodGrossProfit.get(i);
                BigDecimal q = new BigDecimal(bq);
                String bd = thisPeriodTurnover.get(i);
                BigDecimal b = new BigDecimal(bd);
                if (b.compareTo(BigDecimal.ZERO)!=0){
                    BigDecimal divideVal = q.divide(b, 2, BigDecimal.ROUND_HALF_UP);
                    vo.setGrossMarginThisPeriod(divideVal.toString());//毛利率 本期计划 thisPeriodGrossProfit /  thisPeriodTurnover
                }else {
                    vo.setGrossMarginThisPeriod("0");
                }
            }else {
                vo.setGrossMarginThisPeriod("0");
            }

            monthOutDataVos.add(vo);
        }
        map.put("monthOutDataVo",monthOutDataVos);
        //合计
        //List<String> list = new ArrayList<>();//合计list

        /*List<Integer> dayMissionDays =monthOutDataVos.stream().map(Book -> Book.getMissionDays()).collect(Collectors.toList());
        int maxCount =Collections.max(dayMissionDays);//合计任务天数*/

        List<String> turnoverTSP =monthOutDataVos.stream().map(Book -> Book.getTurnoverTSP()).collect(Collectors.toList());//营业额同期日均
        String turnoverTSPCount = count(turnoverTSP); //合计营业额 同期日均

        List<String> turnoverOP = monthOutDataVos.stream().map(Book -> Book.getTurnoverOP()).collect(Collectors.toList());//营业额上期日均
        String turnoverOPCount = count(turnoverOP); //合计营业额 上期日均

        List<String> turnoverYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverYOYDAPlan()).collect(Collectors.toList());//营业额 同比增长日均计划
        String turnoverYOYDAPlanCount = count(turnoverYOYDAPlan); //合计营业额 同比增长日均计划

        List<String> turnoverCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverCRDAPlan()).collect(Collectors.toList());//营业额 环比增长日均计划
        String turnoverCRDAPlanCount = count(turnoverCRDAPlan); //合计营业额 环比增长日均计划

        List<String> turnoverDailyPlan = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyPlan()).collect(Collectors.toList());//营业额 本期日均计划
        String turnoverDailyPlanCount = count(turnoverDailyPlan); //合计营业额 本期日均计划

        List<String> turnoverDailyMonth = monthOutDataVos.stream().map(Book -> Book.getTurnoverDailyMonth()).collect(Collectors.toList());//营业额 本期月计划

        String turnoverDailyMonthCount = count(turnoverDailyMonth); //合计营业额 本期月计划

        List<String> grossProfitTSP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitTSP()).collect(Collectors.toList());//毛利额 同期日均
        String grossProfitTSPCount = count(grossProfitTSP); //合计毛利额 同期日均

        List<String> grossProfitOP = monthOutDataVos.stream().map(Book -> Book.getGrossProfitOP()).collect(Collectors.toList());//毛利额 上期日均
        String grossProfitOPCount = count(grossProfitOP); //合计毛利额 上期日均

        List<String> grossProfitYOYDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitYOYDAPlan()).collect(Collectors.toList());//毛利额 同比增长日均计划
        String grossProfitYOYDAPlanCount = count(grossProfitYOYDAPlan); //合计毛利额 同比增长日均计划

        List<String> grossProfitCRDAPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitCRDAPlan()).collect(Collectors.toList());//毛利额 环比增长日均计划
        String grossProfitCRDAPlanCount = count(grossProfitCRDAPlan); //合计毛利额 环比增长日均计划

        List<String> grossProfitDailyPlan = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyPlan()).collect(Collectors.toList());//毛利额 本期日均计划
        String grossProfitDailyPlanCount = count(grossProfitDailyPlan); //合计毛利额 本期日均计划

        List<String> grossProfitDailyMonth = monthOutDataVos.stream().map(Book -> Book.getGrossProfitDailyMonth()).collect(Collectors.toList());//毛利额 本期月计划
        String grossProfitDailyMonthCount = count(grossProfitDailyMonth); //合计毛利额 本期月计划

        List<String> grossMarginTSP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginTSP()).collect(Collectors.toList());//毛利率 同期
        String grossMarginTSPCount = count(grossMarginTSP); //合计毛利额 同期日均

        List<String> grossMarginOP = monthOutDataVos.stream().map(Book -> Book.getGrossMarginOP()).collect(Collectors.toList());//毛利率 上期
        String grossMarginOPCount = count(grossMarginOP); //合计毛利额 同期日均

        List<String> grossMarginThisPeriod = monthOutDataVos.stream().map(Book -> Book.getGrossMarginThisPeriod()).collect(Collectors.toList());//毛利率 本期计划

        String grossMarginThisPeriodCount = count(grossMarginThisPeriod); //合计毛利额 同期日均
        //map.put("maxCount",maxCount);
        map.put("turnoverTSPCount",turnoverTSPCount);
        map.put("turnoverOPCount",turnoverOPCount);
        map.put("turnoverYOYDAPlanCount",turnoverYOYDAPlanCount);
        map.put("turnoverCRDAPlanCount",turnoverCRDAPlanCount);
        map.put("turnoverDailyPlanCount",turnoverDailyPlanCount);
        map.put("turnoverDailyMonthCount",turnoverDailyMonthCount);
        map.put("grossProfitTSPCount",grossProfitTSPCount);
        map.put("grossProfitOPCount",grossProfitOPCount);
        map.put("grossProfitYOYDAPlanCount",grossProfitYOYDAPlanCount);
        map.put("grossProfitCRDAPlanCount",grossProfitCRDAPlanCount);
        map.put("grossProfitDailyPlanCount",grossProfitDailyPlanCount);
        map.put("grossProfitDailyMonthCount",grossProfitDailyMonthCount);
        map.put("grossMarginTSPCount",grossMarginTSPCount);
        map.put("grossMarginOPCount",grossMarginOPCount);
        map.put("grossMarginThisPeriodCount",grossMarginThisPeriodCount);
        return map;
    }


    /**
     * 获取指定年月的第一天
     * @param year
     * @param month
     * @return
     */
    public static String getFisrtDayOfMonth(int year,int month){
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最小天数
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String firstDayOfMonth = sdf.format(cal.getTime());
        return firstDayOfMonth;
    }

    /**
     * 获取指定年月的最后一天
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year,int month)
    {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String lastDayOfMonth = sdf.format(cal.getTime());
        return lastDayOfMonth;
    }
    /**
     * 根据年 月 获取对应的月份 天数
     * */
    public static int getDaysByYearMonth(int year, int month) {

        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 获取上期 上月
     * @param date
     * @return
     */
    public static String getFisrtDayOfMonth(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
        Calendar calendar = Calendar.getInstance();
        Date time1 = new Date();
        try {
            // 将字符串转换成日期
            time1 = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.setTime(time1);
        // 设置为上一个月
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        return format.format(calendar.getTime());
    }

    /**
     * 获取去年 当前日期
     * @return
     */
    public static String previousYear(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
        Calendar calendar = Calendar.getInstance();
        Date time1 = new Date();
        try {
            // 将字符串转换成日期
            time1 = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.setTime(time1);
        //System.err.println("传入日期："+format.format(time1));
        calendar.add(Calendar.YEAR, -1);
        Date y = calendar.getTime();
        String year = format.format(y);
        return year;
    }

    /**
     * 比较大小
     * @param first
     * @param theSecond
     * @return
     */
    public static List<String> ComparisonMax(List<String> first, List<String> theSecond){
        int length1 =first.size();
        int length2= theSecond.size();
        List<String> list3= new ArrayList<>();//本期日均计划
        int length3=0;
        int length4=0;
        if(length1<=length2){
            length3=length1;
            length4=length2;
        }else{
            length3=length2;
            length4=length1;
        }
        for(int i=0; i<length3;i++){

            String strNum1=first.get(i);
            String strNum2=theSecond.get(i);

        /*BigDecimal num1=new BigDecimal(strNum1);
        BigDecimal num2=new BigDecimal(strNum2);*/

            if(strNum1.compareTo(strNum2) > -1){
                list3.add(strNum1);
            }else{
                list3.add(strNum2);
            }

        }
        if( length1 >  length2){
            for(int i=length3; i<length4;i++){
                String strNum3=first.get(i);
                //BigDecimal num3=new BigDecimal(strNum3);
                list3.add(strNum3);
            }

        }else if( length1<  length2){
            for(int i=length3; i<length4;i++){
                String strNum3=theSecond.get(i);
                //BigDecimal num3=new BigDecimal(strNum3);
                list3.add(strNum3);
            }
        }else{

        }
        return list3;
    }
    //转换
    public static int[] Conversion(String Conversion){
        String year = Conversion.substring(0, 4);//获取年
        String month = Conversion.substring(4, 6);//获取月
        Integer conversionYear = 0; //转换年
        Integer conversionMonth = 0;//转换月 Conversion
        try {
            conversionYear = Integer.valueOf(year).intValue();
            conversionMonth = Integer.valueOf(month).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        int[] arr = {conversionYear,conversionMonth};
        return arr;
    }

    //合计
    public static String count(List<String> list){
        List<BigDecimal> bigDecimalList1 = new ArrayList<>();
        int c =0;
        for (String value : list ){
            if (list.get(c)!="0"){
                bigDecimalList1.add(new BigDecimal(value));
            }
            c++;

        }
        BigDecimal addVal = new BigDecimal(0);
        for (int i = 0; i <bigDecimalList1.size() ; i++) {
            BigDecimal count = bigDecimalList1.get(i);
            addVal = addVal.add(count);
        }
        String str = addVal.toString(); //合计 营业额同期日均
        return str;
    }

    public static BigDecimal stringConversionBigDecimal(String str){
        BigDecimal bd=new BigDecimal(str);
        return  bd;
    }

    //去除前段 传过来的string 类型中 %
    public static BigDecimal remove(String str){
        str=str.replace("%","");
        Float f = Float.valueOf(str) / 100;
        BigDecimal decimal = new BigDecimal(f);
        decimal = decimal.divide(new BigDecimal(1),4,BigDecimal.ROUND_HALF_UP);
        return decimal;
    }

    /**
     * 获取昨天日期
     * @return
     */
    private static String yesterday(){
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE,-1);
        Date d=cal.getTime();

        SimpleDateFormat sp=new SimpleDateFormat("yyyyMMdd");
        String yesterday =sp.format(d);//获取昨天日
        return  yesterday;
    }

    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
}
