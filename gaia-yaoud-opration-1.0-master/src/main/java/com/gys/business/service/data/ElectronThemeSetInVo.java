package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 添加传参
 *
 * @author xiaoyuan on 2020/9/25
 */
@Data
public class ElectronThemeSetInVo implements Serializable {
    private static final long serialVersionUID = 7295993670539048624L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 电子券活动号
     */
    @NotBlank(message = "电子券号不可为空")
    @ApiModelProperty(value = "电子券号")
    private String gsebId;
    /**
     * 电子券描述
     */
    @NotBlank(message = "电子券描述不可为空")
    @Length(max = 20,message = "电子券描述最大长度为20")
    @ApiModelProperty(value = "电子券描述")
    private String gsebName;

    /**
     * 面值
     */
    @NotBlank(message = "面值不可为空")
    @Length(max = 16,message = "面值最大长度为16")
    @ApiModelProperty(value = "面值")
    private String gsebAmt;

    /**
     * 是否启用  N为否，Y为是
     */

    @ApiModelProperty(value = "是否启用   N为否，Y为是")
    private String gsebStatus;

    /**
     * 有效时长  按天
     */
    @ApiModelProperty(value = "有效天数")
    private String gsebDuration;

    private String gsebType;


}
