//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromCouponGrantOutData {
    private String clientId;
    private String gspcgVoucherId;
    private String gspcgSerial;
    private String gspcgProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspcgActNo;
    private String gspcgReachQty1;
    private BigDecimal gspcgReachAmt1;
    private String gspcgResultQty1;
    private String gspcgReachQty2;
    private BigDecimal gspcgReachAmt2;
    private String gspcgResultQty2;
    private String gspcgReachQty3;
    private BigDecimal gspcgReachAmt3;
    private String gspcgResultQty3;
    private String gspcgMemFlag;
    private String gspcgInteFlag;
    private String gspcgInteRate;

    public PromCouponGrantOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspcgVoucherId() {
        return this.gspcgVoucherId;
    }

    public String getGspcgSerial() {
        return this.gspcgSerial;
    }

    public String getGspcgProId() {
        return this.gspcgProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGspcgActNo() {
        return this.gspcgActNo;
    }

    public String getGspcgReachQty1() {
        return this.gspcgReachQty1;
    }

    public BigDecimal getGspcgReachAmt1() {
        return this.gspcgReachAmt1;
    }

    public String getGspcgResultQty1() {
        return this.gspcgResultQty1;
    }

    public String getGspcgReachQty2() {
        return this.gspcgReachQty2;
    }

    public BigDecimal getGspcgReachAmt2() {
        return this.gspcgReachAmt2;
    }

    public String getGspcgResultQty2() {
        return this.gspcgResultQty2;
    }

    public String getGspcgReachQty3() {
        return this.gspcgReachQty3;
    }

    public BigDecimal getGspcgReachAmt3() {
        return this.gspcgReachAmt3;
    }

    public String getGspcgResultQty3() {
        return this.gspcgResultQty3;
    }

    public String getGspcgMemFlag() {
        return this.gspcgMemFlag;
    }

    public String getGspcgInteFlag() {
        return this.gspcgInteFlag;
    }

    public String getGspcgInteRate() {
        return this.gspcgInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspcgVoucherId(final String gspcgVoucherId) {
        this.gspcgVoucherId = gspcgVoucherId;
    }

    public void setGspcgSerial(final String gspcgSerial) {
        this.gspcgSerial = gspcgSerial;
    }

    public void setGspcgProId(final String gspcgProId) {
        this.gspcgProId = gspcgProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGspcgActNo(final String gspcgActNo) {
        this.gspcgActNo = gspcgActNo;
    }

    public void setGspcgReachQty1(final String gspcgReachQty1) {
        this.gspcgReachQty1 = gspcgReachQty1;
    }

    public void setGspcgReachAmt1(final BigDecimal gspcgReachAmt1) {
        this.gspcgReachAmt1 = gspcgReachAmt1;
    }

    public void setGspcgResultQty1(final String gspcgResultQty1) {
        this.gspcgResultQty1 = gspcgResultQty1;
    }

    public void setGspcgReachQty2(final String gspcgReachQty2) {
        this.gspcgReachQty2 = gspcgReachQty2;
    }

    public void setGspcgReachAmt2(final BigDecimal gspcgReachAmt2) {
        this.gspcgReachAmt2 = gspcgReachAmt2;
    }

    public void setGspcgResultQty2(final String gspcgResultQty2) {
        this.gspcgResultQty2 = gspcgResultQty2;
    }

    public void setGspcgReachQty3(final String gspcgReachQty3) {
        this.gspcgReachQty3 = gspcgReachQty3;
    }

    public void setGspcgReachAmt3(final BigDecimal gspcgReachAmt3) {
        this.gspcgReachAmt3 = gspcgReachAmt3;
    }

    public void setGspcgResultQty3(final String gspcgResultQty3) {
        this.gspcgResultQty3 = gspcgResultQty3;
    }

    public void setGspcgMemFlag(final String gspcgMemFlag) {
        this.gspcgMemFlag = gspcgMemFlag;
    }

    public void setGspcgInteFlag(final String gspcgInteFlag) {
        this.gspcgInteFlag = gspcgInteFlag;
    }

    public void setGspcgInteRate(final String gspcgInteRate) {
        this.gspcgInteRate = gspcgInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromCouponGrantOutData)) {
            return false;
        } else {
            PromCouponGrantOutData other = (PromCouponGrantOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label263: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label263;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label263;
                    }

                    return false;
                }

                Object this$gspcgVoucherId = this.getGspcgVoucherId();
                Object other$gspcgVoucherId = other.getGspcgVoucherId();
                if (this$gspcgVoucherId == null) {
                    if (other$gspcgVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspcgVoucherId.equals(other$gspcgVoucherId)) {
                    return false;
                }

                label249: {
                    Object this$gspcgSerial = this.getGspcgSerial();
                    Object other$gspcgSerial = other.getGspcgSerial();
                    if (this$gspcgSerial == null) {
                        if (other$gspcgSerial == null) {
                            break label249;
                        }
                    } else if (this$gspcgSerial.equals(other$gspcgSerial)) {
                        break label249;
                    }

                    return false;
                }

                Object this$gspcgProId = this.getGspcgProId();
                Object other$gspcgProId = other.getGspcgProId();
                if (this$gspcgProId == null) {
                    if (other$gspcgProId != null) {
                        return false;
                    }
                } else if (!this$gspcgProId.equals(other$gspcgProId)) {
                    return false;
                }

                label235: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label235;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label235;
                    }

                    return false;
                }

                Object this$gspgSpecs = this.getGspgSpecs();
                Object other$gspgSpecs = other.getGspgSpecs();
                if (this$gspgSpecs == null) {
                    if (other$gspgSpecs != null) {
                        return false;
                    }
                } else if (!this$gspgSpecs.equals(other$gspgSpecs)) {
                    return false;
                }

                label221: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label221;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label221;
                    }

                    return false;
                }

                label214: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label214;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label214;
                    }

                    return false;
                }

                Object this$gspcgActNo = this.getGspcgActNo();
                Object other$gspcgActNo = other.getGspcgActNo();
                if (this$gspcgActNo == null) {
                    if (other$gspcgActNo != null) {
                        return false;
                    }
                } else if (!this$gspcgActNo.equals(other$gspcgActNo)) {
                    return false;
                }

                label200: {
                    Object this$gspcgReachQty1 = this.getGspcgReachQty1();
                    Object other$gspcgReachQty1 = other.getGspcgReachQty1();
                    if (this$gspcgReachQty1 == null) {
                        if (other$gspcgReachQty1 == null) {
                            break label200;
                        }
                    } else if (this$gspcgReachQty1.equals(other$gspcgReachQty1)) {
                        break label200;
                    }

                    return false;
                }

                label193: {
                    Object this$gspcgReachAmt1 = this.getGspcgReachAmt1();
                    Object other$gspcgReachAmt1 = other.getGspcgReachAmt1();
                    if (this$gspcgReachAmt1 == null) {
                        if (other$gspcgReachAmt1 == null) {
                            break label193;
                        }
                    } else if (this$gspcgReachAmt1.equals(other$gspcgReachAmt1)) {
                        break label193;
                    }

                    return false;
                }

                Object this$gspcgResultQty1 = this.getGspcgResultQty1();
                Object other$gspcgResultQty1 = other.getGspcgResultQty1();
                if (this$gspcgResultQty1 == null) {
                    if (other$gspcgResultQty1 != null) {
                        return false;
                    }
                } else if (!this$gspcgResultQty1.equals(other$gspcgResultQty1)) {
                    return false;
                }

                Object this$gspcgReachQty2 = this.getGspcgReachQty2();
                Object other$gspcgReachQty2 = other.getGspcgReachQty2();
                if (this$gspcgReachQty2 == null) {
                    if (other$gspcgReachQty2 != null) {
                        return false;
                    }
                } else if (!this$gspcgReachQty2.equals(other$gspcgReachQty2)) {
                    return false;
                }

                label172: {
                    Object this$gspcgReachAmt2 = this.getGspcgReachAmt2();
                    Object other$gspcgReachAmt2 = other.getGspcgReachAmt2();
                    if (this$gspcgReachAmt2 == null) {
                        if (other$gspcgReachAmt2 == null) {
                            break label172;
                        }
                    } else if (this$gspcgReachAmt2.equals(other$gspcgReachAmt2)) {
                        break label172;
                    }

                    return false;
                }

                Object this$gspcgResultQty2 = this.getGspcgResultQty2();
                Object other$gspcgResultQty2 = other.getGspcgResultQty2();
                if (this$gspcgResultQty2 == null) {
                    if (other$gspcgResultQty2 != null) {
                        return false;
                    }
                } else if (!this$gspcgResultQty2.equals(other$gspcgResultQty2)) {
                    return false;
                }

                Object this$gspcgReachQty3 = this.getGspcgReachQty3();
                Object other$gspcgReachQty3 = other.getGspcgReachQty3();
                if (this$gspcgReachQty3 == null) {
                    if (other$gspcgReachQty3 != null) {
                        return false;
                    }
                } else if (!this$gspcgReachQty3.equals(other$gspcgReachQty3)) {
                    return false;
                }

                label151: {
                    Object this$gspcgReachAmt3 = this.getGspcgReachAmt3();
                    Object other$gspcgReachAmt3 = other.getGspcgReachAmt3();
                    if (this$gspcgReachAmt3 == null) {
                        if (other$gspcgReachAmt3 == null) {
                            break label151;
                        }
                    } else if (this$gspcgReachAmt3.equals(other$gspcgReachAmt3)) {
                        break label151;
                    }

                    return false;
                }

                Object this$gspcgResultQty3 = this.getGspcgResultQty3();
                Object other$gspcgResultQty3 = other.getGspcgResultQty3();
                if (this$gspcgResultQty3 == null) {
                    if (other$gspcgResultQty3 != null) {
                        return false;
                    }
                } else if (!this$gspcgResultQty3.equals(other$gspcgResultQty3)) {
                    return false;
                }

                label137: {
                    Object this$gspcgMemFlag = this.getGspcgMemFlag();
                    Object other$gspcgMemFlag = other.getGspcgMemFlag();
                    if (this$gspcgMemFlag == null) {
                        if (other$gspcgMemFlag == null) {
                            break label137;
                        }
                    } else if (this$gspcgMemFlag.equals(other$gspcgMemFlag)) {
                        break label137;
                    }

                    return false;
                }

                Object this$gspcgInteFlag = this.getGspcgInteFlag();
                Object other$gspcgInteFlag = other.getGspcgInteFlag();
                if (this$gspcgInteFlag == null) {
                    if (other$gspcgInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspcgInteFlag.equals(other$gspcgInteFlag)) {
                    return false;
                }

                Object this$gspcgInteRate = this.getGspcgInteRate();
                Object other$gspcgInteRate = other.getGspcgInteRate();
                if (this$gspcgInteRate == null) {
                    if (other$gspcgInteRate == null) {
                        return true;
                    }
                } else if (this$gspcgInteRate.equals(other$gspcgInteRate)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromCouponGrantOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspcgVoucherId = this.getGspcgVoucherId();
        result = result * 59 + ($gspcgVoucherId == null ? 43 : $gspcgVoucherId.hashCode());
        Object $gspcgSerial = this.getGspcgSerial();
        result = result * 59 + ($gspcgSerial == null ? 43 : $gspcgSerial.hashCode());
        Object $gspcgProId = this.getGspcgProId();
        result = result * 59 + ($gspcgProId == null ? 43 : $gspcgProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gspcgActNo = this.getGspcgActNo();
        result = result * 59 + ($gspcgActNo == null ? 43 : $gspcgActNo.hashCode());
        Object $gspcgReachQty1 = this.getGspcgReachQty1();
        result = result * 59 + ($gspcgReachQty1 == null ? 43 : $gspcgReachQty1.hashCode());
        Object $gspcgReachAmt1 = this.getGspcgReachAmt1();
        result = result * 59 + ($gspcgReachAmt1 == null ? 43 : $gspcgReachAmt1.hashCode());
        Object $gspcgResultQty1 = this.getGspcgResultQty1();
        result = result * 59 + ($gspcgResultQty1 == null ? 43 : $gspcgResultQty1.hashCode());
        Object $gspcgReachQty2 = this.getGspcgReachQty2();
        result = result * 59 + ($gspcgReachQty2 == null ? 43 : $gspcgReachQty2.hashCode());
        Object $gspcgReachAmt2 = this.getGspcgReachAmt2();
        result = result * 59 + ($gspcgReachAmt2 == null ? 43 : $gspcgReachAmt2.hashCode());
        Object $gspcgResultQty2 = this.getGspcgResultQty2();
        result = result * 59 + ($gspcgResultQty2 == null ? 43 : $gspcgResultQty2.hashCode());
        Object $gspcgReachQty3 = this.getGspcgReachQty3();
        result = result * 59 + ($gspcgReachQty3 == null ? 43 : $gspcgReachQty3.hashCode());
        Object $gspcgReachAmt3 = this.getGspcgReachAmt3();
        result = result * 59 + ($gspcgReachAmt3 == null ? 43 : $gspcgReachAmt3.hashCode());
        Object $gspcgResultQty3 = this.getGspcgResultQty3();
        result = result * 59 + ($gspcgResultQty3 == null ? 43 : $gspcgResultQty3.hashCode());
        Object $gspcgMemFlag = this.getGspcgMemFlag();
        result = result * 59 + ($gspcgMemFlag == null ? 43 : $gspcgMemFlag.hashCode());
        Object $gspcgInteFlag = this.getGspcgInteFlag();
        result = result * 59 + ($gspcgInteFlag == null ? 43 : $gspcgInteFlag.hashCode());
        Object $gspcgInteRate = this.getGspcgInteRate();
        result = result * 59 + ($gspcgInteRate == null ? 43 : $gspcgInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromCouponGrantOutData(clientId=" + this.getClientId() + ", gspcgVoucherId=" + this.getGspcgVoucherId() + ", gspcgSerial=" + this.getGspcgSerial() + ", gspcgProId=" + this.getGspcgProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gspcgActNo=" + this.getGspcgActNo() + ", gspcgReachQty1=" + this.getGspcgReachQty1() + ", gspcgReachAmt1=" + this.getGspcgReachAmt1() + ", gspcgResultQty1=" + this.getGspcgResultQty1() + ", gspcgReachQty2=" + this.getGspcgReachQty2() + ", gspcgReachAmt2=" + this.getGspcgReachAmt2() + ", gspcgResultQty2=" + this.getGspcgResultQty2() + ", gspcgReachQty3=" + this.getGspcgReachQty3() + ", gspcgReachAmt3=" + this.getGspcgReachAmt3() + ", gspcgResultQty3=" + this.getGspcgResultQty3() + ", gspcgMemFlag=" + this.getGspcgMemFlag() + ", gspcgInteFlag=" + this.getGspcgInteFlag() + ", gspcgInteRate=" + this.getGspcgInteRate() + ")";
    }
}
