package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdIntegralExchangeSetDetailMapper;
import com.gys.business.service.GaiaSdIntegralExchangeSetDetailService;
import com.gys.business.service.data.IntegralExchangeSetDetail;
import com.gys.business.service.data.IntegralInData;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_DETAIL】的数据库操作Service实现
* @createDate 2021-11-29 09:46:42
*/
@Service
@Slf4j
public class GaiaSdIntegralExchangeSetDetailServiceImpl implements GaiaSdIntegralExchangeSetDetailService{

    @Autowired
    private GaiaSdIntegralExchangeSetDetailMapper gaiaSdIntegralExchangeSetDetailMapper;
    @Resource
    public CosUtils cosUtils;

    @Override
    public List<IntegralExchangeSetDetail> detailList(IntegralInData inData) {
        if (StringUtils.isBlank(inData.getVoucherId())){
            throw new BusinessException("单号不能为空！");
        }
        return gaiaSdIntegralExchangeSetDetailMapper.detailList(inData.getClient(),inData.getVoucherId());
    }

    @Override
    public Result exportDetailList(IntegralInData inData) {
        if (StringUtils.isBlank(inData.getVoucherId())){
            throw new BusinessException("单号不能为空！");
        }
        Result result = null;
        List<IntegralExchangeSetDetail> list = gaiaSdIntegralExchangeSetDetailMapper.detailList(inData.getClient(),inData.getVoucherId());
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setIndex(i+1);
            list.get(i).setIntegralFlag((StringUtils.isNotBlank(list.get(i).getIntegralFlag()) && list.get(i).getIntegralFlag().equals("0"))?"否":"是");
            list.get(i).setProSelfCode("\t" + (list.get(i).getProSelfCode() == null?"":list.get(i).getProSelfCode()));
        }
        String name = "价格区间导出表";
        if (list.size() > 0){
            CsvFileInfo csvFileInfo = CsvClient.getCsvByte(list,name, Collections.singletonList((short) 1));
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                assert csvFileInfo != null;
                outputStream.write(csvFileInfo.getFileContent());
                result = cosUtils.uploadFile(outputStream, csvFileInfo.getFileName());
            } catch (IOException e){
                log.error("导出文件失败:{}",e.getMessage(), e);
                throw new BusinessException("导出文件失败！");
            } finally {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    log.error("关闭流异常：{}",e.getMessage(),e);
                    throw new BusinessException("关闭流异常！");
                }
            }
            return result;
        }else{
            throw new BusinessException("提示：数据为空！");
        }
    }
}
