package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PushStoreHistoryInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "查询类型 1-周 2-月 不传默认为周")
    private String weekOrMonth;
}
