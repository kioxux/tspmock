//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PromSeriesCondsOutData {
    private String clientId;
    private String gspscVoucherId;
    private String gspscSerial;
    private String gspscProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspscSeriesId;
    private String gspscMemFlag;
    private String gspscInteFlag;
    private String gspscInteRate;

    public PromSeriesCondsOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspscVoucherId() {
        return this.gspscVoucherId;
    }

    public String getGspscSerial() {
        return this.gspscSerial;
    }

    public String getGspscProId() {
        return this.gspscProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGspscSeriesId() {
        return this.gspscSeriesId;
    }

    public String getGspscMemFlag() {
        return this.gspscMemFlag;
    }

    public String getGspscInteFlag() {
        return this.gspscInteFlag;
    }

    public String getGspscInteRate() {
        return this.gspscInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspscVoucherId(final String gspscVoucherId) {
        this.gspscVoucherId = gspscVoucherId;
    }

    public void setGspscSerial(final String gspscSerial) {
        this.gspscSerial = gspscSerial;
    }

    public void setGspscProId(final String gspscProId) {
        this.gspscProId = gspscProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGspscSeriesId(final String gspscSeriesId) {
        this.gspscSeriesId = gspscSeriesId;
    }

    public void setGspscMemFlag(final String gspscMemFlag) {
        this.gspscMemFlag = gspscMemFlag;
    }

    public void setGspscInteFlag(final String gspscInteFlag) {
        this.gspscInteFlag = gspscInteFlag;
    }

    public void setGspscInteRate(final String gspscInteRate) {
        this.gspscInteRate = gspscInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromSeriesCondsOutData)) {
            return false;
        } else {
            PromSeriesCondsOutData other = (PromSeriesCondsOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gspscVoucherId = this.getGspscVoucherId();
                Object other$gspscVoucherId = other.getGspscVoucherId();
                if (this$gspscVoucherId == null) {
                    if (other$gspscVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspscVoucherId.equals(other$gspscVoucherId)) {
                    return false;
                }

                Object this$gspscSerial = this.getGspscSerial();
                Object other$gspscSerial = other.getGspscSerial();
                if (this$gspscSerial == null) {
                    if (other$gspscSerial != null) {
                        return false;
                    }
                } else if (!this$gspscSerial.equals(other$gspscSerial)) {
                    return false;
                }

                label134: {
                    Object this$gspscProId = this.getGspscProId();
                    Object other$gspscProId = other.getGspscProId();
                    if (this$gspscProId == null) {
                        if (other$gspscProId == null) {
                            break label134;
                        }
                    } else if (this$gspscProId.equals(other$gspscProId)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label127;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$gspgSpecs = this.getGspgSpecs();
                    Object other$gspgSpecs = other.getGspgSpecs();
                    if (this$gspgSpecs == null) {
                        if (other$gspgSpecs == null) {
                            break label120;
                        }
                    } else if (this$gspgSpecs.equals(other$gspgSpecs)) {
                        break label120;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                label106: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label106;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label106;
                    }

                    return false;
                }

                Object this$gspscSeriesId = this.getGspscSeriesId();
                Object other$gspscSeriesId = other.getGspscSeriesId();
                if (this$gspscSeriesId == null) {
                    if (other$gspscSeriesId != null) {
                        return false;
                    }
                } else if (!this$gspscSeriesId.equals(other$gspscSeriesId)) {
                    return false;
                }

                label92: {
                    Object this$gspscMemFlag = this.getGspscMemFlag();
                    Object other$gspscMemFlag = other.getGspscMemFlag();
                    if (this$gspscMemFlag == null) {
                        if (other$gspscMemFlag == null) {
                            break label92;
                        }
                    } else if (this$gspscMemFlag.equals(other$gspscMemFlag)) {
                        break label92;
                    }

                    return false;
                }

                Object this$gspscInteFlag = this.getGspscInteFlag();
                Object other$gspscInteFlag = other.getGspscInteFlag();
                if (this$gspscInteFlag == null) {
                    if (other$gspscInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspscInteFlag.equals(other$gspscInteFlag)) {
                    return false;
                }

                Object this$gspscInteRate = this.getGspscInteRate();
                Object other$gspscInteRate = other.getGspscInteRate();
                if (this$gspscInteRate == null) {
                    if (other$gspscInteRate != null) {
                        return false;
                    }
                } else if (!this$gspscInteRate.equals(other$gspscInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromSeriesCondsOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspscVoucherId = this.getGspscVoucherId();
        result = result * 59 + ($gspscVoucherId == null ? 43 : $gspscVoucherId.hashCode());
        Object $gspscSerial = this.getGspscSerial();
        result = result * 59 + ($gspscSerial == null ? 43 : $gspscSerial.hashCode());
        Object $gspscProId = this.getGspscProId();
        result = result * 59 + ($gspscProId == null ? 43 : $gspscProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gspscSeriesId = this.getGspscSeriesId();
        result = result * 59 + ($gspscSeriesId == null ? 43 : $gspscSeriesId.hashCode());
        Object $gspscMemFlag = this.getGspscMemFlag();
        result = result * 59 + ($gspscMemFlag == null ? 43 : $gspscMemFlag.hashCode());
        Object $gspscInteFlag = this.getGspscInteFlag();
        result = result * 59 + ($gspscInteFlag == null ? 43 : $gspscInteFlag.hashCode());
        Object $gspscInteRate = this.getGspscInteRate();
        result = result * 59 + ($gspscInteRate == null ? 43 : $gspscInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromSeriesCondsOutData(clientId=" + this.getClientId() + ", gspscVoucherId=" + this.getGspscVoucherId() + ", gspscSerial=" + this.getGspscSerial() + ", gspscProId=" + this.getGspscProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gspscSeriesId=" + this.getGspscSeriesId() + ", gspscMemFlag=" + this.getGspscMemFlag() + ", gspscInteFlag=" + this.getGspscInteFlag() + ", gspscInteRate=" + this.getGspscInteRate() + ")";
    }
}
