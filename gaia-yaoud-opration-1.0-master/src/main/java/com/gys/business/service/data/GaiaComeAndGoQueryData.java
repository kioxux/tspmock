package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:53 2021/7/22
 * @Description：往来管理明细查询条件
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoQueryData {

    @ApiModelProperty(value = "用户")
    private String client;

    @ApiModelProperty(value = "开始时间")
    private String startDate;

    @ApiModelProperty(value = "结束时间")
    private String endDate;

    @ApiModelProperty(value = "仓库编码")
    private String dcCode;

    @ApiModelProperty(value = "支付方式集合")
    private List<String> paymentIdList;

    @ApiModelProperty(value = "门店编码集合")
    public List<String> stoCodeList;
}
