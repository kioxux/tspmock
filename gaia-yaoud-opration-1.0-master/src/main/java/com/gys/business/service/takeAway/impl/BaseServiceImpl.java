package com.gys.business.service.takeAway.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaSdStockMapper;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.mapper.entity.GaiaTOrderInfo;
import com.gys.business.mapper.takeAway.BaseMapper;
import com.gys.business.mapper.takeAway.GaiaYlCooperationDao;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.common.data.PtsOrderInfo;
import com.gys.common.data.PtsOrderProInfo;
import com.gys.util.takeAway.CommPlatformsUtils;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.OrderAction;
import com.gys.util.takeAway.OrderStatus;
import com.gys.util.takeAway.jd.JdApiUtil;
import com.gys.util.takeAway.jd.SignUtil;
import com.gys.util.takeAway.ky.KyApiUtil;
import com.gys.util.takeAway.pts.PtsApiUtil;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("baseService")
@Transactional("transactionManager")
public class BaseServiceImpl implements BaseService {
    private static final Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);
    @Resource
    private BaseMapper baseMapper;
    @Resource
    private GaiaSdStockMapper gaiaSdStockMapper;
    @Autowired
    private TakeAwayService takeAwayService;
    @Resource
    private GaiaYlCooperationDao gaiaYlCooperationDao;

    /**
     * 创建操作日志map对象
     * @param operatetype
     * @param platform_type
     * @param operatename
     * @param remark
     * @return
     * @author：Li.Deman
     * @date：2018/12/14
     */
    public Map<String, Object> createOperateLogMap(String operatetype,
                                                   String platform_type,
                                                   String operatename,
                                                   String remark){
        //定义接口调用操作日志map对象
        Map<String, Object> logMap = new HashMap<String, Object>();
        //操作类型
        logMap.put("operatetype", operatetype);
        //平台类型
        logMap.put("platform_type", platform_type);
        //接口名称
        logMap.put("operatename", operatename);
        //接口说明
        logMap.put("remark", remark);
        //默认成功
        logMap.put("IsOk", "1");
        //默认reason要有这个key，一开始为空
        logMap.put("reason", null);
        return logMap;
    }

    /**
     * 设置操作日志map对象信息
     * @param paramMap
     * @param IsOk
     * @param reason
     * @author：Li.Deman
     * @date：2018/12/14
     */
    public void setOperateLogInfo(Map<String, Object> paramMap,
                                  String IsOk,String reason){
        //失败
        paramMap.put("IsOk", IsOk);
        //失败原因
        paramMap.put("reason", reason);
    }

    /**
     * 接口操作信息记录到日志表
     * @param param
     * @author：Li.Deman
     * @date：2018/12/13
     */
    public void insertOperatelogWithMap(Map<String, Object> param){
        baseMapper.insertOperatelogWithMap(param);
    }

    /**
     * 接口操作信息记录到日志表
     * @param param
     * @author：Li.Deman
     * @date：2019/1/28
     */
    public void insertOperatelogWithMap(HashMap<String, Object> param){
        baseMapper.insertOperatelogWithMap(param);
    }

    /**
     * 插入订单基础信息 | 商品明细 | 门店在途商品明细
     * 只有新增订单才会调这个方法
     * @param param
     */
    @Override
    public String insertOrderInfoWithMap(BaseOrderInfo param) {
        String result;
        try {
            //EB由于网络抖动，前几个请求还没走完，后面的就来了，导致插入order表主键冲突
            Integer integer = baseMapper.queryByPlatformOrderId(param.getPlatforms_order_id());
            if (integer != null) {
                logger.info("外卖订单插入主数据，重复插入， plaftormOrderId:{}", param.getPlatforms_order_id());
                return "error";
            }
            // 插入外卖订单信息
            if (param.getCaution() != null) {
                param.setCaution(PtsApiUtil.filterOffUtf8Mb4(param.getCaution()));
            }
            baseMapper.insertOrderInfoWithMap(param);
            logger.info("插入订单基础信息！订单ID：" + param.getOrder_id());
            // 插入外卖订单商品明细
            baseMapper.insertOrderProInfoWithList(param.getItems());
            logger.info("插入订单商品明细记录！订单ID：" + param.getOrder_id() + "商品数量：" + param.getItems().size());
            // 插入门店外卖订单在途商品明细
//            baseMapper.insertOrderProLockInfoWithList(param.getItems());
//            logger.info("插入订单门店在途商品明细记录！订单ID：" + param.getOrder_id() + "商品数量：" + param.getItems().size());
            // 插入订单历史变更信息
            baseMapper.insertOrderHistoryInfoWithEntity(param);
            logger.info("插入订单历史变更信息！订单ID：" + param.getOrder_id());
            //插入锁定订单库存功能，尚未完成。
//            Map<String, Object> proMap = new HashMap<>();
//            proMap.put("order_id",param.getOrder_id());
//            proMap.put("shop_no",param.getShop_no());
//            lockStockForOrderStatus(proMap);
            logger.info("推送库存变更信息！订单ID：" + param.getOrder_id());
            //插入锁定订单库存功能，尚未完成。
            result="ok";
        }catch (Exception e){
            result="ERROR"+e.getMessage();
            logger.info("插入订单基础信息 | 商品明细 | 门店在途商品明细失败！订单ID："
                    + param.getOrder_id()
                    + "，"
                    +e.getMessage());
        }
        return result;
    }

    /**
     * 插入订单历史变更信息
     * @param param
     * @author：Li.Deman
     * @date：2018/12/13
     */
    public String insertOrderHistoryInfoWithMap(Map<String, Object> param){
        String result;
        logger.info("插入订单历史变更信息！订单ID：" + nullToEmpty(param.get("order_id")));
        try{
            // 插入订单历史变更信息
            baseMapper.insertOrderHistoryInfoWithMap(param);
            result="ok";
        }catch (Exception e){
            result="ERROR"+e.getMessage();
        }
        return result;
    }

    /**
     * 更新订单基础信息 | 门店在途商品明细
     * 当前针对订单库存表的操作，适用于全额退款
     * 但是不适用于部分退款
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2018/12/26
     */
    @Override
    public String updateOrderInfoWithMap(Map<String, Object> param) {
        String result;
        try{
            //todo 删除在途明细,需要根据状态检查
            String status = nullToEmpty(param.get("status"));
            String action = nullToEmpty(param.get("action"));
            String orderId = nullToEmpty(param.get("order_id"));
            //2-已配送 3-已完成 4-已取消
            if("2".equals(status)
                    || "3".equals(status)
                    || "4".equals(action)){
                // 删除在途商品
//                logger.info("删除订单门店在途商品明细记录！订单ID：" + orderId);
//                baseMapper.deleteOrderProLockById(orderId);
                //一旦删除变更了库存锁定表，就要重新推送库存到各个平台
//                asyncUpdateStockForOrderStatus(param);
            }
            // 更新订单基础信息
            logger.info("更新订单基础信息！订单ID：" + orderId + "状态：" + status);
            baseMapper.updateOrderInfoWithMap(param);
            //每一次更新订单信息，也要存入到订单历史表
            logger.info("插入订单历史变更信息！订单ID：" + orderId + "状态：" + status);
            baseMapper.insertOrderHistoryInfoWithMap(param);
            result="ok";
        }catch (Exception e){
            result="ERROR"+e.getMessage();
            logger.info("更新订单信息|历史变更信息|门店在途明细报错！" + e.getMessage());
        }
        return result;
    }

    public void ddkyUpdateShippingFee(String platformOrderId, String shippingFee) {
        baseMapper.ddkyUpdateShippingFee(platformOrderId, shippingFee);
    }

    @Override
    public String updateRecipientPhone(List<Map<String, String>> items) {
        try {
            baseMapper.updateRecipientPhone(items);
            return "ok";
        } catch (Exception e) {
            logger.error("更新订单客户电话报错，inData:{}, error:{}", JSON.toJSONString(items), e.getMessage());
            return "error";
        }
    }

    /**
     * 更新订单基础信息|订单明细信息|订单库存锁定信息
     * 目前仅支持部分退款
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2018/12/26
     */
    @Override
    public String updateOrderInfoWithEntity(BaseOrderInfo param){
        String result;
        try{
            // 更新外卖订单信息
            baseMapper.updateOrderInfoWithEntity(param);
            logger.info("更新外卖订单信息！订单ID：" + param.getOrder_id());
            // 更新订单明细信息
            baseMapper.updateOrderProInfoWithList(param.getItems());
            logger.info("更新订单明细信息！订单ID：" + param.getOrder_id() + "商品数量：" + param.getItems().size());
            // 更新订单库存锁定信息
//            baseMapper.updateOrderProLockInfoWithList(param.getItems());
//            logger.info("更新订单库存锁定信息！订单ID：" + param.getOrder_id() + "商品数量：" + param.getItems().size());
            // 插入订单历史变更信息
            baseMapper.insertOrderHistoryInfoWithEntity(param);
            logger.info("插入订单历史变更信息！订单ID：" + param.getOrder_id());
//            // 插入订单申请售后记录
//            baseMapper.insertOrderAfterServiceWithEntity(param);
            // 订单申请售后记录表更新（走的是存储过程）
//            baseMapper.orderAfterServiceWithEntity(param);
//            logger.info("插入订单申请售后记录！订单ID：" + param.getOrder_id());
            //一旦更新了库存锁定表，就要推送各个外卖平台，重新更新库存
//            Map<String, Object> paramMap=new HashMap<>();
//            paramMap.put("order_id",param.getOrder_id());
//            paramMap.put("shop_no",param.getShop_no());
//            asyncUpdateStockForOrderStatus(paramMap);
            result="ok";
        }catch (Exception e){
            result="ERROR:"+e.getMessage();
        }
        return result;
    }

    /**
     * 更新订单配送员信息
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2018/11/30
     */
    @Override
    public int updateOrderDispatchInfoWithMap(Map<String, Object> param){
        // 更新订单配送员信息
        int ret = baseMapper.updateOrderDispatchInfoWithMap(param);
        logger.info("更新订单配送员信息！订单ID：" + nullToEmpty(param.get("order_id")));
        return ret;
    }

    /**
     *
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> selectOrderInfoByMap(Map<String, Object> param) {
        Map<String, Object> result = new HashMap<String, Object>();
        try{
            result = baseMapper.selectOrderInfoByMap(param);
            logger.debug("查询订单信息！"+ String.valueOf(result));
            if(result!=null){
                result.put("result","ok");
            }else{
                result = new HashMap<String, Object>();
                result.put("result","NG");
            }
        }catch (Exception e){
            result = new HashMap<String, Object>();
            result.put("result","ERROR" + e.getMessage());
            logger.info("查询订单信息失败！"+e.getMessage());
        }
        return result;
    }

    /**
     * 字符串转换函数
     * @param param 参数
     * @return String
     */
    public static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }
    }

    /**
     * @Description: PTS更新库存接口。
     * @Param: [request] 传入JSON数据
     * @return: java.util.Map<java.lang.String , java.lang.Object>   回传JSON数据
     * @Author: Li.Deman
     * @Date: 2018/11/21
     * accessToken  MT、JD使用参数
     */
    @Override
    public String updateStockWithMap(Map<String, Object> param, String accessToken) {
        String result = "";
        List<Map<String, Object>> shopList = (List<Map<String, Object>>) param.get("shop");
        //对应门店下面的品种都要更新库存，实际上也是多个对象
        List<Map<String, Object>> proList = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> mtList = new ArrayList<Map<String, Object>>();
        GaiaTAccount account = (GaiaTAccount) param.get("account");
        //每一个shop，下面还有json数组对象
        for (Map<String, Object> shopMap : shopList) {
            //定义一个map对象，局部接收美团库存接口参数对象
            Map proMt = new HashMap();
            String shop_no = nullToEmpty(shopMap.get("shop_no"));
            //要重命名门店参数
            proMt.put("app_poi_code", shop_no);
            //美团接口每个门店下的药品
            List<Map<String, Object>> proMtList = new ArrayList<Map<String, Object>>();
            //获取medicine，又是一个json数组
            List<Map<String, Object>> medicineList = (List<Map<String, Object>>) shopMap.get("medicine");
            for (Map medicineMap : medicineList) {
                //再定义美团接口每个药品对象
                Map promedicineMt = new HashMap();
                promedicineMt.put("medicine_code", medicineMap.get("medicine_code"));
                promedicineMt.put("app_medicine_code", medicineMap.get("medicine_code"));
                promedicineMt.put("app_poi_code", shopMap.get("shop_no"));
                String stockStr = nullToEmpty(medicineMap.get("stock"));
                //以后看看是否还要判断传输的库存值是否是有效数字？
                if (stockStr.equals("")) {
                    logger.info("更新门店药品库存，" +
                            "PTS没有传入药品库存数量，药店编号："
                            + shop_no + "药品编码："
                            + nullToEmpty(medicineMap.get("medicine_code")));
                    result += "PTS没有传入药品库存数量，药店编号："
                            + shop_no + "药品编码："
                            + nullToEmpty(medicineMap.get("medicine_code"))
                            + "\n";
                    continue;
                }
                int stock = Double.valueOf(stockStr).intValue();
                if (stock<0) {
                    stock = 0;
                }
                promedicineMt.put("stock", stock);
                /**
                 * 查询订单库存锁定表，获取对应药品库存
                 * 对于订单库存锁定表，
                 * 查询时需要关心是否解锁
                 */
                //新的库存减去锁定库存，是需要传输给美团新的库存--错误的锁定库存更新逻辑。
                //之前的代码写法是错误的。更新stock栏位，不可以减去锁定库存之后再更新。WTF。
                //修改之前的代码逻辑，重新发布2019-01-06，秦青。
                proMtList.add(promedicineMt);
                Map proMap = new HashMap();
                proMap.put("app_medicine_code", promedicineMt.get("app_medicine_code"));
                proMap.put("app_poi_code", promedicineMt.get("app_poi_code"));
                proMap.put("stock", stock);
                proList.add(proMap);
            }
            proMt.put("medicine_data", proMtList);
            mtList.add(proMt);
        }
        //更新库存信息
        String source=String.valueOf(param.get("source"));
        String secret=String.valueOf(param.get("secret"));
        Map<String,String> stoData = new HashMap<>();
        stoData.put("source",source);
        stoData.put("secret",secret);
        stoData.put("platform", String.valueOf(param.get("platform")));
        if (proList.size() > 0) {
            String resultForXB = execMedicineStockUpdate(proList);
            if (resultForXB.equals("ok")) {
                logger.info("更新平台门店药品库存，" + "药品数量：" + proList.size());
                /**
                 * 既然更新药德系统，也要更新外卖平台，更新他们的库存
                 * 按照现在PTS传输的数据，一次检查多家门店药品库存，
                 * 将发生变化的全部怼过来，这样涉及到循环调用外卖平台接口
                 * 先以美团为例
                 * 这边先是更新外卖平台，更新完才更新我们药德系统，
                 * 实际上应该先更新我们药德系统，成功才能更新外卖
                 */
                for (Map<String, Object> item : mtList) {
                    String shop_no = nullToEmpty(item.get("app_poi_code"));
                    Map<String, Object> selectParam = new HashMap<>();
                    selectParam.put("shop_no", shop_no);
                    selectParam.put("medicineList", item.get("medicine_data"));
                    List<Map<String, Object>> medicineList = selectStoreMedicineStockLockByMap(selectParam);

                    Map<String,Object> ptsMedicineMap=new HashMap<>();
                    for (Map<String,Object> m :(List<Map<String, Object>>) item.get("medicine_data")){
                        ptsMedicineMap.put(nullToEmpty(item.get("app_medicine_code")) ,m);
                    }
                    String editMessage="";
                    for(Map<String, Object> m:medicineList ){
                        if (StringUtils.isBlank(nullToEmpty( m.get("stock")))){
                            Map<String,Object> ptsm= (Map<String,Object>)ptsMedicineMap.get(m.get("app_medicine_code"));
                            if (!StringUtils.isBlank(nullToEmpty(ptsm.get("stock")))){
                                m.put("stock", ptsm.get("stock"));
                                editMessage=editMessage+nullToEmpty(m.get("app_medicine_code"))+":"+nullToEmpty(ptsm.get("stock"))+";";
                            }else{
                                m.put("stock", "0");
                                editMessage=editMessage+nullToEmpty(m.get("app_medicine_code"))+":"+"00"+";";
                            }
                        }
                    }
                    if (!StringUtils.isBlank(editMessage)){
                        logger.info("库存更新发生缓存库无数据情况：" + editMessage);
                    }

                    String cooperation = gaiaYlCooperationDao.getCooperationByClient(account.getClient());
                    if (medicineList.size()==((List<Map<String, Object>>) item.get("medicine_data")).size()) {
                        logger.info("药德缓存商品正常，采用缓存数据更新。" + "药品数量：" + proList.size());
                        String resultForMt = CommPlatformsUtils.medicineStock(shop_no, medicineList, stoData, accessToken, account, cooperation);
                        if (!resultForMt.startsWith("ok")) {
                            result += resultForMt;
                        }
                    }else{
                        logger.info("药德缓存无此商品或商品不全，直接更新。" + "药品数量：" + proList.size());
                        String resultForMt = CommPlatformsUtils.medicineStock(shop_no, (List<Map<String, Object>>) item.get("medicine_data"), stoData, accessToken, account, cooperation);
                        if (!resultForMt.startsWith("ok")) {
                            result += resultForMt;
                        }
                    }
                }
            } else {
                result += resultForXB;
            }
        }
        //药德系统都没有这些记录
        if (result.equals("")) result = "ok";
        return result;
    }

    /**
     * 京东API：售后单申请消息
     * true: 处理成功
     */
    public boolean applyAfterSaleBillForJd(Map<String, Object> paramMap, String token, String appKey, String afterSaleOrderId, String appSecret) {
        // 1.查询售后单详情
        Map<String, Object> orderForAfterSale = JdApiUtil.orderAfs(appKey, appSecret, token, afterSaleOrderId);
        if (!"ok".equals(orderForAfterSale.get("result"))) {
            logger.info("京东到家-推送售后单申请消息,获取售后单失败, token:{}, appKey:{}, afterSaleOrderId:{}, result:{}",
                    token, appKey, afterSaleOrderId, JSON.toJSONString(orderForAfterSale));
            return false;
        }
        //获取平台订单ID
        String order_id = nullToEmpty(orderForAfterSale.get("orderId"));

        //获取到原平台单号后获取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_JD, order_id);
        if (orderInfo == null) {
            logger.info("美团推送售后单申请消息, stoCode = null , pass");
            return false;
        }
        String stoCode = orderInfo.getStoCode();
        String client = orderInfo.getClient();
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_JD, appKey, stoCode, client);
        if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
            logger.error("京东到家-推送售后单申请消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", order_id, appKey, stoCode);
            return false;
        }

        //1.校验接口参数
        int resultForValid = validApiParamForJd(paramMap, appSecret);
        if (resultForValid == 0) {
            //接口存活验证成功！
            return true;
        } else if (resultForValid == 2) {
            //京东订单推送API签名验证失败
            Map<String, Object> logMap = createOperateLogMap("5", "3", "applyAfterSaleBillForJd", "京东到家API：售后单申请消息");
            setOperateLogInfo(logMap, "0", "京东订单推送API签名验证失败。" + "平台订单ID：" + order_id);
            insertOperatelogWithMap(logMap);
            return false;
        }

        orderForAfterSale.put("platforms_order_id", order_id);
        orderForAfterSale.put("order_id", "");
        orderForAfterSale.put("platforms", Constants.PLATFORMS_JD);
        /**
         * 售后单状态（10:待审核,20:待取件,30:退款处理中,
         * 31:待商家收货审核,32:退款成功,33:退款失败,
         * 40:审核不通过-驳回,50:客户取消,60:商家收货审核不通过,
         * 70:已解决, 91:直赔,92:直赔成功,93:直赔失败,90:待赔付,
         * 110:待退货,111:取货成功,112:退货成功-待退款,
         * 113:退货失败,114:退货成功）
         */
        if (orderForAfterSale.get("afsServiceState").equals("10")) {
            List<Map<String, Object>> refundList = (List<Map<String, Object>>) orderForAfterSale.get("items");
            //订单+订单明细。model+items
            PtsOrderInfo ptsOrderInfo = GetOrderInfo(orderForAfterSale);
            if (ptsOrderInfo.getResult().equals("ok")) {
                BaseOrderInfo baseOrderInfo = getBaseOrderInfo(afterSaleOrderId, orderForAfterSale, ptsOrderInfo);
                List<BaseOrderProInfo> items = new ArrayList<>();
                Map<String, Object> proMap = null;
                for (int i = 0; i < refundList.size(); i++) {
                    proMap = refundList.get(i);
                    String medicine_code = nullToEmpty(proMap.get("medicine_code"));
                    BaseOrderProInfo item = new BaseOrderProInfo();
                    item.setOrder_id(ptsOrderInfo.getOrder_id());
                    item.setMedicine_code(medicine_code);
                    item.setRefund_qty(nullToEmpty(proMap.get("refund_qty")));
                    item.setRefund_price(nullToEmpty(proMap.get("refund_price")));
                    List<PtsOrderProInfo> ptsList = ptsOrderInfo.getItems().stream().filter((PtsOrderProInfo p) ->
                            medicine_code.equals(p.getMedicine_code())).collect(Collectors.toList());
                    //如果有，那么取第一个
                    if (ptsList.size() > 0) {
                        item.setSequence(ptsList.get(0).getSequence());
                    }
                    items.add(item);
                }
                baseOrderInfo.setItems(items);
                //1.小宝系统更新一份，成功OK，即更新PTS一份
                String resultForXB = updateOrderInfoWithEntity(baseOrderInfo);
                if (resultForXB.equals("ok")) {
                    Map<String, Object> inData = getOrderInfoToPTSByMapForRefund(baseOrderInfo);
                    String shop_no = nullToEmpty(inData.get("shop_no"));
                    String orderIdTemp = nullToEmpty(inData.get("order_id"));
                    if ("".equals(orderIdTemp) || "".equals(shop_no)) return false;
                    String resultParamChange = nullToEmpty(inData.get("result"));
                    if (resultParamChange.equals("ok")) {
                        String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                        inData.put("storeLogo", storeLogo);
                        takeAwayService.pushToStoreMq(client, stoCode, "O2O", "cancel", inData);
                        return true;
                    }
                }
            }
        } else if (orderForAfterSale.get("afsServiceState").equals("32")) {
            //考虑再接收一下：32:退款成功
            Map<String, Object> retMap = selectOrderInfoByMap(orderForAfterSale);
            if (nullToEmpty(retMap.get("result")).equals("ok")) {
                /**
                 * 订单退款。订单退款状态，对于小宝来说就是外卖平台同意退款，
                 * 已发生退款，区别于订单取消
                 */
                retMap.put("status", "5");
                //同意用户退款申请
                retMap.put("action", "6");
                //之所以加这个，是为了记录到小宝订单系统
                retMap.put("last_action", "6");
                //取消原因
                retMap.put("reason", nullToEmpty(orderForAfterSale.get("reason")));
                //取消类型
                retMap.put("reason_code", nullToEmpty(orderForAfterSale.get("reason_code")));
                retMap.put("notice_message", "");
                String resultForXB = updateOrderInfoWithMap(retMap);
                //无论执行结果如何，都不需要将信息推送给PTS
                return true;
            }
        }
        return false;
    }

    private BaseOrderInfo getBaseOrderInfo(String afterSaleOrderId, Map<String, Object> orderForAfterSale, PtsOrderInfo ptsOrderInfo) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        baseOrderInfo.setPlatforms(ptsOrderInfo.getPlatforms());
        baseOrderInfo.setOrder_id(ptsOrderInfo.getOrder_id());
        baseOrderInfo.setPlatforms_order_id(ptsOrderInfo.getPlatforms_order_id());
        baseOrderInfo.setShop_no(ptsOrderInfo.getShop_no());
        baseOrderInfo.setRecipient_address(ptsOrderInfo.getRecipient_address());
        baseOrderInfo.setRecipient_name(ptsOrderInfo.getRecipient_name());
        baseOrderInfo.setRecipient_phone(ptsOrderInfo.getRecipient_phone());
        baseOrderInfo.setShipping_fee(ptsOrderInfo.getShipping_fee());
        baseOrderInfo.setTotal_price(ptsOrderInfo.getTotal_price());
        baseOrderInfo.setOriginal_price(ptsOrderInfo.getOriginal_price());
        baseOrderInfo.setCustomer_pay(ptsOrderInfo.getCustomer_pay());
        baseOrderInfo.setCaution(ptsOrderInfo.getCaution());
        baseOrderInfo.setShipper_name(ptsOrderInfo.getShipper_name());
        baseOrderInfo.setShipper_phone(ptsOrderInfo.getShipper_phone());
        baseOrderInfo.setStatus(ptsOrderInfo.getStatus());
        baseOrderInfo.setAction("5");
        baseOrderInfo.setIs_invoiced(ptsOrderInfo.getIs_invoiced());
        baseOrderInfo.setInvoice_title(ptsOrderInfo.getInvoice_title());
        baseOrderInfo.setTaxpayer_id(ptsOrderInfo.getTaxpayer_id());
        baseOrderInfo.setDelivery_time(ptsOrderInfo.getDelivery_time());
        baseOrderInfo.setPay_type(ptsOrderInfo.getPay_type());
        baseOrderInfo.setOrder_info(ptsOrderInfo.getOrder_info());
        baseOrderInfo.setCreate_time(ptsOrderInfo.getCreate_time());
        baseOrderInfo.setUpdate_time(ptsOrderInfo.getUpdate_time());
        //reason_code
        baseOrderInfo.setReason(nullToEmpty(orderForAfterSale.get("reason")));
        baseOrderInfo.setDay_seq(ptsOrderInfo.getDay_seq());
        baseOrderInfo.setRefund_money(nullToEmpty(orderForAfterSale.get("refund_money")));
        baseOrderInfo.setNotice_message("refund");
        //售后单号
        baseOrderInfo.setAfsServiceOrder(afterSaleOrderId);
        return baseOrderInfo;
    }

    /**
     * 京东到家：推送API接口参数验证
     *
     * @param paramMap
     * @return
     * @author：Li.Deman
     * @date：2019/1/24
     */
    @Override
    public int validApiParamForJd(Map<String, Object> paramMap, String appSecret) {
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String app_key = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id
        String sign = nullToEmpty(paramMap.get("sign"));// 签名结果
        logger.debug("京东订单号： " + paramMap.toString());
        if (StrUtil.isEmpty(token) && StrUtil.isEmpty(app_key) && StrUtil.isEmpty(sign)) {
            logger.debug("京东接口存活验证成功！");
            return 0;
        }
        boolean flag = SignUtil.checkSign(paramMap, appSecret);
        if (flag) {
            logger.debug("京东订单推送API签名验证成功,签名: " + sign);
            return 1;
        } else {
            logger.debug("京东订单推送API签名验证失败,签名: " + sign);
            System.out.println(paramMap);
            return 2;
        }
    }

    /**
     * 药德系统更新药品库存
     * @return
     * @author：Li.Deman
     * @date:2018/12/12
     */
    public String execMedicineStockUpdate(List<Map<String, Object>> stockList){
        String result;
        try{
            //更新库存信息
            baseMapper.updateStockWithMap(stockList);
            result = "ok";
        }catch (Exception e){
            result = "ER:" + e.getMessage();
            logger.info("更新药德系统药品库存信息失败！原因：" + e.getMessage());
        }
        return result;
    }

    /**
     * @Description: PTS更新订单状态（更新药德系统订单状态）
     * @Param: [request] 传入JSON数据
     * @return: java.util.Map<java.lang.String , java.lang.Object> 回传JSON数据
     * @Author: Li.Deman
     * @Date: 2018/11/21
     */
    @Override
    public String updateOrderStatus(Map<String, Object> param) {
        String result = "";
        //param传过来是一个还未解析过的json数组，shop是key
        List<Map> data = (List) param.get("DATA");
        //每个元素，强制转换为对应的map类型
        for (Map<String, Object> proMap : data) {
            String resultLine = "";
            Map<String, Object> selectMap = new HashedMap();
            String order_id = String.valueOf(proMap.get("order_id"));
            String platform_id=String.valueOf(proMap.get("platforms_order_id"));
            String source=String.valueOf(proMap.get("source"));
            String secret=String.valueOf(proMap.get("secret"));
            Map<String,String> stoData = new HashMap<>();
            stoData.put("source",source);
            stoData.put("secret",secret);
            selectMap.put("order_id", proMap.get("order_id"));
            //写日志
            logger.info("PTS更新订单状态，" + "订单ID：" +
                    proMap.get("order_id"));
            Map<String, Object> resultMap = selectPlatformOrderIdInfoByMap(selectMap);
            //如果不是OK，那得记录不OK的原因
            if(!nullToEmpty(resultMap.get("result")).equals("ok")){
                logger.info("PTS更新订单状态，" + "订单ID：" + proMap.get("order_id") + " 订单不存在！");
                resultLine += nullToEmpty(resultMap.get("result")) + "\n";
                continue;
            }
            String shop_no = nullToEmpty(proMap.get("shop_no"));
            String platforms=nullToEmpty(resultMap.get("platforms"));
            String status = String.valueOf(proMap.get("status"));
            String action = String.valueOf(proMap.get("action"));
            //获取售后单号
            String afsServiceOrder = nullToEmpty(resultMap.get("afsServiceOrder"));
            if ((action == null) || (action == "") || (action == "null")) {
                action = String.valueOf(proMap.get("last_action"));
                proMap.put("action", action);
            }
            String last_status = String.valueOf(resultMap.get("status"));
            String resultForMt = "";
            switch (status) {
                case "1"://1:已确认订单
                    if (!last_status.equals("1")) {
                        resultLine += String.format("订单：%s 状态：%s 更新为状态：%s 失败，订单状态不可以从其他状态返回已确认状态", order_id, OrderStatus.getOrderStatusById(last_status), OrderStatus.getOrderStatusById(status));
                        break;
                    }
                    switch (action) {
                        case "0"://无动作
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：无动作 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "1"://1:新订单下发
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：新订单下发 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "2"://2:用户催单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "3"://3:门店申请退单
                            resultForMt = CommPlatformsUtils.orderCancel(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    nullToEmpty(proMap.get("reason_code")),stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        case "4"://4:用户申请退单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "5"://5:用户申请退款
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "6"://6:同意用户退款申请
                            resultForMt = CommPlatformsUtils.orderRefundAgree(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    afsServiceOrder,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        case "7"://7:驳回用户退款申请
                            resultForMt = CommPlatformsUtils.orderRefundReject(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    afsServiceOrder,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        default:
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，动作请求不存在！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                    }
                    break;
                case "2"://2:已配送订单
                    if (!(last_status.equals("1") || last_status.equals("2"))) {
                        resultLine += String.format("订单：%s 状态：%s 更新为状态：%s 失败，订单状态不可以从其他状态返回已配送状态", order_id, OrderStatus.getOrderStatusById(last_status), OrderStatus.getOrderStatusById(status));
                        break;
                    }
                    switch (action) {
                        case "":
                        case "8":
                        case "0"://无动作
                            resultForMt = CommPlatformsUtils.orderLogisticsPush(platforms, shop_no, platform_id,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        case "1"://1:新订单下发
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：新订单下发 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "2"://2:用户催单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "3"://3:门店申请退单
                            resultForMt = CommPlatformsUtils.orderCancel(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    nullToEmpty(proMap.get("reason_code")),stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        case "4"://4:用户申请退单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "5"://5:用户申请退款
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "6"://6:同意用户退款申请
                            resultForMt = CommPlatformsUtils.orderRefundAgree(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    afsServiceOrder,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        case "7"://7:驳回用户退款申请
                            resultForMt = CommPlatformsUtils.orderRefundReject(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    afsServiceOrder,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        default:
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，动作请求不存在！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                    }
                    break;
                case "3"://3:已完成订单
                    if (!(last_status.equals("1") || last_status.equals("2") || last_status.equals("3"))) {
                        resultLine += String.format("订单：%s 状态：%s 更新为状态：%s 失败，订单状态不可以从其他状态返回已完成状态！", order_id, OrderStatus.getOrderStatusById(last_status), OrderStatus.getOrderStatusById(status));
                        break;
                    }
                    switch (action) {
                        case "0"://无动作
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "1"://1:新订单下发
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：新订单下发 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "2"://2:用户催单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：用户催单 动作。", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "3"://3:门店申请退单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：门店申请退单 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "4"://4:用户申请退单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已完成订单不可以执行：用户申请退单 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "5"://5:用户申请退款
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "6"://6:同意用户退款申请
                            resultForMt = CommPlatformsUtils.orderRefundAgree(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    afsServiceOrder,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        case "7"://7:驳回用户退款申请
                            resultForMt = CommPlatformsUtils.orderRefundReject(platforms,shop_no,platform_id,
                                    nullToEmpty(proMap.get("reason")),
                                    afsServiceOrder,stoData);
                            if (!resultForMt.startsWith("ok")) {
                                resultLine += resultForMt;
                            }
                            break;
                        default:
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，动作请求不存在！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                    }
                    break;
                case "4"://4:已取消订单
                    switch (action) {
                        case "0"://无动作
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，由外卖方触发更新，不可以由PTS触发。！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "1"://1:新订单下发
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：新订单下发 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "2"://2:用户催单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：用户催单 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "3"://3:门店申请退单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：门店申请退单 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "4"://4:用户申请退单
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：用户申请退单 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "5"://5:用户申请退款
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：用户申请退款 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "6"://6:同意用户退款申请
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：同意用户退款申请 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        case "7"://7:驳回用户退款申请
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，已取消订单不可以执行：驳回用户退款申请 动作！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                        default:
                            resultLine += String.format("订单：%s 状态：%s 动作请求：%s 失败，动作请求不存在！", order_id, OrderStatus.getOrderStatusById(status), OrderAction.getOrderActionById(action));
                            break;
                    }
                    break;
                default:
                    resultLine += String.format("订单：%s 状态：%s 更新为状态：%s 失败，订单状态不存在！", order_id, OrderStatus.getOrderStatusById(last_status), OrderStatus.getOrderStatusById(status));
                    break;
            }
            if (resultLine.equals("") || resultLine.startsWith("ok")) {
                //pts推送过来的状态，只有从1变成2（已配送），药德才会接收
                if ("2".equals(status)){
                    resultMap.put("status",status);
                }
                resultMap.put("last_action",action);
                resultMap.put("reason",nullToEmpty(proMap.get("reason")));
                resultMap.put("reason_code",nullToEmpty(proMap.get("reason_code")));
                resultMap.put("shipper_name",nullToEmpty(proMap.get("shipper_name")));
                resultMap.put("shipper_phone",nullToEmpty(proMap.get("shipper_phone")));
                String resultForXB = execOrderStatus(resultMap);
                if (!resultForXB.startsWith("ok")) {
                    resultLine += resultForXB;
                }else {
                    asyncUpdateStockForOrderStatus(proMap);
                }
            }
            result += resultLine;
        }
        if (result.equals("")) result = "OK";
        return result;
    }

    /**
     * 查询平台订单详细信息
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2018/12/19
     */
    Map<String, Object> selectPlatformOrderIdInfoByMap(Map<String, Object> param){
        Map<String, Object> resultMap = null;
        try {
            resultMap = baseMapper.selectPlatformOrderIdInfoByMap(param);
            if (resultMap==null) {
                logger.info("PTS更新订单状态，" + "订单ID：" + param.get("order_id")+"，订单不存在！");
                resultMap=new HashMap<String, Object>();
                resultMap.put("result","ERROR:订单不存在！");
            }else {
                resultMap.put("result","ok");
            }
        }catch (Exception e){
            resultMap=new HashMap<String, Object>();
            resultMap.put("result","ERROR"+e.getMessage());
        }
        return resultMap;
    }

    /**
     * 异步更新药德系统订单库存
     * @param proMap
     * @author：Li.Deman
     * @date：2018/12/24
     */
    public void asyncUpdateStockForOrderStatus(Map<String,Object> proMap){
        //及时更新美团药品库存，异步队列，延迟推送
        List<Map<String, Object>> orderStockInfo = baseMapper.selectMedicineLockStockInfo(proMap);
        //肯定是一家门店的，没毛病，现在需要把所有药品加到一个medicine_data中
        if (orderStockInfo.size() > 0) {
            Map medicine_info = new HashMap();
            medicine_info.put("medicine_data", orderStockInfo);
            medicine_info.put("app_poi_code", proMap.get("shop_no"));
            //门店号要检查
            String shop_no = nullToEmpty(medicine_info.get("app_poi_code"));
            /**
             * 药品编码要检查，但是他是包含在medicine_data这个map里面的
             * 而medicine_data本身又是个List<Map<String, Object>>对象
             * 对于加到多线程里面，调用美团库存更新接口，只要具体到
             * 哪一家门店即可，因为他是以门店为单位进行更新的
             */
            if (shop_no.equals("")) return;
            System.out.println("Thread Shop_no = " + shop_no);
            try {
                String source=String.valueOf(proMap.get("source"));
                String secret=String.valueOf(proMap.get("secret"));
                Map<String,String> stoData = new HashMap<>();
                stoData.put("source",source);
                stoData.put("secret",secret);
                String json = JSON.toJSONString(medicine_info.get("medicine_data"));
                String res=CommPlatformsUtils.medicineStock(shop_no,(List<Map<String, Object>>) medicine_info.get("medicine_data"),stoData, null, null, null);//todo 这边的代码应该是没地方用到的
                logger.info("Send Medicine Stock To WaiMai,shop_no:"
                        + shop_no + " result:" + res);
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.info("Send Medicine Stock To MT,ERROR:"+ex.getMessage());
            }
        }
    }

    /**
     * @Description: 同步锁定药德订单库存
     * @Param: [proMap]
     * @return: java.lang.String
     * @Author: Qin.Qing
     * @Date: 2018/12/29
     */
    @Override
    public void lockStockForOrderStatus(Map<String,Object> proMap) {
        List<Map<String, Object>> orderStockInfo = baseMapper.selectMedicineLockStockInfo(proMap);
        if (orderStockInfo.size() > 0) {
            Map shop_medicine_data = new HashMap();
            String shop_no = String.valueOf(proMap.get("shop_no"));
            shop_medicine_data.put("medicine_data", orderStockInfo);
            shop_medicine_data.put("app_poi_code", proMap.get("shop_no"));
            try {
                Map<String,String> stoData = new HashMap<>();
                stoData.put("source",String.valueOf(proMap.get("source")));
                stoData.put("secret",String.valueOf(proMap.get("secret")));
                String json = JSON.toJSONString(shop_medicine_data.get("medicine_data"));
                String res = CommPlatformsUtils.medicineStock(shop_no, (List<Map<String, Object>>) shop_medicine_data.get("medicine_data"),stoData,null, null, null);//todo 这边的代码应该是没地方用到的
                logger.info("Send Medicine Stock To MT,shop_no:" + shop_no + " result:" + res);
                Map<String, Object> logMap = new HashMap<String, Object>();
                logMap.put("operatetype", "4");
                logMap.put("platform_type", "1");
                logMap.put("operatename", "medicineStock");
                logMap.put("remark", "平台药品API：更新锁定药品库存");
                logMap.put("IsOk", "0");
                logMap.put("reason", "门店号：" + shop_no + "药品：" + json + "错误原因：" + res);
                insertOperatelogWithMap(logMap);
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.info("Send Medicine Stock To Platforms,ERROR:" + ex.getMessage());
            }
        }
    }

    /**
     * 药德系统更新订单状态|更新订单库存锁定明细
     * @param orderMap
     * @return
     */
    public String execOrderStatus(Map<String,Object> orderMap) {
        String result;
        try {
            //更新订单状态信息
            baseMapper.updateSingleOrderStatus(orderMap);
            String status = String.valueOf(orderMap.get("status"));
            String order_id = nullToEmpty(orderMap.get("order_id"));
            if (status.equals("2")
                    || status.equals("3") ||
                    (status.equals("4"))) {
//                baseMapper.deleteOrderProLockById(order_id);
            }
            //每一次更新订单信息，也要存入到订单历史表
            if (StringUtils.isBlank(nullToEmpty(orderMap.get("last_action")))) {
                orderMap.put("last_action","0");
            }
            baseMapper.insertOrderHistoryInfoWithMap(orderMap);
            result = "ok";
        } catch (Exception e) {
            logger.info("更新药德系统订单状态失败！原因：" + e.getMessage());
            result = "ERROR" + e.getMessage();
        }
        return result;
    }

    /**
     * PTS-HANA商家取消订单
     * @param orderMap
     * @return
     * @author：Li.Deman
     * @date：2018/12/13
     */
    public String cancelOrder(Map<String,Object> orderMap){
        String result="ok";
//        String shop_no=nullToEmpty(orderMap.get("shop_no"));
//        String appId = ConfigUtil.getshop(shop_no).get("MT").get("app_id");
//        String appSecret =ConfigUtil.getshop(shop_no).get("MT").get("app_secret");
//        String orderId = nullToEmpty(orderMap.get("order_id"));
//        String reason=nullToEmpty(orderMap.get("reason"));
//        String reason_code=nullToEmpty(orderMap.get("reason_code"));
//        Map<String,Object> selectMap=new HashedMap();
//        selectMap.put("order_id",orderId);
//        selectMap.put("shop_no",shop_no);
////        Map<String, Object> resultMap = baseMapper.selectPlatformOrderIdInfoByMap(selectMap);
//        Map<String, Object> resultMap = selectPlatformOrderIdInfoByMap(selectMap);
//        if (!nullToEmpty(resultMap.get("result")).equals("ok")) {
//            logger.info("PTS更新订单状态，" + "订单ID：" + orderId + nullToEmpty(resultMap.get("result")));
//            result += nullToEmpty(resultMap.get("result"));
//        } else {
//            resultMap.put("status",nullToEmpty(orderMap.get("status")));
//            resultMap.put("reason",reason);
//            resultMap.put("reason_code",reason_code);
//            String resultForXB=execOrderStatus(resultMap);
//            if (resultForXB.equals("ok")){
//                // 3.美团取消订单
//                String resultForMt= MtApiUtil.orderCancel(appId, appSecret,
//                        nullToEmpty(resultMap.get("platforms_order_id")),
//                        reason,reason_code);
//                if (!resultForMt.startsWith("ok")){
//                    result+=resultForMt;
//                }
//                asyncUpdateStockForOrderStatus(resultMap);
//            }else {
//                result+=resultForXB;
//            }
//        }
        return result;
    }

    /**
     * 门店开店闭店信息更新
     * @param shopMap
     * @return
     * @author：Li.Deman
     * @date：2018/12/25
     */
    public String shopOpenUpdate(Map<String,Object> shopMap){
        String result;
        result = updateShopOpen(shopMap);
//        if(result.equals("ok")){
//            //攘外必须安内，只有本地更新成功，才能去更新美团
//            String shop_no = nullToEmpty(shopMap.get("shop_no"));
//            String app_id = ConfigUtil.getshop(shop_no).get("MT").get("app_id");
//            String app_secret = ConfigUtil.getshop(shop_no).get("MT").get("app_secret");
//            String open_level = nullToEmpty(shopMap.get("open_level"));
//            //调用美团门店设置为营业状态或门店设置为休息状态接口
//            result = MtApiUtil.shopOpenUpdate(app_id,app_secret,shop_no,open_level);
//        }
        return result;
    }

    /**
     *
     * @param shopMap
     * @return
     */
    public String updateShopOpen(Map<String,Object> shopMap){
        String result;
        try{
            //更新库存信息
            baseMapper.updateShopOpenWithMap(shopMap);
            result = "ok";
        }catch (Exception e){
            result = "ER:" + e.getMessage();
            logger.info("更新药德系统药品库存信息失败！原因：" + e.getMessage());
        }
        return result;
    }

    /**
     * PTS获取订单详细信息
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2018/12/24
     */
    @Override
    public PtsOrderInfo GetOrderInfo(Map<String, Object> param) {
        PtsOrderInfo orderInfo = null;
        try{
            /**
             * 获取订单主体信息
             * 有可能这样的订单在我们这边就不存在，因此，
             * 查询这样的结果，得到orderinfo结果可能为null，
             * 所以我们也要判断
             */
            orderInfo = baseMapper.GetOrderInfo(param);
            if (orderInfo!=null){
                orderInfo.setResult("ok");
            }else {
                //为空，需要重新new一个对象
                orderInfo = new PtsOrderInfo();
                orderInfo.setResult("ERROR：" + "无此平台订单号" + nullToEmpty(param.get("platforms_order_id")));
                logger.info("ERROR：" + "无此平台订单号" + nullToEmpty(param.get("platforms_order_id")));
            }
        }catch (Exception e){
            //为空，需要重新new一个对象
            orderInfo = new PtsOrderInfo();
            orderInfo.setResult("ERROR" + e.getMessage());
            logger.info("ERROR：" + "平台订单号" + nullToEmpty(param.get("platforms_order_id")) + "，" + e.getMessage());
        }
        return orderInfo;
    }

    /**
     * @Description: 查询药德数据库中药品信息，推送至外卖平台。
     * @Param: []
     * @return: java.util.List<java.util.Map < java.lang.String , java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2018/11/26
     */
    @Override
    public List<Map<String, Object>> queryMedicine(Map<String,Object> param) {
        List<Map<String, Object>> list=null;
        try {
            list=baseMapper.queryMedicine(param);
        }catch (Exception ex){
            logger.error(String.valueOf(ex));
        }
        return list;
    }

    /**
     * 插入门店药品信息
     * @param param
     */
    @Override
    public String insertMedicine(Map<String,Object> param){
        String result;
        try {
            if (((List<Map<String,Object>>)param.get("list")).size()>0) {
                baseMapper.insertMedicine(param);
//                List<String> platformsList=new ArrayList<>();
//                for(String colName: param.get(0).keySet()){
//                    if (colName.endsWith("_PRICE")) {
//                        String platforms= colName.substring(0,colName.length()-"_PRICE".length());
//                        platformsList.add(platforms);
//                    }
//                }
//                for (String platforms:platformsList){
//                    String colName=platforms+"_PRICE";
//                    for (Map<String,Object> m:param){
//                        m.put("PRICE_PLATFORMS",m.get(colName));
//                        m.put("PLATFORMS",platforms.toUpperCase());
//                    }
//                    baseMapper.insertMedicinePlatforms(param);
//                }
            }
            result="ok";
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result="ERROR:"+ex.getMessage();
        }
        return result;
    }

    /**
     * 更新门店药品信息
     * @param param
     */
    @Override
    public String updateMedicine(Map<String,Object> param) {
        String result;
        try {
            if (((List<Map<String, Object>>) param.get("list")).size() > 0) {
                baseMapper.updateMedicine(param);
            }
            result = "ok";
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result = "ERROR:" + ex.getMessage();
        }
        return result;
//        List<String> platformsList=new ArrayList<>();
//        for(String colName: param.keySet()){
//            if (colName.endsWith("_PRICE")) {
//                String platforms= colName.substring(0,colName.length()-"_PRICE".length());
//                platformsList.add(platforms);
//            }
//        }
//        for (String platforms:platformsList){
//            String colName=platforms+"_PRICE";
//            param.put("PRICE_PLATFORMS",param.get(colName));
//            param.put("PLATFORMS",platforms.toUpperCase());
//            baseMapper.updateMedicinePlatforms(param);
//        }
    }

    /**
     * @Description: 更新门店药品状态
     * @Param: [param]
     * @return: void
     * @Author: Qin.Qing
     * @Date: 2018/11/27
     */
    @Override
    public String updateMedicineStatus(Map<String, Object> param){
        String result="";
        try {
            if (((List<Map<String, Object>>) param.get("list")).size() > 0) {
                baseMapper.updateMedicineStatus(param);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result = "ERROR:" + ex.getMessage();
        }
        return result;

//        List<Map> shoplist = (List<Map>) param.get("shop");
//        for (Map shop : shoplist) {
//            List<Map> shopMedicineList = (List<Map>) shop.get("medicine");
//            for (Map medicine:shopMedicineList) {
//                if(medicine.get("last_error")!=null) {
//                    baseMapper.updateMedicineStatus(medicine);
//                    List<String> platformsList = new ArrayList<>();
//                    for (Object colObj : medicine.keySet()) {
//                        String colName = String.valueOf(colObj);
//                        if (colName.endsWith("_last_error")) {
//                            String platforms = colName.substring(0, colName.length() - "_last_error".length());
//                            if (!platforms.equals("cat")) {
//                                platformsList.add(platforms);
//                            }
//                        }
//                    }
//
//                    for (String platforms : platformsList) {
//                        String colName = platforms + "_last_error";
//                        medicine.put("last_error_platforms", medicine.get(colName));
//                        medicine.put("platforms", platforms.toUpperCase());
//                        int flag = 3;
//                        if (!String.valueOf(medicine.get(colName)).startsWith("ok")) {
//                            flag = 11;
//                        }
//                        medicine.put("medicine_flag_platforms", flag);
//                        baseMapper.updateMedicinePlatformsStatus(medicine);
//                    }
//                }
//                if (medicine.get("cat_last_error")!=null){
//                    baseMapper.updateMedicineCate(medicine);
//                }
//            }
//        }
    }


    /**
     * @Description: 加载已有药品分类信息
     * @Param: []
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2018/11/28
     */
    @Override
    public List<Map<String,Object>> load_medicine_category_info(){
        List<Map<String,Object>> list=null;
        try {
            list=baseMapper.load_medicine_category_info();
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
        }
        return list;
    }

    /**
     * @Description: 插入药品分类信息
     * @Param: [param]
     * @return: void
     * @Author: Qin.Qing
     * @Date: 2018/11/28
     */
    @Override
    public String insert_medicine_category_info(List<Map<String,Object>> param){
        String result;
        try {
            if (param.size() > 0) {
                baseMapper.insert_medicine_category_info(param);
            }
            result="ok";
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result="ERROR:"+ex.getMessage();
        }
        return result;
    }


    //插入药品平台价格信息
    @Override
    public String insertMedicinePlatforms(Map<String,Object> param){
        String result;
        try {
            if (((List<Map<String, Object>>) param.get("list")).size() > 0) {
                baseMapper.insertMedicinePlatforms(param);
            }
            result="ok";
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result="ERROR:"+ex.getMessage();
        }
        return result;
    }

    //更新药品平台价格信息
    @Override
    public String updateMedicinePlatforms(Map<String, Object> param){
        String result;
        try {
            if (((List<Map<String, Object>>) param.get("list")).size() > 0) {
                baseMapper.updateMedicinePlatforms(param);
            }
            result="ok";
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result="ERROR:"+ex.getMessage();
        }
        return result;
    }

    //更新药品平台状态结果
    @Override
    public String updateMedicinePlatformsStatus(Map<String, Object> param){
        String result;
        try {
            if (((List<Map<String, Object>>) param.get("list")).size() > 0) {
                baseMapper.updateMedicinePlatformsStatus(param);
            }
            result="ok";
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result="ERROR:"+ex.getMessage();
        }
        return result;
    }

    /**
     * @Description: 获取已在线门店信息
     * @Param: []
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2018/11/27
     */
    @Override
    public List<Map<String, Object>> queryShop() {
        return baseMapper.queryShop();
    }

    /**
     * @Description:  加载配置信息
     * @Param: []
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2018/11/27
     */
    @Override
    public List<Map<String,Object>> loadConfig(){
        return baseMapper.loadConfig();
    }

    /**
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> loadAppSecretForJd() {
        return baseMapper.loadAppSecretForJd();
    }

    /**
     * 配置信息查询-京东（基于索引查询）
     * @return
     * @author：Li.Deman
     * @date：2019/1/18
     */
    public List<Map<String, Object>> loadAppSecretForJdByIndex() {
        return baseMapper.loadAppSecretForJdByIndex();
    }

    /**
     * @Description: 加载门店配置信息
     * @Param: []
     * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @Author: Qin.Qing
     * @Date: 2018/11/28
     */
    @Override
    public List<Map<String,Object>> loadShop(Map<String, Object> param){
        return baseMapper.loadShop(param);
    }


    /**
     * 查询开关设置信息
     * @param param
     * @return
     */
    @Override
    public List<Map<String,Object>> selectSwitchInfo(Map<String, Object> param){
        return baseMapper.selectSwitchInfo(param);
    }

    /**
     * 查询每家门店某一种药品的库存锁定数量
     * @param param
     * @return
     */
    @Override
    public List<Map<String,Object>> selectStoreMedicineStockLockByMap(Map<String, Object> param){
        return baseMapper.selectStoreMedicineStockLockByMap(param);
    }

    //查询待处理消息队列
    @Override
    public List<Map<String,Object>> selectMessageQueue(Map<String, Object> param){
        return baseMapper.selectMessageQueue(param);
    }

    //更新事务队列
    @Override
    public void updateMessageQueue(Map<String, Object> param){
        baseMapper.updateMessageQueue(param);
    }

    //插入新的事务进事务队列
    @Override
    public void insertMessageQueue(Map<String, Object> param){
        baseMapper.insertMessageQueue(param);
    }

    @Override
    public void insertSyncObj(Map<String, Object> param) {
        baseMapper.insertSyncObj(param);
    }

    //删除排他信息
    @Override
    public void deleteSyncObj(Map<String, Object> param){
        baseMapper.deleteSyncObj(param);
    }

    @Override
    public List<Map<String, Object>> selectGroupMedicine(Map<String, Object> param){
        return baseMapper.selectGroupMedicine(param);
    }

    /**
     * 将药德系统订单对象转换为PTS需要的map对象
     * 支持部分退款和全额退款
     * @param baseOrderInfo
     * @return
     * @author：Li.Deman
     * @date：2019/1/15
     */
    public Map<String,Object> getOrderInfoToPTSByMapForRefund(BaseOrderInfo baseOrderInfo){
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try{
            resultMap.put("order_id",baseOrderInfo.getOrder_id());
            resultMap.put("platforms_order_id",baseOrderInfo.getPlatforms_order_id());
            resultMap.put("shop_no",baseOrderInfo.getShop_no());
            resultMap.put("reason",baseOrderInfo.getReason());
            resultMap.put("refund_money",baseOrderInfo.getRefund_money());
            resultMap.put("action",baseOrderInfo.getAction());
            resultMap.put("status",baseOrderInfo.getStatus());
            resultMap.put("notice_message",baseOrderInfo.getNotice_message());
            List<Map<String, Object>> detail = new ArrayList<Map<String, Object>>();
            for (BaseOrderProInfo item:baseOrderInfo.getItems()){
                Map<String, Object> itemMap = new HashMap<String, Object>();
                itemMap.put("order_id",item.getOrder_id());
                itemMap.put("medicine_code",item.getMedicine_code());
                itemMap.put("refund_qty",item.getRefund_qty());
                itemMap.put("refund_price",item.getRefund_price());
                itemMap.put("sequence",item.getSequence());
                detail.add(itemMap);
            }
            resultMap.put("detail",detail);
            resultMap.put("result","ok");
        }catch (Exception e){
            resultMap.put("result","ERROR:" + e.getMessage());
        }
        return resultMap;
    }

    //批量查询门店商品清单
    @Override
    public List<Map<String, Object>> queryMedicineByList(Map<String, Object> param){
        return baseMapper.queryMedicineByList(param);
    }

    //定量查询待处理商品清单
    @Override
    public List<Map<String, Object>> queryUnExecMedicine(Map<String, Object> param){
        return baseMapper.queryUnExecMedicine(param);
    }

    /**
     * 快药外卖生成新订单
     */
    @Override
    public void kyCreateNewOrder(String client, String stoCode, Map<String, Object> paramMap, String orderOnService, String appId, String secret) {
        //定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        //todo 数字代表的意思，数据库也没有  待整理
        Map<String, Object> logMapForOrderPay = createOperateLogMap("5", "1", "order.create", "创建订单");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForOrderPay,"1","创建订单成功。平台订单号："
//                +orderId);
        BaseOrderInfo baseOrderInfo = KyApiUtil.convertOrder(paramMap, client, stoCode);
        String shop_no = baseOrderInfo.getShop_no();
        if (!"Y".equalsIgnoreCase(orderOnService)) {
            logger.info("加盟商：[" + client + "],门店号：[" + stoCode + "]店铺Id：[" + shop_no + "]订单处理开关未打开！");
            setOperateLogInfo(logMapForOrderPay, "0",
                    "加盟商：[" + client + "],门店号：[" + stoCode + "]店铺Id：[" + shop_no + "]订单处理开关未打开！");
            //得把日志map记录到日志表呀
            insertOperatelogWithMap(logMapForOrderPay);
//            retBody.put("errno", 10012);
//            retBody.put("error", "加盟商：[" + client + "],门店号：[" + stoCode + "]店铺Id：[" + shop_no + "]订单处理开关未打开！");
        } else {
            String resultForXB = insertOrderInfoWithMap(baseOrderInfo);
            if (resultForXB.startsWith("ok")) {
                takeAwayService.pushToStoreMq(client, stoCode, "O2O", "newOrder", getOrderInfoToPTSByMap(baseOrderInfo));
                Map<String, Object> dataMap = new HashMap<>();
                dataMap.put("source_order_id", baseOrderInfo.getOrder_id());
//                retBody.put("data", dataMap);
//                retBody.put("errno", 0);
//                retBody.put("error", "success");
            } else {
                //定义一个公共的错误信息，用来重复利用
//                String reasonForOrderPay = "创建订单失败。平台订单号：" + orderId + "。";
//                retBody.put("errno", 10011);
//                retBody.put("error", reasonForOrderPay + resultForXB);
            }
        }
    }

    /**
     * 将药德系统订单对象转换为PTS需要的map对象
     * @param baseOrderInfo
     * @return
     * @author：Li.Deman
     * @date：2018/11/30
     */
    public Map<String,Object> getOrderInfoToPTSByMap(BaseOrderInfo baseOrderInfo){
        Map<String, Object> resultMap = new HashMap<>();
        try{
            resultMap.put("client",baseOrderInfo.getClient());
            resultMap.put("stoCode",baseOrderInfo.getStoCode());
            resultMap.put("caution",baseOrderInfo.getCaution());
            resultMap.put("create_time",baseOrderInfo.getCreate_time());
            resultMap.put("customer_pay",baseOrderInfo.getCustomer_pay());
            resultMap.put("delivery_time",baseOrderInfo.getDelivery_time());
            resultMap.put("invoice_title",baseOrderInfo.getInvoice_title());
            resultMap.put("is_invoiced",baseOrderInfo.getIs_invoiced());
            resultMap.put("order_id",baseOrderInfo.getOrder_id());
            resultMap.put("order_info",baseOrderInfo.getOrder_info());
            resultMap.put("original_price",baseOrderInfo.getOriginal_price());
            resultMap.put("pay_type",baseOrderInfo.getPay_type());
            resultMap.put("platforms",baseOrderInfo.getPlatforms());
            resultMap.put("platforms_order_id",baseOrderInfo.getPlatforms_order_id());
            resultMap.put("recipient_address",baseOrderInfo.getRecipient_address());
            resultMap.put("recipient_name",baseOrderInfo.getRecipient_name());
            resultMap.put("recipient_phone",baseOrderInfo.getRecipient_phone());
            resultMap.put("shipping_fee",baseOrderInfo.getShipping_fee());
            resultMap.put("shop_no",baseOrderInfo.getShop_no());
            resultMap.put("status",baseOrderInfo.getStatus());
            resultMap.put("taxpayer_id",baseOrderInfo.getTaxpayer_id());
            resultMap.put("total_price",baseOrderInfo.getTotal_price());
            resultMap.put("update_time",baseOrderInfo.getUpdate_time());
            resultMap.put("day_seq",baseOrderInfo.getDay_seq());
            resultMap.put("notice_message",baseOrderInfo.getNotice_message());
            List<Map<String, Object>> detail = new ArrayList<>();
            Integer rowindex=1;
            for (BaseOrderProInfo item:baseOrderInfo.getItems()){
                Map<String, Object> itemMap = new HashMap<>();
                itemMap.put("client",baseOrderInfo.getClient());
                itemMap.put("stoCode",baseOrderInfo.getStoCode());
                itemMap.put("box_num",item.getBox_num());
                itemMap.put("box_price",item.getBox_price());
                itemMap.put("discount",item.getDiscount());
                itemMap.put("medicine_code",item.getMedicine_code());
                itemMap.put("quantity",item.getQuantity());
                itemMap.put("sequence",rowindex++);
                itemMap.put("unit",item.getUnit());
                itemMap.put("price",item.getPrice());
                detail.add(itemMap);
            }
            resultMap.put("detail",detail);
            resultMap.put("result","ok");
        }catch (Exception e){
            resultMap.put("result","ERROR" + e.getMessage());
        }
        return resultMap;
    }

    @Override
    public void orderCompleteForJdSync(String orderId) {

    }

}
