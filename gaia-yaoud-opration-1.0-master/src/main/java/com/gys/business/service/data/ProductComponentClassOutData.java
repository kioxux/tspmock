package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductComponentClassOutData implements Serializable {


    /**
     * 成分中类编码
     */
    @ApiModelProperty(value="成分中类编码")
    private String midCode;

    /**
     * 成分中类名称
     */
    @ApiModelProperty(value="成分中类名称")
    private String midName;

    /**
     * 成分小类编码
     */
    @ApiModelProperty(value="成分小类编码")
    private String litCode;

    /**
     * 成分小类名称
     */
    @ApiModelProperty(value="成分小类名称")
    private String litName;

    /**
     * 成分分类编码
     */
    @ApiModelProperty(value="成分分类编码")
    private String compCode;

    /**
     * 成分分类名称
     */
    @ApiModelProperty(value="成分分类名称")
    private String compName;

    /**
     * 子节点列表
     */
    @ApiModelProperty(value="子节点列表")
    List<ProductComponentClassOutData> childList;
}
