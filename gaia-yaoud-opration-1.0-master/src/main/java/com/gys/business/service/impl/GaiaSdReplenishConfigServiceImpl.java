package com.gys.business.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaSdReplenishParaMapper;
import com.gys.business.mapper.entity.GaiaSdReplenishConfig;
import com.gys.business.mapper.GaiaSdReplenishConfigMapper;
import com.gys.business.mapper.entity.GaiaSdReplenishPara;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.IGaiaSdReplenishConfigService;
import com.gys.business.service.data.replenishParam.ReplenishSdParamInData;
import com.gys.business.service.data.sdReplenishConfig.GaiaSdReplenishConfigDto;
import com.gys.common.constant.CommonConstant;
import com.gys.common.enums.SdReplenishConfigEnum;
import com.gys.common.redis.RedisManager;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDate;
import java.util.*;


/**
 * <p>
 * 门店补货参数 服务实现类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-17
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class GaiaSdReplenishConfigServiceImpl implements IGaiaSdReplenishConfigService {
    @Autowired
    private GaiaSdReplenishConfigMapper sdReplenishConfigMapper;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private GaiaSdReplenishParaMapper replenishParaMapper;

    //============================  新增  ==========================

    @Override
    public Object build(GetLoginOutData userInfo, GaiaSdReplenishConfigDto inData) {
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        //数据验证
        if(ObjectUtil.isNotEmpty(inData.getStoCodes())) {
            for (int i = 0;i < inData.getStoCodes().length;i++) {
                String stoCode = inData.getStoCodes()[i];
                //封装入库实体
                GaiaSdReplenishConfig sdReplenishConfig = new GaiaSdReplenishConfig();
                BeanUtils.copyProperties(inData, sdReplenishConfig);
                sdReplenishConfig.setClient(userInfo.getClient());
                sdReplenishConfig.setBrId(stoCode);
                //处理逻辑

                //1.判断当前门店是否已经有配置
                Example example = Example.builder(GaiaSdReplenishConfig.class).build();
                example.createCriteria()
                        .andEqualTo(GaiaSdReplenishConfig.CLIENT, userInfo.getClient())
                        .andEqualTo(GaiaSdReplenishConfig.BR_ID, stoCode);
                example.setOrderByClause("CLIENT,BR_ID limit 1");

                GaiaSdReplenishConfig gaiaSdReplenishConfig = sdReplenishConfigMapper.selectOneByExample(example);

                if (Objects.nonNull(gaiaSdReplenishConfig)) {
                    throw new BusinessException("当前门店已经有配置,无法再次添加。");
                }

                this.checkConfig(sdReplenishConfig);

                //处理逻辑
                sdReplenishConfigMapper.insert(sdReplenishConfig);

                if (ObjectUtil.isNotEmpty(inData.getParamInDataList())){
                    List<GaiaSdReplenishPara> paramList = new ArrayList<>();
                    for (ReplenishSdParamInData data : inData.getParamInDataList()) {
                        GaiaSdReplenishPara para = new GaiaSdReplenishPara();
                        BeanUtils.copyProperties(data, para);
                        para.setClientId(userInfo.getClient());
                        para.setGsepBrId(stoCode);
                        paramList.add(para);
                    }
                    if (ObjectUtil.isNotEmpty(paramList) && paramList.size() > 0){
                        this.replenishParaMapper.batchReplenishSdParam(paramList);
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void addReplenishParam(GetLoginOutData userInfo, GaiaSdReplenishConfigDto inData) {
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        List<GaiaSdReplenishConfig> configList = new ArrayList<>();
        //数据验证
        if(ObjectUtil.isNotEmpty(inData.getStoCodes())) {
            for (int i = 0;i < inData.getStoCodes().length;i++) {
                String stoCode = inData.getStoCodes()[i];
                //封装入库实体
                GaiaSdReplenishConfig sdReplenishConfig = new GaiaSdReplenishConfig();
                BeanUtils.copyProperties(inData, sdReplenishConfig);
                sdReplenishConfig.setClient(userInfo.getClient());
                sdReplenishConfig.setBrId(stoCode);
                //处理逻辑

//                //1.判断当前门店是否已经有配置
//                Example example = Example.builder(GaiaSdReplenishConfig.class).build();
//                example.createCriteria()
//                        .andEqualTo(GaiaSdReplenishConfig.CLIENT, userInfo.getClient())
//                        .andEqualTo(GaiaSdReplenishConfig.BR_ID, stoCode);
//                example.setOrderByClause("CLIENT,BR_ID limit 1");
//
//                GaiaSdReplenishConfig gaiaSdReplenishConfig = sdReplenishConfigMapper.selectOneByExample(example);
//
//                if (Objects.nonNull(gaiaSdReplenishConfig)) {
//                    throw new BusinessException("当前门店已经有配置,无法再次添加。");
//                }

                this.checkConfig(sdReplenishConfig);

                //处理逻辑
//                sdReplenishConfigMapper.insert(sdReplenishConfig);
                configList.add(sdReplenishConfig);

                if (ObjectUtil.isNotEmpty(inData.getParamInDataList())){
                    List<GaiaSdReplenishPara> paramList = new ArrayList<>();
                    for (ReplenishSdParamInData data : inData.getParamInDataList()) {
                        GaiaSdReplenishPara para = new GaiaSdReplenishPara();
                        BeanUtils.copyProperties(data, para);
                        para.setClientId(userInfo.getClient());
                        para.setGsepBrId(stoCode);
                        para.setGsepSalesMonth("1");
                        para.setGsepChnmedFlag("1");
                        paramList.add(para);
                    }
                    if (ObjectUtil.isNotEmpty(paramList) && paramList.size() > 0){
                        this.replenishParaMapper.batchReplenishSdParam(paramList);
                    }
                }
            }
        }
        if (ObjectUtil.isNotEmpty(configList) && configList.size() > 0){
            this.sdReplenishConfigMapper.batchReplenishConfig(configList);
        }
    }

    private void checkConfig(GaiaSdReplenishConfig sdReplenishConfig) {
        if (StringUtils.isNotEmpty(sdReplenishConfig.getDelLineRate()) && Objects.nonNull(sdReplenishConfig.getDelLineQty())) {
            throw new BusinessException("百分比,行数为单选");
        }
        if (StringUtils.isNotEmpty(sdReplenishConfig.getReviseLineRate()) && Objects.nonNull(sdReplenishConfig.getReviseLineQty())) {
            throw new BusinessException("百分比,行数为单选");
        }
        if (StringUtils.isNotEmpty(sdReplenishConfig.getReviseSingleRate()) && Objects.nonNull(sdReplenishConfig.getReviseSingleQty())) {
            throw new BusinessException("百分比,数量为单选");
        }

        //百分比
        if (Objects.equals(SdReplenishConfigEnum.RATE.getCode(), sdReplenishConfig.getDelLineFlag()) && StringUtils.isEmpty(sdReplenishConfig.getDelLineRate())) {
            throw new BusinessException("删除行数百分比不能为空");
        }
        //行数
        if (Objects.equals(SdReplenishConfigEnum.QTY.getCode(), sdReplenishConfig.getDelLineFlag()) && Objects.isNull(sdReplenishConfig.getDelLineQty())) {
            throw new BusinessException("删除行数不能为空");
        }

        //百分比
        if (Objects.equals(SdReplenishConfigEnum.RATE.getCode(), sdReplenishConfig.getReviseLineFlag()) && StringUtils.isEmpty(sdReplenishConfig.getReviseLineRate())) {
            throw new BusinessException("修改行数百分比不能为空");
        }
        //行数
        if (Objects.equals(SdReplenishConfigEnum.QTY.getCode(), sdReplenishConfig.getReviseLineFlag()) && Objects.isNull(sdReplenishConfig.getReviseLineQty())) {
            throw new BusinessException("修改行数不能为空");
        }

        //百分比
        if (Objects.equals(SdReplenishConfigEnum.RATE.getCode(), sdReplenishConfig.getReviseSingleFlag()) && StringUtils.isEmpty(sdReplenishConfig.getReviseSingleRate())) {
            throw new BusinessException("减少补货单行数量比例不能为空");
        }
        //行数
        if (Objects.equals(SdReplenishConfigEnum.QTY.getCode(), sdReplenishConfig.getReviseSingleFlag()) && Objects.isNull(sdReplenishConfig.getReviseSingleQty())) {
            throw new BusinessException("修改单行可减少数量不能为空");
        }
        //百分比
//        if (StringUtils.isEmpty(sdReplenishConfig.getReviseSingleAddRate())) {
//            throw new BusinessException("增加补货单行数量比例不能为空");
//        }
//        //行数
//        if (Objects.isNull(sdReplenishConfig.getAddLineRate())) {
//            throw new BusinessException("增加补货总行数比例不能为空");
//        }

    }


    //============================  修改初始化  ==========================
    @Override
    public GaiaSdReplenishConfig updateInit(GetLoginOutData userInfo) {

        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }

        //查询出db对象
        Example example = Example.builder(GaiaSdReplenishConfig.class).build();
        example.createCriteria()
                .andEqualTo(GaiaSdReplenishConfig.CLIENT, userInfo.getClient())
                .andEqualTo(GaiaSdReplenishConfig.BR_ID, userInfo.getDepId());
        example.setOrderByClause("CLIENT,BR_ID limit 1");

        GaiaSdReplenishConfig gaiaSdReplenishConfig = sdReplenishConfigMapper.selectOneByExample(example);

        if (Objects.isNull(gaiaSdReplenishConfig)) {
            throw new BusinessException("查询无此数据");
        }

        return gaiaSdReplenishConfig;
    }

    //============================  修改初始化  ==========================


    //============================  修改  ==========================
    @Override
    public Object update(GetLoginOutData userInfo, GaiaSdReplenishConfigDto inData) {
        if(ObjectUtil.isNotEmpty(inData.getStoCodes())) {
            for (int i = 0;i < inData.getStoCodes().length;i++) {
                String stoCode = inData.getStoCodes()[i];
                //封装入库实体
                GaiaSdReplenishConfig sdReplenishConfig = new GaiaSdReplenishConfig();
                BeanUtils.copyProperties(inData, sdReplenishConfig);

                GaiaSdReplenishConfig dBSdReplenishConfig = updateInit(userInfo);
                //处理更新逻辑
                sdReplenishConfig
                        .setClient(dBSdReplenishConfig.getClient())
                        .setBrId(stoCode);

                this.checkConfig(sdReplenishConfig);
                //处理更新逻辑
                sdReplenishConfigMapper.updateByPrimaryKey(sdReplenishConfig);
                if (ObjectUtil.isNotEmpty(inData.getParamInDataList())){
                    List<GaiaSdReplenishPara> paramList = new ArrayList<>();
                    for (ReplenishSdParamInData data : inData.getParamInDataList()) {
                        GaiaSdReplenishPara para = new GaiaSdReplenishPara();
                        BeanUtils.copyProperties(data, para);
                        para.setClientId(userInfo.getClient());
                        para.setGsepBrId(stoCode);
                        paramList.add(para);
                    }
                    if (ObjectUtil.isNotEmpty(paramList) && paramList.size() > 0){
                        this.replenishParaMapper.batchReplenishSdParam(paramList);
                    }
                }
            }
        }
        return null;
    }


    //============================  修改  ==========================


    //============================  删除  ==========================
    @Override
    public int delete(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (id == null) {
            throw new BusinessException("请传入合法数据！");
        }
        GaiaSdReplenishConfig sdReplenishConfigDb = sdReplenishConfigMapper.selectByPrimaryKey(id);
        if (sdReplenishConfigDb == null) {
            throw new BusinessException("查无此数据！");
        }
        return sdReplenishConfigMapper.deleteByPrimaryKey(id);
    }

    //============================  删除  ==========================


    //============================  详情  ==========================
    @Override
    public GaiaSdReplenishConfig getDetailById(GetLoginOutData userInfo) {
        Example example = Example.builder(GaiaSdReplenishConfig.class).build();
        example.createCriteria()
                .andEqualTo(GaiaSdReplenishConfig.CLIENT, userInfo.getClient())
                .andEqualTo(GaiaSdReplenishConfig.BR_ID, userInfo.getDepId());
        example.setOrderByClause("CLIENT,BR_ID limit 1");

        return sdReplenishConfigMapper.selectOneByExample(example);
    }


    //============================  详情  ==========================


    //============================  获取列表（分页）  ==========================

    @Override
    public PageInfo<Object> getListPage(GetLoginOutData userInfo, Object inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        //if(inData.getPageSize()==null){
        //    inData.setPageSize(100);
        //}
        //if(inData.getPageNum()==null){
        //    inData.setPageNum(1);
        //}
        List<Object> resList = new ArrayList<>();
        PageInfo pageInfo;

        //实际执行的sql，执行定制任务
        //PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        if (ObjectUtil.isNotEmpty(resList)) {
            //处理返回时可能要处理的转换工作

            pageInfo = new PageInfo(resList);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    /**
     * 功能描述: 门店补货配置参数余量查询
     *
     * @param userInfo
     * @return java.lang.Object
     * @author wangQc
     * @date 2021/8/20 5:50 下午
     */
    @Override
    public GaiaSdReplenishConfig getRemain(GetLoginOutData userInfo,String gsrhVoucherId) {
        //不传单号，则返回默认值
        if (StringUtils.isEmpty(gsrhVoucherId)){
            return this.getDetailById(userInfo);
        }

        String key = GaiaSdReplenishConfig.GAIA_SD_REPLENISH_CONFIG_REMAIN
                .concat(CommonConstant.COLON)
                .concat(userInfo.getClient())
                .concat(CommonConstant.COLON)
                .concat(userInfo.getDepId())
                .concat(CommonConstant.COLON)
                .concat(gsrhVoucherId);

        GaiaSdReplenishConfig config = JSONObject.toJavaObject(
                JSON.parseObject((String) redisManager.get(key), JSONObject.class), GaiaSdReplenishConfig.class
        );
        if (Objects.isNull(config)) {
            return this.getDetailById(userInfo);
        }
        return config;
    }

    @Override
    public Object setRemain(GetLoginOutData userInfo, GaiaSdReplenishConfig gaiaSdReplenishConfig) {
        //处理更新逻辑
        gaiaSdReplenishConfig
                .setClient(userInfo.getClient())
                .setBrId(userInfo.getDepId());

        String key = GaiaSdReplenishConfig.GAIA_SD_REPLENISH_CONFIG_REMAIN
                .concat(CommonConstant.COLON)
                .concat(userInfo.getClient())
                .concat(CommonConstant.COLON)
                .concat(userInfo.getDepId())
                .concat(CommonConstant.COLON)
                .concat(gaiaSdReplenishConfig.getGsrhVoucherId());

        redisManager.set(key, JSONObject.toJSONString(gaiaSdReplenishConfig));
        return null;
    }

}
