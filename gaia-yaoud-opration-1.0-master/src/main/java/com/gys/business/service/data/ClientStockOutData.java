package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientStockOutData implements Serializable {
    private String index;
    private String brId;
    private String brName;
    private String batchNo;
    private String expiryDate;
    private String qty;
    private String price;
}