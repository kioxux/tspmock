//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ProductSortService;
import com.gys.business.service.data.*;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.EnumUtil;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.ProductTitleUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductSortServiceimpl implements ProductSortService {
    @Resource
    private GaiaSdProductsGroupMapper gaiaSdProductsGroupMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdReplenishDMapper replenishDMapper;
    @Autowired
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Autowired
    private GaiaSdStockMapper gaiaSdStockMapper;

    public ProductSortServiceimpl() {
    }

    public List<GetProductSortOutData> querySort(GetProductInData inData) {
        List<GetProductSortOutData> outData = this.gaiaSdProductsGroupMapper.querySort(inData);
        if (CollectionUtils.isEmpty((Collection) outData)) {
            outData = new ArrayList();
        } else {
            for (int i = 0; i < ((List) outData).size(); ++i) {
                ((GetProductSortOutData) ((List) outData).get(i)).setIndexSort(i + 1);
            }
        }

        return outData;
    }

    public List<GetProductDetailOutData> queryDetail(GetProductInData inData) {
        List<GetProductDetailOutData> outData = this.gaiaSdProductsGroupMapper.queryDetail(inData);
        if (CollectionUtils.isEmpty(outData)) {
            outData = new ArrayList();
        } else {
            for (int i = 0; i < ((List) outData).size(); ++i) {
                ((GetProductDetailOutData) ((List) outData).get(i)).setIndexDetail(i + 1);
            }
        }

        return outData;
    }

    public List<GetQueryProVoOutData> queryProVo(GetProductInData inData) {
        List<GetQueryProVoOutData> outData = this.gaiaProductBusinessMapper.queryProVo(inData);
        return outData;
    }

    public PageInfo<GetQueryProVoOutData> queryPro(GetProductInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetQueryProVoOutData> outData = this.gaiaProductBusinessMapper.queryPro(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    public List<GetQueryProVoOutData> queryPrice(GetProductInData inData) {
        List<GetQueryProVoOutData> outData = this.gaiaProductBusinessMapper.queryPrice(inData);
        return outData;
    }

    public void add(GetProductInData inData) {
        GaiaSdProductsGroup sdProductsGroupTemp = new GaiaSdProductsGroup();
        sdProductsGroupTemp.setClientId(inData.getClientId());
        sdProductsGroupTemp.setGspgId(inData.getGspgId());
        List<GaiaSdProductsGroup> gaiaSdStoresGroupTemp = this.gaiaSdProductsGroupMapper.select(sdProductsGroupTemp);
        if (ObjectUtil.isNotEmpty(gaiaSdStoresGroupTemp)) {
            throw new BusinessException("分类编号已存在");
        } else {
            sdProductsGroupTemp = new GaiaSdProductsGroup();
            sdProductsGroupTemp.setClientId(inData.getClientId());
//            sdProductsGroupTemp.setGspgName(inData.getGspgName());
            gaiaSdStoresGroupTemp = this.gaiaSdProductsGroupMapper.select(sdProductsGroupTemp);
            if (ObjectUtil.isNotEmpty(gaiaSdStoresGroupTemp)) {
                throw new BusinessException("分类名称已存在");
            } else {
                inData.getProTableOutDataList().forEach((item) -> {
                    GaiaSdProductsGroup sdProductsGroup = new GaiaSdProductsGroup();
                    sdProductsGroup.setClientId(inData.getClientId());
                    sdProductsGroup.setGspgId(inData.getGspgId());
//                    sdProductsGroup.setGspgName(inData.getGspgName());
                    sdProductsGroup.setGspgProId(item.getProCode());
//                    sdProductsGroup.setGspgProName(item.getProName());
//                    sdProductsGroup.setGspgUpdateEmp(inData.getGspgUpdateEmp());
                    this.gaiaSdProductsGroupMapper.insert(sdProductsGroup);
                });
            }
        }
    }

    @Transactional
    public void update(GetProductInData inData) {
        GaiaSdProductsGroup gaiaSdProductsGroupTemp = new GaiaSdProductsGroup();
        gaiaSdProductsGroupTemp.setClientId(inData.getClientId());
        gaiaSdProductsGroupTemp.setGspgId(inData.getGspgId());
        this.gaiaSdProductsGroupMapper.delete(gaiaSdProductsGroupTemp);
        inData.getProTableOutDataList().forEach((item) -> {
            GaiaSdProductsGroup sdProductsGroup = new GaiaSdProductsGroup();
            sdProductsGroup.setClientId(inData.getClientId());
            sdProductsGroup.setGspgId(inData.getGspgId());
//            sdProductsGroup.setGspgName(inData.getGspgName());
            sdProductsGroup.setGspgProId(item.getProCode());
//            sdProductsGroup.setGspgProName(item.getProName());
//            sdProductsGroup.setGspgUpdateEmp(inData.getGspgUpdateEmp());
            this.gaiaSdProductsGroupMapper.insert(sdProductsGroup);
        });
    }

    public Boolean addAll(GetProductListInData inData, List<String> gspgProIds, String clientId) {
        List<GetProOutData> proOutDataList = this.gaiaProductBusinessMapper.selectProductByProCodes(gspgProIds, clientId);
        if (ObjectUtil.isEmpty(proOutDataList)) {
            throw new BusinessException("所有商品编码没有对应商品规格");
        } else {
            Map<String, GetProOutData> proMap = new HashMap();
            for (GetProOutData itemx : proOutDataList) {
                proMap.put(itemx.getProCode(), itemx);
            }
            List<GaiaSdProductsGroup> dataList = new ArrayList();
            Iterator var7 = inData.getInDataList().iterator();

            while (var7.hasNext()) {
                GetProductInData item = (GetProductInData) var7.next();
                GetProOutData mapOut = (GetProOutData) proMap.get(item.getGspgProId());
                if (ObjectUtil.isEmpty(mapOut)) {
                    String message = MessageFormat.format("商品编号:{0}没有对应的商品", item.getGspgProId());
                    throw new BusinessException(message);
                }

                GaiaSdProductsGroup sdProductsGroup = new GaiaSdProductsGroup();
                BeanUtils.copyProperties(item, sdProductsGroup);
//                sdProductsGroup.setGspgProName(mapOut.getProName());
                dataList.add(sdProductsGroup);
            }

            this.gaiaSdProductsGroupMapper.addAll(dataList);
            return true;
        }
    }

    public void importExcel(GetProImportExcelInData inData) {
        String account = inData.getUser().getAccount();
        List<GaiaSdProductsGroup> dataList = new ArrayList();
        Map<String, List<String>> proIdMap = new HashMap();
        inData.getImportExcelInData().forEach((item) -> {
            if (!ObjectUtil.isEmpty(item.getGspgId()) && !ObjectUtil.isEmpty(item.getGspgProId())) {
                List<String> gspgProIdList = (List) proIdMap.get(item.getGspgId());
                if (ObjectUtil.isNotEmpty(gspgProIdList) && gspgProIdList.indexOf(item.getGspgProId()) >= 0) {
                    throw new BusinessException("分类编码：" + item.getGspgId() + ",商品编码：" + item.getGspgProId() + "数据重复");
                } else {
                    if (ObjectUtil.isEmpty(gspgProIdList)) {
                        ArrayList<String> prodIds = new ArrayList();
                        prodIds.add(item.getGspgProId());
                        proIdMap.put(item.getGspgId(), prodIds);
                    } else {
                        gspgProIdList.add(item.getGspgProId());
                        proIdMap.put(item.getGspgId(), gspgProIdList);
                    }

                    GaiaSdProductsGroup gaiaSdProductsGroup = new GaiaSdProductsGroup();
                    gaiaSdProductsGroup.setClientId(inData.getClientId());
                    gaiaSdProductsGroup.setGspgId(item.getGspgId());
                    List<GaiaSdProductsGroup> sdProductsGroups = this.gaiaSdProductsGroupMapper.select(gaiaSdProductsGroup);
                    if (ObjectUtil.isEmpty(sdProductsGroups)) {
                        throw new BusinessException("分类编码：" + item.getGspgId() + "不存在");
                    } else {
                        GaiaProductBusiness productBusiness = new GaiaProductBusiness();
                        productBusiness.setProSelfCode(item.getGspgProId());
                        productBusiness.setClient(inData.getClientId());
                        productBusiness.setProSite(inData.getUser().getDepId());
                        productBusiness = (GaiaProductBusiness) this.gaiaProductBusinessMapper.selectOne(productBusiness);
                        if (ObjectUtil.isEmpty(productBusiness)) {
                            throw new BusinessException("商品编码：" + item.getGspgProId() + "不存在");
                        } else if (ObjectUtil.isEmpty(productBusiness.getProSpecs())) {
                            throw new BusinessException("商品编码：" + item.getGspgProId() + "的规格属性不存在");
                        } else {
                            GaiaSdProductsGroup gaiaSdProductsGroupTemp = new GaiaSdProductsGroup();
                            gaiaSdProductsGroupTemp.setClientId(inData.getClientId());
                            gaiaSdProductsGroupTemp.setGspgId(item.getGspgId());
                            gaiaSdProductsGroupTemp.setGspgProId(item.getGspgProId());
                            gaiaSdProductsGroupTemp = (GaiaSdProductsGroup) this.gaiaSdProductsGroupMapper.selectOne(gaiaSdProductsGroupTemp);
                            if (ObjectUtil.isNotEmpty(gaiaSdProductsGroupTemp)) {
                                throw new BusinessException("商品编码：" + item.getGspgProId() + "，在分类编码：" + item.getGspgId() + "下已存在");
                            } else {
                                GaiaSdProductsGroup gaiaSdProductsGroupItem = new GaiaSdProductsGroup();
                                gaiaSdProductsGroupItem.setClientId(inData.getClientId());
                                gaiaSdProductsGroupItem.setGspgProId(item.getGspgProId());
                                gaiaSdProductsGroupItem.setGspgId(item.getGspgId());
//                                gaiaSdProductsGroupItem.setGspgName(((GaiaSdProductsGroup)sdProductsGroups.get(0)).getGspgName());
//                                gaiaSdProductsGroupItem.setGspgProName(productBusiness.getProName());
//                                gaiaSdProductsGroupItem.setGspgUpdateEmp(account);
                                dataList.add(gaiaSdProductsGroupItem);
                            }
                        }
                    }
                }
            } else {
                throw new BusinessException("数据不完整");
            }
        });
        this.gaiaSdProductsGroupMapper.addAll(dataList);
    }

    public void delete(List<GetProductInData> inData) {
        inData.forEach((item) -> {
            this.gaiaSdProductsGroupMapper.deleteData(item);
        });
    }

    public List<GetProductSortOutData> selectProGroupByClient(GetProductInData inData) {
        List<GetProductSortOutData> list = this.gaiaSdProductsGroupMapper.selectProGroupByClient(inData.getClientId());
        return list;
    }

    @Override
    public List<GetReplenishDetailOutData> queryProList(GetProductInData inData) {
        List<GetReplenishDetailOutData> outData = this.gaiaProductBusinessMapper.queryProList(inData);
        return outData;
    }

    @Override
    public List<GetProductNewOutData> queryProNew(GetProductNewInData inData) {
        List<GetProductNewOutData> outData = new ArrayList<>();
        /**
         * 1.门店用 2.加盟商下查询商品
         */
        switch (inData.getType()) {
            case "1":
                outData = this.gaiaProductBusinessMapper.queryProByStore(inData);
            case "2":
                outData = this.gaiaProductBusinessMapper.queryProByClient(inData);
        }

//        Map<String,String> inMap = new HashMap<>();
//        if(ObjectUtil.isNotEmpty(inData.getLableArr())){
//            inData.getLableArr().forEach(item->{
//                inMap.put(item,item);
//            });
//        }

        return outData;
    }

    @Override
    public PageInfo queryProThirdly(GetProductThirdlyInData inData) {
        GetProductThirdlyOutData cheakMap = new GetProductThirdlyOutData();
        List<GetProductThirdlyOutData> dataMap = new ArrayList<>();
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());

        /**
         * 1.门店用 2.加盟商下查询商品
         */
        switch (inData.getType()) {
            case "1":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_STORE);
                dataMap = this.gaiaProductBusinessMapper.queryProThirdlyByStore(inData);
                //委托第三方编码匹配
                String wtpsParam = storeDataMapper.selectWtpsParam(inData.getClient(), "WTPS_SET");
                if (com.qcloud.cos.utils.StringUtils.isNullOrEmpty(wtpsParam)) {
                    wtpsParam = "0";
                }
                if ("1".equals(wtpsParam)) {
                    //获取第三方仓库库存
                    List<GaiaSdDsfStock> proThreeStockList = replenishDMapper.selectProThreeStock(inData.getClient(), "");
                    for (GetProductThirdlyOutData detail : dataMap) {
                        BigDecimal stock = BigDecimal.ZERO;
                        if (ObjectUtil.isNotEmpty(detail.getKysl())) {
                            stock = new BigDecimal(detail.getKysl());
                        }
                        //商品仓库库存累加
                        for (GaiaSdDsfStock sdDsfStock : proThreeStockList) {
                            if (sdDsfStock.getProId().equals(detail.getProCode())) {
                                detail.setKysl(stock.add(sdDsfStock.getQty()).toString());
                            }
                        }
                    }
                }
                //查询该门店是否允许 补货 禁采商品
                GaiaSdSystemPara para = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClient(), inData.getProSite(), "REPLENISH_NO_PURCHASE");
                if (ObjectUtil.isNotEmpty(para) && "1".equals(para.getGsspPara())) {
                    for (GetProductThirdlyOutData detail : dataMap) {
                        detail.setProNoPurchase("0");
                    }
                }
//                outMap.put("data",dataMap);
                break;
            case "2":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_CLIENT);
                dataMap = this.gaiaProductBusinessMapper.queryProThirdlyByClinet(inData);
//                outMap.put("data",dataMap);
                break;
        }
        cheakMap = this.gaiaProductBusinessMapper.queryProThirdlyByALLot(inData);
        /**
         * 这里加点后金额会有几种情况、
         * 1.根据地点 商品编码确定唯一行 判断是百分比还是固定额度
         * 2.商品编码为空  代表是全地点的商品都是这行 但是要优先有商品的记录  百分比和固定金额  不可以同时存在
         */
        if (ObjectUtil.isNotEmpty(dataMap)) {
            //判断 是否有全地点配置 没有跳过循环
            if (ObjectUtil.isNotEmpty(cheakMap)) {
                for (GetProductThirdlyOutData outMap : dataMap) {
                    //判断 加点率和加点金额是否为空
                    if (ObjectUtil.isEmpty(outMap.getAddRate()) || ObjectUtil.isEmpty(outMap.getAddAmt())) {
                        DecimalFormat df = new DecimalFormat("0.00%");
                        //加点方式为固定金额
                        if (ObjectUtil.isNotEmpty(cheakMap.getAddAmt())) {
                            BigDecimal addAmt = cheakMap.getAddAmt();
                            outMap.setAddRate(df.format(addAmt.divide(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()), 2, BigDecimal.ROUND_HALF_EVEN)));
                            outMap.setAddAmt(addAmt.add(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())));
                        } else {
                            //加点方式为加点率
                            if (ObjectUtil.isNotEmpty(cheakMap.getAddRate())) {
                                String addRate = cheakMap.getAddRate();
//                                System.out.println(addRate);
//                                System.out.println( CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)));
//                                System.out.println(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()));
                                outMap.setAddAmt(
                                        CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)).
                                                multiply(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())).
                                                divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN));
                                outMap.setAddRate(addRate + "%");
                            }
                        }
                    }
                }
            }
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(dataMap)) {
            pageInfo = new PageInfo(dataMap);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo queryProFourthlyForTC(GetProductThirdlyInForTCData inData) {
        GetProductThirdlyOutData cheakMap = new GetProductThirdlyOutData();
        List<GetProductThirdlyOutData> dataMap = new ArrayList<>();
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        if (ObjectUtil.isEmpty(inData.getMateType())) {
            inData.setMateType("0");
        }
        if (StringUtils.isNotEmpty(inData.getContent())) {
            inData.setProArr(inData.getContent().split("\\s+ |\\s+|,"));
            System.out.println(inData.getContent());
        }
        List<String> classList = new ArrayList<>();
        if(inData.getProClass()!=null && inData.getProClass().length>0){
            for (int i = 0; i < inData.getProClass().length; i++) {
                String[] c = inData.getProClass()[i];
                classList.add(c[2]);
            }
            inData.setProClassList(classList);
        }

        /**
         * 1.门店用 2.加盟商下查询商品
         */
        switch (inData.getType()) {
//            case "1":
////                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_STORE);
//                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyByStore(inData);
////                outMap.put("data",dataMap);
//                break;
//            case "2":
////                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_CLIENT);
//                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyByClinet(inData);
////                outMap.put("data",dataMap);
//                break;
            case "3":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_CLIENT);
                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyByClinetNoPriceForTC(inData);
//                outMap.put("data",dataMap);
                break;
        }

        if (StringUtils.isNotEmpty(inData.getProSite())&&ObjectUtil.isNotEmpty(dataMap)) {
            List<String> proCodes = dataMap.stream().map(outDate -> outDate.getProCode()).collect(Collectors.toList());
            List<GaiaSdStock> stockList = gaiaSdStockMapper.findByClientAndGssBrIdAndGssProIdIn(inData.getClient(), inData.getProSite(), proCodes);
            dataMap.forEach(date -> {
                String gssQty = stockList.stream().filter(stock -> stock.getGssProId().equals(date.getProCode())).findFirst().orElseGet(GaiaSdStock::new).getGssQty();
                date.setInventory(StringUtils.isNotEmpty(gssQty) ? gssQty : "0.0000");
            });
        }

        /**
         * 这里加点后金额会有几种情况、
         * 1.根据地点 商品编码确定唯一行 判断是百分比还是固定额度
         * 2.商品编码为空  代表是全地点的商品都是这行 但是要优先有商品的记录  百分比和固定金额  不可以同时存在
         */
        if (ObjectUtil.isNotEmpty(dataMap) && !"3".equals(inData.getType())) {
            GetProductThirdlyInData originInData = new GetProductThirdlyInData();
            BeanUtils.copyProperties(inData,originInData);
            cheakMap = this.gaiaProductBusinessMapper.queryProThirdlyByALLot(originInData);

            //判断 是否有全地点配置 没有跳过循环
            if (ObjectUtil.isNotEmpty(cheakMap)) {
                for (GetProductThirdlyOutData outMap : dataMap) {
                    //判断 加点率和加点金额是否为空
                    if (ObjectUtil.isEmpty(outMap.getAddRate()) && ObjectUtil.isEmpty(outMap.getAddAmt())) {
                        DecimalFormat df = new DecimalFormat("0.00%");
                        //加点方式为固定金额
                        if (ObjectUtil.isNotEmpty(cheakMap.getAddAmt())) {
                            BigDecimal addAmt = cheakMap.getAddAmt();
                            outMap.setAddRate(df.format(addAmt.divide(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()), 2, BigDecimal.ROUND_HALF_EVEN)));
                            outMap.setAddAmt(addAmt.add(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())));
                        } else {
                            //加点方式为加点率
                            if (ObjectUtil.isNotEmpty(cheakMap.getAddRate())) {
                                String addRate = cheakMap.getAddRate();
//                                System.out.println(addRate);
//                                System.out.println( CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)));
//                                System.out.println(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()));
                                outMap.setAddAmt(
                                        CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)).
                                                multiply(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())).
                                                divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN));
                                outMap.setAddRate(addRate + "%");
                            }
                        }
                    }
                }
            }
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(dataMap)) {
            pageInfo = new PageInfo(dataMap);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo queryProFourthly(GetProductThirdlyInData inData) {
        GetProductThirdlyOutData cheakMap = new GetProductThirdlyOutData();
        List<GetProductThirdlyOutData> dataMap = new ArrayList<>();
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        if (ObjectUtil.isEmpty(inData.getMateType())) {
            inData.setMateType("0");
        }
        if (StringUtils.isNotEmpty(inData.getContent())) {
            inData.setProArr(inData.getContent().split("\\s+ |\\s+|,"));
            System.out.println(inData.getContent());
        }
        /**
         * 1.门店用 2.加盟商下查询商品
         */
        switch (inData.getType()) {
            case "1":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_STORE);
                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyByStore(inData);
//                outMap.put("data",dataMap);
                break;
            case "2":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_CLIENT);
                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyByClinet(inData);
//                outMap.put("data",dataMap);
                break;
            case "3":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_CLIENT);
                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyByClinetNoPrice(inData);
//                outMap.put("data",dataMap);
                break;
        }

        if (StringUtils.isNotEmpty(inData.getProSite())&&ObjectUtil.isNotEmpty(dataMap)) {
            List<String> proCodes = dataMap.stream().map(outDate -> outDate.getProCode()).collect(Collectors.toList());
            List<GaiaSdStock> stockList = gaiaSdStockMapper.findByClientAndGssBrIdAndGssProIdIn(inData.getClient(), inData.getProSite(), proCodes);
            dataMap.forEach(date -> {
                String gssQty = stockList.stream().filter(stock -> stock.getGssProId().equals(date.getProCode())).findFirst().orElseGet(GaiaSdStock::new).getGssQty();
                date.setInventory(StringUtils.isNotEmpty(gssQty) ? gssQty : "0.0000");
            });
        }

        /**
         * 这里加点后金额会有几种情况、
         * 1.根据地点 商品编码确定唯一行 判断是百分比还是固定额度
         * 2.商品编码为空  代表是全地点的商品都是这行 但是要优先有商品的记录  百分比和固定金额  不可以同时存在
         */
        if (ObjectUtil.isNotEmpty(dataMap) && !"3".equals(inData.getType())) {
            cheakMap = this.gaiaProductBusinessMapper.queryProThirdlyByALLot(inData);

            //判断 是否有全地点配置 没有跳过循环
            if (ObjectUtil.isNotEmpty(cheakMap)) {
                for (GetProductThirdlyOutData outMap : dataMap) {
                    //判断 加点率和加点金额是否为空
                    if (ObjectUtil.isEmpty(outMap.getAddRate()) && ObjectUtil.isEmpty(outMap.getAddAmt())) {
                        DecimalFormat df = new DecimalFormat("0.00%");
                        //加点方式为固定金额
                        if (ObjectUtil.isNotEmpty(cheakMap.getAddAmt())) {
                            BigDecimal addAmt = cheakMap.getAddAmt();
                            outMap.setAddRate(df.format(addAmt.divide(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()), 2, BigDecimal.ROUND_HALF_EVEN)));
                            outMap.setAddAmt(addAmt.add(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())));
                        } else {
                            //加点方式为加点率
                            if (ObjectUtil.isNotEmpty(cheakMap.getAddRate())) {
                                String addRate = cheakMap.getAddRate();
//                                System.out.println(addRate);
//                                System.out.println( CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)));
//                                System.out.println(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()));
                                outMap.setAddAmt(
                                        CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)).
                                                multiply(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())).
                                                divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN));
                                outMap.setAddRate(addRate + "%");
                            }
                        }
                    }
                }
            }
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(dataMap)) {
            pageInfo = new PageInfo(dataMap);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }


    @Override
    public List<GetProductNewOutData> queryProConditionNew(GetProductNewInData inData) {
        List<GetProductNewOutData> outData = new ArrayList<>();
        /**
         * 1.门店用 2.加盟商下查询商品
         */
        inData.setStrSqls(ConditionalGenerator(inData.getSearchConditionInfos()));
        switch (inData.getType()) {
            case "1":
                outData = this.gaiaProductBusinessMapper.queryProByStore(inData);
                break;
            case "2":
                outData = this.gaiaProductBusinessMapper.queryProByClient(inData);
                break;
        }

//        Map<String,String> inMap = new HashMap<>();
//        if(ObjectUtil.isNotEmpty(inData.getLableArr())){
//            inData.getLableArr().forEach(item->{
//                inMap.put(item,item);
//            });
//        }

        return outData;
    }

    /**
     * 根据前端传值 拼接查询条件
     *
     * @param infoList
     * @return
     */
    public List<StringBuilder> ConditionalGenerator(List<ProSearchConditionInfo> infoList) {
        //sql 汇总
        List<StringBuilder> strSql = new ArrayList();
        if (ObjectUtil.isNotEmpty(infoList)) {

            //遍历所有名称
            for (EnumUtil.ProHeader proHeader : EnumUtil.ProHeader.values()) {
                Integer size = 0;
                // sql 分类
                StringBuilder SqlType = new StringBuilder();
//                SqlType.append(" ( ");
                for (ProSearchConditionInfo info : infoList) {
                    //名称相同放到一个分类
                    if (proHeader.getCode().equals(info.getDesignation())) {

                        //------------------------------   开始     -----------------------------------------
//                        //关系
//                        if (info.getRelation().equals("1")) {
//                            SqlType.append(" AND (");
//                        }else if (info.getRelation().equals("2")) {
//                            SqlType.append(" OR (");
//                        }
                        //d第一次进来不加OR
                        if (size != 0) {
                            SqlType.append(" OR ");
                        }
                        size++;
                        //名称
                        SqlType.append(" gpb. ").
                                append(EnumUtil.ProHeader.getValue(info.getDesignation()));
                        //条件
                        if (info.getCondition().equals("1")) {
                            SqlType.append(" LIKE '%" + info.getContent() + "%' ");
                        } else if (info.getCondition().equals("2")) {
                            SqlType.append(" = '" + info.getContent() + "'");
                        } else if (info.getCondition().equals("3")) {
                            SqlType.append(" != '" + info.getContent() + "'");
                        }
                        //------------------------------   结束     -----------------------------------------

                    }
                }
//                SqlType.append(" ) ");
                if (ObjectUtil.isNotEmpty(SqlType)) {
                    strSql.add(SqlType);
                }
            }
        }
        return strSql;
    }

    //5.0
    @Override
    public PageInfo queryProFourthlyS(GetProductThirdlyInData inData) {
        GetProductThirdlyOutData cheakMap = new GetProductThirdlyOutData();
        List<GetProductThirdlyOutData> dataMap = new ArrayList<>();
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());

        /**
         * 1.门店用 2.加盟商下查询商品
         */
        switch (inData.getType()) {
            case "1":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_STORE);
                dataMap = this.gaiaProductBusinessMapper.queryProFourthlyS(inData);
//                outMap.put("data",dataMap);
                break;
            case "2":
//                outMap.put("header", ProThirdlyHeaderData.PRODUCT_HEADER_CLIENT);
                dataMap = this.gaiaProductBusinessMapper.queryProFourthlySs(inData);
//                outMap.put("data",dataMap);
                break;
        }

        cheakMap = this.gaiaProductBusinessMapper.queryProThirdlyByALLot(inData);
        /**
         * 这里加点后金额会有几种情况、
         * 1.根据地点 商品编码确定唯一行 判断是百分比还是固定额度
         * 2.商品编码为空  代表是全地点的商品都是这行 但是要优先有商品的记录  百分比和固定金额  不可以同时存在
         */
        if (ObjectUtil.isNotEmpty(dataMap)) {
            //判断 是否有全地点配置 没有跳过循环
            if (ObjectUtil.isNotEmpty(cheakMap)) {
                for (GetProductThirdlyOutData outMap : dataMap) {
                    //判断 加点率和加点金额是否为空
                    if (ObjectUtil.isEmpty(outMap.getAddRate()) || ObjectUtil.isEmpty(outMap.getAddAmt())) {
                        DecimalFormat df = new DecimalFormat("0.00%");
                        //加点方式为固定金额
                        if (ObjectUtil.isNotEmpty(cheakMap.getAddAmt())) {
                            BigDecimal addAmt = cheakMap.getAddAmt();
                            outMap.setAddRate(df.format(addAmt.divide(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()), 2, BigDecimal.ROUND_HALF_EVEN)));
                            outMap.setAddAmt(addAmt.add(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())));
                        } else {
                            //加点方式为加点率
                            if (ObjectUtil.isNotEmpty(cheakMap.getAddRate())) {
                                String addRate = cheakMap.getAddRate();
//                                System.out.println(addRate);
//                                System.out.println( CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)));
//                                System.out.println(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()));
                                outMap.setAddAmt(
                                        CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)).
                                                multiply(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())).
                                                divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN));
                                outMap.setAddRate(addRate + "%");
                            }
                        }
                    }
                }
            }
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(dataMap)) {
            pageInfo = new PageInfo(dataMap);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo queryProForColdChain(GetProductThirdlyInData inData) {
        GetProductThirdlyOutData cheakMap = new GetProductThirdlyOutData();
        List<GetProductThirdlyOutData> dataMap = new ArrayList<>();
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());

        dataMap = this.gaiaProductBusinessMapper.queryProForColdChain(inData);
        cheakMap = this.gaiaProductBusinessMapper.queryProThirdlyByALLot(inData);
        /**
         * 这里加点后金额会有几种情况、
         * 1.根据地点 商品编码确定唯一行 判断是百分比还是固定额度
         * 2.商品编码为空  代表是全地点的商品都是这行 但是要优先有商品的记录  百分比和固定金额  不可以同时存在
         */
        if (ObjectUtil.isNotEmpty(dataMap)) {
            //判断 是否有全地点配置 没有跳过循环
            if (ObjectUtil.isNotEmpty(cheakMap)) {
                for (GetProductThirdlyOutData outMap : dataMap) {
                    //判断 加点率和加点金额是否为空
                    if (ObjectUtil.isEmpty(outMap.getAddRate()) || ObjectUtil.isEmpty(outMap.getAddAmt())) {
                        DecimalFormat df = new DecimalFormat("0.00%");
                        //加点方式为固定金额
                        if (ObjectUtil.isNotEmpty(cheakMap.getAddAmt())) {
                            BigDecimal addAmt = cheakMap.getAddAmt();
                            outMap.setAddRate(df.format(addAmt.divide(CommonUtil.stripTrailingZeros(outMap.getUnitPrice()), 2, BigDecimal.ROUND_HALF_EVEN)));
                            outMap.setAddAmt(addAmt.add(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())));
                        } else {
                            //加点方式为加点率
                            if (ObjectUtil.isNotEmpty(cheakMap.getAddRate())) {
                                String addRate = cheakMap.getAddRate();
                                outMap.setAddAmt(
                                        CommonUtil.stripTrailingZerosStr(cheakMap.getAddRate()).add(new BigDecimal(100)).
                                                multiply(CommonUtil.stripTrailingZeros(outMap.getUnitPrice())).
                                                divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN));
                                outMap.setAddRate(addRate + "%");
                            }
                        }
                    }
                }
            }
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(dataMap)) {
            pageInfo = new PageInfo(dataMap);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<ProductTitleSettings> productTitleSetting(GetProductThirdlyInData inData) {
        List<ProductTitleSettings> titleSettingsList = ProductTitleUtil.getTitle(inData.getType());
        GaiaSdSystemPara depotPrice = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClient(), inData.getProSite(), ProductTitleUtil.DEPOT_PRICE_SHOW);
        if (ObjectUtil.isEmpty(depotPrice) || "0".equals(depotPrice.getGsspPara())) {
            titleSettingsList.remove(ProductTitleUtil.ADDAMT);
        }
        GaiaSdSystemPara depotStock = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClient(), inData.getProSite(), ProductTitleUtil.DEPOT_STOCK_SHOW);
        if (ObjectUtil.isEmpty(depotStock) || "0".equals(depotStock.getGsspPara())) {
            titleSettingsList.remove(ProductTitleUtil.KYSL);
        }
        return titleSettingsList;
    }

    @Override
    public PageInfo queryBasicProduct(GetProductThirdlyInData inData) {
        List<GetProductThirdlyOutData> dataMap = new ArrayList<>();
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        if (ObjectUtil.isEmpty(inData.getContent())){
            inData.setContent("");
        }
        dataMap = this.gaiaProductBusinessMapper.queryBasicProduct(inData.getContent());
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(dataMap)) {
            pageInfo = new PageInfo(dataMap);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }
}
