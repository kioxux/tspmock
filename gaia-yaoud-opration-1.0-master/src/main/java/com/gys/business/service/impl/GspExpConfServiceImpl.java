package com.gys.business.service.impl;

import com.gys.business.mapper.GspExpConfMapper;
import com.gys.business.service.GspExpConfService;
import com.gys.business.service.data.GetGspExpConfInData;
import com.gys.business.service.data.GspExpConfData;
import com.gys.common.enums.WarnTypeEnum;
import com.gys.common.exception.BusinessException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class GspExpConfServiceImpl implements GspExpConfService {
    @Resource
    private GspExpConfMapper gspExpConfMapper;

    @Transactional
    @Override
    public Integer saveGspExpConf(GetGspExpConfInData inData) {
    	List<GspExpConfData> rs = inData.getGspExpConfs();
        if(CollectionUtils.isEmpty(rs)){
        	return 0;
        }else{
        	Date now = new Date();
        	String type = inData.getType();
        	if(StringUtils.isEmpty(type)) {
           		 throw new BusinessException("提示：类型不能为空!");
        	}
        	rs.forEach(gspExpConf -> {
        		gspExpConf.setClient(inData.getClient());
        		gspExpConf.setType(type);
        		if(StringUtils.isEmpty(gspExpConf.getWarnDays())) {
        			gspExpConf.setWarnDays("");
        		}
        		gspExpConf.setCreateUser(inData.getUserId()+"-"+inData.getCreateUser());
        		gspExpConf.setUpdateUser(inData.getUserId()+"-"+inData.getCreateUser());
             });
        	gspExpConfMapper.deleteGspExpConf(rs.get(0));
        	gspExpConfMapper.insertGspExpConf(rs);
        }
        return 0;
    }

    @Override
    public List<GspExpConfData> selectGspExpConf(GspExpConfData inData) {
    	String type = inData.getType();
    	if(StringUtils.isEmpty(type)) {
    		 throw new BusinessException("提示：类型不能为空!");
    	}
        List<GspExpConfData> rs = null;
        if(WarnTypeEnum.COMMODITY.type.equals(type)||WarnTypeEnum.REPORT_WARN_DAYS.type.equals(type)) {
        	rs = gspExpConfMapper.selectGspExpConfByWarnDays(inData);
        	if(CollectionUtils.isEmpty(rs)) {
				GspExpConfData gspExpConf = new GspExpConfData();
				gspExpConf.setId(0l);
				if(WarnTypeEnum.COMMODITY.type.equals(type)) {//商品类型
					gspExpConf.setExpiryDateField("商品效期");
					gspExpConf.setExpiryDateFieldName("商品效期");
				}else{//客户类型
					gspExpConf.setExpiryDateField("warn_days");
					gspExpConf.setExpiryDateFieldName("warn_days");
				}
        		rs.add(gspExpConf);
        	}
		}else {
			rs = gspExpConfMapper.selectGspExpConf(inData);
		}
        return rs;
    }
}
