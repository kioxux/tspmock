package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 参数实体
 *
 * @author xiaoyuan on 2020/8/31
 */
@Data
public class SalesInquireData implements Serializable {
    private static final long serialVersionUID = 4811669262040082053L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String brId;

    @NotBlank(message = "销售单号不能为空")
    @ApiModelProperty(value = "销售单号")
    private String billNo;

    @ApiModelProperty(value = "退货单号")
    private String billNoReturn;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "退货审核人员")
    private String gsshEmpReturn;

    @ApiModelProperty(value = "销售日期")
    private String salesDate;

    @ApiModelProperty(value = "销售时间")
    private String salesTime;

    @ApiModelProperty(value = "集合参数")
    private List<SaleReturnDetailData> saleReturnDetailInDataList;


    /**
     * 非正常全退
     */
    private Boolean whether = false;

    /**
     * 正常整单退
     */
    private Boolean normal = false;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 退款总金额
     */
    private String refundAmount;

    /**
     * 会员卡号
     */
    private String salesMemberCardNum;

    /**
     * 唯一id
     */
    private String memberId;


    /**
     * 支付方式
     */
    private List<PaymentInformationOutData> payOutData;

    /**
     * 退货详细
     */
    private List<GetSaleReturnToData> returnToData;


}
