package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaProductSpecialChange implements Serializable {
    /**
    * 唯一值
    */
    @ApiModelProperty(value="唯一值")
    private Long id;

    /**
    * 商品编码 药德通用编码
    */
    @ApiModelProperty(value="商品编码 药德通用编码")
    private String proCode;

    /**
    * 省份
    */
    @ApiModelProperty(value="省份")
    private String proProv;

    /**
    * 地市
    */
    @ApiModelProperty(value="地市")
    private String proCity;

    /**
    * 类型 1-季节，2-品牌
    */
    @ApiModelProperty(value="类型 1-季节，2-品牌")
    private String proType;

    /**
    * 变更字段
    */
    @ApiModelProperty(value="变更字段")
    private String proChangeField;

    /**
    * 变更前值
    */
    @ApiModelProperty(value="变更前值")
    private String proChangeFrom;

    /**
    * 变更后值
    */
    @ApiModelProperty(value="变更后值")
    private String proChangeTo;

    /**
    * 变更人
    */
    @ApiModelProperty(value="变更人")
    private String proChangeUser;

    /**
    * 变更日期
    */
    @ApiModelProperty(value="变更日期")
    private String proChangeDate;

    /**
    * 变更时间
    */
    @ApiModelProperty(value="变更时间")
    private String proChangeTime;

    private static final long serialVersionUID = 1L;

}