package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaAlPlQjmd;
import com.gys.common.kylin.TestData;

import java.util.HashMap;
import java.util.List;

public interface Al_Pl_Analyse {
    void execAnalyse();
    List<TestData> getDistinctIds();
    List<GaiaAlPlQjmd> getSaleSummaryByArea(String area);
    List<GaiaAlPlQjmd> getSaleInfoByComp(HashMap<String,Object> inHashMap);
}
