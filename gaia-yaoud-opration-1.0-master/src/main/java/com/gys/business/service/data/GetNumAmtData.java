//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetNumAmtData implements Serializable {
    private static final long serialVersionUID = -6377689048406789712L;

    /**
     * 数量
     */
    private String num;
    /**
     * 总价
     */
    private String money;
    /**
     * 名称
     */
    private String proName;
    /**
     * 序号
     */
    private String index;
}
