package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 促销方式 数量的实体
 *
 * @author xiaoyuan on 2020/8/12
 */
@Data
public class NumberOfPromotionalMethodsData implements Serializable {
    private static final long serialVersionUID = 6120140341616682001L;

    /**
     * 单品促销阶段一
     */
    private String singleProductOne;

    /**
     * 单品促销阶段二
     */
    private String singleProductTwo;

    /**
     * 单品促销阶段三
     */
    private String singleProductThree;


    /**
     * 系列促销阶段一数量
     */
    private String seriesPromotionQtyOne;
    /**
     * 系列促销阶段一金额
     */
    private String seriesPromotionAmtOne;

    /**
     * 系列促销阶段二数量
     */
    private String seriesPromotionQtyTwo;

    /**
     * 系列促销阶段二金额
     */
    private String seriesPromotionAmtTwo;

    /**
     * 系列促销阶段三数量
     */
    private String seriesPromotionQtyThree;
    /**
     * 系列促销阶段二金额
     */
    private String seriesPromotionAmtThree;
}
