//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetSyncSdStoreOutData {
    private String clientId;
    private String gsstBrId;
    private String gsstBrName;
    private String gsstSaleMaxQty;
    private String gsstLastDay;
    private String gsstPdStartDate;
    private String gsstVersion;
    private String gsstMinDiscountRate;
    private Double gsstDyqMaxAmt;
    private String gsstDailyOpenDate;
    private String gsstDailyChangeDate;
    private String gsstDailyCloseDate;
    private String gsstJfQldate;

    public GetSyncSdStoreOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsstBrId() {
        return this.gsstBrId;
    }

    public String getGsstBrName() {
        return this.gsstBrName;
    }

    public String getGsstSaleMaxQty() {
        return this.gsstSaleMaxQty;
    }

    public String getGsstLastDay() {
        return this.gsstLastDay;
    }

    public String getGsstPdStartDate() {
        return this.gsstPdStartDate;
    }

    public String getGsstVersion() {
        return this.gsstVersion;
    }

    public String getGsstMinDiscountRate() {
        return this.gsstMinDiscountRate;
    }

    public Double getGsstDyqMaxAmt() {
        return this.gsstDyqMaxAmt;
    }

    public String getGsstDailyOpenDate() {
        return this.gsstDailyOpenDate;
    }

    public String getGsstDailyChangeDate() {
        return this.gsstDailyChangeDate;
    }

    public String getGsstDailyCloseDate() {
        return this.gsstDailyCloseDate;
    }

    public String getGsstJfQldate() {
        return this.gsstJfQldate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsstBrId(final String gsstBrId) {
        this.gsstBrId = gsstBrId;
    }

    public void setGsstBrName(final String gsstBrName) {
        this.gsstBrName = gsstBrName;
    }

    public void setGsstSaleMaxQty(final String gsstSaleMaxQty) {
        this.gsstSaleMaxQty = gsstSaleMaxQty;
    }

    public void setGsstLastDay(final String gsstLastDay) {
        this.gsstLastDay = gsstLastDay;
    }

    public void setGsstPdStartDate(final String gsstPdStartDate) {
        this.gsstPdStartDate = gsstPdStartDate;
    }

    public void setGsstVersion(final String gsstVersion) {
        this.gsstVersion = gsstVersion;
    }

    public void setGsstMinDiscountRate(final String gsstMinDiscountRate) {
        this.gsstMinDiscountRate = gsstMinDiscountRate;
    }

    public void setGsstDyqMaxAmt(final Double gsstDyqMaxAmt) {
        this.gsstDyqMaxAmt = gsstDyqMaxAmt;
    }

    public void setGsstDailyOpenDate(final String gsstDailyOpenDate) {
        this.gsstDailyOpenDate = gsstDailyOpenDate;
    }

    public void setGsstDailyChangeDate(final String gsstDailyChangeDate) {
        this.gsstDailyChangeDate = gsstDailyChangeDate;
    }

    public void setGsstDailyCloseDate(final String gsstDailyCloseDate) {
        this.gsstDailyCloseDate = gsstDailyCloseDate;
    }

    public void setGsstJfQldate(final String gsstJfQldate) {
        this.gsstJfQldate = gsstJfQldate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdStoreOutData)) {
            return false;
        } else {
            GetSyncSdStoreOutData other = (GetSyncSdStoreOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label167;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gsstBrId = this.getGsstBrId();
                Object other$gsstBrId = other.getGsstBrId();
                if (this$gsstBrId == null) {
                    if (other$gsstBrId != null) {
                        return false;
                    }
                } else if (!this$gsstBrId.equals(other$gsstBrId)) {
                    return false;
                }

                label153: {
                    Object this$gsstBrName = this.getGsstBrName();
                    Object other$gsstBrName = other.getGsstBrName();
                    if (this$gsstBrName == null) {
                        if (other$gsstBrName == null) {
                            break label153;
                        }
                    } else if (this$gsstBrName.equals(other$gsstBrName)) {
                        break label153;
                    }

                    return false;
                }

                Object this$gsstSaleMaxQty = this.getGsstSaleMaxQty();
                Object other$gsstSaleMaxQty = other.getGsstSaleMaxQty();
                if (this$gsstSaleMaxQty == null) {
                    if (other$gsstSaleMaxQty != null) {
                        return false;
                    }
                } else if (!this$gsstSaleMaxQty.equals(other$gsstSaleMaxQty)) {
                    return false;
                }

                label139: {
                    Object this$gsstLastDay = this.getGsstLastDay();
                    Object other$gsstLastDay = other.getGsstLastDay();
                    if (this$gsstLastDay == null) {
                        if (other$gsstLastDay == null) {
                            break label139;
                        }
                    } else if (this$gsstLastDay.equals(other$gsstLastDay)) {
                        break label139;
                    }

                    return false;
                }

                Object this$gsstPdStartDate = this.getGsstPdStartDate();
                Object other$gsstPdStartDate = other.getGsstPdStartDate();
                if (this$gsstPdStartDate == null) {
                    if (other$gsstPdStartDate != null) {
                        return false;
                    }
                } else if (!this$gsstPdStartDate.equals(other$gsstPdStartDate)) {
                    return false;
                }

                label125: {
                    Object this$gsstVersion = this.getGsstVersion();
                    Object other$gsstVersion = other.getGsstVersion();
                    if (this$gsstVersion == null) {
                        if (other$gsstVersion == null) {
                            break label125;
                        }
                    } else if (this$gsstVersion.equals(other$gsstVersion)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$gsstMinDiscountRate = this.getGsstMinDiscountRate();
                    Object other$gsstMinDiscountRate = other.getGsstMinDiscountRate();
                    if (this$gsstMinDiscountRate == null) {
                        if (other$gsstMinDiscountRate == null) {
                            break label118;
                        }
                    } else if (this$gsstMinDiscountRate.equals(other$gsstMinDiscountRate)) {
                        break label118;
                    }

                    return false;
                }

                Object this$gsstDyqMaxAmt = this.getGsstDyqMaxAmt();
                Object other$gsstDyqMaxAmt = other.getGsstDyqMaxAmt();
                if (this$gsstDyqMaxAmt == null) {
                    if (other$gsstDyqMaxAmt != null) {
                        return false;
                    }
                } else if (!this$gsstDyqMaxAmt.equals(other$gsstDyqMaxAmt)) {
                    return false;
                }

                label104: {
                    Object this$gsstDailyOpenDate = this.getGsstDailyOpenDate();
                    Object other$gsstDailyOpenDate = other.getGsstDailyOpenDate();
                    if (this$gsstDailyOpenDate == null) {
                        if (other$gsstDailyOpenDate == null) {
                            break label104;
                        }
                    } else if (this$gsstDailyOpenDate.equals(other$gsstDailyOpenDate)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$gsstDailyChangeDate = this.getGsstDailyChangeDate();
                    Object other$gsstDailyChangeDate = other.getGsstDailyChangeDate();
                    if (this$gsstDailyChangeDate == null) {
                        if (other$gsstDailyChangeDate == null) {
                            break label97;
                        }
                    } else if (this$gsstDailyChangeDate.equals(other$gsstDailyChangeDate)) {
                        break label97;
                    }

                    return false;
                }

                Object this$gsstDailyCloseDate = this.getGsstDailyCloseDate();
                Object other$gsstDailyCloseDate = other.getGsstDailyCloseDate();
                if (this$gsstDailyCloseDate == null) {
                    if (other$gsstDailyCloseDate != null) {
                        return false;
                    }
                } else if (!this$gsstDailyCloseDate.equals(other$gsstDailyCloseDate)) {
                    return false;
                }

                Object this$gsstJfQldate = this.getGsstJfQldate();
                Object other$gsstJfQldate = other.getGsstJfQldate();
                if (this$gsstJfQldate == null) {
                    if (other$gsstJfQldate != null) {
                        return false;
                    }
                } else if (!this$gsstJfQldate.equals(other$gsstJfQldate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdStoreOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsstBrId = this.getGsstBrId();
        result = result * 59 + ($gsstBrId == null ? 43 : $gsstBrId.hashCode());
        Object $gsstBrName = this.getGsstBrName();
        result = result * 59 + ($gsstBrName == null ? 43 : $gsstBrName.hashCode());
        Object $gsstSaleMaxQty = this.getGsstSaleMaxQty();
        result = result * 59 + ($gsstSaleMaxQty == null ? 43 : $gsstSaleMaxQty.hashCode());
        Object $gsstLastDay = this.getGsstLastDay();
        result = result * 59 + ($gsstLastDay == null ? 43 : $gsstLastDay.hashCode());
        Object $gsstPdStartDate = this.getGsstPdStartDate();
        result = result * 59 + ($gsstPdStartDate == null ? 43 : $gsstPdStartDate.hashCode());
        Object $gsstVersion = this.getGsstVersion();
        result = result * 59 + ($gsstVersion == null ? 43 : $gsstVersion.hashCode());
        Object $gsstMinDiscountRate = this.getGsstMinDiscountRate();
        result = result * 59 + ($gsstMinDiscountRate == null ? 43 : $gsstMinDiscountRate.hashCode());
        Object $gsstDyqMaxAmt = this.getGsstDyqMaxAmt();
        result = result * 59 + ($gsstDyqMaxAmt == null ? 43 : $gsstDyqMaxAmt.hashCode());
        Object $gsstDailyOpenDate = this.getGsstDailyOpenDate();
        result = result * 59 + ($gsstDailyOpenDate == null ? 43 : $gsstDailyOpenDate.hashCode());
        Object $gsstDailyChangeDate = this.getGsstDailyChangeDate();
        result = result * 59 + ($gsstDailyChangeDate == null ? 43 : $gsstDailyChangeDate.hashCode());
        Object $gsstDailyCloseDate = this.getGsstDailyCloseDate();
        result = result * 59 + ($gsstDailyCloseDate == null ? 43 : $gsstDailyCloseDate.hashCode());
        Object $gsstJfQldate = this.getGsstJfQldate();
        result = result * 59 + ($gsstJfQldate == null ? 43 : $gsstJfQldate.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdStoreOutData(clientId=" + this.getClientId() + ", gsstBrId=" + this.getGsstBrId() + ", gsstBrName=" + this.getGsstBrName() + ", gsstSaleMaxQty=" + this.getGsstSaleMaxQty() + ", gsstLastDay=" + this.getGsstLastDay() + ", gsstPdStartDate=" + this.getGsstPdStartDate() + ", gsstVersion=" + this.getGsstVersion() + ", gsstMinDiscountRate=" + this.getGsstMinDiscountRate() + ", gsstDyqMaxAmt=" + this.getGsstDyqMaxAmt() + ", gsstDailyOpenDate=" + this.getGsstDailyOpenDate() + ", gsstDailyChangeDate=" + this.getGsstDailyChangeDate() + ", gsstDailyCloseDate=" + this.getGsstDailyCloseDate() + ", gsstJfQldate=" + this.getGsstJfQldate() + ")";
    }
}
