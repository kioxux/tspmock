//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaTaxCode;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.data.yaoka.CommonResponse;
import com.gys.common.data.yaoka.preCalc.PreCalcResp;
import com.gys.common.response.Result;
import com.pajk.CashCollectionOpen.model.pay.PayReq;
import com.pajk.CashCollectionOpen.model.pollReadyPlan.PollReadyPlanReq;
import com.pajk.CashCollectionOpen.model.preCalc.PreCalcReq;
import com.pajk.CashCollectionOpen.model.undoPay.UndoPayReq;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 胡鑫鑫
 */
public interface YaoKaService {

    CommonResponse<PreCalcResp> preCalcUtil(GetLoginOutData userInfo, PreCalcReq preCalc);

    CommonResponse payUtil(GetLoginOutData userInfo, PayReq pay);

    CommonResponse pollReadyPlanUtil(GetLoginOutData userInfo, PollReadyPlanReq preCalc);

    CommonResponse pingAnRefund(GetLoginOutData userInfo, UndoPayReq undoPayReq);
}
