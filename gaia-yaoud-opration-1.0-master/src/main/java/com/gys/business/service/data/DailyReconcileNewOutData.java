package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DailyReconcileNewOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gpdhBrId;
    @ApiModelProperty(value = "销售日期")
    private String gpdhSaleDate;
    @ApiModelProperty(value = "收银工号")
    private String gpdhEmp;
    @ApiModelProperty(value = "支付类型 1.销售支付 2.储值卡充值 3.挂号支付")
    private String gpdhType;
    @ApiModelProperty(value = "应收金额汇总")
    private String gpdhtotalSalesAmt;
    private List<DailyReconcileDetailNewOutData> detailOutDataList;
}
