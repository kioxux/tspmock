package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 重点商品任务—主表(GaiaKeyCommodityTaskBill)实体类
 *
 * @author XiaoZY
 * @since 2021-09-01 15:37:53
 */
@Data
public class GaiaKeyCommodityTaskBillInData implements Serializable {
    private static final long serialVersionUID = -93074848282309802L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 任务单号
     */
    private String billCode;
    /**
     * 任务名称
     */
    private String billName;
    /**
     * 开始日期
     */
    private Date startTime;
    /**
     * 结束日期
     */
    private Date endTime;
    /**
     * 任务天数
     */
    private Integer validQty;
    /**
     * 任务门店数
     */
    private Integer storeQty;
    /**
     * 销售量
     */
    private Double salesQty;
    /**
     * 销售额
     */
    private Double salesAmt;
    /**
     * 单据状态：1-已保存 1-已审核 2-停用
     */
    private Integer status;
    /**
     * 处理状态：0-未处理 1-已处理
     */
    private Integer dealStatus;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    private Date billDate;
    /**
     * 更新者
     */
    private String updateUser;
    /**
     * 新增门店集合
     */
    private List<String> insertStoCodeList;
    /**
     * 删除门店集合
     */
    private List<String> deleteStoCodeList;

    private List<Long> idList;

    private Integer pageNum;

    private Integer pageSize;
    private String proSelfCode;
    private String proCode;
    private String stoCode;
    private String startTimeStr;
    private String endTimeStr;


}
