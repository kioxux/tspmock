package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class CompanyOutOfStockOutData implements Serializable {

    /**
     * 效期商品数量
     */
    @ApiModelProperty(value = "效期商品数量")
    private Integer validCount;

    /**
     * 库存成本额
     */
    @ApiModelProperty(value = "库存成本额")
    private BigDecimal costAmt;

    /**
     * 30天销售额
     */
    @ApiModelProperty(value = "30天销售额")
    private BigDecimal saleAmt;

    /**
     * 30天毛利额
     */
    @ApiModelProperty(value = "30天毛利额")
    private BigDecimal profitAmt;

    /**
     * 效期商品明细列表
     */
    @ApiModelProperty(value = "效期商品明细列表")
    private List<ValidProductListOutData> productList;
}
