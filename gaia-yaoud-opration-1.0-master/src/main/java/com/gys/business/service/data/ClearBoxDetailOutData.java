//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.beans.ConstructorProperties;

public class ClearBoxDetailOutData {
    private String indexDetail;
    private String gscdProId;
    private String gscdProName;
    private String normalPrice;
    private String gscdBatchNo;
    private String gscdBatch;
    private String gscdValidDate;
    private String gscdQty;
    private String proFactoryName;
    private String proPlace;
    private String proForm;
    private String proUnit;
    private String proSpecs;
    private String proRegisterNo;
    private String gsadStockStatus;
    private String gscdSerial;

    public String getIndexDetail() {
        return this.indexDetail;
    }

    public String getGscdProId() {
        return this.gscdProId;
    }

    public String getGscdProName() {
        return this.gscdProName;
    }

    public String getNormalPrice() {
        return this.normalPrice;
    }

    public String getGscdBatchNo() {
        return this.gscdBatchNo;
    }

    public String getGscdBatch() {
        return this.gscdBatch;
    }

    public String getGscdValidDate() {
        return this.gscdValidDate;
    }

    public String getGscdQty() {
        return this.gscdQty;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public String getGsadStockStatus() {
        return this.gsadStockStatus;
    }

    public String getGscdSerial() {
        return this.gscdSerial;
    }

    public void setIndexDetail(final String indexDetail) {
        this.indexDetail = indexDetail;
    }

    public void setGscdProId(final String gscdProId) {
        this.gscdProId = gscdProId;
    }

    public void setGscdProName(final String gscdProName) {
        this.gscdProName = gscdProName;
    }

    public void setNormalPrice(final String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public void setGscdBatchNo(final String gscdBatchNo) {
        this.gscdBatchNo = gscdBatchNo;
    }

    public void setGscdBatch(final String gscdBatch) {
        this.gscdBatch = gscdBatch;
    }

    public void setGscdValidDate(final String gscdValidDate) {
        this.gscdValidDate = gscdValidDate;
    }

    public void setGscdQty(final String gscdQty) {
        this.gscdQty = gscdQty;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public void setGsadStockStatus(final String gsadStockStatus) {
        this.gsadStockStatus = gsadStockStatus;
    }

    public void setGscdSerial(final String gscdSerial) {
        this.gscdSerial = gscdSerial;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ClearBoxDetailOutData)) {
            return false;
        } else {
            ClearBoxDetailOutData other = (ClearBoxDetailOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label203: {
                    Object this$indexDetail = this.getIndexDetail();
                    Object other$indexDetail = other.getIndexDetail();
                    if (this$indexDetail == null) {
                        if (other$indexDetail == null) {
                            break label203;
                        }
                    } else if (this$indexDetail.equals(other$indexDetail)) {
                        break label203;
                    }

                    return false;
                }

                Object this$gscdProId = this.getGscdProId();
                Object other$gscdProId = other.getGscdProId();
                if (this$gscdProId == null) {
                    if (other$gscdProId != null) {
                        return false;
                    }
                } else if (!this$gscdProId.equals(other$gscdProId)) {
                    return false;
                }

                Object this$gscdProName = this.getGscdProName();
                Object other$gscdProName = other.getGscdProName();
                if (this$gscdProName == null) {
                    if (other$gscdProName != null) {
                        return false;
                    }
                } else if (!this$gscdProName.equals(other$gscdProName)) {
                    return false;
                }

                label182: {
                    Object this$normalPrice = this.getNormalPrice();
                    Object other$normalPrice = other.getNormalPrice();
                    if (this$normalPrice == null) {
                        if (other$normalPrice == null) {
                            break label182;
                        }
                    } else if (this$normalPrice.equals(other$normalPrice)) {
                        break label182;
                    }

                    return false;
                }

                label175: {
                    Object this$gscdBatchNo = this.getGscdBatchNo();
                    Object other$gscdBatchNo = other.getGscdBatchNo();
                    if (this$gscdBatchNo == null) {
                        if (other$gscdBatchNo == null) {
                            break label175;
                        }
                    } else if (this$gscdBatchNo.equals(other$gscdBatchNo)) {
                        break label175;
                    }

                    return false;
                }

                label168: {
                    Object this$gscdBatch = this.getGscdBatch();
                    Object other$gscdBatch = other.getGscdBatch();
                    if (this$gscdBatch == null) {
                        if (other$gscdBatch == null) {
                            break label168;
                        }
                    } else if (this$gscdBatch.equals(other$gscdBatch)) {
                        break label168;
                    }

                    return false;
                }

                Object this$gscdValidDate = this.getGscdValidDate();
                Object other$gscdValidDate = other.getGscdValidDate();
                if (this$gscdValidDate == null) {
                    if (other$gscdValidDate != null) {
                        return false;
                    }
                } else if (!this$gscdValidDate.equals(other$gscdValidDate)) {
                    return false;
                }

                label154: {
                    Object this$gscdQty = this.getGscdQty();
                    Object other$gscdQty = other.getGscdQty();
                    if (this$gscdQty == null) {
                        if (other$gscdQty == null) {
                            break label154;
                        }
                    } else if (this$gscdQty.equals(other$gscdQty)) {
                        break label154;
                    }

                    return false;
                }

                Object this$proFactoryName = this.getProFactoryName();
                Object other$proFactoryName = other.getProFactoryName();
                if (this$proFactoryName == null) {
                    if (other$proFactoryName != null) {
                        return false;
                    }
                } else if (!this$proFactoryName.equals(other$proFactoryName)) {
                    return false;
                }

                label140: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label140;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label140;
                    }

                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                Object this$proUnit = this.getProUnit();
                Object other$proUnit = other.getProUnit();
                if (this$proUnit == null) {
                    if (other$proUnit != null) {
                        return false;
                    }
                } else if (!this$proUnit.equals(other$proUnit)) {
                    return false;
                }

                label119: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label119;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label119;
                    }

                    return false;
                }

                label112: {
                    Object this$proRegisterNo = this.getProRegisterNo();
                    Object other$proRegisterNo = other.getProRegisterNo();
                    if (this$proRegisterNo == null) {
                        if (other$proRegisterNo == null) {
                            break label112;
                        }
                    } else if (this$proRegisterNo.equals(other$proRegisterNo)) {
                        break label112;
                    }

                    return false;
                }

                Object this$gsadStockStatus = this.getGsadStockStatus();
                Object other$gsadStockStatus = other.getGsadStockStatus();
                if (this$gsadStockStatus == null) {
                    if (other$gsadStockStatus != null) {
                        return false;
                    }
                } else if (!this$gsadStockStatus.equals(other$gsadStockStatus)) {
                    return false;
                }

                Object this$gscdSerial = this.getGscdSerial();
                Object other$gscdSerial = other.getGscdSerial();
                if (this$gscdSerial == null) {
                    if (other$gscdSerial != null) {
                        return false;
                    }
                } else if (!this$gscdSerial.equals(other$gscdSerial)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ClearBoxDetailOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $indexDetail = this.getIndexDetail();
        result = result * 59 + ($indexDetail == null ? 43 : $indexDetail.hashCode());
        Object $gscdProId = this.getGscdProId();
        result = result * 59 + ($gscdProId == null ? 43 : $gscdProId.hashCode());
        Object $gscdProName = this.getGscdProName();
        result = result * 59 + ($gscdProName == null ? 43 : $gscdProName.hashCode());
        Object $normalPrice = this.getNormalPrice();
        result = result * 59 + ($normalPrice == null ? 43 : $normalPrice.hashCode());
        Object $gscdBatchNo = this.getGscdBatchNo();
        result = result * 59 + ($gscdBatchNo == null ? 43 : $gscdBatchNo.hashCode());
        Object $gscdBatch = this.getGscdBatch();
        result = result * 59 + ($gscdBatch == null ? 43 : $gscdBatch.hashCode());
        Object $gscdValidDate = this.getGscdValidDate();
        result = result * 59 + ($gscdValidDate == null ? 43 : $gscdValidDate.hashCode());
        Object $gscdQty = this.getGscdQty();
        result = result * 59 + ($gscdQty == null ? 43 : $gscdQty.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        Object $gsadStockStatus = this.getGsadStockStatus();
        result = result * 59 + ($gsadStockStatus == null ? 43 : $gsadStockStatus.hashCode());
        Object $gscdSerial = this.getGscdSerial();
        result = result * 59 + ($gscdSerial == null ? 43 : $gscdSerial.hashCode());
        return result;
    }

    public String toString() {
        return "ClearBoxDetailOutData(indexDetail=" + this.getIndexDetail() + ", gscdProId=" + this.getGscdProId() + ", gscdProName=" + this.getGscdProName() + ", normalPrice=" + this.getNormalPrice() + ", gscdBatchNo=" + this.getGscdBatchNo() + ", gscdBatch=" + this.getGscdBatch() + ", gscdValidDate=" + this.getGscdValidDate() + ", gscdQty=" + this.getGscdQty() + ", proFactoryName=" + this.getProFactoryName() + ", proPlace=" + this.getProPlace() + ", proForm=" + this.getProForm() + ", proUnit=" + this.getProUnit() + ", proSpecs=" + this.getProSpecs() + ", proRegisterNo=" + this.getProRegisterNo() + ", gsadStockStatus=" + this.getGsadStockStatus() + ", gscdSerial=" + this.getGscdSerial() + ")";
    }

    @ConstructorProperties({"indexDetail", "gscdProId", "gscdProName", "normalPrice", "gscdBatchNo", "gscdBatch", "gscdValidDate", "gscdQty", "proFactoryName", "proPlace", "proForm", "proUnit", "proSpecs", "proRegisterNo", "gsadStockStatus", "gscdSerial"})
    public ClearBoxDetailOutData(final String indexDetail, final String gscdProId, final String gscdProName, final String normalPrice, final String gscdBatchNo, final String gscdBatch, final String gscdValidDate, final String gscdQty, final String proFactoryName, final String proPlace, final String proForm, final String proUnit, final String proSpecs, final String proRegisterNo, final String gsadStockStatus, final String gscdSerial) {
        this.indexDetail = indexDetail;
        this.gscdProId = gscdProId;
        this.gscdProName = gscdProName;
        this.normalPrice = normalPrice;
        this.gscdBatchNo = gscdBatchNo;
        this.gscdBatch = gscdBatch;
        this.gscdValidDate = gscdValidDate;
        this.gscdQty = gscdQty;
        this.proFactoryName = proFactoryName;
        this.proPlace = proPlace;
        this.proForm = proForm;
        this.proUnit = proUnit;
        this.proSpecs = proSpecs;
        this.proRegisterNo = proRegisterNo;
        this.gsadStockStatus = gsadStockStatus;
        this.gscdSerial = gscdSerial;
    }

    public ClearBoxDetailOutData() {
    }
}
