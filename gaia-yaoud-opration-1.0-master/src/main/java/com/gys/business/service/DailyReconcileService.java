//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.*;

import java.util.List;
import java.util.Map;

public interface DailyReconcileService {
    List<DailyReconcileOutData> getDailyReconcile(DailyReconcileInData inData);

    List<DailyReconcileDetailOutData> getDailyReconcileDetailList(DailyReconcileOutData inData);

    void insert(DailyReconcileOutData inData);

    void approve(DailyReconcileOutData inData);

    List<SaleReturnOutData> getSaleInfoList(DailyReconcileInData inData);

    List<RechargeCardPaycheckOutData> getRechargeInfoList(DailyReconcileInData inData);

    List<StoreOutData> getStoreData(StoreInData inData);

    void checkDailyReconcileInfo(DailyReconcileInData inData);

    List<DailyReconcileOutData> queryDailyReconcile(DailyReconcileInData inData);

//    List<DailyReconcileDetailNewOutData>getSaleAndRechargeInfoList(DailyReconcileInData inData);

    List<DailyReconcileNewOutData> getDailyInfoList(DailyReconcileInData inData);
}
