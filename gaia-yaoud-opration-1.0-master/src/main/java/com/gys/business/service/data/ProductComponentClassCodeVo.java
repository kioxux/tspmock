package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductComponentClassCodeVo implements Serializable {

    /**
     * 成分分类编码
     */
    @ApiModelProperty(value="成分分类编码")
    private String compCode;

    /**
     * 成分分类名称
     */
    @ApiModelProperty(value="成分分类名称")
    private String compName;

    /**
     * 子节点列表
     */
    @ApiModelProperty(value="子节点列表")
    List<ProductComponentClassCodeVo> childList;
}
