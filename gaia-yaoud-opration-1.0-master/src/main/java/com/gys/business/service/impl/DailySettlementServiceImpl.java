package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaSdDailyReconcile;
import com.gys.business.mapper.entity.GaiaSdDailyReconcileDetail;
import com.gys.business.service.DailySettlementService;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailAuthPay;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailOutData;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailVO;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@SuppressWarnings("all")
@Service
public class DailySettlementServiceImpl implements DailySettlementService {


    @Autowired
    private GaiaSdSaleHMapper saleHMapper; //主表

    @Autowired
    private GaiaSdSalePayMsgMapper payMsgMapper; //支付信息表

    @Autowired
    private GaiaSdRechargeCardPaycheckMapper paycheckDao; //储值卡

    @Autowired
    private GaiaDailyReconcileMapper gaiaDailyReconcileMapper; //日对账主表

    @Autowired
    private GaiaDailyReconcileDetailMapper gaiaDailyReconcileDetailMapper;//日结对账明细


    @Override
    public List<DailyReconcileDetailOutData> getSalesInquireList(GetLoginOutData userInfo, Map<String, Object> map) {
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        List<DailyReconcileDetailOutData> outData = null;
        DailyReconcileDetailAuthPay authPay = null;
        List<String> billNo = this.saleHMapper.getAllByBillAndDate(map);
        if (CollUtil.isNotEmpty(billNo)) {
            authPay = this.saleHMapper.getAuthPay(userInfo.getClient(),userInfo.getDepId(),billNo);
        }

        List<String> cardNo = this.paycheckDao.getAllByBillAndDate(map);
        if (CollUtil.isEmpty(billNo) && CollUtil.isEmpty(cardNo)) {
            return outData ;
        }
        billNo.addAll(cardNo);
        map.put("list",billNo);
        List<DailyReconcileDetailOutData> detailOutData = this.payMsgMapper.getAllBiNo(map);
        for (DailyReconcileDetailOutData outDatum : detailOutData) {
            outDatum.setSaleYsAmt(NumberUtil.toStr(NumberUtil.parseNumber(outDatum.getSaleYsAmt())));
            outDatum.setCardYsAmt(NumberUtil.toStr(NumberUtil.parseNumber(outDatum.getCardYsAmt())));
            outDatum.setOrderAmt(NumberUtil.toStr(NumberUtil.parseNumber(outDatum.getOrderAmt())));
            outDatum.setBillNo(billNo);
            outDatum.setCardNo(cardNo);
            if (ObjectUtil.isNotEmpty(authPay)) {
                if ("9001".equals(outDatum.getPmId())) {//积分抵现
                    outDatum.setSaleYsAmt(NumberUtil.toStr(NumberUtil.parseNumber((StrUtil.isBlank(authPay.getIntegral()) ? "0" : authPay.getIntegral()))));
                }else if ("9002".equals(outDatum.getPmId())) {//电子券
                    outDatum.setSaleYsAmt(NumberUtil.toStr(NumberUtil.parseNumber((StrUtil.isBlank(authPay.getEBrokers()) ? "0" : authPay.getEBrokers()))));
                } else if ("9003".equals(outDatum.getPmId())) {//抵用券
                    outDatum.setSaleYsAmt(NumberUtil.toStr(NumberUtil.parseNumber(StrUtil.isBlank(authPay.getVoucher()) ? "0" : authPay.getVoucher())));
                }
            }
        }
        return detailOutData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void reconciliationReview(GetLoginOutData userInfo, DailyReconcileDetailVO detailVO) {
        if (CollUtil.isNotEmpty(detailVO.getDetail())) {
            boolean flag = false;
            for (DailyReconcileDetailOutData datum : detailVO.getDetail()) {
                if (StrUtil.isNotBlank(datum.getAmountReconciliation()) && (new BigDecimal(StrUtil.isBlank(datum.getAmountReconciliation()) ? "0" : datum.getAmountReconciliation()).compareTo(BigDecimal.ZERO) ==  1) || new BigDecimal(StrUtil.isBlank(datum.getAmountReconciliation()) ? "0" : datum.getAmountReconciliation()).compareTo(BigDecimal.ZERO) == -1 ) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                throw new BusinessException("审核数据不可为空!");
            }
        }

        //所有的销售单号 和 充值单号
        String voucherId = this.gaiaDailyReconcileMapper.selectNextVoucherId(userInfo.getClient());
        Map<String, Object> map =  new HashMap<>();
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());

        List<String> billNo = detailVO.getDetail().get(0).getBillNo();
        if (CollUtil.isNotEmpty(billNo)) {
            map.put("billNo",billNo);
            List<String> review = this.saleHMapper.verifyWhetherToReview(map);
            if (CollUtil.isNotEmpty(review)) {
                throw new BusinessException("此数据已日结,请勿重复操作!");
            }
            this.saleHMapper.updateList(userInfo.getClient(),userInfo.getDepId(),billNo);//销售主表
        }

        List<String> cardNo = detailVO.getDetail().get(0).getCardNo();
        if (CollUtil.isNotEmpty(cardNo)) {
            map.put("cardNo",cardNo);
            List<String> review = this.paycheckDao.verifyWhetherToReview(map);
            if (CollUtil.isNotEmpty(review)) {
                throw new BusinessException("此数据已日结,请勿重复操作!");
            }
            this.paycheckDao.updateList(userInfo.getClient(),userInfo.getDepId(),cardNo);//储值卡
            billNo.addAll(cardNo);
        }

        if (CollUtil.isNotEmpty(billNo)) {
            this.payMsgMapper.updateList(userInfo.getClient(),userInfo.getDepId(),billNo);//支付信息
        }




        GaiaSdDailyReconcile reconcile = this.getGaiaSdDailyReconcile(userInfo,voucherId,detailVO);
        this.gaiaDailyReconcileMapper.inserts(reconcile);

        List<GaiaSdDailyReconcileDetail> list = this.getGaiaSdDailyReconcileDetails(userInfo, detailVO.getDetail(), voucherId);
        this.gaiaDailyReconcileDetailMapper.insertLists(list);


    }

    /**
     * 明细表 填充参数
     * @param userInfo
     * @param detailOutData
     * @param voucherId
     * @return
     */
    private List<GaiaSdDailyReconcileDetail> getGaiaSdDailyReconcileDetails(GetLoginOutData userInfo, List<DailyReconcileDetailOutData> detailOutData, String voucherId) {
        List<GaiaSdDailyReconcileDetail> list = new ArrayList<>();
        int a = 1;
        for (DailyReconcileDetailOutData datum : detailOutData) {
            GaiaSdDailyReconcileDetail detail = new GaiaSdDailyReconcileDetail();
            detail.setClientId(userInfo.getClient());//加盟商
            detail.setGspddVoucherId(voucherId);//单号
            detail.setGspddBrId(userInfo.getDepId());//店号
            detail.setGspddSerial(StrUtil.toString(a));//行号
            detail.setGspddPayType("1");//销售支付
            detail.setGspddSalePaymethodId(datum.getPmId());//支付方式
            detail.setGspddSaleReceivableAmt(datum.getSaleYsAmt());//应收金额
            detail.setGspddSaleInputAmt(StrUtil.isBlank(datum.getAmountReconciliation()) ? "0" : datum.getAmountReconciliation()); //对账金额
            detail.setGspddSaleDifference(StrUtil.isBlank(datum.getDifferenceAmt()) ? "0" : datum.getDifferenceAmt());//差异金额
            detail.setGspddSaleCardAmt(StrUtil.isBlank(datum.getCardYsAmt()) ? "0" : datum.getCardYsAmt());//储值卡金额
            detail.setGspddSaleDgAmt(StrUtil.isBlank(datum.getOrderAmt()) ? "0" : datum.getOrderAmt());//订购
            list.add(detail);
            a++;
        }
        return list;
    }

    /**
     * 填充参数
     * @param userInfo
     * @return
     */
    private GaiaSdDailyReconcile getGaiaSdDailyReconcile(GetLoginOutData userInfo,String voucherId,DailyReconcileDetailVO detailVO) {
        GaiaSdDailyReconcile reconcile = new GaiaSdDailyReconcile();
        reconcile.setClientId(userInfo.getClient()); //加盟商
        reconcile.setGpdhVoucherId(voucherId);//单号
        reconcile.setGpdhBrId(userInfo.getDepId());//店号
        reconcile.setGpdhSaleDate(detailVO.getSaleDate());//销售日期
        reconcile.setGpdhCheckDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));//审核日期
        reconcile.setGpdhCheckTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));//审核时间
        reconcile.setGpdhEmp(userInfo.getUserId());//审核人
        reconcile.setGpdhtotalSalesAmt(detailVO.getSaleYsAmt());//应收金额汇总
        reconcile.setGspdhtotalCardAmt(detailVO.getCardYsAmt());//储值卡金额汇总
        reconcile.setGpdhtotalInputAmt(detailVO.getAmountReconciliation());//对账金额
        reconcile.setGspdhtotalDgAmt(detailVO.getOrderAmt());//订购金额
        reconcile.setGpdhStatus("Y");//审核状态
        return reconcile;
    }
}
