package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PoPriceProductHistoryOutData implements Serializable {

    /**
     * 门店编码
     */
    @ApiModelProperty(value = "门店编码")
    private String stoCode;

    /**
     * 门店名称
     */
    @ApiModelProperty(value = "门店名称")
    private String stoName;

    /**
     * 收货日期
     */
    @ApiModelProperty(value = "收货日期")
    private String acceptDate;

    /**
     * 收货单号
     */
    @ApiModelProperty(value = "收货单号")
    private String acceptOrderId;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码")
    private String proId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String proName;

    /**
     * 商品规格
     */
    @ApiModelProperty(value = "商品规格")
    private String proSpecs;

    /**
     * 生产厂家
     */
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;

    /**
     * 收货数量
     */
    @ApiModelProperty(value = "收货数量")
    private BigDecimal acceptQty;

    /**
     * 收货价格
     */
    @ApiModelProperty(value = "收货价格")
    private BigDecimal acceptPrice;

    /**
     * 供应商编号
     */
    @ApiModelProperty(value = "供应商编号")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

}
