//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.gys.business.mapper.UserMapper;
import com.gys.business.mapper.entity.User;
import com.gys.business.service.UserService;
import com.gys.business.service.data.GetUserBaseInData;
import com.gys.business.service.data.GetUserOutData;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    public UserServiceImpl() {
    }

    public GetUserOutData selectByAccount(GetUserBaseInData inData) {
        GetUserOutData outData = null;
        User user = this.userMapper.selectByAccount(inData.getAccount());
        if (user != null) {
            outData = new GetUserOutData();
            BeanUtils.copyProperties(user, outData);
        }

        return outData;
    }
}
