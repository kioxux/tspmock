package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "商品信息对象",description = "商品信息对象")
public class GetProductInfoQueryOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "商品自编码")
    private String proCode;
    @ApiModelProperty(value = "通用名称")
    private String proCommonname;
    @ApiModelProperty(value = "商品描述")
    private String proDepict;
    @ApiModelProperty(value = "助记码")
    private String proPym;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "零售价")
    private String prcAmount;
    @ApiModelProperty(value = "细分剂型")
    private String proPartform;
    @ApiModelProperty(value = "国际条形码1")
    private String proBarcode;
    @ApiModelProperty(value = "国际条形码2")
    private String proBacode2;
    /**
    *批准文号分类 1-国产药品，2-进口药品，3-国产器械，4-进口器械，5-中药饮片，6-特殊化妆品，7-消毒用品，8-保健食品，9-QS商品，10-其它
     **/
    @ApiModelProperty(value = "批准文号分类")
    private String proRegisterClass;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "商品分类")
    private String proClass;
    @ApiModelProperty(value = "商品分类描述")
    private String proClassName;
    @ApiModelProperty(value = "成分分类")
    private String proCompclass;
    @ApiModelProperty(value = "成分分类描述")
    private String proCompclassName;
    @ApiModelProperty(value = "成分优先级")
    private String proCompclassPriority;
    @ApiModelProperty(value = "处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方")
    private String proPresclass;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "商标")
    private String proMark;
    @ApiModelProperty(value = "品牌标识名")
    private String proBrand;
    @ApiModelProperty(value = "品牌区分 1-全国品牌，2-省份品牌，3-地区品牌，4-本地品牌，5-其它")
    private String proBrandClass;
    @ApiModelProperty(value = "保质期")
    private String proLife;
    @ApiModelProperty(value = "保质期单位 1-天，2-月，3-年")
    private String proLifeUnit;
    @ApiModelProperty(value = "上市许可持有人")
    private String proHolder;
    @ApiModelProperty(value = "药品本位码")
    private String proBasicCode;
    @ApiModelProperty(value = "税务分类编码")
    private String proTaxClass;
    /**
     *  1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制
     */
    @ApiModelProperty(value = "管制特殊分类")
    private String proControlClass;
    @ApiModelProperty(value = "生产类别 1-辅料，2-化学药品，3-生物制品，4-中药，5-器械")
    private String proProduceClass;
    @ApiModelProperty(value = "贮存条件 1-常温，2-阴凉，3-冷藏")
    private String proStorageCondition;
    /**
     *  1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区
     */
    @ApiModelProperty(value = "商品仓储分区")
    private String proStorageArea;
    @ApiModelProperty(value = "是否拆零 1是0否")
    private String proIfpart;
    @ApiModelProperty(value = "拆零比例")
    private String proPartRate;
//    @ApiModelProperty(value = "拆零单位")
//    private String proPartUnit;
    @ApiModelProperty(value = "中包装量")
    private String proMidPackage;
    @ApiModelProperty(value = "大包装量")
    private String proBigPackage;
    @ApiModelProperty(value = "启用电子监管码 0-否，1-是")
    private String proElectronnicCode;
    @ApiModelProperty(value = "最大销售量")
    private String proMaxSales;
    @ApiModelProperty(value = "说明书代码")
    private String proInstructionCode;
    @ApiModelProperty(value = "说明书内容")
    private String proInstruction;
    @ApiModelProperty(value = "国家医保品种 0-否，1-是")
    private String proMedProdct;
    @ApiModelProperty(value = "国家医保品种编码")
    private String proMedProdctCode;
    @ApiModelProperty(value = "生产国家")
    private String proCountry;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "零售价")
    private String gsppPriceNormal;
    @ApiModelProperty(value = "拆零价")
    private String gsppPriceCl;
    @ApiModelProperty(value = "拆零单位")
    private String proPartUint;
    @ApiModelProperty(value = "序号")
    private Integer index;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "数量")
    private String stockQty;
    @ApiModelProperty(value = "效期")
    private String vaildDate;
    @ApiModelProperty(value = "")
    private String vaild;
    @ApiModelProperty(value = "总库存数")
    private String totalQty;
    @ApiModelProperty(value = "区域")
    private String gsplArea;
    @ApiModelProperty(value = "柜组")
    private String gsplGroup;
    @ApiModelProperty(value = "货架")
    private String gsplShelf;
    @ApiModelProperty(value = "层级")
    private String gsplStorey;
    @ApiModelProperty(value = "货位")
    private String gsplSeat;
    @ApiModelProperty(value = "每页数量")
    public int pageSize;
    @ApiModelProperty(value = "页码")
    private int pageNum;
    @ApiModelProperty(value = "用法用量")
    private String proUsage;
    @ApiModelProperty(value = "禁忌说明")
    private String proContraindication;

    @ApiModelProperty(value = "商品提示")
    private String gsstExplain5;

    @ApiModelProperty(value = "商品禁忌")
    private String gsscExplain5;

    @ApiModelProperty(value = "商品冲突")
    private String gssdExplain5;
    //定位
    private String proPosition;
}
