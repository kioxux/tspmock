package com.gys.business.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectSmallOasSicknessPageInfoResponse {
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "疾病编码")
    private String smallSicknessCoding;

    @ApiModelProperty(value = "疾病名称")
    private String smallSicknessCodingName;
}
