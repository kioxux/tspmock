//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class CashPaymentDetailOutData {
    private String clientId;
    private String gsddVoucherId;
    private String gsddBrId;
    private String gsddSaleDate;
    private String gsddEmpGroup;
    private String gsddEmp;
    private String gsddRmbAmt;
    private Integer indexDetail;

    public CashPaymentDetailOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsddVoucherId() {
        return this.gsddVoucherId;
    }

    public String getGsddBrId() {
        return this.gsddBrId;
    }

    public String getGsddSaleDate() {
        return this.gsddSaleDate;
    }

    public String getGsddEmpGroup() {
        return this.gsddEmpGroup;
    }

    public String getGsddEmp() {
        return this.gsddEmp;
    }

    public String getGsddRmbAmt() {
        return this.gsddRmbAmt;
    }

    public Integer getIndexDetail() {
        return this.indexDetail;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsddVoucherId(final String gsddVoucherId) {
        this.gsddVoucherId = gsddVoucherId;
    }

    public void setGsddBrId(final String gsddBrId) {
        this.gsddBrId = gsddBrId;
    }

    public void setGsddSaleDate(final String gsddSaleDate) {
        this.gsddSaleDate = gsddSaleDate;
    }

    public void setGsddEmpGroup(final String gsddEmpGroup) {
        this.gsddEmpGroup = gsddEmpGroup;
    }

    public void setGsddEmp(final String gsddEmp) {
        this.gsddEmp = gsddEmp;
    }

    public void setGsddRmbAmt(final String gsddRmbAmt) {
        this.gsddRmbAmt = gsddRmbAmt;
    }

    public void setIndexDetail(final Integer indexDetail) {
        this.indexDetail = indexDetail;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof CashPaymentDetailOutData)) {
            return false;
        } else {
            CashPaymentDetailOutData other = (CashPaymentDetailOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label107;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label107;
                    }

                    return false;
                }

                Object this$gsddVoucherId = this.getGsddVoucherId();
                Object other$gsddVoucherId = other.getGsddVoucherId();
                if (this$gsddVoucherId == null) {
                    if (other$gsddVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsddVoucherId.equals(other$gsddVoucherId)) {
                    return false;
                }

                Object this$gsddBrId = this.getGsddBrId();
                Object other$gsddBrId = other.getGsddBrId();
                if (this$gsddBrId == null) {
                    if (other$gsddBrId != null) {
                        return false;
                    }
                } else if (!this$gsddBrId.equals(other$gsddBrId)) {
                    return false;
                }

                label86: {
                    Object this$gsddSaleDate = this.getGsddSaleDate();
                    Object other$gsddSaleDate = other.getGsddSaleDate();
                    if (this$gsddSaleDate == null) {
                        if (other$gsddSaleDate == null) {
                            break label86;
                        }
                    } else if (this$gsddSaleDate.equals(other$gsddSaleDate)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$gsddEmpGroup = this.getGsddEmpGroup();
                    Object other$gsddEmpGroup = other.getGsddEmpGroup();
                    if (this$gsddEmpGroup == null) {
                        if (other$gsddEmpGroup == null) {
                            break label79;
                        }
                    } else if (this$gsddEmpGroup.equals(other$gsddEmpGroup)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$gsddEmp = this.getGsddEmp();
                    Object other$gsddEmp = other.getGsddEmp();
                    if (this$gsddEmp == null) {
                        if (other$gsddEmp == null) {
                            break label72;
                        }
                    } else if (this$gsddEmp.equals(other$gsddEmp)) {
                        break label72;
                    }

                    return false;
                }

                Object this$gsddRmbAmt = this.getGsddRmbAmt();
                Object other$gsddRmbAmt = other.getGsddRmbAmt();
                if (this$gsddRmbAmt == null) {
                    if (other$gsddRmbAmt != null) {
                        return false;
                    }
                } else if (!this$gsddRmbAmt.equals(other$gsddRmbAmt)) {
                    return false;
                }

                Object this$indexDetail = this.getIndexDetail();
                Object other$indexDetail = other.getIndexDetail();
                if (this$indexDetail == null) {
                    if (other$indexDetail != null) {
                        return false;
                    }
                } else if (!this$indexDetail.equals(other$indexDetail)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CashPaymentDetailOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsddVoucherId = this.getGsddVoucherId();
        result = result * 59 + ($gsddVoucherId == null ? 43 : $gsddVoucherId.hashCode());
        Object $gsddBrId = this.getGsddBrId();
        result = result * 59 + ($gsddBrId == null ? 43 : $gsddBrId.hashCode());
        Object $gsddSaleDate = this.getGsddSaleDate();
        result = result * 59 + ($gsddSaleDate == null ? 43 : $gsddSaleDate.hashCode());
        Object $gsddEmpGroup = this.getGsddEmpGroup();
        result = result * 59 + ($gsddEmpGroup == null ? 43 : $gsddEmpGroup.hashCode());
        Object $gsddEmp = this.getGsddEmp();
        result = result * 59 + ($gsddEmp == null ? 43 : $gsddEmp.hashCode());
        Object $gsddRmbAmt = this.getGsddRmbAmt();
        result = result * 59 + ($gsddRmbAmt == null ? 43 : $gsddRmbAmt.hashCode());
        Object $indexDetail = this.getIndexDetail();
        result = result * 59 + ($indexDetail == null ? 43 : $indexDetail.hashCode());
        return result;
    }

    public String toString() {
        return "CashPaymentDetailOutData(clientId=" + this.getClientId() + ", gsddVoucherId=" + this.getGsddVoucherId() + ", gsddBrId=" + this.getGsddBrId() + ", gsddSaleDate=" + this.getGsddSaleDate() + ", gsddEmpGroup=" + this.getGsddEmpGroup() + ", gsddEmp=" + this.getGsddEmp() + ", gsddRmbAmt=" + this.getGsddRmbAmt() + ", indexDetail=" + this.getIndexDetail() + ")";
    }
}
