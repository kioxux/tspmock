package com.gys.business.service.data;

import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/7/20 16:13
 */
@Data
public class SaleDRebornData {

    private String batchNo;//批号
    private String validDate;//效期
    private String serial;//序号
    private String bQty;//库存
    private String dQty;//数量
}
