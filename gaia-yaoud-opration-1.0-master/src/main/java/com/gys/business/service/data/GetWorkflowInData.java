package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetWorkflowInData {
    private String wfTitle;
    private String userId;
    private String client;
    private String wfCode;
    private String cc;
    private String result;
    private String memo;
    private BigDecimal rejectSeq;
    private Integer pageNum;
    private Integer pageSize;
    private String depId;
    private String defineCode;
    private String orderNo;
    private int autoApprove = 0;
}
