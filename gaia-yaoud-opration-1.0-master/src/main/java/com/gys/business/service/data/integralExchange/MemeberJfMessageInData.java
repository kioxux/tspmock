package com.gys.business.service.data.integralExchange;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class MemeberJfMessageInData {
    private String clientId;//加盟商
    private String brId;//门店
    private List<IntegralOrderPro> pro;
    /**
     * 会员卡号
     */
    private String cardNo;
    /**
     * 订单号
     */
    private String billNo;
    /**
     * 积分抵用金额
     */
    private BigDecimal integralAmt;
    /**
     * 积分换购的积分统计
     */
    private BigDecimal integralDhPoint;
}
