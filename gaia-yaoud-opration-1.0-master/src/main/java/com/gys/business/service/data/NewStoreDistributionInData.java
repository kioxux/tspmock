package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class NewStoreDistributionInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店编码 必填")
    private String storeCode;

    @ApiModelProperty(value = "新店面积，必填 1-(60-80)   2-(80-100)  3-(100-120)  4-(120以上)")
    private String newStoreSize;

    @ApiModelProperty(value = "预估药品销售占比，必填  1-60  2-70  3-80 4-80及以上")
    private String expectDrugSaleProp;

    @ApiModelProperty(value = "计划药品铺货品项数 必填")
    private Integer distributionNumberOfPlan;

    @ApiModelProperty(value = "参考门店编码")
    private String referenceStoreCode;
}
