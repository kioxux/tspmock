package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/12 20:51
 **/
@Data
public class CallOrderForm {
    @ApiModelProperty(value = "加盟商", example = "10000001", hidden = true)
    private String client;
    @ApiModelProperty(value = "用户ID", example = "1116", hidden = true)
    private String userId;
    @ApiModelProperty(value = "门店编码", example = "10001")
    private String storeId;
    @ApiModelProperty(value = "要货开始日期", example = "2021-05-01")
    private String startDate;
    @ApiModelProperty(value = "要货截止日期", example = "2021=05-30")
    private String endDate;
}
