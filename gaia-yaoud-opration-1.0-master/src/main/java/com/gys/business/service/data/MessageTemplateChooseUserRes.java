package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MessageTemplateChooseUserRes implements Serializable {
    private static final long serialVersionUID = -3635557990291312192L;

    private String deptId;

    private String deptName;

    private String positionId;

    private String positionName;

    private String userId;

    private String userName;

    private String userTel;

    private String client;

    private Integer id;//用户自定义配置表的id

    private List<CommonVo> sites;

    private String gmtIfShowMao;

    private String model;

//    private boolean deleteFlag;// 第一次刷新时不能删除，为false

}
