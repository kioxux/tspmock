//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.yinHaiModel.*;

import java.util.List;

public interface YinHaiMedicalInsuranceService {

    JsonResult insertCancelStockInfo(GetLoginOutData inData);

    JsonResult addOrder(GetLoginOutData inData);

    JsonResult insertInventoryManagerOrders(GetLoginOutData inData);

    JsonResult getOrderModifyInfo(GetLoginOutData inData);

    List<ClientInfoInput> listSendClientInfo(ClientInfoInput inData);

    List<GoodsInfoInput> listSendGoodsInfo(GoodsInfoInput inData);

    List<StockInfoInput>  listSendStockDetailInfo(StockDetailInfoInput inData);

    Boolean insertSwapClientInfo(List<ClientInfoInput> inData, GetLoginOutData userInfo);

    Boolean insertSwapGoodsInfo(List<GoodsInfoInput> inData, GetLoginOutData userInfo);

    Boolean insertSwapStockInfo(List<StockDetailInfoInput> inData, GetLoginOutData userInfo);

    List<TransforStockInfoInput> listSendTransforStockDetailInfo(TransforStockInfoInput inData, GetLoginOutData userInfo);
}
