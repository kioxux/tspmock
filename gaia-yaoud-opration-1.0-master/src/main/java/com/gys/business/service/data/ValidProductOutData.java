package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ValidProductOutData implements Serializable {

    @ApiModelProperty(value = "效期商品数量")
    private BigDecimal validProCount;

    @ApiModelProperty(value = "效期商品库存成本额")
    private BigDecimal costAmt;

    @ApiModelProperty(value = "效期商品库存30天销售额")
    private BigDecimal saleAmt;

    @ApiModelProperty(value = "效期商品库存30天毛利额")
    private BigDecimal profitAmt;

    @ApiModelProperty(value = "效期商品列表")
    private List<ValidProductListOutData> validProductList;
}
