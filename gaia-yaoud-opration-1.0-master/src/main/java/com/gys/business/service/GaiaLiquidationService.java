package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaLiquidation;
import com.gys.common.data.GetLoginOutData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * 医保清算表(GaiaLiquidation)表服务接口
 *
 * @author makejava
 * @since 2021-12-29 15:00:29
 */
public interface GaiaLiquidationService {

    Boolean commit(GetLoginOutData loginUser, GaiaLiquidation inData);

    Boolean revoke(GetLoginOutData loginUser, GaiaLiquidation inData);

    List<GaiaLiquidation> getList(GetLoginOutData loginUser, GaiaLiquidation inData);
}
