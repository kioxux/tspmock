package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

import javax.persistence.Column;

/**
 * @author Zhangchi
 * @since 2021/11/18/11:00
 */
@Data
@CsvRow
public class PriceRangeOutData {
    /**
     * 加盟商
     */
    @CsvCell(title = "加盟商", index = 1, fieldNo = 1)
    private String client;

    /**
     * 客户名称
     */
    @CsvCell(title = "客户名称", index = 2, fieldNo = 1)
    private String customerName;

    /**
     * 城市ID
     */
    private String cityId;

    /**
     * 城市
     */
    @CsvCell(title = "城市", index = 3, fieldNo = 1)
    private String city;

    /**
     * 价格区间等级：ABCD
     */
    @CsvCell(title = "价格区间等级：ABCD", index = 4, fieldNo = 1)
    private String rangeLevel;

    /**
     * 起始价格
     */
    @CsvCell(title = "起始价格", index = 5, fieldNo = 1)
    private Integer startPrice;

    /**
     * 结束价格
     */
    @CsvCell(title = "结束价格", index = 6, fieldNo = 1)
    private Integer endPrice;

    /**
     * 价格间隔
     */
    @CsvCell(title = "价格间隔", index = 7, fieldNo = 1)
    private Integer priceInterval;

    /**
     * 更新时间
     */
    @CsvCell(title = "更新时间", index = 8, fieldNo = 1)
    @Column(name = "UPDATE_TIME")
    private String updateTime;

    /**
     * 更新者
     */
    @CsvCell(title = "更新者", index = 9, fieldNo = 1)
    @Column(name = "UPDATE_USER")
    private String updateUser;
}
