package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class WithoutTheOriginalReturnVo implements Serializable {
    private static final long serialVersionUID = 46168893829363446L;
    /**
     * 序号
     */
    private Integer index;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpec;

    /**
     * 厂家名称
     */
    private String proFactoryName;

    /**
     * 批号
     */
    private String proBatchNo;

    /**
     * 有效期
     */
    private String proValidity;

    /**
     * 进货价(零售价)
     */
    private String proPrice ;

    /**
     * 退货数量
     */
    private String returnNum;

    /**
     * 退货金额
     */
    private String returnAmt;

    /**
     * 营业员编码
     */
    private String assistantNo;

    /**
     * 税率
     */
    private String taxRate;
}
