package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class MonthPushMoneyByStoreInData {
    private String client;
    @ApiModelProperty(value = "提成月份")
    private String suitMonth;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "开始时间")
    private String startDate;
    @ApiModelProperty(value = "结束时间")
    private String endDate;
    private Integer pageNum;
    private Integer pageSize;
}
