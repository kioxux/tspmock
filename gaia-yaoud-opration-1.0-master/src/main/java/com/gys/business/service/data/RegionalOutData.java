package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaRegionalStore;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 14:40 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class RegionalOutData implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 区域编码
     */
    private String regionalCode;

    /**
     * 区域名称
     */
    private String regionalName;

    /**
     * 区域级别 3：三级区域、2：二级区域、1：一级区域
     */
    private Integer level;

    /**
     * 排序编码
     */
    private Integer sortNum;

    /**
     * 门店数量
     */
    private Integer storeNum;

    /**
     * 上级区域ID
     */
    private Long parentId;

    /**
     * 是否为默认 0：否、1：是
     */
    private String isDefault;

    /**
     * 子区域
     */
    private List<RegionalOutData> regionals;

    /**
     * 所属门店
     */
    private List<GaiaRegionalStore> stores;
}
