package com.gys.business.service.data.DtYb.ReturnExamime;

import lombok.Data;

import java.util.List;

/**
 * @Description 大同易联众 门店退货商品
 * @Author huxinxin
 * @Date 2021/5/7 17:35
 * @Version 1.0.0
 **/
@Data
public class ReturnExamimeVO {
    // 单据退回日期
    private String returnDate;
    // 退货单据号
    private String returnCode;
    // 供应商编码
    private String  supCode;
    // 供应商名称
    private String  supName;
    //商品集合
    private List<ReturnExamimeProVO> proList;
}
