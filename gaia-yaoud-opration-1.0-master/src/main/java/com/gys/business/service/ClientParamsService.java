package com.gys.business.service;

import com.gys.business.service.data.GaiaClSystemPara;

/**
 * @desc: 加盟商参数服务类
 * @author: Ryan
 * @createTime: 2021/12/13 17:04
 */
public interface ClientParamsService {

    GaiaClSystemPara getUnique(GaiaClSystemPara cond);

}
