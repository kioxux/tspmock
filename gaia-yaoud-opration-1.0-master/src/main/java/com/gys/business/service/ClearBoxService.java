//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.AddClearBoxOutData;
import com.gys.business.service.data.ClearBoxInfoOutData;
import com.gys.business.service.data.GetOneClearBoxOutData;
import java.util.List;

public interface ClearBoxService {
    GetOneClearBoxOutData getOneClearBox(ClearBoxInfoOutData inData);

    List<ClearBoxInfoOutData> getClearBox(ClearBoxInfoOutData inData);

    void examine(GetOneClearBoxOutData inData);

    void addClearBox(AddClearBoxOutData inData);
}
