package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaComeAndGoDetailData;
import com.gys.business.service.data.GaiaComeAndGoQueryData;
import com.gys.business.service.data.GaiaComeAndGoSavePaymentInData;
import com.gys.business.service.data.GaiaComeAndGoSummeryQueryData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;

import java.util.List;
import java.util.Map;

public interface ComeAndGoManageService {
    JsonResult comeAndGoBatchImportExcel(GaiaComeAndGoBatchImportInData inData);

    JsonResult getDcList(String client);

    JsonResult batchSavePayment(List<GaiaComeAndGoSavePaymentInData> inData, GetLoginOutData userInfo);

    JsonResult batchRemovePayment(List<GaiaComeAndGoSavePaymentInData> inData, GetLoginOutData loginUser);

    PageInfo getComeAndGoDetailList(GaiaComeAndGoQueryData inData);

    PageInfo getComeAndGoSummaryList(GaiaComeAndGoSummeryQueryData inData);

    void computeComeAndGoResAmt();

    JsonResult findPaymentList(String client);

    JsonResult batchSaveComeAndGoDetail(List<GaiaComeAndGoDetailData> detailList, GetLoginOutData loginUser);

    JsonResult getStoList(String client, Map<String,String> queryString);

    void autoComputeDistributionDataAndAddComeAndGoDetail();

    JsonResult getStartDate(String client,Map<String,String> map);
}
