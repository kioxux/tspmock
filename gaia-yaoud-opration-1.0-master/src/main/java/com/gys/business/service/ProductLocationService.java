//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetProductLocationInData;
import com.gys.business.service.data.GetProductLocationOutData;

import java.util.List;

public interface ProductLocationService {
    List<GetProductLocationOutData> selectLocationList(GetProductLocationInData inData);

    void saveLocation(GetProductLocationInData inData);

    void importLocation(GetProductLocationInData inData);

    GetProductLocationOutData selectLocationDrop(GetProductLocationInData inData);
}
