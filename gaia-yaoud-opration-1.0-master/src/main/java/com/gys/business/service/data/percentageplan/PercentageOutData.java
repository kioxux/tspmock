package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PercentageOutData {
    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码组")
    private String[] storeCodes;
    @ApiModelProperty(value = "方案名称")
    private String planName;
    @ApiModelProperty(value = "适用月份")
    private String planSuitMonth;
    @ApiModelProperty(value = "提成方式: 0 按销售额提成 1 按毛利额提成")
    private String planAmtWay;
    @ApiModelProperty(value = "提成方式:0 按商品毛利率  1 按销售毛利率")
    private String planRateWay;
    @ApiModelProperty(value = "负毛利率商品是否不参与销售提成 0 是 1 否")
    private String planIfNegative;
    @ApiModelProperty(value = "单品提成方式 0 不参与销售提成 1 参与销售提成")
    private String planProductWay;
    @ApiModelProperty(value = "创建人编码")
    private String planCreaterId;
    @ApiModelProperty(value = "创建人")
    private String planCreater;
    @ApiModelProperty(value = "创建日期")
    private String planCreateTime;
    @ApiModelProperty(value = "适用店数")
    private String stoCount;
    @ApiModelProperty(value = "方案状态 0 已保存 1 已审核 2 已停用")
    private String planStatus;
    @ApiModelProperty(value = "方案状态名")
    private String planStatusName;
    @ApiModelProperty(value = "起始日期")
    private String planStartDate;
    @ApiModelProperty(value = "结束日期")
    private String planEndDate;
    @ApiModelProperty(value = "提成类型 1 销售 2 单品")
    private String planType;
    @ApiModelProperty(value = "门店分配比例（默认0）")
    private String planScaleSto;
    @ApiModelProperty(value = "员工分配比例（默认100）")
    private String planScaleSaler;
    @ApiModelProperty(value = "停用时间")
    private String planStopDate;
    @ApiModelProperty(value = "变更原因")
    private String planReason;


    @ApiModelProperty(value = "剔除折扣率 操作符号")
    private String planRejectDiscountRateSymbol;

    @ApiModelProperty(value = "剔除折扣率 百分比值")
    private String planRejectDiscountRate;
}
