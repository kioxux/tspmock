package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class GetSaleReturnOutData implements Serializable {
    private static final long serialVersionUID = -1861270844179251105L;
    private String clientId;
    private String gsshBillNo;
    private String gsshBrId;
    private String gsshDate;
    private String gsshTime;
    private String gsshEmp;
    private String gsshTaxNo;
    private String gsshHykNo;
    private String gsshHykName;
    private BigDecimal gsshZkAmt;
    private BigDecimal gsshYsAmt;
    private BigDecimal gsshRmbZlAmt;
    private BigDecimal gsshRmbAmt;
    private String gsshDyqNo;
    private BigDecimal gsshDyqAmt;
    private String gsshDyqType;
    private String gsshRechargeCardNo;
    private BigDecimal gsshRechargeCardAmt;
    private String gsshDzqczActno1;
    private BigDecimal gsshDzqczAmt1;
    private String gsshDzqdyActno1;
    private BigDecimal gsshDzqdyAmt1;
    private String gsshIntegralAdd;
    private String gsshIntegralExchange;
    private BigDecimal gsshIntegralExchangeAmt;
    private String gsshIntegralCash;
    private BigDecimal gsshIntegralCashAmt;
    private String gsshPaymentNo1;
    private BigDecimal gsshPaymentAmt1;
    private String gsshBillNoReturn;
    private String gsshEmpReturn;
    private String gsshPromotionType1;
    private String gsshRegisterVoucherId;
    private String gsshReplaceBrId;
    private String gsshReplaceSalerId;
    private String gsshHideFlag;
    private String gsshCallQty;
    private String gssdMovPrice;
    private String gssdMovTax;
    private String gssdTaxRate;

    private List<GetSaleReturnDetailOutData> saleReturnDetailOutDataList;

    private BigDecimal giftAmt;

    private String gsshBillType;

    private String gsshEmpGroupName;

    //为区分接口调用来源 eg: "外卖"
    private String sourceType;
}
