package com.gys.business.service.data.DailySettlement;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DailyReconcileDetailVO implements Serializable {

    private static final long serialVersionUID = 3693080798161993737L;
    /**
     * 参数集合
     */
    private List<DailyReconcileDetailOutData> detail;

    /**
     * 应收汇总金额
     */
    private String saleYsAmt;

    /**
     * 储值卡汇总金额
     */
    private String cardYsAmt;

    /**
     * 订购金额
     */
    private String orderAmt;

    /**
     * 对账汇总金额
     */
    private String amountReconciliation;


    /**
     * 销售日期
     */
    private String saleDate;

}
