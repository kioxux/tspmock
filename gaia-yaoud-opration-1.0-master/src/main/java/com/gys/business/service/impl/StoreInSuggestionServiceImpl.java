package com.gys.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaStoreInSuggestionDMapper;
import com.gys.business.mapper.GaiaStoreInSuggestionHMapper;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionD;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionH;
import com.gys.business.service.GaiaSdMessageService;
import com.gys.business.service.StoreInSuggestionService;
import com.gys.common.data.PageInfo;
import com.gys.common.data.suggestion.InDetailSaveForm;
import com.gys.common.data.suggestion.SuggestionDetailVO;
import com.gys.common.data.suggestion.SuggestionInForm;
import com.gys.common.data.suggestion.SuggestionInVO;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.enums.SuggestionInStatusEnum;
import com.gys.common.exception.BusinessException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/1 16:31
 */
@Service("storeInSuggestionService")
public class StoreInSuggestionServiceImpl implements StoreInSuggestionService {
    @Resource
    private GaiaStoreInSuggestionHMapper gaiaStoreInSuggestionHMapper;
    @Resource
    private GaiaStoreInSuggestionDMapper gaiaStoreInSuggestionDMapper;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaSdMessageService gaiaSdMessageService;

    @Override
    public PageInfo<SuggestionInVO> list(SuggestionInForm suggestionInForm) {
        PageHelper.startPage(suggestionInForm.getPageNum(), suggestionInForm.getPageSize());
        List<SuggestionInVO> list = gaiaStoreInSuggestionHMapper.findList(suggestionInForm);
        return new PageInfo<>(list);
    }

    @Override
    public Map<String, Object> detail(SuggestionInForm suggestionInForm) {
        Map<String, Object> data = new HashMap<>();
        //详情信息
        List<SuggestionDetailVO> list = gaiaStoreInSuggestionDMapper.findVOList(suggestionInForm);
        if (list != null && list.size() > 0) {
            data.put("list", list);
            //汇总信息
            BigDecimal suggestionQty = BigDecimal.ZERO, confirmQty = BigDecimal.ZERO;
            BigDecimal averageSalesQty = BigDecimal.ZERO, inventoryQty = BigDecimal.ZERO;
            for (SuggestionDetailVO detailVO : list) {
                suggestionQty = suggestionQty.add(detailVO.getSuggestionOutQty());
                if (detailVO.getUpdateTime().startsWith("1970")) {
                    //默认确认量为调出量
                    detailVO.setConfirmQty(detailVO.getSuggestionOutQty());
                }
                confirmQty = confirmQty.add(detailVO.getConfirmQty());
                averageSalesQty = averageSalesQty.add(detailVO.getAverageSalesQty());
                inventoryQty = inventoryQty.add(detailVO.getInventoryQty());
                //处理精度
                detailVO.setAverageSalesQty(detailVO.getAverageSalesQty().setScale(2, RoundingMode.HALF_UP));
                detailVO.setConfirmQty(detailVO.getConfirmQty().setScale(2, RoundingMode.HALF_UP));
                detailVO.setInventoryQty(detailVO.getInventoryQty().setScale(2, RoundingMode.HALF_UP));
                detailVO.setSuggestionOutQty(detailVO.getSuggestionOutQty().setScale(2, RoundingMode.HALF_UP));
            }
            Map<String, BigDecimal> listNum = new HashMap<>();
            listNum.put("suggestionQty", suggestionQty.setScale(2, RoundingMode.HALF_UP));
            listNum.put("confirmQty", confirmQty.setScale(2, RoundingMode.HALF_UP));
            listNum.put("averageSalesQty", averageSalesQty.setScale(2, RoundingMode.HALF_UP));
            listNum.put("inventoryQty", inventoryQty.setScale(2, RoundingMode.HALF_UP));
            data.put("listNum", listNum);
        } else {
            data.put("list", new ArrayList<>());
            data.put("listNum", new ArrayList<>());
        }
        return data;
    }

    @Transactional
    @Override
    public void saveDetail(InDetailSaveForm detailSaveForm) {
        //验证状态
        GaiaStoreInSuggestionH cond = new GaiaStoreInSuggestionH();
        cond.setClient(detailSaveForm.getClient());
        cond.setBillCode(detailSaveForm.getBillCode());
        cond.setStoCode(detailSaveForm.getStoCode());
        GaiaStoreInSuggestionH unique = gaiaStoreInSuggestionHMapper.getUnique(cond);
        if (!SuggestionInStatusEnum.WAIT_CONFIRM.status.equals(unique.getStatus())) {
            throw new BusinessException("当前调剂建议单状态不可保存");
        }
        saveSuggestionInDetail(detailSaveForm, unique);
    }

    private void saveSuggestionInDetail(InDetailSaveForm detailSaveForm, GaiaStoreInSuggestionH unique) {
        GaiaStoreInSuggestionD suggestionDCond = new GaiaStoreInSuggestionD();
        suggestionDCond.setClient(unique.getClient());
        suggestionDCond.setBillCode(unique.getBillCode());
        suggestionDCond.setStoCode(unique.getStoCode());
        List<GaiaStoreInSuggestionD> detailList = gaiaStoreInSuggestionDMapper.findList(suggestionDCond);
        String updateUser = String.format("%s-%s", detailSaveForm.getUserName(), detailSaveForm.getUserId());
        for (InDetailSaveForm.InProductForm inProductForm : detailSaveForm.getProductList()) {
            List<GaiaStoreInSuggestionD> suggestionDList = detailList.stream().filter(entity -> entity.getProSelfCode().equals(inProductForm.getProSelfCode()) && entity.getBatchNo().equals(inProductForm.getBatchNo())).collect(Collectors.toList());
            if (suggestionDList.size() == 0) {
                continue;
            }
            GaiaStoreInSuggestionD storeInSuggestionD = suggestionDList.get(0);
            //检验确认调入数量是否大于调出建议量
            if (inProductForm.getConfirmQty().compareTo(storeInSuggestionD.getSuggestionOutQty()) > 0 || BigDecimal.ZERO.compareTo(inProductForm.getConfirmQty()) > 0) {
                throw new BusinessException(String.format("【%s】确认调出量必须介于0-%s之间", inProductForm.getProSelfCode(), storeInSuggestionD.getSuggestionOutQty()));
            }
            if (storeInSuggestionD.getConfirmQty().compareTo(inProductForm.getConfirmQty()) != 0) {
                storeInSuggestionD.setConfirmQty(inProductForm.getConfirmQty());
                storeInSuggestionD.setUpdateTime(new Date());
                storeInSuggestionD.setUpdateUser(updateUser);
                gaiaStoreInSuggestionDMapper.update(storeInSuggestionD);
            }
        }
    }

    @Transactional
    @Override
    public void confirm(InDetailSaveForm detailSaveForm) {
        //验证状态
        GaiaStoreInSuggestionH cond = new GaiaStoreInSuggestionH();
        cond.setClient(detailSaveForm.getClient());
        cond.setBillCode(detailSaveForm.getBillCode());
        cond.setStoCode(detailSaveForm.getStoCode());
        GaiaStoreInSuggestionH unique = gaiaStoreInSuggestionHMapper.getUnique(cond);
        if (SuggestionInStatusEnum.CONFIRMED.status.equals(unique.getStatus()) || SuggestionInStatusEnum.INVALID.status.equals(unique.getStatus())) {
            throw new BusinessException("当前调剂建议单状态不可审核");
        }
        //保存明细
        saveSuggestionInDetail(detailSaveForm, unique);
        //更改状态
        unique.setStatus(SuggestionInStatusEnum.CONFIRMED.status);
        unique.setUpdateTime(new Date());
        unique.setUpdateUser(String.format("%s-%s", detailSaveForm.getUserName(), detailSaveForm.getUserId()));
        gaiaStoreInSuggestionHMapper.update(unique);
        //发送消息
        threadPoolTaskExecutor.execute(() -> gaiaSdMessageService.sendStoreSuggestionMessage(SdMessageTypeEnum.STORE_IN_SUGGESTION_CONFIRM, null, unique));
    }
}
