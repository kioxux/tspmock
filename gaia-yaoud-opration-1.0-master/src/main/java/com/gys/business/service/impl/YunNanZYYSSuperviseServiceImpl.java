package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.gys.business.mapper.GaiaSdRemotePrescribeQdMapper;
import com.gys.business.mapper.GaiaSdSaleRecipelMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaSdRemotePrescribeQd;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.YunNanZYYSSuperviseService;
import com.gys.business.service.data.YunNanPrescriptionInData;
import com.gys.business.service.data.YunNanSalesDetailInData;
import com.gys.business.service.data.YunNanYjLoginInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import com.gys.util.DateUtil;
import com.gys.util.GuoYaoUtil;
import com.gys.util.JsonUtils;
import com.gys.util.YunnanSuperviseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/12 12:00
 */
@Service
@Slf4j
public class YunNanZYYSSuperviseServiceImpl implements YunNanZYYSSuperviseService {
    @Resource
    private GaiaUserDataMapper gaiaUserDataMapper;
    @Resource
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;
    @Resource
    private GaiaSdRemotePrescribeQdMapper gaiaSdRemotePrescribeQdMapper;
    @Override
    public JsonResult login(GetLoginOutData userInfo) {
        if(userInfo.getClient()!="10000308"){
            return JsonResult.fail(1001,"无授权");
        }
        YunNanYjLoginInData data=new YunNanYjLoginInData();
        //获取药师注册证编号
        List<GaiaUserData> userData = gaiaUserDataMapper.getAllByClientById(userInfo.getClient(), userInfo.getUserId());
        if (CollUtil.isNotEmpty(userData)) {
            if(StringUtils.isEmpty(userData.get(0).getUserYsId()) ){
                return JsonResult.fail(1001,"该药师未维护药师注册证号");
            }else{
                data.setRegistrationNo(userData.get(0).getUserYsId());
            }
        }else{
            return JsonResult.fail(1001,"未能找到登录用户");
        }
        data.setInstNo(YunnanSuperviseUtil.instNo);
        data.setCameraIp(YunnanSuperviseUtil.cameraIp);
        Result result =YunnanSuperviseUtil.login(JSON.toJSONString(data));
        if("200".equals(result.getCode())) {
            GaiaSdRemotePrescribeQd qd=new GaiaSdRemotePrescribeQd();
            qd.setClient(userInfo.getClient());
            qd.setType("1");
            qd.setDate(DateUtil.getCurrentDate());
            qd.setTime(DateUtil.getCurrentTimeStr("HHmmss"));
            qd.setName(userInfo.getLoginName());
            qd.setEmp(userInfo.getUserId());
            gaiaSdRemotePrescribeQdMapper.insert(qd);
            return JsonResult.success("签到成功");
        }else{
            return JsonResult.fail(500,result.getMsg());
        }
    }

    @Override
    public JsonResult logout(GetLoginOutData userInfo) {
        if(userInfo.getClient()!="10000308"){
            return JsonResult.fail(1001,"无授权");
        }
        YunNanYjLoginInData data=new YunNanYjLoginInData();
        //获取药师注册证编号
        List<GaiaUserData> userData = gaiaUserDataMapper.getAllByClientById(userInfo.getClient(), userInfo.getUserId());
        if (CollUtil.isNotEmpty(userData)) {
            if(StringUtils.isEmpty(userData.get(0).getUserYsId()) ){
                return JsonResult.fail(1001,"该药师未维护药师注册证号");
            }else{
                data.setRegistrationNo(userData.get(0).getUserYsId());
            }
        }else{
            return JsonResult.fail(1001,"未能找到登录用户");
        }
        data.setInstNo(YunnanSuperviseUtil.instNo);
        Result result =YunnanSuperviseUtil.logout(JSON.toJSONString(data));
        if("200".equals(result.getCode())) {
            GaiaSdRemotePrescribeQd qd=new GaiaSdRemotePrescribeQd();
            qd.setClient(userInfo.getClient());
            qd.setType("2");
            qd.setDate(DateUtil.getCurrentDate());
            qd.setTime(DateUtil.getCurrentTimeStr("HHmmss"));
            qd.setName(userInfo.getLoginName());
            qd.setEmp(userInfo.getUserId());
            gaiaSdRemotePrescribeQdMapper.insert(qd);
            return JsonResult.success("签退成功");
        }else{
            return JsonResult.fail(500,result.getMsg());
        }
    }

    @Override
    public JsonResult prescription(YunNanPrescriptionInData inData) {
        Result result = YunnanSuperviseUtil.prescription(JSON.toJSONString(inData));
        if("200".equals(result.getCode())) {
            return JsonResult.success("推送成功");
        }else{
            return JsonResult.fail(500,result.getMsg());
        }
    }

    @Override
    public JsonResult salesDetail() {
        //查询昨天的审核通过的处方信息
        List<YunNanSalesDetailInData> inDataList=gaiaSdSaleRecipelMapper.getYunNanSalesDetail();
        if(inDataList.isEmpty()){JsonResult.success("昨日无处方药销售"); }
        //推送监管系统
        int i=1;
        for (YunNanSalesDetailInData inData :inDataList){
            inData.setSeqNo(DateUtil.getCurrentDate()+YunnanSuperviseUtil.instNo+String.format("%05d",i));
            inData.setInstNo(YunnanSuperviseUtil.instNo);
            inData.setAreaNo(88604);
            Result result = YunnanSuperviseUtil.salesDetail(JSON.toJSONString(inData));
            if("200".equals(result.getCode())) {
                log.error("<推送处方销售明细错误>请求参数：{}",JSON.toJSONString(inData));
            }
            i++;
        }
        return JsonResult.success("推送成功");
    }
}

