package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
@ApiModel
public class RechargeChangeInData implements Serializable {

    private static final long serialVersionUID = -3251703709770417463L;

    /**
     * 加盟商
     */
    private String client;
    /**
     * 储值卡卡号
     */
    @ApiModelProperty(value = "储值卡卡号",example = "123213123")
    private String gsrcId;

    /**
     * 会员卡号
     */
    @ApiModelProperty(value = "会员卡号")
    private String gsrcMemberCardId;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String gsrcvalue;

    /**
     * 手机
     */
    @ApiModelProperty(value = "手机")
    private String gsrcMobile;


    /**
     * 开始日期
     */
    @NotBlank(message = "日期不能为空")
    @ApiModelProperty(value = "开始日期")
    private String startDate;

    /**
     * 结束日期
     */
    @NotBlank(message = "日期不能为空")
    @ApiModelProperty(value = "结束日期")
    private String endDate;

    /**
     * 异动门店
     */
    @ApiModelProperty(value = "门店")
    private String[] gsrcBrId;

    /**
     * 异动单号
     */
    @Id
    @ApiModelProperty(value = "单号")
    private String gsrcVoucherId;

    /**
     * 异动类型 1：充值 2：退款 3：消费 4：退货
     */
    @ApiModelProperty(value ="异动类型 1：充值 2：退款 3：消费 4：退货")
    private Integer gsrcType;

    @ApiModelProperty("起始页")
    private Integer pageNum;

    @ApiModelProperty("页面数量")
    private Integer pageSize;
}
