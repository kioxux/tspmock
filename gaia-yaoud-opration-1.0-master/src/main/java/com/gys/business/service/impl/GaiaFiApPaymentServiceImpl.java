package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.gys.business.mapper.entity.GaiaFiApBalance;
import com.gys.business.mapper.entity.GaiaFiApPayment;
import com.gys.business.mapper.GaiaFiApPaymentMapper;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.service.IGaiaFiApPaymentService;
import com.gys.business.service.data.fiApPayment.GaiaFiApPaymentDto;
import com.gys.common.enums.FiApPaymentEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDateTime;
import java.util.*;


/**
 * <p>
 * 应付方式维护表 服务实现类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class GaiaFiApPaymentServiceImpl implements IGaiaFiApPaymentService {
    @Autowired
    private GaiaFiApPaymentMapper fiApPaymentMapper;

    private boolean checkRepeat(GetLoginOutData userInfo, GaiaFiApPaymentDto inData) {
        boolean res = false;
        if (StrUtil.isNotBlank(inData.getIfMod()) && inData.getIfMod().equals("0")) {
            //新增
            Example example = Example.builder(GaiaFiApPayment.class).build();
            Example.Criteria criteria = example.createCriteria();
            Example.Criteria criteria2 = example.createCriteria();
            criteria.andEqualTo("client", userInfo.getClient());
            criteria2.orEqualTo("paymentId", inData.getPaymentId())
                    .orEqualTo("paymentName", inData.getPaymentName());
            example.and(criteria2);
            int i = fiApPaymentMapper.selectCountByExample(example);
            if (i > 0) {
                return true;
            }
        } else if (StrUtil.isNotBlank(inData.getIfMod()) && inData.getIfMod().equals("1")) {
            //表示应付方式名称有所改动
            //新增
            Example example = Example.builder(GaiaFiApPayment.class).build();
            Example.Criteria criteria = example.createCriteria();
            Example.Criteria criteria2 = example.createCriteria();
            criteria.andEqualTo("client", userInfo.getClient());
            criteria2.orEqualTo("paymentName", inData.getPaymentName());
            example.and(criteria2);
            int i = fiApPaymentMapper.selectCountByExample(example);
            if (i > 0) {
                return true;
            }
        }

        return res;
    }

    @Override
    public Object save(GetLoginOutData userInfo, GaiaFiApPaymentDto inData) {
        //数据验证
        if (StringUtils.isNotEmpty(FiApPaymentEnum.getValueByCode(inData.getPaymentId()))) {
            throw new BusinessException("无法修改系统默认应付方式");
        }

        if (checkRepeat(userInfo, inData)) {
            throw new BusinessException("存在重复的应付方式编码或者应付方式名称");
        }

        Example example = Example.builder(GaiaFiApPayment.class).build();
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("client", userInfo.getClient());
        criteria.andEqualTo("paymentId", inData.getPaymentId());
        example.setOrderByClause("CLIENT limit 1");
        GaiaFiApPayment gaiaFiApPayment = fiApPaymentMapper.selectOneByExample(example);


        GaiaFiApPayment entity = new GaiaFiApPayment()
                .setClient(userInfo.getClient())
                .setPaymentId(inData.getPaymentId())
                .setPaymentName(inData.getPaymentName())
                .setPaymentCreId(userInfo.getUserId())
                .setPaymentCreDate(new Date());

        //新增
        if (Objects.isNull(gaiaFiApPayment)) {
            fiApPaymentMapper.insert(entity);
        }
        //修改
        else {
            Example fiApExample = new Example(GaiaFiApPayment.class);
            fiApExample.createCriteria()
                    .andEqualTo("client", userInfo.getClient())
                    .andEqualTo("paymentId", inData.getPaymentId());

            fiApPaymentMapper.updateByExampleSelective(entity, fiApExample);
        }

        return null;
    }


    //============================  新增  ==========================


    //============================  获取列表（分页）  ==========================

    @Override
    public PageInfo<GaiaFiApPayment> getListPage(GetLoginOutData userInfo) {
        List<GaiaFiApPayment> resList = new ArrayList<>();
        Example example = Example.builder(GaiaFiApPayment.class).build();
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("client", userInfo.getClient());
        List<GaiaFiApPayment> gaiaFiApPaymentList = fiApPaymentMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(gaiaFiApPaymentList)) {
            resList.addAll(gaiaFiApPaymentList);
        }
        GaiaFiApPayment fourLiu = new GaiaFiApPayment();
        fourLiu.setClient(userInfo.getClient());
        fourLiu.setPaymentId("6666");
        fourLiu.setPaymentName("期间付款");
        resList.add(fourLiu);

        GaiaFiApPayment fourBa = new GaiaFiApPayment();
        fourBa.setClient(userInfo.getClient());
        fourBa.setPaymentId("8888");
        fourBa.setPaymentName("期初金额");
        resList.add(fourBa);

        GaiaFiApPayment fourJiu = new GaiaFiApPayment();
        fourJiu.setClient(userInfo.getClient());
        fourJiu.setPaymentId("9999");
        fourJiu.setPaymentName("期间发生额");
        resList.add(fourJiu);

        return new PageInfo<>(resList);
    }


    //============================  获取列表（分页）  ==========================
}
