package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 17:29 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class SelectRegionalInData implements Serializable {
    /**
     * 是否展示门店数 0：不展示、1：展示
     */
    @NotBlank(message = "type不能为空！")
    @Pattern(regexp = "[0,1]", message = "type只能为1或0")
    private String type;
}
