package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaGlobalDataMapper;
import com.gys.business.service.GlobalDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:40 2021/8/25
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Service
@Slf4j
public class GlobalDataServiceImpl implements GlobalDataService {

    @Autowired
    private GaiaGlobalDataMapper gaiaGlobalDataMapper;

    @Override
    public Boolean exportGlobalType(String client, String stoCode, String globalId) {
        String globalType = gaiaGlobalDataMapper.globalType(client, stoCode, globalId);
        if (StringUtils.isNotEmpty(globalType) && "1".equals(globalType)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
