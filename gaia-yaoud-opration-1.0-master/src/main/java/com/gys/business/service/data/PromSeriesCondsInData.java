package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class PromSeriesCondsInData implements Serializable {
    private static final long serialVersionUID = -5029602354206818516L;
    private String clientId;
    private String gspscVoucherId;
    private String gspscSerial;
    private String gspscProId;
    private String gspscSeriesId;
    private String gspscMemFlag;
    private String gspscInteFlag;
    private String gspscInteRate;
}
