package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductMidTypeSaleInData implements Serializable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value="加盟商")
    private String clientId;

    /**
     * 查询类型 1-公司 2-仓库 3-门店
     */
    @ApiModelProperty(value = "查询类型 1-公司 2-仓库 3-门店")
    private String type;

    /**
     * 分类编码  分类编码 1-药品 2-中药 3-非药品
     */
    @ApiModelProperty(value="分类编码 1-药品 2-中药 3-非药品")
    private String bigCode;
}
