package com.gys.business.service.data.Menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("菜单导航")
public class ProcessDiagramInData {

    @ApiModelProperty(value = "id")
    private String sId;
}
