package com.gys.business.service;

import com.gys.business.service.data.Menu.MenuInData;
import com.gys.business.service.data.Menu.ProcessDiagramOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface GaiaProcessDiagramService {
    List<ProcessDiagramOutData> getProcessDiagram(GetLoginOutData outData, String type);

    boolean checkUserAuth(GetLoginOutData userInfo,String menuId);

}
