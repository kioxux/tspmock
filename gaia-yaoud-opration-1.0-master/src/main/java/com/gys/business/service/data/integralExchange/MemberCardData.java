package com.gys.business.service.data.integralExchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MemberCardData {
    private static final long serialVersionUID = 1790627152270932691L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 会员ID
     */
    private String gsmbcMemberId;

    /**
     * 会员卡号
     */
    private String gsmbcCardId;

    /**
     * 所属店号
     */
    private String gsmbcBrId;

    /**
     * 渠道
     */
    private String gsmbcChannel;

    /**
     * 卡类型
     */
    private String gsmbcClassId;

    /**
     * 当前积分
     */
    private String gsmbcIntegral;

    /**
     * 最后积分日期
     */
    private String gsmbcIntegralLastdate;

    /**
     * 清零积分日期
     */
    private String gsmbcZeroDate;

    /**
     * 新卡创建日期
     */
    private String gsmbcCreateDate;

    /**
     * 类型
     */
    private String gsmbcType;

    /**
     * openID
     */
    private String gsmbcOpenId;

    /**
     * 卡状态
     */
    private String gsmbcStatus;

    /**
     * 开卡人员
     */
    private String gsmbcCreateSaler;

    /**
     * 开卡门店
     */
    private String gsmbcOpenCard;

    /**
     * 所属组织，单体：店号，连锁：连锁编码
     */
    private String gsmbcOrgId;


    /**
     * 累计积分
     */
    private String gsmbcTotalIntegral;

    /**
     * 累计金额
     */
    private BigDecimal gsmbcTotalAmt;

    /**
     * 当年累计积分
     */
    private String gsmbcYearIntegral;

    /**
     * 当年累计金额
     */
    private BigDecimal gsmbcYearAmt;

    /**
     * 所属组织类型，单体：1，连锁：2
     */
    private String gsmbcOrgType;

    /**
     * 会员组编码
     */
    private String gsmbcOrgGroup;

    /**
     * 0:维持现有模式不变，即免费注册后立即生效，1: 收费注册需线下激活转正，2:收费注册线上支付后激活
     */
    private String gsmbcState;

    /**
     * 激活人工号
     */
    private String gsmbcJhUser;

    /**
     * 激活日期
     */
    private String gsmbcJhDate;

    /**
     * 激活时间
     */
    private String gsmbcJhTime;

    private String memberName;
}
