package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MemeberJfValidateOutData {
    /**
     * 0：失败  1：成功
     */
    private String Code;
    private String Message;
    private BigDecimal integral;
    private BigDecimal integralAmt;
    private String integralAddSet;
}
