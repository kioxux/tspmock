package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.IMessageTemplateService;
import com.gys.business.service.MessageTemplateStoQuery;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.MessageBusinessTypeEnum;
import com.gys.common.enums.MessageGoPageEnum;
import com.gys.common.enums.MessageShowTypeEnum;
import com.gys.common.enums.MessageTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author flynn
 * @since 2021-09-07
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class MessageTemplateServiceImpl implements IMessageTemplateService {
    @Autowired
    private MessageTemplateMapper messageTemplateMapper;
    @Autowired
    private SystemParaMapper systemParaMapper;
    @Autowired
    private GaiaBasicsGroupInfoMapper gaiaBasicsGroupInfoMapper;
    @Autowired
    private MessageTempleClientPositionMapper messageTempleClientPositionMapper;
    @Autowired
    private MessageTempleStoPositionMapper messageTempleStoPositionMapper;
    @Autowired
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;


    //============================  新增初始化  ==========================

    private List<CommonVo> getModels() {
        List<CommonVo> res = new ArrayList<>();
        Example example = new Example(SystemPara.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", "MENU_MODEL");
        example.setOrderByClause(" CRE_DATE,CRE_TIME");
        List<SystemPara> systemParaList = systemParaMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(systemParaList)) {
            for (SystemPara para : systemParaList) {
                res.add(new CommonVo(para.getModel(), para.getName()));
            }
//            res = systemParaList.stream().collect(Collectors.toMap(SystemPara::getModel, SystemPara::getName));
        }
        return res;
    }

    private List<GaiaBasicsGroupInfo> getAllBasicsGroupInfoInUse() {
        List<GaiaBasicsGroupInfo> resList = new ArrayList<>();
        Example example = new Example(GaiaBasicsGroupInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("deleteFlag", "N");
        List<GaiaBasicsGroupInfo> gaiaBasicsGroupInfos = gaiaBasicsGroupInfoMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(gaiaBasicsGroupInfos)) {
            resList = gaiaBasicsGroupInfos.stream().filter(x -> StrUtil.isNotBlank(x.getModel()) && !x.getModel().equals("WMAPP")).collect(Collectors.toList());
        }
        return resList;
    }

    private List<CommonVo> getClients() {
        List<CommonVo> res = new ArrayList<>();
        List<GaiaFranchisee> gaiaFranchisees = gaiaFranchiseeMapper.selectAll();
        if (CollectionUtil.isNotEmpty(gaiaFranchisees)) {
            for (GaiaFranchisee client : gaiaFranchisees) {
                if ("0".equals(client.getFrancStatus())) {
                    CommonVo commonVo = new CommonVo(client.getClient(), client.getFrancName());
                    res.add(commonVo);
                }
            }
        }
        return res;
    }

    private String tranceWeekDayStr(Integer dayNum) {
        String res = "";
        switch (dayNum) {
            case 1:
                res = "一";
                break;
            case 2:
                res = "二";
                break;
            case 3:
                res = "三";
                break;
            case 4:
                res = "四";
                break;
            case 5:
                res = "五";
                break;
            case 6:
                res = "六";
                break;
            case 7:
                res = "日";
                break;
        }
        return res;
    }


    @Override
    public Map<String, Object> buildInit(GetLoginOutData userInfo) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        Map<String, Object> resultMap = new HashMap<>();
        List<CommonIntegerVo> gmtFlags = new ArrayList<>(Arrays.asList(new CommonIntegerVo(1, "启用"), new CommonIntegerVo(0, "停用"), new CommonIntegerVo(2, "已保存")));
        //业务逻辑
        resultMap.put("gmtFlags", gmtFlags);

        //初始化客户
        List<CommonVo> clients = getClients();
        resultMap.put("clients", clients);

        //初始化岗位
        List<ModulePositionVo> positions = new ArrayList<>();
        List<GaiaBasicsGroupInfo> positionInfoList = this.getAllBasicsGroupInfoInUse();
        //获取模块信息，准备构建层级树
        List<CommonVo> models = getModels();
        if (CollectionUtil.isNotEmpty(models)) {
            for (CommonVo modle : models) {
                if ("APP".equals(modle.getLable()) || "RP".equals(modle.getLable())) {
                    continue;
                }
                ModulePositionVo modulePositionVo = new ModulePositionVo();
                List<CommonVo> modlePositions = new ArrayList<>();
                modulePositionVo.setModuleCode(modle.getLable());
                modulePositionVo.setModuleName((String) modle.getValue());
                if (CollectionUtil.isNotEmpty(positionInfoList)) {
                    for (GaiaBasicsGroupInfo groupInfo : positionInfoList) {
                        if (modle.getLable().equals(groupInfo.getModel())) {
                            CommonVo positionCommonVo = new CommonVo(groupInfo.getCode(), groupInfo.getName());
                            modlePositions.add(positionCommonVo);
                        }
                    }
                }
                modulePositionVo.setPositions(modlePositions);
                positions.add(modulePositionVo);
            }
        }
        resultMap.put("positions", positions);


        //初始化日期
        List<CommonVo> gmtSendDates = new ArrayList<>(Arrays.asList(new CommonVo("1", "是"), new CommonVo("0", "否")));
        resultMap.put("gmtSendDates", gmtSendDates);

        List<CommonIntegerVo> monthDays = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            CommonIntegerVo day = new CommonIntegerVo(i, i + "日");
            monthDays.add(day);
        }
        resultMap.put("monthDays", monthDays);

        List<CommonVo> gmtSendWeeks = new ArrayList<>(Arrays.asList(new CommonVo("1", "是"), new CommonVo("0", "否")));
        resultMap.put("gmtSendWeeks", gmtSendWeeks);
        List<CommonIntegerVo> weekDays = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            CommonIntegerVo day = new CommonIntegerVo(i, tranceWeekDayStr(i));
            weekDays.add(day);
        }
        resultMap.put("weekDays", weekDays);


        List<CommonVo> gmtSendActivitys = new ArrayList<>(Arrays.asList(new CommonVo("1", "是"), new CommonVo("0", "否")));
        resultMap.put("gmtSendActivitys", gmtSendActivitys);
        List<CommonIntegerVo> activityDays = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            CommonIntegerVo day = new CommonIntegerVo(i, i + "日");
            activityDays.add(day);
        }
        resultMap.put("activityDays", activityDays);

        //初始化业务类型 1工作 2消息
        List<CommonIntegerVo> gmtBusinessTypes = new ArrayList<>();
        for (MessageBusinessTypeEnum value : MessageBusinessTypeEnum.values()) {
            gmtBusinessTypes.add(new CommonIntegerVo(value.getType(), value.getName()));
        }
        resultMap.put("gmtBusinessTypes", gmtBusinessTypes);

        // 是否跳转 0不跳转 1挑转
        List<CommonIntegerVo> gmtGoPages = new ArrayList<>();
        for (MessageGoPageEnum value : MessageGoPageEnum.values()) {
            gmtGoPages.add(new CommonIntegerVo(value.getType(), value.getName()));
        }
        resultMap.put("gmtGoPages", gmtGoPages);

        // 消息类型 1付费 2公司维护 3精准营销 4 培训 5经营月报
        List<CommonIntegerVo> gmtTypes = new ArrayList<>();
        for (MessageTypeEnum value : MessageTypeEnum.values()) {
            gmtTypes.add(new CommonIntegerVo(value.getType(), value.getName()));
        }
        resultMap.put("gmtTypes", gmtTypes);

        // 表示形式 1消息 2推送 3 消息+推送
        List<CommonIntegerVo> gmtShowTypes = new ArrayList<>();
        for (MessageShowTypeEnum value : MessageShowTypeEnum.values()) {
            gmtShowTypes.add(new CommonIntegerVo(value.getType(), value.getName()));
        }
        resultMap.put("gmtShowTypes", gmtShowTypes);

        //门店人员不显示毛利数据 门店人员是否显示毛利数据Y表示打勾 N表示未打勾
        List<CommonVo> gmtIfShowMaos = new ArrayList<>(Arrays.asList(new CommonVo("1", "是"), new CommonVo("0", "否")));
        resultMap.put("gmtIfShowMaos", gmtIfShowMaos);
        return resultMap;
    }

    //============================  新增初始化  ==========================

    private boolean checkLegal(MessageTemplateInData inData) {
        boolean res = true;
        if (StringUtils.isBlank(inData.getGmtName())) {
            throw new BusinessException("消息名称不能为空");
        }
        if (StrUtil.isNotBlank(inData.getGmtSendDate()) && "1".equals(inData.getGmtSendDate()) && ((StrUtil.isNotBlank(inData.getGmtSendWeek()) && "1".equals(inData.getGmtSendWeek())) || (StrUtil.isNotBlank(inData.getGmtSendActivity())) && "1".equals(inData.getGmtSendActivity()))) {
            throw new BusinessException("推送日期只能单选");
        } else if (StrUtil.isNotBlank(inData.getGmtSendWeek()) && "1".equals(inData.getGmtSendWeek()) && ((StrUtil.isNotBlank(inData.getGmtSendDate()) && "1".equals(inData.getGmtSendDate())) || (StrUtil.isNotBlank(inData.getGmtSendActivity())) && "1".equals(inData.getGmtSendActivity()))) {
            throw new BusinessException("推送日期只能单选");
        } else if (StrUtil.isNotBlank(inData.getGmtSendActivity()) && "1".equals(inData.getGmtSendActivity()) && ((StrUtil.isNotBlank(inData.getGmtSendDate()) && "1".equals(inData.getGmtSendDate())) || (StrUtil.isNotBlank(inData.getGmtSendWeek())) && "1".equals(inData.getGmtSendWeek()))) {
            throw new BusinessException("推送日期只能单选");
        }
        if (StrUtil.isNotBlank(inData.getGmtSendDate()) && "1".equals(inData.getGmtSendDate())) {
            if (!new ArrayList<String>(Arrays.asList("1", "0")).contains(inData.getGmtSendDate())) {
                throw new BusinessException("推送日期-按日期循环参数不合法");
            }
            if (inData.getGmtSendDateSettingNum() == null || (inData.getGmtSendDateSettingNum() != null && (inData.getGmtSendDateSettingNum() > 31 || inData.getGmtSendDateSettingNum() <= 0))) {
                throw new BusinessException("推送日期-按日期循环选定后必须选择1-31内的值");
            }
        }

        if (StrUtil.isNotBlank(inData.getGmtSendWeek()) && "1".equals(inData.getGmtSendWeek())) {
            if (!new ArrayList<String>(Arrays.asList("1", "0")).contains(inData.getGmtSendWeek())) {
                throw new BusinessException("推送日期-按星期循环参数不合法");
            }
            if (inData.getGmtSendWeekSettingNum() == null || (inData.getGmtSendWeekSettingNum() != null && (inData.getGmtSendWeekSettingNum() > 7 || inData.getGmtSendWeekSettingNum() < 1))) {
                throw new BusinessException("推送日期-按星期循环选定后必须选择一~七内的值");
            }
        }

        if (StrUtil.isNotBlank(inData.getGmtSendActivity()) && "1".equals(inData.getGmtSendActivity())) {
            if (!new ArrayList<String>(Arrays.asList("1", "0")).contains(inData.getGmtSendActivity())) {
                throw new BusinessException("推送日期-按活动设置参数不合法");
            }
            if (inData.getGmtSendActivitySettingNum() == null || (inData.getGmtSendActivitySettingNum() != null && (inData.getGmtSendActivitySettingNum() > 7 || inData.getGmtSendActivitySettingNum() < 1))) {
                throw new BusinessException("推送日期-按活动设置选定后必须选择1-7内的值");
            }
        }
        return res;
    }

    //============================  新增  ==========================

    @Override
    public Object build(GetLoginOutData userInfo, MessageTemplateInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        //数据验证
        if (checkLegal(inData)) {
            String uuid = UUID.randomUUID().toString();
            //封装入库实体
            MessageTemplate messageTemplate = new MessageTemplate();
            BeanUtils.copyProperties(inData, messageTemplate);
            messageTemplate.setGmtId(uuid);
            messageTemplate.setGmtFlag(2);//初始化设置为已保存状态
            messageTemplate.setCreId(userInfo.getUserId());
            messageTemplate.setCreName(userInfo.getLoginName());
            messageTemplate.setCreDate(CommonUtil.getyyyyMMdd());
            messageTemplate.setCreTime(CommonUtil.getHHmmss());
            messageTemplateMapper.insertSelective(messageTemplate);

            checkIsChooseAllClientOrPost(inData);

            List<MessageTempleClientPosition> insertRelations = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(inData.getClients())) {
                for (String client : inData.getClients()) {
                    for (String positionId : inData.getPositions()) {
                        String[] arr = positionId.split(StrUtil.DASHED);
                        MessageTempleClientPosition relation = new MessageTempleClientPosition();
                        relation.setGmtId(uuid);
                        relation.setClient(client);
                        relation.setPositionId(arr[1]);
                        relation.setModuleCode(arr[0]);
                        insertRelations.add(relation);
                    }
                }
            }
            messageTempleClientPositionMapper.insertList(insertRelations);
        }
        return null;
    }

    /**
     * 判断推送用户/岗位是否全选
     *
     * @param inData inData
     */
    private void checkIsChooseAllClientOrPost(MessageTemplateInData inData) {
        // 判断是选择所有加盟商还是指定加盟商
        if (Optional.ofNullable(inData.getChooseAllClient()).orElse(false)) {
            List<CommonVo> gaiaFranchisees = getClients();
            inData.setClients(gaiaFranchisees.stream().map(CommonVo::getLable).distinct().filter(Objects::nonNull).collect(Collectors.toList()));
        }

        // 判断是选择所有岗位还是指定岗位
        if (Optional.ofNullable(inData.getChooseAllPosition()).orElse(false)) {
            List<CommonVo> models = getModels();
            if (CollectionUtil.isNotEmpty(models)) {
                List<String> modelPositions = new ArrayList<>();
                List<GaiaBasicsGroupInfo> positionInfoList = this.getAllBasicsGroupInfoInUse();
                for (CommonVo modle : models) {
                    String label = modle.getLable();
                    if ("APP".equals(label) || "RP".equals(label)) {
                        continue;
                    }
                    if (CollectionUtil.isNotEmpty(positionInfoList)) {
                        for (GaiaBasicsGroupInfo groupInfo : positionInfoList) {
                            if (label.equals(groupInfo.getModel())) {
                                modelPositions.add(label + "-" + groupInfo.getCode());
                            }
                        }
                    }
                }
                if (!modelPositions.isEmpty()) {
                    inData.setPositions(modelPositions);
                }
            }
        }

        if (CollectionUtil.isEmpty(inData.getClients())) {
            throw new BusinessException("推送用户为必选");
        }
        if (CollectionUtil.isEmpty(inData.getPositions())) {
            throw new BusinessException("推送岗位为必选");
        }

        // 非空判断
//        if (inData.getGmtBusinessType() == null) {
//            throw new BusinessException("业务类型必填");
//        }

//        if (inData.getGmtShowType() == null) {
//            throw new BusinessException("表示形式必填");
//        }

        if (inData.getGmtType() == null) {
            throw new BusinessException("消息类型必填");
        }

//        if (inData.getGmtGoPage() == null) {
//            throw new BusinessException("跳转必填");
//        }

    }

    //============================  新增  ==========================


    //============================  修改初始化  ==========================
    @Override
    public Map<String, Object> updateInit(GetLoginOutData userInfo, String id) {

        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (id == null) {
            throw new BusinessException("请传入id");
        }
        Map<String, Object> res = this.buildInit(userInfo);
        //查询出db对象
        MessageTemplate MessageTemplateDb = messageTemplateMapper.selectByPrimaryKey(id);
        if (MessageTemplateDb == null) {
            throw new BusinessException("查询无此数据");
        }
        Example example = new Example(MessageTempleClientPosition.class);
        Example.Criteria criteria = example.createCriteria()
                .andEqualTo("gmtId", MessageTemplateDb.getGmtId());
        List<MessageTempleClientPosition> relationsDb = messageTempleClientPositionMapper.selectByExample(example);
        List<String> clients = new ArrayList<>();
        List<String> positions = new ArrayList<>();
        Map<String, List<MessageTempleClientPosition>> map = new HashMap<>();
        if (CollectionUtil.isNotEmpty(relationsDb)) {
            for (MessageTempleClientPosition realtion : relationsDb) {
                if (map.containsKey(realtion.getClient())) {
                    List<MessageTempleClientPosition> groupList = map.get(realtion.getClient());
                    groupList.add(realtion);
                    map.put(realtion.getClient(), groupList);
                } else {
                    List<MessageTempleClientPosition> groupList = new ArrayList<>();
                    groupList.add(realtion);
                    map.put(realtion.getClient(), groupList);
                }
            }
        }
        if (CollectionUtil.isNotEmpty(map)) {
            //因为选择的时候每个加盟商职位是一样的
            String randomClient = "";
            for (String client : map.keySet()) {
                clients.add(client);
                randomClient = client;
            }
            map.get(randomClient).forEach(x -> {
                positions.add(x.getPositionId());
            });
        }
        MessageTemplateDb.setClients(clients);
        MessageTemplateDb.setPositions(positions);
        res.put("data", MessageTemplateDb);
        //业务逻辑
        return res;
    }

    //============================  修改初始化  ==========================


    //============================  修改  ==========================
    @Override
    public Object update(GetLoginOutData userInfo, MessageTemplateInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (ObjectUtil.isEmpty(inData) || StrUtil.isBlank(inData.getGmtId())) {
            throw new BusinessException("请传入合法数据！");
        }
        //查询出db对象
        MessageTemplate MessageTemplateDb = messageTemplateMapper.selectByPrimaryKey(inData.getGmtId());
        if (MessageTemplateDb == null) {
            throw new BusinessException("查询无此数据");
        }

        //封装入库实体
        BeanUtils.copyProperties(inData, MessageTemplateDb);
        MessageTemplateDb.setModiId(userInfo.getUserId());
        MessageTemplateDb.setModiName(userInfo.getLoginName());
        MessageTemplateDb.setModiDate(CommonUtil.getyyyyMMdd());
        MessageTemplateDb.setModiTime(CommonUtil.getHHmmss());
        if (MessageTemplateDb.getGmtFlag() != null && MessageTemplateDb.getGmtFlag() == 1) {
            MessageTemplateDb.setGmtFlag(2);//设置成已保存
        }
        messageTemplateMapper.updateByPrimaryKey(MessageTemplateDb);

        //先删除再更新
        MessageTempleClientPosition position = new MessageTempleClientPosition();
        position.setGmtId(MessageTemplateDb.getGmtId());
        messageTempleClientPositionMapper.delete(position);

        checkIsChooseAllClientOrPost(inData);

        if (CollectionUtil.isEmpty(inData.getClients())) {
            throw new BusinessException("推送用户必选");
        }
        if (CollectionUtil.isEmpty(inData.getPositions())) {
            throw new BusinessException("推送岗位必选");
        }
        List<MessageTempleClientPosition> insertRelations = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(inData.getClients())) {
            for (String client : inData.getClients()) {
                for (String positionId : inData.getPositions()) {
                    String[] arr = positionId.split("-");
                    MessageTempleClientPosition relation = new MessageTempleClientPosition();
                    relation.setGmtId(MessageTemplateDb.getGmtId());
                    relation.setClient(client);
                    relation.setPositionId(arr[1]);
                    relation.setModuleCode(arr[0]);
                    insertRelations.add(relation);
                }
            }
        }
        messageTempleClientPositionMapper.insertList(insertRelations);
        return null;
    }


    //============================  修改  ==========================


    //============================  删除  ==========================
    @Override
    public int delete(GetLoginOutData userInfo, String id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (id == null) {
            throw new BusinessException("请传入合法数据！");
        }
        MessageTemplate messageTemplateDb = messageTemplateMapper.selectByPrimaryKey(id);
        if (messageTemplateDb == null) {
            throw new BusinessException("查无此数据！");
        }
        if (messageTemplateDb.getGmtFlag() != null && messageTemplateDb.getGmtFlag() != 2) {
            throw new BusinessException("只有已保存状态下能删除");
        }

        // 消息类型为：促销报告，经营月报禁止删除
        Integer gmtType = messageTemplateDb.getGmtType();
        if (MessageTypeEnum.PROMOTION_REPORT.getType().equals(gmtType)) {
            throw new BusinessException("促销报告类型禁止删除");
        }

        if (MessageTypeEnum.BUSINESS_MONTHLY_REPORT.getType().equals(gmtType)) {
            throw new BusinessException("经营月报类型禁止删除");
        }

        MessageTempleClientPosition position = new MessageTempleClientPosition();
        position.setGmtId(messageTemplateDb.getGmtId());
        messageTempleClientPositionMapper.delete(position);
        return messageTemplateMapper.deleteByPrimaryKey(id);
    }

    //============================  删除  ==========================


    //============================  详情  ==========================
    @Override
    public Object getDetailById(GetLoginOutData userInfo, Integer id) {
        Object res = new Object();
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        //查询出db对象
        MessageTemplate MessageTemplateDb = messageTemplateMapper.selectByPrimaryKey(id);
        if (MessageTemplateDb == null) {
            throw new BusinessException("查询无此数据");
        }
        //业务逻辑处理最终返回值

        return res;
    }


    //============================  详情  ==========================


    //============================  获取列表（分页）  ==========================

    @Override
    public PageInfo getListPage(GetLoginOutData userInfo, MessageTemplateInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        List<MessageTemplateRes> resList = new ArrayList<>();

        if (inData.getPageSize() == null) {
            inData.setPageSize(100);
        }
        if (inData.getPageNum() == null) {
            inData.setPageNum(1);
        }
        PageInfo pageInfo;

        List<GaiaBasicsGroupInfo> allPositions = getAllBasicsGroupInfoInUse();
        Map<String, GaiaBasicsGroupInfo> positionMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(allPositions)) {
            allPositions.forEach(x -> {
                positionMap.put(x.getModel() + "-" + x.getCode(), x);
            });
        }
        List<CommonVo> clients = getClients();
        Map<String, String> clientMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(clients)) {
            clients.forEach(x -> {
                clientMap.put(x.getLable(), x.getValue().toString());
            });
        }

        //实际执行的sql，执行定制任务
        Page<Object> objects = PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        Example example = new Example(MessageTemplate.class);
        Example.Criteria criteria = example.createCriteria();
        if (StrUtil.isNotBlank(inData.getGmtName())) {
            criteria.andLike("gmtName", "%" + inData.getGmtName() + "%");
        }
        if (StrUtil.isNotBlank(inData.getStartTime())) {
            criteria.andLessThanOrEqualTo("effectDate", inData.getEndTime());
        }
        if (StrUtil.isNotBlank(inData.getEndTime())) {
            criteria.andGreaterThanOrEqualTo("effectDate", inData.getStartTime());
        }
        if (inData.getGmtFlag() != null) {
            criteria.andEqualTo("gmtFlag", inData.getGmtFlag());
        }
        example.setOrderByClause(" CRE_DATE DESC,CRE_TIME DESC");
        List<MessageTemplate> messageTemplates = messageTemplateMapper.selectByExample(example);

        if (CollectionUtil.isNotEmpty(messageTemplates)) {
            List<String> gmtIds = new ArrayList<>();
            messageTemplates.forEach(x -> {
                gmtIds.add(x.getGmtId());
            });
            Example clientPositionExample = new Example(MessageTempleClientPosition.class);
            Example.Criteria clientPositionCriteria = clientPositionExample.createCriteria();
            clientPositionCriteria.andIn("gmtId", gmtIds);
            List<MessageTempleClientPosition> messageTempleClientPositions = messageTempleClientPositionMapper.selectByExample(clientPositionExample);
            Map<String, List<MessageTempleClientPosition>> messageTempleClientPositionsMap = new HashMap<>();
            if (CollectionUtil.isNotEmpty(messageTempleClientPositions)) {
                for (MessageTempleClientPosition clientPosition : messageTempleClientPositions) {
                    if (messageTempleClientPositionsMap.get(clientPosition.getGmtId()) != null) {
                        List<MessageTempleClientPosition> positionList = messageTempleClientPositionsMap.get(clientPosition.getGmtId());
                        positionList.add(clientPosition);
                        messageTempleClientPositionsMap.put(clientPosition.getGmtId(), positionList);
                    } else {
                        List<MessageTempleClientPosition> positionList = new ArrayList<>();
                        positionList.add(clientPosition);
                        messageTempleClientPositionsMap.put(clientPosition.getGmtId(), positionList);
                    }
                }
            }
            List<CommonVo> models = getModels();
            Map<String, Object> modelMap = new HashMap<>();
            if (CollectionUtil.isNotEmpty(models)) {
                modelMap = models.stream().collect(Collectors.toMap(CommonVo::getLable, CommonVo::getValue));
            }
            for (MessageTemplate messageTemplate : messageTemplates) {
                MessageTemplateRes res = new MessageTemplateRes();
                BeanUtils.copyProperties(messageTemplate, res);

                // 消息类型，跳转，表现形式，消息业务类型，无效值置空
                res.setGmtBusinessType(MessageBusinessTypeEnum.matchType(res.getGmtBusinessType()));
                res.setGmtGoPage(MessageGoPageEnum.matchType(res.getGmtGoPage()));
                res.setGmtShowType(MessageShowTypeEnum.matchType(res.getGmtShowType()));
                res.setGmtType(MessageTypeEnum.matchType(res.getGmtType()));

                List<MessageTempleClientPosition> positionList = messageTempleClientPositionsMap.get(messageTemplate.getGmtId());
                if (CollectionUtil.isNotEmpty(positionList)) {
                    List<CommonVo> clientList = new ArrayList<>();
                    List<String> clientIds = new ArrayList<>();
                    List<CommonVo> positions = new ArrayList<>();
                    List<String> positionIds = new ArrayList<>();
                    Map<String, Object> finalModelMap = modelMap;
                    positionList.forEach(x -> {
                        if (!clientIds.contains(x.getClient())) {
                            clientIds.add(x.getClient());
                            clientList.add(new CommonVo(x.getClient(), clientMap.get(x.getClient())));
                        }
                        String positionId = x.getModuleCode() + "-" + x.getPositionId();
                        if (!positionIds.contains(positionId)) {
                            positionIds.add(positionId);
                            CommonExtendsVo commonExtendsVo = new CommonExtendsVo(x.getPositionId(),
                                    positionMap.get(positionId) == null ? "" : positionMap.get(positionId).getName(),
                                    positionMap.get(positionId) == null ? "" : (finalModelMap.get(positionMap.get(positionId).getModel()) == null ? "" : finalModelMap.get(positionMap.get(positionId).getModel()).toString()));
                            commonExtendsVo.setGroupCode(positionMap.get(positionId) == null ? "" : (finalModelMap.get(positionMap.get(positionId).getModel()) == null ? "" : positionMap.get(positionId).getModel()));
                            positions.add(commonExtendsVo);
                        }
                    });

                    // allPositions 所有的 ， positionIds 模板分配的
                    res.setChooseAllPosition(allPositions.size() == positionIds.size());

                    // clients 所有的 ， clientIds 模板分配的
                    res.setChooseAllClient(clients.size() == clientIds.size());

                    res.setClients(clientList);
                    res.setPositions(positions);
                }
                resList.add(res);
            }
        }
        if (ObjectUtil.isNotEmpty(resList)) {
            //处理返回时可能要处理的转换工作
            pageInfo = new PageInfo(resList);
            pageInfo.setTotal(objects.getTotal());
            pageInfo.setPages(objects.getPages());
            pageInfo.setPageNum(objects.getPageNum());
            pageInfo.setPageSize(objects.getPageSize());
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public Object changeStatus(GetLoginOutData userInfo, MessageTemplateInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (ObjectUtil.isEmpty(inData)) {
            throw new BusinessException("非法参数");
        }
        if (inData.getGmtId() == null) {
            throw new BusinessException("非法参数,请传入id");
        }
//        if (inData.getGmtFlag() == null) {
//            throw new BusinessException("非法参数,请传入是否启用参数");
//        }
        if (StrUtil.isBlank(inData.getEffectMode())) {
            throw new BusinessException("非法参数,请传入生效模式参数");
        }
        //启用或者停用 1立即启用 0立即停用 2定时启用 3 定时停用
        if (("2".equals(inData.getEffectMode()) || "3".equals(inData.getEffectMode())) && (StrUtil.isBlank(inData.getEffectDate()) || StrUtil.isBlank(inData.getEffectTime()))) {
            throw new BusinessException("非法参数,请传入启用或者停用时间");
        }
        if (("1".equals(inData.getEffectMode()) || "0".equals(inData.getEffectMode())) && (StrUtil.isNotBlank(inData.getEffectDate()) || StrUtil.isNotBlank(inData.getEffectTime()))) {
            throw new BusinessException("非法参数,立即启用启用或者立即停用时，启用或者停用时间无需填写");
        }

        MessageTemplate messageTemplateDb = messageTemplateMapper.selectByPrimaryKey(inData.getGmtId());
        if (messageTemplateDb == null) {
            throw new BusinessException("查无此数据！");
        }
        //是否启用 0停用 1启用 2已保存
        Integer gmtFlag = messageTemplateDb.getGmtFlag();
        if (0 == gmtFlag && (inData.getEffectMode().equals("0") || inData.getEffectMode().equals("3"))) {
            throw new BusinessException("该数据已处于停用状态！");
        }

        if (1 == gmtFlag && (inData.getEffectMode().equals("1") || inData.getEffectMode().equals("2"))) {
            throw new BusinessException("该数据已处于启用状态！");
        }

        if (inData.getEffectMode().equals("0") || inData.getEffectMode().equals("1")) {
            messageTemplateDb.setGmtFlag(Integer.parseInt(inData.getEffectMode()));
            messageTemplateDb.setEffectMode(inData.getEffectMode());
            messageTemplateDb.setEffectDate(CommonUtil.getyyyyMMdd());
            messageTemplateDb.setEffectTime(CommonUtil.getHHmmss());
        } else {
            messageTemplateDb.setEffectMode(inData.getEffectMode());
            messageTemplateDb.setEffectDate(inData.getEffectDate());
            messageTemplateDb.setEffectTime(inData.getEffectTime());
        }
        messageTemplateDb.setModiId(userInfo.getUserId());
        messageTemplateDb.setModiName(userInfo.getLoginName());
        messageTemplateDb.setModiDate(CommonUtil.getyyyyMMdd());
        messageTemplateDb.setModiTime(CommonUtil.getHHmmss());
        messageTemplateMapper.updateByPrimaryKey(messageTemplateDb);

        return null;
    }

    @Override
    public List<MessageTemplateRes> storeListPage(GetLoginOutData userInfo, MessageTemplateInData inData) {
        List<MessageTemplateRes> resList = new ArrayList<>();
        Example example = new Example(MessageTemplate.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("gmtFlag", 1);//获取启用消息
        if (StrUtil.isNotBlank(inData.getGmtName())) {
            criteria.andLike("gmtName", "%" + inData.getGmtName() + "%");
        }
        example.setOrderByClause(" CRE_DATE DESC,CRE_TIME DESC");
        List<MessageTemplate> messageTemplates = messageTemplateMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(messageTemplates)) {
            messageTemplates.forEach(x -> {
                MessageTemplateRes res = new MessageTemplateRes();
                BeanUtils.copyProperties(x, res);
                resList.add(res);
            });
        }
        return resList;
    }

    @Override
    public Map<String, Object> chooseUser(GetLoginOutData userInfo, String id) {
        Map<String, Object> resMap = new HashMap<>();
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (StrUtil.isBlank(id)) {
            throw new BusinessException("请传入id");
        }
        MessageTemplate template = messageTemplateMapper.selectByPrimaryKey(id);
        if (template == null) {
            throw new BusinessException("查无此数据");
        }
        resMap.put("gmtIfShowMao", template.getGmtIfShowMao());
        List<MessageTemplateChooseUserRes> resList = new ArrayList<>();

        //查询所有的店铺级别的私人设置
        List<MessageTemplateChooseUserDb> allStoSettingDbs = getAllStoSetting(userInfo.getClient(), id);
        if (CollectionUtil.isNotEmpty(allStoSettingDbs)) {
            //表示用户已经设置过，拉取用户的配置
            Map<String, List<CommonVo>> userSitesMap = new HashMap<>();
            for (MessageTemplateChooseUserDb db : allStoSettingDbs) {
                String key = db.getClient() + "_" + db.getPositionId() + "_" + db.getUserId();
                if (userSitesMap.containsKey(key)) {
                    List<CommonVo> sites = userSitesMap.get(key);
                    if (CollectionUtil.isNotEmpty(sites)) {
                        boolean addFlag = true;
                        for (CommonVo commonVo : sites) {
                            //只要存在一个相同的就不能加入
                            if (commonVo.getLable().equals(db.getSite())) {
                                addFlag = false;
                                break;
                            }
                        }
                        if (addFlag) {
                            List<CommonVo> siteList = userSitesMap.get(key);
                            if (StrUtil.isNotBlank(db.getSite())) {
                                CommonVo site = new CommonVo(db.getSite(), db.getStoName() == null ? "" : db.getStoName());
                                siteList.add(site);
                            }
                            userSitesMap.put(key, siteList);
                        }
                    } else {
                        List<CommonVo> siteList = new ArrayList<>();
                        if (StrUtil.isNotBlank(db.getSite())) {
                            CommonVo site = new CommonVo(db.getSite(), db.getStoName() == null ? "" : db.getStoName());
                            siteList.add(site);
                        }
                        userSitesMap.put(key, siteList);
                    }

                } else {
                    List<CommonVo> siteList = new ArrayList<>();
                    if (StrUtil.isNotBlank(db.getSite())) {
                        CommonVo site = new CommonVo(db.getSite(), db.getStoName() == null ? "" : db.getStoName());
                        siteList.add(site);
                    }
                    userSitesMap.put(key, siteList);
                }
            }

            for (MessageTemplateChooseUserDb db : allStoSettingDbs) {
                MessageTemplateChooseUserRes res = new MessageTemplateChooseUserRes();
//                res.setDeleteFlag(true);//已经设置过配置的，拥有删除权限
                String key = db.getClient() + "_" + db.getPositionId() + "_" + db.getUserId();
                BeanUtils.copyProperties(db, res);
                if (userSitesMap.get(key) != null) {
                    res.setSites(userSitesMap.get(key));
                } else {
                    res.setSites(new ArrayList<>());
                }
                resList.add(res);
            }
            resMap.put("gmtIfShowMao", StrUtil.isBlank(resList.get(0).getGmtIfShowMao()) ? template.getGmtIfShowMao() : resList.get(0).getGmtIfShowMao());
            //对resList进行用户级别去重
            if (CollectionUtil.isNotEmpty(resList)) {
                Map<String, MessageTemplateChooseUserRes> removeRepeatMap = new HashMap<>();
                for (MessageTemplateChooseUserRes res : resList) {
                    removeRepeatMap.put(res.getUserId() + "-" + res.getPositionId(), res);
                }
                if (CollectionUtil.isNotEmpty(removeRepeatMap)) {
                    resList.clear();
                    for (String userId : removeRepeatMap.keySet()) {
                        resList.add(removeRepeatMap.get(userId));
                    }
                }
            }
            resMap.put("dataList", resList);
        } else {
            //用户没有设置过，拉取平台级别设置的角色对应的所有人员
            List<MessageTempleClientPosition> allRelationSetting = getAllClientSetting(userInfo.getClient(), template.getGmtId());
            if (CollectionUtil.isNotEmpty(allRelationSetting)) {
                Set<String> positionIdSet = allRelationSetting.stream().map(MessageTempleClientPosition::getPositionId).collect(Collectors.toSet());
                List<String> positionIds = new ArrayList<>(positionIdSet);
                Map<String, String> filterMap = new HashMap<>();
                for (MessageTempleClientPosition clientPosition : allRelationSetting) {
                    String key = clientPosition.getModuleCode() + "-" + clientPosition.getPositionId();
                    if (!filterMap.containsKey(key)) {
                        filterMap.put(key, key);
                    }
                }
                //拉取所有涉及到的店铺人员
                MessageTemplateChooseUserReq req = new MessageTemplateChooseUserReq();
                req.setClient(userInfo.getClient());
//                req.setSite(userInfo.getDepId());
                req.setPositionIds(positionIds);
                List<MessageTemplateChooseUserDb> messageTemplateChooseUserDbs = messageTempleClientPositionMapper.selectStoSettingInfo(req);
                //通过模块过滤掉不需要的职位
                if (CollectionUtil.isNotEmpty(messageTemplateChooseUserDbs)) {
                    List<MessageTemplateChooseUserDb> filterList = new ArrayList<>();
                    for (MessageTemplateChooseUserDb db : messageTemplateChooseUserDbs) {
                        if (filterMap.containsKey(db.getModel() + "-" + db.getPositionId())) {
                            filterList.add(db);
                        }
                    }
                    messageTemplateChooseUserDbs = filterList;
                }

                if (CollectionUtil.isNotEmpty(messageTemplateChooseUserDbs)) {
                    Map<String, List<CommonVo>> userSitesMap = new HashMap<>();
                    for (MessageTemplateChooseUserDb db : messageTemplateChooseUserDbs) {
                        String key = db.getClient() + "_" + db.getPositionId() + "_" + db.getUserId();
                        if (userSitesMap.containsKey(key)) {
                            List<CommonVo> sites = userSitesMap.get(key);
                            if (CollectionUtil.isNotEmpty(sites)) {
                                boolean addFlag = true;
                                for (CommonVo commonVo : sites) {
                                    //只要存在一个相同的就不能加入
                                    if (commonVo.getLable().equals(db.getSite())) {
                                        addFlag = false;
                                        break;
                                    }
                                }
                                if (addFlag) {
                                    List<CommonVo> siteList = userSitesMap.get(key);
                                    if (StrUtil.isNotBlank(db.getSite())) {
                                        CommonVo site = new CommonVo(db.getSite(), db.getStoName() == null ? "" : db.getStoName());
                                        siteList.add(site);
                                    }
                                    userSitesMap.put(key, siteList);
                                }
                            } else {
                                List<CommonVo> siteList = new ArrayList<>();
                                if (StrUtil.isNotBlank(db.getSite())) {
                                    CommonVo site = new CommonVo(db.getSite(), db.getStoName() == null ? "" : db.getStoName());
                                    siteList.add(site);
                                }
                                userSitesMap.put(key, siteList);
                            }

                        } else {
                            List<CommonVo> siteList = new ArrayList<>();
                            if (StrUtil.isNotBlank(db.getSite())) {
                                CommonVo site = new CommonVo(db.getSite(), db.getStoName() == null ? "" : db.getStoName());
                                siteList.add(site);
                            }
                            userSitesMap.put(key, siteList);
                        }
                    }

                    for (MessageTemplateChooseUserDb db : messageTemplateChooseUserDbs) {
                        MessageTemplateChooseUserRes res = new MessageTemplateChooseUserRes();
                        String key = db.getClient() + "_" + db.getPositionId() + "_" + db.getUserId();
                        BeanUtils.copyProperties(db, res);
                        if (userSitesMap.get(key) != null) {
                            res.setSites(userSitesMap.get(key));
                        } else {
                            res.setSites(new ArrayList<>());
                        }
                        resList.add(res);

                    }
                    //对resList进行用户级别去重
                    if (CollectionUtil.isNotEmpty(resList)) {
                        Map<String, MessageTemplateChooseUserRes> removeRepeatMap = new HashMap<>();
                        for (MessageTemplateChooseUserRes chooseUserRes : resList) {
                            removeRepeatMap.put(chooseUserRes.getUserId() + "_" + chooseUserRes.getPositionId(), chooseUserRes);
                        }
                        if (CollectionUtil.isNotEmpty(removeRepeatMap)) {
                            resList.clear();
                            for (String userId : removeRepeatMap.keySet()) {
                                resList.add(removeRepeatMap.get(userId));
                            }
                        }
                    }
                    resMap.put("dataList", resList);
                }
            }

        }
        return resMap;
    }

    @Override
    public Object storeChooseUserSave(GetLoginOutData userInfo, MessageTemplateChooseUserInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (ObjectUtil.isEmpty(inData)) {
            throw new BusinessException("请输入合法参数");
        }

        if (StrUtil.isBlank(inData.getGmtId())) {
            throw new BusinessException("请传入消息id");
        }
        if (StrUtil.isBlank(inData.getGmtIfShowMao())) {
            throw new BusinessException("门店人员不显示毛利率选项必输");
        }
        if (CollectionUtil.isNotEmpty(inData.getChooseItems())) {
            List<MessageTemplateChooseUserInData.Item> chooseItems = inData.getChooseItems();
            for (MessageTemplateChooseUserInData.Item item : chooseItems) {
                if (StrUtil.isBlank(item.getReceiveUserId())) {
                    throw new BusinessException("请传入人员");
                }
                if (StrUtil.isBlank(item.getPositionId())) {
                    throw new BusinessException("请传入职位id");
                }
                if (StrUtil.isBlank(item.getPositionName())) {
                    throw new BusinessException("请传入职位名称");
                }
                if (StrUtil.isBlank(item.getModel())) {
                    throw new BusinessException("请传入模块分类");
                }
            }
        }


        List<MessageTempleStoPosition> insertList = new ArrayList<>();
        //先删除，再保存
        MessageTempleStoPosition deleteEntity = new MessageTempleStoPosition();
        Example exampleDelete = new Example(MessageTempleStoPosition.class);
        Example.Criteria criteria = exampleDelete.createCriteria();
        criteria.andEqualTo("client", userInfo.getClient())
                .andEqualTo("gmtId", inData.getGmtId());
        messageTempleStoPositionMapper.deleteByExample(exampleDelete);
        List<MessageTemplateChooseUserInData.Item> chooseItems = inData.getChooseItems();

        if (CollectionUtil.isNotEmpty(chooseItems)) {
            for (MessageTemplateChooseUserInData.Item item : chooseItems) {
                List<CommonVo> sites = item.getSites();
                if (CollectionUtil.isNotEmpty(sites)) {
                    sites.forEach(x -> {
                        MessageTempleStoPosition db = new MessageTempleStoPosition();
                        BeanUtils.copyProperties(item, db);
                        db.setSiteName(x.getValue() == null ? "" : x.getValue().toString());
                        db.setSite(x.getLable() == null ? "" : x.getLable());
                        db.setReceiveUserId(item.getReceiveUserId());
                        db.setClient(userInfo.getClient());
                        db.setModuleCode(item.getModel());
                        db.setGmtId(inData.getGmtId());
                        db.setGmtIfShowMao(inData.getGmtIfShowMao());
                        insertList.add(db);
                    });
                } else {
                    MessageTempleStoPosition db = new MessageTempleStoPosition();
                    BeanUtils.copyProperties(item, db);
                    db.setSite("");
                    db.setSiteName("");
                    db.setModuleCode(item.getModel());
                    db.setReceiveUserId(item.getReceiveUserId());
                    db.setClient(userInfo.getClient());
                    db.setGmtId(inData.getGmtId());
                    db.setGmtIfShowMao(inData.getGmtIfShowMao());
                    insertList.add(db);
                }


            }
        }
        if (CollectionUtil.isNotEmpty(insertList)) {
            messageTempleStoPositionMapper.insertList(insertList);
        }
        return null;
    }

    @Override
    public Object storeChooseUserQuery(GetLoginOutData userInfo, MessageTemplateQueryUserInData inData) {
        if (StrUtil.isBlank(inData.getMtId())) {
            throw new BusinessException("请传入消息id");
        }
        Map<String, String> checkMap = new HashMap<>();
        List<MessageTemplateChooseUserDb> allStoSettingDbs = getAllStoSetting(userInfo.getClient(), inData.getMtId());
        if (CollectionUtil.isNotEmpty(allStoSettingDbs)) {
            for (MessageTemplateChooseUserDb userDb : allStoSettingDbs) {
                String key = userDb.getClient() + "_" + userDb.getSite() + "_" + userDb.getModel() + "_" + userDb.getPositionId() + "_" + userDb.getUserId();
                checkMap.put(key, "1");
            }
        }
        //如果用户没有个人设定，需要去拉取平台设定
        if (CollectionUtil.isEmpty(allStoSettingDbs)) {
            String mtId = inData.getMtId();
            Map<String, Object> resMap = this.chooseUser(userInfo, mtId);
            List<MessageTemplateChooseUserRes> dataList = (List<MessageTemplateChooseUserRes>) resMap.get("dataList");
            if (CollectionUtil.isNotEmpty(dataList)) {
                for (MessageTemplateChooseUserRes res : dataList) {
                    List<CommonVo> sites = res.getSites();
                    if (CollectionUtil.isNotEmpty(sites)) {
                        for (CommonVo site : sites) {
                            String key = res.getClient() + "_" + site.getLable() + "_" + res.getModel() + "_" + res.getPositionId() + "_" + res.getUserId();
                            checkMap.put(key, "1");
                        }
                    }
                }
            }
        }
        inData.setClient(userInfo.getClient());
        List<MessageTemplateChooseUserDb> dbList = messageTempleStoPositionMapper.selectAllStoUsers(inData);
        dbList.forEach(x -> {
            String key = x.getClient() + "_" + x.getSite() + "_" + x.getModel() + "_" + x.getPositionId() + "_" + x.getUserId();
            x.setCheckFlag(checkMap.get(key) == null ? false : true);
        });
        return dbList;
    }

    @Override
    public List<CommonVo> chooseUserBuildInit(GetLoginOutData userInfo) {
        List<CommonVo> resList = new ArrayList<>();
        Example example = new Example(GaiaStoreData.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("stoStatus", "0")
                .andEqualTo("client", userInfo.getClient());
        List<GaiaStoreData> gaiaStoreData = gaiaStoreDataMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(gaiaStoreData)) {
            gaiaStoreData.forEach(x -> {
                CommonVo res = new CommonVo(x.getStoCode(), StrUtil.isBlank(x.getStoShortName()) ? x.getStoName() : x.getStoShortName());
                resList.add(res);
            });
        }


        return resList;
    }

    @Override
    public void runEffect() {
        String nowDate = CommonUtil.getyyyyMMdd();
        String nowTime = CommonUtil.getHHmmss();
        nowTime = nowTime.substring(0, 4);
        List<MessageTemplate> messageTemplates = messageTemplateMapper.selectAll();

        messageTemplates = messageTemplates.stream().filter(x -> StrUtil.isNotBlank(x.getEffectMode()) && (x.getEffectMode().equals("2") || x.getEffectMode().equals("3"))).collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(messageTemplates)) {
            for (MessageTemplate template : messageTemplates) {
                String effectDate = template.getEffectDate();
                String effectTime = template.getEffectTime();
                if (StrUtil.isNotBlank(effectDate)) {
                    effectDate = effectDate.replaceAll("-", "");
                }
                if (StrUtil.isNotBlank(effectTime)) {
                    effectTime = effectTime.replaceAll(":", "").substring(0, 4);
                }
                if (nowDate.equals(effectDate)) {
                    if (Integer.parseInt(nowTime) >= Integer.parseInt(effectTime)) {
                        if ("2".equals(template.getEffectMode())) {
                            template.setGmtFlag(1);
                            template.setModiId("1");
                            template.setModiDate(nowDate);
                            template.setModiName("sys");
                            template.setModiTime(nowTime);
                        } else if ("3".equals(template.getEffectMode())) {
                            template.setGmtFlag(0);
                            template.setModiId("1");
                            template.setModiDate(nowDate);
                            template.setModiName("sys");
                            template.setModiTime(nowTime);
                        }
                        messageTemplateMapper.updateByPrimaryKey(template);
                    }
                }
            }
        }
    }

    private List<MessageTemplateChooseUserDb> getAllStoSetting(String client, String gmtId) {
        List<MessageTemplateChooseUserDb> resList = new ArrayList<>();
        MessageTemplateStoQuery query = new MessageTemplateStoQuery();
        query.setClient(client);
        query.setGmtId(gmtId);
        List<MessageTemplateChooseUserDb> messageTempleStoPositions = messageTempleStoPositionMapper.selectAllStoSetting(query);
        if (CollectionUtil.isNotEmpty(messageTempleStoPositions)) {
            resList = messageTempleStoPositions;
        }
        return resList;
    }

    private List<MessageTempleClientPosition> getAllClientSetting(String client, String gmtId) {
        List<MessageTempleClientPosition> resList = new ArrayList<>();
        Example example = new Example(MessageTempleClientPosition.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("client", client)
                .andEqualTo("gmtId", gmtId);
        List<MessageTempleClientPosition> positionList = messageTempleClientPositionMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(positionList)) {
            resList = positionList;
        }
        return resList;
    }


    //============================  获取列表（分页）  ==========================
}
