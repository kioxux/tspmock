package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.AcceptService;
import com.gys.business.service.ExamineService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.coldStorage.ProductQueryCondition;
import com.gys.business.service.data.coldStorage.ProductQueryVo;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.common.response.Result;
import com.gys.feign.PurchaseService;
import com.gys.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AcceptServiceImpl implements AcceptService {
    @Resource
    private GaiaSdAcceptHMapper acceptHMapper;

    @Resource
    private GaiaSdAcceptDMapper acceptDMapper;

    @Resource
    private GaiaWmsDiaoboZMapper diaoboZMapper;

    @Resource
    private GaiaWmsDiaoboMMapper diaoboMMapper;

    @Resource
    private GaiaProductBasicMapper productBasicMapper;

    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;

    @Resource
    private GaiaPoHeaderMapper poHeaderMapper;

    @Resource
    private GaiaPoItemMapper poItemMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Resource
    private GaiaBatchInfoMapper batchInfoMapper;

    @Resource
    private GaiaTaxCodeMapper taxCodeMapper;

    @Resource
    private GaiaSupplierBusinessMapper supplierBusinessMapper;

    @Resource
    private PurchaseService purchaseService;

    @Resource
    private GaiaAcceptMapper acceptMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Resource
    private ExamineService examineService;

    @Autowired
    private RedisManager redisManager;

    @Resource
    private AcceptService acceptService;

    @Autowired
    private CosUtils cosUtils;

    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;

    @Resource
    private GaiaSdWtproMapper gaiaSdWtproMapper;

    @Autowired
    private GaiaSdSystemParaMapper sdSystemParaMapper;

    @Resource
    private GaiaSdAcceptHMapper sdAcceptHMapper;

    @Override
    public PageInfo<GetAcceptOutData> selectList(GetAcceptInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        inData.setGsahType("1");
        //查询收货（仓库）主表
        List<GetAcceptOutData> outData = this.acceptHMapper.selectList(inData);

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            //查询收货（仓库）明细表
            for(GetAcceptOutData getAcceptOutData : outData){
                GetAcceptInData acceptInData = new GetAcceptInData();
                acceptInData.setClientId(inData.getClientId());
                acceptInData.setGsahBrId(inData.getGsahBrId());
                acceptInData.setGsahVoucherId(getAcceptOutData.getGsahVoucherId());
                List<GetAcceptDetailOutData> DetailOutData = this.acceptDMapper.detailList(acceptInData);
                getAcceptOutData.setAcceptDetailOutData(DetailOutData);
            }
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }
    @Override
    public PageInfo<GetAcceptOutData> supplierAcceptList(GetAcceptInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) || ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        List<GetAcceptOutData> outData = this.acceptHMapper.supplierAcceptList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<GetDiaoboOutData> diaoboList(GetAcceptInData inData) {
        List<GetDiaoboOutData> outData  = null;
        if (inData.getFlag()){
            outData = this.diaoboZMapper.diaoboList(inData);
        }else {
            outData = this.diaoboZMapper.diaoboListToPrice(inData);
        }
        return outData;
    }

    @Override
    public List<GetPoOutData> poList(GetAcceptInData inData) {
        List<GetPoOutData> outData = this.poHeaderMapper.poList(inData);
        return outData;
    }

    @Override
    public List<GetPoDetailOutData> poDetailList(GetAcceptInData inData) {
        List<GetPoDetailOutData> outData = this.poItemMapper.poDetailList(inData);
        return outData;
    }

    @Override
    public List<GetAcceptDetailOutData> detailList(GetAcceptInData inData) {
        List<GetAcceptDetailOutData> outData = this.acceptDMapper.detailList(inData);
        return outData;
    }

    @Override
    @Transactional
    public String save(GetAcceptInData inData) {
        //查询配送单主表
        Example example = new Example(GaiaWmsDiaoboZ.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId());
        GaiaWmsDiaoboZ diaoboZ = (GaiaWmsDiaoboZ)this.diaoboZMapper.selectOneByExample(example);
        String voucherId = "PS" + inData.getGsahPsVoucherId().substring(2, inData.getGsahPsVoucherId().length());
        //查询配送单明细表
        Example exampleM = new Example(GaiaWmsDiaoboM.class);
        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId());
        List<GaiaWmsDiaoboM> diaoboMList = this.diaoboMMapper.selectByExample(exampleM);
        int i = 1;
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal count = BigDecimal.ZERO;

        GaiaWmsDiaoboM diaoboM;
        for(Iterator var11 = diaoboMList.iterator(); var11.hasNext(); count = count.add(diaoboM.getWmGzsl())) {
            diaoboM = (GaiaWmsDiaoboM)var11.next();
            if (diaoboM.getWmGzsl().compareTo(new BigDecimal(0)) == 1) {
                //商品主数据明细表
                Example examplePro = new Example(GaiaProductBusiness.class);
                examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", diaoboM.getWmSpBm()).andEqualTo("proSite", inData.getGsahBrId());
                GaiaProductBusiness productBusiness = (GaiaProductBusiness) this.productBusinessMapper.selectOneByExample(examplePro);
                //商品批次明细表
                Example exampleBat = new Example(GaiaBatchInfo.class);
                exampleBat.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batBatch", diaoboM.getWmPch()).andEqualTo("batProCode", diaoboM.getWmSpBm());
                List<GaiaBatchInfo> batchInfo = this.batchInfoMapper.selectByExample(exampleBat);
                //保存收货明细表
                GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
                acceptD.setClientId(inData.getClientId());
                acceptD.setGsadVoucherId(voucherId);
                acceptD.setGsadBrId(inData.getGsahBrId());
                acceptD.setGsadProId(diaoboM.getWmSpBm());
                acceptD.setGsadSerial(String.valueOf(i++));
                acceptD.setGsadRefuseQty("0");
                acceptD.setGsadRecipientQty(diaoboM.getWmGzsl() == null ? "0" : diaoboM.getWmGzsl().toString());
                acceptD.setGsadInvoicesQty(diaoboM.getWmGzsl() == null ? "0" : diaoboM.getWmGzsl().toString());
                acceptD.setGsadBatch(diaoboM.getWmPch());
                acceptD.setGsadBatchNo(((GaiaBatchInfo) batchInfo.get(0)).getBatBatchNo());
                acceptD.setGsadValidDate(((GaiaBatchInfo) batchInfo.get(0)).getBatExpiryDate());
                acceptD.setGsadStockStatus(diaoboM.getWmKcztBh());
                acceptD.setGsadAcceptPrice(diaoboM.getWmCkj().toString());
                this.acceptDMapper.insert(acceptD);
                amount = amount.add(diaoboM.getWmCkj().multiply(diaoboM.getWmGzsl()));
            }
        }
        //保存收货主表
        GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
        acceptH.setClientId(inData.getClientId());
        acceptH.setGsahBrId(inData.getGsahBrId());
        acceptH.setGsahVoucherId(voucherId);
        acceptH.setGsahStatus("0");
        acceptH.setGsahType("1");
        acceptH.setGsahAcceptType("3");
        acceptH.setGsahPsVoucherId(inData.getGsahPsVoucherId());
        acceptH.setGsahRemaks("");
        acceptH.setGsahFrom("配送中心");
        acceptH.setGsahTo(inData.getGsahBrId());
        acceptH.setGsahTotalAmt(amount);
        acceptH.setGsahTotalQty(count.toString());
        this.acceptHMapper.insert(acceptH);
        return acceptH.getGsahVoucherId();
    }

    @Transactional
    @Override
    public void insert(GetAcceptInData inData) {
        GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
        acceptH.setClientId(inData.getClientId());
        acceptH.setGsahBrId(inData.getGsahBrId());
        String codePre = DateUtil.format(new Date(), "yyyy");
        String voucherId = this.acceptHMapper.selectNextVoucherId(inData.getClientId(), codePre);
        acceptH.setGsahVoucherId(voucherId);
        acceptH.setGsahBrId(inData.getGsahBrId());
        acceptH.setGsahStatus("0");
        acceptH.setGsahType("2");
        acceptH.setGsahAcceptType("2");
        acceptH.setGsahPoid(inData.getGsahPoid());
        acceptH.setGsahRemaks(inData.getGsahRemaks());
        acceptH.setGsahFrom("配送中心");
        acceptH.setGsahTo(inData.getGsahTo());
        int i = 1;
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;

        for(GetAcceptDetailOutData poItem : inData.getAcceptDetailInDataList()) {
            GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
            acceptD.setClientId(inData.getClientId());
            acceptD.setGsadVoucherId(voucherId);
            acceptD.setGsadProId(poItem.getGsadProId());
            acceptD.setGsadSerial(String.valueOf(i++));
            acceptD.setGsadRefuseQty("0");
            acceptD.setGsadRecipientQty(poItem.getGsadRecipientQty());
            acceptD.setGsadInvoicesQty(poItem.getGsadInvoicesQty());
            acceptD.setGsadBatch(poItem.getGsadBatch());
            acceptD.setGsadBatchNo(poItem.getGsadBatchNo());
            acceptD.setGsadMadeDate(poItem.getGsadMadeDate());
            acceptD.setGsadValidDate(poItem.getGsadValidDate());
            acceptD.setGsadStockStatus("0");
            acceptD.setGsadAcceptPrice(poItem.getProPrice());
            if (ObjectUtil.isNotEmpty(poItem.getProPrice())) {
                totalAmt = totalAmt.add((new BigDecimal(poItem.getProPrice())).multiply(new BigDecimal(poItem.getGsadRecipientQty())));
            }

            totalQty = totalQty.add(new BigDecimal(poItem.getGsadRecipientQty()));
            this.acceptDMapper.insert(acceptD);
        }

        acceptH.setGsahTotalAmt(totalAmt);
        acceptH.setGsahTotalQty(totalQty.toString());
        this.acceptHMapper.insert(acceptH);
    }

    @Transactional
    @Override
    public String insertPo(GetPoInData inData, GetLoginOutData userInfo) {
        String voucherId = poHeaderMapper.selectNextVoucherId(inData.getClientId());
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!日志{}"+voucherId);
        synchronized (voucherId) {
            GaiaPoHeader poHeader = new GaiaPoHeader();
            poHeader.setClient(inData.getClientId());
            poHeader.setPoCompanyCode(inData.getStoreId());
            poHeader.setPoSupplierId(inData.getStoreCode());
            poHeader.setPoId(voucherId);
            poHeader.setPoType("Z001");
            poHeader.setPoSubjectType("2");
            //获取当前时间前一天
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1); //得到前一天
            Date date = calendar.getTime();
            DateFormat df = new SimpleDateFormat("yyyyMMdd");
            poHeader.setPoDate(df.format(date));
            poHeader.setPoHeadRemark(inData.getRemark());
            poHeader.setPoPaymentId(inData.getPaymentId());
            poHeader.setPoCreateBy(userInfo.getUserId());
            poHeader.setPoCreateDate(CommonUtil.getyyyyMMdd());
            poHeader.setPoCreateTime(CommonUtil.getHHmmss());
            this.poHeaderMapper.insert(poHeader);
            int i=1;
            for (GetPoDetailInData detail : inData.getPoDetailList()) {
                GaiaPoItem poItem = new GaiaPoItem();
                poItem.setClient(inData.getClientId());
                poItem.setPoId(voucherId);
                poItem.setPoLineNo(String.valueOf(i));
                poItem.setPoProCode(detail.getProCode());
                poItem.setPoQty(new BigDecimal(detail.getProQty()));
                poItem.setPoLineAmt(new BigDecimal(detail.getProQty()).multiply(new BigDecimal(detail.getProPrice())));
                poItem.setPoCompleteFlag("1");
                poItem.setPoDeliveredQty(new BigDecimal(detail.getProQty()));
                poItem.setPoDeliveredAmt(new BigDecimal(detail.getProQty()).multiply(new BigDecimal(detail.getProPrice())));
                poItem.setPoUnit(detail.getProUnit());
                poItem.setPoPrice(new BigDecimal(detail.getProPrice()));
                poItem.setPoSiteCode(inData.getStoreId());
                poItem.setPoLocationCode("1000");
                poItem.setPoRate(detail.getProRate());
                poItem.setPoDeliveryDate(DateUtil.format(new Date(), "yyyyMMdd"));
                poItem.setPoLineRemark("0");
                this.poItemMapper.insert(poItem);
                i++;
            }
        }
        return voucherId;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePo(GetAcceptInData inData) {
        Example example = new Example(GaiaPoHeader.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", inData.getGsahPoid());
        GaiaPoHeader poHeader = (GaiaPoHeader)this.poHeaderMapper.selectOneByExample(example);
        GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
        acceptH.setClientId(inData.getClientId());
        acceptH.setGsahBrId(inData.getGsahBrId());
        String codePre = DateUtil.format(new Date(), "yyyy");
        String voucherId = this.acceptHMapper.selectNextVoucherId(inData.getClientId(), codePre);
        acceptH.setGsahVoucherId(voucherId);
        acceptH.setGsahStatus("0");
        acceptH.setGsahType("2");
        acceptH.setGsahAcceptType("2");
        acceptH.setGsahPoid(inData.getGsahPoid());
        acceptH.setGsahRemaks(poHeader.getPoHeadRemark());
        acceptH.setGsahFrom(inData.getGsahFrom());
        acceptH.setGsahTo(inData.getGsahBrId());
        acceptH.setGsahDate(DateUtil.format(new Date(), "yyyyMMdd"));
        Example exampleM = new Example(GaiaPoItem.class);
        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", inData.getGsahPoid());
        List<GaiaPoItem> poList = this.poItemMapper.selectByExample(exampleM);
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal count = BigDecimal.ZERO;
        List<GetPoDetailInData> poDetailList=inData.getPoDetailList();
        GaiaPoItem poItem;
        List<MaterialDocRequestDto> materialList = Lists.newArrayList();
        for(Iterator poItemIterator = poList.iterator(); poItemIterator.hasNext(); count = count.add(poItem.getPoQty())) {
            poItem = (GaiaPoItem)poItemIterator.next();
            Example examplePro = new Example(GaiaSdStockBatch.class);
            examplePro.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbProId", poItem.getPoProCode()).andEqualTo("gssbBrId", inData.getGsahBrId()).andEqualTo("gssbBatchNo",poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProBatchNo());
            List<GaiaSdStockBatch> stockBatchList = this.stockBatchMapper.selectByExample(examplePro);
            GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
            acceptD.setClientId(inData.getClientId());
            acceptD.setGsadBrId(inData.getGsahBrId());
            acceptD.setGsadVoucherId(voucherId);
            acceptD.setGsadProId(poItem.getPoProCode());
            acceptD.setGsadSerial(poItem.getPoLineNo());
            acceptD.setGsadBatch(poItem.getPoBatch());
            acceptD.setGsadRefuseQty("0");
            acceptD.setGsadDate(CommonUtil.getyyyyMMdd());
            acceptD.setGsadRecipientQty(poItem.getPoQty().toString());
            acceptD.setGsadInvoicesQty(poItem.getPoQty().toString());
            acceptD.setGsadBatchNo(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProBatchNo());
            if (CollUtil.isNotEmpty(stockBatchList)) {
                acceptD.setGsadValidDate((stockBatchList.get(0)).getGssbVaildDate());
            }else{
                acceptD.setGsadValidDate(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProValiedDate());
            }
            acceptD.setGsadAcceptPrice(poItem.getPoPrice().toString());
            acceptD.setGsadStockStatus("0");
            acceptD.setGsadRowRemark(poItem.getPoLineRemark());
            acceptD.setGsadMadeDate(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProMadeDate());
            acceptD.setGsadTxNo(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getTxNo());
            acceptDMapper.insert(acceptD);

            MaterialDocRequestDto dto = new MaterialDocRequestDto();
            dto.setClient(inData.getClientId());
            dto.setGuid(UUID.randomUUID().toString());
            dto.setGuidNo(acceptD.getGsadSerial());
            dto.setMatPostDate(acceptD.getGsadDate());
            dto.setMatHeadRemark("");
            dto.setMatType("CD");
            dto.setMatProCode(acceptD.getGsadProId());
            dto.setMatPrice(poItem.getPoPrice());
            dto.setMatStoCode("");
            dto.setMatSiteCode(inData.getGsahBrId());
            dto.setMatLocationCode("1000");
            dto.setMatLocationTo("");
            dto.setMatBatch(acceptD.getGsadBatch());
            dto.setMatQty(new BigDecimal(acceptD.getGsadRecipientQty()));
            dto.setMatUnit(poItem.getPoUnit());
            dto.setMatDebitCredit("S");
            dto.setMatPoId(acceptH.getGsahPoid());
            dto.setMatPoLineno(acceptD.getGsadSerial());
            dto.setMatDnId(acceptH.getGsahVoucherId());
            dto.setMatDnLineno(acceptD.getGsadSerial());
            if (ObjectUtil.isNotEmpty(poItem)) {
                dto.setMatLineRemark(poItem.getPoLineRemark());
            }

            dto.setMatCreateBy(inData.getUserId());
            dto.setMatCreateDate(CommonUtil.getyyyyMMdd());
            dto.setMatCreateTime(CommonUtil.getHHmmss());
            materialList.add(dto);
        }

        log.info("list:{}", JSON.toJSONString(materialList));
        String result = purchaseService.insertMaterialDoc(materialList);
        log.info("result:{}", JSON.toJSONString(result));
        if (StrUtil.isBlank(result)) {
            throw new BusinessException("物料凭证接口服务异常");
        }

        acceptH.setGsahTotalAmt(inData.getGsahTotalAmt());
        acceptH.setGsahTotalQty(count.toString());
        acceptHMapper.insert(acceptH);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approve(GetAcceptInData inData, GetLoginOutData userInfo) {
        if (!ObjectUtil.isEmpty(inData.getGsahVoucherId())) {
            Example example = new Example(GaiaSdAcceptH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", inData.getGsahVoucherId()).andEqualTo("gsahBrId", inData.getGsahBrId());
            GaiaSdAcceptH acceptH = this.acceptHMapper.selectOneByExample(example);
            acceptH.setGsahArriveDate(inData.getGsahArriveDate());
            acceptH.setGsahArriveTemperature(inData.getGsahArriveTemperature());
            acceptH.setGsahArriveTime(inData.getGsahArriveTime());
            acceptH.setGsahDeparturePlace(inData.getGsahDeparturePlace());
            acceptH.setGsahDepartureTemperature(inData.getGsahDepartureTemperature());
            acceptH.setGsahDepartureTime(inData.getGsahDepartureTime());
            acceptH.setGsahEmp(inData.getGsahEmp());
            acceptH.setGsahTransportMode(inData.getGsahTransportMode());
            acceptH.setGsahTransportOrganization(inData.getGsahTransportOrganization());
            acceptH.setGsahStatus("1");
            acceptH.setGsahDate(DateUtil.format(new Date(), "yyyyMMdd"));
            this.acceptHMapper.updateByPrimaryKeySelective(acceptH);
            for(GetAcceptDetailOutData detail : inData.getAcceptDetailInDataList()) {
                Example exampleD = new Example(GaiaSdAcceptD.class);
                exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadVoucherId", inData.getGsahVoucherId()).andEqualTo("gsadSerial", detail.getGsadSerial());
                GaiaSdAcceptD acceptD = acceptDMapper.selectOneByExample(exampleD);
                acceptD.setGsadRecipientQty(detail.getGsadRecipientQty());
                acceptD.setGsadRefuseQty(detail.getGsadRefuseQty());
                acceptD.setGsadDate(DateUtil.format(new Date(), "yyyyMMdd"));
                acceptD.setGsadValidDate(detail.getGsadValidDate());
                acceptD.setGsadBatchNo(detail.getGsadBatchNo().trim());
                if (StringUtils.isEmpty(acceptD.getGsadBatchNo())){
                    throw new BusinessException("批号不能为空");
                }
                acceptD.setGsadStockStatus("1");
                if (ObjectUtil.isEmpty(detail.getGsadRefuseQty())){
                    acceptD.setGsadRefuseQty("0");
                }else {
                    acceptD.setGsadRefuseQty(detail.getGsadRefuseQty());
                }
                this.acceptDMapper.updateByPrimaryKey(acceptD);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approvePS(GetAcceptInData inData, GetLoginOutData userInfo) {
        if (!ObjectUtil.isEmpty(inData.getGsahVoucherId())) {
            Example example = new Example(GaiaSdAcceptH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", inData.getGsahVoucherId()).andEqualTo("gsahBrId", inData.getGsahBrId());
            GaiaSdAcceptH acceptH = this.acceptHMapper.selectOneByExample(example);
            acceptH.setGsahArriveDate(inData.getGsahArriveDate());
            acceptH.setGsahArriveTemperature(inData.getGsahArriveTemperature());
            acceptH.setGsahArriveTime(inData.getGsahArriveTime());
            acceptH.setGsahDeparturePlace(inData.getGsahDeparturePlace());
            acceptH.setGsahDepartureTemperature(inData.getGsahDepartureTemperature());
            acceptH.setGsahDepartureTime(inData.getGsahDepartureTime());
            acceptH.setGsahEmp(userInfo.getUserId());
            acceptH.setGsahTransportMode(inData.getGsahTransportMode());
            acceptH.setGsahTransportOrganization(inData.getGsahTransportOrganization());
            acceptH.setGsahStatus("1");
            acceptH.setGsahDate(DateUtil.format(new Date(), "yyyyMMdd"));
            this.acceptHMapper.updateByPrimaryKeySelective(acceptH);
            for(GetAcceptDetailOutData detail : inData.getAcceptDetailInDataList()) {
                Example exampleD = new Example(GaiaSdAcceptD.class);
                exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadVoucherId", inData.getGsahVoucherId()).andEqualTo("gsadSerial", detail.getGsadSerial());
                GaiaSdAcceptD acceptD = acceptDMapper.selectOneByExample(exampleD);
                acceptD.setGsadRecipientQty(detail.getGsadRecipientQty());
                acceptD.setGsadRefuseQty(detail.getGsadRefuseQty());
                acceptD.setGsadDate(DateUtil.format(new Date(), "yyyyMMdd"));
                acceptD.setGsadValidDate(detail.getGsadValidDate());
                acceptD.setGsadStockStatus("1");
                if (ObjectUtil.isEmpty(detail.getGsadRefuseQty())){
                    acceptD.setGsadRefuseQty("0");
                }else {
                    acceptD.setGsadRefuseQty(detail.getGsadRefuseQty());
                }
                this.acceptDMapper.updateByPrimaryKey(acceptD);
            }
            String isExamineAuto = storeDataMapper.selectStoPriceComparison(inData.getClientId(),inData.getGsahBrId(),"EXAMINE_AUTO");
            if (ObjectUtil.isEmpty(isExamineAuto)){
                isExamineAuto = "0";
            }
            if (isExamineAuto.equals("1")){
                GetExamineInData examineInData = new GetExamineInData();
                examineInData.setClientId(inData.getClientId());
                examineInData.setGsehBrId(inData.getStoreCode());
                examineInData.setStoreCode(inData.getStoreCode());
                examineInData.setGsehVoucherAcceptId(inData.getGsahVoucherId());
                examineInData.setGsedEmp(userInfo.getUserId());
                List<GetExamineDetailInData> list = new ArrayList<>();
                for (GetAcceptDetailOutData data1 : inData.getAcceptDetailInDataList()) {
                    GetExamineDetailInData item1 = new GetExamineDetailInData();
                    item1.setGsedProId(data1.getGsadProId());
                    item1.setGsedBatchNo(data1.getGsadBatchNo());
                    item1.setGsedBatch(data1.getGsadBatch());
                    item1.setGsedRecipientQty(data1.getGsadRecipientQty());
                    item1.setGsedQualifiedQty(data1.getGsadInvoicesQty());
                    item1.setGsedUnqualifiedQty(data1.getGsadRefuseQty());
                    item1.setGsedResult("合格");
                    item1.setGsedValidDate(data1.getGsadValidDate());
                    item1.setGsedStockStatus(data1.getGsadStockStatus());
                    item1.setGsedSerial(data1.getGsadSerial());
                    list.add(item1);
                }
                if (list.size() > 0 && list != null){
                    examineInData.setExamineDetailInDataList(list);
                }
                this.examineService.approveEx(examineInData,userInfo);
            }
        }
    }

    @Override
    public String proInfo(GetAcceptInData inData) {
        Example examplePro = new Example(GaiaProductBusiness.class);
        examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", inData.getGsadProId()).andEqualTo("proSite", inData.getGsahBrId());
        GaiaProductBusiness productBusiness = (GaiaProductBusiness)this.productBusinessMapper.selectOneByExample(examplePro);
        if (ObjectUtil.isEmpty(productBusiness)) {
            return "";
        } else {
            Example exampleProd = new Example(GaiaProductBasic.class);
            exampleProd.createCriteria().andEqualTo("proCode", productBusiness.getProCode());
            GaiaProductBasic productBasic = (GaiaProductBasic)this.productBasicMapper.selectOneByExample(exampleProd);
            return ObjectUtil.isNotEmpty(productBasic) ? productBasic.getProName() : "";
        }
    }

    @Override
    public List<GaiaTaxCode> getTax(GetAcceptInData inData) {
        Example examplePro = new Example(GaiaTaxCode.class);
        examplePro.createCriteria().andEqualTo("taxCodeClass", inData.getCodeClass());
        List<GaiaTaxCode> outData = this.taxCodeMapper.selectByExample(examplePro);
        return outData;
    }

    @Override
    public List<GetSupOutData> getSup(GetAcceptInData inData) {
        List<GetSupOutData> outData = this.supplierBusinessMapper.getSup(inData);
        return outData;
    }

    @Override
    public List<IncomeStatementStoreDeficitOutData> queryAcceptProductStock(GetQueryProductInData inData) {
        List<IncomeStatementStoreDeficitOutDataResult> storeDeficitOutDataList = stockBatchMapper.fetchProductStockDetail(inData);
        List<IncomeStatementStoreDeficitOutData> resultList = Lists.newArrayList();
        Table<String, String, BigDecimal> detailTable = HashBasedTable.create();

        for(IncomeStatementStoreDeficitOutDataResult outData : storeDeficitOutDataList){
            BigDecimal existQty = detailTable.get(outData.getBatchNo(), outData.getValidUntil());
            detailTable.put(outData.getBatchNo(), outData.getValidUntil(), existQty == null ? outData.getQty() : outData.getQty().add(existQty));
        }

        for(String batchNo : detailTable.rowMap().keySet()){
            IncomeStatementStoreDeficitOutData incomeStatementStoreDeficitOutData = new IncomeStatementStoreDeficitOutData();
            incomeStatementStoreDeficitOutData.setBatchNo(batchNo);
            List<IncomeStatementStoreDeficitOutDataResult> stockDetailList = Lists.newArrayList();
            for(String key : detailTable.row(batchNo).keySet()){
                Map<String, BigDecimal> map = detailTable.row(batchNo);
                IncomeStatementStoreDeficitOutDataResult incomeStatementStoreDeficitOutDataResult = new IncomeStatementStoreDeficitOutDataResult();
                incomeStatementStoreDeficitOutDataResult.setValidUntil(key);
                incomeStatementStoreDeficitOutDataResult.setQty(map.get(key));
                stockDetailList.add(incomeStatementStoreDeficitOutDataResult);
            }

            incomeStatementStoreDeficitOutData.setStockDetailMap(stockDetailList);
            resultList.add(incomeStatementStoreDeficitOutData);
        }


        return resultList;
    }

    @Override
    public List queryAcceptProduct(GetQueryProductInData inData) {
        List<GetQueryProductOutData> out = this.productBasicMapper.queryAcceptProduct(inData);

        List<IncomeStatementDetailOutData> resultList = Lists.newArrayList();
        for (int i = 0; i < out.size(); ++i) {
            GetQueryProductOutData item = out.get(i);
            item.setIndex(String.valueOf(i + 1));
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
//            item.setPrcAmount(decimalFormat.format(new BigDecimal(item.getPrcAmount())));

            //报损报溢相关接口调用
            if(inData.getAccessType() == 1){
                IncomeStatementDetailOutData detailOutData = new IncomeStatementDetailOutData();
                detailOutData.setGsisdProId(item.getProCode());
                detailOutData.setGsisdProName(item.getProName());
//                detailOutData.setGsisdRetailPrice(item.getPrcAmount());
                detailOutData.setGsisdUnit(item.getProUnit());
                detailOutData.setGsisdFormat(item.getProSpecs());
                detailOutData.setGsisdBatchNo(item.getGssbBatchNo());
                detailOutData.setGsisdValidUntil(item.getGssbValidDate());
                detailOutData.setGsisdQty(item.getProGssQty());
                detailOutData.setGsisdFactory(item.getProFactoryName());
                detailOutData.setGsisdOrigin(item.getProPlace());
                detailOutData.setGsisdDosageForm(item.getProForm());
                detailOutData.setGsisdApprovalNum(item.getProRegisterNo());
                detailOutData.setGsisdBatch(item.getGssbBatch());
                detailOutData.setProRate(item.getProRate());
                detailOutData.setGsrdInputTaxValue(item.getGsrdInputTaxValue());
                detailOutData.setGsisdRetailPrice(item.getPrcAmount());
                resultList.add(detailOutData);
            }
        }
        log.info("queryProduct: {}" + out);
        if(inData.getAccessType() == 1){
            return resultList;
        }
        return out;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String approvePo(GetPoInData inData, GetLoginOutData userInfo) {
        // 检查冷藏商品
        ProductQueryCondition condition = new ProductQueryCondition();
        condition.setClient(inData.getClientId());
        condition.setProSite(inData.getStoreId());
        if (CollectionUtils.isEmpty(inData.getPoDetailList())) {
            throw new BusinessException("收货商品不能为空");
        }
        List<String> proCodes = inData.getPoDetailList().stream().map(GetPoDetailInData::getProCode).collect(Collectors.toList());
        condition.setProSelfCodes(proCodes);
        List<ProductQueryVo> voList = productBusinessMapper.findAllList(condition);
        List<ProductQueryVo> coldProducts = new ArrayList<>();
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(voList)) {
            //过滤是否含有冷藏商品
            coldProducts = voList.stream().filter(ProductQueryVo -> String.valueOf(3).equals(ProductQueryVo.getProStorageCondition())).collect(Collectors.toList());
        }
        // 检查是否填写
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(coldProducts)) {
            if (inData.getColdchainInfo() == null) {
                throw new BusinessException("冷链信息未登记");
            }
            ColdchainInfo coldchainInfo = inData.getColdchainInfo();
            if (StringUtils.isBlank(coldchainInfo.getDeparturePlace())||(coldchainInfo.getBoxQty()==null)||StringUtils.isBlank(coldchainInfo.getTransportOrganization())||
            StringUtils.isBlank(coldchainInfo.getTxFollow())||StringUtils.isBlank(coldchainInfo.getDepartureDate())||StringUtils.isBlank(coldchainInfo.getArriveDate())||
            StringUtils.isBlank(coldchainInfo.getTransportMode())||StringUtils.isBlank(coldchainInfo.getTempFit())||StringUtils.isBlank(coldchainInfo.getDepartureTime())||
            StringUtils.isBlank(coldchainInfo.getArriveTime())||StringUtils.isBlank(coldchainInfo.getTransportEmp())||StringUtils.isBlank(coldchainInfo.getDepartureTemperature())||
            StringUtils.isBlank(coldchainInfo.getArriveTemperature()) ||StringUtils.isBlank(coldchainInfo.getTransportTool())
            ||StringUtils.isBlank(coldchainInfo.getTransportTime())||StringUtils.isBlank(coldchainInfo.getTempType())||StringUtils.isBlank(coldchainInfo.getEmp())) {
                throw new BusinessException("冷链信息未登记完整");
            }
            // 长度限制
            if(coldchainInfo.getDepartureDate().length()>8){
                throw new BusinessException("启运日期长度不能超过8个字符");
            }
            if(coldchainInfo.getDepartureTime().length()>6){
                throw new BusinessException("启运时间长度不能超过6个字符");
            }
            if(coldchainInfo.getArriveDate().length()>8){
                throw new BusinessException("到货日期长度不能超过8个字符");
            }
            if(coldchainInfo.getArriveTime().length()>6){
                throw new BusinessException("到货时间长度不能超过6个字符");
            }
            if(coldchainInfo.getDeparturePlace().length()>40){
                throw new BusinessException("发运地点长度不能超过40个字符");
            }
            if(coldchainInfo.getTransportOrganization().length()>50){
                throw new BusinessException("运输单位长度不能超过50个字符");
            }
            if(coldchainInfo.getTransportCardId().trim()!=null){
                if(coldchainInfo.getTransportCardId().trim().length() != 18 && coldchainInfo.getTransportCardId().trim().length() != 15) {
                    throw new BusinessException("身份证长度不合法，应为15位或18位");
                }
            }
//            if(org.apache.commons.lang3.StringUtils.isNotBlank(coldchainInfo.getTransportCardId())){
//                if(!IDCardUtil.isIDCard(coldchainInfo.getTransportCardId())){
//                    throw new BusinessException("身份证信息不合法！");
//                }
//            }
        }
        String acceptVoucherId = "";
        SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
        Date date = new Date(System.currentTimeMillis());
        String todayDate = formatter.format(date);
        String busEmpCode = inData.getBusEmpCode();
        String busEmpName = "";
        if(ValidateUtil.isNotEmpty(busEmpCode)) {
            busEmpName = acceptMapper.getBusEmpNameListByEmpCode(inData.getClientId(), inData.getStoreId(), inData.getSupCode(), busEmpCode);
        }
        if (inData.getPoDetailList() != null && inData.getPoDetailList().size() > 0) {
            int i = 1;
            for (GetPoDetailInData detail : inData.getPoDetailList()) {
                if (ObjectUtil.isEmpty(detail.getProBatchNo())){
                    throw new BusinessException("第"+i+"行批号不能为空");
                }else {
                    detail.setProBatchNo(detail.getProBatchNo().trim());
                }
                if (ObjectUtil.isEmpty(detail.getProRate())) {
                    detail.setProRate("J0");
                }
                if (ObjectUtil.isEmpty(detail.getRemark())) {
                    detail.setRemark("0");
                }
                if (ObjectUtil.isNull(detail.getProMadeDate())){
                    throw new BusinessException("第"+i+"行生产日期不能为空");
                }
                if (ObjectUtil.isNull(detail.getProValiedDate())){
                    throw new BusinessException("第"+i+"行有效期不能为空");
                }
                if (!isValidDate(detail.getProMadeDate())){
                    throw new BusinessException("第"+i+"行生产日期格式不正确");
                }
                if (!isValidDate(detail.getProValiedDate())){
                    throw new BusinessException("第"+i+"行有效期格式不正确");
                }

                if (!isNumeric(detail.getProQty())){
                    throw new BusinessException("第"+i+"行数量/单价/折扣额填写格式错误，请核对");
                }
                if (!isNumeric(detail.getProPrice())){
                    throw new BusinessException("第"+i+"行数量/单价/折扣额填写格式错误，请核对");
                }
                if (!isNumeric(detail.getRemark())){
                    throw new BusinessException("第"+i+"行数量/单价/折扣额填写格式错误，请核对");
                }
//                if (Long.valueOf(detail.getProMadeDate()) > Long.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date()))){
//                    throw new BusinessException("第"+i+"行生产日期有误，请核对重新输入！");
//                }
                if(StringUtils.isNotEmpty(detail.getProMadeDate()) && StringUtils.isNotEmpty(detail.getProValiedDate())) {
                    if (detail.getProMadeDate().compareTo(todayDate) == 1) {
                        throw new BusinessException("第"+i+"生产日期应该不大于当前日期;");
                    }
                    if (detail.getProValiedDate().compareTo(detail.getProMadeDate()) == -1) {
                        throw new BusinessException("第"+i+"有效期应该大于生产日期;");
                    }
                }
                i++;
            }
            String voucherId = "";
            if (StringUtils.isEmpty(inData.getVoucherId())) {
                //驳回单作废
                if (ObjectUtil.isNotEmpty(inData.getGsahVoucherId())) {
                    acceptHMapper.rejectAcceptOrder(inData.getClientId(), inData.getStoreId(), inData.getGsahVoucherId(), "3");
                }
                String keyName = inData.getClientId() + "-" + inData.getStoreId() + "-" + CommonUtil.getyyyyMMdd() + "-SHCS";
                Boolean f = redisManager.hasKey(keyName);
                if (f) {
                    redisManager.delete(keyName);
                }
                //采购单生成
                voucherId = this.insertPo(inData, userInfo);
            }else {
                //已有采购订单，则更改收货已审核后的商品完成状态
                voucherId = inData.getVoucherId();
//                List<GaiaPoItem> poItems = new ArrayList<>();
//                for (int k = 0;k < inData.getPoDetailList().size(); k ++){
//                    GaiaPoItem item = new GaiaPoItem();
//                    item.setClient(userInfo.getClient());
//                    item.setPoId(voucherId);
//                    item.setPoProCode(inData.getPoDetailList().get(k).getProCode());
//                    item.setPoSiteCode(userInfo.getDepId());
//                    poItems.add(item);
//                }
//                if (ObjectUtil.isNotEmpty(poItems)){
//                    poItemMapper.modifyBatch(poItems);
//                }
            }
            if (ObjectUtil.isNotEmpty(voucherId)) {
                GetAcceptInData saveData = new GetAcceptInData();
                saveData.setClientId(userInfo.getClient());
                saveData.setGsahPoid(voucherId);
                saveData.setGsahBrId(userInfo.getDepId());
                saveData.setGsahFrom(inData.getSupCode());
                saveData.setGsahTotalAmt(inData.getTotalAmt());
                saveData.setGsahRemaks(inData.getRemark());
                saveData.setPoDetailList(inData.getPoDetailList());
                saveData.setGsahEmp(userInfo.getUserId());
                //冷藏信息补充
                if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(coldProducts)){
                    saveData.setColdchainInfo(inData.getColdchainInfo());
                }
                GetAcceptInData outData = new GetAcceptInData();
                if(StringUtils.isEmpty(inData.getVoucherId())) {
                    outData = this.saveWeb(saveData, busEmpName);
                }else {
                    outData = this.saveWebPo(saveData, busEmpName);
                }

                GetAcceptInData poData = new GetAcceptInData();
                poData.setClientId(userInfo.getClient());
                poData.setGsahPoid(voucherId);
                poData.setGsahBrId(userInfo.getDepId());
                PageInfo<GetAcceptOutData> pageInfoList = this.selectList(poData);
                if (CollectionUtil.isNotEmpty(pageInfoList.getList())) {
                    GetAcceptOutData detailOutData = pageInfoList.getList().get(0);
                    saveData.setGsahVoucherId(detailOutData.getGsahVoucherId());
                    saveData.setAcceptDetailInDataList(detailOutData.getAcceptDetailOutData());
                    int idx = 1;
                    for (GetAcceptDetailOutData t : detailOutData.getAcceptDetailOutData()) {
                        t.setGsadBatch(idx + String.valueOf(System.currentTimeMillis()));
                        idx++;
                    }
                }
                saveData.setGsahArriveDate(DateUtil.format(new Date(), "yyyyMMdd"));
                saveData.setGsahArriveTime(DateUtil.format(new Date(), "HHmmss"));
                saveData.setGsahType("2");
                if (ObjectUtil.isNotEmpty(outData.getGsahVoucherId())) {
                    saveData.setAcceptDetailInDataList(outData.getAcceptDetailInDataList());
                    saveData.setGsahVoucherId(outData.getGsahVoucherId());
                    this.approveWeb(saveData, userInfo);
                }
                acceptVoucherId = outData.getGsahVoucherId();
                //判断门店是否可以自动验收
//                String isExamineAuto = storeDataMapper.selectStoPriceComparison(userInfo.getClient(),userInfo.getDepId(),"EXAMINE_AUTO");
//                if (ObjectUtil.isEmpty(isExamineAuto)){
//                    isExamineAuto = "0";
//                }
//                if (isExamineAuto.equals("1")){
//                    GetExamineInData examineInData = new GetExamineInData();
//                    examineInData.setClientId(userInfo.getClient());
//                    examineInData.setGsehBrId(userInfo.getDepId());
//                    examineInData.setStoreCode(userInfo.getDepId());
//                    examineInData.setGsehVoucherAcceptId(voucherId);
//                    List<GetExamineDetailInData> list = new ArrayList<>();
//                    for (GetAcceptDetailOutData data : saveData.getAcceptDetailInDataList()) {
//                        GetExamineDetailInData item = new GetExamineDetailInData();
//                        item.setGsedProId(data.getGsadProId());
//                        item.setGsedBatchNo(data.getGsadBatchNo());
//                        item.setGsedRecipientQty(data.getGsadRecipientQty());
//                        item.setGsedQualifiedQty(data.getGsadInvoicesQty());
//                        item.setGsedUnqualifiedQty(data.getGsadRefuseQty());
//                        item.setGsedResult("合格");
//                        item.setGsedValidDate(data.getGsadValidDate());
//                        item.setGsedStockStatus(data.getGsadStockStatus());
//                        list.add(item);
//                    }
//                    if (list.size() > 0 && list != null){
//                        examineInData.setExamineDetailInDataList(list);
//                    }
//                    this.examineService.approveEx(examineInData,userInfo);
//                }
            }
        }else {
            throw new BusinessException("没有输入审核明细商品!");
        }
        return acceptVoucherId;
    }

    @Override
    public List<GetSupOutData> getSupByList(GetPlaceOrderInData inData) {
        return this.supplierBusinessMapper.getSupByList(inData);
    }

    @Override
    public Map<String,Object> acceptOrderList(AcceptOrderInData inData, GetLoginOutData userInfo) {

//        String[] brIdList = ;
        if(ObjectUtil.isEmpty(inData.getBrIdList()) || inData.getBrIdList().length< 0) {
            throw new BusinessException("请输入门店");
        }

        Map<String,Object> map = new HashMap<>();
//        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),userInfo.getDepId());
//        if (stoInfo.get("stoAttribute").equals("2")){
//            if (ObjectUtil.isNotEmpty(stoInfo.get("stoDcCode"))){
//                inData.setStoCode(stoInfo.get("stoDcCode"));
//            }else{
                inData.setStoCode(userInfo.getDepId());
//            }
//        }
        AcceptOrderTotalOutData listNum = acceptMapper.selectAcceptTotal(inData);
        List<AcceptOrderOutData>  list = acceptMapper.selectAcceptProduct(inData);
        //查询冷链收货信息
        for (AcceptOrderOutData outData : list){
            if (StringUtils.isBlank(outData.getVoucherId())){
                throw new BusinessException("提示：收货单号不可为空！");
            }
            ColdchainInfo coldchainInfo = sdAcceptHMapper.getStoreReceiptCsClodChainInfo(inData.getClientId(),outData.getStoCode(),outData.getVoucherId());
            if (ObjectUtil.isNotEmpty(coldchainInfo)){
                outData.setColdchainInfo(coldchainInfo);
                outData.setColdChainFlag("1");
            }else {
                outData.setColdChainFlag("0");
            }
        }

        if (list.size() > 0 && list != null){
            map.put("list",list);
        }
        map.put("listNum",listNum);
        return map;
    }

    @Override
    public List<GetAcceptOutData> rejectList(GetExamineInData inData) {
        inData.setGsehStatus("2");
        return acceptHMapper.acceptList(inData);
    }

    @Override
    public GetPoInData rejectDetail(GetAcceptInData inData) {
        GetPoInData result = acceptDMapper.selectAcceptTotal(inData);
        ColdchainInfo coldchainInfo = sdAcceptHMapper.getStoreReceiptCsClodChainInfo(inData.getClientId(),inData.getGsahBrId(),inData.getGsahVoucherId());
        if (coldchainInfo == null){
             coldchainInfo = new ColdchainInfo();
             coldchainInfo.setEmp(inData.getUserName());
        }
        result.setColdchainInfo(coldchainInfo);
        if (ObjectUtil.isNotEmpty(result)){
            List<GetPoDetailInData> itemList = acceptDMapper.selectAcceptDetail(inData);
            if (itemList.size() > 0 && itemList != null){
                result.setPoDetailList(itemList);
            }
        }else{
            throw new BusinessException("该驳回单不存在！");
        }
        return result;
    }

    @Override
    public void insertToRedis(GetPoInData inData) {
        if(ObjectUtil.isNotEmpty(inData.getPoDetailList()) && inData.getPoDetailList().size() > 0) {
            String keyName = inData.getClientId() + "-" + inData.getStoreId() + "-" + CommonUtil.getyyyyMMdd() + "-SHCS";
            Integer second = 12 * 3600;
            redisManager.set(keyName, JSON.toJSONString(inData), second.longValue());
        }
    }

    @Override
    public GetPoInData selectFromRedis(GetAcceptInData inData) {
        String keyName = inData.getClientId() + "-" + inData.getStoreCode() + "-" + CommonUtil.getyyyyMMdd() + "-SHCS";
        Object obj = redisManager.get(keyName);
        System.out.println(obj);
        GetPoInData result = JSONArray.parseObject(obj.toString(), GetPoInData.class);
        return result;
    }

    @Override
    public Map<String, String> hasRedisOrNot(GetAcceptInData inData) {
        String keyName = inData.getClientId() + "-" + inData.getStoreCode() + "-" + CommonUtil.getyyyyMMdd() + "-SHCS";
        Boolean f = redisManager.hasKey(keyName);
        Map<String,String> result = new HashMap<>();
        if (f){
            result.put("status","11");
            result.put("msg","已存在未完成收货单，是否继续完成？");
        }else {
            result.put("status","0");
            result.put("msg","");
        }
        return result;
    }

    @Override
    public List<GetSupOutData> getSupByContent(GetAcceptInData inData) {
        return this.supplierBusinessMapper.getSupByContent(inData);
    }

    @Override
    public Result getImportExcelDetailList(HttpServletRequest request,MultipartFile file, GetLoginOutData userInfo) {
        Result result = new Result();
        Map<String,List<ImportAcceptInData>> resultMap = new HashMap<>();
        try {
            String rootPath = request.getSession().getServletContext().getRealPath("/");
            String fileName = file.getOriginalFilename();
            String path = rootPath + fileName;
//            File newFile = new File(path);
//            //判断文件是否存在
//            if(!newFile.exists()) {
//                newFile.mkdirs();//不存在的话，就开辟一个空间
//            }
//            //将上传的文件存储
//            file.transferTo(newFile);
            // 文件全路径
            String url = cosUtils.urlAuth(path);
            String type = url.substring(url.lastIndexOf(".") + 1, url.indexOf("?"));
            InputStream in = file.getInputStream();
            Workbook wk = null;
            if ("xls".equals(type)) {
                //文件流对象
                wk = new HSSFWorkbook(in);
            } else if ("xlsx".equals(type)) {
                wk = new XSSFWorkbook(in);
            } else {
                log.info("非excel文件");
                throw new BusinessException("文件格式有误");
            }
//            if (fileName.indexOf("xlsx") > 0){
                //导入已存在的Excel文件，获得只读的工作薄对象
                //获取第一张Sheet表
                Sheet sheet = wk.getSheetAt(0);
                //取数据
                resultMap = getRowAndCell(sheet,userInfo);
//            inData.setImportInDataList(importInDataList);
                in.close();
                wk.close();
//            }else if(fileName.indexOf("xls") > 0){
//                HSSFWorkbook wk = new HSSFWorkbook(in);
//                //获取第一张Sheet表
//                Sheet sheet = wk.getSheetAt(0);
//                //取数据
//                resultMap = getRowAndCell(sheet,userInfo);
////            inData.setImportInDataList(importInDataList);
//                in.close();
//                wk.close();
//            }


        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        if (resultMap.containsKey("excelOut")){
            //excel数据格式校验不正确，重新写excel导出
            result = exportErrorAcceptPro(resultMap.get("excelOut"),2);
            result.setCode("1000");
        }else if(resultMap.containsKey("excelIn")){
//            String msg = "导入成功，收货单号为：";
            //excel数据格式正确，按照供应商编码分单收货
            List<ImportAcceptInData> importAcceptInDataList = resultMap.get("excelIn");
            //获取不同收货供应商
//            List<ImportAcceptInData> supList = importAcceptInDataList.stream().collect(
//                    Collectors.collectingAndThen(
//                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getSupCode()))), ArrayList::new)
//            );
//            for (ImportAcceptInData supItem : supList){
            GetPoInData inData = new GetPoInData();
            inData.setRemark("");
            inData.setStoreId(userInfo.getDepId());
            inData.setClientId(userInfo.getClient());
            inData.setStoreCode(userInfo.getDepId());
//                inData.setSupCode(supItem.getSupCode());
            BigDecimal totalAmt = BigDecimal.ZERO;
            List<GetPoDetailInData> poDetailInDataList = new ArrayList<>();
            for (ImportAcceptInData item : importAcceptInDataList) {
//                    if (item.getSupCode().equals(supItem.getSupCode())){
                GetPoDetailInData poDetailInData = new GetPoDetailInData();
                poDetailInData.setClientId(userInfo.getClient());
                poDetailInData.setGsrdInputTaxValue(item.getGsrdInputTaxValue());
                poDetailInData.setProBatchNo(item.getProBatchNo());
                poDetailInData.setProCode(item.getProCode());
                poDetailInData.setProFactoryName(item.getProFactoryName());
                poDetailInData.setProMadeDate(item.getProMadeDate());
                poDetailInData.setProValiedDate(item.getProValiedDate());
                poDetailInData.setProName(item.getProName());
                poDetailInData.setProCommonName(item.getProCommonName());
                poDetailInData.setProQty(item.getProQty());
                poDetailInData.setProPrice(item.getProPrice());
                if (ObjectUtil.isNotEmpty(item.getPrcAmount())) {
                    poDetailInData.setGssdPrc1(new BigDecimal(item.getPrcAmount()));
                }
                poDetailInData.setProRate(item.getGsrdInputTax());
                poDetailInData.setProSpecs(item.getProSpecs());
                poDetailInData.setProUnit(item.getProUnit());
                poDetailInDataList.add(poDetailInData);
                //计算总金额
                totalAmt = totalAmt.add(new BigDecimal(item.getProPrice()).multiply(new BigDecimal(item.getProQty())));
//                    }
            }
            inData.setPoDetailList(poDetailInDataList);
            inData.setTotalAmt(totalAmt);
                //直接审核收货单
//                String vourcherId = this.acceptService.approvePo(inData, userInfo);
//                msg = msg + vourcherId + " ";
            result.setData(inData);
            result.setCode("0");
            result.setMsg("商品导入成功");
            result.setMessage("商品导入成功");
        }
        return result;
    }


    @Override
    public List<GetSupOutData> getSupByBrIds(GetAcceptInData inData) {
        return this.supplierBusinessMapper.getSupByBrIds(inData);
    }

    @Transactional
    public GetAcceptInData saveWeb(GetAcceptInData inData, String businessEmp) {
        GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
        acceptH.setClientId(inData.getClientId());
        acceptH.setGsahBrId(inData.getGsahBrId());
        String codePre = DateUtil.format(new Date(), "yyyy");
        String voucherId = this.acceptHMapper.selectNextVoucherId(inData.getClientId(), codePre);
       //冷藏信息补充
        ColdchainInfo coldchainInfo = inData.getColdchainInfo();
        if(coldchainInfo!=null){
            //发运地点
            if(StringUtils.isNotBlank(coldchainInfo.getDeparturePlace())){
                acceptH.setGsahDeparturePlace(coldchainInfo.getDeparturePlace());
            }
             //到货件数
            if(coldchainInfo.getBoxQty()!=null){
                acceptH.setGsahBoxQty(coldchainInfo.getBoxQty());
            }
            //运输单位
            if(StringUtils.isNotBlank(coldchainInfo.getTransportOrganization())){
                acceptH.setGsahTransportOrganization(coldchainInfo.getTransportOrganization());
            }
            //发票同行
            if(StringUtils.isNotBlank(coldchainInfo.getTxFollow())){
                acceptH.setGsahTxFollow(coldchainInfo.getTxFollow());
            }
            //启运日期
            if(StringUtils.isNotBlank(coldchainInfo.getDepartureDate())){
                acceptH.setGsahDepartureDate(coldchainInfo.getDepartureDate());
            }
            //到货日期
            if(StringUtils.isNotBlank(coldchainInfo.getArriveDate())){
                acceptH.setGsahArriveDate(coldchainInfo.getArriveDate());
            }
            //运输方式
            if(StringUtils.isNotBlank(coldchainInfo.getTransportMode())){
                acceptH.setGsahTransportMode(coldchainInfo.getTransportMode());
            }
            //温度符合
            if(StringUtils.isNotBlank(coldchainInfo.getTempFit())){
                acceptH.setGsahTempFit(coldchainInfo.getTempFit());
            }
            //启运时间
            if(StringUtils.isNotBlank(coldchainInfo.getDepartureTime())){
                acceptH.setGsahDepartureTime(coldchainInfo.getDepartureTime());
            }
            //到货时间
            if(StringUtils.isNotBlank(coldchainInfo.getArriveTime())){
                acceptH.setGsahArriveTime(coldchainInfo.getArriveTime());
            }
            //运输人员
            if(StringUtils.isNotBlank(coldchainInfo.getTransportEmp())){
                acceptH.setGsahTransportEmp(coldchainInfo.getTransportEmp());
            }
            //启运温度
            if(StringUtils.isNotBlank(coldchainInfo.getDepartureTemperature())){
                acceptH.setGsahDepartureTemperature(coldchainInfo.getDepartureTemperature());
            }
            //到货温度
            if(StringUtils.isNotBlank(coldchainInfo.getArriveTemperature())){
                acceptH.setGsahArriveTemperature(coldchainInfo.getArriveTemperature());
            }
            //身份证
            if(StringUtils.isNotBlank(coldchainInfo.getTransportCardId())){
                acceptH.setGsahTransportCardId(coldchainInfo.getTransportCardId());
            }
            //运输工具
            if(StringUtils.isNotBlank(coldchainInfo.getTransportTool())){
                acceptH.setGsahTransportTool(coldchainInfo.getTransportTool());
            }
            //运输时长
            if(StringUtils.isNotBlank(coldchainInfo.getTransportTime())){
                acceptH.setGsahTransportTime(coldchainInfo.getTransportTime());
            }
            //温控方式
            if(StringUtils.isNotBlank(coldchainInfo.getTempType())){
                acceptH.setGsahTempType(coldchainInfo.getTempType());
            }
            if(StringUtils.isNotBlank(coldchainInfo.getEmp())){
                acceptH.setGsahEmp(coldchainInfo.getEmp());
            }
            acceptH.setGsahColdchainFlag("1");
        }
        acceptH.setGsahVoucherId(voucherId);
        acceptH.setGsahStatus("0");
        acceptH.setGsahType("2");
        acceptH.setGsahAcceptType("2");
        acceptH.setGsahPoid(inData.getGsahPoid());
        acceptH.setGsahRemaks(inData.getGsahRemaks());
        acceptH.setGsahFrom(inData.getGsahFrom());
        acceptH.setGsahTo(inData.getGsahBrId());
        acceptH.setGsahDate(DateUtil.format(new Date(), "yyyyMMdd"));
        acceptH.setGsahBusinessEmp(businessEmp);
        Example exampleM = new Example(GaiaPoItem.class);
        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", inData.getGsahPoid());
        List<GaiaPoItem> poList = this.poItemMapper.selectByExample(exampleM);
        BigDecimal count = BigDecimal.ZERO;
        BigDecimal countAmt = BigDecimal.ZERO;

        List<GetPoDetailInData> poDetailList = inData.getPoDetailList();
        List<GetAcceptDetailOutData> list = new ArrayList<>();
        GaiaPoItem poItem;
        for(int i = 0; i < poList.size(); i ++){
            poItem = poList.get(i);
            GetAcceptDetailOutData outData = new GetAcceptDetailOutData();
//            Example examplePro = new Example(GaiaSdStockBatch.class);
//            examplePro.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbProId", poItem.getPoProCode()).andEqualTo("gssbBrId", inData.getGsahBrId()).andEqualTo("gssbBatchNo",poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProBatchNo());
//            List<GaiaSdStockBatch> stockBatchList = this.stockBatchMapper.selectByExample(examplePro);
            GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
            acceptD.setClientId(inData.getClientId());
            acceptD.setGsadBrId(inData.getGsahBrId());
            acceptD.setGsadVoucherId(voucherId);
            acceptD.setGsadProId(poItem.getPoProCode());
            acceptD.setGsadSerial(poItem.getPoLineNo());
            acceptD.setGsadRefuseQty("0");
            acceptD.setGsadRecipientQty(poItem.getPoQty().toString());
            acceptD.setGsadInvoicesQty(poItem.getPoQty().toString());
            acceptD.setGsadBatchNo(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProBatchNo());
            acceptD.setGsadAcceptPrice(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProPrice());
            acceptD.setInvoicePrice(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getInvoicePrice());     //发票价
            acceptD.setInvoiceAmount(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getInvoiceAmount());    //发票额
            BigDecimal rowAmt = BigDecimal.ZERO;
            BigDecimal allAmt = new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProPrice()).
                    multiply( new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProQty()));
            if(allAmt.compareTo(new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getRemark())) != 1){
                throw new BusinessException("第"+(Integer.parseInt(poItem.getPoLineNo()) - 1)+"行折扣额填写有误，请核实");
            }
            //行金额   数量乘加个 减去折扣额
            rowAmt = (new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProPrice()).
                    multiply( new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProQty()))).
                    subtract(new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getRemark())).
                    setScale(2, 4);
            countAmt=rowAmt.add(countAmt);
            count = count.add(new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProQty()));
            acceptD.setGsadVoucherAmt(String.valueOf(rowAmt));
            acceptD.setGsadBatch(poItem.getPoBatch());
            acceptD.setGsadValidDate(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProValiedDate());
            acceptD.setGsadStockStatus("0");
//            acceptD.setGsadRowRemark(poItem.getPoLineRemark());
            acceptD.setGsadMadeDate(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProMadeDate());
            acceptD.setGsadTxNo(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getTxNo());
            //计算行总金额
            BigDecimal rAmt = new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProPrice()).multiply( new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProQty()));
                if (StringUtils.isNotEmpty(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getRemark())){
                    acceptD.setGsadRowRemark(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getRemark());
                }else {
                    acceptD.setGsadRowRemark("0");
                }
                if (poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getDiscountRate() != null){
                    BigDecimal discountAmt = new BigDecimal(String.valueOf(rAmt.subtract(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getDiscountRate().multiply(rAmt))));
                    if (new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getRemark()).compareTo(discountAmt) == 0) {
                        acceptD.setDiscountPrice(String.valueOf(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getDiscountPrice()));
                        acceptD.setDiscountRate(String.valueOf(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getDiscountRate()));
                    }
                }

            this.acceptDMapper.insert(acceptD);
            outData.setClientId(inData.getClientId());
            outData.setGsadVoucherId(voucherId);
            outData.setGsadBatchNo(acceptD.getGsadBatchNo());
            outData.setGsadDate(acceptD.getGsadDate());
            outData.setGsadProId(poItem.getPoProCode());
            outData.setGsadSerial(poItem.getPoLineNo());
            outData.setGsadRefuseQty("0");
            outData.setGsadRecipientQty(poItem.getPoQty().toString());
            outData.setGsadInvoicesQty(poItem.getPoQty().toString());
            outData.setGsadBatchNo(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProBatchNo());
            outData.setGsadBatch(poItem.getPoBatch());
            outData.setGsadValidDate(acceptD.getGsadValidDate());
            outData.setGsadStockStatus("0");
            outData.setGsadMadeDate(acceptD.getGsadMadeDate());
            list.add(outData);
            //采购订单数据补全（2020.9.27）
            poItem.setPoCompleteFlag("1");
            poItem.setPoDeliveredQty(new BigDecimal(poDetailList.get(Integer.parseInt(poItem.getPoLineNo())-1).getProQty()));
            poItem.setPoDeliveredAmt(rowAmt);
            this.poItemMapper.updateByPrimaryKey(poItem);
        }
        inData.setAcceptDetailInDataList(list);
        inData.setGsahVoucherId(voucherId);
        acceptH.setGsahTotalAmt(countAmt);
        acceptH.setGsahTotalQty(count.toString());
        this.acceptHMapper.insert(acceptH);
        return inData;
    }

    @Transactional
    public GetAcceptInData saveWebPo(GetAcceptInData inData, String businessEmp) {
        GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
        //冷藏信息补充
        ColdchainInfo coldchainInfo = inData.getColdchainInfo();
        if(coldchainInfo!=null){
            //发运地点
            if(StringUtils.isNotBlank(coldchainInfo.getDeparturePlace())){
                acceptH.setGsahDeparturePlace(coldchainInfo.getDeparturePlace());
            }
            //到货件数
            if(coldchainInfo.getBoxQty()!=null){
                acceptH.setGsahBoxQty(coldchainInfo.getBoxQty());
            }
            //运输单位
            if(StringUtils.isNotBlank(coldchainInfo.getTransportOrganization())){
                acceptH.setGsahTransportOrganization(coldchainInfo.getTransportOrganization());
            }
            //发票同行
            if(StringUtils.isNotBlank(coldchainInfo.getTxFollow())){
                acceptH.setGsahTxFollow(coldchainInfo.getTxFollow());
            }
            //启运日期
            if(StringUtils.isNotBlank(coldchainInfo.getDepartureDate())){
                acceptH.setGsahDepartureDate(coldchainInfo.getDepartureDate());
            }
            //到货日期
            if(StringUtils.isNotBlank(coldchainInfo.getArriveDate())){
                acceptH.setGsahArriveDate(coldchainInfo.getArriveDate());
            }
            //运输方式
            if(StringUtils.isNotBlank(coldchainInfo.getTransportMode())){
                acceptH.setGsahTransportMode(coldchainInfo.getTransportMode());
            }
            //温度符合
            if(StringUtils.isNotBlank(coldchainInfo.getTempFit())){
                acceptH.setGsahTempFit(coldchainInfo.getTempFit());
            }
            //启运时间
            if(StringUtils.isNotBlank(coldchainInfo.getDepartureTime())){
                acceptH.setGsahDepartureTime(coldchainInfo.getDepartureTime());
            }
            //到货时间
            if(StringUtils.isNotBlank(coldchainInfo.getArriveTime())){
                acceptH.setGsahArriveTime(coldchainInfo.getArriveTime());
            }
            //运输人员
            if(StringUtils.isNotBlank(coldchainInfo.getTransportEmp())){
                acceptH.setGsahTransportEmp(coldchainInfo.getTransportEmp());
            }
            //启运温度
            if(StringUtils.isNotBlank(coldchainInfo.getDepartureTemperature())){
                acceptH.setGsahDepartureTemperature(coldchainInfo.getDepartureTemperature());
            }
            //到货温度
            if(StringUtils.isNotBlank(coldchainInfo.getArriveTemperature())){
                acceptH.setGsahArriveTemperature(coldchainInfo.getArriveTemperature());
            }
            //身份证
            if(StringUtils.isNotBlank(coldchainInfo.getTransportCardId())){
                acceptH.setGsahTransportCardId(coldchainInfo.getTransportCardId());
            }
            //运输工具
            if(StringUtils.isNotBlank(coldchainInfo.getTransportTool())){
                acceptH.setGsahTransportTool(coldchainInfo.getTransportTool());
            }
            //运输时长
            if(StringUtils.isNotBlank(coldchainInfo.getTransportTime())){
                acceptH.setGsahTransportTime(coldchainInfo.getTransportTime());
            }
            //温控方式
            if(StringUtils.isNotBlank(coldchainInfo.getTempType())){
                acceptH.setGsahTempType(coldchainInfo.getTempType());
            }
            if(StringUtils.isNotBlank(coldchainInfo.getEmp())){
                acceptH.setGsahEmp(coldchainInfo.getEmp());
            }
            acceptH.setGsahColdchainFlag("1");
        }
        acceptH.setClientId(inData.getClientId());
        acceptH.setGsahBrId(inData.getGsahBrId());
        String codePre = DateUtil.format(new Date(), "yyyy");
        String voucherId = this.acceptHMapper.selectNextVoucherId(inData.getClientId(), codePre);
        acceptH.setGsahVoucherId(voucherId);
        acceptH.setGsahStatus("0");
        acceptH.setGsahType("2");
        acceptH.setGsahAcceptType("2");
        acceptH.setGsahPoid(inData.getGsahPoid());
        acceptH.setGsahRemaks(inData.getGsahRemaks());
        acceptH.setGsahFrom(inData.getGsahFrom());
        acceptH.setGsahTo(inData.getGsahBrId());
        acceptH.setGsahDate(DateUtil.format(new Date(), "yyyyMMdd"));
        acceptH.setGsahBusinessEmp(businessEmp);
//        Example exampleM = new Example(GaiaPoItem.class);
//        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", inData.getGsahPoid());
//        List<GaiaPoItem> poList = this.poItemMapper.selectByExample(exampleM);
        BigDecimal count = BigDecimal.ZERO;
        BigDecimal countAmt = BigDecimal.ZERO;

//        List<GetPoDetailInData> poDetailList = inData.getPoDetailList();
        List<GetAcceptDetailOutData> list = new ArrayList<>();
        for(GetPoDetailInData poDetailInData : inData.getPoDetailList()) {
            GetAcceptDetailOutData outData = new GetAcceptDetailOutData();
            Example exampleM = new Example(GaiaPoItem.class);
            exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", inData.getGsahPoid()).andEqualTo("poLineNo",poDetailInData.getPoLineNo());
            GaiaPoItem poItem = poItemMapper.selectOneByExample(exampleM);
            GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
            acceptD.setClientId(inData.getClientId());
            acceptD.setGsadBrId(inData.getGsahBrId());
            acceptD.setGsadVoucherId(voucherId);
            acceptD.setGsadProId(poDetailInData.getProCode());
            acceptD.setGsadSerial(poItem.getPoLineNo());
            acceptD.setGsadRefuseQty("0");
            acceptD.setGsadRecipientQty(poItem.getPoQty().toString());
            acceptD.setGsadInvoicesQty(poItem.getPoQty().toString());
            acceptD.setGsadBatchNo(poDetailInData.getProBatchNo());
            acceptD.setGsadAcceptPrice(poDetailInData.getProPrice());
            acceptD.setInvoicePrice(poDetailInData.getInvoicePrice());
            acceptD.setInvoiceAmount(poDetailInData.getInvoiceAmount());
            BigDecimal rowAmt = BigDecimal.ZERO;
            BigDecimal allAmt = new BigDecimal(poDetailInData.getProPrice()).
                    multiply( new BigDecimal(poDetailInData.getProQty()));
            if(allAmt.compareTo(new BigDecimal(poDetailInData.getRemark())) != 1){
                throw new BusinessException("第"+poItem.getPoLineNo()+"行折扣额填写有误，请核实");
            }
            //行金额   数量乘加个 减去折扣额
            rowAmt = (new BigDecimal(poDetailInData.getProPrice()).
                    multiply( new BigDecimal(poDetailInData.getProQty()))).
                    subtract(new BigDecimal(poDetailInData.getRemark())).
                    setScale(2, 4);
            countAmt = rowAmt.add(countAmt);
            count = count.add(new BigDecimal(poDetailInData.getProQty()));
            acceptD.setGsadVoucherAmt(String.valueOf(rowAmt));
            acceptD.setGsadBatch(poItem.getPoBatch());
            acceptD.setGsadValidDate(poDetailInData.getProValiedDate());
            acceptD.setGsadStockStatus("0");
//            acceptD.setGsadRowRemark(poItem.getPoLineRemark());
            acceptD.setGsadMadeDate(poDetailInData.getProMadeDate());
            acceptD.setGsadTxNo(poDetailInData.getTxNo());
            //计算行总金额
            BigDecimal rAmt = new BigDecimal(poDetailInData.getProPrice()).multiply( new BigDecimal(poDetailInData.getProQty()));
            if (StringUtils.isNotEmpty(poDetailInData.getRemark())){
                acceptD.setGsadRowRemark(poDetailInData.getRemark());
            }else {
                acceptD.setGsadRowRemark("0");
            }
            if (poDetailInData.getDiscountRate() != null){
                BigDecimal discountAmt = new BigDecimal(String.valueOf(rAmt.subtract(poDetailInData.getDiscountRate().multiply(rAmt))));
                if (new BigDecimal(poDetailInData.getRemark()).compareTo(discountAmt) == 0) {
                    acceptD.setDiscountPrice(String.valueOf(poDetailInData.getDiscountPrice()));
                    acceptD.setDiscountRate(String.valueOf(poDetailInData.getDiscountRate()));
                }
            }
            this.acceptDMapper.insert(acceptD);
            outData.setClientId(inData.getClientId());
            outData.setGsadVoucherId(voucherId);
            outData.setGsadBatchNo(acceptD.getGsadBatchNo());
            outData.setGsadDate(acceptD.getGsadDate());
            outData.setGsadProId(poItem.getPoProCode());
            outData.setGsadSerial(poItem.getPoLineNo());
            outData.setGsadRefuseQty("0");
            outData.setGsadRecipientQty(poDetailInData.getProQty());
            outData.setGsadInvoicesQty(poItem.getPoQty().toString());
            outData.setGsadBatchNo(poDetailInData.getProBatchNo());
            outData.setGsadBatch(poItem.getPoBatch());
            outData.setGsadValidDate(acceptD.getGsadValidDate());
            outData.setGsadStockStatus("0");
            outData.setGsadMadeDate(acceptD.getGsadMadeDate());
            list.add(outData);
//            采购订单数据补全（2020.9.27）
            poItem.setPoCompleteFlag("1");
            poItem.setPoDeliveredQty(new BigDecimal(poDetailInData.getProQty()));
            poItem.setPoDeliveredAmt(rowAmt);
            this.poItemMapper.updateByPrimaryKey(poItem);
        }
        inData.setAcceptDetailInDataList(list);
        inData.setGsahVoucherId(voucherId);
        acceptH.setGsahTotalAmt(countAmt);
        acceptH.setGsahTotalQty(count.toString());
        this.acceptHMapper.insert(acceptH);
        return inData;
    }

    @Transactional(rollbackFor = Exception.class)
    public void approveWeb(GetAcceptInData inData, GetLoginOutData userInfo) {
        if (!ObjectUtil.isEmpty(inData.getGsahVoucherId())) {
            Example example = new Example(GaiaSdAcceptH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", inData.getGsahVoucherId()).andEqualTo("gsahBrId", inData.getGsahBrId());
            GaiaSdAcceptH acceptH = this.acceptHMapper.selectOneByExample(example);
            acceptH.setGsahArriveDate(inData.getGsahArriveDate());
            acceptH.setGsahArriveTemperature(inData.getGsahArriveTemperature());
            acceptH.setGsahArriveTime(inData.getGsahArriveTime());
            acceptH.setGsahDeparturePlace(inData.getGsahDeparturePlace());
            acceptH.setGsahDepartureTemperature(inData.getGsahDepartureTemperature());
            acceptH.setGsahDepartureTime(inData.getGsahDepartureTime());
            acceptH.setGsahEmp(inData.getGsahEmp());
            acceptH.setGsahTransportMode(inData.getGsahTransportMode());
            acceptH.setGsahTransportOrganization(inData.getGsahTransportOrganization());
            acceptH.setGsahStatus("1");
            acceptH.setGsahDate(DateUtil.format(new Date(), "yyyyMMdd"));
            this.acceptHMapper.updateByPrimaryKeySelective(acceptH);
            inData.setUserId(userInfo.getUserId());

            for(GetAcceptDetailOutData detail : inData.getAcceptDetailInDataList()) {
                Example exampleM = new Example(GaiaPoItem.class);
                exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", acceptH.getGsahPoid()).andEqualTo("poLineNo", detail.getGsadSerial());
                GaiaPoItem poItem = this.poItemMapper.selectOneByExample(exampleM);
                String batch = poItem.getPoBatch();
                Example exampleD = new Example(GaiaSdAcceptD.class);
                exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadVoucherId", inData.getGsahVoucherId()).andEqualTo("gsadSerial", detail.getGsadSerial());
                GaiaSdAcceptD acceptD = acceptDMapper.selectOneByExample(exampleD);
                acceptD.setGsadBatch(batch);
                acceptD.setGsadRecipientQty(detail.getGsadRecipientQty());
                if (ObjectUtil.isEmpty(detail.getGsadRefuseQty())){
                    acceptD.setGsadRefuseQty("0");
                }else {
                    acceptD.setGsadRefuseQty(detail.getGsadRefuseQty());
                }
                acceptD.setGsadDate(DateUtil.format(new Date(), "yyyyMMdd"));
                acceptD.setGsadBatchNo(detail.getGsadBatchNo());
                acceptD.setGsadValidDate(detail.getGsadValidDate());
                acceptD.setGsadStockStatus("1");
                this.acceptDMapper.updateByPrimaryKey(acceptD);
            }
        }
    }

    @Override
    public Result exportErrorAcceptPro(List<ImportAcceptInData> list, int type) {
        List<List<Object>> dataList = new ArrayList<>(list.size());
        for(ImportAcceptInData item : list) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
//            //供应商
//            lineList.add(item.getSupCode());
            //商品编码
            lineList.add(item.getProCode());
            //条形码
            lineList.add(item.getProBarCode());
            //商品名称
            lineList.add(item.getProName());
            //通用名
            lineList.add(item.getProCommonName());
            //厂家
            lineList.add(item.getProFactoryName());
            //规格
            lineList.add(item.getProSpecs());
            //单位
            lineList.add(item.getProUnit());
            //零售价
            lineList.add(item.getPrcAmount());
            //批号
            lineList.add(item.getProBatchNo());
            //生产日期
            lineList.add(item.getProMadeDate());
            //有效期
            lineList.add(item.getProValiedDate());
            //数量
            lineList.add(item.getProQty());
            //单价
            lineList.add(item.getProPrice());
            //行金额
//            if (ObjectUtil.isNotEmpty(item.getProLineAmt())) {
//                lineList.add(item.getProLineAmt());
//            }else {
//                lineList.add("");
//            }
            //折扣额
            lineList.add(item.getRemark());
//            //税率
//            lineList.add(item.getGsrdInputTaxValue());
//            //发票号
//            lineList.add(item.getTxNo());
            //状态
            lineList.add(item.getExcelStatus());
            dataList.add(lineList);
        }
        Result result = null;
        if (type == 2) {
            /**
             * 门店信息导入数据错误导出
             */
            String[] header = {"商品编码","国际条形码","商品名称", "通用名", "厂家", "规格", "单位", "零售价", "批号", "生产日期",
                    "有效期", "数量", "单价", "折扣额","状态"};
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(header);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店收货导入反馈");
                    }});
            try {
                workbook.write(bos);
                String fileName = "门店收货导入反馈-" + CommonUtil.getyyyyMMdd() + ".xls";
                result = cosUtils.uploadFile(bos, fileName);
                bos.flush();
            } catch (IOException e) {
                log.error("导出文件失败:{}",e.getMessage(), e);
                throw new BusinessException("导出文件失败！");
            } finally {
                try {
                    bos.close();
                } catch (IOException e) {
                    log.error("关闭流异常:{}",e.getMessage(), e);
                    throw new BusinessException("关闭流异常！");
                }
            }
        }else {
            /**
             * 门店信息导入模板导出
             */
            String[] header = {"商品编码","国际条形码","商品名称", "通用名", "厂家", "规格", "单位", "零售价", "批号", "生产日期",
                    "有效期", "数量", "单价", "折扣额"};
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(header);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店收货导入模板");
                    }});
            try {
                workbook.write(bos);
                String fileName = "门店收货导入模板.xls";
                result = cosUtils.uploadFile(bos, fileName);
                bos.flush();
            } catch (IOException e) {
                log.error("导出文件失败:{}",e.getMessage(), e);
                throw new BusinessException("导出文件失败！");
            } finally {
                try {
                    bos.close();
                } catch (IOException e) {
                    log.error("关闭流异常:{}",e.getMessage(), e);
                    throw new BusinessException("关闭流异常！");
                }
            }
        }
        return result;
    }

    @Override
    public List<HashMap<String, Object>> listUnMatchInfo(GetAcceptInData inData) {
        String[] split = inData.getGsadProId().split(",");
        List<String> list = Arrays.asList(split);
        inData.setProIdList(list);
        return gaiaSdWtproMapper.listWTPSDInfo(inData);
    }

    @Override
    public void checkProSelfCode() {
        //1.0删除之前的消息
        gaiaSdMessageMapper.deleteMessageByType("entrustedDeliveryThirdParty");
        //2.0查询是否有未匹配的数据
        List<HashMap<String, Object>> unMatchList = gaiaSdWtproMapper.listUnMatchedProSelfCode();
        if (!CollectionUtils.isEmpty(unMatchList)) {
            Date now = new Date();
            Map<String, List<HashMap<String, Object>>> clientList = unMatchList.stream().collect(Collectors.groupingBy(map -> map.get("client").toString()));
            Set<String> clientSet = clientList.keySet();
            for (String client : clientSet) {
                List<HashMap<String, Object>> list = clientList.get(client);
                Set<String> proDsfIdSet = list.stream().map(map -> map.get("gswdProDsfId").toString()).collect(Collectors.toSet());
                //3.0如果有数据新增消息，没有数据则不新增
                GaiaSdMessage sdMessage = new GaiaSdMessage();
                sdMessage.setClient(client);
                sdMessage.setGsmId("product");//针对所有加盟商
                String voucherId = gaiaSdMessageMapper.selectNextVoucherId(unMatchList.get(0).get("client").toString(), "company");
                sdMessage.setGsmVoucherId(voucherId);//消息流水号
                sdMessage.setGsmPage(SdMessageTypeEnum.ENTRUSTED_DELIVERY_THIRD_PARTY.page);
                sdMessage.setGsmType(SdMessageTypeEnum.ENTRUSTED_DELIVERY_THIRD_PARTY.code);
                sdMessage.setGsmRemark("截至目前，在委托配送来货单中，共有<font color=\"#FF0000\">" + proDsfIdSet.size() + "</font>条未对码的商品信息，请尽快处理。");
                sdMessage.setGsmFlag("N");//是否查看
                sdMessage.setGsmWarningDay(getMessageProSelfCode(proDsfIdSet));//单号
                sdMessage.setGsmPlatForm("WEB");//消息渠道
                sdMessage.setGsmDeleteFlag("0");
                sdMessage.setGsmArriveDate(com.gys.util.DateUtil.formatDate(now));
                sdMessage.setGsmArriveTime(com.gys.util.DateUtil.dateToString(now, "HHmmss"));
                gaiaSdMessageMapper.insertSelective(sdMessage);
            }
        }
    }

    @Override
    public List<PoOrderOutData> selectOrderList(AcceptOrderInData inData) {
        return acceptMapper.selectOrderList(inData);
    }

    @Override
    public List<Map<String, String>> selectSupplierList(GetAcceptInData inData) {
        return poHeaderMapper.selectSupplierList(inData);
    }

    /**
     * 拼接消息中包含的商品编码字符串
     * @param productCodeList 商品列表
     * @return 商品编码字符串
     */
    private String getMessageProSelfCode(Set<String> productCodeList) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String proSelfCode : productCodeList) {
            sb.append(proSelfCode).append(",");
        }
        return sb.substring(0, sb.length() - 1);
    }


    private static boolean isValidDate(String str) {
        boolean convertSuccess = true;
        // 指定日期格式为四位年/两位月份/两位日期，注意yyyyMMdd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        try {
            // 设置lenient为false.
            // 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(str);
        } catch (ParseException e) {
            // e.printStackTrace();
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            convertSuccess = false;
        }
        return convertSuccess;
    }

    /**
     * 判断输入的是否是数字
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        if(str.indexOf(".")>0){//判断是否有小数点
            if(str.indexOf(".")==str.lastIndexOf(".") && str.split("\\.").length==2){ //判断是否只有一个小数点
                return pattern.matcher(str.replace(".","")).matches();
            }else {
                return false;
            }
        }else {
            return pattern.matcher(str).matches();
        }
    }
    public Map<String,List<ImportAcceptInData>> getRowAndCell(Sheet sheet, GetLoginOutData userInfo) throws FileNotFoundException {
        Map<String,List<ImportAcceptInData>> result = new HashMap<>();
        List<ImportAcceptInData> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            int count = 0;
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                boolean flag = false;
                String msgStr = "";
                Row row = sheet.getRow(i); //取出一行数据放入row
                ImportAcceptInData newExpert = new ImportAcceptInData();
//                GetAcceptInData inData = new GetAcceptInData();
//                inData.setStoreCode(userInfo.getDepId());
//                inData.setClientId(userInfo.getClient());
//                List<GetSupOutData> supList = supplierBusinessMapper.getSup(inData);
//                if (ObjectUtil.isEmpty(supList) || supList.size() <= 0) {
//                    msgStr = msgStr + "供应商不存在;";
//                }
                newExpert.setProCode(dataFormatter.formatCellValue(row.getCell(0)).trim());//商品编码
                newExpert.setProBarCode(dataFormatter.formatCellValue(row.getCell(1)).trim());//国际条形码
                if(ObjectUtil.isNotEmpty(newExpert.getProCode())) {//商品编码不为空 验证国际条形码是否正确
                    GetReplenishInData pro = new GetReplenishInData();
                    pro.setStoCode(userInfo.getDepId());
                    pro.setClientId(userInfo.getClient());
                    pro.setProductCode(newExpert.getProCode());
                    ImportAcceptInData proInfo = productBasicMapper.queryProDetail(pro);
                    if (ObjectUtil.isNotEmpty(proInfo)) {
                        if ("1".equals(proInfo.getNoPurchase())) {
                            flag = true;
                            msgStr = msgStr + "此商品为禁止采购商品;";
                        }
                        if (ObjectUtil.isNotEmpty(newExpert.getProBarCode())) {
                            if (!(newExpert.getProBarCode().equals(proInfo.getProBarCode())) && !(newExpert.getProBarCode().equals(proInfo.getProBarCode2()))) {
                                flag = true;
                                msgStr = msgStr + "国际条形码填写有误;";
                            }
                        }
                        newExpert.setProName(proInfo.getProName());//商品名
                        newExpert.setProCommonName(proInfo.getProCommonName());//通用名
                        newExpert.setProFactoryName(proInfo.getProFactoryName());//厂家
                        newExpert.setProSpecs(proInfo.getProSpecs());//规格
                        newExpert.setProUnit(proInfo.getProUnit());//单位
                        newExpert.setPrcAmount(proInfo.getPrcAmount());//零售价
                        newExpert.setGsrdInputTax(proInfo.getGsrdInputTax());
                        newExpert.setGsrdInputTaxValue(proInfo.getGsrdInputTaxValue());
                    } else {
                        newExpert.setProName(dataFormatter.formatCellValue(row.getCell(2)).trim());//商品名
                        newExpert.setProCommonName(dataFormatter.formatCellValue(row.getCell(3)).trim());//通用名
                        newExpert.setProFactoryName(dataFormatter.formatCellValue(row.getCell(4)).trim());//厂家
                        newExpert.setProSpecs(dataFormatter.formatCellValue(row.getCell(5)).trim());//规格
                        newExpert.setProUnit(dataFormatter.formatCellValue(row.getCell(6)).trim());//单位
                        newExpert.setPrcAmount(dataFormatter.formatCellValue(row.getCell(7)).trim());//零售价
//                    newExpert.setGsrdInputTaxValue(dataFormatter.formatCellValue(row.getCell(14)).trim());//税率值
                        flag = true;
                        msgStr = msgStr + "商品不存在;";
                    }
                }else {//商品编码未填 根据国际条形码查询商品
                    if(StringUtils.isNotEmpty(newExpert.getProBarCode())) {
                        GetReplenishInData pro = new GetReplenishInData();
                        pro.setStoCode(userInfo.getDepId());
                        pro.setClientId(userInfo.getClient());
                        pro.setProductBarCode(newExpert.getProBarCode());
                        ImportAcceptInData proInfo = productBasicMapper.queryProDetail(pro);
                        if (ObjectUtil.isNotEmpty(proInfo)) {
                            if ("1".equals(proInfo.getNoPurchase())) {
                                flag = true;
                                msgStr = msgStr + "此商品为禁止采购商品;";
                            }
//                        if (ObjectUtil.isNotEmpty(newExpert.getProBarCode())) {
//                            if (!(newExpert.getProBarCode().equals(proInfo.getProBarCode())) && !(newExpert.getProBarCode().equals(proInfo.getProBarCode2()))) {
//                                flag = true;
//                                msgStr = msgStr + "国际条形码填写有误;";
//                            }
//                        }
                            newExpert.setProCode(proInfo.getProCode());
                            newExpert.setProName(proInfo.getProName());//商品名
                            newExpert.setProCommonName(proInfo.getProCommonName());//通用名
                            newExpert.setProFactoryName(proInfo.getProFactoryName());//厂家
                            newExpert.setProSpecs(proInfo.getProSpecs());//规格
                            newExpert.setProUnit(proInfo.getProUnit());//单位
                            newExpert.setPrcAmount(proInfo.getPrcAmount());//零售价
                            newExpert.setGsrdInputTax(proInfo.getGsrdInputTax());
                            newExpert.setGsrdInputTaxValue(proInfo.getGsrdInputTaxValue());
                        } else {
                            newExpert.setProName(dataFormatter.formatCellValue(row.getCell(2)).trim());//商品名
                            newExpert.setProCommonName(dataFormatter.formatCellValue(row.getCell(3)).trim());//通用名
                            newExpert.setProFactoryName(dataFormatter.formatCellValue(row.getCell(4)).trim());//厂家
                            newExpert.setProSpecs(dataFormatter.formatCellValue(row.getCell(5)).trim());//规格
                            newExpert.setProUnit(dataFormatter.formatCellValue(row.getCell(6)).trim());//单位
                            newExpert.setPrcAmount(dataFormatter.formatCellValue(row.getCell(7)).trim());//零售价
//                    newExpert.setGsrdInputTaxValue(dataFormatter.formatCellValue(row.getCell(14)).trim());//税率值
                            flag = true;
                            msgStr = msgStr + "商品不存在;";
                        }
                    }else {
                        flag = true;
                        msgStr = msgStr + "商品信息未填写;";
                    }
                }
                newExpert.setProBatchNo(dataFormatter.formatCellValue(row.getCell(8)).trim());//批号
                if(StringUtils.isEmpty(newExpert.getProBatchNo())){
                    msgStr = msgStr + "批号未填写;";
                    flag = true;
                }
                newExpert.setProMadeDate(dataFormatter.formatCellValue(row.getCell(9)).trim());//生产日期
                if (StringUtils.isNotEmpty(newExpert.getProMadeDate())) {
                    if (!isLegalDate(8, newExpert.getProMadeDate(), "yyyyMMdd")) {
                        msgStr = msgStr + "生产日期格式不正确;";
                        flag = true;
                    }
                } else {
                    msgStr = msgStr + "生产日期未填写;";
                    flag = true;
                }
                newExpert.setProValiedDate(dataFormatter.formatCellValue(row.getCell(10)).trim());//有效期
                if (StringUtils.isNotEmpty(newExpert.getProValiedDate())) {
                    if (!isLegalDate(8, newExpert.getProValiedDate(), "yyyyMMdd")) {
                        msgStr = msgStr + "有效期格式不正确;";
                        flag = true;
                    }
                } else {
                    msgStr = msgStr + "有效期未填写;";
                    flag = true;
                }
                SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
                Date date = new Date(System.currentTimeMillis());
                String todayDate = formatter.format(date);
                if(StringUtils.isNotEmpty(newExpert.getProMadeDate()) && StringUtils.isNotEmpty(newExpert.getProValiedDate())) {
                    if (newExpert.getProMadeDate().compareTo(todayDate) == 1) {
                        msgStr = msgStr + "生产日期应该不大于当前日期;";
                        flag = true;
                    }
                    if (newExpert.getProValiedDate().compareTo(newExpert.getProMadeDate()) == -1) {
                        msgStr = msgStr + "有效期应该大于生产日期;";
                        flag = true;
                    }
                }
                newExpert.setProQty(dataFormatter.formatCellValue(row.getCell(11)).trim());//数量
                if (StringUtils.isNotEmpty(newExpert.getProQty())) {
                    if (!isNumeric(newExpert.getProQty())) {
                        msgStr = msgStr + "数量填写不正确;";
                        flag = true;
                    }else{
                        //判断是否是小数
                        Pattern p=Pattern.compile("(\\d+\\.\\d+)");//判断小数
                        Matcher m=p.matcher(newExpert.getProQty());
                        boolean b = m.matches();
                        if(b){              //是小数则判断位数
                            //获取小数点的位置
                            int bitPos = newExpert.getProQty().indexOf(".");
                            //字符串总长度减去小数点位置，再减去1，就是小数位数
                            int numOfBits = newExpert.getProQty().length()-bitPos-1;
                            System.out.println("小数位数为： "+numOfBits);
                            if(numOfBits > 2){
                                msgStr = msgStr + "数量小数位不能大于2位;";
                                flag = true;
                            }
                        }
                    }
                } else {
                    msgStr = msgStr + "数量未填写;";
                    flag = true;
                }
                newExpert.setProPrice(dataFormatter.formatCellValue(row.getCell(12)).trim());//单价
                if (StringUtils.isNotEmpty(newExpert.getProPrice())) {
                    if (!isNumeric(newExpert.getProPrice())) {
                        msgStr = msgStr + "单价填写不正确;";
                        flag = true;
                    }else {
                        //判断是否是小数
                        Pattern p=Pattern.compile("(\\d+\\.\\d+)");
                        Matcher m=p.matcher(newExpert.getProPrice());
                        boolean b = m.matches();
                        if(b){              //是小数则判断位数
                             //获取小数点的位置
                             int bitPos = newExpert.getProPrice().indexOf(".");
                             //字符串总长度减去小数点位置，再减去1，就是小数位数
                             int numOfBits = newExpert.getProPrice().length()-bitPos-1;
                             System.out.println("小数位数为： "+numOfBits);
                             if(numOfBits > 4){
                                 msgStr = msgStr + "单价小数位不能大于4位;";
                                 flag = true;
                             }
                        }
                    }
                } else {
                    msgStr = msgStr + "单价未填写;";
                    flag = true;
                }
                if (StringUtils.isNotEmpty(newExpert.getProPrice())
                        && StringUtils.isNotEmpty(newExpert.getProQty())){
                    if (isNumeric(newExpert.getProPrice()) && isNumeric(newExpert.getProQty())) {
                        newExpert.setProLineAmt(new BigDecimal(newExpert.getProPrice()).multiply(new BigDecimal(newExpert.getProQty())).toString());
                    }
                }
                newExpert.setRemark(dataFormatter.formatCellValue(row.getCell(13)).trim());//折扣额
                if(StringUtils.isNotEmpty(newExpert.getRemark())) {
                    if (!isNumeric(newExpert.getRemark())) {
                        msgStr = msgStr + "折扣额填写不正确;";
                        flag = true;
                    }
                }
                if(!flag){
                    msgStr = "成功";
                    newExpert.setExcelStatus(msgStr);
                }
//                newExpert.setTxNo(dataFormatter.formatCellValue(row.getCell(15)).trim());//发票号
                if(flag){
                    newExpert.setExcelStatus(msgStr);
                    count ++;
                }
                importInDataList.add(newExpert);
            } //行end
            //存在有问题数据，导出数据给客户
            if (count > 0){
                result.put("excelOut",importInDataList);
            }else {
                //数据无问题，分单做收货单
                result.put("excelIn",importInDataList);
            }
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return result;
    }

    public static boolean isLegalDate(int length, String sDate,String format) {
        int legalLen = length;
        if ((sDate == null) || (sDate.length() != legalLen)) {
            return false;
        }
        DateFormat formatter = new SimpleDateFormat(format);
        try {
            Date date = formatter.parse(sDate);
            return sDate.equals(formatter.format(date));
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public JsonResult selectInvoiceShowFlag(GetLoginOutData userInfo) {
        String client = userInfo.getClient();
        String depId = userInfo.getDepId();
        GaiaSdSystemPara systemPara = sdSystemParaMapper.getAuthByClientAndBrId(client, depId, "INVOICE_SHOW");
        if(ValidateUtil.isEmpty(systemPara)) {
            return JsonResult.success("0", "success");
        }
        return JsonResult.success(systemPara.getGsspPara(), "success");
    }


    @Override
    public JsonResult getBusEmpListBySupSelfCode(GetAcceptInData inData) {
        String clientId = inData.getClientId();
        String gsahBrId = inData.getGsahBrId();  //门店编码
        String supCode = inData.getSupCode();    //供应商编码
        if(ValidateUtil.isEmpty(supCode)) {
            throw new BusinessException("提示：供应商编码不能为空");
        }
        //使用供应商编号查询营业员列表
        List<Map<String, String>> busEmpList = acceptMapper.getBusEmpListBySupSelfCode(clientId, gsahBrId, supCode);
        return JsonResult.success(busEmpList, "success");
    }


    @Override
    public JsonResult selectSupplierListByStoCode(PoPriceProductHistoryInData inData) {
        String productSelfCode = inData.getProductSelfCode();
        if(ValidateUtil.isEmpty(productSelfCode)) {
            throw new BusinessException("提示：商品编码不能为空");
        }
        //使用加盟商 门店编码 查询对应供应商列表
        List<Map<String, String>> supplierList = batchInfoMapper.selectSupplierListByStoCode(inData);
        return JsonResult.success(supplierList, "success");
    }


    @Override
    public JsonResult getPoPriceProductHistory(PoPriceProductHistoryInData inData) {
        String productSelfCode = inData.getProductSelfCode();
        Integer pageNo = inData.getPageNo();
        Integer pageSize = inData.getPageSize();
        if(ValidateUtil.isEmpty(productSelfCode)) {
            throw new BusinessException("提示：商品编码不能为空");
        }
        if(ValidateUtil.isEmpty(pageNo)) {
            pageNo = CommonConstant.PAGE_NUM;
        }
        if(ValidateUtil.isEmpty(pageSize)) {
            pageSize = 20;
        }
        //分页
        PageHelper.startPage(pageNo, pageSize);
        //查询该商品历史收货价记录
        List<PoPriceProductHistoryOutData> poPriceProductHistoryList = acceptHMapper.getPoPriceProductHistory(inData);
        com.github.pagehelper.PageInfo<PoPriceProductHistoryOutData> pageInfo
                = new com.github.pagehelper.PageInfo<>(poPriceProductHistoryList);
        return JsonResult.success(pageInfo, "success");
    }


    @Override
    public JsonResult getProductCreateDate(PoPriceProductHistoryInData inData) {
        String productSelfCode = inData.getProductSelfCode();
        String validityDate = inData.getValidityDate();
        if(ValidateUtil.isEmpty(productSelfCode)) {
            throw new BusinessException("提示：商品编码不能为空");
        }
        if(ValidateUtil.isEmpty(validityDate)) {
            throw new BusinessException("提示：有效期不能为空");
        }

        String productCreateDate = acceptHMapper.getProductCreateDate(inData);
        if(ValidateUtil.isEmpty(productCreateDate)) {
            productCreateDate = "";
        }
        return JsonResult.success(productCreateDate, "success");
    }

    @Override
    public void rejectAcceptOreder(GetAcceptInData inData) {
        acceptHMapper.rejectAcceptOrder(inData.getClientId(),inData.getGsahBrId(),inData.getGsahVoucherId(),"3");
    }

    @Override
    public ProductMadeDateOrValidityDateOutData getCreateDateOrValidityDate(PoPriceProductHistoryInData inData) {
        if (StringUtils.isBlank(inData.getProductSelfCode())){
            throw new BusinessException("提示：商品编码不能为空!");
        }
        if (StringUtils.isBlank(inData.getMadeDate()) && StringUtils.isBlank(inData.getValidityDate())){
            throw new BusinessException("提示：有效期和生产日期请至少输入一个!");
        }
        ProductMadeDateOrValidityDateOutData outData = new ProductMadeDateOrValidityDateOutData();
        if (StringUtils.isNotBlank(inData.getValidityDate())){
            outData.setProMadeDate(acceptHMapper.getProductCreateDate(inData));
        }
        if (StringUtils.isNotBlank(inData.getMadeDate())){
            outData.setProValiedDate(acceptHMapper.getProductValidityDate(inData));
        }
        return outData;
    }
}
