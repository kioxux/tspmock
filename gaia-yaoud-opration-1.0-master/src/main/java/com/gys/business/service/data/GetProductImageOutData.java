package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaProductBasicImage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "商品图片信息对象",description = "商品图片信息对象")
public class GetProductImageOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "校验标记 1-成功 2-存在异常记 ")
    private String checkFlag;

    @ApiModelProperty(value = "失败文件清单")
    private List<String> errorList;

    @ApiModelProperty(value = "图片清单")
    private List<GaiaProductBasicImage> imageList;
}
