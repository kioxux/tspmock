package com.gys.business.service.data.replenishParam;

import lombok.Data;

@Data
public class ReplenishParamInData {
    private String clientId;//加盟商编码
    private String[] stoCodes;//门店编码数组
    private String flag1;//冷链 0 否 1 是
    private String flag2;//含麻 0 否 1 是
    private String flag3;//二类精神 0 否 1 是
    private String flag4;//三类器械 0 否 1 是
    private String flag5;//散装中药 0 否 1 是
    private String flag6;//精致饮片 0 否 1 是
    private String flag7;//税率
    private String updateEmp; //更新人
}
