package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class StoreProJylbOutData implements Serializable {

    /**
     * 门店编码+商品自编码拼接字符串
     */
    @ApiModelProperty(value="门店编码+商品自编码拼接字符串")
    private String storeProCodeStr;

    /**
     * 经营类别
     */
    @ApiModelProperty(value="经营类别")
    private String jylb;
}
