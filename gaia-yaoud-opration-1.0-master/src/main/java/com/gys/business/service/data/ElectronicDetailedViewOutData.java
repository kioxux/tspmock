package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/10/4
 */
@Data
public class ElectronicDetailedViewOutData implements Serializable {
    private static final long serialVersionUID = -7371572730174996361L;


    /**
     * 主题单号
     */
    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "主题描述")
    private String gsetsName;

    @ApiModelProperty(value = "主题属性")
    private String gsetsFlag;

    @ApiModelProperty(value = "选择电子券信息")
    private ThemeSettingToAutoSetVO toAutoSetVO;

    @ApiModelProperty(value = "送券所有信息")
    private ElectronicAllCouponOutData coupon;

    @ApiModelProperty(value = "用券所有信息")
    private ElectronicAllVoucherOutData voucher;

}
