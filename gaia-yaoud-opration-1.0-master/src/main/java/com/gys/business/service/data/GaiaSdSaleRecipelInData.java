//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@ApiModel(value = "处方登记")
@Data
public class GaiaSdSaleRecipelInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String stoCode;

    /**
     * 处方类型
     */
    private String inpGssrType;

    /**
     * 处方号
     */
    private String inpGssrRecipelId;

    /**
     * 审方日期
     */
    private String inpGssrCheckDate;

    /**
     * 商品编码
     */
    private String inpGssrProId;

    /**
     * 药师姓名
     */
    private String inpGssrPharmacistName;

    /**
     * 患者姓名
     */
    private String inpGssrCustName;

    /**
     * 门店集合
     */
    private String[] brIdList;

    /**
     * 患者手机
     */
    private String inpGssrCustMobile;

    /**
     * 当前页
     */
    private Integer pageNum;

    /**
     * 每页条数
     */
    private Integer pageSize;

    /**
     * 登记开始时间
     */
    private String startDate;


    /**
     * 登记结束日期
     */
    private String endDate;
}
