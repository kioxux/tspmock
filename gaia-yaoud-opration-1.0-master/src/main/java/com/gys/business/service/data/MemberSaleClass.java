package com.gys.business.service.data;

import lombok.Data;

/**
 * 销售级别
 */
@Data
public class MemberSaleClass {
    private String saleClass;
}
