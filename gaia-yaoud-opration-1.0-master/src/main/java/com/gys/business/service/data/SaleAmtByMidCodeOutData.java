package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SaleAmtByMidCodeOutData implements Serializable {

    @ApiModelProperty(value = "商品大类编码")
    private String midTypeCode;

    @ApiModelProperty(value = "销售额")
    private BigDecimal saleAmt;
}
