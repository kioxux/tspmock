//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdMemberBasicMapper;
import com.gys.business.mapper.GaiaSdRechargeCardMapper;
import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.service.MemberService;
import com.gys.business.service.data.MemberCardInData;
import com.gys.business.service.data.MemberCardOutData;
import com.gys.business.service.data.member.MemberInData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;
    @Autowired
    private GaiaSdRechargeCardMapper rechargeCardMapper;

    public MemberServiceImpl() {
    }

    public List<MemberCardOutData> getCardList(MemberCardInData inData) {
        return this.memberBasicMapper.selectMemberCardList(inData);
    }

    @Override
    public GaiaSdRechargeCard getRechargeCardByMemberCardId(MemberInData memberInData) {
        Map<String,Object> inMap = new HashMap<>();
        inMap.put("client",memberInData.getClient());
        inMap.put("memberCardId",memberInData.getMemberCardId());
        List<GaiaSdRechargeCard> memberCardData =  this.rechargeCardMapper.getRechargeCardByMemberCardId(inMap);
        if(ObjectUtil.isNotEmpty(memberCardData) && memberCardData.size() == 1){
            //  理论上他们说 会员 和 储值卡是一对一 但是那个男人的话不可信
            //  防止报服务器异常
            GaiaSdRechargeCard memberCardInfo = memberCardData.get(0);
            return memberCardInfo;
        }
        return null;
    }
}
