package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfApproveInData {
    private String wfDefineCode;
    private String wfOrder;
    private String wfStatus;
    private String clientId;
    private String site;
    private String createUser;
    private String approverUserId;
    private String memo;
}
