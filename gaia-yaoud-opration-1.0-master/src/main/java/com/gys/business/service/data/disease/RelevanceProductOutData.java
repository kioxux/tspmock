package com.gys.business.service.data.disease;

import lombok.Data;

import java.util.List;

/**
 * @Description 关联推荐 出参
 * @Author huxinxin
 * @Date 2021/8/6 14:03
 * @Version 1.0.0
 **/
@Data
public class RelevanceProductOutData {

    //  疾病编码
    private String diseaseCode;
    //  疾病名称
    private String disease;
    //  方案代码
    private String schemeCode;

    //  首推商品(两个的那个)
    private List<RecommendProductBO> firstRecommend;
    //  关联推荐(六个的那个)
    private List<RecommendProductBO> relevanceRecommend;

}

