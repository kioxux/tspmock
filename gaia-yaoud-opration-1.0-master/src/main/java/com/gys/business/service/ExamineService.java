//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdExamineH;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;
import java.util.Map;

public interface ExamineService {
    PageInfo<GetExamineOutData> selectList(GetExamineInData inData);

    List<GetAcceptOutData> acceptList(GetExamineInData inData);

    List<GetExamineDetailOutData> detailList(GetExamineInData inData);

    String save(GetExamineInData inData);

    void approve(GetExamineInData inData);

    void approveEx(GetExamineInData inData, GetLoginOutData userInfo);

    List<GetExamineOutData> selectExamineList(GetExamineInData inData);

    List<GetExamineDetailOutData> examineDetailList(GetExamineInData inData);

    Map<String,Object> examineOrderList(ExamineOrderInData inData, GetLoginOutData userInfo);

    void rejectAcceptOreder(GetExamineInData inData);

    List<GetAcceptOutData> acceptCSList(GetExamineInData inData);
}
