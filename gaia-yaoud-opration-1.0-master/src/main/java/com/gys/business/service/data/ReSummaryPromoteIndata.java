package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class ReSummaryPromoteIndata{
    private String isMember;
    private GetDisPriceOutData getDisPriceOutData;
    private List<RebornData> rebornDataList;
}
