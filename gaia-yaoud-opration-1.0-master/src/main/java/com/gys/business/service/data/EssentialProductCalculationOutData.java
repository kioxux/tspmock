package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class EssentialProductCalculationOutData implements Serializable {

    @ApiModelProperty(value = "大类")
    private String bigType;

    @ApiModelProperty(value = "大类名称")
    private String bigTypeName;

    @ApiModelProperty(value = "中类")
    private String midType;

    @ApiModelProperty(value = "中类名称")
    private String midTypeName;

    @ApiModelProperty(value = "商品分类")
    private String type;

    @ApiModelProperty(value = "商品分类名称")
    private String typeName;

    @ApiModelProperty(value = "商品编码")
    private String productCode;

    @ApiModelProperty(value = "商品描述")
    private String desc;

    @ApiModelProperty(value = "商品规格")
    private String specs;

    @ApiModelProperty(value = "生产厂家")
    private String factoryName;

    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "仓库成本价")
    private BigDecimal wmsCostPrice;

    @ApiModelProperty(value = "门店编码")
    private String storeId;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    @ApiModelProperty(value = "门店属性")
    private String storeAttribute;

    @ApiModelProperty(value = "店效级别")
    private String storeLevel;

    @ApiModelProperty(value = "建议铺货量")
    private Integer adviseNumber;

    @ApiModelProperty(value = "确认铺货量")
    private Integer resultNumber;
}
