package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class NoMoveChartOutData implements Serializable {

    @ApiModelProperty(value = "不动销品项数-非药品")
    private BigDecimal noMoveNoDrugCount;

    @ApiModelProperty(value = "不动销品项数-中药")
    private BigDecimal noMoveChineseDrugCount;

    @ApiModelProperty(value = "不动销品项数-药品")
    private BigDecimal noMoveDrugCount;

    @ApiModelProperty(value = "不动销品项数-处方药")
    private BigDecimal noMovePrescriptionCount;

    @ApiModelProperty(value = "不动销品项数-OTC")
    private BigDecimal noMoveOTCCount;

    @ApiModelProperty(value = "不动销库存成本额占比-非药品")
    private BigDecimal noMoveNoDrugCostAmtProp;

    @ApiModelProperty(value = "不动销库存成本额占比-中药")
    private BigDecimal noMoveChineseCostAmtProp;

    @ApiModelProperty(value = "不动销库存成本额占比-药品")
    private BigDecimal noMoveDrugCostAmtProp;

    @ApiModelProperty(value = "不动销库存成本额占比-处方药")
    private BigDecimal noMovePrescriptionCostAmtProp;

    @ApiModelProperty(value = "不动销库存成本额占比-OTC")
    private BigDecimal noMoveOTCCostAmtProp;

    @ApiModelProperty(value = "不动销库存占比-合计")
    private BigDecimal noMoveTotalProp;

    @ApiModelProperty(value = "不动销库存占比-处方药")
    private BigDecimal noMovePrescriptionProp;

    @ApiModelProperty(value = "不动销库存占比-OTC")
    private BigDecimal noMoveOTCProp;

    @ApiModelProperty(value = "不动销库存占比-药品")
    private BigDecimal noMoveDrugProp;

    @ApiModelProperty(value = "不动销库存占比-中药")
    private BigDecimal noMoveChineseDrugProp;

    @ApiModelProperty(value = "不动销库存占比-非药品")
    private BigDecimal noMoveNoDrugProp;
}
