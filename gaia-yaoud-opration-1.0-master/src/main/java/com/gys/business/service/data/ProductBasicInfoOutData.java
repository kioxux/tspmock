package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductBasicInfoOutData implements Serializable {

    @ApiModelProperty(value = "药德商品编码")
    private String proCode;

    @ApiModelProperty(value = "通用名")
    private String proCommonName;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "规格")
    private String specs;

    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "助记码")
    private String pym;

    @ApiModelProperty(value = "剂型")
    private String form;

    @ApiModelProperty(value = "产地")
    private String proPlace;
}
