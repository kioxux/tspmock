package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回主题实体
 *
 * @author xiaoyuan on 2020/9/29
 */
@Data
public class ThemeSettingOutData implements Serializable {
    private static final long serialVersionUID = -1846894270510556177L;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "主题描述")
    private String gsetsName;
}
