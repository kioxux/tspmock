package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class StoreDistributaionDataInData {

    private String client;

    @ApiModelProperty(value = "地点 即仓库编码")
    private String dcCode;
    @ApiModelProperty(value = "客户性质 1代表只要内部客户 2代表只要外部客户 其他代表全都要")
    private String customerFlag;
    @ApiModelProperty(value = "客户编码")
    private List<String> customerCodeList;
    @ApiModelProperty(value = "开始日期")
    private String startDate;
    @ApiModelProperty(value = "结束日期")
    private String endDate;
    @ApiModelProperty(value = "是否包含调剂 1表示是 0表示否")
    private String ifHasMn;
    @ApiModelProperty(value = "sql")
    private String sqlString;

}
