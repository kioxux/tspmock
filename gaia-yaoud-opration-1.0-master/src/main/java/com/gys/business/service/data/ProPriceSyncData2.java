package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class ProPriceSyncData2 {
    private String client;
    private List<String> storeList;
    private List<String> proIds;
}
