package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaSdPrintSaleDto implements Serializable {
    private static final long serialVersionUID = 7268621252753794038L;

    private String client;

    private String brId;

    /**
     * 模块1
     */
    private String gspsModule1;

    /**
     * 模块5
     */
    private String gspsModule5;

    /**
     * 模块6
     */
    private String gspsModule6;

    /**
     * 国家定点编码
     */
    private String stoYbcode;

    /**
     * 药师姓名
     */
    private String userNam;

    /**
     * 医保药师编码
     */
    private String userId;

}
