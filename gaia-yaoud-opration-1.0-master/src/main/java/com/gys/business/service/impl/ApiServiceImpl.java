package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;
import com.dianping.lion.client.util.Json;
import com.google.common.collect.Maps;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ApiService;
import com.gys.business.service.HebBillData;
import com.gys.business.service.HebSaleBillService;
import com.gys.business.service.PayService;
import com.gys.business.service.data.HebBillInData;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.common.data.JsonResult;
import com.gys.common.redis.RedisManager;
import com.gys.util.DateUtil;
import com.gys.util.JsonUtils;
import com.gys.util.MapSortUtil;
import com.gys.util.SignErBaiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gys.util.MapSortUtil.sortMapByKey;
import static java.lang.Math.abs;

@Service
@Slf4j
public class ApiServiceImpl implements ApiService {
    @Resource
    private ApiMapper apiMapper;
    @Resource
    private RedisManager redisManager;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;
    @Resource
    private GaiaSdWtpdsHMapper gaiaSdWtpdsHMapper;
    @Resource
    private GaiaSdWtpsdDMapper gaiaSdWtpsdDMapper;
    @Resource
    private GaiaSdWTSTOMapper gaiaSdWTSTOMapper;
    @Resource
    private GaiaSdWtproMapper gaiaSdWtproMapper;
    @Resource
    private GaiaSdDsfStockMapper gaiaSdDsfStockMapper;

    @Autowired
    private HebSaleBillService hebSaleBillService;

    @Autowired
    private PayService payService;

    @Override
    public Map<String, Object> selectStoreStockList(Map<String, Object> inData) {
        Map<String, Object> result = new HashMap<>();
        Map<String,Object> msg = new HashMap<>();
        Long setTime = 6*60l;
        Long time = new Date().getTime()/1000;
        Long timestamp  = Long.valueOf(inData.get("timestamp").toString())/1000;
        if (abs(timestamp - time) <= setTime) {
            String secret = apiMapper.selectPartnerSecret(inData.get("source").toString());
            String storeCode = JSONObject.parseObject(inData.get("body").toString()).get("storeCode").toString();
            if (ObjectUtils.isEmpty(storeCode)) {
                msg.put("errno", 1001);
                msg.put("error", "关键参数门店编码未提交");
                msg.put("data", "");
            } else {
                String key = inData.get("source").toString() + "-" + inData.get("cmd") + "-" + storeCode;
                boolean r = redisManager.hasKey(key);
                if (r){
                    msg.put("errno", 1004);
                    msg.put("error", "请求频次超过阈值");
                    msg.put("data", "");
                }else {
                    Map<String, String> secondMap = apiMapper.selectPartnerParam(inData.get("source").toString(), inData.get("cmd").toString());
                    boolean f = SignErBaiUtil.checkSign(inData, inData.get("source").toString(), secret);
                    if (f) {
                        Map<String, String> storeInfo = apiMapper.selectPartnerStoInfo(inData.get("source").toString(), storeCode);
                        List<Map<String, Object>> proStockList = apiMapper.selectProStockCode(storeInfo.get("clientId"), storeInfo.get("stoCode"));
                        msg.put("errno", 0);
                        msg.put("error", "success");
                        msg.put("data", proStockList);
                        //请求成功加入缓存
                        long l =  new BigDecimal(secondMap.get("requestSecond")).longValue();
                        redisManager.set(key,secondMap.get("appid"),l);
                    } else {
                        msg.put("errno", 1002);
                        msg.put("error", "密钥不正确");
                        msg.put("data", "");
                    }
                }
            }
        }else {
            msg.put("errno", 1003);
            msg.put("error", "请求时间误差太大");
            msg.put("data", "");
        }
        result.put("body", msg);
        result.put("cmd", inData.get("cmd"));
        result.put("encrypt", "");
//        result.put("secret",secret);
        result.put("source", inData.get("source"));
        result.put("ticket", inData.get("ticket"));
//        result.put("timestamp",new Date().getTime());
        result.put("timestamp", inData.get("timestamp"));
        result.put("version", inData.get("version"));
        result = sortMapByKey(result);
        String sign = SignErBaiUtil.getSign1(result);
        result.put("sign", sign);
        return result;
    }

    @Override
    public JsonResult addWTPSD(Map<String,Object> inData){
        JsonResult checkResult=checkRequest(inData);
        if(checkResult.getCode()!=0){return checkResult;}
        //新增GAIA_SD_WTPSD_D
        List<GaiaSdWtpsdD> wtpsdDList=new ArrayList<>();
        //新增GAIA_SD_WTPSD_H
        List<GaiaSdWtpdsH> wtpdsHList=new ArrayList<>();
        String franchisee=inData.containsKey("Client")?inData.get("Client").toString():"10000042";
        //10000042 采芝灵 10000098 全家康健
        transformModel(franchisee,(List<Map<String,Object>>)checkResult.getData(),wtpsdDList,wtpdsHList);
        Boolean addFlg=addWtps(wtpsdDList,wtpdsHList);
        if (addFlg){
            return  JsonResult.success("","写入成功");
        }else{
            return JsonResult.fail(500,"写入失败");
        }
    }
    private void transformModel(String franchisee, List<Map<String,Object>> inData,List<GaiaSdWtpsdD> wtpsdDList,List<GaiaSdWtpdsH> wtpdsHList){
        for (Map<String,Object> data : inData){
            GaiaSdWtpsdD model=new GaiaSdWtpsdD();
            model.setClient(franchisee);
            switch (franchisee){
                case "10000042":
                    model.setGswdSupId("GYS001");
                    model.setGswdCreEMP("紫石写入");
                    //Price*Rate*0.01 四舍五入
                    model.setGswdTAX(new BigDecimal(data.get("Price").toString()).multiply(new BigDecimal(data.get("Rate").toString()).multiply(new BigDecimal(0.01))).setScale(4,BigDecimal.ROUND_HALF_UP).toString() );
                    break;
                case "10000098":
                    model.setGswdSupId("0102");
                    model.setGswdCreEMP("蓬晖写入");
                    model.setGswdTAX("0");
//                    //传入的是GAIA_PRODUCT_BUSINESS.PRO_CODE需要转换成PRO_SELF_CODE
//                    Map<String, Integer> proMap = Maps.newHashMap();
//                    proMap.put(data.get("RfId").toString(),0);
//                    List<GaiaProductBusiness> productList = gaiaProductBusinessMapper.getListByProCode(franchisee, proMap);
//                    if(!productList.isEmpty()){
//                        model.setGswdProId(productList.get(0).getProCode());
//                    }
                    break;
            }
            model.setGswdVoucherId(data.get("BillCode").toString());
            model.setGswdDsfBrId(data.get("OrgNo").toString());
            model.setGswdDate(data.get("Dates").toString().replace("-",""));
            model.setGswdSerial(data.get("BillSn").toString());
            model.setGswdProDsfId(data.get("spbh").toString());
            model.setGswdProId(data.get("RfId").toString());
            model.setGswdMadeDate(data.get("ProduceDate").toString().replace("-",""));
            model.setGswdBatchNo(data.get("BatchCode").toString());
            model.setGswdBatch(data.get("pici").toString());
            model.setGswdExpiryDate(data.get("ValDate").toString().replace("-",""));
            model.setGswdQTY(new BigDecimal(data.get("Num").toString()) );
            model.setGswdPRC(new BigDecimal(data.get("TaxPrice").toString()) );

            model.setGswdAMT(new BigDecimal(data.get("TaxAmount").toString()));
            model.setGswdRemarks(data.get("Remark").toString());

            model.setGswdName(data.get("ProName").toString());
            model.setGswdCommonName(data.get("ProCommonName").toString());
            model.setGswdBarcode(data.get("ProBarCode").toString());
            model.setGswdSpecs(data.get("ProSpecs").toString());
            model.setGswdUnit(data.get("ProUnit").toString());
            model.setGswdFactoryName(data.get("ProFactoryName").toString());
            model.setGswdPlace(data.get("ProPlace").toString());
            model.setGswdPorm(data.get("ProPorm").toString());
            model.setGswdRegisterNo(data.get("ProRegisterNo").toString());
            model.setGswdIfMed(data.get("ProIsMed").toString());

            model.setGswdCreDate(DateUtil.getCurrentDate());
            model.setGswdCreTime(DateUtil.dateToString(new Date(),"HHmmss"));
            wtpsdDList.add(model);
        }
        if(!wtpsdDList.isEmpty()){

            List<GaiaSdWTSTO> stoList=gaiaSdWTSTOMapper.selectAll();

            Map<String, List<GaiaSdWtpsdD>> collect = wtpsdDList.stream().collect(Collectors.groupingBy(GaiaSdWtpsdD::getGswdVoucherId));
            for (String voucherId :collect.keySet() ){
                GaiaSdWtpdsH gaiaSdWtpdsH=new GaiaSdWtpdsH();
                List<GaiaSdWtpsdD> dList=collect.get(voucherId);
                gaiaSdWtpdsH.setClient(dList.get(0).getClient());
                gaiaSdWtpdsH.setGswhSupId(dList.get(0).getGswdSupId());
                gaiaSdWtpdsH.setGswhVoucherId(voucherId);
                gaiaSdWtpdsH.setGswhDate(dList.get(0).getGswdDate());
                gaiaSdWtpdsH.setGswhDsfBrId(dList.get(0).getGswdDsfBrId());
                switch (franchisee){
                    case "10000042":
                        Optional<GaiaSdWTSTO> sto= stoList.stream().filter(s->s.getStoDsfSite().equals(dList.get(0).getGswdDsfBrId())).findFirst();
                        if(sto!=null){
                            gaiaSdWtpdsH.setGswhBrId(sto.get().getStoSite());
                        }else{
                            gaiaSdWtpdsH.setGswhBrId("");
                        }
                        gaiaSdWtpdsH.setGswhCreEMP("紫石写入");
                        break;
                    case "10000098":
                        gaiaSdWtpdsH.setGswhBrId(dList.get(0).getGswdDsfBrId());
                        gaiaSdWtpdsH.setGswhCreEMP("蓬晖写入");
                        break;
                }
                gaiaSdWtpdsH.setGswhAMT(dList.stream().map(GaiaSdWtpsdD::getGswdAMT).reduce(BigDecimal::add).get());
                gaiaSdWtpdsH.setGswhQTY(dList.stream().map(GaiaSdWtpsdD::getGswdQTY).reduce(BigDecimal::add).get());
                gaiaSdWtpdsH.setGswhRemarks(dList.get(0).getGswdRemarks());
                gaiaSdWtpdsH.setGswhCreDate(DateUtil.getCurrentDate());
                gaiaSdWtpdsH.setGswhCreTime(DateUtil.dateToString(new Date(),"HHmmss"));
                gaiaSdWtpdsH.setGswhStatus("0");
                wtpdsHList.add(gaiaSdWtpdsH);
            }
        }
    }
    @Transactional
    public Boolean addWtps(List<GaiaSdWtpsdD> gaiaSdWtpsdDList,List<GaiaSdWtpdsH> wtpdsHList){
        for (GaiaSdWtpdsH h:wtpdsHList){
            gaiaSdWtpdsHMapper.insert(h);
        }
        for (GaiaSdWtpsdD d:gaiaSdWtpsdDList){
            gaiaSdWtpsdDMapper.insert(d);
        }
        return  true;
    }

    private  JsonResult checkRequest(Map<String,Object> inData){
        if(StringUtils.isEmpty(inData.get("Token"))||!"4912A5DFB3C54649BED6C20DF2D82997".equals(inData.get("Token"))){
            return JsonResult.fail(401,"未授权");
        }
        List<Map<String,Object>> requestData=(List<Map<String,Object>>)inData.get("Data");
        if(requestData==null||requestData.isEmpty()){
            return JsonResult.fail(500,"传入参数不正确");
        }
        return JsonResult.success(requestData,"");
    }

    @Override
    public JsonResult addDSFZSStoreInfo(Map<String,Object> inData){
        JsonResult checkResult=checkRequest(inData);
        if(checkResult.getCode()!=0){return checkResult;}
//        //查询采芝灵加盟商id
//        List<GaiaFranchisee> franchisee=gaiaFranchiseeMapper.getAllByName("采芝灵");
//        if(franchisee ==null || franchisee.size()==0){
//            return JsonResult.fail(500,"未找到加盟商");
//        }

        Boolean addFlg=addDsfPro("10000042",(List<Map<String,Object>>)checkResult.getData());

        if (addFlg){
            return  JsonResult.success("","写入成功");
        }else{
            return JsonResult.fail(500,"写入失败");
        }
    }

    private Boolean addDsfPro(String franchisee, List<Map<String,Object>> inData) {

        List<String> oldDsfProCodeList = gaiaSdWtproMapper.getProSelfCodeList(franchisee);
        List<String> oldDsfStoreCodeList = gaiaSdDsfStockMapper.getProIdCodeList(franchisee);

        for (Map<String, Object> data : inData) {
            try {
                GaiaSdWtpro proModel = new GaiaSdWtpro();
                proModel.setClient(franchisee);
                proModel.setProSelfCode(data.get("GoodsCode").toString());
                proModel.setProDsfCode(data.get("spbh").toString());
                proModel.setProDsfName(data.get("GoodsName").toString());
                proModel.setProNm(data.get("czlspid").toString());
                if (oldDsfProCodeList.contains(proModel.getProDsfCode())) {
                    //修改
                    gaiaSdWtproMapper.updateByproCode(proModel);
                } else {
                    //新增
                    gaiaSdWtproMapper.insert(proModel);
                }

                GaiaSdDsfStock stockModel = new GaiaSdDsfStock();
                stockModel.setClient(franchisee);
                stockModel.setSupId("GYS001");
                stockModel.setProIdDsf(data.get("spbh").toString());
                stockModel.setProId(data.get("GoodsCode").toString());
                stockModel.setPrice(StringUtils.isEmpty(data.get("hshjj").toString()) ? BigDecimal.ZERO : new BigDecimal(data.get("hshjj").toString()));
                stockModel.setQty(StringUtils.isEmpty(data.get("shl").toString()) ? BigDecimal.ZERO : new BigDecimal(data.get("shl").toString()));
                if (oldDsfStoreCodeList.contains(stockModel.getProIdDsf())) {
                    //修改
                    gaiaSdDsfStockMapper.updateByDsfId(stockModel);
                } else {
                    //新增
                    gaiaSdDsfStockMapper.insert(stockModel);
                }
            } catch (Exception ex) {
                log.error("同步紫石商品错误："+ex.toString());
            }
        }
        return true;
    }

    private Map<String, Object> checkContent(Map<String, Object> inData) {
        Map<String, Object> result = new HashMap<>();
        Map<String,Object> msg = new HashMap<>();
        long setTime = 6*60L;
        Long time = System.currentTimeMillis()/1000;
        Long timestamp = Long.parseLong(inData.get("timestamp").toString())/1000;
        //校验时间
        if (abs(timestamp - time) <= setTime) {
            msg.put("errno", 1003);
            msg.put("error", "请求时间误差太大");
            msg.put("data", "");
            return msg;
        }
        //根据appId 查询 APP_SECRET
        String secret = apiMapper.selectPartnerSecret(inData.get("source").toString());
        if(StrUtil.isEmpty(secret)){
            msg.put("errno", 1005);
            msg.put("error", "appId不合法");
            msg.put("data", "");
            return msg;
        }
        String storeCode = JSONObject.parseObject(inData.get("body").toString()).get("storeCode").toString();
        if (ObjectUtils.isEmpty(storeCode)) {
            msg.put("errno", 1001);
            msg.put("error", "关键参数门店编码未提交");
            msg.put("data", "");
            return msg;
        }
        String key = inData.get("source").toString() + "-" + inData.get("cmd") + "-" + storeCode;
        boolean r = redisManager.hasKey(key);
        if (r) {
            msg.put("errno", 1004);
            msg.put("error", "请求频次超过阈值");
            msg.put("data", "");
            return msg;
        }
        Map<String, String> secondMap = apiMapper.selectPartnerParam(inData.get("source").toString(), inData.get("cmd").toString());
        boolean f = SignErBaiUtil.checkSign(inData, inData.get("source").toString(), secret);
        if (f) {
            msg.put("errno", 1002);
            msg.put("error", "密钥不正确");
            msg.put("data", "");
            return msg;
        }

        msg.put("errno", 0);
        msg.put("error", "success");
        msg.put("data", "");
        return msg;
    }

    private Map<String,Object> billCommit(Map<String, Object> inData,Map<String, Object> msg) {
        String storeCode = JSONObject.parseObject(inData.get("body").toString()).get("storeCode").toString();
//        Map<String, String> storeInfo = apiMapper.selectPartnerStoInfo(inData.get("source").toString(), storeCode);
//        List<Map<String, Object>> proStockList = apiMapper.selectProStockCode(storeInfo.get("clientId"), storeInfo.get("stoCode"));
//        msg.put("errno", 0);
//        msg.put("error", "success");
//        msg.put("data", proStockList);
//        //请求成功加入缓存
//        long l =  new BigDecimal(secondMap.get("requestSecond")).longValue();
//        redisManager.set(key,secondMap.get("appid"),l);

        Map<String, Object> result = getResult(inData, msg);
        return result;
    }

    private Map<String, Object> getResult(Map<String, Object> inData,Map<String,Object> msg){
        Map<String, Object> result = new HashMap<>();
        result.put("body", msg);
        result.put("cmd", inData.get("cmd"));
        result.put("encrypt", "");
        result.put("source", inData.get("source"));
        result.put("ticket", inData.get("ticket"));
        result.put("timestamp", inData.get("timestamp"));
        result.put("version", inData.get("version"));
        result = sortMapByKey(result);
        String sign = SignErBaiUtil.getSign1(result);
        result.put("sign", sign);
        return result;
    }

    @Override
    public Map<String,Object> billCommit(Map<String,Object> inData){
        Map<String, Object> result = new HashMap<>();
        Map<String,Object> msg = new HashMap<>();
        Long setTime = 6*6000l;
        Long time = new Date().getTime()/1000;
        Long timestamp  = Long.valueOf(inData.get("timestamp").toString())/1000;
        if (abs(timestamp - time) <= setTime) {
            String secret = apiMapper.selectPartnerSecret(inData.get("source").toString());
            String storeCode = JSONObject.parseObject(inData.get("body").toString()).get("storeCode").toString();
            if (ObjectUtils.isEmpty(storeCode)) {
                msg.put("errno", 1001);
                msg.put("error", "关键参数门店编码未提交");
                msg.put("data", "");
            } else {
                String key = inData.get("source").toString() + "-" + inData.get("cmd") + "-" + storeCode;
                boolean r = redisManager.hasKey(key);
                if (r){
                    msg.put("errno", 1004);
                    msg.put("error", "请求频次超过阈值");
                    msg.put("data", "");
                }else {
                    Map<String, String> secondMap = apiMapper.selectPartnerParam(inData.get("source").toString(), inData.get("cmd").toString());
                    HebBillInData aa = JSONObject.parseObject(inData.get("body").toString(),HebBillInData.class);

                    inData.put("body",JSON.parseObject(JSON.toJSONString(aa),Feature.OrderedField));
                    boolean f = SignErBaiUtil.checkSign2(inData, inData.get("source").toString(), secret);
                    if (f) {
                        Map<String, String> storeInfo = apiMapper.selectPartnerStoInfo(inData.get("source").toString(), storeCode);
                        Map<String, Object> body = (Map<String,Object>)inData.get("body");

                        List<HebBillData> billList = JSONObject.parseArray(body.get("billList").toString(),HebBillData.class);
                        List<Map<String, Object>> ngBillList = new ArrayList<>();
                        for (HebBillData hebBillData : billList) {
                            Map<String, Object> resultMap = hebSaleBillService.payCommit(hebBillData);
                            if(ObjectUtil.isNotEmpty(resultMap)) {
                                ngBillList.add(resultMap);
                            }
                            if(ObjectUtil.isNotEmpty(resultMap.get("materialList"))) {
                                payService.insertMaterialDoc((List<MaterialDocRequestDto>) resultMap.get("materialList"));
                            }
                            resultMap.clear();
                        }

                        msg.put("errno", 0);
                        msg.put("error", "success");
                        msg.put("data", ngBillList);
                        //请求成功加入缓存
                        long l =  new BigDecimal(secondMap.get("requestSecond")).longValue();
                        redisManager.set(key,secondMap.get("appid"),l);
                    } else {
                        msg.put("errno", 1002);
                        msg.put("error", "密钥不正确");
                        msg.put("data", "");
                    }
                }
            }
        }else {
            msg.put("errno", 1003);
            msg.put("error", "请求时间误差太大");
            msg.put("data", "");
        }
        result.put("body", msg);
        result.put("cmd", inData.get("cmd"));
        result.put("encrypt", "");
//        result.put("secret",secret);
        result.put("source", inData.get("source"));
        result.put("ticket", inData.get("ticket"));
//        result.put("timestamp",new Date().getTime());
        result.put("timestamp", inData.get("timestamp"));
        result.put("version", inData.get("version"));
        result = sortMapByKey(result);
        String sign = SignErBaiUtil.getSign1(result);
        result.put("sign", sign);
        return result;
    }


    public static void main(String[] args) {
        Map<String,Object> inData=new HashMap<>();
        String aa = "{\"cmd\":\" HebYbBillCommit\",\"version\":\"1.0\",\"timestamp\":1631258744956,\"ticket\":\"9c333db9-ce9b-4316-82c4-e6f83e0eec9a\",\"source\":\"1000001310001\",\"encrypt\":\"\",\"body\":{\"storeCode\":\"cre6yl2o\",\"billList\":[{\"saleH\":{\"client\":\"100050\",\"billNo\":\"SD2109081Q4XIF4LV351\",\"brId\":\"1000000001\",\"date\":\"20210908\",\"time\":\"153000\",\"emp\":\"张三\",\"hykNo\":\"1002104\",\"normalAmt\":30.0000,\"zkAmt\":2.0000,\"ysAmt\":28.0000,\"integralAdd\":30.00,\"billNoReturn\":\"\"},\"saleDList\":[{\"billNo\":\"SD2109081Q4XIF4LV351\",\"client\":\"100050\",\"brId\":\"1000000001\",\"date\":\"20210908\",\"serial\":\"1\",\"proId\":\"100025\",\"batchNo\":\"20210908\",\"batch\":\"0014452\",\"validDate\":\"20211230\",\"stCode\":\"10005112\",\"prc1\":30.0000,\"prc2\":28.0000,\"qty\":1.0000,\"amt\":28.0000,\"zkAmt\":2.0000,\"salerId\":\"10022\",\"doctorId\":\"\"}],\"payMsgList\":[{\"client\":\"100050\",\"brId\":\"1000000001\",\"date\":\"20210908\",\"billNo\":\"SD2109081Q4XIF4LV351\",\"id\":\"1000\",\"type\":\"1\",\"name\":\"现金\",\"cardNo\":\"\",\"amt\":30.0000,\"rmbAmt\":28.0000,\"zlAmt\":2.0000}]}]},\"sign\":\"23D72344E24E3092906AB52AE53B422D\"}";
        System.out.println("1");
//        JSONObject jsonOrdered = JSONObject.parseObject(aa, Feature.OrderedField);
        HebBillInData body = JSON.parseObject(JSON.parseObject(aa).getString("body"),HebBillInData.class);
        List<HebBillData> list = JSON.parseObject(JSON.toJSONString(body.getBillList()), new TypeReference<List<HebBillData>>() {}.getType(), Feature.OrderedField);
        for (HebBillData hebBillData : list) {
            hebBillData.setPayMsgList(JSON.parseObject(JSON.toJSONString(hebBillData.getPayMsgList()), new TypeReference<List<PayMsg>>() {}.getType(), Feature.OrderedField));
            hebBillData.setSaleDList(JSON.parseObject(JSON.toJSONString(hebBillData.getSaleDList()), new TypeReference<List<SaleD>>() {}.getType(), Feature.OrderedField));
            hebBillData.setSaleH(JSON.parseObject(JSON.toJSONString(hebBillData.getSaleH()), new TypeReference<SaleH>() {}.getType(), Feature.OrderedField));
        }
        body.setBillList(list);
        inData.put("body", body);
        System.out.print(inData);
    }
}
