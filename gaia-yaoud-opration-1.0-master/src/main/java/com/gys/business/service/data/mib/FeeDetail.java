package com.gys.business.service.data.mib;

import lombok.Data;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/12/28 13:12
 */
@Data
public class FeeDetail {
    private String item;
    private String fee;
}
