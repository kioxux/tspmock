package com.gys.business.service.goodspurchase.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaProductBasicMapper;
import com.gys.business.mapper.GaiaProductBusinessMapper;
import com.gys.business.mapper.GaiaTaxCodeMapper;
import com.gys.business.mapper.entity.GaiaProductBasic;
import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.business.mapper.entity.GaiaTaxCode;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.data.productBasic.GetProductBasicOutData;
import com.gys.business.service.goodspurchase.ProductRelationMaintainService;
import com.gys.common.annotation.ParseEnumVal;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.data.goodspurchase.*;
import com.gys.common.enums.BaseEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.csv.CsvUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.weekend.Weekend;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wu mao yin
 * @Title: 客户商品对应关系查询及维护
 * @date 2021/10/2717:07
 */
@Slf4j
@Service
public class ProductRelationMaintainServiceImpl implements ProductRelationMaintainService {

    @Resource
    private GaiaTaxCodeMapper gaiaTaxCodeMapper;

    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Override
    public JsonResult selectCustomerClientList() {
        return JsonResult.success(gaiaProductBusinessMapper.selectCustomerClientList());
    }

    @Override
    public JsonResult selectCustomerProSiteList(String client) {
        if (StringUtils.isBlank(client)) {
            return JsonResult.success(Collections.emptyList());
        }
        // 门店列表
        List<Map<String, String>> maps = gaiaProductBusinessMapper.selectCustomerProSiteList(client);
        // 仓库列表
        List<Map<String, String>> repoSiteList = gaiaProductBusinessMapper.selectRepoSiteList(client);
        if (CollectionUtils.isNotEmpty(repoSiteList)) {
            maps.addAll(repoSiteList);
        }
        return JsonResult.success(maps);
    }

    @Override
    public JsonResult selectCustomerProCodeList(GetProductBasicInData getProductBasicInData) {
        String queryString = getProductBasicInData.getQueryString();
        if (StringUtils.isNotBlank(queryString)) {
            getProductBasicInData.setQueryStringList(Arrays.asList(queryString.split(StrUtil.COMMA)));
        }
        PageHelper.startPage(getProductBasicInData.getPageNum(), getProductBasicInData.getPageSize());
        List<GetProductBasicOutData> getProductBasicOutDataList = gaiaProductBusinessMapper.selectCustomerProCodeList(getProductBasicInData);
        return JsonResult.success(new PageInfo<>(getProductBasicOutDataList));
    }

    @Override
    public JsonResult selectProductRelationMaintainList(ProductRelationMaintainListDTO productRelationMaintainListDTO) {
        String clients = productRelationMaintainListDTO.getClients();
        PageHelper.startPage(productRelationMaintainListDTO.getPageNum(), productRelationMaintainListDTO.getPageSize());
        List<ProductRelationMaintainListVO> productRelationMaintainList = gaiaProductBusinessMapper.selectProductRelationMaintainList(productRelationMaintainListDTO);
        return JsonResult.success(new PageInfo<>(productRelationMaintainList));
    }

    @Override
    public void exportProductRelationMaintainList(ProductRelationMaintainListDTO productRelationMaintainListDTO, HttpServletResponse response) {
        productRelationMaintainListDTO.setPageNum(null);
        productRelationMaintainListDTO.setPageSize(null);
        LinkedList<String> fieldSet = Optional.ofNullable(productRelationMaintainListDTO.getFieldSet()).orElse(new LinkedList<>());
        if (CollectionUtils.isEmpty(fieldSet)) {
            throw new BusinessException("请选择需要导出的列!");
        }

        // 标题头
        Class<ProductRelationMaintainListVO> productRelationMaintainClass = ProductRelationMaintainListVO.class;
        Field[] declaredFields1 = productRelationMaintainClass.getDeclaredFields();
        String[] headList = new String[fieldSet.size()];
        int index = 0;
        for (Field field : declaredFields1) {
            ApiModelProperty annotation = field.getAnnotation(ApiModelProperty.class);
            String name = field.getName();
            if (annotation != null && CollectionUtils.isNotEmpty(fieldSet) && fieldSet.contains(name)) {
                headList[index] = annotation.value();
                index++;
            }
        }

        // 进项销项税
        List<GaiaTaxCode> gaiaTaxCodes = gaiaTaxCodeMapper.selectAll();
        HashMap<String, String> taxCodeCacheMap = new HashMap<>();
        for (GaiaTaxCode gaiaTaxCode : gaiaTaxCodes) {
            taxCodeCacheMap.put(gaiaTaxCode.getTaxCode(), gaiaTaxCode.getTaxCodeValue());
        }
        String fileName = "客户商品关系维护";
        try {
            CsvUtil.exportCsvToWebsite(response, 200000, fileName, headList, (writer, currentPage, pageSize) -> {
                int dataLength = pageSize;
                while (dataLength == pageSize) {
                    PageHelper.startPage(currentPage, pageSize, false);
                    List<ProductRelationMaintainListVO> productRelationMaintainList = gaiaProductBusinessMapper.selectProductRelationMaintainList(productRelationMaintainListDTO);
                    dataLength = productRelationMaintainList.size();
                    if (CollectionUtils.isNotEmpty(productRelationMaintainList)) {
                        for (ProductRelationMaintainListVO productRelationMaintainListVO : productRelationMaintainList) {
                            List<Field> declaredFields = Arrays.stream(productRelationMaintainListVO.getClass().getDeclaredFields())
                                    .filter(item -> fieldSet.contains(item.getName()))
                                    .collect(Collectors.toList());
                            StringJoiner stringJoiner = new StringJoiner(StrUtil.COMMA);
                            for (Field declaredField : declaredFields) {
                                // 批准文号分类 处方类别 保质期单位 生产类别 贮存条件 商品仓储分区 品牌区分 管制特殊分类 状态
                                String name = declaredField.getName();
                                declaredField.setAccessible(true);
                                Object obj;
                                try {
                                    obj = Optional.ofNullable(declaredField.get(productRelationMaintainListVO)).orElse(StrUtil.EMPTY);
                                } catch (Exception e) {
                                    obj = StrUtil.EMPTY;
                                }
                                String val = (String) obj;
                                ParseEnumVal parseEnumVal = declaredField.getAnnotation(ParseEnumVal.class);
                                if (parseEnumVal != null) {
                                    Class<? extends BaseEnum> aClass = parseEnumVal.enumVal();
                                    if (aClass.isEnum()) {
                                        String finalVal = val;
                                        val = Arrays.stream(aClass.getEnumConstants())
                                                .filter(item -> item.getType().equals(finalVal))
                                                .map(BaseEnum::getName).findFirst().orElse(StrUtil.EMPTY);
                                    }
                                }
                                if ("ydProInputTax".equals(name) || "ydProOutputTax".equals(name)) {
                                    val = val + "-" + taxCodeCacheMap.get(val);
                                }
                                if ("ydProStatus".equals(name)) {
                                    if (StringUtils.isNotBlank(val)) {
                                        if ("0".equals(val)) {
                                            val = "可用";
                                        }
                                        if ("1".equals(val)) {
                                            val = "不可用";
                                        }
                                    }
                                }
                                if (val.contains(StrUtil.COMMA)) {
                                    val = "\"" + val + "\"";
                                }
                                stringJoiner.add(val + "\t");
                            }
                            stringJoiner.add("\r\n");
                            writer.print(stringJoiner);
                        }
                        writer.flush();
                        currentPage++;
                    }
                }
            });
        } catch (Exception e) {
            log.error("exportProductRelationMaintainList, {}", e.getMessage());
            throw new BusinessException("导出失败!");
        }
    }

    @Override
    public JsonResult selectProductCodeInfo(AddYdProductCodeInfo addYdProductCodeInfo) {
        String ydProCode = addYdProductCodeInfo.getYdProCode();
        GetProductBasicOutData getProductBasicOutData;

        // 药德编码为 99999999 时查询客户商品
        if ("99999999".equals(ydProCode)) {
            ydProCode = null;
        }

        if (StringUtils.isNotBlank(ydProCode)) {
            // 获取药德商品编码信息
            getProductBasicOutData = gaiaProductBasicMapper.getOne(ydProCode);
        } else {
            // 获取客户商品编码信息
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(addYdProductCodeInfo.getClient());
            gaiaProductBusiness.setProSite(addYdProductCodeInfo.getProSite());
            gaiaProductBusiness.setProSelfCode(addYdProductCodeInfo.getProCode());
            gaiaProductBusiness = gaiaProductBusinessMapper.selectOne(gaiaProductBusiness);
            getProductBasicOutData = new GetProductBasicOutData();
            BeanUtil.copyProperties(gaiaProductBusiness, getProductBasicOutData);
            getProductBasicOutData.setProCommonName(gaiaProductBusiness.getProCommonname());
            getProductBasicOutData.setProPartForm(gaiaProductBusiness.getProPartform());
            getProductBasicOutData.setProMinDose(gaiaProductBusiness.getProMindose());
            getProductBasicOutData.setProBarCode(gaiaProductBusiness.getProBarcode());
            getProductBasicOutData.setProBarCode2(gaiaProductBusiness.getProBarcode2());
            getProductBasicOutData.setProTotalDose(gaiaProductBusiness.getProTotaldose());
            getProductBasicOutData.setProCompClass(gaiaProductBusiness.getProCompclass());
            getProductBasicOutData.setProCompClassName(gaiaProductBusiness.getProCompclassName());
            getProductBasicOutData.setProPresClass(gaiaProductBusiness.getProPresclass());
            getProductBasicOutData.setProMedProDCT(gaiaProductBusiness.getProMedProdct());
            getProductBasicOutData.setProMedProDCTCode(gaiaProductBusiness.getProMedProdctcode());
            getProductBasicOutData.setProRegisterExDate(gaiaProductBusiness.getProRegisterExdate());
            getProductBasicOutData.setProMedListNum(gaiaProductBusiness.getProMedListnum());
            getProductBasicOutData.setProMedListName(gaiaProductBusiness.getProMedListname());
            getProductBasicOutData.setProMedListForm(gaiaProductBusiness.getProMedListform());
        }
        return JsonResult.success(getProductBasicOutData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult saveYdProCode(AddYdProductCodeInfo addYdProductCodeInfo) {
        String ydProCode = addYdProductCodeInfo.getYdProCode();
        String productCode;
        int row;
        ProductBasicData productBasicData = addYdProductCodeInfo.getGaiaProductBasic();
        if (StringUtils.isNotBlank(ydProCode)) {
            // 验证药德编码正确性
            if (productBasicData == null || gaiaProductBasicMapper.selectByPrimaryKey(ydProCode) == null) {
                throw new BusinessException("药德商品编码不存在");
            }
            // 更新药德商品编码信息
            productCode = ydProCode;

            // 药德编码为 99999999 时修改客户商品
            if ("99999999".equals(ydProCode)) {
                Weekend<GaiaProductBusiness> gaiaProductBusinessWeekend = new Weekend<>(GaiaProductBusiness.class);
                gaiaProductBusinessWeekend.weekendCriteria()
                        .andEqualTo(GaiaProductBusiness::getClient, addYdProductCodeInfo.getClient())
                        .andEqualTo(GaiaProductBusiness::getProSite, addYdProductCodeInfo.getProSite())
                        .andEqualTo(GaiaProductBusiness::getProSelfCode, addYdProductCodeInfo.getProCode());
                GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
                BeanUtil.copyProperties(productBasicData, gaiaProductBusiness);
                gaiaProductBusiness.setProCommonname(productBasicData.getProCommonName());
                gaiaProductBusiness.setProPartform(productBasicData.getProPartform());
                gaiaProductBusiness.setProMindose(productBasicData.getProMinDose());
                gaiaProductBusiness.setProTotaldose(productBasicData.getProTotalDose());
                gaiaProductBusiness.setProCompclass(productBasicData.getProCompClass());
                gaiaProductBusiness.setProCompclassName(productBasicData.getProCompClassName());
                gaiaProductBusiness.setProPresclass(productBasicData.getProPresClass());
                gaiaProductBusiness.setProMedProdct(productBasicData.getProMedProDCT());
                gaiaProductBusiness.setProMedProdctcode(productBasicData.getProMedProDCTCode());
                gaiaProductBusiness.setProRegisterExdate(productBasicData.getProRegisterExDate());
                gaiaProductBusiness.setProMedListnum(productBasicData.getProMedListNum());
                gaiaProductBusiness.setProMedListname(productBasicData.getProMedListName());
                gaiaProductBusiness.setProMedListform(productBasicData.getProMedListForm());
                gaiaProductBusiness.setProSelfCode(null);
                gaiaProductBusiness.setProCode(null);
                gaiaProductBusiness.setProSite(null);
                gaiaProductBusiness.setClient(null);
                // 置上修改人修改时间
                gaiaProductBusiness.setUpdateClient(productBasicData.getUpdateClient());
                gaiaProductBusiness.setUpdateUser(productBasicData.getUpdater());
                gaiaProductBusiness.setUpdateTime(productBasicData.getUpdateTime());
                String proCompClass = gaiaProductBusiness.getProCompclass();
                String proCompClassName = gaiaProductBusiness.getProCompclassName();
                if (!proCompClassName.startsWith(proCompClass)) {
                    gaiaProductBusiness.setProCompclassName(proCompClass + proCompClassName);
                }
                gaiaProductBusinessMapper.updateByExampleSelective(gaiaProductBusiness, gaiaProductBusinessWeekend);
            } else {
                GaiaProductBasic gaiaProductBasic = new GaiaProductBasic();
                BeanUtil.copyProperties(productBasicData, gaiaProductBasic);
                gaiaProductBasic.setProMedProdct(productBasicData.getProMedProDCT());
                gaiaProductBasic.setProMedProdctcode(productBasicData.getProMedProDCTCode());
                gaiaProductBasic.setProCommonname(productBasicData.getProCommonName());
                gaiaProductBasic.setProMindose(productBasicData.getProMinDose());
                gaiaProductBasic.setProTotaldose(productBasicData.getProTotalDose());
                gaiaProductBasic.setProRegisterExdate(productBasicData.getProRegisterExDate());
                gaiaProductBasic.setProCompclass(productBasicData.getProCompClass());
                gaiaProductBasic.setProCompclassName(productBasicData.getProCompClassName());
                gaiaProductBasic.setProPresclass(productBasicData.getProPresClass());
                gaiaProductBasic.setProCode(ydProCode);
                String proCompClass = gaiaProductBasic.getProCompclass();
                String proCompClassName = gaiaProductBasic.getProCompclassName();
                if (!proCompClassName.startsWith(proCompClass)) {
                    gaiaProductBasic.setProCompclassName(proCompClass + proCompClassName);
                }
                // 置上修改人修改时间
                gaiaProductBasic.setUpdateClient(productBasicData.getUpdateClient());
                gaiaProductBasic.setUpdateUser(productBasicData.getUpdater());
                gaiaProductBasic.setUpdateTime(productBasicData.getUpdateTime());
                gaiaProductBasicMapper.updateByPrimaryKeySelective(gaiaProductBasic);
            }
        } else {
            String lastProductCode = gaiaProductBasicMapper.selectLastProductCode();
            // 生成最新药德商品编码
            productCode = String.valueOf(Long.parseLong(lastProductCode) + 1);
            GaiaProductBasic gaiaProductBasic = new GaiaProductBasic();
            BeanUtil.copyProperties(productBasicData, gaiaProductBasic);
            gaiaProductBasic.setProMedProdct(productBasicData.getProMedProDCT());
            gaiaProductBasic.setProMedProdctcode(productBasicData.getProMedProDCTCode());
            gaiaProductBasic.setProCommonname(productBasicData.getProCommonName());
            gaiaProductBasic.setProMindose(productBasicData.getProMinDose());
            gaiaProductBasic.setProTotaldose(productBasicData.getProTotalDose());
            gaiaProductBasic.setProRegisterExdate(productBasicData.getProRegisterExDate());
            gaiaProductBasic.setProCompclass(productBasicData.getProCompClass());
            gaiaProductBasic.setProCompclassName(productBasicData.getProCompClassName());
            gaiaProductBasic.setProPresclass(productBasicData.getProPresClass());
            String proCompClass = gaiaProductBasic.getProCompclass();
            String proCompClassName = gaiaProductBasic.getProCompclassName();
            if (!proCompClassName.startsWith(proCompClass)) {
                gaiaProductBasic.setProCompclassName(proCompClass + proCompClassName);
            }
            gaiaProductBasic.setProCode(productCode);
            // 置上修改人修改时间
            gaiaProductBasic.setCreateClient(productBasicData.getUpdateClient());
            gaiaProductBasic.setCreateUser(productBasicData.getUpdater());
            gaiaProductBasic.setCreateTime(productBasicData.getUpdateTime());
            gaiaProductBasic.setUpdateClient(productBasicData.getUpdateClient());
            gaiaProductBasic.setUpdateUser(productBasicData.getUpdater());
            gaiaProductBasic.setUpdateTime(productBasicData.getUpdateTime());
            // 新增药德商品编码信息
            row = gaiaProductBasicMapper.insertSelective(gaiaProductBasic);
            if (row == 0) {
                return JsonResult.error("新建药德编码失败!");
            }
        }

        // 根据地点拉取门店信息，修改对应门店下所有商品
        // 生成的对应关系应同步至该仓库对应的所有门店的商品信息中
        String client = addYdProductCodeInfo.getClient();
        String proSite = addYdProductCodeInfo.getProSite();
        String proSelfCode = addYdProductCodeInfo.getProCode();
        List<ProductRelationMaintainListVO> productByRepoCodes = gaiaProductBusinessMapper.selectProductByRepoCode(client, proSite, proSelfCode);

        if (CollectionUtils.isNotEmpty(productByRepoCodes)) {
            List<GaiaProductBusiness> gaiaProductBusinessList = new ArrayList<>();
            for (ProductRelationMaintainListVO productRelationMaintainListVO : productByRepoCodes) {
                // 将新的药德商品编码更新到客户商品编码表
                GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
                gaiaProductBusiness.setClient(productRelationMaintainListVO.getClient());
                gaiaProductBusiness.setProSite(productRelationMaintainListVO.getProSite());
                gaiaProductBusiness.setProSelfCode(productRelationMaintainListVO.getProSelfCode());
                gaiaProductBusiness.setProCode(productCode);
                gaiaProductBusinessList.add(gaiaProductBusiness);
                if (gaiaProductBusinessList.size() == 100) {
                    gaiaProductBusinessMapper.updateProductBusinessByProSelfCodeBatch(gaiaProductBusinessList,productBasicData.getUpdateClient(),productBasicData.getUpdater(),productBasicData.getUpdateTime());
                    gaiaProductBusinessList.clear();
                }
            }
            if (gaiaProductBusinessList.size() > 0) {
                gaiaProductBusinessMapper.updateProductBusinessByProSelfCodeBatch(gaiaProductBusinessList,productBasicData.getUpdateClient(),productBasicData.getUpdater(),productBasicData.getUpdateTime());
            }
        } else {
            // 将新的药德商品编码更新到客户商品编码表
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(client);
            gaiaProductBusiness.setProSite(proSite);
            gaiaProductBusiness.setProSelfCode(addYdProductCodeInfo.getProCode());
            gaiaProductBusiness.setProCode(productCode);
            // 置上修改人修改时间
            gaiaProductBusiness.setUpdateClient(productBasicData.getUpdateClient());
            gaiaProductBusiness.setUpdateUser(productBasicData.getUpdater());
            gaiaProductBusiness.setUpdateTime(productBasicData.getUpdateTime());
            gaiaProductBusinessMapper.updateProductBusinessByProSelfCode(gaiaProductBusiness);
        }
        return JsonResult.success();
    }

    /** add by jinwencheng on 2021-12-30 13:50:13 新建药德商品编码页面，新建前先查询是否有符合条件的商品 **/
    @Override
    public JsonResult getSameProduct(SearchSameProductDTO searchSameProductDTO) {
        return JsonResult.success(gaiaProductBusinessMapper.getSameProduct(searchSameProductDTO));
    }

    @Override
    public JsonResult confirmYdProCode(ConfirmYdProCodeDTO confirmYdProCodeDTO) {
        String productCode = confirmYdProCodeDTO.getYdProCode();
        if (gaiaProductBasicMapper.selectByPrimaryKey(productCode) == null) {
            throw new BusinessException("药德商品编码不存在");
        }
        String client = confirmYdProCodeDTO.getClient();
        String proSite = confirmYdProCodeDTO.getProSite();
        String proSelfCode = confirmYdProCodeDTO.getProCode();
        List<ProductRelationMaintainListVO> productByRepoCodes = gaiaProductBusinessMapper.selectProductByRepoCode(client, proSite, proSelfCode);

        if (CollectionUtils.isNotEmpty(productByRepoCodes)) {
            List<GaiaProductBusiness> gaiaProductBusinessList = new ArrayList<>();
            for (ProductRelationMaintainListVO productRelationMaintainListVO : productByRepoCodes) {
                // 将新的药德商品编码更新到客户商品编码表
                GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
                gaiaProductBusiness.setClient(productRelationMaintainListVO.getClient());
                gaiaProductBusiness.setProSite(productRelationMaintainListVO.getProSite());
                gaiaProductBusiness.setProSelfCode(productRelationMaintainListVO.getProSelfCode());
                gaiaProductBusiness.setProCode(productCode);
                // 置上修改人修改时间
                gaiaProductBusiness.setUpdateClient(confirmYdProCodeDTO.getUpdateClient());
                gaiaProductBusiness.setUpdateUser(confirmYdProCodeDTO.getUpdater());
                gaiaProductBusiness.setUpdateTime(confirmYdProCodeDTO.getUpdateTime());
                gaiaProductBusinessList.add(gaiaProductBusiness);
                if (gaiaProductBusinessList.size() == 100) {
                    gaiaProductBusinessMapper.updateProductBusinessByProSelfCodeBatch(gaiaProductBusinessList,confirmYdProCodeDTO.getUpdateClient(),confirmYdProCodeDTO.getUpdater(),confirmYdProCodeDTO.getUpdateTime());
                    gaiaProductBusinessList.clear();
                }
            }
            if (gaiaProductBusinessList.size() > 0) {
                gaiaProductBusinessMapper.updateProductBusinessByProSelfCodeBatch(gaiaProductBusinessList,confirmYdProCodeDTO.getUpdateClient(),confirmYdProCodeDTO.getUpdater(),confirmYdProCodeDTO.getUpdateTime());
            }
        } else {
            // 将新的药德商品编码更新到客户商品编码表
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(client);
            gaiaProductBusiness.setProSite(proSite);
            gaiaProductBusiness.setProSelfCode(productCode);
            gaiaProductBusiness.setProCode(productCode);
            // 置上修改人修改时间
            gaiaProductBusiness.setUpdateClient(confirmYdProCodeDTO.getUpdateClient());
            gaiaProductBusiness.setUpdateUser(confirmYdProCodeDTO.getUpdater());
            gaiaProductBusiness.setUpdateTime(confirmYdProCodeDTO.getUpdateTime());
            gaiaProductBusinessMapper.updateProductBusinessByProSelfCode(gaiaProductBusiness);
        }
        return JsonResult.success();
    }
    /** add end **/

}
