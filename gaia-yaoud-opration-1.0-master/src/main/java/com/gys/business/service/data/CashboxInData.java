package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
public class CashboxInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gpdhBrId;
    @ApiModelProperty(value = "用户id")
    private String userId = "";
    @ApiModelProperty(value = "用户密码" )
    private String userPassword = "";
    @ApiModelProperty(value = "钱箱号")
    private String roidId = "";
    @ApiModelProperty(value = "机台号")
    private String counterId = "";
    @ApiModelProperty(value = "上一班金额")
    private String lastShiftMoney;
    @ApiModelProperty(value = "剩余余额")
    private String balance;
    @ApiModelProperty(value = "钱箱金额")
    private String chashBoxMoney;
    @ApiModelProperty(value = "备用金")
    private String pettyCashB;

    @ApiModelProperty(value = "收银员编号")
    private String cashierId;
    @ApiModelProperty(value = "收银员名称")
    private String userName;
    @ApiModelProperty(value = "上一班时间")
    private Date lastShiftTime;
    @ApiModelProperty(value = "交班时间")
    private Date handoverTime;
    @ApiModelProperty(value = "交班金额")
    private String handoverAmount;
    @ApiModelProperty(value = "备注")
    private String remarks;


    @ApiModelProperty(value = "从时间")
    private String inDateTime;

    @ApiModelProperty(value = "到时间")
    private String toDateTime;

}
