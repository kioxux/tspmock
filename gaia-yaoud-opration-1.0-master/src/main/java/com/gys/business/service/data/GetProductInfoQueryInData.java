package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class GetProductInfoQueryInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "商品自编码 通用名称 商品名 助记码（模糊匹配）")
    private String gspgProId;
    @ApiModelProperty(value = "国家医保品种 0-否，1-是")
    private String proMedProdct;
    @ApiModelProperty(value = "是否拆零 1是0否")
    private String proIfpart;
    @ApiModelProperty(value = "地点")
    private String gspp_br_id;
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "")
    private String gspgId;
    @ApiModelProperty(value = "效期")
    private String vaildDate;
    @ApiModelProperty(value = "月份")
    private Integer month;
    @ApiModelProperty(value = "数量")
    private Integer stockQty;
    @ApiModelProperty(value = "区域")
    private String gsplArea;
    @ApiModelProperty(value = "柜组")
    private String gsplGroup;
    @ApiModelProperty(value = "货架")
    private String gsplShelf;
    @ApiModelProperty(value = "层级")
    private String gsplStorey;
    @ApiModelProperty(value = "货位")
    private String gsplSeat;
    @ApiModelProperty(value = "商品编码集合")

    private List<String> proList;

    @ApiModelProperty(value = "每页条数")
    public int pageSize;
    @ApiModelProperty(value = "第几页")
    private int pageNum;
}
