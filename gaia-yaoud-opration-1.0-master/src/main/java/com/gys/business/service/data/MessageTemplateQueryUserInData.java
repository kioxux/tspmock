package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MessageTemplateQueryUserInData implements Serializable {


    private static final long serialVersionUID = -7120740496991944747L;
    @ApiModelProperty(value = "门店人员是否显示毛利数据 Y表示打勾 N表示未打勾")
    private String content;

    private String site;

    private String client;

    private String mtId;

}
