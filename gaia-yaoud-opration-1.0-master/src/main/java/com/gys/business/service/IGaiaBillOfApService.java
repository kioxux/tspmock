package com.gys.business.service;

import com.gys.business.service.data.billOfAp.GaiaBillOfApDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApExcelDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApParam;
import com.gys.business.service.data.billOfAp.GaiaBillOfApResponse;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 供应商应付明细清单表 服务类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
public interface IGaiaBillOfApService {

    //获取分页列表
    PageInfo getListPage(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo);

    /**
     * 功能描述: 统计供应商应付
     *
     * @param day
     * @return void
     * @author wangQc
     * @date 2021/8/25 3:20 下午
     */
    void dailyInsert(LocalDate day) throws ParseException;


    /**
     * 功能描述: 手动录入
     *
     * @param userInfo
     * @param inData
     * @return java.lang.Object
     * @author wangQc
     * @date 2021/8/26 4:00 下午
     */
    Object save(GetLoginOutData userInfo, List<GaiaBillOfApDto> inData);

    /**
     * 功能描述: 批量导入
     *
     * @param userInfo
     * @param dataList
     * @return void
     * @author wangQc
     * @date 2021/8/27 9:51 上午
     */
    void batchImport(GetLoginOutData userInfo, List<GaiaBillOfApExcelDto> dataList);


    /**
     * 功能描述: 期初导入
     *
     * @param userInfo
     * @param dataList
     * @return void
     * @author wangQc
     * @date 2021/8/27 10:38 上午
     */
    void beginPeriodImport(GetLoginOutData userInfo, List<GaiaBillOfApExcelDto> dataList);


    /**
     * 功能描述: 导出列表
     *
     * @param gaiaBillOfApParam
     * @return com.gys.common.response.Result
     * @author wangQc
     * @date 2021/8/31 7:25 下午
     */
    SXSSFWorkbook listExport(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo);

    void supBillInsert(String day) throws ParseException;

    void paymentApplicationInsert(String day);


    PageInfo detailList(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo);


    /**
     * 功能描述: 供应商应付明细列表
     *
     * @param gaiaBillOfApParam
     * @return org.apache.poi.xssf.streaming.SXSSFWorkbook
     * @author wangQc
     * @date 2021/9/6 2:22 下午
     */
    SXSSFWorkbook detailListExport(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo);





    List<Map<String, Object>> supplierSalesman(GetLoginOutData userInfo, String depId, String stoCode,String supSelfCode, String name);



    //--------------------------------------------供应商业务员-------------------------------------------------------------//

    /**
     * 供应商业务员汇总列表
     * @param response 请求参数
     * @return
     */
    PageInfo assistantList(GaiaBillOfApResponse response);
    /**
     * 供应商业务员汇总列表导出
     * @param response 请求参数
     * @return
     */
    SXSSFWorkbook exportExcel(GaiaBillOfApResponse data, GetLoginOutData userInfo);

    void updateEndingBalance();

}
