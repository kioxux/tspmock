//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.entity.GaiaNewProductEvaluationH;
import com.gys.business.service.data.GaiaNewProductEvaluationInData;
import com.gys.business.service.data.GaiaNewProductEvaluationOutData;
import com.gys.business.service.data.GaiaProductEvaluationDetailOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.HashMap;
import java.util.List;

public interface GaiaNewProductEvaluationHService {

    void InsertGaiaNewProductEvaluation();

    GaiaNewProductEvaluationOutData getBillByClientAndStorId(GaiaNewProductEvaluationInData inData);

    GaiaNewProductEvaluationOutData getBillInfoByBillCode(GaiaNewProductEvaluationInData inData);

    GaiaNewProductEvaluationH updateDetailList(GaiaNewProductEvaluationOutData param, GetLoginOutData userInfo);

    PageInfo listBillInfo(GaiaNewProductEvaluationOutData param, GetLoginOutData userInfo);

    List<HashMap<String,String>> listBigClass(GaiaNewProductEvaluationOutData param);

    void updateBillStatus();

}
