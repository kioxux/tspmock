package com.gys.business.service.appservice;

import com.gys.business.service.data.GetMemberInData;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.GetQueryMemberOutData;

/**
 * @author xiaoyuan
 */
public interface MemberService {

    /**
     * 会员查询
     * @param inData
     * @return
     */
    GetQueryMemberOutData queryMember(GetQueryMemberInData inData);

    /**
     * 新增会员
     * @param inData
     * @return
     */
    boolean addMember(GetMemberInData inData);
}
