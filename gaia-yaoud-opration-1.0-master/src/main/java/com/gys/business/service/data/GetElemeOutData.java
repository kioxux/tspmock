//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetElemeOutData {
    private String appId;
    private String source;
    private String type;
    private String params;

    public GetElemeOutData() {
    }

    public String getAppId() {
        return this.appId;
    }

    public String getSource() {
        return this.source;
    }

    public String getType() {
        return this.type;
    }

    public String getParams() {
        return this.params;
    }

    public void setAppId(final String appId) {
        this.appId = appId;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setParams(final String params) {
        this.params = params;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetElemeOutData)) {
            return false;
        } else {
            GetElemeOutData other = (GetElemeOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$appId = this.getAppId();
                    Object other$appId = other.getAppId();
                    if (this$appId == null) {
                        if (other$appId == null) {
                            break label59;
                        }
                    } else if (this$appId.equals(other$appId)) {
                        break label59;
                    }

                    return false;
                }

                Object this$source = this.getSource();
                Object other$source = other.getSource();
                if (this$source == null) {
                    if (other$source != null) {
                        return false;
                    }
                } else if (!this$source.equals(other$source)) {
                    return false;
                }

                Object this$type = this.getType();
                Object other$type = other.getType();
                if (this$type == null) {
                    if (other$type != null) {
                        return false;
                    }
                } else if (!this$type.equals(other$type)) {
                    return false;
                }

                Object this$params = this.getParams();
                Object other$params = other.getParams();
                if (this$params == null) {
                    if (other$params != null) {
                        return false;
                    }
                } else if (!this$params.equals(other$params)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetElemeOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $appId = this.getAppId();
        result = result * 59 + ($appId == null ? 43 : $appId.hashCode());
        Object $source = this.getSource();
        result = result * 59 + ($source == null ? 43 : $source.hashCode());
        Object $type = this.getType();
        result = result * 59 + ($type == null ? 43 : $type.hashCode());
        Object $params = this.getParams();
        result = result * 59 + ($params == null ? 43 : $params.hashCode());
        return result;
    }

    public String toString() {
        return "GetElemeOutData(appId=" + this.getAppId() + ", source=" + this.getSource() + ", type=" + this.getType() + ", params=" + this.getParams() + ")";
    }
}
