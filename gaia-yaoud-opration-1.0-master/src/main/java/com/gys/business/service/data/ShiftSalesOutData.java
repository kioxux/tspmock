//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ShiftSalesOutData {
    private String clientId;
    private String billNo;
    private String brId;
    private String saleDate;
    private String saleShift;
    private String saleJobNum;
    private String saleTotalAmt;
    private String cash;
    private String gsshPaymentNo1;
    private String gsshPaymentNo2;
    private String gsshPaymentNo3;
    private String gsshPaymentNo4;
    private String gsshPaymentNo5;
    private String visitNum;
    private String visitUnitPrice;
    private Integer index;

    public ShiftSalesOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBillNo() {
        return this.billNo;
    }

    public String getBrId() {
        return this.brId;
    }

    public String getSaleDate() {
        return this.saleDate;
    }

    public String getSaleShift() {
        return this.saleShift;
    }

    public String getSaleJobNum() {
        return this.saleJobNum;
    }

    public String getSaleTotalAmt() {
        return this.saleTotalAmt;
    }

    public String getCash() {
        return this.cash;
    }

    public String getGsshPaymentNo1() {
        return this.gsshPaymentNo1;
    }

    public String getGsshPaymentNo2() {
        return this.gsshPaymentNo2;
    }

    public String getGsshPaymentNo3() {
        return this.gsshPaymentNo3;
    }

    public String getGsshPaymentNo4() {
        return this.gsshPaymentNo4;
    }

    public String getGsshPaymentNo5() {
        return this.gsshPaymentNo5;
    }

    public String getVisitNum() {
        return this.visitNum;
    }

    public String getVisitUnitPrice() {
        return this.visitUnitPrice;
    }

    public Integer getIndex() {
        return this.index;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBillNo(final String billNo) {
        this.billNo = billNo;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public void setSaleDate(final String saleDate) {
        this.saleDate = saleDate;
    }

    public void setSaleShift(final String saleShift) {
        this.saleShift = saleShift;
    }

    public void setSaleJobNum(final String saleJobNum) {
        this.saleJobNum = saleJobNum;
    }

    public void setSaleTotalAmt(final String saleTotalAmt) {
        this.saleTotalAmt = saleTotalAmt;
    }

    public void setCash(final String cash) {
        this.cash = cash;
    }

    public void setGsshPaymentNo1(final String gsshPaymentNo1) {
        this.gsshPaymentNo1 = gsshPaymentNo1;
    }

    public void setGsshPaymentNo2(final String gsshPaymentNo2) {
        this.gsshPaymentNo2 = gsshPaymentNo2;
    }

    public void setGsshPaymentNo3(final String gsshPaymentNo3) {
        this.gsshPaymentNo3 = gsshPaymentNo3;
    }

    public void setGsshPaymentNo4(final String gsshPaymentNo4) {
        this.gsshPaymentNo4 = gsshPaymentNo4;
    }

    public void setGsshPaymentNo5(final String gsshPaymentNo5) {
        this.gsshPaymentNo5 = gsshPaymentNo5;
    }

    public void setVisitNum(final String visitNum) {
        this.visitNum = visitNum;
    }

    public void setVisitUnitPrice(final String visitUnitPrice) {
        this.visitUnitPrice = visitUnitPrice;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ShiftSalesOutData)) {
            return false;
        } else {
            ShiftSalesOutData other = (ShiftSalesOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label203: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label203;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label203;
                    }

                    return false;
                }

                Object this$billNo = this.getBillNo();
                Object other$billNo = other.getBillNo();
                if (this$billNo == null) {
                    if (other$billNo != null) {
                        return false;
                    }
                } else if (!this$billNo.equals(other$billNo)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                label182: {
                    Object this$saleDate = this.getSaleDate();
                    Object other$saleDate = other.getSaleDate();
                    if (this$saleDate == null) {
                        if (other$saleDate == null) {
                            break label182;
                        }
                    } else if (this$saleDate.equals(other$saleDate)) {
                        break label182;
                    }

                    return false;
                }

                label175: {
                    Object this$saleShift = this.getSaleShift();
                    Object other$saleShift = other.getSaleShift();
                    if (this$saleShift == null) {
                        if (other$saleShift == null) {
                            break label175;
                        }
                    } else if (this$saleShift.equals(other$saleShift)) {
                        break label175;
                    }

                    return false;
                }

                label168: {
                    Object this$saleJobNum = this.getSaleJobNum();
                    Object other$saleJobNum = other.getSaleJobNum();
                    if (this$saleJobNum == null) {
                        if (other$saleJobNum == null) {
                            break label168;
                        }
                    } else if (this$saleJobNum.equals(other$saleJobNum)) {
                        break label168;
                    }

                    return false;
                }

                Object this$saleTotalAmt = this.getSaleTotalAmt();
                Object other$saleTotalAmt = other.getSaleTotalAmt();
                if (this$saleTotalAmt == null) {
                    if (other$saleTotalAmt != null) {
                        return false;
                    }
                } else if (!this$saleTotalAmt.equals(other$saleTotalAmt)) {
                    return false;
                }

                label154: {
                    Object this$cash = this.getCash();
                    Object other$cash = other.getCash();
                    if (this$cash == null) {
                        if (other$cash == null) {
                            break label154;
                        }
                    } else if (this$cash.equals(other$cash)) {
                        break label154;
                    }

                    return false;
                }

                Object this$gsshPaymentNo1 = this.getGsshPaymentNo1();
                Object other$gsshPaymentNo1 = other.getGsshPaymentNo1();
                if (this$gsshPaymentNo1 == null) {
                    if (other$gsshPaymentNo1 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentNo1.equals(other$gsshPaymentNo1)) {
                    return false;
                }

                label140: {
                    Object this$gsshPaymentNo2 = this.getGsshPaymentNo2();
                    Object other$gsshPaymentNo2 = other.getGsshPaymentNo2();
                    if (this$gsshPaymentNo2 == null) {
                        if (other$gsshPaymentNo2 == null) {
                            break label140;
                        }
                    } else if (this$gsshPaymentNo2.equals(other$gsshPaymentNo2)) {
                        break label140;
                    }

                    return false;
                }

                Object this$gsshPaymentNo3 = this.getGsshPaymentNo3();
                Object other$gsshPaymentNo3 = other.getGsshPaymentNo3();
                if (this$gsshPaymentNo3 == null) {
                    if (other$gsshPaymentNo3 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentNo3.equals(other$gsshPaymentNo3)) {
                    return false;
                }

                Object this$gsshPaymentNo4 = this.getGsshPaymentNo4();
                Object other$gsshPaymentNo4 = other.getGsshPaymentNo4();
                if (this$gsshPaymentNo4 == null) {
                    if (other$gsshPaymentNo4 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentNo4.equals(other$gsshPaymentNo4)) {
                    return false;
                }

                label119: {
                    Object this$gsshPaymentNo5 = this.getGsshPaymentNo5();
                    Object other$gsshPaymentNo5 = other.getGsshPaymentNo5();
                    if (this$gsshPaymentNo5 == null) {
                        if (other$gsshPaymentNo5 == null) {
                            break label119;
                        }
                    } else if (this$gsshPaymentNo5.equals(other$gsshPaymentNo5)) {
                        break label119;
                    }

                    return false;
                }

                label112: {
                    Object this$visitNum = this.getVisitNum();
                    Object other$visitNum = other.getVisitNum();
                    if (this$visitNum == null) {
                        if (other$visitNum == null) {
                            break label112;
                        }
                    } else if (this$visitNum.equals(other$visitNum)) {
                        break label112;
                    }

                    return false;
                }

                Object this$visitUnitPrice = this.getVisitUnitPrice();
                Object other$visitUnitPrice = other.getVisitUnitPrice();
                if (this$visitUnitPrice == null) {
                    if (other$visitUnitPrice != null) {
                        return false;
                    }
                } else if (!this$visitUnitPrice.equals(other$visitUnitPrice)) {
                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ShiftSalesOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $billNo = this.getBillNo();
        result = result * 59 + ($billNo == null ? 43 : $billNo.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        Object $saleDate = this.getSaleDate();
        result = result * 59 + ($saleDate == null ? 43 : $saleDate.hashCode());
        Object $saleShift = this.getSaleShift();
        result = result * 59 + ($saleShift == null ? 43 : $saleShift.hashCode());
        Object $saleJobNum = this.getSaleJobNum();
        result = result * 59 + ($saleJobNum == null ? 43 : $saleJobNum.hashCode());
        Object $saleTotalAmt = this.getSaleTotalAmt();
        result = result * 59 + ($saleTotalAmt == null ? 43 : $saleTotalAmt.hashCode());
        Object $cash = this.getCash();
        result = result * 59 + ($cash == null ? 43 : $cash.hashCode());
        Object $gsshPaymentNo1 = this.getGsshPaymentNo1();
        result = result * 59 + ($gsshPaymentNo1 == null ? 43 : $gsshPaymentNo1.hashCode());
        Object $gsshPaymentNo2 = this.getGsshPaymentNo2();
        result = result * 59 + ($gsshPaymentNo2 == null ? 43 : $gsshPaymentNo2.hashCode());
        Object $gsshPaymentNo3 = this.getGsshPaymentNo3();
        result = result * 59 + ($gsshPaymentNo3 == null ? 43 : $gsshPaymentNo3.hashCode());
        Object $gsshPaymentNo4 = this.getGsshPaymentNo4();
        result = result * 59 + ($gsshPaymentNo4 == null ? 43 : $gsshPaymentNo4.hashCode());
        Object $gsshPaymentNo5 = this.getGsshPaymentNo5();
        result = result * 59 + ($gsshPaymentNo5 == null ? 43 : $gsshPaymentNo5.hashCode());
        Object $visitNum = this.getVisitNum();
        result = result * 59 + ($visitNum == null ? 43 : $visitNum.hashCode());
        Object $visitUnitPrice = this.getVisitUnitPrice();
        result = result * 59 + ($visitUnitPrice == null ? 43 : $visitUnitPrice.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        return result;
    }

    public String toString() {
        return "ShiftSalesOutData(clientId=" + this.getClientId() + ", billNo=" + this.getBillNo() + ", brId=" + this.getBrId() + ", saleDate=" + this.getSaleDate() + ", saleShift=" + this.getSaleShift() + ", saleJobNum=" + this.getSaleJobNum() + ", saleTotalAmt=" + this.getSaleTotalAmt() + ", cash=" + this.getCash() + ", gsshPaymentNo1=" + this.getGsshPaymentNo1() + ", gsshPaymentNo2=" + this.getGsshPaymentNo2() + ", gsshPaymentNo3=" + this.getGsshPaymentNo3() + ", gsshPaymentNo4=" + this.getGsshPaymentNo4() + ", gsshPaymentNo5=" + this.getGsshPaymentNo5() + ", visitNum=" + this.getVisitNum() + ", visitUnitPrice=" + this.getVisitUnitPrice() + ", index=" + this.getIndex() + ")";
    }
}
