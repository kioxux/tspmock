package com.gys.business.service;

import com.gys.business.mapper.entity.MibReconciliation;
import com.gys.business.service.data.CommenData.InData;

import com.gys.business.service.data.mib.*;

import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

/**
 * @author wavesen.shen
 */
public interface MibService {
    /**
     * 保存医保订单数据
     * @param userInfo 登录信息
     * @param inData 消息体
     *
     */
    void saveMibDrugInfoAndDetail(GetLoginOutData userInfo, PayInfoData inData);

    /**
     * 保存医保订单数据
     * @param userInfo 登录信息
     * @param inData 消息体
     *
     */
    void saveMibSetlInfoAndDetail(GetLoginOutData userInfo, SetlInfoData inData);
 /**
     * 获取山西省忻州市医疗保险普通门诊结算单数据
     * @param userInfo
     * @param inData
     * @return
     */
    MibSXXZJSFormInfoOutData getMibSXXZJSForm(GetLoginOutData userInfo, MibRePrintFormInData inData);

    MibSXSZHttpOutData getMibSXSZJSForm(GetLoginOutData userInfo, MibRePrintFormInData inData);

    InsuranceSettleTableOutData getMibJSSplitForm(MibRePrintFormInData inData); 	List<MibInfoOutData> getSeltInfo(GetLoginOutData userInfo, InData inData);
    List<MibInfoOutData> getSeltInfoSum(GetLoginOutData userInfo, InData inData);

    List<MibInfoOutData> getSeltInfo3202(GetLoginOutData userInfo, InData inData);

    void updateCzFlag(Map<String, Object> map );

    void reconciliationInsert(MibReconciliation reconciliation);

    MibReconciliation reconciliationSelect(MibReconciliation reconciliation);

    void reconciliationUpdate(MibReconciliation reconciliation);

    void reconciliationInsertOrUpdate(MibReconciliation inData);

    PaymentOut getSeltInfoOne(InData inData);

    void updateSetlInfoState(Map<String, Object> map);
}
