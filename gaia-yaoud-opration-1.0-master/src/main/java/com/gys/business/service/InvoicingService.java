package com.gys.business.service;

import com.gys.business.service.data.InvoicingInData;
import com.gys.business.service.data.InvoicingOutData;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface InvoicingService {
    InvoicingOutDataObj getInvoicingList(InvoicingInData inData);

    /**
     * 进销存查询
     * @param inData
     * @return
     */
    List<InvoicingOutData> getIncomeStatementList(InvoicingInData inData);

    /**
     * 进销存明细导出
     * @param inData
     * @param response
     * @return
     */
    void invoicingExport(InvoicingInData inData, HttpServletResponse response);
}
