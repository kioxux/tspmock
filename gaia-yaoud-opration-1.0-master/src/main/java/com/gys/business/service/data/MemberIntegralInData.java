
package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MemberIntegralInData implements Serializable {
    private static final long serialVersionUID = 1629464568751381941L;
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "会员编号")
    private String memberId;

    @ApiModelProperty(value = "会员卡号")
    private String cardId;

    @ApiModelProperty(value = "积分操作类型,0 - 加积分,1-减积分")
    private String type;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "积分")
    private String integral;

    @ApiModelProperty(value = "修改人员")
    private String updateEmp;

}
