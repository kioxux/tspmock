package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaSdPreordDMapper;
import com.gys.business.mapper.GaiaSdPreordHMapper;
import com.gys.business.mapper.GaiaSdPreordPayMsgMapper;
import com.gys.business.mapper.entity.GaiaSdPreordD;
import com.gys.business.mapper.entity.GaiaSdPreordH;
import com.gys.business.mapper.entity.GaiaSdPreordPayMsg;
import com.gys.business.service.OrderService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.CopyUtil;
import com.gys.util.UtilMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 业务实体
 *
 * @author xiaoyuan on 2020/12/23
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private GaiaSdPreordDMapper gaiaSdPreordDMapper;

    @Autowired
    private GaiaSdPreordHMapper gaiaSdPreordHMapper;

    @Autowired
    private GaiaSdPreordPayMsgMapper payMsgMapper;


    @Override
    public List<OrderOutData> getProductData(GetLoginOutData userInfo, OrderInData inData) {
        if (StrUtil.isBlank(inData.getNameOrCode())){
            throw new BusinessException(UtilMessage.CONDITION_CANNOT_BE_EMPTY);
        }
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        List<OrderOutData> outData = new ArrayList<>();
        if (UtilMessage.LOCAL.equals(inData.getType())){
            outData = this.gaiaSdPreordHMapper.getProductByBusiness(inData);
        }else if (UtilMessage.NATIONWIDE.equals(inData.getType())){
            outData = this.gaiaSdPreordHMapper.getProductByBasic(inData);
        }
        if (CollUtil.isNotEmpty(outData)){
            List<OrderOutData> finalOutData = outData;
            IntStream.range(0, outData.size()).forEach(i -> (finalOutData.get(i)).setIndex(i + 1));
        }
        return outData;
    }

    @Override
    public List<OrderPayOutData> getPayList(GetLoginOutData userInfo) {
        return this.payMsgMapper.getPayList(userInfo);
    }

    @Override
    public List<OrderRecordOutData> getOrderRecordList(GetLoginOutData userInfo, OrderRecordInData inData) {
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        List<OrderRecordOutData> outData = this.gaiaSdPreordHMapper.getOrderRecordList(inData);
        if (CollUtil.isNotEmpty(outData)){
            IntStream.range(0, outData.size()).forEach(i -> (outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }

    @Override
    public OrderAll getOrderDetailsList(GetLoginOutData userInfo, OrderInData inData) {
        OrderAll all = new OrderAll();
        inData.setClient(userInfo.getClient());
        if (UtilMessage.WEB.equals(inData.getFlag()) && StrUtil.isNotBlank(inData.getFlag())){
            if (StrUtil.isBlank(inData.getBrId())){
                throw new BusinessException(UtilMessage.STORE_CODE_CANNOT_BE_EMPTY);
            }
        }else {
            inData.setBrId(userInfo.getDepId());
        }

        List<GaiaSdPreordD> sdPreordD = this.gaiaSdPreordDMapper.getOrderDetailsList(inData);
        if (CollUtil.isNotEmpty(sdPreordD)){
            List<PreordDDetailsOutData> details = CopyUtil.copyList(sdPreordD, PreordDDetailsOutData.class);
            all.setDetails(details);
        }else{
            throw new BusinessException(UtilMessage.CURRENT_ORDER_IS_ABNORMAL);
        }
        List<PreordPayMsgDetails> payList = this.payMsgMapper.getOrderDetailsList(inData);
        if (CollUtil.isNotEmpty(payList)){
            all.setPayMsgDetails(payList);
        }else{
            throw new BusinessException(UtilMessage.CURRENT_ORDER_IS_ABNORMAL);
        }
        return all;
    }


    @Override
    public List<OrderRecordOutData> headquartersRecords(GetLoginOutData userInfo, HeadquartersRecordsData inData) {
        OrderRecordInData record = CopyUtil.copy(inData, OrderRecordInData.class);
        record.setClient(userInfo.getClient());
        List<OrderRecordOutData> outData = this.gaiaSdPreordHMapper.getOrderRecordList(record);
        if (CollUtil.isNotEmpty(outData)){
            IntStream.range(0, outData.size()).forEach(i -> (outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(GetLoginOutData userInfo, OrderSaveInData inData) {
        if (CollUtil.isEmpty(inData.getDetails())){
            throw new BusinessException(UtilMessage.ORDER_DETAILS_CANNOT_BE_EMPTY);
        }

        String voucherId = this.gaiaSdPreordHMapper.getVoucherIdByDG(userInfo.getClient());
        String nowDate = DateUtil.format(new Date(), UtilMessage.YYYYMMDD);
        GaiaSdPreordH preordH =  new GaiaSdPreordH();
        preordH.setClient(userInfo.getClient());
        preordH.setGsphBrId(userInfo.getDepId());
        preordH.setGsphVoucherId(voucherId);
        preordH.setGsphDate(nowDate);
        preordH.setGsphTime(DateUtil.format(new Date(), UtilMessage.HHMMSS));
        preordH.setGsphAmt(new BigDecimal(inData.getGsphAmt()));
        preordH.setGsphCostoerName(inData.getCostoerName());
        preordH.setGsphCostoerMobile(inData.getCostoerMobile());
        preordH.setGsphCostoerId(inData.getCostoerId());
        preordH.setGsphMemberId(inData.getMemberId());
        preordH.setGsphcostoerRemarks(inData.getCostoerRemarks());  //备注信息
        preordH.setGsphEmp(userInfo.getUserId());
        preordH.setGsphFlag(UtilMessage.ZERO);
        preordH.setGsphType("DG");
        preordH.setGsphStatus(UtilMessage.ONE);
        //主表
        this.gaiaSdPreordHMapper.insertSelective(preordH);
        //详情表
        List<GaiaSdPreordD> preordDS = CopyUtil.copyList(inData.getDetails(), GaiaSdPreordD.class);
        preordDS.forEach(preordD -> {
            preordD.setClient(userInfo.getClient());
            preordD.setGspdBrId(userInfo.getDepId());
            preordD.setGspdVoucherId(voucherId);
            preordD.setGspdDate(nowDate);
        });
        this.gaiaSdPreordDMapper.insertLists(preordDS);
        //支付表
        List<GaiaSdPreordPayMsg> payMsgs = CopyUtil.copyList(inData.getPayMsgDetails(), GaiaSdPreordPayMsg.class);
        payMsgs.forEach(payMsg -> {
            payMsg.setClient(userInfo.getClient());
            payMsg.setGsppmBrId(userInfo.getDepId());
            payMsg.setGsppmVoucherId(voucherId);
            payMsg.setGsppmDate(nowDate);
        });
        if(CollUtil.isNotEmpty(payMsgs)) {
            this.payMsgMapper.insertLists(payMsgs);
        }
    }

    @Override
    public void returnOrder(GetLoginOutData userInfo, OrderSaveInData inData) {
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        GaiaSdPreordH preordH =  this.gaiaSdPreordHMapper.getPreordHByVoucherId(inData);
        if (ObjectUtil.isEmpty(preordH)){
            throw new BusinessException(UtilMessage.INCORRECT_ORDER_NUMBER);
        }
        if (!UtilMessage.ONE.equals(preordH.getGsphStatus())){
            throw new BusinessException(UtilMessage.THIS_ORDER_CAN_ONLY_BE_OPERATED_UNDER_ORDER);
        }

        preordH.setGsphStatus(UtilMessage.TWO);
        this.gaiaSdPreordHMapper.update(preordH);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void writeOffAndSave(GetLoginOutData userInfo, OrderSaveInData inData) {
        if (CollUtil.isEmpty(inData.getDetails())){
            throw new BusinessException(UtilMessage.ORDER_DETAILS_CANNOT_BE_EMPTY);
        }

        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        GaiaSdPreordH preordH =  this.gaiaSdPreordHMapper.getPreordHByVoucherId(inData);
        if (ObjectUtil.isEmpty(preordH)){
            throw new BusinessException(UtilMessage.INCORRECT_ORDER_NUMBER);
        }
        if (!UtilMessage.ONE.equals(preordH.getGsphStatus())){
            throw new BusinessException(UtilMessage.THIS_ORDER_CAN_ONLY_BE_OPERATED_UNDER_ORDER);
        }

        String voucherId = this.gaiaSdPreordHMapper.getVoucherIdByHX(userInfo.getClient());
        String nowDate = DateUtil.format(new Date(), UtilMessage.YYYYMMDD);
        preordH.setGsphFlag(UtilMessage.ONE);
        preordH.setGsphUpdateDate(nowDate);
        preordH.setGsphUpdateTime(DateUtil.format(new Date(), UtilMessage.HHMMSS));
        preordH.setGsphBillNo(voucherId);
        preordH.setGsphStatus(UtilMessage.THREE);
        this.gaiaSdPreordHMapper.update(preordH);

        GaiaSdPreordH preordHX =  new GaiaSdPreordH();
        preordHX.setClient(userInfo.getClient());
        preordHX.setGsphBrId(userInfo.getDepId());
        preordHX.setGsphVoucherId(voucherId);
        preordHX.setGsphDate(nowDate);
        preordHX.setGsphTime(DateUtil.format(new Date(), UtilMessage.HHMMSS));
        preordHX.setGsphAmt(new BigDecimal(inData.getGsphAmt()));
        preordHX.setGsphCostoerName(inData.getCostoerName());
        preordHX.setGsphCostoerMobile(inData.getCostoerMobile());
        preordHX.setGsphCostoerId(inData.getCostoerId());
        preordHX.setGsphMemberId(inData.getMemberId());
        preordHX.setGsphEmp(userInfo.getUserId());
        preordHX.setGsphFlag(UtilMessage.ZERO);
        preordHX.setGsphType("HX");
        preordHX.setGsphStatus(UtilMessage.THREE);
        //核销
        this.gaiaSdPreordHMapper.insertSelective(preordHX);

        //详情表
        List<GaiaSdPreordD> preordDS = CopyUtil.copyList(inData.getDetails(), GaiaSdPreordD.class);
        preordDS.forEach(preordD -> {
            preordD.setClient(userInfo.getClient());
            preordD.setGspdBrId(userInfo.getDepId());
            preordD.setGspdVoucherId(voucherId);
            preordD.setGspdDate(nowDate);
        });
        this.gaiaSdPreordDMapper.insertLists(preordDS);
        //支付表
        List<GaiaSdPreordPayMsg> payMsgs = CopyUtil.copyList(inData.getPayMsgDetails(), GaiaSdPreordPayMsg.class);
        payMsgs.forEach(payMsg -> {
            payMsg.setClient(userInfo.getClient());
            payMsg.setGsppmBrId(userInfo.getDepId());
            payMsg.setGsppmVoucherId(voucherId);
            payMsg.setGsppmDate(nowDate);
        });
        this.payMsgMapper.insertLists(payMsgs);
    }


}
