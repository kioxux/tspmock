package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class RestOrderChangeDto implements Serializable {
    private static final long serialVersionUID = -3355534640503978414L;

    private String client;

    /**
     * 销售单
     */
    private String billNo;

    /**
     * 销售日期
     */
    private String gsbcDate;

    /**
     * 行号
     */
    private String gsbcSerial;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 批号
     */
    private String gsbcBatchNo;

    /**
     * 效期
     */
    private String gssdValidDate;

    /**
     * 批次
     */
    private String gsbcBatch;

    /**
     * 数量
     */
    private String gsbcQty;
}
