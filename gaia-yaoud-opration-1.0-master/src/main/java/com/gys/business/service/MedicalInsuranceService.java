package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.service.data.RebornData;
import com.gys.business.service.data.SalesInquireData;

import java.util.List;


public interface MedicalInsuranceService {
    List<RebornData> selectMedProList(String client,String brId,List<RebornData> inData);
    List<RebornData> selectMedProList2(String client,String brId,List<RebornData> inData);
    List<RebornData> matchMedProList(String client, String brId, List<GaiaSdBatchChange> inData);
    List<RebornData> matchReturnProBatchList(String client, String brId, SalesInquireData inData);
}
