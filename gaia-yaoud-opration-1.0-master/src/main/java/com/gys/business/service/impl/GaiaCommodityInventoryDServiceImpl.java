package com.gys.business.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaCommodityInventoryDMapper;
import com.gys.business.mapper.GaiaCommodityInventoryHMapper;
import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.GaiaWmsRkysMapper;
import com.gys.business.mapper.entity.GaiaCommodityInventoryD;
import com.gys.business.mapper.entity.GaiaCommodityInventoryH;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.GaiaCommodityInventoryDService;
import com.gys.business.service.data.CommodityAllocationPushInData;
import com.gys.business.service.data.RecallDTO;
import com.gys.business.service.data.RecallForm;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.vo.BaseResponse;
import com.gys.feign.GysStoreWebService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品调库-明细表(GaiaCommodityInventoryD)表服务实现类
 *
 * @author makejava
 * @since 2021-10-19 16:01:15
 */
@Slf4j
@Service("gaiaCommodityInventoryDService")
public class GaiaCommodityInventoryDServiceImpl implements GaiaCommodityInventoryDService {
    @Resource
    private GaiaCommodityInventoryDMapper gaiaCommodityInventoryDDao;
    @Resource
    GysStoreWebService gysStoreWebService;
    @Resource
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Resource
    private GaiaWmsRkysMapper gaiaWmsRkysMapper;
    @Resource
    private GaiaCommodityInventoryHMapper gaiaCommodityInventoryHMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void push(CommodityAllocationPushInData inData) {
        GaiaCommodityInventoryH inventoryH = gaiaCommodityInventoryHMapper.getByBillNo(inData.getClient(), inData.getBillNo());
        if (ObjectUtil.isEmpty(inventoryH)) {
            throw new BusinessException("该调库单不存在！");
        }
        if (inventoryH.getInvalidDate().compareTo(inData.getUpdateTime()) < 0) {
            throw new BusinessException("该调库单已失效！");
        }
        if ("2".equals(inData.getAllotType())) {
            if (ObjectUtil.isEmpty(inData.getXlxqs())) {
                return;
            }
            List<String> stoCodes = gaiaCommodityInventoryDDao.getStoCode(inData.getClient(), inData.getXlxqs());
            inData.setStores(stoCodes);
        }

        if (ObjectUtil.isEmpty(inData.getStores())) {
            return;
        }
        //查询所有匹配的调库详情
        List<GaiaCommodityInventoryD> commodityInventoryDS = gaiaCommodityInventoryDDao.getCommodityDetail(inData);
        if (ObjectUtil.isEmpty(commodityInventoryDS)){
            return;
        }
        //查询连锁公司
        List<String> compadmIds = gaiaCommodityInventoryDDao.getChainCompany(inData);
        List<GaiaCommodityInventoryD> updateInventoryList = new ArrayList<>();
        ArrayList<RecallDTO> recallDTOS = new ArrayList<>();

        List<String> stoCodes = commodityInventoryDS.stream().map(GaiaCommodityInventoryD::getStoCode).distinct().collect(Collectors.toList());
        List<String> proSelfCodes = commodityInventoryDS.stream().map(GaiaCommodityInventoryD::getProSelfCode).distinct().collect(Collectors.toList());

        //根据调库详情查询库存
        List<GaiaSdStockBatch> allStockBatches = gaiaSdStockBatchMapper.getByClientAndBrIdAndProId(inData.getClient(), stoCodes, proSelfCodes);

        for (GaiaCommodityInventoryD inventoryD : commodityInventoryDS) {
            //过滤出该调库单的库存信息
            List<GaiaSdStockBatch> stockBatches = allStockBatches.stream().filter(o -> o.getClientId().equals(inventoryD.getClient()) && o.getGssbBrId().equals(inventoryD.getStoCode()) && o.getGssbProId().equals(inventoryD.getProSelfCode())).sorted((o1, o2) -> DateUtil.parse(o2.getGssbVaildDate(), DatePattern.PURE_DATE_FORMAT).compareTo(DateUtil.parse(o1.getGssbVaildDate(), DatePattern.PURE_DATE_FORMAT))).collect(Collectors.toList());
            //计算商品总库存量是否大于建议调库量
            BigDecimal inventoryTotal = BigDecimal.ZERO;
            for (GaiaSdStockBatch stockBatch : stockBatches) {
                inventoryTotal = inventoryTotal.add(new BigDecimal(stockBatch.getGssbQty()));
            }
            //如果大于等于不退库，保存原因
            if (inventoryD.getSuggestionReturnQty().compareTo(inventoryTotal) >= 0) {
                GaiaCommodityInventoryD gaiaCommodityInventoryD = new GaiaCommodityInventoryD();
                gaiaCommodityInventoryD.setId(inventoryD.getId());
                gaiaCommodityInventoryD.setStatus(2);
                gaiaCommodityInventoryD.setRemark("库存不足");
                gaiaCommodityInventoryD.setUpdateUser(inData.getUpdateUser());
                gaiaCommodityInventoryD.setUpdateTime(inData.getUpdateTime());
                updateInventoryList.add(gaiaCommodityInventoryD);
                continue;
            }
            //按效期最大的优先退库
            int index = 0;
            BigDecimal suggestionReturnQty = inventoryD.getSuggestionReturnQty();
            while (BigDecimal.ZERO.compareTo(suggestionReturnQty) < 0) {
                if (StringUtil.isBlank(stockBatches.get(index).getGssbBatchNo())) {
                    index++;
                    continue;
                }
                RecallDTO recallDTO = new RecallDTO();
                recallDTO.setStore(inventoryD.getStoCode());
                recallDTO.setCommodityCode(inventoryD.getProSelfCode());
                recallDTO.setBatchNo(stockBatches.get(index).getGssbBatchNo());
                //判断库存是否大于剩余退库数
                BigDecimal gssbQty= new BigDecimal(stockBatches.get(index).getGssbQty());
                if (gssbQty.compareTo(suggestionReturnQty) >= 0) {
                    recallDTO.setRecallNumber(suggestionReturnQty);
                } else {
                    recallDTO.setRecallNumber(new BigDecimal(stockBatches.get(index).getGssbQty()));
                }
                suggestionReturnQty = suggestionReturnQty.subtract(new BigDecimal(stockBatches.get(index).getGssbQty()));
                recallDTO.setRemark("");
                recallDTO.setChainCompany(compadmIds.get(0));
                recallDTOS.add(recallDTO);
                index++;
            }
            GaiaCommodityInventoryD gaiaCommodityInventoryD = new GaiaCommodityInventoryD();
            gaiaCommodityInventoryD.setId(inventoryD.getId());
            gaiaCommodityInventoryD.setStatus(1);
            gaiaCommodityInventoryD.setRemark("");
            gaiaCommodityInventoryD.setUpdateUser(inData.getUpdateUser());
            gaiaCommodityInventoryD.setUpdateTime(inData.getUpdateTime());
            updateInventoryList.add(gaiaCommodityInventoryD);
        }

        if (ObjectUtil.isNotEmpty(recallDTOS)) {
            RecallForm recallForm = new RecallForm();
            recallForm.setRecallList(recallDTOS);
            String recall = gysStoreWebService.recall(recallForm, inData.getToken());
            log.info("gysStoreWebService.recall:" + recallForm + ";res:" + recall);
            BaseResponse resultObj = JSONObject.parseObject(recall, BaseResponse.class);
            int code = resultObj.getCode();
            if (code != 200) {
                throw new BusinessException("商品召回下发接口服务异常");
            }
        }

        if (ObjectUtil.isNotEmpty(updateInventoryList)) {
            gaiaCommodityInventoryDDao.batchUpdateStatusAndRemark(updateInventoryList);
        }

        List<GaiaCommodityInventoryD> list = gaiaCommodityInventoryDDao.getByBillNo(inData.getBillNo());
        if (ObjectUtil.isEmpty(list)) {
            GaiaCommodityInventoryH gaiaCommodityInventoryH = new GaiaCommodityInventoryH();
            gaiaCommodityInventoryH.setClient(inData.getClient());
            gaiaCommodityInventoryH.setBillCode(inData.getBillNo());
            gaiaCommodityInventoryH.setStatus(1);
            gaiaCommodityInventoryH.setFinishTime(inData.getUpdateTime());
            gaiaCommodityInventoryH.setUpdateUser(inData.getUpdateUser());
            gaiaCommodityInventoryH.setUpdateTime(inData.getUpdateTime());
            gaiaCommodityInventoryHMapper.updateStatus(gaiaCommodityInventoryH);
        }
    }
}
