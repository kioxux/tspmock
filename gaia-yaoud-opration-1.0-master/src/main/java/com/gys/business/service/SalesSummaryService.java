package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaCategories;
import com.gys.business.mapper.entity.GaiaUser;
import com.gys.business.mapper.entity.dto.GaiaStoreCategoryDropDownDTO;
import com.gys.business.service.data.ReportForms.StoreSaleDateInData;
import com.gys.business.service.data.ReportForms.WebStoreSaleDateInData;
import com.gys.business.service.data.SalesSummaryData;
import com.gys.business.service.data.StoreOutDatas;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;

import java.util.List;
import java.util.Map;

/**
 * 销售管理汇总查询
 *
 * @author xiaoyuan on 2020/7/24
 */
public interface SalesSummaryService {

    /**
     * 根据条件查询结果
     * @param summaryData
     * @return
     */
    PageInfo findSalesSummary(SalesSummaryData summaryData);

    /**
     * 根据加盟号,地址 查询员工
     * @param client
     * @param depId
     * @return
     */
    List<GaiaUser> findUserByClientAndDepId(String client, String depId);

    /**
     * 根据加盟号,地址 查询商品分类
     * @param client
     * @param depId
     * @return
     */
    List<GaiaCategories> findCategoriesByClientAndDepId(String client,String depId);

//    PageInfo findSalesSummaryByBrId(SalesSummaryData summaryData);

    Map<String,Object> findSalesSummaryByBrId(SalesSummaryData summaryData);

    /**
     * 门店销售列表标题头
     * @param summaryData summaryData
     * @return JsonResult
     */
    JsonResult selectStoreSalesSummaryTitle(SalesSummaryData summaryData);

    List<Map<String,Object>> selectStoreList(Map<String,Object> inData);

    Map<String,Object> salesgrade(GetLoginOutData userInfo);


    PageInfo findSalesSummaryByDate(StoreSaleDateInData summaryData);

    /**
     * WEB端报表(用户)
     *
     * @param data 请求参数
     * @return 返回类型List<Map < String, Object>>
     */
    PageInfo selectWebSalesSummaryByDate(WebStoreSaleDateInData data);

    /**
     * WEB端报表导出
     * @param data
     * @return
     */
    Result exportSalesSummary(WebStoreSaleDateInData data);

    /**
     * 获取用户下的门店
     * @param client
     * @param userId
     * @return
     */
    List<StoreOutDatas> selectAuthStoreList(String client, String userId);

    /**
     * 查询门店分类筛选条件
     *
     * @param clientId 加盟商id
     * @return JsonResult
     */
    JsonResult selectStoreCategoryCondition(String clientId);

    /**
     * 查询门店分类下拉
     *
     * @param gaiaStoreCategoryDropDownDTO gaiaStoreCategoryDropDownDTO
     * @return JsonResult
     */
    JsonResult selectStoreCategoryDropdown(String clientId, GaiaStoreCategoryDropDownDTO gaiaStoreCategoryDropDownDTO);

}
