package com.gys.business.service.data.shanxi;

import lombok.Data;

import java.util.List;

@Data
public class SaleCountReportOutput {
    private String stoCode;
    private String stoName;
    private String userCode;
    private String userName;
    private List<SaleCountReportVO> saleCountReports;
}
