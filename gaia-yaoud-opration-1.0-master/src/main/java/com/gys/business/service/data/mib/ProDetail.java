package com.gys.business.service.data.mib;

import lombok.Data;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/12/28 13:13
 */
@Data
public class ProDetail {
    private String proName;
    private String spec;
    private String price;
    private String num;
    private String amt;
    private String priceAndNum;
}
