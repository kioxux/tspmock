package com.gys.business.service.data;

import lombok.Data;

/**
 * @Author ：gyx
 * @Date ：Created in 18:25 2021/11/1
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class GaiaInWarehouseRejection {
    //供应商号
    private String supplierCoder;
    //供应商名称
    private String supplierName;
    //验收日期(yyyy-MM-dd)
    private String acceptanceConfirmationTime;
    //采购订单号
    private String orderSerialNo;
    //入库验收单号
    private String warehouseReceiptNo;
}
