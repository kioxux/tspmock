package com.gys.business.service.data;

import lombok.Data;

/**
 * 毛利区间划分查询入参
 *
 * @author Zhangchi
 * @since 2021/09/16/10:54
 */
@Data
public class GrossMarginInData {
    /**
     * 入参客户列表
     */
    private String[] clientId;

    private String client;
}
