package com.gys.business.service;


import com.gys.business.mapper.entity.GaiaClientBenefitLevelInfo;
import com.gys.business.service.data.GaiaClientBenefitLevelInfoData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.response.Result;
import com.gys.common.vo.GaiaClientBenefitLevelInfoVo;

import java.util.List;

/**
 * @author csm
 * @date 2021/12/16 - 16:42
 */
public interface GaiaClientBenefitLevelInfoService {
    /**
     * 根据加盟ID查询
     * @param gcliData
     * @return
     */
    List<GaiaClientBenefitLevelInfoVo> getByClient(GaiaClientBenefitLevelInfoData gcliData);

    /**
     * 根据加盟ID修改客户级别相关参数
     * @param userInfo
     * @return
     */
    void update(GetLoginOutData userInfo, GaiaClientBenefitLevelInfoData gcbld);

    /**
     *查询客户默认级别相关参数
     * @return
     */
    List<GaiaClientBenefitLevelInfo> getDefaultValues();

    /**
     * 客户级别相关数据导出
     * @param client
     * @return
     */
    Result exportClientLevel(String client);
}
