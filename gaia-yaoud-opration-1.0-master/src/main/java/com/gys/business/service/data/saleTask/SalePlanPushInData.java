package com.gys.business.service.data.saleTask;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SalePlanPushInData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "加盟商数组")
    private String[] clients;
    @ApiModelProperty(value = "任务ID")
    private String taskId;
    @ApiModelProperty(value = "推送日期")
    private String pushDate;
    @ApiModelProperty(value = "推送时间")
    private String pushTime;
    @ApiModelProperty(value = "推送类型 1 立即推送 2 定时推送")
    private String pushType;
    @ApiModelProperty(value = "推送状态 0 未推送 1 已推送")
    private String pushStatus;
}
