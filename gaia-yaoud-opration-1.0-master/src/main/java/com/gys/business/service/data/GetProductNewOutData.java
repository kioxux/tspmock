package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class GetProductNewOutData {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "通用名称")
    private String proCommonName;
    @ApiModelProperty(value = "商品名")
    private String name;
    @ApiModelProperty(value = "助记码")
    private String pym;
    @ApiModelProperty(value = "规格")
    private String specs;
    @ApiModelProperty(value = "计量单位")
    private String unit;
    @ApiModelProperty(value = "剂型")
    private String form;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "价格")
    private String addAmt;

    private String client;

    private String proSite;

}
