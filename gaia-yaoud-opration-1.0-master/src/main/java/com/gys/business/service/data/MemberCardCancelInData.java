package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MemberCardCancelInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "操作人部门编号")
    private String depId;

    @ApiModelProperty(value = "会员编号")
    private String memberId;

    @ApiModelProperty(value = "会员卡号")
    private String cardId;

    @ApiModelProperty(value = "操作原因")
    private String remark;

    @ApiModelProperty(value = "操作人")
    private String empId;



}
