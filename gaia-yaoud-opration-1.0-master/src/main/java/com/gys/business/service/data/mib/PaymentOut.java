package com.gys.business.service.data.mib;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 结算信息
 */
@Data
public class PaymentOut implements Serializable {
    private static final long serialVersionUID = 5403428150762022434L;
    /**
     * 结算ID
     */
    @JSONField(name = "setl_id")
    private String setlId;

    /**
     * 就诊ID
     */
    @JSONField(name = "mdtrt_id")
    private String mdtrtId;

    /**
     * 人员编号
     */
    @JSONField(name = "psn_no")
    private String psnNo;

    /**
     * 人员姓名
     */
    @JSONField(name = "psn_name")
    private String psnName;

    /**
     * 人员证件类型
     */
    @JSONField(name = "psn_cert_type")
    private String psnCertType;

    /**
     * 证件号码
     */
    private String certno;

    /**
     * 性别
     */
    private String gend;

    /**
     * 民族
     */
    private String naty;

    /**
     * 出生日期
     */
    private String brdy;

    /**
     * 年龄
     */
    private String age;

    /**
     * 险种类型
     */
    private String insutype;

    /**
     * 人员类别
     */
    @JSONField(name = "psn_type")
    private String psnType;

    /**
     * 公务员标志
     */
    @JSONField(name = "cvlserv_flag")
    private String cvlservFlag;

    /**
     * 结算时间
     */
    @JSONField(name = "setl_time")
    private String setlTime;

    /**
     * 就诊凭证类型
     */
    @JSONField(name = "mdtrt_cert_type")
    private String mdtrtCertType;

    /**
     * 医疗类别
     */
    @JSONField(name = "med_type")
    private String medType;

    /**
     * 医疗费总额
     */
    @JSONField(name = "medfee_sumamt")
    private String medfeeSumamt;

    /**
     * 全自费金额
     */
    @JSONField(name = "fulamt_ownpay_amt")
    private String fulamtOwnpayAmt;

    /**
     * 超限价自费费用
     */
    @JSONField(name = "overlmt_selfpay")
    private String overlmtSelfpay;

    /**
     * 先行自付金额
     */
    @JSONField(name = "preselfpay_amt")
    private String preselfpayAmt;

    /**
     * 符合政策范围金额
     */
    @JSONField(name = "inscp_scp_amt")
    private String inscpScpAmt;

    /**
     * 实际支付起付线
     */
    @JSONField(name = "act_pay_dedc")
    private String actPayDedc;

    /**
     * 基本医疗保险统筹基金支出
     */
    @JSONField(name = "hifp_pay")
    private String hifpPay;

    /**
     * 基本医疗保险统筹基金支付比例
     */
    @JSONField(name = "pool_prop_selfpay")
    private String poolPropSelfpay;

    /**
     * 公务员医疗补助资金支出
     */
    @JSONField(name = "cvlserv_pay")
    private String cvlservPay;

    /**
     * 企业补充医疗保险基金支出
     */
    @JSONField(name = "hifes_pay")
    private String hifesPay;

    /**
     * 居民大病保险资金支出
     */
    @JSONField(name = "hifmi_pay")
    private String hifmiPay;

    /**
     * 职工大额医疗费用补助基金支出
     */
    @JSONField(name = "hifob_pay")
    private String hifobPay;

    /**
     * 医疗救助基金支出
     */
    @JSONField(name = "maf_pay")
    private String mafPay;

    /**
     * 其他支出
     */
    @JSONField(name = "oth_pay")
    private String othPay;

    /**
     * 基金支付总额
     */
    @JSONField(name = "fund_pay_sumamt")
    private String fundPaySumamt;

    /**
     * 个人负担总金额
     */
    @JSONField(name = "psn_part_amt")
    private String psnPartAmt;

    /**
     * 个人账户支出
     */
    @JSONField(name = "acct_pay")
    private String acctPay;

    /**
     * 个人现金支出
     */
    @JSONField(name = "psn_cash_pay")
    private String psnCashPay;

    /**
     * 余额
     */
    private String balc;

    /**
     * 个人账户共济支付金额
     */
    @JSONField(name = "acct_mulaid_pay")
    private String acctMulaidPay;

    /**
     * 医药机构结算ID
     */
    @JSONField(name = "medins_setl_id")
    private String medinsSetlId;

    /**
     * 清算经办机构
     */
    @JSONField(name = "clr_optins")
    private String clrOptins;

    /**
     * 清算方式
     */
    @JSONField(name = "clr_way")
    private String clrWay;

    /**
     * 清算类别
     */
    @JSONField(name = "clr_type")
    private String clrType;


}
