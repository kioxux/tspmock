package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CompanyOutOfStockDetailOutData implements Serializable {

    /**
     * 门店编号
     */
    @ApiModelProperty(value = "门店编号")
    private String brId;

    /**
     * 门店名
     */
    @ApiModelProperty(value = "门店名")
    private String brName;

    /**
     * 缺断货品项
     */
    @ApiModelProperty(value = "缺断货品项")
    private Integer outOfStockCount;

    /**
     * 月均销售额
     */
    @ApiModelProperty(value = "月均销售额")
    private BigDecimal avgSaleAmt;

    /**
     * 月均毛利额
     */
    @ApiModelProperty(value = "月均毛利额")
    private BigDecimal avgProfitAmt;


}
