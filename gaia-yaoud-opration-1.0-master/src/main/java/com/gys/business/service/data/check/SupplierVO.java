package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 15:15
 **/
@Data
public class SupplierVO {
    @ApiModelProperty(value = "委托配送方编码", example = "100001")
    private String value;
    @ApiModelProperty(value = "委托配送方名称", example = "南通紫石医药")
    private String name;
    @ApiModelProperty(value = "委托配送方信息", example = "100001-南通紫石医药")
    private String label;
}
