//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class FranchiseeInData {
    private String client;
    private String francName;
    private String francNo;
    private String francLegalPerson;
    private String francLegalPersonName;
    private String francQua;
    private String francPhone;
    private String francAddr;
    private String francCreDate;
    private String francCreTime;
    private String francCreId;
    private String francModiDate;
    private String francModiTime;
    private String francModiId;
    private String francLogo;
    private String francType1;
    private String francType2;
    private String francType3;
    private String francAss;
    private String francAssName;
    private Integer pageNum;
    private Integer pageSize;
    public FranchiseeInData() {
    }

    public String getClient() {
        return this.client;
    }

    public String getFrancName() {
        return this.francName;
    }

    public String getFrancNo() {
        return this.francNo;
    }

    public String getFrancLegalPerson() {
        return this.francLegalPerson;
    }

    public String getFrancLegalPersonName() {
        return this.francLegalPersonName;
    }

    public String getFrancQua() {
        return this.francQua;
    }

    public String getFrancPhone() {
        return this.francPhone;
    }

    public String getFrancAddr() {
        return this.francAddr;
    }

    public String getFrancCreDate() {
        return this.francCreDate;
    }

    public String getFrancCreTime() {
        return this.francCreTime;
    }

    public String getFrancCreId() {
        return this.francCreId;
    }

    public String getFrancModiDate() {
        return this.francModiDate;
    }

    public String getFrancModiTime() {
        return this.francModiTime;
    }

    public String getFrancModiId() {
        return this.francModiId;
    }

    public String getFrancLogo() {
        return this.francLogo;
    }

    public String getFrancType1() {
        return this.francType1;
    }

    public String getFrancType2() {
        return this.francType2;
    }

    public String getFrancType3() {
        return this.francType3;
    }

    public String getFrancAss() {
        return this.francAss;
    }

    public String getFrancAssName() {
        return this.francAssName;
    }

    public void setClient(final String client) {
        this.client = client;
    }

    public void setFrancName(final String francName) {
        this.francName = francName;
    }

    public void setFrancNo(final String francNo) {
        this.francNo = francNo;
    }

    public void setFrancLegalPerson(final String francLegalPerson) {
        this.francLegalPerson = francLegalPerson;
    }

    public void setFrancLegalPersonName(final String francLegalPersonName) {
        this.francLegalPersonName = francLegalPersonName;
    }

    public void setFrancQua(final String francQua) {
        this.francQua = francQua;
    }

    public void setFrancPhone(final String francPhone) {
        this.francPhone = francPhone;
    }

    public void setFrancAddr(final String francAddr) {
        this.francAddr = francAddr;
    }

    public void setFrancCreDate(final String francCreDate) {
        this.francCreDate = francCreDate;
    }

    public void setFrancCreTime(final String francCreTime) {
        this.francCreTime = francCreTime;
    }

    public void setFrancCreId(final String francCreId) {
        this.francCreId = francCreId;
    }

    public void setFrancModiDate(final String francModiDate) {
        this.francModiDate = francModiDate;
    }

    public void setFrancModiTime(final String francModiTime) {
        this.francModiTime = francModiTime;
    }

    public void setFrancModiId(final String francModiId) {
        this.francModiId = francModiId;
    }

    public void setFrancLogo(final String francLogo) {
        this.francLogo = francLogo;
    }

    public void setFrancType1(final String francType1) {
        this.francType1 = francType1;
    }

    public void setFrancType2(final String francType2) {
        this.francType2 = francType2;
    }

    public void setFrancType3(final String francType3) {
        this.francType3 = francType3;
    }

    public void setFrancAss(final String francAss) {
        this.francAss = francAss;
    }

    public void setFrancAssName(final String francAssName) {
        this.francAssName = francAssName;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
