package com.gys.business.service.impl;

import com.google.common.collect.Lists;
import com.gys.business.controller.app.form.OutStockForm;
import com.gys.business.controller.app.vo.ProductInfoVO;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CompanyOutOfStockService;
import com.gys.business.service.DcOutStockHService;
import com.gys.business.service.data.CompanyOutOfStockDetailOutData;
import com.gys.business.service.data.CompanyOutOfStockInfoOutData;
import com.gys.business.service.data.OutOfStockInData;
import com.gys.business.service.data.ProductRank;
import com.gys.util.CommonUtil;
import com.gys.util.DateUtil;
import com.gys.util.SerialNoUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @desc: 公司级缺断货服务类
 * @author: Ryan
 * @createTime: 2021/8/23 10:37
 */
@Slf4j
@Service("dcOutStockHService")
public class DcOutStockHServiceImpl implements DcOutStockHService {
    @Resource
    private CompanyOutOfStockService companyOutOfStockService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private DcOutStockHMapper dcOutStockHMapper;
    @Resource
    private DcOutStockDMapper dcOutStockDMapper;
    @Resource
    private DcOutStockStoreMapper dcOutStockStoreMapper;
    @Resource
    private DcOutStockStoreHMapper dcOutStockStoreHMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void generateOutStock(GaiaDcData dcData) {
        try {
            //获取前品类排名前1000名
            List<ProductRank> productRankList = companyOutOfStockService.getCategoryModelList(dcData.getClient(), 90);
            if (productRankList.size() == 0) {
                return ;
            }
            //获取缺断货情况
            List<ProductInfoVO> productList = getOutStockProductList(dcData, productRankList);
            //生成缺断数据H-D
            DcOutStockH dcOutStockH = generateOutStockHAndD(productList, dcData);
            //生成门店明细H-D
            generateStoreDetail(dcOutStockH, dcData, productRankList);
        } catch (Exception e) {
            log.error("<定时任务><公司级缺断货><任务执行异常：{}>", e.getMessage(), e);
        }
    }

    private void generateStoreDetail(DcOutStockH dcOutStockH, GaiaDcData dcData, List<ProductRank> productRankList) {
        //获取门店统计信息
        OutOfStockInData inData = new OutOfStockInData();
        inData.setClientId(dcData.getClient());
        inData.setQueryDate(CommonUtil.getyyyyMMdd());
        inData.setRankList(productRankList);
        DcOutStockStoreH stockStoreH = transformDcOutStockStoreH(inData);
        if (stockStoreH == null) {
            return;
        }
        stockStoreH.setClient(dcOutStockH.getClient());
        stockStoreH.setVoucherId(dcOutStockH.getVoucherId());
        dcOutStockStoreHMapper.add(stockStoreH);
        //获取门店明细数据
        List<DcOutStockStore> stockStoreList = transformStockStoreDetail(dcOutStockH, inData);
        if (stockStoreList == null || stockStoreList.size() == 0) {
            return;
        }
        for (DcOutStockStore dcOutStockStore : stockStoreList) {
            dcOutStockStoreMapper.add(dcOutStockStore);
        }
    }

    private DcOutStockStoreH transformDcOutStockStoreH(OutOfStockInData inData) {
        CompanyOutOfStockInfoOutData companyOutOfStockInfo = gaiaProductBusinessMapper.getCompanyDcOutOfStockInfo(inData);
        if (companyOutOfStockInfo == null) {
            return null;
        }
        DcOutStockStoreH stockStoreH = new DcOutStockStoreH();
        stockStoreH.setStoreNum(companyOutOfStockInfo.getStoreCount());
        stockStoreH.setProductNum(companyOutOfStockInfo.getOutOfStockCount());
        stockStoreH.setMonthProfitAmount(companyOutOfStockInfo.getAvgProfitAmt());
        stockStoreH.setMonthSaleAmount(companyOutOfStockInfo.getAvgSaleAmt());
        return stockStoreH;
    }

    private List<DcOutStockStore> transformStockStoreDetail(DcOutStockH dcOutStockH, OutOfStockInData inData) {
        List<DcOutStockStore> storeList = new ArrayList<>();
        //获取门店缺断货明细
        List<CompanyOutOfStockDetailOutData> companyOutOfStockList = gaiaProductBusinessMapper.getCompanyDcOutOfStockList(inData);
        if (companyOutOfStockList == null || companyOutOfStockList.size() == 0) {
            return null;
        }
        for (CompanyOutOfStockDetailOutData detailOutData : companyOutOfStockList) {
            DcOutStockStore stockStore = new DcOutStockStore();
            stockStore.setClient(dcOutStockH.getClient());
            stockStore.setVoucherId(dcOutStockH.getVoucherId());
            stockStore.setStoreId(detailOutData.getBrId());
            stockStore.setStoreName(detailOutData.getBrName());
            stockStore.setProNum(detailOutData.getOutOfStockCount());
            stockStore.setMonthProfitAmount(detailOutData.getAvgProfitAmt());
            stockStore.setMonthSaleAmount(detailOutData.getAvgSaleAmt());
            storeList.add(stockStore);
        }
        return storeList;
    }

    private DcOutStockH generateOutStockHAndD(List<ProductInfoVO> productList, GaiaDcData dcData) {
        //主表
        DcOutStockH dcOutStockH = transformDcOutStockH(dcData);
        dcOutStockHMapper.add(dcOutStockH);
        //明细表
        List<DcOutStockD> dcOutStockDList = transformDcOutStockD(dcData, dcOutStockH, productList);
        BigDecimal saleAmount = BigDecimal.ZERO;
        BigDecimal profitAmount = BigDecimal.ZERO;
        for (DcOutStockD dcOutStockD : dcOutStockDList) {
            dcOutStockDMapper.add(dcOutStockD);
            saleAmount = saleAmount.add(dcOutStockD.getMonthSaleAmount());
            profitAmount = profitAmount.add(dcOutStockD.getMonthProfitAmount());
        }
        //更新主表
        dcOutStockH.setProNum(dcOutStockDList.size());
        dcOutStockH.setMonthProfitAmount(profitAmount);
        dcOutStockH.setMonthSaleAmount(saleAmount);
        dcOutStockHMapper.update(dcOutStockH);
        return dcOutStockH;
    }

    private List<DcOutStockD> transformDcOutStockD(GaiaDcData dcData, DcOutStockH dcOutStockH, List<ProductInfoVO> productList) {
        List<DcOutStockD> list = Lists.newArrayList();
        for (ProductInfoVO productInfoVO : productList) {
            DcOutStockD stockD = new DcOutStockD();
            stockD.setClient(dcData.getClient());
            stockD.setVoucherId(dcOutStockH.getVoucherId());
            stockD.setProSelfCode(productInfoVO.getProSelfCode());
            stockD.setProCommonName(productInfoVO.getProSelfName());
            stockD.setProUnit("");
            stockD.setProSpecs(productInfoVO.getProSpec());
            stockD.setFactoryName(productInfoVO.getProFactoryName());
            stockD.setModelSort(productInfoVO.getSortNum());
            stockD.setMonthSaleNum(productInfoVO.getMonthSaleNum());
            stockD.setMonthSaleAmount(productInfoVO.getMonthSaleAmount());
            stockD.setMonthProfitAmount(productInfoVO.getMonthProfitAmount());
            list.add(stockD);
        }
        return list;
    }

    private DcOutStockH transformDcOutStockH(GaiaDcData dcData) {
        DcOutStockH stockH = new DcOutStockH();
        stockH.setClient(dcData.getClient());
        stockH.setDcCode(dcData.getDcCode());
        stockH.setDcName(dcData.getDcName());
        stockH.setDcShortName(dcData.getDcShortName());
        stockH.setPushDate(DateUtil.dateToString(new Date(), "yyyy-MM-dd"));
        stockH.setVoucherId(SerialNoUtils.generate("D", "S"));
        return stockH;
    }

    private List<ProductInfoVO> getOutStockProductList(GaiaDcData dcData, List<ProductRank> productRankList) {
        OutStockForm outStockForm = new OutStockForm();
        outStockForm.setClient(dcData.getClient());
        outStockForm.setDcCodeList(Lists.newArrayList(dcData.getDcCode()));
        Date currentDate = new Date();
        outStockForm.setStartDate(DateUtil.dateToString(DateUtils.addDays(currentDate, -90),"yyyyMMdd"));
        outStockForm.setEndDate(CommonUtil.getyyyyMMdd());
        List<ProductInfoVO> list = gaiaProductBusinessMapper.getOutOfStockList(outStockForm);
        if (list == null || list.size() == 0) {
            return null;
        }
        //品类模型转map
        Map<String, Integer> collect = productRankList.stream().collect(Collectors.toMap(ProductRank::getProSelfCode, ProductRank::getRankNum));
        Iterator<ProductInfoVO> iterator = list.iterator();
        while (iterator.hasNext()) {
            ProductInfoVO productInfoVO = iterator.next();
            if (!collect.containsKey(productInfoVO.getProSelfCode())) {
                iterator.remove();
                continue;
            }
            productInfoVO.setSortNum(collect.get(productInfoVO.getProSelfCode()));
        }
        return list;
    }
}
