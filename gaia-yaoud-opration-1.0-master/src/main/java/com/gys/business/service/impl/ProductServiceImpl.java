//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.ProductMapper;
import com.gys.business.mapper.entity.Product;
import com.gys.business.service.ProductService;
import com.gys.business.service.data.GetProductBaseInData;
import com.gys.business.service.data.GetProductOutData;
import com.gys.common.data.PageInfo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper;

    public ProductServiceImpl() {
    }

    public List<GetProductOutData> findByCondition(GetProductBaseInData inData) {
        List<GetProductOutData> outData = null;
        outData = this.productMapper.selectProductsByCondition();
        return outData;
    }

    public PageInfo<GetProductOutData> findByPage(GetProductBaseInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetProductOutData> outData = null;
        outData = this.productMapper.selectProductsByCondition();
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(outData)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            pageInfo = new PageInfo(outData);
        }

        return pageInfo;
    }

    @Transactional
    public Boolean saveProduct(GetProductBaseInData inData) {
        Product product = new Product();
        BeanUtils.copyProperties(inData, product);
        this.productMapper.insertSelective(product);
        return true;
    }

    @Transactional
    public Boolean editProduct(GetProductBaseInData inData) {
        Product product = new Product();
        BeanUtils.copyProperties(inData, product);
        this.productMapper.updateByPrimaryKeySelective(product);
        return true;
    }

    @Transactional
    public Boolean deleteProducts(GetProductBaseInData inData) {
        if (CollectionUtils.isEmpty(inData.getIds())) {
            return false;
        } else {
            Example example = new Example(Product.class);
            example.createCriteria().andIn("id", inData.getIds());
            this.productMapper.deleteByExample(example);
            return true;
        }
    }
}
