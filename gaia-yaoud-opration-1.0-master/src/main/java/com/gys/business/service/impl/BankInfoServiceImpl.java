//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaBankInfoMapper;
import com.gys.business.mapper.entity.GaiaBankInfo;
import com.gys.business.service.BankInfoService;
import com.gys.business.service.data.BankInfoInData;
import com.gys.business.service.data.BankInfoOutData;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class BankInfoServiceImpl implements BankInfoService {
    @Autowired
    private GaiaBankInfoMapper bankInfoMapper;

    public BankInfoServiceImpl() {
    }

    public List<BankInfoOutData> getBankInfoList(BankInfoInData inData) {
        new ArrayList();
        List<BankInfoOutData> bankInfoList = this.bankInfoMapper.getBankInfoList(inData);
        if (bankInfoList.size() > 0) {
            for(int i = 0; i < bankInfoList.size(); ++i) {
                ((BankInfoOutData)bankInfoList.get(i)).setIndex(i + 1);
            }
        }

        return bankInfoList;
    }

    public List<String> getBankNames(BankInfoInData inData) {
        new ArrayList();
        List<String> bankNamesList = this.bankInfoMapper.getBankNames(inData);
        return bankNamesList;
    }

    public List<String> getAllBankIdByName(BankInfoInData inData) {
        new ArrayList();
        List<String> bankIdsList = this.bankInfoMapper.getAllBankIdByName(inData);
        return bankIdsList;
    }

    public List<String> getAllBankAccountByName(BankInfoInData inData) {
        new ArrayList();
        List<String> bankAccountsList = this.bankInfoMapper.getAllBankAccountByName(inData);
        return bankAccountsList;
    }

    public List<String> getBankIdByBankAccount(BankInfoInData inData) {
        new ArrayList();
        List<String> bankIdsList = this.bankInfoMapper.getBankIdByBankAccount(inData);
        return bankIdsList;
    }

    public void insertBankInfo(BankInfoInData inData) {
        GaiaBankInfo bankInfo = new GaiaBankInfo();
        inData.setGsbsBankId(this.bankInfoMapper.selectNextGsbsBankId());
        BeanUtils.copyProperties(inData, bankInfo);
        this.bankInfoMapper.insert(bankInfo);
    }

    public void editBankInfo(BankInfoInData inData) {
        Example example = new Example(GaiaBankInfo.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsbsBankId", inData.getGsbsBankId());
        GaiaBankInfo bankInfo = (GaiaBankInfo)this.bankInfoMapper.selectOneByExample(example);
        if (StrUtil.isNotBlank(inData.getGsbsBrId())) {
            bankInfo.setGsbsBrId(inData.getGsbsBrId());
        }

        if (StrUtil.isNotBlank(inData.getGsbsBankName())) {
            bankInfo.setGsbsBankName(inData.getGsbsBankName());
        }

        if (StrUtil.isNotBlank(inData.getGsbsBankAccount())) {
            bankInfo.setGsbsBankAccount(inData.getGsbsBankAccount());
        }

        this.bankInfoMapper.updateByPrimaryKey(bankInfo);
    }

    public BankInfoOutData getBankInfoById(BankInfoInData inData) {
        GaiaBankInfo bankInfo = (GaiaBankInfo)this.bankInfoMapper.selectByPrimaryKey(inData);
        BankInfoOutData outData = new BankInfoOutData();
        if (ObjectUtil.isNotEmpty(bankInfo)) {
            outData.setClientId(bankInfo.getClientId());
            outData.setGsbsBrId(bankInfo.getGsbsBrId());
            outData.setGsbsBankId(bankInfo.getGsbsBankId());
            outData.setGsbsBankName(bankInfo.getGsbsBankName());
            outData.setGsbsBankAccount(bankInfo.getGsbsBankAccount());
        }

        return outData;
    }
}
