package com.gys.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.GaiaPointsDetailsQueryMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.PointsDetailsQueryService;
import com.gys.business.service.data.PointsDetailsQueryInData;
import com.gys.business.service.data.PointsDetailsQueryOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 5.会员管理中增加积分明细查询
 *
 * @author xiaoyuan on 2020/7/28
 */
@Slf4j
@Service
public class PointsDetailsQueryServiceImpl implements PointsDetailsQueryService {

    @Autowired
    private GaiaPointsDetailsQueryMapper queryMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Resource
    private CosUtils cosUtils;


    @Override
    public PageInfo<PointsDetailsQueryOutData> selectPointsDetailsQueryByNameAndId(PointsDetailsQueryInData queryInData) {
        PageHelper.startPage(queryInData.getPageNum(), queryInData.getPageSize());
        List<PointsDetailsQueryOutData> outData = queryMapper.selectPointsDetailsQueryByNameAndIdAndDate(queryInData);
        if (!CollectionUtils.isEmpty(outData)) {
            IntStream.range(0, outData.size()).forEach(i -> (outData.get(i)).setIndex(i + 1));
            // 分页信息
            return new PageInfo<>(outData);
        }
        return new PageInfo<>();
    }

    @Override
    public Result selectPointsDetailsQueryExport(PointsDetailsQueryInData queryInData) {
        Result result;
        List<PointsDetailsQueryOutData> outData = queryMapper.selectPointsDetailsQueryByNameAndIdAndDate(queryInData);
        String name = "会员积分明细查询导出";
        if (outData.size() > 0){
            CsvFileInfo csvFileInfo = CsvClient.getCsvByte(outData,name, Collections.singletonList((short) 1));
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                assert csvFileInfo != null;
                outputStream.write(csvFileInfo.getFileContent());
                result = cosUtils.uploadFile(outputStream, csvFileInfo.getFileName());
            } catch (IOException e){
                log.error("导出文件失败:{}",e.getMessage(), e);
                throw new BusinessException("导出文件失败！");
            } finally {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    log.error("关闭流异常：{}",e.getMessage(),e);
                    throw new BusinessException("关闭流异常！");
                }
            }
            return result;
        }else{
            throw new BusinessException("提示：数据为空！");
        }
    }

    @Override
    public List<GaiaStoreData> getSaleStore(String client) {
        return storeDataMapper.getStoreByClient(client);
    }
}
