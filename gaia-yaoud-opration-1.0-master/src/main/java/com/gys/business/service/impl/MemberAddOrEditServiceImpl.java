package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Pair;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaSdMemberCardChange;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.MemberAddOrEditService;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.*;
import com.gys.common.config.RabbitConfig;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.*;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.common.redis.RedisManager;
import com.gys.common.response.Result;
import com.gys.common.vo.MemberClassVo;
import com.gys.util.*;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.gys.common.enums.MemberCardStatusEnum.*;

@Service
public class MemberAddOrEditServiceImpl implements MemberAddOrEditService {
    private static final Logger log = LoggerFactory.getLogger(MemberAddOrEditServiceImpl.class);
    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;
    @Autowired
    private GaiaSdMemberCardDataMapper gaiaSdMemberCardDataMapper;
    @Autowired
    private GaiaSdStoreDataMapper gaiaSdStoreDataMapper;
    @Autowired
    private RabbitTemplateHelper rabbitTemplate;
    @Autowired
    private GaiaSdRechargeCardMapper gaiaSdRechargeCardMapper;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private GaiaSdIntegralChangeMapper gaiaSdIntegralChangeMapper;

    private static final String SDM_CARD_LOCK = "sdm_card_lock";
    private static final String SDM_CARD_KEY = "sdm_card_key:";

    @Autowired
    private CacheService synchronizedService;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Autowired
    private CosUtils cosUtils;
    @Autowired
    private StoreDataService storeDataService;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdMemberCardChangeMapper memberCardChangeMapper;
    @Autowired
    private GaiaSdElectronChangeMapper electronChangeMapper;
    @Autowired
    private GaiaSdMemberAreaMapper memberAreaMapper;

    @Override
    public PageInfo<GetMemberOutData> findMembersByPage(GetMemberInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        PageInfo pageInfo;
        List<GetMemberOutData> outData = this.gaiaSdMemberBasicMapper.selectMembersByCondition(inData);
        if (ObjectUtil.isNotEmpty(outData)) {
            Integer num = 0;
            for (GetMemberOutData outDatum : outData) {
                outDatum.setIndex(num);
                outDatum.setGmbBirth(CommonUtil.parseyyyyMMdd(outDatum.getGmbBirth()));
                ++num;
            }
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }


    @Override
    public JsonResult findMemberListByPage(MemberListInData inData) {
        //获取参数
        int pageNum = inData.getPageNum();
        int pageSize = inData.getPageSize();
        //非空校验 如果为空则赋值默认值
        if(pageNum == 0) {
            pageNum = CommonConstant.PAGE_NUM;
        }
        if(pageSize == 0) {
            pageSize = CommonConstant.PAGE_SIZE;
        }
        //分页
        PageHelper.startPage(pageNum, pageSize);
        List<MemberListOutData> memberList = gaiaSdMemberBasicMapper.getMemberListByCondition(inData);
        //判断查询是否为空  如果为空 则返回空集合
        if(ValidateUtil.isEmpty(memberList)) {
            return JsonResult.success(memberList, "提示：获取数据成功！");
        }
        com.github.pagehelper.PageInfo memberPageList = new com.github.pagehelper.PageInfo(memberList);

        //汇总金额
        List<MemberListOutData> resultList = memberPageList.getList();
        MemberListTotalOutData total = new MemberListTotalOutData();
        if(ValidateUtil.isNotEmpty(resultList)) {
            int memberCount = resultList.size();
            BigDecimal sumTotalAmt = resultList.stream().map(MemberListOutData :: getTotalAmt).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal sumYearTotalAmt = resultList.stream().map(MemberListOutData :: getYearTotalAmt).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal sumTotalIntegeral = resultList.stream().map(MemberListOutData :: getTotalIntegeral).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal sumYearTotalIntegeral = resultList.stream().map(MemberListOutData :: getYearTotalIntegeral).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal sumCurrIntegeral = resultList.stream().map(MemberListOutData :: getCurrIntegeral).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal sumCurrCardAmt = resultList.stream().map(MemberListOutData :: getCurrCardAmt).reduce(BigDecimal.ZERO, BigDecimal :: add);

            total.setMemberCount(memberCount);
            total.setTotalAmt(sumTotalAmt);
            total.setYearTotalAmt(sumYearTotalAmt);
            total.setTotalIntegeral(sumTotalIntegeral);
            total.setYearTotalIntegeral(sumYearTotalIntegeral);
            total.setCurrIntegeral(sumCurrIntegeral);
            total.setCurrCardAmt(sumCurrCardAmt);
        }

        //返回参数
        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("pageList", memberPageList);
        resultMap.put("total", total);
        return JsonResult.success(resultMap, "提示：获取数据成功！");
    }


    @Override
    public Result exportMemberListByPage(MemberListInData inData) {
        //分页
        List<MemberListOutData> memberList = gaiaSdMemberBasicMapper.getMemberListByCondition(inData);

        List<List<Object>> dataList = new ArrayList<>(memberList.size());
        for(MemberListOutData member : memberList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //办卡门店编码
            lineList.add(member.getBrId());
            //办卡门店
            lineList.add(member.getBrName());
            //录入日期
            lineList.add(member.getInputCardDate());
            //最近消费日期
            lineList.add(member.getLastIntegeralDate());
            //会员卡号
            lineList.add(member.getCardId());
            //姓名
            lineList.add(member.getMemberName());
            //性别
            lineList.add(member.getSex());
            //卡类别
            lineList.add(member.getCardType());
            //手机
            lineList.add(member.getMobile());
            //电话
            lineList.add(member.getTel());
            //地址
            lineList.add(member.getAddress());
            //累计总金额
            lineList.add(member.getTotalAmt());
            //年度累计金额
            lineList.add(member.getYearTotalAmt());
            //累计积分
            lineList.add(member.getTotalIntegeral());
            //年度累计积分
            lineList.add(member.getYearTotalIntegeral());
            //积分
            lineList.add(member.getCurrIntegeral());
            //储值卡余额
            lineList.add(member.getCurrCardAmt());
            dataList.add(lineList);
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.MEMBER_HEAD_LIST);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.MEMBER_SHEET_NAME);
                }});
        Result result = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.MEMBER_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            result = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return result;
    }

    @Override
    public HashMap<String, Object> listVIPParam(GetLoginOutData userInfo, MemberClassVo param) {
        HashMap<String, Object> map = gaiaSdMemberBasicMapper.listVIPParam(userInfo.getClient(), userInfo.getDepId());
        if (ObjectUtil.isEmpty(map)) {
            HashMap<String, Object> res = new HashMap<>();
            res.put("integralUpfalg", 0);
            res.put("integralUptype", 0);
            return res;
        }
        return map;
    }

    @Override
    @Transactional
    public Boolean editMember(GetMemberInData inData) {
        List<StoreOutData> storeList = storeDataService.getStoreList(inData.getClientId(), null);
        List<String> stoCodeList = storeList.stream().map(r -> r.getStoCode()).collect(Collectors.toList());
        //校验身份证格式
        String credentials = inData.getGmbCredentials().trim();
        if(ValidateUtil.isNotEmpty(credentials)) {
            MemberListOutData memberInfo = gaiaSdMemberBasicMapper.getMemberById(inData.getClientId(),inData.getMemberId());
            if (ObjectUtil.isNotEmpty(memberInfo)) {
                if (ValidateUtil.isNotEmpty(memberInfo.getIdentityCard())) {
                    if (!memberInfo.getIdentityCard().trim().equals(credentials)){
                        if(!IdcardUtil.isValidCard(credentials)) {
                            throw new BusinessException("提示：身份证号格式不正确, 请检查");
                        }
                    }
                }
            }
        }
        String name = inData.getGmbName().trim();
        if(ValidateUtil.isNotEmpty(name)) {
            if(name.length() > 10) {
                throw new BusinessException("提示：姓名最长10个汉字，请检查长度");
            }
        }
        String address = inData.getGmbAddress().trim();
        if(ValidateUtil.isNotEmpty(address)) {
            if(address.length() > 50) {
                throw new BusinessException("提示：地址最长50个汉字，请检查长度");
            }
        }
        String memberId = inData.getMemberId().trim();
        if(ValidateUtil.isEmpty(memberId)) {
            throw new BusinessException("提示：会员编号不能为空");
        }

        String newMobile = inData.getNewMobile().trim();
        String oldMobile = inData.getGmbMobile().trim(); 
        if(ValidateUtil.isEmpty(newMobile)) {
            newMobile = "";
        }
        if(ValidateUtil.isEmpty(oldMobile)) {
            oldMobile = "";
        }

        //没修改手机号
        if (newMobile.equals(oldMobile)) {
            GaiaSdMemberBasic memberTemp = new GaiaSdMemberBasic();
            memberTemp.setClientId(inData.getClientId());
            memberTemp.setGsmbMobile(inData.getGmbMobile());
            GaiaSdMemberBasic member = this.gaiaSdMemberBasicMapper.getMemberByClientAndPhoneAndCardId(inData);
            if (member != null) {
                member.setGsmbName(inData.getGmbName());
                member.setGsmbAddress(inData.getGmbAddress());
                member.setGsmbMobile(inData.getGmbMobile());
                member.setGsmbTel(inData.getGmbTel());
                member.setGsmbSex(inData.getGmbSex());
                member.setGsmbCredentials(inData.getGmbCredentials());
//                member.setGsmbAge(inData.getGmbAge());
                if (StringUtils.isNotBlank(inData.getGmbBirth())) {
                    member.setGsmbBirth(DateUtil.format(DateUtil.parse(inData.getGmbBirth()), UtilMessage.YYYYMMDD));
                }
                member.setGsmbBbName(inData.getGmbBbName());
                member.setGsmbBbSex(inData.getGmbBbSex());
                member.setGsmbBbAge(inData.getGmbBbAge());
                member.setGsmbUpdateBrId(inData.getBrId());
                member.setGsmbUpdateSaler(inData.getSaler());
                member.setGsmbUpdateDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                member.setGsmbStatus(inData.getGmbStatus());
                member.setGsmbMobile(oldMobile);
                member.setRemark(inData.getRemark());
                GaiaSdMemberCardData gaiaSdMemberCardinData = new GaiaSdMemberCardData();
                gaiaSdMemberCardinData.setClient(inData.getClientId());
                gaiaSdMemberCardinData.setGsmbcBrId(inData.getBrId());
                gaiaSdMemberCardinData.setGsmbcMemberId(member.getGsmbMemberId());

                GaiaSdMemberCardData gaiaSdMemberCard = this.gaiaSdMemberBasicMapper.selectMembersByMemerId(inData.getClientId(), inData.getBrId(), member.getGsmbMemberId(),inData.getGmbCardId());
                if (gaiaSdMemberCard != null) {
//                    gaiaSdMemberCard.setGsmbcCreateDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
//                    gaiaSdMemberCard.setGsmbcCreateSaler(inData.getSaler());
                    gaiaSdMemberCard.setGsmbcClassId(inData.getGsmcId());
                    gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                    GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
                    gaiaSdStoreData.setClientId(inData.getClientId());
                    gaiaSdStoreData.setGsstBrId(inData.getBrId());
                    gaiaSdStoreData = this.gaiaSdStoreDataMapper.selectByPrimaryKey(gaiaSdStoreData);
                    if (ObjectUtil.isNotEmpty(gaiaSdStoreData)) {
                        gaiaSdMemberCard.setGsmbcZeroDate(gaiaSdStoreData.getGsstJfQldate());
                    }
                    this.gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);

//                    GaiaDataSynchronized aSynchronized = getGaiaDataSynchronizedByCard(inData, member, "update", "会员修改");
//                    this.synchronizedService.addOne(aSynchronized);
                    List<GaiaDataSynchronized> synchronizedByCardList = getGaiaDataSynchronizedByCardList(inData, member, "update", "会员修改", stoCodeList);
                    synchronizedService.insertLists(synchronizedByCardList);
                }
                member.setGsmbNationality(inData.getGsmbNationality());
                member.setGsmbBirthMode(inData.getGsmbBirthMode());
                this.gaiaSdMemberBasicMapper.updateMember(member);

//                GaiaDataSynchronized aSynchronized = this.getGaiaDataSynchronizedByBasic(inData, member.getGsmbMemberId(), "update", "会员修改");
//                this.synchronizedService.addOne(aSynchronized);
                List<GaiaDataSynchronized> synchronizedByBasicList = getGaiaDataSynchronizedByBasicList(inData, member.getGsmbMemberId(), "update", "会员修改", stoCodeList);
                synchronizedService.insertLists(synchronizedByBasicList);
                //修改会员卡当前积分
                updateMemberIntegral(inData);

                MQReceiveData mqReceiveData = new MQReceiveData();
                mqReceiveData.setType("DataSync");
                mqReceiveData.setCmd("editMember");
                mqReceiveData.setData(inData.getGmbCardId());
                //会员修改，需要通知到加盟商下所有门店
                for (String stoCode : stoCodeList) {
                    this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + stoCode, JSON.toJSON(mqReceiveData));
                }
            }
        } else {
            if(ValidateUtil.isNotEmpty(newMobile)) {
                if(newMobile.length() != 11) {
                    throw new BusinessException("提示：手机号格式不正确，请检查");
                }
            }
            GetMemberInData getMemberInData = new GetMemberInData();
            getMemberInData.setClientId(inData.getClientId());
            //getMemberInData.setBrId(inData.getBrId());
            getMemberInData.setGmbMobile(newMobile);
            if(ValidateUtil.isNotEmpty(newMobile)) {
                if (this.gaiaSdMemberBasicMapper.selectMembersByCondition(getMemberInData).size() > 0) {
                    throw new BusinessException("提示：手机号已存在");
                }
            }
            GaiaSdMemberBasic memberTemp = new GaiaSdMemberBasic();
            memberTemp.setClientId(inData.getClientId());
            memberTemp.setGsmbMemberId(inData.getMemberId());
            //memberTemp.setGsmbMobile(inData.getGmbMobile());
            GaiaSdMemberBasic member = this.gaiaSdMemberBasicMapper.selectOne(memberTemp);
            if (member != null) {
                member.setGsmbName(inData.getGmbName());
                member.setGsmbAddress(inData.getGmbAddress());
                member.setGsmbMobile(newMobile);
                member.setGsmbTel(inData.getGmbTel());
                member.setGsmbSex(inData.getGmbSex());
                member.setGsmbCredentials(inData.getGmbCredentials());
//                member.setGsmbAge(inData.getGmbAge());
                if (StringUtils.isNotBlank(inData.getGmbBirth())) {
                    member.setGsmbBirth(DateUtil.format(DateUtil.parse(inData.getGmbBirth()), UtilMessage.YYYYMMDD));
                }
                member.setGsmbBbName(inData.getGmbBbName());
                member.setGsmbBbSex(inData.getGmbBbSex());
                member.setGsmbBbAge(inData.getGmbBbAge());
                member.setGsmbUpdateBrId(inData.getBrId());
                member.setGsmbUpdateSaler(inData.getSaler());
                member.setGsmbUpdateDate(DateUtil.format(new Date(), "yyyyMMdd"));
                member.setGsmbStatus(inData.getGmbStatus());
                GaiaSdMemberCardData gaiaSdMemberCardinData = new GaiaSdMemberCardData();
                gaiaSdMemberCardinData.setClient(inData.getClientId());
                gaiaSdMemberCardinData.setGsmbcBrId(inData.getBrId());
                gaiaSdMemberCardinData.setGsmbcMemberId(member.getGsmbMemberId());

                GaiaSdMemberCardData gaiaSdMemberCard = this.gaiaSdMemberBasicMapper.selectMembersByMemerId(inData.getClientId(), inData.getBrId(), member.getGsmbMemberId(), inData.getGmbCardId());
                if (gaiaSdMemberCard != null) {
//                    gaiaSdMemberCard.setGsmbcCreateDate(DateUtil.format(new Date(), "yyyyMMdd"));
//                    gaiaSdMemberCard.setGsmbcCreateSaler(inData.getSaler());
                    gaiaSdMemberCard.setGsmbcClassId(inData.getGsmcId());
                    gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(new Date(), "yyyyMMdd"));
                    GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
                    gaiaSdStoreData.setClientId(inData.getClientId());
                    gaiaSdStoreData.setGsstBrId(inData.getBrId());
                    gaiaSdStoreData = this.gaiaSdStoreDataMapper.selectByPrimaryKey(gaiaSdStoreData);
                    if (ObjectUtil.isNotEmpty(gaiaSdStoreData)) {
                        gaiaSdMemberCard.setGsmbcZeroDate(gaiaSdStoreData.getGsstJfQldate());
                    }
                    this.gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);

//                    GaiaDataSynchronized aSynchronized = this.getGaiaDataSynchronizedByCard(inData, member, "update", "会员修改");
//                    this.synchronizedService.addOne(aSynchronized);
                    List<GaiaDataSynchronized> synchronizedByCardList = getGaiaDataSynchronizedByCardList(inData, member, "update", "会员修改", stoCodeList);
                    synchronizedService.insertLists(synchronizedByCardList);
                }
                member.setGsmbNationality(inData.getGsmbNationality());
                member.setGsmbBirthMode(inData.getGsmbBirthMode());
                this.gaiaSdMemberBasicMapper.updateMember(member);

//                GaiaDataSynchronized aSynchronized = this.getGaiaDataSynchronizedByBasic(inData, member.getGsmbMemberId(), "update", "会员修改");
//                this.synchronizedService.addOne(aSynchronized);
                List<GaiaDataSynchronized> synchronizedByBasicList = getGaiaDataSynchronizedByBasicList(inData, member.getGsmbMemberId(), "update", "会员修改", stoCodeList);
                synchronizedService.insertLists(synchronizedByBasicList);
                //修改会员卡当前积分
                updateMemberIntegral(inData);

                MQReceiveData mqReceiveData = new MQReceiveData();
                mqReceiveData.setType("DataSync");
                mqReceiveData.setCmd("editMember");
                mqReceiveData.setData(inData.getGmbCardId());
                for (String stoCode : stoCodeList) {
                    this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + stoCode, JSON.toJSON(mqReceiveData));
                }
            }
        }
        return true;
    }

    @Override
    @Transactional
    public MemberAddDto addMember(GetMemberInData inData, GetLoginOutData userInfo) {
        GetMemberInData getMemberInData = new GetMemberInData();
        getMemberInData.setClientId(inData.getClientId());
        getMemberInData.setBrId(inData.getBrId());
        getMemberInData.setGmbMobile(inData.getGmbMobile());
        if (this.gaiaSdMemberBasicMapper.selectMembersByCondition(getMemberInData).size() > 0) {
            throw new BusinessException("提示：手机号已存在");
        }
        //查询门店是单体还是连锁
        StoreOutData storeData = gaiaStoreDataMapper.getStoreData(inData.getClientId(), inData.getBrId());

        GaiaSdMemberBasic memberTemp = new GaiaSdMemberBasic();
        memberTemp.setClientId(inData.getClientId());
        memberTemp.setGsmbMobile(inData.getGmbMobile());
        GaiaSdMemberBasic member = this.gaiaSdMemberBasicMapper.selectOne(memberTemp);
        if (member != null) {
            member.setGsmbName(inData.getGmbName());
            member.setGsmbAddress(inData.getGmbAddress());
            member.setGsmbMobile(inData.getGmbMobile());
            member.setGsmbTel(inData.getGmbTel());
            member.setGsmbSex(inData.getGmbSex());
            member.setGsmbCredentials(inData.getGmbCredentials());
            member.setGsmbAge(inData.getGmbAge());
            member.setGsmbBirth(DateUtil.format(DateUtil.parse(inData.getGmbBirth()), UtilMessage.YYYYMMDD));
            member.setGsmbBbName(inData.getGmbBbName());
            member.setGsmbBbSex(inData.getGmbBbSex());
            member.setGsmbBbAge(inData.getGmbBbAge());
            member.setGsmbUpdateBrId(inData.getBrId());
            member.setGsmbUpdateSaler(inData.getSaler());
            member.setGsmbUpdateDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
            member.setGsmbStatus(inData.getGmbStatus());
            member.setRemark(inData.getRemark());
            member.setGsmbArea(inData.getGsmbcArea());
            member.setGsmbNationality(inData.getGsmbNationality());
            member.setGsmbBirthMode(inData.getGsmbBirthMode());
            this.gaiaSdMemberBasicMapper.updateMember(member);

            GaiaDataSynchronized aSynchronized = this.getGaiaDataSynchronizedByBasic(inData, member.getGsmbMemberId(), "update", "会员修改");
            this.synchronizedService.addOne(aSynchronized);
        } else {
            member = new GaiaSdMemberBasic();
            String randomUUID = IdUtil.randomUUID();
            member.setGsmbMemberId(randomUUID);
            member.setClientId(inData.getClientId());
            member.setGsmbMobile(inData.getGmbMobile());
            member.setGsmbName(inData.getGmbName());
            member.setGsmbAddress(inData.getGmbAddress());
            member.setGsmbTel(inData.getGmbTel());
            member.setGsmbSex(inData.getGmbSex());
            member.setGsmbCredentials(inData.getGmbCredentials());
            member.setGsmbAge(inData.getGmbAge());
            member.setGsmbBirth(DateUtil.format(DateUtil.parse(inData.getGmbBirth()), UtilMessage.YYYYMMDD));
            member.setGsmbBbName(inData.getGmbBbName());
            member.setGsmbBbSex(inData.getGmbBbSex());
            member.setGsmbBbAge(inData.getGmbBbAge());
            member.setGsmbContactAllowed("1");
            member.setGsmbUpdateBrId(inData.getBrId());
            member.setGsmbUpdateSaler(inData.getSaler());
            member.setGsmbUpdateDate(DateUtil.format(new Date(), "yyyyMMdd"));
            member.setRemark(inData.getRemark());
            member.setGsmbArea(inData.getGsmbcArea());
            member.setGsmbNationality(inData.getGsmbNationality());
            member.setGsmbBirthMode(inData.getGsmbBirthMode());
            this.gaiaSdMemberBasicMapper.insertMember(member);

            GaiaDataSynchronized aSynchronized = this.getGaiaDataSynchronizedByBasic(inData, randomUUID, "insert", "会员新增");
            this.synchronizedService.addOne(aSynchronized);
        }

        // 会员卡记录
        GaiaSdMemberCardData gaiaSdMemberCardData = this.getGaiaSdMemberCardData(inData, storeData, member);
        GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
        gaiaSdStoreData.setClientId(inData.getClientId());
        gaiaSdStoreData.setGsstBrId(inData.getBrId());
        gaiaSdStoreData = this.gaiaSdStoreDataMapper.selectByPrimaryKey(gaiaSdStoreData);
        if (ObjectUtil.isNotEmpty(gaiaSdStoreData)) {
            gaiaSdMemberCardData.setGsmbcZeroDate(gaiaSdStoreData.getGsstJfQldate());
        }
        Example example = new Example(GaiaSdMemberCardData.class);
        example.createCriteria().andEqualTo("gsmbcCardId", inData.getGmbCardId()).andEqualTo("client", inData.getClientId());
        int count = this.gaiaSdMemberCardDataMapper.selectCountByExample(example);
        if (count > 0) {
            log.info("会员卡新增，卡号重复，client:{}, depId:{}, loginName:{}, 卡号:{}", userInfo.getClient(),
                    userInfo.getDepId(), userInfo.getLoginName(), inData.getGmbCardId());
            throw new BusinessException("此卡号已被使用，请尝试新卡号");
        }
        this.gaiaSdMemberCardDataMapper.insert(gaiaSdMemberCardData);

        GaiaDataSynchronized aSynchronized = this.getGaiaDataSynchronizedByCard(inData, member, "insert", "会员新增");
        this.synchronizedService.addOne(aSynchronized);

        //绑定储值卡
        if (StrUtil.isNotBlank(inData.getStoredValueCard()) && UtilMessage.Suffix1.equals(inData.getStoredValueCard()) && ObjectUtil.isNotEmpty(gaiaSdStoreData)) {
            GaiaSdRechargeCard rechargeCard = this.gaiaSdRechargeCardMapper.getAccountByCardId(inData.getClientId(),inData.getGmbCardId());
            if (ObjectUtil.isNotEmpty(rechargeCard)) {
                throw new BusinessException("储值卡信息已存在,请仔细核查!");
            }
            GaiaSdRechargeCard card = this.getGaiaSdRechargeCard(inData, gaiaSdStoreData);
            this.gaiaSdRechargeCardMapper.insert(card);
        }

        /*MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("addMember");
        mqReceiveData.setData(inData.getGmbCardId());
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + inData.getBrId(), JSON.toJSON(mqReceiveData));*/
        log.info("推送新增会员信息，FX begin========");
        MemberAddDto dto = new MemberAddDto(member, gaiaSdMemberCardData);
        pushMemberInfo(inData.getClientId(), dto);
        return dto;
    }

    /**
     * MQ推送会员信息
     * @param client
     * @param dto
     */
    public void pushMemberInfo(String client, MemberAddDto dto) {
        //查询client下所有门店
        //查询当前加盟商下所有门店
        List<StoreOutData> storeDataList = this.storeDataMapper.getStoreList(client, null);
        List<String> brIdList = storeDataList.stream().map(r -> r.getStoCode()).collect(Collectors.toList());
        if (CollUtil.isEmpty(brIdList)) {
            log.info("推送新增会员信息，client:{} 下无任何门店", client);
            return;
        }
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("addMember");
        mqReceiveData.setData(dto);
        Object pushData = JSON.toJSON(mqReceiveData);
        for (String brId : brIdList) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, client + "." + brId, pushData);
            log.info("推送新增会员信息，client:{}，brId:{}，pushData:{}", client, brId, JSON.toJSONString(pushData));
        }
    }

    private String[] headList = {
            "办卡门店编码",
            "办卡门店名称",
            "创建日期",
            "办卡员工",
            "最近消费日期",
            "会员卡号",
            "姓名",
            "性别",
            "卡类别",
            "手机",
            "电话",
            "地址",
            "累计总金额",
            "年度累计金额",
            "累计积分",
            "年度累计积分",
            "积分",
            "储值卡余额"
    };

    @Override
    public Result findMemberListByConditionExport(MemberConditionInData inData) {

        //是否开启委托配送相应功能 0：关 1：开	  默认或没有值为0
        String exportFlagStr = storeDataMapper.selectWtpsParam(inData.getClient(), "MDHYXXDC");
        if(StrUtil.isBlank(exportFlagStr) || (StrUtil.isNotBlank(exportFlagStr) && "0".equals(exportFlagStr))){
            throw new BusinessException("提示：该加盟没有配置会员信息导出权限！");
        }
        //返回参数
//        Map<String, Object> result = new HashMap<>(16);
        //分页参数
        //查询会员列表
        List<MemberListOutData> memberList = gaiaSdMemberBasicMapper.getMemberListByConditionExt(inData);

        com.github.pagehelper.PageInfo pageInfo = new com.github.pagehelper.PageInfo(memberList);
        List<MemberListOutData> pageList = pageInfo.getList();
        //对当页数据计算各项汇总
        MemberListTotalOutData memberListTotal = new MemberListTotalOutData();
        List<MemberListOutData> brIdList = pageList.stream().filter(x->StrUtil.isNotBlank(x.getBrId())).
                collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(MemberListOutData :: getBrId))),
                        ArrayList::new));
        BigDecimal totalAmt = pageList.stream().map(MemberListOutData::getTotalAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal yearTotalAmt = pageList.stream().map(MemberListOutData::getYearTotalAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalIntegeral = pageList.stream().map(MemberListOutData::getTotalIntegeral).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal yearTotalIntegeral = pageList.stream().map(MemberListOutData::getYearTotalIntegeral).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalCurrIntegeral = pageList.stream().map(MemberListOutData::getCurrIntegeral).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalCurrCardAmt = pageList.stream().map(MemberListOutData::getCurrCardAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        memberListTotal.setBrName(brIdList.size());   //门店数量
        memberListTotal.setCardId(pageList.size());   //会员卡号数量
        memberListTotal.setTotalAmt(totalAmt);            //累计总金额汇总
        memberListTotal.setYearTotalAmt(yearTotalAmt);      //年度累计金额汇总
        memberListTotal.setTotalIntegeral(totalIntegeral);          //累计积分汇总
        memberListTotal.setYearTotalIntegeral(yearTotalIntegeral);   //年度累计积分汇总
        memberListTotal.setCurrIntegeral(totalCurrIntegeral);       //当前积分汇总
        memberListTotal.setCurrCardAmt(totalCurrCardAmt);           //储值卡余额汇总

//        result.put("memberList", pageInfo);
//        result.put("total", memberListTotal);
        Result result = null;
        try {
            List<List<Object>> dataList = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(pageInfo.getList())) {
                MemberListOutData total = new MemberListOutData();
                BeanUtils.copyProperties(memberListTotal,total);
                total.setBrId("合计");
                pageList.add(total);

                CsvFileInfo csvInfo =null;
                // 导出
                // byte数据

                if(CollectionUtil.isNotEmpty(pageList)){
                    csvInfo = CsvClient.getCsvByte(pageList,"会员信息", Collections.singletonList((short) 1));
                }else {
                    throw new BusinessException("提示:会员信息为空！");
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                try {
                    bos.write(csvInfo.getFileContent());
                    result = cosUtils.uploadFile(bos, csvInfo.getFileName());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bos.flush();
                    bos.close();
                }
                return result;

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     *
     * @param inData
     * @param randomUUID
     * @param insert
     * @param inertOrUpdate
     * @return
     */
    public GaiaDataSynchronized getGaiaDataSynchronizedByBasic(GetMemberInData inData, String randomUUID, String insert, String inertOrUpdate) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("CLIENT", inData.getClientId());
        map.put("GSMB_MEMBER_ID", randomUUID);
        GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
        aSynchronized.setClient(inData.getClientId());
        aSynchronized.setSite(inData.getBrId());
        aSynchronized.setVersion(IdUtil.randomUUID());
        aSynchronized.setTableName("GAIA_SD_MEMBER_BASIC");
        aSynchronized.setCommand(insert);
        aSynchronized.setSourceNo(inertOrUpdate);
        aSynchronized.setArg(JSON.toJSONString(map));
        return aSynchronized;
    }

    public List<GaiaDataSynchronized> getGaiaDataSynchronizedByBasicList(GetMemberInData inData, String randomUUID, String insert, String inertOrUpdate, List<String> stoCodeList) {
        List<GaiaDataSynchronized> list = new ArrayList<>(stoCodeList.size());
        for (String stoCode : stoCodeList) {
            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", inData.getClientId());
            map.put("GSMB_MEMBER_ID", randomUUID);
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(inData.getClientId());
            aSynchronized.setSite(stoCode);
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_MEMBER_BASIC");
            aSynchronized.setCommand(insert);
            aSynchronized.setSourceNo(inertOrUpdate);
            aSynchronized.setArg(JSON.toJSONString(map));
            list.add(aSynchronized);
        }
        return list;
    }


    /**
     * 同步会员卡
     * @param inData
     * @param member
     * @param insert
     * @param inertOrUpdate
     * @return
     */
    public GaiaDataSynchronized getGaiaDataSynchronizedByCard(GetMemberInData inData, GaiaSdMemberBasic member, String insert, String inertOrUpdate) {
        Map<String, Object> map = new HashMap<>(16);
        map.put("CLIENT", inData.getClientId());
        map.put("GSMBC_MEMBER_ID", member.getGsmbMemberId());
        map.put("GSMBC_CARD_ID", inData.getGmbCardId());
        map.put("GSMBC_BR_ID", inData.getBrId());
        GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
        aSynchronized.setClient(inData.getClientId());
        aSynchronized.setSite(inData.getBrId());
        aSynchronized.setVersion(IdUtil.randomUUID());
        aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
        aSynchronized.setCommand(insert);
        aSynchronized.setSourceNo(inertOrUpdate);
        aSynchronized.setArg(JSON.toJSONString(map));
        return aSynchronized;
    }

    public List<GaiaDataSynchronized> getGaiaDataSynchronizedByCardList(GetMemberInData inData, GaiaSdMemberBasic member,
                String insert, String inertOrUpdate, List<String> stoCodeList) {
        List<GaiaDataSynchronized> list = new ArrayList<>(stoCodeList.size());
        for (String stoCode : stoCodeList) {
            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", inData.getClientId());
            map.put("GSMBC_MEMBER_ID", member.getGsmbMemberId());
            map.put("GSMBC_CARD_ID", inData.getGmbCardId());
            //map.put("GSMBC_BR_ID", stoCode);
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(inData.getClientId());
            aSynchronized.setSite(stoCode);
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
            aSynchronized.setCommand(insert);
            aSynchronized.setSourceNo(inertOrUpdate);
            aSynchronized.setArg(JSON.toJSONString(map));
            list.add(aSynchronized);
        }
        return list;
    }

    /**
     * 会员卡存值
     * @param inData
     * @param storeData
     * @param member
     * @return
     */
    public GaiaSdMemberCardData getGaiaSdMemberCardData(GetMemberInData inData, StoreOutData storeData, GaiaSdMemberBasic member) {
        GaiaSdMemberCardData gaiaSdMemberCardData = new GaiaSdMemberCardData();
        gaiaSdMemberCardData.setClient(inData.getClientId());
        gaiaSdMemberCardData.setGsmbcBrId(inData.getBrId());
        gaiaSdMemberCardData.setGsmbcMemberId(member.getGsmbMemberId());
        gaiaSdMemberCardData.setGsmbcCardId(inData.getGmbCardId());
        gaiaSdMemberCardData.setGsmbcClassId(inData.getGmbCardClass());
        gaiaSdMemberCardData.setGsmbcType("1");
        gaiaSdMemberCardData.setGsmbcChannel("0");
        gaiaSdMemberCardData.setGsmbcStatus("0");
        gaiaSdMemberCardData.setGsmbcIntegral("0");
        gaiaSdMemberCardData.setGsmbcCreateDate(DateUtil.format(new Date(), "yyyyMMdd"));
        gaiaSdMemberCardData.setGsmbcCreateSaler(inData.getRecommender());//推荐人
        gaiaSdMemberCardData.setGsmbcIntegralLastdate(DateUtil.format(new Date(), "yyyyMMdd"));
        gaiaSdMemberCardData.setGsmbcOrgId(StrUtil.isBlank(storeData.getStoChainHead()) ? inData.getBrId() : storeData.getStoChainHead());//门店还是连锁
        gaiaSdMemberCardData.setGsmbcState(inData.getGsmbcState());//门店还是连锁

        return gaiaSdMemberCardData;
    }

    /**
     * 绑定储值卡信息
     * @param inData
     * @param gaiaSdStoreData
     * @return
     */
    public GaiaSdRechargeCard getGaiaSdRechargeCard(GetMemberInData inData, GaiaSdStoreData gaiaSdStoreData) {
        GaiaSdRechargeCard card = new GaiaSdRechargeCard();
        card.setClientId(inData.getClientId());
        card.setGsrcAccountId(inData.getGmbCardId());
        card.setGsrcId(inData.getGmbCardId());
        card.setGsrcBrId(StrUtil.isBlank(gaiaSdStoreData.getGsstOrgId()) ? null : gaiaSdStoreData.getGsstOrgId());
        card.setGsrcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        card.setGsrcEmp(inData.getSaler());
        card.setGsrcStatus("1");
        card.setGsrcName(inData.getGmbName());
        card.setGsrcSex(inData.getGmbSex());
        card.setGsrcMobile(inData.getGmbMobile());
        card.setGsrcTel(inData.getGmbTel());
        card.setGsrcAddress(inData.getGmbAddress());
        card.setGsrcPassword("123456");
        card.setGsrcAmt(new BigDecimal("0"));
        card.setGsrcMemberCardId(inData.getGmbCardId());
        card.setGsrcOrgType(StrUtil.isBlank(gaiaSdStoreData.getGsstOrgType()) ? null : gaiaSdStoreData.getGsstOrgType());
        card.setGsrcOrgId(StrUtil.isBlank(gaiaSdStoreData.getGsstOrgId()) ? null : gaiaSdStoreData.getGsstOrgId());
        return card;
    }

    /**
     * 会员卡是否包含字母
     *
     * @param gmbCardId
     * @return key: 前缀; value: 序号
     */
    private Pair<String, Long> isCardContainEn(String gmbCardId) {
        if (gmbCardId == null) {
            return new Pair<>(null, null);
        }
        try {
            Long.parseLong(gmbCardId);
        } catch (NumberFormatException e) {
            String codePrefix = gmbCardId.substring(0, 2);
            long index = Long.parseLong(gmbCardId.substring(2));
            return new Pair<>(codePrefix, index);
        }
        return new Pair<>(null, null);
    }


    @Override
    public GetMemberOutData findMemberByPrimary(GetMemberInData inData) {
        return this.gaiaSdMemberBasicMapper.selectMemberByPrimary(inData);
    }

    @Override
    public String getIncrMemberCard(String prefix, GetLoginOutData userInfo) {
        String clientId = userInfo.getClient();
        String cardPrefix;
        String result = "";
        if (prefix == null || prefix.equals("")) {
            cardPrefix = "VM";
        } else {
            cardPrefix = prefix;
        }
        if (redisManager.tryLock(SDM_CARD_LOCK, 400)) {
            try {
                //限制同一前缀返回唯一index
                long start1 = System.currentTimeMillis();
                String maxMemberCard = this.gaiaSdMemberBasicMapper.getIncrMemberCard(cardPrefix, clientId);
                Integer index;
                if (StrUtil.isBlank(maxMemberCard)) {
                    index = 10000001;
                } else {
                    index = Integer.parseInt(maxMemberCard.replaceAll(cardPrefix, "")) + 1;
                }
                //比对redis存在的index；(VM是固定写死的前缀); 如果后续前缀变为可配，key末尾累加，map-value修改为list即可
                //新增业务：同一加盟商下id递增
                Map<String, String> map = (Map<String, String>) redisManager.getHash(SDM_CARD_KEY + clientId);
                final int originalIndex = index;
                List<Integer> variableIndex = new ArrayList<>(map.values().size());
                List<Integer> allIndex = new ArrayList<>(map.values().size());
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    String key = entry.getKey();
                    CardOverTime overTime = JSON.parseObject(entry.getValue(), CardOverTime.class);
                    //会员卡判断置位
                    if (LocalDateTime.now().isAfter(overTime.getResetTime()) && !overTime.state) {
                        log.info("会员卡置位, 卡号:{}, 过期截至时间:{}", key, overTime.getResetTime());
                        overTime.setState(true);
                        entry.setValue(JSON.toJSONString(overTime));
                    }
                    allIndex.add(Integer.valueOf(key));
                    if (overTime.state) {
                        variableIndex.add(Integer.valueOf(key));
                    }
                }
                //存在先删了key，后进这个if判断的情况，故改用全局锁
                if (allIndex.contains(index)) {
                    if (CollectionUtils.isEmpty(variableIndex)) {
                        index = allIndex.stream().mapToInt(r -> r).max().getAsInt() + 1;
                    } else {
                        index = getListMin(variableIndex);
                    }
                } else {
                    if (!CollectionUtils.isEmpty(variableIndex)) {
                        index = getListMin(variableIndex);
                    }
                }
                index = judgeIndex(cardPrefix, index, userInfo.getClient(), map, originalIndex, allIndex, variableIndex);
                map.put(String.valueOf(index), JSON.toJSONString(CardOverTime.defaultInstance()));
                redisManager.setHash(SDM_CARD_KEY + clientId, map);
                result = cardPrefix + index;
            } catch (Exception ex) {
                log.error("获取会员卡报错：" + ex.getMessage());
                throw new BusinessException("提示：获取会员卡号失败！");
            } finally {
                redisManager.unLock(SDM_CARD_LOCK);
            }
        } else {
            throw new BusinessException("提示：获取超时，请重新尝试获取会员卡号！");
        }
        log.info("会员卡号获取, clientId:{}, depId:{}, loginName:{}, 卡号:{}", clientId, userInfo.getDepId(),
                userInfo.getLoginName(), result);
        return result;
    }

    private int judgeIndex(String cardPrefix, int index, String client, Map<String, String> cardMap, int originalIndex, List<Integer> allIndex, List<Integer> variableIndex) {
        Example example = new Example(GaiaSdMemberCardData.class);
        example.createCriteria().andEqualTo("gsmbcCardId", cardPrefix + index).andEqualTo("client", client);
        int count = this.gaiaSdMemberCardDataMapper.selectCountByExample(example);
        if (count <= 0) {
            return index;
        }
        cardMap.remove(String.valueOf(index));
        redisManager.deleteHashKey(SDM_CARD_KEY + client, String.valueOf(index));
        log.info("会员卡， 此卡号：{} 已存在，清除缓存，", cardPrefix + index);
        final int temp = index;
        allIndex.removeIf(r -> r.equals(temp));
        variableIndex.removeIf(r -> r.equals(temp));
        index = originalIndex;

        if (allIndex.contains(index)) {
            if (CollectionUtils.isEmpty(variableIndex)) {
                index = allIndex.stream().mapToInt(r -> r).max().getAsInt() + 1;
            } else {
                index = getListMin(variableIndex);
            }
        } else {
            if (!CollectionUtils.isEmpty(variableIndex)) {
                index = getListMin(variableIndex);
            }
        }
        return judgeIndex(cardPrefix, index, client, cardMap, originalIndex, allIndex, variableIndex);
    }

    @Override
    public void addMemberClass(GetLoginOutData userInfo,MemberClassVo lists) {
        List<MemberClass> memberClasses = lists.getList();
        int count = gaiaSdStoreDataMapper.countMemberClass(userInfo.getClient());
        for (MemberClass memberClass1:memberClasses) {
            if (count > 0) {
                //编辑
                memberClass1.setGsmcUpdateEmp(userInfo.getUserId());
                memberClass1.setGsmcUpdateDate(com.gys.util.DateUtil.getCurrentDate());
            }else {
                //新增
                memberClass1.setGsmcCreEmp(userInfo.getUserId());
                memberClass1.setGsmcCreDate(com.gys.util.DateUtil.getCurrentDate());
            }
            //打折率 消费金额 积分 数值
//            boolean flag = true;
//            flag = checkparam(memberClass1);
//            if (flag){
//                memberClass1.setClientId(userInfo.getClient());
//            }else {
//                throw new BusinessException("提示：请正确输入信息！");
//            }

        }
        gaiaSdStoreDataMapper.addMemberClass(memberClasses);

    }

    private boolean checkparam(MemberClass memberClass1) {
        boolean flag;
        flag = ObjectUtil.isNotEmpty(memberClass1.getGsmcIntegralSale());
        flag = ObjectUtil.isNotEmpty(memberClass1.getGsmcAmtSale());
        flag = ObjectUtil.isNotEmpty(memberClass1.getGsmcDiscountRate());
        flag = ObjectUtil.isNotEmpty(memberClass1.getGsmcUpgradeInte());
        flag = ObjectUtil.isNotEmpty(memberClass1.getGsmcUpgradeRech());
        flag = ObjectUtil.isNotEmpty(memberClass1.getGsmcUpgradeSet());
        return flag;
    }


//    @Override
//    @Transactional()
//    public void typeSet(MemberInData inData) {
//
//    }

    private int getListMin(List<Integer> variableIndex) {
        return variableIndex.stream().mapToInt(r -> r).min().getAsInt();
    }

    /**
     * 会员卡卡号--定期置状态位
     * fixDelay上的设置无需过小，毕竟卡时间点未置位的最多15秒后又会被其他人使用
     */
    //@Scheduled(initialDelay = 1234, fixedDelay = 15432)
    public void resetRedisKey() {
        //删除旧数据
        redisManager.delete(SDM_CARD_KEY + "VM");
        Map<String, Object> map = redisManager.getHashByKeyPrefix(SDM_CARD_KEY);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Map<String, String> tempMap = (Map<String, String>)entry.getValue();
            for (Map.Entry<String, String> stringEntry : tempMap.entrySet()) {
                CardOverTime overTime = JSON.parseObject(stringEntry.getValue(), CardOverTime.class);
                if (LocalDateTime.now().isAfter(overTime.getResetTime()) && !overTime.state) {
                    log.info("会员卡置位, 卡号:{}, 过期截至时间:{}", stringEntry.getKey(), overTime.getResetTime());
                    overTime.setState(true);
                    stringEntry.setValue(JSON.toJSONString(overTime));
                }
            }
            redisManager.setHash(key, tempMap);
        }
    }

    @Data
    static class CardOverTime {
        //状态标识位: false 不可用(默认); true 可用
        private boolean state;
        //到期置位时间(一天后)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private LocalDateTime resetTime;

        static CardOverTime defaultInstance() {
            CardOverTime cardOverTime = new CardOverTime();
            cardOverTime.setResetTime(LocalDateTime.now().plusDays(1));
            return cardOverTime;
        }
    }


    @Override
    public JsonResult findMemberListByCondition(MemberConditionInData inData) {
        //获取参数
        int pageNum = inData.getPageNum();
        int pageSize = inData.getPageSize();
        //非空校验 如果为空则赋值默认值
        if(pageNum == 0) {
            pageNum = CommonConstant.PAGE_NUM;
        }
        if(pageSize == 0) {
            pageSize = CommonConstant.PAGE_SIZE;
        }

        //返回参数
        Map<String, Object> result = new HashMap<>(16);
        //分页参数
        PageHelper.startPage(pageNum, pageSize);
        //查询会员列表
        List<MemberListOutData> memberList = gaiaSdMemberBasicMapper.getMemberListByConditionExt(inData);
        if(ValidateUtil.isEmpty(memberList)) {
            return JsonResult.success(result, "success");
        }
        com.github.pagehelper.PageInfo pageInfo = new com.github.pagehelper.PageInfo(memberList);
        List<MemberListOutData> pageList = pageInfo.getList();
        //对当页数据计算各项汇总
        MemberListTotalOutData memberListTotal = new MemberListTotalOutData();
        List<MemberListOutData> brIdList = pageList.stream().
                collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(MemberListOutData :: getBrId))),
                        ArrayList::new));
        BigDecimal totalAmt = pageList.stream().map(MemberListOutData::getTotalAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal yearTotalAmt = pageList.stream().map(MemberListOutData::getYearTotalAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalIntegeral = pageList.stream().map(MemberListOutData::getTotalIntegeral).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal yearTotalIntegeral = pageList.stream().map(MemberListOutData::getYearTotalIntegeral).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalCurrIntegeral = pageList.stream().map(MemberListOutData::getCurrIntegeral).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalCurrCardAmt = pageList.stream().map(MemberListOutData::getCurrCardAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        memberListTotal.setBrName(brIdList.size());   //门店数量
        memberListTotal.setCardId(pageList.size());   //会员卡号数量
        memberListTotal.setTotalAmt(totalAmt);            //累计总金额汇总
        memberListTotal.setYearTotalAmt(yearTotalAmt);      //年度累计金额汇总
        memberListTotal.setTotalIntegeral(totalIntegeral);          //累计积分汇总
        memberListTotal.setYearTotalIntegeral(yearTotalIntegeral);   //年度累计积分汇总
        memberListTotal.setCurrIntegeral(totalCurrIntegeral);       //当前积分汇总
        memberListTotal.setCurrCardAmt(totalCurrCardAmt);           //储值卡余额汇总

        result.put("memberList", pageInfo);
        result.put("total", memberListTotal);
        return JsonResult.success(result, "success");
    }

    private void updateMemberIntegral(GetMemberInData inData) {
        String channel = inData.getChannel();
        //如果渠道不为1的时候， 直接返回  只有渠道为1时 才可以进行以下逻辑（修改会员积分）
        if(!"1".equals(channel)) {
            return;
        }
        String integral = inData.getIntegral();
        String cardId = inData.getGmbCardId();
        if (ValidateUtil.isEmpty(integral)) {
            throw new BusinessException("提示：请输入积分");
        }
        if(ValidateUtil.isEmpty(cardId)) {
            throw new BusinessException("提示：修改积分时会员卡号不可为空");
        }

        //查询当前积分
        GaiaSdMemberCardToData memberCard = new GaiaSdMemberCardToData();
        memberCard.setClient(inData.getClientId());
        //memberCard.setGsmbcBrId(inData.getBrId());
        memberCard.setGsmbcMemberId(inData.getMemberId());
        memberCard.setGsmbcCardId(cardId);
        GaiaSdMemberCardData cardData = gaiaSdMemberBasicMapper.getCurrIntegralByCondition(memberCard);
        if(ValidateUtil.isEmpty(cardData)) {
            throw new BusinessException("提示：该会员卡号不存在");
        }
        //获取当前积分
        String resultIntegral = cardData.getGsmbcIntegral();
        //处理查询出的当前积分
        BigDecimal currIntegral = ValidateUtil.isEmpty(resultIntegral) ? BigDecimal.ZERO : new BigDecimal(resultIntegral);
        //处理传入的修改积分
        BigDecimal updateIntegral = new BigDecimal(integral);
        updateIntegral = ValidateUtil.isEmpty(updateIntegral) ? BigDecimal.ZERO : updateIntegral;
        //使用当前积分比较修改后的积分 如果相等 则不修改积分  如果不相等则修改积分
        if(currIntegral.compareTo(updateIntegral) == 0) {
            return;
        }
        //修改积分
        memberCard.setGsmbcIntegral(integral);
        memberCard.setGsmbcIntegralLastdate(CommonUtil.getyyyyMMdd());
        gaiaSdMemberBasicMapper.updateCurrIntegralByCondition(memberCard);

        //插入异动表记录
        GaiaSdIntegralChange changeRecord = new GaiaSdIntegralChange();
        changeRecord.setClient(inData.getClientId());
        changeRecord.setGsicBrId(cardData.getGsmbcBrId());
        changeRecord.setGsicVoucherId(gaiaSdIntegralChangeMapper.getNextVoucherId(inData.getClientId()));
        changeRecord.setGsicFlag("2");
        changeRecord.setGsicDate(CommonUtil.getyyyyMMdd());
        changeRecord.setGsicType("5");
        changeRecord.setGsicCardId(cardId);
        changeRecord.setGsicInitialIntegral(currIntegral);   //初始积分
        changeRecord.setGsicChangeIntegral(updateIntegral.subtract(currIntegral));    //异动积分
        changeRecord.setGsicResultIntegral(updateIntegral);    //结果积分
        changeRecord.setGsicEmp(inData.getUpdateEmp());
        //插入积分字段待定
        gaiaSdIntegralChangeMapper.insert(changeRecord);
    }

    public List<GaiaSdSaleD> getControlProNumByMember(GetMemberInData param) {
        return gaiaSdMemberBasicMapper.getControlProNumByMember(param);
    }

    @Override
    @Transactional
    public Boolean activateMemberCard(GetMemberInData inData) {

        // 查询出所有门店
        List<StoreOutData> storeList = storeDataService.getStoreList(inData.getClientId(), null);
        List<String> stoCodeList = storeList.stream().map(r -> r.getStoCode()).collect(Collectors.toList());
        GaiaSdMemberBasic memberTemp = new GaiaSdMemberBasic();
        memberTemp.setClientId(inData.getClientId());
        memberTemp.setGsmbMemberId(inData.getMemberId());
        //memberTemp.setGsmbMobile(inData.getGmbMobile());
        GaiaSdMemberBasic member = this.gaiaSdMemberBasicMapper.selectOne(memberTemp);
        GaiaSdMemberCardData gaiaSdMemberCard = this.gaiaSdMemberBasicMapper.selectMembersByMemerId(inData.getClientId(), inData.getBrId(), inData.getMemberId(), inData.getGmbCardId());
        if (gaiaSdMemberCard != null) {
            gaiaSdMemberCard.setGsmbcClassId(inData.getGsmcId());
            gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(new Date(), "yyyyMMdd"));
            gaiaSdMemberCard.setGsmbcState(inData.getGsmbcState());
            gaiaSdMemberCard.setGsmbcJhUser(inData.getGsmbcJhUser());
            gaiaSdMemberCard.setGsmbcJhDate(inData.getGsmbcJhDate());
            gaiaSdMemberCard.setGsmbcJhTime(inData.getGsmbcJhTime());
            this.gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);
            List<GaiaDataSynchronized> synchronizedByCardList = getGaiaDataSynchronizedByCardList(inData, member, "update", "会员卡激活", stoCodeList);
            synchronizedService.insertLists(synchronizedByCardList);
        }
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("editMemberById");
        mqReceiveData.setData(gaiaSdMemberCard);
        for (String stoCode : stoCodeList) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + stoCode, JSON.toJSON(mqReceiveData));
        }
        return true;
    }

    @Override
    public ArrayList<HashMap<String,String>> getMemberCardStatus() {
        return getStatusList();
    }

    @Override
    @Transactional
    public void uncouplingMemberCard(GetLoginOutData userInfo, String cardId, String remark) {
        // 根据会员卡号查出对应会员卡
        GaiaSdMemberCard gaiaSdMemberCard = gaiaSdMemberCardDataMapper.queryMemberCardByCardId(userInfo.getClient(), cardId);
        if (ObjectUtils.isEmpty(gaiaSdMemberCard)) {
            throw new BusinessException("不存在该会员卡");
        }
        if (!StringUtils.equals(gaiaSdMemberCard.getGsmbcStatus(), MEMBER_CARD_STATUS_LOSS.code)) {
            throw new BusinessException("当前并非挂失状态，无法解挂");
        }
        // 解挂
        gaiaSdMemberCardDataMapper.updateMemberCardStatus(MEMBER_CARD_STATUS_USABLE.code, userInfo.getClient(), cardId);
        log.info("推送会员卡解挂信息，FX begin========");
        GaiaSdMemberBasic pushMember = gaiaSdMemberBasicMapper.selectBasicByCardId(userInfo.getClient(), gaiaSdMemberCard.getGsmbcBrId(), cardId);
        GaiaSdMemberCardData pushCard = new GaiaSdMemberCardData();
        BeanUtils.copyProperties(gaiaSdMemberCard,pushCard);
        pushCard.setGsmbcStatus(MEMBER_CARD_STATUS_USABLE.code);   // 修改为可以状态
        if (ObjectUtils.isEmpty(pushMember) || ObjectUtils.isEmpty(pushCard)){
            throw new BusinessException("提示：同步修改会员信息失败");
        }
        MemberAddDto dto = new MemberAddDto(pushMember, pushCard);
        pushMemberInfo(userInfo.getClient(), dto);
        // 将解挂记录在GAIA_SD_MEMBER_CARD_CHANGE会员卡变动表   卡类型 1-挂失 2-解挂 3-换卡（注销）
        GaiaSdMemberCardChange gaiaSdMemberCardChange = new GaiaSdMemberCardChange(null
                , userInfo.getClient()
                , userInfo.getDepId()
                , gaiaSdMemberCard.getGsmbcMemberId()
                , ""
                , cardId
                , ""
                , ""
                , "2"
                , remark
                , userInfo.getUserId()
                , new Date());
        memberCardChangeMapper.insert(gaiaSdMemberCardChange);

    }

    @Override
    public UncouplingMemberCardInfo getUncouplingMemberCardInfo(GetLoginOutData userInfo, String cardId) {
        return memberCardChangeMapper.queryUncouplingMemberCardInfo(userInfo.getClient(), cardId);
    }



    @Override
    @Transactional
    public JsonResult cardLose(MemberCardLoseInData inData) {
        String clientId = inData.getClientId();
        String memberId = inData.getMemberId();
        String oldCardId = inData.getOldCardId();
        if(ValidateUtil.isEmpty(memberId)) {
            throw new BusinessException("提示：会员编号不能为空！");
        }
        if(ValidateUtil.isEmpty(oldCardId)) {
            throw new BusinessException("提示：原会员卡号不能为空！");
        }
        //查询会员卡是否存在
        checkMemberCardInfo(clientId, oldCardId);

        inData.setType("2");  //挂失
        gaiaSdMemberBasicMapper.updateCardStatus(inData);

        //插入变更记录表
        GaiaSdMemberCardChange memberCardChange = new GaiaSdMemberCardChange();
        memberCardChange.setClient(clientId);
        memberCardChange.setBrId(inData.getBrId());
        memberCardChange.setOldMemberId(memberId);
        memberCardChange.setNewMemberId("");
        memberCardChange.setOldCardId(oldCardId);
        memberCardChange.setInitCardId("");
        memberCardChange.setNewCardId("");
        memberCardChange.setCardType("1");    //1-挂失
        memberCardChange.setRemark((ValidateUtil.isEmpty(inData.getRemark())) ? "挂失" : inData.getRemark());
        memberCardChange.setEmpId(inData.getEmpId());
        memberCardChange.setUpdateTime(new Date());
        memberCardChangeMapper.insert(memberCardChange);


        //推送更新会员信息
        log.info("推送更新会员信息，FX begin========");
        GaiaSdMemberBasic tempMember = new GaiaSdMemberBasic();
        tempMember.setClientId(clientId);
        tempMember.setGsmbMemberId(memberId);
        GaiaSdMemberBasic pushMemberInfo = gaiaSdMemberBasicMapper.selectByPrimaryKey(tempMember);
        GaiaSdMemberCardData tempMemberCard = new GaiaSdMemberCardData();
        tempMemberCard.setClient(clientId);
        tempMemberCard.setGsmbcMemberId(memberId);
        tempMemberCard.setGsmbcCardId(oldCardId);
        GaiaSdMemberCardData pushMemberCardInfo = gaiaSdMemberCardDataMapper.selectByPrimaryKey(tempMemberCard);
        if(ValidateUtil.isEmpty(pushMemberInfo) || ValidateUtil.isEmpty(pushMemberCardInfo)) {
            log.error("原会员basic：" + pushMemberInfo);
            log.error("原会员card：" + pushMemberCardInfo);
            throw new BusinessException("提示：同步会员消息失败！");
        }
        MemberAddDto oldMemberInfoDto = new MemberAddDto(pushMemberInfo, pushMemberCardInfo);
        pushMemberInfo(clientId, oldMemberInfoDto);
        log.info("推送更新会员信息，FX end========");
        return JsonResult.success("", "success");
    }


    private GaiaSdMemberCardData checkMemberCardInfo(String clientId, String oldCardId) {
        //查询会员卡是否存在
        List<GaiaSdMemberCardData> memberCardList = gaiaSdMemberBasicMapper.getMemberCardList(clientId, oldCardId);
        int cardCount = memberCardList.size();
        if(cardCount == 0) {
            throw new BusinessException("提示：该会员卡不存在，请检查卡号！");
        }
        if(cardCount > 1) {
            throw new BusinessException("提示：该会员卡存在多条记录，请检查数据！");
        }
        //获取唯一记录返回
        return memberCardList.get(0);
    }


    @Override
    @Transactional
    public JsonResult cardChange(MemberCardLoseInData inData) {
        String clientId = inData.getClientId();
        String oldMemberId = inData.getMemberId();
        String oldCardId = inData.getOldCardId();    //原会员卡号
        String newCardId = inData.getNewCardId();    //新会员卡号
        if(ValidateUtil.isEmpty(oldMemberId)) {
            throw new BusinessException("提示：会员编号不能为空！");
        }
        if(ValidateUtil.isEmpty(oldCardId)) {
            throw new BusinessException("提示：原会员卡号不能为空！");
        }
        if(ValidateUtil.isEmpty(newCardId)) {
            throw new BusinessException("提示：新会员卡号不能为空！");
        }

        //--------------------------------校验会员信息--------------------------------
        //查询会员是否存在
        GaiaSdMemberBasic oldMemberInfo = new GaiaSdMemberBasic();
        oldMemberInfo.setClientId(clientId);
        oldMemberInfo.setGsmbMemberId(oldMemberId);
        GaiaSdMemberBasic memberBasic = gaiaSdMemberBasicMapper.selectByPrimaryKey(oldMemberInfo);
        if(ValidateUtil.isEmpty(memberBasic)) {
            throw new BusinessException("提示：该会员不存在，无法换卡！");
        }
        /*GetMemberInData getMemberInData = new GetMemberInData();
        getMemberInData.setClientId(inData.getClientId());
        getMemberInData.setBrId(inData.getBrId());
        getMemberInData.setGmbMobile(memberBasic.getGsmbMobile());
        getMemberInData.setGmbStatus("0");
        if (this.gaiaSdMemberBasicMapper.selectMembersByCondition(getMemberInData).size() > 1) {
            throw new BusinessException("提示：该手机号已存在，请检查数据！");
        }*/

        //--------------------------------校验会员卡信息--------------------------------
        //查询会员卡是否存在
        GaiaSdMemberCardData cardData = checkMemberCardInfo(clientId, oldCardId);
        //使用新卡号去查询 如果使用新卡号查询到已经存在 则无法新增 提示新卡号已经存在
        List<GaiaSdMemberCardData> memberCardList = gaiaSdMemberBasicMapper.getMemberCardList(clientId, newCardId);
        if(ValidateUtil.isNotEmpty(memberCardList)) {
            throw new BusinessException("提示：新卡号为已激活的卡，请更换其他卡号！");
        }

        //--------------------------------校验电子券信息--------------------------------
        //使用新会员卡号去查询该会员卡信息在电子券中是否存在 如果存在 则不允许更新
        List<GaiaSdElectronChange> checkElectronChangeList = electronChangeMapper.getListByMemberCardId(clientId, newCardId);
        if(ValidateUtil.isNotEmpty(checkElectronChangeList)) {
            throw new BusinessException("提示：新会员卡号在电子券中已经存在绑定关系，请更换其他卡号！");
        }

        //--------------------------------校验储值卡信息--------------------------------
        GaiaSdRechargeCard checkRechargeCardInfo = gaiaSdRechargeCardMapper.getAccountByMemberCardId(clientId, newCardId);
        if(ValidateUtil.isNotEmpty(checkRechargeCardInfo)) {
            throw new BusinessException("提示：新会员卡号在储值卡中已经存在绑定关系，请更换其他卡号！");
        }

        //--------------------------------更新并新增会员信息--------------------------------
        //将原会员改为停用状态
        oldMemberInfo.setGsmbStatus("1");   //1-停用
        oldMemberInfo.setGsmbUpdateBrId(inData.getBrId());
        oldMemberInfo.setGsmbUpdateSaler(inData.getEmpId());
        oldMemberInfo.setGsmbUpdateDate(CommonUtil.getyyyyMMdd());
        gaiaSdMemberBasicMapper.updateMember(oldMemberInfo);
        //生成新会员
        String newMemberId = UUID.randomUUID().toString().replace("-", "");   //生成新会员编号
        memberBasic.setGsmbMemberId(newMemberId);
        memberBasic.setGsmbStatus("0");   //0-正常
        memberBasic.setGsmbUpdateBrId(inData.getBrId());
        memberBasic.setGsmbUpdateSaler(inData.getEmpId());
        memberBasic.setGsmbUpdateDate(CommonUtil.getyyyyMMdd());
        gaiaSdMemberBasicMapper.insertMember(memberBasic);


        //--------------------------------更新并新增会员卡信息--------------------------------
        inData.setType("3");  //换卡（注销）
        //将原会员卡改为注销状态
        gaiaSdMemberBasicMapper.updateCardStatus(inData);
        //生成新会员 以及 新会员卡号
        cardData.setGsmbcMemberId(newMemberId);
        cardData.setGsmbcCardId(newCardId);
        cardData.setGsmbcStatus("0");
        gaiaSdMemberCardDataMapper.insert(cardData);

        //插入时 先查询用原卡号查询 新号卡 是否存在换卡记录 如果不存在 则原卡号赋值到初始卡号中
        GaiaSdMemberCardChange historyChangeRecord = memberCardChangeMapper.getCardChangeByNewCardId(clientId, oldCardId);
        String initCardId = null;
        if(ValidateUtil.isEmpty(historyChangeRecord)) {
            initCardId = oldCardId;
        } else {
            initCardId = historyChangeRecord.getInitCardId();
        }
        //新增会员卡换卡记录
        GaiaSdMemberCardChange memberCardChange = new GaiaSdMemberCardChange();
        memberCardChange.setClient(clientId);
        memberCardChange.setBrId(inData.getBrId());
        memberCardChange.setOldMemberId(oldMemberId);
        memberCardChange.setNewMemberId(newMemberId);
        memberCardChange.setOldCardId(oldCardId);
        memberCardChange.setInitCardId(initCardId);
        memberCardChange.setNewCardId(newCardId);
        memberCardChange.setCardType("3");    //3-换卡
        memberCardChange.setRemark((ValidateUtil.isEmpty(inData.getRemark())) ? "换卡" : inData.getRemark());
        memberCardChange.setEmpId(inData.getEmpId());
        memberCardChange.setUpdateTime(new Date());
        memberCardChangeMapper.insert(memberCardChange);

        //--------------------------------电子券更新会员卡号--------------------------------
        List<GaiaSdElectronChange> electronChangeList = electronChangeMapper.getListByMemberCardId(clientId, oldCardId);
        if(ValidateUtil.isNotEmpty(electronChangeList)) {
            electronChangeMapper.updateByMemberCardId(clientId, oldCardId, newCardId);
        }

        //--------------------------------储值卡更新会员卡号--------------------------------
        GaiaSdRechargeCard rechargeCardInfo = gaiaSdRechargeCardMapper.getAccountByMemberCardId(clientId, oldCardId);
        if(ValidateUtil.isNotEmpty(rechargeCardInfo)) {
            gaiaSdRechargeCardMapper.updateByNewCardId(clientId, oldCardId, newCardId);
        }

        //推送更新会员信息
        log.info("推送更新会员信息，FX begin========");
        GaiaSdMemberBasic tempMember = new GaiaSdMemberBasic();
        tempMember.setClientId(clientId);
        tempMember.setGsmbMemberId(oldMemberId);
        GaiaSdMemberBasic pushMemberInfo = gaiaSdMemberBasicMapper.selectByPrimaryKey(tempMember);

        GaiaSdMemberCardData tempMemberCard = new GaiaSdMemberCardData();
        tempMemberCard.setClient(clientId);
        tempMemberCard.setGsmbcMemberId(oldMemberId);
        tempMemberCard.setGsmbcCardId(oldCardId);
        GaiaSdMemberCardData pushMemberCardInfo = gaiaSdMemberCardDataMapper.selectByPrimaryKey(tempMemberCard);
        if(ValidateUtil.isEmpty(pushMemberInfo) || ValidateUtil.isEmpty(pushMemberCardInfo)) {
            log.error("原会员basic：" + pushMemberInfo);
            log.error("原会员card：" + pushMemberCardInfo);
            throw new BusinessException("提示：同步原会员消息失败！");
        }

        MemberAddDto oldMemberInfoDto = new MemberAddDto(pushMemberInfo, pushMemberCardInfo);
        pushMemberInfo(clientId, oldMemberInfoDto);
        log.info("推送更新会员信息，FX end========");

        //推送新增会员信息
        log.info("推送新增会员信息，FX begin========");
        MemberAddDto newMemberInfoDto = new MemberAddDto(memberBasic, cardData);
        pushMemberInfo(clientId, newMemberInfoDto);
        log.info("推送新增会员信息，FX end========");

        Map<String, Object> result = new HashMap<>();
        result.put("oldMember", oldMemberInfoDto);
        result.put("newMember", newMemberInfoDto);
        return JsonResult.success(result, "success");
    }


    @Override
    public JsonResult getMemberAreaList(GetLoginOutData userInfo) {
        GaiaSdMemberArea inData = new GaiaSdMemberArea();
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        List<GaiaSdMemberArea> areaList = memberAreaMapper.getMemberAreaList(inData);
        return JsonResult.success(areaList, "success");
    }

    @Override
    @Transactional
    public void editMemberIntegral(MemberIntegralInData inData) {
        List<StoreOutData> storeList = storeDataService.getStoreList(inData.getClient(), null);
        List<String> stoCodeList = storeList.stream().map(r -> r.getStoCode()).collect(Collectors.toList());
        String memberId = inData.getMemberId().trim();
        String integral = inData.getIntegral();
        String cardId = inData.getCardId();
        String type = inData.getType();
        if(ValidateUtil.isEmpty(memberId)) {
            throw new BusinessException("会员编号不能为空");
        }
        if (ValidateUtil.isEmpty(integral)) {
            throw new BusinessException("请输入积分");
        }
        if (ValidateUtil.isEmpty(type)) {
            throw new BusinessException("请选择操作积分类型");
        }
        if (!"0".equals(type)&&!"1".equals(type)) {
            throw new BusinessException("操作积分类型不支持");
        }
        if(ValidateUtil.isEmpty(cardId)) {
            throw new BusinessException("修改积分时会员卡号不可为空");
        }
        //查询当前积分
        GaiaSdMemberCardToData memberCard = new GaiaSdMemberCardToData();
        memberCard.setClient(inData.getClient());
        memberCard.setGsmbcMemberId(inData.getMemberId());
        memberCard.setGsmbcCardId(cardId);
        GaiaSdMemberCardData cardData = gaiaSdMemberBasicMapper.getCurrIntegralByCondition(memberCard);
        if(ValidateUtil.isEmpty(cardData)) {
            throw new BusinessException("该会员卡号不存在");
        }
        if(!MEMBER_CARD_STATUS_USABLE.code.equals(cardData.getGsmbcStatus())){
            throw new BusinessException("该会员卡状态不为可用状态，不允许修改积分");
        }
        //处理查询出的当前积分
        BigDecimal currIntegral = ValidateUtil.isEmpty(cardData.getGsmbcIntegral()) ? BigDecimal.ZERO : new BigDecimal(cardData.getGsmbcIntegral());
        if(!integral.matches("^[0-9]+(.[0-9]+)?$")){
            throw new BusinessException("修改的积分格式不正确！");
        }
        //处理传入的修改积分
        BigDecimal updateIntegral = new BigDecimal(integral);
        updateIntegral = ValidateUtil.isEmpty(updateIntegral) ? BigDecimal.ZERO : updateIntegral;
        //修改积分为0,,跳过
        if(BigDecimal.ZERO.compareTo(updateIntegral) == 0) {
            return;
        }
        BigDecimal resutlIntegral = null;
        //
        if("1".equals(type)){
            updateIntegral = BigDecimal.ZERO.subtract(updateIntegral);
        }
        resutlIntegral = currIntegral.add(updateIntegral);
        if(resutlIntegral.toString().length() > 10){
            throw new BusinessException("修改后的积分值，数值太大！");
        }
        if(-1 == resutlIntegral.compareTo(BigDecimal.ZERO)){
            throw new BusinessException("修改后的积分值，不允许小于0，请重新积分修改！");
        }
        //修改积分
        memberCard.setGsmbcIntegral(resutlIntegral.toString());
        memberCard.setGsmbcIntegralLastdate(CommonUtil.getyyyyMMdd());
        gaiaSdMemberBasicMapper.updateCurrIntegralByCondition(memberCard);

        //插入异动表记录
        GaiaSdIntegralChange changeRecord = new GaiaSdIntegralChange();
        changeRecord.setClient(inData.getClient());
        changeRecord.setGsicBrId(cardData.getGsmbcBrId());
        changeRecord.setGsicVoucherId(gaiaSdIntegralChangeMapper.getNextVoucherId(inData.getClient()));
        changeRecord.setGsicFlag("2");
        changeRecord.setGsicDate(CommonUtil.getyyyyMMdd());
        changeRecord.setGsicType("5");
        changeRecord.setGsicCardId(cardId);
        changeRecord.setGsicInitialIntegral(currIntegral);   //初始积分
        changeRecord.setGsicChangeIntegral(updateIntegral);    //异动积分

        changeRecord.setGsicResultIntegral(resutlIntegral);    //结果积分
        changeRecord.setGsicEmp(inData.getUpdateEmp());
        changeRecord.setRemark(inData.getRemark());
        //插入积分字段待定
        gaiaSdIntegralChangeMapper.insertSelective(changeRecord);

        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("editMember");
        mqReceiveData.setData(inData.getCardId());
        for (String stoCode : stoCodeList) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + stoCode, JSON.toJSON(mqReceiveData));
        }
    }
    @Override
    @Transactional
    public void cancelMemberCard(MemberCardCancelInData inData) {
        // 根据会员卡号查出对应会员卡
        String memberId = inData.getMemberId().trim();
        String  cardId = inData.getCardId().trim();
        if(ValidateUtil.isEmpty(memberId)) {
            throw new BusinessException("会员编号不能为空");
        }
        if(ValidateUtil.isEmpty(cardId)) {
            throw new BusinessException("修改积分时会员卡号不可为空");
        }
        GaiaSdMemberCardToData memberCard = new GaiaSdMemberCardToData();
        memberCard.setClient(inData.getClient());
        memberCard.setGsmbcMemberId(memberId);
        memberCard.setGsmbcCardId(cardId);
        GaiaSdMemberCardData cardData = gaiaSdMemberBasicMapper.getCurrIntegralByCondition(memberCard);
        if (ObjectUtils.isEmpty(cardData)) {
            throw new BusinessException("不存在该会员卡");
        }
        if (!StringUtils.equals(cardData.getGsmbcStatus(), MEMBER_CARD_STATUS_LOSS.code)&&!StringUtils.equals(cardData.getGsmbcStatus(), MEMBER_CARD_STATUS_USABLE.code)) {
            throw new BusinessException("当前并非挂失或可用状态，无法注销");
        }
        // 注销
        gaiaSdMemberCardDataMapper.updateMemberCardStatus(MEMBER_CARD_STATUS_CANCEL.code, inData.getClient(), cardId);
        log.info("推送会员卡解挂信息，FX begin========");
        GaiaSdMemberBasic pushMember = gaiaSdMemberBasicMapper.selectBasicByCardId(inData.getClient(), cardData.getGsmbcBrId(), cardId);
        GaiaSdMemberCardData pushCard = new GaiaSdMemberCardData();
        BeanUtils.copyProperties(cardData,pushCard);
        pushCard.setGsmbcStatus(MEMBER_CARD_STATUS_CANCEL.code);   // 修改为注销状态
        if (ObjectUtils.isEmpty(pushMember) || ObjectUtils.isEmpty(pushCard)){
            throw new BusinessException("同步修改会员信息失败");
        }
        MemberAddDto dto = new MemberAddDto(pushMember, pushCard);
        pushMemberInfo(inData.getClient(), dto);
        // 将注销记录在GAIA_SD_MEMBER_CARD_CHANGE会员卡变动表   卡类型 1-挂失 2-解挂 3-换卡（注销）
        GaiaSdMemberCardChange gaiaSdMemberCardChange = new GaiaSdMemberCardChange(null
                , inData.getClient()
                , inData.getDepId()
                , cardData.getGsmbcMemberId()
                , ""
                , cardId
                , ""
                , ""
                , "3"
                , inData.getRemark()
                , inData.getEmpId()
                , new Date());
        memberCardChangeMapper.insertSelective(gaiaSdMemberCardChange);

    }
}
