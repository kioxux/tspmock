package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaClientBenefitLevelInfoMapper;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaSdStoreEssentialProductParaMapper;
import com.gys.business.mapper.entity.GaiaClientBenefitLevelInfo;
import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.mapper.entity.GaiaSdStoreEssentialProductPara;
import com.gys.business.service.GaiaClientBenefitLevelInfoService;
import com.gys.business.service.data.GaiaClientBenefitLevelInfoData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GaiaClientBenefitLevelInfoExcelData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.common.vo.GaiaClientBenefitLevelInfoVo;
import com.gys.util.CosUtils;
import com.gys.util.DateUtil;
import com.gys.util.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author csm
 * @date 2021/12/16 - 16:45
 */
@Service
@Slf4j
public class GaiaClientBenefitLevelInfoServiceImple implements GaiaClientBenefitLevelInfoService {
    @Resource
    private GaiaClientBenefitLevelInfoMapper gaiaClientBenefitLevelInfoMapper;
    @Resource
    private GaiaSdStoreEssentialProductParaMapper gaiaSdStoreEssentialProductParaMapper;
    @Resource
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;
    @Resource
    public CosUtils cosUtils;
    @Override
    public List<GaiaClientBenefitLevelInfoVo> getByClient(GaiaClientBenefitLevelInfoData gcliData) {
        List<GaiaClientBenefitLevelInfoVo> gaiaClientLevelInfoVos = gaiaClientBenefitLevelInfoMapper.selectByClient(gcliData.getClientIds());
        if(CollectionUtils.isEmpty(gaiaClientLevelInfoVos)){
            String s = gcliData.getClientIds().get(0);
            List<GaiaFranchisee> allClients = gaiaFranchiseeMapper.getAllByClient(s);
            List<GaiaSdStoreEssentialProductPara> clientLevels = gaiaSdStoreEssentialProductParaMapper.getAll();
            for(GaiaSdStoreEssentialProductPara gp : clientLevels){
                GaiaClientBenefitLevelInfoVo gaiaClientLevelInfoVo = new GaiaClientBenefitLevelInfoVo();
                gaiaClientLevelInfoVo.setClient(s);
                gaiaClientLevelInfoVo.setFrancName(allClients.get(0).getFrancName());
                gaiaClientLevelInfoVo.setComprehensiveRank(gp.getComprehensiveRank());
                gaiaClientLevelInfoVo.setStockQualifiedCount(gp.getStockQualifiedCount());
                gaiaClientLevelInfoVo.setStepValue(gp.getStepValue());
                gaiaClientLevelInfoVo.setStoreLevel(gp.getStoreLevel());
                gaiaClientLevelInfoVos.add(gaiaClientLevelInfoVo);
            }
        }else{
            return gaiaClientLevelInfoVos;
        }
        return gaiaClientLevelInfoVos;
    }

    @Override
    public void update(GetLoginOutData userInfo, GaiaClientBenefitLevelInfoData gcbld) {
        //根据client获取数据
        List<GaiaClientBenefitLevelInfoVo> vos = gaiaClientBenefitLevelInfoMapper.selectByClient(gcbld.getClientIds());
        //获取用户提交的数据
        List<GaiaClientBenefitLevelInfo> list1 = gcbld.getList();

        List<GaiaClientBenefitLevelInfo> list = list1.stream().map(gb -> {
            gb.setClient(gcbld.getClientIds().get(0));
            gb.setUpdateTime(new Date());
            gb.setUpdateUser(userInfo.getUserId());
            gb.setIsDelete(0);
            return gb;
        }).collect(Collectors.toList());

        //如果vos为空，走新增。不为空走更新
        if( CollectionUtils.isEmpty(vos)){
            gaiaClientBenefitLevelInfoMapper.insertList(list);
        }else {
          for(GaiaClientBenefitLevelInfo gaiaClientLevelInfo : list){
              if(StringUtils.isEmpty(gaiaClientLevelInfo.getComprehensiveRank())){
                  throw new BusinessException("综合排名上限值不能为空");
              }
              if(StringUtils.isEmpty(gaiaClientLevelInfo.getStepValue())){
                  throw new BusinessException("铺货品项数上限值不能为空");
              }
              if(StringUtils.isEmpty(gaiaClientLevelInfo.getStockQualifiedCount())){
                  throw new BusinessException("合格品项项数不能为空");
              }else if(gaiaClientLevelInfo.getStockQualifiedCount()>gaiaClientLevelInfo.getComprehensiveRank()){
                  throw new BusinessException("合格品项项数不能大于综合排名上限值");
              }
              gaiaClientBenefitLevelInfoMapper.update(gaiaClientLevelInfo);
          }
        }

    }

    @Override
    public List<GaiaClientBenefitLevelInfo> getDefaultValues() {
        List<GaiaSdStoreEssentialProductPara> gaiaSdStoreEssentialProductParas = gaiaSdStoreEssentialProductParaMapper.selectList();
        List<GaiaClientBenefitLevelInfo> list = new ArrayList<>();
        for(GaiaSdStoreEssentialProductPara gssepp : gaiaSdStoreEssentialProductParas){
            GaiaClientBenefitLevelInfo gaiaClientLevelInfo = new GaiaClientBenefitLevelInfo();
            gaiaClientLevelInfo.setComprehensiveRank(gssepp.getComprehensiveRank());
            gaiaClientLevelInfo.setStockQualifiedCount(gssepp.getStockQualifiedCount());
            gaiaClientLevelInfo.setStepValue(gssepp.getStepValue());
            gaiaClientLevelInfo.setStoreLevel(gssepp.getStoreLevel());
            list.add(gaiaClientLevelInfo);
        }
        return list;
        }

    @Override
    public Result exportClientLevel(String client) {
        List<GaiaClientBenefitLevelInfoExcelData> collect = gaiaClientBenefitLevelInfoMapper.getClientLevelExcelData(client);
        if(CollectionUtils.isEmpty(collect)){
            throw new BusinessException("未查到相关数据！");
        }
        for (int i = 0; i < collect.size(); i++) {
            collect.get(i).setIndex(i + 1);
        }

        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>(collect.size());
        for (GaiaClientBenefitLevelInfoExcelData gcblied : collect) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //序号
            lineList.add(gcblied.getIndex());
            //客户加盟ID
            lineList.add(gcblied.getClient());
            //加盟商名称
            lineList.add(gcblied.getFrancName());
            //DX0002-店效效级别
            lineList.add(gcblied.getStoreLevel());
            //综合排名上限值
            lineList.add(gcblied.getComprehensiveRank());
            //合格品项项数
            lineList.add(gcblied.getStockQualifiedCount());
            //铺货品项数上限值
            lineList.add(gcblied.getStepValue());
            //修改人
            lineList.add(gcblied.getUpdateUser());
            //更新时间
            lineList.add(DateUtil.dateToString(gcblied.getUpdateTime(),"yyyy-MM-DD"));
            dataList.add(lineList);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.GAIA_CLIENT_BENEFIT_lEVEL_INFO);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.GAIA_CLIENT_BENEFIT_lEVEL_INFO_NAME);
                }});
        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.GAIA_CLIENT_BENEFIT_lEVEL_INFO_NAME + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}", e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}", e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }
}
