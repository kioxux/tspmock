package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "月销售任务列表返回参数", description = "")
@Data
public class SalesTaskOutData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码")
    private String BrId;
    @ApiModelProperty(value = "任务名称 新建修改都是同一个名字")
    private String staskTaskName;
    @ApiModelProperty(value = "店名简称")
    private String brName;
    @ApiModelProperty(value = "任务时间 起始日期")
    private String staskStartDate;
    @ApiModelProperty(value = "任务时间 结束日期")
    private String staskEndDate;
    @ApiModelProperty(value = "期间天数")
    private String monthDays;
    @ApiModelProperty(value = "任务店数")
    private String shopNumber;
    @ApiModelProperty(value = "营业额")
    private String dailyPayAmt;
    @ApiModelProperty(value = "毛利额")
    private String grossProfit;
    @ApiModelProperty(value = "毛利率")
    private String grossMargin;
    @ApiModelProperty(value = "会员卡")
    private String membershipCard;
    @ApiModelProperty(value = "创建人姓名")
    private String userName;
    @ApiModelProperty(value = "创建日期")
    private String creationDate;
    @ApiModelProperty(value = "状态(S已保存，P已下发)")
    private String staskSplanSta;

}
