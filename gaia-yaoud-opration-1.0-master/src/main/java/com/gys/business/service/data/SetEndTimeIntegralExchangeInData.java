package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/12/1 16:29
 */
@Data
public class SetEndTimeIntegralExchangeInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    @NotBlank(message = "提示：请选择单号")
    private String voucherId;

    /**
     * 结束时间
     */
    @NotNull(message = "提示：请选择结束时间")
    private Date endDate;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改时间
     */
    private Date updateTime;
}
