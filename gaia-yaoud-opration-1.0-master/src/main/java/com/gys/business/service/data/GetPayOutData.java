//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;
import java.util.List;

public class GetPayOutData {
    private String clientId;
    private String billNo;
    private BigDecimal ysAmount;
    private BigDecimal dkAmount;
    private BigDecimal ssAmount;
    private BigDecimal dsAmount;
    private BigDecimal zlAmount;
    private List<GetPayTypeOutData> payTypeList;

    public GetPayOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBillNo() {
        return this.billNo;
    }

    public BigDecimal getYsAmount() {
        return this.ysAmount;
    }

    public BigDecimal getDkAmount() {
        return this.dkAmount;
    }

    public BigDecimal getSsAmount() {
        return this.ssAmount;
    }

    public BigDecimal getDsAmount() {
        return this.dsAmount;
    }

    public BigDecimal getZlAmount() {
        return this.zlAmount;
    }

    public List<GetPayTypeOutData> getPayTypeList() {
        return this.payTypeList;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBillNo(final String billNo) {
        this.billNo = billNo;
    }

    public void setYsAmount(final BigDecimal ysAmount) {
        this.ysAmount = ysAmount;
    }

    public void setDkAmount(final BigDecimal dkAmount) {
        this.dkAmount = dkAmount;
    }

    public void setSsAmount(final BigDecimal ssAmount) {
        this.ssAmount = ssAmount;
    }

    public void setDsAmount(final BigDecimal dsAmount) {
        this.dsAmount = dsAmount;
    }

    public void setZlAmount(final BigDecimal zlAmount) {
        this.zlAmount = zlAmount;
    }

    public void setPayTypeList(final List<GetPayTypeOutData> payTypeList) {
        this.payTypeList = payTypeList;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetPayOutData)) {
            return false;
        } else {
            GetPayOutData other = (GetPayOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label107;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label107;
                    }

                    return false;
                }

                Object this$billNo = this.getBillNo();
                Object other$billNo = other.getBillNo();
                if (this$billNo == null) {
                    if (other$billNo != null) {
                        return false;
                    }
                } else if (!this$billNo.equals(other$billNo)) {
                    return false;
                }

                Object this$ysAmount = this.getYsAmount();
                Object other$ysAmount = other.getYsAmount();
                if (this$ysAmount == null) {
                    if (other$ysAmount != null) {
                        return false;
                    }
                } else if (!this$ysAmount.equals(other$ysAmount)) {
                    return false;
                }

                label86: {
                    Object this$dkAmount = this.getDkAmount();
                    Object other$dkAmount = other.getDkAmount();
                    if (this$dkAmount == null) {
                        if (other$dkAmount == null) {
                            break label86;
                        }
                    } else if (this$dkAmount.equals(other$dkAmount)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$ssAmount = this.getSsAmount();
                    Object other$ssAmount = other.getSsAmount();
                    if (this$ssAmount == null) {
                        if (other$ssAmount == null) {
                            break label79;
                        }
                    } else if (this$ssAmount.equals(other$ssAmount)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$dsAmount = this.getDsAmount();
                    Object other$dsAmount = other.getDsAmount();
                    if (this$dsAmount == null) {
                        if (other$dsAmount == null) {
                            break label72;
                        }
                    } else if (this$dsAmount.equals(other$dsAmount)) {
                        break label72;
                    }

                    return false;
                }

                Object this$zlAmount = this.getZlAmount();
                Object other$zlAmount = other.getZlAmount();
                if (this$zlAmount == null) {
                    if (other$zlAmount != null) {
                        return false;
                    }
                } else if (!this$zlAmount.equals(other$zlAmount)) {
                    return false;
                }

                Object this$payTypeList = this.getPayTypeList();
                Object other$payTypeList = other.getPayTypeList();
                if (this$payTypeList == null) {
                    if (other$payTypeList != null) {
                        return false;
                    }
                } else if (!this$payTypeList.equals(other$payTypeList)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetPayOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $billNo = this.getBillNo();
        result = result * 59 + ($billNo == null ? 43 : $billNo.hashCode());
        Object $ysAmount = this.getYsAmount();
        result = result * 59 + ($ysAmount == null ? 43 : $ysAmount.hashCode());
        Object $dkAmount = this.getDkAmount();
        result = result * 59 + ($dkAmount == null ? 43 : $dkAmount.hashCode());
        Object $ssAmount = this.getSsAmount();
        result = result * 59 + ($ssAmount == null ? 43 : $ssAmount.hashCode());
        Object $dsAmount = this.getDsAmount();
        result = result * 59 + ($dsAmount == null ? 43 : $dsAmount.hashCode());
        Object $zlAmount = this.getZlAmount();
        result = result * 59 + ($zlAmount == null ? 43 : $zlAmount.hashCode());
        Object $payTypeList = this.getPayTypeList();
        result = result * 59 + ($payTypeList == null ? 43 : $payTypeList.hashCode());
        return result;
    }

    public String toString() {
        return "GetPayOutData(clientId=" + this.getClientId() + ", billNo=" + this.getBillNo() + ", ysAmount=" + this.getYsAmount() + ", dkAmount=" + this.getDkAmount() + ", ssAmount=" + this.getSsAmount() + ", dsAmount=" + this.getDsAmount() + ", zlAmount=" + this.getZlAmount() + ", payTypeList=" + this.getPayTypeList() + ")";
    }
}
