package com.gys.business.service;



import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;
import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationH;
import com.gys.business.service.data.GaiaEntNewProductEvaluationDOutData;
import com.gys.business.service.data.GaiaEntNewProductEvaluationInData;
import com.gys.common.data.GetLoginOutData;

import java.util.HashMap;
import java.util.List;

/**
 * 公司级-新品评估-主表(GaiaEntNewProductEvaluationH)表服务接口
 *
 * @author makejava
 * @since 2021-07-19 10:16:54
 */
public interface GaiaEntNewProductEvaluationHService {

    Boolean insertEntProductData(String param);

    Boolean updateEntProductData();

    List<GaiaEntNewProductEvaluationDOutData> listEntProductBill(GaiaEntNewProductEvaluationInData param);

    GaiaEntNewProductEvaluationDOutData listEntProductBillDetail(GaiaEntNewProductEvaluationInData param);

    List<HashMap<String,Object>> listEntInfo(GaiaEntNewProductEvaluationInData param);

    Boolean updateDetailList(GetLoginOutData userInfo, List<GaiaEntNewProductEvaluationD> param);

    Boolean confirmBill(GetLoginOutData userInfo, GaiaEntNewProductEvaluationH param);

    GaiaEntNewProductEvaluationDOutData confirmBillHint(GetLoginOutData userInfo, GaiaEntNewProductEvaluationH param);

    Object export(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param);

    Object listEntProductionDetail(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param);

    Object listEntProductionResult(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param);

    Object getEntProductionResultDetail(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param);

    Object updateResult(GetLoginOutData userInfo, GaiaEntNewProductEvaluationH param);
}
