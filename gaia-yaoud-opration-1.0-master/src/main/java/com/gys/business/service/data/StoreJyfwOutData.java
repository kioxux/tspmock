package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class StoreJyfwOutData implements Serializable {

    /**
     * 机构编码
     */
    @ApiModelProperty(value="机构编码")
    private String orgId;

    /**
     * 经营范围编码
     */
    @ApiModelProperty(value="经营范围编码")
    private String jyfwId;
}
