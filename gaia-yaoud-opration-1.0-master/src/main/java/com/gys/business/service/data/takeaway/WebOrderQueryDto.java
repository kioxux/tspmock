package com.gys.business.service.data.takeaway;

import com.gys.common.data.PageInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/7/21 17:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebOrderQueryDto {

    @ApiModelProperty("具体分页数据")
    private PageInfo<WebOrderDataDto> pageInfo;
    @ApiModelProperty("统计数据")
    private WebOrderSumDto sumDto;
}
