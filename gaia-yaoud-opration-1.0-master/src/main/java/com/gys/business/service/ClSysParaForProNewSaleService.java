package com.gys.business.service;

import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import java.util.Map;

public interface ClSysParaForProNewSaleService {
    JsonResult saveStatus(Map<String, String> statusMap);

    JsonResult getStatus(GetLoginOutData loginUser);

}
