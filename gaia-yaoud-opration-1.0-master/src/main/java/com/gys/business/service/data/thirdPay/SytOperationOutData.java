package com.gys.business.service.data.thirdPay;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value = "收银通操作记录")
@Data
public class SytOperationOutData {
    private Long id;

    @ApiModelProperty(value = "加盟商编码")
    private String client;

    @ApiModelProperty(value = "门店编码")
    private String storeCode;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    /**
     * 渠道编号
     */
    @ApiModelProperty(value = "渠道编号")
    private String channel;

    /**
     * 商户号
     */
    @ApiModelProperty(value = "商户号")
    private String merId;

    /**
     * 商户名称
     */
    @ApiModelProperty(value = "商户名称")
    private String mchtName;

    /**
     * 收单流水号，供后续退货或撤销使用
     */
    @ApiModelProperty(value = "收单流水号，供后续退货或撤销使用")
    private String txnSeqId;

    /**
     * 平台交易日期
     */
    @ApiModelProperty(value = "平台交易日期")
    private String txnDate;

    /**
     * 平台交易时间
     */
    @ApiModelProperty(value = "平台交易时间")
    private String txnTime;

    /**
     * 交易类型 01:消费，34：退款
     */
    @ApiModelProperty(value = "交易类型 01:消费，34：退款")
    private String transType;

    /**
     * 支付类型 15：微信被扫，20：支付宝被扫
     */
    @ApiModelProperty(value = "支付类型 15：微信被扫，20：支付宝被扫")
    private String payType;

    /**
     * 订单金额
     */
    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    /**
     * 设备信息
     */
    @ApiModelProperty(value = "设备信息")
    private String machineInfo;

    /**
     * 订单编号(外部)
     */
    @ApiModelProperty(value = "订单编号(外部)")
    private String outerNumber;

    /**
     * 订单时间
     */
    @ApiModelProperty(value = "订单时间")
    private Date outerDtTm;

    /**
     * 币种 156：人民币
     */
    @ApiModelProperty(value = "币种 156：人民币")
    private String currencyCode;

    /**
     * 订单编号（内部）
     */
    @ApiModelProperty(value = "订单编号（内部）")
    private String billNo;

    /**
     * 操作名称
     */
    @ApiModelProperty(value = "操作名称")
    private String operation;

    /**
     * 交易结果 0：失败，1：成功
     */
    @ApiModelProperty(value = "交易结果 0：失败，1：成功")
    private String result;

    /**
     * 实付金额
     */
    @ApiModelProperty(value = "实付金额")
    private BigDecimal realPayAmount;
}
