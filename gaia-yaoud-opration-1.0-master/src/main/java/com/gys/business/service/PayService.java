package com.gys.business.service;

import com.gys.business.mapper.entity.*;
import com.gys.business.service.data.*;
import com.gys.business.service.data.his.FxBillInfoForm;
import com.gys.business.service.data.integralExchange.MemeberJfMessageInData;
import com.gys.business.service.data.integralExchange.MemeberJfMessageOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface PayService {
    List<GetPayTypeOutData> payTypeList(GetPayInData inData);

    List<GetPayTypeOutData> payTypeListByClient(GetPayInData inData);

    GetPayOutData payInfo(GetPayInData inData);

    ExchangeCashOutData exchangeCash(GetPayInData inData);

    void pay(GetPayInData inData, GetLoginOutData userInfo);

    @Deprecated
    Map<String, Object> payCommit(GetPayInData inData);

    Map<String, Object> payCommit2(GetPayInData inData);

    List<GetDzqOutData> dzqList(GetPayInData inData);

    MemeberJfValidateOutData memeberJfDxValidate(MemeberJfValidateInData inData);

    /**
     * 调用物料凭证接口
     * @param materialList
     */
    void insertMaterialDoc(List<MaterialDocRequestDto> materialList);

    List<GaiaSdElectronChange> giveElectrons(GetPayInData inData);

    List<GaiaSdRechargeCard> queryRecharge(RechargeQueryInData inData);

    GaiaSdRechargeCardSet isNeedPassword(String client, String brId);

    /**
     * 接收到销售单的事件处理
     * @param inData
     * @return
     */
    Map<String, Object> receiveSaleOrder(MQSaleOrderInData inData);

    GaiaSdSytAccount getSytAccount(String client, String brId);

    int checkClinicalSheet(String client, String brId, FxBillInfoForm fxBillInfoForm);

    boolean hisPayCallback(GaiaSdSaleH saleH, List<GaiaSdSaleD> saleDList);

    boolean hisPayRefund(GaiaSdSaleH saleH, List<GaiaSdSaleD> saleDList);

    MemeberJfMessageOutData memeberJfMessage(MemeberJfMessageInData inData);

    List<ElectronDetailOutData> calculateElectrons(GetPayInData inData);

    void insertSendElectronList(String client, String brId, String gsshBillNo, List<GaiaSdSendElectron> sendElectronList);

}
