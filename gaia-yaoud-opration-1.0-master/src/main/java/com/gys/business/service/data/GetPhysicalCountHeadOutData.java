package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "APP盘点单列表返回实体")
public class GetPhysicalCountHeadOutData implements Serializable {
    private static final long serialVersionUID = 7132085225241820716L;

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "盘点单号")
    private String gspcVoucherId;

    @ApiModelProperty(value = "损溢单号")
    private String incomeStatementOutId;

    @ApiModelProperty(value = "盘点日期")
    private String gsphDate;

    @ApiModelProperty(value = "盘点时间")
    private String gsphTime;

    @ApiModelProperty(value = "是否全盘, 0:部分盘点, 1:全盘")
    private String gsphFlag1;

    @ApiModelProperty(value = "是否显示库存, 0:明盘, 1:盲盘")
    private String gsphFlag2;

    @ApiModelProperty(value = "是否批号, 0:数量盘点, 1:批号盘点")
    private String gsphFlag3;

    @ApiModelProperty(value = "盘点行号")
    private int lineNum;

}
