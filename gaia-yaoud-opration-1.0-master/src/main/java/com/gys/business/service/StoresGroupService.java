package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdStoresGroup;
import com.gys.business.service.data.StoresGroupInData;
import com.gys.business.service.data.StoresGroupOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface StoresGroupService {

    List<StoresGroupOutData> listAllStoresGroupByClientId(StoresGroupInData param);

    List<StoresGroupOutData> listAllStoresInfoByClientId(StoresGroupInData param);

    boolean insertOrUpdateStoresGroup(GetLoginOutData userInfo, List<GaiaSdStoresGroup> groupList);
}
