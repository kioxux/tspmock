package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductLimitInData implements Serializable {

    private static final long serialVersionUID = 9141411311280211641L;

    private Integer id;

    private String client;

    private String proId;

    //限购类型. 0：每单限购，1：会员限购
    private String limitType;

    //限购方式 0：每天，1：合计
    private String limitFlag;

    //限购数量
    private String limitQty;

    //是否一直生效. 0：否，1：是
    private String ifAllTime;

    //起始日期
    private String beginDate;

    //结束日期
    private String endDate;

    private List<String> inTimeArr;

    //星期频率
    private String weekFrequency;

    //日期频率
    private String dateFrequency;

    //是否所有门店0：否，1：是
    private String ifAllSto;

    //门店选择
    private List<String> sites;
    //是否销售 0：不可销售 1：原价销售
    private Integer ifSale;
}
