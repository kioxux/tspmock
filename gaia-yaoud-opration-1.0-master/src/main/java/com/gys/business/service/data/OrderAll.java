package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class OrderAll implements Serializable {
    private static final long serialVersionUID = 2461087805527307712L;

    /**
     * 订单详细
     */
    @ApiModelProperty(value = "订购详细")
    private List<PreordDDetailsOutData> details;

    /**
     * 支付详情
     */
    @ApiModelProperty(value = "支付详情")
    private List<PreordPayMsgDetails> payMsgDetails;
}
