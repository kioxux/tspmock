package com.gys.business.service.data.his;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/25 17:16
 */
@Data
public class AddressForm {
    @ApiModelProperty(value = "收件人姓名", example = "华瓴科技")
    private String receiveName;
    @ApiModelProperty(value = "收件人手机号", example = "15988090000")
    private String receivePhone;
    @ApiModelProperty(value = "省份名称", example = "江苏省")
    private String provinceName;
    @ApiModelProperty(value = "城市名称", example = "苏州市")
    private String cityName;
    @ApiModelProperty(value = "县区名称", example = "吴中区")
    private String areaName;
    @ApiModelProperty(value = "详细地址", example = "月亮湾路15号中新大厦")
    private String address;
    /**邮政编码*/
    private String zipCode;
}
