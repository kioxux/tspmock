package com.gys.business.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangyifan
 */
@Data
public class SelectOasSicknessPageInfoRequest {
    @ApiModelProperty(value = "当前页码")
    private Integer pageNum;

    @ApiModelProperty(value = "每页数量")
    private Integer pageSize;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "大类中类成分类型0大类 1 中类 2成分")
    private Integer type;

    @ApiModelProperty(value = "疾病分类编码或名称")
    private String sicknessCodingName;

    @ApiModelProperty(value = "大类疾病编码")
    private String  largeSicknessCoding;

    @ApiModelProperty(value = "大类疾病编码")
    private String  largeSicknessName;

    @ApiModelProperty(value = "中类疾病编码")
    private String  middleSicknessCoding;

    @ApiModelProperty(value = "中类疾病编码")
    private String  middleSicknessName;
}
