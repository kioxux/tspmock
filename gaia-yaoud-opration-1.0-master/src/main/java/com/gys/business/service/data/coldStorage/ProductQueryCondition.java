package com.gys.business.service.data.coldStorage;

import lombok.Data;

import java.util.List;

/**
 * @Auther: tzh
 * @Date: 2021/12/28 17:39
 * @Description: ProductQueryCondition
 * @Version 1.0.0
 */
@Data
public class ProductQueryCondition {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String proSite;

    /**
     * 商品编码
     */
    private List<String> proSelfCodes;

}
