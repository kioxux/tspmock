package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaStoreInSuggestionDMapper;
import com.gys.business.service.GaiaStoreInSuggestionDService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 门店调入建议-明细表(GaiaStoreInSuggestionD)表服务实现类
 *
 * @author makejava
 * @since 2021-10-28 10:53:02
 */
@Service("gaiaStoreInSuggestionDService")
public class GaiaStoreInSuggestionDServiceImpl implements GaiaStoreInSuggestionDService {
    @Resource
    private GaiaStoreInSuggestionDMapper gaiaStoreInSuggestionDDao;

}
