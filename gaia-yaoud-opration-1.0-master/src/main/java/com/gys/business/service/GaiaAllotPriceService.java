package com.gys.business.service;

import com.gys.business.mapper.entity.AllotPrice;
import com.gys.business.service.data.check.OrderOperateForm;
import org.apache.xmlbeans.impl.xb.xsdschema.All;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/16 14:46
 **/
public interface GaiaAllotPriceService {

    /**
     * 批量更新加点目录价
     * @param operateForm 目录价格参数
     */
    void batchUpdatePrice(OrderOperateForm operateForm);

    void updateByClientAndPro(AllotPrice allotPrice);

    AllotPrice add(AllotPrice allotPrice);

}
