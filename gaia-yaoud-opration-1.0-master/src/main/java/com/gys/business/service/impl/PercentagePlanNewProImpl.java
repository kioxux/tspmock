package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.TichengProplanBasic;
import com.gys.business.mapper.entity.TichengProplanProN;
import com.gys.business.mapper.entity.TichengProplanSetting;
import com.gys.business.service.ImportPercentageData;
import com.gys.business.service.PercentagePlanNewStrategy;
import com.gys.business.service.data.MessageParams;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.feign.OperateService;
import com.gys.util.CommonUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 提成方案-单品提成实现
 *
 * @Description: TODO
 * @author: flynn
 * @date: 2021年11月11日 上午10:09
 */
public class PercentagePlanNewProImpl implements PercentagePlanNewStrategy {

    private TichengSaleplanZMapper tichengSaleplanZMapper;

    private TichengProplanBasicMapper tichengProplanBasicMapper;//单品提成方案主表mapper

    private TichengProplanSettingMapper tichengProplanSettingMapper;//单品提成提成设置表

    private TichengProplanProNMapper tichengProplanProNMapper;//单品提成提成商品设置表

    private TichengProplanStoNMapper tichengProplanStoNMapper;//单品提成门店关联表

    private OperateService operateService;

    public PercentagePlanNewProImpl(TichengSaleplanZMapper tichengSaleplanZMapper, TichengProplanBasicMapper tichengProplanBasicMapper, TichengProplanSettingMapper tichengProplanSettingMapper, TichengProplanProNMapper tichengProplanProNMapper, TichengProplanStoNMapper tichengProplanStoNMapper, OperateService operateService) {
        this.tichengSaleplanZMapper = tichengSaleplanZMapper;
        this.tichengProplanBasicMapper = tichengProplanBasicMapper;
        this.tichengProplanSettingMapper = tichengProplanSettingMapper;
        this.tichengProplanProNMapper = tichengProplanProNMapper;
        this.tichengProplanStoNMapper = tichengProplanStoNMapper;
        this.operateService = operateService;
    }

    @Override
    public Integer insert(PercentageBasicInData inData) {
        Integer id = 0;

        if (ObjectUtil.isNotEmpty(inData.getId())) {
            TichengProplanBasic tichengPlan = new TichengProplanBasic();
            tichengPlan.setId(inData.getId());
            TichengProplanBasic tichengProplanBasic = tichengProplanBasicMapper.selectByPrimaryKey(tichengPlan);
            if (tichengProplanBasic == null) {
                throw new BusinessException("查无此数据！");
            }
            tichengProplanBasic.setClient(inData.getClient());
            tichengProplanBasic.setPlanName(inData.getPlanName());
            tichengProplanBasic.setPlanUpdateDatetime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengProplanBasic.setPlanUpdater(inData.getPlanCreater());
            tichengProplanBasic.setPlanUpdateId(inData.getPlanCreaterId());
            tichengProplanBasic.setPlanStartDate(inData.getPlanStartDate());
            tichengProplanBasic.setPlanEndDate(inData.getPlanEndDate());
            tichengProplanBasic.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengProplanBasic.setPlanScaleSto(inData.getPlanScaleSto());
            tichengProplanBasic.setLastUpdateTime(LocalDateTime.now());
            if (StrUtil.isNotBlank(inData.getPlanReason())) {
                tichengProplanBasic.setPlanReason(inData.getPlanReason());
                tichengProplanBasic.setPlanStatus("0");
            }
//            if ("1".equals(inData.getPlanStatus())){
//                tichengPlan.setPlanStatus("3");
//                tichengPlan.setPlanReason(inData.getPlanReason());
//            }
            tichengProplanBasicMapper.updateByPrimaryKey(tichengProplanBasic);
            id = tichengProplanBasic.getId();
        } else {
            String planCode = tichengSaleplanZMapper.selectNextPlanCode(inData.getClient());
            TichengProplanBasic tichengPlan = new TichengProplanBasic();
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanCode(planCode);
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            tichengPlan.setPlanType("2");
            tichengPlan.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengPlan.setPlanScaleSto(inData.getPlanScaleSto());
            tichengPlan.setPlanStatus("0");
            tichengPlan.setDeleteFlag("0");

            tichengPlan.setPlanCreater(inData.getPlanCreater());
            tichengPlan.setPlanCreateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanCreaterId(inData.getPlanCreaterId());
            tichengPlan.setPlanUpdateDatetime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss() + "");
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setLastUpdateTime(LocalDateTime.now());
            tichengProplanBasicMapper.insertUseGeneratedKeys(tichengPlan);
            id = tichengPlan.getId();
        }

        //初始化加盟商门店
        tichengProplanStoNMapper.deleteProStoreByPid(id);
        //新增销售明细
        List<PercentageStoInData> stoList = new ArrayList<>();
        if (inData.getStoreCodes().length > 0) {
            for (String storeCode : inData.getStoreCodes()) {
                PercentageStoInData item = new PercentageStoInData();
                item.setClientId(inData.getClient());
                item.setPid(id);
//                item.setPlanSuitMouth(inData.getPlanSuitMonth());
                item.setStoCode(storeCode);
                stoList.add(item);
            }
        }
        if (stoList.size() > 0 && stoList != null) {
            tichengProplanStoNMapper.addPlanProStores(stoList);
        }
        return id;
    }

    @Override
    public PageInfo<PercentageOutData> list(PercentageInData inData) {
        return null;
    }

    @Override
    public Map<String, Object> tichengDetail(Long planId, String type, GetLoginOutData userInfo) {
        Map<String, Object> resultData = new HashMap<>();
        if (planId == null || planId == 0) {
            throw new BusinessException("主方案id不能为空");
        }
        //查询主表信息
        if (ObjectUtil.isNotEmpty(planId)) {
            TichengProplanBasic tichengPlan = new TichengProplanBasic();
            tichengPlan.setId(Integer.parseInt(planId.toString()));
            TichengProplanBasic tichengProplanBasic = tichengProplanBasicMapper.selectByPrimaryKey(tichengPlan);
            if (tichengProplanBasic == null) {
                throw new BusinessException("查无此数据！");
            }
            //补充适用门店
            List<Map<String, String>> maps = tichengProplanStoNMapper.selectProStoList(planId);
//            resultData.put("stos", maps);
//            tichengPlan.setStos(maps);
            if (CollectionUtil.isNotEmpty(maps)) {
                List<String> stos = new ArrayList<>();
                maps.forEach(x -> {
                    String stoCode = x.get("stoCode");
                    stos.add(stoCode);
                });
                tichengProplanBasic.setStoreCodes(stos);
            }
            resultData.put("mainInfo", tichengProplanBasic);

            List<ProSettingOutData> proSettingOutDataList = new ArrayList<>();
            TichengProplanSetting query = new TichengProplanSetting();
            query.setPid(planId);
            query.setDeleteFlag("0");
            List<TichengProplanSetting> tichengProplanSettingDbs = tichengProplanSettingMapper.select(query);
            //存在多个提成设置情况
            if (CollectionUtil.isNotEmpty(tichengProplanSettingDbs)) {
                for (TichengProplanSetting setting : tichengProplanSettingDbs) {
                    ProSettingOutData result = new ProSettingOutData();
                    //处理返回值
                    result.setPlanId(Integer.parseInt(setting.getPid().toString()));
                    result.setId(setting.getId());
                    result.setPlanType(setting.getPlanType());
                    result.setPlanPercentageWay(setting.getPliantPercentageType());
                    result.setPlanProductWay(setting.getPlanProductWay());
                    result.setPlanRejectDiscountRateSymbol(setting.getPlanRejectDiscountRateSymbol());
                    result.setPlanRejectDiscountRate(setting.getPlanRejectDiscountRate());
                    result.setPlanIfNegative(setting.getPlanIfNegative());
                    result.setCPlanName(setting.getCPlanName());

                    List<PercentageProInData> proInDataList = new ArrayList<>();
                    TichengProplanProN queryTichengProplanProN = new TichengProplanProN();
                    queryTichengProplanProN.setPid(setting.getPid());
                    queryTichengProplanProN.setSettingId(Integer.parseInt(setting.getId().toString()));
                    queryTichengProplanProN.setDeleteFlag("0");
                    queryTichengProplanProN.setClient(userInfo.getClient());
                    List<TichengProplanProN> tichengProplanProNDbs = tichengProplanProNMapper.select(queryTichengProplanProN);
                    if (CollectionUtil.isNotEmpty(tichengProplanProNDbs)) {
                        for (TichengProplanProN tichengProplanProN : tichengProplanProNDbs) {
                            PercentageProInData res = new PercentageProInData();
                            res.setId(tichengProplanProN.getId());
                            res.setClientId(tichengProplanProN.getClient());
                            res.setPid(Integer.parseInt(tichengProplanProN.getPid().toString()));
                            res.setProCode(tichengProplanProN.getProCode());
                            res.setProName(tichengProplanProN.getProName());
                            res.setProSpecs(tichengProplanProN.getProSpecs());
                            res.setProFactoryName(tichengProplanProN.getProFactoryName());
                            res.setProCostPrice(tichengProplanProN.getProCostPrice());
                            res.setProPrice(tichengProplanProN.getProPriceNormal());
//                    res.setGrossProfitAmt(tichengProplanProN.getProPriceNormal());
//                    res.setGrossProfitRate();
                            res.setSaleQty(tichengProplanProN.getSaleQty() == null ? null : tichengProplanProN.getSaleQty().toPlainString());
                            res.setTichengAmt(tichengProplanProN.getTichengAmt() == null ? null : tichengProplanProN.getTichengAmt().toPlainString());
                            res.setTichengRate(tichengProplanProN.getTichengRate() == null ? null : tichengProplanProN.getTichengRate().toPlainString());

                            res.setSaleQty2(tichengProplanProN.getSaleQty2() == null ? null : tichengProplanProN.getSaleQty2().toPlainString());
                            res.setTichengAmt2(tichengProplanProN.getTichengAmt2() == null ? null : tichengProplanProN.getTichengAmt2().toPlainString());
                            res.setTichengRate2(tichengProplanProN.getTichengRate2() == null ? null : tichengProplanProN.getTichengRate2().toPlainString());

                            res.setSaleQty3(tichengProplanProN.getSaleQty3() == null ? null : tichengProplanProN.getSaleQty3().toPlainString());
                            res.setTichengAmt3(tichengProplanProN.getTichengAmt3() == null ? null : tichengProplanProN.getTichengAmt3().toPlainString());
                            res.setTichengRate3(tichengProplanProN.getTichengRate3() == null ? null : tichengProplanProN.getTichengRate3().toPlainString());
                            proInDataList.add(res);
                        }
                    }
                    result.setProInDataList(proInDataList);
                    proSettingOutDataList.add(result);
                }
            }
            resultData.put("saleSettingInfo", proSettingOutDataList);
        }
        return resultData;
    }

    @Override
    public void approve(Long planId, String planStatus, String type) {

        PercentageInData proInfo = tichengProplanBasicMapper.selectTichengZProDetail(planId);

        //添加校验，提成方案同时间段内是否存在方案重复问题
        PercentageInData inData = new PercentageInData();
        inData.setPlanStartDate(proInfo.getPlanStartDate());
        inData.setPlanEndDate(proInfo.getPlanEndDate());
        inData.setPlanType("2");
        inData.setClient(proInfo.getClient());
        inData.setId(proInfo.getId());
//        List<TichengProplanBasic> repeatList = tichengProplanBasicMapper.checkDate(inData);
//        if(CollectionUtil.isNotEmpty(repeatList)){
//            String planName = repeatList.get(0).getPlanName();
//            throw new BusinessException("此单品提成方案与单品提成方案:" + planName + "日期有重叠，不予审核");
//        }
        //提成方案审批的时候需要添加商品的校验逻辑，校验同期时间内其他方案是否存在其他相同商品
        StringBuilder builder = new StringBuilder();
        builder.append("");
        //查询加盟商下同时间段内的商品数据
        List<Map<String, Object>> inTimeDetailList = tichengProplanBasicMapper.selectProPlanDetailByClientAndTime(proInfo.getClient(), proInfo.getPlanStartDate(), proInfo.getPlanEndDate());
        //查询当前方案下的商品数据
        List<Map<String, Object>> planDetailList = tichengProplanBasicMapper.selectProPlanDetailByClientAndPid(proInfo.getClient(), planId);
        if (CollectionUtil.isNotEmpty(inTimeDetailList)) {
            for (Map<String, Object> db : inTimeDetailList) {
                String pro_code = (String) db.get("PRO_CODE");
                String pro_name = (String) db.get("PRO_NAME");
                String planName = (String) db.get("PLAN_NAME");
                String cPlanName = (String) db.get("C_PLAN_NAME");
                //过滤自己，避免修改的时候出现无法保存的情况
                Long planIdDb = (Long) db.get("ID");
                if (planId.intValue() == planIdDb.intValue()) {
                    continue;
                }
                if (CollectionUtil.isNotEmpty(planDetailList)) {
                    for (Map<String, Object> proInData : planDetailList) {
                        String planProCode = (String) proInData.get("PRO_CODE");
                        if (StrUtil.isNotBlank(planProCode) && planProCode.equals(pro_code)) {
                            builder.append(pro_code + "  " + pro_name + "已在" + planName + "方案中的" + cPlanName + "设置;");
                        }
                    }
                }

            }
            if (StrUtil.isNotBlank(builder.toString())) {
                throw new BusinessException(builder.toString());
            }
        }

        if ("0".equals(proInfo.getPlanStatus())) {
            tichengProplanBasicMapper.approveProPlan(planId, planStatus, "审核已通过");
            proInfo.setPlanStatus(planStatus);
            proInfo.setPlanReason("审核已通过");
        } else {
            tichengProplanBasicMapper.approveProPlan(planId, planStatus, "");
            proInfo.setPlanStatus(planStatus);
        }
        //消息推送到APP
        sendMessage(proInfo);

    }

    private void sendMessage(PercentageInData planInfo) {
        List<Map<String, String>> result = tichengSaleplanZMapper.selectProStoList(planInfo.getId().longValue());
        String[] stoCodeArr = new String[result.size()];
        for (int i = 0; i < result.size(); i++) {
            stoCodeArr[i] = result.get(i).get("stoCode");
        }
        planInfo.setStoreCodes(stoCodeArr);
        List<String> userIds = tichengSaleplanZMapper.selectUserIdList(planInfo);
        MessageParams messageParams = new MessageParams();
        messageParams.setClient(planInfo.getClient());
        messageParams.setUserIdList(userIds);
        messageParams.setContentParmas(new ArrayList<String>() {{
            add(planInfo.getPlanName());
            add(planInfo.getPlanReason());
        }});
        messageParams.setId("MSG00010");
//            log.info("销售提成消息参数:{}", messageParams.toString());
        String message = operateService.sendMessageList(messageParams);
    }

    @Override
    public void deletePlan(Long planId, String type) {
        //先删除明细再删除主表
        tichengProplanSettingMapper.deleteProPlanSetting(planId);

        tichengProplanProNMapper.deleteProPlanByPid(Integer.parseInt(planId.toString()), null);

        tichengProplanStoNMapper.deleteProStoreByPid(Integer.parseInt(planId.toString()));

        tichengProplanBasicMapper.deleteProPlan(planId);

    }

    @Override
    public PercentageProInData selectProductByClient(String client, String proCode) {
        return null;
    }

    @Override
    public List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList) {
        return null;
    }

    @Override
    public List<Map<String, String>> selectStoList(Long planId, Long type) {
        List<Map<String, String>> result = new ArrayList<>();
        result = tichengProplanStoNMapper.selectProStoList(planId);
        return result;
    }

    @Override
    public Map<String, Object> tichengDetailCopy(Long planId, Long type) {
        Map<String, Object> resultData = new HashMap<>();
        if (planId == null || planId == 0) {
            throw new BusinessException("主方案id不能为空");
        }
        //查询主表信息
        if (ObjectUtil.isNotEmpty(planId)) {
            TichengProplanBasic tichengPlan = new TichengProplanBasic();
            tichengPlan.setId(Integer.parseInt(planId.toString()));
            TichengProplanBasic tichengProplanBasic = tichengProplanBasicMapper.selectByPrimaryKey(tichengPlan);
            if (tichengProplanBasic == null) {
                throw new BusinessException("查无此数据！");
            }
            tichengProplanBasic.setId(null);
            tichengProplanBasic.setPlanName(tichengProplanBasic.getPlanName() + "-副本");
//            resultData.put("mainInfo", tichengProplanBasic);

            //补充适用门店
            List<Map<String, String>> maps = tichengProplanStoNMapper.selectProStoList(planId);
//            resultData.put("stos", maps);
//            tichengPlan.setStos(maps);
            if (CollectionUtil.isNotEmpty(maps)) {
                List<String> stos = new ArrayList<>();
                maps.forEach(x -> {
                    String stoCode = x.get("stoCode");
                    stos.add(stoCode);
                });
                tichengProplanBasic.setStoreCodes(stos);
            }
            resultData.put("mainInfo", tichengProplanBasic);

            List<ProSettingOutData> proSettingOutDataList = new ArrayList<>();
            TichengProplanSetting query = new TichengProplanSetting();
            query.setPid(planId);
            query.setDeleteFlag("0");
            List<TichengProplanSetting> tichengProplanSettingDbs = tichengProplanSettingMapper.select(query);
            //存在多个提成设置情况
            if (CollectionUtil.isNotEmpty(tichengProplanSettingDbs)) {
                for (TichengProplanSetting setting : tichengProplanSettingDbs) {
                    ProSettingOutData result = new ProSettingOutData();
                    //处理返回值
//                    result.setPlanId(Integer.parseInt(setting.getPid().toString()));
//                    result.setId(setting.getId());
                    result.setPlanType(setting.getPlanType());
                    result.setPlanPercentageWay(setting.getPliantPercentageType());
                    result.setPlanProductWay(setting.getPlanProductWay());
                    result.setPlanRejectDiscountRateSymbol(setting.getPlanRejectDiscountRateSymbol());
                    result.setPlanRejectDiscountRate(setting.getPlanRejectDiscountRate());
                    result.setPlanIfNegative(setting.getPlanIfNegative());
                    result.setCPlanName(setting.getCPlanName());

                    List<PercentageProInData> proInDataList = new ArrayList<>();
                    TichengProplanProN queryTichengProplanProN = new TichengProplanProN();
                    queryTichengProplanProN.setPid(setting.getPid());
                    queryTichengProplanProN.setSettingId(Integer.parseInt(setting.getId().toString()));
                    queryTichengProplanProN.setDeleteFlag("0");
//                    queryTichengProplanProN.setClient(userInfo.getClient());
                    List<TichengProplanProN> tichengProplanProNDbs = tichengProplanProNMapper.select(queryTichengProplanProN);
                    if (CollectionUtil.isNotEmpty(tichengProplanProNDbs)) {
                        for (TichengProplanProN tichengProplanProN : tichengProplanProNDbs) {
                            PercentageProInData res = new PercentageProInData();
//                            res.setId(tichengProplanProN.getId());
                            res.setClientId(tichengProplanProN.getClient());
//                            res.setPid(Integer.parseInt(tichengProplanProN.getPid().toString()));
                            res.setProCode(tichengProplanProN.getProCode());
                            res.setProName(tichengProplanProN.getProName());
                            res.setProSpecs(tichengProplanProN.getProSpecs());
                            res.setProFactoryName(tichengProplanProN.getProFactoryName());
                            res.setProCostPrice(tichengProplanProN.getProCostPrice());
                            res.setProPrice(tichengProplanProN.getProPriceNormal());
//                    res.setGrossProfitAmt(tichengProplanProN.getProPriceNormal());
//                    res.setGrossProfitRate();
                            res.setSaleQty(tichengProplanProN.getSaleQty() == null ? null : tichengProplanProN.getSaleQty().toPlainString());
                            res.setTichengAmt(tichengProplanProN.getTichengAmt() == null ? null : tichengProplanProN.getTichengAmt().toPlainString());
                            res.setTichengRate(tichengProplanProN.getTichengRate() == null ? null : tichengProplanProN.getTichengRate().toPlainString());

                            res.setSaleQty2(tichengProplanProN.getSaleQty2() == null ? null : tichengProplanProN.getSaleQty2().toPlainString());
                            res.setTichengAmt2(tichengProplanProN.getTichengAmt2() == null ? null : tichengProplanProN.getTichengAmt2().toPlainString());
                            res.setTichengRate2(tichengProplanProN.getTichengRate2() == null ? null : tichengProplanProN.getTichengRate2().toPlainString());

                            res.setSaleQty3(tichengProplanProN.getSaleQty3() == null ? null : tichengProplanProN.getSaleQty3().toPlainString());
                            res.setTichengAmt3(tichengProplanProN.getTichengAmt3() == null ? null : tichengProplanProN.getTichengAmt3().toPlainString());
                            res.setTichengRate3(tichengProplanProN.getTichengRate3() == null ? null : tichengProplanProN.getTichengRate3().toPlainString());
                            proInDataList.add(res);
                        }
                        result.setProInDataList(proInDataList);
                    }
                    proSettingOutDataList.add(result);
                }
            }
            resultData.put("saleSettingInfo", proSettingOutDataList);
        }
        return resultData;
    }

    @Override
    public void stopPlan(Long planId, String planType, String stopType, String stopReason) {
        PercentageInData proInfo = tichengProplanBasicMapper.selectTichengZProDetail(planId);
        if ("1".equals(proInfo.getPlanStatus())) {
            if ("1".equals(stopType)) {//立即停用
                tichengProplanBasicMapper.stopProPlan(planId, "2", CommonUtil.getyyyyMMdd(), stopReason);
                //消息推送至APP
                List<Map<String, String>> result = tichengProplanStoNMapper.selectProStoList(proInfo.getId().longValue());
                String[] stoCodeArr = new String[result.size()];
                for (int i = 0; i < result.size(); i++) {
                    stoCodeArr[i] = result.get(i).get("stoCode");
                }
                proInfo.setStoreCodes(stoCodeArr);
                List<String> userIds = tichengSaleplanZMapper.selectUserIdList(proInfo);
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(proInfo.getClient());
                messageParams.setUserIdList(userIds);
                messageParams.setContentParmas(new ArrayList<String>() {{
                    add(proInfo.getPlanName());
                    add(stopReason);
                }});
                messageParams.setId("MSG00011");
//                log.info("销售提成消息参数:{}", messageParams.toString());
                String message = operateService.sendMessageList(messageParams);
//                log.info("培训推送消息结果:{}", message);
            } else {
                tichengProplanBasicMapper.stopProPlan(planId, "1", proInfo.getPlanEndDate(), stopReason);
            }
        } else {
            throw new BusinessException("该方案尚未审核，无法停用");
        }
    }

    @Override
    public void timerStopPlan() {

    }
}

