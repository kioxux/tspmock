//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class GetProductLocationInData {
    private String clientId;
    private String gsplBrId;
    private String gsplProId;
    private String proType;
    private String gsplArea;
    private String gsplGroup;
    private String gsplShelf;
    private String gsplStorey;
    private String gsplSeat;
    private Integer index;
    private List<GetProductLocationInData> loactions;

    public GetProductLocationInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsplBrId() {
        return this.gsplBrId;
    }

    public String getGsplProId() {
        return this.gsplProId;
    }

    public String getProType() {
        return this.proType;
    }

    public String getGsplArea() {
        return this.gsplArea;
    }

    public String getGsplGroup() {
        return this.gsplGroup;
    }

    public String getGsplShelf() {
        return this.gsplShelf;
    }

    public String getGsplStorey() {
        return this.gsplStorey;
    }

    public String getGsplSeat() {
        return this.gsplSeat;
    }

    public Integer getIndex() {
        return this.index;
    }

    public List<GetProductLocationInData> getLoactions() {
        return this.loactions;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsplBrId(final String gsplBrId) {
        this.gsplBrId = gsplBrId;
    }

    public void setGsplProId(final String gsplProId) {
        this.gsplProId = gsplProId;
    }

    public void setProType(final String proType) {
        this.proType = proType;
    }

    public void setGsplArea(final String gsplArea) {
        this.gsplArea = gsplArea;
    }

    public void setGsplGroup(final String gsplGroup) {
        this.gsplGroup = gsplGroup;
    }

    public void setGsplShelf(final String gsplShelf) {
        this.gsplShelf = gsplShelf;
    }

    public void setGsplStorey(final String gsplStorey) {
        this.gsplStorey = gsplStorey;
    }

    public void setGsplSeat(final String gsplSeat) {
        this.gsplSeat = gsplSeat;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public void setLoactions(final List<GetProductLocationInData> loactions) {
        this.loactions = loactions;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductLocationInData)) {
            return false;
        } else {
            GetProductLocationInData other = (GetProductLocationInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label143: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label143;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label143;
                    }

                    return false;
                }

                Object this$gsplBrId = this.getGsplBrId();
                Object other$gsplBrId = other.getGsplBrId();
                if (this$gsplBrId == null) {
                    if (other$gsplBrId != null) {
                        return false;
                    }
                } else if (!this$gsplBrId.equals(other$gsplBrId)) {
                    return false;
                }

                Object this$gsplProId = this.getGsplProId();
                Object other$gsplProId = other.getGsplProId();
                if (this$gsplProId == null) {
                    if (other$gsplProId != null) {
                        return false;
                    }
                } else if (!this$gsplProId.equals(other$gsplProId)) {
                    return false;
                }

                label122: {
                    Object this$proType = this.getProType();
                    Object other$proType = other.getProType();
                    if (this$proType == null) {
                        if (other$proType == null) {
                            break label122;
                        }
                    } else if (this$proType.equals(other$proType)) {
                        break label122;
                    }

                    return false;
                }

                label115: {
                    Object this$gsplArea = this.getGsplArea();
                    Object other$gsplArea = other.getGsplArea();
                    if (this$gsplArea == null) {
                        if (other$gsplArea == null) {
                            break label115;
                        }
                    } else if (this$gsplArea.equals(other$gsplArea)) {
                        break label115;
                    }

                    return false;
                }

                Object this$gsplGroup = this.getGsplGroup();
                Object other$gsplGroup = other.getGsplGroup();
                if (this$gsplGroup == null) {
                    if (other$gsplGroup != null) {
                        return false;
                    }
                } else if (!this$gsplGroup.equals(other$gsplGroup)) {
                    return false;
                }

                Object this$gsplShelf = this.getGsplShelf();
                Object other$gsplShelf = other.getGsplShelf();
                if (this$gsplShelf == null) {
                    if (other$gsplShelf != null) {
                        return false;
                    }
                } else if (!this$gsplShelf.equals(other$gsplShelf)) {
                    return false;
                }

                label94: {
                    Object this$gsplStorey = this.getGsplStorey();
                    Object other$gsplStorey = other.getGsplStorey();
                    if (this$gsplStorey == null) {
                        if (other$gsplStorey == null) {
                            break label94;
                        }
                    } else if (this$gsplStorey.equals(other$gsplStorey)) {
                        break label94;
                    }

                    return false;
                }

                label87: {
                    Object this$gsplSeat = this.getGsplSeat();
                    Object other$gsplSeat = other.getGsplSeat();
                    if (this$gsplSeat == null) {
                        if (other$gsplSeat == null) {
                            break label87;
                        }
                    } else if (this$gsplSeat.equals(other$gsplSeat)) {
                        break label87;
                    }

                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                Object this$loactions = this.getLoactions();
                Object other$loactions = other.getLoactions();
                if (this$loactions == null) {
                    if (other$loactions != null) {
                        return false;
                    }
                } else if (!this$loactions.equals(other$loactions)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductLocationInData;
    }

    public int hashCode() {
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsplBrId = this.getGsplBrId();
        result = result * 59 + ($gsplBrId == null ? 43 : $gsplBrId.hashCode());
        Object $gsplProId = this.getGsplProId();
        result = result * 59 + ($gsplProId == null ? 43 : $gsplProId.hashCode());
        Object $proType = this.getProType();
        result = result * 59 + ($proType == null ? 43 : $proType.hashCode());
        Object $gsplArea = this.getGsplArea();
        result = result * 59 + ($gsplArea == null ? 43 : $gsplArea.hashCode());
        Object $gsplGroup = this.getGsplGroup();
        result = result * 59 + ($gsplGroup == null ? 43 : $gsplGroup.hashCode());
        Object $gsplShelf = this.getGsplShelf();
        result = result * 59 + ($gsplShelf == null ? 43 : $gsplShelf.hashCode());
        Object $gsplStorey = this.getGsplStorey();
        result = result * 59 + ($gsplStorey == null ? 43 : $gsplStorey.hashCode());
        Object $gsplSeat = this.getGsplSeat();
        result = result * 59 + ($gsplSeat == null ? 43 : $gsplSeat.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $loactions = this.getLoactions();
        result = result * 59 + ($loactions == null ? 43 : $loactions.hashCode());
        return result;
    }

    public String toString() {
        return "GetProductLocationInData(clientId=" + this.getClientId() + ", gsplBrId=" + this.getGsplBrId() + ", gsplProId=" + this.getGsplProId() + ", proType=" + this.getProType() + ", gsplArea=" + this.getGsplArea() + ", gsplGroup=" + this.getGsplGroup() + ", gsplShelf=" + this.getGsplShelf() + ", gsplStorey=" + this.getGsplStorey() + ", gsplSeat=" + this.getGsplSeat() + ", index=" + this.getIndex() + ", loactions=" + this.getLoactions() + ")";
    }
}
