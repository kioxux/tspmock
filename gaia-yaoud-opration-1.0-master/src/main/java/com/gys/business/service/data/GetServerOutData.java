package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetServerOutData implements Serializable {
    private static final long serialVersionUID = -3552670130250045261L;
    /**
     * 加盟商
     */
    private String clientId;

    /**
     * 销售单号
     */
    private String billNo;

    /**
     * 店号
     */
    private String storeCode;

    /**
     * 表格 1明1
     */
    private String relevancyExplain1;

    /**
     * 表格 1 说明2
     */
    private String relevancyExplain2;

    /**
     * 表格 1 说明3
     */
    private String relevancyExplain3;

    /**
     * 表格 1 说明4
     */
    private String relevancyExplain4;

    /**
     * 表格 1 说明5
     */
    private String relevancyExplain5;

    /**
     * 表格 2 说明1
     */
    private String relevancyExplain6;

    /**
     * 表格 2 说明2
     */
    private String relevancyExplain7;

    /**
     * 表格 2 说明3
     */
    private String relevancyExplain8;

    /**
     * 表格 2 说明4
     */
    private String relevancyExplain9;

    /**
     * 表格 2 说明5
     */
    private String relevancyExplain10;

    /**
     * 成分 1
     */
    private List<GetProductInfoQueryOutData> productFormList1;

    /**
     * 成分 2
     */
    private List<GetProductInfoQueryOutData> productFormList2;

    /**
     * 关联推荐1
     */
    private List<GetServerProduct1OutData> productList1;

    /**
     * 关联推荐2
     */
    private List<GetServerProduct2OutData> productList2;

    /**
     * 冲突 说明1
     */
    private String diffExplain1;

    /**
     * 冲突 说明2
     */
    private String diffExplain2;

    /**
     * 冲突 说明3
     */
    private String diffExplain3;

    /**
     * 冲突 说明4
     */
    private String diffExplain4;

    /**
     * 冲突 说明5
     */
    private String diffExplain5;

    /**
     * 禁忌说明 1
     */
    private String contraExplain1;

    /**
     * 禁忌说明 2
     */
    private String contraExplain2;

    /**
     * 禁忌说明 3
     */
    private String contraExplain3;

    /**
     * 禁忌说明 4
     */
    private String contraExplain4;

    /**
     * 禁忌说明 5
     */
    private String contraExplain5;

    /**
     * 药事服务 说明1
     */
    private String tipsExplain1;

    /**
     * 药事服务 说明2
     */
    private String tipsExplain2;

    /**
     * 药事服务 说明3
     */
    private String tipsExplain3;

    /**
     * 药事服务 说明4
     */
    private String tipsExplain4;

    /**
     * 药事服务 说明5
     */
    private String tipsExplain5;

    @ApiModelProperty(value = "用法用量")
    private String proUsage;
    @ApiModelProperty(value = "注意事项")
    private String proContraindication;

    private boolean relevancy1 = false;
    private boolean relevancy2 = false;
    private boolean diff = false;
    private boolean contra = false;
    private boolean tips = false;
}
