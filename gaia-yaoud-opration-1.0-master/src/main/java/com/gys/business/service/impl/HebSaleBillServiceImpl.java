package com.gys.business.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.*;
import com.gys.business.service.data.ConsumeStockResponse;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.business.service.data.StockInData;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.MQReceiveData;
import com.gys.common.enums.MaterialTypeEnum;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author shentao
 */
@Slf4j
@Service
public class HebSaleBillServiceImpl implements HebSaleBillService {
    @Resource
    private GaiaSdSaleHMapper saleHMapper;

    @Resource
    private GaiaSdSaleDMapper saleDMapper;

    @Resource
    private GaiaSdSalePayMsgMapper payMsgMapper;

    @Resource
    private GaiaSdStockMapper stockMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Resource
    private GaiaSdMemberCardDataMapper gaiaSdMemberCardDataMapper;

    @Resource
    private GaiaMaterialAssessMapper materialAssessMapper;

    @Resource
    private GaiaSdMemberBasicMapper memberBasicMapper;

    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;

    @Resource
    private GaiaTaxCodeMapper taxCodeMapper;

    @Resource
    private SalesReceiptsService salesReceiptsService;

    @Resource
    private StockService stockService;

    @Autowired
    private CacheService synchronizedService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private PayService payService;

    @Resource
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> payCommit(HebBillData hebBillData) {
        Map<String, Object> resultMap = Maps.newHashMap();

        log.info("bill:{}", JSON.toJSONString(hebBillData));
        SaleH hebSaleH = hebBillData.getSaleH();
        try{
            List<SaleD> hebSaleDList = hebBillData.getSaleDList();
            List<PayMsg> hebPayMsgList = hebBillData.getPayMsgList();
            //表单重复验证
            Example example = new Example(GaiaSdSaleH.class);
            example.createCriteria()
                    .andEqualTo("clientId", hebSaleH.getClient())
                    .andEqualTo("gsshBrId", hebSaleH.getBrId())
                    .andEqualTo("gsshBillNo", hebSaleH.getBillNo())
                    .andEqualTo("gsshHideFlag", "0");
            GaiaSdSaleH gaiaSdSaleH = saleHMapper.selectOneByExample(example);
            if (ObjectUtil.isNotEmpty(gaiaSdSaleH)) {
                resultMap.put("N", hebSaleH.getBillNo()+"订单已提交");
                return resultMap;
            }
            resultMap.put("Y", hebSaleH.getBillNo());

            GaiaSdSaleH gaiaSdSaleH1 = convertSaleH(hebSaleH);

            //若有现金支付 则记录现金和找零在sale_h表中
            PayMsg xjPay = hebPayMsgList.stream().filter(pay->"1000".equals(pay.getId())).findFirst().orElse(null);
            if (xjPay!=null) {
                gaiaSdSaleH1.setGsshRmbZlAmt(xjPay.getZlAmt());
                gaiaSdSaleH1.setGsshRmbAmt(xjPay.getRmbAmt());
            }else{
                gaiaSdSaleH1.setGsshRmbZlAmt(BigDecimal.ZERO);
                gaiaSdSaleH1.setGsshRmbAmt(BigDecimal.ZERO);
            }

            saleHMapper.insert(gaiaSdSaleH1);

            List<GaiaSdSaleD> gaiaSdSaleDList = convertSaleDList(hebSaleDList);
            saleDMapper.insertLists(gaiaSdSaleDList);

            example = new Example(GaiaSdSaleH.class);
            example.createCriteria()
                    .andEqualTo("clientId", hebSaleH.getClient())
                    .andEqualTo("gsshBrId", hebSaleH.getBrId())
                    .andEqualTo("gsshBillNo", hebSaleH.getBillNo());
            GaiaSdSaleH saleH = saleHMapper.selectOneByExample(example);


            List<GaiaSdSalePayMsg> payMsgList = convertPayMsg(hebPayMsgList);
            payMsgMapper.insertLists(payMsgList);

//        Example exampleD = new Example(GaiaSdSaleD.class);
//        exampleD.createCriteria()
//                .andEqualTo("clientId", saleH.getClientId())
//                .andEqualTo("gssdBrId", saleH.getGsshBrId())
//                .andEqualTo("gssdBillNo", saleH.getGsshBillNo());
//        List<GaiaSdSaleD> saleDList = this.saleDMapper.selectByExample(exampleD);


            Iterator<GaiaSdSaleD> saleDIterator = gaiaSdSaleDList.iterator();

            List<MaterialDocRequestDto> materialList = Lists.newArrayList();
            // 遍历顶单明细 修改分摊金额价格等数据 库存扣账 关联推荐
            int guiNoCount = 1;

            StockInData stockInData = new StockInData();
            BigDecimal owingStock = BigDecimal.ZERO;
            while (saleDIterator.hasNext()) {
                GaiaSdSaleD saleD = saleDIterator.next();
                String currentDate = CommonUtil.getyyyyMMdd();

                //移动平均价
                Example materialAssessExample = new Example(GaiaMaterialAssess.class);
                materialAssessExample.createCriteria().andEqualTo("client", saleH.getClientId()).andEqualTo("matAssessSite", saleH.getGsshBrId())
                        .andEqualTo("matProCode", saleD.getGssdProId());
                GaiaMaterialAssess materialAssess = materialAssessMapper.selectOneByExample(materialAssessExample);
                Example productExample = new Example(GaiaProductBusiness.class);
                productExample.createCriteria().andEqualTo("client", saleH.getClientId()).andEqualTo("proSite", saleH.getGsshBrId())
                        .andEqualTo("proSelfCode", saleD.getGssdProId());
                GaiaProductBusiness productBusiness = productBusinessMapper.selectOneByExample(productExample);


                Example taxExample = new Example(GaiaTaxCode.class);
                taxExample.createCriteria().andEqualTo("taxCode", productBusiness.getProOutputTax());
                GaiaTaxCode taxCode = taxCodeMapper.selectOneByExample(taxExample);

                saleD.setGssdMovTax(new BigDecimal(taxCode.getTaxCodeValue().replace("%","").trim()).divide(new BigDecimal("100")));
                if (materialAssess != null) {
                    saleD.setGssdMovPrice(materialAssess.getMatMovPrice());
                    saleD.setGssdMovPrices(materialAssess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())));
                    saleD.setGssdTaxRate(saleD.getGssdMovPrices().multiply(new BigDecimal(taxCode.getTaxCodeValue().replace("%","").trim()).divide(new BigDecimal("100"))));
                    saleD.setGssdAddAmt(materialAssess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())));
                    saleD.setGssdAddTax(saleD.getGssdMovPrices().multiply(new BigDecimal(taxCode.getTaxCodeValue().replace("%","").trim()).divide(new BigDecimal("100"))));
                }

                //修改D表的销售日期
                saleD.setGssdDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                saleDMapper.updateByPrimaryKeySelective(saleD);
                Example exampleStock = new Example(GaiaSdStock.class);
                exampleStock.createCriteria().andEqualTo("clientId", saleH.getClientId()).andEqualTo("gssBrId", saleH.getGsshBrId()).andEqualTo("gssProId", saleD.getGssdProId());

                //更新库存
                stockInData.setClientId(saleH.getClientId());
                stockInData.setGssBrId(saleH.getGsshBrId());
                stockInData.setGssProId(saleD.getGssdProId());
                stockInData.setBatchNo(saleD.getGssdBatchNo());
                stockInData.setNum(new BigDecimal(saleD.getGssdQty()));
                stockInData.setVoucherId(saleH.getGsshBillNo());
                stockInData.setSerial(saleD.getGssdSerial());
                stockInData.setEmpId(saleH.getGsshEmp());
                stockInData.setCrFlag(saleH.getGsshCrFlag());
                Object[] stockObj = consumeShopStock(stockInData, owingStock);
                owingStock = (BigDecimal) stockObj[0];
                List<ConsumeStockResponse> consumeBatchList = (List<ConsumeStockResponse>) stockObj[1];

                //组装物料凭证接口数据
                try {
                    for (int i = 0; i < consumeBatchList.size(); i++) {
                        ConsumeStockResponse consumeStockResponse = consumeBatchList.get(i);
                        String consumeBatch = consumeStockResponse.getBatch();
                        BigDecimal stockNum = consumeStockResponse.getNum();
                        MaterialDocRequestDto dto = new MaterialDocRequestDto();
                        dto.setClient(saleD.getClientId());
                        dto.setGuid(UUID.randomUUID().toString());
                        dto.setGuidNo(StrUtil.toString(guiNoCount));
                        dto.setMatType(MaterialTypeEnum.LING_SHOU.getCode());
                        dto.setMatPrice(saleD.getGssdPrc2());
                        dto.setMatPostDate(currentDate);
                        dto.setMatHeadRemark("");
                        dto.setMatProCode(saleD.getGssdProId());
                        dto.setMatSiteCode(saleH.getGsshBrId());
                        dto.setMatLocationCode("1000");
                        dto.setMatLocationTo("");
                        dto.setMatBatch(consumeBatch);
                        dto.setMatQty(stockNum.negate());
                        dto.setMatUnit("盒");
                        dto.setMatDebitCredit("H");
                        dto.setMatPoId(saleH.getGsshBillNo());
                        dto.setMatPoLineno(saleD.getGssdSerial());
                        dto.setMatDnId(saleH.getGsshBillNo());
                        dto.setMatDnLineno(saleD.getGssdSerial());
                        dto.setMatLineRemark("");
                        dto.setMatCreateBy(saleH.getGsshEmp());
                        dto.setMatCreateDate(currentDate);
                        dto.setMatCreateTime(CommonUtil.getHHmmss());
                        materialList.add(dto);
                        guiNoCount++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            //会员积分增减
            Map<String, Object> map = new HashMap<>(16);
            GaiaSdMemberCardData gaiaSdMemberCard = null;
            if (ObjectUtil.isNotEmpty(saleH.getGsshHykNo())) {
                gaiaSdMemberCard = this.memberBasicMapper.findMemberNameByCardId2(saleH.getGsshHykNo(), saleH.getClientId(), saleH.getGsshBrId());
                BigDecimal beforPoint = BigDecimal.ZERO;
                if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
                    if (!StringUtils.isEmpty(gaiaSdMemberCard.getGsmbcIntegral())) {
                        beforPoint = new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral().trim());
                    }
                    if (ObjectUtil.isNotEmpty(saleH.getGsshIntegralAdd())) { //判断是否增加积分
                        gaiaSdMemberCard.setGsmbcIntegral(beforPoint.add(new BigDecimal(saleH.getGsshIntegralAdd())).toString());
                    }
                    gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(DateUtil.date(), "yyyyMMdd"));
                    gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);

                    map.put("CLIENT", gaiaSdMemberCard.getClient());
                    map.put("GSMBC_MEMBER_ID", gaiaSdMemberCard.getGsmbcMemberId());
                    map.put("GSMBC_CARD_ID", gaiaSdMemberCard.getGsmbcCardId());
                    map.put("GSMBC_BR_ID", gaiaSdMemberCard.getGsmbcBrId());
                    GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
                    aSynchronized.setClient(gaiaSdMemberCard.getClient());
                    aSynchronized.setSite(gaiaSdMemberCard.getGsmbcBrId());
                    aSynchronized.setVersion(IdUtil.randomUUID());
                    aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
                    aSynchronized.setCommand("update");
                    aSynchronized.setSourceNo("会员-销售修改");
                    aSynchronized.setArg(JSON.toJSONString(map));
                    this.synchronizedService.addOne(aSynchronized);
                }
            }

            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("payCommit2");
            mqReceiveData.setData(saleH.getGsshBillNo());
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, saleH.getClientId() + "." + saleH.getGsshBrId(), JSON.toJSON(mqReceiveData));
            resultMap.put("materialList", materialList);

            List<GaiaSdBatchChange> batchChangeList = new ArrayList<>();
            for (GaiaSdSaleD sdSaleD : gaiaSdSaleDList) {
                GaiaSdBatchChange item = new GaiaSdBatchChange();
                item.setGsbcProId(sdSaleD.getGssdProId());
                batchChangeList.add(item);
            }

            List<GaiaSdStock> stockList = stockMapper.findByClientAndGssBrIdAndGssProId(saleH.getClientId(), saleH.getGsshBrId(), batchChangeList);
            //调外卖平台更新库存接口; GaiaSdStock是库存总表
            if (!CollectionUtils.isEmpty(stockList)) {
                stockService.syncStock(stockList);
            }
        }catch (Exception e){
            resultMap.put("N", hebSaleH.getBillNo()+":"+e.getMessage());
            return resultMap;
        }

        return resultMap;
    }

    private GaiaSdSaleH convertSaleH(SaleH hebSaleH) {
        GaiaSdSaleH saleH = new GaiaSdSaleH();
        saleH.setClientId(hebSaleH.getClient());
        saleH.setGsshBillNo(hebSaleH.getBillNo());
        saleH.setGsshBrId(hebSaleH.getBrId());
        saleH.setGsshDate(hebSaleH.getDate());
        saleH.setGsshTime(hebSaleH.getTime());
        saleH.setGsshHykNo(hebSaleH.getHykNo());
        saleH.setGsshNormalAmt(hebSaleH.getNormalAmt());
        saleH.setGsshYsAmt(hebSaleH.getYsAmt());
        saleH.setGsshIntegralAdd(hebSaleH.getIntegralAdd().toString());
        saleH.setGsshEmp(hebSaleH.getEmp());
        saleH.setGsshZkAmt(hebSaleH.getZkAmt()==null?BigDecimal.ZERO:hebSaleH.getZkAmt());
        saleH.setGsshHideFlag("0");
        saleH.setGsshPromotionType1("99");
        return saleH;
    }


    private List<GaiaSdSaleD> convertSaleDList(List<SaleD> hebSaleDList) {
        List<GaiaSdSaleD> data = new ArrayList<>();
        hebSaleDList.forEach(saleD -> {
            GaiaSdSaleD item = new GaiaSdSaleD();
            item.setClientId(saleD.getClient());
            item.setGssdBrId(saleD.getBrId());
            item.setGssdBillNo(saleD.getBillNo());
            item.setGssdAmt(saleD.getAmt());
            item.setGssdQty(saleD.getQty().toString());
            item.setGssdDate(saleD.getDate());
            item.setGssdPrc1(saleD.getPrc1());
            item.setGssdPrc2(saleD.getPrc2());
            item.setGssdProId(saleD.getProId());
            item.setGssdSerial(saleD.getSerial());
            item.setGssdSalerId(saleD.getSalerId());
            item.setGssdZkAmt(saleD.getZkAmt());
            data.add(item);
        });
        return data;
    }

    private List<GaiaSdSalePayMsg> convertPayMsg(List<PayMsg> hebPayMsgList){
        List<GaiaSdSalePayMsg> data = new ArrayList<>();
        hebPayMsgList.forEach(pay -> {
            GaiaSdSalePayMsg item = new GaiaSdSalePayMsg();
            item.setClient(pay.getClient());
            item.setGsspmBrId(pay.getBrId());
            item.setGsspmBillNo(pay.getBillNo());
            item.setGsspmAmt(pay.getAmt());
            item.setGsspmDate(pay.getDate());
            item.setGsspmCardNo(pay.getCardNo());
            item.setGsspmId(pay.getId());
            item.setGsspmName(pay.getName());
            item.setGsspmRmbAmt(pay.getRmbAmt());
            item.setGsspmType(pay.getType());
            item.setGsspmZlAmt(pay.getZlAmt());
            data.add(item);
        });
        return data;
    }

    public Object[] consumeShopStock(StockInData stockInData, BigDecimal owingStock) {
        log.info("stockInData:{}", JSON.toJSONString(stockInData));

        Object[] resultObj = new Object[2];
        //所有消耗过的批次数据
        List<ConsumeStockResponse> consumeBatchList = new ArrayList<>();
        //消耗的库存
        BigDecimal consumeNum = stockInData.getNum();
        Example stockBatchExample = new Example(GaiaSdStockBatch.class);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String now = formatter.format(date);
        stockBatchExample.createCriteria()
                .andEqualTo("clientId", stockInData.getClientId())
                .andEqualTo("gssbBrId", stockInData.getGssBrId())
                .andEqualTo("gssbProId", stockInData.getGssProId())
                .andGreaterThan("gssbVaildDate", now);

        stockBatchExample.setOrderByClause("GSSB_BATCH ASC");
        List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(stockBatchExample);
        Iterator<GaiaSdStockBatch> stockBatchIterator = stockBatchList.iterator();
        //剩余库存数
        BigDecimal surplus;
        //跳出循环的标志
        boolean breakFlag = false;

        ConsumeStockResponse consumeStockResponse;
        while (stockBatchIterator.hasNext()) {
            consumeStockResponse = new ConsumeStockResponse();
            GaiaSdStockBatch stockBatch = stockBatchIterator.next();
            BigDecimal existQty = new BigDecimal(stockBatch.getGssbQty());
            surplus = existQty.subtract(consumeNum);
            consumeStockResponse.setBatch(stockBatch.getGssbBatch());

            //当前批次的库存足够扣减
            if (existQty.compareTo(consumeNum) >= 0) {
                stockBatch.setGssbQty(StrUtil.toString(surplus));
                consumeStockResponse.setNum(consumeNum);
                breakFlag = true;
            } else {
                owingStock = surplus.add(owingStock);
                //当前批次库存不够，依次递减
                surplus = surplus.abs();
                consumeNum = surplus;
                consumeStockResponse.setNum(existQty);
                //库存被消耗完
                stockBatch.setGssbQty("0");
            }

            stockBatch.setGssbUpdateDate(CommonUtil.getyyyyMMdd());
            stockBatch.setGssbUpdateEmp(stockInData.getEmpId());
            stockBatchMapper.updateByPrimaryKeySelective(stockBatch);

            //更新库存异动表
            GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
            batchChange.setClient(stockInData.getClientId());
            batchChange.setGsbcBrId(stockInData.getGssBrId());
            batchChange.setGsbcVoucherId(stockInData.getVoucherId());
            batchChange.setGsbcSerial(stockInData.getSerial());
            batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
            batchChange.setGsbcProId(stockInData.getGssProId());
            batchChange.setGsbcBatchNo(stockBatch.getGssbBatchNo());
            batchChange.setGsbcBatch(StrUtil.isBlank(stockBatch.getGssbBatch()) ? "" : stockBatch.getGssbBatch());
            batchChange.setGsbcQty(breakFlag ? consumeNum : existQty);
            batchChangeMapper.insert(batchChange);

            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", stockInData.getClientId());
            map.put("GSSB_BR_ID", stockInData.getGssBrId());
            map.put("GSSB_PRO_ID", stockInData.getGssProId());
            map.put("GSSB_BATCH_NO", stockInData.getBatchNo());
            map.put("GSSB_BATCH", stockBatch.getGssbBatch());
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(stockInData.getClientId());
            aSynchronized.setSite(stockInData.getGssBrId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_STOCK_BATCH");
            aSynchronized.setCommand("update");
            aSynchronized.setSourceNo("销售");
            aSynchronized.setDatetime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
            aSynchronized.setArg(JSON.toJSONString(map));
            this.synchronizedService.addOne(aSynchronized);

            Map<String, Object> map2 = new HashMap<>(16);
            map2.put("CLIENT", batchChange.getClient());
            map2.put("GSBC_BR_ID", batchChange.getGsbcBrId());
            map2.put("GSBC_VOUCHER_ID", batchChange.getGsbcVoucherId());
            map2.put("GSBC_DATE", batchChange.getGsbcDate());
            map2.put("GSBC_SERIAL", batchChange.getGsbcSerial());
            map2.put("GSBC_PRO_ID", batchChange.getGsbcProId());
            map2.put("GSBC_BATCH", batchChange.getGsbcBatch());
            GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
            aSynchronized2.setClient(stockInData.getClientId());
            aSynchronized2.setSite(stockInData.getGssBrId());
            aSynchronized2.setVersion(IdUtil.randomUUID());
            aSynchronized2.setTableName("GAIA_SD_BATCH_CHANGE");
            aSynchronized2.setCommand("insert");
            aSynchronized2.setSourceNo("销售");
            aSynchronized2.setArg(JSON.toJSONString(map2));
            this.synchronizedService.addOne(aSynchronized2);

            consumeBatchList.add(consumeStockResponse);
            //足够扣减，跳出循环
            if (breakFlag) {
                consumeNum = BigDecimal.ZERO;
                break;
            }
        }

        //如果库存不足 则扣到第一行批次中
        if(consumeNum.compareTo(BigDecimal.ZERO)>0){
            consumeStockResponse = new ConsumeStockResponse();
            GaiaSdStockBatch stockBatch = stockBatchList.get(0);
            BigDecimal existQty = new BigDecimal(stockBatch.getGssbQty());
            surplus = existQty.subtract(consumeNum);
            consumeStockResponse.setBatch(stockBatch.getGssbBatch());

            stockBatch.setGssbQty(StrUtil.toString(surplus));
            consumeStockResponse.setNum(consumeNum);

            stockBatch.setGssbUpdateDate(CommonUtil.getyyyyMMdd());
            stockBatch.setGssbUpdateEmp(stockInData.getEmpId());
            stockBatchMapper.updateByPrimaryKeySelective(stockBatch);

            //更新库存异动表
            GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
            batchChange.setClient(stockInData.getClientId());
            batchChange.setGsbcBrId(stockInData.getGssBrId());
            batchChange.setGsbcVoucherId(stockInData.getVoucherId());
            batchChange.setGsbcSerial(stockInData.getSerial());
            batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
            batchChange.setGsbcProId(stockInData.getGssProId());
            batchChange.setGsbcBatchNo(stockBatch.getGssbBatchNo());
            batchChange.setGsbcBatch(StrUtil.isBlank(stockBatch.getGssbBatch()) ? "" : stockBatch.getGssbBatch());
            batchChange.setGsbcQty(breakFlag ? consumeNum : existQty);
            batchChangeMapper.insert(batchChange);

            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", stockInData.getClientId());
            map.put("GSSB_BR_ID", stockInData.getGssBrId());
            map.put("GSSB_PRO_ID", stockInData.getGssProId());
            map.put("GSSB_BATCH_NO", stockInData.getBatchNo());
            map.put("GSSB_BATCH", stockBatch.getGssbBatch());
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(stockInData.getClientId());
            aSynchronized.setSite(stockInData.getGssBrId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_STOCK_BATCH");
            aSynchronized.setCommand("update");
            aSynchronized.setSourceNo("销售");
            aSynchronized.setDatetime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
            aSynchronized.setArg(JSON.toJSONString(map));
            this.synchronizedService.addOne(aSynchronized);

            Map<String, Object> map2 = new HashMap<>(16);
            map2.put("CLIENT", batchChange.getClient());
            map2.put("GSBC_BR_ID", batchChange.getGsbcBrId());
            map2.put("GSBC_VOUCHER_ID", batchChange.getGsbcVoucherId());
            map2.put("GSBC_DATE", batchChange.getGsbcDate());
            map2.put("GSBC_SERIAL", batchChange.getGsbcSerial());
            map2.put("GSBC_PRO_ID", batchChange.getGsbcProId());
            map2.put("GSBC_BATCH", batchChange.getGsbcBatch());
            GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
            aSynchronized2.setClient(stockInData.getClientId());
            aSynchronized2.setSite(stockInData.getGssBrId());
            aSynchronized2.setVersion(IdUtil.randomUUID());
            aSynchronized2.setTableName("GAIA_SD_BATCH_CHANGE");
            aSynchronized2.setCommand("insert");
            aSynchronized2.setSourceNo("销售");
            aSynchronized2.setArg(JSON.toJSONString(map2));
            this.synchronizedService.addOne(aSynchronized2);

            consumeBatchList.add(consumeStockResponse);
        }

        stockInData.setNum(stockInData.getNum());
        stockInData.setUpdateDate(CommonUtil.getyyyyMMdd());
        stockMapper.consumeShopStockQty(stockInData);

        Map<String, Object> map = new HashMap<>(16);
        map.put("CLIENT", stockInData.getClientId());
        map.put("GSS_BR_ID", stockInData.getGssBrId());
        map.put("GSS_PRO_ID", stockInData.getGssProId());
        GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
        aSynchronized.setClient(stockInData.getClientId());
        aSynchronized.setSite(stockInData.getGssBrId());
        aSynchronized.setVersion(IdUtil.randomUUID());
        aSynchronized.setTableName("GAIA_SD_STOCK");
        aSynchronized.setCommand("update");
        aSynchronized.setSourceNo("销售");
        aSynchronized.setArg(JSON.toJSONString(map));
        this.synchronizedService.addOne(aSynchronized);

        resultObj[0] = owingStock;
        resultObj[1] = consumeBatchList;
        return resultObj;
    }
}
