package com.gys.business.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectDataEchoRequest {


    @ApiModelProperty(value = "疾病大类编码")
    private String largeSicknessCoding;


    @ApiModelProperty(value = "疾病中类编码")
    private String middleSicknessCoding;

    @ApiModelProperty(value = "成分细类编码")
    private String smallSicknessCoding;

}
