
package com.gys.business.service;

import java.util.Map;

public interface TenCentMapService {

    void updateLngAndLat(Map<String, Object> inData);
}
