package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromHyrPriceInData implements Serializable {
    private static final long serialVersionUID = 2846743411225389756L;
    private String clientId;
    private String gsphpVoucherId;
    private String gsphpSerial;
    private String gsphpProId;
    private BigDecimal gsphpPrice;
    private String gsphpRebate;
    private String gsphpInteFlag;
    private String gsphpInteRate;

}
