package com.gys.business.service;

import com.gys.business.service.data.MaterialDocRequestDto;

import java.util.List;

public interface MaterialDocService {
    String addLog(List<MaterialDocRequestDto> list);
}
