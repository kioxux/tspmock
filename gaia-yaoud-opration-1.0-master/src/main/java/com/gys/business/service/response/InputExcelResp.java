package com.gys.business.service.response;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class InputExcelResp extends BaseRowModel implements Serializable {

    @ExcelProperty(index = 0)
    @ApiModelProperty(value = "疾病大类编码")
    private String largeSicknessCoding;

    @ExcelProperty(index = 1)
    @ApiModelProperty(value = "疾病大类名称")
    private String largeSicknessName;

    @ExcelProperty(index = 2)
    @ApiModelProperty(value = "疾病中类编码")
    private String middleSicknessCoding;

    @ExcelProperty(index = 3)
    @ApiModelProperty(value = "疾病中类名称")
    private String middleSicknessName;

    @ExcelProperty(index = 4)
    @ApiModelProperty(value = "成分细类编码")
    private String smallSicknessCoding;

    @ExcelProperty(index = 5)
    @ApiModelProperty(value = "成分细类名称")
    private String smallSicknessName;
}
