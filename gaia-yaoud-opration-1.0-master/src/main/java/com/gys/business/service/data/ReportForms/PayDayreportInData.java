package com.gys.business.service.data.ReportForms;

import com.gys.business.service.data.GetPayTypeOutData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class PayDayreportInData {
    @ApiModelProperty(value = "开始日期")
    private String startDate;
    @ApiModelProperty(value = "截至日期")
    private String endDate;
    @ApiModelProperty(value = "门店")
    private String stoCode;
    @ApiModelProperty(value = "收款方式")
    private String saleType;
    private List<GetPayTypeOutData> payTypeOutData;
    private String client;
    private Integer pageNum;
    private Integer pageSize;

    @ApiModelProperty(value = "审核时间开始")
    private String checkTimeStart;

    @ApiModelProperty(value = "审核时间结束")
    private String checkTimeEnd;
}
