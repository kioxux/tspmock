package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetProductThirdlyOutData {
    private String proCode ;// "商品编码";
    private String proCommonName ;// "通用名称";
    private String name ;// "商品名称";
    private String pym ;// "助记码";
    private String specs ;// "规格";
    private String unit ;// "计量单位";
    private String form ;// "剂型";
    private String proPlace ;// "产地";
    private String proFactoryName ;// "生产企业";
    private String kysl ;// "总部数量";
    private String ifMed ;// "是否医保";
    private String proThreeCode ;// "第三方编码";
    private BigDecimal priceNormal ;// "零售价";
    private BigDecimal addAmt ;// "配送价";
    private String inventory;// "本地库存";

    private BigDecimal unitPrice;
    private String addRate; //getAddAmt
    private BigDecimal addPrice; //加点金额
    private String proNoPurchase; //禁止采购 0 否 1 是
    private String supName;
    private String proNoApply;   //是否禁止请货 0-否 1-是
    /**
     * 国际条形码
     */
    private String proBarcode;
    /**
     * 批准文号
     */
    private String proRegisterNo;
    /**
     * 销售级别
     */
    private String proSlaeClass;
}
