package com.gys.business.service;

import com.gys.business.service.data.check.*;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 15:18
 **/
public interface RequestOrderCheckService {

    List<SupplierVO> supplierList(String client);

    List<RequestOrderVO> callList(CallOrderForm orderCheckForm);

    void closeOrder(CloseOrderForm closeOrderForm);

    RequestOrderDetailListVO getOrderDetail(OrderDetailForm orderCheckForm);

    void auditOrder(OrderOperateForm orderOperateForm);

    void rejectOrder(CloseOrderForm operateForm);

    void deleteOrder(DeleteOrderForm deleteOrderForm);

    List<RequestOrderVO> orderList(OrderCheckForm orderCheckForm);

    void saveOrder(OrderOperateForm operateForm);

    List<RequestOrderDetailVO> getStoreProList(ProductForm productForm);

    boolean validPrice(String client);

}
