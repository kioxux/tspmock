package com.gys.business.service.data.his;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @desc: 订单信息
 * @author: Ryan
 * @createTime: 2021/8/25 17:02
 */
@Data
public class AddOrderForm {
    @ApiModelProperty(hidden = true)
    private Integer orderType;
    @ApiModelProperty(value = "加盟商", example = "10000003")
    private String client;
    @ApiModelProperty(value = "门店ID", example = "10000")
    private String storeId;
    @ApiModelProperty(value = "门店名称", example = "四平店")
    private String storeName;
    private String dsfOrderCode;
    @ApiModelProperty(value = "联系人姓名", example = "万荣婷")
    private String relationName;
    @ApiModelProperty(value = "联系人电话", example = "1870987123")
    private String relationPhone;
    @ApiModelProperty(value = "订单金额", example = "1299")
    private BigDecimal orderAmount;
    @ApiModelProperty(value = "立减金额（优惠金额）", example = "299")
    private BigDecimal reduceAmount;
    @ApiModelProperty(value = "支付金额", example = "1000")
    private BigDecimal paymentAmount;
    @ApiModelProperty(value = "订单状态:1-待支付 2-已支付 3-已发货 4-退款中 5-已退款 6-已取消", example = "2")
    private Integer orderStatus;
    @ApiModelProperty(value = "下单时间：（yyyy-MM-dd HH:mm:ss）", example = "2021-08-10 10:09:56")
    private Date orderTime;
    @ApiModelProperty(value = "支付时间：（yyyy-MM-dd HH:mm:ss）", example = "2021-08-10 10:09:56")
    private Date payTime;
    @ApiModelProperty(value = "取货方式：1-快递邮寄 2-到店自取", example = "1")
    private Integer pickType;
    @ApiModelProperty(value = "邮寄费用")
    private BigDecimal postFee;
    @ApiModelProperty(value = "小程序APPId", example = "wx890002321fd")
    private String appId;
    @ApiModelProperty(value = "用户openId", example = "o_myd97jdy7j78d3n3098d1")
    private String openId;
    @ApiModelProperty(value = "订单明细", example = "[]")
    private List<OrderDetailForm> orderDetailList;
    @ApiModelProperty(value = "收货地址", example = "{}")
    private AddressForm address;
    @ApiModelProperty(value = "优惠券集合",example = "[]")
    private List<OrderCouponForm> couponList;
    private RecipelForm recipelInfo;
    /**订单物品总数量*/
    private Integer orderNum;
    /**操作人*/
    private String operateEmp;

    private List<PayTypeInfo> payInfo;

    @Data
    public static class PayTypeInfo {
        /**支付类型*/
        private String payType;
        /**支付方式ID*/
        private String payId;
        /**支付方式名称*/
        private String payName;
        /**支付金额*/
        private BigDecimal payAmount;
        /**支付卡号*/
        private String payCardNo;
        /**医保卡号*/
        private String medicalCardNo;
        /**医保余额*/
        private BigDecimal medicalBalance;
        /**现金支付金额*/
        private BigDecimal cashAmount;
        /**找零金额*/
        private BigDecimal zlAmount;
    }

}
