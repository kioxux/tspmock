package com.gys.business.service;

import com.gys.business.service.data.NewStoreDistributionBaseInData;
import com.gys.business.service.data.NewStoreDistributionRecordDInData;
import com.gys.common.data.JsonResult;

public interface NewStoreDistributionService {

    JsonResult getBaseInfo(NewStoreDistributionBaseInData inData);

    JsonResult export(NewStoreDistributionRecordDInData inData);

    JsonResult getCalculationList(NewStoreDistributionBaseInData inData);
}
