package com.gys.business.service.data.check;

import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/14 17:49
 **/
@Data
public class UserVO {
    private String userId;
    private String userName;
}
