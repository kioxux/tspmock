//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetWfCreateDpgjCompadmInData {
    private String saleOrder = "";
    private String saleDate = "";
    private String storeId = "";
    private String proCode = "";
    private String proName = "";
    private String proSpecs = "";
    private String qty = "";
    private String salePrice = "";
    private String totalPrice = "";
    private String changeSalePrice = "";
    private String changeTotalPrice = "";
    private String batchCost = "";
    private String wmaCost = "";

    public GetWfCreateDpgjCompadmInData() {
    }

    public String getSaleOrder() {
        return this.saleOrder;
    }

    public String getSaleDate() {
        return this.saleDate;
    }

    public String getStoreId() {
        return this.storeId;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getQty() {
        return this.qty;
    }

    public String getSalePrice() {
        return this.salePrice;
    }

    public String getTotalPrice() {
        return this.totalPrice;
    }

    public String getChangeSalePrice() {
        return this.changeSalePrice;
    }

    public String getChangeTotalPrice() {
        return this.changeTotalPrice;
    }

    public String getBatchCost() {
        return this.batchCost;
    }

    public String getWmaCost() {
        return this.wmaCost;
    }

    public void setSaleOrder(final String saleOrder) {
        this.saleOrder = saleOrder;
    }

    public void setSaleDate(final String saleDate) {
        this.saleDate = saleDate;
    }

    public void setStoreId(final String storeId) {
        this.storeId = storeId;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setQty(final String qty) {
        this.qty = qty;
    }

    public void setSalePrice(final String salePrice) {
        this.salePrice = salePrice;
    }

    public void setTotalPrice(final String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setChangeSalePrice(final String changeSalePrice) {
        this.changeSalePrice = changeSalePrice;
    }

    public void setChangeTotalPrice(final String changeTotalPrice) {
        this.changeTotalPrice = changeTotalPrice;
    }

    public void setBatchCost(final String batchCost) {
        this.batchCost = batchCost;
    }

    public void setWmaCost(final String wmaCost) {
        this.wmaCost = wmaCost;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetWfCreateDpgjCompadmInData)) {
            return false;
        } else {
            GetWfCreateDpgjCompadmInData other = (GetWfCreateDpgjCompadmInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$saleOrder = this.getSaleOrder();
                    Object other$saleOrder = other.getSaleOrder();
                    if (this$saleOrder == null) {
                        if (other$saleOrder == null) {
                            break label167;
                        }
                    } else if (this$saleOrder.equals(other$saleOrder)) {
                        break label167;
                    }

                    return false;
                }

                Object this$saleDate = this.getSaleDate();
                Object other$saleDate = other.getSaleDate();
                if (this$saleDate == null) {
                    if (other$saleDate != null) {
                        return false;
                    }
                } else if (!this$saleDate.equals(other$saleDate)) {
                    return false;
                }

                label153: {
                    Object this$storeId = this.getStoreId();
                    Object other$storeId = other.getStoreId();
                    if (this$storeId == null) {
                        if (other$storeId == null) {
                            break label153;
                        }
                    } else if (this$storeId.equals(other$storeId)) {
                        break label153;
                    }

                    return false;
                }

                Object this$proCode = this.getProCode();
                Object other$proCode = other.getProCode();
                if (this$proCode == null) {
                    if (other$proCode != null) {
                        return false;
                    }
                } else if (!this$proCode.equals(other$proCode)) {
                    return false;
                }

                label139: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label139;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label139;
                    }

                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                label125: {
                    Object this$qty = this.getQty();
                    Object other$qty = other.getQty();
                    if (this$qty == null) {
                        if (other$qty == null) {
                            break label125;
                        }
                    } else if (this$qty.equals(other$qty)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$salePrice = this.getSalePrice();
                    Object other$salePrice = other.getSalePrice();
                    if (this$salePrice == null) {
                        if (other$salePrice == null) {
                            break label118;
                        }
                    } else if (this$salePrice.equals(other$salePrice)) {
                        break label118;
                    }

                    return false;
                }

                Object this$totalPrice = this.getTotalPrice();
                Object other$totalPrice = other.getTotalPrice();
                if (this$totalPrice == null) {
                    if (other$totalPrice != null) {
                        return false;
                    }
                } else if (!this$totalPrice.equals(other$totalPrice)) {
                    return false;
                }

                label104: {
                    Object this$changeSalePrice = this.getChangeSalePrice();
                    Object other$changeSalePrice = other.getChangeSalePrice();
                    if (this$changeSalePrice == null) {
                        if (other$changeSalePrice == null) {
                            break label104;
                        }
                    } else if (this$changeSalePrice.equals(other$changeSalePrice)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$changeTotalPrice = this.getChangeTotalPrice();
                    Object other$changeTotalPrice = other.getChangeTotalPrice();
                    if (this$changeTotalPrice == null) {
                        if (other$changeTotalPrice == null) {
                            break label97;
                        }
                    } else if (this$changeTotalPrice.equals(other$changeTotalPrice)) {
                        break label97;
                    }

                    return false;
                }

                Object this$batchCost = this.getBatchCost();
                Object other$batchCost = other.getBatchCost();
                if (this$batchCost == null) {
                    if (other$batchCost != null) {
                        return false;
                    }
                } else if (!this$batchCost.equals(other$batchCost)) {
                    return false;
                }

                Object this$wmaCost = this.getWmaCost();
                Object other$wmaCost = other.getWmaCost();
                if (this$wmaCost == null) {
                    if (other$wmaCost != null) {
                        return false;
                    }
                } else if (!this$wmaCost.equals(other$wmaCost)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetWfCreateDpgjCompadmInData;
    }

    public int hashCode() {

        int result = 1;
        Object $saleOrder = this.getSaleOrder();
        result = result * 59 + ($saleOrder == null ? 43 : $saleOrder.hashCode());
        Object $saleDate = this.getSaleDate();
        result = result * 59 + ($saleDate == null ? 43 : $saleDate.hashCode());
        Object $storeId = this.getStoreId();
        result = result * 59 + ($storeId == null ? 43 : $storeId.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $qty = this.getQty();
        result = result * 59 + ($qty == null ? 43 : $qty.hashCode());
        Object $salePrice = this.getSalePrice();
        result = result * 59 + ($salePrice == null ? 43 : $salePrice.hashCode());
        Object $totalPrice = this.getTotalPrice();
        result = result * 59 + ($totalPrice == null ? 43 : $totalPrice.hashCode());
        Object $changeSalePrice = this.getChangeSalePrice();
        result = result * 59 + ($changeSalePrice == null ? 43 : $changeSalePrice.hashCode());
        Object $changeTotalPrice = this.getChangeTotalPrice();
        result = result * 59 + ($changeTotalPrice == null ? 43 : $changeTotalPrice.hashCode());
        Object $batchCost = this.getBatchCost();
        result = result * 59 + ($batchCost == null ? 43 : $batchCost.hashCode());
        Object $wmaCost = this.getWmaCost();
        result = result * 59 + ($wmaCost == null ? 43 : $wmaCost.hashCode());
        return result;
    }

    public String toString() {
        return "GetWfCreateDpgjCompadmInData(saleOrder=" + this.getSaleOrder() + ", saleDate=" + this.getSaleDate() + ", storeId=" + this.getStoreId() + ", proCode=" + this.getProCode() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", qty=" + this.getQty() + ", salePrice=" + this.getSalePrice() + ", totalPrice=" + this.getTotalPrice() + ", changeSalePrice=" + this.getChangeSalePrice() + ", changeTotalPrice=" + this.getChangeTotalPrice() + ", batchCost=" + this.getBatchCost() + ", wmaCost=" + this.getWmaCost() + ")";
    }
}
