package com.gys.business.service.data.mib.mibEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：gyx
 * @Date ：Created in 13:51 2021/12/28
 * @Description：医疗类别枚举
 * @Modified By：gyx
 * @Version:
 */
@Getter
@AllArgsConstructor
public enum MedTypeEnum {

    PTMZ("11","普通门诊"),
    MZGH("12","门诊挂号"),
    JZ("13","急诊"),
    MZMTB("14","门诊慢特病"),
    YWSHMZ("19","意外伤害门诊"),
    PTZY("21","普通住院"),
    JZZZY("24","急诊转住院"),
    DDYDGW("41","定点药店购物"),
    SYMZ("51","生育门诊"),
    SYZY("52","生育住院"),
    JHSYSSF("53","计划生育手术费"),
    ZDJBMZ("9001","重大疾病门诊"),
    ZDJBZY("9002","重大疾病住院"),
    ETLBMZ("9003","儿童两病门诊"),
    ETLBZY("9004","儿童两病住院"),
    NDYJHMZ("9006","耐多药结核门诊"),
    NDYJHZY("9007","耐多药结核住院"),
    ZRZY("9925","转入住院"),
    XSESMZY("9921","新生儿随母住院"),
    ZGLB("9914","职工两病");

    private String code;
    private String value;

    public static MedTypeEnum getEnumByCode(String code){
        for (MedTypeEnum medTypeEnum : values()) {
            if (code.equals(medTypeEnum.getCode())) {
                return medTypeEnum;
            }
        }
        return null;
    }
}
