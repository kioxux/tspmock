package com.gys.business.service;

import com.gys.business.service.data.StoreListOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.data.remoteReview.dto.QueryReviewOutDataDTO;
import com.gys.common.data.remoteReview.dto.QueryReviewParamDTO;
import com.gys.common.data.remoteReview.dto.ReviewCheckStatus;

import java.util.ArrayList;
import java.util.List;

public interface RemoteReviewService {


    // 查询处方信息列表
    PageInfo<QueryReviewOutDataDTO> queryReviewList(GetLoginOutData userInfo, QueryReviewParamDTO queryReviewParamDTO, Integer pageNum, Integer pageSize);

    // 查询审核状态列表
    ArrayList<ReviewCheckStatus> queryReviewCheckStatus();

    // 审核处方
    void approveReview(GetLoginOutData userInfo, List<String> gssrSaleBillNoList);

    // 驳回处方
    void rejectReview(GetLoginOutData userInfo, List<String> gssrSaleBillNoList);

    // 获取审核人员校验方式 0：密码；1：指纹；2：密码和指纹
    Integer queryAuthenticationType(GetLoginOutData userInfo);

    // 通过密码校验审核用户
    void checkRegisterByPassword(GetLoginOutData userInfo, String password);

    // 通过指纹校验登录用户（预留）
    String checkRegisterByFingerMark(GetLoginOutData userInfo, String password);

    // 通过密码和指纹校验登录用户（预留）
    String checkRegisterByPasswordAndFingerMark(GetLoginOutData userInfo, String password, Object fingerMark);

    void remoteCallYunNan(GetLoginOutData userInfo, List<String> gssrSaleBillNoList);

    List<StoreListOutData> storeListQry(String client);
}
