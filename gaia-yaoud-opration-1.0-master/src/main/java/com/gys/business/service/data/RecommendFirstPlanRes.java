package com.gys.business.service.data;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.entity.RecommendFirstPalnSto;
import com.gys.business.mapper.entity.RecommendFirstPlanProductRes;
import com.gys.util.CommonUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class RecommendFirstPlanRes implements Serializable {


    private static final long serialVersionUID = 7833801429774034935L;

    private Long id;

    private String planName;

    private String effectStartDate;

//    private String effectEndDate;

//    private String effectTimeStr;

//    public String getEffectTimeStr() {
//        String res = "";
//        if (StrUtil.isNotBlank(effectStartDate)) {
//            res = effectStartDate + "-" + effectEndDate;
//        }
//        return res;
//    }


    private List<RecommendFirstPalnSto> palnStos;

    private List<String> stos;

    public List<String> getStos() {
        List<String> resList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(palnStos)) {
            palnStos.forEach(x -> {
                if (StrUtil.isNotBlank(x.getBrId())) {
                    resList.add(x.getBrId());
                }
            });
        }
        return resList;
    }

    private Integer stoNum;

    public Integer getStoNum() {
        Integer res = 0;
        if (CollectionUtil.isNotEmpty(palnStos)) {
            res = palnStos.size();
        }
        return res;
    }

    private String createName;

    private String createDate;

    public String getCreateDate() {
        String res = "";
        if(StrUtil.isNotBlank(createDate)){
            res = CommonUtil.parseyyyyMMdd(createDate);
        }
        return res;
    }

    private String status;

    private String statusStr;

    //状态 0表示已保存 1表示已审核 2表示已停用
    public String getStatusStr() {
        String res = "";
        if(StrUtil.isNotBlank(status) && "0".equals(status)){
            res = "已保存";
        }
        if(StrUtil.isNotBlank(status) && "1".equals(status)){
            res = "已审核";
        }
        if(StrUtil.isNotBlank(status) && "2".equals(status)){
            res = "已停用";
        }
        return res;
    }

    private String preferences;

    private List<String> proIds;

    private List<RecommendFirstPlanProductRes> proInfos;

}
