package com.gys.business.service.data.mib;

import com.gys.business.mapper.entity.MibDetlcutInfo;
import com.gys.business.mapper.entity.MibSetlDetail;
import com.gys.business.mapper.entity.MibSetlInfo;
import lombok.Data;

import java.util.List;

/**
 * @author wavesen.shen
 */
@Data
public class SetlInfoData {
    private MibSetlInfo mibSetlInfo;
    private List<MibSetlDetail> mibSetlDetails;
    private List<MibDetlcutInfo> mibDetlcutInfos;

}
