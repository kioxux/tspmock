//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetLoginStoreInData;

public interface CashierDutyService {
    String checkChange(GetLoginStoreInData loginOutData);
}
