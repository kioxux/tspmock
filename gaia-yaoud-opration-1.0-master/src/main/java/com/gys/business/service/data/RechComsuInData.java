//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class RechComsuInData {
    private String clientId;
    private String cardNo;
    private String mobile;
    private String stoNo;
    private String queryType;
    private String name;
    private String date;
    private String emp;
    private Integer pageNum;
    private Integer pageSize;

    public RechComsuInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getCardNo() {
        return this.cardNo;
    }

    public String getMobile() {
        return this.mobile;
    }

    public String getStoNo() {
        return this.stoNo;
    }

    public String getQueryType() {
        return this.queryType;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

    public String getEmp() {
        return this.emp;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setCardNo(final String cardNo) {
        this.cardNo = cardNo;
    }

    public void setMobile(final String mobile) {
        this.mobile = mobile;
    }

    public void setStoNo(final String stoNo) {
        this.stoNo = stoNo;
    }

    public void setQueryType(final String queryType) {
        this.queryType = queryType;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public void setEmp(final String emp) {
        this.emp = emp;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RechComsuInData)) {
            return false;
        } else {
            RechComsuInData other = (RechComsuInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$cardNo = this.getCardNo();
                Object other$cardNo = other.getCardNo();
                if (this$cardNo == null) {
                    if (other$cardNo != null) {
                        return false;
                    }
                } else if (!this$cardNo.equals(other$cardNo)) {
                    return false;
                }

                Object this$mobile = this.getMobile();
                Object other$mobile = other.getMobile();
                if (this$mobile == null) {
                    if (other$mobile != null) {
                        return false;
                    }
                } else if (!this$mobile.equals(other$mobile)) {
                    return false;
                }

                label110: {
                    Object this$stoNo = this.getStoNo();
                    Object other$stoNo = other.getStoNo();
                    if (this$stoNo == null) {
                        if (other$stoNo == null) {
                            break label110;
                        }
                    } else if (this$stoNo.equals(other$stoNo)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$queryType = this.getQueryType();
                    Object other$queryType = other.getQueryType();
                    if (this$queryType == null) {
                        if (other$queryType == null) {
                            break label103;
                        }
                    } else if (this$queryType.equals(other$queryType)) {
                        break label103;
                    }

                    return false;
                }

                Object this$name = this.getName();
                Object other$name = other.getName();
                if (this$name == null) {
                    if (other$name != null) {
                        return false;
                    }
                } else if (!this$name.equals(other$name)) {
                    return false;
                }

                label89: {
                    Object this$date = this.getDate();
                    Object other$date = other.getDate();
                    if (this$date == null) {
                        if (other$date == null) {
                            break label89;
                        }
                    } else if (this$date.equals(other$date)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$emp = this.getEmp();
                    Object other$emp = other.getEmp();
                    if (this$emp == null) {
                        if (other$emp == null) {
                            break label82;
                        }
                    } else if (this$emp.equals(other$emp)) {
                        break label82;
                    }

                    return false;
                }

                Object this$pageNum = this.getPageNum();
                Object other$pageNum = other.getPageNum();
                if (this$pageNum == null) {
                    if (other$pageNum != null) {
                        return false;
                    }
                } else if (!this$pageNum.equals(other$pageNum)) {
                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RechComsuInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $cardNo = this.getCardNo();
        result = result * 59 + ($cardNo == null ? 43 : $cardNo.hashCode());
        Object $mobile = this.getMobile();
        result = result * 59 + ($mobile == null ? 43 : $mobile.hashCode());
        Object $stoNo = this.getStoNo();
        result = result * 59 + ($stoNo == null ? 43 : $stoNo.hashCode());
        Object $queryType = this.getQueryType();
        result = result * 59 + ($queryType == null ? 43 : $queryType.hashCode());
        Object $name = this.getName();
        result = result * 59 + ($name == null ? 43 : $name.hashCode());
        Object $date = this.getDate();
        result = result * 59 + ($date == null ? 43 : $date.hashCode());
        Object $emp = this.getEmp();
        result = result * 59 + ($emp == null ? 43 : $emp.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        return result;
    }

    public String toString() {
        return "RechComsuInData(clientId=" + this.getClientId() + ", cardNo=" + this.getCardNo() + ", mobile=" + this.getMobile() + ", stoNo=" + this.getStoNo() + ", queryType=" + this.getQueryType() + ", name=" + this.getName() + ", date=" + this.getDate() + ", emp=" + this.getEmp() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
