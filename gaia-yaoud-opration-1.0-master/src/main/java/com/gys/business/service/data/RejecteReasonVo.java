package com.gys.business.service.data;

import lombok.Data;

/**
 * @Auther: tzh
 * @Date: 2022/2/9 15:30
 * @Description: RejecteReasonVo
 * @Version 1.0.0
 */
@Data
public class RejecteReasonVo {
    private String type;
    private String code;
    private String name;

}
