//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ClearBoxInfoOutData {
    private String index;
    private String gschVoucherId;
    private String gschDate;
    private String gschStatus;
    private String gschEmp;
    private String gschRemaks;
    private String proId;
    private String gscdBatchNo;
    private String clientId;
    private String brId;

    public ClearBoxInfoOutData() {
    }

    public String getIndex() {
        return this.index;
    }

    public String getGschVoucherId() {
        return this.gschVoucherId;
    }

    public String getGschDate() {
        return this.gschDate;
    }

    public String getGschStatus() {
        return this.gschStatus;
    }

    public String getGschEmp() {
        return this.gschEmp;
    }

    public String getGschRemaks() {
        return this.gschRemaks;
    }

    public String getProId() {
        return this.proId;
    }

    public String getGscdBatchNo() {
        return this.gscdBatchNo;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    public void setGschVoucherId(final String gschVoucherId) {
        this.gschVoucherId = gschVoucherId;
    }

    public void setGschDate(final String gschDate) {
        this.gschDate = gschDate;
    }

    public void setGschStatus(final String gschStatus) {
        this.gschStatus = gschStatus;
    }

    public void setGschEmp(final String gschEmp) {
        this.gschEmp = gschEmp;
    }

    public void setGschRemaks(final String gschRemaks) {
        this.gschRemaks = gschRemaks;
    }

    public void setProId(final String proId) {
        this.proId = proId;
    }

    public void setGscdBatchNo(final String gscdBatchNo) {
        this.gscdBatchNo = gscdBatchNo;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ClearBoxInfoOutData)) {
            return false;
        } else {
            ClearBoxInfoOutData other = (ClearBoxInfoOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                Object this$gschVoucherId = this.getGschVoucherId();
                Object other$gschVoucherId = other.getGschVoucherId();
                if (this$gschVoucherId == null) {
                    if (other$gschVoucherId != null) {
                        return false;
                    }
                } else if (!this$gschVoucherId.equals(other$gschVoucherId)) {
                    return false;
                }

                Object this$gschDate = this.getGschDate();
                Object other$gschDate = other.getGschDate();
                if (this$gschDate == null) {
                    if (other$gschDate != null) {
                        return false;
                    }
                } else if (!this$gschDate.equals(other$gschDate)) {
                    return false;
                }

                label110: {
                    Object this$gschStatus = this.getGschStatus();
                    Object other$gschStatus = other.getGschStatus();
                    if (this$gschStatus == null) {
                        if (other$gschStatus == null) {
                            break label110;
                        }
                    } else if (this$gschStatus.equals(other$gschStatus)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gschEmp = this.getGschEmp();
                    Object other$gschEmp = other.getGschEmp();
                    if (this$gschEmp == null) {
                        if (other$gschEmp == null) {
                            break label103;
                        }
                    } else if (this$gschEmp.equals(other$gschEmp)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gschRemaks = this.getGschRemaks();
                Object other$gschRemaks = other.getGschRemaks();
                if (this$gschRemaks == null) {
                    if (other$gschRemaks != null) {
                        return false;
                    }
                } else if (!this$gschRemaks.equals(other$gschRemaks)) {
                    return false;
                }

                label89: {
                    Object this$proId = this.getProId();
                    Object other$proId = other.getProId();
                    if (this$proId == null) {
                        if (other$proId == null) {
                            break label89;
                        }
                    } else if (this$proId.equals(other$proId)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gscdBatchNo = this.getGscdBatchNo();
                    Object other$gscdBatchNo = other.getGscdBatchNo();
                    if (this$gscdBatchNo == null) {
                        if (other$gscdBatchNo == null) {
                            break label82;
                        }
                    } else if (this$gscdBatchNo.equals(other$gscdBatchNo)) {
                        break label82;
                    }

                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ClearBoxInfoOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $gschVoucherId = this.getGschVoucherId();
        result = result * 59 + ($gschVoucherId == null ? 43 : $gschVoucherId.hashCode());
        Object $gschDate = this.getGschDate();
        result = result * 59 + ($gschDate == null ? 43 : $gschDate.hashCode());
        Object $gschStatus = this.getGschStatus();
        result = result * 59 + ($gschStatus == null ? 43 : $gschStatus.hashCode());
        Object $gschEmp = this.getGschEmp();
        result = result * 59 + ($gschEmp == null ? 43 : $gschEmp.hashCode());
        Object $gschRemaks = this.getGschRemaks();
        result = result * 59 + ($gschRemaks == null ? 43 : $gschRemaks.hashCode());
        Object $proId = this.getProId();
        result = result * 59 + ($proId == null ? 43 : $proId.hashCode());
        Object $gscdBatchNo = this.getGscdBatchNo();
        result = result * 59 + ($gscdBatchNo == null ? 43 : $gscdBatchNo.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        return result;
    }

    public String toString() {
        return "ClearBoxInfoOutData(index=" + this.getIndex() + ", gschVoucherId=" + this.getGschVoucherId() + ", gschDate=" + this.getGschDate() + ", gschStatus=" + this.getGschStatus() + ", gschEmp=" + this.getGschEmp() + ", gschRemaks=" + this.getGschRemaks() + ", proId=" + this.getProId() + ", gscdBatchNo=" + this.getGscdBatchNo() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ")";
    }
}
