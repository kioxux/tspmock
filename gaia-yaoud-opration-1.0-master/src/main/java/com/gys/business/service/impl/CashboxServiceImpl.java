//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.gys.business.mapper.*;
import com.gys.business.service.CashboxService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailOutData;
import com.gys.common.data.GetLoginOutData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CashboxServiceImpl implements CashboxService {
    @Autowired
    private GaiaSdHandoverMapper dailyReconcileMapper;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd HHmmss");
    private SimpleDateFormat simpleDateFormatTo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private DailySettlementServiceImpl dailySettlementServiceImpl;
    public CashboxServiceImpl() {
    }


    @Override
    public Integer checkPassword(CashboxInData inData) {

        return dailyReconcileMapper.checkPassword(inData) ;
    }

    @Override
    public void save(CashboxInData inData) {
        dailyReconcileMapper.save(inData); ;
    }

    @Override
    public CashboxOutData thePreviousData(CashboxInData inData) {
        return dailyReconcileMapper.thePreviousData(inData);
    }
    @Override
    public CashboxOutData getThisFrom(CashboxInData inData , GetLoginOutData userInfo) throws ParseException {

        CashboxOutData cashboxOutData = dailyReconcileMapper.thePreviousData(inData);

//        计算上一班余额
        BigDecimal balance = cashboxOutData.getBalance() == null ? new BigDecimal("0") : cashboxOutData.getBalance();
        BigDecimal pettyCashB = cashboxOutData.getPettyCashB() == null ? new BigDecimal("0") :  cashboxOutData.getPettyCashB();
        cashboxOutData.setLastShiftMoney(pettyCashB.add(balance));

        //现金来源：上一班时间到当前交班时间 现金支付的收款总额

        String inDateTime = this.simpleDateFormatTo.format(cashboxOutData.getHandoverTime()); //上一班交班时间

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_MONTH, 1);
        Date tomorrow = c.getTime();
        String toDateTime = simpleDateFormatTo.format(tomorrow);//当前时间

        inData.setInDateTime(inDateTime);
        inData.setToDateTime(toDateTime);

        CashboxOutData cashSource = getCashSource(inData, userInfo);
        cashboxOutData.setCashSource(cashSource.getCashSource());

        return cashboxOutData;
    }

    /**
     * 通过时间获取现金来源
     * @param inData
     * @param userInfo
     * @return
     * @throws ParseException
     */
    @Override
    public CashboxOutData getCashSource(CashboxInData inData , GetLoginOutData userInfo) throws ParseException {

        CashboxOutData cashboxOutData = new CashboxOutData();



        Date inDateTimeDate = this.simpleDateFormatTo.parse(inData.getInDateTime());
        String inDateTime = this.simpleDateFormat.format(inDateTimeDate);

        Date toDateTimeDate = this.simpleDateFormatTo.parse(inData.getToDateTime());
        String toDateTime = this.simpleDateFormat.format(toDateTimeDate);

        //获取当前间隔时间的交易金额(现金来源)
        Map<String, Object> map = new HashMap<>();
        map.put("client",inData.getClientId());
        map.put("brId",inData.getGpdhBrId());

        map.put("inDateTime",inDateTime);
        map.put("toDateTime",toDateTime);

        List<DailyReconcileDetailOutData> salesInquireList = dailySettlementServiceImpl.getSalesInquireList(userInfo, map);
        BigDecimal cashSource = new BigDecimal("0");
        if(salesInquireList != null){
            for (DailyReconcileDetailOutData salesInquireItem:
                    salesInquireList) {
                String saleYsAmt = salesInquireItem.getSaleYsAmt();
                if (
                        "9001".equals(salesInquireItem.getPmId())||
                        "9002".equals(salesInquireItem.getPmId())||
                        "9003".equals(salesInquireItem.getPmId())
                )   continue;
                cashSource = cashSource.add(new BigDecimal(saleYsAmt));
            }
        }
        cashboxOutData.setCashSource(cashSource);
        // 现金来源结束
        return cashboxOutData;
    }



    /**
     * 根据时间获取列表
     * @param inData
     * @return
     */
    @Override
    public List<CashboxOutData> queryList(CashboxInData inData) {
        List<CashboxOutData> cashboxOutData = dailyReconcileMapper.queryList(inData);
        return cashboxOutData;
    }

    @Override
    public List<GetUserOutData> queryUserList(CashboxInData inData){
        List<GetUserOutData> getUserOutData = dailyReconcileMapper.queryUserList(inData);
        return getUserOutData;
    }

}
