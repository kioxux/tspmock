package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回实体
 * @author xiaoyuan on 2020/12/23
 */
@Data
public class OrderOutData implements Serializable {
    private static final long serialVersionUID = -1754361084405230551L;

    /**
     * 序号
     */
    private Integer index;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 厂家
     */
    private String proFactoryName;

    /**
     * 条形码
     */
    private String proBarcode;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 参考价格
     */
    private String priceNormal;

    /**
     * 数量
     */
    private String qty;
}
