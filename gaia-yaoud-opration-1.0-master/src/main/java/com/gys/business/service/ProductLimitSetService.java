package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;

public interface ProductLimitSetService {

    ProductLimitSetInitRes init(GetLoginOutData userInfo);

    boolean checkIfInDb(GetLoginOutData userInfo, ProductLimitCheckInDb inData);

    Object save(GetLoginOutData userInfo, ProductLimitInReq inData);

    Object edit(GetLoginOutData userInfo, ProductLimitInData inData);

    PageInfo<ProductLimitRes> list(GetLoginOutData userInfo, ProductLimitSetSearchVo inData);

    ProductLimitRes editInit(GetLoginOutData userInfo, Integer id);

    Object delete(GetLoginOutData userInfo, Integer id);

    List<GetProductThirdlyOutDataExtends> getProductInfoList(GetLoginOutData userInfo, List<String> importInDataList);

    /**
     * 根据FX 传来的商品 验证 是否 限购
     * @param userInfo
     * @param map
     * @return
     */
    void commodityPurchaseRestrictions(GetLoginOutData userInfo, PurchaseLimitVo map);

}
