package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class StoreStockOutData implements Serializable {

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "店名")
    private String brName;

    @ApiModelProperty(value = "库存品项数")
    private BigDecimal stockProCount;

    @ApiModelProperty(value = "库存成本额")
    private BigDecimal stockCostAmt;

    @ApiModelProperty(value = "周转天数")
    private BigDecimal turnoverDays;

}
