package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaSdIshFeeDetailMapper;
import com.gys.business.mapper.GaiaSdIshMapper;
import com.gys.business.mapper.entity.GaiaSdIsh;
import com.gys.business.mapper.entity.GaiaSdIshFeeDetail;
import com.gys.business.service.IshService;
import com.gys.business.service.data.DongruanYb.SaleReturnOutput;
import com.gys.common.data.GetLoginOutData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @Description 东软对接
 * @Author huxinxin
 * @Date 2021/3/28 14:02
 * @Version 1.0.0
 **/
@Service
@Slf4j
public class IshServiceImpl implements IshService {

    @Autowired
    private GaiaSdIshMapper ishMapper;

    @Autowired
    private GaiaSdIshFeeDetailMapper ishFeeDetailMapper;

    @Override
    @Transactional
    public void insertIsh(Map record){
        String client  = String.valueOf(record.get("client"));
        String ishBrId  = String.valueOf(record.get("ishBrId"));
        if(ObjectUtil.isNotEmpty(client) || ObjectUtil.isNotEmpty(ishBrId)) {
            if (ObjectUtil.isNotEmpty(record.get("ish"))) {
                GaiaSdIsh ish = JSONObject.parseObject(record.get("ish").toString(),GaiaSdIsh.class);
                ish.setClient(client);
                ish.setIshBrId(ishBrId);
                ishMapper.insertSelective(ish);
            }
            if (ObjectUtil.isNotEmpty(record.get("ishFeeDetail"))) {
                List<GaiaSdIshFeeDetail> ishFeeDetail = JSONObject.parseArray( record.get("ishFeeDetail").toString(),GaiaSdIshFeeDetail.class);
                for (GaiaSdIshFeeDetail detail : ishFeeDetail){
                    detail.setClient(client);
                    detail.setIshBrId(ishBrId);
                }
                ishFeeDetailMapper.insertSelectiveBatch(ishFeeDetail);
            }
        }

    }

    @Override
    public void insertIshFeeDetail(GaiaSdIshFeeDetail record){
        record = new GaiaSdIshFeeDetail();
        if (ObjectUtil.isNotEmpty(record)){

        }
    }

    @Override
    public GaiaSdIsh queryIsh(Map record) {
        return ishMapper.queryIsh(record);
    }

    @Override
    @Transactional
    public void ishReturn(GetLoginOutData userInfo, Map record) {
        record.put("client",userInfo.getClient());
        record.put("ishBrId",userInfo.getDepId());
        GaiaSdIsh ish = ishMapper.queryIsh(record);
//        record.get("");
        SaleReturnOutput saleReturnOut = JSONObject.parseObject(record.get("saleReturnOutput").toString(),SaleReturnOutput.class);
        ish.setClient(userInfo.getClient());
        ish.setIshBrId(userInfo.getDepId());
        ish.setIshDealSerial(saleReturnOut.getZjsjljylsh());
        ish.setIshFoundPay(saleReturnOut.getYttcje());
        ish.setIshPersonalCurrPay(saleReturnOut.getYtzhje());
        ish.setIshPersonalTotalPay(saleReturnOut.getYtgwybz());
        ish.setIshCashPay(saleReturnOut.getYtxzje());
        ish.setIshCashPayD(saleReturnOut.getYtdelpje());
        ish.setIshQfzfAmt(saleReturnOut.getYtlsqfxgwyfh());
        ish.setIshQfbzcashAmt(saleReturnOut.getYtmzjzje());
        ish.setIshCgfdxgrzfAmt(saleReturnOut.getYtshjzjjzfs());
        ish.setIshJydate(DateUtil.parse(saleReturnOut.getZxjssj(), "yyyyMMdd"));
        ish.setIshJsqzhyeAmt(saleReturnOut.getYtsyjjje());
        ish.setIshJsqbnzhyeAmt(saleReturnOut.getYtsyxjzf());
        ish.setIshJsqlnzhyeAmt(saleReturnOut.getYtgsjjje());
        ish.setIshJshzhyeAmt(saleReturnOut.getYtgsxjje());
        ish.setIshLjmjjAmt(saleReturnOut.getYtqtbz());
        ish.setIshSnetjjAmt(saleReturnOut.getYtsyzhzf());
        ish.setIshNmgjjAmt(saleReturnOut.getYtgszhzf());
        ish.setIshStatus("1");
        ish.setIshTxCode("01");
        ishMapper.insertSelective(ish);

    }
}
