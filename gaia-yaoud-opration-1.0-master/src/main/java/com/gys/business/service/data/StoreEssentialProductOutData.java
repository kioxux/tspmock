package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class StoreEssentialProductOutData implements Serializable {

    /**
     * 门店编码+商品自编码拼接字符串
     */
    @ApiModelProperty(value="门店编码+商品自编码拼接字符串")
    private String storeProCodeStr;

    /**
     * 是否被过滤 0-没被过滤 1-被过滤
     */
    @ApiModelProperty(value="是否被过滤 0-没被过滤 1-被过滤")
    private String isRule;
}
