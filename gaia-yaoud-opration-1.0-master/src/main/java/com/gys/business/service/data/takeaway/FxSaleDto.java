package com.gys.business.service.data.takeaway;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/2 11:03
 */
@Data
public class FxSaleDto {

    private String billNo;//销售单号
    private String billNoReturn;//退货时有值，存储原销售单号
}
