package com.gys.business.service.data.takeaway;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/9 9:55
 */
@Data
public class FxYlOrderDetailInputBean {

    private String skuId;
    private String batchNo;
    private String expiryDate;
}
