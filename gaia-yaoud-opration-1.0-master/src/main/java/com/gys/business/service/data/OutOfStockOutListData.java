package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OutOfStockOutListData implements Serializable {

    private static final long serialVersionUID = -3781934088499015328L;

    @ApiModelProperty(value = "产品编号")
    private String productId;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "产品规格")
    private String productSpecs;

    @ApiModelProperty(value = "生产厂家")
    private String factoryName;

    @ApiModelProperty(value = "月均销量")
    private BigDecimal avgQty;

    @ApiModelProperty(value = "月均销售额")
    private BigDecimal avgAmt;

    @ApiModelProperty(value = "月均毛利额")
    private BigDecimal avgProfitAmt;

    @ApiModelProperty(value = "是否特殊商品 0-普通商品 1-特殊商品")
    private String isSpecial;

    @ApiModelProperty(value = "仓库")
    private String dcCode;

    @ApiModelProperty(value = "零售价")
    private BigDecimal priceNormal;

    @ApiModelProperty(value = "剩余门店库存")
    private String storeStock;

    @ApiModelProperty(value = "补货日期")
    private String replenishDate;

    @ApiModelProperty(value = "是否提示已处理 1-提示")
    private String source;
}
