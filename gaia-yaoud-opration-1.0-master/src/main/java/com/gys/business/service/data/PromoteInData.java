package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PromoteInData implements Serializable {
    private static final long serialVersionUID = -1790664280114343445L;
    private String clientId;
    private String gsphBrId;
    private String gsphVoucherId;
    private String gsphTheme;
    private String gsphName;
    private String gsphRemarks;
    private String gsphStatus;
    private String gsphBeginDate;
    private String gsphEndDate;
    private String gsphDateFrequency;
    private String gsphWeekFrequency;
    private String gsphBeginTime;
    private String gsphEndTime;
    private String gsphType;
    private String gsphPart;
    private String gsphPara1;
    private String gsphPara2;
    private String gsphPara3;
    private String gsphPara4;
    private String gsphExclusion;
    private String storeType;
    private String storeStart;
    private String storeEnd;
    private String proCode;
    private String storeGroup;
    public int pageSize;
    private int pageNum;
    private List<String> storeIds;
    private List<String> storeList;
    private List<String> idList;
    private List<PromUnitarySetInData> promUnitarySetInDataList;
    private List<PromSeriesCondsInData> promSeriesCondsInDataList;
    private List<PromSeriesSetInData> promSeriesSetInDataList;
    private List<PromGiftCondsInData> promGiftCondsInDataList;
    private List<PromGiftSetInData> promGiftSetInDataList;
    private List<PromGiftResultInData> promGiftResultInDataList;
    private List<PromCouponSetInData> promCouponSetInDataList;
    private List<PromCouponGrantInData> promCouponGrantInDataList;
    private List<PromCouponUseInData> promCouponUseInDataList;
    private List<PromHySetInData> promHySetInDataList;
    private List<PromHyrDiscountInData> promHyrDiscountInDataList;
    private List<PromHyrPriceInData> promHyrPriceInDataList;
    private List<PromAssoCondsInData> promAssoCondsInDataList;
    private List<PromAssoSetInData> promAssoSetInDataList;
    private List<PromAssoResultInData> promAssoResultInDataList;
}
