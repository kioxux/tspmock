package com.gys.business.service.impl;


import com.github.pagehelper.util.StringUtil;
import com.gys.business.mapper.GaiaEntNewProductEvaluationDMapper;
import com.gys.business.mapper.GaiaEntNewProductEvaluationHMapper;
import com.gys.business.mapper.GaiaProductBusinessMapper;
import com.gys.business.mapper.GaiaSdMessageMapper;
import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;
import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationH;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.service.CompanyOutOfStockService;
import com.gys.business.service.GaiaEntNewProductEvaluationHService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CommonUtil;
import com.gys.util.CosUtils;
import com.gys.util.DateUtil;
import com.gys.util.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * 公司级-新品评估-主表(GaiaEntNewProductEvaluationH)表服务实现类
 *
 * @author makejava
 * @since 2021-07-19 10:16:54
 */
@Service("gaiaEntNewProductEvaluationHService")
@Slf4j
public class GaiaEntNewProductEvaluationHServiceImpl implements GaiaEntNewProductEvaluationHService {
    @Resource
    private GaiaEntNewProductEvaluationHMapper gaiaEntNewProductEvaluationHMapper;
    @Resource
    private GaiaEntNewProductEvaluationDMapper gaiaEntNewProductEvaluationDMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    private CompanyOutOfStockService companyOutOfStockService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertEntProductData(String pa) {
        long startTime = System.currentTimeMillis();
        System.out.println("开始时间:" + startTime);
        List<String> clientList = gaiaEntNewProductEvaluationHMapper.listClient();
        if (!CollectionUtils.isEmpty(clientList)) {
            Date now = new Date();
            Date billDate = new Date();
            String nowString = DateUtil.formatDate(now);
            String thisWeekMonday = DateUtil.formatDate(DateUtil.getThisWeekMonday(now));
            String thisMonthFirstDay = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getThisMonthFirstDay(now),  4));
            String singleMonthFirstDay = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getSingleMonthFirstDay(now),  4));
            String quarterlyFirstDay = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getQuarterlyFirstDay(now),  4));

            for (String client : clientList) {
                //查询后台参数
                HashMap<String, Object> paramMap = gaiaEntNewProductEvaluationHMapper.getClientParam(client);
                boolean flag = false;
                flag = checkFlag(nowString, thisWeekMonday, thisMonthFirstDay, singleMonthFirstDay, quarterlyFirstDay, paramMap, flag);
                GaiaEntNewProductEvaluationInData inData = new GaiaEntNewProductEvaluationInData();
                inData.setClient(client);
                List<HashMap<String, Object>> dcList = gaiaEntNewProductEvaluationHMapper.listCK(inData);
                //sender 用于实时生成数据用测试
                if (flag || "sender".equals(pa)) {
                    String param = "90";
                    if (!ObjectUtils.isEmpty(paramMap) && !ObjectUtils.isEmpty(paramMap.get("param2"))) {
                        param = paramMap.get("param2").toString();
                    }
                    List<GaiaEntNewProductEvaluationDOutData> waitGoods = gaiaEntNewProductEvaluationHMapper.listWaitEvalustion(param,client);
                    if (!CollectionUtils.isEmpty(waitGoods)) {
                        //商品品类报表 top1000
                        List<ProductRank> top1000List = companyOutOfStockService.getCategoryModelList(client, Integer.parseInt(param));

                        String billNo = gaiaEntNewProductEvaluationHMapper.getBillNo(client);
                        GaiaEntNewProductEvaluationH evaluationH = setHead(billDate, client, billNo);
                        gaiaEntNewProductEvaluationHMapper.insert(evaluationH);
                        List<GaiaEntNewProductEvaluationD> insertDetail =new ArrayList<>();
                        for (GaiaEntNewProductEvaluationDOutData waitGood : waitGoods) {
                            waitGood.setParam2(param);
                            //查90天毛利，铺货门店数，动销门店数
                            GaiaEntNewProductEvaluationDOutData storeQty = gaiaEntNewProductEvaluationHMapper.getGrossAndStoreQty(waitGood);
                            GaiaEntNewProductEvaluationD evaluationD = setDetail(billDate, client, billNo, waitGood, storeQty,param);
                            //查询该条药品的成分
                            List<String> commentClassList = gaiaEntNewProductEvaluationHMapper.getCommentClass(waitGood);
                            waitGood.setCommentClass(commentClassList.get(0));
                            //查询相关的必备成分或不必被成分
                            //当GAIA_PRODUCT_BUSINESS 中该加盟商  药品中该 商品成分 的药品编码 >1 为非必备  =1 为必备
                            //status =1 为必备  status =2 为非必备
                            waitGood.setStoreId(dcList.get(0).get("dcCode").toString());
                            GaiaEntNewProductEvaluationDOutData mustHaveStatus = gaiaEntNewProductEvaluationHMapper.getCommentStstus(waitGood);
                            if (waitGood.getSalesQuantity() > 0) {
                                GaiaEntNewProductEvaluationDOutData averagePrice = gaiaEntNewProductEvaluationHMapper.getAveragePrice(waitGood);
                                //动销
                                if (1 >= mustHaveStatus.getMustHaveStatus()) {
                                    //必备
                                    evaluationD.setSuggestedStatus(4);
                                    evaluationD.setStatus(1);
                                } else {
                                    //非必备  单品单店平均销售额 = 单品销售额之和 / 单品动销天数（在计算范围内 有销售的日期 加盟商+店号+商品编码+销售日期 去重）
                                    //       所在成分分类商品的单品单店平均销售额 = 成分分类内所有商品的销售额之和/成分分类内所有商品动销天数之和
                                    //单品单店平均销售额>=所在成分分类商品的单品单店平均销售额   状态为 3 强烈建议保留

                                    if (!ObjectUtils.isEmpty(averagePrice) && averagePrice.getDpddprice().compareTo(averagePrice.getClassAveragePrice()) >= 0) {
                                        evaluationD.setSuggestedStatus(3);
                                        evaluationD.setStatus(1);
                                    } else {
                                        //todo 等强哥那边的表建好
                                        boolean topFlag = false;
                                        if (!ObjectUtils.isEmpty(averagePrice)) {
                                            for (ProductRank productRank : top1000List) {
                                                if (productRank.getProSelfCode().equals(productRank.getProSelfCode())) {
                                                    topFlag = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (topFlag) {
                                            //单品单店平均销售额<所在成分分类商品的单品单店平均销售额  且商品品类排名<=1000位 状态为 4 建议保留
                                            evaluationD.setSuggestedStatus(4);
                                            evaluationD.setStatus(1);
                                        } else {
                                            //单品单店平均销售额<所在成分分类商品的单品单店平均销售额  且商品品类排名》1000位 状态为 2 建议淘汰
                                            evaluationD.setSuggestedStatus(2);
                                            evaluationD.setStatus(0);
                                        }
                                    }

                                }
                            } else {
                                //不动销
                                if (1 >= mustHaveStatus.getMustHaveStatus()) {
                                    //必备
                                    evaluationD.setSuggestedStatus(1);
                                    evaluationD.setStatus(1);
                                } else {
                                    //非必备
                                    evaluationD.setSuggestedStatus(2);
                                    evaluationD.setStatus(0);
                                }
                            }
                            insertDetail.add(evaluationD);
                        }
                        gaiaEntNewProductEvaluationDMapper.insertBatch(insertDetail);
                        GaiaSdMessage sdMessage = new GaiaSdMessage();
                        sdMessage.setClient(client);
                        sdMessage.setGsmId("product");//针对所有加盟商
                        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(client, "company");
                        sdMessage.setGsmVoucherId(voucherId);//消息流水号
                        sdMessage.setGsmType(SdMessageTypeEnum.ENT_NEW_PRODUCT_EVALUATION.code);
                        sdMessage.setGsmPage(SdMessageTypeEnum.ENT_NEW_PRODUCT_EVALUATION.page);
                        sdMessage.setGsmRemark("您有<font color=\"#FF0000\">" + insertDetail.size() + "</font>个新品评估未处理，请处理。");
                        sdMessage.setGsmFlag("N");//是否查看
                        sdMessage.setGsmWarningDay(billNo);//单号
                        sdMessage.setGsmPlatForm("WEB");//消息渠道
                        sdMessage.setGsmDeleteFlag("0");
                        sdMessage.setGsmArriveDate(DateUtil.formatDate(billDate));
                        sdMessage.setGsmArriveTime(DateUtil.dateToString(billDate, "HHmmss"));
                        gaiaSdMessageMapper.insertSelective(sdMessage);
                    }
                }

            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("结束时间:" + endTime);
        System.out.println("使用了时间:" + (endTime - startTime));
        return true;
    }

    private boolean isFlag(String nowString, String thisWeekMonday, String thisMonthFirstDay, String singleMonthFirstDay, String quarterlyFirstDay, boolean flag, String param1) {
        //1、推送周期：周=每周一推送
        if ("1".equals(param1) && nowString.equals(thisWeekMonday)) {
            flag = true;
        } //2、推送周期：月=每月1日推送
        else if ("2".equals(param1) && nowString.equals(thisMonthFirstDay)) {
            flag = true;
        }//3、推送周期：隔月=单月1日推送
        else if ("3".equals(param1) && nowString.equals(singleMonthFirstDay)) {
            flag = true;
        }//4、推送周期：季度=每季度首月1日推送
        else if ("4".equals(param1) && nowString.equals(quarterlyFirstDay)) {
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean updateEntProductData() {
        Date now = new Date();
        String nowString = DateUtil.formatDate(now);
        String thisWeekMonday = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getThisWeekMonday(now),5));
        String thisMonthFirstDay = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getThisMonthFirstDay(now),15));
        String singleMonthFirstDay = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getSingleMonthFirstDay(now),15));
        String quarterlyFirstDay = DateUtil.formatDate(DateUtil.moveTime(DateUtil.getQuarterlyFirstDay(now),15));
        GaiaEntNewProductEvaluationInData param = new GaiaEntNewProductEvaluationInData();
        param.setStatus(0);
        List<GaiaEntNewProductEvaluationDOutData> billList = gaiaEntNewProductEvaluationHMapper.listEntProductBill(param);
        for (GaiaEntNewProductEvaluationDOutData billInfo : billList) {
            HashMap<String, Object> paramMap = gaiaEntNewProductEvaluationHMapper.getClientParam(billInfo.getClient());
            boolean flag = false;
            flag = checkFlag(nowString, thisWeekMonday, thisMonthFirstDay, singleMonthFirstDay, quarterlyFirstDay, paramMap, flag);
            if (flag) {
                GetLoginOutData login = new GetLoginOutData();
                login.setUserId("system");
                GaiaEntNewProductEvaluationH param2 = new GaiaEntNewProductEvaluationH();
                param2.setBillCode(billInfo.getBillCode());
                param2.setClient(billInfo.getClient());
                param2.setId(billInfo.getId());
                this.confirmBill(login, param2);
            }
        }
        return true;
    }

    private boolean checkFlag(String nowString, String thisWeekMonday, String thisMonthFirstDay, String singleMonthFirstDay, String quarterlyFirstDay, HashMap<String, Object> paramMap, boolean flag) {
        String param1 = "2";
        if (!ObjectUtils.isEmpty(paramMap) && !ObjectUtils.isEmpty(paramMap.get("param1"))) {
            param1 = paramMap.get("param1").toString();
        }
        //校验日期
        flag = isFlag(nowString, thisWeekMonday, thisMonthFirstDay, singleMonthFirstDay, quarterlyFirstDay, flag, param1);
        return flag;
    }

    @Override
    public List<GaiaEntNewProductEvaluationDOutData> listEntProductBill(GaiaEntNewProductEvaluationInData param) {
        if (StringUtil.isNotEmpty(param.getProSelfCode())) {
            List<String> billCodeList = gaiaEntNewProductEvaluationHMapper.listBillCodeByProSelfCode(param);
            if (CollectionUtils.isEmpty(billCodeList)) {
                return null;
            } else {
                param.setBillCodeList(billCodeList);
            }
        }
        return gaiaEntNewProductEvaluationHMapper.listEntProductBill(param);
    }

    @Override
    public GaiaEntNewProductEvaluationDOutData listEntProductBillDetail(GaiaEntNewProductEvaluationInData param) {
        checkParam(param);
        GaiaEntNewProductEvaluationH evaluationH = new GaiaEntNewProductEvaluationH();
        evaluationH.setClient(param.getClient());
        evaluationH.setBillCode(param.getBillCode());
        GaiaEntNewProductEvaluationDOutData headInfo = gaiaEntNewProductEvaluationHMapper.getHintInfo(evaluationH);
        List<GaiaEntNewProductEvaluationDOutData> detailList = gaiaEntNewProductEvaluationHMapper.listEntProductBillDetail(param);
        if (!CollectionUtils.isEmpty(detailList)) {
            headInfo.setDetailList(detailList);
        }
        return headInfo;
    }

    @Override
    public List<HashMap<String, Object>> listEntInfo(GaiaEntNewProductEvaluationInData param) {
        return gaiaEntNewProductEvaluationHMapper.listEntInfo(param);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateDetailList(GetLoginOutData userInfo, List<GaiaEntNewProductEvaluationD> list) {
        if (!CollectionUtils.isEmpty(list)) {
            Date now = new Date();
            for (GaiaEntNewProductEvaluationD evaluationD : list) {
                evaluationD.setDealStatus(1);
                evaluationD.setUpdateUser(userInfo.getUserId());
                evaluationD.setUpdateTime(now);
                gaiaEntNewProductEvaluationDMapper.update(evaluationD);
            }
        } else {
            throw new BusinessException("请选择需要保存的明细！");
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean confirmBill(GetLoginOutData userInfo, GaiaEntNewProductEvaluationH param) {
        Date now = new Date();
        param.setUpdateTime(now);
        param.setUpdateUser(userInfo.getUserId());
        param.setFinishTime(now);
        param.setStatus(1);
        gaiaEntNewProductEvaluationHMapper.update(param);
        List<GaiaEntNewProductEvaluationD> detailList = gaiaEntNewProductEvaluationHMapper.listUnDealList(param);
        if (!CollectionUtils.isEmpty(detailList)) {
            for (GaiaEntNewProductEvaluationD evaluationD : detailList) {
                evaluationD.setUpdateTime(now);
                evaluationD.setUpdateUser(userInfo.getUserId());
                evaluationD.setDealStatus(1);
                gaiaEntNewProductEvaluationDMapper.update(evaluationD);
            }
        }
        //商品定位回写  GAIA_PRODUCT_BUSINESS 评估结果转正常回写定位为H，评估结果淘汰回写定位为T
        //查询所有参与评估得门店
        GaiaEntNewProductEvaluationInData inData = new GaiaEntNewProductEvaluationInData();
        inData.setClient(param.getClient());
        inData.setBillCode(param.getBillCode());
        List<HashMap<String, Object>> stoList = gaiaEntNewProductEvaluationHMapper.listEntInfo(inData);
        List<HashMap<String, Object>> dcList = gaiaEntNewProductEvaluationHMapper.listCK(inData);
        List<GaiaEntNewProductEvaluationDOutData> outDataList = gaiaEntNewProductEvaluationHMapper.listEntProductBillDetail(inData);
        for (GaiaEntNewProductEvaluationDOutData outData : outDataList) {
            if (!CollectionUtils.isEmpty(stoList)) {
                for (HashMap<String, Object> map : stoList) {
                    InvalidOutOfStockInData gaiaProductBusiness = new InvalidOutOfStockInData();
                    gaiaProductBusiness.setClientId(param.getClient());
                    gaiaProductBusiness.setBrId(map.get("stoCode").toString());
                    gaiaProductBusiness.setProductId(outData.getProSelfCode());
                    String status = checkStatus(outData);
                    gaiaProductBusiness.setPosition(status);
                    //update
                    gaiaProductBusinessMapper.updateByProductId(gaiaProductBusiness);
                }
            }
            if (!CollectionUtils.isEmpty(dcList)) {
                for (HashMap<String, Object> dc : dcList) {
                    InvalidOutOfStockInData gaiaProductBusiness = new InvalidOutOfStockInData();
                    gaiaProductBusiness.setClientId(param.getClient());
                    gaiaProductBusiness.setBrId(dc.get("dcCode").toString());
                    gaiaProductBusiness.setProductId(outData.getProSelfCode());
                    String status = checkStatus(outData);
                    gaiaProductBusiness.setPosition(status);
                    //update
                    gaiaProductBusinessMapper.updateByProductId(gaiaProductBusiness);
                }
            }
        }

        return true;
    }

    private String checkStatus(GaiaEntNewProductEvaluationDOutData outData) {
        String status = "";
        if (0 == outData.getStatus()) {
            status = "T";
        } else if (2 == outData.getStatus()) {
            status = "D";
        } else {
            status = "Z";
        }
        return status;
    }

    @Override
    public GaiaEntNewProductEvaluationDOutData confirmBillHint(GetLoginOutData userInfo, GaiaEntNewProductEvaluationH param) {
        return gaiaEntNewProductEvaluationHMapper.getHintInfo(param);
    }

    @Override
    public Object export(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param) {
        List<GaiaEntNewProductEvaluationDOutData> detailList = gaiaEntNewProductEvaluationHMapper.listEntProductBillDetail(param);
        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>(detailList.size());
        for (GaiaEntNewProductEvaluationDOutData detail : detailList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //评估状态
            lineList.add(detail.getDealStatusName());
            //通用编码
            lineList.add(detail.getProSelfCode());
            //名称
            lineList.add(detail.getProName());
            //商品规格
            lineList.add(detail.getProSpecs());
            //商品生产厂家
            lineList.add(detail.getProFactoryName());
            //90天销量
            lineList.add(detail.getSalesQuantity());
            //90天销售额
            lineList.add(detail.getSalesAmt());
            //90毛利额
            lineList.add(detail.getGross());
            //铺货门店数
            lineList.add(detail.getStoreQty());
            //动销门店数
            lineList.add(detail.getSalesStoreQty());
            //动销率
            lineList.add(detail.getSaleRate());
            //商品分类
            lineList.add(detail.getProCompBigName());
            //成分分类
            lineList.add(detail.getProBigClassName());
            //评估建议
            lineList.add(detail.getSuggestion());
            //评估结果
            lineList.add(detail.getStatusName());
            dataList.add(lineList);
        }
        List<HashMap<String, Object>> hashMaps = gaiaEntNewProductEvaluationHMapper.listEntInfo(param);
        //组装导出内容
        List<List<Object>> entList = new ArrayList<>(detailList.size());
        for (HashMap<String, Object> ent : hashMaps) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //门店id
            lineList.add(ent.get("stoCode"));
            //门店名称
            lineList.add(ent.get("stoName"));
            //门店属性
            lineList.add(ent.get("stoAttribute"));
            //店型
            lineList.add(ent.get("stoType"));
            //管理属性
            lineList.add(ent.get("manageAttributes"));
            entList.add(lineList);
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.ENT_NEW_PRODUCT_EXPORT_EXCEL_HEAD);
                    add(CommonConstant.ENT_INFO_EXPORT_EXCEL_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                    add(entList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.ENT_NEW_PRODUCT_EXPORT_SHEET_NAME);
                    add(CommonConstant.ENT_INFO_EXPORT_SHEET_NAME);
                }});
        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.ENT_NEW_PRODUCT_EXPORT_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }


    @Override
    public Object listEntProductionDetail(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param) {
        if ("0".equals(param.getStoAttribute())) {
            param.setStoAttribute(null);
        }
        GaiaEntNewProductEvaluationH evaluationH = new GaiaEntNewProductEvaluationH();
        evaluationH.setClient(param.getClient());
        evaluationH.setBillCode(param.getBillCode());
        GaiaEntNewProductEvaluationDOutData head = gaiaEntNewProductEvaluationHMapper.getHintInfo(evaluationH);
        if (!ObjectUtils.isEmpty(head)) {
            List<GaiaEntNewProductEvaluationDOutData> outDataList = gaiaEntNewProductEvaluationHMapper.listEntProductionDetail(param);
            for (GaiaEntNewProductEvaluationDOutData outData : outDataList) {
                outData.setBillCode(param.getBillCode());
                outData.setBillDate(head.getBillDate());
                List<GaiaEntNewProductEvaluationDOutData> infoList = gaiaEntNewProductEvaluationHMapper.listPGQty(outData);
                if (!CollectionUtils.isEmpty(infoList)) {
                    int pgQty = 0;
                    int zcQty = 0;
                    int ttQty = 0;
                    for (GaiaEntNewProductEvaluationDOutData gaiaEntNewProductEvaluationDOutData : infoList) {
                        if (gaiaEntNewProductEvaluationDOutData.getStatus() == 0) {
                            //淘汰
                            ttQty += gaiaEntNewProductEvaluationDOutData.getPgQty();
                        }
                        if (gaiaEntNewProductEvaluationDOutData.getStatus() == 1) {
                            //正常
                            zcQty += gaiaEntNewProductEvaluationDOutData.getPgQty();
                        }
                        pgQty += gaiaEntNewProductEvaluationDOutData.getPgQty();
                    }
                    outData.setPgQty(pgQty);
                    outData.setTTQty(ttQty);
                    outData.setZCQty(zcQty);
                }
            }
            head.setDetailList(outDataList);
            return head;
        } else {
            throw new BusinessException("未查询到该单据明细！");
        }
    }

    @Override
    public GaiaNewProductEvaluationOutData listEntProductionResult(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param) {
        return gaiaEntNewProductEvaluationHMapper.listEntProductionResult(param);
    }

    @Override
    public Object getEntProductionResultDetail(GetLoginOutData userInfo, GaiaEntNewProductEvaluationInData param) {
        GaiaEntNewProductEvaluationH evaluationH = new GaiaEntNewProductEvaluationH();
        evaluationH.setClient(userInfo.getClient());
        evaluationH.setBillCode(param.getBillCode());
        GaiaEntNewProductEvaluationDOutData hintInfo = gaiaEntNewProductEvaluationHMapper.getHintInfo(evaluationH);
        if (2 == param.getSendRange()) {
            //正常
            param.setStatus(1);
        } else if (3 == param.getSendRange()) {
            //淘汰
            param.setStatus(0);
        } else if (4 == param.getSendRange()) {
            //转订购
            param.setStatus(2);
        }
        List<GaiaEntNewProductEvaluationDOutData> outDataList = gaiaEntNewProductEvaluationHMapper.listEntProductBillDetail(param);
        hintInfo.setDetailList(outDataList);
        return hintInfo;
    }

    @Override
    public Object updateResult(GetLoginOutData userInfo, GaiaEntNewProductEvaluationH param) {
        param.setReadFlag(1);
        return gaiaEntNewProductEvaluationHMapper.update(param);
    }

    private void checkParam(GaiaEntNewProductEvaluationInData param) {
        if ("ALL".equals(param.getStatus())) {
            param.setStatus(null);
        }
        if ("ALL".equals(param.getDealStatus())) {
            param.setDealStatus(null);
        }
    }

    private GaiaEntNewProductEvaluationD setDetail(Date now, String client, String billNo, GaiaEntNewProductEvaluationDOutData waitGood, GaiaEntNewProductEvaluationDOutData storeQty, String param) {
        GaiaEntNewProductEvaluationD evaluationD = new GaiaEntNewProductEvaluationD();
        evaluationD.setClient(client);
        evaluationD.setBillCode(billNo);
        evaluationD.setProSelfCode(waitGood.getProSelfCode());
        if (waitGood.getSalesAmt() < 0) {
            evaluationD.setSalesAmt(0.0);
            evaluationD.setSalesQuantity(0.0);
            evaluationD.setGross(0.0);
            evaluationD.setSalesStoreQty(0);
        } else {
            evaluationD.setSalesAmt(new BigDecimal("30").multiply(new BigDecimal(waitGood.getSalesAmt().toString())).divide(new BigDecimal(param),2, BigDecimal.ROUND_HALF_UP).doubleValue());
            evaluationD.setSalesQuantity(new BigDecimal("30").multiply(new BigDecimal(waitGood.getSalesQuantity().toString())).divide(new BigDecimal(param),2, BigDecimal.ROUND_HALF_UP).doubleValue());
            evaluationD.setGross(new BigDecimal("30").multiply(new BigDecimal(storeQty.getGross().toString())).divide(new BigDecimal(param),2, BigDecimal.ROUND_HALF_UP).doubleValue());
            evaluationD.setSalesStoreQty(storeQty.getSalesStoreQty());
        }
        evaluationD.setStoreQty(waitGood.getStoreQty());
        evaluationD.setInvStoreQty(ObjectUtils.isEmpty(storeQty.getInvStoreQty()) ? 0 : storeQty.getInvStoreQty());
        evaluationD.setDealStatus(0);
        evaluationD.setIsDelete(0);
        evaluationD.setCreateTime(now);
        evaluationD.setUpdateTime(now);
        evaluationD.setCreateUser("system");
        evaluationD.setUpdateUser("system");
        return evaluationD;
    }

    private GaiaEntNewProductEvaluationH setHead(Date now, String client, String billNo) {
        GaiaEntNewProductEvaluationH evaluationH = new GaiaEntNewProductEvaluationH();
        evaluationH.setClient(client);
        evaluationH.setBillCode(billNo);
        evaluationH.setBillDate(now);
        evaluationH.setStatus(0);
        evaluationH.setIsDelete(0);
        evaluationH.setCreateUser("system");
        evaluationH.setCreateTime(now);
        evaluationH.setUpdateTime(now);
        evaluationH.setUpdateUser("system");
        return evaluationH;
    }
}
