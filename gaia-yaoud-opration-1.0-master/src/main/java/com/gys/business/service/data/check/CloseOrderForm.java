package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 17:08
 **/
@Data
public class CloseOrderForm {
    @ApiModelProperty(value = "加盟商", example = "10000001", hidden = true)
    private String client;
    @ApiModelProperty(value = "用户ID", example = "1116", hidden = true)
    private String userId;
    @ApiModelProperty(value = "补货单号", example = "PD091232001")
    private String replenishCode;
    @ApiModelProperty(value = "门店编码", example = "10001")
    private String storeId;
}
