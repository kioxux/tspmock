package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/13
 */
@Data
public class GaiaSdMemberCardToData implements Serializable {
    private static final long serialVersionUID = -794213492108824931L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 会员ID
     */
    private String gsmbcMemberId;

    /**
     * 会员卡号
     */
    private String gsmbcCardId;

    /**
     * 所属店号
     */
    private String gsmbcBrId;

    /**
     * 渠道
     */
    private String gsmbcChannel;

    /**
     * 卡类型
     */
    private String gsmbcClassId;

    /**
     * 当前积分
     */
    private String gsmbcIntegral;

    /**
     * 最后积分日期
     */
    private String gsmbcIntegralLastdate;

    /**
     * 清零积分日期
     */
    private String gsmbcZeroDate;

    /**
     * 新卡创建日期
     */
    private String gsmbcCreateDate;

    /**
     * 类型
     */
    private String gsmbcType;

    /**
     * openID
     */
    private String gsmbcOpenId;

    /**
     * 卡状态
     */
    private String gsmbcStatus;

    /**
     * 开卡人员
     */
    private String gsmbcCreateSaler;

    /**
     * 消费金额
     */
    private String gsmcAmtSale;

    /**
     * 积分数值
     */
    private String gsmcIntegralSale;
}
