package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel
public class InventoryChangeSummaryInData {
    @ApiModelProperty(value = "开始日期")
    @NotBlank(message = "开始日期必输")
    private String startDate;
    @ApiModelProperty(value = "截至日期")
    @NotBlank(message = "结束日期必输")
    private String endDate;
    @ApiModelProperty(value = "发生地点")
    private String siteCode;
    @ApiModelProperty(value = "地点批量查询")
    private String[] siteArr;
    @ApiModelProperty(value = "商品分类查询")
    private String[][] classArr;
    private List<String> classArrs;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品编码批量查询")
    private String[] proArr;
    @ApiModelProperty(value = "单据类型")
    private String type;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "商品分类")
    private String proClass;
    @ApiModelProperty(value = "是否医保")
    private String medProdctStatus;

    private String client;
    private Integer pageNum;
    private Integer pageSize;
}
