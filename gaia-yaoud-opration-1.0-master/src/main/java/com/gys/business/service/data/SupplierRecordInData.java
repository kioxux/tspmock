package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/9/10
 */
@Data
public class SupplierRecordInData implements Serializable {
    private static final long serialVersionUID = -4040126902905515879L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "收货单号")
    private String gsahVoucherId;

    @ApiModelProperty(value = "商品编码")
    private String gsadProId;

    @ApiModelProperty(value = "批号")
    private String gsadBatchNo;

    @ApiModelProperty(value = "供应商名称")
    private String supName;

    @NotBlank(message = "起始日期不能为空")
    @ApiModelProperty(value = "起始日期")
    private String startDate;

    @NotBlank(message = "结束日期不能为空")
    @ApiModelProperty(value = "结束日期")
    private String endDate;
}
