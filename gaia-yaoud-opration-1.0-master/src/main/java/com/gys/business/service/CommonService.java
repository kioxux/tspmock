package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.data.GetFileOutData;
import com.gys.business.service.data.upload.UpLoadDto;
import com.gys.common.enums.SerialCodeTypeEnum;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface CommonService {
    GetFileOutData fileUpload(MultipartFile file);

    GetFileOutData getFileUrl(GetFileOutData file);

    GetFileOutData fileUploadLocal(File file);

    /**
     * 获取加盟商维度流水号
     * @param client 加盟商
     * @param serialCodeTypeEnum 流水号类型
     * @return 流水号
     */
    String getSerialCode(String client, SerialCodeTypeEnum serialCodeTypeEnum);

    /**
     * 获取门店维度流水号
     * @param client  加盟商
     * @param stoCode 门店编码
     * @param serialCodeTypeEnum 流水号类型
     * @return 流水号
     */
    String getSdSerialCode(String client, String stoCode, SerialCodeTypeEnum serialCodeTypeEnum);

    String selectNextVoucherId(GaiaSdReplenishH inData);

    String selectNextCancelId(String clientId);

    GetFileOutData fileUpload(UpLoadDto upLoadDto);
}
