package com.gys.business.service;

import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.mapper.entity.WmsRkys;
import com.gys.business.service.data.DistributionPlanForm;
import com.gys.business.service.data.DistributionPlanVO;
import com.gys.business.service.data.PreReplenishVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 13:05
 */
public interface NewDistributionPlanService {

    NewDistributionPlan getById(Long id);

    NewDistributionPlan getUnique(NewDistributionPlan cond);

    List<NewDistributionPlan> findList(NewDistributionPlan cond);

    NewDistributionPlan add(NewDistributionPlan newDistributionPlan);

    NewDistributionPlan update(NewDistributionPlan newDistributionPlan);

    List<DistributionPlanVO> findPage(DistributionPlanForm distributionPlanForm);

    void confirmReplenish(NewDistributionPlan distributionPlan);

    List<PreReplenishVO> preReplenish(NewDistributionPlan distributionPlan);

    void execCheckDistribution();

    void confirmCall(NewDistributionPlan distributionPlan);

    void execPlanExpireJob();

    List<WmsRkys> getWmsRkysList(NewDistributionPlan distributionPlan);

    BigDecimal getWmsRkysQuantity(NewDistributionPlan distributionPlan);

    boolean validStock(NewDistributionPlan distributionPlan);
}
