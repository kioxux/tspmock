package com.gys.business.service.data;


import lombok.Data;

import java.io.Serializable;
@Data
public class PromHyrDiscountInData implements Serializable {
    private static final long serialVersionUID = -7661244066888965306L;
    private String clientId;
    private String gsppVoucherId;
    private String gsppSerial;
    private String gsppBeginDate;
    private String gsppEndDate;
    private String gsppBeginTime;
    private String gsppEndTime;
    private String gsppDateFrequency;
    private String gsppTimeFrequency;
    private String gsppRebate;
    private String gsppInteFlag;
    private String gsppInteRate;
}
