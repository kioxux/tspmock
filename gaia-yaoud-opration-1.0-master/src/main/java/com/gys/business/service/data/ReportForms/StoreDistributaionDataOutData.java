package com.gys.business.service.data.ReportForms;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel
@CsvRow
public class StoreDistributaionDataOutData {

       @ApiModelProperty(value = "客户类型")
       @CsvCell(title = "客户类型",index = 1,fieldNo = 1)
       private String cusType;

       @ApiModelProperty(value = "客户编码")
       @CsvCell(title = "客户编码", index = 2, fieldNo = 1)
       private String cusCode;

       @ApiModelProperty(value = "客户名称")
       @CsvCell(title = "客户名称",index = 3,fieldNo = 1)
       private String cusName;

       @ApiModelProperty(value = "出库调拨额")
       @CsvCell(title = "出库调拨额",index = 4,fieldNo = 1)
       private BigDecimal outStockDBE;

       @ApiModelProperty(value = "退库调拨额")
       @CsvCell(title = "退库调拨额",index = 5,fieldNo = 1)
       private BigDecimal refoundDBE;

       @ApiModelProperty(value = "调拨额合计")
       @CsvCell(title = "调拨额合计",index = 6,fieldNo = 1)
       private BigDecimal dbeRightTotal;

       @ApiModelProperty(value = "出库成本额")
       @CsvCell(title = "出库成本额",index = 7,fieldNo = 1)
       private BigDecimal outStockMOV;

       @ApiModelProperty(value = "退库成本额")
       @CsvCell(title = "退库成本额",index = 8,fieldNo = 1)
       private BigDecimal refoundMOV;

       @ApiModelProperty(value = "成本额合计")
       @CsvCell(title = "成本额合计",index = 9,fieldNo = 1)
       private BigDecimal movRightTotal;

       @ApiModelProperty(value = "出库毛利额")
       @CsvCell(title = "成本额合计",index = 10,fieldNo = 1)
       private BigDecimal outStockGrossAmt;

       @ApiModelProperty(value = "退库毛利额")
       @CsvCell(title = "退库毛利额",index = 11,fieldNo = 1)
       private BigDecimal refoundGrossAmt;

       @ApiModelProperty(value = "毛利额合计")
       @CsvCell(title = "毛利额合计",index = 12,fieldNo = 1)
       private BigDecimal grossAmtRightTotal;

       @ApiModelProperty(value = "出库毛利率")
       @CsvCell(title = "出库毛利率",index = 13,fieldNo = 1)
       private String outStockGrossRate;

       @ApiModelProperty(value = "退库毛利率")
       @CsvCell(title = "退库毛利率",index = 14,fieldNo = 1)
       private String refoundGrossRate;

       @ApiModelProperty(value = "毛利率合计")
       @CsvCell(title = "毛利率合计",index = 15,fieldNo = 1)
       private String grossRateRightTotal;


}
