package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.DtYbDataInterfaceService;
import com.gys.business.service.data.DtYb.Inventory.InventoryVO;
import com.gys.business.service.data.DtYb.Physical.PhysicalVO;
import com.gys.business.service.data.DtYb.ProfitLoss.ProfitlossVO;
import com.gys.business.service.data.DtYb.ReturnExamime.ReturnExamimeVO;
import com.gys.business.service.data.DtYb.Rkmxlb;
import com.gys.business.service.data.DtYb.RkmxlbEntity;
import com.gys.business.service.data.DtYb.RkmxlbParam;
import com.gys.business.service.data.YbExamineOutData;
import com.gys.business.service.data.YbInterfaceQueryData;
import com.gys.common.YiLianDaModel.GYSXXGL.Gysxxlb;
import com.gys.common.YiLianDaModel.YPXXGL.Ypxxlb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DtYbDataInterfaceServiceImpl implements DtYbDataInterfaceService {
    @Autowired
    private GaiaSdExamineDMapper gaiaSdExamineDMapper;
    @Autowired
    private GaiaYbStockBatchMapper gaiaYbStockBatchMapper;
    @Autowired
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Autowired
    private GaiaYbYdMatchMapper gaiaYbYdMatchMapper;
    @Autowired
    private MedicalInsuranceMapper medicalInsuranceMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private GaiaWmsTkysMapper gaiaWmsTkysMapper;
    @Autowired
    private GaiaWmsTuigongMMapper gaiaWmsTuigongMMapper;
    @Autowired
    private GaiaSdIncomeStatementHMapper gaiaSdIncomeStatementHMapper;
    @Override
    public List<RkmxlbParam> queryExamime(YbInterfaceQueryData inData) {
        List<RkmxlbParam> result = new ArrayList<>();
        List<YbExamineOutData> resList = gaiaSdExamineDMapper.queryExamime(inData);
        if(CollectionUtil.isNotEmpty(resList)){
            for(YbExamineOutData item : resList){
                for(RkmxlbParam item2 : result){
                    if(item2.getYsdjh0().equals(item.getGSEH_VOUCHER_ID())){
                        Rkmxlb temp2 = new Rkmxlb();
                        temp2.setBzmc00(item.getPRO_COMMONNAME());
                        temp2.setDwmc00(item.getPRO_UNIT());
                        temp2.setYpbh00(item.getGSED_PRO_ID());
                        temp2.setYpmc00(item.getPRO_NAME());
                        temp2.setXh0000(item.getPRO_FORM());
                        temp2.setRkpch0(item.getGSED_BATCH());
                        temp2.setScrq00(item.getPRODUCTION_DATE());
                        temp2.setYpgg00(item.getPRO_SPECS());
                        temp2.setYxrq00(item.getGSED_VALID_DATE());
                        temp2.setYpjj00(item.getPO_PRICE());
                        temp2.setYpcjmc(item.getPRO_FACTORY_NAME());
                        temp2.setHgsl00(item.getGSED_QUALIFIED_QTY());
                        temp2.setYpph00(item.getGSED_BATCH_NO());

                        item2.getRkmxlb().add(temp2);
                        break;
                    }
                }
                RkmxlbParam temp= new RkmxlbParam();
                temp.setGysbh0(item.getGSEH_FROM());
                temp.setGysmc0(item.getSUP_NAME());
                temp.setRkysrq(item.getGSEH_DATE());
                temp.setYsdjh0(item.getGSEH_VOUCHER_ID());
                temp.setRkmxlb(new ArrayList<>());

                Rkmxlb temp2 = new Rkmxlb();
                temp2.setBzmc00(item.getPRO_COMMONNAME());
                temp2.setDwmc00(item.getPRO_UNIT());
                temp2.setYpbh00(item.getGSED_PRO_ID());
                temp2.setYpmc00(item.getPRO_NAME());
                temp2.setXh0000(item.getPRO_FORM());
                temp2.setRkpch0(item.getGSED_BATCH());
                temp2.setScrq00(item.getPRODUCTION_DATE());
                temp2.setYpgg00(item.getPRO_SPECS());
                temp2.setYxrq00(item.getGSED_VALID_DATE());
                temp2.setYpjj00(item.getPO_PRICE());
                temp2.setYpcjmc(item.getPRO_FACTORY_NAME());
                temp2.setHgsl00(item.getGSED_QUALIFIED_QTY());
                temp2.setYpph00(item.getGSED_BATCH_NO());

                temp.getRkmxlb().add(temp2);
                result.add(temp);
            }
        }
        return result;
    }

    @Transactional
    @Override
    public int insertExamime(String client, String brId, RkmxlbEntity inData) {
        List<GaiaYbStockBatch> param = new ArrayList<>();
        inData.getRkmxlb().forEach(item->{
            GaiaYbStockBatch temp = new GaiaYbStockBatch();
            temp.setBatchNo(item.getYpph00());
            temp.setBatch(item.getRkpch0());
            temp.setClient(client);
            temp.setBrId(brId);
            temp.setProCode(item.getYpbh00());
            temp.setStockCode(item.getStockcode());
            temp.setYbStockBatch(item.getYpbh00());
            param.add(temp);
        });
        gaiaYbStockBatchMapper.insertBatch(param);

        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(currentTime);

        GaiaYbYdMatch data = new GaiaYbYdMatch();
        data.setClient(client);
        data.setBrId(brId);
        data.setVoucherId(inData.getYsdjh0());
        data.setBatch("");
        data.setBatchNo("");
        data.setDate(dateString);
        data.setProId("");
        data.setType("YS");
        data.setBatch("");
        data.setSerial("");
        gaiaYbYdMatchMapper.insert(data);

        return param.size();
    }

    @Override
    public List<Ypxxlb> selectPushProduct(String client, String brId,String startDate,String endDate) {
        return medicalInsuranceMapper.selectPushProduct(client,brId,startDate,endDate);
    }

    @Override
    public int insertPushProduct(String client, String brId,List<Ypxxlb> inData){
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(currentTime);
        List<GaiaYbYdMatch> dataList = new ArrayList<>();
        inData.forEach(item -> {
            GaiaYbYdMatch data = new GaiaYbYdMatch();
            data.setClient(client);
            data.setBrId(brId);
            data.setVoucherId("");
            data.setBatch("");
            data.setBatchNo("");
            data.setDate(dateString);
            data.setProId(item.getYpbh00());
            data.setType("SP");
            data.setBatch("");
            data.setSerial("");
            dataList.add(data);
        });
        return gaiaYbYdMatchMapper.insertBatch(dataList);
    }

    @Override
    public List<Gysxxlb> selectPushSupplier(String client, String brId,String startDate,String endDate){
        return medicalInsuranceMapper.selectPushSupplier(client,brId,startDate,endDate);
    }

    @Override
    public int insertPushSupplier(String client, String brId,List<Gysxxlb> inData){
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(currentTime);
        List<GaiaYbYdMatch> dataList = new ArrayList<>();

        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client",client).andEqualTo("stoCode",brId);
        GaiaStoreData store = gaiaStoreDataMapper.selectOneByExample(example);

        if(ObjectUtil.isNotEmpty(store)&&"1".equals(store.getStoAttribute())){
            inData.forEach(item -> {
                GaiaYbYdMatch data = new GaiaYbYdMatch();
                data.setClient(client);
                data.setBrId(brId);
                data.setVoucherId(item.getGysbh0());
                data.setBatch("");
                data.setBatchNo("");
                data.setDate(dateString);
                data.setProId("");
                data.setType("SUP");
                data.setBatch("");
                data.setSerial("");
                dataList.add(data);
            });
        }else if(ObjectUtil.isNotEmpty(store)&&"2".equals(store.getStoAttribute())){
            inData.forEach(item -> {
                GaiaYbYdMatch data = new GaiaYbYdMatch();
                data.setClient(client);
                data.setBrId(store.getStoDcCode());
                data.setVoucherId(item.getGysbh0());
                data.setBatch("");
                data.setBatchNo("");
                data.setDate(dateString);
                data.setProId("");
                data.setType("SUP");
                data.setBatch("");
                data.setSerial("");
                dataList.add(data);
            });
        }

        return gaiaYbYdMatchMapper.insertBatch(dataList);
    }

    @Override
    public List<ReturnExamimeVO> queryReturnExamime(YbInterfaceQueryData inData) {
        List<ReturnExamimeVO> outData = new ArrayList<>();
        List<ReturnExamimeVO> tkys = gaiaWmsTkysMapper.queryTkysByYLZ(inData);
        outData.addAll(tkys);
        List<ReturnExamimeVO> tuiGong = gaiaWmsTuigongMMapper.queryTuiGongByYLZ(inData);
        outData.addAll(tuiGong);

        return outData;
    }

    @Override
    public List<PhysicalVO> queryPhysical(YbInterfaceQueryData inData) {
        List<PhysicalVO> outData = gaiaSdIncomeStatementHMapper.queryPhysical(inData);
        return outData;
    }

    @Override
    public List<ProfitlossVO> queryProfitloss(YbInterfaceQueryData inData) {
        List<ProfitlossVO> outData = gaiaSdIncomeStatementHMapper.queryProfitloss(inData);
        return outData;

    }

    @Override
    public List<InventoryVO> queryInventory(YbInterfaceQueryData inData) {
        List<InventoryVO> outData = gaiaSdStockBatchMapper.queryInventory(inData);
        return outData;
    }
}
