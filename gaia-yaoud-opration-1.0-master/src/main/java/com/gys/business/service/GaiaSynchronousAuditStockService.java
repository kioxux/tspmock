package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSynchronousAuditStock;
import com.gys.business.service.data.GaiaSynchronousAuditStockOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

/**
 * 库存审计(GaiaSynchronousAuditStock)表服务接口
 *
 * @author makejava
 * @since 2021-07-01 13:43:14
 */
public interface GaiaSynchronousAuditStockService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaSynchronousAuditStock queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaSynchronousAuditStock> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 实例对象
     */
    GaiaSynchronousAuditStock insert(GaiaSynchronousAuditStock gaiaSynchronousAuditStock);

    /**
     * 修改数据
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 实例对象
     */
    GaiaSynchronousAuditStock update(GaiaSynchronousAuditStock gaiaSynchronousAuditStock);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

    Object insertStockInfo(GaiaSynchronousAuditStock inData);

    Object listAuditInfo(GaiaSynchronousAuditStockOutData inData);

    GaiaSynchronousAuditStock auditQuery(Map<String, String> map, GetLoginOutData userInfo);
}
