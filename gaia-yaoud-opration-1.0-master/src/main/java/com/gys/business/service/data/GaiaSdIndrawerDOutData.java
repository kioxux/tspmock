//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdIndrawerD;

public class GaiaSdIndrawerDOutData extends GaiaSdIndrawerD {
    private String gsidProName;
    private String normalPrice;
    private String proForm;
    private String proSpecs;
    private String proPlace;
    private String proUnit;
    private String proFactoryName;
    private String gsadStockStatus;
    private String proRegisterNo;

    public GaiaSdIndrawerDOutData() {
    }

    public String getGsidProName() {
        return this.gsidProName;
    }

    public String getNormalPrice() {
        return this.normalPrice;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getGsadStockStatus() {
        return this.gsadStockStatus;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public void setGsidProName(final String gsidProName) {
        this.gsidProName = gsidProName;
    }

    public void setNormalPrice(final String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setGsadStockStatus(final String gsadStockStatus) {
        this.gsadStockStatus = gsadStockStatus;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdIndrawerDOutData)) {
            return false;
        } else {
            GaiaSdIndrawerDOutData other = (GaiaSdIndrawerDOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$gsidProName = this.getGsidProName();
                    Object other$gsidProName = other.getGsidProName();
                    if (this$gsidProName == null) {
                        if (other$gsidProName == null) {
                            break label119;
                        }
                    } else if (this$gsidProName.equals(other$gsidProName)) {
                        break label119;
                    }

                    return false;
                }

                Object this$normalPrice = this.getNormalPrice();
                Object other$normalPrice = other.getNormalPrice();
                if (this$normalPrice == null) {
                    if (other$normalPrice != null) {
                        return false;
                    }
                } else if (!this$normalPrice.equals(other$normalPrice)) {
                    return false;
                }

                label105: {
                    Object this$proForm = this.getProForm();
                    Object other$proForm = other.getProForm();
                    if (this$proForm == null) {
                        if (other$proForm == null) {
                            break label105;
                        }
                    } else if (this$proForm.equals(other$proForm)) {
                        break label105;
                    }

                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                label91: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label91;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label91;
                    }

                    return false;
                }

                Object this$proUnit = this.getProUnit();
                Object other$proUnit = other.getProUnit();
                if (this$proUnit == null) {
                    if (other$proUnit != null) {
                        return false;
                    }
                } else if (!this$proUnit.equals(other$proUnit)) {
                    return false;
                }

                label77: {
                    Object this$proFactoryName = this.getProFactoryName();
                    Object other$proFactoryName = other.getProFactoryName();
                    if (this$proFactoryName == null) {
                        if (other$proFactoryName == null) {
                            break label77;
                        }
                    } else if (this$proFactoryName.equals(other$proFactoryName)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$gsadStockStatus = this.getGsadStockStatus();
                    Object other$gsadStockStatus = other.getGsadStockStatus();
                    if (this$gsadStockStatus == null) {
                        if (other$gsadStockStatus == null) {
                            break label70;
                        }
                    } else if (this$gsadStockStatus.equals(other$gsadStockStatus)) {
                        break label70;
                    }

                    return false;
                }

                Object this$proRegisterNo = this.getProRegisterNo();
                Object other$proRegisterNo = other.getProRegisterNo();
                if (this$proRegisterNo == null) {
                    if (other$proRegisterNo != null) {
                        return false;
                    }
                } else if (!this$proRegisterNo.equals(other$proRegisterNo)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdIndrawerDOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsidProName = this.getGsidProName();
        result = result * 59 + ($gsidProName == null ? 43 : $gsidProName.hashCode());
        Object $normalPrice = this.getNormalPrice();
        result = result * 59 + ($normalPrice == null ? 43 : $normalPrice.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $gsadStockStatus = this.getGsadStockStatus();
        result = result * 59 + ($gsadStockStatus == null ? 43 : $gsadStockStatus.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdIndrawerDOutData(gsidProName=" + this.getGsidProName() + ", normalPrice=" + this.getNormalPrice() + ", proForm=" + this.getProForm() + ", proSpecs=" + this.getProSpecs() + ", proPlace=" + this.getProPlace() + ", proUnit=" + this.getProUnit() + ", proFactoryName=" + this.getProFactoryName() + ", gsadStockStatus=" + this.getGsadStockStatus() + ", proRegisterNo=" + this.getProRegisterNo() + ")";
    }
}
