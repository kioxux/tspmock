package com.gys.business.service.data.DelegationReturn;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
public class ExcelReturnApprovalData  extends BaseRowModel {
    @ExcelProperty(value = "门店", index = 0)
    private String stoCode;
    @ExcelProperty(value = "店名", index = 1)
    private String stoName;
    @ExcelProperty(value = "商品编码", index = 2)
    private String proId;
    @ExcelProperty(value = "商品名称", index = 3)
    private String proCommonname;
    @ExcelProperty(value = "规格", index = 4)
    private String proSpecs;
    @ExcelProperty(value = "厂家", index = 5)
    private String factoryName;
    @ExcelProperty(value = "单位", index = 6)
    private String proUnit;
    @ApiModelProperty(value = "退货数量")
    private Integer retdepQty;
    @ApiModelProperty(value = "修正数量")
    private Integer reviseQty;
    @ExcelProperty(value = "退货申请单", index = 8)
    private String voucherId;
    @ExcelProperty(value = "行号", index = 9)
    private String serial;
    @ExcelProperty(value = "退货批号", index = 10)
    private String batchNO;
    @ExcelProperty(value = "退货供应商编码", index = 11)
    private String supplierCode;
    @ExcelProperty(value = "退货供应商名称", index = 12)
    private String supplierName;
    @ExcelProperty(value = "采购进价", index = 13)
    private String poPrice;
    @ExcelProperty(value = "有效期", index = 14)
    private String expiryDate;
    @ExcelProperty(value = "退货原因", index = 15)
    private String retdepCause;
    @ExcelProperty(value = "门店库存", index = 16)
    private String qty;
    @ExcelProperty(value = "审核意见", index = 17)
    private String status;
}
