package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaProductSpecialOutData implements Serializable {

    /**
     * 商品编码  药德通用编码
     */
    @ApiModelProperty(value="商品编码  药德通用编码")
    private String proCode;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品描述")
    private String proDesc;

    /**
     * 商品规格
     */
    @ApiModelProperty(value="商品规格")
    private String proSpecs;

    /**
     * 商品生产厂家
     */
    @ApiModelProperty(value="商品生产厂家")
    private String proFactoryName;

    /**
     * 单位
     */
    @ApiModelProperty(value="单位")
    private String proUnit;

    /**
     * 成分分类
     */
    @ApiModelProperty(value="成分分类")
    private String proCompClass;

    /**
     * 成分分类描述
     */
    @ApiModelProperty(value="成分分类描述")
    private String proCompClassName;

    /**
     * 商品分类
     */
    @ApiModelProperty(value="商品分类")
    private String proClass;

    /**
     * 商品分类描述
     */
    @ApiModelProperty(value="商品分类描述")
    private String proClassName;

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private String proProv;

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private String proCity;

    /**
     * 类型  1-季节，2-品牌
     */
    @ApiModelProperty(value="类型  1-季节，2-品牌")
    private String proType;

    /**
     * 省中文名
     */
    @ApiModelProperty(value="省中文名")
    private String proProvName;

    /**
     * 市中文名
     */
    @ApiModelProperty(value="市中文名")
    private String proCityName;

    /**
     * 类型中文名  1-季节，2-品牌
     */
    @ApiModelProperty(value="类型中文名  1-季节，2-品牌")
    private String proTypeName;

    /**
     * 标识 1-春，2-夏，3-秋，4-冬
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬")
    private String flag;

    /**
     * 状态 0-正常，1-停用
     */
    @ApiModelProperty(value="状态 0-正常，1-停用")
    private String proStatus;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    private String createDate;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createUser;

    /**
     * 最后更新日期
     */
    @ApiModelProperty(value="最后更新日期")
    private String lastUpdateDate;

    /**
     * 最后更新人
     */
    @ApiModelProperty(value="最后更新人")
    private String lastUpdateUser;
}
