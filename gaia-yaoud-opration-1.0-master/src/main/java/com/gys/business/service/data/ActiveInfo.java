package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan
 */
@Data
public class ActiveInfo implements Serializable {
    private static final long serialVersionUID = 4596782160894905375L;
    private String gsphVoucherId;
    private String gsphType;
    private String gsphTypeId;
    private String proCode;
    private String proName;
    private String normalPrice;
    private String serial;
    private String priceOne;
    private String resultDisOne;
    private String disOne;
    private String resultSubOne;
    private String disTwo;
    private String resultDisTwo;
    private String priceTwo;
    private String resultSubTwo;
    private String resultDisThree;
    private String disThree;
    private String priceThree;
    private String resultSubThree;
    private String unionMoney;
    private String unionDis;
    private String memberFlag;
    private String disPrice;
    private String dis;
    private List<GetSalesReceiptsTableOutData> numAmtDataList;
}
