package com.gys.business.service.data.CommenData;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description 通用的入参类
 * @Author huxinxin
 * @Date 2021/4/19 16:40
 * @Version 1.0.0
 **/
@Data
@ApiModel
public class InData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店")
    private String stoCode;
    @ApiModelProperty(value = "门店数组")
    private String[] stoArr;
    @ApiModelProperty(value = "仓库")
    private String dcCode;
    @ApiModelProperty(value = "地点(包括门店和仓库)")
    private String siteCode;
    @ApiModelProperty(value = "地点(包括门店和仓库)")
    private List<String> siteArr;
    //自定义用途 注释自己写到代码里
    @ApiModelProperty(value = "类型")
    private String type;
    @ApiModelProperty(value = "起始时间")
    private String startDate;
    @ApiModelProperty(value = "结束时间")
    private String endDate;
    /**
     * 销售单号
     */
    private String billNo;
    /**
     * 医保结算Id
     */
    private String setlId;
    /**
     * 医保交易流水号
     */
    private String msgId;



    private String insutype;//险种类别
    private String clrOptins;// 清算地点
    private String clrType;//清算类别
    private int pageSize;
    private int pageNum;
}
