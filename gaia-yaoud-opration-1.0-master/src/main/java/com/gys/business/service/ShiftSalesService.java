//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.ShiftSalesOutData;
import com.gys.business.service.data.shiftSalesInData;
import java.util.List;

public interface ShiftSalesService {
    List<ShiftSalesOutData> getShiftSalesList(shiftSalesInData inData);
}
