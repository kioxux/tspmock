package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/14 19:27
 **/
@Data
public class HeadInfo {
    @ApiModelProperty(value = "补货单号", example = "PD202100098")
    private String replenishCode;
    @ApiModelProperty(value = "要货日期", example = "2021-09-01")
    private String requestDate;
    @ApiModelProperty(value = "补货门店", example = "10004")
    private String storeName;
    @ApiModelProperty(value = "补货类型：0-1-2", example = "0")
    private String replenishType;
    @ApiModelProperty(value = "审核状态：0-未审核、1-已审核", example = "1")
    private String status;
    @ApiModelProperty(value = "审核日期", example = "2021-07-01")
    private String reviewDate;
    @ApiModelProperty(value = "审核人", example = "1002-张翠莎")
    private String reviewUser;
    @ApiModelProperty(value = "备注", example = "已经电话沟通，急！")
    private String remarks;
}
