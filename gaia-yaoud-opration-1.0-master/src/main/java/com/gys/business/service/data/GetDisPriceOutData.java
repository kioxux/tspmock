
package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

@Data
public class GetDisPriceOutData implements Serializable {
    private static final long serialVersionUID = -6027967957464401690L;
    String money;
    /**
     * 互斥方式
     */
    String gsphExclusion;
    Set<String> proIds;
    String allPro;

    /**
     * 加盟商
     */
    private String clientId;
    /**
     * 单号
     */
    private String gspusVoucherId;
    /**
     * 促销类型
     */
    private String gspusVoucherType;
    /**
     * 行号
     */
    private String gspusSerial;
    /**
     * 商品编码
     */
    private String gspusProId;
    /**
     * 系列编码
     */
    private String gspusSeriesId;
    /**
     * 效期天数
     */
    private String gspusVaild;
    /**
     * 达到数量1
     */
    private String gspusQty1;
    /**
     * 达到金额1
     */
    private String gspusAmt1;
    /**
     * 生成促销减额1
     */
    private BigDecimal gspusSub1;
    /**
     * 生成促销价1
     */
    private BigDecimal gspusPrc1;
    /**
     * 生成促销折扣1
     */
    private String gspusRebate1;
    /**
     * 达到数量2
     */
    private String gspusQty2;
    /**
     * 达到金额2
     */
    private String gspusAmt2;
    /**
     * 生成促销减额2
     */
    private BigDecimal gspusSub2;
    /**
     * 生成促销价2
     */
    private BigDecimal gspusPrc2;
    /**
     * 生成促销折扣2
     */
    private String gspusRebate2;
    /**
     * 达到数量3
     */
    private String gspusQty3;
    /**
     * 达到金额3
     */
    private String gspusAmt3;
    /**
     * 生成促销减额3
     */
    private BigDecimal gspusSub3;
    /**
     * 生成促销价3
     */
    private BigDecimal gspusPrc3;
    /**
     * 生成促销折扣3
     */
    private String gspusRebate3;
    /**
     * 是否会员
     */
    private String gspusMemFlag;
    /**
     * 是否积分
     */
    private String gspusInteFlag;
    /**
     * 积分倍率
     */
    private String gspusInteRate;
    /**
     * (促销主表)阶梯
     */
    private String gsphPart;
    /**
     * (促销主表)参数1
     */
    private String gsphPara1;
    /**
     * (促销主表)参数2
     */
    private String gsphPara2;
    /**
     * (促销主表)参数3
     */
    private String gsphPara3;
    /**
     * (促销主表)参数4
     */
    private String gsphPara4;

}
