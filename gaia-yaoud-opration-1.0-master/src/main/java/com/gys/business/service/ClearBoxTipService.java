//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.AddBoxInfoOutData;
import com.gys.business.service.data.ClearBoxTipOutData;
import com.gys.business.service.data.GetAddBoxInData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface ClearBoxTipService {
    List<ClearBoxTipOutData> selectList(GetLoginOutData userInfo);

    AddBoxInfoOutData getAddBoxInfo(GetAddBoxInData inData);
}
