package com.gys.business.service.data;

import lombok.Data;

@Data
public class BatchControlItemData {
    private String proId;
    private String batchNo;
}
