package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class StoreStockReportInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String brId;
}
