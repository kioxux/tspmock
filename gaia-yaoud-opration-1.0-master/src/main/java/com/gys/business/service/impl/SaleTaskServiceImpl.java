package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.GaiaSaletaskDplanMapper;
import com.gys.business.mapper.GaiaSaletaskHplanMapper;
import com.gys.business.mapper.entity.GaiaSaletaskDplan;
import com.gys.business.mapper.entity.GaiaSaletaskHplan;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.SaleTaskService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import java.beans.Transient;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SaleTaskServiceImpl implements SaleTaskService {
    @Autowired
    private GaiaSaletaskHplanMapper hplanMapper;
    @Autowired
    private GaiaSaletaskDplanMapper dplanMapper;

    @Override
    @Transactional
    public SaleTaskHplan insertH(GetLoginOutData userInfo, SaleTaskInData saletasks) {
        Example example = new Example(GaiaSaletaskHplan.class);
        example.createCriteria().andEqualTo("client", saletasks.getClient()).andEqualTo("monthPlan", saletasks.getMonthPlan());
        int count = hplanMapper.selectCountByExample(example);
        if(count > 0){
            throw new BusinessException("月份:"+saletasks.getMonthPlan()+"已存在！");
        }
        GaiaSaletaskHplan hplan = new GaiaSaletaskHplan();
        hplan.setClient(userInfo.getClient());
        hplan.setMonthPlan(saletasks.getMonthPlan());
        hplan.setDaysPlan(saletasks.getDaysPlan());
        hplan.setCreId(userInfo.getUserId());
        hplan.setCreDate(DateUtil.format(new Date(), "yyyyMMddHHmmss"));
        hplan.setSplanSta("S");
        hplan.setLastUpdateTime(new Date());
        hplanMapper.insert(hplan);
        SaleTaskHplan sth =new SaleTaskHplan();
        sth.setMonthPlan(hplan.getMonthPlan());
        sth.setDaysPlan(String.valueOf(hplan.getDaysPlan()));
        sth.setSplanSta(  hplan.getSplanSta());
        return sth;


    }

    @Override
    @Transactional
    public void insertD(GetLoginOutData userInfo, SaleTaskInData saletasks,String type) {

        GaiaSaletaskHplan hplan = new GaiaSaletaskHplan();
        List<GaiaSaletaskDplan> dplansInsert = new ArrayList<>();
        List<GaiaSaletaskDplan> dplansUpdate = new ArrayList<>();
        List<String> dplansDelect = new ArrayList<>();
        hplan.setClient(userInfo.getClient());
        hplan.setMonthPlan(saletasks.getMonthPlan());
        hplan.setDaysPlan(CommonUtil.dayNum());
//        hplan.settCount(saletasks.getSaletasks().size());

//        BigDecimal thSaleAmt = BigDecimal.ZERO;
//        BigDecimal thSaleGRross = BigDecimal.ZERO;
//        BigDecimal thMcardQty = BigDecimal.ZERO;
//        Example exampleDel = new Example(GaiaSaletaskDplan.class);
//        exampleDel.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("monthPlan", saletasks.getMonthPlan());
//        dplanMapper.deleteByExample(exampleDel);
        for (Saletask saletaskDplan : saletasks.getSaletasks()){
            Example example = new Example(GaiaSaletaskDplan.class);
            example.createCriteria().andEqualTo("client", saletasks.getClient()).andEqualTo("monthPlan", saletasks.getMonthPlan()).andEqualTo("stoCode" , saletaskDplan.getStoCode());
            GaiaSaletaskDplan dplan = dplanMapper.selectOneByExample(example);
            //如果不是导入的 要判断表格有没有删除之前的行
            if (type == null ){
                dplansDelect.add(saletaskDplan.getStoCode());
            }
            if(ObjectUtils.isEmpty(dplan)){
                dplan = new GaiaSaletaskDplan();
                dplan.setClient(userInfo.getClient());
                dplan.setMonthPlan(saletasks.getMonthPlan());
                dplan.setStoCode(saletaskDplan.getStoCode());
                dplan.settSaleAmt(saletaskDplan.getTSaleAmt());
                dplan.settSaleGross(saletaskDplan.getTSaleGross());
                dplan.settMcardQty(saletaskDplan.getTMcardQty());
                dplan.setaSaleAmt(saletaskDplan.getASaleAmt());
                dplan.setaSaleGross(saletaskDplan.getASaleGross());
                dplan.setaMcardQty(saletaskDplan.getAMcardQty());
                dplansInsert.add(dplan);
            }else {
                dplan.settSaleAmt(saletaskDplan.getTSaleAmt());
                dplan.settSaleGross(saletaskDplan.getTSaleGross());
                dplan.settMcardQty(saletaskDplan.getTMcardQty());
                dplan.setaSaleAmt(saletaskDplan.getASaleAmt());
                dplan.setaSaleGross(saletaskDplan.getASaleGross());
                dplan.setaMcardQty(saletaskDplan.getAMcardQty());
                dplansUpdate.add(dplan);
            }



//                throw new BusinessException("门店:"+saletaskDplan.getStoName()+"月份"+saletasks.getMonthPlan()+"已存在！");
        }

//
        Example delExample = new Example(GaiaSaletaskDplan.class);
        delExample.createCriteria().andEqualTo("client", saletasks.getClient()).andEqualTo("monthPlan", saletasks.getMonthPlan()).andNotIn("stoCode" , dplansDelect);
        dplanMapper.deleteByExample(delExample);

        if(ObjectUtil.isNotEmpty(dplansInsert)){
            dplanMapper.insertBatch(dplansInsert);
        }
        if(ObjectUtil.isNotEmpty(dplansUpdate)){
            dplanMapper.updateBatch(dplansUpdate);
        }
        if(ObjectUtil.isNotEmpty(dplansInsert) || ObjectUtil.isNotEmpty(dplansUpdate)){
            GaiaSaletaskDplan dplan = dplanMapper.selectBySum(saletasks);
            hplan.setLastUpdateTime(new Date());
            hplan.setThSaleAmt(dplan.gettSaleAmt());
            hplan.setThSaleGross(dplan.gettSaleGross());
            hplan.setThMcardQty(dplan.gettMcardQty());
            hplan.settCount(Integer.valueOf(dplan.getStoCode()));
            hplanMapper.updateByPrimaryKeySelective(hplan);

        }
    }

    @Override
    public SaleTaskInData selectById(SaleTaskInData saletask) {
        SaleTaskInData outData= new SaleTaskInData();
        outData.setMonthPlan(saletask.getMonthPlan());
        List<Saletask> saletaskData=dplanMapper.selectSaletaskById(saletask);
        outData.setSaletasks(saletaskData);
        return outData;
    }

    @Override
    public List<SaleTaskOutData> selectPage(SaleTaskInData saletask) {
      return hplanMapper.selectPage(saletask.getClient());
    }

    @Override
    @Transactional
    public void edit(GetLoginOutData userInfo,SaleTaskInData saletask) {
        Example example = new Example(GaiaSaletaskHplan.class);
        example.createCriteria().andEqualTo("client", saletask.getClient()).andEqualTo("monthPlan", saletask.getMonthPlan());
        hplanMapper.deleteByExample(example);
        dplanMapper.deleteByExample(example);
//        insert(userInfo,saletask);
    }

    @Override
    @Transactional
    public void delect(GetLoginOutData userInfo, SaleTaskInData saletask) {
        Example example = new Example(GaiaSaletaskHplan.class);
        example.createCriteria().andEqualTo("client", saletask.getClient()).andEqualTo("monthPlan", saletask.getMonthPlan());
        hplanMapper.deleteByExample(example);
        dplanMapper.deleteByExample(example);
    }

    @Override
    public List<SaleTaskStroeOutData> getStoreBySaleTask(GaiaStoreData storeData) {
        return hplanMapper.getStoreBySaleTask(storeData);
    }

    @Override
    @Transactional
    public void getImportExcelSaleTask(SaleTaskInData inData,GetLoginOutData userInfo) {
        insertD(userInfo,inData,"2");
    }

    @Override
    public PageInfo<Saletask> selectSaleTaskReachPage(Saletask saletask) {
        if (ObjectUtil.isNotNull(saletask.getPageNum()) || ObjectUtil.isNotNull(saletask.getPageSize())) {
            PageHelper.startPage(saletask.getPageNum(), saletask.getPageSize());
        }
        List<Saletask> outData = this.dplanMapper.selectSaleTaskReachPage(saletask);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    @Transactional
    public void update(GaiaSaletaskHplan saletask) {
        saletask.setSplanSta("P");
        int count = this.hplanMapper.updateByPrimaryKeySelective(saletask);
        if(count == 0){
            throw new BusinessException("审核失败!");
        }
    }
}
