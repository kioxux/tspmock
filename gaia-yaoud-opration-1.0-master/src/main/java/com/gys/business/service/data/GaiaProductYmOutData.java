package com.gys.business.service.data;

import lombok.Data;

@Data
public class GaiaProductYmOutData {

    private String client;
    private String proSite;
    private String proSelfCode;
    private String proCommonname;
    private String proName;
    private String proSpecs;
    private String proUnit;
    private String proFactoryName;
    private String proBarcode;
    private String drugId;
    private String drugName;
    private String spec;
    private String unit;
    private String approvalNumber;
    private String proCode;
    private String proRegisterClass;
    private String proRegisterNo;
}
