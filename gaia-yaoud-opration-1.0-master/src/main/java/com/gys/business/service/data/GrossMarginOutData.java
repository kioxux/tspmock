package com.gys.business.service.data;


import lombok.Data;

import java.math.BigDecimal;

/**
 * 毛利区间划分传参
 *
 * @author Zhangchi
 * @since 2021/09/16/9:30
 */
@Data
public class GrossMarginOutData {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 客户名称
     */
    private String customerName;
    /**
     * 毛利区间分类：ABCDE
     */
    private String intervalType;
    /**
     * 起始毛利率
     */
    private BigDecimal startValue;
    /**
     * 结束毛利率
     */
    private BigDecimal endValue;
    /**
     * 更新人
     */
    private String updateUser;
    /**
     * 更新时间
     */
    private String updateTime;

}
