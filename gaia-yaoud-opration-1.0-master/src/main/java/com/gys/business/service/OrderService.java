package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface OrderService {

    /**
     * 根绝不同type 查询不同 商品
     * @param userInfo
     * @param inData
     * @return
     */
    List<OrderOutData> getProductData(GetLoginOutData userInfo, OrderInData inData);

    /**
     * 获取支付类型方式
     * @param userInfo
     * @return
     */
    List<OrderPayOutData> getPayList(GetLoginOutData userInfo);

    /**
     * 订购记录查询
     * @param userInfo
     * @param inData
     * @return
     */
    List<OrderRecordOutData> getOrderRecordList(GetLoginOutData userInfo, OrderRecordInData inData);

    /**
     * 订购明细 和 退货查询
     * @param userInfo
     * @param inData
     * @return
     */
    OrderAll getOrderDetailsList(GetLoginOutData userInfo, OrderInData inData);

    /**
     * 总部门店
     * @param userInfo
     * @param inData
     * @return
     */
    List<OrderRecordOutData> headquartersRecords(GetLoginOutData userInfo, HeadquartersRecordsData inData);

    /**
     * 核销保存
     * @param userInfo
     * @param inData
     */
    void writeOffAndSave(GetLoginOutData userInfo, OrderSaveInData inData);

    /**
     * 保存
     * @param userInfo
     * @param inData
     */
    void save(GetLoginOutData userInfo, OrderSaveInData inData);

    /**
     * 退货
     * @param userInfo
     * @param inData
     */
    void returnOrder(GetLoginOutData userInfo, OrderSaveInData inData);
}
