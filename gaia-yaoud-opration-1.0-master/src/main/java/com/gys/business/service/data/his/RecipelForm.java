package com.gys.business.service.data.his;

import lombok.Data;

/**
 * @desc: 处方信息
 * @author: Ryan
 * @createTime: 2021/10/6 10:28
 */
@Data
public class RecipelForm {
    /**姓名*/
    private String name;
    /**身份证号*/
    private String cardNo;
    /**手机号*/
    private String mobile;
    /**性别*/
    private String gender;
    /**年龄*/
    private String age;
    /**会员ID*/
    private String memberId;
    /**会员卡号*/
    private String memberCardNo;
}
