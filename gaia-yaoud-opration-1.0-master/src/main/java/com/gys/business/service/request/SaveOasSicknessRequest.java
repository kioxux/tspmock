package com.gys.business.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SaveOasSicknessRequest {
    @ApiModelProperty(value = "创建人")
    private String userId;
    @ApiModelProperty(value = "创建人姓名")
    private String userName;
    @ApiModelProperty(value = "大类中类成分类型0大类 1 中类 2成分")
    private Integer type;
    @ApiModelProperty(value = "疾病编码")
    private String sicknessCoding;
    @ApiModelProperty(value = "疾病分类集合")
    private List<SaveOasSickness> oasSicknessList;

    @Data
    public static class SaveOasSickness {
        @ApiModelProperty(value = "疾病编码")
        private String sicknessCoding;

        @ApiModelProperty(value = "疾病名称")
        private String sicknessName;
    }
}
