package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/10/21
 */
@Data
public class GaiaAcceptID implements Serializable {
    private static final long serialVersionUID = -5209989910288542503L;

    /**
     * 采购订单号
     */
    private String gsahPoid;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 供应商名称
     */
    private String poSupplierName;
}
