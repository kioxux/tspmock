package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SaleChartOutData implements Serializable {

    @ApiModelProperty(value = "动销品项数-非药品")
    private Integer movNoDrug;

    @ApiModelProperty(value = "动销品项数-中药")
    private Integer movChineseDrug;

    @ApiModelProperty(value = "动销品项数-药品")
    private Integer movDrug;

    @ApiModelProperty(value = "动销品项数-处方药")
    private Integer movPrescription;

    @ApiModelProperty(value = "动销品项数-OTC")
    private Integer movOTC;

    @ApiModelProperty(value = "动销天数")
    private Integer movDays;

    @ApiModelProperty(value = "动销门店数")
    private Integer movStoreCount;

    @ApiModelProperty(value = "销售额占比-非药品")
    private BigDecimal saleAmtNoDrugProp;

    @ApiModelProperty(value = "销售额占比-中药")
    private BigDecimal saleAmtChineseDrugProp;

    @ApiModelProperty(value = "销售额占比-药品")
    private BigDecimal saleAmtDrugProp;

    @ApiModelProperty(value = "销售额占比-处方药")
    private BigDecimal saleAmtPrescriptionProp;

    @ApiModelProperty(value = "销售额占比-OTC")
    private BigDecimal saleAmtOTCProp;

    @ApiModelProperty(value = "毛利率-合计")
    private BigDecimal profitRateTotal;

    @ApiModelProperty(value = "毛利率-（药品-处方药）")
    private BigDecimal profitRatePrescription;

    @ApiModelProperty(value = "毛利率-OTC")
    private BigDecimal profitRateOTC;

    @ApiModelProperty(value = "毛利率-药品小计")
    private BigDecimal profitRateDrug;

    @ApiModelProperty(value = "毛利率-中药")
    private BigDecimal profitRateChineseDrug;

    @ApiModelProperty(value = "毛利率-非药品")
    private BigDecimal profitRateNoDrug;

}
