package com.gys.business.service;

import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.common.data.PageInfo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 门店调入建议-主表(GaiaStoreInSuggestionH)表服务接口
 *
 * @author makejava
 * @since 2021-10-28 10:53:20
 */
public interface GaiaStoreInSuggestionHService {

    PageInfo queryDetail(CallUpOrInStoreDetailDTO storeDetailDTO);

    void queryDetailExport(HttpServletResponse response ,CallUpOrInStoreDetailDTO storeDetailDTO);

    List<Map<String ,String>> queryBillCode(String startDate,String endDate);

}
