package com.gys.business.service.data.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/24 16:57
 **/
@Data
@ApiModel
public class DeviceForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String gsthBrId;
    @ApiModelProperty(value = "设备名称", example = "货架1")
    private String deviceName;
    @ApiModelProperty(value = "登记人员", example = "贾老板")
    private String userName;
    @ApiModelProperty(value = "开始启用日期", example = "20210629")
    private String startDate;
    @ApiModelProperty(value = "结束启用日期",example = "20210629")
    private String endDate;

    /**
     * 地点集合
     */
    private List<String> siteCodes;
}
