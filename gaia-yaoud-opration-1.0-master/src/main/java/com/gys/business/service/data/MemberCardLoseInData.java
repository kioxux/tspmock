package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MemberCardLoseInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "门店编号")
    private String brId;

    @ApiModelProperty(value = "会员编号")
    private String memberId;

    @ApiModelProperty(value = "原会员卡号")
    private String oldCardId;

    @ApiModelProperty(value = "新会员卡号")
    private String newCardId;

    @ApiModelProperty(value = "操作类型")
    private String type;

    @ApiModelProperty(value = "操作原因")
    private String remark;

    @ApiModelProperty(value = "操作人")
    private String empId;

}
