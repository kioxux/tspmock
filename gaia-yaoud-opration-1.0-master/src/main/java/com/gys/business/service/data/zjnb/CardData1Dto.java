package com.gys.business.service.data.zjnb;

import lombok.Data;

/**
 * 卡数据格式1
 *
 * @author cai si jun
 * @date 2021/10/22 15:55
 */
@Data
public class CardData1Dto {
    /**
     * 卡类别
     */
    private String cardType;
    /**
     * 卡内数据
     */
    private String cardData;

}
