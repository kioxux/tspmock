package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MemberCardAddOutData implements Serializable {

    /**
     * 汇总门店数量
     */
    @ApiModelProperty(value = "汇总门店数量")
    private int brName;

    /**
     * 汇总办卡员工姓名数量
     */
    @ApiModelProperty(value = "汇总办卡员工姓名数量")
    private int userName;

    /**
     * 汇总新增会员数量
     */
    @ApiModelProperty(value = "汇总新增会员数量")
    private int addNum;

}
