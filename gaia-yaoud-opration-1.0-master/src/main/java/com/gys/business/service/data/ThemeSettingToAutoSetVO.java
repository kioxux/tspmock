package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/9/27
 */
@Data
public class ThemeSettingToAutoSetVO implements Serializable {
    private static final long serialVersionUID = -6067009567779447361L;

    /**
     * 主题属性
     */
    private String gseasFlag;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 主题单号
     */
    private String gsetsId;

    @ApiModelProperty(value = "起始日期")
    private String gseasBeginDate;

    @ApiModelProperty(value = "结束日期")
    private String gseasEndDate;

    @ApiModelProperty(value = "选择电子券")
    List<ElectronAutoSetInVO> setInVOS;
}
