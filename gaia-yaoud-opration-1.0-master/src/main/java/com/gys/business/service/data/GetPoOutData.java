package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetPoOutData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "采购订单号")
    private String poId;
    @ApiModelProperty(value = "采购订单类型")
    private String poType;
    @ApiModelProperty(value = "供应商编码")
    private String poSupplierId;
    @ApiModelProperty(value = "供应商名称")
    private String poSupplierName;
    @ApiModelProperty(value = "采购主体（店号）")
    private String poCompanyCode;
    @ApiModelProperty(value = "凭证日期")
    private String poDate;
    @ApiModelProperty(value = "付款条款")
    private String poPaymentId;
    @ApiModelProperty(value = "抬头备注")
    private String poHeadRemark;
    @ApiModelProperty(value = "物流模式")
    private String poDeliveryType;
    private String poPrePoid;
    private String poReqId;
    @ApiModelProperty(value = "订单商品总数量")
    private BigDecimal totalQty;
    @ApiModelProperty(value = "订单商品总金额")
    private BigDecimal totalAmount;
    private String poCreateBy;
    private String poCreateDate;
    private String poCreateTime;
    @ApiModelProperty(value = "审批状态")
    private String poApproveStatus;
    private String poUpdateBy;
    private String poUpdateDate;
    private String poUpdateTime;
}
