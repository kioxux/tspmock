package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaStoreInSuggestionD;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * 门店调入建议-明细表(GaiaStoreInSuggestionD)表服务接口
 *
 * @author makejava
 * @since 2021-10-28 10:52:59
 */
public interface GaiaStoreInSuggestionDService {

}
