package com.gys.business.service.data.saleTask;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "药德系统月度计划入参")
public class SaleTaskSystemInData {
    @ApiModelProperty(value = "任务ID")
    private String staskTaskId;
    @ApiModelProperty(value = "任务名称 新建修改都是同一个名字")
    private String staskTaskName;
    @ApiModelProperty(value = "任务时间 起始日期")
    private String staskStartDate;
    @ApiModelProperty(value = "任务时间 结束日期")
    private String staskEndDate;
    @ApiModelProperty(value = "状态(S已保存，P已下发)")
    private String staskSplanSta;
    @ApiModelProperty(value = "创建人姓名")
    private String userName;
    @ApiModelProperty(value = "任务月份")
    //@NotEmpty(message = "monthPlan 任务月份不可为空!")
    private String monthPlan;
    @ApiModelProperty(value = "同比月份")
    @NotEmpty(message = "同比月份不可为空!")
    private String yoyMonth;

    @ApiModelProperty(value = "环比月份")
    @NotNull(message = "环比月份不可为空!")
    private String momMonth;

    @ApiModelProperty(value = "营业额同比增长率")
    @NotNull(message = "营业额同比增长率不可为空!")
    private String yoySaleAmt;

    @ApiModelProperty(value = "营业额环比增长率")
    @NotNull(message = "营业额环比增长率不可为空!")
    private String momSaleAmt;


    @ApiModelProperty(value = "毛利额同比增长率")
    @NotNull(message = "毛利额同比增长率不可为空!")
    private String yoySaleGross;

    @ApiModelProperty(value = "毛利额环比增长率")
    @NotNull(message = "毛利额环比增长率不可为空!")
    private String momSaleGross;

    @ApiModelProperty(value = "会员卡同比增长率")
    @NotNull(message = "会员卡同比增长率不可为空!")
    private String yoyMcardQty;

    @ApiModelProperty(value = "会员卡环比增长率")
    @NotNull(message = "会员卡环比增长率不可为空!")
    private String momMcardQty;
}
