package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2021/3/11
 */
@Data
public class QueryProductAppData implements Serializable {
    private static final long serialVersionUID = -2709691532499739635L;


    @ApiModelProperty(value = "商品集合")
    private List<GetQueryProductOutData> productData;

    @ApiModelProperty(value = "药事推荐等信息")
    private GetServerOutData serverOutData;




}
