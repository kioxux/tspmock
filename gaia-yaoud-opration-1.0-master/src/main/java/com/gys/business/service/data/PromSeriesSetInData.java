package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromSeriesSetInData implements Serializable {
    private static final long serialVersionUID = 8544170295063613358L;
    private String clientId;
    private String gspssVoucherId;
    private String gspssSerial;
    private String gspssSeriesId;
    private String gspssReachQty1;
    private BigDecimal gspssReachAmt1;
    private BigDecimal gspssResultAmt1;
    private String gspssResultRebate1;
    private String gspssReachQty2;
    private BigDecimal gspssReachAmt2;
    private BigDecimal gspssResultAmt2;
    private String gspssResultRebate2;
    private String gspssReachQty3;
    private BigDecimal gspssReachAmt3;
    private BigDecimal gspssResultAmt3;
    private String gspssResultRebate3;
}
