package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ProductLimitSetService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.UtilMessage;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductLimitSetServiceImpl implements ProductLimitSetService {

    @Autowired
    private ProductLimitSetMapper productLimitSetMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private ProductLimitStoMapper productLimitStoMapper;

    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Autowired
    private GaiaSdProductPriceMapper productPriceMapper;

    @Autowired
    private GaiaSdSaleHMapper saleHMapper;


    @Override
    public ProductLimitSetInitRes init(GetLoginOutData userInfo) {
        ProductLimitSetInitRes res = new ProductLimitSetInitRes();
        Map<String, String> stores = new HashMap<>();
        Map<String, String> limitTypes = new HashMap<>();
        Map<String, String> limitFlag = new HashMap<>();
        Map<String, String> yesOrNo = new HashMap<>();
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        String client = userInfo.getClient();//获取客户编号
        //初始化门店
        Example exampleStore = new Example(GaiaStoreData.class);
        exampleStore.createCriteria().andEqualTo("stoStatus", "0").andEqualTo("client", client);
        List<GaiaStoreData> storeDataList = storeDataMapper.selectByExample(exampleStore);
        if (CollectionUtil.isNotEmpty(storeDataList)) {
            for (GaiaStoreData storeData : storeDataList) {
                stores.put(storeData.getStoCode(), storeData.getStoName());
            }
        }
        res.setStores(stores);
        //初始化限购类型0：每单限购，1：会员限购
        limitTypes.put("0", "每单限购");
        limitTypes.put("1", "会员限购");
        res.setLimitTypes(limitTypes);
        //初始化限购方式
        limitFlag.put("0", "每天");
        limitFlag.put("1", "合计");
        res.setLimitFlag(limitFlag);
        yesOrNo.put("0", "否");
        yesOrNo.put("1", "是");
        res.setYesOrNo(yesOrNo);
        return res;
    }

    /**
     * 返回true表示数据库中已经存在设置过的商品数据
     *
     * @param userInfo
     * @param inData
     * @return
     */
    @Override
    public boolean checkIfInDb(GetLoginOutData userInfo, ProductLimitCheckInDb inData) {
        boolean res = false;
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (CollectionUtil.isEmpty(inData.getProIds())) {
            throw new BusinessException("请先选择药品！");
        }
        String client = userInfo.getClient();//获取客户编号
//        Example example = new Example(ProductLimitSet.class);
//        example.createCriteria().andEqualTo("client", client).andIn("proId", inData.getProIds());
        List<ProductLimitCheckRes> checkResList = productLimitSetMapper.selectCountNumber(client, inData.getProIds());
        String message = "";
        if (CollectionUtil.isNotEmpty(checkResList)) {
            for (ProductLimitCheckRes checkRes : checkResList) {
                if (checkRes != null && checkRes.getCount() > 0) {
                    message = message + checkRes.getProCommonName() + ";编码:" + checkRes.getProId();
//                    throw new BusinessException(checkRes.getProCommonName() + "该商品已有设置记录，商品编码 " + checkRes.getProId() + "请查询后重新修改！");
                }
            }
        }
        if (StrUtil.isNotBlank(message)) {
            res = true;
            throw new BusinessException("下列商品：" + message + "存在设置，请检查");
        }

        return res;
    }

    @Override
    public Object save(GetLoginOutData userInfo, ProductLimitInReq inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        String client = userInfo.getClient();//获取客户编号
        ProductLimitSetInitRes init = this.init(userInfo);
        if (!checkIfDataLegal(userInfo, inData, true)) {
            List<ProductLimitSet> productLimitSetList = new ArrayList<>();
            List<ProductLimitSto> productLimitStoList = new ArrayList<>();
            //拿到前端传递的数据
            List<ProductLimitInData> dataList = inData.getDataList();
            for (ProductLimitInData data : dataList) {
                //处理前端
                List<String> inTimeArr = data.getInTimeArr();
                if (CollectionUtil.isNotEmpty(inTimeArr) && inTimeArr.size() == 2) {
                    data.setBeginDate(DateUtil.format(DateUtil.parse(inTimeArr.get(0)), DatePattern.PURE_DATE_PATTERN));
                    data.setEndDate(DateUtil.format(DateUtil.parse(inTimeArr.get(1)), DatePattern.PURE_DATE_PATTERN));
                }
                ProductLimitSet productLimitSet = new ProductLimitSet();
                productLimitSet.setClient(client);
                productLimitSet.setProId(data.getProId());
                productLimitSet.setLimitType(data.getLimitType());
                productLimitSet.setLimitFlag(data.getLimitFlag());
                productLimitSet.setLimitQty(data.getLimitQty());
                productLimitSet.setIfAllTime(data.getIfAllTime());
                if(data.getIfSale() != null){
                    productLimitSet.setIfSale(data.getIfSale());
                }
                if (StrUtil.isNotBlank(data.getBeginDate()) && data.getBeginDate().contains("-")) {
                    productLimitSet.setBeginDate(data.getBeginDate().replaceAll("-", ""));
                } else {
                    productLimitSet.setBeginDate(data.getBeginDate());
                }
                if (StrUtil.isNotBlank(data.getEndDate()) && data.getEndDate().contains("-")) {
                    productLimitSet.setEndDate(data.getEndDate().replaceAll("-", ""));
                } else {
                    productLimitSet.setEndDate(data.getEndDate());
                }
//                productLimitSet.setEndDate(data.getEndDate());
                productLimitSet.setWeekFrequency(data.getWeekFrequency());
                productLimitSet.setDateFrequency(data.getDateFrequency());
                productLimitSet.setIfAllSto(data.getIfAllSto());
                productLimitSetList.add(productLimitSet);
                List<String> sites = data.getSites();
//                if ("1".equals(data.getIfAllSto())) {
//                    List<String> allSites = new ArrayList<>();
//                    Map<String, String> storeMap = init.getStores();
//                    if (CollectionUtil.isNotEmpty(storeMap)) {
//                        for (String s : storeMap.keySet()) {
//                            allSites.add(s);
//                        }
//                        sites = allSites;
//                    }
//                }
                if (CollectionUtil.isNotEmpty(sites)) {
                    for (String site : sites) {
                        ProductLimitSto sto = new ProductLimitSto();
                        sto.setClient(client);
                        sto.setProId(data.getProId());
                        sto.setBrId(site);
                        productLimitStoList.add(sto);
                    }
                }
            }
            if (CollectionUtil.isNotEmpty(productLimitSetList)) {
                productLimitSetMapper.insertList(productLimitSetList);
            }
            if (CollectionUtil.isNotEmpty(productLimitStoList)) {
                productLimitStoMapper.insertList(productLimitStoList);
            }

        }
        return null;
    }

    @Override
    public Object edit(GetLoginOutData userInfo, ProductLimitInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (ObjectUtil.isEmpty(inData) || inData.getId() == null) {
            throw new BusinessException("请传入合法数据！");
        }

        if (CollectionUtil.isNotEmpty(inData.getInTimeArr()) && inData.getInTimeArr().size() == 2) {
            inData.setBeginDate(DateUtil.format(DateUtil.parse(inData.getInTimeArr().get(0)), DatePattern.PURE_DATE_PATTERN));
            inData.setEndDate(DateUtil.format(DateUtil.parse(inData.getInTimeArr().get(1)), DatePattern.PURE_DATE_PATTERN));

        }
        ProductLimitInReq req = new ProductLimitInReq();
        List<ProductLimitInData> dataList = new ArrayList<>();
        dataList.add(inData);
        req.setDataList(dataList);
        this.checkIfDataLegal(userInfo, req, false);
        //获取原来的数据，用于删除sto表中的数据
        ProductLimitSet productLimitSetOld = productLimitSetMapper.selectByPrimaryKey(inData.getId());
        if (productLimitSetOld == null) {
            throw new BusinessException("查无此数据！");
        }
        if (!productLimitSetOld.getProId().equals(inData.getProId())) {
            throw new BusinessException("商品不可以修改！");
        }

        ProductLimitSetInitRes init = this.init(userInfo);
        String client = userInfo.getClient();//获取客户编号
        ProductLimitSet productLimitSet = new ProductLimitSet();
        productLimitSet.setId(inData.getId());
        productLimitSet.setClient(client);
        productLimitSet.setProId(inData.getProId());
        productLimitSet.setLimitType(inData.getLimitType());
        productLimitSet.setLimitFlag(inData.getLimitFlag());
        productLimitSet.setLimitQty(inData.getLimitQty());
        productLimitSet.setIfAllTime(inData.getIfAllTime());
        if(inData.getIfSale()!=null){
            productLimitSet.setIfSale(inData.getIfSale());
        }
        if (StrUtil.isNotBlank(inData.getBeginDate()) && inData.getBeginDate().contains("-")) {
            productLimitSet.setBeginDate(inData.getBeginDate().replaceAll("-", ""));
        } else {
            productLimitSet.setBeginDate(inData.getBeginDate());
        }
        if (StrUtil.isNotBlank(inData.getEndDate()) && inData.getEndDate().contains("-")) {
            productLimitSet.setEndDate(inData.getEndDate().replaceAll("-", ""));
        } else {
            productLimitSet.setEndDate(inData.getEndDate());
        }
//        productLimitSet.setBeginDate(inData.getBeginDate());
//        productLimitSet.setEndDate(inData.getEndDate());
        productLimitSet.setWeekFrequency(inData.getWeekFrequency());
        productLimitSet.setDateFrequency(inData.getDateFrequency());
        productLimitSet.setIfAllSto(inData.getIfAllSto());
        List<String> sites = inData.getSites();
//        if ("1".equals(inData.getIfAllSto())) {
//            List<String> allSites = new ArrayList<>();
//            Map<String, String> storeMap = init.getStores();
//            if (CollectionUtil.isNotEmpty(storeMap)) {
//                for (String s : storeMap.keySet()) {
//                    allSites.add(s);
//                }
//                sites = allSites;
//            }
//        }
        List<ProductLimitSto> productLimitStoList = new ArrayList<>();

        //处理productLimitSto 先删除在新增
        Example exampleStoDelete = new Example(ProductLimitSto.class);
        exampleStoDelete.createCriteria().andEqualTo("client", client).andEqualTo("proId", productLimitSetOld.getProId());
        productLimitStoMapper.deleteByExample(exampleStoDelete);
        if (CollectionUtil.isNotEmpty(sites)) {
            for (String site : sites) {
                ProductLimitSto sto = new ProductLimitSto();
                sto.setClient(client);
                sto.setProId(inData.getProId());
                sto.setBrId(site);
                productLimitStoList.add(sto);
            }
            if (CollectionUtil.isNotEmpty(productLimitStoList)) {
                productLimitStoMapper.insertList(productLimitStoList);
            }
        }
        productLimitSetMapper.updateByPrimaryKey(productLimitSet);
        return null;
    }

    @Override
    public PageInfo<ProductLimitRes> list(GetLoginOutData userInfo, ProductLimitSetSearchVo inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
//        if (CollectionUtil.isEmpty(inData.getSites())) {
//            throw new BusinessException("门店为必填");
//        }
        ProductLimitSetInitRes init = this.init(userInfo);
        Map<String, String> storesMap = init.getStores();
        PageInfo pageInfo;
        List<ProductLimitRes> productLimitList = productLimitSetMapper.selectByCondition(inData, userInfo.getClient());
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        //处理设置的门店
        if (ObjectUtil.isNotEmpty(productLimitList)) {
            Example example = new Example(GaiaSdProductPrice.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andIn("clientId", productLimitList.stream().map(client -> client.getClient()).collect(Collectors.toList()));
            criteria.andIn("gsppProId", productLimitList.stream().map(prId -> prId.getProId()).collect(Collectors.toList()));
            List<GaiaSdProductPrice> prices = productPriceMapper.selectByExample(example);
            Map<String, GaiaSdProductPrice> pricesMap = new HashMap<>();
            if (CollectionUtil.isNotEmpty(prices)) {
                prices.forEach(x -> {
                    pricesMap.put(x.getClientId() + x.getGsppProId(), x);
                });
            }
            productLimitList.forEach(x -> {
                GaiaSdProductPrice gaiaSdProductPrice = pricesMap.get(x.getClient() + x.getProId());
                if (gaiaSdProductPrice != null) {
                    x.setPriceNormal(gaiaSdProductPrice.getGsppPriceNormal());
                    List<ProductLimitSto> stoList = x.getStoList();
                    if (CollectionUtil.isNotEmpty(stoList)) {
                        for (ProductLimitSto sto : stoList) {
                            sto.setBrName(storesMap.get(sto.getBrId()) == null ? "" : storesMap.get(sto.getBrId()));
                        }
                    }
                }
            });
            pageInfo = new PageInfo(productLimitList);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public ProductLimitRes editInit(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        Map<String, String> stores = new HashMap<>();
        //初始化门店
        Example exampleStore = new Example(GaiaStoreData.class);
        exampleStore.createCriteria().andEqualTo("stoStatus", "0").andEqualTo("client", userInfo.getClient());
        List<GaiaStoreData> storeDataList = storeDataMapper.selectByExample(exampleStore);
        if (CollectionUtil.isNotEmpty(storeDataList)) {
            for (GaiaStoreData storeData : storeDataList) {
                stores.put(storeData.getStoCode(), storeData.getStoName());
            }
        }
        ProductLimitRes res = new ProductLimitRes();
        ProductLimitSet productLimitSet = productLimitSetMapper.selectByPrimaryKey(id);
        if (productLimitSet == null) {
            throw new BusinessException("查询无此数据");
        }
        BeanUtils.copyProperties(productLimitSet, res);
        List<ProductLimitSto> stoList = new ArrayList<>();
        String client = productLimitSet.getClient();
        String proId = productLimitSet.getProId();
        Example example = new Example(ProductLimitSet.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("proId", proId);
        List<ProductLimitSto> productLimitStos = productLimitStoMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(productLimitStos)) {
            for (ProductLimitSto sto : productLimitStos) {
                sto.setBrName(stores.get(sto.getBrId()));
                stoList.add(sto);
            }
        }
        res.setStoList(stoList);

        return res;
    }

    @Override
    public Object delete(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (id == null) {
            throw new BusinessException("请传入合法数据！");
        }
        //获取原来的数据，用于删除sto表中的数据
        ProductLimitSet productLimitSetOld = productLimitSetMapper.selectByPrimaryKey(id);
        if (productLimitSetOld == null) {
            throw new BusinessException("查无此数据！");
        }
        Example exampleStoDelete = new Example(ProductLimitSto.class);
        exampleStoDelete.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("proId", productLimitSetOld.getProId());
        productLimitStoMapper.deleteByExample(exampleStoDelete);
        productLimitSetMapper.deleteByPrimaryKey(id);
        return null;
    }

    @Override
    public List<GetProductThirdlyOutDataExtends> getProductInfoList(GetLoginOutData userInfo, List<String> importInDataList) {
        List<GetProductThirdlyOutDataExtends> resList = new ArrayList<>();
        if (CollectionUtil.isEmpty(importInDataList)) {
            throw new BusinessException("请填写药品编码！");
        }
        Example exampleBussiness = new Example(GaiaProductBusiness.class);
        exampleBussiness.createCriteria().andEqualTo("client", userInfo.getClient()).andIn("proSelfCode", importInDataList);
        List<GaiaProductBusiness> businessDbList = gaiaProductBusinessMapper.selectByExample(exampleBussiness);
        List<GaiaProductBusiness> businessList = new ArrayList<>();
        List<String> dbIds = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(businessDbList)) {
            Map<String, GaiaProductBusiness> businessMap = new HashMap<>();
            for (GaiaProductBusiness business : businessDbList) {
                String key = business.getClient() + business.getProSelfCode();
                businessMap.put(key, business);
            }

            if (CollectionUtil.isNotEmpty(businessMap)) {
                for (String key : businessMap.keySet()) {
                    dbIds.add(businessMap.get(key).getProSelfCode());
                    businessList.add(businessMap.get(key));
                }
            }
            //每个商品编码取一个即可
            //进行商品编码的匹配，如果存在无法查询的商品编码需要进行提示
            String notExitMeaasge = "";
            if (CollectionUtil.isNotEmpty(dbIds)) {
                for (String inProId : importInDataList) {
                    if (!dbIds.contains(inProId)) {
                        notExitMeaasge = notExitMeaasge + ";";
                    }
                }
            }
            if (StrUtil.isNotBlank(notExitMeaasge)) {
                throw new BusinessException("如下药品编码：" + notExitMeaasge + "无法匹配，请重新核验");
            }
        } else {
            throw new BusinessException("未匹配到可识别的药品编码！");
        }

        if (CollectionUtil.isNotEmpty(businessList)) {
            Example example = new Example(GaiaSdProductPrice.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("clientId", userInfo.getClient());
            criteria.andIn("gsppProId", dbIds);
            List<GaiaSdProductPrice> prices = productPriceMapper.selectByExample(example);
            Map<String, GaiaSdProductPrice> pricesMap = new HashMap<>();
            if (CollectionUtil.isNotEmpty(prices)) {
                prices.forEach(x -> {
                    pricesMap.put(x.getClientId() + x.getGsppProId(), x);
                });
            }
            if (CollectionUtil.isNotEmpty(businessList)) {
                for (GaiaProductBusiness business : businessList) {
                    GetProductThirdlyOutDataExtends outData = new GetProductThirdlyOutDataExtends();
                    outData.setProCode(business.getProSelfCode());
                    outData.setProCommonName(business.getProCommonname());
                    outData.setSpecs(business.getProSpecs());
                    outData.setUnit(business.getProUnit());
                    outData.setProFactoryName(business.getProFactoryName());
                    outData.setProPlace(business.getProPlace());
                    outData.setPriceNormal(pricesMap.get(userInfo.getClient() + business.getProSelfCode()) == null ? new BigDecimal(0) : pricesMap.get(userInfo.getClient() + business.getProSelfCode()).getGsppPriceNormal());
                    resList.add(outData);
                }
            }
        }
        return resList;
    }

    /**
     * 检查前台数据是否合法
     *
     * @param inDataReq
     */
    private boolean checkIfDataLegal(GetLoginOutData userInfo, ProductLimitInReq inDataReq, boolean flag) {
        boolean res = false;
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        List<ProductLimitInData> dataList = inDataReq.getDataList();
        List<String> proIdsChoose = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(inDataReq.getDataList())) {
            inDataReq.getDataList().forEach(x -> {
                if (StrUtil.isNotBlank(x.getProId())) {
                    proIdsChoose.add(x.getProId());
                }
            });
            if (CollectionUtil.isEmpty(proIdsChoose)) {
                throw new BusinessException("请先选择商品再保存！");
            }
        }
        List<String> legalList = new ArrayList<>();
        legalList.add("0");
        legalList.add("1");
        ProductLimitCheckInDb checkInDb = new ProductLimitCheckInDb();
        List<String> proIds = new ArrayList<>();
        for (ProductLimitInData inData : dataList) {
            proIds.add(inData.getProId());
            //限购类型
            String limitType = inData.getLimitType();
            if (StrUtil.isBlank(limitType)) {
                throw new BusinessException("请选择商品限购方式！");
            }
            if (!legalList.contains(limitType)) {
                throw new BusinessException("请选择合法的商品限购方式！");
            }
            //限购方式
            String limitFlag = inData.getLimitFlag();
            if ("0".equals(limitType) && StrUtil.isNotBlank(limitFlag)) {
                //每单限购
                throw new BusinessException("限购方式为每单限购时限购方式不可选！");
            }
            if ("1".equals(limitType) && StrUtil.isBlank(limitFlag)) {
                //会员限购
                throw new BusinessException("限购方式为会员限购时必须选择限购方式！");
            }
            if ("1".equals(limitType) && StrUtil.isNotBlank(limitFlag) && !legalList.contains(limitFlag)) {
                throw new BusinessException("限购方式不合法！");
            }
            // 是否一直生效. 0：否，1：是
            String ifAllTime = inData.getIfAllTime();
            if ("1".equals(limitType) && "1".equals(limitFlag) && StrUtil.isNotBlank(ifAllTime) && "1".equals(ifAllTime)) {
                throw new BusinessException("会员限购限购方式为合计时，是否一直生效只能选择否！");
            }

            // 是否一直生效. 0：否，1：是
//            String ifAllTime = inData.getIfAllTime();
            //起始日期
            String beginDate = inData.getBeginDate();
            //结束日期
            String endDate = inData.getEndDate();
            if (!legalList.contains(ifAllTime)) {
                throw new BusinessException("是否一直生效数据不合法！");
            }
            if ("1".equals(ifAllTime) && (StrUtil.isNotBlank(beginDate) || StrUtil.isNotBlank(endDate))) {
                throw new BusinessException("是否一直生效为是时，不可以选择生效日期");
            }
            if ("0".equals(ifAllTime) && (StrUtil.isBlank(beginDate) || StrUtil.isBlank(endDate))) {
                throw new BusinessException("是否一直生效为否时，需要选择生效日期");
            }

            //是否所有门店0：否，1：是
            String ifAllSto = inData.getIfAllSto();
            List<String> sites = inData.getSites();
            if (!legalList.contains(ifAllSto)) {
                throw new BusinessException("是否所有门店数据不合法！");
            }
            if ("1".equals(ifAllSto) && (CollectionUtil.isNotEmpty(sites))) {
                throw new BusinessException("是否所有门店选择为是的情况下，门店选择数据应为空！");
            }
            if ("0".equals(ifAllSto) && (CollectionUtil.isEmpty(sites))) {
                throw new BusinessException("是否所有门店选择为否的情况下，需要选择对应门店！");
            }
        }
        if (flag) {
            checkInDb.setProIds(proIds);
            this.checkIfInDb(userInfo, checkInDb);
        }
        return res;
    }

    @Override
    public void commodityPurchaseRestrictions(GetLoginOutData userInfo, PurchaseLimitVo limitVo) {
        //汇总所有的商品编码
        List<String> proIds = limitVo.getMap().keySet().stream().collect(Collectors.toList());
        //根据商品编码 加盟商 门店 拿到条件 商品
        List<ProductLimitSet> setList = this.productLimitSetMapper.getAllByPros(userInfo.getClient(), userInfo.getDepId(), proIds);
        if (CollUtil.isNotEmpty(setList)) {
            for (ProductLimitSet limitSet : setList) {
                String nowDate = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
                BigDecimal limitQty = new BigDecimal(limitSet.getLimitQty());//限购数量
                BigDecimal qty = new BigDecimal(limitVo.getMap().get(limitSet.getProId()));//当前商品购买数量
                //先验证购买类型
                if ("0".equals(limitSet.getLimitType()) && ("1".equals(limitSet.getIfAllTime()) || DateUtil.isIn(new Date(), DateUtil.parse(limitSet.getBeginDate()), DateUtil.parse(limitSet.getEndDate())))) { //每单限购
                    if (qty.compareTo(limitQty) == 1) { //购买数量 大于 限购数量
                        throw new BusinessException(limitSet.getProId() + UtilMessage.THE_SALES_QUANTITY_HAS_EXCEEDED_THE_PURCHASE_LIMIT);
                    }
                } else if ("1".equals(limitSet.getLimitType()) && StrUtil.isNotBlank(limitVo.getCardNum())) {//会员限购 ()
                    if ("0".equals(limitSet.getLimitFlag()) && ("1".equals(limitSet.getIfAllTime()) || DateUtil.isIn(new Date(), DateUtil.parse(limitSet.getBeginDate()), DateUtil.parse(limitSet.getEndDate())))) {//每天 验证 是否 一直生效 ,以及 是否在范围内
                        PurchaseLimitDto limitDto = this.saleHMapper.getPurchaseQuantityByCardNum(userInfo.getClient(), limitSet.getProId(), limitVo.getCardNum(), nowDate, null, null);
                        this.verificationConditions(limitSet, limitQty, qty, limitDto);

                    } else if ("1".equals(limitSet.getLimitFlag()) && DateUtil.isIn(new Date(), DateUtil.parse(limitSet.getBeginDate()), DateUtil.parse(limitSet.getEndDate()))) {//合计 (不验证一直生效) 当前日期 在 设置范围内
                        PurchaseLimitDto limitDto = this.saleHMapper.getPurchaseQuantityByCardNum(userInfo.getClient(), limitSet.getProId(), limitVo.getCardNum(), null, limitSet.getBeginDate(), limitSet.getEndDate());
                        this.verificationConditions(limitSet, limitQty, qty, limitDto);
                    }
                }
            }
        }
    }

    /**
     * 验证条件
     *
     * @param limitSet
     * @param limitQty
     * @param qty
     * @param limitDto
     */
    private void verificationConditions(ProductLimitSet limitSet, BigDecimal limitQty, BigDecimal qty, PurchaseLimitDto limitDto) {
        if (ObjectUtil.isNotEmpty(limitDto)) {
            BigDecimal buyNum = new BigDecimal(limitDto.getBuyNum());//查询汇总数量
            if (buyNum.compareTo(limitQty) == 1 || buyNum.add(qty).compareTo(limitQty) == 1) {//统计购买 数量 大于 限购数量
                throw new BusinessException(limitSet.getProId() + UtilMessage.THE_SALES_QUANTITY_HAS_EXCEEDED_THE_PURCHASE_LIMIT);
            }
        } else {
            if (qty.compareTo(limitQty) == 1) {//当前商品购买数量 大于 限购数量
                throw new BusinessException(limitSet.getProId() + UtilMessage.THE_SALES_QUANTITY_HAS_EXCEEDED_THE_PURCHASE_LIMIT);
            }
        }
    }
}
