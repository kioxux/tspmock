//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GiftCardSet {
    private String gspcsCouponType;
    private String gspcsCouponRemarks;
    private String gspcgActNo;
    private String gspcsBeginDate;
    private String gspcsEndDate;
    private String gspcsBeginTime;
    private String gspcsEndTime;
    private String gspcsSingleUseAmt;
    private String gspcsSinglePaycheckAmt;

    public GiftCardSet() {
    }

    public String getGspcsCouponType() {
        return this.gspcsCouponType;
    }

    public String getGspcsCouponRemarks() {
        return this.gspcsCouponRemarks;
    }

    public String getGspcgActNo() {
        return this.gspcgActNo;
    }

    public String getGspcsBeginDate() {
        return this.gspcsBeginDate;
    }

    public String getGspcsEndDate() {
        return this.gspcsEndDate;
    }

    public String getGspcsBeginTime() {
        return this.gspcsBeginTime;
    }

    public String getGspcsEndTime() {
        return this.gspcsEndTime;
    }

    public String getGspcsSingleUseAmt() {
        return this.gspcsSingleUseAmt;
    }

    public String getGspcsSinglePaycheckAmt() {
        return this.gspcsSinglePaycheckAmt;
    }

    public void setGspcsCouponType(final String gspcsCouponType) {
        this.gspcsCouponType = gspcsCouponType;
    }

    public void setGspcsCouponRemarks(final String gspcsCouponRemarks) {
        this.gspcsCouponRemarks = gspcsCouponRemarks;
    }

    public void setGspcgActNo(final String gspcgActNo) {
        this.gspcgActNo = gspcgActNo;
    }

    public void setGspcsBeginDate(final String gspcsBeginDate) {
        this.gspcsBeginDate = gspcsBeginDate;
    }

    public void setGspcsEndDate(final String gspcsEndDate) {
        this.gspcsEndDate = gspcsEndDate;
    }

    public void setGspcsBeginTime(final String gspcsBeginTime) {
        this.gspcsBeginTime = gspcsBeginTime;
    }

    public void setGspcsEndTime(final String gspcsEndTime) {
        this.gspcsEndTime = gspcsEndTime;
    }

    public void setGspcsSingleUseAmt(final String gspcsSingleUseAmt) {
        this.gspcsSingleUseAmt = gspcsSingleUseAmt;
    }

    public void setGspcsSinglePaycheckAmt(final String gspcsSinglePaycheckAmt) {
        this.gspcsSinglePaycheckAmt = gspcsSinglePaycheckAmt;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GiftCardSet)) {
            return false;
        } else {
            GiftCardSet other = (GiftCardSet)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$gspcsCouponType = this.getGspcsCouponType();
                    Object other$gspcsCouponType = other.getGspcsCouponType();
                    if (this$gspcsCouponType == null) {
                        if (other$gspcsCouponType == null) {
                            break label119;
                        }
                    } else if (this$gspcsCouponType.equals(other$gspcsCouponType)) {
                        break label119;
                    }

                    return false;
                }

                Object this$gspcsCouponRemarks = this.getGspcsCouponRemarks();
                Object other$gspcsCouponRemarks = other.getGspcsCouponRemarks();
                if (this$gspcsCouponRemarks == null) {
                    if (other$gspcsCouponRemarks != null) {
                        return false;
                    }
                } else if (!this$gspcsCouponRemarks.equals(other$gspcsCouponRemarks)) {
                    return false;
                }

                label105: {
                    Object this$gspcgActNo = this.getGspcgActNo();
                    Object other$gspcgActNo = other.getGspcgActNo();
                    if (this$gspcgActNo == null) {
                        if (other$gspcgActNo == null) {
                            break label105;
                        }
                    } else if (this$gspcgActNo.equals(other$gspcgActNo)) {
                        break label105;
                    }

                    return false;
                }

                Object this$gspcsBeginDate = this.getGspcsBeginDate();
                Object other$gspcsBeginDate = other.getGspcsBeginDate();
                if (this$gspcsBeginDate == null) {
                    if (other$gspcsBeginDate != null) {
                        return false;
                    }
                } else if (!this$gspcsBeginDate.equals(other$gspcsBeginDate)) {
                    return false;
                }

                label91: {
                    Object this$gspcsEndDate = this.getGspcsEndDate();
                    Object other$gspcsEndDate = other.getGspcsEndDate();
                    if (this$gspcsEndDate == null) {
                        if (other$gspcsEndDate == null) {
                            break label91;
                        }
                    } else if (this$gspcsEndDate.equals(other$gspcsEndDate)) {
                        break label91;
                    }

                    return false;
                }

                Object this$gspcsBeginTime = this.getGspcsBeginTime();
                Object other$gspcsBeginTime = other.getGspcsBeginTime();
                if (this$gspcsBeginTime == null) {
                    if (other$gspcsBeginTime != null) {
                        return false;
                    }
                } else if (!this$gspcsBeginTime.equals(other$gspcsBeginTime)) {
                    return false;
                }

                label77: {
                    Object this$gspcsEndTime = this.getGspcsEndTime();
                    Object other$gspcsEndTime = other.getGspcsEndTime();
                    if (this$gspcsEndTime == null) {
                        if (other$gspcsEndTime == null) {
                            break label77;
                        }
                    } else if (this$gspcsEndTime.equals(other$gspcsEndTime)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$gspcsSingleUseAmt = this.getGspcsSingleUseAmt();
                    Object other$gspcsSingleUseAmt = other.getGspcsSingleUseAmt();
                    if (this$gspcsSingleUseAmt == null) {
                        if (other$gspcsSingleUseAmt == null) {
                            break label70;
                        }
                    } else if (this$gspcsSingleUseAmt.equals(other$gspcsSingleUseAmt)) {
                        break label70;
                    }

                    return false;
                }

                Object this$gspcsSinglePaycheckAmt = this.getGspcsSinglePaycheckAmt();
                Object other$gspcsSinglePaycheckAmt = other.getGspcsSinglePaycheckAmt();
                if (this$gspcsSinglePaycheckAmt == null) {
                    if (other$gspcsSinglePaycheckAmt != null) {
                        return false;
                    }
                } else if (!this$gspcsSinglePaycheckAmt.equals(other$gspcsSinglePaycheckAmt)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GiftCardSet;
    }

    public int hashCode() {

        int result = 1;
        Object $gspcsCouponType = this.getGspcsCouponType();
        result = result * 59 + ($gspcsCouponType == null ? 43 : $gspcsCouponType.hashCode());
        Object $gspcsCouponRemarks = this.getGspcsCouponRemarks();
        result = result * 59 + ($gspcsCouponRemarks == null ? 43 : $gspcsCouponRemarks.hashCode());
        Object $gspcgActNo = this.getGspcgActNo();
        result = result * 59 + ($gspcgActNo == null ? 43 : $gspcgActNo.hashCode());
        Object $gspcsBeginDate = this.getGspcsBeginDate();
        result = result * 59 + ($gspcsBeginDate == null ? 43 : $gspcsBeginDate.hashCode());
        Object $gspcsEndDate = this.getGspcsEndDate();
        result = result * 59 + ($gspcsEndDate == null ? 43 : $gspcsEndDate.hashCode());
        Object $gspcsBeginTime = this.getGspcsBeginTime();
        result = result * 59 + ($gspcsBeginTime == null ? 43 : $gspcsBeginTime.hashCode());
        Object $gspcsEndTime = this.getGspcsEndTime();
        result = result * 59 + ($gspcsEndTime == null ? 43 : $gspcsEndTime.hashCode());
        Object $gspcsSingleUseAmt = this.getGspcsSingleUseAmt();
        result = result * 59 + ($gspcsSingleUseAmt == null ? 43 : $gspcsSingleUseAmt.hashCode());
        Object $gspcsSinglePaycheckAmt = this.getGspcsSinglePaycheckAmt();
        result = result * 59 + ($gspcsSinglePaycheckAmt == null ? 43 : $gspcsSinglePaycheckAmt.hashCode());
        return result;
    }

    public String toString() {
        return "GiftCardSet(gspcsCouponType=" + this.getGspcsCouponType() + ", gspcsCouponRemarks=" + this.getGspcsCouponRemarks() + ", gspcgActNo=" + this.getGspcgActNo() + ", gspcsBeginDate=" + this.getGspcsBeginDate() + ", gspcsEndDate=" + this.getGspcsEndDate() + ", gspcsBeginTime=" + this.getGspcsBeginTime() + ", gspcsEndTime=" + this.getGspcsEndTime() + ", gspcsSingleUseAmt=" + this.getGspcsSingleUseAmt() + ", gspcsSinglePaycheckAmt=" + this.getGspcsSinglePaycheckAmt() + ")";
    }
}
