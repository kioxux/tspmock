package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSoBatchAccount;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/23 9:24
 */
public interface GaiaSoBatchAccountService {

    List<GaiaSoBatchAccount> findAllOpen();

    GaiaSoBatchAccount findByClient(String client);
}
