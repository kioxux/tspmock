package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class ClientSiteChoose implements Serializable {

    private static final long serialVersionUID = -6943830956389606763L;

    @ApiModelProperty(value = "客户集合", required = false)
    private List<KeyValue> clients;

    @ApiModelProperty(value = "客户地点集合", required = false)
    private Map<String, List<KeyValue>> clientSiteMap;

    @ApiModelProperty(value = "库存推算模式", required = false)
    private List<String> caculateTypes;


    @Data
    public
    class KeyValue {

        private String key;

        private String value;
    }
}
