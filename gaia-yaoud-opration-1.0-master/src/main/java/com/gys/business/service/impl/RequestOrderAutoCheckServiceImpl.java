package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.mapper.GaiaSdReplenishHMapper;
import com.gys.business.service.RequestOrderAutoCheckService;
import com.gys.business.service.RequestOrderCheckService;
import com.gys.business.service.data.GaiaClSystemPara;
import com.gys.business.service.data.check.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 15:19
 **/
@Slf4j
@Service("requestOrderAutoCheckService")
public class RequestOrderAutoCheckServiceImpl implements RequestOrderAutoCheckService {

    @Resource
    private RequestOrderCheckService requestOrderCheckService;
    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;


    /**
     * 自动审核委托配送请货数据
     */
    public void autoAuditRequestOrder(){
        //查询配置自动审核标志的加盟商列表
        List<GaiaClSystemPara> clSystemParaList = gaiaClSystemParaMapper.getParaByGcspIdAndParaId("WTPS_PD_AUTO","1");
        for(GaiaClSystemPara gaiaClSystemPara : clSystemParaList){
            String client = gaiaClSystemPara.getClient();
            //查询未审核的委托配送订单信息
            List<OrderOperateForm> orderDetailFormList  = getAutoReviewOrder(client,"0","system");
            autoReviewOrder(orderDetailFormList);
        }
    }

    /**
     * 获取加盟商下订单数据
     * @param client
     * @param state
     * @return
     */
    private List<OrderOperateForm> getAutoReviewOrder(String client,String state,String gsrhDnBy){
        OrderCheckForm orderCheckForm = new OrderCheckForm();
        orderCheckForm.setClient(client);
        orderCheckForm.setStatus(state);
        //获取订单信息
        List<OrderOperateForm> orderDetailFormList = new ArrayList<OrderOperateForm>();
        List<RequestOrderVO> orderList = gaiaSdReplenishHMapper.orderList(orderCheckForm);
        for(RequestOrderVO requestOrderVO : orderList) {
            OrderDetailForm orderDetailForm = new OrderDetailForm();
            orderDetailForm.setClient(client);
            orderDetailForm.setStoreId(requestOrderVO.getStoreId());
            orderDetailForm.setReplenishCode(requestOrderVO.getReplenishCode());

            RequestOrderDetailListVO requestOrderDetailListVO = requestOrderCheckService.getOrderDetail(orderDetailForm);
            OrderOperateForm orderOperateForm = new OrderOperateForm();
            orderOperateForm.setClient(client);
            orderOperateForm.setReplenishCode(requestOrderVO.getReplenishCode());
            orderOperateForm.setStoreId(requestOrderVO.getStoreId());
            orderOperateForm.setUserId(gsrhDnBy);
            if (requestOrderDetailListVO.getList() == null && requestOrderDetailListVO.getList().size() == 0) {
                continue;
            }
            List<OrderOperateForm.ReplenishDetail> detailList = new ArrayList<>();

            for (RequestOrderDetailVO requestOrderDetailVO : requestOrderDetailListVO.getList()) {
                OrderOperateForm.ReplenishDetail replenishDetail = new OrderOperateForm.ReplenishDetail();
                replenishDetail.setSendPrice(requestOrderDetailVO.getSendPrice());
                replenishDetail.setProSelfCode(requestOrderDetailVO.getProSelfCode());
                replenishDetail.setRequestQuantity(requestOrderDetailVO.getRequestQuantity());
                detailList.add(replenishDetail);
            }
            orderOperateForm.setDetailList(detailList);
            if (null != requestOrderDetailListVO.getHeadInfo()) {
                orderOperateForm.setRemarks(requestOrderDetailListVO.getHeadInfo().getRemarks());
            }
            orderDetailFormList.add(orderOperateForm);
        }
        return orderDetailFormList;
    }

    /**
     *
     * @param orderDetailFormList
     */
    private void autoReviewOrder(List<OrderOperateForm> orderDetailFormList){
        if(CollectionUtils.isEmpty(orderDetailFormList)){
            return;
        }
        for(OrderOperateForm operateForm : orderDetailFormList){
            try {
                requestOrderCheckService.auditOrder(operateForm);
            } catch (Exception e) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("加盟商：").append(operateForm.getClient()).append("门店号：").append(operateForm.getStoreId()).append("补货单号：")
                        .append(operateForm.getReplenishCode()).append("自动审核失败,失败原因为：").append(e.getLocalizedMessage());
                log.error(stringBuilder.toString());
            }
        }
    }
}
