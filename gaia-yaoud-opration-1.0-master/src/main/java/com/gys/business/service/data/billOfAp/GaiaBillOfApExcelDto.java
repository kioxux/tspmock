package com.gys.business.service.data.billOfAp;

import com.gys.common.excel.ExcelField;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaBillOfApExcelDto {

    private String client;

    @ExcelField(title = "发生地编码")
    private String dcCode;

    @ExcelField(title = "应付方式编码")
    private String paymentId;

    @ExcelField(title = "结算金额")
    private BigDecimal amountOfPayment;

    @ExcelField(title = "发生日期")
    private String paymentDate;

    @ExcelField(title = "供应商编码")
    private String supSelfCode;

    @ExcelField(title = "业务员编码")
    private String salesmanName;

    @ExcelField(title = "备注")
    private String remarks;
}
