package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 列表传参
 *
 * @author xiaoyuan on 2020/10/12
 */
@Data
public class ThemeSettingListInData implements Serializable {
    private static final long serialVersionUID = -1046082361736251271L;

    private String client;

    @ApiModelProperty(value = "送券时 传 CM")
    private String gsetsFlag;
}
