package com.gys.business.service.data.integralExchange;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.read.listener.ReadListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author ：gyx
 * @Date ：Created in 11:11 2021/11/29
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
public class IntegralExchangeImportVoListener implements ReadListener<IntegralExchangeProductVo> {

    public List<IntegralExchangeProductVo> cacheData=new ArrayList<>();
    @Override
    public void onException(Exception e, AnalysisContext analysisContext) throws Exception {

    }

    @Override
    public void invokeHead(Map<Integer, CellData> map, AnalysisContext analysisContext) {

    }

    @Override
    public void invoke(IntegralExchangeProductVo integralExchangeProductVo, AnalysisContext analysisContext) {
        cacheData.add(integralExchangeProductVo);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    @Override
    public boolean hasNext(AnalysisContext analysisContext) {
        return false;
    }
}
