package com.gys.business.service.data;

import lombok.Data;

/**
 * 自定义名称匹配
 */
@Data
public class ZDYNameOutData {
    private String zdy1;
    private String zdy2;
    private String zdy3;
    private String zdy4;
    private String zdy5;
}
