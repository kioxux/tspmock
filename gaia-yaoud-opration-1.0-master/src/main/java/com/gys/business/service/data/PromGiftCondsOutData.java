//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PromGiftCondsOutData {
    private String clientId;
    private String gspgcVoucherId;
    private String gspgcSerial;
    private String gspgcProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspgcSeriesId;
    private String gspgcMemFlag;
    private String gspgcInteFlag;
    private String gspgcInteRate;

    public PromGiftCondsOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspgcVoucherId() {
        return this.gspgcVoucherId;
    }

    public String getGspgcSerial() {
        return this.gspgcSerial;
    }

    public String getGspgcProId() {
        return this.gspgcProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGspgcSeriesId() {
        return this.gspgcSeriesId;
    }

    public String getGspgcMemFlag() {
        return this.gspgcMemFlag;
    }

    public String getGspgcInteFlag() {
        return this.gspgcInteFlag;
    }

    public String getGspgcInteRate() {
        return this.gspgcInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspgcVoucherId(final String gspgcVoucherId) {
        this.gspgcVoucherId = gspgcVoucherId;
    }

    public void setGspgcSerial(final String gspgcSerial) {
        this.gspgcSerial = gspgcSerial;
    }

    public void setGspgcProId(final String gspgcProId) {
        this.gspgcProId = gspgcProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGspgcSeriesId(final String gspgcSeriesId) {
        this.gspgcSeriesId = gspgcSeriesId;
    }

    public void setGspgcMemFlag(final String gspgcMemFlag) {
        this.gspgcMemFlag = gspgcMemFlag;
    }

    public void setGspgcInteFlag(final String gspgcInteFlag) {
        this.gspgcInteFlag = gspgcInteFlag;
    }

    public void setGspgcInteRate(final String gspgcInteRate) {
        this.gspgcInteRate = gspgcInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromGiftCondsOutData)) {
            return false;
        } else {
            PromGiftCondsOutData other = (PromGiftCondsOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gspgcVoucherId = this.getGspgcVoucherId();
                Object other$gspgcVoucherId = other.getGspgcVoucherId();
                if (this$gspgcVoucherId == null) {
                    if (other$gspgcVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspgcVoucherId.equals(other$gspgcVoucherId)) {
                    return false;
                }

                Object this$gspgcSerial = this.getGspgcSerial();
                Object other$gspgcSerial = other.getGspgcSerial();
                if (this$gspgcSerial == null) {
                    if (other$gspgcSerial != null) {
                        return false;
                    }
                } else if (!this$gspgcSerial.equals(other$gspgcSerial)) {
                    return false;
                }

                label134: {
                    Object this$gspgcProId = this.getGspgcProId();
                    Object other$gspgcProId = other.getGspgcProId();
                    if (this$gspgcProId == null) {
                        if (other$gspgcProId == null) {
                            break label134;
                        }
                    } else if (this$gspgcProId.equals(other$gspgcProId)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label127;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$gspgSpecs = this.getGspgSpecs();
                    Object other$gspgSpecs = other.getGspgSpecs();
                    if (this$gspgSpecs == null) {
                        if (other$gspgSpecs == null) {
                            break label120;
                        }
                    } else if (this$gspgSpecs.equals(other$gspgSpecs)) {
                        break label120;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                label106: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label106;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label106;
                    }

                    return false;
                }

                Object this$gspgcSeriesId = this.getGspgcSeriesId();
                Object other$gspgcSeriesId = other.getGspgcSeriesId();
                if (this$gspgcSeriesId == null) {
                    if (other$gspgcSeriesId != null) {
                        return false;
                    }
                } else if (!this$gspgcSeriesId.equals(other$gspgcSeriesId)) {
                    return false;
                }

                label92: {
                    Object this$gspgcMemFlag = this.getGspgcMemFlag();
                    Object other$gspgcMemFlag = other.getGspgcMemFlag();
                    if (this$gspgcMemFlag == null) {
                        if (other$gspgcMemFlag == null) {
                            break label92;
                        }
                    } else if (this$gspgcMemFlag.equals(other$gspgcMemFlag)) {
                        break label92;
                    }

                    return false;
                }

                Object this$gspgcInteFlag = this.getGspgcInteFlag();
                Object other$gspgcInteFlag = other.getGspgcInteFlag();
                if (this$gspgcInteFlag == null) {
                    if (other$gspgcInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspgcInteFlag.equals(other$gspgcInteFlag)) {
                    return false;
                }

                Object this$gspgcInteRate = this.getGspgcInteRate();
                Object other$gspgcInteRate = other.getGspgcInteRate();
                if (this$gspgcInteRate == null) {
                    if (other$gspgcInteRate != null) {
                        return false;
                    }
                } else if (!this$gspgcInteRate.equals(other$gspgcInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromGiftCondsOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspgcVoucherId = this.getGspgcVoucherId();
        result = result * 59 + ($gspgcVoucherId == null ? 43 : $gspgcVoucherId.hashCode());
        Object $gspgcSerial = this.getGspgcSerial();
        result = result * 59 + ($gspgcSerial == null ? 43 : $gspgcSerial.hashCode());
        Object $gspgcProId = this.getGspgcProId();
        result = result * 59 + ($gspgcProId == null ? 43 : $gspgcProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gspgcSeriesId = this.getGspgcSeriesId();
        result = result * 59 + ($gspgcSeriesId == null ? 43 : $gspgcSeriesId.hashCode());
        Object $gspgcMemFlag = this.getGspgcMemFlag();
        result = result * 59 + ($gspgcMemFlag == null ? 43 : $gspgcMemFlag.hashCode());
        Object $gspgcInteFlag = this.getGspgcInteFlag();
        result = result * 59 + ($gspgcInteFlag == null ? 43 : $gspgcInteFlag.hashCode());
        Object $gspgcInteRate = this.getGspgcInteRate();
        result = result * 59 + ($gspgcInteRate == null ? 43 : $gspgcInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromGiftCondsOutData(clientId=" + this.getClientId() + ", gspgcVoucherId=" + this.getGspgcVoucherId() + ", gspgcSerial=" + this.getGspgcSerial() + ", gspgcProId=" + this.getGspgcProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gspgcSeriesId=" + this.getGspgcSeriesId() + ", gspgcMemFlag=" + this.getGspgcMemFlag() + ", gspgcInteFlag=" + this.getGspgcInteFlag() + ", gspgcInteRate=" + this.getGspgcInteRate() + ")";
    }
}
