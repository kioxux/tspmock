package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.entity.vo.CallUpAndInStoreTotalVO;
import com.gys.business.entity.vo.CallUpStoreDetailExportVO;
import com.gys.business.entity.vo.CallUpStoreDetailVO;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.GaiaSdMessageService;
import com.gys.business.service.GaiaStoreOutSuggestionHService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.data.suggestion.SuggestionDetailVO;
import com.gys.common.data.suggestion.SuggestionInForm;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.DateUtil;
import com.gys.util.ExcelStyleUtil;
import com.gys.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionH)表服务实现类
 *
 * @author makejava
 * @since 2021-10-28 10:54:04
 */
@Service("gaiaStoreOutSuggestionHService")
@Slf4j
public class GaiaStoreOutSuggestionHServiceImpl implements GaiaStoreOutSuggestionHService {
    @Resource
    private GaiaStoreOutSuggestionHMapper gaiaStoreOutSuggestionHMapper;
    @Resource
    private GaiaStoreOutSuggestionDMapper gaiaStoreOutSuggestionDMapper;
    @Resource
    private GaiaCommodityInventoryHMapper gaiaCommodityInventoryHMapper;
    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;
    @Resource
    private GaiaSaletaskPlanClientStoNewMapper gaiaSaletaskPlanClientStoNewMapper;
    @Resource
    private GaiaStoreOutSuggestionRelationMapper gaiaStoreOutSuggestionRelationMapper;
    @Resource
    private CommonMapper commonMapper;
    @Resource
    private GaiaStoreInSuggestionHMapper gaiaStoreInSuggestionHMapper;
    @Resource
    private GaiaStoreInSuggestionDMapper gaiaStoreInSuggestionDMapper;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaSdMessageService gaiaSdMessageService;

    @Override
    public List<GaiaStoreOutSuggestionDOutData> calculateBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        log.info(String.format("<商品调剂><计算><开始时间：%s>", LocalDateTime.now()));
        GaiaClSystemPara para1 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "OUT_SUGGESTION1");
        String param1 = "week";
        int param2 = 90;
        checkParam1(para1, param1, param2);
        //1.0根据规则查询出所有带匹配的商品信息
        List<GaiaStoreOutSuggestionDOutData> waitGoods = gaiaStoreOutSuggestionHMapper.listWaitGoods(inData);
        //2.0查询门店销售信息
        List<GaiaCommodityInventoryDOut> salesInfos = gaiaCommodityInventoryHMapper.listSalesInfo(inData.getClient(), inData.getStoCode(), param2);
        List<GaiaStoreOutSuggestionDOutData> result = new ArrayList();
        if (!CollectionUtils.isEmpty(waitGoods)) {
            Map<String, List<GaiaStoreOutSuggestionDOutData>> collect = waitGoods.stream().collect(Collectors.groupingBy(GaiaStoreOutSuggestionDOutData::getProSelfCode));
            Set<String> proSelfCodeSet = collect.keySet();
            //匹配商品销售额
            GaiaClSystemPara para4 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "OUT_SUGGESTION4");
            GaiaClSystemPara para2 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "OUT_SUGGESTION2");
            GaiaClSystemPara para3 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "OUT_SUGGESTION3");
            BigDecimal param7 = new BigDecimal("1");
            BigDecimal param8 = new BigDecimal("10");
            checkParam(para4, param7, param8);
            //参数5：商品调库安全库存量（＜成本价界值），默认值：3
            BigDecimal param5 = new BigDecimal("3");
            //参数6：商品调库调库量取值（≥成本价界值），默认值1
            BigDecimal param6 = new BigDecimal("1");
            checkParam(para3, param5, param6);
            //参数3：商品调库调库量成本价界值，默认值：100
            BigDecimal param3 = new BigDecimal("100");
            //参数4：商品调库安全库存量（≥成本价界值），默认值：2
            BigDecimal param4 = new BigDecimal("2");
            checkParam(para2, param3, param4);
            for (String proSelfCode : proSelfCodeSet) {
                //相同商品 不同批号集合
                List<GaiaStoreOutSuggestionDOutData> proInfoList = collect.get(proSelfCode);
                BigDecimal inventoryQty = proInfoList.stream().map(GaiaStoreOutSuggestionDOutData::getInventoryQty).reduce(BigDecimal.ZERO, BigDecimal::add);
                //按照有效期正序排列
                List<GaiaStoreOutSuggestionDOutData> sorted = proInfoList.stream()
                        .sorted(Comparator.comparing(GaiaStoreOutSuggestionDOutData::getInventoryQty, Comparator.reverseOrder()))
                        .sorted(Comparator.comparing(GaiaStoreOutSuggestionDOutData::getValidDay).reversed())
                        .collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(salesInfos)) {

                    Boolean flag = false;
                    for (GaiaCommodityInventoryDOut salesInfo : salesInfos) {

                        if (sorted.get(0).getClient().equals(salesInfo.getClient()) && sorted.get(0).getStoCode().equals(salesInfo.getStoCode())
                                && sorted.get(0).getProSelfCode().equals(salesInfo.getProSelfCode())) {
                            BigDecimal max = BigDecimal.ZERO;
                            //查询商品的 90天销量，90天销售额，90天毛利额，
                            BigDecimal suggestionReturnQty = BigDecimal.ZERO;
                            if (sorted.get(0).getCost().compareTo(param3) >= 0) {

                                if (ObjectUtil.isNotEmpty(salesInfo) && param4.compareTo(salesInfo.getAverageSalesQty() == null ? BigDecimal.ZERO : salesInfo.getAverageSalesQty()) < 0) {
                                    max = new BigDecimal(salesInfo.getAverageSalesQty().toString());
                                } else {
                                    max = param4;
                                }
                                //计算调库量
                                //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                                suggestionReturnQty = inventoryQty.subtract(max).setScale(0, RoundingMode.DOWN);
                                if (suggestionReturnQty.compareTo(param6) >= 0) {
                                    setDetail(param2, result, sorted, salesInfo, suggestionReturnQty, inventoryQty);
                                }
                            }//如果成本价 < 100
                            else {
                                if (ObjectUtil.isNotEmpty(salesInfo) && param5.compareTo(salesInfo.getAverageSalesQty() == null ? BigDecimal.ZERO : salesInfo.getAverageSalesQty()) < 0) {
                                    max = new BigDecimal(salesInfo.getAverageSalesQty().toString());
                                } else {
                                    max = param5;
                                }
                                //计算调库量
                                //计算调库量
                                //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                                suggestionReturnQty = inventoryQty.subtract(max).setScale(0, RoundingMode.DOWN);
                                if (suggestionReturnQty.compareTo(param7) > 0) {
                                    setDetail(param2, result, sorted, salesInfo, suggestionReturnQty, inventoryQty);
                                }
                            }
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        //查询商品的 90天销量，90天销售额，90天毛利额，
                        BigDecimal suggestionReturnQty = BigDecimal.ZERO;
                        if (sorted.get(0).getCost().compareTo(param3) >= 0) {
                            //计算调库量
                            //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                            suggestionReturnQty = inventoryQty.subtract(param4).setScale(0, RoundingMode.DOWN);
                            if (suggestionReturnQty.compareTo(param6) >= 0) {
                                GaiaCommodityInventoryDOut sales = new GaiaCommodityInventoryDOut();
                                setDetail(param2, result, sorted, sales, suggestionReturnQty, inventoryQty);
                            }
                        }//如果成本价 < 100
                        else {
                            //计算调库量
                            //计算调库量
                            //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                            suggestionReturnQty = inventoryQty.subtract(param5).setScale(0, RoundingMode.DOWN);
                            if (suggestionReturnQty.compareTo(param7) > 0) {
                                GaiaCommodityInventoryDOut sales = new GaiaCommodityInventoryDOut();
                                setDetail(param2, result, sorted, sales, suggestionReturnQty, inventoryQty);
                            }
                        }
                    }
                }
            }
        }
        log.info(String.format("<商品调剂><计算><结束时间：%s>", LocalDateTime.now()));
        return result;
    }

    @Override
    @Transactional
    public Long saveOutBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        Date now = new Date();
        if (ObjectUtil.isEmpty(inData.getId())) {
            //新增
            boolean allow = isAllow(loginUser, inData);
            if (!allow) {
                throw new BusinessException("已存在有效且未确认的调剂单！");
            }
            if (CollectionUtils.isEmpty(inData.getInsertList())) {
                throw new BusinessException("请确认调出明细不为空！");
            }
            List<GaiaStoreOutSuggestionDOutData> insertList = inData.getInsertList();
            String billCode = commonMapper.selectNextCode(inData.getClient(), "TCO", 7);
            //插入主表
            GaiaStoreOutSuggestionH suggestionH = new GaiaStoreOutSuggestionH();
            setBillH(loginUser, inData, now, insertList, billCode, suggestionH);
            gaiaStoreOutSuggestionHMapper.insert(suggestionH);
            //插入明细表
            List<GaiaStoreOutSuggestionD> detailList = new ArrayList<>();
            for (GaiaStoreOutSuggestionDOutData detail : insertList) {
                //校验建议调剂量>=0且不大于批次库存量
                BigDecimal suggestionQry = detail.getSuggestionQty();
                if (suggestionQry.compareTo(BigDecimal.ZERO) == -1) {
                    throw new BusinessException("建议调剂量不可为负数！");
                }
                if (suggestionQry.compareTo(detail.getBatchInventoryQty()) == 1) {
                    throw new BusinessException("建议调剂量不可大于批次库存量！");
                }
                GaiaStoreOutSuggestionD suggestionD = new GaiaStoreOutSuggestionD();
                BeanUtils.copyProperties(detail, suggestionD);
                suggestionD.setBillCode(billCode);
                suggestionD.setIsDelete(0);
                suggestionD.setCreateTime(now);
                suggestionD.setUpdateTime(now);
                suggestionD.setCreateUser(loginUser.getUserId() + "-" + loginUser.getLoginName());
                suggestionD.setUpdateUser(loginUser.getUserId() + "-" + loginUser.getLoginName());
                detailList.add(suggestionD);
            }
            gaiaStoreOutSuggestionDMapper.insertBatch(detailList);
            //插入关系表
            if (StringUtils.isEmpty(inData.getStoList())) {
                throw new BusinessException("请依次选择关联门店！");
            }
            List<GaiaStoreOutSuggestionRelation> relationList = new ArrayList<>();
            for (GaiaStoreOutSuggestionRelation relation : inData.getStoList()) {
                setRelation(loginUser, inData, now, billCode, relationList, relation);
            }
            gaiaStoreOutSuggestionRelationMapper.insertBatch(relationList);
            String id = gaiaStoreOutSuggestionHMapper.getId(suggestionH);
            return Long.parseLong(id);
        } else {
            //编辑
            GaiaStoreOutSuggestionH suggestionH = gaiaStoreOutSuggestionHMapper.queryById(inData.getId());
            if (suggestionH.getStatus() > 0) {
                throw new BusinessException("单据状态已变更，请重新进入！");
            }
            if (!CollectionUtils.isEmpty(inData.getStoList())) {
                List<GaiaStoreOutSuggestionRelation> stoList = inData.getStoList();
                for (GaiaStoreOutSuggestionRelation relation : stoList) {
                    relation.setUpdateTime(now);
                    relation.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
                    gaiaStoreOutSuggestionRelationMapper.update(relation);
                }
            }
            if (!CollectionUtils.isEmpty(inData.getInsertList())) {
                List<GaiaStoreOutSuggestionDOutData> updateList = inData.getInsertList();
                for (GaiaStoreOutSuggestionDOutData update : updateList) {
                    GaiaStoreOutSuggestionD suggestionD = new GaiaStoreOutSuggestionD();
                    suggestionD.setClient(update.getClient());
                    suggestionD.setBillCode(suggestionH.getBillCode());
                    suggestionD.setProSelfCode(update.getProSelfCode());
                    suggestionD.setBatchNo(update.getBatchNo());
                    suggestionD.setValidDay(update.getValidDay());
                    suggestionD.setSuggestionQty(update.getSuggestionQty());
                    suggestionD.setUpdateTime(now);
                    suggestionD.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
                    gaiaStoreOutSuggestionDMapper.updateByCode(suggestionD);
                }
            }
            if (!CollectionUtils.isEmpty(inData.getStoList())) {
                List<GaiaStoreOutSuggestionRelation> stoList = inData.getStoList();
                for (GaiaStoreOutSuggestionRelation relation : stoList) {
                    GaiaStoreOutSuggestionRelation update = new GaiaStoreOutSuggestionRelation();
                    update.setSort(relation.getSort());
                    update.setInStoCode(relation.getInStoCode());
                    update.setBillCode(suggestionH.getBillCode());
                    GetStoreInData param = new GetStoreInData();
                    param.setClient(inData.getClient());
                    param.setBrId(relation.getInStoCode());
                    MonthOutDataVo dataVo = gaiaSaletaskPlanClientStoNewMapper.selectStoreName(param);
                    if (ObjectUtil.isEmpty(dataVo)) {
                        throw new BusinessException("未查询到关联门店信息，请重新选择关联门店！");
                    }
                    update.setInStoName(dataVo.getBrName());
                    update.setUpdateTime(now);
                    update.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
                    gaiaStoreOutSuggestionRelationMapper.updateByCode(update);
                }
            }
            return inData.getId();
        }
    }

    private void setRelation(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData, Date now, String billCode, List<GaiaStoreOutSuggestionRelation> relationList, GaiaStoreOutSuggestionRelation relation) {
        GaiaStoreOutSuggestionRelation inset = new GaiaStoreOutSuggestionRelation();
        relation.setClient(inData.getClient());
        relation.setBillCode(billCode);
        relation.setOutStoCode(inData.getStoCode());
        relation.setInStoCode(relation.getInStoCode());
        GetStoreInData param = new GetStoreInData();
        param.setClient(inData.getClient());
        param.setBrId(relation.getInStoCode());
        MonthOutDataVo dataVo = gaiaSaletaskPlanClientStoNewMapper.selectStoreName(param);
        if (ObjectUtil.isEmpty(dataVo)) {
            throw new BusinessException("未查询到关联门店信息，请重新选择关联门店！");
        }
        relation.setInStoName(dataVo.getBrName());
        relation.setSort(relation.getSort());
        relation.setIsDelete(0);
        relation.setCreateTime(now);
        relation.setUpdateTime(now);
        relation.setCreateUser(loginUser.getUserId() + "-" + loginUser.getLoginName());
        relation.setUpdateUser(loginUser.getUserId() + "-" + loginUser.getLoginName());
        relationList.add(relation);
    }

    @Override
    public PageInfo listBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        //获取参数
        int pageNum = inData.getPageNum();
        int pageSize = inData.getPageSize();
        //非空校验 如果为空则赋值默认值
        if (pageNum == 0) {
            pageNum = CommonConstant.PAGE_NUM;
        }
        if (pageSize == 0) {
            pageSize = CommonConstant.PAGE_SIZE;
        }
        //分页参数
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaStoreOutSuggestionDOutData> outData = gaiaStoreOutSuggestionHMapper.listBill(inData);
        PageInfo pageInfo = new PageInfo(outData);
        return pageInfo;
    }

    @Override
    public GaiaStoreOutSuggestionOutData listDetail(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        //1.0查询主表信息
        GaiaStoreOutSuggestionH storeOutSuggestionH = gaiaStoreOutSuggestionHMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(storeOutSuggestionH)) {
            throw new BusinessException("未查询到该单据，请刷新页面");
        }
        inData.setBillCode(storeOutSuggestionH.getBillCode());
        GaiaStoreOutSuggestionOutData result = new GaiaStoreOutSuggestionOutData();
        BeanUtils.copyProperties(storeOutSuggestionH, result);
        //2.0查询关联门店
        List<GaiaStoreOutSuggestionRelationOutData> relationList = gaiaStoreOutSuggestionRelationMapper.listDetail(inData);
        result.setStoreList(relationList);
        //3.0查询单据明细
        List<GaiaStoreOutSuggestionDOutData> detailList = gaiaStoreOutSuggestionDMapper.listDetail(inData);
        result.setDetailList(detailList);
        return result;
    }

    @Override
    @Transactional
    public Object pushToIn(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        //先保存
        Long id = saveOutBill(loginUser, inData);
        inData.setId(id);
        GaiaStoreOutSuggestionH storeOutSuggestionH = gaiaStoreOutSuggestionHMapper.queryById(inData.getId());
        if (storeOutSuggestionH.getStatus() > 0) {
            throw new BusinessException("单据状态已变更，请重新进入！");
        }
        if (ObjectUtil.isEmpty(storeOutSuggestionH)) {
            throw new BusinessException("未查询到该单据，请刷新页面");
        }
        //2.0查询关联门店
        inData.setBillCode(storeOutSuggestionH.getBillCode());
        List<GaiaStoreOutSuggestionRelationOutData> relationList = gaiaStoreOutSuggestionRelationMapper.listDetail(inData);
        if (CollectionUtils.isEmpty(relationList)) {
            throw new BusinessException("请先维护关联门店，再推送！");
        }
        //3.0查询单据明细
        List<GaiaStoreOutSuggestionDOutData> detailList = gaiaStoreOutSuggestionDMapper.listDetail(inData);
        if (CollectionUtils.isEmpty(detailList)) {
            throw new BusinessException("未查询到改单据据明细，无法推送！");
        }
        List<GaiaStoreInSuggestionH> insertH = new ArrayList<>();
        List<GaiaStoreInSuggestionD> insertD = new ArrayList<>();
        Date now = new Date();
        for (GaiaStoreOutSuggestionRelationOutData relation : relationList) {
            //插入 GAIA_STORE_IN_SUGGESTION_H
            GaiaStoreInSuggestionH gaiaStoreInSuggestionH = new GaiaStoreInSuggestionH();
            setInHead(loginUser, inData, storeOutSuggestionH, now, relation, gaiaStoreInSuggestionH);
            insertH.add(gaiaStoreInSuggestionH);
            //查询当前门店所有药品库存
            List<GaiaSdStock> stockList = gaiaStoreInSuggestionDMapper.listProStockQty(inData.getClient(), relation.getInStoCode());
            GaiaClSystemPara para1 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "OUT_SUGGESTION1");
            String param1 = "week";
            int param2 = 90;
            checkParam1(para1, param1, param2);
            //查询90天销售额
            List<GaiaCommodityInventoryDOut> salesInfos = gaiaCommodityInventoryHMapper.listSalesInfo(inData.getClient(), inData.getStoCode(), param2);
            //插入 GAIA_STORE_IN_SUGGESTION_D
            for (GaiaStoreOutSuggestionDOutData detail : detailList) {
                GaiaStoreInSuggestionD gaiaStoreInSuggestionD = new GaiaStoreInSuggestionD();
                setInDetail(loginUser, now, relation, gaiaStoreInSuggestionH, stockList, param2, salesInfos, detail, gaiaStoreInSuggestionD);
                insertD.add(gaiaStoreInSuggestionD);
            }
        }
        gaiaStoreInSuggestionHMapper.insertBatch(insertH);
        gaiaStoreInSuggestionDMapper.insertBatch(insertD);
        //更新调出单 状态为已推送
        storeOutSuggestionH.setStatus(1);
        storeOutSuggestionH.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
        storeOutSuggestionH.setUpdateTime(now);
        gaiaStoreOutSuggestionHMapper.update(storeOutSuggestionH);
        //推送消息
        threadPoolTaskExecutor.execute(() -> gaiaSdMessageService.sendStoreSuggestionMessage(SdMessageTypeEnum.STORE_OUT_SUGGESTION_CONFIRM, insertH, null));
        return true;
    }

    private void setInHead(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData, GaiaStoreOutSuggestionH storeOutSuggestionH, Date now, GaiaStoreOutSuggestionRelationOutData relation, GaiaStoreInSuggestionH gaiaStoreInSuggestionH) {
        gaiaStoreInSuggestionH.setClient(inData.getClient());
        gaiaStoreInSuggestionH.setStoCode(relation.getInStoCode());
        gaiaStoreInSuggestionH.setStoName(relation.getInStoName());
        gaiaStoreInSuggestionH.setOutStoCode(storeOutSuggestionH.getStoCode());
        gaiaStoreInSuggestionH.setOutStoName(storeOutSuggestionH.getStoName());
        String billCode = commonMapper.selectNextCode(inData.getClient(), "TCI", 7);
        gaiaStoreInSuggestionH.setBillCode(billCode);
        gaiaStoreInSuggestionH.setOutBillCode(storeOutSuggestionH.getBillCode());
        gaiaStoreInSuggestionH.setBillDate(now);
        gaiaStoreInSuggestionH.setInvalidDate(storeOutSuggestionH.getInvalidDate());
        gaiaStoreInSuggestionH.setStatus(0);
        gaiaStoreInSuggestionH.setItemsQty(storeOutSuggestionH.getItemsQty());
        gaiaStoreInSuggestionH.setIsDelete(0);
        gaiaStoreInSuggestionH.setCreateTime(now);
        gaiaStoreInSuggestionH.setCreateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
        gaiaStoreInSuggestionH.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
        gaiaStoreInSuggestionH.setUpdateTime(now);
    }

    private void setInDetail(GetLoginOutData loginUser, Date now, GaiaStoreOutSuggestionRelationOutData relation, GaiaStoreInSuggestionH gaiaStoreInSuggestionH, List<GaiaSdStock> stockList, int param2, List<GaiaCommodityInventoryDOut> salesInfos, GaiaStoreOutSuggestionDOutData detail, GaiaStoreInSuggestionD gaiaStoreInSuggestionD) {
        gaiaStoreInSuggestionD.setClient(detail.getClient());
        gaiaStoreInSuggestionD.setStoCode(relation.getInStoCode());
        gaiaStoreInSuggestionD.setStoName(relation.getInStoName());
        gaiaStoreInSuggestionD.setBillCode(gaiaStoreInSuggestionH.getBillCode());
        gaiaStoreInSuggestionD.setProName(detail.getProName());
        gaiaStoreInSuggestionD.setProSelfCode(detail.getProSelfCode());
        gaiaStoreInSuggestionD.setProSpces(detail.getProSpces());
        gaiaStoreInSuggestionD.setProFactoryName(detail.getProFactoryName());
        gaiaStoreInSuggestionD.setProUnit(detail.getProUnit());
        gaiaStoreInSuggestionD.setConfirmQty(BigDecimal.ZERO);
        gaiaStoreInSuggestionD.setFinalQty(BigDecimal.ZERO);
        gaiaStoreInSuggestionD.setSuggestionOutQty(detail.getSuggestionQty());
        gaiaStoreInSuggestionD.setBatchNo(detail.getBatchNo());
        gaiaStoreInSuggestionD.setValidDay(detail.getValidDay());
        gaiaStoreInSuggestionD.setProCompclass(detail.getProCompclass());
        gaiaStoreInSuggestionD.setProClass(detail.getProClassCode());
        gaiaStoreInSuggestionD.setIsDelete(0);
        gaiaStoreInSuggestionD.setCreateTime(now);
        gaiaStoreInSuggestionD.setCreateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
        gaiaStoreInSuggestionD.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
        gaiaStoreInSuggestionD.setUpdateTime(now);
        //匹配库存
        matchStockQty(stockList, detail, gaiaStoreInSuggestionD);
        //匹配销售信息
        matchSalesInfo(param2, salesInfos, detail, gaiaStoreInSuggestionD);
    }

    private void matchSalesInfo(int param2, List<GaiaCommodityInventoryDOut> salesInfos, GaiaStoreOutSuggestionDOutData detail, GaiaStoreInSuggestionD gaiaStoreInSuggestionD) {
        if (CollectionUtils.isEmpty(salesInfos)) {
            gaiaStoreInSuggestionD.setAverageSalesQty(BigDecimal.ZERO);
            gaiaStoreInSuggestionD.setAverageSalesAmt(BigDecimal.ZERO);
        } else {
            gaiaStoreInSuggestionD.setAverageSalesQty(BigDecimal.ZERO);
            gaiaStoreInSuggestionD.setAverageSalesAmt(BigDecimal.ZERO);
            for (GaiaCommodityInventoryDOut sales : salesInfos) {
                //根据 client stoCode proSelfCode 匹配
                if (sales.getClient().equals(detail.getClient()) && sales.getStoCode().equals(gaiaStoreInSuggestionD.getStoCode())
                        && sales.getProSelfCode().equals(gaiaStoreInSuggestionD.getProSelfCode())) {
                    gaiaStoreInSuggestionD.setAverageSalesQty(Util.divide(Util.checkNull(sales.getAverageSalesQty()), new BigDecimal(param2)).multiply(new BigDecimal("30")));
                    gaiaStoreInSuggestionD.setAverageSalesAmt(Util.divide(Util.checkNull(sales.getAverageSalesAmt()), new BigDecimal(param2)).multiply(new BigDecimal("30")));
                    break;
                }
            }
        }
    }

    private void matchStockQty(List<GaiaSdStock> stockList, GaiaStoreOutSuggestionDOutData detail, GaiaStoreInSuggestionD gaiaStoreInSuggestionD) {
        if (CollectionUtils.isEmpty(stockList)) {
            gaiaStoreInSuggestionD.setInventoryQty(BigDecimal.ZERO);
            gaiaStoreInSuggestionD.setAverageSalesQty(BigDecimal.ZERO);
            gaiaStoreInSuggestionD.setAverageSalesAmt(BigDecimal.ZERO);
        } else {
            gaiaStoreInSuggestionD.setInventoryQty(BigDecimal.ZERO);
            for (GaiaSdStock gaiaSdStock : stockList) {
                //根据 client stoCode proSelfCode 匹配
                if (gaiaSdStock.getClientId().equals(detail.getClient()) && gaiaSdStock.getGssBrId().equals(gaiaStoreInSuggestionD.getStoCode())
                        && gaiaSdStock.getGssProId().equals(gaiaStoreInSuggestionD.getProSelfCode())) {
                    gaiaStoreInSuggestionD.setInventoryQty(new BigDecimal(gaiaSdStock.getGssQty()));
                    break;
                }
            }
        }
    }

    @Override
    public GaiaClSystemPara listParam(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        GaiaClSystemPara para1 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "ADJUSTMENT_PARAMETER");
        if (ObjectUtil.isEmpty(para1)) {
            GaiaClSystemPara systemPara = new GaiaClSystemPara();
            systemPara.setClient(inData.getClient());
            systemPara.setGcspId("ADJUSTMENT_PARAMETER");
            systemPara.setGcspName("门店调剂-关联门店参数");
            systemPara.setGcspParaRemark("param1 为关联调剂门店数");
            systemPara.setGcspPara1("3");
            gaiaClSystemParaMapper.insert(systemPara);
            return systemPara;
        } else {
            return para1;
        }
    }

    @Override
    public Boolean isAllow(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        int allow = gaiaStoreOutSuggestionHMapper.isAllow(inData);
        return allow > 0 ? false : true;
    }

    @Override
    public List<GaiaStoreOutSuggestionRelationOutData> listSto(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        GaiaStoreOutSuggestionH suggestionH = gaiaStoreOutSuggestionHMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(suggestionH)) {
            throw new BusinessException("未查询到改单据，请刷新页面！");
        }
        inData.setBillCode(suggestionH.getBillCode());
        return gaiaStoreOutSuggestionRelationMapper.listDetail(inData);
    }

    @Override
    public GaiaStoreOutSuggestionOutData listDetailForConfirm(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        List<GaiaStoreOutSuggestionRelationOutData> relationList = listSto(loginUser, inData);
        if (CollectionUtils.isEmpty(relationList)) {
            throw new BusinessException("未查询到该单据的关联门店，请刷新页面！");
        }
        GaiaStoreOutSuggestionH storeOutSuggestionH = gaiaStoreOutSuggestionHMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(storeOutSuggestionH)) {
            throw new BusinessException("未查询到该单据，请刷新页面");
        }
        inData.setBillCode(storeOutSuggestionH.getBillCode());
        GaiaStoreOutSuggestionOutData result = new GaiaStoreOutSuggestionOutData();
        BeanUtils.copyProperties(storeOutSuggestionH, result);
        List<GaiaStoreOutSuggestionDOutData> detailList = gaiaStoreOutSuggestionDMapper.listDetail(inData);
        if (!CollectionUtils.isEmpty(detailList)) {
            List<JSONObject> returnDetailList = new ArrayList<>();
            detailList.stream().forEach(
                    item -> returnDetailList.add(JSON.parseObject(JSON.toJSON(item).toString()))
            );
            for (GaiaStoreOutSuggestionRelationOutData relation : relationList) {
                SuggestionInForm suggestionD = new SuggestionInForm();
                suggestionD.setClient(inData.getClient());
                suggestionD.setOutBillCode(relation.getBillCode());
                suggestionD.setStoCode(relation.getInStoCode());
                List<SuggestionDetailVO> list = gaiaStoreInSuggestionDMapper.findVOList(suggestionD);
                if (!CollectionUtils.isEmpty(list)) {
                    for (JSONObject suggestionDOutData : returnDetailList) {
                        for (SuggestionDetailVO inSuggestionD : list) {
                            if (suggestionDOutData.get("client").toString().equals(inSuggestionD.getClient()) &&
                                    suggestionDOutData.get("stoCode").toString().equals(inSuggestionD.getOutStoCode()) &&
                                    suggestionDOutData.get("proSelfCode").toString().equals(inSuggestionD.getProSelfCode()) &&
                                    suggestionDOutData.get("batchNo").toString().equals(inSuggestionD.getBatchNo())) {
                                suggestionDOutData.put(relation.getInStoCode(), storeOutSuggestionH.getStatus() == 3 ?
                                        Util.checkNull(inSuggestionD.getFinalQty().setScale(0, BigDecimal.ROUND_DOWN))
                                        : Util.checkNull(inSuggestionD.getConfirmQty().setScale(0, BigDecimal.ROUND_DOWN)));
                                break;
                            }
                        }
                    }
                }
            }
            result.setConfirmDetailList(returnDetailList);
        }
        return result;
    }

    @Override
    @Transactional
    public Object confirmBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData) {
        Date now = new Date();
        List<HashMap<String, Object>> confirmList = inData.getConfirmDetailList();
        //0.0查询调出单主表
        GaiaStoreOutSuggestionH outSuggestionH = gaiaStoreOutSuggestionHMapper.queryById(inData.getId());
        if (outSuggestionH.getStatus() > 2) {
            throw new BusinessException("该单据已审核或已失效！请刷新页面！");
        }
        //1.0查询关联门店
        List<GaiaStoreOutSuggestionRelationOutData> relationOutData = gaiaStoreOutSuggestionRelationMapper.listDetail(inData);
        List<GaiaStoreOutSuggestionRelationOutData> collect = relationOutData.stream().filter(item -> 1 == item.getStatus()).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(collect)) {
            throw new BusinessException("该单据没有任何关联门店确认单据！请等待关联门店确认！");
        }

        int i = 1;
        for (GaiaStoreOutSuggestionRelationOutData relation : collect) {
            //查询调入单号
            GaiaStoreInSuggestionH inHParam = new GaiaStoreInSuggestionH();
            inHParam.setClient(inData.getClient());
            inHParam.setStoCode(relation.getInStoCode());
            inHParam.setOutStoCode(relation.getOutStoCode());
            inHParam.setOutBillCode(outSuggestionH.getBillCode());
            GaiaStoreInSuggestionH inSuggestionH = gaiaStoreInSuggestionHMapper.getUnique(inHParam);
            for (HashMap<String, Object> map : confirmList) {
                BigDecimal finalQty = getFinalQty(relation, i, map);
                //1.0更新  in表的最终确认量
                GaiaStoreInSuggestionD suggestionD = new GaiaStoreInSuggestionD();
                suggestionD.setClient(loginUser.getClient());
                suggestionD.setBillCode(inSuggestionH.getBillCode());
                suggestionD.setProSelfCode(map.get("proSelfCode").toString());
                suggestionD.setBatchNo(map.get("batchNo").toString());
                suggestionD.setStoCode(relation.getInStoCode());
                suggestionD.setFinalQty(finalQty);
                suggestionD.setUpdateTime(now);
                suggestionD.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
                gaiaStoreInSuggestionDMapper.updateByCode(suggestionD);
            }
            //更新调入单主表状态
            inSuggestionH.setStatus(3);
            inSuggestionH.setFinishTime(now);
            inSuggestionH.setUpdateTime(now);
            inSuggestionH.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
            gaiaStoreInSuggestionHMapper.update(inSuggestionH);
            i++;
        }
        //2.0 更新调出单主表状态
        outSuggestionH.setStatus(3);
        outSuggestionH.setFinishTime(now);
        outSuggestionH.setUpdateTime(now);
        outSuggestionH.setUpdateUser(loginUser.getLoginName() + "-" + loginUser.getUserId());
        gaiaStoreOutSuggestionHMapper.update(outSuggestionH);
        return true;
    }

    private BigDecimal getFinalQty(GaiaStoreOutSuggestionRelationOutData relation, int i, HashMap<String, Object> map) {
        //献给第一家门店，第一家多了再给第二家
        BigDecimal lastQty = BigDecimal.ZERO;
        BigDecimal finalQty = BigDecimal.ZERO;
        if (i == 1) {
            lastQty = Util.checkNull(new BigDecimal(map.get("suggestionQty").toString())).subtract(Util.checkNull(new BigDecimal(map.get(relation.getInStoCode()).toString())));
            finalQty = Util.checkNull(new BigDecimal(map.get(relation.getInStoCode()).toString()));
            map.put("lastQty", lastQty);
        } else {
            lastQty = new BigDecimal(map.get("lastQty").toString());
            if (lastQty.compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal subtract = lastQty.subtract(new BigDecimal(map.get(relation.getInStoCode()).toString()));
                if (subtract.compareTo(BigDecimal.ZERO) >= 0) {
                    finalQty = new BigDecimal(map.get(relation.getInStoCode()).toString());
                    map.put("lastQty", subtract);
                } else {
                    finalQty = lastQty;
                    map.put("lastQty", BigDecimal.ZERO);
                }
            } else {
                finalQty = BigDecimal.ZERO;
                map.put("lastQty", BigDecimal.ZERO);
            }
        }
        return finalQty;
    }

    @Override
    public void validBill() {
        List<GaiaStoreOutSuggestionH> outList = gaiaStoreOutSuggestionHMapper.listInvalidBill();
        Date now = new Date();
        if (!CollectionUtils.isEmpty(outList)) {
            for (GaiaStoreOutSuggestionH outSuggestionH : outList) {
                outSuggestionH.setFinishTime(now);
                outSuggestionH.setStatus(4);
                gaiaStoreOutSuggestionHMapper.update(outSuggestionH);
            }
        }
        List<GaiaStoreInSuggestionH> inList = gaiaStoreInSuggestionHMapper.listInvalidBill();
        if (!CollectionUtils.isEmpty(inList)) {
            for (GaiaStoreInSuggestionH inSuggestionH : inList) {
                inSuggestionH.setFinishTime(now);
                inSuggestionH.setStatus(2);
                gaiaStoreInSuggestionHMapper.update(inSuggestionH);
            }
        }
    }

    @Override
    public PageInfo queryDetail(CallUpOrInStoreDetailDTO storeDetailDTO) {
        //校验参数
        if (StrUtil.isAllBlank(storeDetailDTO.getFinishEndDate(), storeDetailDTO.getFinishStartDate()
                , storeDetailDTO.getSuggestedEndDate(), storeDetailDTO.getSuggestedStartDate())) {
            throw new BusinessException("日期不能为空");
        }
        List<CallUpStoreDetailVO> detailVOS = this.gaiaStoreOutSuggestionHMapper.queryDetail(storeDetailDTO);
        //统计计算
        if(CollectionUtil.isNotEmpty(detailVOS)){
            //品项数
            BigDecimal numberOfStoreOUtTotal = BigDecimal.ZERO;
            //实际品项数
            BigDecimal actualAmountTotal = BigDecimal.ZERO;
            for (CallUpStoreDetailVO detailVO : detailVOS) {
               numberOfStoreOUtTotal= numberOfStoreOUtTotal.add(detailVO.getNumberOfStoreOUt());
               actualAmountTotal=actualAmountTotal.add(detailVO.getActualAmount());
            }
            String fillRate = CommonUtil.divideFormat(actualAmountTotal, numberOfStoreOUtTotal, "00.00%");
            CallUpAndInStoreTotalVO totalVO = new CallUpAndInStoreTotalVO();
            totalVO.setNumberOfStoreOUt(numberOfStoreOUtTotal);
            totalVO.setActualAmount(actualAmountTotal);
            totalVO.setFillRate(fillRate);
            return  new com.gys.common.data.PageInfo(detailVOS,totalVO) ;
        }

        return new com.gys.common.data.PageInfo(detailVOS);
    }

    @Override
    public List<String> queryType() {
        return this.gaiaStoreOutSuggestionHMapper.queryType();
    }

    @Override
    public void queryDetailExport(HttpServletResponse response ,CallUpOrInStoreDetailDTO storeDetailDTO) {
        //校验参数
        if (StrUtil.isAllBlank(storeDetailDTO.getFinishEndDate(), storeDetailDTO.getFinishStartDate()
                , storeDetailDTO.getSuggestedEndDate(), storeDetailDTO.getSuggestedStartDate())) {
            throw new BusinessException("日期不能为空");
        }
        List<CallUpStoreDetailExportVO> detailVOS = this.gaiaStoreOutSuggestionHMapper.queryDetailExport(storeDetailDTO);
        List<CallUpStoreDetailExportVO> list = null;
        if(CollectionUtil.isNotEmpty(detailVOS)) {
            list = new ArrayList<>();
            for (CallUpStoreDetailExportVO detailVO : detailVOS) {
                CallUpStoreDetailExportVO callUpStoreDetailExportVO = this.splitStr(detailVO);
                list.add(callUpStoreDetailExportVO);
            }
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = "调出门店明细表";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        try {
            EasyExcel.write(response.getOutputStream(),CallUpStoreDetailExportVO.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CallUpStoreDetailExportVO splitStr(CallUpStoreDetailExportVO exportVO){
        if(StrUtil.isBlank(exportVO.getAssociatedStores())){
            return exportVO;
        }
        String[] split = exportVO.getAssociatedStores().split(",");
        switch (split.length){
            case 1 :
                exportVO.setAssociatedStoreOne(split[0]);
                break;
            case 2 :
                exportVO.setAssociatedStoreOne(split[0]);
                exportVO.setAssociatedStoreTwo(split[1]);
                break;
            case 3 :
                exportVO.setAssociatedStoreOne(split[0]);
                exportVO.setAssociatedStoreTwo(split[1]);
                exportVO.setAssociatedStoreThree(split[2]);
               break;
        }
        return exportVO;
    }

    @Override
    public List<Map<String,String>> queryBillCode(String startDate,String endDate) {
        return this.gaiaStoreOutSuggestionHMapper.queryBillCode(startDate,endDate);
    }

    private void setBillH(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData, Date now, List<GaiaStoreOutSuggestionDOutData> insertList, String billCode, GaiaStoreOutSuggestionH suggestionH) {
        suggestionH.setClient(inData.getClient());
        suggestionH.setStoCode(inData.getStoCode());
        suggestionH.setStoName(inData.getStoName());
        suggestionH.setBillCode(billCode);
        suggestionH.setBillDate(now);
        String invalidDay = "";
        BigDecimal param7 = BigDecimal.ONE;
        BigDecimal param8 = BigDecimal.TEN;
        GaiaClSystemPara para4 = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClient(), "COMMODITY_PARAM4");
        checkParam(para4, param7, param8);
        if ("week".equals(inData.getType())) {
            invalidDay = DateUtil.addDay(now, "yyyy-MM-dd", 5);
        } else {
            invalidDay = DateUtil.addDay(now, "yyyy-MM-dd", param8.intValue());
        }
        suggestionH.setInvalidDate(DateUtil.stringToDate(invalidDay, "yyyy-MM-dd"));
        suggestionH.setStatus(0);
        suggestionH.setType(inData.getType());
        Set<String> proSelfCodeSet = insertList.stream().map(GaiaStoreOutSuggestionDOutData::getProSelfCode).collect(Collectors.toSet());
        suggestionH.setItemsQty(proSelfCodeSet.size());
        suggestionH.setIsDelete(0);
        suggestionH.setCreateTime(now);
        suggestionH.setCreateUser(loginUser.getUserId() + "-" + loginUser.getLoginName());
        suggestionH.setUpdateTime(now);
        suggestionH.setUpdateUser(loginUser.getUserId() + "-" + loginUser.getLoginName());
    }

    private void setDetail(int param2, List<GaiaStoreOutSuggestionDOutData> result, List<GaiaStoreOutSuggestionDOutData> sorted, GaiaCommodityInventoryDOut salesInfo, BigDecimal suggestionReturnQty, BigDecimal inventoryQty) {
        for (GaiaStoreOutSuggestionDOutData waitIn : sorted) {
            BigDecimal confirmQty;
            if (BigDecimal.ZERO.compareTo(suggestionReturnQty) == 0) {
                break;
            }
            if (suggestionReturnQty.compareTo(waitIn.getInventoryQty()) > 0) {
                suggestionReturnQty = suggestionReturnQty.subtract(waitIn.getInventoryQty());
                confirmQty = waitIn.getInventoryQty();
            } else {
                confirmQty = suggestionReturnQty;
                suggestionReturnQty = BigDecimal.ZERO;
            }
            GaiaStoreOutSuggestionDOutData suggestionD = new GaiaStoreOutSuggestionDOutData();
            suggestionD.setClient(waitIn.getClient());
            suggestionD.setProName(waitIn.getProName());
            suggestionD.setProSelfCode(waitIn.getProSelfCode());
            suggestionD.setProSpces(waitIn.getProSpces());
            suggestionD.setProFactoryName(waitIn.getProFactoryName());
            suggestionD.setProUnit(waitIn.getProUnit());
            suggestionD.setAverageSalesQty(Util.divide(Util.checkNull(salesInfo.getAverageSalesQty()), new BigDecimal(param2)).multiply(new BigDecimal("30")));
            suggestionD.setInventoryQty(inventoryQty);
            suggestionD.setBatchInventoryQty(waitIn.getInventoryQty());
            suggestionD.setSuggestionQty(confirmQty.setScale(0, BigDecimal.ROUND_DOWN));
            suggestionD.setBatchNo(waitIn.getBatchNo());
            suggestionD.setValidDay(waitIn.getValidDay());
            suggestionD.setAverageSalesAmt(Util.divide(Util.checkNull(salesInfo.getAverageSalesAmt()), new BigDecimal(param2)).multiply(new BigDecimal("30")));
            suggestionD.setProCompclass(waitIn.getProCompclass());
            suggestionD.setProClass(waitIn.getProClass());
            suggestionD.setProClassName(waitIn.getProClassName());
            suggestionD.setBigClass(waitIn.getBigClass());
            suggestionD.setBigClassName(waitIn.getBigClassName());
            result.add(suggestionD);
        }
    }

    private void checkParam1(GaiaClSystemPara para2, String param1, int param2) {
        if (ObjectUtil.isNotEmpty(para2) && ObjectUtil.isNotEmpty(para2.getGcspPara1())) {
            param1 = para2.getGcspPara1();
        }
        if (ObjectUtil.isNotEmpty(para2) && ObjectUtil.isNotEmpty(para2.getGcspPara2())) {
            param2 = Integer.valueOf(para2.getGcspPara2());
        }
    }

    private void checkParam(GaiaClSystemPara para2, BigDecimal param3, BigDecimal param4) {
        if (ObjectUtil.isNotEmpty(para2) && ObjectUtil.isNotEmpty(para2.getGcspPara1())) {
            param3 = new BigDecimal(para2.getGcspPara1());
        }
        if (ObjectUtil.isNotEmpty(para2) && ObjectUtil.isNotEmpty(para2.getGcspPara2())) {
            param4 = new BigDecimal(para2.getGcspPara2());
        }
    }

}
