package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2021/3/18
 */
@Data
public class AppAmountVo implements Serializable {
    private static final long serialVersionUID = 668032958739771911L;


    @ApiModelProperty(value = "传参集合")
    @Valid
    @NotNull(message = "商品信息不可为空!")
    private List<AppAmountCalculationVo> voList;

    @ApiModelProperty(value = "异动商品集合")
    private List<AppAmountCalculationVo> changeList;

    @ApiModelProperty(value = "是否下单 : ture :下单 / false : 不下单")
    private boolean flag ;

    @ApiModelProperty(value = "实收金额")
    private String actualAmount;

    @ApiModelProperty(value = "优惠减(折让金额)")
    private String discountedPrice;

    @ApiModelProperty(value = "门店号")
    private String brId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    private String memberId;

    @ApiModelProperty(value = "处方登记信息")
    private RecipelInfoInputData inputData;

    @ApiModelProperty(value = "选择的促销")
    private AppPromData selectedProm;

}
