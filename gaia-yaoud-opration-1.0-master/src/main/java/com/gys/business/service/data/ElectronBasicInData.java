package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 *
 * @author xiaoyuan on 2020/9/25
 */
@Data
public class ElectronBasicInData implements Serializable {
    private static final long serialVersionUID = 7394912740787462849L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "电子券活动号")
    private String gsebId;

    @ApiModelProperty(value = "电子券描述")
    private String gsebName;

    @ApiModelProperty(value = "面值")
    private String gsebAmt;

    @ApiModelProperty(value = "y:是  / n :否")
    private String gsebStatus;

    @ApiModelProperty(value = "电子券主题号")
    private String gsetsId;

}
