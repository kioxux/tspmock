package com.gys.business.service.appservice.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.controller.app.form.SalesChangeStatisticForm;
import com.gys.business.controller.app.vo.SalesChangeStatisticVO;
import com.gys.business.mapper.SalesChangeStatisticMapper;
import com.gys.business.service.appservice.SalesChangeStatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author jiht
 * @Description 成分销售变化TOP排名
 * @Date 2021/12/3 14:28
 **/
@Service
@Slf4j
public class SalesChangeStatisticServiceImpl implements SalesChangeStatisticService {

    @Autowired
    private SalesChangeStatisticMapper salesChangeStatisticMapper;

    @Override
    public List<SalesChangeStatisticVO> salesChangeStatistic(SalesChangeStatisticForm inData) {
        /*// 查询商品成分属性
        List<SalesChangeStatisticVO> productBusinessList = salesChangeStatisticMapper.queryProductBusinessInfo(inData);
        Map<String, String> productBusinessMap = productBusinessList.stream().collect(Collectors.toMap(o -> o.getProSelfCode(), v -> v.getComponentName(), (a, b) -> a));
        // 查询交易数据
        List<SalesChangeStatisticVO> saleDataList = salesChangeStatisticMapper.querySaleData(inData);
        // 置上成分名称
        saleDataList.stream().forEach(o -> {
            o.setComponentName(productBusinessMap.get(o.getProSelfCode()));
        });

        // 根据成分汇总
        List<SalesChangeStatisticVO> sumList = new ArrayList<>();
        saleDataList.stream().collect(Collectors.groupingBy(SalesChangeStatisticVO::getComponentName)).forEach((k, v) -> {
            log.info("成分：{}，合并计算开始",k);
            BigDecimal amtChange = BigDecimal.ZERO;
            BigDecimal grossProfitChange = BigDecimal.ZERO;
            Set<String> moveItems = new HashSet<>();
            Optional<SalesChangeStatisticVO> sum = v.stream().reduce((v1, v2) -> {  //合并
                if (Integer.parseInt(v2.getGssdDate()) >= Integer.parseInt(inData.getStartDate()) && Integer.parseInt(v2.getGssdDate()) <= Integer.parseInt(inData.getEndDate())) {
                    // 查询区间数据
                    v1.setAmt(v1.getAmt().add(v2.getAmt()));
                    v1.setGrossProfit(v1.getGrossProfit().add(v2.getGrossProfit()));
                    v1.setGrossMargin(v1.getAmt().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : v1.getGrossProfit().divide(v1.getAmt(),4, BigDecimal.ROUND_DOWN));
                    v1.setAmtChange(amtChange.add(v2.getAmt()));
                    v1.setGrossProfitChange(grossProfitChange.add(v2.getGrossProfit()));
                    moveItems.add(v2.getProSelfCode());
                    v1.setMovableSalesItems(moveItems.size());
                } else {
                    // 对比区间数据
                    v1.setAmtChange(amtChange.subtract(v2.getAmt()));
                    v1.setGrossProfitChange(grossProfitChange.subtract(v2.getGrossProfit()));
                }
                return v1;
            });
            sumList.add(sum.orElse(null));
        });

        // 排序
        List<SalesChangeStatisticVO> resultList = null;
        if ("1".equals(inData.getStatisticType()) && "1".equals(inData.getStatisticMode())) {// 销售额,增长
            resultList = sumList.stream().filter(o -> o.getAmtChange().compareTo(BigDecimal.ZERO) > 0).sorted(Comparator.comparing(SalesChangeStatisticVO::getAmtChange).reversed()).limit(Long.parseLong(inData.getStatisticCount())).collect(Collectors.toList());
        } else if ("1".equals(inData.getStatisticType()) && "2".equals(inData.getStatisticMode())) {// 销售额,下降
            resultList = sumList.stream().filter(o -> o.getAmtChange().compareTo(BigDecimal.ZERO) < 0).sorted(Comparator.comparing(SalesChangeStatisticVO::getAmtChange)).limit(Long.parseLong(inData.getStatisticCount())).collect(Collectors.toList());
        } else if ("2".equals(inData.getStatisticType()) && "1".equals(inData.getStatisticMode())) {//毛利额,增长
            resultList = sumList.stream().filter(o -> o.getGrossProfitChange().compareTo(BigDecimal.ZERO) > 0).sorted(Comparator.comparing(SalesChangeStatisticVO::getGrossProfitChange).reversed()).limit(Long.parseLong(inData.getStatisticCount())).collect(Collectors.toList());
        } else if ("2".equals(inData.getStatisticType()) && "2".equals(inData.getStatisticMode())) {//毛利额,下降
            resultList = sumList.stream().filter(o -> o.getGrossProfitChange().compareTo(BigDecimal.ZERO) < 0).sorted(Comparator.comparing(SalesChangeStatisticVO::getGrossProfitChange)).limit(Long.parseLong(inData.getStatisticCount())).collect(Collectors.toList());
        }

        return resultList;*/

        // 查询销售额/毛利额变化排序数据
        List<SalesChangeStatisticVO> salesChangeStatisticVOList = salesChangeStatisticMapper.salesChangeStatisticTop(inData);
        if (ObjectUtil.isEmpty(salesChangeStatisticVOList)) {
            return new ArrayList<>();
        }

        return salesChangeStatisticVOList;
    }


}