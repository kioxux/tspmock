package com.gys.business.service.yaoshibang;

import com.gys.business.mapper.entity.GaiaSoBatchH;
import com.gys.business.service.data.yaoshibang.CustomerDto;
import com.gys.business.service.data.yaoshibang.OrderDetailDto;
import com.gys.business.service.data.yaoshibang.OrderQueryBean;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/7 11:01
 */
public interface YaoshibangService {

    void orderSync();

    PageInfo<GaiaSoBatchH> orderQuery(OrderQueryBean bean);

    List<OrderDetailDto> orderDetail(Long id, String client);

    List<CustomerDto> customerQuery(String name, String client);

    void orderCreate(Long id, GetLoginOutData userInfo);

    void compensate(String customerName, String customerId, String client);

    void priceSync();

    void stockSync();
}
