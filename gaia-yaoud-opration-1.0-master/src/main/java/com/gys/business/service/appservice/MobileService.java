package com.gys.business.service.appservice;

import com.gys.business.service.data.GaiaStoreOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * @author xiaoyuan on 2021/3/9
 */
public interface MobileService {

    /**
     * 获取门店信息
     * @param userInfo
     * @return
     */
    List<GaiaStoreOutData> getStoreList(GetLoginOutData userInfo);
}
