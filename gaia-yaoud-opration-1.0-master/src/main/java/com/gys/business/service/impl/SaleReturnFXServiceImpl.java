package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.TableMap;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.*;
import com.gys.business.service.data.*;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.DataSubServer;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.MQReceiveData;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.feign.PurchaseService;
import com.gys.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.groupingBy;

@Slf4j
@SuppressWarnings("all")
@Service
public class SaleReturnFXServiceImpl implements SaleReturnFXService {

    @Autowired
    private GaiaSdSaleHMapper saleHMapper;

    @Autowired
    private GaiaSdSaleDMapper saleDMapper;

    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;

    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;

    @Autowired
    private GaiaProductBasicMapper productBasicMapper;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private GaiaSdExamineDMapper examineDMapper;

    @Autowired
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Autowired
    private GaiaSdStockMapper stockMapper;

    @Autowired
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Autowired
    private GaiaSdSalePayMsgMapper payMsgMapper;

    @Autowired
    private GaiaSdPaymentMethodMapper paymentMethodMapper;

    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;

    @Autowired
    private GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;

    @Autowired
    private GaiaSdPaymentMethodMapper methodMapper;

    @Autowired
    private CacheService synchronizedService;

    @Autowired
    private RabbitTemplateHelper rabbitTemplate;

    @Autowired
    private GaiaMaterialAssessMapper assessMapper;

    @Autowired
    private GaiaSdRechargeCardMapper gaiaSdRechargeCardMapper;

    @Autowired
    private GaiaTAccountMapper gaiaTAccountMapper;

    @Autowired
    private BaseService baseService;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Autowired
    private StockService stockService;

    @Autowired
    private TakeAwayService takeAwayService;

    @Autowired
    private ThirdPayService thirdPayService;

    @Autowired
    private RechargeChangeMapper rechargeChangeMapper;

    @Autowired
    private GaiaSdIntegralRateSetMapper gaiaSdIntegralRateSetMapper;

    @Autowired
    private GaiaSdSaleHandHMapper gaiaSdSaleHandHMapper;

    @Autowired
    private GaiaSdSaleHandDMapper gaiaSdSaleHandDMapper;

    @Autowired
    private GaiaSdPrintSaleMapper gaiaSdPrintSaleMapper;

    @Autowired
    private GaiaSdIntegralChangeMapper integralChangeMapper;

    @Autowired
    private GaiaWmsHudiaoZMapper gaiaWmsHudiaoZMapper;

    @Autowired
    private GaiaSdSaleHLocalMapper saleHLocalMapper;

    @Autowired
    private GaiaSdSaleDLocalMapper saleDLocalMapper;

    @Autowired
    private GaiaSdSalePayMsgLocalMapper salePayMsgLocalMapper;

    @Autowired
    private GaiaSdBatchChangeLocalMapper batchChangeLocalMapper;

    @Autowired
    private GaiaSdIntegralExchangeSetHeadService gaiaSdIntegralExchangeSetHeadService;

    @Autowired
    private GaiaSdElectronChangeMapper gaiaSdElectronChangeMapper;

    @Autowired
    private PayService payService;

    @Autowired
    private GaiaHideCommonRemarkMapper gaiaHideCommonRemarkMapper;

    @Override
    public List<SalesInquireOutData> getSalesInquireList(SalesInquireInData inData) {
        List<SalesInquireOutData> outData = this.saleHMapper.getSalesInquireList(inData);
        List<String> payMethodList = new ArrayList<>();
        if (CollUtil.isNotEmpty(outData)) {
            outData.forEach(datum -> datum.setSalesDate(DateUtil.format(DateUtil.parse(datum.getSalesDate()), DatePattern.NORM_DATETIME_PATTERN)));
            IntStream.range(0, outData.size()).forEach(i -> ((SalesInquireOutData) outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }
    @Override
    public boolean updateSalesInquire(String client, String depId, List<String> billNo) {
        boolean bool = false;
        try{
            this.saleHMapper.updateSalesInquire( client,  depId,   billNo);
            bool = true;
        }catch (Exception e){
            bool = false;
        }
        return bool;
    }

    @Override
    public SaleReturnAllData detail(SalesInquireData inData) {
        SaleReturnAllData returnAllData = new SaleReturnAllData();
        List<GetSaleReturnToData> toDataList = this.saleHMapper.detail(inData);
        List<PaymentInformationOutData> information = this.payMsgMapper.getPayDetail(inData);
        if (CollUtil.isEmpty(toDataList)) {
            throw new BusinessException(UtilMessage.INCORRECT_SALES_ORDER_NUMBER);
        }
        toDataList.forEach(toData -> {
            toData.setGssdPrc2(NumberUtil.toStr(NumberUtil.parseNumber(toData.getGssdPrc2())));
            toData.setGssdPrc1(NumberUtil.toStr(NumberUtil.parseNumber(toData.getGssdPrc1())));
            toData.setGssdQty(NumberUtil.toStr(NumberUtil.parseNumber(toData.getGssdQty())));
        });
        IntStream.range(0, toDataList.size()).forEach(i -> ((GetSaleReturnToData) toDataList.get(i)).setIndex(i + 1));
        IntStream.range(0, information.size()).forEach(i -> ((PaymentInformationOutData) information.get(i)).setIndex(i + 1));
        returnAllData.setReturnToData(toDataList);
        returnAllData.setPayOutData(information);
        return returnAllData;
    }

    /**
     * 退货保存
     *
     * @param inData
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> returnAndSave2(SalesInquireData inData) {
        GaiaSdSaleH sd = this.saleHMapper.selectByBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        if (ObjectUtil.isEmpty(sd)) {
            throw new BusinessException(UtilMessage.SALES_SLIP_ABNORMAL);
        }
        //判断是否已经退货
        GaiaSdSaleH sdSaleH = this.saleHMapper.getSaleHByReturnBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        if (ObjectUtil.isNotEmpty(sdSaleH)) {
            throw new BusinessException(UtilMessage.SALE_RETURN_HAVE);
        }
        //查询该单据是否有 送出电子券 且判断是否可以退货。
        List<GaiaSdElectronChange> electronChangeList = gaiaSdElectronChangeMapper.listElectronChange(inData.getClientId(), inData.getBillNo());
        if (!CollectionUtils.isEmpty(electronChangeList)) {
            //使用过的电子券 有使用过 且 未退货的单据
            List<GaiaSdElectronChange> userdElectronList = electronChangeList.stream().filter(item -> "Y".equals(item.getGsecStatus()))
                    .filter(item -> StringUtil.isBlank(item.getReBillNo())).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(userdElectronList)) {
                //送出的电子券 有使用过 且 未退货的单据
                Map<String, List<GaiaSdElectronChange>> usesMap = userdElectronList.stream().collect(groupingBy(GaiaSdElectronChange::getGsecBillNo));
                Set<String> billNoSet = usesMap.keySet();
                //根据销售单号 查询这些单据中 退货
                String meg = "此单送出的电子券已被使用，如需退货请将使用电子券的销售单同时退货！使用电子券的";
                for (String billNo : billNoSet) {
                    List<GaiaSdElectronChange> changeList1 = usesMap.get(billNo);
                    meg = meg + "门店：" + changeList1.get(0).getStoName() + "，日期为：" + changeList1.get(0).getGsecSaleDate() + "，销售单号为：" + changeList1.get(0).getGsecBillNo() + ";";
                }
                throw new BusinessException(meg);
            } else {
                //送出的电子券 未使用过
                //作废赠送的电子券
                electronChangeList.forEach(item -> item.setGsecStatus("F"));
                gaiaSdElectronChangeMapper.updateStatus(electronChangeList);
            }
        }

        List<MaterialDocRequestDto> listDto = new ArrayList<>();//物料凭证
        //生成的退货单号
        String billNo = inData.getBillNoReturn();
        String guid = UUID.randomUUID().toString();

        //收银通退货
//        for (PaymentInformationOutData information : inData.getPayOutData()) {
//            if ("2001".equals(information.getGsspmId())) {
//                GetLoginOutData userInfo = new GetLoginOutData();
//                userInfo.setClient(inData.getClientId());
//                userInfo.setDepId(inData.getBrId());
//                Map<String, Object> params = new HashMap<>();
//                params.put("orderNumber", information.getGsspmCardNo());
//                params.put("outerNumber", billNo);
//                params.put("orderAmount", information.getReturnAmount());
//                ResultVO<Map<String, Object>> refundBack = this.thirdPayService.refundBack(userInfo, params);
//                if (!refundBack.isSuccess()) {
//                    throw new BusinessException(refundBack.getMsg());
//                }
//            }
//        }

        //退货
        //门店销售主表
        GaiaSdSaleH saleReturn = this.getGaiaSdSaleH(inData, sd, billNo);
        GaiaSdMemberCardToData cardData = null;
        //全部退款 怎么来的 怎么退
        List<GaiaSdSalePayMsg> payMsgsList = new ArrayList<>();
        for (PaymentInformationOutData salePayMsgs : inData.getPayOutData()) {
            GaiaSdSalePayMsg salePayMsg = this.getGaiaSdSalePayMsg(billNo, salePayMsgs);
            if ("1000".equals(salePayMsgs.getGsspmId())) {
                salePayMsg.setGsspmRmbAmt(new BigDecimal(salePayMsgs.getReturnAmount()).negate());//退还现金
                salePayMsg.setGsspmAmt(new BigDecimal(salePayMsgs.getReturnAmount()).negate());//应收金额
            } else if ("5000".equals(salePayMsgs.getGsspmId()) && StrUtil.isNotBlank(salePayMsgs.getGsspmCardNo())) {
                GaiaSdRechargeCard card = this.gaiaSdRechargeCardMapper.getAccountByCardId(salePayMsgs.getClient(), salePayMsgs.getGsspmCardNo());
                if (ObjectUtil.isEmpty(card)) {
                    throw new BusinessException(UtilMessage.THE_STORED_VALUE_CARD_DOES_NOT_EXIST);
                }
                BigDecimal gsrcAmt = card.getGsrcAmt();//初始金额
                BigDecimal decimal = new BigDecimal(salePayMsgs.getReturnAmount());//退款金额
                card.setGsrcAmt(card.getGsrcAmt().add(decimal));
                card.setGsrcUpdateBrId(salePayMsgs.getGsspmBrId());
                card.setGsrcUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                card.setGsrcUpdateEmp(inData.getUserId());
                this.gaiaSdRechargeCardMapper.updateByCardId(card);

                //储值卡异动表
                GaiaSdRechargeChange gaiaSdRechargeChange = this.getGaiaSdRechargeChange(salePayMsg, saleReturn, salePayMsgs, gsrcAmt, decimal, card);
                this.rechargeChangeMapper.insertOne(gaiaSdRechargeChange);
                salePayMsg.setGsspmAmt(new BigDecimal(salePayMsgs.getReturnAmount()).negate());
            } else if ("9001".equals(salePayMsgs.getGsspmId())) {//积分抵现
                if (StrUtil.isNotBlank(sd.getGsshHykNo())) {
                    cardData = this.memberBasicMapper.selectByCardId(inData.getClientId(), inData.getBrId(), inData.getMemberId(), inData.getSalesMemberCardNum());
                    if ((StrUtil.isNotBlank(salePayMsgs.getGsshIntegralCash()) && new BigDecimal(salePayMsgs.getGsshIntegralCash()).compareTo(BigDecimal.ZERO) > 0)) {
                        BigDecimal nowJf = new BigDecimal(cardData.getGsmbcIntegral());//初始积分
                        BigDecimal ydJF = new BigDecimal(salePayMsgs.getGsshIntegralCash()); //异动积分
                        BigDecimal add = new BigDecimal(cardData.getGsmbcIntegral()).add(new BigDecimal(salePayMsgs.getGsshIntegralCash())); //结果积分
                        cardData.setGsmbcIntegral(NumberUtil.parseNumber(add.toString()).toString());
                        //插入积分异动表
                        GaiaSdIntegralChange changeRecord = this.getGaiaSdIntegralChange(inData, ydJF.negate(), nowJf, add, billNo, "2");
                        this.integralChangeMapper.insert(changeRecord);//插入积分字段待定
                    }
                }
                continue;
            } else {
                salePayMsg.setGsspmAmt(new BigDecimal(salePayMsgs.getReturnAmount()).negate());//应收金额
            }
            salePayMsg.setGsspmDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
            salePayMsg.setGsspmZlAmt(BigDecimal.ZERO);//找零金额
            salePayMsg.setGsspmType("1");
            payMsgsList.add(salePayMsg);
        }

        if (CollUtil.isNotEmpty(payMsgsList)) {
            this.payMsgMapper.insertLists(payMsgsList);
        }


        BigDecimal ysAmt = BigDecimal.ZERO;//应收总金额
        BigDecimal lsAmt = BigDecimal.ZERO;//零售总金额
        List<GaiaSdSaleD> saleDList = new ArrayList<>();
        List<GaiaSdBatchChange> changeList = new ArrayList<>();
        List<GaiaSdStock> stockList = new ArrayList<>();
        List<GaiaSdStockBatch> batchList = new ArrayList<>();

        List<GaiaSdSaleD> sdSaleDList = this.saleDMapper.getAllByBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        List<String> proList = sdSaleDList.stream().map(GaiaSdSaleD::getGssdProId).collect(Collectors.toList());
        //批号库存表
        Map<String, GaiaSdStockBatch> stockBatchMap = this.getStockBatchMap(inData, proList);
        Map<String, GaiaProductBusiness> businessMap = this.getBusinessMap(inData, proList);
        //获得库存表
        Map<String, GaiaSdStock> stockMap = this.getStockMap(inData, proList);
        //获得批号异动表
        Map<String, GaiaSdBatchChange> batchChangeMap = this.getBatchChangeMap(inData, proList);
        //物料评估表
        Map<String, GaiaMaterialAssess> assessMap = this.getMaterialAssessMap(inData, proList);
        //缓存表
        List<GaiaDataSynchronized> synStockList = new ArrayList<>();
        List<GaiaDataSynchronized> synstockBatchList = new ArrayList<>();
        List<GaiaDataSynchronized> synBatchChangeList = new ArrayList<>();
        Map<String, GaiaSdStock> decimalMap = new HashMap<>();
        //积分换购商品退货集
        Map<String, BigDecimal> pointIntegralMap = new HashMap<>(8);
        for (GaiaSdSaleD sdSaleD : sdSaleDList) {//当前销售单所有的信息
            for (GetSaleReturnToData toDatum : inData.getReturnToData()) {//退货信息
                if (sdSaleD.getGssdProId().equals(toDatum.getGssdProId()) && sdSaleD.getGssdSerial().equals(toDatum.getGssdSerial())) {
                    if (StrUtil.isBlank(toDatum.getGssdReturnQty())) {
                        continue;
                    }
                    //门店销售明细表
                    GaiaSdSaleD saleD = CopyUtil.copy(sdSaleD, GaiaSdSaleD.class);
                    GaiaMaterialAssess assess = assessMap.get(sdSaleD.getGssdProId());
                    //购买数量
                    BigDecimal gssdQty = new BigDecimal(sdSaleD.getGssdQty());
                    //退货数量
                    BigDecimal returnQty = new BigDecimal(NumberUtil.parseNumber(toDatum.getGssdReturnQty()).toString());
                    if (returnQty.compareTo(gssdQty) == 1) {
                        throw new BusinessException("当前商品 :" + saleD.getGssdProId() + "退货数量不可大于购买数量!");
                    }
                    String gssdProStatus = saleD.getGssdProStatus();
                    String gssdProId = saleD.getGssdProId();
                    if ("积分换购".equals(gssdProStatus)) {
                        BigDecimal lastRetrunQty = pointIntegralMap.get(gssdProId);
                        if (lastRetrunQty == null) {
                            pointIntegralMap.put(gssdProId, returnQty);
                        } else {
                            BigDecimal add = lastRetrunQty.add(returnQty);
                            pointIntegralMap.put(gssdProId, add);
                        }
                    }
                    saleD.setGssdPrc1(sdSaleD.getGssdPrc1().negate());// 单品零售价
                    if ("拆零".equals(sdSaleD.getGssdProStatus())) {
                        ysAmt = ysAmt.add(sdSaleD.getGssdAmt());
                        lsAmt = lsAmt.add(returnQty.multiply(sdSaleD.getGssdPrc1()));
                        saleD.setGssdQty("-" + saleD.getGssdQty());//退货数量
                        saleD.setGssdAmt(sdSaleD.getGssdAmt().negate());//退货金额 反转
                    } else if ((gssdQty.compareTo(returnQty) == 0)) {//判断当前行 是否是全部退
                        ysAmt = ysAmt.add(sdSaleD.getGssdAmt());
                        lsAmt = lsAmt.add(returnQty.multiply(sdSaleD.getGssdPrc1()));
                        saleD.setGssdAmt(sdSaleD.getGssdAmt().negate());//退货金额 反转
                        saleD.setGssdQty("-" + toDatum.getGssdReturnQty());//退货数量
                    } else {//部分退
                        //退货数量 乘以 单品应收价格
                        BigDecimal gssdAmt = returnQty.multiply(sdSaleD.getGssdPrc2());
                        ysAmt = ysAmt.add(gssdAmt);
                        lsAmt = lsAmt.add(returnQty.multiply(sdSaleD.getGssdPrc1()));
                        saleD.setGssdAmt(gssdAmt.negate());//汇总应收金额(单品应收价  *  退货数量)  负数
                        saleD.setGssdQty("-" + toDatum.getGssdReturnQty());//退货数量

                    }
                    saleD.setGssdPrc2(sdSaleD.getGssdPrc2().negate());//单品应收价  负数
                    saleD.setClientId(inData.getClientId());
                    saleD.setGssdBillNo(billNo);
                    saleD.setGssdProStatus(UtilMessage.RETURN_STATUS);
                    saleD.setGssdDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    BigDecimal sub = sdSaleD.getGssdPrc1().subtract(sdSaleD.getGssdPrc2()).setScale(4, BigDecimal.ROUND_UP);//减去
                    BigDecimal zkAmt = sub.multiply(new BigDecimal(toDatum.getGssdReturnQty())).setScale(4, BigDecimal.ROUND_UP);//乘以
                    saleD.setGssdZkAmt(zkAmt.negate());
                    if (ObjectUtil.isNotEmpty(assess) && (StrUtil.isBlank(toDatum.getGssdIfXnsp()) || "0".equals(toDatum.getGssdIfXnsp()))) {
                        saleD.setGssdMovPrices(assess.getMatMovPrice().multiply(returnQty));//移动平均总价
                        saleD.setGssdTaxRate(assess.getMatMovPrice().multiply(returnQty).multiply(saleD.getGssdMovTax()));//移动税额
                        saleD.setGssdAddAmt(assess.getMatMovPrice().multiply(returnQty));//加点后金额
                        saleD.setGssdAddTax(assess.getMatMovPrice().multiply(returnQty).multiply(saleD.getGssdMovTax()));//加点后税金
                    } else if ("1".equals(toDatum.getGssdIfXnsp())) {
                        if (saleD.getGssdIfCkj() != null && saleD.getGssdIfCkj().compareTo(BigDecimal.ZERO) == 1) {
                            saleD.setGssdMovPrice(saleD.getGssdIfCkj());//参考价
                            saleD.setGssdMovPrices(saleD.getGssdIfCkj().multiply(returnQty));//参考价/数量?
                            saleD.setGssdTaxRate(saleD.getGssdMovPrices().multiply(saleD.getGssdMovTax()));//(参考价/数量)*税率?
//                        加点金额= [参考价*销售数量]/(1+税率)，保留4位小数。
                            BigDecimal decimal = (saleD.getGssdIfCkj().multiply(returnQty)).divide(BigDecimal.ONE.add(saleD.getGssdMovTax()), 4, BigDecimal.ROUND_HALF_UP);
                            saleD.setGssdAddAmt(decimal);
//                        加点税额= [参考价*销售数量]-加点金额，保留4位小数。
                            saleD.setGssdAddTax((saleD.getGssdIfCkj().multiply(returnQty)).subtract(decimal).setScale(4, BigDecimal.ROUND_HALF_UP));
                        } else {
                            //参考价木有维护时,一切皆0
                            saleD.setGssdMovPrice(BigDecimal.ZERO);//参考价
                            saleD.setGssdMovPrices(BigDecimal.ZERO);//参考价/数量?
                            saleD.setGssdTaxRate(BigDecimal.ZERO);//(参考价/数量)*税率?
                            saleD.setGssdAddAmt(BigDecimal.ZERO);
                            saleD.setGssdAddTax(BigDecimal.ZERO);
                        }
                    }
                    saleD.setIntegral(Util.divide(Util.checkNull(new BigDecimal(sdSaleD.getIntegral())).multiply(new BigDecimal(toDatum.getGssdReturnQty())),Util.checkNull(new BigDecimal(sdSaleD.getGssdQty()))).multiply(new BigDecimal("-1")).setScale(2, BigDecimal.ROUND_DOWN).toString());
                    saleD.setIntegralFlag(sdSaleD.getIntegralFlag());
                    saleDList.add(saleD);
                    if (StrUtil.isBlank(toDatum.getGssdIfXnsp()) || "0".equals(toDatum.getGssdIfXnsp())) {
                        //加库存
                        this.inventory(inData, billNo, saleD, guid, listDto, batchChangeMap, stockMap, stockBatchMap, businessMap, changeList, stockList, batchList, synStockList, synstockBatchList, synBatchChangeList, decimalMap);
                    }

                }
            }
        }

        saleReturn.setGsshYsAmt(ysAmt.setScale(4, BigDecimal.ROUND_UP).negate()); //应收金额 负数(从明细表算出退货数量 和金额)
        saleReturn.setGsshNormalAmt(lsAmt.setScale(4, BigDecimal.ROUND_UP).negate());//零售总金额

        Set<Map.Entry<String, GaiaSdStock>> entries = decimalMap.entrySet();
        for (Map.Entry<String, GaiaSdStock> entry : entries) {
            GaiaSdStock value = entry.getValue();
            Map<String, Object> map3 = new HashMap<>(16);
            map3.put("CLIENT", inData.getClientId());
            map3.put("GSS_BR_ID", inData.getBrId());
            map3.put("GSS_PRO_ID", value.getGssProId());
            GaiaDataSynchronized aSyncStock = new GaiaDataSynchronized();
            aSyncStock.setClient(inData.getClientId());
            aSyncStock.setSite(inData.getBrId());
            aSyncStock.setVersion(IdUtil.randomUUID());
            aSyncStock.setTableName("GAIA_SD_STOCK");
            aSyncStock.setCommand("update");
            aSyncStock.setSourceNo("退货");
            aSyncStock.setArg(JSON.toJSONString(map3));
            synStockList.add(aSyncStock);
            stockList.add(value);
        }

        //拿到所有的 退货商品编码
        List<String> prolist = inData.getReturnToData().stream().map(GetSaleReturnToData::getGssdProId).collect(Collectors.toList());
        //用所有的商品编码去 拿不积分商品
        List<GaiaSdIntegralRateSet> setList = this.gaiaSdIntegralRateSetMapper.getAllPointsProducts(inData.getClientId(), inData.getBrId(), prolist);
        log.info("当前商品是否属于多倍或不积分商品 : " + setList);
        BigDecimal refundAmount = new BigDecimal(inData.getRefundAmount()); //退货总金额
        for (GaiaSdIntegralRateSet rateSet : setList) {
            for (GetSaleReturnToData toDatum : inData.getReturnToData()) {
                if (toDatum.getGssdProId().equals(rateSet.getProId()) && "0".equals(rateSet.getIfInte())) {
                    refundAmount = refundAmount.subtract(new BigDecimal(toDatum.getGssdAmt()));
                    log.info("不积分商品 : " + toDatum.getGssdProId() + "   积分 :" + refundAmount);
                } else if (toDatum.getGssdProId().equals(rateSet.getProId()) && "1".equals(rateSet.getIfInte())) {
                    //表示当前商品 属于积分积分状态
                    if (StrUtil.isBlank(rateSet.getInteRate())) {
                        throw new BusinessException("请设置该商品 : " + toDatum.getGssdProId() + " 的积分倍率!");
                    }
                    BigDecimal gssdAmt = new BigDecimal(toDatum.getGssdReturnQty()).compareTo(new BigDecimal(toDatum.getGssdQty())) == 0 ? new BigDecimal(toDatum.getGssdAmt()) : new BigDecimal(toDatum.getGssdReturnQty()).multiply(new BigDecimal(toDatum.getGssdPrc2()));
                    log.info("多倍积分商品 : " + toDatum.getGssdProId() + "  当前行金额 : " + gssdAmt);
                    BigDecimal multiply = gssdAmt.multiply(new BigDecimal(rateSet.getInteRate()));//当前商品实收价 乘 积分倍率
                    refundAmount = refundAmount.subtract(gssdAmt);//先减去 当前的 一倍金额
                    refundAmount = refundAmount.add(multiply);
                    log.info("当前订单每行积分 : " + refundAmount);
                }
            }
        }

        BigDecimal finalPoints = BigDecimal.ZERO;
        if (StrUtil.isNotBlank(saleReturn.getGsshHykNo()) && StrUtil.isNotBlank(inData.getSalesMemberCardNum())) {
            if (ObjectUtil.isNotEmpty(cardData)) {
                BigDecimal integral = new BigDecimal(StrUtil.isBlank(cardData.getGsmbcIntegral()) ? "0" : cardData.getGsmbcIntegral()); //当前积分
                log.info("当前会员积分 : " + integral);
                BigDecimal amtSale = new BigDecimal(StrUtil.isBlank(cardData.getGsmcAmtSale()) ? "1" : cardData.getGsmcAmtSale());//消费金额
                log.info("当前会员消费金额 : " + amtSale);
                BigDecimal integralSale = new BigDecimal(StrUtil.isBlank(cardData.getGsmcIntegralSale()) ? "0" : cardData.getGsmcIntegralSale());//积分数值
                log.info("当前会员积分数值 : " + integralSale);
                if (amtSale.compareTo(BigDecimal.ZERO) == 0) {
                    cardData.setGsmbcIntegral(BigDecimal.ZERO.toString());
                    cardData.setGsmbcZeroDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                } else {
                    BigDecimal atlast = BigDecimal.ZERO;
                    //退货积分  这边是正数
                    finalPoints = saleDList.stream().map(item -> new BigDecimal(item.getIntegral())).reduce(BigDecimal.ZERO, BigDecimal::add).negate();
                    List<GaiaSdSaleD> collectList = saleDList.stream().filter(item -> ObjectUtil.isNotEmpty(item.getIntegralFlag())).collect(Collectors.toList());
                    if (BigDecimal.ZERO.compareTo(finalPoints) == 0 && CollUtil.isEmpty(collectList)) {
                        //如果sale_d 积分没有值 走 原逻辑
                        //退货总金额 / 消费金额  * 积分数值 = 积分
                        finalPoints = refundAmount.divide(amtSale, 0, RoundingMode.DOWN).multiply(integralSale).setScale(2, BigDecimal.ROUND_DOWN);
                        log.info("原逻辑: 当前订单最终应退积分 : " + finalPoints);
                        //当前积分 -  退货积分 = 结果
                        atlast = integral.subtract(finalPoints);
                        log.info("原逻辑: 当前积分 -  退货积分  = 结果 : " + atlast);
                    } else {
                        //新逻辑
                        log.info("新逻辑: 当前订单最终应退积分 : " + finalPoints);
                        //当前积分 +  退货积分（这里是正数） = 结果
                        atlast = integral.subtract(finalPoints);
                        log.info("新逻辑: 当前积分 -  退货积分  = 结果 : " + atlast);
                    }
                    //结果 小于 或者等于 0  直接存0
                    if (integral.compareTo(BigDecimal.ZERO) == 0 || atlast.compareTo(BigDecimal.ZERO) == -1) {
                        cardData.setGsmbcIntegral(BigDecimal.ZERO.toString());
                    } else {
                        cardData.setGsmbcIntegral(atlast.setScale(2,BigDecimal.ROUND_HALF_UP).toString());
                    }
                    cardData.setGsmbcIntegralLastdate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    this.memberBasicMapper.updateByCardId(cardData);

                    //插入积分异动表
                    GaiaSdIntegralChange changeRecord = this.getGaiaSdIntegralChange(inData, finalPoints, integral, atlast, billNo, "1");
                    this.integralChangeMapper.insert(changeRecord);//插入积分字段待定

                    GaiaDataSynchronized aSyncStockBatch = this.getGaiaDataSynchronized(inData, cardData);
                    this.synchronizedService.addOne(aSyncStockBatch);
                }
            }
        }


        if (CollUtil.isNotEmpty(changeList)) {
            this.batchChangeMapper.insertLists(changeList);//批量插入 批次异动表
        }
        if (CollUtil.isNotEmpty(stockList)) {
            this.stockMapper.updateList(stockList);//批量更新库存表
        }

        //调外卖平台更新库存接口; GaiaSdStock是库存总表
        if (!CollectionUtils.isEmpty(stockList)) {
            stockService.syncStock(stockList);
        }
        if (CollUtil.isNotEmpty(batchList)) {
            this.stockBatchMapper.updateList(batchList);//批量更新批号库存表
        }


        saleReturn.setGsshIntegralAdd(finalPoints.negate().toString());//存的是当前退货积分
        saleReturn.setGsshDayreport(null);//日结问题改成null
        this.saleHMapper.insert(saleReturn);
        this.saleDMapper.insertLists(saleDList);//批量插入D表
        if (CollUtil.isNotEmpty(synStockList)) {
            this.synchronizedService.insertLists(synStockList);
        }
        if (CollUtil.isNotEmpty(synstockBatchList)) {
            this.synchronizedService.insertLists(synstockBatchList);
        }
        if (CollUtil.isNotEmpty(synBatchChangeList)) {
            this.synchronizedService.insertLists(synBatchChangeList);
        }
        //新积分换购商品退货处理
        if (!pointIntegralMap.isEmpty()) {
            new Thread(() -> {
                try {
                    gaiaSdIntegralExchangeSetHeadService.returnQty(inData.getClientId(), inData.getBrId(), inData.getBillNo(), pointIntegralMap);
                    
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
        if("6".equals(sd.getGsshBillType()))
            payService.hisPayRefund(sd,saleDList);
        
        Map<String, Object> map = new HashMap<>(8);
        //物料凭证
        if (CollUtil.isNotEmpty(listDto)) {
            map.put("listDto", listDto);
        }
        map.put("billNo", billNo);
        return map;
    }

    /**
     * 积分异动表填充参数
     *
     * @param inData
     * @param finalPoints
     * @param integral
     * @param atlast
     * @return
     */
    private GaiaSdIntegralChange getGaiaSdIntegralChange(SalesInquireData inData, BigDecimal finalPoints, BigDecimal integral, BigDecimal atlast, String billNo, String type) {
        GaiaSdIntegralChange changeRecord = new GaiaSdIntegralChange();
        changeRecord.setClient(inData.getClientId());//异动加盟
        changeRecord.setGsicBrId(inData.getBrId());//异动门店
        changeRecord.setGsicVoucherId(billNo);//异动单号
        changeRecord.setGsicFlag("1");//销售单
        changeRecord.setGsicDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));//异动日期
        changeRecord.setGsicType(type);//增加积分
        changeRecord.setGsicCardId(inData.getSalesMemberCardNum());//会员卡号
        changeRecord.setGsicInitialIntegral(integral);   //初始积分
        changeRecord.setGsicChangeIntegral(finalPoints.negate());    //异动积分
        changeRecord.setGsicResultIntegral(atlast);//结果积分
        changeRecord.setGsicEmp(inData.getUserId());//异动人
        return changeRecord;
    }

    public GaiaSdRechargeChange getGaiaSdRechargeChange(GaiaSdSalePayMsg inData, GaiaSdSaleH saleReturn, PaymentInformationOutData salePayMsgs, BigDecimal gsrcAmt, BigDecimal decimal, GaiaSdRechargeCard card) {
        //生成一条充值卡金额异动记录
        GaiaSdRechargeChange gaiaSdRechargeChange = new GaiaSdRechargeChange();
        //加盟商
        gaiaSdRechargeChange.setClient(inData.getClient());
        //储值卡号
        gaiaSdRechargeChange.setGsrcAccountId(card.getGsrcId());
        //异动门店
        gaiaSdRechargeChange.setGsrcBrId(inData.getGsspmBrId());
        //异动单号
        gaiaSdRechargeChange.setGsrcVoucherId(saleReturn.getGsshBillNo());
        //异动日期
        gaiaSdRechargeChange.setGsrcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        //异动时间
        gaiaSdRechargeChange.setGsrcTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
        //异动类型1:充值，2：退款 3：消费 4：退货
        gaiaSdRechargeChange.setGsrcType("4");
        //储值卡初始金额
        gaiaSdRechargeChange.setGsrcInitialAmt(gsrcAmt);
        //储值卡异动金额
        gaiaSdRechargeChange.setGsrcChangeAmt(new BigDecimal(salePayMsgs.getReturnAmount()).abs());
        //储值卡结果金额
        gaiaSdRechargeChange.setGsrcResultAmt(gsrcAmt.add(decimal));
        //异动人员
        gaiaSdRechargeChange.setGsrcEmp(saleReturn.getGsshEmp());
        return gaiaSdRechargeChange;
    }

    /**
     * 积分添加
     *
     * @param inData
     * @param sd
     * @param salePayMsgs
     * @return
     */
    public GaiaSdMemberCardData getGaiaSdMemberCardData(SalesInquireData inData, GaiaSdSaleH sd, PaymentInformationOutData salePayMsgs) {
        if (StrUtil.isBlank(sd.getGsshHykNo())) {
            throw new BusinessException(UtilMessage.NO_MEMBER_INFORMATION);
        }
        GaiaSdMemberCardData cardData = this.memberBasicMapper.getMembershipCardInformation(inData.getClientId(), inData.getBrId(), sd.getGsshHykNo());
        BigDecimal add = new BigDecimal(cardData.getGsmbcIntegral()).add(new BigDecimal(salePayMsgs.getGsshIntegralCash()));
        cardData.setGsmbcIntegral(NumberUtil.parseNumber(add.toString()).toString());
        return cardData;
    }

    public GaiaSdSalePayMsg getGaiaSdSalePayMsg(String billNo, PaymentInformationOutData salePayMsgs) {
        GaiaSdSalePayMsg salePayMsg = CopyUtil.copy(salePayMsgs, GaiaSdSalePayMsg.class);
        salePayMsg.setGsspmDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        salePayMsg.setGsspmBillNo(billNo);
        salePayMsg.setGsspmId(salePayMsgs.getGsspmId());
        salePayMsg.setGsspmDayreport(null);
        return salePayMsg;
    }

    /**
     * 储值卡新增
     *
     * @param inData
     * @param salePayMsgs
     * @return
     */
    public GaiaSdRechargeCard getGaiaSdRechargeCard(SalesInquireData inData, PaymentInformationOutData salePayMsgs) {
        GaiaSdRechargeCard card = this.gaiaSdRechargeCardMapper.getAccountByCardId(salePayMsgs.getClient(), salePayMsgs.getGsspmCardNo());
        if (ObjectUtil.isEmpty(card)) {
            throw new BusinessException(UtilMessage.THE_STORED_VALUE_CARD_DOES_NOT_EXIST);
        }
        BigDecimal gsrcAmt = card.getGsrcAmt();//初始金额
        BigDecimal decimal = new BigDecimal(salePayMsgs.getReturnAmount());//退款金额
        card.setGsrcAmt(card.getGsrcAmt().add(decimal));
        card.setGsrcUpdateBrId(salePayMsgs.getGsspmBrId());
        card.setGsrcUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        card.setGsrcUpdateEmp(inData.getUserId());
        return card;
    }

    public GaiaDataSynchronized getGaiaDataSynchronized(SalesInquireData inData, GaiaSdMemberCardToData cardData) {
        Map<String, Object> map = new HashMap<>(8);
        map.put("CLIENT", cardData.getClient());
        map.put("GSMBC_MEMBER_ID", cardData.getGsmbcMemberId());
        map.put("GSMBC_BR_ID", cardData.getGsmbcBrId());
        GaiaDataSynchronized aSyncStockBatch = new GaiaDataSynchronized();
        aSyncStockBatch.setClient(inData.getClientId());
        aSyncStockBatch.setSite(inData.getBrId());
        aSyncStockBatch.setVersion(IdUtil.randomUUID());
        aSyncStockBatch.setTableName("GAIA_SD_MEMBER_CARD");
        aSyncStockBatch.setCommand("update");
        aSyncStockBatch.setSourceNo("退货-退积分");
        aSyncStockBatch.setArg(JSON.toJSONString(map));
        return aSyncStockBatch;
    }


    public GaiaSdSaleH getGaiaSdSaleH(SalesInquireData inData, GaiaSdSaleH sd, String billNo) {
        GaiaSdSaleH saleReturn = CopyUtil.copy(sd, GaiaSdSaleH.class);
        saleReturn.setClientId(inData.getClientId());//加盟号
        saleReturn.setGsshBrId(sd.getGsshBrId());//店号
        saleReturn.setGsshDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));//日期
        saleReturn.setGsshTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));//时间
        saleReturn.setGsshYsAmt(sd.getGsshYsAmt().negate()); //应收金额 负数(从明细表算出退货数量 和金额)
        saleReturn.setGsshZkAmt(sd.getGsshZkAmt().negate());
        saleReturn.setGsshEmp(inData.getUserId());//收银工号(当前人)
        saleReturn.setGsshEmpReturn(inData.getGsshEmpReturn()); //退货审核人
        saleReturn.setGsshHideFlag(sd.getGsshHideFlag());//挂单不显示
        saleReturn.setGsshBillNoReturn(inData.getBillNo());//原销售单号
        saleReturn.setGsshBillNo(billNo);//新销售单号(退货单号)
        saleReturn.setGsshCallAllow(UtilMessage.ONE);
        saleReturn.setGsshReturnStatus(UtilMessage.ZERO);
        saleReturn.setGsshIntegralCash(StrUtil.isBlank(sd.getGsshIntegralCash()) ? null : "-" + sd.getGsshIntegralCash());//退的积分
        saleReturn.setGsshIntegralCashAmt(sd.getGsshIntegralCashAmt() == null ? null : sd.getGsshIntegralCashAmt().negate()); //退积分的金额
        return saleReturn;
    }

    /**
     * 判断订单是否存在
     *
     * @param inData
     * @return
     */
    @Override
    public boolean judgment(SalesInquireData inData) {
        Example exampleRe = null;
        exampleRe = new Example(GaiaSdSaleH.class);
        exampleRe.createCriteria().andEqualTo("gsshBillNoReturn", inData.getBillNo()).andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getBrId());
        //查询门店销售主表
        GaiaSdSaleH sdSaleH = this.saleHMapper.selectOneByExample(exampleRe);
        if (ObjectUtil.isNotEmpty(sdSaleH)) {
            return false;
        }

        exampleRe = new Example(GaiaSdSaleH.class);
        exampleRe.createCriteria().andEqualTo("gsshBillNo", inData.getBillNo()).andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getBrId());
        //查询门店销售主表c
        sdSaleH = this.saleHMapper.selectOneByExample(exampleRe);
        if (StrUtil.isNotBlank(sdSaleH.getGsshBillNoReturn())) {
            return false;
        }

        return true;
    }

    /**
     * 物料凭证
     *
     * @param dtos
     */
    @Override
    public void materialDocumentInterface(List<MaterialDocRequestDto> dtos) {
        log.info("materialList:{}", JSON.toJSONString(dtos));
        String result = purchaseService.insertMaterialDoc(dtos);
        log.info("insertMaterialDoc result:{}", JSON.toJSONString(result));
        JSONObject jsonObject = JSONObject.parseObject(result);
        if (!"0".equals(jsonObject.getString("code"))) {
            log.info("insertMaterialDoc :\"物料凭证接口错误\"");
        }

    }

    /**
     * 判断积分是否够减
     *
     * @param inData
     * @return
     */
    @Override
    public SaleReturnAllowOutData JudgmentPoints(SalesInquireData inData) {
        //拿到所有的 退货商品编码
        List<GetSaleReturnToData> returnToData = inData.getReturnToData();
        List<String> prolist = inData.getReturnToData().stream().map(GetSaleReturnToData::getGssdProId).collect(Collectors.toList());
        //用所有的商品编码去 拿不积分商品
        List<GaiaSdIntegralRateSet> setList = this.gaiaSdIntegralRateSetMapper.getAllPointsProducts(inData.getClientId(), inData.getBrId(), prolist);
        BigDecimal refundAmount = new BigDecimal(inData.getRefundAmount()); //退货总金额
        for (GaiaSdIntegralRateSet rateSet : setList) {
            for (GetSaleReturnToData toDatum : inData.getReturnToData()) {
                if (toDatum.getGssdProId().equals(rateSet.getProId()) && "0".equals(rateSet.getIfInte())) {
                    refundAmount = refundAmount.subtract(new BigDecimal(toDatum.getGssdAmt()));
                } else if (toDatum.getGssdProId().equals(rateSet.getProId()) && "1".equals(rateSet.getIfInte())) {
                    //表示当前商品 属于积分积分状态
                    if (StrUtil.isBlank(rateSet.getInteRate())) {
                        throw new BusinessException("请设置该商品 : " + toDatum.getGssdProId() + " 的积分倍率!");
                    }

                    BigDecimal gssdAmt = new BigDecimal(toDatum.getGssdReturnQty()).compareTo(new BigDecimal(toDatum.getGssdQty())) == 0 ? new BigDecimal(toDatum.getGssdAmt()) : new BigDecimal(toDatum.getGssdReturnQty()).multiply(new BigDecimal(toDatum.getGssdPrc2()));
                    BigDecimal multiply = gssdAmt.multiply(new BigDecimal(rateSet.getInteRate()));//当前商品实收价 乘 积分倍率
                    refundAmount = refundAmount.subtract(gssdAmt);//先减去 当前的 一倍金额
                    refundAmount = refundAmount.add(multiply);
                }
            }
        }

        boolean flag = true;
        SaleReturnAllowOutData outData = new SaleReturnAllowOutData();
        if (StrUtil.isNotBlank(inData.getSalesMemberCardNum()) && StrUtil.isNotBlank(inData.getMemberId())) {
            GaiaSdMemberCardToData cardData = this.memberBasicMapper.selectByCardId(inData.getClientId(), inData.getBrId(), inData.getMemberId(), inData.getSalesMemberCardNum());
            if (ObjectUtil.isEmpty(cardData)) {
                throw new BusinessException(UtilMessage.CARD_NUMBER_DOES_NOT_EXIST);
            } else {
                BigDecimal integral = new BigDecimal(StrUtil.isBlank(cardData.getGsmbcIntegral()) ? "0.000" : cardData.getGsmbcIntegral()); //当前积分
                BigDecimal amtSale = new BigDecimal(StrUtil.isBlank(cardData.getGsmcAmtSale()) ? "0.000" : cardData.getGsmcAmtSale());//消费金额
                BigDecimal integralSale = new BigDecimal(StrUtil.isBlank(cardData.getGsmcIntegralSale()) ? "0.000" : cardData.getGsmcIntegralSale());//积分数值
                List<String> serialList = returnToData.stream().map(GetSaleReturnToData::getGssdSerial).collect(Collectors.toList());//行号集合
                List<GetSaleReturnToData> oriList = saleDMapper.listOriSaleDetail(inData.getClientId(),inData.getBrId(),inData.getBillNo(),serialList);//原销售单积分明细
                if (amtSale.compareTo(BigDecimal.ZERO) == 0) {
                    flag = false;
                } else {
                    BigDecimal refundIntegral = BigDecimal.ZERO;
                    for (GetSaleReturnToData returnData : returnToData) {
                        for (GetSaleReturnToData ori : oriList) {
                            if (ori.getGssdProId().equals(returnData.getGssdProId()) && ori.getGssdSerial().equals(returnData.getGssdSerial())) {
                                // （ 购买时的积分 / 购买数量 ）* 退货数量
                                BigDecimal returnintegral = Util.divide(Util.checkNull(ori.getIntegral()).multiply(new BigDecimal(returnData.getGssdReturnQty())), Util.checkNull(new BigDecimal(ori.getGssdQty())));
                                refundIntegral = refundIntegral.add(returnintegral);
                                break;
                            }
                        }
                    }
                    //当前积分 -  退货积分
                    BigDecimal atlast = integral.subtract(refundIntegral.setScale(2,BigDecimal.ROUND_DOWN));
                    String param = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), inData.getBrId(), "SALE_BACK_INTEGRAL_CHECK");
                    if (atlast.compareTo(BigDecimal.ZERO) == -1 && "1".equals(param)) {
                        throw new BusinessException("当前会员卡可用积分"+integral.toString()+"，低于退货需冲减积分"+refundIntegral.setScale(2,BigDecimal.ROUND_DOWN).toString()+",\n无法继续退货");
                    } else if (atlast.compareTo(BigDecimal.ZERO) == -1) {
                        flag = false;
                        outData.setMsg("     当前会员卡可用积分"+integral.toString()+"，\n     低于退货需冲减积分"+refundIntegral.setScale(2,BigDecimal.ROUND_DOWN).toString()+",\n     是否继续退货");
                    }
                }
            }
        }
        outData.setFlag(flag);
        return outData;
    }
    @Override
    public ReturnCacheData cache(SalesInquireData inData) {
        return this.saleHMapper.cache(inData);
    }


    /**
     * 获取批次异动表当前订单的所有数据
     *
     * @param inData
     * @param detailInData
     * @return
     */
    private Map<String, GaiaSdBatchChange> getBatchChangeMap(SalesInquireData inData, List<String> gsbcProId) {
        Example batchChangeExample = new Example(GaiaSdBatchChange.class);
        batchChangeExample.createCriteria().andEqualTo("gsbcVoucherId", inData.getBillNo()).andEqualTo("client", inData.getClientId())
                .andEqualTo("gsbcBrId", inData.getBrId()).andEqualTo("gsbcDate", DateUtil.format(DateUtil.parse(inData.getGsshDate()), DatePattern.PURE_DATE_PATTERN)).andIn("gsbcProId", gsbcProId);
        batchChangeExample.setOrderByClause("GSBC_BATCH ASC");
        List<GaiaSdBatchChange> batchChanges = this.batchChangeMapper.selectByExample(batchChangeExample);
        Map<String, GaiaSdBatchChange> objectMap = new TableMap<>(16);
        batchChanges.forEach(batchChange -> objectMap.put(batchChange.getGsbcProId(), batchChange));
        return objectMap;
    }


    /**
     * 获得当前加盟商下门店下的所有库存
     *
     * @param inData
     * @return
     */
    private Map<String, GaiaSdStock> getStockMap(SalesInquireData inData, List<String> proList) {
        Example stockExample = new Example(GaiaSdStock.class);
        stockExample.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getBrId()).andIn("gssProId", proList);
        List<GaiaSdStock> sdStocks = this.stockMapper.selectByExample(stockExample);
        Map<String, GaiaSdStock> stockBatchMap = new HashMap<>(16);
        for (GaiaSdStock sdStock : sdStocks) {
            stockBatchMap.put(sdStock.getGssProId(), sdStock);
        }
        return stockBatchMap;
    }

    /**
     * 获取当前订单的所有批号库存表
     *
     * @param inData
     * @return
     */
    private Map<String, GaiaSdStockBatch> getStockBatchMap(SalesInquireData inData, List<String> proList) {
        List<GaiaSdStockBatch> sdStockBatches = this.stockBatchMapper.selectAllByBillNo(inData.getClientId(), inData.getBrId(), proList);
        Map<String, GaiaSdStockBatch> stockBatchMap = new TableMap<>(16);
        sdStockBatches.forEach(sdStockBatch -> stockBatchMap.put(sdStockBatch.getGssbProId(), sdStockBatch));
        return stockBatchMap;
    }

    /**
     * 获取当前订单的所有批号库存表
     *
     * @param inData
     * @return
     */
    private Map<String, List<GaiaSdStockBatch>> getStockBatchMap2(Map<String, Object> map) {
        List<GaiaSdStockBatch> sdStockBatches = this.stockBatchMapper.selectAllByBillNo2(map);
        Map<String, List<GaiaSdStockBatch>> stockBatchMap = new HashMap<>(16);
        for (GaiaSdStockBatch sdStockBatch : sdStockBatches) {
            if (!stockBatchMap.containsKey(sdStockBatch.getGssbProId())) {
                List<GaiaSdStockBatch> batchList = new ArrayList<>();
                batchList.add(sdStockBatch);
                stockBatchMap.put(sdStockBatch.getGssbProId(), batchList);
            } else {
                List<GaiaSdStockBatch> batchList = stockBatchMap.get(sdStockBatch.getGssbProId());
                batchList.add(sdStockBatch);
            }

        }
        return stockBatchMap;
    }


    /**
     * 查询商品主数据
     *
     * @param inData
     * @return
     */
    private Map<String, GaiaProductBusiness> getBusinessMap(SalesInquireData inData, List<String> proList) {
        Map<String, GaiaProductBusiness> businessMap;
        Example examplePro = new Example(GaiaProductBusiness.class);
        examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSite", inData.getBrId()).andIn("proSelfCode", proList);
        List<GaiaProductBusiness> businesses = this.productBusinessMapper.selectByExample(examplePro);
        businessMap = businesses.stream().collect(Collectors.toMap(GaiaProductBusiness::getProSelfCode, business -> business, (a, b) -> b, () -> new HashMap<>(16)));
        return businessMap;
    }

    /**
     * 查询物料评估表
     *
     * @param inData
     * @param proList
     * @return
     */
    private Map<String, GaiaMaterialAssess> getMaterialAssessMap(SalesInquireData inData, List<String> proList) {
        List<GaiaMaterialAssess> assessList = this.assessMapper.getAllByProIds(inData.getClientId(), inData.getBrId(), proList);
        Map<String, GaiaMaterialAssess> map = assessList.stream().collect(Collectors.toMap(GaiaMaterialAssess::getMatProCode, assess -> assess, (a, b) -> b, () -> new HashMap<>(8)));
        return map;
    }


    /**
     * 更新添加 批次,库存,批号库存表等信息
     *
     * @param detailInData
     * @param inData
     * @param billNo
     * @param list
     * @param synStockList
     * @param synstockBatchList
     */
    @Transactional(rollbackFor = Exception.class)
    public void inventory(SalesInquireData inData, String billNo, GaiaSdSaleD saleD,
                          String guid, List<MaterialDocRequestDto> listDto,
                          Map<String, GaiaSdBatchChange> objectMap,
                          Map<String, GaiaSdStock> stockMap,
                          Map<String, GaiaSdStockBatch> stockBatchMap,
                          Map<String, GaiaProductBusiness> businessMap,
                          List<GaiaSdBatchChange> changeList,
                          List<GaiaSdStock> stockList, List<GaiaSdStockBatch> batchList,
                          List<GaiaDataSynchronized> synStockList,
                          List<GaiaDataSynchronized> synstockBatchList,
                          List<GaiaDataSynchronized> synBatchChangeList,
                          Map<String, GaiaSdStock> decimalMap) {

        BigDecimal qty = new BigDecimal(saleD.getGssdQty()).negate();
        // 批号库存表
        for (Map.Entry<String, GaiaSdBatchChange> entry : objectMap.entrySet()) {
            BigDecimal num = BigDecimal.ZERO;
            if (saleD.getGssdProId().equals(entry.getKey()) && saleD.getGssdSerial().equals(entry.getValue().getGsbcSerial())) {
                GaiaProductBusiness productBusiness = businessMap.get(saleD.getGssdProId());
                // 异动批次表
                GaiaSdBatchChange gaiaSdBatchChange = entry.getValue();
                if (ObjectUtil.isEmpty(gaiaSdBatchChange.getGsbcBatch())) {
                    throw new BusinessException("该商品 : " + saleD.getGssdProId() + " 批次异动表没有批次信息!");
                }
                if (qty.compareTo(BigDecimal.ZERO) < 1) {
                    break;
                }

                qty = qty.subtract(gaiaSdBatchChange.getGsbcQty());
                if (qty.compareTo(BigDecimal.ZERO) == 0) {
                    num = gaiaSdBatchChange.getGsbcQty();
                } else if (qty.compareTo(BigDecimal.ZERO) == -1) {
                    num = qty.add(gaiaSdBatchChange.getGsbcQty());
                } else {
                    num = gaiaSdBatchChange.getGsbcQty();
                }
                //更新批次异动表
                GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
                batchChange.setClient(inData.getClientId());
                batchChange.setGsbcBrId(inData.getBrId());
                batchChange.setGsbcVoucherId(billNo);
                batchChange.setGsbcSerial(saleD.getGssdSerial());
                batchChange.setGsbcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                batchChange.setGsbcProId(saleD.getGssdProId());
                batchChange.setGsbcBatchNo(gaiaSdBatchChange.getGsbcBatchNo());
                batchChange.setGsbcBatch(gaiaSdBatchChange.getGsbcBatch());
                batchChange.setGsbcQty(num);
                changeList.add(batchChange);

                Map<String, Object> map = new HashMap<>(16);
                map.put("CLIENT", gaiaSdBatchChange.getClient());
                map.put("GSBC_BR_ID", gaiaSdBatchChange.getGsbcBrId());
                map.put("GSBC_VOUCHER_ID", gaiaSdBatchChange.getGsbcVoucherId());
                map.put("GSBC_DATE", gaiaSdBatchChange.getGsbcDate());
                map.put("GSBC_SERIAL", gaiaSdBatchChange.getGsbcSerial());
                map.put("GSBC_PRO_ID", gaiaSdBatchChange.getGsbcProId());
                map.put("GSBC_BATCH", gaiaSdBatchChange.getGsbcBatch());
                GaiaDataSynchronized aSyncStockBatch = new GaiaDataSynchronized();
                aSyncStockBatch.setClient(inData.getClientId());
                aSyncStockBatch.setSite(inData.getBrId());
                aSyncStockBatch.setVersion(IdUtil.randomUUID());
                aSyncStockBatch.setTableName("GAIA_SD_BATCH_CHANGE");
                aSyncStockBatch.setCommand("update");
                aSyncStockBatch.setSourceNo("退货");
                aSyncStockBatch.setArg(JSON.toJSONString(map));
                synBatchChangeList.add(aSyncStockBatch);

                // 批号库存表
                for (Map.Entry<String, GaiaSdStockBatch> batchEntry : stockBatchMap.entrySet()) {
                    if (saleD.getGssdProId().equals(batchEntry.getKey())) {
                        GaiaSdStockBatch sdStockBatch = batchEntry.getValue();
                        if (ObjectUtil.isEmpty(sdStockBatch)) {
                            throw new BusinessException("该商品 : " + saleD.getGssdProId() + " 门店库存明细表不存在!");
                        }
                        if (gaiaSdBatchChange.getGsbcBatchNo().equals(sdStockBatch.getGssbBatchNo()) && gaiaSdBatchChange.getGsbcBatch().equals(sdStockBatch.getGssbBatch())) {
                            BigDecimal addGssbQty = new BigDecimal(sdStockBatch.getGssbQty()).add(num);
                            sdStockBatch.setGssbQty(NumberUtil.toStr(addGssbQty));
                            sdStockBatch.setGssbUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                            sdStockBatch.setGssbUpdateEmp(inData.getUserId());
                            batchList.add(sdStockBatch);
                        }
                    }
                }


                Map<String, Object> map2 = new HashMap<>(16);
                map2.put("CLIENT", inData.getClientId());
                map2.put("GSSB_BR_ID", inData.getBrId());
                map2.put("GSSB_PRO_ID", saleD.getGssdProId());
                map2.put("GSSB_BATCH_NO", gaiaSdBatchChange.getGsbcBatchNo());
                map2.put("GSSB_BATCH", gaiaSdBatchChange.getGsbcBatch());
                GaiaDataSynchronized aSyncStockBatch2 = new GaiaDataSynchronized();
                aSyncStockBatch2.setClient(inData.getClientId());
                aSyncStockBatch2.setSite(inData.getBrId());
                aSyncStockBatch2.setVersion(IdUtil.randomUUID());
                aSyncStockBatch2.setTableName("GAIA_SD_STOCK_BATCH");
                aSyncStockBatch2.setCommand("update");
                aSyncStockBatch2.setSourceNo("退货");
                aSyncStockBatch2.setArg(JSON.toJSONString(map2));
                synstockBatchList.add(aSyncStockBatch2);

                System.out.println("批次异动表 : " + gaiaSdBatchChange.getGsbcQty());
                if (ObjectUtil.isNotEmpty(productBusiness)) {
                    MaterialDocRequestDto dto = new MaterialDocRequestDto();
                    dto.setClient(inData.getClientId());
                    dto.setGuid(guid);
                    dto.setGuidNo(saleD.getGssdSerial());
                    dto.setMatPostDate(saleD.getGssdDate());
                    dto.setMatHeadRemark("");
                    dto.setMatType("LS");
                    dto.setMatProCode(saleD.getGssdProId());
                    dto.setMatStoCode("");
                    dto.setMatSiteCode(inData.getBrId());
                    dto.setMatLocationCode("1000");
                    dto.setMatLocationTo("");
                    dto.setMatBatch(gaiaSdBatchChange.getGsbcBatch());
                    dto.setMatQty(num.abs());//转正数
                    dto.setMatUnit(productBusiness.getProUnit());
                    dto.setMatDebitCredit("S");
                    dto.setMatPrice(new BigDecimal(0));
                    dto.setMatPoId(billNo);
                    dto.setMatPoLineno(saleD.getGssdSerial());
                    dto.setMatDnId(billNo);
                    dto.setMatDnLineno(saleD.getGssdSerial());
                    dto.setMatCreateBy(inData.getUserId());
                    dto.setMatCreateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    dto.setMatCreateTime(CommonUtil.getHHmmss());
                    listDto.add(dto);
                }
            }
        }

        if (decimalMap.containsKey(saleD.getGssdProId())) {
            GaiaSdStock sdStock = decimalMap.get(saleD.getGssdProId());
            BigDecimal addNum = new BigDecimal(sdStock.getGssQty()).add(new BigDecimal(saleD.getGssdQty()).negate());
            sdStock.setGssQty(NumberUtil.toStr(addNum));
            sdStock.setGssUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
            sdStock.setGssUpdateEmp(inData.getUserId());
        } else {
            //  库存表
            GaiaSdStock stock = stockMap.get(saleD.getGssdProId());
            if (ObjectUtil.isEmpty(stock)) {
                throw new BusinessException("该商品 : " + saleD.getGssdProId() + " 门店库存主表不存在!");
            }
            BigDecimal addNum = new BigDecimal(stock.getGssQty()).add(new BigDecimal(saleD.getGssdQty()).negate());
            stock.setGssQty(NumberUtil.toStr(addNum));
            stock.setGssUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
            stock.setGssUpdateEmp(inData.getUserId());
            decimalMap.put(saleD.getGssdProId(), stock);
        }
    }

    @Override
    public List<GetUserOutData> queryUser(GetSaleScheduleInData inData) {
        GaiaUserData gaiaUserData = new GaiaUserData();
        gaiaUserData.setClient(inData.getClientId());
        gaiaUserData.setDepId(inData.getGsegBrId());
        List<GaiaUserData> out = gaiaUserDataMapper.getAllByClientAndDepId(gaiaUserData);
        List<GetUserOutData> outDataList = new ArrayList();
        out.forEach((item) -> {
            if ("0".equals(item.getUserSta())) {
                GetUserOutData user = new GetUserOutData();
                user.setUserId(item.getUserId());
                user.setLoginName(item.getUserNam());
                outDataList.add(user);
            }

        });
        return outDataList;
    }

    @Override
    public List<GetUserOutData> queryUserNew(GetSaleScheduleInData inData) {
        GaiaUserData gaiaUserData = new GaiaUserData();
        gaiaUserData.setClient(inData.getClientId());
        gaiaUserData.setDepId(inData.getGsegBrId());
        List<GaiaUserData> out = gaiaUserDataMapper.getAllByClientAndDepIdNew(gaiaUserData);
        List<GetUserOutData> outDataList = new ArrayList();
        out.forEach((item) -> {
            if ("0".equals(item.getUserSta())) {
                GetUserOutData user = new GetUserOutData();
                user.setUserId(item.getUserId());
                user.setLoginName(item.getUserNam());
                outDataList.add(user);
            }

        });
        return outDataList;
    }

    @Override
    public List<GetDoctorOutData> queryDoctor(GetLoginOutData inData) {
        List<GetDoctorOutData> outData = this.gaiaAuthconfiDataMapper.queryDoctor(inData);
        return outData;
    }

    @Override
    public int updateReturnBillStatus(ReturnSaleBillInData inData) {
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gsshBrId", inData.getBrId()).andEqualTo("gsshBillNo", inData.getReturnBillNo());
        GaiaSdSaleH saleH = new GaiaSdSaleH();
        if (ObjectUtil.isNotEmpty(saleH)) {
            if (inData.getStatus()) {
                saleH.setGsshReturnStatus("1");
            } else {
                saleH.setGsshReturnStatus("2");
            }
            return this.saleHMapper.updateByExampleSelective(saleH, example);
        }
        return 0;
    }

    @Override
    public List<GaiaSdPayMethodOutData> queryPayMethod(GetLoginOutData userInfo) {
        Example example = new Example(GaiaSdPaymentMethod.class);
        example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("gspmBrId", userInfo.getDepId());
        List<GaiaSdPaymentMethod> paymentMethods = this.methodMapper.selectByExample(example);
        List<GaiaSdPayMethodOutData> outData = CopyUtil.copyList(paymentMethods, GaiaSdPayMethodOutData.class);
        return outData;
    }

    @Override
    public List<ClientStockOutData> queryClientStock(GetQueryProductInData inData) {
        String isShowBatchNo = inData.getIsShowBatchNo();   //是否查询批号效期 0-否 1-是

        List<ClientStockOutData> clientStockOutData = null;
        if("1".equals(isShowBatchNo)) {
            clientStockOutData = stockMapper.queryClientStockByBatchNo(inData);
        } else {
            clientStockOutData = stockMapper.queryClientStock(inData);
        }
        return clientStockOutData;
    }


    @Override
    public List<GetQueryMemberOutData> querySaleMember(Map<String, String> map) {
        String universal = map.get("universal");
        GetQueryMemberOutData member = new GetQueryMemberOutData();
        member.setClientId(map.get("clientId"));
        member.setBrId(map.get("brId"));
        if (StrUtil.isNotBlank(universal)) {
            member.setUniversal(universal);
        }
        return this.memberBasicMapper.queryMemberByCondition(member);
    }

    @Override
    public List<MemberPurchaseRecordOutData> memberPurchaseRecord(Map<String, String> map) {
        if (StrUtil.isBlank(map.get("startDate")) || StrUtil.isBlank(map.get("endDate"))) {
            throw new BusinessException("起始日期或结束日期不可为空!");
        }
        if (StrUtil.isBlank(map.get("memberCardNo"))) {
            throw new BusinessException("会员卡号不可为空!");
        }
        return this.saleDMapper.memberPurchaseRecord(map);
    }

    @Override
    public GetSaleBillInfoData queryBillInfo(Map<String, String> map) {
        GetSaleBillInfoData out = new GetSaleBillInfoData();

        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", map.get("client")).andEqualTo("gsshBrId", map.get("brId")).andEqualTo("gsshBillNo", map.get("billNo"));
        GaiaSdSaleH saleH = this.saleHMapper.selectOneByExample(example);
        out.setGaiaSdSaleH(saleH);

        example = new Example(GaiaSdSaleD.class);
        example.createCriteria().andEqualTo("clientId", map.get("client")).andEqualTo("gssdBrId", map.get("brId")).andEqualTo("gssdBillNo", map.get("billNo"));
        List<GaiaSdSaleD> saleDList = this.saleDMapper.selectByExample(example);
        out.setGaiaSdSaleDList(saleDList);

//        example = new Example(Member.class);
//        example.createCriteria().andEqualTo("clientId", map.get("client")).andEqualTo("gssdBrId", map.get("brId")).andEqualTo("gssdBillNo",map.get("billNo"));
//        List<GaiaSdSaleD> saleDList = this.saleDMapper.selectByExample(example);
//        out.setGaiaSdSaleDList(saleDList);

        return out;
    }

    @Override
    public PendingPrintDto getPendingOrderInformation(Map<String, String> map) {
        List<GetRestOrderInfoOutData> saleHand = new ArrayList<>();
        if ("6".equals(map.get("gsshHideFlag"))) {//推荐销售
            saleHand = this.gaiaSdSaleHandHMapper.getSaleHandForSaleOther(map.get("client"), map.get("brId"), map.get("saleDate"), map.get("billNo"), map.get("gsshHideFlag"));
        } else {
            saleHand = this.gaiaSdSaleHandHMapper.getSaleHand(map.get("client"), map.get("brId"), map.get("saleDate"), map.get("billNo"));
        }
        if (CollUtil.isEmpty(saleHand)) {
            throw new BusinessException("查无此数据!");
        }
        List<RestOrderDetailOutData> saleHandDetail = new ArrayList<>();
        if ("6".equals(map.get("gsshHideFlag"))) {//推荐销售
            saleHandDetail = this.gaiaSdSaleHandDMapper.listRestOrderDetail(map.get("client"), map.get("brId"), map.get("billNo"), map.get("saleDate"), map.get("gsshHideFlag"));
        } else {
            saleHandDetail = this.gaiaSdSaleHandDMapper.getSaleHandDetail(map.get("client"), map.get("brId"), map.get("billNo"), map.get("saleDate"));
        }
        if (CollUtil.isEmpty(saleHandDetail)) {
            throw new BusinessException("查无此数据!");
        }
        GaiaSdPrintSale gaiaSdPrintSale = this.gaiaSdPrintSaleMapper.getPrintSaleByClientAndBrId(map.get("client"), map.get("brId"));
        PendingPrintDto printDto = new PendingPrintDto();
        printDto.setDetail(saleHandDetail);
        printDto.setModule(gaiaSdPrintSale.getGspsModule1());
        printDto.setHEmp(saleHand.get(0).getGsshEmp());
        printDto.setDEmp(saleHandDetail.get(0).getGssdSalerName());
        printDto.setMemberId(saleHand.get(0).getGsshHykNo());
        printDto.setMemberName(saleHand.get(0).getGsshHykName());
        printDto.setSaleYsAmt(saleHand.get(0).getGsshYsAmt());
        printDto.setSaleZkAmt(saleHand.get(0).getGsshZkAmt());
        printDto.setSaleYjAmt(saleHand.get(0).getGsshNormalAmt());
        printDto.setSaleDate(DateUtil.formatDate(DateUtil.parse(saleHandDetail.get(0).getGsshDate())) + " " + DateUtil.formatTime(DateUtil.parse(saleHandDetail.get(0).getGsshTime())));
        return printDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> returnWithoutOrder(Map<String, Object> map, GetLoginOutData userInfo) {
        String table = JSON.toJSONString(map.get("allTable"));
        List<WithoutTheOriginalReturnVo> allTable = JSONObject.parseArray(table, WithoutTheOriginalReturnVo.class);
        if (CollUtil.isEmpty(allTable)) {
            throw new BusinessException("请勿提交数据!");
        }
        List<MaterialDocRequestDto> listDto = new ArrayList<>();//物料凭证
        //生成的退货单号
        String billNo = CommonUtil.generateBillNo(userInfo.getDepId());
        String guid = UUID.randomUUID().toString();
        List<GaiaSdSalePayMsg> payMsgsList = new ArrayList<>();

        GaiaSdSalePayMsg payMsg = new GaiaSdSalePayMsg();
        payMsg.setClient(userInfo.getClient());
        payMsg.setGsspmBrId(userInfo.getDepId());
        payMsg.setGsspmDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        payMsg.setGsspmBillNo(billNo);
        payMsg.setGsspmId((String) map.get("pay_Code"));
        payMsg.setGsspmType("1");
        payMsg.setGsspmName((String) map.get("pay_Name"));
        payMsg.setGsspmAmt(new BigDecimal((String) map.get("pay_ReturnAmt")).negate());
        if ("1000".equals((String) map.get("pay_Code"))) {
            payMsg.setGsspmRmbAmt(new BigDecimal((String) map.get("pay_ReturnAmt")).negate());
        }
        payMsg.setGsspmZlAmt(BigDecimal.ZERO);
        payMsgsList.add(payMsg);

        GaiaSdSaleH sd = new GaiaSdSaleH();
        sd.setClientId(userInfo.getClient());
        sd.setGsshBillNo(billNo);
        sd.setGsshBrId(userInfo.getDepId());
        sd.setGsshDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        sd.setGsshTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
        sd.setGsshEmp(userInfo.getUserId());
        sd.setGsshZkAmt(new BigDecimal("0"));
        sd.setGsshNormalAmt(new BigDecimal((String) map.get("pay_ReturnAmt")).negate());
        sd.setGsshYsAmt(new BigDecimal((String) map.get("pay_ReturnAmt")).negate());
        sd.setGsshRmbZlAmt(new BigDecimal("0"));
        sd.setGsshRmbAmt(new BigDecimal("0"));
        sd.setGsshBillNoReturn(billNo);
        sd.setGsshEmpReturn(userInfo.getUserId());
        sd.setGsshHideFlag("0");
        sd.setGsshCallAllow("1");
        sd.setGsshCallQty("1");
        sd.setHerbalNum("1");
        sd.setGsshDyqAmt(new BigDecimal("0"));
        sd.setGsshIntegralCashAmt(new BigDecimal("0"));
        sd.setGsshDzqdyAmt1(new BigDecimal("0"));
        sd.setGsshDzqdyAmt2(new BigDecimal("0"));
        sd.setGsshDzqdyAmt3(new BigDecimal("0"));
        sd.setGsshDzqdyAmt4(new BigDecimal("0"));
        sd.setGsshDzqdyAmt5(new BigDecimal("0"));


        List<String> proList = allTable.stream().map(WithoutTheOriginalReturnVo::getProCode).collect(Collectors.toList());

        List<Map<String, String>> voList = new ArrayList<>();
        allTable.forEach(returnVo -> {
            Map<String, String> orderKV = new HashMap<>();
            orderKV.put("proCode", returnVo.getProCode());
            orderKV.put("batchNo", returnVo.getProBatchNo());
            voList.add(orderKV);
        });

        SalesInquireData inData = new SalesInquireData();
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());

        map.put("list", voList);
        map.put("client", userInfo.getClient());
        map.put("brId", userInfo.getDepId());
        //批号库存表
        Map<String, List<GaiaSdStockBatch>> stockBatchMap = this.getStockBatchMap2(map);
        //商品主数据
        Map<String, GaiaProductBusiness> businessMap = this.getBusinessMap(inData, proList);
        //获得库存表
        Map<String, GaiaSdStock> stockMap = this.getStockMap(inData, proList);
        //物料评估表
        Map<String, GaiaMaterialAssess> assessMap = this.getMaterialAssessMap(inData, proList);
        //缓存表
        List<GaiaDataSynchronized> synStockList = new ArrayList<>();
        List<GaiaDataSynchronized> synstockBatchList = new ArrayList<>();
        List<GaiaDataSynchronized> synBatchChangeList = new ArrayList<>();
        List<GaiaSdBatchChange> changeList = new ArrayList<>();
        List<GaiaSdStock> stockList = new ArrayList<>();
        List<GaiaSdStockBatch> batchList = new ArrayList<>();
        List<GaiaSdSaleD> sdSaleDList = new ArrayList<>();

        Map<String, GaiaSdStock> decimalMap = new HashMap<>();
        for (WithoutTheOriginalReturnVo returnVo : allTable) {
            GaiaProductBusiness business = businessMap.get(returnVo.getProCode());
            if (ObjectUtil.isEmpty(business)) {
                throw new BusinessException("该商品 : " + returnVo.getProCode() + " 无商品主数据!");
            }
            List<GaiaSdStockBatch> sdStockBatch = stockBatchMap.get(returnVo.getProCode());
            if (CollUtil.isEmpty(sdStockBatch)) {
                throw new BusinessException("该商品 : " + returnVo.getProCode() + " 门店库存明细表不存在!");
            }
            for (GaiaSdStockBatch stockBatch : sdStockBatch) {
                if (stockBatch.getGssbBatchNo().equals(returnVo.getProBatchNo())) {
                    GaiaSdSaleD saleD = new GaiaSdSaleD();
                    saleD.setClientId(userInfo.getClient());
                    saleD.setGssdBillNo(billNo);
                    saleD.setGssdBrId(userInfo.getDepId());
                    saleD.setGssdDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    saleD.setGssdSerial(String.valueOf(returnVo.getIndex()));
                    saleD.setGssdProId(returnVo.getProCode());
                    if (!"3".equals(business.getProStorageArea())) { //西药存批号,效期
                        saleD.setGssdBatchNo(returnVo.getProBatchNo());
                        saleD.setGssdValidDate(returnVo.getProValidity());
                    }
                    saleD.setGssdPrc1(new BigDecimal(returnVo.getProPrice()).negate());
                    saleD.setGssdPrc2(new BigDecimal(returnVo.getProPrice()).negate());
                    saleD.setGssdQty("-" + returnVo.getReturnNum());
                    saleD.setGssdAmt(new BigDecimal(returnVo.getReturnAmt()).negate());
                    saleD.setGssdZkAmt(new BigDecimal("0"));
                    saleD.setGssdZkPm(new BigDecimal("0"));
                    saleD.setGssdSalerId(returnVo.getAssistantNo());
                    saleD.setGssdOriQty(new BigDecimal(returnVo.getReturnNum()));
                    saleD.setGssdDose("1");
                    saleD.setGssdProStatus("退货");
                    saleD.setGssdMovTax(StrUtil.isBlank(returnVo.getTaxRate()) ? BigDecimal.ZERO : new BigDecimal(returnVo.getTaxRate()));
                    GaiaMaterialAssess assess = assessMap.get(returnVo.getProCode());
                    if (ObjectUtil.isNotEmpty(assess)) {
                        saleD.setGssdMovPrices(assess.getMatMovPrice().multiply(new BigDecimal(returnVo.getReturnNum())));//移动平均总价
                        saleD.setGssdTaxRate(assess.getMatMovPrice().multiply(new BigDecimal(returnVo.getReturnNum())).multiply(saleD.getGssdMovTax()));//移动税额
                        saleD.setGssdAddAmt(assess.getMatMovPrice().multiply(new BigDecimal(returnVo.getReturnNum())));//加点后金额
                        saleD.setGssdAddTax(assess.getMatMovPrice().multiply(new BigDecimal(returnVo.getReturnNum())).multiply(saleD.getGssdMovTax()));//加点后税金
                    }
                    sdSaleDList.add(saleD);


                    //更新批次异动表
                    GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
                    batchChange.setClient(saleD.getClientId());
                    batchChange.setGsbcBrId(saleD.getGssdBrId());
                    batchChange.setGsbcVoucherId(billNo);
                    batchChange.setGsbcSerial(saleD.getGssdSerial());
                    batchChange.setGsbcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    batchChange.setGsbcProId(saleD.getGssdProId());
                    batchChange.setGsbcBatchNo(stockBatch.getGssbBatchNo());
                    batchChange.setGsbcBatch(stockBatch.getGssbBatch());
                    batchChange.setGsbcQty(new BigDecimal(saleD.getGssdQty()));
                    batchChange.setGsbcOldQty(new BigDecimal(stockBatch.getGssbQty()));
                    changeList.add(batchChange);

                    // 批号库存表
                    BigDecimal addGssbQty = new BigDecimal(stockBatch.getGssbQty()).add(new BigDecimal(returnVo.getReturnNum()));
                    stockBatch.setGssbQty(NumberUtil.toStr(addGssbQty));
                    stockBatch.setGssbUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    stockBatch.setGssbUpdateEmp(userInfo.getUserId());
                    batchList.add(stockBatch);


                    //同步表
                    Map<String, Object> map1 = new HashMap<>(16);
                    map1.put("CLIENT", batchChange.getClient());
                    map1.put("GSBC_BR_ID", batchChange.getGsbcBrId());
                    map1.put("GSBC_VOUCHER_ID", batchChange.getGsbcVoucherId());
                    map1.put("GSBC_DATE", batchChange.getGsbcDate());
                    map1.put("GSBC_SERIAL", batchChange.getGsbcSerial());
                    map1.put("GSBC_PRO_ID", batchChange.getGsbcProId());
                    map1.put("GSBC_BATCH", batchChange.getGsbcBatch());
                    GaiaDataSynchronized aSyncStockBatch = new GaiaDataSynchronized();
                    aSyncStockBatch.setClient(userInfo.getClient());
                    aSyncStockBatch.setSite(userInfo.getDepId());
                    aSyncStockBatch.setVersion(IdUtil.randomUUID());
                    aSyncStockBatch.setTableName("GAIA_SD_BATCH_CHANGE");
                    aSyncStockBatch.setCommand("update");
                    aSyncStockBatch.setSourceNo("退货");
                    aSyncStockBatch.setArg(JSON.toJSONString(map1));
                    synBatchChangeList.add(aSyncStockBatch);


                    Map<String, Object> map2 = new HashMap<>(16);
                    map2.put("CLIENT", userInfo.getClient());
                    map2.put("GSSB_BR_ID", userInfo.getDepId());
                    map2.put("GSSB_PRO_ID", saleD.getGssdProId());
                    map2.put("GSSB_BATCH_NO", batchChange.getGsbcBatchNo());
                    map2.put("GSSB_BATCH", batchChange.getGsbcBatch());
                    GaiaDataSynchronized aSyncStockBatch2 = new GaiaDataSynchronized();
                    aSyncStockBatch2.setClient(userInfo.getClient());
                    aSyncStockBatch2.setSite(userInfo.getDepId());
                    aSyncStockBatch2.setVersion(IdUtil.randomUUID());
                    aSyncStockBatch2.setTableName("GAIA_SD_STOCK_BATCH");
                    aSyncStockBatch2.setCommand("update");
                    aSyncStockBatch2.setSourceNo("退货");
                    aSyncStockBatch2.setArg(JSON.toJSONString(map2));
                    synstockBatchList.add(aSyncStockBatch2);

                    MaterialDocRequestDto dto = new MaterialDocRequestDto();
                    dto.setClient(userInfo.getClient());
                    dto.setGuid(guid);
                    dto.setGuidNo(saleD.getGssdSerial());
                    dto.setMatPostDate(saleD.getGssdDate());
                    dto.setMatHeadRemark("");
                    dto.setMatType("LS");
                    dto.setMatProCode(saleD.getGssdProId());
                    dto.setMatStoCode("");
                    dto.setMatSiteCode(userInfo.getDepId());
                    dto.setMatLocationCode("1000");
                    dto.setMatLocationTo("");
                    dto.setMatBatch(batchChange.getGsbcBatch());
                    dto.setMatQty(batchChange.getGsbcQty().abs());//转正数
                    dto.setMatUnit(business.getProUnit());
                    dto.setMatDebitCredit("S");
                    dto.setMatPrice(new BigDecimal(0));
                    dto.setMatPoId(billNo);
                    dto.setMatPoLineno(saleD.getGssdSerial());
                    dto.setMatDnId(billNo);
                    dto.setMatDnLineno(saleD.getGssdSerial());
                    dto.setMatCreateBy(userInfo.getUserId());
                    dto.setMatCreateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    dto.setMatCreateTime(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    listDto.add(dto);

                    if (decimalMap.containsKey(returnVo.getProCode())) {
                        GaiaSdStock sdStock = decimalMap.get(returnVo.getProCode());
                        BigDecimal addNum = new BigDecimal(sdStock.getGssQty()).add(new BigDecimal(returnVo.getReturnNum()));
                        sdStock.setGssQty(NumberUtil.toStr(addNum));
                        sdStock.setGssUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                        sdStock.setGssUpdateEmp(userInfo.getUserId());
                    } else {
                        //  库存表
                        GaiaSdStock stock = stockMap.get(returnVo.getProCode());
                        if (ObjectUtil.isEmpty(stock)) {
                            throw new BusinessException("该商品 : " + returnVo.getProCode() + " 门店库存主表不存在!");
                        }
                        BigDecimal addNum = new BigDecimal(stock.getGssQty()).add(new BigDecimal(returnVo.getReturnNum()));
                        stock.setGssQty(NumberUtil.toStr(addNum));
                        stock.setGssUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                        stock.setGssUpdateEmp(userInfo.getUserId());
                        decimalMap.put(returnVo.getProCode(), stock);
                    }
                }
            }
        }

        Set<Map.Entry<String, GaiaSdStock>> entries = decimalMap.entrySet();
        for (Map.Entry<String, GaiaSdStock> entry : entries) {
            GaiaSdStock value = entry.getValue();
            Map<String, Object> map3 = new HashMap<>(16);
            map3.put("CLIENT", userInfo.getClient());
            map3.put("GSS_BR_ID", userInfo.getDepId());
            map3.put("GSS_PRO_ID", value.getGssProId());
            GaiaDataSynchronized aSyncStock = new GaiaDataSynchronized();
            aSyncStock.setClient(userInfo.getClient());
            aSyncStock.setSite(userInfo.getDepId());
            aSyncStock.setVersion(IdUtil.randomUUID());
            aSyncStock.setTableName("GAIA_SD_STOCK");
            aSyncStock.setCommand("update");
            aSyncStock.setSourceNo("退货");
            aSyncStock.setArg(JSON.toJSONString(map3));
            synStockList.add(aSyncStock);
            stockList.add(value);
        }
        if (CollUtil.isEmpty(sdSaleDList) || CollUtil.isEmpty(changeList) || CollUtil.isEmpty(stockList) || CollUtil.isEmpty(batchList)) {
            throw new BusinessException("此单可能存在一些异常!!!");
        }

        this.saleHMapper.insert(sd);
        this.saleDMapper.insertLists(sdSaleDList);//批量插入D表
        this.payMsgMapper.insertLists(payMsgsList);
        this.batchChangeMapper.insertLists(changeList);//批量插入 批次异动表
        this.stockMapper.updateList(stockList);//批量更新库存表
        this.stockBatchMapper.updateList(batchList);//批量更新批号库存表

        //调外卖平台更新库存接口; GaiaSdStock是库存总表
        if (!CollectionUtils.isEmpty(stockList)) {
            stockService.syncStock(stockList);
        }
        this.synchronizedService.insertLists(synStockList);
        this.synchronizedService.insertLists(synstockBatchList);
        this.synchronizedService.insertLists(synBatchChangeList);

        Map<String, Object> resultMap = new HashMap<>();
        //物料凭证
        if (CollUtil.isNotEmpty(listDto)) {
            resultMap.put("listDto", listDto);
        }
        resultMap.put("billNo", billNo);
        return resultMap;
    }

    @Override
    public List<GaiaWmsHudiaoZOutData> listAdjustBill(GaiaWmsHudiaoZOutData param) {
        List<GaiaWmsHudiaoZOutData> result = gaiaWmsHudiaoZMapper.listAdjustBill(param);
        if (!CollectionUtils.isEmpty(result)) {
            int index = 1;
            for (GaiaWmsHudiaoZOutData data : result) {
                data.setWmXh1(String.valueOf(index));
                index++;
            }
        }
        return result;
    }

    @Override
    public List<GaiaWmsHudiaoZOutData> listAdjustBillDetail(GaiaWmsHudiaoZOutData param) {
        return gaiaWmsHudiaoZMapper.listAdjustBillDetail(param);
    }

    @Override
    @Transactional
    public void dataSubServer(DataSubServer param) {
        log.info("FX本地数据同步服务器 数据同步 dataSubServer: START");
        if (ObjectUtil.isEmpty(param)) {
            log.info("FX本地数据同步服务器 数据同步 dataSubServer: 没有查询到销售数据");
            log.info("FX本地数据同步服务器 数据同步 dataSubServer: END");
        }
        List<GaiaSdSaleHLocal> saleHList = param.getSaleHList();
        if (CollUtil.isNotEmpty(saleHList)) {
            log.info("FX本地数据同步服务器 数据同步 dataSubServer: saleHList 数量: " + saleHList.size());
            saleHLocalMapper.insertOrUpdate(saleHList);
        }
        List<GaiaSdSaleDLocal> saleDList = param.getSaleDList();
        if (CollUtil.isNotEmpty(saleDList)) {
            log.info("FX本地数据同步服务器 数据同步 dataSubServer: saleDList 数量: " + saleDList.size());
            saleDLocalMapper.insertOrUpdate(saleDList);
        }
        List<GaiaSdSalePayMsgLocal> salePayMsgList = param.getSalePayMsgList();
        if (CollUtil.isNotEmpty(salePayMsgList)) {
            log.info("FX本地数据同步服务器 数据同步 dataSubServer: salePayMsgList 数量: " + salePayMsgList.size());
            salePayMsgLocalMapper.insertOrUpdate(salePayMsgList);
        }
        List<GaiaSdBatchChangeLocal> batchChangeList = param.getBatchChangeList();
        if (CollUtil.isNotEmpty(batchChangeList)) {
            log.info("FX本地数据同步服务器 数据同步 dataSubServer: batchChangeList 数量: " + batchChangeList.size());
            batchChangeLocalMapper.insertOrUpdate(batchChangeList);
        }
        log.info("FX本地数据同步服务器 数据同步 dataSubServer: END");
    }

    @Override
    public void dataSubServerRequest(InData param) {
        if (StrUtil.isEmpty(param.getClient())) {
            throw new BusinessException("请输入加盟商");
        }
        if (StrUtil.isEmpty(param.getStoCode()) && ObjectUtil.isEmpty(param.getStoArr())) {
            throw new BusinessException("请输入门店");
        }
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("dataSubServerSale");
        mqReceiveData.setCmd("dataSubServerSale");
        mqReceiveData.setData(param);
        Object pushData = JSON.toJSON(mqReceiveData);
        if (StrUtil.isNotEmpty(param.getStoCode())) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, param.getClient() + "." + param.getStoCode(), pushData);
            log.info("推送销售同步信息，client:{}，brId:{}，pushData:{}", param.getClient(), param.getStoCode(), JSON.toJSONString(pushData));
        } else if (ObjectUtil.isNotEmpty(param.getStoArr())) {
            for (String brId : param.getStoArr()) {
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, param.getClient() + "." + brId, pushData);
                log.info("推送销售同步信息，client:{}，brId:{}，pushData:{}", param.getClient(), brId, JSON.toJSONString(pushData));
            }
        }
    }

    @Override
    public List<GetQueryProductOutData> listOtherStoreProduct(HashMap<String, String> param) {
        return saleDMapper.listOtherStoreProduct(param);
    }

    @Override
    public List<GetPdAndExpOutData> queryStockAndExpGroupByArea(HashMap<String, String> param) {
        return saleDMapper.queryStockAndExpGroupByArea(param);
    }

    @Override
    public List<GetPdAndExpOutData> queryStockAndExpGroupByBatchNo(HashMap<String, String> param) {
        return saleDMapper.queryStockAndExpGroupByBatchNo(param);
    }

    @Override
    public GetSalesReceiptsTableOutData queryOtherProductDetail(HashMap<String, String> param) {
        return saleDMapper.queryOtherProductDetail(param);
    }

    @Override
    public boolean commercePlatform(GetLoginOutData userInfo, HashMap<String, String> param) {
        List<GaiaSdElectronBusinessSet> list =  gaiaSdElectronChangeMapper.queryElectionByDate(userInfo.getClient(),userInfo.getDepId());
        if (CollUtil.isEmpty(list)) {
            return true;
        }
        List<GaiaSdElectronChange> electronChangeList = this.gaiaSdElectronChangeMapper.listElectronChange(userInfo.getClient(), param.get("billNo"));
        return CollUtil.isNotEmpty(electronChangeList) ? true : false;
    }

    @Override
    public List<GetQueryProductOutData> listOtherStoreProductBatch(HashMap<String, String> param) {
        return saleDMapper.listOtherStoreProductBatch(param);
    }


    @Override
    public List<RestOrderDetailOutData> listControlGoodsQtyByIdCard(Map<String, Object> map) {
        return saleDMapper.listControlGoodsQtyByIdCard(map.get("client").toString(), (List<String>) map.get("idCardList"));
    }


    @Override
    public void updateHideCommonRemark(List<HashMap<String, String>> param) {
        gaiaHideCommonRemarkMapper.updateHideCommonRemark(param);
    }

    @Override
    public List<GaiaHideCommonRemark> listHideCommonRemark(HashMap<String, String> param) {
        return gaiaHideCommonRemarkMapper.listHideCommonRemark(param);
    }

    @Override
    public void saveHideRemark(HashMap<String, Object> param) {
        GaiaHideCommonRemark remark = GaiaHideCommonRemark.builder()
                .content(NullUtil.defaultNull(param.get("content"),"").toString())
                .client(NullUtil.defaultNull(param.get("client"),"").toString())
                .stoCode(NullUtil.defaultNull(param.get("stoCode"),"").toString())
                .createTime(DateUtil.format(DateUtil.date(),DatePattern.PURE_DATETIME_PATTERN))
                .deleteFlag("0")
                .build();
        gaiaHideCommonRemarkMapper.saveHideRemark(remark);
    }
}
