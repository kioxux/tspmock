package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 小票传参实体
 *
 * @author xiaoyuan on 2020/8/3
 */
@Data
public class ReceiptInData implements Serializable {
    private static final long serialVersionUID = 1806550026618380378L;

    /**
     * 单号
     */
    private String saleBillNo;

    /**
     * 加盟号
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

}
