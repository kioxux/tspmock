package com.gys.business.service.data;

import lombok.Data;

@Data
public class CompadmOutData {
   private String client;
   private String compadmId;
   private String compadmName;
   private String compadmNo;
   private String compadmLegalPersonName;
   private String compadmLegalPersonPhone;
   private String compadmLegalPerson;
   private String compadmQuaName;
   private String compadmQua;
   private String compadmQuaPhone;
   private String compadmAddr;
   private String compadmCreDate;
   private String compadmCreTime;
   private String compadmCreId;
   private String compadmModiDate;
   private String compadmModiTime;
   private String compadmModiId;
   private String compadmLogo;
   private String compadmLogoUrl;
   private String compadmStatus;
}
