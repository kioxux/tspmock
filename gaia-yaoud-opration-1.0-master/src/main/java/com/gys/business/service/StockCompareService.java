package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;

public interface StockCompareService {

    void compare(StockCompareCondition condition, GetLoginOutData userInfo);

    ClientSiteChoose clientSite();

    PageInfo<StockCompareRes> getStockCompareListPage(StockCompareCondition condition);

    Result export(StockCompareCondition condition);

    PageInfo<StockDiffRes> getStockDiffListPage(StockDiffCondition condition);

    Result detailListExport(StockDiffCondition condition);
}
