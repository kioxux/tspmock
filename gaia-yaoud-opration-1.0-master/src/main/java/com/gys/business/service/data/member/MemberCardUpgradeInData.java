package com.gys.business.service.data.member;

import com.gys.business.service.data.GaiaSdIntegralChange;
import com.gys.business.service.data.GetQueryMemberOutData;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/16 13:38
 * @Version 1.0.0
 **/
@Data
public class MemberCardUpgradeInData {
    // 会员信息
    GetQueryMemberOutData memberInfo;
    //  积分异动
    List<GaiaSdIntegralChange> integralChanges;
}
