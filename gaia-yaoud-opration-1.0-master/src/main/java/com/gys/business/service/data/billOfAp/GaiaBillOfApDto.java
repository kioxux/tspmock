package com.gys.business.service.data.billOfAp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 供应商应付明细清单表
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
public class GaiaBillOfApDto implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "DC编码")
    @NotBlank(message = "DC编码不能为空")
    private String dcCode;

    @ApiModelProperty(value = "应付方式编码")
    @NotBlank(message = "应付方式编码不能为空")
    private String paymentId;

    @ApiModelProperty(value = "发生金额")
//    @NotNull(message = "发生金额不能为空")
    @DecimalMax("999999999")
    private BigDecimal amountOfPayment;

    @ApiModelProperty(value = "去税金额")
    private BigDecimal amountExcludingTax;

    @ApiModelProperty(value = "税额")
    private BigDecimal amountOfTax;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "发生日期")
    private LocalDate paymentDate;

    @ApiModelProperty(value = "供应商自编码")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商编码")
    private String supCode;

    @ApiModelProperty(value = "供应商名称")
    private String supName;

    @ApiModelProperty(value = "业务员名称")
    private String salesmanName;
    @ApiModelProperty(value = "业务员名称")
    private String salesmanId;
}
