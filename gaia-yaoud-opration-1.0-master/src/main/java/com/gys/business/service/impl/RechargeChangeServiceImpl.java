package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.mapper.RechargeChangeMapper;
import com.gys.business.service.RechargeChangeService;
import com.gys.business.service.data.RechargeChangeInData;
import com.gys.business.service.data.RechargeChangeOutData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

@Service
@Slf4j
public class RechargeChangeServiceImpl implements RechargeChangeService {

    @Autowired
    private RechargeChangeMapper rechargeChangeMapper;

    @Autowired
    private CosUtils cosUtils;

    @Override
    public List<RechargeChangeOutData> rechargeChangePage(RechargeChangeInData inData, GetLoginOutData userInfo) {
        inData.setClient(userInfo.getClient());
        List<RechargeChangeOutData> outDataList = this.rechargeChangeMapper.rechargeChangePage(inData);
        if (CollUtil.isNotEmpty(outDataList)) {
            IntStream.range(0, outDataList.size()).forEach(i -> outDataList.get(i).setIndex(i + 1));
        }
        return outDataList;
    }

    @Override
    public Result export(RechargeChangeInData inData, HttpServletResponse response, GetLoginOutData userInfo) {
        inData.setClient(userInfo.getClient());
        //查询要导出的数据
        List<RechargeChangeOutData> rechargeChangeOutDataList = this.rechargeChangeMapper.rechargeChangePage(inData);
        String fileName = CommonConstant.RECHARGE_CHANGE_EXPORT_FILE_NAME + "-";
        Result result = null;
        Integer index = 1;
        for (RechargeChangeOutData rechargeChangeOutData:rechargeChangeOutDataList) {
            rechargeChangeOutData.setIndex(index);
            index++;
            if (rechargeChangeOutData.getGsrcSex() == "0") {
                rechargeChangeOutData.setGsrcSex("女");
            }else {
                rechargeChangeOutData.setGsrcSex("男");
            }

            switch (rechargeChangeOutData.getGsrcType()){
                case "1":
                    rechargeChangeOutData.setGsrcType("充值");
                    break;
                case "2":
                    rechargeChangeOutData.setGsrcType("退款");
                    break;
                case "3":
                    rechargeChangeOutData.setGsrcType("消费");
                    break;
                case "4":
                    rechargeChangeOutData.setGsrcType("退货");
                    break;
            }
        }

        CsvFileInfo csvInfo = CsvClient.getCsvByte(rechargeChangeOutDataList, fileName, Collections.singletonList((short) 1));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bos.write(csvInfo.getFileContent());
            result = this.cosUtils.uploadFile(bos, csvInfo.getFileName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
