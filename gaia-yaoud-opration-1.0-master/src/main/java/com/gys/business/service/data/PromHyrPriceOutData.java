//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromHyrPriceOutData {
    private String clientId;
    private String gsphpVoucherId;
    private String gsphpSerial;
    private String gsphpProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private BigDecimal gsphpPrice;
    private String gsphpRebate;
    private String gsphpInteFlag;
    private String gsphpInteRate;

    public PromHyrPriceOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsphpVoucherId() {
        return this.gsphpVoucherId;
    }

    public String getGsphpSerial() {
        return this.gsphpSerial;
    }

    public String getGsphpProId() {
        return this.gsphpProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public BigDecimal getGsphpPrice() {
        return this.gsphpPrice;
    }

    public String getGsphpRebate() {
        return this.gsphpRebate;
    }

    public String getGsphpInteFlag() {
        return this.gsphpInteFlag;
    }

    public String getGsphpInteRate() {
        return this.gsphpInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsphpVoucherId(final String gsphpVoucherId) {
        this.gsphpVoucherId = gsphpVoucherId;
    }

    public void setGsphpSerial(final String gsphpSerial) {
        this.gsphpSerial = gsphpSerial;
    }

    public void setGsphpProId(final String gsphpProId) {
        this.gsphpProId = gsphpProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGsphpPrice(final BigDecimal gsphpPrice) {
        this.gsphpPrice = gsphpPrice;
    }

    public void setGsphpRebate(final String gsphpRebate) {
        this.gsphpRebate = gsphpRebate;
    }

    public void setGsphpInteFlag(final String gsphpInteFlag) {
        this.gsphpInteFlag = gsphpInteFlag;
    }

    public void setGsphpInteRate(final String gsphpInteRate) {
        this.gsphpInteRate = gsphpInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromHyrPriceOutData)) {
            return false;
        } else {
            PromHyrPriceOutData other = (PromHyrPriceOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gsphpVoucherId = this.getGsphpVoucherId();
                Object other$gsphpVoucherId = other.getGsphpVoucherId();
                if (this$gsphpVoucherId == null) {
                    if (other$gsphpVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsphpVoucherId.equals(other$gsphpVoucherId)) {
                    return false;
                }

                Object this$gsphpSerial = this.getGsphpSerial();
                Object other$gsphpSerial = other.getGsphpSerial();
                if (this$gsphpSerial == null) {
                    if (other$gsphpSerial != null) {
                        return false;
                    }
                } else if (!this$gsphpSerial.equals(other$gsphpSerial)) {
                    return false;
                }

                label134: {
                    Object this$gsphpProId = this.getGsphpProId();
                    Object other$gsphpProId = other.getGsphpProId();
                    if (this$gsphpProId == null) {
                        if (other$gsphpProId == null) {
                            break label134;
                        }
                    } else if (this$gsphpProId.equals(other$gsphpProId)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label127;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$gspgSpecs = this.getGspgSpecs();
                    Object other$gspgSpecs = other.getGspgSpecs();
                    if (this$gspgSpecs == null) {
                        if (other$gspgSpecs == null) {
                            break label120;
                        }
                    } else if (this$gspgSpecs.equals(other$gspgSpecs)) {
                        break label120;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                label106: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label106;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label106;
                    }

                    return false;
                }

                Object this$gsphpPrice = this.getGsphpPrice();
                Object other$gsphpPrice = other.getGsphpPrice();
                if (this$gsphpPrice == null) {
                    if (other$gsphpPrice != null) {
                        return false;
                    }
                } else if (!this$gsphpPrice.equals(other$gsphpPrice)) {
                    return false;
                }

                label92: {
                    Object this$gsphpRebate = this.getGsphpRebate();
                    Object other$gsphpRebate = other.getGsphpRebate();
                    if (this$gsphpRebate == null) {
                        if (other$gsphpRebate == null) {
                            break label92;
                        }
                    } else if (this$gsphpRebate.equals(other$gsphpRebate)) {
                        break label92;
                    }

                    return false;
                }

                Object this$gsphpInteFlag = this.getGsphpInteFlag();
                Object other$gsphpInteFlag = other.getGsphpInteFlag();
                if (this$gsphpInteFlag == null) {
                    if (other$gsphpInteFlag != null) {
                        return false;
                    }
                } else if (!this$gsphpInteFlag.equals(other$gsphpInteFlag)) {
                    return false;
                }

                Object this$gsphpInteRate = this.getGsphpInteRate();
                Object other$gsphpInteRate = other.getGsphpInteRate();
                if (this$gsphpInteRate == null) {
                    if (other$gsphpInteRate != null) {
                        return false;
                    }
                } else if (!this$gsphpInteRate.equals(other$gsphpInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromHyrPriceOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsphpVoucherId = this.getGsphpVoucherId();
        result = result * 59 + ($gsphpVoucherId == null ? 43 : $gsphpVoucherId.hashCode());
        Object $gsphpSerial = this.getGsphpSerial();
        result = result * 59 + ($gsphpSerial == null ? 43 : $gsphpSerial.hashCode());
        Object $gsphpProId = this.getGsphpProId();
        result = result * 59 + ($gsphpProId == null ? 43 : $gsphpProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gsphpPrice = this.getGsphpPrice();
        result = result * 59 + ($gsphpPrice == null ? 43 : $gsphpPrice.hashCode());
        Object $gsphpRebate = this.getGsphpRebate();
        result = result * 59 + ($gsphpRebate == null ? 43 : $gsphpRebate.hashCode());
        Object $gsphpInteFlag = this.getGsphpInteFlag();
        result = result * 59 + ($gsphpInteFlag == null ? 43 : $gsphpInteFlag.hashCode());
        Object $gsphpInteRate = this.getGsphpInteRate();
        result = result * 59 + ($gsphpInteRate == null ? 43 : $gsphpInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromHyrPriceOutData(clientId=" + this.getClientId() + ", gsphpVoucherId=" + this.getGsphpVoucherId() + ", gsphpSerial=" + this.getGsphpSerial() + ", gsphpProId=" + this.getGsphpProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gsphpPrice=" + this.getGsphpPrice() + ", gsphpRebate=" + this.getGsphpRebate() + ", gsphpInteFlag=" + this.getGsphpInteFlag() + ", gsphpInteRate=" + this.getGsphpInteRate() + ")";
    }
}
