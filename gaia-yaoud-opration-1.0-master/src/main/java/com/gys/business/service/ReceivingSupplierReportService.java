package com.gys.business.service;

import com.gys.business.service.data.SupplierRecordInData;
import com.gys.business.service.data.SupplierRecordOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface ReceivingSupplierReportService {

    /**
     * 条件查询
     * @param inData
     * @return
     */
    List<SupplierRecordOutData> findReportRecord(SupplierRecordInData inData);
}
