package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductLimitInReq implements Serializable {

    private static final long serialVersionUID = -3820333044437683837L;

    private List<ProductLimitInData> dataList;

//    private List<String> sites;
}
