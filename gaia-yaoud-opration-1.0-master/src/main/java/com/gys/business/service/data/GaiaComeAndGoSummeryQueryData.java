package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 13:16 2021/7/22
 * @Description：往来管理汇总表查询条件
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoSummeryQueryData {

    @ApiModelProperty(value = "用户")
    private String client;

    @ApiModelProperty(value = "仓库编码")
    private String dcCode;

    @ApiModelProperty(value = "门店编码")
    private List<String> stoCodeList;

    @ApiModelProperty(value = "开始日期")
    private String startDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;

    private List<Map<String,String>> paymentMethods;

}
