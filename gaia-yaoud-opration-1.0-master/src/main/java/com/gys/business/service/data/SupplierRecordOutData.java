package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回实体
 *
 * @author xiaoyuan on 2020/9/10
 */
@Data
public class SupplierRecordOutData implements Serializable {
    private static final long serialVersionUID = 4991200049728559872L;

    @ApiModelProperty(value = "收货单号")
    private String gsahVoucherId;

    @ApiModelProperty(value = "收货日期")
    private String gsahDate;

    @ApiModelProperty(value = "商品编码")
    private String gsadProId;

    @ApiModelProperty(value = "批号")
    private String gsadBatchNo;

    @ApiModelProperty(value = "有效期")
    private String gsadValidDate;

    @ApiModelProperty(value = "到货数量")
    private String gsadRecipientQty;

    @ApiModelProperty(value = "发票号")
    private String gsadTxNo;

    @ApiModelProperty(value = "折扣额")
    private String gsadRowRemark;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "厂家")
    private String proFactoryName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "税率")
    private String poRate;

    @ApiModelProperty(value = "订单行金额")
    private String poLineAmt;

    @ApiModelProperty(value = "单价")
    private String poPrice;

    @ApiModelProperty(value = "供应商名称")
    private String supName;
}
