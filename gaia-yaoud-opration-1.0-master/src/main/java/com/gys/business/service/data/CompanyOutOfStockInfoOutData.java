package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class CompanyOutOfStockInfoOutData implements Serializable {

    /**
     * 门店数量
     */
    @ApiModelProperty(value = "门店数量")
    private Integer storeCount;

    /**
     * 缺断货品项
     */
    @ApiModelProperty(value = "缺断货品项")
    private Integer outOfStockCount;

    /**
     * 月均销售额
     */
    @ApiModelProperty(value = "月均销售额")
    private BigDecimal avgSaleAmt;

    /**
     * 月均毛利额
     */
    @ApiModelProperty(value = "月均毛利额")
    private BigDecimal avgProfitAmt;

    /**
     * 是否自动推送 1-自动推送（勾选） 2-手动推送（未勾选）
     */
    @ApiModelProperty(value = "是否勾选 1-自动推送（勾选） 2-手动推送（未勾选）")
    private String checkFlag;

    /**
     * 推送标志 null-所有 2-直营店 3-加盟店 4-其他
     */
    @ApiModelProperty(value = "推送标志 null-所有 2-直营店 3-加盟店 4-其他")
    private String pushFlag;

    /**
     * 门店明细列表
     */
    @ApiModelProperty(value = "门店明细列表")
    private List<CompanyOutOfStockDetailOutData> outOfStockDetail;


}
