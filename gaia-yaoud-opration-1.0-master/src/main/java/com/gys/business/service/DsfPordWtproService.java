package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.DsfPordWtproInData;
import com.gys.business.service.data.DsfPordWtproModInData;
import com.gys.business.service.data.DsfPordWtproOutData;

public interface DsfPordWtproService {

    /**
     *  查询委托配送商品对码信息
     * @param inData
     * @return
     */
    public PageInfo<DsfPordWtproOutData> selectDsfPordWtproList(DsfPordWtproInData inData);

    /**
     *  委托配送商品对码
     * @param inData
     * @return
     */
    void updateWtpro(DsfPordWtproModInData inData);

    /**
     *  查询委单据未对码委托配送商品
     * @param inData
     * @return
     */
    PageInfo<DsfPordWtproOutData> selectDsfPordWtpsdList(DsfPordWtproInData inData);
    /**
     *  委单据未对码提醒
     * @param
     * @return
     */
    void autoMatchCodeNotice();
}
