package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LevelInfoOutData  {
    /**
     * 等级名称
     */
    private String levelName;
    /**
     * 等级code
     */
    private String levelCode;
    /**
     * 店数
     */
    private String qty;
    /**
     * 店均计划销售额
     */
    private BigDecimal averageSalesAmt;
    /**
     * 店均计划销售量
     */
    private BigDecimal averageSalesQty;

}
