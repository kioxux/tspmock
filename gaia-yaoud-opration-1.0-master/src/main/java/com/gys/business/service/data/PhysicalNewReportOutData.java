package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 陈浩
 */
@Data
public class PhysicalNewReportOutData {
    @ApiModelProperty(value = "盘点数量")
    private BigDecimal pcFirstQty;

    @ApiModelProperty(value = "盘点差异")
    private BigDecimal pcSecondQty;

    @ApiModelProperty(value = "复盘差点差异")
    private BigDecimal diffSecondQty;

    @ApiModelProperty(value = "商品ID")
    private String proId;

    @ApiModelProperty(value = "加权移动成本价")
    private BigDecimal movPrice;

    @ApiModelProperty(value = "物料凭证金额")
    private BigDecimal movAmt;

    @ApiModelProperty(value = "商品零售价")
    private BigDecimal priceNormal;

    @ApiModelProperty(value = "总库存")
    private BigDecimal matTotalQty = BigDecimal.ZERO;

    @ApiModelProperty(value = "总金额")
    private BigDecimal matTotalAmt = BigDecimal.ZERO;

    @ApiModelProperty(value = "税金")
    private BigDecimal matRateAmt = BigDecimal.ZERO;

    @ApiModelProperty(value = "采购单价(入库收货价)")
    private BigDecimal poPrice = BigDecimal.ZERO;
}
