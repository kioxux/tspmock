package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/24 22:01
 */
@Data
public class DcReplenishVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "紧急需求单号", example = "JCD202104001")
    private String voucherId;
    @ApiModelProperty(value = "紧急需求时间", example = "2021-06-01 12:00:00")
    private String createTime;
    @ApiModelProperty(value = "处理时间", example = "2021-06-01 14:23:56")
    private String processTime;
    @ApiModelProperty(value = "需求品项量", example = "13")
    private Integer requestNum;
    @ApiModelProperty(value = "处理状态：0-未处理 1-已处理", example = "1")
    private Integer status;
    @ApiModelProperty(value = "颜色类型：0-无 1-黄色", example = "1")
    private Integer colorType;
}
