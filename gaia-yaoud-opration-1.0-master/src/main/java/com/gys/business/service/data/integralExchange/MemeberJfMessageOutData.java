package com.gys.business.service.data.integralExchange;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class MemeberJfMessageOutData {
    /**
     * 0：失败  1：成功
     */
    private String Code;
    private String Message;
    private String integralType;//类型 0-单次固定，1-最优
    private BigDecimal integralMaxAmt;//本单抵用上限
    private String memberName;//会员姓名
    private BigDecimal integral;//可用积分
    private BigDecimal maxIntegral;//最大抵用积分
    private List<IntegralRule> integralRules;//抵现规则列表
    private BigDecimal usableIntegral;
    private String gsicsIntegralAddSet;
    private BigDecimal creditUpperLimit;
}
