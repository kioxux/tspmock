package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author flynn
 * @since 2021-09-07
 */
public interface IMessageTemplateService {

    //新增初始化
    Map<String, Object> buildInit(GetLoginOutData userInfo);

    //新增
    Object build(GetLoginOutData userInfo, MessageTemplateInData inData);

    //修改初始化
    Map<String, Object> updateInit(GetLoginOutData userInfo, String id);

    //修改
    Object update(GetLoginOutData userInfo, MessageTemplateInData updateVo);

    //删除
    int delete(GetLoginOutData userInfo, String id);

    //详情
    Object getDetailById(GetLoginOutData userInfo, Integer id);


    //获取分页列表
    PageInfo getListPage(GetLoginOutData userInfo, MessageTemplateInData inData);

    Object changeStatus(GetLoginOutData userInfo, MessageTemplateInData inData);

    List<MessageTemplateRes> storeListPage(GetLoginOutData userInfo, MessageTemplateInData inData);

    Map<String,Object> chooseUser(GetLoginOutData userInfo, String id);

    Object storeChooseUserSave(GetLoginOutData userInfo, MessageTemplateChooseUserInData inData);

    Object storeChooseUserQuery(GetLoginOutData userInfo, MessageTemplateQueryUserInData inData);

    List<CommonVo> chooseUserBuildInit(GetLoginOutData userInfo);

    void runEffect();

}
