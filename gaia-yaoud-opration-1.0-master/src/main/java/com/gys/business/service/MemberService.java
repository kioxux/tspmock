//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.GetQueryMemberOutData;
import com.gys.business.service.data.MemberCardInData;
import com.gys.business.service.data.MemberCardOutData;
import com.gys.business.service.data.member.MemberInData;

import java.util.List;
import java.util.Map;

public interface MemberService {
    List<MemberCardOutData> getCardList(MemberCardInData inData);

    GaiaSdRechargeCard getRechargeCardByMemberCardId(MemberInData memberInData);


}
