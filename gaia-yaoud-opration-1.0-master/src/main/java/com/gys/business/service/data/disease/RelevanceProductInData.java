package com.gys.business.service.data.disease;

import com.gys.business.mapper.entity.RecommendFirstPlanProduct;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description 关联推荐 入参
 * @Author huxinxin
 * @Date 2021/8/6 14:03
 * @Version 1.0.0
 **/
@Data
public class RelevanceProductInData implements Serializable {

    private static final long serialVersionUID = -4835757903213354059L;
    private String client;
    private String stoCode;
    //  商品编码
    private String proCode;
    //  成分分类编码
    private String classificationCode;

    //  LSJ 表示零售价优先 MLL表示毛利额优先 XQ表示效期优先
    private String preferences;
    //  LSJ 表示零售价优先 MLL表示毛利额优先 XQ表示效期优先
    private String preferences2;
    //  剔除商品编码
    private List<String> proArr;

    //
    private List<RecommendFirstPlanProduct> planProducts;

    //  上一次疾病编码
    private String diseaseCode;
    //  上一次方案编码
    private String schemeCode;



}
