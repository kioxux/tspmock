package com.gys.business.service;

import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionH)表服务接口
 *
 * @author makejava
 * @since 2021-10-28 10:54:04
 */
public interface GaiaStoreOutSuggestionHService {

    List<GaiaStoreOutSuggestionDOutData> calculateBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    Object saveOutBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    PageInfo listBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    GaiaStoreOutSuggestionOutData listDetail(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    Object pushToIn(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    GaiaClSystemPara listParam(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    Boolean isAllow(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    List<GaiaStoreOutSuggestionRelationOutData> listSto(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    GaiaStoreOutSuggestionOutData listDetailForConfirm(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    Object confirmBill(GetLoginOutData loginUser, GaiaStoreOutSuggestionInData inData);

    void validBill();

    //系统级门店调出建议明细
    com.gys.common.data.PageInfo queryDetail(CallUpOrInStoreDetailDTO storeDetailDTO);

    //查询单据类型
    List<String> queryType();
    //系统级门店调出建议明细导出
    void queryDetailExport(HttpServletResponse response ,CallUpOrInStoreDetailDTO storeDetailDTO);

    List<Map<String,String>> queryBillCode(String startDate,String endDate);

}
