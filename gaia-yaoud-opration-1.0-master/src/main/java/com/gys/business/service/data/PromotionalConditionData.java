package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/***
 * 促销条件类
 */
@Data
public class PromotionalConditionData implements Serializable {
    private static final long serialVersionUID = 6120140341616682001L;

    /**
     * 促销单号
     */
    private String voucherId;
    /**
     * 促销主题
     */
    private String theme;
    /**
     * 促销名称
     */
    private String name;
    /**
     * 促销说明
     */
    private String remarks;
    /**
     * 促销开始日期
     */
    private String gsphBeginDate;
    /**
     * 促销结束日期
     */
    private String gsphEndDate;
    /**
     * 促销日期频率
     */
    private String gsphDateFrequency;
    /**
     * 促销星期频率
     */
    private String gsphWeekFrequency;
    /**
     * 促销起始时间
     */
    private String gsphBeginTime;
    /**
     * 促销结束时间
     */
    private String gsphEndTime;
    private String gsphPara1;
    private String gsphPara2;
    private String gsphPara3;
    private String gsphPara4;
    /**
     * 互斥条件
     */
    private String gsphExclusion;
    /**
     * 促销类型
     */
    private String type;
    /**
     * 阶段
     */
    private String part;
    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 商品名称
     */
    private String proName;

    /**
     * 系列Id
     */
    private String seriesId;
    /**
     * 是否会员
     */
    private String memFlag;
    /**
     * 是否积分
     */
    private String inteFlag;
    /**
     * 积分倍率
     */
    private String inteRate;
    /**
     * 行号
     */
    private String serial;
    /**
     * 阶段一到达数量
     */
    private String qtyOneStage;
    /**
     * 阶段一到达金额
     */
    private String amtOneStage;
    /**
     * 阶段一价格(单品)
     */
    private String priceOne;
    /**
     * 阶段一减额(系列)
     */
    private String reductionOne;
    /**
     * 阶段一折扣(系列)(单品)
     */
    private String rebateOne;
    /**
     * 阶段一赠品数(赠送)
     */
    private String qtyOne;
    /**
     * 阶段二到达数量
     */
    private String qtyTwoStage;
    /**
     * 阶段二到达金额
     */
    private String amtTwoStage;
    /**
     * 阶段二价格(单品)
     */
    private String priceTwo;
    /**
     * 阶段二减额(系列)
     */
    private String reductionTwo;
    /**
     * 阶段二折扣(系列)(单品)
     */
    private String rebateTwo;
    /**
     * 阶段二赠品数(赠送)
     */
    private String qtyTwo;
    /**
     * 阶段三到达数量
     */
    private String qtyThreeStage;
    /**
     * 阶段三到达金额
     */
    private String amtThreeStage;
    /**
     * 阶段三价格(单品)
     */
    private String priceThree;
    /**
     * 阶段三减额(系列)
     */
    private String reductionThree;
    /**
     * 阶段三折扣(系列)(单品)
     */
    private String rebateThree;
    /**
     * 阶段三赠品数(赠送)
     */
    private String qtyThree;
}
