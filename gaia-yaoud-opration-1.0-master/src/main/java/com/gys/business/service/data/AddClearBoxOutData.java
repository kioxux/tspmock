//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class AddClearBoxOutData {
    private String emp;
    private List<ClearBoxDetailOutData> clearBoxDetailOutDataList;
    private String clientId;
    private String brId;

    public AddClearBoxOutData() {
    }

    public String getEmp() {
        return this.emp;
    }

    public List<ClearBoxDetailOutData> getClearBoxDetailOutDataList() {
        return this.clearBoxDetailOutDataList;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public void setEmp(final String emp) {
        this.emp = emp;
    }

    public void setClearBoxDetailOutDataList(final List<ClearBoxDetailOutData> clearBoxDetailOutDataList) {
        this.clearBoxDetailOutDataList = clearBoxDetailOutDataList;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AddClearBoxOutData)) {
            return false;
        } else {
            AddClearBoxOutData other = (AddClearBoxOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$emp = this.getEmp();
                    Object other$emp = other.getEmp();
                    if (this$emp == null) {
                        if (other$emp == null) {
                            break label59;
                        }
                    } else if (this$emp.equals(other$emp)) {
                        break label59;
                    }

                    return false;
                }

                Object this$clearBoxDetailOutDataList = this.getClearBoxDetailOutDataList();
                Object other$clearBoxDetailOutDataList = other.getClearBoxDetailOutDataList();
                if (this$clearBoxDetailOutDataList == null) {
                    if (other$clearBoxDetailOutDataList != null) {
                        return false;
                    }
                } else if (!this$clearBoxDetailOutDataList.equals(other$clearBoxDetailOutDataList)) {
                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AddClearBoxOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $emp = this.getEmp();
        result = result * 59 + ($emp == null ? 43 : $emp.hashCode());
        Object $clearBoxDetailOutDataList = this.getClearBoxDetailOutDataList();
        result = result * 59 + ($clearBoxDetailOutDataList == null ? 43 : $clearBoxDetailOutDataList.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        return result;
    }

    public String toString() {
        return "AddClearBoxOutData(emp=" + this.getEmp() + ", clearBoxDetailOutDataList=" + this.getClearBoxDetailOutDataList() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ")";
    }
}
