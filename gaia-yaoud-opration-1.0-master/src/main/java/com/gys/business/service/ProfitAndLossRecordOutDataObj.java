package com.gys.business.service;

import com.gys.business.service.data.ProfitAndLossRecordOutData;
import lombok.Data;

import java.util.List;

@Data
public class ProfitAndLossRecordOutDataObj {
    private List<ProfitAndLossRecordOutData> list;


}
