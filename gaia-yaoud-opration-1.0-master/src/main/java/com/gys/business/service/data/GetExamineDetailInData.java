package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetExamineDetailInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "验收单号")
    private String gsedVoucherId;
    @ApiModelProperty(value = "验收日期")
    private String gsedDate;
    @ApiModelProperty(value = "行号")
    private String gsedSerial;
    @ApiModelProperty(value = "编码")
    private String gsedProId;
    @ApiModelProperty(value = "批号")
    private String gsedBatchNo;
    @ApiModelProperty(value = "批次")
    private String gsedBatch;
    @ApiModelProperty(value = "有效期")
    private String gsedValidDate;
    @ApiModelProperty(value = "验收数量")
    private String gsedRecipientQty;
    @ApiModelProperty(value = "合格数量")
    private String gsedQualifiedQty;
    @ApiModelProperty(value = "不合格数量")
    private String gsedUnqualifiedQty;
    @ApiModelProperty(value = "不合格事项")
    private String gsedUnqualifiedCause;
    @ApiModelProperty(value = "验收人员")
    private String gsedEmp;
    @ApiModelProperty(value = "验收结论")
    private String gsedResult;
    @ApiModelProperty(value = "库存状态")
    private String gsedStockStatus;
}
