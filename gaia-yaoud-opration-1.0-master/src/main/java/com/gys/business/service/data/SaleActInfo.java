//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class SaleActInfo {
    private String voucherId;
    private int num;
    private float money;

    public SaleActInfo() {
    }

    public String getVoucherId() {
        return this.voucherId;
    }

    public int getNum() {
        return this.num;
    }

    public float getMoney() {
        return this.money;
    }

    public void setVoucherId(final String voucherId) {
        this.voucherId = voucherId;
    }

    public void setNum(final int num) {
        this.num = num;
    }

    public void setMoney(final float money) {
        this.money = money;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof SaleActInfo)) {
            return false;
        } else {
            SaleActInfo other = (SaleActInfo)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label31: {
                    Object this$voucherId = this.getVoucherId();
                    Object other$voucherId = other.getVoucherId();
                    if (this$voucherId == null) {
                        if (other$voucherId == null) {
                            break label31;
                        }
                    } else if (this$voucherId.equals(other$voucherId)) {
                        break label31;
                    }

                    return false;
                }

                if (this.getNum() != other.getNum()) {
                    return false;
                } else {
                    return Float.compare(this.getMoney(), other.getMoney()) == 0;
                }
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof SaleActInfo;
    }

    public int hashCode() {
        
        int result = 1;
        Object $voucherId = this.getVoucherId();
        result = result * 59 + ($voucherId == null ? 43 : $voucherId.hashCode());
        result = result * 59 + this.getNum();
        result = result * 59 + Float.floatToIntBits(this.getMoney());
        return result;
    }

    public String toString() {
        return "SaleActInfo(voucherId=" + this.getVoucherId() + ", num=" + this.getNum() + ", money=" + this.getMoney() + ")";
    }
}
