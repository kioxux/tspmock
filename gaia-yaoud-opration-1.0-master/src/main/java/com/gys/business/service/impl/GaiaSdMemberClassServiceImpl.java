package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdMemberClass;
import com.gys.business.service.GaiaSdMemberClassService;
import com.gys.business.service.data.GaiaSdIntegralChange;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.GetQueryMemberOutData;
import com.gys.business.service.data.member.GetMemberCardUpgrade;
import com.gys.business.service.data.member.MemberCardUpgradeInData;
import com.gys.common.data.GetLoginOutData;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/12 17:53
 * @Version 1.0.0
 **/
@Service
public class GaiaSdMemberClassServiceImpl implements GaiaSdMemberClassService {
    @Autowired
    private GaiaSdMemberClassMapper memberClassMapper;
    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;
    @Autowired
    private GaiaSdMemberCardDataMapper memberCardDataMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdIntegralChangeMapper integralChangeMapper;

    @Override
    public List<GaiaSdMemberClass> getAll(Map<String, Object> map) {
        Example exampleD = new Example(GaiaSdMemberClass.class);
        exampleD.createCriteria().andEqualTo("clientId", map.get("clientId"));
        return memberClassMapper.selectByExample(exampleD);
    }

    @Override
    public GetMemberCardUpgrade getMemberCardUpgrade(GetQueryMemberInData inData) {
        GetMemberCardUpgrade memberOutData = new GetMemberCardUpgrade();;

        //  会员信息
        GetQueryMemberOutData memberInfo = this.memberBasicMapper.queryMember(inData);
        if(ObjectUtil.isNotEmpty(memberInfo)){
            memberOutData.setMemberInfo(memberInfo);
            //  当前加盟商 会员卡类型全部信息
            Map<String,Object> map = new HashMap<>();
            map.put("clientId",inData.getClientId());
            List<GaiaSdMemberClass> memberClass = this.getAll(map);
            memberOutData.setMemberClass(memberClass);
        }else {
            return null;
        }
        String flag = storeDataMapper.selectStoPriceComparison(inData.getClientId(), inData.getBrId(),"INTEGRAL_UPFALG");
        memberOutData.setFlag(StringUtils.isNotEmpty(flag) ? flag: "0");
        String type = storeDataMapper.selectStoPriceComparison(inData.getClientId(), inData.getBrId(),"INTEGRAL_UPTYPE");
        memberOutData.setType(StringUtils.isNotEmpty(type) ? type: "0");
        return memberOutData;
    }

    @Override
    @Transactional
    public void UpdMemberCardChange(GetLoginOutData userInfo, MemberCardUpgradeInData inData) {

        GetQueryMemberOutData memberInfo = inData.getMemberInfo();
        if (ObjectUtil.isNotEmpty(inData.getMemberInfo())){
            GaiaSdMemberCardData memberCardData = new GaiaSdMemberCardData();
            Example cardExample = new Example(GaiaSdMemberCardData.class);
            cardExample.createCriteria().andEqualTo("client", userInfo.getClient())
                    .andEqualTo("gsmbcMemberId",memberInfo.getMemberId())
                    .andEqualTo("gsmbcCardId",memberInfo.getCardNum());
            memberCardData.setGsmbcClassId(memberInfo.getTypeId());
            memberCardData.setGsmbcIntegral(memberInfo.getPoint());
            memberCardData.setGsmbcState(memberInfo.getGsmbcState());
            memberCardData.setGsmbcJhUser(memberInfo.getGsmbcJhUser());
            memberCardData.setGsmbcJhDate(memberInfo.getGsmbcJhDate());
            memberCardData.setGsmbcJhTime(memberInfo.getGsmbcJhTime());
            memberCardDataMapper.updateByExampleSelective(memberCardData,cardExample);
        }


        if (!inData.getIntegralChanges().isEmpty()){
            int voucherMaxId = integralChangeMapper.getNextVoucherIdByJFSJ(userInfo.getClient());
            List<GaiaSdIntegralChange> integralChanges = inData.getIntegralChanges();
            for (int i = 0; i < integralChanges.size(); i++) {
                GaiaSdIntegralChange item = integralChanges.get(i);
                item.setClient(userInfo.getClient());
                item.setGsicBrId(userInfo.getDepId());
                item.setGsicChangeIntegral(item.getGsicChangeIntegral().negate());
                item.setGsicVoucherId(nextVoucherId(voucherMaxId,i+1));
            }
            integralChangeMapper.batchInsert(inData.getIntegralChanges());
        }
    }

    public String nextVoucherId(int voucherMaxId,int i) {
        StringBuilder nextVoucherId = new StringBuilder("JFSJ")
                .append(DateUtil.format(new Date(), "yyyyMM"))
                .append(String.format("%07d", voucherMaxId+i));
        return String.valueOf(nextVoucherId);
    }
}
