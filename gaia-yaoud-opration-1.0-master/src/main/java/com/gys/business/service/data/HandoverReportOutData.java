package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2021/1/28
 */
@Data
public class HandoverReportOutData implements Serializable {
    private static final long serialVersionUID = 8015003838183896903L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 收银员id
     */
    private String gsshEmp;

    /**
     * 收银员名称
     */
    private String userNam;

    /**
     * 销售时间
     */
    private String saleDate;

    /**
     * 合计笔数
     */
    private String totalNumber;

    /**
     * 合计金额
     */
    private String totalAmount;

    /**
     * 客单价
     */
    private String customerPrice;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 销售笔数
     */
    private String numberOfSales;

    /**
     * 退货笔数
     */
    private String numberOfReturns;

    /**
     * 销售金额
     */
    private String salesAmount;

    /**
     * 退货金额
     */
    private String returnAmount;

    /**
     * 充值金额
     */
    private String recharge;

    /**
     * 开始日期
     */
    private String startDate;

    /**
     * 开始日期
     */
    private String endDate;

    /**
     * 支付信息
     */
    private List<HandoverReportPayData> dataList;

    /**
     * 储值卡信息
     */
    private List<HandoverReportPayData> storeList;

}
