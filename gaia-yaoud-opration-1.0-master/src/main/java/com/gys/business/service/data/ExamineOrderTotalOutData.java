package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ExamineOrderTotalOutData {
    @ApiModelProperty("验收总数量")
    private String recipientTotalQty;
    @ApiModelProperty("合格总数量")
    private String qualifiedTotalQty;
}
