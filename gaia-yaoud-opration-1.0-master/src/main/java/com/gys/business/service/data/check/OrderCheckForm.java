package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 14:04
 **/
@Data
public class OrderCheckForm {
    @ApiModelProperty(value = "加盟商", example = "10000001", hidden = true)
    private String client;
    @ApiModelProperty(value = "用户ID", example = "1116", hidden = true)
    private String userId;
    @ApiModelProperty(value = "委托配送方编码", example = "1000001")
    private String supplierCode;
    @ApiModelProperty(value = "要货开始日期", example = "2021-05-01")
    private String startDate;
    @ApiModelProperty(value = "要货截止日期", example = "2021=05-30")
    private String endDate;
    @ApiModelProperty(value = "补货单号", example = "PD091232001")
    private String replenishCode;
    @ApiModelProperty(value = "门店编码", example = "10001")
    private String storeId;
    @ApiModelProperty(value = "补货类型：0-正常请货、1-紧急请货、2-铺货", example = "0")
    private String replenishType;
    @ApiModelProperty(value = "审核状态：0-未审核、1-已审核", example = "1")
    private String status;
    @ApiModelProperty(value = "审核日期", example = "2021-06-09")
    private String reviewDate;
}
