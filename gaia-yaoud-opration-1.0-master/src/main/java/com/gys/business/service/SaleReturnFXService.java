
package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.business.service.data.CommenData.InData;
import com.gys.common.data.DataSubServer;
import com.gys.common.data.GetLoginOutData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface SaleReturnFXService {

    /**
     * 列表
     * @param inData
     * @return
     */
    List<SalesInquireOutData> getSalesInquireList(SalesInquireInData inData);

    boolean updateSalesInquire(String client, String depId, List<String> billNo);

    SaleReturnAllData detail(SalesInquireData inData);

    Map<String,Object> returnAndSave2(SalesInquireData inData);

    boolean judgment(SalesInquireData inData);

    /**
     * 物料评估
     * @param dtos
     */
    void materialDocumentInterface(List<MaterialDocRequestDto> dtos);

    /**
     * 判断积分
     * @param inData
     * @return
     */
    SaleReturnAllowOutData JudgmentPoints(SalesInquireData inData);

    ReturnCacheData cache(SalesInquireData inData);

    List<GetUserOutData> queryUser(GetSaleScheduleInData inData);
    List<GetUserOutData> queryUserNew(GetSaleScheduleInData inData);

    List<GetDoctorOutData> queryDoctor(GetLoginOutData inData);

    /**
     * 更新退货单状态
     * @param inData
     * @return
     */
    int updateReturnBillStatus(ReturnSaleBillInData inData);

    /**
     * 获取支付方式
     * @param userInfo
     * @return
     */
    List<GaiaSdPayMethodOutData> queryPayMethod(GetLoginOutData userInfo);

    /**
     * 查询加盟商下所有门店此商品库存
     * @param inData
     * @return
     */
    List<ClientStockOutData> queryClientStock(GetQueryProductInData inData);

    List<GetQueryMemberOutData> querySaleMember(Map<String, String> map);

    /**
     * 会员历史销售查询
     * @param map
     * @return
     */
    List<MemberPurchaseRecordOutData> memberPurchaseRecord(Map<String, String> map);

    /**
     * 查询saleh和 saled
     * @param map
     * @return
     */
    GetSaleBillInfoData queryBillInfo(Map<String, String> map);

    PendingPrintDto getPendingOrderInformation(Map<String, String> map);

    List<RestOrderDetailOutData> listControlGoodsQtyByIdCard(Map<String, Object> map);


    /**
     * 无订单退货
     * @param map
     * @param userInfo
     * @return
     */
    Map<String, Object> returnWithoutOrder(Map<String, Object> map, GetLoginOutData userInfo);

    List<GaiaWmsHudiaoZOutData> listAdjustBill(GaiaWmsHudiaoZOutData map);

    List<GaiaWmsHudiaoZOutData> listAdjustBillDetail(GaiaWmsHudiaoZOutData param);

    void dataSubServer(DataSubServer param);

    void dataSubServerRequest(InData param);

    List<GetQueryProductOutData> listOtherStoreProduct(HashMap<String, String> param);

    List<GetPdAndExpOutData> queryStockAndExpGroupByArea(HashMap<String, String> param);

    List<GetPdAndExpOutData> queryStockAndExpGroupByBatchNo(HashMap<String, String> param);

    GetSalesReceiptsTableOutData queryOtherProductDetail(HashMap<String, String> param);

    boolean commercePlatform(GetLoginOutData userInfo, HashMap<String, String> param);

    List<GetQueryProductOutData> listOtherStoreProductBatch(HashMap<String, String> param);

    void updateHideCommonRemark(List<HashMap<String, String>> param);

    List<GaiaHideCommonRemark> listHideCommonRemark(HashMap<String, String> param);

    void saveHideRemark(HashMap<String, Object> param);
}
