//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaTaxCode;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AcceptService {
    PageInfo<GetAcceptOutData> selectList(GetAcceptInData inData);

    PageInfo<GetAcceptOutData> supplierAcceptList(GetAcceptInData inData);

    List<GetDiaoboOutData> diaoboList(GetAcceptInData inData);

    List<GetPoOutData> poList(GetAcceptInData inData);

    List<GetPoDetailOutData> poDetailList(GetAcceptInData inData);

    List<GetAcceptDetailOutData> detailList(GetAcceptInData inData);

    String save(GetAcceptInData inData);

    void insert(GetAcceptInData inData);

    String insertPo(GetPoInData inData, GetLoginOutData userInfo);

    void savePo(GetAcceptInData inData);

    void approve(GetAcceptInData inData, GetLoginOutData userInfo);

    void approvePS(GetAcceptInData inData, GetLoginOutData userInfo);

    String proInfo(GetAcceptInData inData);

    List<GaiaTaxCode> getTax(GetAcceptInData inData);

    List<GetSupOutData> getSup(GetAcceptInData inData);

    List<GetSupOutData> getSupByBrIds(GetAcceptInData inData);

    List<GetQueryProductOutData> queryAcceptProduct(GetQueryProductInData inData);

    List<IncomeStatementStoreDeficitOutData> queryAcceptProductStock(GetQueryProductInData inData);

    String approvePo(GetPoInData inData, GetLoginOutData userInfo);

    List<GetSupOutData> getSupByList(GetPlaceOrderInData inData);

    Map<String,Object> acceptOrderList(AcceptOrderInData inData, GetLoginOutData userInfo);

    List<GetAcceptOutData> rejectList(GetExamineInData inData);

    GetPoInData rejectDetail(GetAcceptInData inData);

    void insertToRedis(GetPoInData inData);

    GetPoInData selectFromRedis(GetAcceptInData inData);

    Map<String,String>hasRedisOrNot(GetAcceptInData inData);

    List<GetSupOutData> getSupByContent(GetAcceptInData inData);

    Result getImportExcelDetailList(HttpServletRequest request,MultipartFile file, GetLoginOutData userInfo);

    Result exportErrorAcceptPro(List<ImportAcceptInData> list,int type);

    List<HashMap<String,Object>> listUnMatchInfo(GetAcceptInData inData);

    void checkProSelfCode();

    List<PoOrderOutData> selectOrderList(AcceptOrderInData inData);

    List<Map<String,String>> selectSupplierList(GetAcceptInData inData);

    JsonResult selectInvoiceShowFlag(GetLoginOutData userInfo);

    JsonResult getBusEmpListBySupSelfCode(GetAcceptInData inData);

    JsonResult selectSupplierListByStoCode(PoPriceProductHistoryInData inData);

    JsonResult getPoPriceProductHistory(PoPriceProductHistoryInData inData);

    JsonResult getProductCreateDate(PoPriceProductHistoryInData inData);

    void rejectAcceptOreder(GetAcceptInData inData);

    ProductMadeDateOrValidityDateOutData getCreateDateOrValidityDate(PoPriceProductHistoryInData inData);
}
