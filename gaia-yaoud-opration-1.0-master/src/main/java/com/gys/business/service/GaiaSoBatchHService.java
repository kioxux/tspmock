package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSoBatchH;
import com.gys.business.service.data.yaoshibang.CustomerDto;
import com.gys.business.service.data.yaoshibang.OrderDetailDto;
import com.gys.business.service.data.yaoshibang.OrderQueryBean;

import java.util.List;
import java.util.Map;

/**
 * @author zhangdong
 * @date 2021/6/10 18:13
 */
public interface GaiaSoBatchHService {

    List<GaiaSoBatchH> selectAll();

    void insertList(List<GaiaSoBatchH> list);

    void updateStatus(List<GaiaSoBatchH> batchHList, String status);

    void updateCustomerId(Long id, String customerId);

    void updateStatusByClientAndCustomerName(String client, String customerName, String status);

    void updateCustomerIdClientAndCustomerName(String client, String customerName, String customerId);

    List<GaiaSoBatchH> orderQuery(OrderQueryBean bean);

    List<OrderDetailDto> orderDetail(Long id, String client, String depId);

    List<CustomerDto> customerQuery(String name, String client);

    GaiaSoBatchH selectByPrimaryKey(Long id);

    void updateBatchOrderId(Long id, String batchOrderId);

    Map<String, GaiaSoBatchH> selectAllCustomerId(String client);

    void compensate(String customerName, String customerId, String client);

    List<String> queryOrderIdExist(String client, List<String> orderIdQueryList);
}
