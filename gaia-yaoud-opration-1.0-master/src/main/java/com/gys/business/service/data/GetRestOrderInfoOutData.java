package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetRestOrderInfoOutData implements Serializable {
    private static final long serialVersionUID = -1455587357431707314L;

    /**
     * 销售日期和时间
     */
    private String gsshDateTime;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 员工工号
     */
    private String gsshEmp;


    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 会员卡号
     */
    private String gsshHykNo;

    /**
     * 会员名字
     */
    private String gsshHykName;

    /**
     * 应收金额
     */
    private String gsshYsAmt;


    /**
     * 折让金额
     */
    private String gsshZkAmt;

    /**
     * 零售金额
     */
    private String gsshNormalAmt;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 验证 挂单前状态
     */
    private String centralAndWestern;

    /**
     * 当前贴数
     */
    private String herbalNum;

    /**
     * 服务器当天日期
     */
    private String nowDate;

    private String gsshCallAllow;
    private String gsshHideFlag;

    private String gsshCallAllowName;

    private String gsshBillType;
    private String gsshReplaceBrId;
    private String gsshReplaceBrName;
    private String gsshReplaceSalerId;
    private String gsshReplaceSalerName;
    private String gsshOrderSource;
    private String gsshBdFlag;
    private String gsshBdFlagName;
    private int index;
    /**
     * 挂号单号
     */
    private String gsshRegisterVoucherId;

    /**
     * 会员手机号
     */
    private String gsshHykMobile;
    /**
     * 医生
     */
    private String doctorName;

    /**
     * 备注
     */
    private String memo;

    /**
     * 挂单备注
     */
    private String gsshHideRemark;


    private GetQueryMemberOutData queryMemberOutData;
}
