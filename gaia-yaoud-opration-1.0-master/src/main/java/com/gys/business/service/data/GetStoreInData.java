package com.gys.business.service.data;

import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;

@Data
public class GetStoreInData {
    private String monthPlan;
    private String client;
    private String brId;
    private int monthDays;
    private String turnoverDailyPlan;
    private String grossProfitDailyPlan;
    private String mCardDailyPlan;
    private String index;
}