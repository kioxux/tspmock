package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaSdProuctLocationMapper;
import com.gys.business.mapper.entity.GaiaSdProuctLocation;
import com.gys.business.service.ProductLocationService;
import com.gys.business.service.data.GetProductLocationInData;
import com.gys.business.service.data.GetProductLocationOutData;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
@Service
public class ProductLocationServiceImpl implements ProductLocationService {
    @Autowired
    private GaiaSdProuctLocationMapper prouctLocationMapper;

    public ProductLocationServiceImpl() {
    }

    public List<GetProductLocationOutData> selectLocationList(GetProductLocationInData inData) {
        List<GetProductLocationOutData> list = this.prouctLocationMapper.selectLocationList(inData);
        return list;
    }

    @Transactional
    public void saveLocation(GetProductLocationInData inData) {
        if (!CollUtil.isEmpty(inData.getLoactions())) {
            Iterator var2 = inData.getLoactions().iterator();

            while(var2.hasNext()) {
                GetProductLocationInData productLocation = (GetProductLocationInData)var2.next();
                productLocation.setClientId(inData.getClientId());
                productLocation.setGsplBrId(inData.getGsplBrId());
                GaiaSdProuctLocation location = (GaiaSdProuctLocation)this.prouctLocationMapper.selectByPrimaryKey(productLocation);
                if (ObjectUtil.isNull(location)) {
                    location = new GaiaSdProuctLocation();
                    BeanUtils.copyProperties(productLocation, location);
                    this.prouctLocationMapper.insert(location);
                } else {
                    BeanUtils.copyProperties(productLocation, location);
                    this.prouctLocationMapper.updateByPrimaryKey(location);
                }
            }

        }
    }

    @Transactional
    public void importLocation(GetProductLocationInData inData) {
        List<GetProductLocationInData> locations = inData.getLoactions();
        if (CollUtil.isEmpty(locations)) {
            throw new BusinessException("导入数据不能为空");
        } else {
            Map<String, Integer> proIndexMap = new HashMap();
            Iterator var4 = locations.iterator();

            while(var4.hasNext()) {
                GetProductLocationInData location = (GetProductLocationInData)var4.next();
                if (StrUtil.isBlank(location.getGsplProId())) {
                    throw new BusinessException(StrUtil.format("第{}行商品编号不能为空", new Object[]{location.getIndex()}));
                }

                if (ObjectUtil.isNotNull(proIndexMap.get(location.getGsplProId()))) {
                    throw new BusinessException(StrUtil.format("第{}行和第{}行商品编号重复", new Object[]{proIndexMap.get(location.getGsplProId()), location.getIndex()}));
                }

                proIndexMap.put(location.getGsplProId(), location.getIndex());
            }

            List<GetProductLocationOutData> list = this.prouctLocationMapper.selectLocationList(inData);
            Map<String, String> proMap = new HashMap();
            Iterator var6 = list.iterator();

            while(var6.hasNext()) {
                GetProductLocationOutData product = (GetProductLocationOutData)var6.next();
                proMap.put(product.getGsplProId(), product.getGsplProId());
            }

            var6 = locations.iterator();

            GetProductLocationInData location;
            do {
                if (!var6.hasNext()) {
                    this.saveLocation(inData);
                    return;
                }

                location = (GetProductLocationInData)var6.next();
            } while(!StrUtil.isBlank((CharSequence)proMap.get(location.getGsplProId())));

            throw new BusinessException(StrUtil.format("第{}行商品不存在或者库存为0", new Object[]{location.getIndex()}));
        }
    }

    public GetProductLocationOutData selectLocationDrop(GetProductLocationInData inData) {
        GetProductLocationOutData outData = new GetProductLocationOutData();
        outData.setAreaList(this.prouctLocationMapper.selectLocationDrop("GSPL_AREA", inData.getClientId(), inData.getGsplBrId()));
        outData.setGroupList(this.prouctLocationMapper.selectLocationDrop("GSPL_GROUP", inData.getClientId(), inData.getGsplBrId()));
        outData.setShelfList(this.prouctLocationMapper.selectLocationDrop("GSPL_SHELF", inData.getClientId(), inData.getGsplBrId()));
        outData.setStoreyList(this.prouctLocationMapper.selectLocationDrop("GSPL_STOREY", inData.getClientId(), inData.getGsplBrId()));
        outData.setSeatList(this.prouctLocationMapper.selectLocationDrop("GSPL_SEAT", inData.getClientId(), inData.getGsplBrId()));
        return outData;
    }
}
