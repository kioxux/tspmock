package com.gys.business.service;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

@Data
@CsvRow
public class GaiaLicenseQualificationOutData {
    @CsvCell(title = "查询类型",index = 1,fieldNo = 1)
    private String queryType;

    private String queryTypeStr;
    @CsvCell(title = "编码",index = 2,fieldNo = 1)
    private String code;
    @CsvCell(title = "名称",index = 3,fieldNo = 1)
    private String name;
    @CsvCell(title = "证书编码",index = 4,fieldNo = 1)
    private String licenseCode;
    @CsvCell(title = "证书名称",index = 5,fieldNo = 1)
    private String licenseName;
    @CsvCell(title = "证书有效期",index = 6,fieldNo = 1)
    private String licenseDate;
    @CsvCell(title = "到期天数",index = 7,fieldNo = 1)
    private Integer licenseDays;
    @CsvCell(title = "禁采状态",index = 8,fieldNo = 1)
    private String purchaseState;
    @CsvCell(title = "停用状态",index = 9,fieldNo = 1)
    private String stopSate;
}
