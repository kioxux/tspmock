package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 医保清算表(GaiaLiquidation)实体类
 *
 * @author makejava
 * @since 2021-12-29 15:00:27
 */
@Data
public class GaiaLiquidationOutData implements Serializable {
    private static final long serialVersionUID = -30237818434743495L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 清算类别
     */
    private String clrType;
    /**
     * 清算年月
     */
    private String clrNy;
    /**
     * 清算申请流 水号
     */
    private String cleAppySn;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     *  清算状态 0-撤销 1-生效
     */
    private Integer status;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 更新者
     */
    private String updateUser;

}

