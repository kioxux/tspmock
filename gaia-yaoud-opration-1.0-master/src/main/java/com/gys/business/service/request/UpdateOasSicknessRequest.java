package com.gys.business.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdateOasSicknessRequest {
    @ApiModelProperty(value = "修改人")
    private String userId;

    @ApiModelProperty(value = "修改人姓名")
    private String userName;

    @ApiModelProperty(value = "大类中类成分类型0大类 1 中类 2成分")
    private Integer type;

    @ApiModelProperty(value = "原疾病编码")
    private String originalSicknessCoding;

    @ApiModelProperty(value = "新疾病编码")
    private String newSicknessCoding;

    @ApiModelProperty(value = "名称")
    private String sicknessName;


    @ApiModelProperty(value = "大类疾病编码")
    private String  largeSicknessCoding;

    @ApiModelProperty(value = "中类疾病编码")
    private String  middleSicknessCoding;
}
