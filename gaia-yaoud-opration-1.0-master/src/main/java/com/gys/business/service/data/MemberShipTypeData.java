package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/8
 */
@Data
public class MemberShipTypeData implements Serializable {
    private static final long serialVersionUID = 3094129461332432353L;

    private String gsmcId;

    private String gsmcName;
}
