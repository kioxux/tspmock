package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ValidProductListOutData implements Serializable {

    @ApiModelProperty(value = "产品编号")
    private String productId;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "产品规格")
    private String productSpecs;

    @ApiModelProperty(value = "生产厂家")
    private String factoryName;

    @ApiModelProperty(value = "效期成本额")
    private BigDecimal costAmt;

    @ApiModelProperty(value = "效期数量")
    private BigDecimal validProCount;

    @ApiModelProperty(value = "30天销量")
    private BigDecimal saleQty;

    @ApiModelProperty(value = "30天销售额")
    private BigDecimal saleAmt;
}
