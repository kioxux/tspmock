package com.gys.business.service.data.fiApPayment;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 应付方式维护表
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
public class GaiaFiApPaymentDto implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "应付方式编码")
    @NotBlank(message = "应付方式编码不能为空")
    private String paymentId;

    @ApiModelProperty(value = "应付方式名称")
    @NotBlank(message = "应付方式名称不能为空")
    private String paymentName;

    @ApiModelProperty(value = "新增或者修改标识")
    private String ifMod;

}
