package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.GaiaSdStoresGroupMapper;
import com.gys.business.service.AppNoMoveProductService;
import com.gys.business.service.data.*;
import com.gys.common.data.JsonResult;
import com.gys.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AppNoMoveProductServiceImpl implements AppNoMoveProductService {

    @Autowired
    private GaiaSdStockBatchMapper stockBatchMapper;
    @Autowired
    private GaiaSdStoresGroupMapper storesGroupMapper;


    @Override
    public JsonResult getBigTypeList(AppNoMoveInData inData) {
        Integer noMoveDays = inData.getNoMoveDays();
        String wmsFlag = inData.getWmsFlag();
        if(ValidateUtil.isEmpty(noMoveDays)) {
            inData.setNoMoveDays(45);
        }
        if(ValidateUtil.isEmpty(wmsFlag)) {
            inData.setWmsFlag("1");
        }

        //出参
        Map<String, Object> resultMap = new HashMap<>(2);
        //查询所有直营店
        List<String> directStoreList = storesGroupMapper.getListByClientAndType(inData.getClient(), "DX0003");
        if(ValidateUtil.isEmpty(directStoreList)) {
            return JsonResult.success(resultMap, "success");
        }
        inData.setDirectStoreList(directStoreList);
        List<AppNoMoveOutData> bigTypeList = stockBatchMapper.getBigTypeList(inData);
        BigDecimal totalNoMoveCostAmt = BigDecimal.ZERO;    //总不动销库存成本额
        BigDecimal totalCostAmt = BigDecimal.ZERO;          //总库存成本额
        for(AppNoMoveOutData appNoMove : bigTypeList) {
            //追加不动销库存成本额
            BigDecimal tempNoMoveCostAmt = appNoMove.getNoMoveCostAmt();
            totalNoMoveCostAmt = totalNoMoveCostAmt.add(tempNoMoveCostAmt);
            //追加总库存成本额
            BigDecimal tempCostAmt = appNoMove.getTotalCostAmt();
            totalCostAmt = totalCostAmt.add(tempCostAmt);

            //四舍五入  保留整数
            appNoMove.setNoMoveCostAmt(tempNoMoveCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
            appNoMove.setTotalCostAmt(tempCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
        }
        //计算合计
        AppNoMoveOutData total = getTotal(bigTypeList, totalNoMoveCostAmt, totalCostAmt);
        //将list转为map  key为类型编码 value为实体
        Map<String, AppNoMoveOutData> noMoveMap = bigTypeList.stream().collect(Collectors.toMap(AppNoMoveOutData::getTypeCode, item -> item));
        //计算小计
        AppNoMoveOutData subtotal = getSubtotal(noMoveMap);
        List<AppNoMoveOutData> resultList = new ArrayList<>(bigTypeList.size() + 2);
        if(ValidateUtil.isNotEmpty(total)) {
            resultList.add(total);
        }
        if(ValidateUtil.isNotEmpty(subtotal)) {
            resultList.add(subtotal);
        }
        //大类列表（出去 合计 以及 小计）
        resultList.addAll(bigTypeList);
        //获取饼图数据
        NoMoveChartOutData noMoveChartInfo = getNoMoveChartInfo(resultList);

        resultMap.put("typeList", resultList);
        resultMap.put("noMoveChart", noMoveChartInfo);
        return JsonResult.success(resultMap, "success");
    }


    /**
     * 计算合计
     * @param bigTypeList
     * @param totalNoMoveCostAmt
     * @param totalCostAmt
     * @return
     */
    private AppNoMoveOutData getTotal(List<AppNoMoveOutData> bigTypeList, BigDecimal totalNoMoveCostAmt, BigDecimal totalCostAmt) {
        AppNoMoveOutData total = new AppNoMoveOutData();
        total.setTypeName("合计");
        total.setTypeCode("999");
        total.setNoMoveCount(bigTypeList.stream().map(AppNoMoveOutData::getNoMoveCount).reduce(BigDecimal.ZERO, BigDecimal::add));
        total.setNoMoveCostAmt(totalNoMoveCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
        //合计不动销库粗成本额占比
        if(BigDecimal.ZERO.compareTo(totalCostAmt) != 0) {
            total.setNoMoveCostAmtProp(totalNoMoveCostAmt.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
        } else {
            total.setNoMoveCostAmtProp(BigDecimal.ZERO);
        }
        total.setTotalCostAmt(totalCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
        return total;
    }


    /**
     * 计算小计
     * @param
     * @return
     */
    private AppNoMoveOutData getSubtotal(Map<String, AppNoMoveOutData> noMoveMap) {
        AppNoMoveOutData prescription = noMoveMap.get("1");    //处方药
        AppNoMoveOutData otc = noMoveMap.get("2");             //OTC
        AppNoMoveOutData subtotal = new AppNoMoveOutData();
        subtotal.setTypeName("药品-小计");
        subtotal.setTypeCode("888");
        subtotal.setNoMoveCount(prescription.getNoMoveCount().add(otc.getNoMoveCount()));                //不动销品项数
        BigDecimal subNoMoveCostAmt = prescription.getNoMoveCostAmt().add(otc.getNoMoveCostAmt());       //不动销库存成本额
        BigDecimal subNoMoveTotalCostAmt = prescription.getTotalCostAmt().add(otc.getTotalCostAmt());    //总库存成本额
        subtotal.setNoMoveCostAmt(subNoMoveCostAmt);
        subtotal.setTotalCostAmt(subNoMoveTotalCostAmt);
        //小计不动销库存成本额占比
        if(BigDecimal.ZERO.compareTo(subNoMoveTotalCostAmt) != 0) {
            subtotal.setNoMoveCostAmtProp(subNoMoveCostAmt.divide(subNoMoveTotalCostAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
        } else {
            subtotal.setNoMoveCostAmtProp(BigDecimal.ZERO);
        }
        return subtotal;
    }


    /**
     * 获取饼图数据
     * @return
     */
    private NoMoveChartOutData getNoMoveChartInfo(List<AppNoMoveOutData> resultNoMoveList) {
        //将list转为map  key为类型编码 value为实体
        Map<String, AppNoMoveOutData> noMoveMap = resultNoMoveList.stream().collect(Collectors.toMap(AppNoMoveOutData::getTypeCode, item -> item));
        AppNoMoveOutData noMoveData1 = noMoveMap.get("1");                //处方药
        AppNoMoveOutData noMoveData2 = noMoveMap.get("2");                //OTC
        AppNoMoveOutData noMoveData3 = noMoveMap.get("3");                //中药
        AppNoMoveOutData noMoveData4 = noMoveMap.get("4");                //保健食品
        AppNoMoveOutData noMoveData5 = noMoveMap.get("5");                //医疗器械及护理
        AppNoMoveOutData noMoveData6 = noMoveMap.get("6");                //个人护理
        AppNoMoveOutData noMoveData7 = noMoveMap.get("7");                //家庭用品
        AppNoMoveOutData noMoveData8 = noMoveMap.get("8");                //赠品
        AppNoMoveOutData noMoveData9 = noMoveMap.get("9");                //其它
        AppNoMoveOutData noMoveData888 = noMoveMap.get("888");            //药品小计
        AppNoMoveOutData noMoveData999 = noMoveMap.get("999");            //合计

        //出参
        NoMoveChartOutData charInfo = new NoMoveChartOutData();
        //不动销品项数
        //非药品
        charInfo.setNoMoveNoDrugCount(noMoveData4.getNoMoveCount().add(noMoveData5.getNoMoveCount())
                .add(noMoveData6.getNoMoveCount()).add(noMoveData7.getNoMoveCount()).add(noMoveData8.getNoMoveCount()).add(noMoveData9.getNoMoveCount()));
        //中药
        charInfo.setNoMoveChineseDrugCount(noMoveData3.getNoMoveCount());
        //药品
        charInfo.setNoMoveDrugCount(noMoveData1.getNoMoveCount().add(noMoveData2.getNoMoveCount()));
        //处方药
        charInfo.setNoMovePrescriptionCount(noMoveData1.getNoMoveCount());
        //OTC
        charInfo.setNoMoveOTCCount(noMoveData2.getNoMoveCount());

        //不动销库存成本额占比
        //合计库存成本额
        BigDecimal noMoveCostAmt = noMoveData999.getNoMoveCostAmt();
        BigDecimal oneHundred = new BigDecimal(100);       //百分比计算使用
        if(BigDecimal.ZERO.compareTo(noMoveCostAmt) != 0) {
            //非药品库存成本额
            BigDecimal noDrugNoMoveCostAmt = noMoveData4.getNoMoveCostAmt().add(noMoveData5.getNoMoveCostAmt()).add(noMoveData6.getNoMoveCostAmt())
                    .add(noMoveData7.getNoMoveCostAmt()).add(noMoveData8.getNoMoveCostAmt()).add(noMoveData9.getNoMoveCostAmt());
            charInfo.setNoMoveNoDrugCostAmtProp(noDrugNoMoveCostAmt.divide(noMoveCostAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(oneHundred).setScale(0, BigDecimal.ROUND_HALF_UP));
            //中药
            BigDecimal noMoveCostAmt3 = noMoveData3.getNoMoveCostAmt();
            charInfo.setNoMoveChineseCostAmtProp(noMoveCostAmt3.divide(noMoveCostAmt, 5 ,BigDecimal.ROUND_HALF_UP)
                    .multiply(oneHundred).setScale(0, BigDecimal.ROUND_HALF_UP));
            //药品
            BigDecimal noMoveCostAmt1 = noMoveData1.getNoMoveCostAmt();         //处方药
            BigDecimal noMoveCostAmt2 = noMoveData2.getNoMoveCostAmt();         //OTC
            BigDecimal noMoveCostAmt888 = noMoveCostAmt1.add(noMoveCostAmt2);           //药品小计
            charInfo.setNoMoveDrugCostAmtProp(noMoveCostAmt888.divide(noMoveCostAmt, 5 ,BigDecimal.ROUND_HALF_UP)
                    .multiply(oneHundred).setScale(0, BigDecimal.ROUND_HALF_UP));
            //处方药
            charInfo.setNoMovePrescriptionCostAmtProp(noMoveCostAmt1.divide(noMoveCostAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(oneHundred).setScale(0, BigDecimal.ROUND_HALF_UP));
            //OTC
            charInfo.setNoMoveOTCCostAmtProp(noMoveCostAmt2.divide(noMoveCostAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(oneHundred).setScale(0, BigDecimal.ROUND_HALF_UP));
        }

        //不动销库存占比
        //合计
        charInfo.setNoMoveTotalProp(noMoveData999.getNoMoveCostAmtProp());
        //处方药
        charInfo.setNoMovePrescriptionProp(noMoveData1.getNoMoveCostAmtProp());
        //OTC
        charInfo.setNoMoveOTCProp(noMoveData2.getNoMoveCostAmtProp());
        //药品小计
        charInfo.setNoMoveDrugProp(noMoveData888.getNoMoveCostAmtProp());
        //中药
        charInfo.setNoMoveChineseDrugProp(noMoveData3.getNoMoveCostAmtProp());
        //不动销非药品库存成本额 / 非药品库存成本额 * 100
        BigDecimal noDrugNoMoveCostAmt = noMoveData4.getNoMoveCostAmt().add(noMoveData5.getNoMoveCostAmt()).add(noMoveData6.getNoMoveCostAmt())
                .add(noMoveData7.getNoMoveCostAmt()).add(noMoveData8.getNoMoveCostAmt()).add(noMoveData9.getNoMoveCostAmt());               //不动销非药品库存成本额
        BigDecimal noDrugTotalCostAmt = noMoveData4.getTotalCostAmt().add(noMoveData5.getTotalCostAmt()).add(noMoveData6.getTotalCostAmt())
                .add(noMoveData7.getTotalCostAmt()).add(noMoveData8.getTotalCostAmt()).add(noMoveData9.getTotalCostAmt());                  //非药品库存成本额
        charInfo.setNoMoveNoDrugProp(noDrugNoMoveCostAmt.divide(noDrugTotalCostAmt, 5 , BigDecimal.ROUND_HALF_UP)
                .multiply(oneHundred).setScale(1, BigDecimal.ROUND_HALF_UP));
        return charInfo;
    }


    @Override
    public JsonResult getMidTypeList(AppNoMoveInData inData) {
        Integer noMoveDays = inData.getNoMoveDays();
        String wmsFlag = inData.getWmsFlag();
        String typeCode = inData.getTypeCode();
        if(ValidateUtil.isEmpty(noMoveDays)) {
            inData.setNoMoveDays(45);
        }
        if(ValidateUtil.isEmpty(wmsFlag)) {
            inData.setWmsFlag("1");
        }
        if(ValidateUtil.isEmpty(typeCode)) {
            inData.setTypeCode("888");
        }
        //查询所有直营店
        List<String> directStoreList = storesGroupMapper.getListByClientAndType(inData.getClient(), "DX0003");
        //如果没有直营门店 则直接返回空
        if(ValidateUtil.isEmpty(directStoreList)) {
            return JsonResult.success(new ArrayList<>(), "success");
        }

        inData.setDirectStoreList(directStoreList);
        List<AppNoMoveOutData> midTypeList = stockBatchMapper.getMidTypeList(inData);

        BigDecimal totalNoMoveCostAmt = BigDecimal.ZERO;     //合计不动销库存成本额
        BigDecimal totalCostAmt = BigDecimal.ZERO;           //合计库存成本额
        //计算合计
        for(AppNoMoveOutData noMove : midTypeList) {
            BigDecimal tempNoMoveCostAmt = noMove.getNoMoveCostAmt();    //不动销库存成本额
            BigDecimal tempTotalCostAmt = noMove.getTotalCostAmt();      //库存成本额

            //累加
            totalNoMoveCostAmt = totalNoMoveCostAmt.add(tempNoMoveCostAmt);
            totalCostAmt = totalCostAmt.add(tempTotalCostAmt);

            //保留小数取整
            noMove.setNoMoveCostAmt(tempNoMoveCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
            noMove.setTotalCostAmt(tempTotalCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
        }

        //组装合计数据
        AppNoMoveOutData total = new AppNoMoveOutData();
        total.setTypeName("合计");
        total.setTypeCode("999");
        total.setNoMoveCount(midTypeList.stream().map(AppNoMoveOutData::getNoMoveCount).reduce(BigDecimal.ZERO, BigDecimal::add));
        total.setNoMoveCostAmt(totalNoMoveCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));      //不动销成本额合计
        total.setTotalCostAmt(totalCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));             //所有库存成本额合计
        if(BigDecimal.ZERO.compareTo(totalCostAmt) != 0) {
            total.setNoMoveCostAmtProp(totalNoMoveCostAmt.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));         //占比
        } else {
            total.setNoMoveCostAmtProp(BigDecimal.ZERO);
        }


        //出参
        List<AppNoMoveOutData> resultList = new ArrayList<>();
        resultList.add(total);
        resultList.addAll(midTypeList);
        return JsonResult.success(resultList, "success");
    }


    @Override
    public JsonResult getStoreList(NoMoveProInData inData) {
        String orderByFlag = inData.getOrderByFlag();     //排序标志 1-升序 2-降序 传空默认为降序
        String topOrLast = inData.getTopOrLast();         //排序前后 0-全部 1-前(从大到小) 2-后(从小到大)
        String orderByCondition = inData.getOrderByCondition();    //排序字段 1-库存品项 2-库存成本额 3-库存占比
        Integer pageSize = inData.getPageSize();          //传空默认10条
        Integer noMoveDays = inData.getNoMoveDays();      //动销天数 默认45天
        if(ValidateUtil.isEmpty(orderByFlag)) {
            inData.setOrderByFlag("2");
        }
        if(ValidateUtil.isEmpty(orderByCondition)) {
            inData.setOrderByCondition("2");
        }
        if(ValidateUtil.isEmpty(topOrLast)) {
            inData.setTopOrLast("0");
        }
        if(ValidateUtil.isEmpty(pageSize)) {
            inData.setPageSize(10);
        }
        if(ValidateUtil.isEmpty(noMoveDays)) {
            inData.setNoMoveDays(45);
        }

        //查询所有直营店
        List<String> directStoreList = storesGroupMapper.getListByClientAndType(inData.getClient(), "DX0003");
        //如果没有直营门店 则返回空集合
        if(ValidateUtil.isEmpty(directStoreList)) {
            return JsonResult.success(new ArrayList<>(), "success");
        }
        inData.setDirectStoreList(directStoreList);
        List<AppNoMoveStoreOutData> noMoveStoreList = stockBatchMapper.getNoMoveStoreList(inData);
        return JsonResult.success(noMoveStoreList, "success");
    }
}
