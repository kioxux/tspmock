//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class GetSyncProductBusinessOutData {
    private String client;
    private String proCode;
    private String proSite;
    private String proSelfCode;
    private String proMatchStatus;
    private String proCommonname;
    private String proDepict;
    private String proPym;
    private String proName;
    private String proSpecs;
    private String proUnit;
    private String proForm;
    private String proPartform;
    private String proMindose;
    private String proTotaldose;
    private String proBarcode;
    private String proBarcode2;
    private String proRegisterClass;
    private String proRegisterNo;
    private String proRegisterDate;
    private String proRegisterExdate;
    private String proClass;
    private String proClassName;
    private String proCompclass;
    private String proCompclassName;
    private String proPresclass;
    private String proFactoryCode;
    private String proFactoryName;
    private String proMark;
    private String proBrand;
    private String proBrandClass;
    private String proLife;
    private String proLifeUnit;
    private String proHolder;
    private String proInputTax;
    private String proOutputTax;
    private String proBasicCode;
    private String proTaxClass;
    private String proControlClass;
    private String proProduceClass;
    private String proStorageCondition;
    private String proStorageArea;
    private String proLong;
    private String proWide;
    private String proHigh;
    private String proMidPackage;
    private String proBigPackage;
    private String proElectronicCode;
    private String proQsCode;
    private String proMaxSales;
    private String proInstructionCode;
    private String proInstruction;
    private String proMedProdct;
    private String proStatus;
    private String proMedProdctcode;
    private String proCountry;
    private String proPlace;
    private String proTakeDays;
    private String proUsage;
    private String proContraindication;
    private String proPosition;
    private String proNoRetail;
    private String proNoPurchase;
    private String proNoDistributed;
    private String proNoSupplier;
    private String proNoDc;
    private String proNoAdjust;
    private String proNoSale;
    private String proNoApply;
    private String proIfpart;
    private String proPartUint;
    private String proPartRate;
    private String proPurchaseUnit;
    private String proPurchaseRate;
    private String proSaleUnit;
    private String proSaleRate;
    private BigDecimal proMinQty;
    private String proIfMed;
    private String proSlaeClass;
    private BigDecimal proLimitQty;
    private BigDecimal proMaxQty;
    private String proPackageFlag;
    private String proKeyCare;
    private String proTcmSpecs;
    private String proTcmRegisterNo;
    private String proTcmFactoryCode;
    private String proTcmPlace;

    public GetSyncProductBusinessOutData() {
    }

    public String getClient() {
        return this.client;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProSite() {
        return this.proSite;
    }

    public String getProSelfCode() {
        return this.proSelfCode;
    }

    public String getProMatchStatus() {
        return this.proMatchStatus;
    }

    public String getProCommonname() {
        return this.proCommonname;
    }

    public String getProDepict() {
        return this.proDepict;
    }

    public String getProPym() {
        return this.proPym;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProPartform() {
        return this.proPartform;
    }

    public String getProMindose() {
        return this.proMindose;
    }

    public String getProTotaldose() {
        return this.proTotaldose;
    }

    public String getProBarcode() {
        return this.proBarcode;
    }

    public String getProBarcode2() {
        return this.proBarcode2;
    }

    public String getProRegisterClass() {
        return this.proRegisterClass;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public String getProRegisterDate() {
        return this.proRegisterDate;
    }

    public String getProRegisterExdate() {
        return this.proRegisterExdate;
    }

    public String getProClass() {
        return this.proClass;
    }

    public String getProClassName() {
        return this.proClassName;
    }

    public String getProCompclass() {
        return this.proCompclass;
    }

    public String getProCompclassName() {
        return this.proCompclassName;
    }

    public String getProPresclass() {
        return this.proPresclass;
    }

    public String getProFactoryCode() {
        return this.proFactoryCode;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProMark() {
        return this.proMark;
    }

    public String getProBrand() {
        return this.proBrand;
    }

    public String getProBrandClass() {
        return this.proBrandClass;
    }

    public String getProLife() {
        return this.proLife;
    }

    public String getProLifeUnit() {
        return this.proLifeUnit;
    }

    public String getProHolder() {
        return this.proHolder;
    }

    public String getProInputTax() {
        return this.proInputTax;
    }

    public String getProOutputTax() {
        return this.proOutputTax;
    }

    public String getProBasicCode() {
        return this.proBasicCode;
    }

    public String getProTaxClass() {
        return this.proTaxClass;
    }

    public String getProControlClass() {
        return this.proControlClass;
    }

    public String getProProduceClass() {
        return this.proProduceClass;
    }

    public String getProStorageCondition() {
        return this.proStorageCondition;
    }

    public String getProStorageArea() {
        return this.proStorageArea;
    }

    public String getProLong() {
        return this.proLong;
    }

    public String getProWide() {
        return this.proWide;
    }

    public String getProHigh() {
        return this.proHigh;
    }

    public String getProMidPackage() {
        return this.proMidPackage;
    }

    public String getProBigPackage() {
        return this.proBigPackage;
    }

    public String getProElectronicCode() {
        return this.proElectronicCode;
    }

    public String getProQsCode() {
        return this.proQsCode;
    }

    public String getProMaxSales() {
        return this.proMaxSales;
    }

    public String getProInstructionCode() {
        return this.proInstructionCode;
    }

    public String getProInstruction() {
        return this.proInstruction;
    }

    public String getProMedProdct() {
        return this.proMedProdct;
    }

    public String getProStatus() {
        return this.proStatus;
    }

    public String getProMedProdctcode() {
        return this.proMedProdctcode;
    }

    public String getProCountry() {
        return this.proCountry;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProTakeDays() {
        return this.proTakeDays;
    }

    public String getProUsage() {
        return this.proUsage;
    }

    public String getProContraindication() {
        return this.proContraindication;
    }

    public String getProPosition() {
        return this.proPosition;
    }

    public String getProNoRetail() {
        return this.proNoRetail;
    }

    public String getProNoPurchase() {
        return this.proNoPurchase;
    }

    public String getProNoDistributed() {
        return this.proNoDistributed;
    }

    public String getProNoSupplier() {
        return this.proNoSupplier;
    }

    public String getProNoDc() {
        return this.proNoDc;
    }

    public String getProNoAdjust() {
        return this.proNoAdjust;
    }

    public String getProNoSale() {
        return this.proNoSale;
    }

    public String getProNoApply() {
        return this.proNoApply;
    }

    public String getProIfpart() {
        return this.proIfpart;
    }

    public String getProPartUint() {
        return this.proPartUint;
    }

    public String getProPartRate() {
        return this.proPartRate;
    }

    public String getProPurchaseUnit() {
        return this.proPurchaseUnit;
    }

    public String getProPurchaseRate() {
        return this.proPurchaseRate;
    }

    public String getProSaleUnit() {
        return this.proSaleUnit;
    }

    public String getProSaleRate() {
        return this.proSaleRate;
    }

    public BigDecimal getProMinQty() {
        return this.proMinQty;
    }

    public String getProIfMed() {
        return this.proIfMed;
    }

    public String getProSlaeClass() {
        return this.proSlaeClass;
    }

    public BigDecimal getProLimitQty() {
        return this.proLimitQty;
    }

    public BigDecimal getProMaxQty() {
        return this.proMaxQty;
    }

    public String getProPackageFlag() {
        return this.proPackageFlag;
    }

    public String getProKeyCare() {
        return this.proKeyCare;
    }

    public String getProTcmSpecs() {
        return this.proTcmSpecs;
    }

    public String getProTcmRegisterNo() {
        return this.proTcmRegisterNo;
    }

    public String getProTcmFactoryCode() {
        return this.proTcmFactoryCode;
    }

    public String getProTcmPlace() {
        return this.proTcmPlace;
    }

    public void setClient(final String client) {
        this.client = client;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProSite(final String proSite) {
        this.proSite = proSite;
    }

    public void setProSelfCode(final String proSelfCode) {
        this.proSelfCode = proSelfCode;
    }

    public void setProMatchStatus(final String proMatchStatus) {
        this.proMatchStatus = proMatchStatus;
    }

    public void setProCommonname(final String proCommonname) {
        this.proCommonname = proCommonname;
    }

    public void setProDepict(final String proDepict) {
        this.proDepict = proDepict;
    }

    public void setProPym(final String proPym) {
        this.proPym = proPym;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProPartform(final String proPartform) {
        this.proPartform = proPartform;
    }

    public void setProMindose(final String proMindose) {
        this.proMindose = proMindose;
    }

    public void setProTotaldose(final String proTotaldose) {
        this.proTotaldose = proTotaldose;
    }

    public void setProBarcode(final String proBarcode) {
        this.proBarcode = proBarcode;
    }

    public void setProBarcode2(final String proBarcode2) {
        this.proBarcode2 = proBarcode2;
    }

    public void setProRegisterClass(final String proRegisterClass) {
        this.proRegisterClass = proRegisterClass;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public void setProRegisterDate(final String proRegisterDate) {
        this.proRegisterDate = proRegisterDate;
    }

    public void setProRegisterExdate(final String proRegisterExdate) {
        this.proRegisterExdate = proRegisterExdate;
    }

    public void setProClass(final String proClass) {
        this.proClass = proClass;
    }

    public void setProClassName(final String proClassName) {
        this.proClassName = proClassName;
    }

    public void setProCompclass(final String proCompclass) {
        this.proCompclass = proCompclass;
    }

    public void setProCompclassName(final String proCompclassName) {
        this.proCompclassName = proCompclassName;
    }

    public void setProPresclass(final String proPresclass) {
        this.proPresclass = proPresclass;
    }

    public void setProFactoryCode(final String proFactoryCode) {
        this.proFactoryCode = proFactoryCode;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProMark(final String proMark) {
        this.proMark = proMark;
    }

    public void setProBrand(final String proBrand) {
        this.proBrand = proBrand;
    }

    public void setProBrandClass(final String proBrandClass) {
        this.proBrandClass = proBrandClass;
    }

    public void setProLife(final String proLife) {
        this.proLife = proLife;
    }

    public void setProLifeUnit(final String proLifeUnit) {
        this.proLifeUnit = proLifeUnit;
    }

    public void setProHolder(final String proHolder) {
        this.proHolder = proHolder;
    }

    public void setProInputTax(final String proInputTax) {
        this.proInputTax = proInputTax;
    }

    public void setProOutputTax(final String proOutputTax) {
        this.proOutputTax = proOutputTax;
    }

    public void setProBasicCode(final String proBasicCode) {
        this.proBasicCode = proBasicCode;
    }

    public void setProTaxClass(final String proTaxClass) {
        this.proTaxClass = proTaxClass;
    }

    public void setProControlClass(final String proControlClass) {
        this.proControlClass = proControlClass;
    }

    public void setProProduceClass(final String proProduceClass) {
        this.proProduceClass = proProduceClass;
    }

    public void setProStorageCondition(final String proStorageCondition) {
        this.proStorageCondition = proStorageCondition;
    }

    public void setProStorageArea(final String proStorageArea) {
        this.proStorageArea = proStorageArea;
    }

    public void setProLong(final String proLong) {
        this.proLong = proLong;
    }

    public void setProWide(final String proWide) {
        this.proWide = proWide;
    }

    public void setProHigh(final String proHigh) {
        this.proHigh = proHigh;
    }

    public void setProMidPackage(final String proMidPackage) {
        this.proMidPackage = proMidPackage;
    }

    public void setProBigPackage(final String proBigPackage) {
        this.proBigPackage = proBigPackage;
    }

    public void setProElectronicCode(final String proElectronicCode) {
        this.proElectronicCode = proElectronicCode;
    }

    public void setProQsCode(final String proQsCode) {
        this.proQsCode = proQsCode;
    }

    public void setProMaxSales(final String proMaxSales) {
        this.proMaxSales = proMaxSales;
    }

    public void setProInstructionCode(final String proInstructionCode) {
        this.proInstructionCode = proInstructionCode;
    }

    public void setProInstruction(final String proInstruction) {
        this.proInstruction = proInstruction;
    }

    public void setProMedProdct(final String proMedProdct) {
        this.proMedProdct = proMedProdct;
    }

    public void setProStatus(final String proStatus) {
        this.proStatus = proStatus;
    }

    public void setProMedProdctcode(final String proMedProdctcode) {
        this.proMedProdctcode = proMedProdctcode;
    }

    public void setProCountry(final String proCountry) {
        this.proCountry = proCountry;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProTakeDays(final String proTakeDays) {
        this.proTakeDays = proTakeDays;
    }

    public void setProUsage(final String proUsage) {
        this.proUsage = proUsage;
    }

    public void setProContraindication(final String proContraindication) {
        this.proContraindication = proContraindication;
    }

    public void setProPosition(final String proPosition) {
        this.proPosition = proPosition;
    }

    public void setProNoRetail(final String proNoRetail) {
        this.proNoRetail = proNoRetail;
    }

    public void setProNoPurchase(final String proNoPurchase) {
        this.proNoPurchase = proNoPurchase;
    }

    public void setProNoDistributed(final String proNoDistributed) {
        this.proNoDistributed = proNoDistributed;
    }

    public void setProNoSupplier(final String proNoSupplier) {
        this.proNoSupplier = proNoSupplier;
    }

    public void setProNoDc(final String proNoDc) {
        this.proNoDc = proNoDc;
    }

    public void setProNoAdjust(final String proNoAdjust) {
        this.proNoAdjust = proNoAdjust;
    }

    public void setProNoSale(final String proNoSale) {
        this.proNoSale = proNoSale;
    }

    public void setProNoApply(final String proNoApply) {
        this.proNoApply = proNoApply;
    }

    public void setProIfpart(final String proIfpart) {
        this.proIfpart = proIfpart;
    }

    public void setProPartUint(final String proPartUint) {
        this.proPartUint = proPartUint;
    }

    public void setProPartRate(final String proPartRate) {
        this.proPartRate = proPartRate;
    }

    public void setProPurchaseUnit(final String proPurchaseUnit) {
        this.proPurchaseUnit = proPurchaseUnit;
    }

    public void setProPurchaseRate(final String proPurchaseRate) {
        this.proPurchaseRate = proPurchaseRate;
    }

    public void setProSaleUnit(final String proSaleUnit) {
        this.proSaleUnit = proSaleUnit;
    }

    public void setProSaleRate(final String proSaleRate) {
        this.proSaleRate = proSaleRate;
    }

    public void setProMinQty(final BigDecimal proMinQty) {
        this.proMinQty = proMinQty;
    }

    public void setProIfMed(final String proIfMed) {
        this.proIfMed = proIfMed;
    }

    public void setProSlaeClass(final String proSlaeClass) {
        this.proSlaeClass = proSlaeClass;
    }

    public void setProLimitQty(final BigDecimal proLimitQty) {
        this.proLimitQty = proLimitQty;
    }

    public void setProMaxQty(final BigDecimal proMaxQty) {
        this.proMaxQty = proMaxQty;
    }

    public void setProPackageFlag(final String proPackageFlag) {
        this.proPackageFlag = proPackageFlag;
    }

    public void setProKeyCare(final String proKeyCare) {
        this.proKeyCare = proKeyCare;
    }

    public void setProTcmSpecs(final String proTcmSpecs) {
        this.proTcmSpecs = proTcmSpecs;
    }

    public void setProTcmRegisterNo(final String proTcmRegisterNo) {
        this.proTcmRegisterNo = proTcmRegisterNo;
    }

    public void setProTcmFactoryCode(final String proTcmFactoryCode) {
        this.proTcmFactoryCode = proTcmFactoryCode;
    }

    public void setProTcmPlace(final String proTcmPlace) {
        this.proTcmPlace = proTcmPlace;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncProductBusinessOutData)) {
            return false;
        } else {
            GetSyncProductBusinessOutData other = (GetSyncProductBusinessOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label1055: {
                    Object this$client = this.getClient();
                    Object other$client = other.getClient();
                    if (this$client == null) {
                        if (other$client == null) {
                            break label1055;
                        }
                    } else if (this$client.equals(other$client)) {
                        break label1055;
                    }

                    return false;
                }

                Object this$proCode = this.getProCode();
                Object other$proCode = other.getProCode();
                if (this$proCode == null) {
                    if (other$proCode != null) {
                        return false;
                    }
                } else if (!this$proCode.equals(other$proCode)) {
                    return false;
                }

                Object this$proSite = this.getProSite();
                Object other$proSite = other.getProSite();
                if (this$proSite == null) {
                    if (other$proSite != null) {
                        return false;
                    }
                } else if (!this$proSite.equals(other$proSite)) {
                    return false;
                }

                label1034: {
                    Object this$proSelfCode = this.getProSelfCode();
                    Object other$proSelfCode = other.getProSelfCode();
                    if (this$proSelfCode == null) {
                        if (other$proSelfCode == null) {
                            break label1034;
                        }
                    } else if (this$proSelfCode.equals(other$proSelfCode)) {
                        break label1034;
                    }

                    return false;
                }

                label1027: {
                    Object this$proMatchStatus = this.getProMatchStatus();
                    Object other$proMatchStatus = other.getProMatchStatus();
                    if (this$proMatchStatus == null) {
                        if (other$proMatchStatus == null) {
                            break label1027;
                        }
                    } else if (this$proMatchStatus.equals(other$proMatchStatus)) {
                        break label1027;
                    }

                    return false;
                }

                Object this$proCommonname = this.getProCommonname();
                Object other$proCommonname = other.getProCommonname();
                if (this$proCommonname == null) {
                    if (other$proCommonname != null) {
                        return false;
                    }
                } else if (!this$proCommonname.equals(other$proCommonname)) {
                    return false;
                }

                Object this$proDepict = this.getProDepict();
                Object other$proDepict = other.getProDepict();
                if (this$proDepict == null) {
                    if (other$proDepict != null) {
                        return false;
                    }
                } else if (!this$proDepict.equals(other$proDepict)) {
                    return false;
                }

                label1006: {
                    Object this$proPym = this.getProPym();
                    Object other$proPym = other.getProPym();
                    if (this$proPym == null) {
                        if (other$proPym == null) {
                            break label1006;
                        }
                    } else if (this$proPym.equals(other$proPym)) {
                        break label1006;
                    }

                    return false;
                }

                label999: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label999;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label999;
                    }

                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                label985: {
                    Object this$proUnit = this.getProUnit();
                    Object other$proUnit = other.getProUnit();
                    if (this$proUnit == null) {
                        if (other$proUnit == null) {
                            break label985;
                        }
                    } else if (this$proUnit.equals(other$proUnit)) {
                        break label985;
                    }

                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                label971: {
                    Object this$proPartform = this.getProPartform();
                    Object other$proPartform = other.getProPartform();
                    if (this$proPartform == null) {
                        if (other$proPartform == null) {
                            break label971;
                        }
                    } else if (this$proPartform.equals(other$proPartform)) {
                        break label971;
                    }

                    return false;
                }

                Object this$proMindose = this.getProMindose();
                Object other$proMindose = other.getProMindose();
                if (this$proMindose == null) {
                    if (other$proMindose != null) {
                        return false;
                    }
                } else if (!this$proMindose.equals(other$proMindose)) {
                    return false;
                }

                Object this$proTotaldose = this.getProTotaldose();
                Object other$proTotaldose = other.getProTotaldose();
                if (this$proTotaldose == null) {
                    if (other$proTotaldose != null) {
                        return false;
                    }
                } else if (!this$proTotaldose.equals(other$proTotaldose)) {
                    return false;
                }

                label950: {
                    Object this$proBarcode = this.getProBarcode();
                    Object other$proBarcode = other.getProBarcode();
                    if (this$proBarcode == null) {
                        if (other$proBarcode == null) {
                            break label950;
                        }
                    } else if (this$proBarcode.equals(other$proBarcode)) {
                        break label950;
                    }

                    return false;
                }

                label943: {
                    Object this$proBarcode2 = this.getProBarcode2();
                    Object other$proBarcode2 = other.getProBarcode2();
                    if (this$proBarcode2 == null) {
                        if (other$proBarcode2 == null) {
                            break label943;
                        }
                    } else if (this$proBarcode2.equals(other$proBarcode2)) {
                        break label943;
                    }

                    return false;
                }

                Object this$proRegisterClass = this.getProRegisterClass();
                Object other$proRegisterClass = other.getProRegisterClass();
                if (this$proRegisterClass == null) {
                    if (other$proRegisterClass != null) {
                        return false;
                    }
                } else if (!this$proRegisterClass.equals(other$proRegisterClass)) {
                    return false;
                }

                Object this$proRegisterNo = this.getProRegisterNo();
                Object other$proRegisterNo = other.getProRegisterNo();
                if (this$proRegisterNo == null) {
                    if (other$proRegisterNo != null) {
                        return false;
                    }
                } else if (!this$proRegisterNo.equals(other$proRegisterNo)) {
                    return false;
                }

                label922: {
                    Object this$proRegisterDate = this.getProRegisterDate();
                    Object other$proRegisterDate = other.getProRegisterDate();
                    if (this$proRegisterDate == null) {
                        if (other$proRegisterDate == null) {
                            break label922;
                        }
                    } else if (this$proRegisterDate.equals(other$proRegisterDate)) {
                        break label922;
                    }

                    return false;
                }

                label915: {
                    Object this$proRegisterExdate = this.getProRegisterExdate();
                    Object other$proRegisterExdate = other.getProRegisterExdate();
                    if (this$proRegisterExdate == null) {
                        if (other$proRegisterExdate == null) {
                            break label915;
                        }
                    } else if (this$proRegisterExdate.equals(other$proRegisterExdate)) {
                        break label915;
                    }

                    return false;
                }

                Object this$proClass = this.getProClass();
                Object other$proClass = other.getProClass();
                if (this$proClass == null) {
                    if (other$proClass != null) {
                        return false;
                    }
                } else if (!this$proClass.equals(other$proClass)) {
                    return false;
                }

                Object this$proClassName = this.getProClassName();
                Object other$proClassName = other.getProClassName();
                if (this$proClassName == null) {
                    if (other$proClassName != null) {
                        return false;
                    }
                } else if (!this$proClassName.equals(other$proClassName)) {
                    return false;
                }

                label894: {
                    Object this$proCompclass = this.getProCompclass();
                    Object other$proCompclass = other.getProCompclass();
                    if (this$proCompclass == null) {
                        if (other$proCompclass == null) {
                            break label894;
                        }
                    } else if (this$proCompclass.equals(other$proCompclass)) {
                        break label894;
                    }

                    return false;
                }

                label887: {
                    Object this$proCompclassName = this.getProCompclassName();
                    Object other$proCompclassName = other.getProCompclassName();
                    if (this$proCompclassName == null) {
                        if (other$proCompclassName == null) {
                            break label887;
                        }
                    } else if (this$proCompclassName.equals(other$proCompclassName)) {
                        break label887;
                    }

                    return false;
                }

                Object this$proPresclass = this.getProPresclass();
                Object other$proPresclass = other.getProPresclass();
                if (this$proPresclass == null) {
                    if (other$proPresclass != null) {
                        return false;
                    }
                } else if (!this$proPresclass.equals(other$proPresclass)) {
                    return false;
                }

                label873: {
                    Object this$proFactoryCode = this.getProFactoryCode();
                    Object other$proFactoryCode = other.getProFactoryCode();
                    if (this$proFactoryCode == null) {
                        if (other$proFactoryCode == null) {
                            break label873;
                        }
                    } else if (this$proFactoryCode.equals(other$proFactoryCode)) {
                        break label873;
                    }

                    return false;
                }

                Object this$proFactoryName = this.getProFactoryName();
                Object other$proFactoryName = other.getProFactoryName();
                if (this$proFactoryName == null) {
                    if (other$proFactoryName != null) {
                        return false;
                    }
                } else if (!this$proFactoryName.equals(other$proFactoryName)) {
                    return false;
                }

                label859: {
                    Object this$proMark = this.getProMark();
                    Object other$proMark = other.getProMark();
                    if (this$proMark == null) {
                        if (other$proMark == null) {
                            break label859;
                        }
                    } else if (this$proMark.equals(other$proMark)) {
                        break label859;
                    }

                    return false;
                }

                Object this$proBrand = this.getProBrand();
                Object other$proBrand = other.getProBrand();
                if (this$proBrand == null) {
                    if (other$proBrand != null) {
                        return false;
                    }
                } else if (!this$proBrand.equals(other$proBrand)) {
                    return false;
                }

                Object this$proBrandClass = this.getProBrandClass();
                Object other$proBrandClass = other.getProBrandClass();
                if (this$proBrandClass == null) {
                    if (other$proBrandClass != null) {
                        return false;
                    }
                } else if (!this$proBrandClass.equals(other$proBrandClass)) {
                    return false;
                }

                label838: {
                    Object this$proLife = this.getProLife();
                    Object other$proLife = other.getProLife();
                    if (this$proLife == null) {
                        if (other$proLife == null) {
                            break label838;
                        }
                    } else if (this$proLife.equals(other$proLife)) {
                        break label838;
                    }

                    return false;
                }

                label831: {
                    Object this$proLifeUnit = this.getProLifeUnit();
                    Object other$proLifeUnit = other.getProLifeUnit();
                    if (this$proLifeUnit == null) {
                        if (other$proLifeUnit == null) {
                            break label831;
                        }
                    } else if (this$proLifeUnit.equals(other$proLifeUnit)) {
                        break label831;
                    }

                    return false;
                }

                Object this$proHolder = this.getProHolder();
                Object other$proHolder = other.getProHolder();
                if (this$proHolder == null) {
                    if (other$proHolder != null) {
                        return false;
                    }
                } else if (!this$proHolder.equals(other$proHolder)) {
                    return false;
                }

                Object this$proInputTax = this.getProInputTax();
                Object other$proInputTax = other.getProInputTax();
                if (this$proInputTax == null) {
                    if (other$proInputTax != null) {
                        return false;
                    }
                } else if (!this$proInputTax.equals(other$proInputTax)) {
                    return false;
                }

                label810: {
                    Object this$proOutputTax = this.getProOutputTax();
                    Object other$proOutputTax = other.getProOutputTax();
                    if (this$proOutputTax == null) {
                        if (other$proOutputTax == null) {
                            break label810;
                        }
                    } else if (this$proOutputTax.equals(other$proOutputTax)) {
                        break label810;
                    }

                    return false;
                }

                label803: {
                    Object this$proBasicCode = this.getProBasicCode();
                    Object other$proBasicCode = other.getProBasicCode();
                    if (this$proBasicCode == null) {
                        if (other$proBasicCode == null) {
                            break label803;
                        }
                    } else if (this$proBasicCode.equals(other$proBasicCode)) {
                        break label803;
                    }

                    return false;
                }

                Object this$proTaxClass = this.getProTaxClass();
                Object other$proTaxClass = other.getProTaxClass();
                if (this$proTaxClass == null) {
                    if (other$proTaxClass != null) {
                        return false;
                    }
                } else if (!this$proTaxClass.equals(other$proTaxClass)) {
                    return false;
                }

                Object this$proControlClass = this.getProControlClass();
                Object other$proControlClass = other.getProControlClass();
                if (this$proControlClass == null) {
                    if (other$proControlClass != null) {
                        return false;
                    }
                } else if (!this$proControlClass.equals(other$proControlClass)) {
                    return false;
                }

                label782: {
                    Object this$proProduceClass = this.getProProduceClass();
                    Object other$proProduceClass = other.getProProduceClass();
                    if (this$proProduceClass == null) {
                        if (other$proProduceClass == null) {
                            break label782;
                        }
                    } else if (this$proProduceClass.equals(other$proProduceClass)) {
                        break label782;
                    }

                    return false;
                }

                label775: {
                    Object this$proStorageCondition = this.getProStorageCondition();
                    Object other$proStorageCondition = other.getProStorageCondition();
                    if (this$proStorageCondition == null) {
                        if (other$proStorageCondition == null) {
                            break label775;
                        }
                    } else if (this$proStorageCondition.equals(other$proStorageCondition)) {
                        break label775;
                    }

                    return false;
                }

                Object this$proStorageArea = this.getProStorageArea();
                Object other$proStorageArea = other.getProStorageArea();
                if (this$proStorageArea == null) {
                    if (other$proStorageArea != null) {
                        return false;
                    }
                } else if (!this$proStorageArea.equals(other$proStorageArea)) {
                    return false;
                }

                label761: {
                    Object this$proLong = this.getProLong();
                    Object other$proLong = other.getProLong();
                    if (this$proLong == null) {
                        if (other$proLong == null) {
                            break label761;
                        }
                    } else if (this$proLong.equals(other$proLong)) {
                        break label761;
                    }

                    return false;
                }

                Object this$proWide = this.getProWide();
                Object other$proWide = other.getProWide();
                if (this$proWide == null) {
                    if (other$proWide != null) {
                        return false;
                    }
                } else if (!this$proWide.equals(other$proWide)) {
                    return false;
                }

                label747: {
                    Object this$proHigh = this.getProHigh();
                    Object other$proHigh = other.getProHigh();
                    if (this$proHigh == null) {
                        if (other$proHigh == null) {
                            break label747;
                        }
                    } else if (this$proHigh.equals(other$proHigh)) {
                        break label747;
                    }

                    return false;
                }

                Object this$proMidPackage = this.getProMidPackage();
                Object other$proMidPackage = other.getProMidPackage();
                if (this$proMidPackage == null) {
                    if (other$proMidPackage != null) {
                        return false;
                    }
                } else if (!this$proMidPackage.equals(other$proMidPackage)) {
                    return false;
                }

                Object this$proBigPackage = this.getProBigPackage();
                Object other$proBigPackage = other.getProBigPackage();
                if (this$proBigPackage == null) {
                    if (other$proBigPackage != null) {
                        return false;
                    }
                } else if (!this$proBigPackage.equals(other$proBigPackage)) {
                    return false;
                }

                label726: {
                    Object this$proElectronicCode = this.getProElectronicCode();
                    Object other$proElectronicCode = other.getProElectronicCode();
                    if (this$proElectronicCode == null) {
                        if (other$proElectronicCode == null) {
                            break label726;
                        }
                    } else if (this$proElectronicCode.equals(other$proElectronicCode)) {
                        break label726;
                    }

                    return false;
                }

                label719: {
                    Object this$proQsCode = this.getProQsCode();
                    Object other$proQsCode = other.getProQsCode();
                    if (this$proQsCode == null) {
                        if (other$proQsCode == null) {
                            break label719;
                        }
                    } else if (this$proQsCode.equals(other$proQsCode)) {
                        break label719;
                    }

                    return false;
                }

                Object this$proMaxSales = this.getProMaxSales();
                Object other$proMaxSales = other.getProMaxSales();
                if (this$proMaxSales == null) {
                    if (other$proMaxSales != null) {
                        return false;
                    }
                } else if (!this$proMaxSales.equals(other$proMaxSales)) {
                    return false;
                }

                Object this$proInstructionCode = this.getProInstructionCode();
                Object other$proInstructionCode = other.getProInstructionCode();
                if (this$proInstructionCode == null) {
                    if (other$proInstructionCode != null) {
                        return false;
                    }
                } else if (!this$proInstructionCode.equals(other$proInstructionCode)) {
                    return false;
                }

                label698: {
                    Object this$proInstruction = this.getProInstruction();
                    Object other$proInstruction = other.getProInstruction();
                    if (this$proInstruction == null) {
                        if (other$proInstruction == null) {
                            break label698;
                        }
                    } else if (this$proInstruction.equals(other$proInstruction)) {
                        break label698;
                    }

                    return false;
                }

                label691: {
                    Object this$proMedProdct = this.getProMedProdct();
                    Object other$proMedProdct = other.getProMedProdct();
                    if (this$proMedProdct == null) {
                        if (other$proMedProdct == null) {
                            break label691;
                        }
                    } else if (this$proMedProdct.equals(other$proMedProdct)) {
                        break label691;
                    }

                    return false;
                }

                Object this$proStatus = this.getProStatus();
                Object other$proStatus = other.getProStatus();
                if (this$proStatus == null) {
                    if (other$proStatus != null) {
                        return false;
                    }
                } else if (!this$proStatus.equals(other$proStatus)) {
                    return false;
                }

                Object this$proMedProdctcode = this.getProMedProdctcode();
                Object other$proMedProdctcode = other.getProMedProdctcode();
                if (this$proMedProdctcode == null) {
                    if (other$proMedProdctcode != null) {
                        return false;
                    }
                } else if (!this$proMedProdctcode.equals(other$proMedProdctcode)) {
                    return false;
                }

                label670: {
                    Object this$proCountry = this.getProCountry();
                    Object other$proCountry = other.getProCountry();
                    if (this$proCountry == null) {
                        if (other$proCountry == null) {
                            break label670;
                        }
                    } else if (this$proCountry.equals(other$proCountry)) {
                        break label670;
                    }

                    return false;
                }

                label663: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label663;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label663;
                    }

                    return false;
                }

                Object this$proTakeDays = this.getProTakeDays();
                Object other$proTakeDays = other.getProTakeDays();
                if (this$proTakeDays == null) {
                    if (other$proTakeDays != null) {
                        return false;
                    }
                } else if (!this$proTakeDays.equals(other$proTakeDays)) {
                    return false;
                }

                label649: {
                    Object this$proUsage = this.getProUsage();
                    Object other$proUsage = other.getProUsage();
                    if (this$proUsage == null) {
                        if (other$proUsage == null) {
                            break label649;
                        }
                    } else if (this$proUsage.equals(other$proUsage)) {
                        break label649;
                    }

                    return false;
                }

                Object this$proContraindication = this.getProContraindication();
                Object other$proContraindication = other.getProContraindication();
                if (this$proContraindication == null) {
                    if (other$proContraindication != null) {
                        return false;
                    }
                } else if (!this$proContraindication.equals(other$proContraindication)) {
                    return false;
                }

                label635: {
                    Object this$proPosition = this.getProPosition();
                    Object other$proPosition = other.getProPosition();
                    if (this$proPosition == null) {
                        if (other$proPosition == null) {
                            break label635;
                        }
                    } else if (this$proPosition.equals(other$proPosition)) {
                        break label635;
                    }

                    return false;
                }

                Object this$proNoRetail = this.getProNoRetail();
                Object other$proNoRetail = other.getProNoRetail();
                if (this$proNoRetail == null) {
                    if (other$proNoRetail != null) {
                        return false;
                    }
                } else if (!this$proNoRetail.equals(other$proNoRetail)) {
                    return false;
                }

                Object this$proNoPurchase = this.getProNoPurchase();
                Object other$proNoPurchase = other.getProNoPurchase();
                if (this$proNoPurchase == null) {
                    if (other$proNoPurchase != null) {
                        return false;
                    }
                } else if (!this$proNoPurchase.equals(other$proNoPurchase)) {
                    return false;
                }

                label614: {
                    Object this$proNoDistributed = this.getProNoDistributed();
                    Object other$proNoDistributed = other.getProNoDistributed();
                    if (this$proNoDistributed == null) {
                        if (other$proNoDistributed == null) {
                            break label614;
                        }
                    } else if (this$proNoDistributed.equals(other$proNoDistributed)) {
                        break label614;
                    }

                    return false;
                }

                label607: {
                    Object this$proNoSupplier = this.getProNoSupplier();
                    Object other$proNoSupplier = other.getProNoSupplier();
                    if (this$proNoSupplier == null) {
                        if (other$proNoSupplier == null) {
                            break label607;
                        }
                    } else if (this$proNoSupplier.equals(other$proNoSupplier)) {
                        break label607;
                    }

                    return false;
                }

                Object this$proNoDc = this.getProNoDc();
                Object other$proNoDc = other.getProNoDc();
                if (this$proNoDc == null) {
                    if (other$proNoDc != null) {
                        return false;
                    }
                } else if (!this$proNoDc.equals(other$proNoDc)) {
                    return false;
                }

                Object this$proNoAdjust = this.getProNoAdjust();
                Object other$proNoAdjust = other.getProNoAdjust();
                if (this$proNoAdjust == null) {
                    if (other$proNoAdjust != null) {
                        return false;
                    }
                } else if (!this$proNoAdjust.equals(other$proNoAdjust)) {
                    return false;
                }

                label586: {
                    Object this$proNoSale = this.getProNoSale();
                    Object other$proNoSale = other.getProNoSale();
                    if (this$proNoSale == null) {
                        if (other$proNoSale == null) {
                            break label586;
                        }
                    } else if (this$proNoSale.equals(other$proNoSale)) {
                        break label586;
                    }

                    return false;
                }

                label579: {
                    Object this$proNoApply = this.getProNoApply();
                    Object other$proNoApply = other.getProNoApply();
                    if (this$proNoApply == null) {
                        if (other$proNoApply == null) {
                            break label579;
                        }
                    } else if (this$proNoApply.equals(other$proNoApply)) {
                        break label579;
                    }

                    return false;
                }

                Object this$proIfpart = this.getProIfpart();
                Object other$proIfpart = other.getProIfpart();
                if (this$proIfpart == null) {
                    if (other$proIfpart != null) {
                        return false;
                    }
                } else if (!this$proIfpart.equals(other$proIfpart)) {
                    return false;
                }

                Object this$proPartUint = this.getProPartUint();
                Object other$proPartUint = other.getProPartUint();
                if (this$proPartUint == null) {
                    if (other$proPartUint != null) {
                        return false;
                    }
                } else if (!this$proPartUint.equals(other$proPartUint)) {
                    return false;
                }

                label558: {
                    Object this$proPartRate = this.getProPartRate();
                    Object other$proPartRate = other.getProPartRate();
                    if (this$proPartRate == null) {
                        if (other$proPartRate == null) {
                            break label558;
                        }
                    } else if (this$proPartRate.equals(other$proPartRate)) {
                        break label558;
                    }

                    return false;
                }

                label551: {
                    Object this$proPurchaseUnit = this.getProPurchaseUnit();
                    Object other$proPurchaseUnit = other.getProPurchaseUnit();
                    if (this$proPurchaseUnit == null) {
                        if (other$proPurchaseUnit == null) {
                            break label551;
                        }
                    } else if (this$proPurchaseUnit.equals(other$proPurchaseUnit)) {
                        break label551;
                    }

                    return false;
                }

                Object this$proPurchaseRate = this.getProPurchaseRate();
                Object other$proPurchaseRate = other.getProPurchaseRate();
                if (this$proPurchaseRate == null) {
                    if (other$proPurchaseRate != null) {
                        return false;
                    }
                } else if (!this$proPurchaseRate.equals(other$proPurchaseRate)) {
                    return false;
                }

                label537: {
                    Object this$proSaleUnit = this.getProSaleUnit();
                    Object other$proSaleUnit = other.getProSaleUnit();
                    if (this$proSaleUnit == null) {
                        if (other$proSaleUnit == null) {
                            break label537;
                        }
                    } else if (this$proSaleUnit.equals(other$proSaleUnit)) {
                        break label537;
                    }

                    return false;
                }

                Object this$proSaleRate = this.getProSaleRate();
                Object other$proSaleRate = other.getProSaleRate();
                if (this$proSaleRate == null) {
                    if (other$proSaleRate != null) {
                        return false;
                    }
                } else if (!this$proSaleRate.equals(other$proSaleRate)) {
                    return false;
                }

                label523: {
                    Object this$proMinQty = this.getProMinQty();
                    Object other$proMinQty = other.getProMinQty();
                    if (this$proMinQty == null) {
                        if (other$proMinQty == null) {
                            break label523;
                        }
                    } else if (this$proMinQty.equals(other$proMinQty)) {
                        break label523;
                    }

                    return false;
                }

                Object this$proIfMed = this.getProIfMed();
                Object other$proIfMed = other.getProIfMed();
                if (this$proIfMed == null) {
                    if (other$proIfMed != null) {
                        return false;
                    }
                } else if (!this$proIfMed.equals(other$proIfMed)) {
                    return false;
                }

                Object this$proSlaeClass = this.getProSlaeClass();
                Object other$proSlaeClass = other.getProSlaeClass();
                if (this$proSlaeClass == null) {
                    if (other$proSlaeClass != null) {
                        return false;
                    }
                } else if (!this$proSlaeClass.equals(other$proSlaeClass)) {
                    return false;
                }

                label502: {
                    Object this$proLimitQty = this.getProLimitQty();
                    Object other$proLimitQty = other.getProLimitQty();
                    if (this$proLimitQty == null) {
                        if (other$proLimitQty == null) {
                            break label502;
                        }
                    } else if (this$proLimitQty.equals(other$proLimitQty)) {
                        break label502;
                    }

                    return false;
                }

                label495: {
                    Object this$proMaxQty = this.getProMaxQty();
                    Object other$proMaxQty = other.getProMaxQty();
                    if (this$proMaxQty == null) {
                        if (other$proMaxQty == null) {
                            break label495;
                        }
                    } else if (this$proMaxQty.equals(other$proMaxQty)) {
                        break label495;
                    }

                    return false;
                }

                Object this$proPackageFlag = this.getProPackageFlag();
                Object other$proPackageFlag = other.getProPackageFlag();
                if (this$proPackageFlag == null) {
                    if (other$proPackageFlag != null) {
                        return false;
                    }
                } else if (!this$proPackageFlag.equals(other$proPackageFlag)) {
                    return false;
                }

                Object this$proKeyCare = this.getProKeyCare();
                Object other$proKeyCare = other.getProKeyCare();
                if (this$proKeyCare == null) {
                    if (other$proKeyCare != null) {
                        return false;
                    }
                } else if (!this$proKeyCare.equals(other$proKeyCare)) {
                    return false;
                }

                label474: {
                    Object this$proTcmSpecs = this.getProTcmSpecs();
                    Object other$proTcmSpecs = other.getProTcmSpecs();
                    if (this$proTcmSpecs == null) {
                        if (other$proTcmSpecs == null) {
                            break label474;
                        }
                    } else if (this$proTcmSpecs.equals(other$proTcmSpecs)) {
                        break label474;
                    }

                    return false;
                }

                label467: {
                    Object this$proTcmRegisterNo = this.getProTcmRegisterNo();
                    Object other$proTcmRegisterNo = other.getProTcmRegisterNo();
                    if (this$proTcmRegisterNo == null) {
                        if (other$proTcmRegisterNo == null) {
                            break label467;
                        }
                    } else if (this$proTcmRegisterNo.equals(other$proTcmRegisterNo)) {
                        break label467;
                    }

                    return false;
                }

                Object this$proTcmFactoryCode = this.getProTcmFactoryCode();
                Object other$proTcmFactoryCode = other.getProTcmFactoryCode();
                if (this$proTcmFactoryCode == null) {
                    if (other$proTcmFactoryCode != null) {
                        return false;
                    }
                } else if (!this$proTcmFactoryCode.equals(other$proTcmFactoryCode)) {
                    return false;
                }

                Object this$proTcmPlace = this.getProTcmPlace();
                Object other$proTcmPlace = other.getProTcmPlace();
                if (this$proTcmPlace == null) {
                    if (other$proTcmPlace != null) {
                        return false;
                    }
                } else if (!this$proTcmPlace.equals(other$proTcmPlace)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncProductBusinessOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $client = this.getClient();
        result = result * 59 + ($client == null ? 43 : $client.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proSite = this.getProSite();
        result = result * 59 + ($proSite == null ? 43 : $proSite.hashCode());
        Object $proSelfCode = this.getProSelfCode();
        result = result * 59 + ($proSelfCode == null ? 43 : $proSelfCode.hashCode());
        Object $proMatchStatus = this.getProMatchStatus();
        result = result * 59 + ($proMatchStatus == null ? 43 : $proMatchStatus.hashCode());
        Object $proCommonname = this.getProCommonname();
        result = result * 59 + ($proCommonname == null ? 43 : $proCommonname.hashCode());
        Object $proDepict = this.getProDepict();
        result = result * 59 + ($proDepict == null ? 43 : $proDepict.hashCode());
        Object $proPym = this.getProPym();
        result = result * 59 + ($proPym == null ? 43 : $proPym.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proPartform = this.getProPartform();
        result = result * 59 + ($proPartform == null ? 43 : $proPartform.hashCode());
        Object $proMindose = this.getProMindose();
        result = result * 59 + ($proMindose == null ? 43 : $proMindose.hashCode());
        Object $proTotaldose = this.getProTotaldose();
        result = result * 59 + ($proTotaldose == null ? 43 : $proTotaldose.hashCode());
        Object $proBarcode = this.getProBarcode();
        result = result * 59 + ($proBarcode == null ? 43 : $proBarcode.hashCode());
        Object $proBarcode2 = this.getProBarcode2();
        result = result * 59 + ($proBarcode2 == null ? 43 : $proBarcode2.hashCode());
        Object $proRegisterClass = this.getProRegisterClass();
        result = result * 59 + ($proRegisterClass == null ? 43 : $proRegisterClass.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        Object $proRegisterDate = this.getProRegisterDate();
        result = result * 59 + ($proRegisterDate == null ? 43 : $proRegisterDate.hashCode());
        Object $proRegisterExdate = this.getProRegisterExdate();
        result = result * 59 + ($proRegisterExdate == null ? 43 : $proRegisterExdate.hashCode());
        Object $proClass = this.getProClass();
        result = result * 59 + ($proClass == null ? 43 : $proClass.hashCode());
        Object $proClassName = this.getProClassName();
        result = result * 59 + ($proClassName == null ? 43 : $proClassName.hashCode());
        Object $proCompclass = this.getProCompclass();
        result = result * 59 + ($proCompclass == null ? 43 : $proCompclass.hashCode());
        Object $proCompclassName = this.getProCompclassName();
        result = result * 59 + ($proCompclassName == null ? 43 : $proCompclassName.hashCode());
        Object $proPresclass = this.getProPresclass();
        result = result * 59 + ($proPresclass == null ? 43 : $proPresclass.hashCode());
        Object $proFactoryCode = this.getProFactoryCode();
        result = result * 59 + ($proFactoryCode == null ? 43 : $proFactoryCode.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proMark = this.getProMark();
        result = result * 59 + ($proMark == null ? 43 : $proMark.hashCode());
        Object $proBrand = this.getProBrand();
        result = result * 59 + ($proBrand == null ? 43 : $proBrand.hashCode());
        Object $proBrandClass = this.getProBrandClass();
        result = result * 59 + ($proBrandClass == null ? 43 : $proBrandClass.hashCode());
        Object $proLife = this.getProLife();
        result = result * 59 + ($proLife == null ? 43 : $proLife.hashCode());
        Object $proLifeUnit = this.getProLifeUnit();
        result = result * 59 + ($proLifeUnit == null ? 43 : $proLifeUnit.hashCode());
        Object $proHolder = this.getProHolder();
        result = result * 59 + ($proHolder == null ? 43 : $proHolder.hashCode());
        Object $proInputTax = this.getProInputTax();
        result = result * 59 + ($proInputTax == null ? 43 : $proInputTax.hashCode());
        Object $proOutputTax = this.getProOutputTax();
        result = result * 59 + ($proOutputTax == null ? 43 : $proOutputTax.hashCode());
        Object $proBasicCode = this.getProBasicCode();
        result = result * 59 + ($proBasicCode == null ? 43 : $proBasicCode.hashCode());
        Object $proTaxClass = this.getProTaxClass();
        result = result * 59 + ($proTaxClass == null ? 43 : $proTaxClass.hashCode());
        Object $proControlClass = this.getProControlClass();
        result = result * 59 + ($proControlClass == null ? 43 : $proControlClass.hashCode());
        Object $proProduceClass = this.getProProduceClass();
        result = result * 59 + ($proProduceClass == null ? 43 : $proProduceClass.hashCode());
        Object $proStorageCondition = this.getProStorageCondition();
        result = result * 59 + ($proStorageCondition == null ? 43 : $proStorageCondition.hashCode());
        Object $proStorageArea = this.getProStorageArea();
        result = result * 59 + ($proStorageArea == null ? 43 : $proStorageArea.hashCode());
        Object $proLong = this.getProLong();
        result = result * 59 + ($proLong == null ? 43 : $proLong.hashCode());
        Object $proWide = this.getProWide();
        result = result * 59 + ($proWide == null ? 43 : $proWide.hashCode());
        Object $proHigh = this.getProHigh();
        result = result * 59 + ($proHigh == null ? 43 : $proHigh.hashCode());
        Object $proMidPackage = this.getProMidPackage();
        result = result * 59 + ($proMidPackage == null ? 43 : $proMidPackage.hashCode());
        Object $proBigPackage = this.getProBigPackage();
        result = result * 59 + ($proBigPackage == null ? 43 : $proBigPackage.hashCode());
        Object $proElectronicCode = this.getProElectronicCode();
        result = result * 59 + ($proElectronicCode == null ? 43 : $proElectronicCode.hashCode());
        Object $proQsCode = this.getProQsCode();
        result = result * 59 + ($proQsCode == null ? 43 : $proQsCode.hashCode());
        Object $proMaxSales = this.getProMaxSales();
        result = result * 59 + ($proMaxSales == null ? 43 : $proMaxSales.hashCode());
        Object $proInstructionCode = this.getProInstructionCode();
        result = result * 59 + ($proInstructionCode == null ? 43 : $proInstructionCode.hashCode());
        Object $proInstruction = this.getProInstruction();
        result = result * 59 + ($proInstruction == null ? 43 : $proInstruction.hashCode());
        Object $proMedProdct = this.getProMedProdct();
        result = result * 59 + ($proMedProdct == null ? 43 : $proMedProdct.hashCode());
        Object $proStatus = this.getProStatus();
        result = result * 59 + ($proStatus == null ? 43 : $proStatus.hashCode());
        Object $proMedProdctcode = this.getProMedProdctcode();
        result = result * 59 + ($proMedProdctcode == null ? 43 : $proMedProdctcode.hashCode());
        Object $proCountry = this.getProCountry();
        result = result * 59 + ($proCountry == null ? 43 : $proCountry.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proTakeDays = this.getProTakeDays();
        result = result * 59 + ($proTakeDays == null ? 43 : $proTakeDays.hashCode());
        Object $proUsage = this.getProUsage();
        result = result * 59 + ($proUsage == null ? 43 : $proUsage.hashCode());
        Object $proContraindication = this.getProContraindication();
        result = result * 59 + ($proContraindication == null ? 43 : $proContraindication.hashCode());
        Object $proPosition = this.getProPosition();
        result = result * 59 + ($proPosition == null ? 43 : $proPosition.hashCode());
        Object $proNoRetail = this.getProNoRetail();
        result = result * 59 + ($proNoRetail == null ? 43 : $proNoRetail.hashCode());
        Object $proNoPurchase = this.getProNoPurchase();
        result = result * 59 + ($proNoPurchase == null ? 43 : $proNoPurchase.hashCode());
        Object $proNoDistributed = this.getProNoDistributed();
        result = result * 59 + ($proNoDistributed == null ? 43 : $proNoDistributed.hashCode());
        Object $proNoSupplier = this.getProNoSupplier();
        result = result * 59 + ($proNoSupplier == null ? 43 : $proNoSupplier.hashCode());
        Object $proNoDc = this.getProNoDc();
        result = result * 59 + ($proNoDc == null ? 43 : $proNoDc.hashCode());
        Object $proNoAdjust = this.getProNoAdjust();
        result = result * 59 + ($proNoAdjust == null ? 43 : $proNoAdjust.hashCode());
        Object $proNoSale = this.getProNoSale();
        result = result * 59 + ($proNoSale == null ? 43 : $proNoSale.hashCode());
        Object $proNoApply = this.getProNoApply();
        result = result * 59 + ($proNoApply == null ? 43 : $proNoApply.hashCode());
        Object $proIfpart = this.getProIfpart();
        result = result * 59 + ($proIfpart == null ? 43 : $proIfpart.hashCode());
        Object $proPartUint = this.getProPartUint();
        result = result * 59 + ($proPartUint == null ? 43 : $proPartUint.hashCode());
        Object $proPartRate = this.getProPartRate();
        result = result * 59 + ($proPartRate == null ? 43 : $proPartRate.hashCode());
        Object $proPurchaseUnit = this.getProPurchaseUnit();
        result = result * 59 + ($proPurchaseUnit == null ? 43 : $proPurchaseUnit.hashCode());
        Object $proPurchaseRate = this.getProPurchaseRate();
        result = result * 59 + ($proPurchaseRate == null ? 43 : $proPurchaseRate.hashCode());
        Object $proSaleUnit = this.getProSaleUnit();
        result = result * 59 + ($proSaleUnit == null ? 43 : $proSaleUnit.hashCode());
        Object $proSaleRate = this.getProSaleRate();
        result = result * 59 + ($proSaleRate == null ? 43 : $proSaleRate.hashCode());
        Object $proMinQty = this.getProMinQty();
        result = result * 59 + ($proMinQty == null ? 43 : $proMinQty.hashCode());
        Object $proIfMed = this.getProIfMed();
        result = result * 59 + ($proIfMed == null ? 43 : $proIfMed.hashCode());
        Object $proSlaeClass = this.getProSlaeClass();
        result = result * 59 + ($proSlaeClass == null ? 43 : $proSlaeClass.hashCode());
        Object $proLimitQty = this.getProLimitQty();
        result = result * 59 + ($proLimitQty == null ? 43 : $proLimitQty.hashCode());
        Object $proMaxQty = this.getProMaxQty();
        result = result * 59 + ($proMaxQty == null ? 43 : $proMaxQty.hashCode());
        Object $proPackageFlag = this.getProPackageFlag();
        result = result * 59 + ($proPackageFlag == null ? 43 : $proPackageFlag.hashCode());
        Object $proKeyCare = this.getProKeyCare();
        result = result * 59 + ($proKeyCare == null ? 43 : $proKeyCare.hashCode());
        Object $proTcmSpecs = this.getProTcmSpecs();
        result = result * 59 + ($proTcmSpecs == null ? 43 : $proTcmSpecs.hashCode());
        Object $proTcmRegisterNo = this.getProTcmRegisterNo();
        result = result * 59 + ($proTcmRegisterNo == null ? 43 : $proTcmRegisterNo.hashCode());
        Object $proTcmFactoryCode = this.getProTcmFactoryCode();
        result = result * 59 + ($proTcmFactoryCode == null ? 43 : $proTcmFactoryCode.hashCode());
        Object $proTcmPlace = this.getProTcmPlace();
        result = result * 59 + ($proTcmPlace == null ? 43 : $proTcmPlace.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncProductBusinessOutData(client=" + this.getClient() + ", proCode=" + this.getProCode() + ", proSite=" + this.getProSite() + ", proSelfCode=" + this.getProSelfCode() + ", proMatchStatus=" + this.getProMatchStatus() + ", proCommonname=" + this.getProCommonname() + ", proDepict=" + this.getProDepict() + ", proPym=" + this.getProPym() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", proUnit=" + this.getProUnit() + ", proForm=" + this.getProForm() + ", proPartform=" + this.getProPartform() + ", proMindose=" + this.getProMindose() + ", proTotaldose=" + this.getProTotaldose() + ", proBarcode=" + this.getProBarcode() + ", proBarcode2=" + this.getProBarcode2() + ", proRegisterClass=" + this.getProRegisterClass() + ", proRegisterNo=" + this.getProRegisterNo() + ", proRegisterDate=" + this.getProRegisterDate() + ", proRegisterExdate=" + this.getProRegisterExdate() + ", proClass=" + this.getProClass() + ", proClassName=" + this.getProClassName() + ", proCompclass=" + this.getProCompclass() + ", proCompclassName=" + this.getProCompclassName() + ", proPresclass=" + this.getProPresclass() + ", proFactoryCode=" + this.getProFactoryCode() + ", proFactoryName=" + this.getProFactoryName() + ", proMark=" + this.getProMark() + ", proBrand=" + this.getProBrand() + ", proBrandClass=" + this.getProBrandClass() + ", proLife=" + this.getProLife() + ", proLifeUnit=" + this.getProLifeUnit() + ", proHolder=" + this.getProHolder() + ", proInputTax=" + this.getProInputTax() + ", proOutputTax=" + this.getProOutputTax() + ", proBasicCode=" + this.getProBasicCode() + ", proTaxClass=" + this.getProTaxClass() + ", proControlClass=" + this.getProControlClass() + ", proProduceClass=" + this.getProProduceClass() + ", proStorageCondition=" + this.getProStorageCondition() + ", proStorageArea=" + this.getProStorageArea() + ", proLong=" + this.getProLong() + ", proWide=" + this.getProWide() + ", proHigh=" + this.getProHigh() + ", proMidPackage=" + this.getProMidPackage() + ", proBigPackage=" + this.getProBigPackage() + ", proElectronicCode=" + this.getProElectronicCode() + ", proQsCode=" + this.getProQsCode() + ", proMaxSales=" + this.getProMaxSales() + ", proInstructionCode=" + this.getProInstructionCode() + ", proInstruction=" + this.getProInstruction() + ", proMedProdct=" + this.getProMedProdct() + ", proStatus=" + this.getProStatus() + ", proMedProdctcode=" + this.getProMedProdctcode() + ", proCountry=" + this.getProCountry() + ", proPlace=" + this.getProPlace() + ", proTakeDays=" + this.getProTakeDays() + ", proUsage=" + this.getProUsage() + ", proContraindication=" + this.getProContraindication() + ", proPosition=" + this.getProPosition() + ", proNoRetail=" + this.getProNoRetail() + ", proNoPurchase=" + this.getProNoPurchase() + ", proNoDistributed=" + this.getProNoDistributed() + ", proNoSupplier=" + this.getProNoSupplier() + ", proNoDc=" + this.getProNoDc() + ", proNoAdjust=" + this.getProNoAdjust() + ", proNoSale=" + this.getProNoSale() + ", proNoApply=" + this.getProNoApply() + ", proIfpart=" + this.getProIfpart() + ", proPartUint=" + this.getProPartUint() + ", proPartRate=" + this.getProPartRate() + ", proPurchaseUnit=" + this.getProPurchaseUnit() + ", proPurchaseRate=" + this.getProPurchaseRate() + ", proSaleUnit=" + this.getProSaleUnit() + ", proSaleRate=" + this.getProSaleRate() + ", proMinQty=" + this.getProMinQty() + ", proIfMed=" + this.getProIfMed() + ", proSlaeClass=" + this.getProSlaeClass() + ", proLimitQty=" + this.getProLimitQty() + ", proMaxQty=" + this.getProMaxQty() + ", proPackageFlag=" + this.getProPackageFlag() + ", proKeyCare=" + this.getProKeyCare() + ", proTcmSpecs=" + this.getProTcmSpecs() + ", proTcmRegisterNo=" + this.getProTcmRegisterNo() + ", proTcmFactoryCode=" + this.getProTcmFactoryCode() + ", proTcmPlace=" + this.getProTcmPlace() + ")";
    }
}
