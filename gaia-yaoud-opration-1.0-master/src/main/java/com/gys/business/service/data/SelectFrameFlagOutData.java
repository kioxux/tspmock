package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SelectFrameFlagOutData implements Serializable {

    /**
     * 类型key
     */
    @ApiModelProperty(value="类型key")
    private String typeId;


    /**
     * 类型value
     */
    @ApiModelProperty(value="类型value")
    private String typeName;

    /**
     * 标识key
     */
    @ApiModelProperty(value="标识key")
    private String flagId;


    /**
     * 标识value
     */
    @ApiModelProperty(value="标识value")
    private String flagName;

}
