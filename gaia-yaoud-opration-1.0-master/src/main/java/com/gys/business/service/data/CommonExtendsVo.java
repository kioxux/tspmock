package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;


public class CommonExtendsVo extends CommonVo {

    private static final long serialVersionUID = -5389841119341725903L;
    private String groupName;

    private String groupCode;


    public CommonExtendsVo(String lable, String value) {
        super(lable, value);
    }

    public CommonExtendsVo(String lable, String value, String groupName) {
        super(lable, value);
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public CommonExtendsVo(String lable, String value, String groupName, String groupCode) {
        super(lable, value);
        this.groupName = groupName;
        this.groupCode = groupCode;
    }
}
