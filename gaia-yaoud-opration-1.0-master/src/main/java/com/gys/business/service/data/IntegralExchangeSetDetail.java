package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/11/30 9:56
 */
@Data
@CsvRow
public class IntegralExchangeSetDetail {
    /**
     * 编号
     */
    @CsvCell(title = "序号", index = 1, fieldNo = 1)
    private Integer index;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品编码
     */
    @CsvCell(title = "商品编码", index = 2, fieldNo = 1)
    private String proSelfCode;


    /**
     * 拼音码
     */
    private String pym;

    /**
     * 商品名称
     */
    @CsvCell(title = "商品名称", index = 3, fieldNo = 1)
    private String productName;

    /**
     * 规格
     */
    @CsvCell(title = "规格", index = 4, fieldNo = 1)
    private String productSpecs;

    /**
     * 单位
     */
    @CsvCell(title = "单位", index = 5, fieldNo = 1)
    private String productUnit;

    /**
     * 买满金额
     */
    @CsvCell(title = "买满金额", index = 6, fieldNo = 1)
    private BigDecimal buyAmt;

    /**
     * 所需积分
     */
    @CsvCell(title = "所需积分", index = 7, fieldNo = 1)
    private BigDecimal exchangeIntegra;

    /**
     * 换购金额
     */
    @CsvCell(title = "换购金额", index = 8, fieldNo = 1)
    private BigDecimal exchangeAmt;

    /**
     * 兑换数量
     */
    @CsvCell(title = "兑换数量", index = 9, fieldNo = 1)
    private BigDecimal exchangeQty;

    /**
     * 限兑数量
     */
    @CsvCell(title = "限兑数量", index = 10, fieldNo = 1)
    private BigDecimal periodMaxQty;

    /**
     * 期间限兑（购）天数
     */
    @CsvCell(title = "限兑期间天数", index = 11, fieldNo = 1)
    private BigDecimal periodDays;

    /**
     * 本商品是否积分（1-是、0-否）
     */
    @CsvCell(title = "是否积分", index = 12, fieldNo = 1)
    private String integralFlag;


}
