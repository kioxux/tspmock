package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImportInData {
    @ApiModelProperty(value = "补货门店")
    private String gsrdBrId;
    @ApiModelProperty(value = "商品编码")
    private String gsrdProId;
    @ApiModelProperty(value = "补货量")
    private String gsrdNeedQty;
    @ApiModelProperty(value = "补货供应商编码")
    private String gsrdSupid;
    @ApiModelProperty(value = "补货供应商")
    private String gsrdSupName;
    @ApiModelProperty(value = "采购订单单价")
    private String poPrice;
}
