package com.gys.business.service.data.DongruanYb;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 退货参数 接收
 * @Author huxinxin
 * @Date 2021/3/30 13:21
 * @Version 1.0.0
 **/
@Data
public class SaleReturnOutput {
    private int statusCode;
    private String zjsjljylsh;
    private BigDecimal yttcje;
    private BigDecimal ytzhje;
    private BigDecimal ytgwybz;
    private BigDecimal ytxzje;
    private BigDecimal ytdelpje;
    private BigDecimal ytlsqfxgwyfh;
    private BigDecimal ytmzjzje;
    private BigDecimal ytzgjjzfje;
    private BigDecimal ytjxxmje;
    private BigDecimal ytshjzjjzfs;
    private BigDecimal ytzfkzyl2;
    private String zxjssj;
    private BigDecimal ytsyjjje;
    private BigDecimal ytsyxjzf;
    private BigDecimal ytgsjjje;
    private BigDecimal ytgsxjje;
    private BigDecimal ytqtbz;
    private BigDecimal ytsyzhzf;
    private BigDecimal ytgszhzf;
}
