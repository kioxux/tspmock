package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdIsh;
import com.gys.business.mapper.entity.GaiaSdIshFeeDetail;
import com.gys.common.data.GetLoginOutData;

import java.util.Map;

/**
 * @Description 东软对接
 * @Author huxinxin
 * @Date 2021/3/28 14:02
 * @Version 1.0.0
 **/
public interface IshService {
    void insertIsh(Map record);

    void insertIshFeeDetail(GaiaSdIshFeeDetail record);

    GaiaSdIsh queryIsh(Map record);

    void ishReturn(GetLoginOutData userInfo, Map record);
}
