package com.gys.business.service.salesReceiptsPriceUtil;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaSdPromSeriesSet;

import java.math.BigDecimal;

@SuppressWarnings("all")
public class PromSeries {
    public static BigDecimal gsphPart1Repeat1(float numTotal, BigDecimal moneyTotal, GaiaSdPromSeriesSet gaiaSdPromSeriesSet) {
        BigDecimal gspssReachAmt1 = gaiaSdPromSeriesSet.getGspssReachAmt1();
        String gspssReachQty1 = gaiaSdPromSeriesSet.getGspssReachQty1();
        BigDecimal gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
        String gspssResultRebate1 = gaiaSdPromSeriesSet.getGspssResultRebate1();
        BigDecimal result = new BigDecimal(0);
        if (ObjectUtil.isNotEmpty(gspssReachAmt1)) {
            if (moneyTotal.compareTo(gspssReachAmt1) == -1) {
                result = moneyTotal;
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                    while(moneyTotal.compareTo(gspssReachAmt1) > -1) {
                        result = result.subtract(gspssResultAmt1);
                        moneyTotal = moneyTotal.subtract(gspssReachAmt1);
                    }
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspssReachQty1)) {
            if (numTotal < Float.parseFloat(gspssReachQty1)) {
                result = moneyTotal;
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                    while(numTotal >= Float.parseFloat(gspssReachQty1)) {//如果总数量到达 促销数量一时
                        result = result.subtract(gspssResultAmt1);//活动前金额 减去 活动金额
                        numTotal = numTotal - Float.parseFloat(gspssReachQty1);//总数量减去 促销 数量
                    }
                   //if (proNumAmtMap.get())
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                }
            }
        }

        return result;
    }

    public static BigDecimal gsphPart2Repeat1(float numTotal, BigDecimal moneyTotal, GaiaSdPromSeriesSet gaiaSdPromSeriesSet) {
        BigDecimal gspssReachAmt1 = gaiaSdPromSeriesSet.getGspssReachAmt1();
        BigDecimal gspssReachAmt2 = gaiaSdPromSeriesSet.getGspssReachAmt2();
        String gspssReachQty1 = gaiaSdPromSeriesSet.getGspssReachQty1();
        String gspssReachQty2 = gaiaSdPromSeriesSet.getGspssReachQty2();
        BigDecimal gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
        BigDecimal gspssResultAmt2 = gaiaSdPromSeriesSet.getGspssResultAmt2();
        String gspssResultRebate1 = gaiaSdPromSeriesSet.getGspssResultRebate1();
        String gspssResultRebate2 = gaiaSdPromSeriesSet.getGspssResultRebate2();
        BigDecimal result = new BigDecimal(0);
        if (ObjectUtil.isNotEmpty(gspssReachAmt2)) {
            if (moneyTotal.compareTo(gspssReachAmt2) == -1) {
                if (moneyTotal.compareTo(gspssReachAmt1) > -1) {
                    if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                        result = moneyTotal.subtract(gspssResultAmt1);
                    } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                        result = moneyTotal.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                    }
                } else {
                    result = moneyTotal;
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultAmt2)) {
                result = new BigDecimal(moneyTotal.toString());

                while(moneyTotal.compareTo(gspssReachAmt2) > -1) {
                    result = result.subtract(gspssResultAmt2);
                    moneyTotal = moneyTotal.subtract(gspssReachAmt2);
                    if (moneyTotal.compareTo(gspssReachAmt2) == -1 && moneyTotal.compareTo(gspssReachAmt1) > -1) {
                        result = result.subtract(gspssResultAmt1);
                    }
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultRebate2)) {
                result = moneyTotal.multiply(new BigDecimal(gspssResultRebate2)).divide(new BigDecimal(100));
            }
        } else if (ObjectUtil.isNotEmpty(gspssReachQty2)) {
            if (numTotal < (float)Integer.valueOf(gspssReachQty2)) {
                if (numTotal >= (float)Integer.valueOf(gspssReachQty1)) {
                    if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                        result = moneyTotal.subtract(gspssResultAmt1);
                    } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                        result = moneyTotal.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                    }
                } else {
                    result = moneyTotal;
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultAmt2)) {
                result = new BigDecimal(moneyTotal.toString());

                while(numTotal >= (float)Integer.valueOf(gspssReachQty2)) {
                    result = result.subtract(gspssResultAmt2);
                    numTotal -= (float)Integer.valueOf(gspssReachQty2);
                    if (numTotal < (float)Integer.valueOf(gspssReachQty2) && numTotal > (float)Integer.valueOf(gspssReachQty1)) {
                        result = result.subtract(gspssResultAmt1);
                    }
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultRebate2)) {
                result = moneyTotal.multiply(new BigDecimal(gspssResultRebate2)).divide(new BigDecimal(100));
            }
        }

        return result;
    }

    public static BigDecimal gsphPart3Repeat1(float numTotal, BigDecimal moneyTotal, GaiaSdPromSeriesSet gaiaSdPromSeriesSet) {
        BigDecimal gspssReachAmt1 = gaiaSdPromSeriesSet.getGspssReachAmt1();
        BigDecimal gspssReachAmt2 = gaiaSdPromSeriesSet.getGspssReachAmt2();
        BigDecimal gspssReachAmt3 = gaiaSdPromSeriesSet.getGspssReachAmt3();
        String gspssReachQty1 = gaiaSdPromSeriesSet.getGspssReachQty1();
        String gspssReachQty2 = gaiaSdPromSeriesSet.getGspssReachQty2();
        String gspssReachQty3 = gaiaSdPromSeriesSet.getGspssReachQty2();
        BigDecimal gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
        BigDecimal gspssResultAmt2 = gaiaSdPromSeriesSet.getGspssResultAmt2();
        BigDecimal gspssResultAmt3 = gaiaSdPromSeriesSet.getGspssResultAmt2();
        String gspssResultRebate1 = gaiaSdPromSeriesSet.getGspssResultRebate1();
        String gspssResultRebate2 = gaiaSdPromSeriesSet.getGspssResultRebate2();
        String gspssResultRebate3 = gaiaSdPromSeriesSet.getGspssResultRebate2();
        BigDecimal result = new BigDecimal(0);
        if (ObjectUtil.isNotEmpty(gspssReachAmt3)) {
            if (moneyTotal.compareTo(gspssReachAmt3) == -1) {
                if (moneyTotal.compareTo(gspssReachAmt2) > -1) {
                    if (ObjectUtil.isNotEmpty(gspssResultAmt2)) {
                        result = moneyTotal.subtract(gspssResultAmt2);
                    } else if (ObjectUtil.isNotEmpty(gspssResultRebate2)) {
                        result = moneyTotal.multiply(new BigDecimal(gspssResultRebate2)).divide(new BigDecimal(100));
                    }
                } else if (moneyTotal.compareTo(gspssReachAmt1) > -1) {
                    if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                        result = moneyTotal.subtract(gspssResultAmt1);
                    } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                        result = moneyTotal.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                    }
                } else {
                    result = moneyTotal;
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultAmt3)) {
                result = new BigDecimal(moneyTotal.toString());

                while(moneyTotal.compareTo(gspssReachAmt3) > -1) {
                    result = result.subtract(gspssResultAmt3);
                    moneyTotal = moneyTotal.subtract(gspssReachAmt3);
                    if (moneyTotal.compareTo(gspssReachAmt3) == -1) {
                        if (moneyTotal.compareTo(gspssReachAmt2) > -1) {
                            result = result.subtract(gspssResultAmt2);
                        } else if (moneyTotal.compareTo(gspssReachAmt1) > -1) {
                            result = result.subtract(gspssResultAmt1);
                        }
                    }
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultRebate3)) {
                result = moneyTotal.multiply(new BigDecimal(gspssResultRebate3)).divide(new BigDecimal(100));
            }
        } else if (ObjectUtil.isNotEmpty(gspssReachQty3)) {
            if (numTotal < (float)Integer.valueOf(gspssReachQty3)) {
                if (numTotal >= (float)Integer.valueOf(gspssReachQty2)) {
                    if (ObjectUtil.isNotEmpty(gspssResultAmt2)) {
                        result = moneyTotal.subtract(gspssResultAmt2);
                    } else if (ObjectUtil.isNotEmpty(gspssResultRebate2)) {
                        result = moneyTotal.multiply(new BigDecimal(gspssResultRebate2)).divide(new BigDecimal(100));
                    }
                } else if (numTotal >= (float)Integer.valueOf(gspssReachQty1)) {
                    if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                        result = moneyTotal.subtract(gspssResultAmt1);
                    } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                        result = moneyTotal.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                    }
                } else {
                    result = moneyTotal;
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultAmt3)) {
                result = new BigDecimal(moneyTotal.toString());

                while(numTotal >= (float)Integer.valueOf(gspssReachQty3)) {
                    result = result.subtract(gspssResultAmt3);
                    numTotal -= (float)Integer.valueOf(gspssReachQty3);
                    if (numTotal < (float)Integer.valueOf(gspssReachQty3)) {
                        if (numTotal >= (float)Integer.valueOf(gspssReachQty2)) {
                            result = result.subtract(gspssResultAmt2);
                        } else if (numTotal >= (float)Integer.valueOf(gspssReachQty1)) {
                            result = result.subtract(gspssResultAmt1);
                        }
                    }
                }
            } else if (ObjectUtil.isNotEmpty(gspssResultRebate3)) {
                result = moneyTotal.multiply(new BigDecimal(gspssResultRebate3)).divide(new BigDecimal(100));
            }
        }

        return result;
    }

    public static BigDecimal gsphPart1Repeat2(float numTotal, BigDecimal moneyTotal, GaiaSdPromSeriesSet gaiaSdPromSeriesSet) {
        BigDecimal gspssReachAmt1 = gaiaSdPromSeriesSet.getGspssReachAmt1();
        String gspssReachQty1 = gaiaSdPromSeriesSet.getGspssReachQty1();
        BigDecimal gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
        String gspssResultRebate1 = gaiaSdPromSeriesSet.getGspssResultRebate1();
        BigDecimal result = new BigDecimal(0);
        if (ObjectUtil.isNotEmpty(gspssReachAmt1)) {
            if (moneyTotal.compareTo(gspssReachAmt1) == -1) {
                result = moneyTotal;
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                    result = result.subtract(gspssResultAmt1);
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspssReachQty1)) {
            if (numTotal < (float)Integer.valueOf(gspssReachQty1)) {
                result = moneyTotal;
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt1)) {
                    result = result.subtract(gspssResultAmt1);
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate1)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate1)).divide(new BigDecimal(100));
                }
            }
        }

        return result;
    }

    public static BigDecimal gsphPart2Repeat2(float numTotal, BigDecimal moneyTotal, GaiaSdPromSeriesSet gaiaSdPromSeriesSet) {
        BigDecimal gspssReachAmt1 = gaiaSdPromSeriesSet.getGspssReachAmt1();
        BigDecimal gspssReachAmt2 = gaiaSdPromSeriesSet.getGspssReachAmt2();
        String gspssReachQty1 = gaiaSdPromSeriesSet.getGspssReachQty1();
        String gspssReachQty2 = gaiaSdPromSeriesSet.getGspssReachQty2();
        BigDecimal gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
        BigDecimal gspssResultAmt2 = gaiaSdPromSeriesSet.getGspssResultAmt2();
        String gspssResultRebate1 = gaiaSdPromSeriesSet.getGspssResultRebate1();
        String gspssResultRebate2 = gaiaSdPromSeriesSet.getGspssResultRebate2();
        BigDecimal result = new BigDecimal(0);
        if (ObjectUtil.isNotEmpty(gspssReachAmt2)) {
            if (moneyTotal.compareTo(gspssReachAmt2) == -1) {
                result = gsphPart1Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt2)) {
                    result = result.subtract(gspssResultAmt2);
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate2)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate2)).divide(new BigDecimal(100));
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspssReachQty2)) {
            if (numTotal < (float)Integer.valueOf(gspssReachQty2)) {
                result = gsphPart1Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt2)) {
                    result = result.subtract(gspssResultAmt2);
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate2)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate2)).divide(new BigDecimal(100));
                }
            }
        }

        return result;
    }

    public static BigDecimal gsphPart3Repeat2(float numTotal, BigDecimal moneyTotal, GaiaSdPromSeriesSet gaiaSdPromSeriesSet) {
        BigDecimal gspssReachAmt1 = gaiaSdPromSeriesSet.getGspssReachAmt1();
        BigDecimal gspssReachAmt2 = gaiaSdPromSeriesSet.getGspssReachAmt2();
        BigDecimal gspssReachAmt3 = gaiaSdPromSeriesSet.getGspssReachAmt3();
        String gspssReachQty1 = gaiaSdPromSeriesSet.getGspssReachQty1();
        String gspssReachQty2 = gaiaSdPromSeriesSet.getGspssReachQty2();
        String gspssReachQty3 = gaiaSdPromSeriesSet.getGspssReachQty3();
        BigDecimal gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
        BigDecimal gspssResultAmt2 = gaiaSdPromSeriesSet.getGspssResultAmt2();
        BigDecimal gspssResultAmt3 = gaiaSdPromSeriesSet.getGspssResultAmt3();
        String gspssResultRebate1 = gaiaSdPromSeriesSet.getGspssResultRebate1();
        String gspssResultRebate2 = gaiaSdPromSeriesSet.getGspssResultRebate2();
        String gspssResultRebate3 = gaiaSdPromSeriesSet.getGspssResultRebate3();
        BigDecimal result = new BigDecimal(0);
        if (ObjectUtil.isNotEmpty(gspssReachAmt3)) {
            if (moneyTotal.compareTo(gspssReachAmt3) == -1) {
                result = gsphPart2Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt3)) {
                    result = result.subtract(gspssResultAmt3);
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate3)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate3)).divide(new BigDecimal(100));
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspssReachQty3)) {
            if (numTotal < (float)Integer.valueOf(gspssReachQty3)) {
                result = gsphPart2Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else {
                result = new BigDecimal(moneyTotal.toString());
                if (ObjectUtil.isNotEmpty(gspssResultAmt3)) {
                    result = result.subtract(gspssResultAmt3);
                } else if (ObjectUtil.isNotEmpty(gspssResultRebate3)) {
                    result = result.multiply(new BigDecimal(gspssResultRebate3)).divide(new BigDecimal(100));
                }
            }
        }

        return result;
    }
}
