package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaUserTrainingRecord;
import com.gys.business.service.data.UserTrainingRecordInData;
import com.gys.business.service.data.UserTrainingRecordOutData;
import com.gys.common.response.Result;

import java.util.List;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/3 20:45
 */
public interface UserTrainingRecordService {
    void saveUserTrainingRecord(List<GaiaUserTrainingRecord> recordList, String client, String user);

    List<UserTrainingRecordOutData> getUserTrainingRecordList(UserTrainingRecordInData inData);

    Result exportUserTrainingRecordList(UserTrainingRecordInData inData);

    void deleteUserTrainingRecord(List<GaiaUserTrainingRecord> recordList, String client);
}
