package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc: 入参
 * @author: ZhangChi
 * @createTime: 2021/11/29 14:15
 */
@Data
public class IntegralInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     *开始生效日期
     */
    private String startEffectiveDate;

    /**
     * 结束生效日期
     */
    private String endEffectiveDate;

    /**
     * 卡类型编号：1钻石，2白金，3金，4银，5普通
     */
    private String[] cardType;

    /**
     * 状态：0-保存，1-已审核
     */
    private Integer state;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 活动门店
     */
    private String[] stoCode;

    private Integer pageNum;

    private Integer pageSize;

    /**
     * 排序 0：单号、1：开始时间、2：结束时间、1：正序、2：倒序
     */
    private String order;
}
