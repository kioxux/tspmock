//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;
import java.util.List;

public class GetShopInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private String clientId;
    private String gssgName;
    private String gssgBrName;
    private String gssgBrId;
    private String gssgId;
    private String gssgUpdateEmp;
    private List<AddShopTableOutData> shops;

    public GetShopInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssgName() {
        return this.gssgName;
    }

    public String getGssgBrName() {
        return this.gssgBrName;
    }

    public String getGssgBrId() {
        return this.gssgBrId;
    }

    public String getGssgId() {
        return this.gssgId;
    }

    public String getGssgUpdateEmp() {
        return this.gssgUpdateEmp;
    }

    public List<AddShopTableOutData> getShops() {
        return this.shops;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssgName(final String gssgName) {
        this.gssgName = gssgName;
    }

    public void setGssgBrName(final String gssgBrName) {
        this.gssgBrName = gssgBrName;
    }

    public void setGssgBrId(final String gssgBrId) {
        this.gssgBrId = gssgBrId;
    }

    public void setGssgId(final String gssgId) {
        this.gssgId = gssgId;
    }

    public void setGssgUpdateEmp(final String gssgUpdateEmp) {
        this.gssgUpdateEmp = gssgUpdateEmp;
    }

    public void setShops(final List<AddShopTableOutData> shops) {
        this.shops = shops;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetShopInData)) {
            return false;
        } else {
            GetShopInData other = (GetShopInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gssgName = this.getGssgName();
                Object other$gssgName = other.getGssgName();
                if (this$gssgName == null) {
                    if (other$gssgName != null) {
                        return false;
                    }
                } else if (!this$gssgName.equals(other$gssgName)) {
                    return false;
                }

                Object this$gssgBrName = this.getGssgBrName();
                Object other$gssgBrName = other.getGssgBrName();
                if (this$gssgBrName == null) {
                    if (other$gssgBrName != null) {
                        return false;
                    }
                } else if (!this$gssgBrName.equals(other$gssgBrName)) {
                    return false;
                }

                label74: {
                    Object this$gssgBrId = this.getGssgBrId();
                    Object other$gssgBrId = other.getGssgBrId();
                    if (this$gssgBrId == null) {
                        if (other$gssgBrId == null) {
                            break label74;
                        }
                    } else if (this$gssgBrId.equals(other$gssgBrId)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gssgId = this.getGssgId();
                    Object other$gssgId = other.getGssgId();
                    if (this$gssgId == null) {
                        if (other$gssgId == null) {
                            break label67;
                        }
                    } else if (this$gssgId.equals(other$gssgId)) {
                        break label67;
                    }

                    return false;
                }

                Object this$gssgUpdateEmp = this.getGssgUpdateEmp();
                Object other$gssgUpdateEmp = other.getGssgUpdateEmp();
                if (this$gssgUpdateEmp == null) {
                    if (other$gssgUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gssgUpdateEmp.equals(other$gssgUpdateEmp)) {
                    return false;
                }

                Object this$shops = this.getShops();
                Object other$shops = other.getShops();
                if (this$shops == null) {
                    if (other$shops != null) {
                        return false;
                    }
                } else if (!this$shops.equals(other$shops)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetShopInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssgName = this.getGssgName();
        result = result * 59 + ($gssgName == null ? 43 : $gssgName.hashCode());
        Object $gssgBrName = this.getGssgBrName();
        result = result * 59 + ($gssgBrName == null ? 43 : $gssgBrName.hashCode());
        Object $gssgBrId = this.getGssgBrId();
        result = result * 59 + ($gssgBrId == null ? 43 : $gssgBrId.hashCode());
        Object $gssgId = this.getGssgId();
        result = result * 59 + ($gssgId == null ? 43 : $gssgId.hashCode());
        Object $gssgUpdateEmp = this.getGssgUpdateEmp();
        result = result * 59 + ($gssgUpdateEmp == null ? 43 : $gssgUpdateEmp.hashCode());
        Object $shops = this.getShops();
        result = result * 59 + ($shops == null ? 43 : $shops.hashCode());
        return result;
    }

    public String toString() {
        return "GetShopInData(clientId=" + this.getClientId() + ", gssgName=" + this.getGssgName() + ", gssgBrName=" + this.getGssgBrName() + ", gssgBrId=" + this.getGssgBrId() + ", gssgId=" + this.getGssgId() + ", gssgUpdateEmp=" + this.getGssgUpdateEmp() + ", shops=" + this.getShops() + ")";
    }
}
