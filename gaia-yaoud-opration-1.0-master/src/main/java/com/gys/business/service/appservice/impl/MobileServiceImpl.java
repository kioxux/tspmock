package com.gys.business.service.appservice.impl;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.mapper.GaiaParamDetailMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.ParamDetailData;
import com.gys.business.service.appservice.MobileService;
import com.gys.business.service.data.GaiaStoreOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaoyuan on 2021/3/9
 */
@Service("mobileServiceImpl")
public class MobileServiceImpl implements MobileService {

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private GaiaParamDetailMapper gaiaParamDetailMapper;

    @Override
    public List<GaiaStoreOutData> getStoreList(GetLoginOutData userInfo) {
        List<GaiaStoreOutData> storeList = this.storeDataMapper.getAPPStoreList(userInfo.getClient(), userInfo.getUserId());
        if (CollUtil.isEmpty(storeList)) {
            throw new BusinessException("该人员没有门店权限,请先设置!");
        }
        List<ParamDetailData> detailDataList = this.gaiaParamDetailMapper.getAllByClient(userInfo.getClient());
        if (CollUtil.isNotEmpty(detailDataList)) {
            storeList.forEach(outData -> detailDataList.stream()
                    .filter(detailData -> outData.getStoCode().equals(detailData.getGsspBrId()) && "RECISALE_SET".equals(detailData.getGsspId()))
                    .forEach(detailData -> outData.setFlag(detailData.getGsspPara())));
        }
        return storeList;
    }
}
