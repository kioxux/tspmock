package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdSaleD;
import com.gys.business.mapper.entity.GaiaSdSaleH;
import lombok.Data;

import java.util.List;

@Data
public class GetSaleBillInfoData {
    private GaiaSdSaleH gaiaSdSaleH;
    private List<GaiaSdSaleD> gaiaSdSaleDList;
    private GetQueryMemberOutData getQueryMemberOutData;
}
