package com.gys.business.service.data.DelegationReturn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "委托退货列表")
public class RetrunApprovalInData {
    @ApiModelProperty(value = "门店")
    private String stoCode;
    @ApiModelProperty(value = "店名")
    private String stoName;
    @ApiModelProperty(value = "配送中心")
    private String stoDcCode;
    @ApiModelProperty(value = "商品编码")
    private String proId;
    @ApiModelProperty(value = "商品名称")
    private String proCommonname;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "厂家")
    private String factoryName;
    @ApiModelProperty(value = "单位")
    private String proUnit;
    @ApiModelProperty(value = "进项税率")
    private String inputTax;
    @ApiModelProperty(value = "退货数量")
    private BigDecimal retdepQty;
    @ApiModelProperty(value = "修正数量")
    private BigDecimal reviseQty;
    @ApiModelProperty(value = "退货申请单")
    private String voucherId;
    @ApiModelProperty(value = "行号")
    private String serial;
    @ApiModelProperty(value = "退货批号")
    private String batchNO;
    @ApiModelProperty(value = "退货批次")
    private String batch;
    @ApiModelProperty(value = "退货供应商编码")
    private String supplierCode;
    @ApiModelProperty(value = "退货供应商名称")
    private String supplierName;
    @ApiModelProperty(value = "采购进价")
    private BigDecimal poPrice;
    @ApiModelProperty(value = "有效期")
    private String expiryDate;
    @ApiModelProperty(value = "退货原因")
    private String retdepCause;
    @ApiModelProperty(value = "门店库存")
    private String qty;
    @ApiModelProperty(value = "审核意见")
    private String status;
    @ApiModelProperty(value = "付款条款")
    private String paymentId;
    @ApiModelProperty(value = "退库人员")
    private String emp;
    @ApiModelProperty(value = "备注")
    private String remaks;
}
