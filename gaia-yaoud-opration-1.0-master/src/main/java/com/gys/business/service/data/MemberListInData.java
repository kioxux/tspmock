package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MemberListInData implements Serializable {

    private static final long serialVersionUID = 1629464568751381941L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "门店号")
    private String brId;

    @ApiModelProperty(value = "门店编号列表")
    private List<String> brIdList;

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty(value = "会员卡号")
    private String cardId;

    @ApiModelProperty(value = "会员名称")
    private String memberName;

    @ApiModelProperty(value = "手机号")
    private String mobileOrTel;

    @ApiModelProperty(value = "起始日期")
    private String startDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "起始")
    private int pageNum;

    @ApiModelProperty(value = "总共")
    private int pageSize;
}
