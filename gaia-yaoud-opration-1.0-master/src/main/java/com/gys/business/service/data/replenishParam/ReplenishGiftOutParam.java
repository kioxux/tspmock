package com.gys.business.service.data.replenishParam;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReplenishGiftOutParam {
    private String clientId;//加盟商
    private String siteCode;//地点（门店配送中心）
    private String proCode;//商品编码
    private String giftCode;//赠品编码
    private BigDecimal proQty;//商品数量
    private BigDecimal giftQty;//赠品数量
    private String deleteFlag;//是否删除 1否，2是
    private String proName;
    private String proSpecs;
    private String needQty;
}
