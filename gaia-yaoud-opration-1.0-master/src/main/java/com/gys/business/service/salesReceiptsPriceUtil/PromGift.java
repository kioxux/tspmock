//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.salesReceiptsPriceUtil;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaSdPromGiftSet;
import java.math.BigDecimal;

public class PromGift {
    public PromGift() {
    }

    public static int gsphPart1Repeat1(float numTotal, BigDecimal moneyTotal, GaiaSdPromGiftSet gaiaSdPromGiftSet) {
        int count = 0;
        BigDecimal gspgsReachAmt1 = gaiaSdPromGiftSet.getGspgsReachAmt1();
        String gspgsReachQty1 = gaiaSdPromGiftSet.getGspgsReachQty1();
        String gspgsResultQty1 = gaiaSdPromGiftSet.getGspgsResultQty1();
        if (ObjectUtil.isNotEmpty(gspgsReachAmt1)) {
            if (moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                while(moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                    count += Integer.valueOf(gspgsResultQty1);
                    moneyTotal = moneyTotal.subtract(gspgsReachAmt1);
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspgsReachQty1) && numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
            while(numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
                count += Integer.valueOf(gspgsResultQty1);
                numTotal -= (float)Integer.valueOf(gspgsReachQty1);
            }
        }

        return count;
    }

    public static int gsphPart2Repeat1(float numTotal, BigDecimal moneyTotal, GaiaSdPromGiftSet gaiaSdPromGiftSet) {
        int count = 0;
        BigDecimal gspgsReachAmt1 = gaiaSdPromGiftSet.getGspgsReachAmt1();
        BigDecimal gspgsReachAmt2 = gaiaSdPromGiftSet.getGspgsReachAmt2();
        String gspgsReachQty1 = gaiaSdPromGiftSet.getGspgsReachQty1();
        String gspgsReachQty2 = gaiaSdPromGiftSet.getGspgsReachQty2();
        String gspgsResultQty1 = gaiaSdPromGiftSet.getGspgsResultQty1();
        String gspgsResultQty2 = gaiaSdPromGiftSet.getGspgsResultQty1();
        if (ObjectUtil.isNotEmpty(gspgsReachAmt2)) {
            if (moneyTotal.compareTo(gspgsReachAmt2) > -1) {
                while(moneyTotal.compareTo(gspgsReachAmt2) > -1) {
                    count += Integer.valueOf(gspgsResultQty2);
                    moneyTotal = moneyTotal.subtract(gspgsReachAmt2);
                    if (moneyTotal.compareTo(gspgsReachAmt2) == -1 && moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                        count += Integer.valueOf(gspgsResultQty1);
                    }
                }
            } else if (moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                count += Integer.valueOf(gspgsResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspgsReachQty2)) {
            if (numTotal >= (float)Integer.valueOf(gspgsReachQty2)) {
                while(numTotal >= (float)Integer.valueOf(gspgsReachQty2)) {
                    count += Integer.valueOf(gspgsResultQty2);
                    numTotal -= (float)Integer.valueOf(gspgsReachQty2);
                    if (numTotal < (float)Integer.valueOf(gspgsReachQty2) && numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
                        count += Integer.valueOf(gspgsResultQty1);
                    }
                }
            } else if (numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
                count += Integer.valueOf(gspgsResultQty1);
            }
        }

        return count;
    }

    public static int gsphPart3Repeat1(float numTotal, BigDecimal moneyTotal, GaiaSdPromGiftSet gaiaSdPromGiftSet) {
        int count = 0;
        BigDecimal gspgsReachAmt1 = gaiaSdPromGiftSet.getGspgsReachAmt1();
        BigDecimal gspgsReachAmt2 = gaiaSdPromGiftSet.getGspgsReachAmt2();
        BigDecimal gspgsReachAmt3 = gaiaSdPromGiftSet.getGspgsReachAmt3();
        String gspgsReachQty1 = gaiaSdPromGiftSet.getGspgsReachQty1();
        String gspgsReachQty2 = gaiaSdPromGiftSet.getGspgsReachQty2();
        String gspgsReachQty3 = gaiaSdPromGiftSet.getGspgsReachQty2();
        String gspgsResultQty1 = gaiaSdPromGiftSet.getGspgsResultQty1();
        String gspgsResultQty2 = gaiaSdPromGiftSet.getGspgsResultQty1();
        String gspgsResultQty3 = gaiaSdPromGiftSet.getGspgsResultQty1();
        if (ObjectUtil.isNotEmpty(gspgsReachAmt3)) {
            if (moneyTotal.compareTo(gspgsReachAmt3) > -1) {
                while(moneyTotal.compareTo(gspgsReachAmt3) > -1) {
                    count += Integer.valueOf(gspgsResultQty3);
                    moneyTotal = moneyTotal.subtract(gspgsReachAmt3);
                    if (moneyTotal.compareTo(gspgsReachAmt3) == -1) {
                        if (moneyTotal.compareTo(gspgsReachAmt2) > -1) {
                            count += Integer.valueOf(gspgsResultQty2);
                        } else if (moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                            count += Integer.valueOf(gspgsResultQty1);
                        }
                    }
                }
            } else if (ObjectUtil.isNotEmpty(gspgsReachAmt2)) {
                if (moneyTotal.compareTo(gspgsReachAmt2) > -1) {
                    count += Integer.valueOf(gspgsResultQty2);
                } else if (moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                    count += Integer.valueOf(gspgsResultQty1);
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspgsReachQty3)) {
            if (numTotal >= (float)Integer.valueOf(gspgsReachQty3)) {
                while(numTotal >= (float)Integer.valueOf(gspgsReachQty3)) {
                    count += Integer.valueOf(gspgsResultQty3);
                    numTotal -= (float)Integer.valueOf(gspgsReachQty3);
                    if (numTotal < (float)Integer.valueOf(gspgsReachQty3)) {
                        if (numTotal >= (float)Integer.valueOf(gspgsReachQty2)) {
                            count += Integer.valueOf(gspgsResultQty2);
                        } else if (numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
                            count += Integer.valueOf(gspgsResultQty1);
                        }
                    }
                }
            } else if (numTotal >= (float)Integer.valueOf(gspgsReachQty2)) {
                count += Integer.valueOf(gspgsResultQty2);
            } else if (numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
                count += Integer.valueOf(gspgsResultQty1);
            }
        }

        return count;
    }

    public static int gsphPart1Repeat2(float numTotal, BigDecimal moneyTotal, GaiaSdPromGiftSet gaiaSdPromGiftSet) {
        int count = 0;
        BigDecimal gspgsReachAmt1 = gaiaSdPromGiftSet.getGspgsReachAmt1();
        String gspgsReachQty1 = gaiaSdPromGiftSet.getGspgsReachQty1();
        String gspgsResultQty1 = gaiaSdPromGiftSet.getGspgsResultQty1();
        if (ObjectUtil.isNotEmpty(gspgsReachAmt1)) {
            if (moneyTotal.compareTo(gspgsReachAmt1) > -1) {
                count += Integer.valueOf(gspgsResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspgsReachQty1) && numTotal >= (float)Integer.valueOf(gspgsReachQty1)) {
            count += Integer.valueOf(gspgsResultQty1);
        }

        return count;
    }

    public static int gsphPart2Repeat2(float numTotal, BigDecimal moneyTotal, GaiaSdPromGiftSet gaiaSdPromGiftSet) {
        int count = 0;
        BigDecimal gspgsReachAmt1 = gaiaSdPromGiftSet.getGspgsReachAmt1();
        BigDecimal gspgsReachAmt2 = gaiaSdPromGiftSet.getGspgsReachAmt2();
        String gspgsReachQty1 = gaiaSdPromGiftSet.getGspgsReachQty1();
        String gspgsReachQty2 = gaiaSdPromGiftSet.getGspgsReachQty2();
        String gspgsResultQty1 = gaiaSdPromGiftSet.getGspgsResultQty1();
        String gspgsResultQty2 = gaiaSdPromGiftSet.getGspgsResultQty1();
        if (ObjectUtil.isNotEmpty(gspgsReachAmt2)) {
            if (moneyTotal.compareTo(gspgsReachAmt2) > -1) {
                count += Integer.valueOf(gspgsResultQty2);
            } else {
                count = gsphPart1Repeat2(numTotal, moneyTotal, gaiaSdPromGiftSet);
            }
        } else if (ObjectUtil.isNotEmpty(gspgsReachQty2)) {
            if (numTotal >= (float)Integer.valueOf(gspgsReachQty2)) {
                count += Integer.valueOf(gspgsResultQty2);
            } else {
                count = gsphPart1Repeat2(numTotal, moneyTotal, gaiaSdPromGiftSet);
            }
        }

        return count;
    }

    public static int gsphPart3Repeat2(float numTotal, BigDecimal moneyTotal, GaiaSdPromGiftSet gaiaSdPromGiftSet) {
        int count = 0;
        BigDecimal gspgsReachAmt1 = gaiaSdPromGiftSet.getGspgsReachAmt1();
        BigDecimal gspgsReachAmt2 = gaiaSdPromGiftSet.getGspgsReachAmt2();
        BigDecimal gspgsReachAmt3 = gaiaSdPromGiftSet.getGspgsReachAmt3();
        String gspgsReachQty1 = gaiaSdPromGiftSet.getGspgsReachQty1();
        String gspgsReachQty2 = gaiaSdPromGiftSet.getGspgsReachQty2();
        String gspgsReachQty3 = gaiaSdPromGiftSet.getGspgsReachQty2();
        String gspgsResultQty1 = gaiaSdPromGiftSet.getGspgsResultQty1();
        String gspgsResultQty2 = gaiaSdPromGiftSet.getGspgsResultQty1();
        String gspgsResultQty3 = gaiaSdPromGiftSet.getGspgsResultQty1();
        if (ObjectUtil.isNotEmpty(gspgsReachAmt3)) {
            if (moneyTotal.compareTo(gspgsReachAmt3) > -1) {
                count += Integer.valueOf(gspgsResultQty3);
            } else {
                count = gsphPart2Repeat2(numTotal, moneyTotal, gaiaSdPromGiftSet);
            }
        } else if (ObjectUtil.isNotEmpty(gspgsReachQty3)) {
            if (numTotal >= (float)Integer.valueOf(gspgsReachQty3)) {
                count += Integer.valueOf(gspgsResultQty3);
            } else {
                count = gsphPart2Repeat2(numTotal, moneyTotal, gaiaSdPromGiftSet);
            }
        }

        return count;
    }
}
