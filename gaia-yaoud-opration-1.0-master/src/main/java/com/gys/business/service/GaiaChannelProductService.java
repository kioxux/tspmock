package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

public interface GaiaChannelProductService {


    List<HashMap<String,Object>> listChannelDefault(GaiaChannelProductInData inData);

    boolean insertGaiaChannelData(GetLoginOutData loginUser, GaiaChannelData inData);

    List<GaiaChannelDataOutData> listGaiaChannelData(GetLoginOutData loginUser, GaiaChannelData inData);

    Object deleteGaiaChannelData(GetLoginOutData loginUser, GaiaChannelData inData);

    List<GaiaChannelProductOutData> listWaiteInsertData(GetLoginOutData loginUser, GaiaChannelProductInData inData);

    Object insertBatchChannelProduct(GetLoginOutData loginUser, GaiaChannelProductInData inData);

    JsonResult importData(MultipartFile file, GetLoginOutData userInfo);

	JsonResult batchImportUpdate(MultipartFile file, GetLoginOutData userInfo);

    List<GaiaChannelProductOutData> listChannelProduct(GetLoginOutData loginUser, GaiaChannelProductInData inData);

    Boolean updateChannelProduct(GetLoginOutData loginUser, GaiaChannelProduct inData);

    Object deleteChannelProduct(GetLoginOutData loginUser, GaiaChannelProduct inData);

    Object updateBatchChannelProduct(GetLoginOutData loginUser, List<GaiaChannelProduct> list);

    Object deleteBatchChannelProduct(GetLoginOutData loginUser, List<GaiaChannelProduct> list);

    List<HashMap<String,Object>> listStores(GetLoginOutData loginUser, GaiaChannelProduct inData);

    List<GaiaChannelProductOutData> listGoods(GetLoginOutData loginUser, GaiaChannelProduct inData);
}
