//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class GetDeviceCheckInData {
    @ApiModelProperty(hidden = true)
    private String clientId;
    @ApiModelProperty(hidden = true)
    private String gsdcBrId;
    @ApiModelProperty(value = "编号", example = "")
    private String gsdcVoucherId;
    @ApiModelProperty(value = "审核状态：1-审核 0-未审核", example = "")
    private String gsdcStatus;
    @ApiModelProperty(hidden = true)
    private String gsdcUpdateDate;
    @ApiModelProperty(hidden = true)
    private String gsdcBrName;
    @ApiModelProperty(value = "设备编号")
    private String gsdcDeviceId;
    @ApiModelProperty(value = "设备名称", example = "")
    private String gsdcDeviceName;
    @ApiModelProperty(value = "设备型号", example = "")
    private String gsdcDeviceModel;
    @ApiModelProperty(value = "维护内容", example = "")
    private String gsdcCheckRemarks;
    @ApiModelProperty(value = "维护情况", example = "")
    private String gsdcCheckResult;
    @ApiModelProperty(value = "维护措施", example = "")
    private String gsdcCheckStep;
    @ApiModelProperty(hidden = true)
    private String gsdcUpdateEmp;
    @ApiModelProperty(hidden = true)
    private String gsdcUpdateTime;
    @ApiModelProperty(hidden = true)
    private List<String> gsdcVoucherIds;
    @ApiModelProperty(hidden = true)
    private Integer pageNum;
    @ApiModelProperty(hidden = true)
    private Integer pageSize;

    public GetDeviceCheckInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsdcBrId() {
        return this.gsdcBrId;
    }

    public String getGsdcVoucherId() {
        return this.gsdcVoucherId;
    }

    public String getGsdcStatus() {
        return this.gsdcStatus;
    }

    public String getGsdcUpdateDate() {
        return this.gsdcUpdateDate;
    }

    public String getGsdcBrName() {
        return this.gsdcBrName;
    }

    public String getGsdcDeviceId() {
        return this.gsdcDeviceId;
    }

    public String getGsdcDeviceName() {
        return this.gsdcDeviceName;
    }

    public String getGsdcDeviceModel() {
        return this.gsdcDeviceModel;
    }

    public String getGsdcCheckRemarks() {
        return this.gsdcCheckRemarks;
    }

    public String getGsdcCheckResult() {
        return this.gsdcCheckResult;
    }

    public String getGsdcCheckStep() {
        return this.gsdcCheckStep;
    }

    public String getGsdcUpdateEmp() {
        return this.gsdcUpdateEmp;
    }

    public String getGsdcUpdateTime() {
        return this.gsdcUpdateTime;
    }

    public List<String> getGsdcVoucherIds() {
        return this.gsdcVoucherIds;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsdcBrId(final String gsdcBrId) {
        this.gsdcBrId = gsdcBrId;
    }

    public void setGsdcVoucherId(final String gsdcVoucherId) {
        this.gsdcVoucherId = gsdcVoucherId;
    }

    public void setGsdcStatus(final String gsdcStatus) {
        this.gsdcStatus = gsdcStatus;
    }

    public void setGsdcUpdateDate(final String gsdcUpdateDate) {
        this.gsdcUpdateDate = gsdcUpdateDate;
    }

    public void setGsdcBrName(final String gsdcBrName) {
        this.gsdcBrName = gsdcBrName;
    }

    public void setGsdcDeviceId(final String gsdcDeviceId) {
        this.gsdcDeviceId = gsdcDeviceId;
    }

    public void setGsdcDeviceName(final String gsdcDeviceName) {
        this.gsdcDeviceName = gsdcDeviceName;
    }

    public void setGsdcDeviceModel(final String gsdcDeviceModel) {
        this.gsdcDeviceModel = gsdcDeviceModel;
    }

    public void setGsdcCheckRemarks(final String gsdcCheckRemarks) {
        this.gsdcCheckRemarks = gsdcCheckRemarks;
    }

    public void setGsdcCheckResult(final String gsdcCheckResult) {
        this.gsdcCheckResult = gsdcCheckResult;
    }

    public void setGsdcCheckStep(final String gsdcCheckStep) {
        this.gsdcCheckStep = gsdcCheckStep;
    }

    public void setGsdcUpdateEmp(final String gsdcUpdateEmp) {
        this.gsdcUpdateEmp = gsdcUpdateEmp;
    }

    public void setGsdcUpdateTime(final String gsdcUpdateTime) {
        this.gsdcUpdateTime = gsdcUpdateTime;
    }

    public void setGsdcVoucherIds(final List<String> gsdcVoucherIds) {
        this.gsdcVoucherIds = gsdcVoucherIds;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetDeviceCheckInData)) {
            return false;
        } else {
            GetDeviceCheckInData other = (GetDeviceCheckInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label215: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label215;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label215;
                    }

                    return false;
                }

                Object this$gsdcBrId = this.getGsdcBrId();
                Object other$gsdcBrId = other.getGsdcBrId();
                if (this$gsdcBrId == null) {
                    if (other$gsdcBrId != null) {
                        return false;
                    }
                } else if (!this$gsdcBrId.equals(other$gsdcBrId)) {
                    return false;
                }

                label201: {
                    Object this$gsdcVoucherId = this.getGsdcVoucherId();
                    Object other$gsdcVoucherId = other.getGsdcVoucherId();
                    if (this$gsdcVoucherId == null) {
                        if (other$gsdcVoucherId == null) {
                            break label201;
                        }
                    } else if (this$gsdcVoucherId.equals(other$gsdcVoucherId)) {
                        break label201;
                    }

                    return false;
                }

                Object this$gsdcStatus = this.getGsdcStatus();
                Object other$gsdcStatus = other.getGsdcStatus();
                if (this$gsdcStatus == null) {
                    if (other$gsdcStatus != null) {
                        return false;
                    }
                } else if (!this$gsdcStatus.equals(other$gsdcStatus)) {
                    return false;
                }

                label187: {
                    Object this$gsdcUpdateDate = this.getGsdcUpdateDate();
                    Object other$gsdcUpdateDate = other.getGsdcUpdateDate();
                    if (this$gsdcUpdateDate == null) {
                        if (other$gsdcUpdateDate == null) {
                            break label187;
                        }
                    } else if (this$gsdcUpdateDate.equals(other$gsdcUpdateDate)) {
                        break label187;
                    }

                    return false;
                }

                Object this$gsdcBrName = this.getGsdcBrName();
                Object other$gsdcBrName = other.getGsdcBrName();
                if (this$gsdcBrName == null) {
                    if (other$gsdcBrName != null) {
                        return false;
                    }
                } else if (!this$gsdcBrName.equals(other$gsdcBrName)) {
                    return false;
                }

                label173: {
                    Object this$gsdcDeviceId = this.getGsdcDeviceId();
                    Object other$gsdcDeviceId = other.getGsdcDeviceId();
                    if (this$gsdcDeviceId == null) {
                        if (other$gsdcDeviceId == null) {
                            break label173;
                        }
                    } else if (this$gsdcDeviceId.equals(other$gsdcDeviceId)) {
                        break label173;
                    }

                    return false;
                }

                label166: {
                    Object this$gsdcDeviceName = this.getGsdcDeviceName();
                    Object other$gsdcDeviceName = other.getGsdcDeviceName();
                    if (this$gsdcDeviceName == null) {
                        if (other$gsdcDeviceName == null) {
                            break label166;
                        }
                    } else if (this$gsdcDeviceName.equals(other$gsdcDeviceName)) {
                        break label166;
                    }

                    return false;
                }

                Object this$gsdcDeviceModel = this.getGsdcDeviceModel();
                Object other$gsdcDeviceModel = other.getGsdcDeviceModel();
                if (this$gsdcDeviceModel == null) {
                    if (other$gsdcDeviceModel != null) {
                        return false;
                    }
                } else if (!this$gsdcDeviceModel.equals(other$gsdcDeviceModel)) {
                    return false;
                }

                label152: {
                    Object this$gsdcCheckRemarks = this.getGsdcCheckRemarks();
                    Object other$gsdcCheckRemarks = other.getGsdcCheckRemarks();
                    if (this$gsdcCheckRemarks == null) {
                        if (other$gsdcCheckRemarks == null) {
                            break label152;
                        }
                    } else if (this$gsdcCheckRemarks.equals(other$gsdcCheckRemarks)) {
                        break label152;
                    }

                    return false;
                }

                label145: {
                    Object this$gsdcCheckResult = this.getGsdcCheckResult();
                    Object other$gsdcCheckResult = other.getGsdcCheckResult();
                    if (this$gsdcCheckResult == null) {
                        if (other$gsdcCheckResult == null) {
                            break label145;
                        }
                    } else if (this$gsdcCheckResult.equals(other$gsdcCheckResult)) {
                        break label145;
                    }

                    return false;
                }

                Object this$gsdcCheckStep = this.getGsdcCheckStep();
                Object other$gsdcCheckStep = other.getGsdcCheckStep();
                if (this$gsdcCheckStep == null) {
                    if (other$gsdcCheckStep != null) {
                        return false;
                    }
                } else if (!this$gsdcCheckStep.equals(other$gsdcCheckStep)) {
                    return false;
                }

                Object this$gsdcUpdateEmp = this.getGsdcUpdateEmp();
                Object other$gsdcUpdateEmp = other.getGsdcUpdateEmp();
                if (this$gsdcUpdateEmp == null) {
                    if (other$gsdcUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gsdcUpdateEmp.equals(other$gsdcUpdateEmp)) {
                    return false;
                }

                label124: {
                    Object this$gsdcUpdateTime = this.getGsdcUpdateTime();
                    Object other$gsdcUpdateTime = other.getGsdcUpdateTime();
                    if (this$gsdcUpdateTime == null) {
                        if (other$gsdcUpdateTime == null) {
                            break label124;
                        }
                    } else if (this$gsdcUpdateTime.equals(other$gsdcUpdateTime)) {
                        break label124;
                    }

                    return false;
                }

                Object this$gsdcVoucherIds = this.getGsdcVoucherIds();
                Object other$gsdcVoucherIds = other.getGsdcVoucherIds();
                if (this$gsdcVoucherIds == null) {
                    if (other$gsdcVoucherIds != null) {
                        return false;
                    }
                } else if (!this$gsdcVoucherIds.equals(other$gsdcVoucherIds)) {
                    return false;
                }

                Object this$pageNum = this.getPageNum();
                Object other$pageNum = other.getPageNum();
                if (this$pageNum == null) {
                    if (other$pageNum != null) {
                        return false;
                    }
                } else if (!this$pageNum.equals(other$pageNum)) {
                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetDeviceCheckInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsdcBrId = this.getGsdcBrId();
        result = result * 59 + ($gsdcBrId == null ? 43 : $gsdcBrId.hashCode());
        Object $gsdcVoucherId = this.getGsdcVoucherId();
        result = result * 59 + ($gsdcVoucherId == null ? 43 : $gsdcVoucherId.hashCode());
        Object $gsdcStatus = this.getGsdcStatus();
        result = result * 59 + ($gsdcStatus == null ? 43 : $gsdcStatus.hashCode());
        Object $gsdcUpdateDate = this.getGsdcUpdateDate();
        result = result * 59 + ($gsdcUpdateDate == null ? 43 : $gsdcUpdateDate.hashCode());
        Object $gsdcBrName = this.getGsdcBrName();
        result = result * 59 + ($gsdcBrName == null ? 43 : $gsdcBrName.hashCode());
        Object $gsdcDeviceId = this.getGsdcDeviceId();
        result = result * 59 + ($gsdcDeviceId == null ? 43 : $gsdcDeviceId.hashCode());
        Object $gsdcDeviceName = this.getGsdcDeviceName();
        result = result * 59 + ($gsdcDeviceName == null ? 43 : $gsdcDeviceName.hashCode());
        Object $gsdcDeviceModel = this.getGsdcDeviceModel();
        result = result * 59 + ($gsdcDeviceModel == null ? 43 : $gsdcDeviceModel.hashCode());
        Object $gsdcCheckRemarks = this.getGsdcCheckRemarks();
        result = result * 59 + ($gsdcCheckRemarks == null ? 43 : $gsdcCheckRemarks.hashCode());
        Object $gsdcCheckResult = this.getGsdcCheckResult();
        result = result * 59 + ($gsdcCheckResult == null ? 43 : $gsdcCheckResult.hashCode());
        Object $gsdcCheckStep = this.getGsdcCheckStep();
        result = result * 59 + ($gsdcCheckStep == null ? 43 : $gsdcCheckStep.hashCode());
        Object $gsdcUpdateEmp = this.getGsdcUpdateEmp();
        result = result * 59 + ($gsdcUpdateEmp == null ? 43 : $gsdcUpdateEmp.hashCode());
        Object $gsdcUpdateTime = this.getGsdcUpdateTime();
        result = result * 59 + ($gsdcUpdateTime == null ? 43 : $gsdcUpdateTime.hashCode());
        Object $gsdcVoucherIds = this.getGsdcVoucherIds();
        result = result * 59 + ($gsdcVoucherIds == null ? 43 : $gsdcVoucherIds.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        return result;
    }

    public String toString() {
        return "GetDeviceCheckInData(clientId=" + this.getClientId() + ", gsdcBrId=" + this.getGsdcBrId() + ", gsdcVoucherId=" + this.getGsdcVoucherId() + ", gsdcStatus=" + this.getGsdcStatus() + ", gsdcUpdateDate=" + this.getGsdcUpdateDate() + ", gsdcBrName=" + this.getGsdcBrName() + ", gsdcDeviceId=" + this.getGsdcDeviceId() + ", gsdcDeviceName=" + this.getGsdcDeviceName() + ", gsdcDeviceModel=" + this.getGsdcDeviceModel() + ", gsdcCheckRemarks=" + this.getGsdcCheckRemarks() + ", gsdcCheckResult=" + this.getGsdcCheckResult() + ", gsdcCheckStep=" + this.getGsdcCheckStep() + ", gsdcUpdateEmp=" + this.getGsdcUpdateEmp() + ", gsdcUpdateTime=" + this.getGsdcUpdateTime() + ", gsdcVoucherIds=" + this.getGsdcVoucherIds() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
