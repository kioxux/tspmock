//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetCardLoseOrChangeOutData implements Serializable {
    private String date;
    private String shop;
    private String cardName;
    private String index;
    private String oldCardId;
    private String newCardId;
    private String sex;
    private String voucherId;
    private String time;
    private String user;
    private String status;

    public GetCardLoseOrChangeOutData() {
    }

    public String getDate() {
        return this.date;
    }

    public String getShop() {
        return this.shop;
    }

    public String getCardName() {
        return this.cardName;
    }

    public String getIndex() {
        return this.index;
    }

    public String getOldCardId() {
        return this.oldCardId;
    }

    public String getNewCardId() {
        return this.newCardId;
    }

    public String getSex() {
        return this.sex;
    }

    public String getVoucherId() {
        return this.voucherId;
    }

    public String getTime() {
        return this.time;
    }

    public String getUser() {
        return this.user;
    }

    public String getStatus() {
        return this.status;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public void setShop(final String shop) {
        this.shop = shop;
    }

    public void setCardName(final String cardName) {
        this.cardName = cardName;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    public void setOldCardId(final String oldCardId) {
        this.oldCardId = oldCardId;
    }

    public void setNewCardId(final String newCardId) {
        this.newCardId = newCardId;
    }

    public void setSex(final String sex) {
        this.sex = sex;
    }

    public void setVoucherId(final String voucherId) {
        this.voucherId = voucherId;
    }

    public void setTime(final String time) {
        this.time = time;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetCardLoseOrChangeOutData)) {
            return false;
        } else {
            GetCardLoseOrChangeOutData other = (GetCardLoseOrChangeOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label143: {
                    Object this$date = this.getDate();
                    Object other$date = other.getDate();
                    if (this$date == null) {
                        if (other$date == null) {
                            break label143;
                        }
                    } else if (this$date.equals(other$date)) {
                        break label143;
                    }

                    return false;
                }

                Object this$shop = this.getShop();
                Object other$shop = other.getShop();
                if (this$shop == null) {
                    if (other$shop != null) {
                        return false;
                    }
                } else if (!this$shop.equals(other$shop)) {
                    return false;
                }

                Object this$cardName = this.getCardName();
                Object other$cardName = other.getCardName();
                if (this$cardName == null) {
                    if (other$cardName != null) {
                        return false;
                    }
                } else if (!this$cardName.equals(other$cardName)) {
                    return false;
                }

                label122: {
                    Object this$index = this.getIndex();
                    Object other$index = other.getIndex();
                    if (this$index == null) {
                        if (other$index == null) {
                            break label122;
                        }
                    } else if (this$index.equals(other$index)) {
                        break label122;
                    }

                    return false;
                }

                label115: {
                    Object this$oldCardId = this.getOldCardId();
                    Object other$oldCardId = other.getOldCardId();
                    if (this$oldCardId == null) {
                        if (other$oldCardId == null) {
                            break label115;
                        }
                    } else if (this$oldCardId.equals(other$oldCardId)) {
                        break label115;
                    }

                    return false;
                }

                Object this$newCardId = this.getNewCardId();
                Object other$newCardId = other.getNewCardId();
                if (this$newCardId == null) {
                    if (other$newCardId != null) {
                        return false;
                    }
                } else if (!this$newCardId.equals(other$newCardId)) {
                    return false;
                }

                Object this$sex = this.getSex();
                Object other$sex = other.getSex();
                if (this$sex == null) {
                    if (other$sex != null) {
                        return false;
                    }
                } else if (!this$sex.equals(other$sex)) {
                    return false;
                }

                label94: {
                    Object this$voucherId = this.getVoucherId();
                    Object other$voucherId = other.getVoucherId();
                    if (this$voucherId == null) {
                        if (other$voucherId == null) {
                            break label94;
                        }
                    } else if (this$voucherId.equals(other$voucherId)) {
                        break label94;
                    }

                    return false;
                }

                label87: {
                    Object this$time = this.getTime();
                    Object other$time = other.getTime();
                    if (this$time == null) {
                        if (other$time == null) {
                            break label87;
                        }
                    } else if (this$time.equals(other$time)) {
                        break label87;
                    }

                    return false;
                }

                Object this$user = this.getUser();
                Object other$user = other.getUser();
                if (this$user == null) {
                    if (other$user != null) {
                        return false;
                    }
                } else if (!this$user.equals(other$user)) {
                    return false;
                }

                Object this$status = this.getStatus();
                Object other$status = other.getStatus();
                if (this$status == null) {
                    if (other$status != null) {
                        return false;
                    }
                } else if (!this$status.equals(other$status)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetCardLoseOrChangeOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $date = this.getDate();
        result = result * 59 + ($date == null ? 43 : $date.hashCode());
        Object $shop = this.getShop();
        result = result * 59 + ($shop == null ? 43 : $shop.hashCode());
        Object $cardName = this.getCardName();
        result = result * 59 + ($cardName == null ? 43 : $cardName.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $oldCardId = this.getOldCardId();
        result = result * 59 + ($oldCardId == null ? 43 : $oldCardId.hashCode());
        Object $newCardId = this.getNewCardId();
        result = result * 59 + ($newCardId == null ? 43 : $newCardId.hashCode());
        Object $sex = this.getSex();
        result = result * 59 + ($sex == null ? 43 : $sex.hashCode());
        Object $voucherId = this.getVoucherId();
        result = result * 59 + ($voucherId == null ? 43 : $voucherId.hashCode());
        Object $time = this.getTime();
        result = result * 59 + ($time == null ? 43 : $time.hashCode());
        Object $user = this.getUser();
        result = result * 59 + ($user == null ? 43 : $user.hashCode());
        Object $status = this.getStatus();
        result = result * 59 + ($status == null ? 43 : $status.hashCode());
        return result;
    }

    public String toString() {
        return "GetCardLoseOrChangeOutData(date=" + this.getDate() + ", shop=" + this.getShop() + ", cardName=" + this.getCardName() + ", index=" + this.getIndex() + ", oldCardId=" + this.getOldCardId() + ", newCardId=" + this.getNewCardId() + ", sex=" + this.getSex() + ", voucherId=" + this.getVoucherId() + ", time=" + this.getTime() + ", user=" + this.getUser() + ", status=" + this.getStatus() + ")";
    }
}
