package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaGroupDataMapper;
import com.gys.business.mapper.GaiaSdIncomeStatementDMapper;
import com.gys.business.service.ProfitAndLossRecordOutDataObj;
import com.gys.business.service.ProfitAndLossRecordOutDataTotal;
import com.gys.business.service.ProfitAndLossRecordService;
import com.gys.business.service.data.ProfitAndLossRecordInData;
import com.gys.business.service.data.ProfitAndLossRecordOutData;
import com.gys.common.data.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author xiaoyuan on 2020/9/9
 */
@Slf4j
@Service
public class ProfitAndLossRecordServiceImpl implements ProfitAndLossRecordService {
    @Resource
    private GaiaSdIncomeStatementDMapper incomeStatementDetailMapper;

    @Resource
    private GaiaGroupDataMapper groupDataMapper;

    @Override
    public PageInfo<ProfitAndLossRecordOutDataObj> conditionQuery(ProfitAndLossRecordInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum() - 1, inData.getPageSize());
        }

        ProfitAndLossRecordOutDataObj profitAndLossRecordOutDataObj = new ProfitAndLossRecordOutDataObj();
        List<ProfitAndLossRecordOutData> outData = this.incomeStatementDetailMapper.conditionQuery(inData);
        BigDecimal differenceTotal = BigDecimal.ZERO;
        BigDecimal costAmountTotal = BigDecimal.ZERO;
        BigDecimal retailSalesTotal = BigDecimal.ZERO;
        BigDecimal qtyTotal = BigDecimal.ZERO;
        BigDecimal retailQtyTotal = BigDecimal.ZERO;

        if (CollUtil.isNotEmpty(outData)){
            int i = 0;
            for(ProfitAndLossRecordOutData recordOutData : outData){
                i++;
                recordOutData.setIndex(i);
                differenceTotal = differenceTotal.add(recordOutData.getDifference() == null ? BigDecimal.ZERO : recordOutData.getDifference());
                costAmountTotal = costAmountTotal.add(recordOutData.getCostAmount() == null ? BigDecimal.ZERO : recordOutData.getCostAmount());
                retailSalesTotal = retailSalesTotal.add(recordOutData.getRetailSales() == null ? BigDecimal.ZERO : recordOutData.getRetailSales());
                qtyTotal = qtyTotal.add(recordOutData.getGsisdStockQty() == null ? BigDecimal.ZERO : recordOutData.getGsisdStockQty());
                retailQtyTotal = retailQtyTotal.add(recordOutData.getGsisdRealQty() == null ? BigDecimal.ZERO : recordOutData.getGsisdRealQty());
            }
        }
        ProfitAndLossRecordOutDataTotal outDataTotal = new ProfitAndLossRecordOutDataTotal();
        outDataTotal.setDifference(differenceTotal);
        outDataTotal.setCostAmount(costAmountTotal);
        outDataTotal.setRetailSales(retailSalesTotal);
        outDataTotal.setGsisdStockQty(qtyTotal);
        outDataTotal.setGsisdRealQty(retailQtyTotal);
        outDataTotal.setIndex(outData.size());
        profitAndLossRecordOutDataObj.setList(outData);
        PageInfo pageInfo = new PageInfo(outData, outDataTotal);
        return pageInfo;
    }
}
