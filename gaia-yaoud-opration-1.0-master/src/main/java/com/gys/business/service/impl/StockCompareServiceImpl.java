package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.StockCompareService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.*;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
public class StockCompareServiceImpl implements StockCompareService {

    @Autowired
    private StockCompareMapper stockCompareMapper;
    @Autowired
    private StockMouthMapper stockMouthMapper;
    @Autowired
    private GaiaMaterialDocMapper materialDocMapper;

    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Autowired
    private StockDiffMapper stockDiffMapper;

    @Resource
    CosUtils cosUtils;

    @Override
    public void compare(StockCompareCondition condition, GetLoginOutData userInfo) {

        //查询所有的地点合集，用于物料凭证数据的过滤，因为物料凭证中存在仓库和门店的所有数据，需要过滤掉门店数据
        ClientSiteChoose clientSiteChoose = this.clientSite();
        //按照需求，对比基于当前的时间进行
        String currentDate = DateUtil.getCurrentDate();
//        String currentDate = "20210201";
        condition.setChooseDate(currentDate.substring(0, currentDate.length() - 2));
        //先进行验证，用户输入的加盟商是否需要是没有QC
        Map<String, List<String>> clientSiteMap = condition.getClientSiteMap();
        List<String> clients = new ArrayList<>();
        clientSiteMap.forEach((x, y) -> {
            clients.add(x);
        });
//        List<String> clients = condition.getClients();
        //此处扩展多客户运算的逻辑
        clients.forEach(c -> {
            List<ClientSiteChoose.KeyValue> initClients = clientSiteChoose.getClientSiteMap().get(c);
            List<String> initKuCenSites = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(initClients)) {
                for (ClientSiteChoose.KeyValue keyValue : initClients) {
                    initKuCenSites.add(keyValue.getKey());
                }
            }
            List<String> sites = clientSiteMap.get(c);
            String chooseYearMonth = condition.getChooseDate();
            String lastMonth = DateUtil.getLastMonth(chooseYearMonth);
            //先进行验证，看看月度库存表中是够存在上月数据
            Example example = new Example(StockMouth.class);
            Example.Criteria criteria = example.createCriteria()
                    .andEqualTo("client", c)
                    .andEqualTo("gsmYearMonth", lastMonth);
            if (CollectionUtil.isNotEmpty(sites)) {
                criteria.andIn("gsmSiteCode", sites);
            }
            example.setOrderByClause("GSM_YEAR_MONTH limit 1 ");
            List<StockMouth> stockMouths = this.stockMouthMapper.selectByExample(example);
            if (CollectionUtil.isEmpty(stockMouths)) {
                throw new BusinessException("客户" + lastMonth + "无月度数据，无法对比,客户编号：" + c);
            }

            //根据用户选择的年月获取月度表中上个月的记录
            List<StockMouth> basicList = getStockMouthByChooseMonth(this.stockMouthMapper, c, sites, lastMonth);
            List<GaiaMaterialDoc> changeList = getChangeMaterialDoc(this.materialDocMapper, c, sites, chooseYearMonth);

            //分别将数据进行按维度分组--分组维度为地点+商品+批次
            Map<String, StockMouth> basicGroupMap = new HashMap<>();
            for (StockMouth b : basicList) {
                //过滤掉虚拟门店只有在initKuCenSites才是最终需要对比的
                if (CollectionUtil.isEmpty(initKuCenSites) || !initKuCenSites.contains(b.getGsmSiteCode())) {
                    continue;
                }
                String key = b.getClient() + b.getGsmSiteCode() + b.getGsmProCode() + b.getGsmBatch();
                basicGroupMap.put(key, b);
            }
            if (CollectionUtil.isNotEmpty(changeList)) {
                List<String> excludeMatTypeInChange = new ArrayList<>();
                excludeMatTypeInChange.add("LX");
                excludeMatTypeInChange.add("CX");
                excludeMatTypeInChange.add("PX");
                excludeMatTypeInChange.add("QC");
//                //过滤一些类型不进行处理
                changeList = changeList.stream().filter(change -> !excludeMatTypeInChange.contains(change.getMatType())).collect(Collectors.toList());

                for (GaiaMaterialDoc ch : changeList) {
                    //过滤掉虚拟门店只有在initKuCenSites才是最终需要对比的
                    if (CollectionUtil.isEmpty(initKuCenSites) || !initKuCenSites.contains(ch.getMatSiteCode())) {
                        continue;
                    }
                    String key = ch.getClient() + ch.getMatSiteCode() + ch.getMatProCode() + ch.getMatBatch();
//                            changeGroupMap.put(key,c);
                    if (basicGroupMap.containsKey(key)) {
                        //QC物料凭证中存在，那么需要将本月的进出情况进行加减
//                                String matType = c.getMatType();
                        String matDebitCredit = ch.getMatDebitCredit();//H-减，S-加
                        if (StrUtil.isNotBlank(matDebitCredit)) {
                            StockMouth qCStockMouth = basicGroupMap.get(key);
                            BigDecimal matQtyFinal = null;
                            BigDecimal matBatAmtFinal = null;
                            BigDecimal matRateBatFinal = null;
                            BigDecimal matQtySY = qCStockMouth.getGsmStockQty();//上月物料凭证数量
                            BigDecimal matBatAmtSY = qCStockMouth.getGsmStockAmt();//上月的总金额（批次）
                            BigDecimal matRateBatSY = qCStockMouth.getGsmTaxAmt();//上月的税金（批次）
                            if ("S".equals(matDebitCredit)) {
                                matQtyFinal = matQtySY.add(ch.getMatQty());
                                matBatAmtFinal = matBatAmtSY.add(ch.getMatBatAmt());
                                matRateBatFinal = matRateBatSY.add(ch.getMatRateBat());
                            } else if ("H".equals(matDebitCredit)) {
                                matQtyFinal = matQtySY.subtract(ch.getMatQty());
                                matBatAmtFinal = matBatAmtSY.subtract(ch.getMatBatAmt());
                                matRateBatFinal = matRateBatSY.subtract(ch.getMatRateBat());
                            }
                            qCStockMouth.setGsmStockQty(matQtyFinal);
                            qCStockMouth.setGsmStockAmt(matBatAmtFinal);
                            qCStockMouth.setGsmTaxAmt(matRateBatFinal);
                            basicGroupMap.put(key, qCStockMouth);//将单条的计算结果汇总到qc的计算结果中
                        }
                    } else {
                        //因为取不到对应的key，这意味着，可能是新的单据，而且理论上只能是新增种类
                        //构建月度库存对象
                        StockMouth stockMouth = new StockMouth();
                        stockMouth.setClient(ch.getClient());
                        stockMouth.setGsmProCode(ch.getMatProCode());
                        stockMouth.setGsmSiteCode(ch.getMatSiteCode());
                        stockMouth.setGsmBatch(ch.getMatBatch());
                        stockMouth.setGsmStockQty(ch.getMatQty());
                        stockMouth.setGsmStockAmt(ch.getMatBatAmt());
                        stockMouth.setGsmTaxAmt(ch.getMatRateBat());
                        stockMouth.setLastUpdateTime(new Date());
                        basicGroupMap.put(key, stockMouth);
                    }
                }
            }

            //得到当前根据物料凭证表获取到根据物料凭证推算的实时库存
            List<StockMouth> stockMouthList = new ArrayList<>();
            for (String key : basicGroupMap.keySet()) {
                StockMouth doc = basicGroupMap.get(key);
                //构建月度库存对象
                doc.setGsmYearMonth(chooseYearMonth);
                doc.setLastUpdateTime(new Date());
                stockMouthList.add(doc);
            }
            //获取仓库的实时库存
            List<StockCompareKuCen> kuCenList = stockCompareMapper.selectKuCenList(c, sites);
            Map<String, StockCompareKuCen> kuCenMap = new HashMap<>();
            if (CollectionUtil.isNotEmpty(kuCenList)) {
                for (StockCompareKuCen k : kuCenList) {
                    //过滤掉虚拟门店只有在initKuCenSites才是最终需要对比的
                    if (CollectionUtil.isEmpty(initKuCenSites) || !initKuCenSites.contains(k.getSite())) {
                        continue;
                    }
                    //存在该维度下多条数据的情况，对数据进行累加
                    String key = k.getClient() + "-" + k.getSite() + "-" + k.getProCode() + "-" + k.getBatch();
                    if (kuCenMap.containsKey(key)) {
                        StockCompareKuCen stockCompareKuCen = kuCenMap.get(key);
                        stockCompareKuCen.setKcsl(stockCompareKuCen.getKcsl().add(k.getKcsl()));
                        kuCenMap.put(key, stockCompareKuCen);
                    } else {
                        kuCenMap.put(key, k);
                    }
                }
            }
            compareDocAndKuCen(stockMouthList, kuCenMap, c, currentDate, userInfo);
        });
    }


    //进行库存对比
    private void compareDocAndKuCen(List<StockMouth> stockMouthList, Map<String, StockCompareKuCen> kuCenMap, String client, String compareDate, GetLoginOutData userInfo) {
        Map<String, List<StockMouth>> clientSiteStockMonthMap = new HashMap<>();
        //对stockMouthList进行针对site的分组
        if (CollectionUtil.isNotEmpty(stockMouthList)) {
            for (StockMouth stockMouth : stockMouthList) {
                String key = stockMouth.getClient() + "-" + stockMouth.getGsmSiteCode();
                if (!clientSiteStockMonthMap.containsKey(key)) {
                    List<StockMouth> list = new ArrayList<>();
                    list.add(stockMouth);
                    clientSiteStockMonthMap.put(key, list);
                } else {
                    List<StockMouth> list = clientSiteStockMonthMap.get(key);
                    list.add(stockMouth);
                    clientSiteStockMonthMap.put(key, list);
                }
            }
        }

        String createDate = CommonUtil.getyyyyMMdd();
        String createTime = CommonUtil.getHHmmss();
        //循环处理不同客户的不同店
        if (CollectionUtil.isNotEmpty(clientSiteStockMonthMap)) {
            for (String key : clientSiteStockMonthMap.keySet()) {
                String site = key.split("-")[1];
                String taskNo = client + site + compareDate;
                //此处需要请求数据库获取最新的任务行号。其实没有很大的意义。。。姑且按设计进行实现，抽象出方法，然后方便后期注释
                int taskItemNum = getLatestTaskItem(client, site, compareDate) + 1;
                //获取每家地点的月度库存
                List<StockMouth> clientSiteStockMouthList = clientSiteStockMonthMap.get(key);
                //每次对一家客户的一家店进行处理
                //记录总的对比情况
                StockCompare stockCompare = new StockCompare();
                stockCompare.setTaskNo(taskNo);
                stockCompare.setTaskItem(taskItemNum + "");
                stockCompare.setClient(client);
                stockCompare.setGscSiteCode(site);
                stockCompare.setGscCompareRow(new BigDecimal(clientSiteStockMouthList.size()));


                //对比条目数
                BigDecimal gscCompareRow = new BigDecimal(0);
                //差异条目数
                BigDecimal gscDiffRow = new BigDecimal(0);
                //实时数量
                BigDecimal gscStockQty = new BigDecimal(0);
                //推算数量
                BigDecimal gscCountQty = new BigDecimal(0);
                //差异数量
//                BigDecimal gscDiffQty = new BigDecimal(0);
                //实时金额
                BigDecimal gscStockAmt = new BigDecimal(0);
                //推算金额
                BigDecimal gscCountAmt = new BigDecimal(0);
                //差异金额
//                BigDecimal gscDiffAmt = new BigDecimal(0);
                //实时税金
                BigDecimal gscTaxAmt = new BigDecimal(0);
                //推算税金
                BigDecimal gscCountTax = new BigDecimal(0);
                //差异金额
//                BigDecimal gscDiffTax = new BigDecimal(0);

                //计算实时金额
                if (CollectionUtil.isNotEmpty(kuCenMap)) {
                    for (String kuCeKey : kuCenMap.keySet()) {
                        if (site.equals(kuCeKey.split("-")[1])) {
//                            if (kuCeKey.equals("10000016-102-21760-F200917999902601")) {
//                                System.out.println("test");
//                            }
                            //表示同一家店，进行计算
                            StockCompareKuCen stockCompareKuCen = kuCenMap.get(kuCeKey);
                            gscStockQty = gscStockQty.add(stockCompareKuCen.getKcsl());
//                            if (stockCompareKuCen == null || stockCompareKuCen.getTotalAmt() == null) {
//                                System.out.println(111);
//                            }
                            if (stockCompareKuCen.getTotalAmt() == null || stockCompareKuCen.getTotalAmt().intValue() == 0) {
                                if (stockCompareKuCen.getMovPrice() == null || stockCompareKuCen.getMovPrice().intValue() == 0) {
                                    gscStockAmt = gscStockAmt.add(new BigDecimal(0));
                                } else {
                                    gscStockAmt = gscStockAmt.add(stockCompareKuCen.getMovPrice().multiply(stockCompareKuCen.getKcsl()));
                                }
                            } else {
                                gscStockAmt = gscStockAmt.add(stockCompareKuCen.getTotalAmt().divide(stockCompareKuCen.getTotalQty(), 4, RoundingMode.HALF_UP).multiply(stockCompareKuCen.getKcsl()));
                            }

                            gscTaxAmt = gscTaxAmt.add(stockCompareKuCen.getRateAmt() == null ? BigDecimal.ZERO : stockCompareKuCen.getRateAmt());
                        }
                    }
                }

                //记录对比存在差异的结果
                List<StockDiff> stockDiffList = new ArrayList<>();
                if (CollectionUtil.isNotEmpty(clientSiteStockMouthList)) {
                    int detailItem = 1;
                    for (StockMouth stockMouth : clientSiteStockMouthList) {
                        //对比条目数加1
                        gscCompareRow = gscCompareRow.add(new BigDecimal(1));
                        //推算数量相加
                        gscCountQty = gscCountQty.add(stockMouth.getGsmStockQty());
                        //推算金额相加
                        gscCountAmt = gscCountAmt.add(stockMouth.getGsmStockAmt());
                        //推算税金相加
                        gscCountTax = gscCountTax.add(stockMouth.getGsmTaxAmt());

                        String kuCenKey = stockMouth.getClient() + "-" + stockMouth.getGsmSiteCode() + "-" + stockMouth.getGsmProCode() + "-" + stockMouth.getGsmBatch();
//                        if (kuCenKey.equals("10000016-102-21760-F200917999902601")) {
//                            System.out.println("test");
//                        }
                        StockCompareKuCen kuCen = kuCenMap.get(kuCenKey);//实时库存
                        if (kuCen != null) {
                            //表示条目对应上了
                            //进行对比
                            BigDecimal totalAmtKC = kuCen.getTotalAmt();
//                            BigDecimal totalQtyKC = kuCen.getTotalQty();
                            BigDecimal kcslKC = kuCen.getKcsl();
                            BigDecimal rateAmtKC = kuCen.getRateAmt();
//                            BigDecimal singlePriceKC = new BigDecimal(0);
//                            //根据描述，totalQtyKC总库存可能存在0的情况，这种情况下需要取移动值
//                            if (totalQtyKC.intValue() == 0) {
//                                singlePriceKC = kuCen.getMovPrice();
//                            } else {
//                                singlePriceKC = totalAmtKC.divide(totalQtyKC);
//                            }
                            //判断存在差异的情况下，再记录对比差异数量
                            if (kcslKC.setScale(2, BigDecimal.ROUND_HALF_UP).compareTo(stockMouth.getGsmStockQty().setScale(2, BigDecimal.ROUND_HALF_UP)) != 0 || totalAmtKC.setScale(2, BigDecimal.ROUND_HALF_UP).compareTo(stockMouth.getGsmStockAmt().setScale(2, BigDecimal.ROUND_HALF_UP)) != 0 || rateAmtKC.setScale(2, BigDecimal.ROUND_HALF_UP).compareTo(stockMouth.getGsmTaxAmt().setScale(2, BigDecimal.ROUND_HALF_UP)) != 0) {
                                StockDiff stockDiff = new StockDiff();
                                stockDiff.setTaskNo(taskNo);
                                stockDiff.setTaskItem(taskItemNum + "");
                                stockDiff.setDetailItem(detailItem + "");
                                stockDiff.setClient(client);
                                stockDiff.setGsdProCode(stockMouth.getGsmProCode());
                                stockDiff.setGscSiteCode(stockMouth.getGsmSiteCode());
                                stockDiff.setGsdBatch(stockMouth.getGsmBatch());
                                stockDiff.setGsdStockQty(kcslKC);
                                stockDiff.setGsdCountQty(stockMouth.getGsmStockQty());
                                stockDiff.setGsdDiffQty(kcslKC.subtract(stockMouth.getGsmStockQty()));
                                stockDiff.setGsdStockAmt(totalAmtKC);
                                stockDiff.setGsdCountAmt(stockMouth.getGsmStockAmt());
                                stockDiff.setGsdDiffAmt(totalAmtKC.subtract(stockMouth.getGsmStockAmt()));
                                stockDiff.setGsdTaxAmt(rateAmtKC);
                                stockDiff.setGsdCountTax(stockMouth.getGsmTaxAmt());
                                stockDiff.setGsdDiffTax(rateAmtKC.subtract(stockMouth.getGsmTaxAmt()));
                                stockDiff.setGscCreateDate(createDate);
                                stockDiff.setGscCreateTime(createTime);
                                stockDiff.setGscCreateBy(userInfo.getUserId());
                                stockDiff.setLastUpdateTime(new Date());
                                detailItem++;
                                gscDiffRow = gscDiffRow.add(new BigDecimal(1));
                                stockDiffList.add(stockDiff);
                            }


                        } else {
                            //表示条目对应不上，月度推算多余实时库存，必定为差异
                            StockDiff stockDiff = new StockDiff();
                            stockDiff.setTaskNo(taskNo);
                            stockDiff.setTaskItem(taskItemNum + "");
                            stockDiff.setDetailItem(detailItem + "");
                            stockDiff.setClient(client);
                            stockDiff.setGsdProCode(stockMouth.getGsmProCode());
                            stockDiff.setGscSiteCode(stockMouth.getGsmSiteCode());
                            stockDiff.setGsdBatch(stockMouth.getGsmBatch());
                            stockDiff.setGsdStockQty(new BigDecimal(0));
                            stockDiff.setGsdCountQty(stockMouth.getGsmStockQty());
                            stockDiff.setGsdDiffQty(new BigDecimal(0).subtract(stockMouth.getGsmStockQty()));
                            stockDiff.setGsdStockAmt(new BigDecimal(0));
                            stockDiff.setGsdCountAmt(stockMouth.getGsmStockAmt());
                            stockDiff.setGsdDiffAmt(new BigDecimal(0).subtract(stockMouth.getGsmStockAmt()));
                            stockDiff.setGsdTaxAmt(new BigDecimal(0));
                            stockDiff.setGsdCountTax(stockMouth.getGsmTaxAmt());
                            stockDiff.setGsdDiffTax(new BigDecimal(0).subtract(stockMouth.getGsmTaxAmt()));
                            stockDiff.setGscCreateDate(createDate);
                            stockDiff.setGscCreateTime(createTime);
                            stockDiff.setGscCreateBy(userInfo.getUserId());
                            stockDiff.setLastUpdateTime(new Date());
                            detailItem++;
                            gscDiffRow = gscDiffRow.add(new BigDecimal(1));
                            stockDiffList.add(stockDiff);
                        }
                    }
                }
                stockCompare.setGscDiffRow(gscDiffRow);

                stockCompare.setGscStockQty(gscStockQty);
                stockCompare.setGscCountQty(gscCountQty);
                stockCompare.setGscDiffQty(gscStockQty.subtract(gscCountQty));

                stockCompare.setGscStockAmt(gscStockAmt);
                stockCompare.setGscCountAmt(gscCountAmt);
                stockCompare.setGscDiffAmt(gscStockAmt.subtract(gscCountAmt));

                stockCompare.setGscTaxAmt(gscTaxAmt);
                stockCompare.setGscCountTax(gscCountTax);
                stockCompare.setGscDiffTax(gscTaxAmt.subtract(gscCountTax));
                stockCompare.setGscCreateDate(createDate);
                stockCompare.setGscCreateTime(createTime);
                stockCompare.setGscCreateBy(userInfo.getUserId());
                stockCompare.setLastUpdateTime(new Date());
                stockCompareMapper.insert(stockCompare);
                stockDiffMapper.insertList(stockDiffList);
            }


        }

    }

    private int getLatestTaskItem(String client, String site, String compareDate) {
        int res = 0;
        Example example = new Example(StockCompare.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("gscSiteCode", site).andEqualTo("taskNo", client + site + compareDate);
        example.setOrderByClause(" GSC_TASK_ITEM desc limit 1");
        StockCompare compare = stockCompareMapper.selectOneByExample(example);
        if (compare != null) {
            if (StrUtil.isNotBlank(compare.getTaskItem())) {
                res = Integer.parseInt(compare.getTaskItem());
            }
        }
        return res;
    }

    @Override
    public ClientSiteChoose clientSite() {
        ClientSiteChoose res = new ClientSiteChoose();
        List<ClientSiteChoose.KeyValue> clients = new ArrayList<>();
        Map<String, List<ClientSiteChoose.KeyValue>> clientSiteMap = new HashMap<>();
        Set<String> clientSet = new HashSet<>();
        List<String> allClients = new ArrayList<>();

        List<GaiaFranchisee> allFranchisees = franchiseeMapper.selectAll();
        Map<String, GaiaFranchisee> franchiseeMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(allFranchisees)) {
            allFranchisees.forEach(f -> {
                franchiseeMap.put(f.getClient(), f);
            });
        }
        //查询所有的仓库信息
        GaiaDcData gaiaDcData = new GaiaDcData();
        gaiaDcData.setDcStatus("0");
        List<GaiaDcData> dcDataDbList = gaiaDcDataMapper.findList(gaiaDcData);
        List<GaiaDcData> dcDataList = dcDataDbList.stream().filter(dc -> dc.getDcInvent() != "1").collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(dcDataList)) {
            for (GaiaDcData dcData : dcDataList) {
                String client = dcData.getClient();
                clientSet.add(client);
            }
            allClients = new ArrayList<>(clientSet);
            if (CollectionUtil.isNotEmpty(allClients)) {
                for (GaiaDcData dcData : dcDataList) {
                    String client = dcData.getClient();
                    ClientSiteChoose.KeyValue info = res.new KeyValue();
                    info.setKey(dcData.getDcCode());
                    info.setValue(dcData.getDcName());
                    if (clientSiteMap.containsKey(client)) {
                        List<ClientSiteChoose.KeyValue> keyValues = clientSiteMap.get(client);
                        keyValues.add(info);
                        clientSiteMap.put(client, keyValues);
                    } else {
                        List<ClientSiteChoose.KeyValue> keyValues = new ArrayList<>();
                        keyValues.add(info);
                        clientSiteMap.put(client, keyValues);
                    }

                }
            }

        }
        allClients = new ArrayList<>(clientSet);
        allClients.forEach(c -> {
            ClientSiteChoose.KeyValue keyValue = res.new KeyValue();
            keyValue.setKey(c);
            keyValue.setValue(franchiseeMap.get(c).getFrancName());
            clients.add(keyValue);
        });
        res.setClients(clients);
        res.setClientSiteMap(clientSiteMap);
        return res;
    }

    @Override
    public PageInfo<StockCompareRes> getStockCompareListPage(StockCompareCondition inData) {
        if (StrUtil.isBlank(inData.getChooseDate())) {
            throw new BusinessException("必须选择年月");
        }
//        if (StrUtil.isBlank(inData.getCompareTaskNo())) {
//            throw new BusinessException("对比任务号为必填项");
//        }
        //为前端处理
        String chooseDateTemp = inData.getChooseDate().replaceAll("-", "");
        inData.setChooseDate(chooseDateTemp);
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        PageInfo pageInfo;
        //处理inData，方便sql处理
        List<String> clients = new ArrayList<>();
        List<String> sites = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(inData.getClientSiteMap())) {
            inData.getClientSiteMap().forEach((x, y) -> {
                if (StrUtil.isNotBlank(x)) {
                    clients.add(x);
                }
                if (CollectionUtil.isNotEmpty(y)) {
                    sites.addAll(y);
                }
            });

        }
        if (CollectionUtil.isNotEmpty(clients)) {
            inData.setClients(clients);
        }

        if (CollectionUtil.isNotEmpty(sites)) {
            inData.setSites(sites);
        }

        List<StockCompareRes> stockMouths = stockCompareMapper.selectByCondition(inData);
//        stockMouths.forEach(x->{
//            x.setGscCreateTime(CommonUtil.parseHHmmss(x.getGscCreateTime()));
//        });

        if (ObjectUtil.isNotEmpty(stockMouths)) {
            pageInfo = new PageInfo(stockMouths);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    private String[] headTotalList = {
            "客户",
            "地点",
            "对比日期",
            "对比时间",
            "对比条目",
            "差异条目",
            "推算数量",
            "实时数量",
            "差异数量",
            "推算金额",
            "实时金额",
            "差异金额"
    };

    @Override
    public Result export(StockCompareCondition inData) {
        if (StrUtil.isBlank(inData.getChooseDate())) {
            throw new BusinessException("必须选择年月");
        }
        String chooseDateTemp = inData.getChooseDate().replaceAll("-", "");
        inData.setChooseDate(chooseDateTemp);
        //处理inData，方便sql处理
        List<String> clients = new ArrayList<>();
        List<String> sites = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(inData.getClientSiteMap())) {
            inData.getClientSiteMap().forEach((x, y) -> {
                if (StrUtil.isNotBlank(x)) {
                    clients.add(x);
                }
                if (CollectionUtil.isNotEmpty(y)) {
                    sites.addAll(y);
                }
            });

        }
        if (CollectionUtil.isNotEmpty(clients)) {
            inData.setClients(clients);
        }

        if (CollectionUtil.isNotEmpty(sites)) {
            inData.setSites(sites);
        }

        List<StockCompareRes> stockCompareResList = stockCompareMapper.selectByCondition(inData);
        try {
            List<List<Object>> dataList = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(stockCompareResList)) {
                for (StockCompareRes stockMouthRes : stockCompareResList) {
                    List<Object> item = new ArrayList<>();
                    dataList.add(item);
                    item.add(stockMouthRes.getFrancName());
                    item.add(stockMouthRes.getSiteName());
                    item.add(stockMouthRes.getGscCreateDate());
                    item.add(stockMouthRes.getGscCreateTime());
                    item.add(stockMouthRes.getGscCompareRow());
                    item.add(stockMouthRes.getGscDiffRow());
                    item.add(stockMouthRes.getGscCountQty());
                    item.add(stockMouthRes.getGscStockQty());
                    item.add(stockMouthRes.getGscDiffQty());
                    item.add(stockMouthRes.getGscCountAmt());
                    item.add(stockMouthRes.getGscCountAmt());
                    item.add(stockMouthRes.getGscDiffAmt());
                }
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(headTotalList);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("月度库存");
                    }});
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, "月度库存." + ExcelUtils.OFFICE_EXCEL_XLSX);
            bos.flush();
            bos.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public PageInfo<StockDiffRes> getStockDiffListPage(StockDiffCondition inData) {

        if (StrUtil.isBlank(inData.getTaskNo()) || StrUtil.isBlank(inData.getTaskItem())) {
            throw new BusinessException("请传递唯一的任务号");
        }

//        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        PageInfo pageInfo;
        List<StockDiffRes> diffResList = stockDiffMapper.selectByCondition(inData);
//        diffResList.forEach(x->{
//            x.setGscCreateTime(CommonUtil.parseHHmmss(x.getGscCreateTime()));
//        });
        if (ObjectUtil.isNotEmpty(diffResList)) {
            pageInfo = new PageInfo(diffResList);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    private String[] headDetailList = {
            "客户",
            "地点",
            "对比日期",
            "对比时间",
            "商品编号",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "批次",
            "推算数量",
            "实时数量",
            "差异数量",
            "推算金额",
            "实时金额",
            "差异金额"
    };

    @Override
    public Result detailListExport(StockDiffCondition inData) {
        if (StrUtil.isBlank(inData.getTaskNo()) || StrUtil.isBlank(inData.getTaskItem())) {
            throw new BusinessException("请传递唯一的任务号");
        }
//        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
//        PageInfo pageInfo;
//        List<StockDiffRes> diffResList = stockDiffMapper.selectByCondition(inData);
//        try {
//            List<List<Object>> dataList = new ArrayList<>();
//            if (CollectionUtil.isNotEmpty(diffResList)) {
//                for (StockDiffRes diffRes : diffResList) {
//                    List<Object> item = new ArrayList<>();
//                    dataList.add(item);
//                    item.add(diffRes.getFrancName());
//                    item.add(diffRes.getSiteName());
//                    item.add(diffRes.getGscCreateDate());
//                    item.add(CommonUtil.parseHHmmss(diffRes.getGscCreateTime()));
//                    item.add(diffRes.getGsdProCode());
//                    item.add(diffRes.getProName());
//                    item.add(diffRes.getProSpecs());
//                    item.add(diffRes.getProFactoryName());
//                    item.add(diffRes.getProUnit());
//                    item.add(diffRes.getGsmBatch());
//                    item.add(diffRes.getGsdCountQty());
//                    item.add(diffRes.getGsdStockQty());
//                    item.add(diffRes.getGsdDiffQty());
//                    item.add(diffRes.getGsdCountAmt());
//                    item.add(diffRes.getGsdStockAmt());
//                    item.add(diffRes.getGsdDiffAmt());
//                }
//            }
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
//                    new ArrayList<String[]>() {{
//                        add(headDetailList);
//                    }},
//                    new ArrayList<List<List<Object>>>() {{
//                        add(dataList);
//                    }},
//                    new ArrayList<String>() {{
//                        add("月度库存对比明细");
//                    }});
//            workbook.write(bos);
//            Result result = cosUtils.uploadFile(bos, "月度库存对比明细." + ExcelUtils.OFFICE_EXCEL_XLSX);
//            bos.flush();
//            bos.close();
//            return result;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;

        List<StockDiffRes> outData = stockDiffMapper.selectByCondition(inData);
        String fileName = "月度库存对比明细";
        if (outData.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            csvInfo = CsvClient.getCsvByte(outData,fileName, Collections.singletonList((short) 1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        } else {
            throw new BusinessException("提示：没有查询到数据,请修改查询条件!");
        }
    }

    /**
     * 根据条件查询出月度库存表中的数据
     *
     * @param stockMouthMapper
     * @param client
     * @param sites
     * @param yearMonth
     * @return
     */
    private List<StockMouth> getStockMouthByChooseMonth(StockMouthMapper stockMouthMapper, String client, List<String> sites, String yearMonth) {
        List<StockMouth> resList = new ArrayList<>();
        Example example = new Example(StockMouth.class);
        Example.Criteria criteria = example.createCriteria()
                .andEqualTo("client", client)
                .andEqualTo("gsmYearMonth", yearMonth);
        if (CollectionUtil.isNotEmpty(sites)) {
            criteria.andIn("gsmSiteCode", sites);
        }
        List<StockMouth> stockMouthList = stockMouthMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(stockMouthList)) {
            resList = stockMouthList;
        }
        return resList;
    }

    private List<GaiaMaterialDoc> getChangeMaterialDoc(GaiaMaterialDocMapper materialDocMapper, String client, List<String> sites, String yearMonth) {
        List<GaiaMaterialDoc> resList = new ArrayList<>();
        Example example = new Example(GaiaMaterialDoc.class);
        Example.Criteria criteria = example.createCriteria()
                .andEqualTo("client", client)
                .andLike("matPostDate", yearMonth + "%");
        if (CollectionUtil.isNotEmpty(sites)) {
            criteria.andIn("matSiteCode", sites);
        }
        example.orderBy("matCreateTime");
        List<GaiaMaterialDoc> materialDocList = materialDocMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(materialDocList)) {
            resList = materialDocList;
        }
        return resList;
    }
}
