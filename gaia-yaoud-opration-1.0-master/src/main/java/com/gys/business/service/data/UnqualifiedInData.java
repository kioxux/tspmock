package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UnqualifiedInData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码(可多选)")
    private String[] brId;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "起始日期")
    private String startDate;
    @ApiModelProperty(value = "结束日期")
    private String endDate;
    @ApiModelProperty(value = "单号")
    private String voucherId;
}
