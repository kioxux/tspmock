package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProvCityRelationshipOutData implements Serializable {

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private String cityCode;

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private String provCode;
}
