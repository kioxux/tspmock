//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class StoreInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gsstBrId;

    private String stoStatus;

    private String stoName;

    private String stoCode;

    public StoreInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsstBrId() {
        return this.gsstBrId;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsstBrId(final String gsstBrId) {
        this.gsstBrId = gsstBrId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof StoreInData)) {
            return false;
        } else {
            StoreInData other = (StoreInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsstBrId = this.getGsstBrId();
                Object other$gsstBrId = other.getGsstBrId();
                if (this$gsstBrId == null) {
                    if (other$gsstBrId != null) {
                        return false;
                    }
                } else if (!this$gsstBrId.equals(other$gsstBrId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof StoreInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsstBrId = this.getGsstBrId();
        result = result * 59 + ($gsstBrId == null ? 43 : $gsstBrId.hashCode());
        return result;
    }

    public String toString() {
        return "StoreInData(clientId=" + this.getClientId() + ", gsstBrId=" + this.getGsstBrId() + ")";
    }
}
