//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaSdMedCheckDMapper;
import com.gys.business.mapper.GaiaSdMedCheckHMapper;
import com.gys.business.mapper.entity.GaiaSdMedCheckD;
import com.gys.business.mapper.entity.GaiaSdMedCheckH;
import com.gys.business.service.MedCheckService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MedCheckServiceImpl implements MedCheckService {
    @Autowired
    private GaiaSdMedCheckHMapper medCheckHMapper;
    @Autowired
    private GaiaSdMedCheckDMapper medCheckDMapper;

    public MedCheckServiceImpl() {
    }

    public List<GetMedCheckOutData> selectMedCheckList(GetMedCheckInData inData) {
//        if (StrUtil.isBlank(inData.getGsmcdProId()) && StrUtil.isBlank(inData.getGsmchEmp()) && StrUtil.isBlank(inData.getGsmchDate()) && StrUtil.isBlank(inData.getGsmcdBatchNo()) && StrUtil.isBlank(inData.getGsmchStatus()) && StrUtil.isBlank(inData.getGsmchType())) {
//            throw new BusinessException("必须至少填写一项内容进行查询！");
//        } else {
            List<GetMedCheckOutData> list = this.medCheckHMapper.selectMedCheckList(inData);
            return list;
//        }
    }

    @Transactional
    public void saveMedCheck(GetMedCheckInData inData, GetLoginOutData userInfo) {
        String client = userInfo.getClient();
        String codePre = DateUtil.format(new Date(), "yyyy");
        String voucherId = this.medCheckHMapper.selectNextVoucherId(client, codePre);
        GaiaSdMedCheckH h = new GaiaSdMedCheckH();
        h.setClientId(client);
        h.setGsmchVoucherId(voucherId);
        h.setGsmchBrId(userInfo.getDepId());
        h.setGsmchBrName(userInfo.getDepName());
        h.setGsmchType(inData.getGsmchType());
        h.setGsmchDate(inData.getGsmchDate());
        h.setGsmchEmp(inData.getGsmchEmp());
        h.setGsmchStatus("0");
        this.medCheckHMapper.insert(h);
        List<GetMedCheckDInData> medCheckDs = inData.getMedCheckDs();
        int index = 1;

        for(Iterator var9 = medCheckDs.iterator(); var9.hasNext(); ++index) {
            GetMedCheckDInData medCheckD = (GetMedCheckDInData)var9.next();
            GaiaSdMedCheckD d = new GaiaSdMedCheckD();
            d.setClientId(client);
            d.setGsmcdVoucherId(voucherId);
            d.setGsmcdBrId(userInfo.getDepId());
            d.setGsmcdSerial(StrUtil.toString(index));
            d.setGsmcdProId(medCheckD.getGsmcdProId());
            d.setGsmcdBatchNo(medCheckD.getGsmcdBatchNo());
            d.setGsmcdQty(medCheckD.getGsmcdQty());
            d.setGsmcdResult(medCheckD.getGsmcdResult());
            this.medCheckDMapper.insert(d);
        }

    }

    public GetMedCheckOutData selectMedCheckOne(GetMedCheckInData inData) {
        GaiaSdMedCheckH h = (GaiaSdMedCheckH)this.medCheckHMapper.selectByPrimaryKey(inData);
        if (ObjectUtil.isNull(h)) {
            throw new BusinessException("药品养护单据不存在");
        } else {
            GetMedCheckOutData outData = new GetMedCheckOutData();
            outData.setGsmchVoucherId(h.getGsmchVoucherId());
            outData.setGsmchBrName(h.getGsmchBrName());
            outData.setGsmchDate(h.getGsmchDate());
            outData.setGsmchEmp(h.getGsmchEmp());
            outData.setGsmchType(h.getGsmchType());
            outData.setGsmchStatus(h.getGsmchStatus());
            List<GetMedCheckDOutData> medCheckDs = this.medCheckDMapper.selectMedCheckDs(inData);
            Iterator var5 = medCheckDs.iterator();

            while(var5.hasNext()) {
                GetMedCheckDOutData medCheckD = (GetMedCheckDOutData)var5.next();
                if (!StrUtil.isBlank(medCheckD.getGssbVaildDate())) {
                    long days = DateUtil.betweenDay(new Date(), DateUtil.parse(medCheckD.getGssbVaildDate(), "yyyyMMdd"), true);
                    medCheckD.setVaild(StrUtil.toString(days));
                }
            }

            outData.setMedCheckDs(medCheckDs);
            return outData;
        }
    }

    @Transactional
    public void approveMedCheck(GetMedCheckInData inData, GetLoginOutData userInfo) {
        inData.setClientId(userInfo.getClient());
        GaiaSdMedCheckH h = (GaiaSdMedCheckH)this.medCheckHMapper.selectByPrimaryKey(inData);
        if (ObjectUtil.isNull(h)) {
            throw new BusinessException("药品养护单据不存在");
        } else if ("1".equals(h.getGsmchStatus())) {
            throw new BusinessException("该药品养护单据已审核");
        } else {
            h.setGsmchExamineDate(DateUtil.format(new Date(), "yyyyMMdd"));
            h.setGsmchExamineEmp(userInfo.getUserId());
            h.setGsmchStatus("1");
            this.medCheckHMapper.updateByPrimaryKey(h);
        }
    }

    public List<UserOutData> selectPharmacistByBrId(UserInData inData) {
        List<UserOutData> list = this.medCheckHMapper.selectPharmacistByBrId(inData);
        return list;
    }
}
