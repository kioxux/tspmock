package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@CsvRow("会员信息")
public class MemberListOutData implements Serializable {

    private static final long serialVersionUID = 1629464568751381941L;

    @CsvCell(title = "办卡门店编码", index = 1, fieldNo = 1)
    @ApiModelProperty(value = "门店号")
    private String brId;

    @CsvCell(title = "办卡门店名称", index = 2, fieldNo = 1)
    @ApiModelProperty(value = "门店名称")
    private String brName;

    @ApiModelProperty(value = "会员编号")
    private String memberId;

    @CsvCell(title = "创建日期", index = 3, fieldNo = 1)
    @ApiModelProperty(value = "办卡日期")
    private String inputCardDate;

    @CsvCell(title = "办卡员工", index = 4, fieldNo = 1)
    @ApiModelProperty(value = "办卡员工")
    private String inputCardEmp;

    @CsvCell(title = "最近消费日期", index = 5, fieldNo = 1)
    @ApiModelProperty(value = "最近消费日期")
    private String lastIntegeralDate;

    @CsvCell(title = "会员卡号", index = 6, fieldNo = 1)
    @ApiModelProperty(value = "会员卡号")
    private String cardId;

    @CsvCell(title = "姓名", index = 7, fieldNo = 1)
    @ApiModelProperty(value = "会员姓名")
    private String memberName;

    @CsvCell(title = "性别", index = 8, fieldNo = 1)
    @ApiModelProperty(value = "会员性别")
    private String sex;

    @CsvCell(title = "卡类别", index = 9, fieldNo = 1)
    @ApiModelProperty(value = "会员卡类别")
    private String cardType;

    @ApiModelProperty(value = "卡类型编号")
    private String cardTypeId;

    @CsvCell(title = "手机", index = 10, fieldNo = 1)
    @ApiModelProperty(value = "手机")
    private String mobile;

    @CsvCell(title = "电话", index = 11, fieldNo = 1)
    @ApiModelProperty(value = "电话")
    private String tel;

    @CsvCell(title = "地址", index = 12, fieldNo = 1)
    @ApiModelProperty(value = "地址")
    private String address;

    @CsvCell(title = "累计总金额", index = 13, fieldNo = 1)
    @ApiModelProperty(value = "累计金额")
    private BigDecimal totalAmt;

    @CsvCell(title = "年度累计金额", index = 14, fieldNo = 1)
    @ApiModelProperty(value = "年度累计金额")
    private BigDecimal yearTotalAmt;

    @CsvCell(title = "累计积分", index = 15, fieldNo = 1)
    @ApiModelProperty(value = "累计积分")
    private BigDecimal totalIntegeral;

    @CsvCell(title = "年度累计积分", index = 16, fieldNo = 1)
    @ApiModelProperty(value = "年度累计积分")
    private BigDecimal yearTotalIntegeral;

    @CsvCell(title = "积分", index = 17, fieldNo = 1)
    @ApiModelProperty(value = "当前积分")
    private BigDecimal currIntegeral;

    @CsvCell(title = "储值卡余额", index = 18, fieldNo = 1)
    @ApiModelProperty(value = "储值卡余额")
    private BigDecimal currCardAmt;

    @ApiModelProperty(value = "出生年月")
    private String birthDay;

    @ApiModelProperty(value = "身份证")
    private String identityCard;

    @ApiModelProperty(value = "年龄")
    private String age;
    
    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "卡状态")
    private String gsmbcStatus;

    @ApiModelProperty(value = "民族名称")
    private String gsmbNationality;

    @ApiModelProperty(value = "生日历法 默认为0 -公历（阳历）；1-农历（阴历）")
    private String gsmbBirthMode;

    @ApiModelProperty(value = "办卡渠道")
    private String gsmbcChannel;
}
