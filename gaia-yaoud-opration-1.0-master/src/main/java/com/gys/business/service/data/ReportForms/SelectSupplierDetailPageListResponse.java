package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SelectSupplierDetailPageListResponse {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "仓库地点/门店编码")
    private String depId;

    @ApiModelProperty(value = "仓库地点/门店编码名称拼接")
    private String depIdName;

    @ApiModelProperty(value = "供应商编码")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商名称")
    private String supSelfName;

    @ApiModelProperty(value = "二级部门编码")
    private String salesmanCode;

    @ApiModelProperty(value = "业务员名称")
    private String salesmanName;

    @ApiModelProperty(value = "业务员类型")
    private String matType;

    @ApiModelProperty(value = "发生日期")
    private String postDate;

    @ApiModelProperty(value = "单据编号")
    private String poId;

    @ApiModelProperty(value = "结算金额")
    private BigDecimal billingAmount;

    @ApiModelProperty(value = "发票金额")
    private BigDecimal invoiceAmount;

}
