package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class GetPoInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    private String storeId;
    private String storeCode;
    private String paymentId;
    private String remark;
    @ApiModelProperty(value = "供应商编码")
    private String supCode;
    @ApiModelProperty(value = "订单总价")
    private BigDecimal totalAmt;
    private List<GetPoDetailInData> poDetailList;
    @ApiModelProperty(value = "收货单号")
    private String gsahVoucherId;
    @ApiModelProperty(value = "创建人")
    private String userId;
    @ApiModelProperty(value = "采购订单号")
    private String voucherId;
    @ApiModelProperty(value = "业务员编号")
    private String busEmpCode;
    @ApiModelProperty(value = "业务员姓名")
    private String busEmpName;

    private  ColdchainInfo coldchainInfo;
}
