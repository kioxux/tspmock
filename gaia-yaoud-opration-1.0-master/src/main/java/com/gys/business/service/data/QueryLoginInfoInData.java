package com.gys.business.service.data;

import lombok.Data;


@Data
public class QueryLoginInfoInData {
    private String clientId;

    //门店ID或连锁ID
    private String brId;
    //爬虫脚本类名
    private String className;
    //用户名
    private String userName;

    private String cookies;

    private String jsessionId;

    private String token;
    private String loginHtml;
    private String searchHtml;
    private String addition1;
    private String addition2;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"clientId\":\"")
                .append(clientId).append('\"');
        sb.append(",\"brId\":\"")
                .append(brId).append('\"');
        sb.append(",\"className\":\"")
                .append(className).append('\"');
        sb.append(",\"userName\":\"")
                .append(userName).append('\"');
        sb.append(",\"cookies\":\"")
                .append(cookies).append('\"');
        sb.append(",\"jsessionId\":\"")
                .append(jsessionId).append('\"');
        sb.append(",\"token\":\"")
                .append(token).append('\"');
        sb.append(",\"loginHtml\":\"")
                .append(loginHtml).append('\"');
        sb.append(",\"searchHtml\":\"")
                .append(searchHtml).append('\"');
        sb.append(",\"addition1\":\"")
                .append(addition1).append('\"');
        sb.append(",\"addition2\":\"")
                .append(addition2).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
