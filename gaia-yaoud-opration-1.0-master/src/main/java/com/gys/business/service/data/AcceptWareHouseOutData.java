package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AcceptWareHouseOutData {
    @ApiModelProperty("拣货单号")
    private String pickOrderId;
    @ApiModelProperty("配送单号")
    private String psVoucherId;
    @ApiModelProperty("拣货日期")
    private String pickOrderDate;
    @ApiModelProperty("配送数量")
    private String pickProQty;
    @ApiModelProperty("配送金额")
    private String pickProAmt;
    @ApiModelProperty("零售金额")
    private String lsProAmt;
    @ApiModelProperty("拣货单复核方式 1正常复核 2一键复核")
    private String checkType;
    @ApiModelProperty("收货日期")
    private String gsahDate;
    @ApiModelProperty("配送地点")
    private String gsahFrom;
    @ApiModelProperty("收货地点")
    private String gsahTo;
    @ApiModelProperty("收货状态")
    private String acceptStatus;
    @ApiModelProperty("拣货件数")
    private String boxCount;
    @ApiModelProperty(value = "医保机构定点编码")
    private String stoYbCode;
    /**
     * 是否包含冷链商品 1：包含， 0：不包含
     */
    private String coldchainFlag;
}
