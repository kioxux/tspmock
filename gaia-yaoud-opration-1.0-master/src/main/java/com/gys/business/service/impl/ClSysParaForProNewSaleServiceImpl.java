package com.gys.business.service.impl;

import com.gys.business.mapper.ClSysParaForProNewSaleMapper;
import com.gys.business.service.ClSysParaForProNewSaleService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.netflix.discovery.converters.Auto;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 17:05 2021/8/20
 * @Description：商品首营页面新品铺货开关service
 * @Modified By：guoyuxi.
 * @Version:
 */
@Service
public class ClSysParaForProNewSaleServiceImpl implements ClSysParaForProNewSaleService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ClSysParaForProNewSaleMapper clSysParaForProNewSaleMapper;

    @Override
    public JsonResult saveStatus(Map<String, String> statusMap) {
        Map<String, Object> clientMap = redisTemplate.opsForHash().entries(statusMap.get("client"));
        if (clientMap == null || clientMap.size() <= 0) {
            redisTemplate.opsForHash().put(statusMap.get("client"), "status", statusMap.get("status"));
            redisTemplate.opsForHash().put(statusMap.get("client"), "numbers", "1");
        }
        clientMap = redisTemplate.opsForHash().entries(statusMap.get("client"));
        if ("0".equals(statusMap.get("status"))) {
            String countstr = clientMap.get("numbers").toString();
            Integer count = new Integer(countstr == null ? "0" : countstr);
            if (count>2) {
                clSysParaForProNewSaleMapper.changeStatus(statusMap.get("client"), "0");
                redisTemplate.opsForHash().put(statusMap.get("client"), "numbers", "1");
            } else {
                redisTemplate.opsForHash().increment(statusMap.get("client"),"numbers",1);
            }
        } else {
            clSysParaForProNewSaleMapper.changeStatus(statusMap.get("client"),"1");
        }

        return JsonResult.success(null, "保存成功");
    }

    @Override
    public JsonResult getStatus(GetLoginOutData loginUser) {
        if (loginUser.getClient() == null) {
            throw new BusinessException("登录已过期");
        }
        String status = clSysParaForProNewSaleMapper.getStatus(loginUser.getClient());
        if (status == null) {
            saveNewClientStatus(loginUser.getClient());
            return JsonResult.success(clSysParaForProNewSaleMapper.getStatus(loginUser.getClient()), "查询成功");
        }
        return JsonResult.success(status, "查询成功");
    }

    public void saveNewClientStatus(String client) {
        clSysParaForProNewSaleMapper.saveNewClientStatus(client);
    }
}
