package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.MibService;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.mib.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.business.service.data.mib.mibEnum.InsuTypeEnum;
import com.gys.business.service.data.mib.mibEnum.MedTypeEnum;
import com.gys.business.service.data.mib.mibEnum.PersonTypeEnum;
import com.gys.common.excel.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @author wavesen.shen
 */
@Service
public class MibServiceImpl implements MibService {

    @Autowired
    private MibDrugDetailMapper mibDrugDetailMapper;
    @Autowired
    private MibDetlcutInfoMapper mibDetlcutInfoMapper;
    @Autowired
    private MibDrugInfoMapper mibDrugInfoMapper;
    @Autowired
    private MibSetlDetailMapper mibSetlDetailMapper;
    @Autowired
    private MibSetlInfoMapper mibSetlInfoMapper;
    @Autowired
    private MibReconciliationMapper mibReconciliationMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Override
    public void saveMibDrugInfoAndDetail(GetLoginOutData userInfo, PayInfoData inData) {
        if (ObjectUtil.isNotEmpty(inData.getMibDrugInfo())) {
            Example example = new Example(MibDrugInfo.class);
            example.createCriteria()
                    .andEqualTo("client", (inData.getMibDrugInfo()).getClient())
                    .andEqualTo("brId", (inData.getMibDrugInfo()).getBrId())
                    .andEqualTo("billNo", (inData.getMibDrugInfo()).getBillNo())
                    .andEqualTo("msgId", (inData.getMibDrugInfo()).getMsgId());
            List<MibDrugInfo> hasList = mibDrugInfoMapper.selectByExample(example);
            if (CollUtil.isEmpty(hasList)) {
                mibDrugInfoMapper.insert(inData.getMibDrugInfo());
                if (CollUtil.isNotEmpty(inData.getMibDrugDetails())) {
                    mibDrugDetailMapper.insertList(inData.getMibDrugDetails());
                }
            }
        }
    }

    @Override
    public void saveMibSetlInfoAndDetail(GetLoginOutData userInfo, SetlInfoData inData) {
        if (ObjectUtil.isNotEmpty(inData.getMibSetlInfo())) {
            Example example = new Example(MibSetlInfo.class);
            example.createCriteria()
                    .andEqualTo("client", inData.getMibSetlInfo().getClient())
                    .andEqualTo("brId", inData.getMibSetlInfo().getBrId())
                    .andEqualTo("billNo", inData.getMibSetlInfo().getBillNo())
                    .andEqualTo("msgId", (inData.getMibSetlInfo()).getMsgId());
            List<MibSetlInfo> hasList = mibSetlInfoMapper.selectByExample(example);
            if (CollUtil.isEmpty(hasList)) {
                mibSetlInfoMapper.insert(inData.getMibSetlInfo());
                if (CollUtil.isNotEmpty(inData.getMibSetlDetails())) {
                    mibSetlDetailMapper.insertList(inData.getMibSetlDetails());
                }
                if (CollUtil.isNotEmpty(inData.getMibDetlcutInfos())) {
                    mibDetlcutInfoMapper.insertList(inData.getMibDetlcutInfos());
                }
            }
        }
    }

    /**
     * 获取山西省忻州市医疗保险普通门诊结算单数据
     * @param userInfo
     * @param inData
     * @return
     */
    @Override
    public MibSXXZJSFormInfoOutData getMibSXXZJSForm(GetLoginOutData userInfo, MibRePrintFormInData inData) {
        MibSXXZJSFormInfoOutData jsForm = mibSetlInfoMapper.getMibSXXZJSForm(inData);
        MedTypeEnum medTypeEnum = MedTypeEnum.getEnumByCode(jsForm.getMedicalCategory());
        PersonTypeEnum personTypeEnum = PersonTypeEnum.getEnumByCode(jsForm.getPersonalCategory());
        InsuTypeEnum insuTypeEnum = InsuTypeEnum.getEnumByCode(jsForm.getInsuranceType());
        if (Objects.nonNull(medTypeEnum)) {
            jsForm.setMedicalCategory(medTypeEnum.getValue());
        }
        if (Objects.nonNull(personTypeEnum)) {
            jsForm.setPersonalCategory(personTypeEnum.getValue());
        }
        if (Objects.nonNull(insuTypeEnum)) {
            jsForm.setInsuranceType(insuTypeEnum.getValue());
        }
        return jsForm;
    }

    /**
     * 获取山西省朔州市医疗保险普通门诊结算单数据
     * @param userInfo
     * @param inData
     * @return
     */
    @Override
    public MibSXSZHttpOutData getMibSXSZJSForm(GetLoginOutData userInfo, MibRePrintFormInData inData) {
        MibSXSZHttpOutData httpOutData=new MibSXSZHttpOutData();
        MibSXSZJSFormData jsForm = mibSetlInfoMapper.getMibSXSZJSForm(inData);
        PersonTypeEnum personTypeEnum = PersonTypeEnum.getEnumByCode(jsForm.getPersonalCategory());
        if (Objects.nonNull(personTypeEnum)) {
            jsForm.setPersonalCategory(personTypeEnum.getValue());
        }
        List<MibSXSZJSProListData> dataList=mibSetlInfoMapper.getMibSXSZJSProList(inData);
        httpOutData.setFormData(jsForm);
        httpOutData.setListData(dataList);
        return httpOutData;
    }

    @Override
    public InsuranceSettleTableOutData getMibJSSplitForm(MibRePrintFormInData inData) {
        InsuranceSettleTableOutData form = mibSetlInfoMapper.getMibJSSplitForm(inData);
        form.setUserType(PersonTypeEnum.getEnumByCode(form.getUserType())==null?form.getUserType():PersonTypeEnum.getEnumByCode(form.getUserType()).getValue());
        form.setMedicalType(MedTypeEnum.getEnumByCode(form.getMedicalType())==null?form.getMedicalType():MedTypeEnum.getEnumByCode(form.getMedicalType()).getValue());
        form.setOperationTime(DateUtil.format(DateUtil.parse(form.getOperationTime(), "yyyyMMddHHmmss"),"yyyy-MM-dd HH:mm:ss"));
        form = InsuranceSettleTableOutData.disposalData(form);
        return form;
    }
	@Override
    public List<MibInfoOutData> getSeltInfo(GetLoginOutData userInfo, InData inData) {
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        List<MibInfoOutData> hasList = mibSetlInfoMapper.getSeltInfo(inData);
        return hasList;
    }
    @Override
    public List<MibInfoOutData> getSeltInfoSum(GetLoginOutData userInfo, InData inData) {
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        String mibArea = storeDataMapper.selectStoPriceComparison(inData.getClient(), inData.getStoCode(), "MIB_AREA");
        List<MibInfoOutData> hasList;
        if ("HLJ".equals(mibArea)) {
            hasList = mibSetlInfoMapper.getSeltInfoSumByHlj(inData);
        } else {
            hasList = mibSetlInfoMapper.getSeltInfoSum(inData);
        }

        return hasList;
    }

    @Override
    public List<MibInfoOutData> getSeltInfo3202(GetLoginOutData userInfo, InData inData) {
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        List<MibInfoOutData> hasList = mibSetlInfoMapper.getSeltInfo3202(inData);
        return hasList;
    }
    @Override
    public void updateCzFlag(Map<String, Object> map) {
//        inData.setClient(userInfo.getClient());
//        inData.setStoCode(userInfo.getDepId());
        mibSetlInfoMapper.updateCzFlag(map);
    }

    @Override
    public void updateSetlInfoState(Map<String, Object> map) {
        mibSetlInfoMapper.updateSetlInfoState(map);
    }

    @Override
    public void reconciliationInsert(MibReconciliation reconciliation) {
        mibReconciliationMapper.insert(reconciliation);
    }

    @Override
    public MibReconciliation reconciliationSelect(MibReconciliation reconciliation) {
        return mibReconciliationMapper.selectOne(reconciliation);
    }

    @Override
    public void reconciliationUpdate(MibReconciliation reconciliation) {
//        Example example = new Example(MibReconciliation.class);
//        example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("gspmBrId", userInfo.getDepId());
        mibReconciliationMapper.updateByPrimaryKeySelective(reconciliation);
    }

    @Override
    public void reconciliationInsertOrUpdate(MibReconciliation reconciliation) {
        MibReconciliation outData = mibReconciliationMapper.selectByPrimaryKey(reconciliation);
        if(ObjectUtil.isEmpty(outData)){
            mibReconciliationMapper.insert(reconciliation);
        }else {
            mibReconciliationMapper.updateByPrimaryKeySelective(reconciliation);
        }
    }

    @Override
    public PaymentOut getSeltInfoOne(InData inData) {
        List<PaymentOut> outList = mibSetlInfoMapper.getSeltInfoList(inData);
        if (CollUtil.isEmpty(outList)){
            return null;
        }
        return outList.get(0);
    }
}
