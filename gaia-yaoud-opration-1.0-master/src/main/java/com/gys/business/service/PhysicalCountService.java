package com.gys.business.service;

import com.gys.business.service.data.GetPhysicalCountHeadOutData;
import com.gys.business.service.data.GetPhysicalCountInData;
import com.gys.business.service.data.GetPhysicalCountOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface PhysicalCountService {
    List<GetPhysicalCountHeadOutData> listPhysical(GetPhysicalCountInData inData);

    List<GetPhysicalCountHeadOutData> selectDetailById(GetPhysicalCountInData inData);

    List<GetPhysicalCountOutData> selectPhysical(GetPhysicalCountInData inData);

    void savePhysical(GetPhysicalCountInData inData);

    GetPhysicalCountOutData selectPhysicalOne(GetPhysicalCountInData inData);

    void approvePhysical(GetPhysicalCountInData inData, GetLoginOutData userInfo);

    void savePhysicalDiff(GetPhysicalCountInData inData, GetLoginOutData userInfo);

    List<GetPhysicalCountOutData> selectPhysicalDiff(GetPhysicalCountInData inData);

    void updatePhysicalDiff(GetPhysicalCountInData inData, GetLoginOutData userInfo);

    void approvePhysicalDiff(GetPhysicalCountInData inData, GetLoginOutData userInfo);
}
