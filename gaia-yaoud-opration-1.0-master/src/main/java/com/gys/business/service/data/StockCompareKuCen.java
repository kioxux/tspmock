package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class StockCompareKuCen implements Serializable {
    private static final long serialVersionUID = 7783480617845023896L;

    @ApiModelProperty(value = "客户", required = true)
    private String client;

    @ApiModelProperty(value = "地点", required = true)
    private String site;

    @ApiModelProperty(value = "商品编码", required = true)
    private String proCode;

    @ApiModelProperty(value = "批次号", required = true)
    private String batch;

    @ApiModelProperty(value = "库存数量", required = true)
    private BigDecimal kcsl;

    @ApiModelProperty(value = "可用数量", required = true)
    private BigDecimal kysl;

    @ApiModelProperty(value = "总库存", required = true)
    private BigDecimal totalQty;

    @ApiModelProperty(value = "总金额", required = true)
    private BigDecimal totalAmt;

    @ApiModelProperty(value = "移动平均价", required = true)
    private BigDecimal movPrice;

    @ApiModelProperty(value = "税金", required = true)
    private BigDecimal rateAmt;

    @ApiModelProperty(value = "加点后金额", required = true)
    private BigDecimal addAmt;

    @ApiModelProperty(value = "加点后税金", required = true)
    private BigDecimal addTax;
}
