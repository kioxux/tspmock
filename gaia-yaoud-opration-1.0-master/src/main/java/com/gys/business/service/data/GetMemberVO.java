package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 请求参数实体
 *
 * @author xiaoyuan on 2020/9/1
 */
@Data
//@ApiModel(value = "请求参数")
public class GetMemberVO implements Serializable {
    private static final long serialVersionUID = -3423225764422739145L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "会员卡号")
    private String gmbCardId;

    @ApiModelProperty(value = "会员名称")
    private String gmbName;

    @ApiModelProperty(value = "手机号")
    private String gmbMobile;

    private int pageNum;
    private int pageSize;
}
