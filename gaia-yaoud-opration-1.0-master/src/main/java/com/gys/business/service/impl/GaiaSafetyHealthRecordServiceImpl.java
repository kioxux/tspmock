package com.gys.business.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.excel.EasyExcel;
import com.gys.business.mapper.GaiaSafetyHealthRecordMapper;
import com.gys.business.mapper.entity.GaiaSafetyHealthRecord;
import com.gys.business.mapper.entity.GaiaSafetyHealthRecordExport;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.CommonService;
import com.gys.business.service.GaiaSafetyHealthRecordService;
import com.gys.common.enums.SerialCodeTypeEnum;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.common.response.ResultUtil;
import com.gys.util.CommonUtil;
import com.gys.util.CosUtils;
import com.gys.util.ExcelStyleUtil;
import org.apache.commons.collections.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GaiaSafetyHealthRecordServiceImpl implements GaiaSafetyHealthRecordService {
    @Resource
    private GaiaSafetyHealthRecordMapper gaiaSafetyHealthRecordMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    CosUtils cosUtils;

    @Override
    public void add(GaiaSafetyHealthRecord inData) {
        String voucherId = commonService.getSdSerialCode(inData.getClient(),inData.getStoreId(), SerialCodeTypeEnum.SAFETY_HEALTH);
        inData.setVoucherId(voucherId);
        inData.setIsDelete(0);
        inData.setStatus("0");
        gaiaSafetyHealthRecordMapper.insertSelective(inData);
    }

    @Override
    public void delete(GaiaSafetyHealthRecord inData) {
        if(CollectionUtils.isEmpty(inData.getIds())) {
            throw new BusinessException("提示：必填参数不能为空!");
        }
        inData.setIsDelete(1);
        gaiaSafetyHealthRecordMapper.deleteByPrimaryKey(inData);
    }

    @Override
    public void update(GaiaSafetyHealthRecord inData) {
        if(CollectionUtils.isEmpty(inData.getIds())) {
            throw new BusinessException("提示：必填参数不能为空!");
        }
        if(ObjectUtil.isEmpty(inData.getCheckDate())){
            inData.setCheckDate("1970-01-01 00:00:00");
        }
        if(ObjectUtil.isEmpty(inData.getRecordDate())){
            inData.setRecordDate("1970-01-01 00:00:00");
        }
        if(ObjectUtil.isEmpty(inData.getRecorderId())){
            inData.setRecorderId("");
            inData.setRecorderName("");
        }
        if(ObjectUtil.isEmpty(inData.getMasterId())){
            inData.setMasterId("");
            inData.setMasterName("");
        }
        if(ObjectUtil.isEmpty(inData.getHealthCheck())){
            inData.setHealthCheck("");
        }
        if(ObjectUtil.isEmpty(inData.getSafetyCheck())){
            inData.setSafetyCheck("");
        }
        if(ObjectUtil.isEmpty(inData.getRemark())){
            inData.setRemark("");
        }

        gaiaSafetyHealthRecordMapper.updateByPrimaryKeySelective(inData);
    }

    @Override
    public List<GaiaSafetyHealthRecord> query(GaiaSafetyHealthRecord inData) {
        return gaiaSafetyHealthRecordMapper.selectSafetyHealthRecord(inData);
    }

    @Override
    public void submit(GaiaSafetyHealthRecord inData) {
        if(CollectionUtils.isEmpty(inData.getIds())) {
            throw new BusinessException("提示：必填参数不能为空!");
        }
        inData.setStatus("1");
        gaiaSafetyHealthRecordMapper.submitByPrimaryKeySelective(inData);
    }

    @Override
    public Result export(GaiaSafetyHealthRecord inData) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        List<GaiaSafetyHealthRecord> dataList = query(inData);
        if (CollUtil.isEmpty(dataList)) {
            throw new BusinessException("导出数据不能为空");
        }
        List<GaiaSafetyHealthRecordExport> dataListExport = dataList.stream().map(outData -> {
            GaiaSafetyHealthRecordExport exportOutData = new GaiaSafetyHealthRecordExport();
            BeanUtil.copyProperties(outData, exportOutData);
            return exportOutData;
        }).collect(Collectors.toList());
        String fileName = null;
        Result uploadResult = null;
        try {
            EasyExcel.write(bos, GaiaSafetyHealthRecordExport.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(dataListExport);
            fileName = "安全卫生检查记录" + "-" + CommonUtil.getyyyyMMdd()+ CommonUtil.getHHmmss() + ".xlsx";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;

    }

    @Override
    public Result getUserList(GaiaSafetyHealthRecord inData) {
        List<GaiaUserData> list = gaiaSafetyHealthRecordMapper.selectUserList(inData);
        return ResultUtil.success(list);
    }
}
