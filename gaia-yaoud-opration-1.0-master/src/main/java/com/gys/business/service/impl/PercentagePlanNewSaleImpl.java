package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.TichengRejectClass;
import com.gys.business.mapper.entity.TichengRejectPro;
import com.gys.business.mapper.entity.TichengSaleplanM;
import com.gys.business.mapper.entity.TichengSaleplanZ;
import com.gys.business.service.ImportPercentageData;
import com.gys.business.service.PercentagePlanNewStrategy;
import com.gys.business.service.data.MessageParams;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.vo.BaseResponse;
import com.gys.feign.OperateService;
import com.gys.util.CommonUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 提成方案-销售提成实现
 *
 * @Description: TODO
 * @author: flynn
 * @date: 2021年11月11日 上午10:09
 */
public class PercentagePlanNewSaleImpl implements PercentagePlanNewStrategy {

    private TichengSaleplanZMapper tichengSaleplanZMapper;

    private TichengSaleplanStoMapper tichengSaleplanStoMapper;


    private TichengSaleplanMMapper tichengSaleplanMMapper;//销售提成-销售提成设置明细mapper

    private TichengRejectClassMapper tichengRejectClassMapper;//销售提成-剔除分类

    private TichengRejectProMapper tichengRejectProMapper;//销售提成-剔除商品设置

    private OperateService operateService;


    public PercentagePlanNewSaleImpl(TichengSaleplanZMapper tichengSaleplanZMapper, TichengSaleplanStoMapper tichengSaleplanStoMapper, TichengSaleplanMMapper tichengSaleplanMMapper, TichengRejectClassMapper tichengRejectClassMapper, TichengRejectProMapper tichengRejectProMapper, OperateService operateService) {
        this.tichengSaleplanZMapper = tichengSaleplanZMapper;
        this.tichengSaleplanStoMapper = tichengSaleplanStoMapper;
        this.tichengSaleplanMMapper = tichengSaleplanMMapper;
        this.tichengRejectClassMapper = tichengRejectClassMapper;
        this.tichengRejectProMapper = tichengRejectProMapper;
        this.operateService = operateService;
    }

    @Override
    public Integer insert(PercentageBasicInData inData) {
        Integer id = 0;
        if (ObjectUtil.isNotEmpty(inData.getId())) {
            //修改
            TichengSaleplanZ tichengPlan = new TichengSaleplanZ();
            tichengPlan.setId(inData.getId());
            tichengPlan.setClient(inData.getClient());
            TichengSaleplanZ tichengSaleplanZ = tichengSaleplanZMapper.selectByPrimaryKey(tichengPlan);
            if (tichengSaleplanZ == null) {
                throw new BusinessException("查无此数据！");
            }
//            tichengPlan.setId(inData.getId());
            tichengSaleplanZ.setClient(inData.getClient());
            tichengSaleplanZ.setPlanName(inData.getPlanName());
            tichengSaleplanZ.setPlanStartDate(inData.getPlanStartDate());
            tichengSaleplanZ.setPlanEndDate(inData.getPlanEndDate());
            tichengSaleplanZ.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengSaleplanZ.setPlanScaleSto(inData.getPlanScaleSto());
            tichengSaleplanZ.setPlanUpdateDatetime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengSaleplanZ.setPlanUpdater(inData.getPlanCreater());
            tichengSaleplanZ.setPlanUpdateId(inData.getPlanCreaterId());
            tichengSaleplanZ.setLastUpdateTime(LocalDateTime.now());

            if (StrUtil.isNotBlank(inData.getPlanReason())) {
                tichengSaleplanZ.setPlanReason(inData.getPlanReason());
                tichengSaleplanZ.setPlanStatus("0");
            }

            tichengSaleplanZMapper.updateByPrimaryKey(tichengSaleplanZ);
            id = tichengPlan.getId();
        } else {
            TichengSaleplanZ tichengPlan = new TichengSaleplanZ();
            String planCode = tichengSaleplanZMapper.selectNextPlanCode(inData.getClient());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanCreater(inData.getPlanCreater());
            tichengPlan.setPlanCreateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanCode(planCode);
            tichengPlan.setPlanStatus("0");
            tichengPlan.setDeleteFlag("0");
            tichengPlan.setPlanType(inData.getPlanType());
            tichengPlan.setPlanCreaterId(inData.getPlanCreaterId());
            tichengPlan.setPlanUpdateDatetime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss() + "");
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            tichengPlan.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengPlan.setPlanScaleSto(inData.getPlanScaleSto());
            tichengPlan.setLastUpdateTime(LocalDateTime.now());
            tichengSaleplanZMapper.insertUseGeneratedKeys(tichengPlan);
            id = tichengPlan.getId();
        }

        //----------------- 处理适用门店-------------------
        //删除之前的配置
        tichengSaleplanStoMapper.deleteStoreByPid(id);
        //新增销售明细
        List<PercentageStoInData> stoList = new ArrayList<>();
        if (inData.getStoreCodes().length > 0) {
            for (String storeCode : inData.getStoreCodes()) {
                PercentageStoInData item = new PercentageStoInData();
                item.setClientId(inData.getClient());
                item.setPid(id);
                item.setStoCode(storeCode);
                stoList.add(item);
            }
        }
        if (stoList.size() > 0 && stoList != null) {
            tichengSaleplanStoMapper.addPlanStores(stoList);
        }
        //----------------- 处理适用门店-------------------
        return id;
    }

    @Override
    public PageInfo<PercentageOutData> list(PercentageInData inData) {
        return null;
    }

    @Override
    public Map<String, Object> tichengDetail(Long planId, String type, GetLoginOutData userInfo) {
        Map<String, Object> result = new HashMap<>();
        //获取主表信息
        if (ObjectUtil.isNotEmpty(planId)) {
            //修改
            TichengSaleplanZ tichengPlan = new TichengSaleplanZ();
            tichengPlan.setId(Integer.parseInt(planId.toString()));
            tichengPlan.setClient(userInfo.getClient());
            TichengSaleplanZ tichengSaleplanZ = tichengSaleplanZMapper.selectByPrimaryKey(tichengPlan);
            if (tichengSaleplanZ == null) {
                throw new BusinessException("查无此数据！");
            }
            //补充适用门店
            List<Map<String, String>> maps = tichengSaleplanStoMapper.selectSaleStoList(planId);
            if (CollectionUtil.isNotEmpty(maps)) {
                List<String> stos = new ArrayList<>();
                maps.forEach(x -> {
                    String stoCode = x.get("stoCode");
                    stos.add(stoCode);
                });
                tichengSaleplanZ.setStoreCodes(stos);
            }
            result.put("mainInfo", tichengSaleplanZ);


            //查询提成设置
            SaleSettingOutData saleSettingOutData = new SaleSettingOutData();
            //处理返回结果
            saleSettingOutData.setPlanId(Integer.parseInt(planId.toString()));
            saleSettingOutData.setPlanAmtWay(tichengSaleplanZ.getPlanAmtWay());
            saleSettingOutData.setPlanRateWay(tichengSaleplanZ.getPlanRateWay());
            saleSettingOutData.setPlanIfNegative(tichengSaleplanZ.getPlanIfNegative());
            saleSettingOutData.setPlanRejectDiscountRate(tichengSaleplanZ.getPlanRejectDiscountRate());
            saleSettingOutData.setPlanRejectDiscountRateSymbol(tichengSaleplanZ.getPlanRejectDiscountRateSymbol());
            saleSettingOutData.setPlanRejectPro(tichengSaleplanZ.getPlanRejectPro());
            saleSettingOutData.setCPlanName("默认");

            //处理销售提成明细返回
            List<PercentageSaleInData> saleInDataList = new ArrayList<>();
            TichengSaleplanM quey = new TichengSaleplanM();
            quey.setPid(Long.parseLong(planId.toString()));
            quey.setDeleteFlag("0");
            List<TichengSaleplanM> tichengSaleplanMDbs = tichengSaleplanMMapper.select(quey);
            if (CollectionUtil.isNotEmpty(tichengSaleplanMDbs)) {
                for (TichengSaleplanM tichengSaleplanM : tichengSaleplanMDbs) {
                    PercentageSaleInData res = new PercentageSaleInData();
                    res.setId(tichengSaleplanM.getId());
                    res.setClientId(tichengSaleplanM.getClient());
                    res.setPid(tichengSaleplanM.getPid().intValue());
                    res.setMinDailySaleAmt(tichengSaleplanM.getMinDailySaleAmt());
                    res.setMaxDailySaleAmt(tichengSaleplanM.getMaxDailySaleAmt());
                    res.setProSaleClass(tichengSaleplanM.getProSaleClass());
                    res.setMinProMll(tichengSaleplanM.getMinProMll() == null ? "0" : tichengSaleplanM.getMinProMll().toPlainString());
                    res.setMaxProMll(tichengSaleplanM.getMaxProMll() == null ? "0" : tichengSaleplanM.getMaxProMll().toPlainString());
                    res.setTichengScale(tichengSaleplanM.getTichengScale() == null ? "0" : tichengSaleplanM.getTichengScale().toPlainString());
                    saleInDataList.add(res);
                }
            }
            saleSettingOutData.setSaleInDataList(saleInDataList);


            //处理删除剔除分类返回结果
            TichengRejectClass queryRejectClass = new TichengRejectClass();
            queryRejectClass.setPid(Long.parseLong(planId.toString()));
            queryRejectClass.setDeleteFlag("0");
            List<TichengRejectClass> tichengRejectClasseDbs = tichengRejectClassMapper.select(queryRejectClass);
            if (CollectionUtil.isNotEmpty(tichengRejectClasseDbs)) {
                if (ObjectUtil.isNotEmpty(tichengRejectClasseDbs) && tichengRejectClasseDbs.size() > 0) {
                    String[][] rejectArr = new String[tichengRejectClasseDbs.size()][3];
                    for (int i = 0; i < tichengRejectClasseDbs.size(); i++) {
                        String[] arr = new String[3];
                        arr[0] = tichengRejectClasseDbs.get(i).getProBigClass();
                        arr[1] = tichengRejectClasseDbs.get(i).getProMidClass();
                        arr[2] = tichengRejectClasseDbs.get(i).getProClass();
                        rejectArr[i] = arr;
                    }
                    saleSettingOutData.setClassArr(rejectArr);
                }
            }

            //处理剔除商品返回结果
            List<PercentageSalePlanRemoveProData> rejectProList = new ArrayList<>();
            TichengRejectPro queryTichengRejectPro = new TichengRejectPro();
            queryTichengRejectPro.setPid(Long.parseLong(planId.toString()));
            queryTichengRejectPro.setDeleteFlag("0");
            List<TichengRejectPro> tichengRejectProDbs = tichengRejectProMapper.select(queryTichengRejectPro);
            if (CollectionUtil.isNotEmpty(tichengRejectProDbs)) {
                for (TichengRejectPro pro : tichengRejectProDbs) {
                    PercentageSalePlanRemoveProData res = new PercentageSalePlanRemoveProData();
                    res.setId(pro.getId());
                    res.setPid(Integer.parseInt(pro.getPid().toString()));
                    res.setProCode(pro.getProCode());
                    res.setProName(pro.getProName());
                    res.setProSpecs(pro.getProSpecs());
                    res.setProFactoryName(pro.getProFactoryName());
                    res.setProCostPrice(pro.getProCostPrice());
                    res.setProPrice(pro.getProPriceNormal());
                    rejectProList.add(res);
                }
            }
            saleSettingOutData.setRejectProList(rejectProList);
            result.put("saleSettingInfo", saleSettingOutData);
        } else {
            throw new BusinessException("查无此数据！");
        }
        return result;
    }

    @Override
    public void approve(Long planId, String planStatus, String type) {
        PercentageInData saleInfo = tichengSaleplanZMapper.selectTichengZSaleDetail(planId.intValue());
        if(saleInfo==null){
            throw new BusinessException("查无此数据");
        }
        PercentageInData inData = new PercentageInData();
        inData.setPlanStartDate(saleInfo.getPlanStartDate());
        inData.setPlanEndDate(saleInfo.getPlanEndDate());
        inData.setPlanType("1");
        inData.setClient(saleInfo.getClient());
        inData.setId(saleInfo.getId());
        List<TichengSaleplanZ> repeatList = tichengSaleplanZMapper.checkDate(inData);
        if (CollectionUtil.isNotEmpty(repeatList)) {
            String planName = repeatList.get(0).getPlanName();
            throw new BusinessException("此销售提成方案与销售提成方案:" + planName + "日期有重叠，不予审核");
        }
        if ("0".equals(saleInfo.getPlanStatus())) {
            tichengSaleplanZMapper.approveSalePlan(planId, planStatus, "审核已通过");
            saleInfo.setPlanStatus(planStatus);
            saleInfo.setPlanReason("审核已通过");
        } else {
            tichengSaleplanZMapper.approveSalePlan(planId, planStatus, "");
            saleInfo.setPlanStatus(planStatus);
        }
        //消息推送到APP
        sendMessage(saleInfo);
    }


    private void sendMessage(PercentageInData planInfo) {
        List<Map<String, String>> result = tichengSaleplanZMapper.selectSaleStoList(planInfo.getId().longValue());
        String[] stoCodeArr = new String[result.size()];
        for (int i = 0; i < result.size(); i++) {
            stoCodeArr[i] = result.get(i).get("stoCode");
        }
        planInfo.setStoreCodes(stoCodeArr);
        List<String> userIds = tichengSaleplanZMapper.selectUserIdList(planInfo);
        MessageParams messageParams = new MessageParams();
        messageParams.setClient(planInfo.getClient());
        messageParams.setUserIdList(userIds);
        messageParams.setContentParmas(new ArrayList<String>() {{
            add(planInfo.getPlanName());
            add(planInfo.getPlanReason());
        }});
        messageParams.setId("MSG00009");
//            log.info("销售提成消息参数:{}", messageParams.toString());
        String message = operateService.sendMessageList(messageParams);
//            log.info("培训推送消息结果:{}", message);
    }

    @Override
    public void deletePlan(Long planId, String type) {
        //删除门店关联
        tichengSaleplanStoMapper.deleteStoreByPid(Integer.parseInt(planId.toString()));
        //删除销售提成设置明细
        tichengSaleplanMMapper.deleteSalePlanByPid(Integer.parseInt(planId.toString()));
        //删除剔除分类
        tichengRejectClassMapper.deleteClassByPid(Integer.parseInt(planId.toString()));
        //删除剔除商品设置
        tichengRejectProMapper.deleteRejectProByPid(Integer.parseInt(planId.toString()));
        //删除主表
        tichengSaleplanZMapper.deleteSalePlan(planId);
    }

    @Override
    public PercentageProInData selectProductByClient(String client, String proCode) {
        return null;
    }

    @Override
    public List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList) {
        return null;
    }

    @Override
    public List<Map<String, String>> selectStoList(Long planId, Long type) {
        List<Map<String, String>> result = new ArrayList<>();
        result = tichengSaleplanStoMapper.selectSaleStoList(planId);
        return result;
    }

    @Override
    public Map<String, Object> tichengDetailCopy(Long planId, Long type) {
        Map<String, Object> result = new HashMap<>();
        //获取主表信息
        if (ObjectUtil.isNotEmpty(planId)) {
            //修改
            TichengSaleplanZ tichengPlan = new TichengSaleplanZ();
            tichengPlan.setId(Integer.parseInt(planId.toString()));
            TichengSaleplanZ tichengSaleplanZ = tichengSaleplanZMapper.selectByPrimaryKey(tichengPlan);
            if (tichengSaleplanZ == null) {
                throw new BusinessException("查无此数据！");
            }
            tichengSaleplanZ.setId(null);
            tichengSaleplanZ.setPlanName(tichengSaleplanZ.getPlanName() + "-副本");
//            result.put("mainInfo", tichengSaleplanZ);

            //补充适用门店
            List<Map<String, String>> maps = tichengSaleplanStoMapper.selectSaleStoList(planId);
            if (CollectionUtil.isNotEmpty(maps)) {
                List<String> stos = new ArrayList<>();
                maps.forEach(x -> {
                    String stoCode = x.get("stoCode");
                    stos.add(stoCode);
                });
                tichengSaleplanZ.setStoreCodes(stos);
            }
            result.put("mainInfo", tichengSaleplanZ);

            //查询提成设置
            SaleSettingOutData saleSettingOutData = new SaleSettingOutData();
            //处理返回结果
            saleSettingOutData.setPlanAmtWay(tichengSaleplanZ.getPlanAmtWay());
            saleSettingOutData.setPlanRateWay(tichengSaleplanZ.getPlanRateWay());
            saleSettingOutData.setPlanIfNegative(tichengSaleplanZ.getPlanIfNegative());
            saleSettingOutData.setPlanRejectDiscountRate(tichengSaleplanZ.getPlanRejectDiscountRate());
            saleSettingOutData.setPlanRejectDiscountRateSymbol(tichengSaleplanZ.getPlanRejectDiscountRateSymbol());
            saleSettingOutData.setPlanRejectPro(tichengSaleplanZ.getPlanRejectPro());
            saleSettingOutData.setCPlanName("默认");

            //处理销售提成明细返回
            List<PercentageSaleInData> saleInDataList = new ArrayList<>();
            TichengSaleplanM quey = new TichengSaleplanM();
            quey.setPid(Long.parseLong(planId.toString()));
            quey.setDeleteFlag("0");
            List<TichengSaleplanM> tichengSaleplanMDbs = tichengSaleplanMMapper.select(quey);
            if (CollectionUtil.isNotEmpty(tichengSaleplanMDbs)) {
                for (TichengSaleplanM tichengSaleplanM : tichengSaleplanMDbs) {
                    PercentageSaleInData res = new PercentageSaleInData();
//                    res.setId(tichengSaleplanM.getId());
                    res.setClientId(tichengSaleplanM.getClient());
//                    res.setPid(tichengSaleplanM.getPid().intValue());
                    res.setMinDailySaleAmt(tichengSaleplanM.getMinDailySaleAmt());
                    res.setMaxDailySaleAmt(tichengSaleplanM.getMaxDailySaleAmt());
                    res.setProSaleClass(tichengSaleplanM.getProSaleClass());
                    res.setMinProMll(tichengSaleplanM.getMinProMll() == null ? "0" : tichengSaleplanM.getMinProMll().toPlainString());
                    res.setMaxProMll(tichengSaleplanM.getMaxProMll() == null ? "0" : tichengSaleplanM.getMaxProMll().toPlainString());
                    res.setTichengScale(tichengSaleplanM.getTichengScale() == null ? "0" : tichengSaleplanM.getTichengScale().toPlainString());
                    saleInDataList.add(res);
                }
            }
            saleSettingOutData.setSaleInDataList(saleInDataList);


            //处理删除剔除分类返回结果
            TichengRejectClass queryRejectClass = new TichengRejectClass();
            queryRejectClass.setPid(Long.parseLong(planId.toString()));
            queryRejectClass.setDeleteFlag("0");
            List<TichengRejectClass> tichengRejectClasseDbs = tichengRejectClassMapper.select(queryRejectClass);
            if (CollectionUtil.isNotEmpty(tichengRejectClasseDbs)) {
                if (ObjectUtil.isNotEmpty(tichengRejectClasseDbs) && tichengRejectClasseDbs.size() > 0) {
                    String[][] rejectArr = new String[tichengRejectClasseDbs.size()][3];
                    for (int i = 0; i < tichengRejectClasseDbs.size(); i++) {
                        String[] arr = new String[3];
                        arr[0] = tichengRejectClasseDbs.get(i).getProBigClass();
                        arr[1] = tichengRejectClasseDbs.get(i).getProMidClass();
                        arr[2] = tichengRejectClasseDbs.get(i).getProClass();
                        rejectArr[i] = arr;
                    }
                    saleSettingOutData.setClassArr(rejectArr);
                }
            }

            //处理剔除商品返回结果
            List<PercentageSalePlanRemoveProData> rejectProList = new ArrayList<>();
            TichengRejectPro queryTichengRejectPro = new TichengRejectPro();
            queryTichengRejectPro.setPid(Long.parseLong(planId.toString()));
            queryTichengRejectPro.setDeleteFlag("0");
            List<TichengRejectPro> tichengRejectProDbs = tichengRejectProMapper.select(queryTichengRejectPro);
            if (CollectionUtil.isNotEmpty(tichengRejectProDbs)) {
                for (TichengRejectPro pro : tichengRejectProDbs) {
                    PercentageSalePlanRemoveProData res = new PercentageSalePlanRemoveProData();
//                    res.setId(pro.getId());
//                    res.setPid(Integer.parseInt(pro.getPid().toString()));
                    res.setProCode(pro.getProCode());
                    res.setProName(pro.getProName());
                    res.setProSpecs(pro.getProSpecs());
                    res.setProFactoryName(pro.getProFactoryName());
                    res.setProCostPrice(pro.getProCostPrice());
                    res.setProPrice(pro.getProPriceNormal());
                    rejectProList.add(res);
                }
            }
            saleSettingOutData.setRejectProList(rejectProList);
            result.put("saleSettingInfo", saleSettingOutData);
        }
        return result;
    }

    @Override
    public void stopPlan(Long planId, String planType, String stopType, String stopReason) {
        PercentageInData saleInfo = tichengSaleplanZMapper.selectTichengZSaleDetail(planId.intValue());
        if ("1".equals(saleInfo.getPlanStatus())) {
            if ("1".equals(stopType)) {//立即停用
                tichengSaleplanZMapper.stopSalePlan(planId, "2", CommonUtil.getyyyyMMdd(), stopReason);
                //消息推送至APP
                List<Map<String, String>> result = tichengSaleplanZMapper.selectSaleStoList(saleInfo.getId().longValue());
                String[] stoCodeArr = new String[result.size()];
                for (int i = 0; i < result.size(); i++) {
                    stoCodeArr[i] = result.get(i).get("stoCode");
                }
                saleInfo.setStoreCodes(stoCodeArr);
                List<String> userIds = tichengSaleplanZMapper.selectUserIdList(saleInfo);
                MessageParams messageParams = new MessageParams();
                messageParams.setClient(saleInfo.getClient());
                messageParams.setUserIdList(userIds);
                messageParams.setContentParmas(new ArrayList<String>() {{
                    add(saleInfo.getPlanName());
                    add(stopReason);
                }});
                messageParams.setId("MSG00011");
//                log.info("销售提成消息参数:{}", messageParams.toString());
                String message = operateService.sendMessageList(messageParams);
//                log.info("销售提成消息结果:{}", message);
                BaseResponse resultObj = JSONObject.parseObject(message, BaseResponse.class);
                int code = resultObj.getCode();
                if (code != 0) {
                    throw new BusinessException("消息接口服务异常");
                }
            } else {
                tichengSaleplanZMapper.stopSalePlan(planId, "1", saleInfo.getPlanEndDate(), stopReason);
            }
        } else {
            throw new BusinessException("该方案尚未审核，无法停用");
        }
        //消息推送到APP
        sendMessage(saleInfo);
    }

    @Override
    public void timerStopPlan() {

    }
}

