package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSoBatchAccountMapper;
import com.gys.business.mapper.entity.GaiaSoBatchAccount;
import com.gys.business.service.GaiaSoBatchAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/23 9:24
 */
@Service
@Slf4j
public class GaiaSoBatchAccountServiceImpl implements GaiaSoBatchAccountService {

    @Autowired
    private GaiaSoBatchAccountMapper accountMapper;

    /**
     * 查询所有开启状态的服务
     */
    @Override
    public List<GaiaSoBatchAccount> findAllOpen() {
        return accountMapper.findAllOpen();
    }

    @Override
    public GaiaSoBatchAccount findByClient(String client) {
        return accountMapper.findByClient(client);
    }
}
