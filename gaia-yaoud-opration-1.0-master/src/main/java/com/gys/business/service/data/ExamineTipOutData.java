//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ExamineTipOutData {
    private String gsehDate;
    private String gsehType;
    private String gsehVoucherId;
    private String gsahVoucherId;
    private String gsehTotalAmt;
    private String gsehTotalQty;
    private String clientId;
    private String brId;
    private String gschVoucherId;

    public ExamineTipOutData() {
    }

    public String getGsehDate() {
        return this.gsehDate;
    }

    public String getGsehType() {
        return this.gsehType;
    }

    public String getGsehVoucherId() {
        return this.gsehVoucherId;
    }

    public String getGsahVoucherId() {
        return this.gsahVoucherId;
    }

    public String getGsehTotalAmt() {
        return this.gsehTotalAmt;
    }

    public String getGsehTotalQty() {
        return this.gsehTotalQty;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public String getGschVoucherId() {
        return this.gschVoucherId;
    }

    public void setGsehDate(final String gsehDate) {
        this.gsehDate = gsehDate;
    }

    public void setGsehType(final String gsehType) {
        this.gsehType = gsehType;
    }

    public void setGsehVoucherId(final String gsehVoucherId) {
        this.gsehVoucherId = gsehVoucherId;
    }

    public void setGsahVoucherId(final String gsahVoucherId) {
        this.gsahVoucherId = gsahVoucherId;
    }

    public void setGsehTotalAmt(final String gsehTotalAmt) {
        this.gsehTotalAmt = gsehTotalAmt;
    }

    public void setGsehTotalQty(final String gsehTotalQty) {
        this.gsehTotalQty = gsehTotalQty;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public void setGschVoucherId(final String gschVoucherId) {
        this.gschVoucherId = gschVoucherId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ExamineTipOutData)) {
            return false;
        } else {
            ExamineTipOutData other = (ExamineTipOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$gsehDate = this.getGsehDate();
                    Object other$gsehDate = other.getGsehDate();
                    if (this$gsehDate == null) {
                        if (other$gsehDate == null) {
                            break label119;
                        }
                    } else if (this$gsehDate.equals(other$gsehDate)) {
                        break label119;
                    }

                    return false;
                }

                Object this$gsehType = this.getGsehType();
                Object other$gsehType = other.getGsehType();
                if (this$gsehType == null) {
                    if (other$gsehType != null) {
                        return false;
                    }
                } else if (!this$gsehType.equals(other$gsehType)) {
                    return false;
                }

                label105: {
                    Object this$gsehVoucherId = this.getGsehVoucherId();
                    Object other$gsehVoucherId = other.getGsehVoucherId();
                    if (this$gsehVoucherId == null) {
                        if (other$gsehVoucherId == null) {
                            break label105;
                        }
                    } else if (this$gsehVoucherId.equals(other$gsehVoucherId)) {
                        break label105;
                    }

                    return false;
                }

                Object this$gsahVoucherId = this.getGsahVoucherId();
                Object other$gsahVoucherId = other.getGsahVoucherId();
                if (this$gsahVoucherId == null) {
                    if (other$gsahVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsahVoucherId.equals(other$gsahVoucherId)) {
                    return false;
                }

                label91: {
                    Object this$gsehTotalAmt = this.getGsehTotalAmt();
                    Object other$gsehTotalAmt = other.getGsehTotalAmt();
                    if (this$gsehTotalAmt == null) {
                        if (other$gsehTotalAmt == null) {
                            break label91;
                        }
                    } else if (this$gsehTotalAmt.equals(other$gsehTotalAmt)) {
                        break label91;
                    }

                    return false;
                }

                Object this$gsehTotalQty = this.getGsehTotalQty();
                Object other$gsehTotalQty = other.getGsehTotalQty();
                if (this$gsehTotalQty == null) {
                    if (other$gsehTotalQty != null) {
                        return false;
                    }
                } else if (!this$gsehTotalQty.equals(other$gsehTotalQty)) {
                    return false;
                }

                label77: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label77;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$brId = this.getBrId();
                    Object other$brId = other.getBrId();
                    if (this$brId == null) {
                        if (other$brId == null) {
                            break label70;
                        }
                    } else if (this$brId.equals(other$brId)) {
                        break label70;
                    }

                    return false;
                }

                Object this$gschVoucherId = this.getGschVoucherId();
                Object other$gschVoucherId = other.getGschVoucherId();
                if (this$gschVoucherId == null) {
                    if (other$gschVoucherId != null) {
                        return false;
                    }
                } else if (!this$gschVoucherId.equals(other$gschVoucherId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ExamineTipOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gsehDate = this.getGsehDate();
        result = result * 59 + ($gsehDate == null ? 43 : $gsehDate.hashCode());
        Object $gsehType = this.getGsehType();
        result = result * 59 + ($gsehType == null ? 43 : $gsehType.hashCode());
        Object $gsehVoucherId = this.getGsehVoucherId();
        result = result * 59 + ($gsehVoucherId == null ? 43 : $gsehVoucherId.hashCode());
        Object $gsahVoucherId = this.getGsahVoucherId();
        result = result * 59 + ($gsahVoucherId == null ? 43 : $gsahVoucherId.hashCode());
        Object $gsehTotalAmt = this.getGsehTotalAmt();
        result = result * 59 + ($gsehTotalAmt == null ? 43 : $gsehTotalAmt.hashCode());
        Object $gsehTotalQty = this.getGsehTotalQty();
        result = result * 59 + ($gsehTotalQty == null ? 43 : $gsehTotalQty.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        Object $gschVoucherId = this.getGschVoucherId();
        result = result * 59 + ($gschVoucherId == null ? 43 : $gschVoucherId.hashCode());
        return result;
    }

    public String toString() {
        return "ExamineTipOutData(gsehDate=" + this.getGsehDate() + ", gsehType=" + this.getGsehType() + ", gsehVoucherId=" + this.getGsehVoucherId() + ", gsahVoucherId=" + this.getGsahVoucherId() + ", gsehTotalAmt=" + this.getGsehTotalAmt() + ", gsehTotalQty=" + this.getGsehTotalQty() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ", gschVoucherId=" + this.getGschVoucherId() + ")";
    }
}
