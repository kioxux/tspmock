package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OutOfStockOutData implements Serializable {

    private static final long serialVersionUID = -3781934088499015325L;

    @ApiModelProperty(value = "缺断货品项个数")
    private BigDecimal outOfStockCount;

    @ApiModelProperty(value = "月均销售额")
    private BigDecimal avgSaleAmt;

    @ApiModelProperty(value = "月均毛利额")
    private BigDecimal avgProfitAmt;

    @ApiModelProperty(value = "交易次数")
    private BigDecimal avgTransactionCount;


}
