package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UpdateParaByStoreListInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "参数ID")
    private String paraId;

    @ApiModelProperty(value = "参数值")
    private String para;

    @ApiModelProperty(value = "更新日期")
    private String updateDate;

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty(value="是否勾选 0-取消 1-勾选")
    private String checkFlag;

    @ApiModelProperty(value="推送标志 null-所有 2-直营店 3-加盟店 4-其他  选择所有时此字段传null")
    private String pushFlag;

    @ApiModelProperty(value = "门店编号列表")
    private List<UpdateParaByStoreIdList> brIdList;

}
