package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.GaiaSdIntegralRateSetService;
import com.gys.business.service.ProductSortService;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.request.*;
import com.gys.common.response.InteProductSettingResponse;
import com.gys.common.response.SelectInteProductListResponse;
import com.gys.common.response.SelectProductStResponse;
import com.gys.common.response.SelectStoreListResponse;
import com.gys.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * 积分商品设置表 服务实现类
 * </p>
 *
 * @author wangyifan
 * @since 2021-07-28
 */
@Service
public class GaiaSdIntegralRateSetServiceImpl implements GaiaSdIntegralRateSetService {

    @Autowired
    private GaiaSdIntegralRateSetMapper gaiaSdIntegralRateSetMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Autowired
    private GaiaSdIntegralRateStoMapper gaiaSdIntegralRateStoMapper;

    @Autowired
    private ProductSortService productSortService;

    @Autowired
    private GaiaSdProductPriceMapper productPriceMapper;

    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;

    /**
     * 初始化数据
     *
     * @param client 加盟商
     * @return
     */
    @Override
    public List<SelectStoreListResponse> setClientId(String client, String stoName) {

        Example example = new Example(GaiaStoreData.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("client", client);
        criteria.andEqualTo("stoStatus", 0);
        if (StrUtil.isNotBlank(stoName)) {
            criteria.andLike("stoName", '%' + stoName + '%');
        }
        List<GaiaStoreData> selectStoreList = gaiaStoreDataMapper.selectByExample(example);
        List<SelectStoreListResponse> storeListResponses = JSONObject.parseArray(JSONObject.toJSONString(selectStoreList), SelectStoreListResponse.class);
        return storeListResponses;
    }

    /**
     * 新增商品设置
     *
     * @param req
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InteProductSettingResponse insertProductSetting(InsertProductSettingReq req) {
        InteProductSettingResponse resp = new InteProductSettingResponse();
        //获取集合数据
        List<InsertProductSettingReq.InsertProductSetting> reqList = reqList(req);
        //副表数据
        List<GaiaSdIntegralRateSto> listSto = new ArrayList<>();
        if (CollUtil.isNotEmpty(reqList)) {
            reqList.forEach(rs -> {
                rs.setClient(req.getClient());
                if (rs.getInTimeArr() != null && rs.getInTimeArr().length > 1) {
                    String[] inTimeArr = rs.getInTimeArr();
                    rs.setBeginDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(inTimeArr[0]), "YYYYMMdd"));
                    rs.setEndDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(inTimeArr[1]), "YYYYMMdd"));
                }
                //是所有门店
                if (rs.getIfAllsto().equals("0")) {
                    //不是所有门店
                    if (ArrayUtil.isNotEmpty(rs.getSites())) {
                        Stream.of(rs.getSites()).forEach(es -> {
                            GaiaSdIntegralRateSto sto = new GaiaSdIntegralRateSto();
                            sto.setClient(req.getClient());
                            sto.setBrId(es);
                            sto.setProId(rs.getProId());
                            listSto.add(sto);
                        });
                    }
                }
                if (ArrayUtil.isNotEmpty(rs.getWeekFrequency())) {
                    rs.setWeekFrequencyStr(Stream.of(rs.getWeekFrequency()).filter(s -> StrUtil.isNotBlank(s)).collect(Collectors.joining("/")));
                }
                if (ArrayUtil.isNotEmpty(rs.getDateFrequency())) {
                    rs.setDateFrequencyStr(Stream.of(rs.getDateFrequency()).filter(s -> StrUtil.isNotBlank(s)).collect(Collectors.joining("/")));
                }

            });
            Example example = new Example(GaiaSdIntegralRateSet.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("client", req.getClient());
            criteria.andIn("proId", reqList.stream().map(s -> s.getProId()).collect(Collectors.toList()));
            List<GaiaSdIntegralRateSet> rateSets = this.gaiaSdIntegralRateSetMapper.selectByExample(example);
            if (CollUtil.isNotEmpty(rateSets)) {
                throw new BusinessException("该商品已有设置记录，请查询后重新修改!" + StringUtils.join(rateSets.stream().map(prId -> prId.getProId()).collect(Collectors.toList()), ","));
            }
            //插入会员商品信息
            if (CollUtil.isNotEmpty(reqList)) {
                gaiaSdIntegralRateSetMapper.insertProductSetting(reqList);
            }
            if (CollUtil.isNotEmpty(listSto)) {
                gaiaSdIntegralRateStoMapper.insertList(listSto);
            }
        }
        resp.setResult("新增成功");
        return resp;
    }

    /**
     * 新增校验逻辑
     *
     * @param req
     * @return
     */
    private List<InsertProductSettingReq.InsertProductSetting> reqList(InsertProductSettingReq req) {
        List<InsertProductSettingReq.InsertProductSetting> settingReqList = req.getSettingReqList();
        if (CollUtil.isNotEmpty(settingReqList)) {
            settingReqList.forEach(rs -> {
                if (StrUtil.isBlank(rs.getIfInte())) {
                    throw new BusinessException("是否积分必须选择一个");
                }
                if (StrUtil.isBlank(rs.getIfAlltime())) {
                    throw new BusinessException("是否一直生效必须选择一个");
                }
                //验证是否积分，一直生效等选择否不允许输入
                if (Objects.equals(rs.getIfInte(), "0")) {
                    if (StrUtil.isNotBlank(rs.getInteRate())) {
                        throw new BusinessException("是否积分为否情况下,积分倍率不可输入！");
                    }
                } else if (Objects.equals(rs.getIfInte(), "1")) {
                    if (StrUtil.isBlank(rs.getInteRate())) {
                        throw new BusinessException("积分倍率不可为空！");
                    } else {
                        if (new BigDecimal("0.00").compareTo(new BigDecimal(rs.getInteRate())) == 0) {
                            throw new BusinessException("积分倍率不可等于0!");
                        }
                        if (!(rs.getInteRate().matches("\\d+\\.\\d+$|-\\d+\\.\\d+$"))) {
                            throw new BusinessException("积分倍率必须是小数而且保留一位小数");
                        }
                        boolean matches = rs.getInteRate().matches("^[+]?([0-9]+(.[0-9]{1})?)$");
                        if (!matches) {
                            throw new BusinessException("积分倍率小数位必须是一位");
                        }
                    }
                }
                //是一直生效
                if (Objects.equals(rs.getIfAlltime(), "1")) {
                    if (StrUtil.isNotBlank(rs.getBeginDate()) || StrUtil.isNotBlank(rs.getEndDate())) {
                        throw new BusinessException("是一直生效情况下开始日期和结束日期不能选择!");
                    }
                   /* //是否星期频率
                    if (ArrayUtil.isNotEmpty(rs.getWeekFrequency())) {
                        throw new BusinessException("是一直生效情况下星期频率不能输入!");
                    }
                    if (ArrayUtil.isNotEmpty(rs.getDateFrequency())) {
                        throw new BusinessException("是一直生效情况下日期频率不能输入!");
                    }*/
                } else if (Objects.equals(rs.getIfAlltime(), "0")) {
                    if (StrUtil.isBlank(rs.getBeginDate()) || StrUtil.isBlank(rs.getEndDate())) {
                        throw new BusinessException("不是一直生效情况下开始日期和结束日期不能为空!");
                    }
                   /* //是否星期频率
                    if (ArrayUtil.isEmpty(rs.getWeekFrequency())) {
                        throw new BusinessException("不是一直生效情况下星期频率必须选择!");
                    }
                    //是否日期频率
                    if (ArrayUtil.isEmpty(rs.getDateFrequency())) {
                        throw new BusinessException("不是一直生效情况下日期频率必须选择!");
                    }*/
                }

            });
        }
        return settingReqList;
    }

    /**
     * 查询
     *
     * @param req
     * @return
     */
    @Override
    public PageInfo<SelectInteProductListResponse> selectInteProductPageList(SelectInteProductListReq req) {
        PageInfo<SelectInteProductListResponse> pageInfo = new PageInfo<>();
        if (Objects.isNull(req.getPageNum()) || Objects.isNull(req.getPageSize())) {
            throw new BusinessException("分页参数不能为空");
        }
        if (ArrayUtil.isNotEmpty(req.getInTimeArr()) && req.getInTimeArr().length > 1) {
            String[] inTimeArr = req.getInTimeArr();
            req.setBeginDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(inTimeArr[0]), "YYYYMMdd"));
            req.setEndDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(inTimeArr[1]), "YYYYMMdd"));
        } else {
            if (StrUtil.isNotBlank(req.getBeginDate())) {
                req.setBeginDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(req.getBeginDate()), "YYYYMMdd"));
            }
            if (StrUtil.isNotBlank(req.getEndDate())) {
                req.setEndDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(req.getEndDate()), "YYYYMMdd"));
            }
        }
        if (StrUtil.isNotBlank(req.getInteRate())) {
            if (new BigDecimal("0.00").compareTo(new BigDecimal(req.getInteRate())) == 0) {
                throw new BusinessException("积分倍率不能为0");
            }
        }
        //查询
        PageHelper.startPage(req.getPageNum(), req.getPageSize());
        List<SelectInteProductListResponse> rateSetList = gaiaSdIntegralRateSetMapper.selectProductPageList(req);
        if (CollUtil.isNotEmpty(rateSetList)) {
            //获取加盟商
            Example example = new Example(GaiaSdProductPrice.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("clientId", req.getClient());
            criteria.andIn("gsppProId", rateSetList.stream().map(prId -> prId.getProId()).collect(Collectors.toList()));
            List<GaiaSdProductPrice> prices = productPriceMapper.selectByExample(example);

            List<SelectInteProductListResponse> collect = rateSetList.stream().filter(rate -> !(Objects.equals(rate.getIfAllsto(), "1"))).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(collect)) {
                List<GaiaSdIntegralRateSto> list = productPriceMapper.selectList(req.getClient(), collect.stream().map(s -> s.getProId()).distinct().collect(Collectors.toList()));
                if (CollUtil.isNotEmpty(list)) {
                    for (SelectInteProductListResponse response : rateSetList) {
                        List<InsertProductStortReq> reqList = new ArrayList<>();
                        list.forEach(li -> {
                            if (Objects.equals(response.getClient(), li.getClient()) && Objects.equals(response.getProId(), li.getProId())) {
                                InsertProductStortReq req1 = new InsertProductStortReq();
                                req1.setProId(response.getProId());
                                req1.setBrId(li.getBrId());
                                req1.setClient(req.getClient());
                                reqList.add(req1);
                            }
                        });
                        response.setProductStortList(reqList);
                    }
                }
            }
            //获取列表中是否有设置是所有门店
            List<SelectStoreListResponse> stos = setClientId(req.getClient(), null);
            for (SelectInteProductListResponse list : rateSetList) {
                if (CollUtil.isNotEmpty(stos)) {
                    //是所有门店单独获取
                    if (Objects.equals(list.getIfAllsto(), "1")) {
                        list.setProductStortList(stos.stream().map(sto -> {
                            InsertProductStortReq inser = new InsertProductStortReq();
                            inser.setClient(req.getClient());
                            inser.setBrId(sto.getStoCode());
                            inser.setStoName(sto.getStoName());
                            inser.setProId(list.getProId());
                            return inser;
                        }).collect(Collectors.toList()));

                    } else {
                        if (CollUtil.isNotEmpty(list.getProductStortList())) {
                            for (InsertProductStortReq stortReq : list.getProductStortList()) {
                                stos.forEach(sto -> {
                                    if (Objects.equals(stortReq.getBrId(), sto.getStoCode())) {
                                        stortReq.setStoName(sto.getStoName());
                                    }
                                });
                            }
                        }
                    }
                }
                //获取零售价
                if (CollUtil.isNotEmpty(prices)) {
                    prices.stream().filter(price -> price.getGsppProId().equals(list.getProId())).findFirst().ifPresent(pr -> list.setPrice(Convert.toStr(pr.getGsppPriceNormal())));
                }
                list.setSites(list.getProductStortList().stream().map(stor -> stor.getBrId()).collect(Collectors.toList()));
            }
        }
        pageInfo = new PageInfo<>(rateSetList);
        return pageInfo;
    }

    /**
     * 删除配置
     *
     * @param id     不计分主键
     * @param client 加盟商
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InteProductSettingResponse deleteInteProductSetting(String id, String client) {
        GaiaSdIntegralRateSet set = new GaiaSdIntegralRateSet();
        set.setId(Convert.toLong(id));
        set.setClient(client);
        GaiaSdIntegralRateSet rateSet = this.gaiaSdIntegralRateSetMapper.selectOne(set);
        if (Objects.isNull(rateSet)) {
            throw new BusinessException("ID不存在！");
        }
        InteProductSettingResponse resp = new InteProductSettingResponse();
        Example example = new Example(GaiaSdIntegralRateSet.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("proId", rateSet.getProId()).andEqualTo("client", client);
        this.gaiaSdIntegralRateSetMapper.deleteByExample(example);
        GaiaSdIntegralRateSto sto = new GaiaSdIntegralRateSto();
        sto.setProId(rateSet.getProId());
        sto.setClient(client);
        this.gaiaSdIntegralRateStoMapper.delete(sto);
        resp.setResult("删除成功");
        return resp;
    }

    /**
     * 修改会员积分商品设置
     *
     * @param req 请求参数
     * @param
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InteProductSettingResponse updateInteProductSetting(UpdateInteProductSettingReq req) {
        InteProductSettingResponse resp = new InteProductSettingResponse();
        GaiaSdIntegralRateSet rateSt = new GaiaSdIntegralRateSet();
        rateSt.setId(Convert.toLong(req.getId()));
        GaiaSdIntegralRateSet sto = this.gaiaSdIntegralRateSetMapper.selectOne(rateSt);
        if (Objects.isNull(sto)) {
            throw new BusinessException("ID不存在！");
        }
        if (!sto.getProId().equals(req.getProId()) && sto.getClient().equals(req.getClient())) {
            throw new BusinessException("商品编码不能修改！");
        }
        if (StrUtil.isBlank(req.getIfInte())) {
            throw new BusinessException("是否积分必须选择一个");
        }
        if (StrUtil.isBlank(req.getIfAlltime())) {
            throw new BusinessException("是否一直生效必须选择一个");
        }
        if (StrUtil.isNotBlank(req.getIfInte())) {
            //验证是否积分，一直生效等选择否不允许输入
            if (Objects.equals(req.getIfInte(), "0")) {
                if (StrUtil.isNotBlank(req.getInteRate())) {
                    throw new BusinessException("是否积分为否情况下,积分倍率不可输入！");
                }
            } else if (Objects.equals(req.getIfInte(), "1")) {
                if (StrUtil.isBlank(req.getInteRate())) {
                    throw new BusinessException("积分倍率不可为空！");
                }
                if (new BigDecimal("0.00").compareTo(new BigDecimal(req.getInteRate())) == 0) {
                    throw new BusinessException("积分倍率不能为0");
                }

            }
        }
        //是一直生效
        if (Objects.equals(req.getIfAlltime(), "1")) {
            if (StrUtil.isNotBlank(req.getBeginDate()) || StrUtil.isNotBlank(req.getEndDate())) {
                throw new BusinessException("是一直生效情况下开始日期和结束日期不能选择!");
            }
        } else if (Objects.equals(req.getIfAlltime(), "0")) {
            if (StrUtil.isBlank(req.getBeginDate()) || StrUtil.isBlank(req.getEndDate())) {
                throw new BusinessException("不是一直生效情况下开始日期和结束日期不能为空!");
            }
        }
        //修改会员积分设置表
        GaiaSdIntegralRateSet integralRateSet = new GaiaSdIntegralRateSet();
        BeanUtils.copyProperties(req, integralRateSet);
        integralRateSet.setId(Convert.toLong(req.getId()));
        integralRateSet.setProId(req.getProId());
        if (ArrayUtil.isNotEmpty(req.getWeekFrequency())) {
            integralRateSet.setWeekFrequency(Stream.of(req.getWeekFrequency()).filter(s -> StrUtil.isNotBlank(s)).collect(Collectors.joining("/")));
        }
        if (ArrayUtil.isNotEmpty(req.getDateFrequency())) {
            integralRateSet.setDateFrequency(Stream.of(req.getDateFrequency()).filter(s -> StrUtil.isNotBlank(s)).collect(Collectors.joining("/")));
        }

        if (ArrayUtil.isNotEmpty(req.getInTimeArr()) && req.getInTimeArr().length > 1) {
            String[] inTimeArr = req.getInTimeArr();
            integralRateSet.setBeginDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(inTimeArr[0]), "YYYYMMdd"));
            integralRateSet.setEndDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(inTimeArr[1]), "YYYYMMdd"));
        } else {
            if (StrUtil.isNotBlank(req.getBeginDate())) {
                integralRateSet.setBeginDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(req.getBeginDate()), "YYYYMMdd"));
            }
            if (StrUtil.isNotBlank(req.getEndDate())) {
                integralRateSet.setEndDate(DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(req.getEndDate()), "YYYYMMdd"));
            }
        }
        this.gaiaSdIntegralRateSetMapper.updateByPrimaryKey(integralRateSet);
        GaiaSdIntegralRateSto rateSto = new GaiaSdIntegralRateSto();
        rateSto.setProId(sto.getProId());
        rateSto.setClient(req.getClient());
        this.gaiaSdIntegralRateStoMapper.delete(rateSto);
        List<GaiaSdIntegralRateSto> stoList = new ArrayList<>();
        if (StrUtil.isNotBlank(req.getIfAllsto())) {
            if (req.getIfAllsto().equals("0")) {
                if (ArrayUtil.isNotEmpty(req.getSites())) {
                    Stream.of(req.getSites()).forEach(s -> {
                        GaiaSdIntegralRateSto stoo = new GaiaSdIntegralRateSto();
                        stoo.setProId(req.getProId());
                        stoo.setBrId(s);
                        stoo.setClient(req.getClient());
                        stoList.add(stoo);
                    });
                }
            }
        }
        if (CollUtil.isNotEmpty(stoList)) {
            gaiaSdIntegralRateStoMapper.insertList(stoList);
        }
        resp.setResult("修改成功");
        return resp;
    }

    /**
     * 会员商品积分数据回显
     *
     * @param id
     * @param client
     * @return
     */
    @Override
    public SelectInteProductListResponse selectInteProductSetting(String id, String client) {
        SelectInteProductListResponse response = new SelectInteProductListResponse();
        GaiaSdIntegralRateSet rateSet = new GaiaSdIntegralRateSet();
        rateSet.setId(Convert.toLong(id));
        rateSet.setClient(client);
        GaiaSdIntegralRateSet integralRateSet = gaiaSdIntegralRateSetMapper.selectOne(rateSet);
        if (Objects.nonNull(integralRateSet)) {
            BeanUtils.copyProperties(integralRateSet, response);
            response.setId(id);
            response.setIfAlltimeName(integralRateSet.getIfAlltime().equals("0") ? "否" : "是");
            response.setIfInteName(integralRateSet.getIfInte().equals("0") ? "否" : "是");
            response.setIfAllstoName(integralRateSet.getIfAllsto().equals("0") ? "否" : "是");
            response.setWeekFrequency(integralRateSet.getWeekFrequency().replace('/', ','));
            response.setDateFrequency(integralRateSet.getDateFrequency().replace('/', ','));
            String startDate = DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(integralRateSet.getBeginDate()), "YYYY.MM.dd");
            String endDate = DateUtil.dateToString(cn.hutool.core.date.DateUtil.parse(integralRateSet.getEndDate()), "YYYY.MM.dd");
            response.setTimeFormat(startDate + "-" + endDate);
            if (Objects.equals(integralRateSet.getIfAllsto(), "1")) {
                List<SelectStoreListResponse> storeList = setClientId(client, null);
                if (CollUtil.isNotEmpty(storeList)) {
                    response.setProductStortList(storeList.stream().map(store -> {
                        InsertProductStortReq req = new InsertProductStortReq();
                        req.setStoName(store.getStoName());
                        req.setBrId(store.getStoCode());
                        return req;
                    }).collect(Collectors.toList()));
                }
            } else {
                List<InsertProductStortReq> stortReqs = gaiaSdIntegralRateStoMapper.selectStorts(integralRateSet.getProId(), client);
                response.setProductStortList(stortReqs);
                if (CollUtil.isNotEmpty(stortReqs)) {
                    response.setStoreList(stortReqs.stream().map(store -> store.getBrId()).collect(Collectors.toList()));
                }
            }

        }
        return response;
    }

    /**
     * 校验商品编码是否已存在
     *
     * @param req
     * @return
     */
    @Override
    public InteProductSettingResponse selectProductCode(SelectCheckIfInDbReq req) {
        InteProductSettingResponse response = new InteProductSettingResponse();
        if (CollUtil.isEmpty(req.getSettingReqList())) {
            throw new BusinessException("请先选择药品！");
        }
        List<String> proIds = this.gaiaSdIntegralRateSetMapper.selectProIds(req);
        if (CollUtil.isNotEmpty(proIds)) {
            throw new BusinessException("该商品已有设置记录" + StringUtils.join(proIds, ",") + "请查询后重新修改!");
        }
        response.setResult("未发现重复的商品编码");
        return response;
    }

    /**
     * 商品编码导入
     *
     * @param client
     * @param file
     * @return
     */
    @Override
    public List<SelectProductStResponse> insertProductCodeImport(List<Map<String, String>> importInDataList, String client) {
        List<String> codes = new ArrayList<>();
        if(CollUtil.isNotEmpty(importInDataList)){
            Set<String> proCode = importInDataList.stream().map(s->s.get("proCode")).collect(Collectors.toSet());
            codes.addAll(proCode);
        }
        List<SelectProductStResponse> responseList = new ArrayList<>();
        /*if (CollUtil.isNotEmpty(importInDataList)) {
            importInDataList.forEach(data -> codes.add(data.get("proCode")));
        }*/
        if (CollUtil.isEmpty(codes)) {
            throw new BusinessException("商品编码不可为空！");
        }
        if(codes.size()>1000){
            throw new BusinessException("商品数量一次导入限制1000条！");
        }
        List<List<String>> partition = Lists.partition(codes, 500);
        List<String> proId = new ArrayList<>();
        List<GaiaProductBusiness> businesses = new ArrayList<>();
        if (CollUtil.isNotEmpty(partition)) {
            for (List<String> proCodes : partition) {
                if (CollUtil.isNotEmpty(proCodes)) {
                    Example example = new Example(GaiaSdIntegralRateSet.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andIn("proId", proCodes).andEqualTo("client", client);
                    List<GaiaSdIntegralRateSet> rateSets = this.gaiaSdIntegralRateSetMapper.selectByExample(example);
                    if (CollUtil.isNotEmpty(rateSets)) {
                        //存在
                        proId.addAll(rateSets.stream().map(s -> s.getProId()).collect(Collectors.toList()));
                    }
                }
            }
            for (List<String> code : partition) {
                Example product = new Example(GaiaProductBusiness.class);
                Example.Criteria productCriteria = product.createCriteria();
                productCriteria.andIn("proSelfCode", code).andEqualTo("client", client);
                List<GaiaProductBusiness> list = this.productBusinessMapper.selectByExample(product);
                if (CollUtil.isNotEmpty(list)) {
                    //codeList.addAll(code);
                    businesses.addAll(list);
                }

            }
            if (CollUtil.isNotEmpty(proId)) {
                throw new BusinessException("商品编码已存在！" + StringUtils.join(proId, ","));
            }
            /*if (CollUtil.isNotEmpty(codeList)) {
                throw new BusinessException("商品编码不存在！" + StringUtils.join(codeList, ","));
            }*/
        }


       /* //校验商品编码是否已经存在
        Example example = new Example(GaiaSdIntegralRateSet.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("proId", codes).andEqualTo("client", client);
        List<GaiaSdIntegralRateSet> rateSets = this.gaiaSdIntegralRateSetMapper.selectByExample(example);
        if (CollUtil.isNotEmpty(rateSets)) {
            //存在
            throw new BusinessException("商品编码已存在！" + StringUtils.join(rateSets.stream().map(s -> s.getProId()).collect(Collectors.toList()), ","));
        }*/
      /*  Example product = new Example(GaiaProductBusiness.class);
        Example.Criteria productCriteria = product.createCriteria();
        productCriteria.andIn("proSelfCode", codes).andEqualTo("client", client);
        List<GaiaProductBusiness> businesses = this.productBusinessMapper.selectByExample(product);
        if (CollUtil.isEmpty(businesses)) {
            throw new BusinessException("商品编码不存在！" + StringUtils.join(codes, ","));
        }*/

        List<String> collect = businesses.stream().map(s -> s.getProSelfCode()).distinct().collect(Collectors.toList());
        if (CollUtil.isNotEmpty(collect)) {
            codes.removeAll(collect);
            if (CollUtil.isNotEmpty(codes)) {
                throw new BusinessException("商品编码不存在！" + StringUtils.join(codes, ","));
            }
        }
        ArrayList<GaiaProductBusiness> arrayList = businesses.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(s -> s.getProSelfCode()))), ArrayList::new)
        );
        if (CollUtil.isNotEmpty(arrayList)) {
            responseList = arrayList.stream().map(busi -> {
                SelectProductStResponse resp = new SelectProductStResponse();
                resp.setProId(busi.getProSelfCode());
                resp.setProCommonName(busi.getProCommonname());
                resp.setUnit(busi.getProUnit());
                resp.setProFactoryName(busi.getProFactoryName());
                resp.setSpecs(busi.getProSpecs());
                resp.setProCountry(busi.getProCountry());
                resp.setProPlace(busi.getProPlace());
                return resp;
            }).collect(Collectors.toList());
        }
        //获取价格
        if (CollUtil.isNotEmpty(responseList)) {
            Example examplePrice = new Example(GaiaSdProductPrice.class);
            examplePrice.createCriteria().andEqualTo("clientId", client).andIn("gsppProId", responseList.stream().map(s -> s.getProId()).collect(Collectors.toList()));
            List<GaiaSdProductPrice> prices = productPriceMapper.selectByExample(examplePrice);
            if (CollUtil.isNotEmpty(prices)) {
                responseList.forEach(resp -> {
                    prices.stream().filter(price -> price.getGsppProId().equals(resp.getProId())).findFirst().ifPresent(pr -> resp.setPriceNormal(Convert.toStr(pr.getGsppPriceNormal())));
                });
            }
        }
        return responseList;
    }

    public static void main(String[] args) {
        List list = Lists.newArrayList();
        while (true){
            String a = RandomUtil.randomString(100);
            list.add(a);
        }
    }

    /**
     * 初始化日期
     *
     * @param client
     * @return
     */
    @Override
    public Map<String, Object> getPeriodDate() {
        Map<String, Object> weekDate = new LinkedHashMap<>();
        Map<String, Object> weekData = new LinkedHashMap<>();
        weekData.put("1", "周一");
        weekData.put("2", "周二");
        weekData.put("3", "周三");
        weekData.put("4", "周四");
        weekData.put("5", "周五");
        weekData.put("6", "周六");
        weekData.put("0", "周日");
        weekDate.put("week", weekData);

        Map<String, Object> dateData = new LinkedHashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int actualMaximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 1; i <= actualMaximum; i++) {
            dateData.put(Convert.toStr(i), i);
        }
        weekDate.put("date", dateData);
        return weekDate;
    }
}
