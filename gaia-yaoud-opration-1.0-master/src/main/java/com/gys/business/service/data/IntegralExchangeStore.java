package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/11/29 16:19
 */
@Data
public class IntegralExchangeStore {
    private String stoCode;
    private String stoName;
}
