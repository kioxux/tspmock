package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.GaiaSalesCatalog;
import com.gys.business.mapper.GaiaSalesCatalogMapper;
import com.gys.business.service.GaiaSalesCatalogService;
import com.gys.business.service.data.GaiaSalesCatalogInData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 销售目录维护表(GaiaSalesCatalog)表服务实现类
 *
 * @author makejava
 * @since 2021-08-25 10:51:56
 */
@Service("gaiaSalesCatalogService")
@Slf4j
public class GaiaSalesCatalogServiceImpl implements GaiaSalesCatalogService {
    @Resource
    private GaiaSalesCatalogMapper gaiaSalesCatalogMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GaiaSalesCatalog queryById(Long id) {
        return this.gaiaSalesCatalogMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaSalesCatalog> queryAllByLimit(int offset, int limit) {
        return this.gaiaSalesCatalogMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaSalesCatalog insert(GaiaSalesCatalog gaiaSalesCatalog) {
        this.gaiaSalesCatalogMapper.insert(gaiaSalesCatalog);
        return gaiaSalesCatalog;
    }

    /**
     * 修改数据
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaSalesCatalog update(GaiaSalesCatalog gaiaSalesCatalog) {
        this.gaiaSalesCatalogMapper.update(gaiaSalesCatalog);
        return this.queryById(gaiaSalesCatalog.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean insertInfo(GaiaSalesCatalog inData) {
        if (StringUtils.isEmpty(inData.getProSelfCode())) {
            throw new BusinessException("商品编码不能为空！");
        }
        //先查询是否已存在
        int count = gaiaSalesCatalogMapper.checkHas(inData);
        if (count > 0) {
            throw new BusinessException("商品编码："+inData.getProSelfCode()+"，已存在，请勿重复添加");
        }
        GaiaSalesCatalogInData data = new GaiaSalesCatalogInData();
        data.setClient(inData.getClient());
        Object param = this.getParam(data);
        Date now = new Date();
        inData.setCreateTime(now);
        inData.setUpdateUser(inData.getCreateUser());
        inData.setUpdateTime(now);
        inData.setStatus(ObjectUtil.isNotEmpty(param) ? Integer.valueOf(param.toString()) : 0);
        inData.setIsDelete(0);
        return gaiaSalesCatalogMapper.insert(inData) > 0;
    }

    @Override
    public List<GaiaSalesCatalog> list(GaiaSalesCatalogInData inData) {
        return gaiaSalesCatalogMapper.list(inData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(GaiaSalesCatalogInData inData) {
        List<Long> idList = inData.getIdList();
        if (CollectionUtils.isEmpty(idList)) {
            throw new BusinessException("请选择需要删除的记录！");
        }
        for (Long  id: idList) {
            GaiaSalesCatalog catalog = new GaiaSalesCatalog();
            Date now = new Date();
            catalog.setId(id);
            catalog.setUpdateTime(now);
            catalog.setUpdateUser(inData.getUpdateUser());
            catalog.setIsDelete(1);
            gaiaSalesCatalogMapper.update(catalog);
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateStatus(GaiaSalesCatalogInData inData) {
        GaiaSalesCatalog catalog = gaiaSalesCatalogMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(catalog)) {
            throw new BusinessException("没有查询到该记录，请刷新页面！");
        }
        Date now = new Date();
        catalog.setUpdateTime(now);
        catalog.setUpdateUser(inData.getUpdateUser());
        catalog.setStatus(2);
        return gaiaSalesCatalogMapper.update(catalog) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean enableFunction(GaiaSalesCatalogInData inData) {
        if (StringUtils.isEmpty(inData.getGcspPara1())) {
            throw new BusinessException("请求参数异常！");
        }
        inData.setType("1");
        List<GaiaSalesCatalog> list = gaiaSalesCatalogMapper.list(inData);
        if (!CollectionUtils.isEmpty(list)) {
            Date now = new Date();
            for (GaiaSalesCatalog gaiaSalesCatalog : list) {
                gaiaSalesCatalog.setStatus(Integer.valueOf(inData.getGcspPara1()));
                gaiaSalesCatalog.setUpdateUser(inData.getUpdateUser());
                gaiaSalesCatalog.setUpdateTime(now);
                gaiaSalesCatalogMapper.update(gaiaSalesCatalog);
            }
        }
        return true;
    }

    @Override
    public Object getParam(GaiaSalesCatalogInData inData) {
        inData.setType("1");
        List<GaiaSalesCatalog> list = gaiaSalesCatalogMapper.list(inData);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        } else {
            return list.get(0).getStatus();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchImport(MultipartFile file, GetLoginOutData userInfo) {
        //检查文件以及数据完整性
        List<GaiaSalesCatalog> insertDataList = checkFileData(file);
        Date now = new Date();
        for (int i = 0; i < insertDataList.size(); i++) {
            GaiaSalesCatalog catalog = insertDataList.get(i);
            if (StringUtils.isEmpty(catalog.getProSelfCode())) {
                throw new BusinessException("第" + (i + 2) +"行数据，商品编码不能为空！");
            }
            int count = gaiaSalesCatalogMapper.checkHas(catalog);
            if (count > 1) {
                throw new BusinessException("第" + (i + 2) + "行数据已存在！，请勿重复添加");
            }
            GaiaSalesCatalogInData inData = new GaiaSalesCatalogInData();
            inData.setClient(userInfo.getClient());
            inData.setProSelfCode(catalog.getProSelfCode());
            GaiaSalesCatalog insertInfo = gaiaSalesCatalogMapper.getProductInfo(inData);
            if (ObjectUtil.isEmpty(insertInfo)) {
                throw new BusinessException("第" + (i + 2) +"行数据，系统中未查询到商品编码为:"+inData.getProSelfCode()+"的商品！");
            }
            insertInfo.setCreateUser(userInfo.getUserId());
            insertInfo.setCreateTime(now);
            insertInfo.setUpdateUser(userInfo.getUserId());
            insertInfo.setUpdateTime(now);
            insertInfo.setStatus(1);
            insertInfo.setIsDelete(0);
            this.insertInfo(insertInfo);
        }
        return true;
    }

    /**
     * 检查文件以及数据完整性
     * @author SunJiaNan
     * @Date 2021-07-08
     * @param file
     */
    private List<GaiaSalesCatalog> checkFileData(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }

        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaSalesCatalog.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传商品目录,读取excel失败: {}",e.getMessage(), e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<GaiaSalesCatalog> catalogList = new ArrayList<>(excelDataList.size());
        try {
            catalogList = JSON.parseArray(JSON.toJSONString(excelDataList), GaiaSalesCatalog.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：{}",e.getMessage(), e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(catalogList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //校验表头
        GaiaSalesCatalog dataHead = catalogList.get(0);
        String[] channelManagerExcelHead = CommonConstant.GAIA_SALES_CATALOG_IMPORT;
        if (!channelManagerExcelHead[0].equals(dataHead.getProSelfCode())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<GaiaSalesCatalog> insertData = catalogList.subList(1, catalogList.size());
        if (ValidateUtil.isEmpty(insertData)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return insertData;
    }
}
