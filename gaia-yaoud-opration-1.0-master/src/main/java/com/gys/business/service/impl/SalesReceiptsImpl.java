package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.AtomicDouble;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.SalesReceiptsService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.workflow.ChangePriceWFData;
import com.gys.business.service.salesReceiptsPriceUtil.PromCouponProType;
import com.gys.business.service.salesReceiptsPriceUtil.PromGift;
import com.gys.business.service.salesReceiptsPriceUtil.PromSeries;
import com.gys.business.service.salesReceiptsPriceUtil.PromSingle;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.*;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.feign.AuthService;
import com.gys.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("all")
@Slf4j
@Service
public class SalesReceiptsImpl implements SalesReceiptsService {
    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;
    @Autowired
    private GaiaProductBasicMapper gaiaProductBasicMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetMapper gaiaSdIntegralExchangeSetMapper;
    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;
    @Autowired
    private GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;
    @Autowired
    private GaiaSdPromHeadMapper gaiaSdPromHeadMapper;
    @Autowired
    private GaiaSdPromUnitarySetMapper gaiaSdPromUnitarySetMapper;
    @Autowired
    private GaiaSdSaleHMapper gaiaSdSaleHMapper;
    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Autowired
    private GaiaSdPromSeriesCondsMapper gaiaSdPromSeriesCondsMapper;
    @Autowired
    private GaiaSdPromSeriesSetMapper gaiaSdPromSeriesSetMapper;
    @Autowired
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;
    @Autowired
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Autowired
    private GaiaRetailPriceMapper gaiaRetailPriceMapper;
    @Autowired
    private GaiaSdPromGiftCondsMapper gaiaSdPromGiftCondsMapper;
    @Autowired
    private GaiaSdPromGiftSetMapper gaiaSdPromGiftSetMapper;
    @Autowired
    private GaiaSdPromCouponUseMapper gaiaSdPromCouponUseMapper;
    @Autowired
    private GaiaSdPromCouponSetMapper gaiaSdPromCouponSetMapper;
    @Autowired
    private GaiaSdPromCouponGrantMapper gaiaSdPromCouponGrantMapper;
    @Autowired
    private GaiaSdIntegralAddSetMapper gaiaSdIntegralAddSetMapper;
    @Autowired
    private GaiaSdStoreDataMapper sdStoreDataMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdPromHyrPriceMapper gaiaSdPromHyrPriceMapper;
    @Autowired
    private GaiaSdPromHyrDiscountMapper gaiaSdPromHyrDiscountMapper;
    @Autowired
    private GaiaSdPromHySetMapper gaiaSdPromHySetMapper;
    @Autowired
    private GaiaSdPromAssoCondsMapper gaiaSdPromAssoCondsMapper;
    @Autowired
    private GaiaSdPromAssoResultMapper gaiaSdPromAssoResultMapper;
    @Autowired
    private GaiaSdPromAssoSetMapper gaiaSdPromAssoSetMapper;
    @Autowired
    private GaiaSdPromCouponBasicMapper gaiaSdPromCouponBasicMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Autowired
    private GaiaSdPromGiftResultMapper gaiaSdPromGiftResultMapper;
    @Autowired
    private GaiaSdMemberClassMapper gaiaSdMemberClassMapper;
    @Autowired//总库存表
    private GaiaSdStockMapper gaiaSdStockMapper;
    @Autowired
    private GaiaWmsMenDianHuoWeiHaoMapper gaiaWmsMenDianHuoWeiHaoMapper;
    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;
    @Autowired
    private CacheService synchronizedService;
    @Autowired
    private GaiaUserUsemapMapper gaiaUserUsemapMapper;
    @Autowired
    private RabbitTemplateHelper rabbitTemplate;
    @Autowired
    private GaiaSdSalePayMsgMapper msgMapper;
    @Autowired
    private GaiaSdStockBatchMapper stockBatchMapper;
    @Autowired
    private GaiaDataSynchronizedMapper synchronizedMapper;
    @Autowired
    private AuthService authService;
    @Autowired
    private GaiaSdRechargeCardMapper gaiaSdRechargeCardMapper;

    @Autowired
    private GaiaSdSaleHandHMapper gaiaSdSaleHandHMapper;

    @Autowired
    private GaiaSdSaleHandDMapper gaiaSdSaleHandDMapper;

    @Autowired
    private GaiaSdElectronChangeMapper electronChangeMapper;

    //电子券异动表
    @Autowired
    private GaiaSdElectronChangeMapper gaiaSdElectronChangeMapper;

    @Autowired
    private GaiaSdProductsGroupMapper gaiaSdProductsGroupMapper;


    DecimalFormat decimalFormat = new DecimalFormat("##########.##########");

    @Override
    public GetQueryMemberOutData queryMember(GetQueryMemberInData inData) {
        GetQueryMemberOutData outData = this.gaiaSdMemberBasicMapper.queryMember(inData);
        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void givePoint(GetQueryMemberInData inData) {
        GaiaSdMemberBasic gaiaSdMemberBasic = new GaiaSdMemberBasic();
        gaiaSdMemberBasic.setClientId(inData.getClientId());
        gaiaSdMemberBasic = this.gaiaSdMemberBasicMapper.selectByPrimaryKey(gaiaSdMemberBasic);
        this.gaiaSdMemberBasicMapper.updateByPrimaryKeySelective(gaiaSdMemberBasic);

        Map<String, Object> map = new HashMap<>(16);
        map.put("CLIENT", inData.getClientId());
        map.put("GSMB_MEMBER_ID", gaiaSdMemberBasic.getGsmbMemberId());
        GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
        aSynchronized.setClient(inData.getClientId());
        aSynchronized.setSite(inData.getBrId());
        aSynchronized.setVersion(IdUtil.randomUUID());
        aSynchronized.setTableName("GAIA_SD_MEMBER_BASIC");
        aSynchronized.setCommand("update");
        aSynchronized.setSourceNo("会员修改");
        aSynchronized.setArg(JSON.toJSONString(map));
        this.synchronizedService.addOne(aSynchronized);
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("givePoint");
        mqReceiveData.setData(inData.getCardId());
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + inData.getBrId(), JSON.toJSON(mqReceiveData));

    }

    @Override
    public List<GetQueryProductOutData> queryProduct(GetQueryProductInData inData) {
        List<GetQueryProductOutData> out;
        if ("1".equals(inData.getIsThird())) {
            out = this.gaiaProductBusinessMapper.queryProduct2(inData);
        } else {
            out = this.gaiaProductBusinessMapper.queryProduct(inData);
        }
        IntStream.range(0, out.size()).forEach(i -> {
            GetQueryProductOutData item = (GetQueryProductOutData) out.get(i);
            item.setIndex(String.valueOf(i + 1));
        });
        return out;
    }

    @Override
    public GetSalesReceiptsTableOutData queryProductDetail(GetQueryProductInData inData) {
        GetSalesReceiptsTableOutData out = this.gaiaProductBusinessMapper.queryProductDetail(inData);
        if (ObjectUtil.isNotEmpty(out)) {
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
            out.setPrcAmount(decimalFormat.format(new BigDecimal(out.getPrcAmount())));
            out.setPrice(decimalFormat.format(new BigDecimal(out.getPrice())));
            out.setTotal(decimalFormat.format(new BigDecimal(out.getTotal())));
        }
        return out;
    }

    @Override
    public List<GetPdAndExpOutData> queryBatchNoAndExp(GetQueryProductInData inData) {
        List<GetPdAndExpOutData> outData = this.gaiaProductBusinessMapper.queryBatchNoAndExp(inData);
        return outData;
    }

    @Override
    public List<GetPdAndExpOutData> queryStockAndExpGroupByBatchNo(GetQueryProductInData inData) {
        List<GetPdAndExpOutData> outData = null;
        if ("3".equals(inData.getProStorageArea())) {
            outData = this.gaiaProductBusinessMapper.queryStockAndExpGroupByArea(inData);
        } else {
            outData = this.gaiaProductBusinessMapper.queryStockAndExpGroupByBatchNo(inData);
        }
        return outData;
    }

    @Override
    public List<GetPdAndExpOutData> queryStockAndExpGroupByThird(GetQueryProductInData inData) {
        List<GetPdAndExpOutData> outData = null;
        outData = this.gaiaProductBusinessMapper.queryStockAndExpGroupByThird(inData);
        return outData;
    }

    @Override
    public GetPointExchangeOutData pointExchange(GetQueryProductInData inData) {
        GetPointExchangeOutData outData = this.gaiaSdIntegralExchangeSetMapper.pointExchange(inData);

        if (ObjectUtil.isNotEmpty(outData)) {
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
            outData.setGsiesExchangeAmt(decimalFormat.format(new BigDecimal(outData.getGsiesExchangeAmt())));
        }
        return outData;
    }

    @Override
    public List<GetEmpOutData> queryEmp(GetLoginOutData inData) {
        List<GetEmpOutData> outData = this.gaiaAuthconfiDataMapper.queryEmp(inData);
        return outData;
    }

    @Override
    public List<GetEmpOutData> queryAllEmp(GetLoginOutData inData, CommonData content) {
        List<GetEmpOutData> outData = this.gaiaAuthconfiDataMapper.queryAllEmp(inData.getClient(), content.getContent());
        return outData;
    }

    @Override
    public List<GetDoctorOutData> queryDoctor(GetLoginOutData inData) {
        List<GetDoctorOutData> outData = this.gaiaAuthconfiDataMapper.queryDoctor(inData);
        return outData;
    }

    @Override
    public List<GetDoctorOutData> queryDoctorByClient(Map map) {
        List<GetDoctorOutData> outData = this.gaiaAuthconfiDataMapper.queryDoctorByClient(map);
        return outData;
    }

    @Override
    public List<GetDoctorOutData> queryDruggist(GetLoginOutData inData) {
        List<GetDoctorOutData> outData = this.gaiaAuthconfiDataMapper.queryDruggist(inData);
        return outData;
    }

    private void comBinRemove(GetQueryProductInData inData, List<GetPromByProCodeOutData> outData) {
        List<GetPromByProCodeOutData> removeListCombin = new ArrayList();

        for (int i = 0; i < outData.size(); ++i) {
            GetPromByProCodeOutData item = (GetPromByProCodeOutData) outData.get(i);
            if ("PROM_COMBIN".equals(item.getPromotionTypeId())) {
                GaiaSdPromAssoConds gaiaSdPromAssoConds = new GaiaSdPromAssoConds();
                gaiaSdPromAssoConds.setClientId(inData.getClientId());
                gaiaSdPromAssoConds.setGspacVoucherId(item.getGsphVoucherId());
                gaiaSdPromAssoConds.setGspacSeriesId(item.getSeriesId());
                List<GaiaSdPromAssoConds> sdPromAssoCondsList = this.gaiaSdPromAssoCondsMapper.select(gaiaSdPromAssoConds);
                List<GetSalesReceiptsTableOutData> numAmtDataList = new ArrayList();
                int j = 0;
                if (j < sdPromAssoCondsList.size()) {
                    GaiaSdPromAssoConds conds = (GaiaSdPromAssoConds) sdPromAssoCondsList.get(j);

                    for (int k = 0; k < inData.getAllPros().size(); ++k) {
                        GetSalesReceiptsTableOutData itemK = (GetSalesReceiptsTableOutData) inData.getAllPros().get(k);
                        if (itemK.getProCode().equals(conds.getGspacProId()) && itemK.getNum().equals(conds.getGspacQty()) && numAmtDataList.indexOf(itemK) == -1) {
                            numAmtDataList.add(itemK);
                            break;
                        }
                    }
                }

                if (sdPromAssoCondsList.size() != numAmtDataList.size()) {
                    removeListCombin.add(item);
                } else {
                    item.setNumAmtDataList(numAmtDataList);
                }
            }
        }
        outData.removeAll(removeListCombin);
    }

    @Override
    public GetPromByProCodeOutData memByProCode(GetQueryProductInData inData) {
        List<GetPromByProCodeOutData> promByProCodeOutDataMemberDay = null;
        List<GetPromByProCodeOutData> promByProCodeOutDataMemberDayU = null;
        List<GetPromByProCodeOutData> promByProCodeOutDataMember = null;
        List<GetPromByProCodeOutData> outData = new ArrayList();
        int memberFlag = 0;
        if (ObjectUtil.isNotEmpty(inData.getMemberType())) {
            promByProCodeOutDataMemberDay = this.gaiaSdPromHeadMapper.promByProCodeMemberDay(inData);
            if (ObjectUtil.isEmpty(promByProCodeOutDataMemberDay)) {
                promByProCodeOutDataMemberDayU = this.gaiaSdPromHeadMapper.promByProCodeMemberDayU(inData);
                if (ObjectUtil.isEmpty(promByProCodeOutDataMemberDayU)) {
                    promByProCodeOutDataMember = this.gaiaSdPromHeadMapper.promByProCodeMember(inData);
                    if (ObjectUtil.isNotEmpty(promByProCodeOutDataMember) && promByProCodeOutDataMember.size() == 1) {
                        memberFlag = 3;
                        outData.addAll(promByProCodeOutDataMember);
                    }
                } else if (ObjectUtil.isNotEmpty(promByProCodeOutDataMemberDayU) && promByProCodeOutDataMemberDayU.size() == 1) {
                    memberFlag = 2;
                    outData.addAll(promByProCodeOutDataMemberDayU);
                }
            } else if (ObjectUtil.isNotEmpty(promByProCodeOutDataMemberDay) && promByProCodeOutDataMemberDay.size() == 1) {
                memberFlag = 1;
                outData.addAll(promByProCodeOutDataMemberDay);
            }
        }

        if (ObjectUtil.isNotEmpty(outData)) {
            GetPromByProCodeOutData promByProCodeOutData = (GetPromByProCodeOutData) outData.get(0);
            promByProCodeOutData.setMemberFlag(String.valueOf(memberFlag));
            List<ActiveInfo> activeInfoList = new ArrayList();
            ActiveInfo activeInfo = new ActiveInfo();
            activeInfo.setGsphVoucherId(promByProCodeOutData.getGsphVoucherId());
            activeInfo.setGsphType(promByProCodeOutData.getPromotionType());
            activeInfo.setGsphTypeId(promByProCodeOutData.getPromotionTypeId());
            activeInfo.setSerial(promByProCodeOutData.getSerial());
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(inData.getClientId());
            gaiaProductBusiness.setProSelfCode(inData.getNameOrCode());
            gaiaProductBusiness.setProSite(inData.getBrId());
            gaiaProductBusiness.setProStatus("0");
            gaiaProductBusiness = (GaiaProductBusiness) this.gaiaProductBusinessMapper.selectOne(gaiaProductBusiness);
            activeInfo.setProCode(gaiaProductBusiness.getProSelfCode());
            activeInfo.setProName(gaiaProductBusiness.getProName());
            GaiaSdProductPrice gaiaSdProductPrice = new GaiaSdProductPrice();
            gaiaSdProductPrice.setClientId(inData.getClientId());
            gaiaSdProductPrice.setGsppBrId(inData.getBrId());
            gaiaSdProductPrice.setGsppProId(inData.getNameOrCode());
            gaiaSdProductPrice = (GaiaSdProductPrice) this.gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPrice);
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
            String priceMoney = decimalFormat.format(gaiaSdProductPrice.getGsppPriceNormal());
            activeInfo.setNormalPrice(priceMoney);
            this.promHy(inData, (GetPromByProCodeOutData) outData.get(0), activeInfo, activeInfoList, memberFlag);
            promByProCodeOutData.setActiveInfo(activeInfoList);
            return (GetPromByProCodeOutData) outData.get(0);

        } else {
            return null;
        }
    }

    public String countSenglePro(GetQueryProductInData inData) {
        String code = inData.getNameOrCode();
        float count = 0.0F;
        for (int i = 0; i < inData.getAllProduct().size(); ++i) {
            GetSalesReceiptsTableOutData item = (GetSalesReceiptsTableOutData) inData.getAllProduct().get(i);
            if (code.equals(item.getProCode())
                    && !"1".equals(item.getIsGift())
                    && !"拆零".equals(item.getStatus())
                    && !"改价".equals(item.getStatus())
                    && !"exclusion1".equals(item.getGsphExclusion())
                    && !"exclusion3".equals(item.getGsphExclusion())) {
                count += Float.valueOf(item.getNum());
            }
        }
        return String.valueOf(count);
    }

    public List<GetSalesReceiptsTableOutData> canJoinSaleActPro(GetQueryProductInData inData) {
        List<GetSalesReceiptsTableOutData> canJoinSaleActPro = new ArrayList();
        for (int i = 0; i < inData.getAllProduct().size(); ++i) {
            GetSalesReceiptsTableOutData item = (GetSalesReceiptsTableOutData) inData.getAllProduct().get(i);
            if (!"1".equals(item.getIsGift()) && !"拆零".equals(item.getStatus()) && !"改价".equals(item.getStatus()) && !"exclusion1".equals(item.getGsphExclusion()) && !"exclusion3".equals(item.getGsphExclusion())) {
                canJoinSaleActPro.add(item);
            }
        }
        return canJoinSaleActPro;
    }

    /**
     * 组合
     * * @param inData
     *
     * @return
     */
    @Override
    public List<GetPromByProCodeOutData> promByProCode(GetQueryProductInData inData) {
        String totalNum = this.countSenglePro(inData);
        inData.setTotalNum(totalNum);
        inData.setTotalMoney((new BigDecimal(totalNum)).multiply(new BigDecimal(inData.getSelePro().getPrcAmount())));
        inData.setNum(inData.getSelePro().getNum());
        List<GetSalesReceiptsTableOutData> canJoinSaleActPro = this.canJoinSaleActPro(inData);
        inData.setAllPros(canJoinSaleActPro);

        List<GetPromByProCodeOutData> outData = this.gaiaSdPromHeadMapper.promByProCode(inData);


        List<GetPromByProCodeOutData> promByProCodeOutDataMemberDay = null;
        List<GetPromByProCodeOutData> promByProCodeOutDataMemberDayU = null;
        List<GetPromByProCodeOutData> promByProCodeOutDataMember = null;
        this.comBinRemove(inData, outData);
        int memberFlag = 0;
        if (ObjectUtil.isNotEmpty(inData.getMemberType())) {
            promByProCodeOutDataMemberDay = this.gaiaSdPromHeadMapper.promByProCodeMemberDay(inData);
            if (ObjectUtil.isEmpty(promByProCodeOutDataMemberDay)) {
                promByProCodeOutDataMemberDayU = this.gaiaSdPromHeadMapper.promByProCodeMemberDayU(inData);
                if (ObjectUtil.isEmpty(promByProCodeOutDataMemberDayU)) {
                    promByProCodeOutDataMember = this.gaiaSdPromHeadMapper.promByProCodeMember(inData);
                    if (ObjectUtil.isNotEmpty(promByProCodeOutDataMember) && promByProCodeOutDataMember.size() == 1) {
                        memberFlag = 3;
                        outData.addAll(promByProCodeOutDataMember);
                    }
                } else if (ObjectUtil.isNotEmpty(promByProCodeOutDataMemberDayU) && promByProCodeOutDataMemberDayU.size() == 1) {
                    memberFlag = 2;
                    outData.addAll(promByProCodeOutDataMemberDayU);
                }
            } else if (ObjectUtil.isNotEmpty(promByProCodeOutDataMemberDay) && promByProCodeOutDataMemberDay.size() == 1) {
                memberFlag = 1;
                outData.addAll(promByProCodeOutDataMemberDay);
            }
        }

        List<GetPromByProCodeOutData> removeArray = new ArrayList();

        for (int i = 0; i < outData.size(); ++i) {
            GetPromByProCodeOutData promByProCodeOutData = (GetPromByProCodeOutData) outData.get(i);
            List<ActiveInfo> activeInfoList = new ArrayList();
            ActiveInfo activeInfo = new ActiveInfo();
            activeInfo.setGsphVoucherId(promByProCodeOutData.getGsphVoucherId());
            activeInfo.setGsphType(promByProCodeOutData.getPromotionType());
            activeInfo.setGsphTypeId(promByProCodeOutData.getPromotionTypeId());
            activeInfo.setSerial(promByProCodeOutData.getSerial());
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(inData.getClientId());
            gaiaProductBusiness.setProSelfCode(inData.getNameOrCode());
            gaiaProductBusiness.setProSite(inData.getBrId());
            gaiaProductBusiness.setProStatus("0");
            gaiaProductBusiness = (GaiaProductBusiness) this.gaiaProductBusinessMapper.selectOne(gaiaProductBusiness);
            activeInfo.setProCode(gaiaProductBusiness.getProSelfCode());
            activeInfo.setProName(gaiaProductBusiness.getProName());
            GaiaSdProductPrice gaiaSdProductPrice = new GaiaSdProductPrice();
            gaiaSdProductPrice.setClientId(inData.getClientId());
            gaiaSdProductPrice.setGsppBrId(inData.getBrId());
            gaiaSdProductPrice.setGsppProId(inData.getNameOrCode());
            gaiaSdProductPrice = (GaiaSdProductPrice) this.gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPrice);
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
            String priceMoney = decimalFormat.format(gaiaSdProductPrice.getGsppPriceNormal());
            activeInfo.setNormalPrice(priceMoney);
            if ("PROM_SINGLE".equals(promByProCodeOutData.getPromotionTypeId())) {
                this.promSingle(inData, promByProCodeOutData, activeInfo, activeInfoList);
            } else if ("PROM_SERIES".equals(promByProCodeOutData.getPromotionTypeId())) {
                this.promSeries(inData, promByProCodeOutData, removeArray, activeInfo, activeInfoList);
            } else if ("PROM_HYJ".equals(promByProCodeOutData.getPromotionTypeId())) {
                this.promHy(inData, promByProCodeOutData, activeInfo, activeInfoList, memberFlag);
            } else if ("PROM_HYRJ".equals(promByProCodeOutData.getPromotionTypeId())) {
                this.promHy(inData, promByProCodeOutData, activeInfo, activeInfoList, memberFlag);
            } else if ("PROM_HYRZ".equals(promByProCodeOutData.getPromotionTypeId())) {
                this.promHy(inData, promByProCodeOutData, activeInfo, activeInfoList, memberFlag);
            } else if ("PROM_COMBIN".equals(promByProCodeOutData.getPromotionTypeId())) {
                this.promCombin(inData, promByProCodeOutData, activeInfo, activeInfoList);
            }

            promByProCodeOutData.setActiveInfo(activeInfoList);
        }

        List<GetPromByProCodeOutData> tempList = new ArrayList();
        for (int i = 0; i < outData.size(); ++i) {
            if (ObjectUtil.isEmpty(((GetPromByProCodeOutData) outData.get(i)).getActiveInfo())) {
                tempList.add(outData.get(i));
            }
        }

        outData.removeAll(tempList);
        List<GetPromByProCodeOutData> promByProCodeOutData = new ArrayList();
        Set<String> set = new HashSet();

        for (int i = 0; i < outData.size(); ++i) {
            GetPromByProCodeOutData item = (GetPromByProCodeOutData) outData.get(i);
            if (set.add(item.getGsphVoucherId())) {
                promByProCodeOutData.add(item);
            } else {
                for (int j = 0; j < promByProCodeOutData.size(); ++j) {
                    GetPromByProCodeOutData temp = (GetPromByProCodeOutData) promByProCodeOutData.get(j);
                    if (temp.getGsphVoucherId().equals(item.getGsphVoucherId())) {
                        temp.getActiveInfo().addAll(item.getActiveInfo());
                        break;
                    }
                }
            }
        }
        return promByProCodeOutData;
    }

    private void promCombin(GetQueryProductInData inData, GetPromByProCodeOutData promByProCodeOutData, ActiveInfo activeInfo, List<ActiveInfo> activeInfoList) {
        activeInfo.setNumAmtDataList(promByProCodeOutData.getNumAmtDataList());
        GaiaSdPromAssoSet gaiaSdPromAssoSet = new GaiaSdPromAssoSet();
        gaiaSdPromAssoSet.setClientId(inData.getClientId());
        gaiaSdPromAssoSet.setGspasVoucherId(promByProCodeOutData.getGsphVoucherId());
        gaiaSdPromAssoSet.setGspasSeriesId(promByProCodeOutData.getSeriesId());
        gaiaSdPromAssoSet = (GaiaSdPromAssoSet) this.gaiaSdPromAssoSetMapper.selectOne(gaiaSdPromAssoSet);
        GaiaSdPromAssoConds gaiaSdPromAssoConds = new GaiaSdPromAssoConds();
        gaiaSdPromAssoConds.setClientId(inData.getClientId());
        gaiaSdPromAssoConds.setGspacVoucherId(promByProCodeOutData.getGsphVoucherId());
        gaiaSdPromAssoConds.setGspacSeriesId(promByProCodeOutData.getSeriesId());
        List<GaiaSdPromAssoConds> gaiaSdPromAssoCondsList = this.gaiaSdPromAssoCondsMapper.select(gaiaSdPromAssoConds);
        if (gaiaSdPromAssoCondsList.size() > 0) {
            GaiaSdPromAssoConds gaiaSdPromAssoConds1 = (GaiaSdPromAssoConds) gaiaSdPromAssoCondsList.get(0);
            promByProCodeOutData.setGspscInteRate(gaiaSdPromAssoConds1.getGspacInteRate());
            promByProCodeOutData.setGspscInteFlag(gaiaSdPromAssoConds1.getGspacInteFlag());
        }

        DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
        BigDecimal gspasAmt = gaiaSdPromAssoSet.getGspasAmt();
        activeInfo.setUnionMoney(gspasAmt == null ? null : decimalFormat.format(gspasAmt));
        activeInfo.setUnionDis(gaiaSdPromAssoSet.getGspasRebate());
        activeInfoList.add(activeInfo);
    }

    private void promHy(GetQueryProductInData inData, GetPromByProCodeOutData promByProCodeOutData, ActiveInfo activeInfo, List<ActiveInfo> activeInfoList, int memberFlag) {
        activeInfo.setMemberFlag(String.valueOf(memberFlag));
        DecimalFormat decimalFormat;
        BigDecimal gsphsPrice;
        if (memberFlag == 1) {
            GaiaSdPromHyrPrice promHyrPrice = new GaiaSdPromHyrPrice();
            promHyrPrice.setClientId(inData.getClientId());
            promHyrPrice.setGsphpVoucherId(promByProCodeOutData.getGsphVoucherId());
            promHyrPrice.setGsphpProId(inData.getNameOrCode());
            promHyrPrice = (GaiaSdPromHyrPrice) this.gaiaSdPromHyrPriceMapper.selectOne(promHyrPrice);
            promByProCodeOutData.setGspscInteFlag(promHyrPrice.getGsphpInteFlag());
            promByProCodeOutData.setGspscInteRate(promHyrPrice.getGsphpInteRate());
            decimalFormat = new DecimalFormat("##########.##########");
            gsphsPrice = promHyrPrice.getGsphpPrice();
            activeInfo.setPriceOne(gsphsPrice == null ? null : decimalFormat.format(gsphsPrice));
            activeInfo.setDisOne(promHyrPrice.getGsphpRebate());
            activeInfo.setDis(promHyrPrice.getGsphpRebate());
            activeInfo.setDisPrice(promHyrPrice.getGsphpPrice() == null ? null : promHyrPrice.getGsphpPrice().toString());
            activeInfoList.add(activeInfo);
        } else if (memberFlag == 2) {
            GaiaSdPromHyrDiscount gaiaSdPromHyrDiscount = new GaiaSdPromHyrDiscount();
            gaiaSdPromHyrDiscount.setClientId(inData.getClientId());
            gaiaSdPromHyrDiscount.setGsppVoucherId(promByProCodeOutData.getGsphVoucherId());
            gaiaSdPromHyrDiscount.setGsppSerial(promByProCodeOutData.getSerial());
            gaiaSdPromHyrDiscount = (GaiaSdPromHyrDiscount) this.gaiaSdPromHyrDiscountMapper.selectOne(gaiaSdPromHyrDiscount);
            promByProCodeOutData.setGspscInteRate(gaiaSdPromHyrDiscount.getGsppInteRate());
            promByProCodeOutData.setGspscInteFlag(gaiaSdPromHyrDiscount.getGsppInteFlag());
            activeInfo.setDisOne(gaiaSdPromHyrDiscount.getGsppRebate());
            activeInfo.setDis(gaiaSdPromHyrDiscount.getGsppRebate());
            activeInfoList.add(activeInfo);
        } else if (memberFlag == 3) {
            GaiaSdPromHySet promHySet = new GaiaSdPromHySet();
            promHySet.setClientId(inData.getClientId());
            promHySet.setGsphsVoucherId(promByProCodeOutData.getGsphVoucherId());
            promHySet.setGsphsProId(inData.getNameOrCode());
            promHySet = (GaiaSdPromHySet) this.gaiaSdPromHySetMapper.selectOne(promHySet);
            promByProCodeOutData.setGspscInteFlag(promHySet.getGsphsInteFlag());
            promByProCodeOutData.setGspscInteRate(promHySet.getGsphsInteRate());
            decimalFormat = new DecimalFormat("##########.##########");
            gsphsPrice = promHySet.getGsphsPrice();
            activeInfo.setPriceOne(gsphsPrice == null ? null : decimalFormat.format(gsphsPrice));
            activeInfo.setDisOne(promHySet.getGsphsRebate());
            activeInfo.setDis(promHySet.getGsphsRebate());
            activeInfo.setDisPrice(promHySet.getGsphsPrice() == null ? null : promHySet.getGsphsPrice().toString());
            activeInfoList.add(activeInfo);
        }

    }

    private void promSingle(GetQueryProductInData inData, GetPromByProCodeOutData promByProCodeOutData, ActiveInfo activeInfo, List<ActiveInfo> activeInfoList) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = new GaiaSdPromUnitarySet();
        gaiaSdPromUnitarySet.setClientId(inData.getClientId());
        gaiaSdPromUnitarySet.setGspusVoucherId(promByProCodeOutData.getGsphVoucherId());
        gaiaSdPromUnitarySet.setGspusSerial(promByProCodeOutData.getSerial());
        gaiaSdPromUnitarySet.setGspusProId(inData.getNameOrCode());
        gaiaSdPromUnitarySet = (GaiaSdPromUnitarySet) this.gaiaSdPromUnitarySetMapper.selectByPrimaryKey(gaiaSdPromUnitarySet);
        promByProCodeOutData.setGspscInteFlag(gaiaSdPromUnitarySet.getGspusInteFlag());
        promByProCodeOutData.setGspscInteRate(gaiaSdPromUnitarySet.getGspusInteRate());
        DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
        BigDecimal gspusPrc1 = gaiaSdPromUnitarySet.getGspusPrc1();
        activeInfo.setPriceOne(gspusPrc1 == null ? null : decimalFormat.format(gspusPrc1));
        activeInfo.setDisOne(gaiaSdPromUnitarySet.getGspusRebate1());
        BigDecimal gspusPrc2 = gaiaSdPromUnitarySet.getGspusPrc2();
        activeInfo.setPriceTwo(gspusPrc2 == null ? null : decimalFormat.format(gspusPrc2));
        activeInfo.setDisTwo(gaiaSdPromUnitarySet.getGspusRebate2());
        BigDecimal gspusPrc3 = gaiaSdPromUnitarySet.getGspusPrc3();
        activeInfo.setPriceThree(gspusPrc3 == null ? null : decimalFormat.format(gspusPrc3));
        activeInfo.setDisThree(gaiaSdPromUnitarySet.getGspusRebate3());
        activeInfo.setDisOne(gaiaSdPromUnitarySet.getGspusQty1());
        activeInfoList.add(activeInfo);
    }

    private void promSeries(GetQueryProductInData inData,
                            GetPromByProCodeOutData promByProCodeOutData,
                            List<GetPromByProCodeOutData> removeArray,
                            ActiveInfo activeInfo,
                            List<ActiveInfo> activeInfoList) {

        //促销主表
        GaiaSdPromHead gaiaSdPromHead = new GaiaSdPromHead();
        gaiaSdPromHead.setClientId(inData.getClientId());
        gaiaSdPromHead.setGsphBrId(inData.getBrId());
        gaiaSdPromHead.setGsphVoucherId(promByProCodeOutData.getGsphVoucherId());
        gaiaSdPromHead = (GaiaSdPromHead) this.gaiaSdPromHeadMapper.selectByPrimaryKey(gaiaSdPromHead);

        //系列类促销系列编码设置表
        GaiaSdPromSeriesSet gaiaSdPromSeriesSet = new GaiaSdPromSeriesSet();
        gaiaSdPromSeriesSet.setClientId(inData.getClientId());
        gaiaSdPromSeriesSet.setGspssVoucherId(promByProCodeOutData.getGsphVoucherId());
        gaiaSdPromSeriesSet.setGspssSerial(promByProCodeOutData.getSerial());
        gaiaSdPromSeriesSet.setGspssSeriesId(promByProCodeOutData.getSeriesId());
        gaiaSdPromSeriesSet = (GaiaSdPromSeriesSet) this.gaiaSdPromSeriesSetMapper.selectByPrimaryKey(gaiaSdPromSeriesSet);

        //系列类促销条件商品设置表
        GaiaSdPromSeriesConds gaiaSdPromSeriesConds = new GaiaSdPromSeriesConds();
        gaiaSdPromSeriesConds.setClientId(inData.getClientId());
        gaiaSdPromSeriesConds.setGspscVoucherId(promByProCodeOutData.getGsphVoucherId());
        gaiaSdPromSeriesConds.setGspscSeriesId(promByProCodeOutData.getSeriesId());
        List<GaiaSdPromSeriesConds> promSeriesCondsList = this.gaiaSdPromSeriesCondsMapper.select(gaiaSdPromSeriesConds);

        if (promSeriesCondsList.size() > 0) {
            promByProCodeOutData.setGspscInteFlag(((GaiaSdPromSeriesConds) promSeriesCondsList.get(0)).getGspscInteFlag());
            promByProCodeOutData.setGspscInteRate(((GaiaSdPromSeriesConds) promSeriesCondsList.get(0)).getGspscInteRate());
        }

        Float count;
        BigDecimal money;
        Map proNumAmtMap;
        String s;
        GetNumAmtData getNumAmtData;
        if ("assign2".equals(gaiaSdPromHead.getGsphPara3())) {
            count = 0.0F;
            money = new BigDecimal(0);
            proNumAmtMap = inData.getProNumAmtMap();

            for (int j = 0; j < promSeriesCondsList.size(); ++j) {
                s = ((GaiaSdPromSeriesConds) promSeriesCondsList.get(j)).getGspscProId();
                if (ObjectUtil.isNotEmpty(proNumAmtMap.get(s))) {
                    getNumAmtData = (GetNumAmtData) proNumAmtMap.get(s);
                    count = count + Float.valueOf(getNumAmtData.getNum());
                    money = money.add(new BigDecimal(getNumAmtData.getMoney()));
                }
            }
            //如果达到数量1 大于 count  或者 达到金额1 大于等1
//            if ((float)Integer.valueOf(gaiaSdPromSeriesSet.getGspssReachQty1()) > count || gaiaSdPromSeriesSet.getGspssReachAmt1().compareTo(money) >= 1) {
//                removeArray.add(promByProCodeOutData);
//            }
        } else if ("assign1".equals(gaiaSdPromHead.getGsphPara3())) {
            count = 0.0F;
            money = new BigDecimal(0);
            proNumAmtMap = inData.getProNumAmtMap();
            Iterator var13 = proNumAmtMap.keySet().iterator();

            while (true) {
                if (!var13.hasNext()) {
                    if (Float.valueOf(gaiaSdPromSeriesSet.getGspssReachQty1()) > count || gaiaSdPromSeriesSet.getGspssReachAmt1().compareTo(money) >= 1) {
                        removeArray.add(promByProCodeOutData);
                        return;
                    }
                    break;
                }

                s = (String) var13.next();
                getNumAmtData = (GetNumAmtData) proNumAmtMap.get(s);
                count = count + Float.valueOf(getNumAmtData.getNum());
                money = money.add(new BigDecimal(getNumAmtData.getMoney()));
            }
        }

        DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
        String gsphPara = gaiaSdPromHead.getGsphPart();
        BigDecimal gspssResultAmt1;
        if ("1".equals(gsphPara)) {
            gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
            activeInfo.setResultSubOne(gspssResultAmt1 == null ? null : decimalFormat.format(gspssResultAmt1));
            activeInfo.setResultDisOne(gaiaSdPromSeriesSet.getGspssResultRebate1());
        } else {
            BigDecimal gspssResultAmt2;
            if ("2".equals(gsphPara)) {
                gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
                activeInfo.setResultSubOne(gspssResultAmt1 == null ? null : decimalFormat.format(gspssResultAmt1));
                activeInfo.setResultDisOne(gaiaSdPromSeriesSet.getGspssResultRebate1());
                gspssResultAmt2 = gaiaSdPromSeriesSet.getGspssResultAmt2();
                activeInfo.setResultSubTwo(gspssResultAmt2 == null ? null : decimalFormat.format(gspssResultAmt2));
                activeInfo.setResultDisTwo(gaiaSdPromSeriesSet.getGspssResultRebate2());
            } else if ("3".equals(gsphPara)) {
                gspssResultAmt1 = gaiaSdPromSeriesSet.getGspssResultAmt1();
                activeInfo.setResultSubOne(gspssResultAmt1 == null ? null : decimalFormat.format(gspssResultAmt1));
                activeInfo.setResultDisOne(gaiaSdPromSeriesSet.getGspssResultRebate1());
                gspssResultAmt2 = gaiaSdPromSeriesSet.getGspssResultAmt2();
                activeInfo.setResultSubTwo(gspssResultAmt2 == null ? null : decimalFormat.format(gspssResultAmt2));
                activeInfo.setResultDisTwo(gaiaSdPromSeriesSet.getGspssResultRebate2());
                BigDecimal gspssResultAmt3 = gaiaSdPromSeriesSet.getGspssResultAmt3();
                activeInfo.setResultSubThree(gspssResultAmt3 == null ? null : decimalFormat.format(gspssResultAmt3));
                activeInfo.setResultDisThree(gaiaSdPromSeriesSet.getGspssResultRebate3());
            }
        }

        activeInfoList.add(activeInfo);
    }

    /**
     * 添加表格
     *
     * @param inData
     */
    @Override
    @Transactional
    public void createSale(GetCreateSaleOutData inData) {
        GaiaSdSaleH gaiaSdSaleH = inData.getGaiaSdSaleH();
        GaiaSdSaleH gaiaSdSaleHSource = this.gaiaSdSaleHMapper.selectByPrimaryKey(gaiaSdSaleH);
        if (ObjectUtil.isEmpty(gaiaSdSaleHSource)) {
            this.gaiaSdSaleHMapper.insert(gaiaSdSaleH);
        } else {
            gaiaSdSaleHSource.setGsshEmp(gaiaSdSaleH.getGsshEmp());
            gaiaSdSaleHSource.setGsshHykNo(gaiaSdSaleH.getGsshHykNo());
            gaiaSdSaleHSource.setGsshZkAmt(gaiaSdSaleH.getGsshZkAmt());
            gaiaSdSaleHSource.setGsshYsAmt(gaiaSdSaleH.getGsshYsAmt());
            gaiaSdSaleHSource.setGsshEmpGroupName(gaiaSdSaleH.getGsshEmpGroupName());
            this.gaiaSdSaleHMapper.updateByPrimaryKey(gaiaSdSaleHSource);
        }


        GaiaSdSaleD gaiaSdSaleD = new GaiaSdSaleD();
        gaiaSdSaleD.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
        gaiaSdSaleD.setClientId(gaiaSdSaleH.getClientId());
        this.gaiaSdSaleDMapper.delete(gaiaSdSaleD);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String now = formatter.format(date);
        if (CollectionUtil.isNotEmpty(inData.getGiftPromOutDataList())) {
            for (GiftPromOutData item : inData.getGiftPromOutDataList()) {
                List<GaiaSdSaleD> saleDListTemp = new ArrayList<>();
                if ("3".equals(item.getProStorageArea())) {
                    GaiaSdSaleD temp = new GaiaSdSaleD();
                    temp.setClientId(gaiaSdSaleH.getClientId());
                    temp.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
                    temp.setGssdBrId(gaiaSdSaleH.getGsshBrId());
                    temp.setGssdDate(gaiaSdSaleH.getGsshDate());
                    temp.setGssdProId(item.getGssdProId());
                    temp.setGssdSerial(String.valueOf(inData.getGaiaSdSaleDList().size() + 1));
                    temp.setGssdPrc1(item.getGssdPrc1());
                    temp.setGssdPrc2(item.getGssdPrc2());
                    temp.setGssdQty(item.getGssdQty().toString());
                    temp.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(temp.getGssdQty())).setScale(2, RoundingMode.HALF_UP));
                    temp.setGssdDose("1");
                    temp.setGssdPmId("PROM_GIFT");
                    temp.setGssdPmActivityId(item.getGssdPmActivityId());
                    temp.setGssdSalerId(inData.getGaiaSdSaleDList().get(0).getGssdSalerId());
                    temp.setGssdZkAmt(item.getGssdPrc1().multiply(item.getGssdQty()).subtract(temp.getGssdAmt()));
                    temp.setGssdProStatus("促销");
                    temp.setGssdOriQty(item.getGssdQty());
                    temp.setGssdRecipelFlag("0");
                    temp.setGssdMovTax(new BigDecimal(item.getMovTax()));
                    saleDListTemp.add(temp);
                } else {
                    if (item.getGssdQty().compareTo(BigDecimal.ZERO) > 0) {
                        //按批次倒序 自动选择批号库存
                        //查询批次信息
                        Example stockBatchExample = new Example(GaiaSdStockBatch.class);

                        stockBatchExample.createCriteria().andEqualTo("clientId", gaiaSdSaleH.getClientId()).andEqualTo("gssbBrId", gaiaSdSaleH.getGsshBrId())
                                .andEqualTo("gssbProId", item.getGssdProId()).andGreaterThan("gssbQty", "0").andGreaterThan("gssbVaildDate", now);

                        stockBatchExample.setOrderByClause("GSSB_BATCH ASC");
                        List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(stockBatchExample);

                        List<GaiaSdSaleD> saleDList = CopyUtil.copyList(inData.getGaiaSdSaleDList(), GaiaSdSaleD.class);


                        Iterator<GaiaSdStockBatch> stockBatchIterator = stockBatchList.iterator();
                        BigDecimal qtyTemp = item.getGssdQty();
                        while (stockBatchIterator.hasNext()) {
                            GaiaSdStockBatch stockBatch = stockBatchIterator.next();
                            //先扣除商品列表中的批号库存数据
                            if (new BigDecimal(stockBatch.getGssbQty()).compareTo(BigDecimal.ZERO) > 0) {
                                for (int j = 0; j < saleDList.size(); j++) {
                                    if (new BigDecimal(stockBatch.getGssbQty()).compareTo(BigDecimal.ZERO) > 0) {
                                        GaiaSdSaleD saleD = saleDList.get(j);
                                        if (stockBatch.getGssbBatchNo().equals(saleD.getGssdBatchNo()) && saleD.getGssdProId().equals(stockBatch.getGssbProId()) && new BigDecimal(saleD.getGssdQty()).compareTo(BigDecimal.ZERO) > 0) {
                                            if (new BigDecimal(stockBatch.getGssbQty()).compareTo(new BigDecimal(saleD.getGssdQty())) > -1) {
                                                stockBatch.setGssbQty(new BigDecimal(stockBatch.getGssbQty()).subtract(new BigDecimal(saleD.getGssdQty())).toString());
                                                saleD.setGssdQty(BigDecimal.ZERO.toString());
                                            } else {
                                                saleD.setGssdQty(new BigDecimal(saleD.getGssdQty()).subtract(new BigDecimal(stockBatch.getGssbQty())).toString());
                                                stockBatch.setGssbQty(BigDecimal.ZERO.toString());
                                            }
                                        }
                                    } else break;
                                }
                            }
                            //扣减完后 库存大于零的话 可以用于赠品扣减
                            if (new BigDecimal(stockBatch.getGssbQty()).compareTo(BigDecimal.ZERO) > 0) {
                                if (new BigDecimal(stockBatch.getGssbQty()).compareTo(qtyTemp) > -1 && qtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    GaiaSdSaleD saleD = saleDListTemp.stream().filter(saled -> saled.getGssdProId().equals(stockBatch.getGssbProId()) && stockBatch.getGssbBatchNo().equals(saled.getGssdBatchNo())).findFirst().orElse(null);
                                    if (ObjectUtil.isNotEmpty(saleD)) {
                                        saleD.setGssdQty(new BigDecimal(saleD.getGssdQty()).add(qtyTemp).toString());
                                        saleD.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(saleD.getGssdQty())).setScale(2, RoundingMode.HALF_UP));
                                        saleD.setGssdZkAmt(item.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).subtract(saleD.getGssdAmt()));
                                    } else {
                                        GaiaSdSaleD temp = new GaiaSdSaleD();
                                        temp.setClientId(gaiaSdSaleH.getClientId());
                                        temp.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
                                        temp.setGssdBrId(gaiaSdSaleH.getGsshBrId());
                                        temp.setGssdDate(gaiaSdSaleH.getGsshDate());
                                        temp.setGssdProId(item.getGssdProId());
                                        temp.setGssdSerial(String.valueOf(inData.getGaiaSdSaleDList().size() + 1));
                                        temp.setGssdPrc1(item.getGssdPrc1());
                                        temp.setGssdPrc2(item.getGssdPrc2());
                                        temp.setGssdQty(qtyTemp.toString());
                                        temp.setGssdBatchNo(stockBatch.getGssbBatchNo());
                                        temp.setGssdValidDate(stockBatch.getGssbVaildDate());
                                        temp.setGssdAmt(item.getGssdPrc2().multiply(qtyTemp).setScale(2, RoundingMode.HALF_UP));
                                        temp.setGssdZkAmt(item.getGssdPrc1().multiply(qtyTemp).subtract(temp.getGssdAmt()));
                                        temp.setGssdDose("1");
                                        temp.setGssdPmId("PROM_GIFT");
                                        temp.setGssdPmActivityId(item.getGssdPmActivityId());
                                        temp.setGssdSalerId(inData.getGaiaSdSaleDList().get(0).getGssdSalerId());
                                        temp.setGssdProStatus("促销");
                                        temp.setGssdOriQty(qtyTemp);
                                        temp.setGssdRecipelFlag("0");
                                        temp.setGssdMovTax(new BigDecimal(item.getMovTax()));
                                        saleDListTemp.add(temp);
                                    }
                                    qtyTemp = BigDecimal.ZERO;
                                    break;
                                } else {
                                    GaiaSdSaleD saleD = saleDListTemp.stream().filter(saled -> saled.getGssdProId().equals(stockBatch.getGssbProId()) && stockBatch.getGssbBatchNo().equals(saled.getGssdBatchNo())).findFirst().orElse(null);
                                    if (ObjectUtil.isNotEmpty(saleD)) {
                                        saleD.setGssdQty(new BigDecimal(saleD.getGssdQty()).add(new BigDecimal(stockBatch.getGssbQty())).toString());
                                        saleD.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(saleD.getGssdQty())).setScale(2, RoundingMode.HALF_UP));
                                        saleD.setGssdZkAmt(item.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).subtract(saleD.getGssdAmt()));
                                    } else {
                                        GaiaSdSaleD temp = new GaiaSdSaleD();
                                        temp.setClientId(gaiaSdSaleH.getClientId());
                                        temp.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
                                        temp.setGssdBrId(gaiaSdSaleH.getGsshBrId());
                                        temp.setGssdDate(gaiaSdSaleH.getGsshDate());
                                        temp.setGssdProId(item.getGssdProId());
                                        temp.setGssdPrc1(item.getGssdPrc1());
                                        temp.setGssdPrc2(item.getGssdPrc2());
                                        temp.setGssdQty(stockBatch.getGssbQty().toString());
                                        temp.setGssdBatchNo(stockBatch.getGssbBatchNo());
                                        temp.setGssdValidDate(stockBatch.getGssbVaildDate());
                                        temp.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(temp.getGssdQty())).setScale(2, RoundingMode.HALF_UP));
                                        temp.setGssdZkAmt(item.getGssdPrc1().multiply(item.getGssdQty()).subtract(temp.getGssdAmt()));
                                        temp.setGssdDose("1");
                                        temp.setGssdPmId("PROM_GIFT");
                                        temp.setGssdPmActivityId(item.getGssdPmActivityId());
                                        temp.setGssdSalerId(inData.getGaiaSdSaleDList().get(0).getGssdSalerId());
                                        temp.setGssdProStatus("促销");
                                        temp.setGssdOriQty(item.getGssdQty());
                                        temp.setGssdRecipelFlag("0");
                                        temp.setGssdMovTax(new BigDecimal(item.getMovTax()));
                                        saleDListTemp.add(temp);
                                    }
                                    qtyTemp = qtyTemp.subtract(new BigDecimal(stockBatch.getGssbQty()));
                                }
                            }
                        }
                        if (qtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                            //库存数量不足,无法扣减完,无法销售
                            throw new BusinessException(UtilMessage.GIFT_STOCK_LIMIT);
                        }
                        for (int i = 0; i < saleDListTemp.size(); i++) {
                            saleDListTemp.get(i).setGssdSerial(String.valueOf(inData.getGaiaSdSaleDList().size() + i + 1));
                        }
                        inData.getGaiaSdSaleDList().addAll(saleDListTemp);
                    }
                }
            }
        }

        if (StrUtil.isNotBlank(gaiaSdSaleH.getGsshHykNo())) { //判断是否存在会员卡信息
            String cardId = inData.getGaiaSdSaleDList().stream().filter(sdSaleD -> StrUtil.isNotBlank(sdSaleD.getGssdSpecialmedIdcard())).findFirst().map(GaiaSdSaleD::getGssdSpecialmedIdcard).orElse(null);
            if (StrUtil.isNotBlank(cardId)) {
                GaiaSdMemberBasic cardToData = this.gaiaSdMemberBasicMapper.selectBasicByCardId(gaiaSdSaleH.getClientId(), gaiaSdSaleH.getGsshBrId(), gaiaSdSaleH.getGsshHykNo());
                if (ObjectUtil.isNotEmpty(cardToData)) {
                    cardToData.setGsmbCredentials(cardId);
                    this.gaiaSdMemberBasicMapper.updateMember(cardToData);
                }
            }
        }
        List<GaiaDataSynchronized> synchronizedList = new ArrayList<>();
        List<GaiaUserUsemap> usemapList = new ArrayList<>();
        for (int i = 0; i < inData.getGaiaSdSaleDList().size(); ++i) {
            GaiaSdSaleD item = inData.getGaiaSdSaleDList().get(i);
            GaiaUserUsemap usemap = new GaiaUserUsemap();
            usemap.setClient(item.getClientId());
            usemap.setUserId(item.getGssdSalerId());
            usemap.setLastStoreCode(item.getGssdBrId());
            usemap.setLastDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
            usemap.setLastTime(DateUtil.format(new Date(), UtilMessage.HHMMSS));
            usemapList.add(usemap);

            this.gaiaSdSaleDMapper.insert(item);

            Map<String, Object> map = new HashMap<>(8);
            map.put("CLIENT", item.getClientId());
            map.put("USER_ID", item.getGssdSalerId());
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(item.getClientId());
            aSynchronized.setSite(item.getGssdBrId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_USER_USEMAP");
            aSynchronized.setCommand("insert");
            aSynchronized.setSourceNo("销售支付");
            aSynchronized.setDatetime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
            aSynchronized.setArg(JSON.toJSONString(map));
            synchronizedList.add(aSynchronized);

        }
        this.synchronizedMapper.batchInsert(synchronizedList);
        this.gaiaUserUsemapMapper.batchInsert(usemapList);
    }

    /**
     * 单品促销
     *
     * @param inData
     * @return
     */
    @Override
    public GetDisPriceOutData getDisPrice(GetDisPriceInData inData) {
        String clientId = inData.getClientId();
        String gsphVoucherId = inData.getVoucherId();
        GaiaSdPromHead gaiaSdPromHead = new GaiaSdPromHead();
        gaiaSdPromHead.setClientId(clientId);
        gaiaSdPromHead.setGsphBrId(inData.getBrId());
        gaiaSdPromHead.setGsphVoucherId(gsphVoucherId);//促销单号
        gaiaSdPromHead = this.gaiaSdPromHeadMapper.selectByPrimaryKey(gaiaSdPromHead);
        GetDisPriceOutData disPriceOutData = new GetDisPriceOutData();
        disPriceOutData.setGsphExclusion(gaiaSdPromHead.getGsphExclusion());
        BigDecimal money = new BigDecimal(0);
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = new GaiaSdPromUnitarySet();
        if ("PROM_SINGLE".equals(inData.getGsphType())) {
            //单品促销
            gaiaSdPromUnitarySet.setClientId(gaiaSdPromHead.getClientId());
            gaiaSdPromUnitarySet.setGspusVoucherId(gaiaSdPromHead.getGsphVoucherId());
            gaiaSdPromUnitarySet.setGspusProId(inData.getProCode());
            gaiaSdPromUnitarySet.setGspusSerial(inData.getSerial());
            gaiaSdPromUnitarySet = this.gaiaSdPromUnitarySetMapper.selectOne(gaiaSdPromUnitarySet);
            BeanUtils.copyProperties(gaiaSdPromUnitarySet, disPriceOutData);
        } else if ("PROM_SERIES".equals(inData.getGsphType())) {
            //系列促销
            money = this.promSeries(gaiaSdPromHead, inData, disPriceOutData);
        }
        BeanUtils.copyProperties(gaiaSdPromHead, disPriceOutData);
        disPriceOutData.setMoney(inData.getNormalPrice());
        disPriceOutData.setGspusVoucherType(inData.getGsphType());
        return disPriceOutData;
    }


    @Override
    @Transactional
    public void saveRecipelInfo(RecipelInfoInputData recipelInfoInputData) {
        GaiaSdSaleRecipel gaiaSdSaleRecipelTemp;
        if (recipelInfoInputData.isReInput()) {
            gaiaSdSaleRecipelTemp = new GaiaSdSaleRecipel();
            gaiaSdSaleRecipelTemp.setClientId(recipelInfoInputData.getClientId());
            gaiaSdSaleRecipelTemp.setGssrSaleBillNo(recipelInfoInputData.getGssrSaleBillNo());
            List<GaiaSdSaleRecipel> gaiaSdSaleRecipels = this.gaiaSdSaleRecipelMapper.select(gaiaSdSaleRecipelTemp);
            if (ObjectUtil.isNotEmpty(gaiaSdSaleRecipels)) {
                for (int i = 0; i < gaiaSdSaleRecipels.size(); ++i) {
                    GaiaSdSaleRecipel gaiaSdSaleRecipel = (GaiaSdSaleRecipel) gaiaSdSaleRecipels.get(i);
                    gaiaSdSaleRecipel.setGssrCheckStatus("1");
                    this.gaiaSdSaleRecipelMapper.updateByPrimaryKeySelective(gaiaSdSaleRecipel);
                }

                return;
            }
        }

        if (recipelInfoInputData.isFirstInput()) {
            gaiaSdSaleRecipelTemp = new GaiaSdSaleRecipel();
            gaiaSdSaleRecipelTemp.setClientId(recipelInfoInputData.getClientId());
            gaiaSdSaleRecipelTemp.setGssrSaleBillNo(recipelInfoInputData.getGssrSaleBillNo());
            gaiaSdSaleRecipelTemp.setGssrProId(recipelInfoInputData.getGssrBrId());
            this.gaiaSdSaleRecipelMapper.delete(gaiaSdSaleRecipelTemp);
        }

        for (int i = 0; i < recipelInfoInputData.getRecipelDrugsList().size(); ++i) {
            SelectRecipelDrugsOutData selectRecipelDrugsOutData = (SelectRecipelDrugsOutData) recipelInfoInputData.getRecipelDrugsList().get(i);
            GaiaSdSaleRecipel gaiaSdSaleRecipel = new GaiaSdSaleRecipel();
            gaiaSdSaleRecipel.setClientId(recipelInfoInputData.getClientId());
            String nextVoucherId = this.gaiaSdSaleRecipelMapper.selectNextVoucherId();
            gaiaSdSaleRecipel.setGssrVoucherId(nextVoucherId);
            if (ObjectUtil.isEmpty(recipelInfoInputData.getGssrType())) {
                gaiaSdSaleRecipel.setGssrType("1");
            }

            gaiaSdSaleRecipel.setGssrBrId(recipelInfoInputData.getGssrBrId());
            gaiaSdSaleRecipel.setGssrBrName(recipelInfoInputData.getGssrBrName());
            gaiaSdSaleRecipel.setGssrDate(recipelInfoInputData.getGssrDate());
            gaiaSdSaleRecipel.setGssrTime(recipelInfoInputData.getGssrTime());
            gaiaSdSaleRecipel.setGssrEmp(recipelInfoInputData.getGssrEmp());
            gaiaSdSaleRecipel.setGssrSaleBrId(recipelInfoInputData.getGssrSaleBrId());
            gaiaSdSaleRecipel.setGssrSaleDate(recipelInfoInputData.getGssrSaleDate());
            gaiaSdSaleRecipel.setGssrSaleBillNo(recipelInfoInputData.getGssrSaleBillNo());
            gaiaSdSaleRecipel.setGssrCustName(this.nulltoString(recipelInfoInputData.getName()));
            gaiaSdSaleRecipel.setGssrCustSex(this.nulltoString(recipelInfoInputData.getSex()));
            gaiaSdSaleRecipel.setGssrCustAge(this.nulltoString(recipelInfoInputData.getAge()));
            gaiaSdSaleRecipel.setGssrCustIdcard(this.nulltoString(recipelInfoInputData.getCardId()));
            gaiaSdSaleRecipel.setGssrCustMobile(this.nulltoString(recipelInfoInputData.getPhone()));
            gaiaSdSaleRecipel.setGssrCheckStatus(recipelInfoInputData.getGssrCheckStatus());
            gaiaSdSaleRecipel.setGssrProId(selectRecipelDrugsOutData.getProCode());
            gaiaSdSaleRecipel.setGssrBatchNo(this.nulltoString(selectRecipelDrugsOutData.getGssdBatch()));
            gaiaSdSaleRecipel.setGssrQty(this.nulltoString(selectRecipelDrugsOutData.getNum()));
            gaiaSdSaleRecipel.setGssrSymptom(this.nulltoString(recipelInfoInputData.getSymptom()));
            gaiaSdSaleRecipel.setGssrDiagnose(this.nulltoString(recipelInfoInputData.getDiacrisis()));
            gaiaSdSaleRecipel.setGssrType("1");
            gaiaSdSaleRecipel.setGssrPharmacistId(this.nulltoString(recipelInfoInputData.getGssrPharmacistId()));
            gaiaSdSaleRecipel.setGssrPharmacistName(this.nulltoString(recipelInfoInputData.getGssrPharmacistName()));
            gaiaSdSaleRecipel.setGssrRecipelId(this.nulltoString(recipelInfoInputData.getCaseId()));
            gaiaSdSaleRecipel.setGssrRecipelHospital(this.nulltoString(recipelInfoInputData.getHospitalName()));
            gaiaSdSaleRecipel.setGssrRecipelDepartment(this.nulltoString(recipelInfoInputData.getOffice()));
            gaiaSdSaleRecipel.setGssrRecipelDoctor(this.nulltoString(recipelInfoInputData.getDoctor()));
            this.gaiaSdSaleRecipelMapper.insert(gaiaSdSaleRecipel);
        }

    }

    @Override
    @Transactional
    public void updateRecipelInfo(RecipelInfoInputData recipelInfoInputData) {

        for (int i = 0; i < recipelInfoInputData.getRecipelDrugsList().size(); ++i) {
            SelectRecipelDrugsOutData selectRecipelDrugsOutData = (SelectRecipelDrugsOutData) recipelInfoInputData.getRecipelDrugsList().get(i);
            GaiaSdSaleRecipel gaiaSdSaleRecipel = new GaiaSdSaleRecipel();
            gaiaSdSaleRecipel.setClientId(recipelInfoInputData.getClientId());
            gaiaSdSaleRecipel.setGssrType("1");
            gaiaSdSaleRecipel.setGssrBrId(recipelInfoInputData.getGssrBrId());
            gaiaSdSaleRecipel.setGssrBrName(recipelInfoInputData.getGssrBrName());
            gaiaSdSaleRecipel.setGssrDate(recipelInfoInputData.getGssrDate());
            gaiaSdSaleRecipel.setGssrTime(recipelInfoInputData.getGssrTime());
            gaiaSdSaleRecipel.setGssrEmp(recipelInfoInputData.getGssrEmp());
            gaiaSdSaleRecipel.setGssrSaleBrId(recipelInfoInputData.getGssrSaleBrId());
            gaiaSdSaleRecipel.setGssrSaleDate(recipelInfoInputData.getGssrSaleDate());
            gaiaSdSaleRecipel.setGssrSaleBillNo(recipelInfoInputData.getGssrSaleBillNo());
            gaiaSdSaleRecipel.setGssrCustName(this.nulltoString(recipelInfoInputData.getName()));
            gaiaSdSaleRecipel.setGssrCustSex(this.nulltoString(recipelInfoInputData.getSex()));
            gaiaSdSaleRecipel.setGssrCustAge(this.nulltoString(recipelInfoInputData.getAge()));
            gaiaSdSaleRecipel.setGssrCustIdcard(this.nulltoString(recipelInfoInputData.getCardId()));
            gaiaSdSaleRecipel.setGssrCustMobile(this.nulltoString(recipelInfoInputData.getPhone()));
            gaiaSdSaleRecipel.setGssrCheckStatus(recipelInfoInputData.getGssrCheckStatus());
            gaiaSdSaleRecipel.setGssrProId(selectRecipelDrugsOutData.getProCode());
            gaiaSdSaleRecipel.setGssrBatchNo(this.nulltoString(selectRecipelDrugsOutData.getGssdBatch()));
            gaiaSdSaleRecipel.setGssrQty(this.nulltoString(selectRecipelDrugsOutData.getNum()));
            gaiaSdSaleRecipel.setGssrSymptom(this.nulltoString(recipelInfoInputData.getSymptom()));
            gaiaSdSaleRecipel.setGssrDiagnose(this.nulltoString(recipelInfoInputData.getDiacrisis()));
            gaiaSdSaleRecipel.setGssrPharmacistId(this.nulltoString(recipelInfoInputData.getGssrPharmacistId()));
            gaiaSdSaleRecipel.setGssrPharmacistName(this.nulltoString(recipelInfoInputData.getGssrPharmacistName()));
            gaiaSdSaleRecipel.setGssrType("1");
            Integer flag = this.gaiaSdSaleRecipelMapper.checkRecipelCount(gaiaSdSaleRecipel);
            if (flag > 0) {
                gaiaSdSaleRecipel.setGssrVoucherId(recipelInfoInputData.getGssrVoucherId());
                this.gaiaSdSaleRecipelMapper.updateByPrimaryKey(gaiaSdSaleRecipel);
                throw new BusinessException("该记录已经生成过处方单！");
            } else {
                gaiaSdSaleRecipel.setGssrVoucherId(this.gaiaSdSaleRecipelMapper.selectNextVoucherId());
                this.gaiaSdSaleRecipelMapper.insertSelective(gaiaSdSaleRecipel);
            }
            GaiaSdSaleD gaiaSdSaleD = new GaiaSdSaleD();
            gaiaSdSaleD.setClientId(recipelInfoInputData.getClientId());
            gaiaSdSaleD.setGssdBillNo(selectRecipelDrugsOutData.getGssrSaleBillNo());
            gaiaSdSaleD.setGssdBrId(recipelInfoInputData.getGssrBrId());
            gaiaSdSaleD.setGssdDate(selectRecipelDrugsOutData.getGssrSaleDate());
            gaiaSdSaleD.setGssdSerial(selectRecipelDrugsOutData.getSerial());
            gaiaSdSaleD = (GaiaSdSaleD) this.gaiaSdSaleDMapper.selectByPrimaryKey(gaiaSdSaleD);
            gaiaSdSaleD.setGssdRecipelFlag("1");
            this.gaiaSdSaleDMapper.updateByPrimaryKeySelective(gaiaSdSaleD);
        }

    }

    private String nulltoString(String str) {
        return str == null ? "" : str;
    }

    /**
     * 挂单表格一
     *
     * @param inData
     * @return
     */
    @Override
    public List<GetRestOrderInfoOutData> getRestOrder(GaiaSdSaleH inData) {
        List<GaiaSdSaleH> gaiaSdSaleHList = this.gaiaSdSaleHMapper.getAllListByGsshHideFlag2(inData);
        List<GetRestOrderInfoOutData> restOrderInfoOutDataList = new ArrayList();
        for (GaiaSdSaleH gaiaSdSaleH : gaiaSdSaleHList) {
            GetRestOrderInfoOutData outData = new GetRestOrderInfoOutData();
            outData.setGsshBillNo(gaiaSdSaleH.getGsshBillNo());
            outData.setGsshDate(gaiaSdSaleH.getGsshDate());
            outData.setGsshHykNo(gaiaSdSaleH.getGsshHykNo());
            if (ObjectUtil.isNotEmpty(gaiaSdSaleH.getGsshHykNo())) {
                MemberNameByCardIdData byCardId = this.gaiaSdMemberBasicMapper.findMemberNameByCardId(gaiaSdSaleH.getGsshHykNo(), inData.getClientId(), inData.getGsshBrId());
                outData.setGsshHykName(byCardId.getMemberName());//会员名称
                GetQueryMemberInData queryMemberInData = new GetQueryMemberInData();
                queryMemberInData.setClientId(inData.getClientId());
                queryMemberInData.setPhone(byCardId.getPhone());
                queryMemberInData.setMemberId(byCardId.getMemberId());
                GetQueryMemberOutData queryMemberOutData = this.queryMember(queryMemberInData);
                outData.setQueryMemberOutData(queryMemberOutData);
            }
            outData.setGsshZkAmt(String.format("%.2f", gaiaSdSaleH.getGsshZkAmt()));//折让金额
            outData.setGsshNormalAmt(String.format("%.2f", gaiaSdSaleH.getGsshNormalAmt()));
            outData.setGsshTime(gaiaSdSaleH.getGsshTime());
            outData.setGsshYsAmt(String.format("%.2f", gaiaSdSaleH.getGsshYsAmt()));
            restOrderInfoOutDataList.add(outData);
        }
        return restOrderInfoOutDataList;
    }

    /**
     * 挂单表格一
     *
     * @param inData
     * @return
     */
    @Override
    public List<RestOrderInfoOutData> getRestOrder2(GetLoginOutData inData, Map<String, String> map) {
        map.put("client", inData.getClient());
        map.put("brId", inData.getDepId());
        List<RestOrderInfoOutData> outData = this.gaiaSdSaleHMapper.getAllListByGsshHideFlag(map);
        if (CollUtil.isNotEmpty(outData)) {
            outData.forEach(outDatum -> outDatum.setGsshDate(DateUtil.format(DateUtil.parse(outDatum.getGsshDate()), DatePattern.NORM_DATETIME_PATTERN)));
        }
        return outData;
    }

    @Override
    public List<GetRestOrderDetailOutData> getRestOrderDetail(GaiaSdSaleD inData) {
        GaiaSdSaleH gaiaSdSaleH = this.gaiaSdSaleHMapper.selectByBillNo(inData.getGssdBillNo(), inData.getClientId(), inData.getGssdBrId());
        List<GaiaSdSaleD> gaiaSdSaleDList = this.gaiaSdSaleDMapper.getAllByBillNo(inData.getGssdBillNo(), inData.getClientId(), inData.getGssdBrId());
        List<GetRestOrderDetailOutData> outData = new ArrayList();
        for (int k = 0; k < gaiaSdSaleDList.size(); ++k) {
            GaiaSdSaleD item = gaiaSdSaleDList.get(k);
            GetRestOrderDetailOutData orderDetailOutData = new GetRestOrderDetailOutData();
            BeanUtils.copyProperties(item, orderDetailOutData);
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(inData.getClientId());
            gaiaProductBusiness.setProSite(inData.getGssdBrId());
            gaiaProductBusiness.setProSelfCode(orderDetailOutData.getGssdProId());
            gaiaProductBusiness.setProStatus("0");
            gaiaProductBusiness = this.gaiaProductBusinessMapper.selectOne(gaiaProductBusiness);
            BigDecimal ss = null;
            if (ObjectUtil.isNotEmpty(gaiaProductBusiness)) {
                orderDetailOutData.setGssdProName(gaiaProductBusiness.getProName());//商品名称
                orderDetailOutData.setGssdProCommonName(gaiaProductBusiness.getProCommonname()); //通用名称
                orderDetailOutData.setSpec(gaiaProductBusiness.getProSpecs());//规格
                orderDetailOutData.setProFactoryName(gaiaProductBusiness.getProFactoryName());//生产企业
                orderDetailOutData.setProUsage(gaiaProductBusiness.getProUsage());//用法 用量
                orderDetailOutData.setProContraindication(gaiaProductBusiness.getProContraindication());//禁忌说明
                orderDetailOutData.setProStorageAreas(gaiaProductBusiness.getProStorageArea());//中药表示
                orderDetailOutData.setProPresclass(gaiaProductBusiness.getProPresclass());
                orderDetailOutData.setSaleClass(gaiaProductBusiness.getProClass()); //等级
                orderDetailOutData.setProIfMed("1".equals(gaiaProductBusiness.getProIfMed()) ? "✔" : ""); //是否医保
                orderDetailOutData.setGssdMovTax(item.getGssdMovTax().toString());
                orderDetailOutData.setGsshDate(gaiaSdSaleH.getGsshDate());
                orderDetailOutData.setGsshTime(gaiaSdSaleH.getGsshTime());
                orderDetailOutData.setProZdy1(gaiaProductBusiness.getProZdy1());
                orderDetailOutData.setProBdz(gaiaProductBusiness.getProBdz());
                Example example = new Example(GaiaUserData.class);
                example.createCriteria()
                        .andEqualTo("client", inData.getClientId())
                        .andEqualTo("userId", item.getGssdSalerId())
                        .andEqualTo("userSta", "0");
                GaiaUserData userData = this.gaiaUserDataMapper.selectOneByExample(example);
                if (ObjectUtil.isNotEmpty(userData)) {
                    orderDetailOutData.setGssdSalerName(userData.getUserNam());//营业员名称
                }
            }

            GaiaWmsMenDianHuoWeiHao huoWeiHao = gaiaWmsMenDianHuoWeiHaoMapper.selectOne(inData.getClientId(), inData.getGssdBrId(), orderDetailOutData.getGssdProId());
            if (ObjectUtil.isNotEmpty(huoWeiHao)) {
                orderDetailOutData.setWmHwhBm(StrUtil.isBlank(huoWeiHao.getWmHwhBm()) ? "" : huoWeiHao.getWmHwhBm());
            }
            GetQueryProductInData queryProductInData = new GetQueryProductInData();
            queryProductInData.setBrId(inData.getGssdBrId());
            queryProductInData.setClientId(inData.getClientId());
            queryProductInData.setNameOrCode(orderDetailOutData.getGssdProId());
            GetSalesReceiptsTableOutData getSalesReceiptsTableOutData = this.queryProductDetail(queryProductInData);
            getSalesReceiptsTableOutData.setNum(new BigDecimal(item.getGssdQty()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            getSalesReceiptsTableOutData.setGssdDose(item.getGssdDose());
            getSalesReceiptsTableOutData.setGssdOriQty(item.getGssdOriQty().toString());
            getSalesReceiptsTableOutData.setIndex(String.valueOf(k + 1));
            getSalesReceiptsTableOutData.setPrcAmount(NumberUtil.parseNumber(item.getGssdPrc1().toString()).toString());
            orderDetailOutData.setGssdPrc1(String.format("%.2f", item.getGssdPrc2().setScale(2, BigDecimal.ROUND_HALF_UP)));
            getSalesReceiptsTableOutData.setPrice(NumberUtil.parseNumber(item.getGssdPrc2().toString()).toString());
            getSalesReceiptsTableOutData.setTotal(String.format("%.2f", item.getGssdAmt().setScale(2, BigDecimal.ROUND_HALF_UP)));
            getSalesReceiptsTableOutData.setStatus(item.getGssdProStatus());
            getSalesReceiptsTableOutData.setGssdBatchNo(item.getGssdBatchNo());//批号
            getSalesReceiptsTableOutData.setGssdValidDate(item.getGssdValidDate());
            getSalesReceiptsTableOutData.setGssdSalerId(item.getGssdSalerId());//营业员id
            getSalesReceiptsTableOutData.setProPosition(StrUtil.isBlank(gaiaProductBusiness.getProPosition()) ? "" : gaiaProductBusiness.getProPosition());
            getSalesReceiptsTableOutData.setGssdDoctorId(item.getGssdDoctorId());//医生id
            getSalesReceiptsTableOutData.setProStatus(item.getGssdProStatus());
            getSalesReceiptsTableOutData.setGssdSpecialmedIdcard(item.getGssdSpecialmedIdcard());
            getSalesReceiptsTableOutData.setGssdSpecialmedName(item.getGssdSpecialmedName());
            getSalesReceiptsTableOutData.setGssdSpecialmedSex(item.getGssdSpecialmedSex());
            getSalesReceiptsTableOutData.setGssdSpecialmedBirthday(item.getGssdSpecialmedBirthday());
            getSalesReceiptsTableOutData.setGssdSpecialmedMobile(item.getGssdSpecialmedMobile());
            getSalesReceiptsTableOutData.setGssdSpecialmedAddress(item.getGssdSpecialmedAddress());
            getSalesReceiptsTableOutData.setGssdSpecialmedEcode(item.getGssdSpecialmedEcode());
            GaiaSdStock sdStock = new GaiaSdStock();
            sdStock.setClientId(inData.getClientId());
            sdStock.setGssBrId(inData.getGssdBrId());
            sdStock.setGssProId(item.getGssdProId());
            GaiaSdStock gaiaSdStock = this.gaiaSdStockMapper.selectByPrimaryKey(sdStock);
            if (ObjectUtil.isNotEmpty(gaiaSdStock)) {
                getSalesReceiptsTableOutData.setGssQty(gaiaSdStock.getGssQty());
            }
            if (!"3".equals(gaiaProductBusiness.getProStorageArea())) {
                GaiaSdStockBatch sdStockBatch = this.stockBatchMapper.getAllByBatchNo(inData.getClientId(), inData.getGssdBrId(), item.getGssdProId(), item.getGssdBatchNo());
                if (ObjectUtil.isNotEmpty(sdStockBatch)) {
                    getSalesReceiptsTableOutData.setBatchNoQty(sdStockBatch.getGssbQty());
                }
            }

            if (StrUtil.isNotBlank(item.getGssdDoctorId())) {
                Example example2 = new Example(GaiaUserData.class);
                example2.createCriteria()
                        .andEqualTo("client", inData.getClientId())
                        .andEqualTo("userId", item.getGssdDoctorId())
                        .andEqualTo("userSta", "0");
                GaiaUserData userData2 = this.gaiaUserDataMapper.selectOneByExample(example2);
                if (ObjectUtil.isNotEmpty(userData2)) {
                    getSalesReceiptsTableOutData.setDoctorName(userData2.getUserNam());//医生名称
                }
            }
            getSalesReceiptsTableOutData.setGssdPmActivityId(item.getGssdPmActivityId());
            getSalesReceiptsTableOutData.setGssdPmActivityFlag(item.getGssdPmActivityFlag());
            getSalesReceiptsTableOutData.setGssdPmId(item.getGssdPmId());//促销id
            getSalesReceiptsTableOutData.setDisc(item.getGssdStCode());
            GaiaUserData gaiaUserData;
            if (ObjectUtil.isNotEmpty(orderDetailOutData.getGssdSalerId())) {
                getSalesReceiptsTableOutData.setEmpId(orderDetailOutData.getGssdSalerId());
                gaiaUserData = new GaiaUserData();
                gaiaUserData.setClient(inData.getClientId());
                gaiaUserData.setUserId(orderDetailOutData.getGssdSalerId());
                gaiaUserData = this.gaiaUserDataMapper.selectOne(gaiaUserData);
                getSalesReceiptsTableOutData.setEmpName(gaiaUserData.getUserNam());
            }

            if (ObjectUtil.isNotEmpty(orderDetailOutData.getGssdDoctorId())) {
                getSalesReceiptsTableOutData.setDoctorId(orderDetailOutData.getGssdDoctorId());
                gaiaUserData = new GaiaUserData();
                gaiaUserData.setClient(inData.getClientId());
                gaiaUserData.setUserId(orderDetailOutData.getGssdDoctorId());
                gaiaUserData = this.gaiaUserDataMapper.selectOne(gaiaUserData);
                getSalesReceiptsTableOutData.setDoctorName(gaiaUserData.getUserNam());
            }

            GetQueryProductInData pdInData = new GetQueryProductInData();
            pdInData.setClientId(inData.getClientId());
            pdInData.setBrId(inData.getGssdBrId());
            pdInData.setNameOrCode(orderDetailOutData.getGssdProId());
            pdInData.setGssbBatchNo(orderDetailOutData.getGssdBatchNo());
            List<GetPdAndExpOutData> pdAndExpOutDataList01 = this.gaiaProductBusinessMapper.queryBatchNoAndExp(pdInData);
            if (pdAndExpOutDataList01.size() > 0) {
                GetPdAndExpOutData pdAndExpOutData = pdAndExpOutDataList01.get(0);
                getSalesReceiptsTableOutData.setPdAndExpData(pdAndExpOutData);
            }
            GetQueryProductInData beInData = new GetQueryProductInData();
            beInData.setClientId(inData.getClientId());
            beInData.setBrId(inData.getGssdBrId());
            pdInData.setNameOrCode(orderDetailOutData.getGssdProId());
            List<GetPdAndExpOutData> pdAndExpOutDataList = this.queryBatchNoAndExp(beInData);
            getSalesReceiptsTableOutData.setPdAndExpOutDataList(pdAndExpOutDataList);
            orderDetailOutData.setGetSalesReceiptsTableOutData(getSalesReceiptsTableOutData);
            outData.add(orderDetailOutData);
        }


        BigDecimal count = BigDecimal.ZERO;
        if (ObjectUtil.isNotEmpty(gaiaSdSaleH)) {
            if (ObjectUtil.isNotEmpty(gaiaSdSaleH.getGsshCallQty())) {
                count = new BigDecimal(gaiaSdSaleH.getGsshCallQty()).add(new BigDecimal(1));
            }
            gaiaSdSaleH.setGsshCallQty(String.valueOf(count.setScale(0, RoundingMode.CEILING)));//调用次数
            this.gaiaSdSaleHMapper.updateByPrimaryKey(gaiaSdSaleH);
        }

        return outData;
    }

    /**
     * 获得拆零价格
     *
     * @param inData
     * @return
     */
    @Override
    public String afterSplitePrice(GaiaRetailPriceInData inData) {
        String money = this.gaiaRetailPriceMapper.afterSplitePrice(inData);
        if (ObjectUtil.isNotEmpty(money)) {
            money = String.format("%.2f", Double.valueOf(money));
        }
        return money;
    }

    @Override
    public String getStoreDiscount(GaiaRetailPrice inData) {
        Example example = new Example(GaiaSdStoreData.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gsstBrId", inData.getPrcStore());
        GaiaSdStoreData storeData = (GaiaSdStoreData) this.sdStoreDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(storeData)) {
            throw new BusinessException("未获取到对应门店最小折扣率！");
        } else {
            return storeData.getGsstMinDiscountRate();
        }
    }

    @Override
    public String getStoreDyqMaxAmount(GaiaRetailPrice inData) {
        Example example = new Example(GaiaSdStoreData.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gsstBrId", inData.getPrcStore());
        GaiaSdStoreData storeData = (GaiaSdStoreData) this.sdStoreDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(storeData)) {
            throw new BusinessException("未获取到对应门店最大抵用金额！");
        } else {
            return storeData.getGsstDyqMaxAmt().toString();
        }
    }

    @Override
    @Transactional
    public void approveChangePrice(ApproveChangePriceInData inData, String token) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoreCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(storeData)) {
            throw new BusinessException("门店不存在！");
        } else {
            GetWfCreateInData wfCreateInData = new GetWfCreateInData();
            wfCreateInData.setToken(token);
            wfCreateInData.setWfOrder(inData.getBillNo());
            wfCreateInData.setWfSite(inData.getStoreCode());
            if (ObjectUtil.isEmpty(storeData.getStoChainHead())) {
                wfCreateInData.setWfDefineCode("GAIA_WF_009");
            } else {
                wfCreateInData.setWfDefineCode("GAIA_WF_008");
            }

            wfCreateInData.setWfTitle("单品改价工作流");
            wfCreateInData.setWfDescription("单品改价工作流");
            List<GetWfCreateDpgjCompadmInData> details = new ArrayList();
            Example example1 = new Example(GaiaSdSaleD.class);
            example1.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBillNo", inData.getBillNo()).andEqualTo("gssdBrId", inData.getStoreCode()).andEqualTo("gssdDate", DateUtil.format(new Date(), "yyyyMMdd")).andEqualTo("gssdSerial", inData.getIndex());
            GaiaSdSaleD saleD = (GaiaSdSaleD) this.gaiaSdSaleDMapper.selectOneByExample(example1);
            Example examplePro = new Example(GaiaProductBusiness.class);
            examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSite", inData.getStoreCode()).andEqualTo("proSelfCode", saleD.getGssdProId());
            GaiaProductBusiness productBasic = (GaiaProductBusiness) this.gaiaProductBusinessMapper.selectOneByExample(examplePro);
            GetWfCreateDpgjCompadmInData compadmInData = new GetWfCreateDpgjCompadmInData();
            compadmInData.setSaleDate(DateUtil.format(new Date(), "yyyyMMdd"));
            compadmInData.setChangeSalePrice(inData.getChangePrice());
            compadmInData.setChangeTotalPrice((new BigDecimal(inData.getChangePrice())).multiply(new BigDecimal(saleD.getGssdQty())).toString());
            compadmInData.setQty(saleD.getGssdQty());
            compadmInData.setProCode(saleD.getGssdProId());
            compadmInData.setSaleOrder(saleD.getGssdBillNo());
            compadmInData.setSalePrice(String.format("%.2f", saleD.getGssdPrc1()));
            if (ObjectUtil.isNotEmpty(productBasic)) {
                compadmInData.setProName(productBasic.getProName());
                compadmInData.setProSpecs(productBasic.getProSpecs());
            }

            if (ObjectUtil.isNotEmpty(storeData.getStoChainHead())) {
                compadmInData.setStoreId(saleD.getGssdBrId());
            }

            compadmInData.setTotalPrice(saleD.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).toString());
            details.add(compadmInData);
            wfCreateInData.setWfDetail(details);
            String result = this.authService.createWorkflow(wfCreateInData);
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (!"0".equals(jsonObject.getString("code"))) {
                throw new BusinessException(jsonObject.getString("message"));
            } else {
                saleD.setGssdAmt((new BigDecimal(inData.getChangePrice())).multiply(new BigDecimal(saleD.getGssdQty())));
                saleD.setGssdPrc2(new BigDecimal(inData.getChangePrice()));
                this.gaiaSdSaleDMapper.updateByPrimaryKeySelective(saleD);
                Example exampleH = new Example(GaiaSdSaleH.class);
                exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBillNo", inData.getBillNo());
                GaiaSdSaleH saleH = (GaiaSdSaleH) this.gaiaSdSaleHMapper.selectOneByExample(exampleH);
                saleH.setGsshCallAllow("0");
                this.gaiaSdSaleHMapper.updateByPrimaryKeySelective(saleH);
            }
        }
    }

    @Override
    @Transactional
    public void approveChangeRate(ApproveChangePriceInData inData, String token) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoreCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(storeData)) {
            throw new BusinessException("门店不存在！");
        } else {
            GetWfCreateInData wfCreateInData = new GetWfCreateInData();
            wfCreateInData.setToken(token);
            wfCreateInData.setWfOrder(inData.getBillNo());
            wfCreateInData.setWfSite(inData.getStoreCode());
            if (ObjectUtil.isEmpty(storeData.getStoChainHead())) {
                wfCreateInData.setWfDefineCode("GAIA_WF_011");
            } else {
                wfCreateInData.setWfDefineCode("GAIA_WF_010");
            }

            wfCreateInData.setWfTitle("单品折率工作流");
            wfCreateInData.setWfDescription("单品折率工作流");
            List<GetWfCreateDpzlCompadmInData> details = new ArrayList();
            Example example1 = new Example(GaiaSdSaleD.class);
            example1.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBillNo", inData.getBillNo()).andEqualTo("gssdBrId", inData.getStoreCode()).andEqualTo("gssdDate", DateUtil.format(new Date(), "yyyyMMdd")).andEqualTo("gssdSerial", inData.getIndex());
            GaiaSdSaleD saleD = (GaiaSdSaleD) this.gaiaSdSaleDMapper.selectOneByExample(example1);
            Example examplePro = new Example(GaiaProductBusiness.class);
            examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSite", inData.getStoreCode()).andEqualTo("proSelfCode", saleD.getGssdProId());
            GaiaProductBusiness productBasic = (GaiaProductBusiness) this.gaiaProductBusinessMapper.selectOneByExample(examplePro);
            GetWfCreateDpzlCompadmInData compadmInData = new GetWfCreateDpzlCompadmInData();
            compadmInData.setSaleDate(DateUtil.format(new Date(), "yyyyMMdd"));
            compadmInData.setChangeSalePrice((new BigDecimal(inData.getChangeRate())).divide(new BigDecimal(100)).multiply(saleD.getGssdPrc1()).toString());
            compadmInData.setChangeTotalPrice((new BigDecimal(compadmInData.getChangeSalePrice())).multiply(new BigDecimal(saleD.getGssdQty())).toString());
            compadmInData.setQty(saleD.getGssdQty());
            compadmInData.setProCode(saleD.getGssdProId());
            compadmInData.setSaleOrder(saleD.getGssdBillNo());
            compadmInData.setSalePrice(String.format("%.2f", saleD.getGssdPrc1()));
            compadmInData.setRate(inData.getChangeRate());
            if (ObjectUtil.isNotEmpty(productBasic)) {
                compadmInData.setProName(productBasic.getProName());
                compadmInData.setProSpecs(productBasic.getProSpecs());
            }

            if (ObjectUtil.isNotEmpty(storeData.getStoChainHead())) {
                compadmInData.setStoreId(saleD.getGssdBrId());
            }

            compadmInData.setTotalPrice(saleD.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).toString());
            details.add(compadmInData);
            wfCreateInData.setWfDetail(details);
            String result = this.authService.createWorkflow(wfCreateInData);
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (!"0".equals(jsonObject.getString("code"))) {
                throw new BusinessException(jsonObject.getString("message"));
            } else {
                saleD.setGssdPrc2((new BigDecimal(inData.getChangeRate())).divide(new BigDecimal(100)).multiply(saleD.getGssdPrc2()));
                saleD.setGssdAmt(saleD.getGssdPrc2().multiply(new BigDecimal(saleD.getGssdQty())));
                this.gaiaSdSaleDMapper.updateByPrimaryKeySelective(saleD);
                Example exampleH = new Example(GaiaSdSaleH.class);
                exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBillNo", inData.getBillNo());
                GaiaSdSaleH saleH = (GaiaSdSaleH) this.gaiaSdSaleHMapper.selectOneByExample(exampleH);
                saleH.setGsshCallAllow("0");
                this.gaiaSdSaleHMapper.updateByPrimaryKeySelective(saleH);
            }
        }
    }

    @Override
    @Transactional
    public void approveChangeAllRate(ApproveChangePriceInData inData, String token) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoreCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(storeData)) {
            throw new BusinessException("门店不存在！");
        } else {
            GetWfCreateInData wfCreateInData = new GetWfCreateInData();
            wfCreateInData.setToken(token);
            wfCreateInData.setWfOrder(inData.getBillNo());
            wfCreateInData.setWfSite(inData.getStoreCode());
            if (ObjectUtil.isEmpty(storeData.getStoChainHead())) {
                wfCreateInData.setWfDefineCode("GAIA_WF_013");
            } else {
                wfCreateInData.setWfDefineCode("GAIA_WF_012");
            }

            wfCreateInData.setWfTitle("整单折率工作流");
            wfCreateInData.setWfDescription("整单折率工作流");
            List<GetWfCreateZdzlCompadmInData> details = new ArrayList();
            Example example1 = new Example(GaiaSdSaleD.class);
            example1.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBillNo", inData.getBillNo()).andEqualTo("gssdBrId", inData.getStoreCode()).andEqualTo("gssdDate", DateUtil.format(new Date(), "yyyyMMdd"));
            List<GaiaSdSaleD> saleDList = this.gaiaSdSaleDMapper.selectByExample(example1);
            Iterator var9 = saleDList.iterator();

            while (var9.hasNext()) {
                GaiaSdSaleD saleD = (GaiaSdSaleD) var9.next();
                Example examplePro = new Example(GaiaProductBusiness.class);
                examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", saleD.getGssdBrId()).andEqualTo("proSite", inData.getStoreCode());
                GaiaProductBusiness productBusiness = (GaiaProductBusiness) this.gaiaProductBusinessMapper.selectOneByExample(examplePro);
                GetWfCreateZdzlCompadmInData compadmInData = new GetWfCreateZdzlCompadmInData();
                compadmInData.setSaleDate(DateUtil.format(new Date(), "yyyyMMdd"));
                compadmInData.setChangeTotalPrice((new BigDecimal(inData.getChangeRate())).divide(new BigDecimal(100)).multiply(saleD.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty()))).toString());
                compadmInData.setQty(saleD.getGssdQty());
                compadmInData.setProCode(saleD.getGssdProId());
                compadmInData.setSaleOrder(saleD.getGssdBillNo());
                compadmInData.setRate(inData.getChangeRate());
                if (ObjectUtil.isNotEmpty(productBusiness)) {
                    compadmInData.setProName(productBusiness.getProName());
                    compadmInData.setProSpecs(productBusiness.getProSpecs());
                }

                if (ObjectUtil.isNotEmpty(storeData.getStoChainHead())) {
                    compadmInData.setStoreId(saleD.getGssdBrId());
                }

                compadmInData.setTotalPrice(String.format("%.2f", saleD.getGssdAmt()));
                details.add(compadmInData);
                saleD.setGssdPrc2((new BigDecimal(inData.getChangeRate())).divide(new BigDecimal(100)).multiply(saleD.getGssdPrc1()));
                saleD.setGssdAmt(saleD.getGssdPrc2().multiply(new BigDecimal(saleD.getGssdQty())));
                this.gaiaSdSaleDMapper.updateByPrimaryKeySelective(saleD);
            }

            Example exampleH = new Example(GaiaSdSaleH.class);
            exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBillNo", inData.getBillNo());
            GaiaSdSaleH saleH = (GaiaSdSaleH) this.gaiaSdSaleHMapper.selectOneByExample(exampleH);
            saleH.setGsshCallAllow("0");
            this.gaiaSdSaleHMapper.updateByPrimaryKeySelective(saleH);
            wfCreateInData.setWfDetail(details);
            String result = this.authService.createWorkflow(wfCreateInData);
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (!"0".equals(jsonObject.getString("code"))) {
                throw new BusinessException(jsonObject.getString("message"));
            }
        }
    }

    private GetDisPriceOutData promCouponProType1(GaiaSdPromHead gaiaSdPromHead, GetPromByProCodeOutData item, GetDisPriceInData inData) {
        int gsphPart = Integer.valueOf(gaiaSdPromHead.getGsphPart());
        GaiaSdPromCouponUse gaiaSdPromCouponUse = new GaiaSdPromCouponUse();
        gaiaSdPromCouponUse.setClientId(gaiaSdPromHead.getClientId());
        gaiaSdPromCouponUse.setGspcuVoucherId(gaiaSdPromHead.getGsphVoucherId());
        gaiaSdPromCouponUse.setGspcuProId(inData.getProCode());
        gaiaSdPromCouponUse.setGspcuSerial(item.getSerial());
        gaiaSdPromCouponUse = (GaiaSdPromCouponUse) this.gaiaSdPromCouponUseMapper.selectOne(gaiaSdPromCouponUse);
        GaiaSdPromCouponSet gaiaSdPromCouponSet = new GaiaSdPromCouponSet();
        gaiaSdPromCouponSet.setClientId(gaiaSdPromCouponUse.getClientId());
        gaiaSdPromCouponSet.setGspcsVoucherId(gaiaSdPromCouponUse.getGspcuVoucherId());
        gaiaSdPromCouponSet.setGspcsActNo(gaiaSdPromCouponUse.getGspcuActNo());
        gaiaSdPromCouponSet = (GaiaSdPromCouponSet) this.gaiaSdPromCouponSetMapper.selectOne(gaiaSdPromCouponSet);
        int numTotal = Integer.valueOf(inData.getNum());
        int count = 0;
        BigDecimal moneyTotal = (new BigDecimal(inData.getNormalPrice())).multiply(new BigDecimal(numTotal));
        if ("repeat1".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                PromCouponProType.gsphPart1Repeat1Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            } else if (2 == gsphPart) {
                PromCouponProType.gsphPart2Repeat1Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            } else if (3 == gsphPart) {
                PromCouponProType.gsphPart3Repeat1Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            }
        } else if ("repeat2".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                PromCouponProType.gsphPart1Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            } else if (2 == gsphPart) {
                PromCouponProType.gsphPart2Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            } else if (3 == gsphPart) {
                PromCouponProType.gsphPart3Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            }
        }

        return null;
    }

    private String promCouponProType0(GetNumAmtData numAmtData, GiftCardOutData giftCardOutData) {
        GaiaSdPromHead gaiaSdPromHead = new GaiaSdPromHead();
        gaiaSdPromHead.setClientId(giftCardOutData.getClientId());
        gaiaSdPromHead.setGsphBrId(giftCardOutData.getBrId());
        gaiaSdPromHead.setGsphVoucherId(giftCardOutData.getGsphVoucherId());
        gaiaSdPromHead = (GaiaSdPromHead) this.gaiaSdPromHeadMapper.selectByPrimaryKey(gaiaSdPromHead);
        int gsphPart = Integer.valueOf(gaiaSdPromHead.getGsphPart());
        GaiaSdPromCouponGrant gaiaSdPromCouponGrant = new GaiaSdPromCouponGrant();
        gaiaSdPromCouponGrant.setClientId(gaiaSdPromHead.getClientId());
        gaiaSdPromCouponGrant.setGspcgVoucherId(gaiaSdPromHead.getGsphVoucherId());
        gaiaSdPromCouponGrant.setGspcgProId(giftCardOutData.getProId());
        gaiaSdPromCouponGrant.setGspcgSerial(giftCardOutData.getSerial());
        gaiaSdPromCouponGrant = (GaiaSdPromCouponGrant) this.gaiaSdPromCouponGrantMapper.selectOne(gaiaSdPromCouponGrant);
        Float numTotal = Float.valueOf(numAmtData.getNum());
        int count = 0;
        BigDecimal moneyTotal = new BigDecimal(numAmtData.getMoney());
        if ("repeat1".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                count = PromCouponProType.gsphPart1Repeat1Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            } else if (2 == gsphPart) {
                count = PromCouponProType.gsphPart2Repeat1Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            } else if (3 == gsphPart) {
                count = PromCouponProType.gsphPart3Repeat1Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            }
        } else if ("repeat2".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                count = PromCouponProType.gsphPart1Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            } else if (2 == gsphPart) {
                count = PromCouponProType.gsphPart2Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            } else if (3 == gsphPart) {
                count = PromCouponProType.gsphPart3Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            }
        }

        return String.valueOf(count);
    }

    private int promGift(GaiaSdPromHead gaiaSdPromHead, GetPromByProCodeOutData item, Float numTotal, BigDecimal totalMoney) {
        int gsphPart = Integer.valueOf(gaiaSdPromHead.getGsphPart());
        GaiaSdPromGiftConds gaiaSdPromGiftConds = new GaiaSdPromGiftConds();
        gaiaSdPromGiftConds.setClientId(gaiaSdPromHead.getClientId());
        gaiaSdPromGiftConds.setGspgcVoucherId(gaiaSdPromHead.getGsphVoucherId());
        gaiaSdPromGiftConds.setGspgcSerial(item.getSerial());
        gaiaSdPromGiftConds = (GaiaSdPromGiftConds) this.gaiaSdPromGiftCondsMapper.selectOne(gaiaSdPromGiftConds);
        GaiaSdPromGiftSet gaiaSdPromGiftSet = new GaiaSdPromGiftSet();
        gaiaSdPromGiftSet.setClientId(gaiaSdPromGiftConds.getClientId());
        gaiaSdPromGiftSet.setGspgsVoucherId(gaiaSdPromGiftConds.getGspgcVoucherId());
        gaiaSdPromGiftSet.setGspgsSeriesProId(gaiaSdPromGiftConds.getGspgcSeriesId());
        gaiaSdPromGiftSet = (GaiaSdPromGiftSet) this.gaiaSdPromGiftSetMapper.selectOne(gaiaSdPromGiftSet);
        int count = 0;
        if ("repeat1".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                count = PromGift.gsphPart1Repeat1(numTotal, totalMoney, gaiaSdPromGiftSet);
            } else if (2 == gsphPart) {
                count = PromGift.gsphPart2Repeat1(numTotal, totalMoney, gaiaSdPromGiftSet);
            } else if (3 == gsphPart) {
                count = PromGift.gsphPart3Repeat1(numTotal, totalMoney, gaiaSdPromGiftSet);
            }
        } else if ("repeat2".equals(gaiaSdPromHead.getGsphPara1()) || "repeat3".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                count = PromGift.gsphPart1Repeat2(numTotal, totalMoney, gaiaSdPromGiftSet);
            } else if (2 == gsphPart) {
                count = PromGift.gsphPart2Repeat2(numTotal, totalMoney, gaiaSdPromGiftSet);
            } else if (3 == gsphPart) {
                count = PromGift.gsphPart3Repeat2(numTotal, totalMoney, gaiaSdPromGiftSet);
            }
        }

        return count;
    }

    private BigDecimal promSeries(GaiaSdPromHead gaiaSdPromHead, GetDisPriceInData inData, GetDisPriceOutData disPriceOutData) {
        String serial = inData.getSerial();
        //阶梯
        int gsphPart = Integer.valueOf(gaiaSdPromHead.getGsphPart());
        GaiaSdPromSeriesSet gaiaSdPromSeriesSet = new GaiaSdPromSeriesSet();
        gaiaSdPromSeriesSet.setClientId(inData.getClientId());
        gaiaSdPromSeriesSet.setGspssVoucherId(inData.getVoucherId());//促销单号
        gaiaSdPromSeriesSet.setGspssSerial(serial);
        gaiaSdPromSeriesSet = (GaiaSdPromSeriesSet) this.gaiaSdPromSeriesSetMapper.selectOne(gaiaSdPromSeriesSet);
        disPriceOutData.setAllPro(gaiaSdPromHead.getGsphPara3());
        float numTotal = 0.0F;
        BigDecimal moneyTotal = new BigDecimal(0);
        Map<String, GetNumAmtData> map = inData.getNumAmtDataMap();
        GetNumAmtData item;
        if ("assign1".equals(gaiaSdPromHead.getGsphPara3())) {
            for (Iterator var10 = map.values().iterator(); var10.hasNext(); moneyTotal = moneyTotal.add(new BigDecimal(item.getMoney()))) {
                item = (GetNumAmtData) var10.next();
                numTotal += Float.valueOf(item.getNum());
            }
        } else if ("assign2".equals(gaiaSdPromHead.getGsphPara3())) {
            Set<String> proIds = new HashSet();
            GaiaSdPromSeriesConds gaiaSdPromSeriesConds = new GaiaSdPromSeriesConds();
            gaiaSdPromSeriesConds.setClientId(gaiaSdPromHead.getClientId());
            gaiaSdPromSeriesConds.setGspscVoucherId(gaiaSdPromHead.getGsphVoucherId());
            List<GaiaSdPromSeriesConds> gaiaSdPromSeriesCondsList = this.gaiaSdPromSeriesCondsMapper.select(gaiaSdPromSeriesConds);

            for (int i = 0; i < gaiaSdPromSeriesCondsList.size(); ++i) {
                GaiaSdPromSeriesConds promItem = (GaiaSdPromSeriesConds) gaiaSdPromSeriesCondsList.get(i);
                GetNumAmtData numAmtData = (GetNumAmtData) map.get(promItem.getGspscProId());
                if (ObjectUtil.isNotEmpty(numAmtData)) {
                    numTotal += Float.valueOf(numAmtData.getNum());//数量总数
                    moneyTotal = moneyTotal.add(new BigDecimal(numAmtData.getMoney()));//总价
                    proIds.add(promItem.getGspscProId());
                }
            }

            disPriceOutData.setProIds(proIds);
        }

        BigDecimal price = new BigDecimal(0);
        if ("repeat1".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                price = PromSeries.gsphPart1Repeat1(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else if (2 == gsphPart) {
                price = PromSeries.gsphPart2Repeat1(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else if (3 == gsphPart) {
                price = PromSeries.gsphPart3Repeat1(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            }
        } else if ("repeat2".equals(gaiaSdPromHead.getGsphPara1()) || "repeat3".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                price = PromSeries.gsphPart1Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else if (2 == gsphPart) {
                price = PromSeries.gsphPart2Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            } else if (3 == gsphPart) {
                price = PromSeries.gsphPart3Repeat2(numTotal, moneyTotal, gaiaSdPromSeriesSet);
            }
        }

        return price;
    }

    /**
     * 促销功能
     *
     * @param gaiaSdPromHead
     * @param inData
     * @return
     */
    private BigDecimal promSingle(GaiaSdPromHead gaiaSdPromHead, GetDisPriceInData inData, GetDisPriceOutData disPriceOutData) {
        String serial = inData.getSerial();
        BigDecimal normalPrice = new BigDecimal(inData.getNormalPrice());
        BigDecimal money = new BigDecimal(0);
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = new GaiaSdPromUnitarySet();
        gaiaSdPromUnitarySet.setClientId(gaiaSdPromHead.getClientId());
        gaiaSdPromUnitarySet.setGspusVoucherId(gaiaSdPromHead.getGsphVoucherId());
        gaiaSdPromUnitarySet.setGspusProId(inData.getProCode());
        gaiaSdPromUnitarySet.setGspusSerial(inData.getSerial());
        gaiaSdPromUnitarySet = (GaiaSdPromUnitarySet) this.gaiaSdPromUnitarySetMapper.selectOne(gaiaSdPromUnitarySet);

        System.out.println(" BigDecimal promSingle " + gaiaSdPromUnitarySet);
        //数量
        float num = Float.valueOf(inData.getNum());
        //阶梯 一段 两段 三段
        int gsphPart = Integer.valueOf(gaiaSdPromHead.getGsphPart());
        PromSingleInData singleInData = new PromSingleInData();
        singleInData.setGaiaSdPromUnitarySet(gaiaSdPromUnitarySet);
        singleInData.setMoney(money);
        singleInData.setNormalPrice(normalPrice);
        singleInData.setNum(num);
        //循环生效
        if ("repeat1".equals(gaiaSdPromHead.getGsphPara1())) {
            if (1 == gsphPart) {
                money = PromSingle.gsphPart1Repeat1(singleInData);
            } else if (2 == gsphPart) {
                money = PromSingle.gsphPart2Repeat1(singleInData);
            } else if (3 == gsphPart) {
                money = PromSingle.gsphPart3Repeat1(singleInData);
            }
            //按最后价格折扣生效
        } else if ("repeat2".equals(gaiaSdPromHead.getGsphPara1())) {
            if (3 == gsphPart) {
                money = PromSingle.gsphPart3Repeat2(singleInData);
            } else if (2 == gsphPart) {
                money = PromSingle.gsphPart2Repeat2(singleInData);
            } else if (1 == gsphPart) {
                money = PromSingle.gsphPart1Repeat2(singleInData);
            }
            //恢复原价
        } else if ("repeat3".equals(gaiaSdPromHead.getGsphPara1())) {
            if (3 == gsphPart) {
                money = PromSingle.gsphPart3Repeat3(singleInData);
            } else if (2 == gsphPart) {
                money = PromSingle.gsphPart2Repeat2(singleInData);
            } else if (1 == gsphPart) {
                money = PromSingle.gsphPart1Repeat2(singleInData);
            }
        } else {
            //  原价
            money = normalPrice.multiply(new BigDecimal((double) num));
        }

        return money;
    }

    @Override
    public int checkPrintBill(CheckParamData inData) {
        GaiaSdSaleH gaiaSdSaleH = this.gaiaSdSaleHMapper.selectByBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        if (ObjectUtil.isEmpty(gaiaSdSaleH)) {
            return 0;
        } else {
            String gsshHideFlag = gaiaSdSaleH.getGsshHideFlag();
            String gsshCallAllow = gaiaSdSaleH.getGsshCallAllow();
            if ("1".equals(gsshHideFlag)) {
                return 1;
            } else if ("2".equals(gsshHideFlag)) {
                return 2;
            } else {
                return "0".equals(gsshCallAllow) ? 3 : 4;
            }
        }
    }

    @Override
    public List<GaiaStoreData> otherShops(GetLoginOutData userInfo) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", userInfo.getClient()).andNotEqualTo("stoCode", userInfo.getDepId()).andEqualTo("stoStatus", "0");
        List<GaiaStoreData> storeDatas = this.storeDataMapper.selectByExample(example);
        return storeDatas;
    }

    @Override
    public List<GetGiftCardOutData> giftCardByProCode(GetQueryProductInData inData) {
        List<GetGiftCardOutData> giftCardOutDataList = new ArrayList();
        Map<String, GetNumAmtData> proNumAmtMap = inData.getProNumAmtMap();
        Iterator var4 = proNumAmtMap.keySet().iterator();

        while (true) {
            GetNumAmtData numAmtData;
            List outData;
            do {
                if (!var4.hasNext()) {
                    return giftCardOutDataList;
                }

                String proId = (String) var4.next();
                numAmtData = (GetNumAmtData) proNumAmtMap.get(proId);
                inData.setNameOrCode(proId);
                inData.setTotalNum(numAmtData.getNum());
                inData.setTotalMoney(new BigDecimal(numAmtData.getMoney()));
                inData.setProName(numAmtData.getProName());
                outData = this.gaiaSdPromHeadMapper.giftCardByProCode(inData);
            } while (!ObjectUtil.isNotEmpty(outData));

            GiftCardOutData param = (GiftCardOutData) outData.get(0);
            param.setClientId(inData.getClientId());
            param.setBrId(inData.getBrId());
            String count = this.promCouponProType0(numAmtData, param);
            GetGiftCardOutData giftCardOutData = new GetGiftCardOutData();
            giftCardOutData.setTotalNum(count);
            BeanUtils.copyProperties(outData.get(0), giftCardOutData);
            List<GiftCardSet> giftCardSetList = new ArrayList();

            for (int i = 0; i < outData.size(); ++i) {
                GiftCardOutData item = (GiftCardOutData) outData.get(i);
                GiftCardSet giftCardSet = new GiftCardSet();
                BeanUtils.copyProperties(item, giftCardSet);
                DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
                String gspcsSingleUseAmt = decimalFormat.format(new BigDecimal(giftCardSet.getGspcsSingleUseAmt()));
                giftCardSet.setGspcsSingleUseAmt(gspcsSingleUseAmt);
                String gspcsSinglePaycheckAmt = decimalFormat.format(new BigDecimal(giftCardSet.getGspcsSinglePaycheckAmt()));
                giftCardSet.setGspcsSinglePaycheckAmt(gspcsSinglePaycheckAmt);
                giftCardSetList.add(giftCardSet);
            }

            giftCardOutData.setGiftCardSetList(giftCardSetList);
            giftCardOutDataList.add(giftCardOutData);
        }

    }

    @Override
    @Transactional
    public void giveGiftCard(List<GaiaSdPromCouponBasic> inData) {
        GaiaSdPromCouponBasic gaiaSdPromCouponBasicTemp = new GaiaSdPromCouponBasic();
        gaiaSdPromCouponBasicTemp.setClientId(((GaiaSdPromCouponBasic) inData.get(0)).getClientId());
        gaiaSdPromCouponBasicTemp.setGspcbGrantBillNo(((GaiaSdPromCouponBasic) inData.get(0)).getGspcbGrantBillNo());
        this.gaiaSdPromCouponBasicMapper.delete(gaiaSdPromCouponBasicTemp);

        for (int i = 0; i < inData.size(); ++i) {
            GaiaSdPromCouponBasic gaiaSdPromCouponBasic = (GaiaSdPromCouponBasic) inData.get(i);
            String gspcbCouponId = this.gaiaSdPromCouponBasicMapper.getMaxGspcbCouponId();
            gaiaSdPromCouponBasic.setGspcbCouponId(gspcbCouponId);
            this.gaiaSdPromCouponBasicMapper.insert(gaiaSdPromCouponBasic);
        }
    }

    @Override
    public List<GetGiftOutData> giftSelect(GetQueryProductInData inData) {
        List<GetGiftOutData> getGiftOutDataList = new ArrayList();
        this.gift(inData, getGiftOutDataList);
        this.combin(inData, getGiftOutDataList);

        for (int i = 0; i < getGiftOutDataList.size(); ++i) {
            GetGiftOutData getGiftOutData = (GetGiftOutData) getGiftOutDataList.get(i);
            getGiftOutData.setIndex(String.valueOf(i + 1));
            getGiftOutData.setIndexD(String.valueOf(i + 1));
        }
        return getGiftOutDataList;
    }

    private void combin(GetQueryProductInData inData, List<GetGiftOutData> getGiftOutDataList) {
        Map<String, GetNumAmtData> proIdNumMap = inData.getProNumAmtMap();
        List<GetPromByProCodeOutData> outData = this.gaiaSdPromHeadMapper.promCombinByProCode(inData);
        this.comBinRemove(inData, outData);

        for (int i = 0; i < outData.size(); ++i) {
            GetPromByProCodeOutData item = (GetPromByProCodeOutData) outData.get(i);
            GaiaSdPromHead gaiaSdPromHead = new GaiaSdPromHead();
            gaiaSdPromHead.setClientId(inData.getClientId());
            gaiaSdPromHead.setGsphVoucherId(item.getGsphVoucherId());
            gaiaSdPromHead.setGsphBrId(inData.getBrId());
            gaiaSdPromHead = (GaiaSdPromHead) this.gaiaSdPromHeadMapper.selectByPrimaryKey(gaiaSdPromHead);
            if (!"combin2".equals(gaiaSdPromHead.getGsphPara1())) {
                GaiaSdPromAssoSet gaiaSdPromAssoSet = new GaiaSdPromAssoSet();
                gaiaSdPromAssoSet.setClientId(inData.getClientId());
                gaiaSdPromAssoSet.setGspasVoucherId(item.getGsphVoucherId());
                gaiaSdPromAssoSet.setGspasSeriesId(item.getSeriesId());
                gaiaSdPromAssoSet = (GaiaSdPromAssoSet) this.gaiaSdPromAssoSetMapper.selectOne(gaiaSdPromAssoSet);
                int count = Integer.valueOf(gaiaSdPromAssoSet.getGspasQty());
                GaiaSdPromAssoResult gaiaSdPromAssoResult = new GaiaSdPromAssoResult();
                gaiaSdPromAssoResult.setClientId(inData.getClientId());
                gaiaSdPromAssoResult.setGsparVoucherId(item.getGsphVoucherId());
                gaiaSdPromAssoResult.setGsparSeriesId(item.getSeriesId());
                List<GaiaSdPromAssoResult> gaiaSdPromAssoResultList = this.gaiaSdPromAssoResultMapper.select(gaiaSdPromAssoResult);

                for (int j = 0; j < gaiaSdPromAssoResultList.size(); ++j) {
                    GaiaSdPromAssoResult jItem = (GaiaSdPromAssoResult) gaiaSdPromAssoResultList.get(j);
                    GetGiftOutData getGiftOutData = new GetGiftOutData();
                    getGiftOutData.setParam4(gaiaSdPromHead.getGsphPara2());
                    getGiftOutData.setGiveNum(String.valueOf(count));
                    getGiftOutData.setPromotionName(item.getPromotionName());
                    getGiftOutData.setGiftCode(jItem.getGsparProId());
                    getGiftOutData.setPromotionType(item.getPromotionType());
                    getGiftOutData.setFlag(item.getGsphVoucherId() + "-" + item.getSeriesId());
                    DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
                    String gspgcGiftPrc = decimalFormat.format(jItem.getGsparGiftPrc());
                    getGiftOutData.setAddPrice(gspgcGiftPrc);
                    getGiftOutData.setAddDisc(jItem.getGsparGiftRebate());
                    getGiftOutData.setVoucherId(item.getGsphVoucherId());
                    getGiftOutData.setPromotionContent(item.getPromotionContent());
                    GetQueryProductInData productInData = new GetQueryProductInData();
                    productInData.setClientId(inData.getClientId());
                    productInData.setNameOrCode(jItem.getGsparProId());
                    productInData.setBrId(inData.getBrId());
                    GiveProData giveProData = this.gaiaProductBusinessMapper.selectGiftInfo(productInData);
                    if (!ObjectUtil.isEmpty(giveProData) && !"0".equals(giveProData.getGiftStock())) {
                        BeanUtils.copyProperties(giveProData, getGiftOutData);
                        String giftNormalPrice = decimalFormat.format(new BigDecimal(getGiftOutData.getGiftNormalPrice()));
                        getGiftOutData.setGiftNormalPrice(giftNormalPrice);
                        getGiftOutData.setGiveNumD(getGiftOutData.getGiveNum());
                        getGiftOutData.setGiveNumHave("0");
                        getGiftOutDataList.add(getGiftOutData);
                    }
                }
            }
        }

    }

    private void gift(GetQueryProductInData inData, List<GetGiftOutData> getGiftOutDataList) {
        Map<String, GetNumAmtData> proIdNumMap = inData.getProNumAmtMap();
        List<GetPromByProCodeOutData> outData = this.gaiaSdPromHeadMapper.giveSelect(inData);

        for (int i = 0; i < outData.size(); ++i) {
            GetPromByProCodeOutData item = (GetPromByProCodeOutData) outData.get(i);
            GaiaSdPromHead gaiaSdPromHead = new GaiaSdPromHead();
            gaiaSdPromHead.setClientId(inData.getClientId());
            gaiaSdPromHead.setGsphVoucherId(item.getGsphVoucherId());
            gaiaSdPromHead.setGsphBrId(inData.getBrId());
            gaiaSdPromHead = (GaiaSdPromHead) this.gaiaSdPromHeadMapper.selectByPrimaryKey(gaiaSdPromHead);
            GaiaSdPromGiftSet gaiaSdPromGiftSet = new GaiaSdPromGiftSet();
            gaiaSdPromGiftSet.setClientId(inData.getClientId());
            gaiaSdPromGiftSet.setGspgsVoucherId(item.getGsphVoucherId());
            gaiaSdPromGiftSet.setGspgsSeriesProId(item.getSeriesId());
            gaiaSdPromGiftSet = (GaiaSdPromGiftSet) this.gaiaSdPromGiftSetMapper.selectOne(gaiaSdPromGiftSet);
            GaiaSdPromGiftConds gaiaSdPromGiftConds = new GaiaSdPromGiftConds();
            gaiaSdPromGiftConds.setClientId(inData.getClientId());
            gaiaSdPromGiftConds.setGspgcSeriesId(item.getSeriesId());
            gaiaSdPromGiftConds.setGspgcVoucherId(item.getGsphVoucherId());
            List<GaiaSdPromGiftConds> sdPromGiftCondsList = this.gaiaSdPromGiftCondsMapper.select(gaiaSdPromGiftConds);
            Float numTotal = 0.0F;
            Float totalMoney = 0.0F;

            int count;
            for (count = 0; count < sdPromGiftCondsList.size(); ++count) {
                GaiaSdPromGiftConds gaiaSdPromGiftConds1 = (GaiaSdPromGiftConds) sdPromGiftCondsList.get(count);
                GetNumAmtData numAmtData = (GetNumAmtData) proIdNumMap.get(gaiaSdPromGiftConds1.getGspgcProId());
                if (!ObjectUtil.isEmpty(numAmtData)) {
                    numTotal = numTotal + Float.valueOf(numAmtData.getNum());
                    totalMoney = totalMoney + Float.valueOf(numAmtData.getMoney());
                }
            }

            if (numTotal >= Float.valueOf(gaiaSdPromGiftSet.getGspgsReachQty1()) && totalMoney >= gaiaSdPromGiftSet.getGspgsReachAmt1().floatValue()) {
                count = this.promGift(gaiaSdPromHead, item, numTotal, new BigDecimal((double) totalMoney));
                GaiaSdPromGiftResult gaiaSdPromGiftResult = new GaiaSdPromGiftResult();
                gaiaSdPromGiftResult.setClientId(inData.getClientId());
                gaiaSdPromGiftResult.setGspgrVoucherId(item.getGsphVoucherId());
                gaiaSdPromGiftResult.setGspgrSeriesId(item.getSeriesId());
                List<GaiaSdPromGiftResult> gaiaSdPromGiftResultList = this.gaiaSdPromGiftResultMapper.select(gaiaSdPromGiftResult);

                for (int j = 0; j < gaiaSdPromGiftResultList.size(); ++j) {
                    GaiaSdPromGiftResult jItem = (GaiaSdPromGiftResult) gaiaSdPromGiftResultList.get(j);
                    GetGiftOutData getGiftOutData = new GetGiftOutData();
                    getGiftOutData.setParam4(gaiaSdPromHead.getGsphPara4());
                    getGiftOutData.setGiveNum(String.valueOf(count));
                    getGiftOutData.setPromotionName(item.getPromotionName());
                    getGiftOutData.setGiftCode(jItem.getGspgrProId());
                    getGiftOutData.setPromotionType(item.getPromotionType());
                    getGiftOutData.setFlag(item.getGsphVoucherId() + "-" + item.getSeriesId());
                    DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
                    String gspgcGiftPrc = decimalFormat.format(jItem.getGspgrGiftPrc());
                    getGiftOutData.setAddPrice(gspgcGiftPrc);
                    getGiftOutData.setAddDisc(jItem.getGspgrGiftRebate());
                    getGiftOutData.setVoucherId(item.getGsphVoucherId());
                    getGiftOutData.setPromotionContent(item.getPromotionContent());
                    GetQueryProductInData productInData = new GetQueryProductInData();
                    productInData.setClientId(inData.getClientId());
                    productInData.setNameOrCode(jItem.getGspgrProId());
                    productInData.setBrId(inData.getBrId());
                    GiveProData giveProData = this.gaiaProductBusinessMapper.selectGiftInfo(productInData);
                    if (!ObjectUtil.isEmpty(giveProData) && !"0".equals(giveProData.getGiftStock())) {
                        BeanUtils.copyProperties(giveProData, getGiftOutData);
                        String giftNormalPrice = decimalFormat.format(new BigDecimal(getGiftOutData.getGiftNormalPrice()));
                        getGiftOutData.setGiftNormalPrice(giftNormalPrice);
                        getGiftOutData.setGiveNumD(getGiftOutData.getGiveNum());
                        getGiftOutData.setGiveNumHave("0");
                        getGiftOutDataList.add(getGiftOutData);
                    }
                }
            }
        }

    }

    @Override
    public GaiaSdIntegralAddSet getPointSet(GetLoginOutData loginOutData) {
        GaiaSdIntegralAddSet gaiaSdIntegralAddSet = new GaiaSdIntegralAddSet();
        gaiaSdIntegralAddSet.setClientId(loginOutData.getClient());
        gaiaSdIntegralAddSet.setGsiasBrId(loginOutData.getDepId());
        gaiaSdIntegralAddSet = (GaiaSdIntegralAddSet) this.gaiaSdIntegralAddSetMapper.selectByPrimaryKey(gaiaSdIntegralAddSet);
        return gaiaSdIntegralAddSet;
    }

    @Override
    public GaiaSdMemberClass getMemberPointSet(GetQueryProductInData productInData) {
        GaiaSdMemberClass gaiaSdMemberClass = new GaiaSdMemberClass();
        gaiaSdMemberClass.setClientId(productInData.getClientId());
        gaiaSdMemberClass.setGsmcId(productInData.getMemberType());
        gaiaSdMemberClass = this.gaiaSdMemberClassMapper.selectByPrimaryKey(gaiaSdMemberClass);
        return gaiaSdMemberClass;
    }

    @Override
    public NumberOfPromotionalMethodsData findSingleProductPromotionByCode(PromotionMethodInData inData) {
        return gaiaSdPromHeadMapper.findSingleProductPromotionByCode(inData);
    }

    @Override
    public List<PromotionalConditionData> findProductPromotionByCode(PromotionMethodInData inData) {
        if (StringUtils.isEmpty(inData.getIsMember())) {
            inData.setIsMember("0");
        }
        List<PromotionalConditionData> result = new ArrayList<PromotionalConditionData>();
        List<PromotionalConditionData> promotionalConditionDataList = gaiaSdPromHeadMapper.findProductPromotionByCode(inData);

        //去除商品列表中的改价 拆零 积分换购商品
        List<RebornData> rebornDataList = inData.getRebornDataList().stream()
                .filter(item -> !item.getStatus().equals("改价") && !item.getStatus().equals("拆零") && !item.getStatus().equals("积分换购")).collect(Collectors.toList());

        //遍历汇总出此商品的总数量(单品条件)
        int singleQty = rebornDataList.stream()
                .filter(item -> item.getGssdProId().equals(inData.getCode()))
                .filter(item -> CommonUtil.isBigDecimal(item.getGssdQty()))
                .mapToInt(item -> new BigDecimal(item.getGssdQty()).intValue()).sum();

        //查出此商品所属系列活动集合（系列条件）
        GaiaSdPromSeriesConds temp = new GaiaSdPromSeriesConds();
        temp.setGspscProId(inData.getCode());
        temp.setClientId(inData.getClient());
        List<GaiaSdPromSeriesConds> promSeriesConds = gaiaSdPromSeriesCondsMapper.select(temp);

        //查询出商品列表所属的系列活动（系列条件）
        List<String> proList = new ArrayList<>();
        proList.add("");
        rebornDataList.stream().forEach(item -> proList.add(item.getGssdProId()));
        Example example = new Example(GaiaSdPromSeriesConds.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andIn("gspscProId", proList);
        List<GaiaSdPromSeriesConds> allPromSeriesConds = this.gaiaSdPromSeriesCondsMapper.selectByExample(example);

        //遍历筛选出符合条件的活动
        promotionalConditionDataList.forEach(item -> {
            if ("PROM_SINGLE".equals(item.getType())) { // 单品促销
                if (singleQty >= new BigDecimal(item.getQtyOneStage()).intValue()) { // 数量满足单品第一阶梯
                    result.add(item);
                }
            } else if ("PROM_SERIES".equals(item.getType())) { // 系列促销
                promSeriesConds.forEach(item2 -> { // 遍历当前商品的系列活动 与此商品的所有活动做对比 看是否满足活动条件
                    if (item2.getGspscSeriesId().equals(item.getSeriesId()) && item2.getGspscVoucherId().equals(item.getVoucherId())) {
                        if (item.getQtyOneStage() != null && !"".equals(item.getQtyOneStage())) { // 达量
                            AtomicInteger count = new AtomicInteger();
                            allPromSeriesConds.forEach(item3 -> {
                                if (item3.getGspscSeriesId().equals(item.getSeriesId())) {
                                    //统计商品列表内的 此系列商品的数量
                                    count.set(rebornDataList.stream()
                                            .filter(item4 -> allPromSeriesConds.stream().anyMatch(condsItem -> condsItem.getGspscProId().equals(item4.getGssdProId())))
//                                            .filter(item4 -> item4.getGssdProId().equals(item3.getGspscProId()))
                                            .filter(item4 -> CommonUtil.isBigDecimal(item4.getGssdQty()))
                                            .mapToInt(item4 -> new BigDecimal(item4.getGssdQty()).intValue()).sum());
                                }
                            });
                            if (count.get() >= new BigDecimal(item.getQtyOneStage()).intValue()) { // 数量满足
                                result.add(item);
                            }
                        } else if (item.getAmtOneStage() != null && !"".equals(item.getAmtOneStage())) { // 达额
                            final BigDecimal[] sum = {BigDecimal.ZERO};
                            allPromSeriesConds.forEach(item3 -> {
                                if (item3.getGspscSeriesId().equals(item.getSeriesId()) && item3.getGspscVoucherId().equals(item.getVoucherId())) {
                                    //统计商品列表内的 此系列商品的金额
                                    rebornDataList.stream()
                                            .filter(item4 -> allPromSeriesConds.stream().anyMatch(condsItem -> condsItem.getGspscProId().equals(item4.getGssdProId())))
//                                            .filter(item4 -> item4.getGssdProId().equals(item3.getGspscProId()))
                                            .filter(item4 -> CommonUtil.isBigDecimal(item4.getGssdAmt()))
                                            .map(item4 -> new BigDecimal(item4.getGssdAmt()))
                                            .forEach(item5 -> {
                                                sum[0] = sum[0].add(item5);
                                            });
                                }
                            });
                            if (sum[0].compareTo(new BigDecimal(item.getAmtOneStage())) > -1) { // 金额满足
                                result.add(item);
                            }
                        }
                    }
                });
            } else if ("PROM_GIFT".equals(item.getType())) {
                //Todo 赠品促销

            } else if ("电子券促销".equals(item.getType())) {
                //Todo 电子券促销
            } else if ("会员日设置".equals(item.getType())) {
                //Todo 会员日设置
            } else if ("组合促销".equals(item.getType())) {
                //Todo 组合促销
            }
        });
        return result;
    }

    @Override
    public List<PromotionalConditionData> findProductPromotionByCode2(PromotionMethodInData inData) {
        if (StringUtils.isEmpty(inData.getIsMember())) {
            inData.setIsMember("0");
        }
        List<PromotionalConditionData> result = new ArrayList<PromotionalConditionData>();
        List<PromotionalConditionData> promotionalConditionDataList = gaiaSdPromHeadMapper.findProductPromotionByCode2(inData);

        //去除商品列表中的改价 拆零 积分换购商品
        List<RebornData> rebornDataList = inData.getRebornDataList().stream()
                .filter(item -> !"改价".equals(item.getStatus()) && !"拆零".equals(item.getStatus()) && !"积分换购".equals(item.getStatus()))
                .collect(Collectors.toList());

        //遍历汇总出此商品的总数量(单品条件)
        double singleQty = rebornDataList.stream()
                .filter(item -> item.getGssdProId().equals(inData.getCode()) && !"PROM_GIFT".equals(item.getGssdPmId()))
                .filter(item -> CommonUtil.isBigDecimal(item.getGssdQty()))
                .mapToDouble(item -> new BigDecimal(item.getGssdQty()).doubleValue()).sum();

        //查出此商品所属系列活动集合（系列条件）
        GaiaSdPromSeriesConds temp = new GaiaSdPromSeriesConds();
        temp.setGspscProId(inData.getCode());
        temp.setClientId(inData.getClient());
        List<GaiaSdPromSeriesConds> promSeriesConds = gaiaSdPromSeriesCondsMapper.select(temp);

        //查询出商品列表所属的系列活动（系列条件）
        List<String> proList = new ArrayList<>();
        proList.add("");
        rebornDataList.stream().filter(item -> !"PROM_GIFT".equals(item.getGssdPmId())).forEach(item -> proList.add(item.getGssdProId()));
        Example example = new Example(GaiaSdPromSeriesConds.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andIn("gspscProId", proList);
        List<GaiaSdPromSeriesConds> allPromSeriesConds = this.gaiaSdPromSeriesCondsMapper.selectByExample(example);

        //遍历筛选出符合条件的活动
        promotionalConditionDataList.forEach(item -> {
            if ("PROM_SINGLE".equals(item.getType())) { // 单品促销
                if (singleQty >= new BigDecimal(item.getQtyOneStage()).intValue()) { // 数量满足单品第一阶梯
                    result.add(item);
                }
            } else if ("PROM_SERIES".equals(item.getType())) { // 系列促销
                promSeriesConds.forEach(item2 -> { // 遍历当前商品的系列活动 与此商品的所有活动做对比 看是否满足活动条件
                    if (item2.getGspscSeriesId().equals(item.getSeriesId()) && item2.getGspscVoucherId().equals(item.getVoucherId())) {
                        if (item.getQtyOneStage() != null && !"".equals(item.getQtyOneStage())) { // 达量
                            AtomicDouble count = new AtomicDouble();
                            allPromSeriesConds.forEach(item3 -> {
                                if (item3.getGspscSeriesId().equals(item.getSeriesId())) {
                                    //统计商品列表内的 此系列商品的数量
                                    count.set(rebornDataList.stream()
                                            .filter(item4 -> allPromSeriesConds.stream().anyMatch(condsItem -> condsItem.getGspscProId().equals(item4.getGssdProId()) && item2.getGspscVoucherId().equals(condsItem.getGspscVoucherId())))
//                                            .filter(item4 -> item4.getGssdProId().equals(item3.getGspscProId()))
                                            .filter(item4 -> CommonUtil.isBigDecimal(item4.getGssdQty()) && !"PROM_GIFT".equals(item4.getGssdPmId()))
                                            .mapToDouble(item4 -> new BigDecimal(item4.getGssdQty()).doubleValue()).sum());
                                }
                            });
                            if (count.get() >= new BigDecimal(item.getQtyOneStage()).intValue()) { // 数量满足
                                result.add(item);
                            }
                        } else if (item.getAmtOneStage() != null && !"".equals(item.getAmtOneStage())) { // 达额
                            final BigDecimal[] sum = {BigDecimal.ZERO};
                            allPromSeriesConds.forEach(item3 -> {
                                if (item3.getGspscSeriesId().equals(item.getSeriesId()) && item3.getGspscVoucherId().equals(item.getVoucherId())) {
                                    //统计商品列表内的 此系列商品的金额
                                    rebornDataList.stream()
                                            .filter(item4 -> allPromSeriesConds.stream().anyMatch(condsItem -> condsItem.getGspscProId().equals(item4.getGssdProId()) && item2.getGspscVoucherId().equals(condsItem.getGspscVoucherId())) && !"PROM_GIFT".equals(item4.getGssdPmId()))
//                                            .filter(item4 -> item4.getGssdProId().equals(item3.getGspscProId()))
                                            .filter(item4 -> CommonUtil.isBigDecimal(item4.getGssdAmt()))
                                            .map(item4 -> new BigDecimal(item4.getGssdAmt()))
                                            .forEach(item5 -> {
                                                sum[0] = sum[0].add(item5);
                                            });
                                }
                            });
                            if (sum[0].compareTo(new BigDecimal(item.getAmtOneStage())) > -1) { // 金额满足
                                result.add(item);
                            }
                        }
                    }
                });
            } else if ("PROM_GIFT".equals(item.getType())) {
                //Todo 赠品促销
                //查询商品列表中未参与活动的和参与本活动的或参加了会员日活动的 所属系列 条件商品数量或金额
                List<String> proList2 = new ArrayList<>();
                rebornDataList.stream()
                        .filter(rebornItem -> item.getVoucherId().equals(rebornItem.getGssdPmActivityId()) || StringUtils.isEmpty(rebornItem.getGssdPmActivityId()) || "PROM_HYJ".equals(rebornItem.getGssdPmId()) || "PROM_HYRJ".equals(rebornItem.getGssdPmId()) || "PROM_HYRZ".equals(rebornItem.getGssdPmId()))
                        .forEach(rebornItem -> proList2.add(rebornItem.getGssdProId()));
                if (CollectionUtil.isNotEmpty(proList2)) {
                    BigDecimal qtySendTemp = new BigDecimal("1");
                    //查询商品列表中空白和参与本活动 所属系列 赠品的数量
                    Example exampleGift = new Example(GaiaSdPromGiftResult.class);
                    exampleGift.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gspgrSeriesId", item.getSeriesId()).andEqualTo("gspgrVoucherId", item.getVoucherId()).andIn("gspgrProId", proList2);
                    List<GaiaSdPromGiftResult> allGiftSeriesResults = this.gaiaSdPromGiftResultMapper.selectByExample(exampleGift);

                    BigDecimal giftResultQty = BigDecimal.ZERO;
                    //商品列表 按单价排序
//                    inData.setRebornDataList(inData.getRebornDataList().stream().sorted(Comparator.comparingDouble(ritm->Double.parseDouble(ritm.getGssdPrc1()))).collect(Collectors.toList()));
                    List<RebornData> rdList = inData.getRebornDataList().stream().sorted(Comparator.comparingDouble(ritm -> Double.parseDouble(ritm.getGssdPrc1()))).collect(Collectors.toList());
                    List<RebornData> newRebronDataList = new ArrayList<>();
                    for (RebornData copyItem : rdList) {
                        if (item.getVoucherId().equals(copyItem.getGssdPmActivityId()) || StringUtils.isEmpty(copyItem.getGssdPmActivityId()) || "PROM_HYJ".equals(copyItem.getGssdPmId()) || "PROM_HYRJ".equals(copyItem.getGssdPmId()) || "PROM_HYRZ".equals(copyItem.getGssdPmId())) {
                            RebornData d = new RebornData();
                            BeanUtils.copyProperties(copyItem, d);
                            newRebronDataList.add(d);
                        }
                    }
                    //先将一阶段赠品数扣除后看其条件商品是否满足
                    for (RebornData rebornData : newRebronDataList) {
                        for (GaiaSdPromGiftResult giftResult : allGiftSeriesResults) {
                            if (rebornData.getGssdProId().equals(giftResult.getGspgrProId()) && PromUtil.isJoinActivity2(rebornData.getStatus())) {
                                BigDecimal rQty = new BigDecimal(rebornData.getGssdQty());
                                if (rQty.compareTo(qtySendTemp) > -1 && qtySendTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    giftResultQty = giftResultQty.add(qtySendTemp);
                                    rebornData.setGssdQty(rQty.subtract(qtySendTemp).toString());
                                    qtySendTemp = BigDecimal.ZERO;
                                } else if (rQty.compareTo(qtySendTemp) == -1 && qtySendTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    giftResultQty = giftResultQty.add(rQty);
                                    qtySendTemp = qtySendTemp.subtract(rQty);
                                    rebornData.setGssdQty("0");
                                }
                            }
                        }
                    }

                    BigDecimal giftCondsAmt = BigDecimal.ZERO;
                    BigDecimal giftCondsQty = BigDecimal.ZERO;
                    if ("assign1".equals(item.getGsphPara3())) { //全场条件商品
                        for (RebornData rebornData : newRebronDataList) {
                            if (PromUtil.isJoinActivity2(rebornData.getStatus()))
                                if (item.getVoucherId().equals(rebornData.getGssdPmActivityId()) || StringUtils.isEmpty(rebornData.getGssdPmActivityId()) || "PROM_HYJ".equals(rebornData.getGssdPmId()) || "PROM_HYRJ".equals(rebornData.getGssdPmId()) || "PROM_HYRZ".equals(rebornData.getGssdPmId())) {
                                    giftCondsAmt = giftCondsAmt.add(new BigDecimal(rebornData.getGssdQty()).multiply(new BigDecimal(rebornData.getGssdPrc1())));
                                    giftCondsQty = giftCondsQty.add(new BigDecimal(rebornData.getGssdQty()));
                                }
                        }
                    } else { //部分条件商品
                        exampleGift = new Example(GaiaSdPromGiftConds.class);
                        exampleGift.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gspgcSeriesId", item.getSeriesId()).andEqualTo("gspgcVoucherId", item.getVoucherId()).andIn("gspgcProId", proList2);
                        List<GaiaSdPromGiftConds> allGiftSeriesConds = this.gaiaSdPromGiftCondsMapper.selectByExample(exampleGift);
                        for (RebornData rebornData : newRebronDataList) {
                            if (PromUtil.isJoinActivity2(rebornData.getStatus()))
                                if (item.getVoucherId().equals(rebornData.getGssdPmActivityId()) || StringUtils.isEmpty(rebornData.getGssdPmActivityId()) || "PROM_HYJ".equals(rebornData.getGssdPmId()) || "PROM_HYRJ".equals(rebornData.getGssdPmId()) || "PROM_HYRZ".equals(rebornData.getGssdPmId())) {
                                    for (GaiaSdPromGiftConds giftConds : allGiftSeriesConds) {
                                        if (rebornData.getGssdProId().equals(giftConds.getGspgcProId()) && new BigDecimal(giftConds.getGspgcMemFlag()).compareTo(new BigDecimal(inData.getIsMember())) < 1) {
                                            giftCondsAmt = giftCondsAmt.add(new BigDecimal(rebornData.getGssdQty()).multiply(new BigDecimal(rebornData.getGssdPrc1())));
                                            giftCondsQty = giftCondsQty.add(new BigDecimal(rebornData.getGssdQty()));
                                        }
                                    }
                                }
                        }
                    }


                    if (item.getQtyOneStage() != null && !"".equals(item.getQtyOneStage())) { // 达量
                        if (giftCondsQty.compareTo(new BigDecimal(item.getQtyOneStage())) > -1 && giftResultQty.compareTo(new BigDecimal("1")) > -1) { // 数量满足
                            result.add(item);
                        }
                    } else if (item.getAmtOneStage() != null && !"".equals(item.getAmtOneStage())) { // 达额
                        if (giftCondsAmt.compareTo(new BigDecimal(item.getAmtOneStage())) > -1 && giftResultQty.compareTo(new BigDecimal("1")) > -1) { // 数量满足
                            result.add(item);
                        }
                    }
                }
            } else if ("组合促销".equals(item.getType())) {
                //Todo 组合促销
            }
        });
        return result;
    }

    @Override
    public List<GetDisPriceOutData> findPromotionById(List<FindPromotionInData> inData) {
        return this.gaiaSdPromHeadMapper.findPromotionById(inData);
    }

    @Override
    public List<PromotionalConditionData> findProductPromotionByIdAndArgs(List<FindPromotionInData> inData) {
        return this.gaiaSdPromHeadMapper.findProductPromotionByIdAndArgs(inData);
    }

    @Override
    public List<GaiaSdPromSeriesConds> findSeriesCondsByRebornList(PromotionMethodInData inData) {
        List<GaiaSdPromSeriesConds> result = new ArrayList<GaiaSdPromSeriesConds>();
        //查询出商品列表所属的系列（系列条件）
        List<String> proList = new ArrayList<>();
        inData.getRebornDataList().stream().forEach(item -> proList.add(item.getGssdProId()));
        Example example = new Example(GaiaSdPromSeriesConds.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andIn("gspscProId", proList);
        return this.gaiaSdPromSeriesCondsMapper.selectByExample(example);
    }

    @Override
    public List<GaiaSdIntegralExchangeSet> findIntegralExchangeByProCode(IntegralExchangeInData inData) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String now = formatter.format(date);
        Example example = new Example(GaiaSdIntegralExchangeSet.class);
        example.createCriteria()
                .andEqualTo("clientId", inData.getClient())
                .andEqualTo("gsiesBrId", inData.getBrId())
                .andEqualTo("gsiesExchangeProId", inData.getProCode())
                .andEqualTo("gsiesStatus", "1")
                .andGreaterThanOrEqualTo("gsiesDateEnd", now)
                .andLessThanOrEqualTo("gsiesDateStart", now);
        return this.gaiaSdIntegralExchangeSetMapper.selectByExample(example);
    }

    @Override
    public List<MemberPromotionOutData> findMemberPromotion(MemberPromotionInData inData) {
        return gaiaSdPromHySetMapper.findMemberPromotion(inData);
    }

    @Override
    public GaiaSdStock verifyInventory(MemberPromotionInData inData) {
        return gaiaSdStockMapper.verifyInventory(inData);
    }

    @Override
    public CurrentMembersOutData checkCurrentMembers(GaiaSdSaleD inData) {
        return this.memberBasicMapper.checkCurrentMembers(inData);
    }

    @Override
    public HandoverReportOutData handoverReport(GaiaRetailPriceInData inData) {
        HandoverReportOutData outData = this.gaiaUserDataMapper.handoverReport(inData);
        List<HandoverReportPayData> dataList = this.msgMapper.handoverReport(inData);
        List<HandoverReportPayData> storeList = this.msgMapper.handoverReportByStore(inData);
        if (ObjectUtil.isNotEmpty(outData) && CollUtil.isNotEmpty(dataList)) {
            if (StrUtil.isBlank(inData.getCashier())) {
                outData.setGsshEmp(null);
                outData.setUserNam(null);
            }
            outData.setDataList(dataList);
        }

        if (ObjectUtil.isNotEmpty(outData) && CollUtil.isNotEmpty(storeList)) {
            outData.setStoreList(storeList);
        }
        return outData;
    }

    @Override
    public List<GaiaSdStock> getAllByProIds(MemberPromotionInData inData) {
        return gaiaSdStockMapper.getAllByProIds(inData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createSalePendingOrder(GetCreateSaleOutData inData) {
        GaiaSdSaleH gaiaSdSaleH = inData.getGaiaSdSaleH();
        GaiaSdSaleH gaiaSdSaleHSource = this.gaiaSdSaleHMapper.selectByBillNo(gaiaSdSaleH.getGsshBillNo(), gaiaSdSaleH.getClientId(), gaiaSdSaleH.getGsshBrId());
        if (ObjectUtil.isEmpty(gaiaSdSaleHSource)) {
            this.gaiaSdSaleHMapper.insert(gaiaSdSaleH);
        } else {
            if (!"1".equals(gaiaSdSaleHSource.getGsshHideFlag())) {
                throw new BusinessException("已支付商品无法挂单!");
            }
            gaiaSdSaleHSource.setGsshEmp(gaiaSdSaleH.getGsshEmp());
            gaiaSdSaleHSource.setGsshHykNo(gaiaSdSaleH.getGsshHykNo());
            gaiaSdSaleHSource.setGsshZkAmt(gaiaSdSaleH.getGsshZkAmt());
            gaiaSdSaleHSource.setGsshYsAmt(gaiaSdSaleH.getGsshYsAmt());
            gaiaSdSaleHSource.setGsshEmpGroupName(gaiaSdSaleH.getGsshEmpGroupName());
            gaiaSdSaleHSource.setGsshHideFlag(gaiaSdSaleH.getGsshHideFlag());
            this.gaiaSdSaleHMapper.updateByPrimaryKey(gaiaSdSaleHSource);
        }

        GaiaSdSaleD gaiaSdSaleD = new GaiaSdSaleD();
        gaiaSdSaleD.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
        gaiaSdSaleD.setClientId(gaiaSdSaleH.getClientId());
        this.gaiaSdSaleDMapper.delete(gaiaSdSaleD);
        String now = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        if (CollectionUtil.isNotEmpty(inData.getGiftPromOutDataList())) {
            for (GiftPromOutData item : inData.getGiftPromOutDataList()) {
                List<GaiaSdSaleD> saleDListTemp = new ArrayList<>();
                if ("3".equals(item.getProStorageArea())) {
                    GaiaSdSaleD temp = new GaiaSdSaleD();
                    temp.setClientId(gaiaSdSaleH.getClientId());
                    temp.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
                    temp.setGssdBrId(gaiaSdSaleH.getGsshBrId());
                    temp.setGssdDate(gaiaSdSaleH.getGsshDate());
                    temp.setGssdProId(item.getGssdProId());
                    temp.setGssdSerial(String.valueOf(inData.getGaiaSdSaleDList().size() + 1));
                    temp.setGssdPrc1(item.getGssdPrc1());
                    temp.setGssdPrc2(item.getGssdPrc2());
                    temp.setGssdQty(item.getGssdQty().toString());
                    temp.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(temp.getGssdQty())));
                    temp.setGssdDose("1");
                    temp.setGssdPmId("PROM_GIFT");
                    temp.setGssdPmActivityId(item.getGssdPmActivityId());
                    temp.setGssdSalerId(inData.getGaiaSdSaleDList().get(0).getGssdSalerId());
                    temp.setGssdZkAmt(item.getGssdPrc1().multiply(item.getGssdQty()).subtract(temp.getGssdAmt()));
                    temp.setGssdProStatus("促销");
                    temp.setGssdOriQty(item.getGssdQty());
                    temp.setGssdRecipelFlag("0");
                    temp.setGssdMovTax(new BigDecimal(item.getMovTax()));
                    saleDListTemp.add(temp);
                } else {
                    if (item.getGssdQty().compareTo(BigDecimal.ZERO) > 0) {
                        //按批次倒序 自动选择批号库存
                        //查询批次信息
                        Example stockBatchExample = new Example(GaiaSdStockBatch.class);

                        stockBatchExample.createCriteria().andEqualTo("clientId", gaiaSdSaleH.getClientId()).andEqualTo("gssbBrId", gaiaSdSaleH.getGsshBrId())
                                .andEqualTo("gssbProId", item.getGssdProId()).andGreaterThan("gssbQty", "0").andGreaterThan("gssbVaildDate", now);

                        stockBatchExample.setOrderByClause("GSSB_BATCH ASC");
                        List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(stockBatchExample);

                        List<GaiaSdSaleD> saleDList = CopyUtil.copyList(inData.getGaiaSdSaleDList(), GaiaSdSaleD.class);
                        Iterator<GaiaSdStockBatch> stockBatchIterator = stockBatchList.iterator();
                        BigDecimal qtyTemp = item.getGssdQty();
                        while (stockBatchIterator.hasNext()) {
                            GaiaSdStockBatch stockBatch = stockBatchIterator.next();
                            //先扣除商品列表中的批号库存数据
                            if (new BigDecimal(stockBatch.getGssbQty()).compareTo(BigDecimal.ZERO) > 0) {
                                for (int j = 0; j < saleDList.size(); j++) {
                                    if (new BigDecimal(stockBatch.getGssbQty()).compareTo(BigDecimal.ZERO) > 0) {
                                        GaiaSdSaleD saleD = saleDList.get(j);
                                        if (stockBatch.getGssbBatchNo().equals(saleD.getGssdBatchNo()) && saleD.getGssdProId().equals(stockBatch.getGssbProId()) && new BigDecimal(saleD.getGssdQty()).compareTo(BigDecimal.ZERO) > 0) {
                                            if (new BigDecimal(stockBatch.getGssbQty()).compareTo(new BigDecimal(saleD.getGssdQty())) > -1) {
                                                stockBatch.setGssbQty(new BigDecimal(stockBatch.getGssbQty()).subtract(new BigDecimal(saleD.getGssdQty())).toString());
                                                saleD.setGssdQty(BigDecimal.ZERO.toString());
                                            } else {
                                                saleD.setGssdQty(new BigDecimal(saleD.getGssdQty()).subtract(new BigDecimal(stockBatch.getGssbQty())).toString());
                                                stockBatch.setGssbQty(BigDecimal.ZERO.toString());
                                            }
                                        }
                                    } else break;
                                }
                            }
                            //扣减完后 库存大于零的话 可以用于赠品扣减
                            if (new BigDecimal(stockBatch.getGssbQty()).compareTo(BigDecimal.ZERO) > 0) {
                                if (new BigDecimal(stockBatch.getGssbQty()).compareTo(qtyTemp) > -1 && qtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    GaiaSdSaleD saleD = saleDListTemp.stream().filter(saled -> saled.getGssdProId().equals(stockBatch.getGssbProId()) && stockBatch.getGssbBatchNo().equals(saled.getGssdBatchNo())).findFirst().orElse(null);
                                    if (ObjectUtil.isNotEmpty(saleD)) {
                                        saleD.setGssdQty(new BigDecimal(saleD.getGssdQty()).add(qtyTemp).toString());
                                        saleD.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(saleD.getGssdQty())));
                                        saleD.setGssdZkAmt(item.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).subtract(saleD.getGssdAmt()));
                                    } else {
                                        GaiaSdSaleD temp = new GaiaSdSaleD();
                                        temp.setClientId(gaiaSdSaleH.getClientId());
                                        temp.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
                                        temp.setGssdBrId(gaiaSdSaleH.getGsshBrId());
                                        temp.setGssdDate(gaiaSdSaleH.getGsshDate());
                                        temp.setGssdProId(item.getGssdProId());
                                        temp.setGssdSerial(String.valueOf(inData.getGaiaSdSaleDList().size() + 1));
                                        temp.setGssdPrc1(item.getGssdPrc1());
                                        temp.setGssdPrc2(item.getGssdPrc2());
                                        temp.setGssdQty(qtyTemp.toString());
                                        temp.setGssdBatchNo(stockBatch.getGssbBatchNo());
                                        temp.setGssdValidDate(stockBatch.getGssbVaildDate());
                                        temp.setGssdAmt(item.getGssdPrc2().multiply(qtyTemp));
                                        temp.setGssdZkAmt(item.getGssdPrc1().multiply(qtyTemp).subtract(temp.getGssdAmt()));
                                        temp.setGssdDose("1");
                                        temp.setGssdPmId("PROM_GIFT");
                                        temp.setGssdPmActivityId(item.getGssdPmActivityId());
                                        temp.setGssdSalerId(inData.getGaiaSdSaleDList().get(0).getGssdSalerId());
                                        temp.setGssdProStatus("促销");
                                        temp.setGssdOriQty(qtyTemp);
                                        temp.setGssdRecipelFlag("0");
                                        temp.setGssdMovTax(new BigDecimal(item.getMovTax()));
                                        saleDListTemp.add(temp);
                                    }
                                    qtyTemp = BigDecimal.ZERO;
                                    break;
                                } else {
                                    GaiaSdSaleD saleD = saleDListTemp.stream().filter(saled -> saled.getGssdProId().equals(stockBatch.getGssbProId()) && stockBatch.getGssbBatchNo().equals(saled.getGssdBatchNo())).findFirst().orElse(null);
                                    if (ObjectUtil.isNotEmpty(saleD)) {
                                        saleD.setGssdQty(new BigDecimal(saleD.getGssdQty()).add(new BigDecimal(stockBatch.getGssbQty())).toString());
                                        saleD.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(saleD.getGssdQty())));
                                        saleD.setGssdZkAmt(item.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).subtract(saleD.getGssdAmt()));
                                    } else {
                                        GaiaSdSaleD temp = new GaiaSdSaleD();
                                        temp.setClientId(gaiaSdSaleH.getClientId());
                                        temp.setGssdBillNo(gaiaSdSaleH.getGsshBillNo());
                                        temp.setGssdBrId(gaiaSdSaleH.getGsshBrId());
                                        temp.setGssdDate(gaiaSdSaleH.getGsshDate());
                                        temp.setGssdProId(item.getGssdProId());
                                        temp.setGssdPrc1(item.getGssdPrc1());
                                        temp.setGssdPrc2(item.getGssdPrc2());
                                        temp.setGssdQty(stockBatch.getGssbQty().toString());
                                        temp.setGssdBatchNo(stockBatch.getGssbBatchNo());
                                        temp.setGssdValidDate(stockBatch.getGssbVaildDate());
                                        temp.setGssdAmt(item.getGssdPrc2().multiply(new BigDecimal(temp.getGssdQty())));
                                        temp.setGssdZkAmt(item.getGssdPrc1().multiply(item.getGssdQty()).subtract(temp.getGssdAmt()));
                                        temp.setGssdDose("1");
                                        temp.setGssdPmId("PROM_GIFT");
                                        temp.setGssdPmActivityId(item.getGssdPmActivityId());
                                        temp.setGssdSalerId(inData.getGaiaSdSaleDList().get(0).getGssdSalerId());
                                        temp.setGssdProStatus("促销");
                                        temp.setGssdOriQty(item.getGssdQty());
                                        temp.setGssdRecipelFlag("0");
                                        temp.setGssdMovTax(new BigDecimal(item.getMovTax()));
                                        saleDListTemp.add(temp);
                                    }
                                    qtyTemp = qtyTemp.subtract(new BigDecimal(stockBatch.getGssbQty()));
                                }
                            }
                        }
                        if (qtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                            //库存数量不足,无法扣减完,无法销售
                            throw new BusinessException(UtilMessage.GIFT_STOCK_LIMIT);
                        }
                        for (int i = 0; i < saleDListTemp.size(); i++) {
                            saleDListTemp.get(i).setGssdSerial(String.valueOf(inData.getGaiaSdSaleDList().size() + i + 1));
                        }
                        inData.getGaiaSdSaleDList().addAll(saleDListTemp);
                    }
                }
            }
        }
        List<GaiaUserUsemap> usemapList = new ArrayList<>();
        for (int i = 0; i < inData.getGaiaSdSaleDList().size(); ++i) {
            GaiaSdSaleD item = inData.getGaiaSdSaleDList().get(i);
            this.gaiaSdSaleDMapper.insert(item);
        }
    }

    @Override
    public GaiaSdRechargeCard getRechargeByCardId(MemberPromotionInData inData) {
        return this.gaiaSdRechargeCardMapper.getAccountByCardId(inData.getClient(), inData.getGsspmCardNo());
    }

    @Override
    public List<GetEmpOutData> pharmacist(GetLoginOutData userInfo) {
        List<GetEmpOutData> outData = this.gaiaAuthconfiDataMapper.pharmacist(userInfo);
        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deletePendingOrder(GetLoginOutData userInfo, Map<String, String> map) {
        GaiaSdSaleH saleH = new GaiaSdSaleH();
        saleH.setClientId(userInfo.getClient());
        saleH.setGsshBrId(userInfo.getDepId());
        saleH.setGsshDate(map.get("gsshDate"));
        saleH.setGsshBillNo(map.get("gssdBillNo"));
        saleH.setGsshCallAllow("0");
        this.gaiaSdSaleHMapper.update(saleH);
    }

    @Override
    public CommodityCostDto commodityCost(GetLoginOutData userInfo, Map<String, String> map) {
        map.put("client", userInfo.getClient());
        map.put("brId", userInfo.getDepId());
        CommodityCostDto costDto = this.gaiaSdProductPriceMapper.commodityCost(map);
        return costDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createSaleHand(GetLoginOutData userInfo, GetCreateSaleOutData inData) {
        GaiaSdSaleH gaiaSdSaleH = inData.getGaiaSdSaleH();
        GaiaSdSaleH gaiaSdSaleHSource = this.gaiaSdSaleHMapper.selectByBillNo(gaiaSdSaleH.getGsshBillNo(), gaiaSdSaleH.getClientId(), gaiaSdSaleH.getGsshBrId());
        if (ObjectUtil.isNotEmpty(gaiaSdSaleHSource)) {
            throw new BusinessException("已支付商品无法挂单!");
        }
        GaiaSdSaleHandH saleHandH = this.gaiaSdSaleHandHMapper.getSaleHByBillNo(gaiaSdSaleH.getGsshBillNo(), gaiaSdSaleH.getClientId(), gaiaSdSaleH.getGsshBrId());
        String date = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        String time = DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN);
        if (ObjectUtil.isEmpty(saleHandH)) {
            GaiaSdSaleHandH handH = CopyUtil.copy(gaiaSdSaleH, GaiaSdSaleHandH.class);
            handH.setGsshHideRemark(inData.getHideRemark());
            handH.setSaleHFlag(UtilMessage.N);
            this.gaiaSdSaleHandHMapper.insert(handH);
        } else {
            if ("Y".equals(saleHandH.getSaleHFlag())) {
                throw new BusinessException("已支付商品无法挂单!");
            }
            saleHandH.setGsshEmp(gaiaSdSaleH.getGsshEmp());
            saleHandH.setGsshHykNo(gaiaSdSaleH.getGsshHykNo());
            saleHandH.setGsshZkAmt(gaiaSdSaleH.getGsshZkAmt());
            saleHandH.setGsshYsAmt(gaiaSdSaleH.getGsshYsAmt());
            saleHandH.setGsshEmpGroupName(gaiaSdSaleH.getGsshEmpGroupName());
            saleHandH.setGsshHideFlag(gaiaSdSaleH.getGsshHideFlag());
            saleHandH.setCentralAndWestern(gaiaSdSaleH.getCentralAndWestern());
            saleHandH.setHerbalNum(gaiaSdSaleH.getHerbalNum());
            saleHandH.setGsshCallAllow(gaiaSdSaleH.getGsshCallAllow());
            saleHandH.setGsshOrderSource(gaiaSdSaleH.getGsshOrderSource());
            saleHandH.setGsshDate(date);
            saleHandH.setGsshTime(time);
            saleHandH.setGsshHideRemark(inData.getHideRemark());
            this.gaiaSdSaleHandHMapper.update(saleHandH);
        }

        this.gaiaSdSaleHandDMapper.delete(gaiaSdSaleH.getClientId(), gaiaSdSaleH.getGsshBrId(), gaiaSdSaleH.getGsshBillNo());
        String now = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        List<GaiaSdSaleHandD> copyList = CopyUtil.copyList(inData.getGaiaSdSaleDList(), GaiaSdSaleHandD.class);
        if (CollUtil.isNotEmpty(copyList)) {
            copyList.forEach(handD -> {
                handD.setSaleDFlag(UtilMessage.N);
                handD.setGssdDate(date);
            });

            this.gaiaSdSaleHandDMapper.insertList(copyList);
        }
        if ("7".equals(gaiaSdSaleH.getGsshHideFlag())) {
            List<GaiaSdSaleD> wfList = inData.getGaiaSdSaleDList().stream().filter(item -> "1".equals(item.getChangePriceWFFlag())).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(wfList)) {
                //发起工作流
                GetWfCreateInData wfInData = new GetWfCreateInData();
                wfInData.setToken(userInfo.getToken());
                wfInData.setWfTitle("收银界面改价");
                wfInData.setWfDescription("收银界面改价，发送工作流任务审批");
                wfInData.setWfOrder(gaiaSdSaleH.getGsshBillNo());
                wfInData.setWfSite(userInfo.getDepId());
                wfInData.setWfDefineCode("GAIA_WF_052");
                List<ChangePriceWFData> wfDetailList = new ArrayList();
                List<String> proSelfCodeList = wfList.stream().map(item -> item.getGssdProId()).collect(Collectors.toList());
                BigDecimal totalAmt = wfList.stream().map(GaiaSdSaleD::getGssdAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
                List<ChangePriceWFData> costInfo = gaiaProductBasicMapper.listCost(userInfo.getClient(), userInfo.getDepId(), proSelfCodeList);
                for (int i = 0; i < wfList.size(); i++) {
                    ChangePriceWFData priceWFData = new ChangePriceWFData();
                    priceWFData.setIndex(String.valueOf(i + 1));
                    priceWFData.setStoCode(wfList.get(i).getGssdBrId());
                    priceWFData.setCreateTime(date);
                    priceWFData.setBillAmt(totalAmt.toString());
                    priceWFData.setBillCode(gaiaSdSaleH.getGsshBillNo());
                    priceWFData.setRemark(gaiaSdSaleH.getGsshDzqdyActno5());
                    priceWFData.setProSelfCode(wfList.get(i).getGssdProId());
                    priceWFData.setBatchNo(wfList.get(i).getGssdBatchNo());
                    priceWFData.setValidDay(wfList.get(i).getGssdValidDate());
                    priceWFData.setSaleQty(new BigDecimal(wfList.get(i).getGssdQty()));
                    priceWFData.setSaleAmt(wfList.get(i).getGssdPrc1().multiply(priceWFData.getSaleQty()).setScale( 2, BigDecimal.ROUND_HALF_UP));
                    priceWFData.setChangePrice(wfList.get(i).getGssdPrc2());
                    priceWFData.setChangeAmt(wfList.get(i).getGssdAmt());
                    for (ChangePriceWFData proInfo : costInfo) {
                        if (wfList.get(i).getGssdProId().equals(proInfo.getProSelfCode())) {
                            priceWFData.setProCommonname(proInfo.getProCommonname());
                            priceWFData.setProSpecs(proInfo.getProSpecs());
                            priceWFData.setProFactoryName(proInfo.getProFactoryName());
                            priceWFData.setPrice(proInfo.getPrice());
                            priceWFData.setStoName(proInfo.getStoName());
                            priceWFData.setCost(proInfo.getCost().setScale( 2, BigDecimal.ROUND_HALF_UP));
                            //原毛利   (原来销售价 - 成本)*销售数量
                            BigDecimal oriGross = proInfo.getPrice().subtract(proInfo.getCost());
                            priceWFData.setOriGross(oriGross.multiply(priceWFData.getSaleQty()).setScale( 2, BigDecimal.ROUND_HALF_UP));
                            //原毛利率
                            priceWFData.setOriGrossRate(Util.divide(Util.checkNull(priceWFData.getOriGross()),Util.checkNull(priceWFData.getSaleAmt())).multiply(new BigDecimal("100")).toString()+"%");
                            //改价后毛利  （改价后销售价 - 成本）* 销售数量
                            BigDecimal changeGross = priceWFData.getChangePrice().subtract(proInfo.getCost());
                            priceWFData.setChangeGross(changeGross.multiply(priceWFData.getSaleQty()).setScale( 2, BigDecimal.ROUND_HALF_UP));
                            //改价后毛利率
                            priceWFData.setChangGrossRate(Util.divide(Util.checkNull(priceWFData.getChangeGross()),Util.checkNull(priceWFData.getChangeAmt())).multiply(new BigDecimal("100")).toString()+"%");
                            break;
                        }
                    }
                    wfDetailList.add(priceWFData);
                }
                System.out.println("wfDetailList = " + wfDetailList);
                wfInData.setWfDetail(wfDetailList);
                String result = authService.createWorkflow(wfInData);
                log.info("改价-请求创建工作流：{}", JSON.toJSONString(wfInData));
                com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(result);
                log.info("改价-工作流回调：{}", JSON.toJSONString(result));
                if (jsonObject.containsKey("code") && !"0".equals(jsonObject.getString("code"))) {
                    log.info("改价-创建工作流失败：{}", JSON.toJSONString(result));
                    throw new BusinessException("改价-创建工作流失败");
                }
            }
        }
    }

    @Override
    public List<GetRestOrderInfoOutData> getSaleHand(GetLoginOutData userInfo, Map<String, String> map) {
        List<GetRestOrderInfoOutData> gsshDate = null;
        if ("0".equals(map.get("HIDE_ALLWAYS"))) {
            gsshDate = this.gaiaSdSaleHandHMapper.getSaleHand(userInfo.getClient(), userInfo.getDepId(), DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN), null);
        } else if ("1".equals(map.get("HIDE_ALLWAYS"))) {
            gsshDate = this.gaiaSdSaleHandHMapper.getAllSaleHand(userInfo.getClient(), userInfo.getDepId(),map.get("startDate"), map.get("endDate"));
        }
        if (CollUtil.isNotEmpty(gsshDate)) {
            gsshDate.forEach(datum -> {
                datum.setGsshDateTime(DateUtil.format(DateUtil.parse(datum.getGsshDateTime()), DatePattern.NORM_DATETIME_PATTERN));
                datum.setNowDate(DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN));
            });
        }
        return gsshDate;
    }

    @Override
    public List<RestOrderDetailOutData> getSaleHandDetail(GetLoginOutData userInfo, Map<String, String> map) {
        List<RestOrderDetailOutData> saleHandDetail = this.gaiaSdSaleHandDMapper.getSaleHandDetail(userInfo.getClient(), userInfo.getDepId(), map.get("gssdBillNo"), null);
        saleHandDetail.forEach(outData -> outData.setNum(outData.getNum()));
        return saleHandDetail;
    }

    @Override
    public List<MemberPurchaseRecordOutData> getClientPurchaseRecordsByMember(String client, String hykNo) {
        return gaiaSdSaleDMapper.getClientPurchaseRecordsByMember(client, hykNo);
    }

    @Override
    public List<RestOrderDetailDto> getPrintDetails(Map<String, String> map) {
        return this.gaiaSdSaleHMapper.getPrintDetails(map);
    }

    @Override
    public RestOrderChangeAll getPrintAll(Map<String, String> map) {
        RestOrderChangeAll changeAll = new RestOrderChangeAll();
        List<RestOrderDetailDto> details = this.gaiaSdSaleHMapper.getPrintDetails(map);
        changeAll.setDetailDtos(details);

        boolean flag = details.stream().anyMatch(outDatum -> "3".equals(outDatum.getProStorageArea()));;
        if (flag) {
            List<RestOrderChangeDto> dtoList = stockBatchMapper.getChangeByBillNo(map);
            changeAll.setDtoList(dtoList);
        }
        return changeAll;
    }

    @Override
    public void updateHandSaleH(GetLoginOutData userInfo, Map<String, String> map) {
        GaiaSdSaleHandH handH = new GaiaSdSaleHandH();
        handH.setClientId(userInfo.getClient());
        handH.setGsshBrId(userInfo.getDepId());
        handH.setGsshBillNo(map.get("gssdBillNo"));
        handH.setGsshHideFlag("3");
        this.gaiaSdSaleHandHMapper.update(handH);
    }

    @Override
    public List<GaiaSdElectronChange> getGiveAndUseAll(Map<String, String> map) {
        return this.electronChangeMapper.getGiveAndUseAll(map.get("client"),map.get("billNo"));
    }

    @Override
    public List<GetRestOrderInfoOutData> listSaleHand(GetLoginOutData userInfo, Map<String, String> map) {
        List<GetRestOrderInfoOutData> gsshDate = null;
        String gsshBillType = "";
        String gsshHideFlag = "5";
        String gsshOrderSource = "";
        String gsshBillOrderId = "";
        if ("5".equals(map.get("gsshBillType"))) {
            gsshBillType = "5";
            gsshHideFlag = "6";
        }
        if (StringUtils.isNotBlank(map.get("gsshHideFlag"))) {
            gsshHideFlag = map.get("gsshHideFlag");
        }
        if (StringUtils.isNotBlank(map.get("gsshOrderSource"))) {
            gsshOrderSource = map.get("gsshOrderSource");
        }
        if (StringUtils.isNotBlank(map.get("billCode"))) {
            gsshBillOrderId = map.get("billCode");
        }
        if (StringUtils.isNotBlank(map.get("gsshBillOrderId"))) {
            gsshBillOrderId = map.get("gsshBillOrderId");
        }
        if ("7".equals(map.get("gsshHideFlag"))) {
            gsshHideFlag = "7";
        }
        gsshDate = this.gaiaSdSaleHandHMapper.listSaleHand(userInfo.getClient(), userInfo.getDepId(), map.get("startDate"),
                map.get("endDate"), gsshHideFlag, gsshBillType, gsshOrderSource, gsshBillOrderId);
        if (CollUtil.isNotEmpty(gsshDate)) {
            int i = 1;
            for (GetRestOrderInfoOutData datum : gsshDate) {
                datum.setGsshDateTime(DateUtil.format(DateUtil.parse(datum.getGsshDateTime()), DatePattern.NORM_DATETIME_PATTERN));
                datum.setNowDate(DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN));
                datum.setIndex(i);
                i++;
            }
        }
        return gsshDate;
    }

    @Override
    public List<RestOrderDetailOutData> listRestOrderDetail(GetLoginOutData userInfo, Map<String, String> map) {
        String gsshHideFlag = "5";
        // 6-门店代售
        if (StringUtils.isNotBlank(map.get("gsshHideFlag"))) {
            gsshHideFlag = map.get("gsshHideFlag").toString();
        }
        if ("7".equals(map.get("gsshHideFlag"))) {
            gsshHideFlag = "7";
        }
        List<RestOrderDetailOutData> saleHandDetail = this.gaiaSdSaleHandDMapper.listRestOrderDetail(userInfo.getClient(), userInfo.getDepId(), map.get("gssdBillNo"), null, gsshHideFlag);
        saleHandDetail.forEach(outData -> outData.setNum(NumberUtil.parseNumber(outData.getNum()).toString()));
        return saleHandDetail;
    }

    @Override
    public List<GetRestOrderInfoOutData> getHisHand(GetLoginOutData userInfo, Map<String, String> map) {
        List<GetRestOrderInfoOutData> gsshDate = null;
        gsshDate = this.gaiaSdSaleHandHMapper.getHisHand(userInfo.getClient(),
                userInfo.getDepId(),
                map.get("startDate"),
                map.get("endDate"),
                map.get("billNo"),
                map.get("proCode"));
        if (CollUtil.isNotEmpty(gsshDate)) {
            for(int i=0;i<gsshDate.size();i++) {
                GetRestOrderInfoOutData datum = gsshDate.get(i);
                datum.setIndex(i+1);
                datum.setGsshDateTime(DateUtil.format(DateUtil.parse(datum.getGsshDateTime()), DatePattern.NORM_DATETIME_PATTERN));
                datum.setNowDate(DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN));
            }
            gsshDate.forEach(datum -> {

            });
        }
        return gsshDate;
    }


    @Override
    public JsonResult updateSaleHandStatus(GetLoginOutData userInfo, Map<String, String> map) {
        String client = userInfo.getClient();
        String stoCode = userInfo.getDepId();
        String saleBillNo = map.get("saleBillNo");
        String gsshBdFlag = map.get("gsshBdFlag");
        gaiaSdSaleHandHMapper.updateSaleHandStatus(client, stoCode, saleBillNo, gsshBdFlag);
        log.info("配送代煎，更新挂单表配送状态，client:{}, stoCode:{}, saleBillNo:{}, gsshBdFlag:{}", client, stoCode, saleBillNo, gsshBdFlag);
        return JsonResult.success();
    }

    @Override
    public JsonResult getSite(GetLoginOutData userInfo) {
        String client = userInfo.getClient();
        String stoCode = userInfo.getDepId();
        StoreDcDataDto site = storeDataMapper.getSite(client, stoCode);
        return JsonResult.success(site);
    }

    //true 已配送
    @Override
    public JsonResult updateAndReturnStatus(GetLoginOutData userInfo, Map<String, String> map) {
        String saleBillNo = map.get("saleBillNo");
        String client = userInfo.getClient();
        String stoCode = userInfo.getDepId();
        GaiaSdSaleHandH gaiaSdSaleHandH = gaiaSdSaleHandHMapper.selectByBillNo(saleBillNo, client, stoCode);
        if (gaiaSdSaleHandH == null) {
            return JsonResult.success(false);
        }
        gaiaSdSaleHandH.setGsshHideFlag("3");
        this.gaiaSdSaleHandHMapper.update(gaiaSdSaleHandH);
        String gsshBdFlag = gaiaSdSaleHandH.getGsshBdFlag();
        log.info("配送代煎，挂单作废并返回配送状态，client:{}, stoCode:{}, saleBillNo:{}, gsshBdFlag:{}", client, stoCode, saleBillNo, gsshBdFlag);
        return JsonResult.success("1".equals(gsshBdFlag));
    }

    @Override
    public List<GetQueryMemberOutData> memberOfTheQuery(Map<String, Object> map, GetLoginOutData userInfo) {
        map.put("client",userInfo.getClient());
        List<GetQueryMemberOutData> dto = gaiaSdMemberBasicMapper.memberOfTheQuery(map);
        if (CollUtil.isNotEmpty(dto)) {
            for (GetQueryMemberOutData outData : dto) {
                GaiaSdMemberBasic basic = gaiaSdMemberBasicMapper.getMemberByMembrId(userInfo.getClient(),outData.getMemberId());
                GaiaSdMemberCardData cardData = gaiaSdMemberBasicMapper.getMemberCardByMembrId(userInfo.getClient(),outData.getMemberId());
                outData.setBasic(basic);
                outData.setCardData(cardData);

                GaiaSdMemberCardData param = new GaiaSdMemberCardData();
                param.setClient(userInfo.getClient());
                param.setGsmbcBrId(userInfo.getDepId());
                param.setGsmbcCardId(outData.getCardNum());
                List<ElectronDetailOutData> electronDetailOut = gaiaSdElectronChangeMapper.queryCanUseElectron(param);
                List<ElectronDetailOutData> electronDetailOutData = new ArrayList<>();
                if (CollUtil.isNotEmpty(electronDetailOut)) {
                    Map<String, List<ElectronDetailOutData>> aa = electronDetailOut.stream().collect(Collectors.groupingBy(ElectronDetailOutData::getGsecId));
                    aa.forEach((key, value) -> {
                        electronDetailOutData.add(value.stream().max(Comparator.comparing(e -> e.getGsebsCreateDate() + e.getGsebsCreateTime())).get());
                    });
                    electronDetailOutData.forEach(item -> {
                        if (StrUtil.isNotBlank(item.getGsecsProGroup())) {
                            Example example = new Example(GaiaSdProductsGroup.class);
                            example.createCriteria().andEqualTo("clientId", param.getClient()).andEqualTo("gspgId", item.getGsecsProGroup());
                            item.setProGroup(gaiaSdProductsGroupMapper.selectByExample(example));
                        }
                    });
                }

                if (CollectionUtil.isNotEmpty(electronDetailOutData)) {
                    outData.setElNum(String.valueOf(electronDetailOutData.size()));
                } else {
                    outData.setElNum("0");
                }
            }
        }
        return dto;
    }

    @Override
    public void orderCommit(Map<String, String> map) {
        GaiaSdSaleHandH saleHandH = gaiaSdSaleHandHMapper.selectByBillNo(map.get("wfOrder"), map.get("clientId"), map.get("site"));
        if (ObjectUtil.isEmpty(saleHandH)) {
            throw new BusinessException("未查询到该单据！");
        }
        map.put("billCode", map.get("wfOrder"));
        if ("1".equals(map.get("wfStatus"))) {
            map.put("flag", "0");
            //驳回
            //1.0变更挂单主表状态
            saleHandH.setGsshCallAllow("3");
            gaiaSdSaleHandHMapper.updateByAppPrimaryKey(saleHandH);
            //2.0推送mq消息到fx主页面
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("Message");
            mqReceiveData.setCmd("changePrice");
            map.put("msg","改价提交的销售单审核完成，是否调出继续销售？");
            mqReceiveData.setData(map);
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, map.get("clientId") + "." + map.get("site"), JSON.toJSON(mqReceiveData));
        } else {
            //同意
            map.put("flag", "1");
            //1.0变更挂单主表状态
            saleHandH.setGsshCallAllow("1");
            gaiaSdSaleHandHMapper.updateByAppPrimaryKey(saleHandH);
            //2.0推送mq消息到fx主页面
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("Message");
            mqReceiveData.setCmd("changePrice");
            map.put("msg","改价提交的销售单审批不通过，请按照当前生效价格销售！是否调出继续销售？");
            mqReceiveData.setData(map);
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, map.get("clientId") + "." + map.get("site"), JSON.toJSON(mqReceiveData));
        }
    }

}
