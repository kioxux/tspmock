package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdMemberBasicMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.service.MemberSaleSummaryService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberSaleSummaryServiceImpl implements MemberSaleSummaryService {
    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Override
    public List<GetMemberOutData> findMembersByPage(GetMemberInData inData) {
        List<GetMemberOutData> outData = this.gaiaSdMemberBasicMapper.selectMembersByCondition(inData);
        if (ObjectUtil.isNotEmpty(outData)) {
            for (int i = 0; i < outData.size(); ++i) {
                (outData.get(i)).setIndex(i + 1);
            }
        }
        return outData;
    }

    @Override
    public List<MemberSaleSummaryOutData> memberSalesSummary(MemberSaleSummaryInData inData) {
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        inData.setFlag(flag);
        List<MemberSaleSummaryOutData> outData = this.gaiaSdMemberBasicMapper.findMemberSalesSummary(inData);

        /*PageInfo pageInfo = null;
        if(ObjectUtil.isNotEmpty(outData)){
            IntStream.range(0, outData.size()).forEach(i -> outData.get(i).setIndex(i + 1));
            MemberSaleSummaryOutDataTotal outTotal = gaiaSdMemberBasicMapper.findMemberSalesSummaryTotal(inData);
            pageInfo = new PageInfo(outData,outTotal);
        }else {
            pageInfo = new PageInfo();
        }*/
        return outData;
    }

    @Override
    public Map<String,Object> queryMemberCardList(GetSalesSummaryOfSalesmenReportInData inData) {
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> totalCensus = new HashMap<>();
        if(StringUtils.isEmpty(inData.getQueryStartDate())){
            throw new BusinessException(" 起始日期不能为空！");
        }
        if(StringUtils.isEmpty(inData.getQueryEndDate())){
            throw new BusinessException(" 结束日期不能为空！");
        }
        BigDecimal grossProfitAmt = BigDecimal.ZERO;
        String grossProfitRate = "0.00%";
        BigDecimal discountAmt = BigDecimal.ZERO;
        String discountRate = "0.00%";
        BigDecimal costAmt = BigDecimal.ZERO;
        BigDecimal ssAmount = BigDecimal.ZERO;
        BigDecimal gssdnormalAmt = BigDecimal.ZERO;
        BigDecimal dailyPayAmt = BigDecimal.ZERO;
        BigDecimal dailyPayCount = BigDecimal.ZERO;
        BigDecimal selledDays = BigDecimal.ZERO;
        BigDecimal tradedTime = BigDecimal.ZERO;
        BigDecimal proAvgCount = BigDecimal.ZERO;
        BigDecimal billAvgPrice = BigDecimal.ZERO;
        BigDecimal allCostAmt = BigDecimal.ZERO;
        List<Map<String,Object>> itemCensus = gaiaSdMemberBasicMapper.queryMemberCardList(inData);
        if (itemCensus.size() > 0 && itemCensus != null){
            for (Map<String,Object> item : itemCensus) {
                grossProfitAmt = grossProfitAmt.add(new BigDecimal(item.get("grossProfitAmt").toString()));
                discountAmt = discountAmt.add(new BigDecimal(item.get("discountAmt").toString()));
                costAmt = costAmt.add(new BigDecimal(item.get("costAmt").toString()));
                ssAmount = ssAmount.add(new BigDecimal(item.get("ssAmount").toString()));
                gssdnormalAmt = gssdnormalAmt.add(new BigDecimal(item.get("gssdnormalAmt").toString()));
                dailyPayCount = dailyPayCount.add(new BigDecimal(item.get("dailyPayCount").toString()));
                dailyPayAmt = dailyPayAmt.add(new BigDecimal(item.get("dailyPayAmt").toString()));
                selledDays = selledDays.add(new BigDecimal(item.get("selledDays").toString()));
                tradedTime = tradedTime.add(new BigDecimal(item.get("tradedTime").toString()));
                proAvgCount = proAvgCount.add(new BigDecimal(item.get("proAvgCount").toString()));
//                if (item.containsKey("billAvgPrice")) {
//                    billAvgPrice = billAvgPrice.add(new BigDecimal(item.get("billAvgPrice").toString()));
//                }else {
//                    billAvgPrice = billAvgPrice.add(new BigDecimal("0"));
//                }
                allCostAmt = allCostAmt.add(new BigDecimal(item.get("allCostAmt").toString()));
                String discountRateNum = StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").indexOf("%"));
                item.put("discountRate",new BigDecimal(discountRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                String grossProfitRateNum = StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").indexOf("%"));
                item.put("grossProfitRate",new BigDecimal(grossProfitRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
            }
            totalCensus.put("grossProfitAmt",grossProfitAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("discountAmt",discountAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("costAmt",costAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("ssAmount",ssAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("gssdnormalAmt",gssdnormalAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("selledDays",selledDays.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("tradedTime",tradedTime.setScale(2,BigDecimal.ROUND_HALF_UP));
            totalCensus.put("allCostAmt",allCostAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
            if (!(selledDays.compareTo(BigDecimal.ZERO) == 0)) {
                dailyPayCount = tradedTime.divide(selledDays, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                dailyPayAmt = ssAmount.divide(selledDays, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            totalCensus.put("dailyPayAmt",dailyPayAmt);
            totalCensus.put("dailyPayCount",dailyPayCount);
            totalCensus.put("proAvgCount",proAvgCount.divide(new BigDecimal(itemCensus.size()),4,BigDecimal.ROUND_HALF_UP).setScale(2,BigDecimal.ROUND_HALF_UP));
            if (!(tradedTime.compareTo(BigDecimal.ZERO) == 0)) {
                billAvgPrice = ssAmount.divide(tradedTime, 2, BigDecimal.ROUND_HALF_UP);
            }
            totalCensus.put("billAvgPrice", billAvgPrice);
            if(!(ssAmount.compareTo(BigDecimal.ZERO) == 0)) {
                grossProfitRate = grossProfitAmt.divide(ssAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
            }
            if(!(gssdnormalAmt.compareTo(BigDecimal.ZERO) == 0)) {
                discountRate = discountAmt.divide(gssdnormalAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
            }
            totalCensus.put("grossProfitRate",grossProfitRate);
            totalCensus.put("discountRate",discountRate);
            totalCensus.put("totalSelledDays",itemCensus.get(0).get("totalSelledDays"));
            result.put("totalCensus",totalCensus);
            result.put("itemCensus",itemCensus);
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> selectHykList(Map<String, Object> inData) {
        return gaiaSdMemberBasicMapper.selecthykList(inData);
    }

    @Override
    public List<MemberShipTypeData> membershipCardType(String client) {
        return this.gaiaSdMemberBasicMapper.membershipCardType(client);
    }

    //会员消费汇总表
    @Override
    public Map<String,Object> selectMemberCollectList(MemberSalesInData inData){
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> listNum = new HashMap<>();
        //消费天数
        BigDecimal selledDays = BigDecimal.ZERO;
        //应收金额
        BigDecimal gssdnormalAmt = BigDecimal.ZERO;
        //实收金额
        BigDecimal ssAmount = BigDecimal.ZERO;
        //含税成本额
        BigDecimal allCostAmt = BigDecimal.ZERO;
        //毛利额
        BigDecimal grossProfitAmt = BigDecimal.ZERO;
        //毛利率
        String grossProfitRate = "0.00%";
        //消费次数
        BigDecimal tradedTime = BigDecimal.ZERO;
        //消费单价
        BigDecimal dailyPayAmt = BigDecimal.ZERO;
        //折扣金额
        BigDecimal discountAmt = BigDecimal.ZERO;
        //折扣率
        String discountRate = "0.00%";
        //消费频次
        BigDecimal dailyPayCount = BigDecimal.ZERO;
        //客品次
        BigDecimal proAvgCount = BigDecimal.ZERO;
        //品单价
        BigDecimal billAvgPrice = BigDecimal.ZERO;
        //期间天数
        BigDecimal totalSelledDays = BigDecimal.ZERO;
        //商品数目
        BigDecimal count = BigDecimal.ZERO;
        //商品品项数
        BigDecimal proCount = BigDecimal.ZERO;
        if (ObjectUtil.isEmpty(inData.getStartDate())){
            throw new BusinessException("开始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())){
            throw new BusinessException("结束日期不能为空！");
        }
        if (inData.getState().equals("0")){
            //汇总
            List<LinkedHashMap<String,Object>> list = this.gaiaSdMemberBasicMapper.selectMemberList(inData);
            if(list.size() > 0 && list != null){
                for (Map<String,Object> item : list){
                    totalSelledDays = totalSelledDays.add(new BigDecimal(item.get("totalSelledDays").toString()));
                    selledDays = selledDays.add(new BigDecimal(item.get("selledDays").toString()));
                    gssdnormalAmt = gssdnormalAmt.add(new BigDecimal(item.get("gssdnormalAmt").toString()));
                    ssAmount = ssAmount.add(new BigDecimal(item.get("ssAmount").toString()));
                    allCostAmt = allCostAmt.add(new BigDecimal(item.get("allCostAmt").toString()));
                    grossProfitAmt = grossProfitAmt.add(new BigDecimal(item.get("grossProfitAmt").toString()));
                    String grossProfitRateNum = StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").indexOf("%"));
                    item.put("grossProfitRate",new BigDecimal(grossProfitRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    tradedTime = tradedTime.add(new BigDecimal(item.get("tradedTime").toString()));
                    dailyPayAmt = dailyPayAmt.add(new BigDecimal(item.get("dailyPayAmt").toString()));
                    discountAmt = discountAmt.add(new BigDecimal(item.get("discountAmt").toString()));
                    String discountRateNum = StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").indexOf("%"));
                    item.put("discountRate",new BigDecimal(discountRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    dailyPayCount = dailyPayCount.add(new BigDecimal(item.get("dailyPayCount").toString()));
                    proAvgCount = proAvgCount.add(new BigDecimal(item.get("proAvgCount").toString()));
                    billAvgPrice = billAvgPrice.add(new BigDecimal(item.get("billAvgPrice").toString()));
                    count = count.add(new BigDecimal(item.get("count").toString()));
                    proCount = proCount.add(new BigDecimal(item.get("proCount").toString()));
                }
                listNum.put("count",count);
                listNum.put("proCount",proCount);
                listNum.put("selledDays",selledDays);
                listNum.put("gssdnormalAmt",gssdnormalAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("ssAmount",ssAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("allCostAmt",allCostAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("grossProfitAmt",grossProfitAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("tradedTime",tradedTime);
                if (!(selledDays.compareTo(BigDecimal.ZERO) == 0)) {
                    dailyPayCount = totalSelledDays.divide(tradedTime, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                }
                listNum.put("discountAmt",discountAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("dailyPayCount",dailyPayCount);
                if (!(tradedTime.compareTo(BigDecimal.ZERO) == 0)) {
                    dailyPayAmt = ssAmount.divide(tradedTime, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                    proAvgCount = proCount.divide(tradedTime,2,BigDecimal.ROUND_HALF_UP);
                }
                if (!(count.compareTo(BigDecimal.ZERO) == 0)) {
                    billAvgPrice = ssAmount.divide(count, 2, BigDecimal.ROUND_HALF_UP);
                }
                listNum.put("dailyPayAmt",dailyPayAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("billAvgPrice",billAvgPrice);
                listNum.put("proAvgCount",proAvgCount);
                if(!(ssAmount.compareTo(BigDecimal.ZERO) == 0)) {
                    grossProfitRate = grossProfitAmt.divide(ssAmount,4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("grossProfitRate",grossProfitRate);
                if(!(gssdnormalAmt.compareTo(BigDecimal.ZERO) == 0)) {
                    discountRate = discountAmt.divide(gssdnormalAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("discountRate",discountRate);
                listNum.put("totalSelledDays",totalSelledDays);
                result.put("list",list);
                result.put("listNum",listNum);
            }

        }else {
            //按门店
            List<LinkedHashMap<String,Object>> list = this.gaiaSdMemberBasicMapper.selectMemberStoreList(inData);
            if(list.size() > 0 && list != null){
                for (Map<String,Object> item : list){
                    totalSelledDays = totalSelledDays.add(new BigDecimal(item.get("totalSelledDays").toString())) ;
                    selledDays = selledDays.add(new BigDecimal(item.get("selledDays").toString()));
                    gssdnormalAmt = gssdnormalAmt.add(new BigDecimal(item.get("gssdnormalAmt").toString()));
                    ssAmount = ssAmount.add(new BigDecimal(item.get("ssAmount").toString()));
                    allCostAmt = allCostAmt.add(new BigDecimal(item.get("allCostAmt").toString()));
                    grossProfitAmt = grossProfitAmt.add(new BigDecimal(item.get("grossProfitAmt").toString()));
                    String grossProfitRateNum = StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").indexOf("%"));
                    item.put("grossProfitRate",new BigDecimal(grossProfitRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    tradedTime = tradedTime.add(new BigDecimal(item.get("tradedTime").toString()));
                    dailyPayAmt = dailyPayAmt.add(new BigDecimal(item.get("dailyPayAmt").toString()));
                    discountAmt = discountAmt.add(new BigDecimal(item.get("discountAmt").toString()));
                    String discountRateNum = StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").indexOf("%"));
                    item.put("discountRate",new BigDecimal(discountRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    dailyPayCount = dailyPayCount.add(new BigDecimal(item.get("dailyPayCount").toString()));
                    proAvgCount = proAvgCount.add(new BigDecimal(item.get("proAvgCount").toString()));
                    billAvgPrice = billAvgPrice.add(new BigDecimal(item.get("billAvgPrice").toString()));
                    count = count.add(new BigDecimal(item.get("count").toString()));
                    proCount = proCount.add(new BigDecimal(item.get("proCount").toString()));
                }
                listNum.put("count",count);
                listNum.put("proCount",proCount);
                listNum.put("selledDays",selledDays);
                listNum.put("gssdnormalAmt",gssdnormalAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("ssAmount",ssAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("allCostAmt",allCostAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("grossProfitAmt",grossProfitAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("tradedTime",tradedTime);
                if (!(selledDays.compareTo(BigDecimal.ZERO) == 0)) {
                    dailyPayCount = totalSelledDays.divide(tradedTime, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                }
                listNum.put("discountAmt",discountAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("dailyPayCount",dailyPayCount);
                if (!(tradedTime.compareTo(BigDecimal.ZERO) == 0)) {
                    dailyPayAmt = ssAmount.divide(tradedTime, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                    proAvgCount = proCount.divide(tradedTime,2,BigDecimal.ROUND_HALF_UP);
                }
                if (!(count.compareTo(BigDecimal.ZERO) == 0)) {
                    billAvgPrice = ssAmount.divide(count, 2, BigDecimal.ROUND_HALF_UP);
                }
                listNum.put("dailyPayAmt",dailyPayAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("billAvgPrice",billAvgPrice);
                listNum.put("proAvgCount",proAvgCount);
                if(!(ssAmount.compareTo(BigDecimal.ZERO) == 0)) {
                    grossProfitRate = grossProfitAmt.divide(ssAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("grossProfitRate",grossProfitRate);
                if(!(gssdnormalAmt.compareTo(BigDecimal.ZERO) == 0)) {
                    discountRate = discountAmt.divide(gssdnormalAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("discountRate",discountRate);
                listNum.put("totalSelledDays",totalSelledDays);
                result.put("list",list);
                result.put("listNum",listNum);
            }
        }
        return result;
    }

    //会员消费明细表
    @Override
    public Map<String,Object> selectMenberDetailList(MemberSalesInData inData){
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> listNum = new HashMap<>();
        //应收金额
        BigDecimal gssdnormalAmt = BigDecimal.ZERO;
        //商品数量
        BigDecimal count = BigDecimal.ZERO;
        //实收金额
        BigDecimal ssAmount = BigDecimal.ZERO;
        //折扣金额
        BigDecimal discountAmt = BigDecimal.ZERO;
        //折扣率
        String discountRate = "0.00%";
        //含税成本额
        BigDecimal allCostAmt = BigDecimal.ZERO;
        //毛利额
        BigDecimal grossProfitAmt = BigDecimal.ZERO;
        //毛利率
        String grossProfitRate = "0.00%";
        //商品分类
        if(ObjectUtil.isNotEmpty(inData.getClassArr())){
            inData.setProductClass(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        if(ObjectUtil.isNotEmpty(inData.getProCode())){
            inData.setProductCode(inData.getProCode().split(","));
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())){
            throw new BusinessException("开始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())){
            throw new BusinessException("结束日期不能为空！");
        }
        if (inData.getState().equals("0")){
            //明细
            List<LinkedHashMap<String,Object>> list = gaiaSdMemberBasicMapper.selectMemberPro(inData);
            if(list.size() > 0 && list != null){
                for (Map<String,Object> item : list){
                    gssdnormalAmt = gssdnormalAmt.add(new BigDecimal(item.get("gssdnormalAmt").toString()));
                    ssAmount = ssAmount.add(new BigDecimal(item.get("ssAmount").toString()));
                    allCostAmt = allCostAmt.add(new BigDecimal(item.get("allCostAmt").toString()));
                    grossProfitAmt = grossProfitAmt.add(new BigDecimal(item.get("grossProfitAmt").toString()));
                    String grossProfitRateNum = StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").indexOf("%"));
                    item.put("grossProfitRate",new BigDecimal(grossProfitRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    discountAmt = discountAmt.add(new BigDecimal(item.get("discountAmt").toString()));
                    String discountRateNum = StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").indexOf("%"));
                    item.put("discountRate",new BigDecimal(discountRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    count = count.add(new BigDecimal(item.get("count").toString()));
                }
                listNum.put("gssdnormalAmt",gssdnormalAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("ssAmount",ssAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("allCostAmt",allCostAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("grossProfitAmt",grossProfitAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("discountAmt",discountAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                if(!(ssAmount.compareTo(BigDecimal.ZERO) == 0)) {
                    grossProfitRate = grossProfitAmt.divide(ssAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("grossProfitRate",grossProfitRate);
                if(!(gssdnormalAmt.compareTo(BigDecimal.ZERO) == 0)) {
                    discountRate = discountAmt.divide(gssdnormalAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("discountRate",discountRate);
                listNum.put("count",count);
                result.put("list",list);
                result.put("listNum",listNum);
            }
        }else {
            //按门店
            List<LinkedHashMap<String,Object>> list = this.gaiaSdMemberBasicMapper.selectMemberProSto(inData);
            if(list.size() > 0 && list != null){
                for (Map<String,Object> item : list){
                    gssdnormalAmt = gssdnormalAmt.add(new BigDecimal(item.get("gssdnormalAmt").toString()));
                    ssAmount = ssAmount.add(new BigDecimal(item.get("ssAmount").toString()));
                    allCostAmt = allCostAmt.add(new BigDecimal(item.get("allCostAmt").toString()));
                    grossProfitAmt = grossProfitAmt.add(new BigDecimal(item.get("grossProfitAmt").toString()));
                    String grossProfitRateNum = StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("grossProfitRate"),"0%").indexOf("%"));
                    item.put("grossProfitRate",new BigDecimal(grossProfitRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    discountAmt = discountAmt.add(new BigDecimal(item.get("discountAmt").toString()));
                    String discountRateNum = StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").substring(0,StringUtils.defaultIfEmpty((String) item.get("discountRate"),"0%").indexOf("%"));
                    item.put("discountRate",new BigDecimal(discountRateNum).setScale(2,BigDecimal.ROUND_HALF_UP) + "%");
                    count = count.add(new BigDecimal(item.get("count").toString()));
                }
                listNum.put("gssdnormalAmt",gssdnormalAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("ssAmount",ssAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("allCostAmt",allCostAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("grossProfitAmt",grossProfitAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                listNum.put("discountAmt",discountAmt.setScale(2,BigDecimal.ROUND_HALF_UP));
                if(!(ssAmount.compareTo(BigDecimal.ZERO) == 0)) {
                    grossProfitRate = grossProfitAmt.divide(ssAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("grossProfitRate",grossProfitRate);
                if(!(gssdnormalAmt.compareTo(BigDecimal.ZERO) == 0)) {
                    discountRate = discountAmt.divide(gssdnormalAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                }
                listNum.put("discountRate",discountRate);
                listNum.put("count",count);
                result.put("list",list);
                result.put("listNum",listNum);
            }
        }
        return result;
    }

    //查询条件：门店编码
    @Override
    public List<MemberSalesOutData> findStoreId(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findStoreId(inData);
    }

    //查询条件：销售等级
    public List<MemberSaleClass> findSaleClass(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findSaleClass(inData);
    }

    //查询条件：商品定位
    public List<MemberSalePosition> findSalePosition(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findSalePosition(inData);
    }

    //查询条件：商品自分类
    public List<MemberProClass> findProClass(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findProClass(inData);
    }

    //查询条件：自定义1
    public List<MemberZDY1> findZDY1(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findZDY1(inData);
    }

    //查询条件：自定义2
    public List<MemberZDY2> findZDY2(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findZDY2(inData);
    }

    //查询条件：自定义3
    public List<MemberZDY3> findZDY3(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findZDY3(inData);
    }

    //查询条件：自定义4
    public List<MemberZDY4> findZDY4(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findZDY4(inData);
    }

    //查询条件：自定义5
    public List<MemberZDY5> findZDY5(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.findZDY5(inData);
    }

    //搜索条件
    public JsonResult searchBox(MemberSalesInData inData){
        Map<String, Object> result = new HashMap<>(16);
        result.put("findSaleClass", findSaleClass(inData));
        result.put("findSalePosition", findSalePosition(inData));
        result.put("findProClass", findProClass(inData));
        result.put("findZDY1", findZDY1(inData));
        result.put("findZDY2", findZDY2(inData));
        result.put("findZDY3", findZDY3(inData));
        result.put("findZDY4", findZDY4(inData));
        result.put("findZDY5", findZDY5(inData));
        return JsonResult.success(result, "success");
    }

    //导出
    @Override
    public List<LinkedHashMap<String, Object>> selectMemberListCSV(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.selectMemberList(inData);
    }

    @Override
    public List<LinkedHashMap<String, Object>> selectMemberStoListCSV(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.selectMemberStoreList(inData);
    }

    @Override
    public List<LinkedHashMap<String, Object>> selectMemberProCSV(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.selectMemberProSto(inData);
    }

    @Override
    public List<LinkedHashMap<String, Object>> selectMemberProStoCSV(MemberSalesInData inData){
        return this.gaiaSdMemberBasicMapper.selectMemberProSto(inData);
    }
}
