//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.TempHumiInData;
import com.gys.business.service.data.TempHumiOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface TempHumiService {
    PageInfo<TempHumiOutData> getTempHumiList(TempHumiInData inData);

    void insertHumi(TempHumiInData inData, GetLoginOutData userInfo);

    void aduitTempHumi(TempHumiInData inData, GetLoginOutData userInfo);

    void updateTempHumi(TempHumiInData inData, GetLoginOutData userInfo);

    JsonResult genTempHumi(GetLoginOutData userInfo);
}
