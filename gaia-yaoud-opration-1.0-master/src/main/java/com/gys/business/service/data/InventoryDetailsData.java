package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/3/5
 */
@Data
public class InventoryDetailsData implements Serializable {
    private static final long serialVersionUID = -680578064753902070L;

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "地点")
    private String gspcBrId;

    @ApiModelProperty(value = "店名")
    private String stoName;

    @ApiModelProperty(value = "日期")
    private String gspcDate;

    @ApiModelProperty(value = "盘点单号")
    private String gspcVoucherId;

    @ApiModelProperty(value = "账面数量")
    private String gspcStockQty;

    @ApiModelProperty(value = "初盘数量")
    private String gspcQty;

    @ApiModelProperty(value = "初盘差异")
    private String initialDiscrepancies;

    @ApiModelProperty(value = "初盘人")
    private String novice;

    @ApiModelProperty(value = "复盘数量")
    private String gspcdPcSecondQty;

    @ApiModelProperty(value = "复盘差异")
    private String checkTheDifference;

    @ApiModelProperty(value = "复盘人")
    private String reviewer;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "厂家")
    private String proFactoryName;
}
