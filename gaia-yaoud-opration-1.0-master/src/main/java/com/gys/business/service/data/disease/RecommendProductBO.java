package com.gys.business.service.data.disease;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/7 15:34
 * @Version 1.0.0
 **/
@Data
public class RecommendProductBO {
    //  辅助用药1/预防保健2
    private String auxiliaryMedication;
    //  商品编码
    private String proCode;
    //  商品通用名称
    private String proCommonName;
    //  关联成分编码
    private String componentCode;
    //  规格
    private String specs;
    //  关联说明
    private String associationDescription;
    //  库存数量
    private BigDecimal qty;
    //  零售价
    private BigDecimal priceNormal;
    //  零售价减平均零售额
    private BigDecimal avgNormalPrice;
    //  毛利额
    private BigDecimal grossProfit;
    //  毛利额减平均毛利额
    private BigDecimal avgGrossprofit;
    //  效期
    private String vaildDate;
    //  排序
    private String sort;
    //  传入的商品编码
    private String inProCode;
}
