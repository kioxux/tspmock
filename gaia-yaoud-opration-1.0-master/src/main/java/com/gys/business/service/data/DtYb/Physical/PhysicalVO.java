package com.gys.business.service.data.DtYb.Physical;

import lombok.Data;

import java.util.List;

/**
 * @Description 大同易联众 盘点商品
 * @Author huxinxin
 * @Date 2021/5/11 13:38
 * @Version 1.0.0
 **/
@Data
public class PhysicalVO {
    /*
     * 上传时间
     **/
    private String physicalDate;
    /*
     * 盘点单据号
     **/
    private String physicalCode;
    /*
     * 盘点商品
     **/
    private List<PhysicalProVO> proList;

}
