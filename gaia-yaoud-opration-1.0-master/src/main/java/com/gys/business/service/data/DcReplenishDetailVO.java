package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/24 22:29
 */
@Data
public class DcReplenishDetailVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "仓库编码", example = "102")
    private String proSite;
    @ApiModelProperty(value = "仓库地点", example = "配送中心1")
    private String proSiteName;
    @ApiModelProperty(value = "商品编码", example = "1027989")
    private String proSelfCode;
    @ApiModelProperty(value = "商品名称", example = "阿莫西林胶囊")
    private String proCommonName;
    @ApiModelProperty(value = "商品规格", example = "10*4g")
    private String proSpecs;
    @ApiModelProperty(value = "商品单位", example = "盒")
    private String proUnit;
    @ApiModelProperty(value = "生产厂家", example = "云南药业")
    private String factoryName;
}
