//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaDailyReconcileDetailMapper;
import com.gys.business.mapper.GaiaDailyReconcileMapper;
import com.gys.business.mapper.GaiaDefaultParamMapper;
import com.gys.business.mapper.GaiaParamDetailMapper;
import com.gys.business.mapper.GaiaSdStoreDataMapper;
import com.gys.business.mapper.entity.DefultParamData;
import com.gys.business.mapper.entity.GaiaSdDailyReconcile;
import com.gys.business.mapper.entity.GaiaSdDailyReconcileDetail;
import com.gys.business.mapper.entity.ParamDetailData;
import com.gys.business.service.DailyReconcileService;
import com.gys.business.service.data.*;
import com.gys.common.exception.BusinessException;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

@Service
public class DailyReconcileServiceImpl implements DailyReconcileService {
    @Autowired
    private GaiaDailyReconcileMapper dailyReconcileMapper;
    @Autowired
    private GaiaDailyReconcileDetailMapper dailyReconcileDetailMapper;
    @Autowired
    private GaiaSdStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaDefaultParamMapper defaultParamMapper;
    @Autowired
    private GaiaParamDetailMapper paramDetailMapper;

    public DailyReconcileServiceImpl() {
    }

    public List<DailyReconcileOutData> getDailyReconcile(DailyReconcileInData inData) {
        return this.dailyReconcileMapper.getDailyReconcile(inData);
    }

    public List<DailyReconcileDetailOutData> getDailyReconcileDetailList(DailyReconcileOutData inData) {
        new ArrayList();
        List<DailyReconcileDetailOutData> dailyReconcileDetailOutDataList = this.dailyReconcileDetailMapper.getDailyReconcileDetailList(inData);
        if (dailyReconcileDetailOutDataList.size() > 0) {
            for(int i = 0; i < dailyReconcileDetailOutDataList.size(); ++i) {
                ((DailyReconcileDetailOutData)dailyReconcileDetailOutDataList.get(i)).setIndexDetail(i + 1);
            }
        }

        return dailyReconcileDetailOutDataList;
    }

    @Transactional
    public void insert(DailyReconcileOutData inData) {
        DailyReconcileInData param = new DailyReconcileInData();
        param.setClientId(inData.getClientId());
        param.setGpdhBrId(inData.getGpdhBrId());
        param.setGpdhSaleDate(inData.getGpdhSaleDate());
        param.setGpdhVoucherId(inData.getGpdhVoucherId());
        List<DailyReconcileOutData> dailyReconcileOutDataList = this.dailyReconcileMapper.getDailyReconcile(param);
        if (dailyReconcileOutDataList.size() == 0) {
            GaiaSdDailyReconcile dailyReconcile = new GaiaSdDailyReconcile();
            dailyReconcile.setClientId(inData.getClientId());
            dailyReconcile.setGpdhBrId(inData.getGpdhBrId());
            String voucherId = this.dailyReconcileMapper.selectNextVoucherId(inData.getClientId());
            dailyReconcile.setGpdhVoucherId(voucherId);
            dailyReconcile.setGpdhSaleDate(inData.getGpdhSaleDate());
            dailyReconcile.setGpdhEmp(inData.getGpdhEmp());
            dailyReconcile.setGpdhtotalSalesAmt(inData.getGpdhtotalSalesAmt());
            dailyReconcile.setGpdhtotalInputAmt(inData.getGpdhtotalInputAmt());
//            dailyReconcile.setGpdhStatus("0");
            dailyReconcile.setGpdhCheckDate(DateUtil.format(new Date(), "yyyyMMdd"));
            dailyReconcile.setGpdhStatus("1");
            this.dailyReconcileMapper.insert(dailyReconcile);
            List<DailyReconcileDetailOutData> details = inData.getDetailOutDataList();
            for(int i = 0; i < details.size(); ++i) {
                GaiaSdDailyReconcileDetail detail = new GaiaSdDailyReconcileDetail();
                detail.setGspddSerial(String.valueOf(i + 1));
                detail.setClientId(inData.getClientId());
                detail.setGspddVoucherId(voucherId);
                detail.setGspddBrId(inData.getGpdhBrId());
                if (StrUtil.isNotEmpty(details.get(i).getGspddPayType())) {
                    detail.setGspddPayType(details.get(i).getGspddPayType());
                }else{
                    detail.setGspddPayType("");
                }
                if (!StrUtil.isEmpty((details.get(i)).getGspddSalePaymethodId())) {
                    detail.setGspddSalePaymethodId((details.get(i)).getGspddSalePaymethodId());
                }
                if (!StrUtil.isEmpty((details.get(i)).getGspddSaleReceivableAmt())) {
                    detail.setGspddSaleReceivableAmt((details.get(i)).getGspddSaleReceivableAmt());
                }
                if (!StrUtil.isEmpty((details.get(i)).getGspddSaleInputAmt())) {
                    detail.setGspddSaleInputAmt((details.get(i)).getGspddSaleInputAmt());
                }
                if (!StrUtil.isEmpty((details.get(i)).getGspddRecardPaymethodId())) {
                    detail.setGspddSalePaymethodId((details.get(i)).getGspddRecardPaymethodId());
                }
                if (!StrUtil.isEmpty((details.get(i)).getGspddRecardReceivableAmt())) {
                    detail.setGspddSaleReceivableAmt((details.get(i)).getGspddRecardReceivableAmt());
                }
                if (!StrUtil.isEmpty((details.get(i)).getGspddRecardInputAmt())) {
                    detail.setGspddSaleInputAmt((details.get(i)).getGspddRecardInputAmt());
                }

                this.dailyReconcileDetailMapper.insert(detail);
            }

        } else {
            throw new BusinessException(inData.getGpdhSaleDate() + "已经做过日结对账");
        }
    }

    @Transactional
    public void approve(DailyReconcileOutData inData) {
        Example example = new Example(GaiaSdDailyReconcile.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gpdhVoucherId", inData.getGpdhVoucherId());
        GaiaSdDailyReconcile dailyReconcile = (GaiaSdDailyReconcile)this.dailyReconcileMapper.selectOneByExample(example);
        dailyReconcile.setGpdhCheckDate(DateUtil.format(new Date(), "yyyyMMdd"));
        dailyReconcile.setGpdhStatus("1");
        this.dailyReconcileMapper.updateByPrimaryKeySelective(dailyReconcile);
    }

    public List<SaleReturnOutData> getSaleInfoList(DailyReconcileInData inData) {
        return this.dailyReconcileMapper.getSaleInfoList(inData);
    }

    public List<RechargeCardPaycheckOutData> getRechargeInfoList(DailyReconcileInData inData) {
        return this.dailyReconcileMapper.getRechargeInfoList(inData);
    }

    public List<StoreOutData> getStoreData(StoreInData inData) {
        return this.storeDataMapper.getStoreData(inData);
    }

    @Transactional
    public void checkDailyReconcileInfo(DailyReconcileInData inData) {
        List<DailyReconcileOutData> dailyReconcileOutDataList = this.dailyReconcileMapper.getDailyReconcile(inData);
        Example example = new Example(ParamDetailData.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsspBrId", inData.getGpdhBrId()).andEqualTo("gsspId", "PAY_DAYREPORT_SET");
        ParamDetailData paramDetailData = (ParamDetailData)this.paramDetailMapper.selectOneByExample(example);
        if (ObjectUtil.isNull(paramDetailData)) {
            Example sysExample = new Example(DefultParamData.class);
            sysExample.createCriteria().andEqualTo("gsspId", "PAY_DAYREPORT_SET");
            DefultParamData defultParamData = (DefultParamData)this.defaultParamMapper.selectOneByExample(sysExample);
            if (ObjectUtil.isNotNull(defultParamData) && "0".equals(paramDetailData.getGsspPara()) && dailyReconcileOutDataList.size() == 0) {
                throw new BusinessException(inData.getGpdhSaleDate() + "还未做日结对账，不可销售，请先完成日结对账");
            }
        } else if ("0".equals(paramDetailData.getGsspPara()) && dailyReconcileOutDataList.size() == 0) {
            throw new BusinessException(inData.getGpdhSaleDate() + "还未做日结对账，不可销售，请先完成日结对账");
        }

    }

    public List<DailyReconcileOutData> queryDailyReconcile(DailyReconcileInData inData) {
        List<DailyReconcileOutData> list = dailyReconcileMapper.getDailyReconcile(inData);
        for (DailyReconcileOutData item : list){
            DailyReconcileOutData data = new DailyReconcileOutData();
            data.setClientId(inData.getClientId());
            data.setGpdhVoucherId(item.getGpdhVoucherId());
            List<DailyReconcileDetailOutData> dailyReconcileDetailOutDataList = this.dailyReconcileDetailMapper.getDailyReconcileDetailList(data);
            if (dailyReconcileDetailOutDataList.size() > 0) {
                for(int i = 0; i < dailyReconcileDetailOutDataList.size(); ++i) {
                    ((DailyReconcileDetailOutData)dailyReconcileDetailOutDataList.get(i)).setIndexDetail(i + 1);
                }
            }
            item.setDetailOutDataList(dailyReconcileDetailOutDataList);
        }
        return list;
    }

//    @Override
//    public List<DailyReconcileDetailNewOutData> getSaleAndRechargeInfoList(DailyReconcileInData inData) {
//        return this.dailyReconcileMapper.querySaleAndRechargeInfoList(inData);
//    }

    @Override
    public List<DailyReconcileNewOutData> getDailyInfoList(DailyReconcileInData inData) {
        List<DailyReconcileNewOutData> outData = new ArrayList<>();
        List<DailyReconcileDetailNewOutData> detailOutDataList = this.dailyReconcileMapper.getDailyInfoList(inData);
        BigDecimal saleTotalAmt = BigDecimal.ZERO;
        BigDecimal recardTotalAmt = BigDecimal.ZERO;
        for (DailyReconcileDetailNewOutData detail : detailOutDataList) {
             if (("1").equals(detail.getGsspmType())){
                 saleTotalAmt = saleTotalAmt.add(detail.getGsspmAmt());
             }
             if (("2").equals(detail.getGsspmType())){
                 recardTotalAmt = recardTotalAmt.add(detail.getGsspmAmt());
             }
        }
        List<DailyReconcileDetailNewOutData> list = detailOutDataList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(DailyReconcileDetailNewOutData :: getGsspmType))), ArrayList::new));
        for (DailyReconcileDetailNewOutData detailData : list) {
            DailyReconcileNewOutData data = new DailyReconcileNewOutData();
            data.setClientId(detailData.getClientId());
            data.setGpdhBrId(detailData.getGsspmBrId());
            data.setGpdhSaleDate(detailData.getGsspmDate());
            data.setGpdhType(detailData.getGsspmType());
            if (("1").equals(detailData.getGsspmType())) {
                data.setGpdhtotalSalesAmt(saleTotalAmt.toString());
            }else if(("2").equals(detailData.getGsspmType())){
                data.setGpdhtotalSalesAmt(recardTotalAmt.toString());
            }
            outData.add(data);
        }
        List<DailyReconcileDetailNewOutData> lst = detailOutDataList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(
                () -> new TreeSet<>(Comparator.comparing(o -> o.getGsspmType() + "#" + o.getGsspmId()))),ArrayList::new));
        for (DailyReconcileDetailNewOutData item : lst) {
            BigDecimal itemTotal = BigDecimal.ZERO;
            for (DailyReconcileDetailNewOutData detailNewOutData : detailOutDataList) {
                if (item.getGsspmType().equals(detailNewOutData.getGsspmType())
                        && item.getGsspmId().equals(detailNewOutData.getGsspmId())){
                    itemTotal = itemTotal.add(detailNewOutData.getGsspmAmt());
                }
            }
            item.setGsspmAmt(itemTotal);
        }

        for (DailyReconcileNewOutData data : outData){
            List<DailyReconcileDetailNewOutData> detailList = new ArrayList<>();
            for (DailyReconcileDetailNewOutData detailNewOutData : lst){
                if(data.getGpdhType().equals(detailNewOutData.getGsspmType())){
                    detailList.add(detailNewOutData);
                }
            }
            data.setDetailOutDataList(detailList);
        }
        return outData;
    }
}
