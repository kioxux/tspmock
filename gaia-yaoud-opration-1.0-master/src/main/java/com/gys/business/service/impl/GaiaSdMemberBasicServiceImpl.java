package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdMemberBasicMapper;
import com.gys.business.service.GaiaSdMemberBasicService;
import com.gys.business.service.data.GetQueryMemberOutData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GaiaSdMemberBasicServiceImpl implements GaiaSdMemberBasicService {

    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;


    @Override
    public List<GetQueryMemberOutData> queryMemberByCondition(GetQueryMemberOutData getQueryMemberOutData) {
        return gaiaSdMemberBasicMapper.queryMemberByCondition(getQueryMemberOutData);
    }
}
