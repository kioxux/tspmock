package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AcceptWareHouseProDetailData {
    @ApiModelProperty("箱号")
    private String boxVoucherId;
    @ApiModelProperty("序号")
    private String boxNum;
    @ApiModelProperty("商品编码")
    private String proCode;
    @ApiModelProperty("商品名")
    private String proName;
    @ApiModelProperty("规格")
    private String proSpecs;
    @ApiModelProperty("生产厂家")
    private String factoryName;
    @ApiModelProperty("零售价")
    private String proPrice;
    @ApiModelProperty("配送价")
    private String psPrice;
    @ApiModelProperty("批号")
    private String batchNo;
    @ApiModelProperty("有效期至")
    private String expiryDate;
    @ApiModelProperty("单据数量")
    private String orderQty;
    @ApiModelProperty("收货数量")
    private String acceptQty;
    @ApiModelProperty("拒收数量")
    private String refuseQty;
    @ApiModelProperty("配送单号")
    private String psVoucherId;
    @ApiModelProperty("产地")
    private String proPlace;
    @ApiModelProperty("剂型")
    private String proForm;
    @ApiModelProperty("单位")
    private String proUnit;
    @ApiModelProperty("批准文号")
    private String proRegisterNo;
    @ApiModelProperty("批次")
    private String batBatch;
    @ApiModelProperty(value = "国家贯标编码")
    private String medProductCode;
    /**
     * 拒收原因 拒收数量大于0时为必填
     */
    private String rejecteReason;

}
