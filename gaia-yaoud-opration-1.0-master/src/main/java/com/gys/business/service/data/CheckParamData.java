//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class CheckParamData {
    private String billNo;
    private String clientId;
    private String brId;

    public CheckParamData() {
    }

    public String getBillNo() {
        return this.billNo;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public void setBillNo(final String billNo) {
        this.billNo = billNo;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof CheckParamData)) {
            return false;
        } else {
            CheckParamData other = (CheckParamData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$billNo = this.getBillNo();
                    Object other$billNo = other.getBillNo();
                    if (this$billNo == null) {
                        if (other$billNo == null) {
                            break label47;
                        }
                    } else if (this$billNo.equals(other$billNo)) {
                        break label47;
                    }

                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CheckParamData;
    }

    public int hashCode() {

        int result = 1;
        Object $billNo = this.getBillNo();
        result = result * 59 + ($billNo == null ? 43 : $billNo.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        return result;
    }

    public String toString() {
        return "CheckParamData(billNo=" + this.getBillNo() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ")";
    }
}
