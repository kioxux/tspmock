//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdStoresGroupMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaSdStoresGroup;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.ShopSortService;
import com.gys.business.service.data.GetGaiaDepInData;
import com.gys.business.service.data.GetShopDetailOutData;
import com.gys.business.service.data.GetShopInData;
import com.gys.business.service.data.GetShopSortOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Service
public class ShopSortServiceimpl implements ShopSortService {
    @Resource
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    public ShopSortServiceimpl() {
    }

    public List<GetShopSortOutData> querySort(GetShopInData inData) {
        List<GetShopSortOutData> outData = null;
        outData = this.gaiaSdStoresGroupMapper.querySort(inData);
        if (CollectionUtils.isEmpty((Collection)outData)) {
            outData = new ArrayList();
        } else {
            for(int i = 0; i < ((List)outData).size(); ++i) {
                ((GetShopSortOutData)((List)outData).get(i)).setIndexSort(i + 1);
            }
        }

        return (List)outData;
    }

    public List<GetShopDetailOutData> queryDetail(GetShopInData inData) {
        List<GetShopDetailOutData> outData = null;
        outData = this.gaiaSdStoresGroupMapper.queryDetail(inData);
        if (CollectionUtils.isEmpty((Collection)outData)) {
            outData = new ArrayList();
        } else {
            for(int i = 0; i < ((List)outData).size(); ++i) {
                ((GetShopDetailOutData)((List)outData).get(i)).setIndexDetail(i + 1);
            }
        }

        return (List)outData;
    }

    @Transactional
    public void add(GetShopInData inData) {
        GaiaSdStoresGroup sdStoresGroupTemp = new GaiaSdStoresGroup();
        sdStoresGroupTemp.setClientId(inData.getClientId());
        sdStoresGroupTemp.setGssgId(inData.getGssgId());
        List<GaiaSdStoresGroup> gaiaSdStoresGroupTemp = this.gaiaSdStoresGroupMapper.select(sdStoresGroupTemp);
        if (ObjectUtil.isNotEmpty(gaiaSdStoresGroupTemp)) {
            throw new BusinessException("分类编号已存在");
        } else {
            sdStoresGroupTemp = new GaiaSdStoresGroup();
            sdStoresGroupTemp.setClientId(inData.getClientId());
            sdStoresGroupTemp.setGssgName(inData.getGssgName());
            gaiaSdStoresGroupTemp = this.gaiaSdStoresGroupMapper.select(sdStoresGroupTemp);
            if (ObjectUtil.isNotEmpty(gaiaSdStoresGroupTemp)) {
                throw new BusinessException("分类名称已存在");
            } else {
                inData.getShops().forEach((item) -> {
                    GaiaSdStoresGroup sdStoresGroup = new GaiaSdStoresGroup();
                    sdStoresGroup.setClientId(inData.getClientId());
                    sdStoresGroup.setGssgId(inData.getGssgId());
                    sdStoresGroup.setGssgName(inData.getGssgName());
                    sdStoresGroup.setGssgBrId(item.getShopId());
                    sdStoresGroup.setGssgBrName(item.getShopName());
                    sdStoresGroup.setGssgUpdateEmp(inData.getGssgUpdateEmp());
                    this.gaiaSdStoresGroupMapper.insert(sdStoresGroup);
                });
            }
        }
    }

    @Transactional
    public void editSave(GetShopInData inData) {
        GaiaSdStoresGroup sdStoresGroupTemp = new GaiaSdStoresGroup();
        sdStoresGroupTemp.setClientId(inData.getClientId());
        sdStoresGroupTemp.setGssgId(inData.getGssgId());
        this.gaiaSdStoresGroupMapper.delete(sdStoresGroupTemp);
        inData.getShops().forEach((item) -> {
            GaiaSdStoresGroup sdStoresGroup = new GaiaSdStoresGroup();
            sdStoresGroup.setClientId(inData.getClientId());
            sdStoresGroup.setGssgId(inData.getGssgId());
            sdStoresGroup.setGssgName(inData.getGssgName());
            sdStoresGroup.setGssgBrId(item.getShopId());
            sdStoresGroup.setGssgBrName(item.getShopName());
            sdStoresGroup.setGssgUpdateEmp(inData.getGssgUpdateEmp());
            this.gaiaSdStoresGroupMapper.insert(sdStoresGroup);
        });
    }

    public void addAll(List<GetShopInData> inDataList) {
        List<GaiaSdStoresGroup> dataList = new ArrayList();
        Map<String, List<String>> brIdMap = new HashMap();
        inDataList.forEach((item) -> {
            if (!ObjectUtil.isEmpty(item.getGssgId()) && !ObjectUtil.isEmpty(item.getGssgBrId())) {
                List<String> gssgBrIds = (List)brIdMap.get(item.getGssgId());
                if (ObjectUtil.isNotEmpty(gssgBrIds) && gssgBrIds.indexOf(item.getGssgBrId()) >= 0) {
                    throw new BusinessException("分类编码：" + item.getGssgId() + ",门店编码：" + item.getGssgBrId() + "数据重复");
                } else {
                    if (ObjectUtil.isEmpty(gssgBrIds)) {
                        ArrayList<String> brIds = new ArrayList();
                        brIds.add(item.getGssgBrId());
                        brIdMap.put(item.getGssgId(), brIds);
                    } else {
                        gssgBrIds.add(item.getGssgBrId());
                        brIdMap.put(item.getGssgId(), gssgBrIds);
                    }

                    GaiaStoreData gaiaStoreData = new GaiaStoreData();
                    gaiaStoreData.setClient(item.getClientId());
                    gaiaStoreData.setStoCode(item.getGssgBrId());
                    gaiaStoreData = (GaiaStoreData)this.gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreData);
                    if (ObjectUtil.isEmpty(gaiaStoreData)) {
                        throw new BusinessException("门店编码：" + item.getGssgBrId() + "不存在");
                    } else {
                        GaiaSdStoresGroup gaiaSdStoresGroup = new GaiaSdStoresGroup();
                        gaiaSdStoresGroup.setClientId(item.getClientId());
                        gaiaSdStoresGroup.setGssgId(item.getGssgId());
                        List<GaiaSdStoresGroup> sdStoresGroups = this.gaiaSdStoresGroupMapper.select(gaiaSdStoresGroup);
                        if (ObjectUtil.isEmpty(sdStoresGroups)) {
                            throw new BusinessException("分类编码：" + item.getGssgId() + "不存在");
                        } else {
                            GaiaSdStoresGroup gaiaSdStoresGroupTemp = new GaiaSdStoresGroup();
                            gaiaSdStoresGroupTemp.setClientId(item.getClientId());
                            gaiaSdStoresGroupTemp.setGssgId(item.getGssgId());
                            gaiaSdStoresGroupTemp.setGssgBrId(item.getGssgBrId());
                            gaiaSdStoresGroupTemp = (GaiaSdStoresGroup)this.gaiaSdStoresGroupMapper.selectOne(gaiaSdStoresGroupTemp);
                            if (ObjectUtil.isNotEmpty(gaiaSdStoresGroupTemp)) {
                                throw new BusinessException("门店编码：" + item.getGssgBrId() + "，在分类编码：" + item.getGssgId() + "下已存在");
                            } else {
                                GaiaSdStoresGroup sdStoresGroup = new GaiaSdStoresGroup();
                                sdStoresGroup.setClientId(item.getClientId());
                                sdStoresGroup.setGssgBrId(item.getGssgBrId());
                                sdStoresGroup.setGssgBrName(gaiaStoreData.getStoName());
                                sdStoresGroup.setGssgId(item.getGssgId());
                                sdStoresGroup.setGssgName(((GaiaSdStoresGroup)sdStoresGroups.get(0)).getGssgName());
                                sdStoresGroup.setGssgUpdateEmp(item.getGssgUpdateEmp());
                                dataList.add(sdStoresGroup);
                            }
                        }
                    }
                }
            } else {
                throw new BusinessException("数据不完整");
            }
        });
        this.gaiaSdStoresGroupMapper.addAll(dataList);
    }

    public void delete(List<GetShopInData> inData) {
        inData.forEach((item) -> {
            this.gaiaSdStoresGroupMapper.deleteData(item);
        });
    }

    @Transactional
    public void update(List<GetShopDetailOutData> inData, GetLoginOutData userInfo) {
        for(int i = 0; i < inData.size(); ++i) {
            GetShopDetailOutData shopDetailOutData = (GetShopDetailOutData)inData.get(i);
            GaiaSdStoresGroup storesGroup = new GaiaSdStoresGroup();
            storesGroup.setClientId(shopDetailOutData.getClientId());
            storesGroup.setGssgId(shopDetailOutData.getGssgIdDetail());
            storesGroup.setGssgBrId(shopDetailOutData.getGssgBrIdDetail());
            List<GaiaSdStoresGroup> out = this.gaiaSdStoresGroupMapper.select(storesGroup);
            if (out.size() > 0) {
                throw new BusinessException("已存在店号：" + shopDetailOutData.getGssgBrIdDetail());
            }

            shopDetailOutData.setUpdateUser(userInfo.getUserId());
            this.gaiaSdStoresGroupMapper.updateDetail(shopDetailOutData);
        }

    }

    public List<GetGaiaDepInData> queryStore(GetShopInData inData) {
        GaiaStoreData in = new GaiaStoreData();
        in.setClient(inData.getClientId());
        List<GaiaStoreData> out = this.gaiaStoreDataMapper.select(in);
        List<GetGaiaDepInData> outData = new ArrayList();
        out.forEach((item) -> {
            GetGaiaDepInData tem = new GetGaiaDepInData();
            tem.setStoCode(item.getStoCode());
            tem.setStoName(item.getStoName());
            outData.add(tem);
        });
        return outData;
    }
}
