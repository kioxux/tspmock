package com.gys.business.service.data.mib;

import com.gys.business.mapper.entity.MibDrugDetail;
import com.gys.business.mapper.entity.MibDrugInfo;
import lombok.Data;

import java.util.List;

/**
 * @author wavesen.shen
 */
@Data
public class PayInfoData {
    private MibDrugInfo mibDrugInfo;
    private List<MibDrugDetail> mibDrugDetails;
}
