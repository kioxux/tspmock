package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PushStoreOutOfStockInData implements Serializable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value="加盟商")
    private String client;


}
