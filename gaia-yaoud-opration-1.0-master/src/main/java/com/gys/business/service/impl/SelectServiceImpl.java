package com.gys.business.service.impl;

import com.gys.business.mapper.*;
import com.gys.business.service.SelectService;
import com.gys.business.service.data.SelectData;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.EnumUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SelectServiceImpl implements SelectService {

    @Autowired
    private GaiaCustomerBusinessMapper customerBusinessMapper;
    @Autowired
    private GaiaProductClassMapper productClassMapper;
    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;
    @Autowired
    private GaiaSalesSummaryMapper summaryMapper;
    @Autowired
    private GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;



    @Override
    public List<SelectData> getCustomer(Map<String, String> map) {
        return customerBusinessMapper.getCustomer(map);
    }
    @Override
    public List<SelectData> getProClass() {
        return productClassMapper.getProClass();
    }
    @Override
    public List<SelectData> getProBigClass() {
        return productClassMapper.getProBigClass();
    }
    @Override
    public List<SelectData> getProMidClass() {
        return productClassMapper.getProMidClass();
    }
    @Override
    public   List<Map<String,Object>> selectProBySaleD(Map<String, Object> inData) {
        return summaryMapper.selectProBySaleD(inData);
    }

    @Override
    public List<Map<String, Object>> selectMemberCard(Map<String, Object> inData) {
        return gaiaSdMemberBasicMapper.selecthykList(inData);

    }

    @Override
    public  List<Map> getMemberByClientOrBrId(String client, String brId) {
        return gaiaSdMemberBasicMapper.getMemberByClientOrBrId(client,brId);

    }

    @Override
    public Map selectStaff(String client, String brId) {
        Map outMap = new HashMap();

        //收银员
        List<Map<String,Object>> cashier =  gaiaAuthconfiDataMapper.selectStaff(client,brId, EnumUtil.StaffEnum.getValue("cashier"));
        outMap.put("cashier",cashier);

        //营业员
        List<Map<String,Object>> assistant =  gaiaAuthconfiDataMapper.selectStaff(client,brId, EnumUtil.StaffEnum.getValue("assistant"));
        outMap.put("assistant",assistant);

        //医生
        List<Map<String,Object>> doctor =  gaiaAuthconfiDataMapper.selectStaff(client,brId, EnumUtil.StaffEnum.getValue("doctor"));
        outMap.put("doctor",doctor);
        return outMap;
    }

    @Override
    public  List<Map<String,Object>> selectCashier(String client, String brId) {
        List<Map<String,Object>> cashier =  gaiaAuthconfiDataMapper.selectStaff(client,brId, EnumUtil.StaffEnum.getValue("cashier"));
        return cashier;
    }

    @Override
    public List<Map<String,Object>> selectAssistant(String client, String brId) {
        List<Map<String,Object>> assistant =  gaiaAuthconfiDataMapper.selectStaff(client,brId, EnumUtil.StaffEnum.getValue("assistant"));
        return assistant;
    }

    @Override
    public List<Map<String,Object>> selectDoctor(String client, String brId) {
        List<Map<String,Object>> doctor =  gaiaAuthconfiDataMapper.selectStaff(client,brId, EnumUtil.StaffEnum.getValue("doctor"));
        return doctor;
    }

    @Override
    public List<Map<String, Object>> listCustomer(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        return customerBusinessMapper.listCustomer(inData);
    }

    @Override
    public List<Map<String, Object>> listDc(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        return customerBusinessMapper.listDc(inData);
    }


}
