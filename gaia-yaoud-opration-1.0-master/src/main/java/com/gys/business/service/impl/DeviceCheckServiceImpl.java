//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdDeviceCheckMapper;
import com.gys.business.mapper.entity.GaiaSdDeviceCheck;
import com.gys.business.service.DeviceCheckService;
import com.gys.business.service.data.GetDeviceCheckInData;
import com.gys.business.service.data.GetDeviceCheckOutData;
import com.gys.business.service.data.device.MaintenanceForm;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

@Service
public class DeviceCheckServiceImpl implements DeviceCheckService {
    @Autowired
    private GaiaSdDeviceCheckMapper deviceCheckMapper;

    public DeviceCheckServiceImpl() {
    }

    public PageInfo<GetDeviceCheckOutData> selectList(GetDeviceCheckInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetDeviceCheckOutData> outData = this.deviceCheckMapper.selectList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Transactional
    public void save(GetDeviceCheckInData inData) {

        GaiaSdDeviceCheck deviceCheck = new GaiaSdDeviceCheck();
        deviceCheck.setClientId(inData.getClientId());
        String codePre = DateUtil.format(new Date(), "yyyy");
        String voucherId = this.deviceCheckMapper.selectNextVoucherId(inData.getClientId(), codePre);
        deviceCheck.setGsdcVoucherId(voucherId);
        deviceCheck.setGsdcBrId(inData.getGsdcBrId());
        deviceCheck.setGsdcBrName(inData.getGsdcBrName());
        deviceCheck.setGsdcDeviceId(inData.getGsdcDeviceId());
        deviceCheck.setGsdcDeviceName(inData.getGsdcDeviceName());
        deviceCheck.setGsdcDeviceModel(inData.getGsdcDeviceModel());
        deviceCheck.setGsdcCheckRemarks(inData.getGsdcCheckRemarks());
        deviceCheck.setGsdcCheckResult(inData.getGsdcCheckResult());
        deviceCheck.setGsdcCheckStep(inData.getGsdcCheckStep());
        deviceCheck.setGsdcUpdateEmp(inData.getGsdcUpdateEmp());
        deviceCheck.setGsdcUpdateDate(inData.getGsdcUpdateDate());
        deviceCheck.setGsdcUpdateTime(inData.getGsdcUpdateTime());
        if (StringUtils.isEmpty(inData.getGsdcStatus())) {
            deviceCheck.setGsdcStatus("0");
        } else {
            deviceCheck.setGsdcStatus(inData.getGsdcStatus());
        }
        this.deviceCheckMapper.insert(deviceCheck);
    }

    public void update(GetDeviceCheckInData inData) {
        GaiaSdDeviceCheck checkRecord = new GaiaSdDeviceCheck();
        checkRecord.setClientId(inData.getClientId());
        checkRecord.setGsdcVoucherId(inData.getGsdcVoucherId());
        GaiaSdDeviceCheck check = (GaiaSdDeviceCheck)this.deviceCheckMapper.selectByPrimaryKey(checkRecord);
        if (ObjectUtil.isNull(check)) {
            throw new BusinessException("提示：设备维护记录不存在！");
        } else if ("1".equals(check.getGsdcStatus())) {
            throw new BusinessException("提示：设备维护记录已审核，不可修改！");
        } else {
            check.setGsdcDeviceId(inData.getGsdcDeviceId());
            check.setGsdcDeviceName(inData.getGsdcDeviceName());
            check.setGsdcDeviceModel(inData.getGsdcDeviceModel());
            check.setGsdcCheckRemarks(inData.getGsdcCheckRemarks());
            check.setGsdcCheckResult(inData.getGsdcCheckResult());
            check.setGsdcCheckStep(inData.getGsdcCheckStep());
            check.setGsdcUpdateEmp(inData.getGsdcUpdateEmp());
            this.deviceCheckMapper.updateByPrimaryKey(check);
        }
    }

    @Transactional
    public void approve(GetDeviceCheckInData inData) {
        if (!CollUtil.isEmpty(inData.getGsdcVoucherIds())) {
            Example example = new Example(GaiaSdDeviceCheck.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andIn("gsdcVoucherId", inData.getGsdcVoucherIds());
            List<GaiaSdDeviceCheck> deviceCheckList = this.deviceCheckMapper.selectByExample(example);
            Iterator var4 = deviceCheckList.iterator();

            while(var4.hasNext()) {
                GaiaSdDeviceCheck deviceCheck = (GaiaSdDeviceCheck)var4.next();
                if ("1".equals(deviceCheck.getGsdcStatus())) {
                    throw new BusinessException(StrUtil.format("提示：({})设备维护记录已审核！", new Object[]{deviceCheck.getGsdcVoucherId()}));
                }

                Date date = new Date();
                deviceCheck.setGsdcUpdateDate(DateUtil.format(date, "yyyyMMdd"));
                deviceCheck.setGsdcUpdateTime(DateUtil.format(date, "HHmmss"));
                deviceCheck.setGsdcStatus("1");
                this.deviceCheckMapper.updateByPrimaryKey(deviceCheck);
            }

        }
    }

    @Override
    public List<GetDeviceCheckOutData> findList(MaintenanceForm maintenanceForm) {
        return deviceCheckMapper.findList(maintenanceForm);
    }

}
