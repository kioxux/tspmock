package com.gys.business.service.data.Unqualified;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UnqualifiedSum {

    @ApiModelProperty(value = "商品编码")
    private BigDecimal proId;

    @ApiModelProperty(value = "不合格品数量")
    private BigDecimal qty;


}
