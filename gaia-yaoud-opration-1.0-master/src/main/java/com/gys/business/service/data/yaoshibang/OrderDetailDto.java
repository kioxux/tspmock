package com.gys.business.service.data.yaoshibang;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/6/17 22:25
 */
@Data
public class OrderDetailDto {

    @ApiModelProperty("项次")
    private Integer index;
    @ApiModelProperty("药师帮商品编码")
    private String soDrugId;
    @ApiModelProperty("商品编码")
    private String soDrugCode;
    @ApiModelProperty("商品名称")
    private String drugName;
    @ApiModelProperty("规格")
    private String drugSpecs;
    @ApiModelProperty("单位")
    private String drugUnit;
    @ApiModelProperty("下单数")
    private String soNewQty;
    @ApiModelProperty("单价")
    private String soPrice;
    @ApiModelProperty("总金额")
    private String soNewAmount;
    @ApiModelProperty("库存")
    private String qty;
    @ApiModelProperty("订单活动类型")
    private String soDelivertype;
}
