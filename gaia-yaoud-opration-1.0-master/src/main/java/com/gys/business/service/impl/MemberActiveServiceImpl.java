package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaSdMemberGradeMapper;
import com.gys.business.mapper.entity.GaiaSdMemberGrade;
import com.gys.business.service.IMemberActiveService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：gyx
 * @Date ：Created in 13:58 2021/11/19
 * @Description：会员活跃度service
 * @Modified By：gyx
 * @Version:
 */
@Service
@Slf4j
public class MemberActiveServiceImpl implements IMemberActiveService {

    @Autowired
    private GaiaSdMemberGradeMapper gaiaSdMemberGradeMapper;
    @Autowired
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Override
    public void dailyMaintenMemberActive(String param) {
        if (StringUtils.isNotEmpty(param)) {
            insertToMemberGrade(param, 1);
        } else {
            //1.所有加盟商
            List<String> clientId = gaiaFranchiseeMapper.getAllClientId();
            for (String client : clientId) {
                insertToMemberGrade(client,1);
            }
        }
    }

    private void insertToMemberGrade(String client,Integer pageNum) {
        log.info("当前加盟商:"+client);
        //分页查
        log.info("设置分页参数"+pageNum);
        PageHelper.startPage(pageNum,50000);
        List<GaiaSdMemberGrade> memberGradeDataList=new ArrayList<>();
        memberGradeDataList=gaiaSdMemberGradeMapper.selectMemberActiveDataFromSale(client);
        PageInfo<GaiaSdMemberGrade> pageInfo = new PageInfo<>(memberGradeDataList);
        if (CollUtil.isNotEmpty(memberGradeDataList)) {
            gaiaSdMemberGradeMapper.batchInsertMemberGradeData(memberGradeDataList);
        }
        log.info("判断是否还有下一页");
        while (pageInfo.isHasNextPage()) {
            pageNum++;
            PageHelper.startPage(pageNum,50000);
            memberGradeDataList=new ArrayList<>();
            memberGradeDataList=gaiaSdMemberGradeMapper.selectMemberActiveDataFromSale(client);
            pageInfo = new PageInfo<>(memberGradeDataList);
            if (CollUtil.isNotEmpty(memberGradeDataList)) {
                gaiaSdMemberGradeMapper.batchInsertMemberGradeData(memberGradeDataList);
            }
        }
        pageInfo=new PageInfo<>();
        memberGradeDataList=new ArrayList<>();
        System.gc();
    }
}
