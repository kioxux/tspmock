package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
    * 字典明细表
    */
@ApiModel(value="com-gys-business-service-data-GaiaDictionary")
@Data
public class GaiaDictionary {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
     * 类型Code拼接字符串
     */
    @ApiModelProperty(value="类型Code拼接字符串")
    private String typeCodeStr;

    /**
    * 字典类型
    */
    @ApiModelProperty(value="字典类型")
    private String type;

    /**
    * 字典code
    */
    @ApiModelProperty(value="字典code")
    private String code;

    /**
    * 字典name
    */
    @ApiModelProperty(value="字典name")
    private String name;

    /**
    * 备注
    */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
    * 排序
    */
    @ApiModelProperty(value="排序")
    private Integer sort;

    /**
    * 状态（1：启用  0：停用）
    */
    @ApiModelProperty(value="状态（1：启用  0：停用）")
    private Integer status;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String createUser;

    /**
    * 创建时间
    */
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
    * 修改人
    */
    @ApiModelProperty(value="修改人")
    private String updateUser;

    /**
    * 修改日期
    */
    @ApiModelProperty(value="修改日期")
    private Date updateTime;

    /**
    * 是否删除 0为有效  1为删除
    */
    @ApiModelProperty(value="是否删除 0为有效  1为删除")
    private Integer isDelete;


}