package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RatioProDetail {
    @ApiModelProperty("加盟商")
    private String clientId;
    @ApiModelProperty("商品编码")
    private String proId;
    @ApiModelProperty("补货门店")
    private String brId;
    @ApiModelProperty("比价单号")
    private String pgId;
    @ApiModelProperty("比价单行号")
    private String pgItem;
    @ApiModelProperty("比价日期")
    private String repDate;
    @ApiModelProperty("补货量")
    private String needQty;
    @ApiModelProperty("创建日期")
    private String createDate;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("创建人")
    private String createUser;
    @ApiModelProperty("状态：0:需要抓取，1：抓取中，2;抓取完成")
    private String repStatus;
    @ApiModelProperty("类型：0:门店，1：连锁公司")
    private String repType;
    @ApiModelProperty("是否手工添加 1 是")
    private String gsrdFlag;
}
