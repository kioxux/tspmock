package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdPromGroup;

import java.util.List;

/**
 * @author csm
 * @date 2021/12/15 - 13:34
 */
public interface GaiaSdPromGroupService {
    /**
     * 查询所有促销商品分组
     * @return
     */
    List<GaiaSdPromGroup> selectAll(String client);
}
