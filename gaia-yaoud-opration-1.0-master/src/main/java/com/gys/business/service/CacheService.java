package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaComputerInformation;
import com.gys.business.mapper.entity.GaiaDataSynchronized;
import com.gys.business.mapper.entity.GaiaSdRightingRecord;
import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.service.data.HealthCareQueryDto;
import com.gys.business.service.data.PartialSynDto;
import com.gys.business.service.data.PartialSynVO;
import com.gys.business.service.data.StartDayAndEndDayInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface CacheService {

    /**
     * 获取所有缓存 文件传输方式
     * @param userInfo
     * @return
     */
    void getAllCache(GetLoginOutData userInfo, boolean flag, HttpServletResponse response);


    /**
     * 新增一条
     * @param aSynchronized
     */
    void addOne(GaiaDataSynchronized aSynchronized);


    /**
     * 所有更新内容
     * @return
     * @param aSyn
     */
    Map<String, Object> updateContentAll(GaiaDataSynchronized aSyn);

    /**
     * 批量新增
     * @param synList
     */
    void insertLists(List<GaiaDataSynchronized> synList);

    /**
     * 部分更新
     * @param userInfo
     * @return
     */
    Map<String, String> partialUpdate(GetLoginOutData userInfo);


    /**
     * 数据审计
     * @param audit
     */
    void dataAuditSave(Map<String,String> audit);

    /**
     * 数据审计
     * @param inData
     * @return
     */
    JsonResult selectAuditByDay(StartDayAndEndDayInData inData);

    /**
     * 获取当前门店的最大更新时间
     * @return
     */
    String getAuditMaxDate(GetLoginOutData userInfo, Map<String, String> map);


    /**
     * 查询当前会员 电子券 和 储值卡金额
     * @param map
     * @return
     */
    Map<String, String>  otherInformation(Map<String, String> map);

    /**
     * 全量同步
     * @param synVO
     */
    void realTimeFullAmount(GetLoginOutData userInfo,PartialSynVO synVO);

    /**
     * 获取同步表数据
     * @param userInfo
     * @param map
     * @param response
     */
    void synchronousData(GetLoginOutData userInfo, Map<String, String> map, HttpServletResponse response);

    /**
     * 获取需要同步的所有表名
     * @return
     */
    PartialSynDto getAllTableNames();

    /**
     * 根据加盟商拿到所有门店号
     * @param map
     * @return
     */
    List<PartialSynDto> getAllBrIds(Map<String, Object> map);

    /**
     * 获取当前最大更新数据
     * @param aSynchronized
     * @return
     */
    List<GaiaDataSynchronized> getTheDataYouNeed(GaiaDataSynchronized aSynchronized);

    /**
     * 提交 电脑信息
     * @param information
     */
    void insertInformation(GaiaComputerInformation information);


    /**
     * 获取门店的参数配置
     */
    List<GaiaSdSystemPara> getSystemParaByStore(Map<String, Object> map);

    /**
     * 医保 信息明细查询
     * @param map
     * @return
     */
    PageInfo<List<HealthCareQueryDto>> healthCareQuery(Map<String, Object> map);

    /**
     * 新增冲正记录
     * @param inData
     * @return
     */
    JsonResult insertRightingRecord(GaiaSdRightingRecord inData);
}
