package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回实体
 *
 * @author xiaoyuan on 2020/9/8
 */
@Data
public class InventoryReportOutData implements Serializable {
    private static final long serialVersionUID = -7161245081492337146L;

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "门店编码")
    private String storeCode;

    @ApiModelProperty(value = "门店名")
    private String storeName;

    @ApiModelProperty(value = "差异单号")
    private String gspcdVoucherId;

    @ApiModelProperty(value = "盘点单号")
    private String gspcVoucherId;

    @ApiModelProperty(value = "行序号")
    private String gspcRowNo;

    @ApiModelProperty(value = "复盘日期")
    private String gspcDate;

    @ApiModelProperty(value = "复盘时间")
    private String gspcTime;

    @ApiModelProperty(value = "盘点类型")
    private String gspcType;

    @ApiModelProperty(value = "编码")
    private String gspcProId;

    @ApiModelProperty(value = "库存数量")
    private String stockQty;

    @ApiModelProperty(value = "初盘修改(复盘数量)")
    private String gspcQty;

    @ApiModelProperty(value = "初盘差异")
    private String gspcdDiffFirstQty;

    @ApiModelProperty(value = "复盘差异")
    private String gspcdDiffSecondQty;

    @ApiModelProperty(value = "批号")
    private String batchNo;

    @ApiModelProperty(value = "状态 0-未审核 1-已审核")
    private String gspcStatus;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    @ApiModelProperty(value = "企业名称(厂家)")
    private String proFactoryName;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "商品自编码")
    private String proSelfCode;

    @ApiModelProperty(value = "国际条形码1")
    private String proBarCode;

    @ApiModelProperty(value = "国际条形码2")
    private String proBarCode2;

    @ApiModelProperty(value = "助记码")
    private String proPym;

    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;

    @ApiModelProperty(value = "剂型")
    private String proForm;

    @ApiModelProperty(value = "初盘日期")
    private String gspcdDate;

    @ApiModelProperty(value = "初盘时间")
    private String gspcdTime;

    @ApiModelProperty(value = "初盘人")
    private String gspcdEmp;

    @ApiModelProperty(value = "复盘人")
    private String gspcEmp;
}
