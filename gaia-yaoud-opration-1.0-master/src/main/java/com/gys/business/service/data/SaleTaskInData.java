package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
@Data
@ApiModel
public class SaleTaskInData {
    private String client;
    @ApiModelProperty(value = "计划月份")
    @NotNull
    private String monthPlan;
    @ApiModelProperty(value = "计划天数")
    @NotNull
    private Integer daysPlan;
//    List<GaiaSaletaskDplan> saletaskDplan;
    List<Saletask> saletasks;
}
