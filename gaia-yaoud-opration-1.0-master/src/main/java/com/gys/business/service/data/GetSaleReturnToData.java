package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan
 */
@Data
public class GetSaleReturnToData implements Serializable {
    private static final long serialVersionUID = -5482836884631477538L;

    private Integer index;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 店号
     */
    private String gsshBrId;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 收银工号
     */
    private String gsshEmp;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 厂家
     */
    private String proFactoryName;


    /**
     * 单位
     */
    private String proUnit;

    /**
     * 规格
     */
    private String proSpecs;


    /**
     * 行号
     */
    private String gssdSerial;

    /**
     * 商品编码
     */
    private String gssdProId;

    /**
     * 商品批号
     */
    private String gssdBatchNo;

    /**
     * 商品有效期
     */
    private String gssdValidDate;

    /**
     * 单品零售价
     */
    private String gssdPrc1;

    /**
     * 单品应收价
     */
    private String gssdPrc2;

    /**
     * 数量
     */
    private String gssdQty;

    /**
     * 汇总应收金额
     */
    private String gssdAmt;

    /**
     * 商品折扣总金额
     */
    private String gssdZkAmt;

    /**
     * 营业员编号
     */
    private String gssdSalerId;

    /**
     * 医生编号
     */
    private String gssdDoctorId;


    /**
     * 移动平均单价
     */
    private String gssdMovPrice;

    /**
     * 移动平均总价
     */
    private String gssdMovPrices;

    /**
     * 税率
     */
    private String gssdMovTax;

    /**
     * 是否生成凭证 1-是 0-否
     */
    private String gssdAdFlag;

    /**
     * 初始值数量
     */
    private String gssdOriQty;

    /**
     * 贴数
     */
    private String gssdDose;

    /**
     * 移动税额
     */
    private String gssdTaxRate;

    /**
     * 退货数量
     */
    private String gssdReturnQty;

    /**
     * 抵用金额
     */
    private String gsshDyqAmt;

    /**
     * 促销单号
     */
    private String gssdPmSubjectId;

    /**
     * 促销类型编码
     */
    private String gssdPmId;

    /**
     * 抵现金额
     */
    private String gsshIntegralCashAmt;

    /**
     * 电子券金额
     */
    private String gsecAmt;

    /**
     * 营业员
     */
    private String salesDetailPerson;

    /**
     * 医生
     */
    private String salesDetailDoctor;

    /**
     * 退货原单号
     */
    private String gsshBillNoReturn;

    /**
     * 3 表示中药
     */
    private String proStorageArea;

    /**
     *  价格状态
     */
    private String gssdProStatus;


    /**
     *
     */
    private String proMedProductcode;

    /**
     * 移动收银订单/外卖订单
     */
    private String registerVoucherId;

    /**
     * 是否医保
     */
    private String proIfMed;
    /**
     * 用法用量
     */
    private String proUsage;

    /**
     * 订单类型， 0-本地pos销售单；1-中药代煎单；2-HIS门诊单
     */
    private String gsshOrderSource;


    /**
     * 是否属于虚拟商品 /0:否/1:是
     */
    private String gssdIfXnsp;

    /**
     * 参考价
     */
    private String gssdIfCkj;
    /**
     * 明细积分
     */
    private BigDecimal integral;
    /**
     * 处方类别
     */
    private String proPresclass;
    /**
     * 当前商品大类编码 3开头 为中药饮片
     */
    private String proClass;
    /**
     * 商品分类编码
     */
    private String proBigClassCode;
}
