package com.gys.business.service.data;

import lombok.Data;

import java.util.Date;

/**
 * @desc: 年度计划查询入参
 * @author: ZhangChi
 * @createTime: 2021/12/31 15:42
 */
@Data
public class AnnualTrainingPlanInData {
    private String client;
    /**
     * 年份
     */
    private String gatpYear;

    /**
     * 计划编号
     */
    private String gatpPlanId;

    /**
     * 培训目标
     */
    private String gatpGoals;

    /**
     * 培训开始时间
     */
    private String gatpTrainingStartDate;
    private Date startDate;

    /**
     * 培训结束时间
     */
    private String gatpTrainingEndDate;
    private Date endDate;

    /**
     * 培训内容
     */
    private String gatpContent;

    /**
     * 授课人
     */
    private String gatpLecturer;

    /**
     * 创建人（制订人）
     */
    private String gatpMaker;
}
