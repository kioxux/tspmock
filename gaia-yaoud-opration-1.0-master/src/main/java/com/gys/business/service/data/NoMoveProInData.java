package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class NoMoveProInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "排序标志 1-升序 2-降序 传空默认为降序")
    private String orderByFlag;

    @ApiModelProperty(value = "不动销商品报表 1-库存品项 2-库存成本额 3-库存占比 默认2")
    private String orderByCondition;

    @ApiModelProperty(value = "排序前后 0-全部 1-前(从大到小) 2-后(从小到大) 默认0")
    private String topOrLast;

    @ApiModelProperty(value = "查询条数")
    private Integer pageSize;

    @ApiModelProperty(value = "不动销天数 默认45天")
    private Integer noMoveDays;

    @ApiModelProperty(value = "直营管理门店列表")
    List<String> directStoreList;
}
