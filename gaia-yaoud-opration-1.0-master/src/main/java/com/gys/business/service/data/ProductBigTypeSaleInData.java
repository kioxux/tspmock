package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductBigTypeSaleInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "周或月 1-月 2-周 传空默认为月")
    private String weekOrMonth;

    @ApiModelProperty(value = "商品大类编码 1-处方药 2-OTC 3-中药 888-药品小计 999-非药品 传空默认为药品小计")
    private String bigCode;
}
