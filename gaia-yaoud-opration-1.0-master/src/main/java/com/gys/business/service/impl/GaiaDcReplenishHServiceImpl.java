package com.gys.business.service.impl;

import com.gys.business.controller.app.form.PushForm;
import com.gys.business.mapper.GaiaDcReplenishDMapper;
import com.gys.business.mapper.GaiaDcReplenishHMapper;
import com.gys.business.mapper.GaiaWmKuCunMapper;
import com.gys.business.mapper.entity.GaiaDcReplenishD;
import com.gys.business.mapper.entity.GaiaDcReplenishH;
import com.gys.business.service.GaiaDcReplenishHService;
import com.gys.business.service.data.DcReplenishDetailVO;
import com.gys.business.service.data.DcReplenishForm;
import com.gys.business.service.data.DcReplenishVO;
import com.gys.common.exception.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 17:12
 */
@Service("gaiaDcReplenishHService")
public class GaiaDcReplenishHServiceImpl implements GaiaDcReplenishHService {
    @Resource
    private GaiaDcReplenishHMapper gaiaDcReplenishHMapper;
    @Resource
    private GaiaDcReplenishDMapper gaiaDcReplenishDMapper;
    @Resource
    private GaiaWmKuCunMapper gaiaWmKuCunMapper;

    @Transactional
    @Override
    public void saveDcReplenish(PushForm pushForm) {
        List<String> list = pushForm.getProSelfCodeList();
        //获取仓库列表
        List<String> dcCodeList = gaiaWmKuCunMapper.getDcCodeList(pushForm.getClient());
        if (dcCodeList == null || dcCodeList.size() == 0) {
            throw new BusinessException("未获取到仓库信息");
        }
        try {
            String dcCode = dcCodeList.get(0);
            //仓库紧急补货主表
            GaiaDcReplenishH gaiaDcReplenishH = transformDcReplenishH(pushForm, dcCode);
            gaiaDcReplenishHMapper.add(gaiaDcReplenishH);
            //仓库紧急补货明细表
            for (String proSelfCode : list) {
                GaiaDcReplenishD gaiaDcReplenishD = new GaiaDcReplenishD();
                gaiaDcReplenishD.setClient(gaiaDcReplenishH.getClient());
                gaiaDcReplenishD.setVoucherId(gaiaDcReplenishH.getVoucherId());
                gaiaDcReplenishD.setProSite(gaiaDcReplenishH.getProSite());
                gaiaDcReplenishD.setProSelfCode(proSelfCode);
                gaiaDcReplenishDMapper.add(gaiaDcReplenishD);
            }
        } catch (Exception e) {
            throw new BusinessException("保存仓库紧急补货需求异常", e);
        }
    }

    @Override
    public List<DcReplenishVO> replenishList(DcReplenishForm replenishForm) {
        return gaiaDcReplenishHMapper.replenishList(replenishForm);
    }

    @Override
    public GaiaDcReplenishH getUnique(GaiaDcReplenishH cond) {
        return gaiaDcReplenishHMapper.getUnique(cond);
    }

    @Override
    public List<DcReplenishDetailVO> getReplenishDetail(DcReplenishForm replenishForm) {
        return gaiaDcReplenishDMapper.getReplenishDetail(replenishForm);
    }

    private GaiaDcReplenishH transformDcReplenishH(PushForm pushForm, String dcCode) {
        GaiaDcReplenishH gaiaDcReplenishH = new GaiaDcReplenishH();
        gaiaDcReplenishH.setClient(pushForm.getClient());
        gaiaDcReplenishH.setCreateUser(pushForm.getUserId());
        gaiaDcReplenishH.setProSite(dcCode);
        gaiaDcReplenishH.setVoucherId(gaiaDcReplenishHMapper.getNextVoucherId(pushForm.getClient()));
        return gaiaDcReplenishH;
    }
}
