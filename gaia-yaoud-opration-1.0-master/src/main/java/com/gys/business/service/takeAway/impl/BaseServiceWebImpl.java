package com.gys.business.service.takeAway.impl;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.takeAway.BaseMapper;
import com.gys.business.mapper.takeAway.BaseMapperPTS;
import com.gys.business.mapper.takeAway.BaseMapperWeb;
import com.gys.business.service.takeAway.BaseServiceWeb;
import com.qcloud.cos.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.gys.util.takeAway.WebConofig.*;

@Service("BaseServiceWeb")
@Transactional("transactionManager_web")
public class BaseServiceWebImpl implements BaseServiceWeb {
    private static final Logger logger = LoggerFactory.getLogger(BaseServiceWebImpl.class);
    private BaseMapperWeb baseMapperWeb;
    private BaseMapperPTS baseMapperPTS;
    private BaseMapper baseMapper;
    public BaseMapperWeb getBaseMapperWeb() {
        return baseMapperWeb;
    }
    @Autowired
    public void setBaseMapperWeb(BaseMapperWeb baseMapperWeb) {
        this.baseMapperWeb = baseMapperWeb;
    }
    public BaseMapperPTS getBaseMapperPTS() {
        return baseMapperPTS;
    }
    @Autowired
    public void setBaseMapperPTS(BaseMapperPTS baseMapperPTS) {
        this.baseMapperPTS = baseMapperPTS;
    }
    public BaseMapper getBaseMapper() {
        return baseMapper;
    }
    @Autowired
    public void setBaserMapper(BaseMapper baseMapper) {
        this.baseMapper = baseMapper;
    }

    /**
     * 获取订单开关信息。支持分页查询
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getSwitchInfo(Map<String, Object> param){
        Map<String, Object> retMap = new HashMap<>();
        try {
            //解析shopArray
            String shopArray="";
            List<Map<String,Object>> shopList=(List)param.get("shopArray");
            for (Map m : shopList){
                shopArray+="SELECT '"+nullToEmpty(m.get("DEPTCODE"))+"' shop_no UNION ";
            }
            //去掉最后一个UNION
            shopArray=shopArray.substring(0,shopArray.lastIndexOf("UNION")-1);
            String shop_no = nullToEmpty(param.get("shop_no"));
            String shop_name = nullToEmpty(param.get("shop_name"));
            String platforms = nullToEmpty(param.get("platforms"));
            List<String> paramList = new ArrayList<>();
            paramList.add(shop_no);
            if(!StringUtils.isNullOrEmpty(shop_name)){
                paramList.add("%"+shop_name+"%");
            }else {
                paramList.add("");
            }
            paramList.add(platforms);
            paramList.add("");
            paramList.add("");
            paramList.add("");
            paramList.add("");
            Map<String, Object> paramTemp=checkNullOrEmpty(paramList);
            //查询字段
            paramTemp.put("fields", "t1.*, t2.shop_name");
            //表名
            paramTemp.put("tables", "t_switch_info t1\n" +
                    "INNER JOIN t_shop_info t2 ON t2.shop_no = t1.shop_no\n"
                    +"INNER JOIN (" + shopArray +")t3 ON t3.shop_no = t1.shop_no");
            //条件
            String where="1=1";
            if(!StringUtils.isNullOrEmpty(shop_no)){
                where+=" AND t1.shop_no=?";
            }
            if(!StringUtils.isNullOrEmpty(shop_name)){
                where+=" AND t2.shop_name LIKE ?";
            }
            if(!StringUtils.isNullOrEmpty(platforms)){
                where+=" AND t1.platforms=?";
//                paramTemp.put("param2", platforms);
            }
            paramTemp.put("where", where);
            //排序字段
            paramTemp.put("orderbyFields", nullToEmpty(param.get("sortField")));
            //排序规则
            paramTemp.put("orderby", nullToEmpty(param.get("sortOrder")));
            //页码
            paramTemp.put("pageindex", Integer.valueOf(nullToEmpty(param.get("pageIndex"))));
            //每页记录数
            paramTemp.put("pagesize", Integer.valueOf(nullToEmpty(param.get("pageSize"))));
//            //输出总记录数
//            paramTemp.put("totalcount", "0");
//            //输出总页数
//            paramTemp.put("pagecount", "0");
            List<Map<String,Object>> retTemp = baseMapperWeb.getInfoByProc(paramTemp);
            Map<String, Object> retTempMap = new HashMap<>();
            retTempMap.put("totalCount",nullToEmpty(paramTemp.get("totalcount")));
            retTempMap.put("totalPage",nullToEmpty(paramTemp.get("pagecount")));
            retTempMap.put("pageIndex",nullToEmpty(param.get("pageIndex")));
            retTempMap.put("pageSize",nullToEmpty(param.get("pageSize")));
            retTempMap.put("rows",retTemp);
            retMap.put("data",retTempMap);
            retMap.put(retCode,successCode);
            retMap.put(retDesc,successDesc);
        }catch (Exception e){
            retMap.put(retCode,otherFailCode);
            otherFailDesc = e.getMessage();
            retMap.put(retDesc,otherFailDesc);
        }
        return retMap;
    }

    /**
     * 设置订单开关信息
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> setSwitchInfo(Map<String, Object> param){
        Map<String, Object> retMap=new HashMap<>();
        //遍历循环，以每家门店为单位
        List<Map<String, Object>> shopArray=(List) JSONObject.parseArray(nullToEmpty(param.get("body")));
        //挑选出全部medicine_on_service=1的记录，成为一个单独list
//        List<Map<String, Object>> medicineInfoArray=new ArrayList<Map<String,Object>>();
        //挑选出全部medicine_on_service=0的记录，成为一个单独list
//        List<Map<String, Object>> switchInfoArray=new ArrayList<Map<String,Object>>();
//        for (Map m : shopArray){
//            //medicine_on_service=1是一套逻辑，涉及到读取异步任务表+更新药德系统和外卖平台
//            if (nullToEmpty(m.get("medicine_on_service")).equals("1")){
//                medicineInfoArray.add(m);
//            }else {
//                //medicine_on_service=0是另一套逻辑，直接更新开关信息表
//                switchInfoArray.add(m);
//            }
//        }
        //如果存在medicine_on_service=1的记录，执行下面逻辑
//        if (medicineInfoArray.size()>0){
//            //异步任务信息汇总表map
//            Map<String, Object> myAsyncTaskMap=new HashMap<>();
//            myAsyncTaskMap.put("loginid",param.get("loginid"));
//            myAsyncTaskMap.put("transaction_kind","switchupdate");
//            //查询当前用户，事务类别为switchupdate(开关设置)，是否有还在执行的任务。成功和异常都视为结束
//            int exist=baseMapperWeb.existMyAsyncTaskInfo(myAsyncTaskMap);
//            if (exist>0){
//                //如果存在，直接返回前台：当前用户还有未执行完的事务
//                retMap.put(retCode,existMyAsyncTaskCode);
//                retMap.put(retDesc,existMyAsyncTaskDesc);
//            }else {
//                //如果不存在，同步执行switchinfo表的信息更新+异步执行外卖平台等信息同步
//                retMap=synchSwitchUpdate(medicineInfoArray);
//                //只有同步更新开关设置表成功，才执行异步更新
//                if (retMap.get(retCode).equals("0")){
//                    String loginid = nullToEmpty(param.get("loginid"));
//                    TaskExecutorUtil.asyncSwitchUpdate(loginid, medicineInfoArray);
//                }
//            }
//        }else {
//            //如果不存在medicine_on_service=1的记录，说明都是medicine_on_service=0，走下面
//            retMap=synchSwitchUpdate(switchInfoArray);
//        }
        retMap=synchSwitchUpdate(shopArray);
        return retMap;
    }

    /**
     * 获取当前用户全部异步任务信息。支持分页查询
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getMyAsyncTaskInfo(Map<String, Object> param){
        Map<String, Object> retMap = new HashMap<>();
        try {
            //loginid
            String loginid = nullToEmpty(param.get("loginid"));
            String create_time = nullToEmpty(param.get("create_time"));
            String update_time = nullToEmpty(param.get("update_time"));
            String kind = nullToEmpty(param.get("kind"));
            String status = nullToEmpty(param.get("status"));
            List<String> paramList = new ArrayList<>();
            //创建时间
            if (!StringUtils.isNullOrEmpty(create_time)){
                String[] createtimeArr = create_time.split("~");
                //开始时间
                paramList.add(createtimeArr[0]);
                paramList.add(createtimeArr[1]);
            }else {
                paramList.add("");
                paramList.add("");
            }
            //更新时间
            if (!StringUtils.isNullOrEmpty(update_time)){
                String[] updatetimeArr = update_time.split("~");
                //开始时间
                paramList.add(updatetimeArr[0]);
                paramList.add(updatetimeArr[1]);
            }else {
                paramList.add("");
                paramList.add("");
            }
            paramList.add(kind);
            paramList.add(status);
            paramList.add(loginid);
            Map<String, Object> paramTemp=checkNullOrEmpty(paramList);
            //查询字段
            paramTemp.put("fields", "*");
            //表名
            paramTemp.put("tables", "t_asynctasksummary_info");
            //条件
            String where="1=1";
            if(!StringUtils.isNullOrEmpty(create_time)){
                where+=" AND create_time>=? AND create_time<=?";
            }
            if(!StringUtils.isNullOrEmpty(update_time)){
                where+=" AND update_time>=? AND update_time<=?";
            }
            if(!StringUtils.isNullOrEmpty(kind)){
                where+=" AND transaction_kind=?";
            }
            if(!StringUtils.isNullOrEmpty(status)){
                where+=" AND transaction_status=?";
            }
            where+=" AND create_user=?";
            paramTemp.put("where", where);
            //排序字段
            paramTemp.put("orderbyFields", nullToEmpty(param.get("sortField")));
            //排序规则
            paramTemp.put("orderby", nullToEmpty(param.get("sortOrder")));
            //页码
            paramTemp.put("pageindex", Integer.valueOf(nullToEmpty(param.get("pageIndex"))));
            //每页记录数
            paramTemp.put("pagesize", Integer.valueOf(nullToEmpty(param.get("pageSize"))));
            List<Map<String,Object>> retTemp = baseMapperWeb.getInfoByProc(paramTemp);
            Map<String, Object> retTempMap = new HashMap<>();
            retTempMap.put("totalCount",nullToEmpty(paramTemp.get("totalcount")));
            retTempMap.put("totalPage",nullToEmpty(paramTemp.get("pagecount")));
            retTempMap.put("pageIndex",nullToEmpty(param.get("pageIndex")));
            retTempMap.put("pageSize",nullToEmpty(param.get("pageSize")));
            retTempMap.put("rows",retTemp);
            retMap.put("data",retTempMap);
            retMap.put(retCode,successCode);
            retMap.put(retDesc,successDesc);
        }catch (Exception e){
            retMap.put(retCode,otherFailCode);
            otherFailDesc = e.getMessage();
            retMap.put(retDesc,otherFailDesc);
        }
        return retMap;
    }

    /**
     * 设置订单开关信息
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getMyAsyncTaskDetail(Map<String, Object> param){
        Map<String, Object> retMap=new HashMap<>();
        try {
            Map<String, Object> switchUpdateMap=(Map)JSONObject.parse(nullToEmpty(param.get("body")));
            switchUpdateMap.put("type",2);
            switchUpdateMap.put("tableName",nullToEmpty(switchUpdateMap.get("transaction_table_name")));
            switchUpdateMap.put("shop_no",null);
            switchUpdateMap.put("platforms",null);
            switchUpdateMap.put("switchinfo",null);
            switchUpdateMap.put("result",null);
            List<Map<String,Object>> result = SwichSetTable(switchUpdateMap);
            if (result==null){
                retMap.put(retCode,otherFailCode);
                retMap.put(retDesc,otherFailDesc);
            }else {
                retMap.put(retCode,successCode);
                //成功，需要返回一个单独的list
                retMap.put("rows",result);
                retMap.put(retDesc,successDesc);
            }
        }catch (Exception e){
            retMap.put(retCode,otherFailCode);
            otherFailDesc=e.getMessage();
            retMap.put(retDesc,otherFailDesc);
        }
        return retMap;
    }

    /**
     * 获取药品信息
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getMedicineInfo(Map<String, Object> param){
        Map<String, Object> retMap=new HashMap<>();
        try {
            //解析shopArray
            String shopArray="";
            List<Map<String,Object>> shopList=(List)param.get("shopArray");
            for (Map m : shopList){
                shopArray+="SELECT '"+nullToEmpty(m.get("DEPTCODE"))+"' shop_no UNION ";
            }
            //去掉最后一个UNION
            shopArray=shopArray.substring(0,shopArray.lastIndexOf("UNION")-1);
            String update_time = nullToEmpty(param.get("update_time"));
            String shop_no = nullToEmpty(param.get("shop_no"));
//            String shop_name = nullToEmpty(param.get("shop_name"));
            String medicine_code = nullToEmpty(param.get("medicine_code"));
//            String medicine_name = nullToEmpty(param.get("medicine_name"));
            List<String> paramList = new ArrayList<>();
            if (!StringUtils.isNullOrEmpty(update_time)){
                String[] updatetimeArr = update_time.split("~");
                //开始时间
                paramList.add(updatetimeArr[0]);
                paramList.add(updatetimeArr[1]);
            }else {
                paramList.add("");
                paramList.add("");
            }
            paramList.add(shop_no);
//            if(!StringUtils.isNullOrEmpty(shop_name)){
//                paramList.add("%"+shop_name+"%");
//            }else {
//                paramList.add("");
//            }
            paramList.add(medicine_code);
//            if(!StringUtils.isNullOrEmpty(medicine_name)){
////                paramList.add("%"+medicine_name+"%");
////            }else {
////                paramList.add("");
////            }
            paramList.add("");
            paramList.add("");
            paramList.add("");
            Map<String, Object> paramTemp=checkNullOrEmpty(paramList);
            //查询字段
            paramTemp.put("fields", "t1.*,t2.shop_name");
            //表名
            paramTemp.put("tables", "t_medicine_info t1\n"
                    +"INNER JOIN t_shop_info t2 ON t2.shop_no = t1.shop_no\n"
                    +"INNER JOIN (" + shopArray +")t3 ON t3.shop_no = t1.shop_no");
            //条件
            String where="1=1";
            if(!StringUtils.isNullOrEmpty(update_time)){
                where+=" AND t1.update_time>=? AND t1.update_time<=?";
            }
            if(!StringUtils.isNullOrEmpty(shop_no)){
                where+=" AND t1.shop_no=?";
            }
//            if(!StringUtils.isNullOrEmpty(shop_name)){
//                where+=" AND t2.shop_name LIKE ?";
//            }
            if(!StringUtils.isNullOrEmpty(medicine_code)){
                where+=" AND t1.medicine_code=?";
            }
//            if(!StringUtils.isNullOrEmpty(medicine_name)){
//                where+=" AND t1.medicine_name LIKE ?";
//            }
            paramTemp.put("where", where);
            //排序字段
            paramTemp.put("orderbyFields", nullToEmpty(param.get("sortField")));
            //排序规则
            paramTemp.put("orderby", nullToEmpty(param.get("sortOrder")));
            //页码
            paramTemp.put("pageindex", Integer.valueOf(nullToEmpty(param.get("pageIndex"))));
            //每页记录数
            paramTemp.put("pagesize", Integer.valueOf(nullToEmpty(param.get("pageSize"))));
            List<Map<String,Object>> retTemp = baseMapperWeb.getInfoByProc(paramTemp);
            Map<String, Object> retTempMap = new HashMap<>();
            retTempMap.put("totalCount",nullToEmpty(paramTemp.get("totalcount")));
            retTempMap.put("totalPage",nullToEmpty(paramTemp.get("pagecount")));
            retTempMap.put("pageIndex",nullToEmpty(param.get("pageIndex")));
            retTempMap.put("pageSize",nullToEmpty(param.get("pageSize")));
            retTempMap.put("rows",retTemp);
            retMap.put("data",retTempMap);
            retMap.put(retCode,successCode);
            retMap.put(retDesc,successDesc);
        }catch (Exception e){
            retMap.put(retCode,otherFailCode);
            otherFailDesc = e.getMessage();
            retMap.put(retDesc,otherFailDesc);
        }
        return retMap;
    }

    /**
     * 同步SAP药品信息(遵循：少则新增；有则更新。药品价格+药品库存重置)
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> setMedicineInfo(Map<String, Object> param){
        Map<String, Object> retMap=new HashMap<>();
        //勾选数据，修改读取pts接口数据逻辑：改成按时间去搜索
        //由于有可能勾选的很多，因此我们要考虑后台是一次性怼玩再返回还是异步返回？
        //感觉应该是同步，毕竟是勾选那么多，再打开谁记得自己刚刚做了什么呀

        return retMap;
    }

    /**
     * 同步执行switchinfo表的信息更新
     * @param paramArray
     * @return
     */
    public Map<String, Object> synchSwitchUpdate(List<Map<String, Object>> paramArray){
        Map<String, Object> retMap=new HashMap<>();
        try {
            baseMapperWeb.setSwitchInfo(paramArray);
            retMap.put(retCode,successCode);
            retMap.put(retDesc,successDesc);
        }catch (Exception e){
            retMap.put(retCode,otherFailCode);
            otherFailDesc=e.getMessage();
            retMap.put(retDesc,otherFailDesc);
            logger.info(otherFailDesc);
        }
        return retMap;
    }

    /**
     * 插入异步任务信息
     * @param param
     */
    @Override
    public String insertMyAsyncTask(Map<String, Object> param){
        String result;
        try {
            baseMapperWeb.insertMyAsyncTask(param);
            result="ok";
        }catch (Exception e){
            result="ERROR:"+e.getMessage();
            logger.info(result);
        }
        return result;
    }

    /**
     * 创建开关设置记录表
     * @param param
     * @return
     */
    @Override
    public List<Map<String,Object>> SwichSetTable(Map<String, Object> param){
        List<Map<String,Object>> result=null;
        try {
            result = baseMapperWeb.SwichSetTable(param);
        }catch (Exception e){
            logger.info(e.getMessage());
        }
        return result;
    }

    /**
     * 更新异步任务信息汇总表
     * @param param
     */
    @Override
    public void updateMyAsyncTask(Map<String, Object> param){
        try {
            baseMapperWeb.updateMyAsyncTask(param);
        }catch (Exception e){
            logger.info(e.getMessage());
        }
    }

    /**
     * 字符串转换函数
     * @param param 参数
     * @return String
     */
    public static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }
    }

    /**
     * 判断传入的5个参数哪个为空，返回不为空的字符串
     * @param paramList
     * @return
     */
    public static Map<String, Object> checkNullOrEmpty(List<String> paramList){
        Map<String, Object> paramMap = new HashMap<>();
        int i=1;
        for (int j=0;j<paramList.size();j++){
            if(!StringUtils.isNullOrEmpty(paramList.get(j))){
                paramMap.put("param"+i, paramList.get(j));
                i++;
            }
        }
        for (int j=0;j<paramList.size();j++){
            if(StringUtils.isNullOrEmpty(paramList.get(j))){
                paramMap.put("param"+i, paramList.get(j));
                i++;
            }
        }
        return paramMap;
    }
}

