package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author
 */
@ApiModel(value = "", description = "处方信息")
@Data
public class RecipelInfoInputData implements Serializable {
    private static final long serialVersionUID = 7474439905294388181L;

    @ApiModelProperty(value = "症状")
    private String symptom;

    @ApiModelProperty(value = "手机/电话")
    private String phone;

    @ApiModelProperty(value = "诊断")
    private String diacrisis;

    @ApiModelProperty(value = "身份证号")
    private String cardId;

    @ApiModelProperty(value = "性别")
    private String sex;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "年龄")
    private String age;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "销售单号")
    private String gssrVoucherId;

    @ApiModelProperty(value = "")
    private String gssrType;

    @ApiModelProperty(value = "登记店号")
    private String gssrBrId;

    @ApiModelProperty(value = "登记店名")
    private String gssrBrName;

    @ApiModelProperty(value = "登记日期-yyyyMMdd")
    private String gssrDate;

    @ApiModelProperty(value = "登记时间-HH:mm:ss")
    private String gssrTime;

    @ApiModelProperty(value = "登记店员")
    private String gssrEmp;

    @ApiModelProperty(value = "销售门店")
    private String gssrSaleBrId;

    @ApiModelProperty(value = "销售日期-yyyyMMdd")
    private String gssrSaleDate;

    @ApiModelProperty(value = "销售单号")
    private String gssrSaleBillNo;

    @ApiModelProperty(value = "审方状态 （默认写死0）")
    private String gssrCheckStatus;

    @ApiModelProperty(value = "")
    private boolean reInput;

    @ApiModelProperty(value = "")
    private boolean firstInput;

    @ApiModelProperty(value = "审方药师编号")
    private String gssrPharmacistId;

    @ApiModelProperty(value = "审方药师名称")
    private String gssrPharmacistName;

    @ApiModelProperty(value = "FX处方单号")
    private String caseId;

    @ApiModelProperty(value = "FX处方医院")
    private String hospitalName;

    @ApiModelProperty(value = "FX处方科室")
    private String office;

    @ApiModelProperty(value = "FX处方医生")
    private String doctor;

    @ApiModelProperty(value = "Web处方单号")
    private String prescriptionNumber;

    @ApiModelProperty(value = "Web处方医院")
    private String prescriptionHospital;

    @ApiModelProperty(value = "Web处方科室")
    private String department;

    @ApiModelProperty(value = "Web处方医生")
    private String doctors;


    @ApiModelProperty(value = "处方药品信息")
    private List<SelectRecipelDrugsOutData> recipelDrugsList;



    /**
     * 是否是补录信息 GSSR_FLAG，0/NULL正常，1为补录gssrFlag
     */
    @ApiModelProperty(value = "是否是补录信息")
    private String gssrFlag;

}
