package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class EssentialProductCalculationInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "店效级别 null-全部 1-超高 2-高 3-中 4-低")
    private String storeLevel;

    @ApiModelProperty(value = "门店属性 null-全部 1-单体 2-连锁 3-加盟 4-门诊")
    private String storeAttribute;

    @ApiModelProperty(value = "数据来源")
    private String sourceDate;

    @ApiModelProperty(value = "起始日期")
    private String beginDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "门店编码列表")
    private List<String> storeIdList;

    @ApiModelProperty(value = "订单号")
    private String orderId;
}
