package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/11 14:20
 */
@Data
@CsvRow
public class UserTrainingRecordOutData {
    /**
     * 单号
     */
    private String gutrVoucherId;

    /**
     * 店号
     */
    private String gutrBrId;

    /**
     * 店名
     */
    @CsvCell(title = "部门",index = 1, fieldNo = 1)
    private String gutrBrName;

    /**
     * 员工姓名
     */
    @CsvCell(title = "姓名",index = 2, fieldNo = 1)
    private String gutrUserName;

    /**
     * 起始时间
     */
    @CsvCell(title = "起始日期",index = 3, fieldNo = 1)
    private String gutrDateStart;

    /**
     * 终止时间
     */
    @CsvCell(title = "终止日期",index = 4, fieldNo = 1)
    private String gutrDateEnd;

    /**
     * 培训类别
     */
    @CsvCell(title = "培训类别",index = 5, fieldNo = 1)
    private String gutrCategory;

    /**
     * 培训类型
     */
    @CsvCell(title = "培训类型",index = 6, fieldNo = 1)
    private String gutrType;

    /**
     * 培训内容
     */
    @CsvCell(title = "培训内容",index = 7, fieldNo = 1)
    private String gutrContent;

    /**
     * 主办单位
     */
    @CsvCell(title = "主办单位",index = 8, fieldNo = 1)
    private String gutrOrg;

    /**
     * 培训教师
     */
    @CsvCell(title = "培训教师",index = 9, fieldNo = 1)
    private String gutrTrainer;

    /**
     * 课时
     */
    @CsvCell(title = "课时",index = 10, fieldNo = 1)
    private String gutrClassHour;

    /**
     * 培训地点
     */
    @CsvCell(title = "培训地点",index = 11, fieldNo = 1)
    private String gutrLocation;

    /**
     * 成绩
     */
    @CsvCell(title = "成绩",index = 12, fieldNo = 1)
    private String gutrScore;

    /**
     * 性别
     */
    @CsvCell(title = "性别",index = 13, fieldNo = 1)
    private String gutrSex;

    /**
     * 职务
     */
    @CsvCell(title = "职务",index = 14, fieldNo = 1)
    private String gutrPost;

    /**
     * 职称
     */
    @CsvCell(title = "职称",index = 19, fieldNo = 1)
    private String gutrTitle;

    /**
     * 岗位
     */
    @CsvCell(title = "岗位",index = 18, fieldNo = 1)
    private String gutrJob;

    /**
     * 学历
     */
    @CsvCell(title = "学历",index = 15, fieldNo = 1)
    private String gutrEducation;

    /**
     * 资格证书
     */
    @CsvCell(title = "资格证书",index = 20, fieldNo = 1)
    private String gutrQualification;

    /**
     * 出生日期
     */
    @CsvCell(title = "出生日期",index = 16, fieldNo = 1)
    private String gutrBirthday;

    /**
     * 入司日期
     */
    @CsvCell(title = "入司日期",index = 17, fieldNo = 1)
    private String gutrJoinDate;
}
