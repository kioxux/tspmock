//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class CashPaymentInData {
    private String clientId;
    private String gsdhBrId;
    private String gsdhVoucherId;
    private String gsdhDepositAmt;
    private String gsdhBankId;
    private String gsdhDepositDate;
    private String gsddEmpGroup;
    private String gsddEmp;
    private String gsdhStatus;
    private String gsdhCheckDate;

    public CashPaymentInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsdhBrId() {
        return this.gsdhBrId;
    }

    public String getGsdhVoucherId() {
        return this.gsdhVoucherId;
    }

    public String getGsdhDepositAmt() {
        return this.gsdhDepositAmt;
    }

    public String getGsdhBankId() {
        return this.gsdhBankId;
    }

    public String getGsdhDepositDate() {
        return this.gsdhDepositDate;
    }

    public String getGsddEmpGroup() {
        return this.gsddEmpGroup;
    }

    public String getGsddEmp() {
        return this.gsddEmp;
    }

    public String getGsdhStatus() {
        return this.gsdhStatus;
    }

    public String getGsdhCheckDate() {
        return this.gsdhCheckDate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsdhBrId(final String gsdhBrId) {
        this.gsdhBrId = gsdhBrId;
    }

    public void setGsdhVoucherId(final String gsdhVoucherId) {
        this.gsdhVoucherId = gsdhVoucherId;
    }

    public void setGsdhDepositAmt(final String gsdhDepositAmt) {
        this.gsdhDepositAmt = gsdhDepositAmt;
    }

    public void setGsdhBankId(final String gsdhBankId) {
        this.gsdhBankId = gsdhBankId;
    }

    public void setGsdhDepositDate(final String gsdhDepositDate) {
        this.gsdhDepositDate = gsdhDepositDate;
    }

    public void setGsddEmpGroup(final String gsddEmpGroup) {
        this.gsddEmpGroup = gsddEmpGroup;
    }

    public void setGsddEmp(final String gsddEmp) {
        this.gsddEmp = gsddEmp;
    }

    public void setGsdhStatus(final String gsdhStatus) {
        this.gsdhStatus = gsdhStatus;
    }

    public void setGsdhCheckDate(final String gsdhCheckDate) {
        this.gsdhCheckDate = gsdhCheckDate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof CashPaymentInData)) {
            return false;
        } else {
            CashPaymentInData other = (CashPaymentInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsdhBrId = this.getGsdhBrId();
                Object other$gsdhBrId = other.getGsdhBrId();
                if (this$gsdhBrId == null) {
                    if (other$gsdhBrId != null) {
                        return false;
                    }
                } else if (!this$gsdhBrId.equals(other$gsdhBrId)) {
                    return false;
                }

                Object this$gsdhVoucherId = this.getGsdhVoucherId();
                Object other$gsdhVoucherId = other.getGsdhVoucherId();
                if (this$gsdhVoucherId == null) {
                    if (other$gsdhVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsdhVoucherId.equals(other$gsdhVoucherId)) {
                    return false;
                }

                label110: {
                    Object this$gsdhDepositAmt = this.getGsdhDepositAmt();
                    Object other$gsdhDepositAmt = other.getGsdhDepositAmt();
                    if (this$gsdhDepositAmt == null) {
                        if (other$gsdhDepositAmt == null) {
                            break label110;
                        }
                    } else if (this$gsdhDepositAmt.equals(other$gsdhDepositAmt)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gsdhBankId = this.getGsdhBankId();
                    Object other$gsdhBankId = other.getGsdhBankId();
                    if (this$gsdhBankId == null) {
                        if (other$gsdhBankId == null) {
                            break label103;
                        }
                    } else if (this$gsdhBankId.equals(other$gsdhBankId)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gsdhDepositDate = this.getGsdhDepositDate();
                Object other$gsdhDepositDate = other.getGsdhDepositDate();
                if (this$gsdhDepositDate == null) {
                    if (other$gsdhDepositDate != null) {
                        return false;
                    }
                } else if (!this$gsdhDepositDate.equals(other$gsdhDepositDate)) {
                    return false;
                }

                label89: {
                    Object this$gsddEmpGroup = this.getGsddEmpGroup();
                    Object other$gsddEmpGroup = other.getGsddEmpGroup();
                    if (this$gsddEmpGroup == null) {
                        if (other$gsddEmpGroup == null) {
                            break label89;
                        }
                    } else if (this$gsddEmpGroup.equals(other$gsddEmpGroup)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gsddEmp = this.getGsddEmp();
                    Object other$gsddEmp = other.getGsddEmp();
                    if (this$gsddEmp == null) {
                        if (other$gsddEmp == null) {
                            break label82;
                        }
                    } else if (this$gsddEmp.equals(other$gsddEmp)) {
                        break label82;
                    }

                    return false;
                }

                Object this$gsdhStatus = this.getGsdhStatus();
                Object other$gsdhStatus = other.getGsdhStatus();
                if (this$gsdhStatus == null) {
                    if (other$gsdhStatus != null) {
                        return false;
                    }
                } else if (!this$gsdhStatus.equals(other$gsdhStatus)) {
                    return false;
                }

                Object this$gsdhCheckDate = this.getGsdhCheckDate();
                Object other$gsdhCheckDate = other.getGsdhCheckDate();
                if (this$gsdhCheckDate == null) {
                    if (other$gsdhCheckDate != null) {
                        return false;
                    }
                } else if (!this$gsdhCheckDate.equals(other$gsdhCheckDate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CashPaymentInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsdhBrId = this.getGsdhBrId();
        result = result * 59 + ($gsdhBrId == null ? 43 : $gsdhBrId.hashCode());
        Object $gsdhVoucherId = this.getGsdhVoucherId();
        result = result * 59 + ($gsdhVoucherId == null ? 43 : $gsdhVoucherId.hashCode());
        Object $gsdhDepositAmt = this.getGsdhDepositAmt();
        result = result * 59 + ($gsdhDepositAmt == null ? 43 : $gsdhDepositAmt.hashCode());
        Object $gsdhBankId = this.getGsdhBankId();
        result = result * 59 + ($gsdhBankId == null ? 43 : $gsdhBankId.hashCode());
        Object $gsdhDepositDate = this.getGsdhDepositDate();
        result = result * 59 + ($gsdhDepositDate == null ? 43 : $gsdhDepositDate.hashCode());
        Object $gsddEmpGroup = this.getGsddEmpGroup();
        result = result * 59 + ($gsddEmpGroup == null ? 43 : $gsddEmpGroup.hashCode());
        Object $gsddEmp = this.getGsddEmp();
        result = result * 59 + ($gsddEmp == null ? 43 : $gsddEmp.hashCode());
        Object $gsdhStatus = this.getGsdhStatus();
        result = result * 59 + ($gsdhStatus == null ? 43 : $gsdhStatus.hashCode());
        Object $gsdhCheckDate = this.getGsdhCheckDate();
        result = result * 59 + ($gsdhCheckDate == null ? 43 : $gsdhCheckDate.hashCode());
        return result;
    }

    public String toString() {
        return "CashPaymentInData(clientId=" + this.getClientId() + ", gsdhBrId=" + this.getGsdhBrId() + ", gsdhVoucherId=" + this.getGsdhVoucherId() + ", gsdhDepositAmt=" + this.getGsdhDepositAmt() + ", gsdhBankId=" + this.getGsdhBankId() + ", gsdhDepositDate=" + this.getGsdhDepositDate() + ", gsddEmpGroup=" + this.getGsddEmpGroup() + ", gsddEmp=" + this.getGsddEmp() + ", gsdhStatus=" + this.getGsdhStatus() + ", gsdhCheckDate=" + this.getGsdhCheckDate() + ")";
    }
}
