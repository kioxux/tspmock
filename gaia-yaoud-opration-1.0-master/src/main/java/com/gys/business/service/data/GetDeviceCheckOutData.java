//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class GetDeviceCheckOutData {
    @ApiModelProperty(value = "单号", example = "")
    private String gsdcVoucherId;
    @ApiModelProperty(value = "门店ID", example = "")
    private String gsdcBrId;
    @ApiModelProperty(value = "门店名称", example = "")
    private String gsdcBrName;
    @ApiModelProperty(value = "设备编号", example = "")
    private String gsdcDeviceId;
    @ApiModelProperty(value = "设备名称", example = "")
    private String gsdcDeviceName;
    @ApiModelProperty(value = "设备型号", example = "")
    private String gsdcDeviceModel;
    @ApiModelProperty(value = "检查内容", example = "")
    private String gsdcCheckRemarks;
    @ApiModelProperty(value = "检查情况", example = "")
    private String gsdcCheckResult;
    @ApiModelProperty(value = "检查措施", example = "")
    private String gsdcCheckStep;
    @ApiModelProperty(value = "登记人员", example = "")
    private String gsdcUpdateEmp;
    @ApiModelProperty(value = "登记日期", example = "")
    private String gsdcUpdateDate;
    @ApiModelProperty(value = "检查日期", example = "")
    private String gsdcUpdateTime;
    @ApiModelProperty(value = "审核状态", example = "")
    private String gsdcStatus;

    public GetDeviceCheckOutData() {
    }

    public String getGsdcVoucherId() {
        return this.gsdcVoucherId;
    }

    public String getGsdcBrId() {
        return this.gsdcBrId;
    }

    public String getGsdcBrName() {
        return this.gsdcBrName;
    }

    public String getGsdcDeviceId() {
        return this.gsdcDeviceId;
    }

    public String getGsdcDeviceName() {
        return this.gsdcDeviceName;
    }

    public String getGsdcDeviceModel() {
        return this.gsdcDeviceModel;
    }

    public String getGsdcCheckRemarks() {
        return this.gsdcCheckRemarks;
    }

    public String getGsdcCheckResult() {
        return this.gsdcCheckResult;
    }

    public String getGsdcCheckStep() {
        return this.gsdcCheckStep;
    }

    public String getGsdcUpdateEmp() {
        return this.gsdcUpdateEmp;
    }

    public String getGsdcUpdateDate() {
        return this.gsdcUpdateDate;
    }

    public String getGsdcUpdateTime() {
        return this.gsdcUpdateTime;
    }

    public String getGsdcStatus() {
        return this.gsdcStatus;
    }

    public void setGsdcVoucherId(final String gsdcVoucherId) {
        this.gsdcVoucherId = gsdcVoucherId;
    }

    public void setGsdcBrId(final String gsdcBrId) {
        this.gsdcBrId = gsdcBrId;
    }

    public void setGsdcBrName(final String gsdcBrName) {
        this.gsdcBrName = gsdcBrName;
    }

    public void setGsdcDeviceId(final String gsdcDeviceId) {
        this.gsdcDeviceId = gsdcDeviceId;
    }

    public void setGsdcDeviceName(final String gsdcDeviceName) {
        this.gsdcDeviceName = gsdcDeviceName;
    }

    public void setGsdcDeviceModel(final String gsdcDeviceModel) {
        this.gsdcDeviceModel = gsdcDeviceModel;
    }

    public void setGsdcCheckRemarks(final String gsdcCheckRemarks) {
        this.gsdcCheckRemarks = gsdcCheckRemarks;
    }

    public void setGsdcCheckResult(final String gsdcCheckResult) {
        this.gsdcCheckResult = gsdcCheckResult;
    }

    public void setGsdcCheckStep(final String gsdcCheckStep) {
        this.gsdcCheckStep = gsdcCheckStep;
    }

    public void setGsdcUpdateEmp(final String gsdcUpdateEmp) {
        this.gsdcUpdateEmp = gsdcUpdateEmp;
    }

    public void setGsdcUpdateDate(final String gsdcUpdateDate) {
        this.gsdcUpdateDate = gsdcUpdateDate;
    }

    public void setGsdcUpdateTime(final String gsdcUpdateTime) {
        this.gsdcUpdateTime = gsdcUpdateTime;
    }

    public void setGsdcStatus(final String gsdcStatus) {
        this.gsdcStatus = gsdcStatus;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetDeviceCheckOutData)) {
            return false;
        } else {
            GetDeviceCheckOutData other = (GetDeviceCheckOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$gsdcVoucherId = this.getGsdcVoucherId();
                    Object other$gsdcVoucherId = other.getGsdcVoucherId();
                    if (this$gsdcVoucherId == null) {
                        if (other$gsdcVoucherId == null) {
                            break label167;
                        }
                    } else if (this$gsdcVoucherId.equals(other$gsdcVoucherId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gsdcBrId = this.getGsdcBrId();
                Object other$gsdcBrId = other.getGsdcBrId();
                if (this$gsdcBrId == null) {
                    if (other$gsdcBrId != null) {
                        return false;
                    }
                } else if (!this$gsdcBrId.equals(other$gsdcBrId)) {
                    return false;
                }

                label153: {
                    Object this$gsdcBrName = this.getGsdcBrName();
                    Object other$gsdcBrName = other.getGsdcBrName();
                    if (this$gsdcBrName == null) {
                        if (other$gsdcBrName == null) {
                            break label153;
                        }
                    } else if (this$gsdcBrName.equals(other$gsdcBrName)) {
                        break label153;
                    }

                    return false;
                }

                Object this$gsdcDeviceId = this.getGsdcDeviceId();
                Object other$gsdcDeviceId = other.getGsdcDeviceId();
                if (this$gsdcDeviceId == null) {
                    if (other$gsdcDeviceId != null) {
                        return false;
                    }
                } else if (!this$gsdcDeviceId.equals(other$gsdcDeviceId)) {
                    return false;
                }

                label139: {
                    Object this$gsdcDeviceName = this.getGsdcDeviceName();
                    Object other$gsdcDeviceName = other.getGsdcDeviceName();
                    if (this$gsdcDeviceName == null) {
                        if (other$gsdcDeviceName == null) {
                            break label139;
                        }
                    } else if (this$gsdcDeviceName.equals(other$gsdcDeviceName)) {
                        break label139;
                    }

                    return false;
                }

                Object this$gsdcDeviceModel = this.getGsdcDeviceModel();
                Object other$gsdcDeviceModel = other.getGsdcDeviceModel();
                if (this$gsdcDeviceModel == null) {
                    if (other$gsdcDeviceModel != null) {
                        return false;
                    }
                } else if (!this$gsdcDeviceModel.equals(other$gsdcDeviceModel)) {
                    return false;
                }

                label125: {
                    Object this$gsdcCheckRemarks = this.getGsdcCheckRemarks();
                    Object other$gsdcCheckRemarks = other.getGsdcCheckRemarks();
                    if (this$gsdcCheckRemarks == null) {
                        if (other$gsdcCheckRemarks == null) {
                            break label125;
                        }
                    } else if (this$gsdcCheckRemarks.equals(other$gsdcCheckRemarks)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$gsdcCheckResult = this.getGsdcCheckResult();
                    Object other$gsdcCheckResult = other.getGsdcCheckResult();
                    if (this$gsdcCheckResult == null) {
                        if (other$gsdcCheckResult == null) {
                            break label118;
                        }
                    } else if (this$gsdcCheckResult.equals(other$gsdcCheckResult)) {
                        break label118;
                    }

                    return false;
                }

                Object this$gsdcCheckStep = this.getGsdcCheckStep();
                Object other$gsdcCheckStep = other.getGsdcCheckStep();
                if (this$gsdcCheckStep == null) {
                    if (other$gsdcCheckStep != null) {
                        return false;
                    }
                } else if (!this$gsdcCheckStep.equals(other$gsdcCheckStep)) {
                    return false;
                }

                label104: {
                    Object this$gsdcUpdateEmp = this.getGsdcUpdateEmp();
                    Object other$gsdcUpdateEmp = other.getGsdcUpdateEmp();
                    if (this$gsdcUpdateEmp == null) {
                        if (other$gsdcUpdateEmp == null) {
                            break label104;
                        }
                    } else if (this$gsdcUpdateEmp.equals(other$gsdcUpdateEmp)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$gsdcUpdateDate = this.getGsdcUpdateDate();
                    Object other$gsdcUpdateDate = other.getGsdcUpdateDate();
                    if (this$gsdcUpdateDate == null) {
                        if (other$gsdcUpdateDate == null) {
                            break label97;
                        }
                    } else if (this$gsdcUpdateDate.equals(other$gsdcUpdateDate)) {
                        break label97;
                    }

                    return false;
                }

                Object this$gsdcUpdateTime = this.getGsdcUpdateTime();
                Object other$gsdcUpdateTime = other.getGsdcUpdateTime();
                if (this$gsdcUpdateTime == null) {
                    if (other$gsdcUpdateTime != null) {
                        return false;
                    }
                } else if (!this$gsdcUpdateTime.equals(other$gsdcUpdateTime)) {
                    return false;
                }

                Object this$gsdcStatus = this.getGsdcStatus();
                Object other$gsdcStatus = other.getGsdcStatus();
                if (this$gsdcStatus == null) {
                    if (other$gsdcStatus != null) {
                        return false;
                    }
                } else if (!this$gsdcStatus.equals(other$gsdcStatus)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetDeviceCheckOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsdcVoucherId = this.getGsdcVoucherId();
        result = result * 59 + ($gsdcVoucherId == null ? 43 : $gsdcVoucherId.hashCode());
        Object $gsdcBrId = this.getGsdcBrId();
        result = result * 59 + ($gsdcBrId == null ? 43 : $gsdcBrId.hashCode());
        Object $gsdcBrName = this.getGsdcBrName();
        result = result * 59 + ($gsdcBrName == null ? 43 : $gsdcBrName.hashCode());
        Object $gsdcDeviceId = this.getGsdcDeviceId();
        result = result * 59 + ($gsdcDeviceId == null ? 43 : $gsdcDeviceId.hashCode());
        Object $gsdcDeviceName = this.getGsdcDeviceName();
        result = result * 59 + ($gsdcDeviceName == null ? 43 : $gsdcDeviceName.hashCode());
        Object $gsdcDeviceModel = this.getGsdcDeviceModel();
        result = result * 59 + ($gsdcDeviceModel == null ? 43 : $gsdcDeviceModel.hashCode());
        Object $gsdcCheckRemarks = this.getGsdcCheckRemarks();
        result = result * 59 + ($gsdcCheckRemarks == null ? 43 : $gsdcCheckRemarks.hashCode());
        Object $gsdcCheckResult = this.getGsdcCheckResult();
        result = result * 59 + ($gsdcCheckResult == null ? 43 : $gsdcCheckResult.hashCode());
        Object $gsdcCheckStep = this.getGsdcCheckStep();
        result = result * 59 + ($gsdcCheckStep == null ? 43 : $gsdcCheckStep.hashCode());
        Object $gsdcUpdateEmp = this.getGsdcUpdateEmp();
        result = result * 59 + ($gsdcUpdateEmp == null ? 43 : $gsdcUpdateEmp.hashCode());
        Object $gsdcUpdateDate = this.getGsdcUpdateDate();
        result = result * 59 + ($gsdcUpdateDate == null ? 43 : $gsdcUpdateDate.hashCode());
        Object $gsdcUpdateTime = this.getGsdcUpdateTime();
        result = result * 59 + ($gsdcUpdateTime == null ? 43 : $gsdcUpdateTime.hashCode());
        Object $gsdcStatus = this.getGsdcStatus();
        result = result * 59 + ($gsdcStatus == null ? 43 : $gsdcStatus.hashCode());
        return result;
    }

    public String toString() {
        return "GetDeviceCheckOutData(gsdcVoucherId=" + this.getGsdcVoucherId() + ", gsdcBrId=" + this.getGsdcBrId() + ", gsdcBrName=" + this.getGsdcBrName() + ", gsdcDeviceId=" + this.getGsdcDeviceId() + ", gsdcDeviceName=" + this.getGsdcDeviceName() + ", gsdcDeviceModel=" + this.getGsdcDeviceModel() + ", gsdcCheckRemarks=" + this.getGsdcCheckRemarks() + ", gsdcCheckResult=" + this.getGsdcCheckResult() + ", gsdcCheckStep=" + this.getGsdcCheckStep() + ", gsdcUpdateEmp=" + this.getGsdcUpdateEmp() + ", gsdcUpdateDate=" + this.getGsdcUpdateDate() + ", gsdcUpdateTime=" + this.getGsdcUpdateTime() + ", gsdcStatus=" + this.getGsdcStatus() + ")";
    }
}
