//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetSyncSdStockOutData {
    private String clientId;
    private String gssBrId;
    private String gssProId;
    private String gssQty;
    private String gssUpdateDate;
    private String gssUpdateEmp;

    public GetSyncSdStockOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssBrId() {
        return this.gssBrId;
    }

    public String getGssProId() {
        return this.gssProId;
    }

    public String getGssQty() {
        return this.gssQty;
    }

    public String getGssUpdateDate() {
        return this.gssUpdateDate;
    }

    public String getGssUpdateEmp() {
        return this.gssUpdateEmp;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssBrId(final String gssBrId) {
        this.gssBrId = gssBrId;
    }

    public void setGssProId(final String gssProId) {
        this.gssProId = gssProId;
    }

    public void setGssQty(final String gssQty) {
        this.gssQty = gssQty;
    }

    public void setGssUpdateDate(final String gssUpdateDate) {
        this.gssUpdateDate = gssUpdateDate;
    }

    public void setGssUpdateEmp(final String gssUpdateEmp) {
        this.gssUpdateEmp = gssUpdateEmp;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdStockOutData)) {
            return false;
        } else {
            GetSyncSdStockOutData other = (GetSyncSdStockOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gssBrId = this.getGssBrId();
                Object other$gssBrId = other.getGssBrId();
                if (this$gssBrId == null) {
                    if (other$gssBrId != null) {
                        return false;
                    }
                } else if (!this$gssBrId.equals(other$gssBrId)) {
                    return false;
                }

                Object this$gssProId = this.getGssProId();
                Object other$gssProId = other.getGssProId();
                if (this$gssProId == null) {
                    if (other$gssProId != null) {
                        return false;
                    }
                } else if (!this$gssProId.equals(other$gssProId)) {
                    return false;
                }

                label62: {
                    Object this$gssQty = this.getGssQty();
                    Object other$gssQty = other.getGssQty();
                    if (this$gssQty == null) {
                        if (other$gssQty == null) {
                            break label62;
                        }
                    } else if (this$gssQty.equals(other$gssQty)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$gssUpdateDate = this.getGssUpdateDate();
                    Object other$gssUpdateDate = other.getGssUpdateDate();
                    if (this$gssUpdateDate == null) {
                        if (other$gssUpdateDate == null) {
                            break label55;
                        }
                    } else if (this$gssUpdateDate.equals(other$gssUpdateDate)) {
                        break label55;
                    }

                    return false;
                }

                Object this$gssUpdateEmp = this.getGssUpdateEmp();
                Object other$gssUpdateEmp = other.getGssUpdateEmp();
                if (this$gssUpdateEmp == null) {
                    if (other$gssUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gssUpdateEmp.equals(other$gssUpdateEmp)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdStockOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssBrId = this.getGssBrId();
        result = result * 59 + ($gssBrId == null ? 43 : $gssBrId.hashCode());
        Object $gssProId = this.getGssProId();
        result = result * 59 + ($gssProId == null ? 43 : $gssProId.hashCode());
        Object $gssQty = this.getGssQty();
        result = result * 59 + ($gssQty == null ? 43 : $gssQty.hashCode());
        Object $gssUpdateDate = this.getGssUpdateDate();
        result = result * 59 + ($gssUpdateDate == null ? 43 : $gssUpdateDate.hashCode());
        Object $gssUpdateEmp = this.getGssUpdateEmp();
        result = result * 59 + ($gssUpdateEmp == null ? 43 : $gssUpdateEmp.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdStockOutData(clientId=" + this.getClientId() + ", gssBrId=" + this.getGssBrId() + ", gssProId=" + this.getGssProId() + ", gssQty=" + this.getGssQty() + ", gssUpdateDate=" + this.getGssUpdateDate() + ", gssUpdateEmp=" + this.getGssUpdateEmp() + ")";
    }
}
