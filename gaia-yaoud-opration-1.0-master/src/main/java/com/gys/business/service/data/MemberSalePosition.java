package com.gys.business.service.data;


import lombok.Data;

/**
 * 商品定位
 */
@Data
public class MemberSalePosition {
    private String proPosition;
}
