package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetServerInData implements Serializable {
    private static final long serialVersionUID = 1275145688196471110L;
    private String clientId;
    private String billNo;
    private String storeCode;
    private String type;
    private String callMethod;
    private List<GetServerProductInData> productList;
}
