//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class FranchiseeOutData {
    private String client;
    private String francName;
    private String francNo;
    private String francLegalPerson;
    private String francLegalPersonName;
    private String francQua;
    private String francAddr;
    private String francCreDate;
    private String francCreTime;
    private String francCreId;
    private String francModiDate;
    private String francModiTime;
    private String francModiId;
    private String francLogo;
    private String francType1;
    private String francType2;
    private String francType3;
    private String francAss;
    private String francAssName;
    /*
     * 是否分配过菜单权限 1.是 2 否
     **/
    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getClient() {
        return this.client;
    }

    public String getFrancName() {
        return this.francName;
    }

    public String getFrancNo() {
        return this.francNo;
    }

    public String getFrancLegalPerson() {
        return this.francLegalPerson;
    }

    public String getFrancLegalPersonName() {
        return this.francLegalPersonName;
    }

    public String getFrancQua() {
        return this.francQua;
    }

    public String getFrancAddr() {
        return this.francAddr;
    }

    public String getFrancCreDate() {
        return this.francCreDate;
    }

    public String getFrancCreTime() {
        return this.francCreTime;
    }

    public String getFrancCreId() {
        return this.francCreId;
    }

    public String getFrancModiDate() {
        return this.francModiDate;
    }

    public String getFrancModiTime() {
        return this.francModiTime;
    }

    public String getFrancModiId() {
        return this.francModiId;
    }

    public String getFrancLogo() {
        return this.francLogo;
    }

    public String getFrancType1() {
        return this.francType1;
    }

    public String getFrancType2() {
        return this.francType2;
    }

    public String getFrancType3() {
        return this.francType3;
    }

    public String getFrancAss() {
        return this.francAss;
    }

    public String getFrancAssName() {
        return this.francAssName;
    }

    public void setClient(final String client) {
        this.client = client;
    }

    public void setFrancName(final String francName) {
        this.francName = francName;
    }

    public void setFrancNo(final String francNo) {
        this.francNo = francNo;
    }

    public void setFrancLegalPerson(final String francLegalPerson) {
        this.francLegalPerson = francLegalPerson;
    }

    public void setFrancLegalPersonName(final String francLegalPersonName) {
        this.francLegalPersonName = francLegalPersonName;
    }

    public void setFrancQua(final String francQua) {
        this.francQua = francQua;
    }

    public void setFrancAddr(final String francAddr) {
        this.francAddr = francAddr;
    }

    public void setFrancCreDate(final String francCreDate) {
        this.francCreDate = francCreDate;
    }

    public void setFrancCreTime(final String francCreTime) {
        this.francCreTime = francCreTime;
    }

    public void setFrancCreId(final String francCreId) {
        this.francCreId = francCreId;
    }

    public void setFrancModiDate(final String francModiDate) {
        this.francModiDate = francModiDate;
    }

    public void setFrancModiTime(final String francModiTime) {
        this.francModiTime = francModiTime;
    }

    public void setFrancModiId(final String francModiId) {
        this.francModiId = francModiId;
    }

    public void setFrancLogo(final String francLogo) {
        this.francLogo = francLogo;
    }

    public void setFrancType1(final String francType1) {
        this.francType1 = francType1;
    }

    public void setFrancType2(final String francType2) {
        this.francType2 = francType2;
    }

    public void setFrancType3(final String francType3) {
        this.francType3 = francType3;
    }

    public void setFrancAss(final String francAss) {
        this.francAss = francAss;
    }

    public void setFrancAssName(final String francAssName) {
        this.francAssName = francAssName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof FranchiseeOutData)) {
            return false;
        } else {
            FranchiseeOutData other = (FranchiseeOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label239: {
                    Object this$client = this.getClient();
                    Object other$client = other.getClient();
                    if (this$client == null) {
                        if (other$client == null) {
                            break label239;
                        }
                    } else if (this$client.equals(other$client)) {
                        break label239;
                    }

                    return false;
                }

                Object this$francName = this.getFrancName();
                Object other$francName = other.getFrancName();
                if (this$francName == null) {
                    if (other$francName != null) {
                        return false;
                    }
                } else if (!this$francName.equals(other$francName)) {
                    return false;
                }

                Object this$francNo = this.getFrancNo();
                Object other$francNo = other.getFrancNo();
                if (this$francNo == null) {
                    if (other$francNo != null) {
                        return false;
                    }
                } else if (!this$francNo.equals(other$francNo)) {
                    return false;
                }

                label218: {
                    Object this$francLegalPerson = this.getFrancLegalPerson();
                    Object other$francLegalPerson = other.getFrancLegalPerson();
                    if (this$francLegalPerson == null) {
                        if (other$francLegalPerson == null) {
                            break label218;
                        }
                    } else if (this$francLegalPerson.equals(other$francLegalPerson)) {
                        break label218;
                    }

                    return false;
                }

                label211: {
                    Object this$francLegalPersonName = this.getFrancLegalPersonName();
                    Object other$francLegalPersonName = other.getFrancLegalPersonName();
                    if (this$francLegalPersonName == null) {
                        if (other$francLegalPersonName == null) {
                            break label211;
                        }
                    } else if (this$francLegalPersonName.equals(other$francLegalPersonName)) {
                        break label211;
                    }

                    return false;
                }

                Object this$francQua = this.getFrancQua();
                Object other$francQua = other.getFrancQua();
                if (this$francQua == null) {
                    if (other$francQua != null) {
                        return false;
                    }
                } else if (!this$francQua.equals(other$francQua)) {
                    return false;
                }

                Object this$francAddr = this.getFrancAddr();
                Object other$francAddr = other.getFrancAddr();
                if (this$francAddr == null) {
                    if (other$francAddr != null) {
                        return false;
                    }
                } else if (!this$francAddr.equals(other$francAddr)) {
                    return false;
                }

                label190: {
                    Object this$francCreDate = this.getFrancCreDate();
                    Object other$francCreDate = other.getFrancCreDate();
                    if (this$francCreDate == null) {
                        if (other$francCreDate == null) {
                            break label190;
                        }
                    } else if (this$francCreDate.equals(other$francCreDate)) {
                        break label190;
                    }

                    return false;
                }

                label183: {
                    Object this$francCreTime = this.getFrancCreTime();
                    Object other$francCreTime = other.getFrancCreTime();
                    if (this$francCreTime == null) {
                        if (other$francCreTime == null) {
                            break label183;
                        }
                    } else if (this$francCreTime.equals(other$francCreTime)) {
                        break label183;
                    }

                    return false;
                }

                Object this$francCreId = this.getFrancCreId();
                Object other$francCreId = other.getFrancCreId();
                if (this$francCreId == null) {
                    if (other$francCreId != null) {
                        return false;
                    }
                } else if (!this$francCreId.equals(other$francCreId)) {
                    return false;
                }

                label169: {
                    Object this$francModiDate = this.getFrancModiDate();
                    Object other$francModiDate = other.getFrancModiDate();
                    if (this$francModiDate == null) {
                        if (other$francModiDate == null) {
                            break label169;
                        }
                    } else if (this$francModiDate.equals(other$francModiDate)) {
                        break label169;
                    }

                    return false;
                }

                Object this$francModiTime = this.getFrancModiTime();
                Object other$francModiTime = other.getFrancModiTime();
                if (this$francModiTime == null) {
                    if (other$francModiTime != null) {
                        return false;
                    }
                } else if (!this$francModiTime.equals(other$francModiTime)) {
                    return false;
                }

                label155: {
                    Object this$francModiId = this.getFrancModiId();
                    Object other$francModiId = other.getFrancModiId();
                    if (this$francModiId == null) {
                        if (other$francModiId == null) {
                            break label155;
                        }
                    } else if (this$francModiId.equals(other$francModiId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$francLogo = this.getFrancLogo();
                Object other$francLogo = other.getFrancLogo();
                if (this$francLogo == null) {
                    if (other$francLogo != null) {
                        return false;
                    }
                } else if (!this$francLogo.equals(other$francLogo)) {
                    return false;
                }

                Object this$francType1 = this.getFrancType1();
                Object other$francType1 = other.getFrancType1();
                if (this$francType1 == null) {
                    if (other$francType1 != null) {
                        return false;
                    }
                } else if (!this$francType1.equals(other$francType1)) {
                    return false;
                }

                label134: {
                    Object this$francType2 = this.getFrancType2();
                    Object other$francType2 = other.getFrancType2();
                    if (this$francType2 == null) {
                        if (other$francType2 == null) {
                            break label134;
                        }
                    } else if (this$francType2.equals(other$francType2)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$francType3 = this.getFrancType3();
                    Object other$francType3 = other.getFrancType3();
                    if (this$francType3 == null) {
                        if (other$francType3 == null) {
                            break label127;
                        }
                    } else if (this$francType3.equals(other$francType3)) {
                        break label127;
                    }

                    return false;
                }

                Object this$francAss = this.getFrancAss();
                Object other$francAss = other.getFrancAss();
                if (this$francAss == null) {
                    if (other$francAss != null) {
                        return false;
                    }
                } else if (!this$francAss.equals(other$francAss)) {
                    return false;
                }

                Object this$francAssName = this.getFrancAssName();
                Object other$francAssName = other.getFrancAssName();
                if (this$francAssName == null) {
                    if (other$francAssName != null) {
                        return false;
                    }
                } else if (!this$francAssName.equals(other$francAssName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof FranchiseeOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $client = this.getClient();
        result = result * 59 + ($client == null ? 43 : $client.hashCode());
        Object $francName = this.getFrancName();
        result = result * 59 + ($francName == null ? 43 : $francName.hashCode());
        Object $francNo = this.getFrancNo();
        result = result * 59 + ($francNo == null ? 43 : $francNo.hashCode());
        Object $francLegalPerson = this.getFrancLegalPerson();
        result = result * 59 + ($francLegalPerson == null ? 43 : $francLegalPerson.hashCode());
        Object $francLegalPersonName = this.getFrancLegalPersonName();
        result = result * 59 + ($francLegalPersonName == null ? 43 : $francLegalPersonName.hashCode());
        Object $francQua = this.getFrancQua();
        result = result * 59 + ($francQua == null ? 43 : $francQua.hashCode());
        Object $francAddr = this.getFrancAddr();
        result = result * 59 + ($francAddr == null ? 43 : $francAddr.hashCode());
        Object $francCreDate = this.getFrancCreDate();
        result = result * 59 + ($francCreDate == null ? 43 : $francCreDate.hashCode());
        Object $francCreTime = this.getFrancCreTime();
        result = result * 59 + ($francCreTime == null ? 43 : $francCreTime.hashCode());
        Object $francCreId = this.getFrancCreId();
        result = result * 59 + ($francCreId == null ? 43 : $francCreId.hashCode());
        Object $francModiDate = this.getFrancModiDate();
        result = result * 59 + ($francModiDate == null ? 43 : $francModiDate.hashCode());
        Object $francModiTime = this.getFrancModiTime();
        result = result * 59 + ($francModiTime == null ? 43 : $francModiTime.hashCode());
        Object $francModiId = this.getFrancModiId();
        result = result * 59 + ($francModiId == null ? 43 : $francModiId.hashCode());
        Object $francLogo = this.getFrancLogo();
        result = result * 59 + ($francLogo == null ? 43 : $francLogo.hashCode());
        Object $francType1 = this.getFrancType1();
        result = result * 59 + ($francType1 == null ? 43 : $francType1.hashCode());
        Object $francType2 = this.getFrancType2();
        result = result * 59 + ($francType2 == null ? 43 : $francType2.hashCode());
        Object $francType3 = this.getFrancType3();
        result = result * 59 + ($francType3 == null ? 43 : $francType3.hashCode());
        Object $francAss = this.getFrancAss();
        result = result * 59 + ($francAss == null ? 43 : $francAss.hashCode());
        Object $francAssName = this.getFrancAssName();
        result = result * 59 + ($francAssName == null ? 43 : $francAssName.hashCode());
        return result;
    }

    public String toString() {
        return "FranchiseeOutData(client=" + this.getClient() + ", francName=" + this.getFrancName() + ", francNo=" + this.getFrancNo() + ", francLegalPerson=" + this.getFrancLegalPerson() + ", francLegalPersonName=" + this.getFrancLegalPersonName() + ", francQua=" + this.getFrancQua() + ", francAddr=" + this.getFrancAddr() + ", francCreDate=" + this.getFrancCreDate() + ", francCreTime=" + this.getFrancCreTime() + ", francCreId=" + this.getFrancCreId() + ", francModiDate=" + this.getFrancModiDate() + ", francModiTime=" + this.getFrancModiTime() + ", francModiId=" + this.getFrancModiId() + ", francLogo=" + this.getFrancLogo() + ", francType1=" + this.getFrancType1() + ", francType2=" + this.getFrancType2() + ", francType3=" + this.getFrancType3() + ", francAss=" + this.getFrancAss() + ", francAssName=" + this.getFrancAssName() + ")";
    }
}
