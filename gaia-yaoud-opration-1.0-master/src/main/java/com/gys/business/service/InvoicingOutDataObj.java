package com.gys.business.service;

import com.gys.business.service.data.InvoicingOutData;
import lombok.Data;

import java.util.List;

@Data
public class InvoicingOutDataObj {
    private List<InvoicingOutData> resultList;

    private String totalDec;
}
