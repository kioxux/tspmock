//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetShopSortOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private String clientId;
    private String gssgIdSort;
    private String gssgNameSort;
    private String gssgBrIdSort;
    private String gssgBrNameSort;
    private String gssgUpdateEmp;
    private Integer indexSort;

    public GetShopSortOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssgIdSort() {
        return this.gssgIdSort;
    }

    public String getGssgNameSort() {
        return this.gssgNameSort;
    }

    public String getGssgBrIdSort() {
        return this.gssgBrIdSort;
    }

    public String getGssgBrNameSort() {
        return this.gssgBrNameSort;
    }

    public String getGssgUpdateEmp() {
        return this.gssgUpdateEmp;
    }

    public Integer getIndexSort() {
        return this.indexSort;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssgIdSort(final String gssgIdSort) {
        this.gssgIdSort = gssgIdSort;
    }

    public void setGssgNameSort(final String gssgNameSort) {
        this.gssgNameSort = gssgNameSort;
    }

    public void setGssgBrIdSort(final String gssgBrIdSort) {
        this.gssgBrIdSort = gssgBrIdSort;
    }

    public void setGssgBrNameSort(final String gssgBrNameSort) {
        this.gssgBrNameSort = gssgBrNameSort;
    }

    public void setGssgUpdateEmp(final String gssgUpdateEmp) {
        this.gssgUpdateEmp = gssgUpdateEmp;
    }

    public void setIndexSort(final Integer indexSort) {
        this.indexSort = indexSort;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetShopSortOutData)) {
            return false;
        } else {
            GetShopSortOutData other = (GetShopSortOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gssgIdSort = this.getGssgIdSort();
                Object other$gssgIdSort = other.getGssgIdSort();
                if (this$gssgIdSort == null) {
                    if (other$gssgIdSort != null) {
                        return false;
                    }
                } else if (!this$gssgIdSort.equals(other$gssgIdSort)) {
                    return false;
                }

                Object this$gssgNameSort = this.getGssgNameSort();
                Object other$gssgNameSort = other.getGssgNameSort();
                if (this$gssgNameSort == null) {
                    if (other$gssgNameSort != null) {
                        return false;
                    }
                } else if (!this$gssgNameSort.equals(other$gssgNameSort)) {
                    return false;
                }

                label74: {
                    Object this$gssgBrIdSort = this.getGssgBrIdSort();
                    Object other$gssgBrIdSort = other.getGssgBrIdSort();
                    if (this$gssgBrIdSort == null) {
                        if (other$gssgBrIdSort == null) {
                            break label74;
                        }
                    } else if (this$gssgBrIdSort.equals(other$gssgBrIdSort)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gssgBrNameSort = this.getGssgBrNameSort();
                    Object other$gssgBrNameSort = other.getGssgBrNameSort();
                    if (this$gssgBrNameSort == null) {
                        if (other$gssgBrNameSort == null) {
                            break label67;
                        }
                    } else if (this$gssgBrNameSort.equals(other$gssgBrNameSort)) {
                        break label67;
                    }

                    return false;
                }

                Object this$gssgUpdateEmp = this.getGssgUpdateEmp();
                Object other$gssgUpdateEmp = other.getGssgUpdateEmp();
                if (this$gssgUpdateEmp == null) {
                    if (other$gssgUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gssgUpdateEmp.equals(other$gssgUpdateEmp)) {
                    return false;
                }

                Object this$indexSort = this.getIndexSort();
                Object other$indexSort = other.getIndexSort();
                if (this$indexSort == null) {
                    if (other$indexSort != null) {
                        return false;
                    }
                } else if (!this$indexSort.equals(other$indexSort)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetShopSortOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssgIdSort = this.getGssgIdSort();
        result = result * 59 + ($gssgIdSort == null ? 43 : $gssgIdSort.hashCode());
        Object $gssgNameSort = this.getGssgNameSort();
        result = result * 59 + ($gssgNameSort == null ? 43 : $gssgNameSort.hashCode());
        Object $gssgBrIdSort = this.getGssgBrIdSort();
        result = result * 59 + ($gssgBrIdSort == null ? 43 : $gssgBrIdSort.hashCode());
        Object $gssgBrNameSort = this.getGssgBrNameSort();
        result = result * 59 + ($gssgBrNameSort == null ? 43 : $gssgBrNameSort.hashCode());
        Object $gssgUpdateEmp = this.getGssgUpdateEmp();
        result = result * 59 + ($gssgUpdateEmp == null ? 43 : $gssgUpdateEmp.hashCode());
        Object $indexSort = this.getIndexSort();
        result = result * 59 + ($indexSort == null ? 43 : $indexSort.hashCode());
        return result;
    }

    public String toString() {
        return "GetShopSortOutData(clientId=" + this.getClientId() + ", gssgIdSort=" + this.getGssgIdSort() + ", gssgNameSort=" + this.getGssgNameSort() + ", gssgBrIdSort=" + this.getGssgBrIdSort() + ", gssgBrNameSort=" + this.getGssgBrNameSort() + ", gssgUpdateEmp=" + this.getGssgUpdateEmp() + ", indexSort=" + this.getIndexSort() + ")";
    }
}
