package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBill;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillDetail;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillRelation;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillInData;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillRelationInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 重点商品任务—主表(GaiaKeyCommodityTaskBill)表服务接口
 *
 * @author makejava
 * @since 2021-09-01 15:37:55
 */
public interface GaiaKeyCommodityTaskBillService {

    Object insertBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData param);

    PageInfo<GaiaKeyCommodityTaskBill> listBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    boolean deleteBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBill inData);

    boolean checkBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBill inData);

    Object copyBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    Object getBillInfoById(GetLoginOutData userInfo, GaiaKeyCommodityTaskBill inData);

    GaiaKeyCommodityTaskBillRelation getGoodsInfoById(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    boolean saveGoodsInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData);

    Object getGoodsTaskById(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData);

    Object saveStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData);

    Object updateStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillDetail inData);

    Object deleteBatchStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    Object deleteBatchGoodsInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    List<GaiaKeyCommodityTaskBillRelation> listGoodsInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    Object listStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);

    Object batchImportGoods(MultipartFile file, GetLoginOutData userInfo, String billCode);

    Object batchImportStores(MultipartFile file, GetLoginOutData userInfo, String billCode, String proSelfCode);

    Object assessStoreLevel(GetLoginOutData userInfo);

    Object getImportTaskHBTime(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData);
}
