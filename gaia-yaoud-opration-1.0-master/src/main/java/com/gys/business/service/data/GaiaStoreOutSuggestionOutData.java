package com.gys.business.service.data;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionRelation;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionD)实体类
 *
 * @author makejava
 * @since 2021-10-28 10:53:43
 */
@Data
public class GaiaStoreOutSuggestionOutData {
    private Long id;
    private String client;
    private String stoCode;
    private String billCode;
    private Integer status;
    private String type;
    private List<GaiaStoreOutSuggestionRelationOutData> storeList;
    private List<GaiaStoreOutSuggestionDOutData> detailList;
    private List<JSONObject> confirmDetailList;
}

