package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.vo.GaiaProductBusinessZdysVo;

import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface ElectronicCouponsService {
    /**
     * 列表查询
     *
     * @param inData
     * @return
     */
    List<ElectronBasicOutData> getElectronThemeSetByList(ElectronBasicInData inData);

    /**
     * 电子券新增
     *
     * @param inVo
     */
    void insertToElectronThemeSet(ElectronThemeSetInVo inVo);

    /**
     * 获得电子券号
     *
     * @param client
     * @return
     */
    String getGsebIdToElectronThemeSet(String client);

    /**
     * 修改电子券
     *
     * @param inVo
     */
    void updateToElectronThemeSet(ElectronThemeSetInVo inVo);


    /**
     * 商品组条件模糊查询等
     *
     * @param inData
     * @return
     */
    List<FuzzyQueryOfProductGroupOutData> fuzzyQueryOfProductGroup(FuzzyQueryOfProductGroupInData inData);



    /**
     * 用券/送券 详细查询
     *
     * @param inData
     * @return
     */
    ElectronicDetailedViewOutData detailedView(ElectronicDetailedViewInData inData);

    /**
     * 查询当前会员可用电子券
     *
     * @param inData 会员信息
     * @return
     */
    List<ElectronDetailOutData> queryCanUseElectron(GaiaSdMemberCardData inData);

    /**
     * 查询当前会员可赠送电子券
     *
     * @param inData 门店信息
     * @return
     */
    List<ElectronDetailOutData> selectGiftCoupons(GaiaSdStoreData inData);

    /**
     * 查询当前会员可赠送电子券
     *
     * @param inData 门店信息
     * @return
     */
    List<ElectronDetailOutData> selectGiftCouponsAndUse(GaiaSdStoreData inData);

    /**
     * 查询商品,品牌分组
     *
     * @param client
     * @return
     */
    Classification getclassification(String client);

    /**
     * 商品编码导入
     * @param proCode
     * @return
     */
    List<FuzzyQueryOfProductGroupOutData> importExcel(List<String> proCode,String client,String brId);


    /**
     * 新增电子券所有信息
     * @param inVo
     */
    String insertAll(ElectronicAllData inVo);


    /**
     * 电子券报表查询
     * @param inVo
     * @return
     */
    List<ElectronicSubjectQueryOutData> subjectQuery(ElectronicSubjectQueryInData inVo);

    /**
     * 电子券审核
     * @param inVo
     */
    void review(ElectronicReviewInData inVo);

    /**
     * 添加之前 做查询
     * @param inVo
     * @return
     */
    Map<String, Object> timeJudgment(ElectronicAllData inVo);

    List<ElectronBasicOutData> listUnusedElectronBasic(ElectronBasicInData inData);

    GaiaProductBusinessZdysVo QueryOfProductGroupZdys(GetLoginOutData userInfo);

    /**
     * 主题状态修改
     * @param inData
     */
    void queryStatus(ElectronicStatusInData inData);
}
