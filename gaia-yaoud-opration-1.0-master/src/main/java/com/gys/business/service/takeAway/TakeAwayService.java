package com.gys.business.service.takeAway;

import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.mapper.entity.GaiaTOrderInfo;
import com.gys.business.service.data.*;
import com.gys.business.service.data.takeaway.FxMainSaleSetBean;
import com.gys.business.service.data.takeaway.FxSaleQueryBean;
import com.gys.business.service.data.takeaway.FxYlOrderInputBean;
import com.gys.business.service.data.takeaway.WebOrderQueryBean;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.WeideProIdDto;
import com.gys.common.data.yaolian.common.YlResponseData;
import com.gys.util.takeAway.ddky.DdkyRefundBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface TakeAwayService {

//    GaiaTAccount getPlatformAccount(String platform, String appId, String shopId);

    GaiaTAccount getEbPlatformAccount(String platform, String appId, String shopId);

    GaiaTAccount getMtPlatformAccount(String platform, String appId, String shopId);

    GaiaTAccount getPlatformAccountByStoCode(String platform, String appId, String stoCode, String client);

    GaiaTOrderInfo selectStoCodeByPlatformsAndPlatformsOrderId(String platform, String platformsOrderId);

    String getJdSecret(String appKey);

    List<GaiaTAccount> getAllAccountByAppId(String platform, String appId);

    GaiaTAccount selectStoCodeByPlatformsAndPublicCodeAndAppId(String platforms, String publicCode, String appId);

    GaiaTAccount getAccountByClientAndStoCodeAndPlat(String client, String stoCode, String platform);

    void execOrdersPush(Map<String,Object> order_info, String platform,String source,String secret, GaiaTAccount account);

    List<TakeAwayOrderOutData> getOrderList(GetTakeAwayInData inData);

    JsonResult returnOrder(TakeAwayOrderBean bean, String client, String StoCode);

    JsonResult log(TakeAwayOrderBean bean, String client, String StoCode);

    void pushToStoreMq(String client, String stoCode, String type, String cmd, Map<String, Object> inData);

    JsonResult sendAction(String orderNo, String client, String StoCode);

    JsonResult syncStock(String client, String depId);

    String getAccessToken(GaiaTAccount account);

    JsonResult syncPayTime(OrderPayTimeBean bean);

    JsonResult getPayTime(OrderPayTimeBean bean);

    JsonResult setMainSale(FxMainSaleSetBean bean);

    JsonResult getMainSale(FxMainSaleSetBean bean);

    JsonResult getByProdIdAndOrderNo(OrderDetailBean bean);

    JsonResult orderQuery(WebOrderQueryBean bean);

    void orderQueryOutput(WebOrderQueryBean bean, HttpServletRequest request, HttpServletResponse response);

    JsonResult orderDetailQuery(WebOrderQueryBean bean);

    void orderDetailQueryOutput(WebOrderQueryBean bean, HttpServletRequest request, HttpServletResponse response);

    String getJdToken(String venderId);

    void updateJdToken(String venderId, String token);

    GaiaTAccount getAccountByVenderId(String venderId);

    String getAppSecretByToken(String token);

    String getStoreLogoByClientAndBrId(String client, String stoCode);

    List<GaiaTAccount> getAllAccountByClientAndStoCode(String platform, String client, String stoCode);

    JsonResult getSaleByOrderId(FxSaleQueryBean bean);

    YlResponseData queryStoreByYaolian(String client);


    JsonResult ylOrderInput(FxYlOrderInputBean bean);
    /**
     * 查询商品库存同步至药联
     * @param client 加盟商
     * @param brIds 门店列表
     * @param proIds 商品列表
     * @return
     */
    boolean syncStockToYaolian(String client,List<String> brIds,List<String> proIds, boolean syncStock);

    /**
     * 商品 录入
     * @param map
     * @return
     */
    YlResponseData commodityDataEntry(Map<String, Object> map);

    boolean getTipPopByClientAndBrId(String client, String depId);

    String getMainSaleBtn(String client, String depId);

    JsonResult ddkyReturnOrder(GetLoginOutData userInfo, DdkyRefundBean bean);

    String goodsSync(String client, String stoCode, String proId);

    List<WeideProIdDto> getWeideEbProId(String client, String stoCode, boolean isPrescibe, String orderId);

    List<WeideProIdDto> getWeideMtProId(String client, String stoCode, boolean isPrescibe, String orderId);
}
