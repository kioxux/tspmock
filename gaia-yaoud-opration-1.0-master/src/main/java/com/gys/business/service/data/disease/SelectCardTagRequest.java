package com.gys.business.service.data.disease;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectCardTagRequest {

    @ApiModelProperty(value = "加盟商")
    private String client;


    @ApiModelProperty(value = "会员卡号")
    private String memberCard;
}
