//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PromHyrDiscountOutData {
    private String clientId;
    private String gsppVoucherId;
    private String gsppSerial;
    private String gsppBeginDate;
    private String gsppEndDate;
    private String gsppBeginTime;
    private String gsppEndTime;
    private String gsppDateFrequency;
    private String gsppTimeFrequency;
    private String gsppRebate;
    private String gsppInteFlag;
    private String gsppInteRate;

    public PromHyrDiscountOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsppVoucherId() {
        return this.gsppVoucherId;
    }

    public String getGsppSerial() {
        return this.gsppSerial;
    }

    public String getGsppBeginDate() {
        return this.gsppBeginDate;
    }

    public String getGsppEndDate() {
        return this.gsppEndDate;
    }

    public String getGsppBeginTime() {
        return this.gsppBeginTime;
    }

    public String getGsppEndTime() {
        return this.gsppEndTime;
    }

    public String getGsppDateFrequency() {
        return this.gsppDateFrequency;
    }

    public String getGsppTimeFrequency() {
        return this.gsppTimeFrequency;
    }

    public String getGsppRebate() {
        return this.gsppRebate;
    }

    public String getGsppInteFlag() {
        return this.gsppInteFlag;
    }

    public String getGsppInteRate() {
        return this.gsppInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsppVoucherId(final String gsppVoucherId) {
        this.gsppVoucherId = gsppVoucherId;
    }

    public void setGsppSerial(final String gsppSerial) {
        this.gsppSerial = gsppSerial;
    }

    public void setGsppBeginDate(final String gsppBeginDate) {
        this.gsppBeginDate = gsppBeginDate;
    }

    public void setGsppEndDate(final String gsppEndDate) {
        this.gsppEndDate = gsppEndDate;
    }

    public void setGsppBeginTime(final String gsppBeginTime) {
        this.gsppBeginTime = gsppBeginTime;
    }

    public void setGsppEndTime(final String gsppEndTime) {
        this.gsppEndTime = gsppEndTime;
    }

    public void setGsppDateFrequency(final String gsppDateFrequency) {
        this.gsppDateFrequency = gsppDateFrequency;
    }

    public void setGsppTimeFrequency(final String gsppTimeFrequency) {
        this.gsppTimeFrequency = gsppTimeFrequency;
    }

    public void setGsppRebate(final String gsppRebate) {
        this.gsppRebate = gsppRebate;
    }

    public void setGsppInteFlag(final String gsppInteFlag) {
        this.gsppInteFlag = gsppInteFlag;
    }

    public void setGsppInteRate(final String gsppInteRate) {
        this.gsppInteRate = gsppInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromHyrDiscountOutData)) {
            return false;
        } else {
            PromHyrDiscountOutData other = (PromHyrDiscountOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gsppVoucherId = this.getGsppVoucherId();
                Object other$gsppVoucherId = other.getGsppVoucherId();
                if (this$gsppVoucherId == null) {
                    if (other$gsppVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsppVoucherId.equals(other$gsppVoucherId)) {
                    return false;
                }

                Object this$gsppSerial = this.getGsppSerial();
                Object other$gsppSerial = other.getGsppSerial();
                if (this$gsppSerial == null) {
                    if (other$gsppSerial != null) {
                        return false;
                    }
                } else if (!this$gsppSerial.equals(other$gsppSerial)) {
                    return false;
                }

                label134: {
                    Object this$gsppBeginDate = this.getGsppBeginDate();
                    Object other$gsppBeginDate = other.getGsppBeginDate();
                    if (this$gsppBeginDate == null) {
                        if (other$gsppBeginDate == null) {
                            break label134;
                        }
                    } else if (this$gsppBeginDate.equals(other$gsppBeginDate)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gsppEndDate = this.getGsppEndDate();
                    Object other$gsppEndDate = other.getGsppEndDate();
                    if (this$gsppEndDate == null) {
                        if (other$gsppEndDate == null) {
                            break label127;
                        }
                    } else if (this$gsppEndDate.equals(other$gsppEndDate)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$gsppBeginTime = this.getGsppBeginTime();
                    Object other$gsppBeginTime = other.getGsppBeginTime();
                    if (this$gsppBeginTime == null) {
                        if (other$gsppBeginTime == null) {
                            break label120;
                        }
                    } else if (this$gsppBeginTime.equals(other$gsppBeginTime)) {
                        break label120;
                    }

                    return false;
                }

                Object this$gsppEndTime = this.getGsppEndTime();
                Object other$gsppEndTime = other.getGsppEndTime();
                if (this$gsppEndTime == null) {
                    if (other$gsppEndTime != null) {
                        return false;
                    }
                } else if (!this$gsppEndTime.equals(other$gsppEndTime)) {
                    return false;
                }

                label106: {
                    Object this$gsppDateFrequency = this.getGsppDateFrequency();
                    Object other$gsppDateFrequency = other.getGsppDateFrequency();
                    if (this$gsppDateFrequency == null) {
                        if (other$gsppDateFrequency == null) {
                            break label106;
                        }
                    } else if (this$gsppDateFrequency.equals(other$gsppDateFrequency)) {
                        break label106;
                    }

                    return false;
                }

                Object this$gsppTimeFrequency = this.getGsppTimeFrequency();
                Object other$gsppTimeFrequency = other.getGsppTimeFrequency();
                if (this$gsppTimeFrequency == null) {
                    if (other$gsppTimeFrequency != null) {
                        return false;
                    }
                } else if (!this$gsppTimeFrequency.equals(other$gsppTimeFrequency)) {
                    return false;
                }

                label92: {
                    Object this$gsppRebate = this.getGsppRebate();
                    Object other$gsppRebate = other.getGsppRebate();
                    if (this$gsppRebate == null) {
                        if (other$gsppRebate == null) {
                            break label92;
                        }
                    } else if (this$gsppRebate.equals(other$gsppRebate)) {
                        break label92;
                    }

                    return false;
                }

                Object this$gsppInteFlag = this.getGsppInteFlag();
                Object other$gsppInteFlag = other.getGsppInteFlag();
                if (this$gsppInteFlag == null) {
                    if (other$gsppInteFlag != null) {
                        return false;
                    }
                } else if (!this$gsppInteFlag.equals(other$gsppInteFlag)) {
                    return false;
                }

                Object this$gsppInteRate = this.getGsppInteRate();
                Object other$gsppInteRate = other.getGsppInteRate();
                if (this$gsppInteRate == null) {
                    if (other$gsppInteRate != null) {
                        return false;
                    }
                } else if (!this$gsppInteRate.equals(other$gsppInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromHyrDiscountOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsppVoucherId = this.getGsppVoucherId();
        result = result * 59 + ($gsppVoucherId == null ? 43 : $gsppVoucherId.hashCode());
        Object $gsppSerial = this.getGsppSerial();
        result = result * 59 + ($gsppSerial == null ? 43 : $gsppSerial.hashCode());
        Object $gsppBeginDate = this.getGsppBeginDate();
        result = result * 59 + ($gsppBeginDate == null ? 43 : $gsppBeginDate.hashCode());
        Object $gsppEndDate = this.getGsppEndDate();
        result = result * 59 + ($gsppEndDate == null ? 43 : $gsppEndDate.hashCode());
        Object $gsppBeginTime = this.getGsppBeginTime();
        result = result * 59 + ($gsppBeginTime == null ? 43 : $gsppBeginTime.hashCode());
        Object $gsppEndTime = this.getGsppEndTime();
        result = result * 59 + ($gsppEndTime == null ? 43 : $gsppEndTime.hashCode());
        Object $gsppDateFrequency = this.getGsppDateFrequency();
        result = result * 59 + ($gsppDateFrequency == null ? 43 : $gsppDateFrequency.hashCode());
        Object $gsppTimeFrequency = this.getGsppTimeFrequency();
        result = result * 59 + ($gsppTimeFrequency == null ? 43 : $gsppTimeFrequency.hashCode());
        Object $gsppRebate = this.getGsppRebate();
        result = result * 59 + ($gsppRebate == null ? 43 : $gsppRebate.hashCode());
        Object $gsppInteFlag = this.getGsppInteFlag();
        result = result * 59 + ($gsppInteFlag == null ? 43 : $gsppInteFlag.hashCode());
        Object $gsppInteRate = this.getGsppInteRate();
        result = result * 59 + ($gsppInteRate == null ? 43 : $gsppInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromHyrDiscountOutData(clientId=" + this.getClientId() + ", gsppVoucherId=" + this.getGsppVoucherId() + ", gsppSerial=" + this.getGsppSerial() + ", gsppBeginDate=" + this.getGsppBeginDate() + ", gsppEndDate=" + this.getGsppEndDate() + ", gsppBeginTime=" + this.getGsppBeginTime() + ", gsppEndTime=" + this.getGsppEndTime() + ", gsppDateFrequency=" + this.getGsppDateFrequency() + ", gsppTimeFrequency=" + this.getGsppTimeFrequency() + ", gsppRebate=" + this.getGsppRebate() + ", gsppInteFlag=" + this.getGsppInteFlag() + ", gsppInteRate=" + this.getGsppInteRate() + ")";
    }
}
