//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdMemberBasicMapper;
import com.gys.business.service.MemberCardReportService;
import com.gys.business.service.data.GetMemberCardReportInData;
import com.gys.business.service.data.GetMemberCardReportOutData;
import com.gys.business.service.data.MemberCardAddOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MemberCardReportServiceImpl implements MemberCardReportService {
    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;

    public MemberCardReportServiceImpl() {
    }

    public Map<String, Object> queryReport(GetMemberCardReportInData inData) {
        if(ObjectUtil.isEmpty(inData.getBrIdList()) || inData.getBrIdList().length< 0) {
            throw new BusinessException("请输入门店");
        }

        Map<String, Object> result = new HashMap<>(16);
        List<GetMemberCardReportOutData> outDataList = this.gaiaSdMemberBasicMapper.queryReport(inData);
        MemberCardAddOutData memberCardAddOutData = new MemberCardAddOutData();
        if(ValidateUtil.isEmpty(outDataList)) {
            return result;
        }

        //利用Set特性 去重汇总计算门店数量
        Set<String> stoCountSet = new HashSet<>(10);
        //利用Set特性 去重汇总计算办卡员工数量
        Set<String> inputCardNameSet = new HashSet<>(10);
        //汇总新增会员数量
        int totalAddMemberNum = 0;
        for(int i = 0; i < outDataList.size(); ++i) {
            GetMemberCardReportOutData memberCard = outDataList.get(i);
            memberCard.setIndex(String.valueOf(i + 1));

            //门店名
            String brName = memberCard.getBrName();
            if(ValidateUtil.isNotEmpty(brName)) {
                stoCountSet.add(brName);
            }
            //办卡员工姓名
            String userName = memberCard.getUserName();
            if(ValidateUtil.isNotEmpty(userName)) {
                inputCardNameSet.add(userName);
            }
            //新增会员数量
            String addNum = memberCard.getAddNum();
            if(ValidateUtil.isNotEmpty(addNum)) {
                totalAddMemberNum = totalAddMemberNum + Integer.valueOf(addNum).intValue();
            }
        }
        memberCardAddOutData.setBrName(stoCountSet.size());
        memberCardAddOutData.setUserName(inputCardNameSet.size());
        memberCardAddOutData.setAddNum(totalAddMemberNum);
        result.put("total", memberCardAddOutData);
        result.put("cardList", outDataList);
        return result;
    }
}
