package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 商品组传参
 *
 * @author xiaoyuan on 2020/10/6
 */
@Data
public class ProductGroupDetailsViewInData implements Serializable {
    private static final long serialVersionUID = -214456347892882879L;

    private String client;

    @NotBlank(message = "商品组id不可为空!")
    @ApiModelProperty(value = "商品组id")
    private String gspgId;
}
