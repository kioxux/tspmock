
package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GetSaleReturnDetailInData implements Serializable {
    private static final long serialVersionUID = -7167949706571048654L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    private String gssdBillNo;
    private String gssdBrId;
    private String gssdDate;
    private String gssdSerial;
    private String gssdProId;
    private String gssdBatchNo;
    private String gssdBatch;
    private String gssdValidDate;
    private String gssdStCode;
    private String gssdPrc1;
    private String gssdPrc2;
    private String gssdQty;
    private String gssdReturnQty;
    private String gssdAmt;
    private String gssdIntegralExchangeSingle;
    private String gssdIntegralExchangeCollect;
    private String gssdIntegralExchangeAmtSingle;
    private String gssdIntegralExchangeAmtCollect;
    private String gssdZkAmt;
    private String gssdZkJfdh;
    private String gssdZkJfdx;
    private String gssdZkDzq;
    private String gssdZkDyq;
    private String gssdZkPm;
    private String gssdSalerId;
    private String gssdDoctorId;
    private String gssdPmSubjectId;
    private String gssdPmId;
    private String gssdPmContent;
    private String gssdRecipelFlag;
    private String gssdRelationFlag;
    private String gssdSpecialmedIdcard;
    private String gssdSpecialmedName;
    private String gssdSpecialmedSex;
    private String gssdSpecialmedBirthday;
    private String gssdSpecialmedMobile;
    private String gssdSpecialmedAddress;
    private String proName;
    private String proFactoryName;
    private String proPlace;
    private String proForm;
    private String proPrice;
    private String proUnit;
    private String proSpecs;
    private String proRegisterNo;
}
