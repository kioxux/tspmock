package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GaiaSdDefecProDInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "商品编码")
    private String gsddProId;
    @ApiModelProperty(value = "商品名称")
    private String gsddProName;
    @ApiModelProperty(value = "通用名")
    private String gsddProCommonName;
    @ApiModelProperty(value = "规格")
    private String gsddProSpecs;
    @ApiModelProperty(value = "厂家")
    private String gsddFactoryName;
    @ApiModelProperty(value = "单位")
    private String gsddProUnit;
    @ApiModelProperty(value = "有效期")
    private String gsddVaildDate;
    @ApiModelProperty(value = "批次")
    private String gsddBatch;
    @ApiModelProperty(value = "批号")
    private String gsddBatchNo;
    @ApiModelProperty(value = "现有库存")
    private String stockQty;
    @ApiModelProperty(value = "不合格数量")
    private String gsddQty;
    @ApiModelProperty(value = "不合格原因")
    private String gsddRemaks;
    @ApiModelProperty(value = "门店编码")
    private String brId;
    @ApiModelProperty(value = "单号")
    private String gsddVoucherId;
    @ApiModelProperty(value = "行号")
    private String gsddSerial;
    @ApiModelProperty(value = "时间")
    private String gsddDate;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "产地")
    private String proPlace;
}
