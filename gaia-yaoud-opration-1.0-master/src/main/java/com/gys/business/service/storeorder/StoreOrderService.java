package com.gys.business.service.storeorder;

import com.gys.common.data.JsonResult;
import com.gys.common.data.storeorder.StoreOrderDTO;

import javax.servlet.http.HttpServletResponse;

/**
 * @author wu mao yin
 * @Title: 门店请货数据查询
 * @date 2021/12/315:11
 */
public interface StoreOrderService {

    /**
     * 查询连锁公司列表
     *
     * @param client client
     * @return JsonResult
     */
    JsonResult selectChainCompList(String client);

    /**
     * 根据连锁公司查询门店
     *
     * @param client      client
     * @param chainCompId chainCompId
     * @return JsonResult
     */
    JsonResult selectStoListByChainComp(String client, String chainCompId);

    /**
     * 查询门店明细清单
     *
     * @param storeOrderDTO storeOrderDTO
     * @return JsonResult
     */
    JsonResult selectStoreDetailedList(StoreOrderDTO storeOrderDTO);

    /**
     * 请货数据查询：门店明细导出
     */
    void exportExcel(StoreOrderDTO storeOrderDTO, HttpServletResponse response);

    /**
     * 请货数据查询：商品汇总导出
     */
    void exportProSummaryList(StoreOrderDTO storeOrderDTO, HttpServletResponse response);

    /**
     * 查询商品汇总清单
     *
     * @param storeOrderDTO storeOrderDTO
     * @return JsonResult
     */
    JsonResult selectProSummaryList(StoreOrderDTO storeOrderDTO);

}
