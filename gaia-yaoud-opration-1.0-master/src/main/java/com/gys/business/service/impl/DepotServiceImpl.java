//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.DepotService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.feign.AuthService;
import com.gys.feign.PurchaseService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

@Service
public class DepotServiceImpl implements DepotService {
    @Autowired
    private GaiaSdReturnDepotHMapper returnDepotHMapper;
    @Autowired
    private GaiaSdReturnDepotDMapper returnDepotDMapper;
    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;
    @Autowired
    private GaiaSdStockBatchMapper stockBatchMapper;
    @Autowired
    private GaiaBatchStockMapper batchStockMapper;
    @Autowired
    private GaiaSdAcceptDMapper acceptDMapper;
    @Autowired
    private GaiaSdAcceptHMapper acceptHMapper;
    @Autowired
    private GaiaPoHeaderMapper poHeaderMapper;
    @Autowired
    private GaiaPoItemMapper poItemMapper;
    @Autowired
    private AuthService authService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private GaiaSdBatchChangeMapper sdBatchChangeMapper;
    @Autowired
    private  GaiaMaterialAssessMapper materialAssessMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Value("${thirdServer}")
    private String thirdServer;

    public DepotServiceImpl() {
    }

    public PageInfo<GetDepotOutData> selectList(GetDepotInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        List<GetDepotOutData> outData = this.returnDepotHMapper.selectList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            if ("2".equals(inData.getGsrdhType())) {
                for(GetDepotOutData getDepotOutData:outData){
                    GetDepotInData depotInData = new GetDepotInData();
                    depotInData.setClientId(inData.getClientId());
                    depotInData.setStoreCode(inData.getStoreCode());
                    depotInData.setGsrdhVoucherId(getDepotOutData.getGsrdhVoucherId());
                    List<GetDepotDetailOutData> depotOutData = this.returnDepotDMapper.detailList(depotInData);
                    getDepotOutData.setDepotDetailOutDataList(depotOutData);
                }
            }
                pageInfo = new PageInfo(outData);
        } else {
                pageInfo = new PageInfo();
        }

        return pageInfo;
    }


    public GetDepotOutData detail(GetDepotInData inData) {
        GetDepotOutData outData = new GetDepotOutData();
        Example example = new Example(GaiaSdReturnDepotH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrdhVoucherId", inData.getGsrdhVoucherId());
        GaiaSdReturnDepotH depotH = (GaiaSdReturnDepotH)this.returnDepotHMapper.selectOneByExample(example);
        outData.setGsrdhVoucherId(depotH.getGsrdhVoucherId());
        Example exampleUser = new Example(GaiaUserData.class);
        exampleUser.createCriteria().andEqualTo("client", depotH.getClientId()).andEqualTo("userId", depotH.getGsrdhEmp());
        GaiaUserData userData = (GaiaUserData)this.userDataMapper.selectOneByExample(exampleUser);
        outData.setGsrdhEmp(userData.getUserNam());
        outData.setGsrdhStatus(depotH.getGsrdhStatus());
        outData.setGsrdhRemaks(depotH.getGsrdhRemaks());
        outData.setGsrdhFrom(depotH.getGsrdhFrom());
        List<GetDepotDetailOutData> detailList = this.returnDepotDMapper.detailList(inData);
        outData.setDepotDetailOutDataList(detailList);
        return outData;
    }

    public List<GetDepotDetailOutData> detailList(GetDepotInData inData) {
        List<GetDepotDetailOutData> outData = this.returnDepotDMapper.detailList(inData);
        if ("2".equals(inData.getGsrdhType())) {
            Iterator var3 = outData.iterator();
            while(var3.hasNext()) {
                GetDepotDetailOutData detail = (GetDepotDetailOutData)var3.next();
                Example example = new Example(GaiaSdAcceptD.class);
                example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadBatch", detail.getBatch()).andEqualTo("gsadProId", detail.getProCode()).andEqualTo("gsadBatchNo", detail.getBatchNo());
                List<GaiaSdAcceptD> list = this.acceptDMapper.selectByExample(example);
                if (ObjectUtil.isEmpty(list)) {
                    throw new BusinessException("该批次商品未匹配到采购单！");
                }

                GaiaSdAcceptD acceptD = (GaiaSdAcceptD)list.get(0);
                Example exampleH = new Example(GaiaSdAcceptH.class);
                exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", acceptD.getGsadVoucherId());
                GaiaSdAcceptH acceptH = (GaiaSdAcceptH)this.acceptHMapper.selectOneByExample(exampleH);
                Example examplePo = new Example(GaiaPoItem.class);
                examplePo.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", acceptH.getGsahPoid()).andEqualTo("poLineNo", acceptD.getGsadSerial()).andEqualTo("poProCode", acceptD.getGsadProId());
                GaiaPoItem poItem = (GaiaPoItem)this.poItemMapper.selectOneByExample(examplePo);
                detail.setPoId(poItem.getPoId());
                detail.setPoLineNo(poItem.getPoLineNo());
                detail.setPoPrice(poItem.getPoPrice().toString());
                detail.setPoRate(poItem.getPoRate());
            }
        }

        return outData;
    }

    @Transactional
    public String insert(GetDepotInData inData, GetLoginOutData userInfo) {
        GaiaSdReturnDepotH depotH = new GaiaSdReturnDepotH();
        depotH.setClientId(inData.getClientId());
        String voucherId = "";
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;
        String poId;
        int serial = 1;
        depotH.setGsrdhBrId(inData.getStoreCode());
        depotH.setGsrdhStatus("1");
        depotH.setGsrdhType(inData.getGsrdhType());
        depotH.setGsrdhRemaks(inData.getGsrdhRemaks());
        depotH.setGsrdhFrom(inData.getStoreCode());
        depotH.setGsrdhEmp(userInfo.getUserId());
        depotH.setGsrdhDate(DateUtil.format(new Date(), "yyyyMMdd"));
        depotH.setGsrdhPattern("1");
        if ("1".equals(inData.getGsrdhType())) {     //退库（仓库）
            //判断 如果门店配送类型为委托配送  退库单的type值为3
            GaiaStoreData storeData= new GaiaStoreData();
            storeData.setClient(inData.getClientId());
            storeData.setStoCode(inData.getStoreCode());
            storeData = storeDataMapper.selectByPrimaryKey(storeData);
            if(storeData.getStoDeliveryMode()!= null && storeData.getStoDeliveryMode().equals("3")){
                depotH.setGsrdhType("3");
            }
            voucherId = this.returnDepotHMapper.selectNextVoucherId(inData.getClientId());
            depotH.setGsrdhVoucherId(voucherId);
            depotH.setGsrdhTo("配送中心");
            depotH.setGsrdhProcedure("0");
            GetWfCreateInData wfCreateInData = new GetWfCreateInData();
            wfCreateInData.setToken(inData.getToken());
            wfCreateInData.setWfOrder(voucherId);
            wfCreateInData.setWfDefineCode("GAIA_WF_005");
            wfCreateInData.setWfTitle("退库工作流");
            wfCreateInData.setWfDescription("退库工作流");
            wfCreateInData.setWfSite(inData.getStoreCode());

            List<GetWfCreateReturnCompadmInData> details = new ArrayList();

            GetWfCreateReturnCompadmInData compadmInData;
            for(Iterator var10 = inData.getDepotDetailInDataList().iterator(); var10.hasNext(); details.add(compadmInData)) {
                GetDepotDetailInData poItem = (GetDepotDetailInData)var10.next();
                GaiaSdReturnDepotD depotD = new GaiaSdReturnDepotD();
                depotD.setClientId(inData.getClientId());
                depotD.setGsrddBrId(inData.getGsrdhBrId());
                depotD.setGsrddVoucherId(voucherId);
                depotD.setGsrddBrId(inData.getStoreCode());
                depotD.setGsrddProId(poItem.getProCode());
                depotD.setGsrddSerial(String.valueOf(serial++));
                depotD.setGsrddDate(DateUtil.format(new Date(), "yyyyMMdd"));
                depotD.setGsrddRetdepCause(poItem.getGsrddRetdepCause());
                depotD.setGsrddRetdepQty(poItem.getGsrddRetdepQty());
                depotD.setGsrddReviseQty("0");
                depotD.setGsrddStartQty(poItem.getGsrddStartQty());
                depotD.setGsrddStockQty(poItem.getStockQty());
                depotD.setGsrddBatch(poItem.getBatch());
                depotD.setGsrddBatchNo(poItem.getBatchNo());
                depotD.setGsrddValidDate(poItem.getValidDate());
                depotD.setGsrddStockStatus("1");
//                poItem.setGsrddStartQty(poItem.getGsrddStartQty());
                totalAmt = totalAmt.add((new BigDecimal(poItem.getProPrice())).multiply(new BigDecimal(poItem.getGsrddRetdepQty())));
                totalQty = totalQty.add(new BigDecimal(poItem.getGsrddRetdepQty()));
                this.returnDepotDMapper.insert(depotD);
                Example exampleStock = new Example(GaiaBatchStock.class);
                exampleStock.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batProCode", poItem.getProCode()).andEqualTo("batBatch", poItem.getBatch());
                GaiaBatchStock stock = (GaiaBatchStock)this.batchStockMapper.selectOneByExample(exampleStock);
                compadmInData = new GetWfCreateReturnCompadmInData();
                compadmInData.setStoreId(inData.getStoreCode());
                compadmInData.setBatch(poItem.getBatch());
                compadmInData.setReturnDate(DateUtil.format(new Date(), "yyyyMMdd"));
                compadmInData.setReturnOrder(voucherId);
                compadmInData.setProCode(poItem.getProCode());
                compadmInData.setProName(poItem.getProName());
                compadmInData.setProSpecs(poItem.getProSpecs());
                compadmInData.setRejectQty(poItem.getGsrddRetdepQty());
                compadmInData.setSccj(poItem.getProFactoryName());
                if (ObjectUtil.isNotEmpty(stock)) {
                    compadmInData.setBatchCost(stock.getBatBatchCost().toString());
                    //物料评估表获取  移动平均价
                    Example exampleMateria = new Example(GaiaMaterialAssess.class);
                    exampleMateria.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("matProCode", poItem.getProCode()).andEqualTo("matAssessSite", inData.getStoreCode());
                    GaiaMaterialAssess materialAssess = (GaiaMaterialAssess)this.materialAssessMapper.selectOneByExample(exampleMateria);
                    compadmInData.setWmaCost((stock.getBatMovPrice()==null ? "":materialAssess.getMatMovPrice()).toString());
                    compadmInData.setWmaCostTax((stock.getBatRateAmt()==null ? "":stock.getBatRateAmt()).toString());
                }
            }
            wfCreateInData.setWfDetail(details);
//            poId = this.authService.createWorkflow(wfCreateInData);
//            JSONObject jsonObject = JSONObject.parseObject(poId);
//            if (!"0".equals(jsonObject.getString("code"))) {
//                throw new BusinessException(jsonObject.getString("message"));
//            }
        } else {  //退库（供应商）
            voucherId = this.returnDepotHMapper.selectNextVoucherId1(inData.getClientId());
            depotH.setGsrdhVoucherId(voucherId);
            depotH.setGsrdhTo(inData.getGsrdhTo());
            depotH.setGsrdhProcedure("1");
            new ArrayList();
            Iterator var18 = inData.getDepotDetailInDataList().iterator();
            while(var18.hasNext()) {
                GetDepotDetailInData poItem = (GetDepotDetailInData)var18.next();
                GaiaSdReturnDepotD depotD = new GaiaSdReturnDepotD();
                depotD.setClientId(inData.getClientId());
                depotD.setGsrddBrId(inData.getGsrdhBrId());
                depotD.setGsrddVoucherId(voucherId);
                depotD.setGsrddProId(poItem.getProCode());
                depotD.setGsrddSerial(String.valueOf(serial++));
                depotD.setGsrddDate(DateUtil.format(new Date(), "yyyyMMdd"));
                depotD.setGsrddRetdepCause(poItem.getGsrddRetdepCause());
                depotD.setGsrddRetdepQty(poItem.getGsrddRetdepQty());
                depotD.setGsrddReviseQty("0");
                depotD.setGsrddStartQty("0");
                depotD.setGsrddStockQty(poItem.getStockQty());
                depotD.setGsrddBatch(poItem.getBatch());
                depotD.setGsrddBatchNo(poItem.getBatchNo());
                depotD.setGsrddValidDate(poItem.getValidDate());
                depotD.setGsrddStockStatus("1");
                BigDecimal totalLineAmt = (new BigDecimal(poItem.getPoPrice())).multiply(new BigDecimal(poItem.getGsrddRetdepQty()));
                totalAmt = totalAmt.add(totalLineAmt);
                totalQty = totalQty.add(new BigDecimal(poItem.getGsrddRetdepQty()));
                this.returnDepotDMapper.insert(depotD);
            }
            GaiaPoHeader poHeader = new GaiaPoHeader();
            poHeader.setClient(inData.getClientId());
            poHeader.setPoCompanyCode(inData.getStoreCode());
            poHeader.setPoSupplierId(inData.getGsrdhTo());
            poId = this.poHeaderMapper.selectNextVoucherId1(inData.getClientId());
            poHeader.setPoId(poId);
            poHeader.setPoType("Z002");
            poHeader.setPoSubjectType("2");
            poHeader.setPoDate(DateUtil.format(new Date(), "yyyyMMdd"));
            poHeader.setPoHeadRemark(inData.getGsrdhRemaks() ==  null ? "" : inData.getGsrdhRemaks());
            poHeader.setPoCreateBy(userInfo.getUserId());
            poHeader.setPoCreateDate(DateUtil.format(new Date(), "yyyyMMdd"));
            poHeader.setPoCreateTime(DateUtil.format(new Date(), "HHmmss"));
            this.poHeaderMapper.insert(poHeader);
            serial = 1;
            Iterator var24 = inData.getDepotDetailInDataList().iterator();

            while(var24.hasNext()) {
                GetDepotDetailInData detail = (GetDepotDetailInData)var24.next();
                GaiaPoItem poItem = new GaiaPoItem();
                poItem.setClient(inData.getClientId());
                poItem.setPoSiteCode(inData.getStoreCode());
                poItem.setPoId(poId);
                poItem.setPoLineNo(String.valueOf(serial++));
                poItem.setPoProCode(detail.getProCode());
                poItem.setPoQty(new BigDecimal(detail.getGsrddRetdepQty()));
                poItem.setPoUnit(detail.getProUnit());
                poItem.setPoPrice(new BigDecimal(detail.getProPrice()));
                poItem.setPoRate(detail.getPoRate());
                poItem.setPoDeliveryDate(DateUtil.format(new Date(), "yyyyMMdd"));
                this.poItemMapper.insert(poItem);
            }
        }
        depotH.setGsrdhRemaks(inData.getGsrdhRemaks());
        depotH.setGsrdhProcedure("1");
        depotH.setGsrdhTotalAmt(totalAmt);
        depotH.setGsrdhTotalQty(totalQty.toString());
        this.returnDepotHMapper.insert(depotH);
        return voucherId;
    }

    @Transactional
    public void update(GetDepotInData inData, GetLoginOutData userInfo) {
        Example exampleH = new Example(GaiaSdReturnDepotH.class);
        exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrdhVoucherId", inData.getGsrdhVoucherId());
        GaiaSdReturnDepotH depotH = (GaiaSdReturnDepotH)this.returnDepotHMapper.selectOneByExample(exampleH);
        Example exampleD = new Example(GaiaSdReturnDepotD.class);
        exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrddVoucherId", inData.getGsrdhVoucherId());
        this.returnDepotDMapper.deleteByExample(exampleD);
        int i = 1;
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;
        Iterator var9 = inData.getDepotDetailInDataList().iterator();

        while(var9.hasNext()) {
            GetDepotDetailInData poItem = (GetDepotDetailInData)var9.next();
            GaiaSdReturnDepotD depotD = new GaiaSdReturnDepotD();
            depotD.setClientId(inData.getClientId());
            depotD.setGsrddBrId(depotH.getGsrdhBrId());
            depotD.setGsrddVoucherId(inData.getGsrdhVoucherId());
            depotD.setGsrddProId(poItem.getProCode());
            depotD.setGsrddSerial(String.valueOf(i++));
            depotD.setGsrddDate(DateUtil.format(new Date(), "yyyyMMdd"));
            depotD.setGsrddRetdepCause(poItem.getGsrddRetdepCause());
            depotD.setGsrddRetdepQty(poItem.getGsrddRetdepQty());
            depotD.setGsrddReviseQty("0");
            depotD.setGsrddStartQty(poItem.getGsrddRetdepQty());
            depotD.setGsrddStockQty(poItem.getStockQty());
            depotD.setGsrddBatch(poItem.getBatch());
            depotD.setGsrddBatchNo(poItem.getBatchNo());
            depotD.setGsrddValidDate(poItem.getValidDate());
            depotD.setGsrddStockStatus("1");
            totalAmt = totalAmt.add((new BigDecimal(poItem.getProPrice())).multiply(new BigDecimal(poItem.getGsrddRetdepQty())));
            totalQty = totalQty.add(new BigDecimal(poItem.getGsrddRetdepQty()));
            this.returnDepotDMapper.insertSelective(depotD);
        }

        depotH.setGsrdhTotalAmt(totalAmt);
        depotH.setGsrdhTotalQty(totalQty.toString());
        this.returnDepotHMapper.updateByPrimaryKeySelective(depotH);
    }

    @Transactional
    public void approve(GetDepotInData inData) {
        //查询退货主表
        Example example = new Example(GaiaSdReturnDepotH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrdhVoucherId", inData.getGsrdhVoucherId());
        GaiaSdReturnDepotH depotH = (GaiaSdReturnDepotH)this.returnDepotHMapper.selectOneByExample(example);
        depotH.setGsrdhStatus(inData.getGsrdhStatus());
        if ("2".equals(inData.getGsrdhStatus())) {
            depotH.setGsrdhDate(DateUtil.format(new Date(), "yyyyMMdd"));
        }

        if ("4".equals(inData.getGsrdhStatus())) {
            depotH.setGsrdhFinishDate(DateUtil.format(new Date(), "yyyyMMdd"));
        }

        depotH.setGsrdhProcedure("1");
        //查询退货明细
        Example exampleD = new Example(GaiaSdReturnDepotD.class);
        exampleD.createCriteria().andEqualTo("clientId", depotH.getClientId()).andEqualTo("gsrddVoucherId", depotH.getGsrdhVoucherId()).andEqualTo("gsrddDate", depotH.getGsrdhDate());
        List<GaiaSdReturnDepotD> depotDList = this.returnDepotDMapper.selectByExample(exampleD);
        String guid = UUID.randomUUID().toString();
        List<MaterialDocRequestDto> list = new ArrayList();
        Iterator var8 = depotDList.iterator();

        while(var8.hasNext()) {
            GaiaSdReturnDepotD depotD = (GaiaSdReturnDepotD)var8.next();
            depotD.setGsrddStockStatus(inData.getGsrdhStatus());
            //修改退货明细表
            this.returnDepotDMapper.updateByPrimaryKeySelective(depotD);
            if ("2".equals(depotH.getGsrdhType())) {
                example = new Example(GaiaSdStockBatch.class);
                example.createCriteria().andEqualTo("clientId", depotD.getClientId()).andEqualTo("gssbBrId", depotH.getGsrdhFrom()).andEqualTo("gssbProId", depotD.getGsrddProId()).andEqualTo("gssbBatchNo", depotD.getGsrddBatchNo()).andEqualTo("gssbBatch", depotD.getGsrddBatch());
                GaiaSdStockBatch stockBatch = (GaiaSdStockBatch)this.stockBatchMapper.selectOneByExample(example);
                int leaveCount = (int)((Double.parseDouble(stockBatch.getGssbQty()) -Double.parseDouble(depotD.getGsrddRetdepQty())));
                if (leaveCount < 0) {
                    stockBatch.setGssbQty("0");
                } else {
                    stockBatch.setGssbQty(StrUtil.toString(leaveCount));
                }
                //修改批次信息表
                this.stockBatchMapper.updateByPrimaryKey(stockBatch);
                GaiaSdBatchChange sdBatchChange =new GaiaSdBatchChange();
                sdBatchChange.setClient(depotD.getClientId());
                sdBatchChange.setGsbcBrId(depotD.getGsrddBrId());
                sdBatchChange.setGsbcVoucherId(depotD.getGsrddVoucherId());
                sdBatchChange.setGsbcSerial(depotD.getGsrddSerial());
                sdBatchChange.setGsbcDate(DateUtil.format(new Date(), "yyyyMMdd"));
                sdBatchChange.setGsbcProId(depotD.getGsrddProId());
                sdBatchChange.setGsbcBatchNo(depotD.getGsrddBatchNo());
                sdBatchChange.setGsbcBatch(StrUtil.isBlank(depotD.getGsrddBatch()) ? "" : depotD.getGsrddBatch());
                sdBatchChange.setGsbcQty(new BigDecimal(depotD.getGsrddRetdepQty()));
                //添加批次异动表
                sdBatchChangeMapper.insert(sdBatchChange);
                Example examplePro = new Example(GaiaProductBusiness.class);
                examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", depotD.getGsrddProId()).andEqualTo("proSite", inData.getStoreCode());
                GaiaProductBusiness productBusiness = (GaiaProductBusiness)this.productBusinessMapper.selectOneByExample(examplePro);
                MaterialDocRequestDto dto = new MaterialDocRequestDto();
                dto.setClient(inData.getClientId());
                dto.setGuid(guid);
                dto.setGuidNo(depotD.getGsrddSerial());
                dto.setMatPostDate(depotD.getGsrddDate());
                dto.setMatHeadRemark("");
                dto.setMatType("TD");
                dto.setMatProCode(depotD.getGsrddProId());
                dto.setMatStoCode("");
                dto.setMatSiteCode(inData.getStoreCode());
                dto.setMatLocationCode("1000");
                dto.setMatLocationTo("");
                dto.setMatBatch(depotD.getGsrddBatch());
                dto.setMatQty(new BigDecimal(depotD.getGsrddRetdepQty()));
                dto.setMatUnit(productBusiness.getProUnit());
                dto.setMatDebitCredit("H");
                dto.setMatPoLineno(depotD.getGsrddSerial());
                dto.setMatDnId(depotH.getGsrdhVoucherId());
                dto.setMatDnLineno(depotD.getGsrddSerial());
                dto.setMatCreateBy(inData.getGsrdhEmp());
                dto.setMatCreateDate(DateUtil.format(new Date(), "yyyyMMdd"));
                dto.setMatCreateTime(DateUtil.format(new Date(), "HHmmss"));
                list.add(dto);
            }
        }
        //修改退货主表
        this.returnDepotHMapper.updateByPrimaryKeySelective(depotH);
        if (ObjectUtil.isNotEmpty(list) && "1".equals(this.thirdServer) && "5".equals(inData.getGsrdhStatus())) {
            String result = this.purchaseService.insertMaterialDoc(list);
            if (StrUtil.isBlank(result)) {
                throw new BusinessException("服务异常");
            }

            JSONObject jsonObject = JSONObject.parseObject(result);
            if (!"0".equals(jsonObject.getString("code"))) {
                throw new BusinessException(jsonObject.getString("message"));
            }
        }

    }

    public PageInfo<GetQueryProVoOutData> queryPro(GetProductInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        List<GetQueryProVoOutData> outData = this.productBasicMapper.queryProFromBatch(inData);
//        int a=0,b=0,c=0;
        PageInfo pageInfo =new PageInfo();
        if(ObjectUtil.isNotEmpty(outData)){
//            for (GetQueryProVoOutData queryProVoOutData : outData){
//                Example example = new Example(GaiaSdAcceptD.class);
//                example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadBatch", queryProVoOutData.getBatch()).andEqualTo("gsadBatchNo", queryProVoOutData.getBatchNo());
//                List<GaiaSdAcceptD> list = this.acceptDMapper.selectByExample(example);
//                a++;
//                //可能出现批次信息表 没有对应的采购单
//                //根据 批次信息 查询 收货单  再查询 采购单
//                if (ObjectUtil.isEmpty(list)) {
//                    continue;
//                } else {
//                    GaiaSdAcceptD acceptD = (GaiaSdAcceptD)list.get(0);
//                    Example exampleH = new Example(GaiaSdAcceptH.class);
//                    exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", acceptD.getGsadVoucherId());
//                    GaiaSdAcceptH acceptH = (GaiaSdAcceptH)this.acceptHMapper.selectOneByExample(exampleH);
//                    b++;
//                    Example examplePo = new Example(GaiaPoItem.class);
//                    examplePo.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", acceptH.getGsahPoid()).andEqualTo("poLineNo", acceptD.getGsadSerial()).andEqualTo("poProCode", acceptD.getGsadProId());
//                    GaiaPoItem poItem = (GaiaPoItem)this.poItemMapper.selectOneByExample(examplePo);
//                    c++;
//                    if (ObjectUtil.isEmpty(poItem)) {
//                        continue;
//                    } else {
//                        queryProVoOutData.setPoId(poItem.getPoId());
//                        queryProVoOutData.setPoLineNo(poItem.getPoLineNo());
//                        queryProVoOutData.setPoPrice(poItem.getPoPrice().toString());
//                        queryProVoOutData.setPoRate(poItem.getPoRate());
//                    }
//                }
//            }
            pageInfo =new PageInfo(outData);
        }

//        System.out.println("a="+a);
//        System.out.println("b="+b);
//        System.out.println("c="+c);

        return pageInfo;
    }

    @Transactional
    public void approveDepot(GetWfApproveInData inData) {
        Example example = new Example(GaiaSdReturnDepotH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrdhVoucherId", inData.getWfOrder());
        GaiaSdReturnDepotH depotH = (GaiaSdReturnDepotH)this.returnDepotHMapper.selectOneByExample(example);
        if (ObjectUtil.isNull(depotH)) {
            throw new BusinessException("该退库单不存在！");
        } else {
            if ("3".equals(inData.getWfStatus())) {
                if ("3".equals(depotH.getGsrdhStatus())) {
                    return;
                }

                depotH.setGsrdhStatus("3");
                this.returnDepotHMapper.updateByPrimaryKey(depotH);
                example = new Example(GaiaSdReturnDepotD.class);
                example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrddVoucherId", inData.getWfOrder());
                List<GaiaSdReturnDepotD> detailList = this.returnDepotDMapper.selectByExample(example);
                Iterator var5 = detailList.iterator();

                while(var5.hasNext()) {
                    GaiaSdReturnDepotD detail = (GaiaSdReturnDepotD)var5.next();
                    example = new Example(GaiaSdStockBatch.class);
                    example.createCriteria().andEqualTo("clientId", detail.getClientId()).andEqualTo("gssbBrId", depotH.getGsrdhFrom()).andEqualTo("gssbProId", detail.getGsrddProId()).andEqualTo("gssbBatchNo", detail.getGsrddBatchNo()).andEqualTo("gssbBatch", detail.getGsrddBatch());
                    GaiaSdStockBatch stockBatch = (GaiaSdStockBatch)this.stockBatchMapper.selectOneByExample(example);
                    int leaveCount = Integer.valueOf(stockBatch.getGssbQty()) - Integer.valueOf(detail.getGsrddRetdepQty());
                    if (leaveCount < 0) {
                        stockBatch.setGssbQty("0");
                    } else {
                        stockBatch.setGssbQty(StrUtil.toString(leaveCount));
                    }

                    this.stockBatchMapper.updateByPrimaryKey(stockBatch);
                    example = new Example(GaiaSdStockBatch.class);
                    example.createCriteria().andEqualTo("clientId", detail.getClientId()).andEqualTo("gssbBrId", depotH.getGsrdhTo()).andEqualTo("gssbProId", detail.getGsrddProId()).andEqualTo("gssbBatchNo", detail.getGsrddBatchNo()).andEqualTo("gssbBatch", detail.getGsrddBatch());
                    GaiaSdStockBatch stockBatchTo = (GaiaSdStockBatch)this.stockBatchMapper.selectOneByExample(example);
                    int recCount = Integer.valueOf(stockBatchTo.getGssbQty()) + Integer.valueOf(detail.getGsrddRetdepQty());
                    stockBatchTo.setGssbQty(StrUtil.toString(recCount));
                    this.stockBatchMapper.updateByPrimaryKey(stockBatchTo);
                }
            }

        }
    }

    public List<GetQueryProVoOutData> exportIn(GetDepotInData inData) {
        List<GetQueryProVoOutData> outData = new ArrayList();
        Iterator var3 = inData.getDepotDetailInDataList().iterator();

        while(var3.hasNext()) {
            GetDepotDetailInData detailInData = (GetDepotDetailInData)var3.next();
            GetProductInData productInData = new GetProductInData();
            productInData.setClientId(inData.getClientId());
            productInData.setGspgProId(detailInData.getProCode());
            productInData.setStoreCode(inData.getStoreCode());
            productInData.setBatch(detailInData.getBatch());
            productInData.setBatchNo(detailInData.getBatchNo());
            List<GetQueryProVoOutData> list = this.productBasicMapper.queryProFromBatch(productInData);
            if (!ObjectUtil.isNotEmpty(list)) {
                throw new BusinessException("商品编码:" + detailInData.getProCode() + "，批次:" + detailInData.getBatch() + "，批号:" + detailInData.getBatchNo() + "的商品不存在！");
            }

            ((GetQueryProVoOutData)list.get(0)).setAllotQty(detailInData.getGsrddRetdepQty());
            outData.addAll(list);
        }

        return outData;
    }

    public GetDepotPoDetailOutData getPoDetail(GetDepotInData inData) {
        Example example = new Example(GaiaSdAcceptD.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadBatch", inData.getBatch()).andEqualTo("gsadProId", inData.getProId()).andEqualTo("gsadBatchNo", inData.getBatchNo());
        List<GaiaSdAcceptD> list = this.acceptDMapper.selectByExample(example);
        if (ObjectUtil.isEmpty(list)) {
            throw new BusinessException("该批次商品未匹配到采购单！");
        } else {
            GaiaSdAcceptD acceptD = (GaiaSdAcceptD)list.get(0);
            Example exampleH = new Example(GaiaSdAcceptH.class);
            exampleH.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", acceptD.getGsadVoucherId());
            GaiaSdAcceptH acceptH = (GaiaSdAcceptH)this.acceptHMapper.selectOneByExample(exampleH);
            Example examplePo = new Example(GaiaPoItem.class);
            examplePo.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", acceptH.getGsahPoid()).andEqualTo("poLineNo", acceptD.getGsadSerial()).andEqualTo("poProCode", acceptD.getGsadProId());
            GaiaPoItem poItem = (GaiaPoItem)this.poItemMapper.selectOneByExample(examplePo);
            if (ObjectUtil.isEmpty(poItem)) {
                throw new BusinessException("该批次商品未匹配到采购单！");
            } else {
                GetDepotPoDetailOutData outData = new GetDepotPoDetailOutData();
                outData.setPoId(poItem.getPoId());
                outData.setPoLineNo(poItem.getPoLineNo());
                outData.setPoPrice(poItem.getPoPrice().toString());
                outData.setPoRate(poItem.getPoRate());
                return outData;
            }
        }
    }

    @Override
    public List<GetStockBatchProOutData> getStockBatchPro(GetStockBatchProInData inData) {
        return null;
//        return stockBatchMapper.getStockBatchPro(inData);
    }
}
