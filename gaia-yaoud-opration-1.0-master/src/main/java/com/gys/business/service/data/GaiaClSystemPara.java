package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
    * 公司参数表
    */
@ApiModel(value="com-gys-business-service-data-GaiaClSystemPara")
public class GaiaClSystemPara implements Serializable {
    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 参数名
    */
    @ApiModelProperty(value="参数名")
    private String gcspId;

    /**
    * 参数描述
    */
    @ApiModelProperty(value="参数描述")
    private String gcspName;

    /**
    * 值描述
    */
    @ApiModelProperty(value="值描述")
    private String gcspParaRemark;

    /**
    * 值1
    */
    @ApiModelProperty(value="值1")
    private String gcspPara1;

    /**
    * 值2
    */
    @ApiModelProperty(value="值2")
    private String gcspPara2;

    /**
    * 修改人员
    */
    @ApiModelProperty(value="修改人员")
    private String gcspUpdateEmp;

    /**
    * 修改日期
    */
    @ApiModelProperty(value="修改日期")
    private String gcspUpdateDate;

    /**
    * 修改时间
    */
    @ApiModelProperty(value="修改时间")
    private String gcspUpdateTime;

    private static final long serialVersionUID = 1L;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getGcspId() {
        return gcspId;
    }

    public void setGcspId(String gcspId) {
        this.gcspId = gcspId;
    }

    public String getGcspName() {
        return gcspName;
    }

    public void setGcspName(String gcspName) {
        this.gcspName = gcspName;
    }

    public String getGcspParaRemark() {
        return gcspParaRemark;
    }

    public void setGcspParaRemark(String gcspParaRemark) {
        this.gcspParaRemark = gcspParaRemark;
    }

    public String getGcspPara1() {
        return gcspPara1;
    }

    public void setGcspPara1(String gcspPara1) {
        this.gcspPara1 = gcspPara1;
    }

    public String getGcspPara2() {
        return gcspPara2;
    }

    public void setGcspPara2(String gcspPara2) {
        this.gcspPara2 = gcspPara2;
    }

    public String getGcspUpdateEmp() {
        return gcspUpdateEmp;
    }

    public void setGcspUpdateEmp(String gcspUpdateEmp) {
        this.gcspUpdateEmp = gcspUpdateEmp;
    }

    public String getGcspUpdateDate() {
        return gcspUpdateDate;
    }

    public void setGcspUpdateDate(String gcspUpdateDate) {
        this.gcspUpdateDate = gcspUpdateDate;
    }

    public String getGcspUpdateTime() {
        return gcspUpdateTime;
    }

    public void setGcspUpdateTime(String gcspUpdateTime) {
        this.gcspUpdateTime = gcspUpdateTime;
    }
}