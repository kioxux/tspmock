package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:28 2021/10/20
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class RecallForm {
    private List<RecallDTO> recallList;
}
