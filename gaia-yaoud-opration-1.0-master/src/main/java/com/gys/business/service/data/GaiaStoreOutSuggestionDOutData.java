package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionD)实体类
 *
 * @author makejava
 * @since 2021-10-28 10:53:43
 */
@Data
public class GaiaStoreOutSuggestionDOutData {
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    private String stoCode;
    /**
     * 商品调库单号
     */
    private String billCode;
    private String billDate;
    private String invalidDate;
    private String status;
    private String proBigClassCode;
    private String proBigClassName;
    private String proClassCode;
    private String billDateStr;
    private String invalidDateStr;
    private int itemsQty;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 规格
     */
    private String proSpces;
    
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 月均销量
     */
    private BigDecimal averageSalesQty;
    /**
     * 库存量
     */
    private BigDecimal inventoryQty;
    /**
     * 建议调剂量
     */
    private BigDecimal suggestionQty;
    /**
     * 批号
     */
    private String batchNo;
    /**
     * 有效期至
     */
    private String validDay;
    /**
     * 月均销售额
     */
    private BigDecimal averageSalesAmt;
    /**
     * 关联门店1 确认量
     */
    private BigDecimal storeOneQty;
    /**
     * 关联门店2 确认量
     */
    private BigDecimal storeTwoQty;
    /**
     * 关联门店3 确认量
     */
    private BigDecimal storeThreeQty;
    /**
     * 成分分类
     */
    private String proCompclass;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;
    /**
     * 大类
     */
    private String bigClass;
    /**
     * 大类名称
     */
    private String bigClassName;
    /**
     * 商品分类描述
     */
    private String proClassName;
    private BigDecimal cost;
    private BigDecimal batchInventoryQty;



}

