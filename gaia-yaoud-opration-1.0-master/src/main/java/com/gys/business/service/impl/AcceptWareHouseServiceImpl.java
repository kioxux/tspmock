package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.*;
import com.gys.business.mapper.cancelStock.CancelStockMapper;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.AcceptWareHouseService;
import com.gys.business.service.CommonService;
import com.gys.business.service.ExamineService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.cancelStock.CancelStockData;
import com.gys.business.service.data.cancelStock.CancelStockDetail;
import com.gys.business.service.data.workflow.GetWfDistributionReturnInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.feign.AuthService;
import com.gys.util.*;
import com.qcloud.cos.utils.StringUtils;
import com.xxl.job.core.context.XxlJobHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.gys.common.constant.CommonConstant.DICTIONARY_TYPE_REJECTE;

@Service
@Slf4j
public class AcceptWareHouseServiceImpl implements AcceptWareHouseService {
    @Resource
    private GaiaSdAcceptHMapper acceptHMapper;
    @Resource
    private GaiaWmsDiaoboZMapper diaoboZMapper;
    @Resource
    private GaiaWmsDiaoboMMapper diaoboMMapper;
    @Resource
    private GaiaSdAcceptDMapper acceptDMapper;
    @Resource
    private AcceptWareHouseMapper acceptWareHouseMapper;
    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;
    @Resource
    private GaiaBatchInfoMapper batchInfoMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Resource
    private ExamineService examineService;
    @Resource
    private GaiaPoHeaderMapper poHeaderMapper;
    @Resource
    private GaiaPoItemMapper poItemMapper;
    @Resource
    private GaiaSdWtpsdDMapper gaiaSdWtpsdDMapper;
    @Resource
    private GaiaSdWtbhdHMapper gaiaSdWtbhdHMapper;
    @Resource
    private GaiaSdWtpdsHMapper gaiaSdWtpdsHMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;
    @Resource
    private GaiaSdWtproMapper gaiaSdWtproMapper;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CancelStockMapper cancelStockMapper;
    @Resource
    private AuthService authService;
    @Resource
    private DictionaryUtil dictionaryUtil;


    private final String COLDCHAIN_KEY = "COLDECHAIN";

    @Override
    public List<AcceptWareHouseOutData> pickOrderList(GetLoginOutData userInfo) {
        String dcCode = "";
        StoreOutData storeOutData = storeDataMapper.getStoreData(userInfo.getClient(), userInfo.getDepId());
        if(ObjectUtil.isNotEmpty(storeOutData)){
            dcCode = storeOutData.getStoDcCode();
        }else {
            throw new BusinessException("当前门店不存在！");
        }
        if (org.apache.commons.lang3.StringUtils.isEmpty(dcCode)){
            throw new BusinessException("当前门店不存在仓库，请检查！");
        }
        return acceptWareHouseMapper.selectPickOrderList(userInfo.getClient(), userInfo.getDepId(),dcCode);
    }

    @Override
    public Map<String, Object> selectList(AcceptWareHouseInData inData) {
        if (ObjectUtil.isEmpty(inData.getPsVoucherId())
                && ObjectUtil.isEmpty(inData.getPickProId())) {
            if (org.apache.commons.lang3.StringUtils.isEmpty(inData.getStartDate())) {
                throw new BusinessException("请选择起始日期");
            }
            if (org.apache.commons.lang3.StringUtils.isEmpty(inData.getEndDate())) {
                throw new BusinessException("请选择结束日期");
            }
        }
        String regex = "^([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$";
        Pattern pattern = Pattern.compile(regex);
        if (ObjectUtil.isNotEmpty(inData.getStartDate())) {
            if(pattern.matcher(inData.getStartDate()).matches()){
                inData.setStartDate(CommonUtil.parseWebDate(inData.getStartDate()));
            }else {
                inData.setStartDate(inData.getStartDate());
            }
        }
        if (ObjectUtil.isNotEmpty(inData.getEndDate())) {
            if(pattern.matcher(inData.getStartDate()).matches()){
                inData.setEndDate(CommonUtil.parseWebDate(inData.getEndDate()));
            }else {
                inData.setEndDate(inData.getEndDate());
            }
        }
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(), inData.getStoCode());
        if(ObjectUtil.isNotEmpty(storeOutData)){
            inData.setDcCode(storeOutData.getStoDcCode());
        }else {
            throw new BusinessException("当前门店不存在！");
        }
        if (org.apache.commons.lang3.StringUtils.isEmpty(inData.getDcCode())){
            throw new BusinessException("当前门店不存在仓库，请检查！");
        }
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> totalMap = new HashMap<>();
        List<AcceptWareHouseOutData> outData = acceptWareHouseMapper.selectList(inData);
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;
        for (AcceptWareHouseOutData data : outData) {
            if (ObjectUtil.isNotEmpty(data.getPickProAmt())) {
                totalAmt = totalAmt.add(new BigDecimal(data.getPickProAmt()));
            } else {
                totalAmt = totalAmt.add(BigDecimal.ZERO);
            }
            if (ObjectUtil.isNotEmpty(data.getPickProQty())) {
                totalQty = totalQty.add(new BigDecimal(data.getPickProQty()));
            } else {
                totalQty = totalQty.add(BigDecimal.ZERO);
            }
        }
        totalMap.put("pickProAmt", totalAmt);
        totalMap.put("pickProQty", totalQty);
        result.put("totalMap", totalMap);
        if (ObjectUtil.isNotEmpty(outData) && outData.size() > 0) {
            result.put("outData", outData);
        }
        return result;
    }

    @Override
    public List<AcceptWareHouseOutData> selectListById(AcceptWareHouseInData inData) {
        String dcCode = "";
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(),inData.getStoCode());
        if(ObjectUtil.isNotEmpty(storeOutData)){
            dcCode = storeOutData.getStoDcCode();
        }else {
            throw new BusinessException("当前门店不存在！");
        }
        if (org.apache.commons.lang3.StringUtils.isEmpty(dcCode)){
            throw new BusinessException("当前门店不存在仓库，请检查！");
        }
        return acceptWareHouseMapper.selectListById(inData.getClientId(), inData.getStoCode(),dcCode, inData.getPickProId());
    }

    @Override
    public AcceptVoucherOutData selectTotalDetail(AcceptWareHouseInData inData) {
        AcceptVoucherOutData result = new AcceptVoucherOutData();
        String dcCode = "";
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(), inData.getStoCode());
        if(ObjectUtil.isNotEmpty(storeOutData)){
            dcCode = storeOutData.getStoDcCode();
        }else {
            throw new BusinessException("当前门店不存在！");
        }
        inData.setDcCode(dcCode);
        List<AcceptWareHouseOutData> orderList = acceptWareHouseMapper.selectList(inData);
        if (orderList.size() > 0 && orderList != null) {
            result.setPsTotalAmt(orderList.get(0).getPickProAmt());
            result.setTotalCount(orderList.get(0).getPickProQty());
            result.setPickProId(orderList.get(0).getPickOrderId());
            result.setTotalPickCount(orderList.get(0).getBoxCount());
            result.setTotalAmt(orderList.get(0).getLsProAmt());
        }
        List<AcceptSentOrderOutData> itemList = acceptWareHouseMapper.selectPsOrderTotalList(inData);
        result.setOrderCount(itemList.size() + "");
        if (itemList.size() > 0 && itemList != null) {
            result.setOrderList(itemList);
        }
        return result;
    }

    @Override
    public List<AcceptWareHouseProDetailData> selectPickProductList(AcceptWareHouseInData inData) {
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(), inData.getStoCode());
        if(ObjectUtil.isNotEmpty(storeOutData)){
            inData.setDcCode(storeOutData.getStoDcCode());
        }else {
            throw new BusinessException("当前门店不存在！");
        }
        if (org.apache.commons.lang3.StringUtils.isEmpty(inData.getDcCode())){
            throw new BusinessException("当前门店不存在仓库，请检查！");
        }
        List<AcceptWareHouseProDetailData> result = new ArrayList<>();
        List<AcceptWareHouseOutData> outData = acceptWareHouseMapper.selectList(inData);
        if (ObjectUtil.isNotEmpty(outData) && outData.size() > 0){
            if ("1".equals(outData.get(0).getAcceptStatus())){
                result = acceptWareHouseMapper.selectApproveProductList(inData);
            }else {
                result = acceptWareHouseMapper.selectPickProductList(inData);
            }
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approve(AcceptWareHouseInData inData, GetLoginOutData userInfo) {
        String dcCode = "";
        StoreOutData storeOutData = storeDataMapper.getStoreData(userInfo.getClient(), userInfo.getDepId());
        if(ObjectUtil.isNotEmpty(storeOutData)){
            dcCode = storeOutData.getStoDcCode();
            inData.setDcCode(storeOutData.getStoDcCode());
        }else {
            throw new BusinessException("当前门店不存在！");
        }
        if (org.apache.commons.lang3.StringUtils.isEmpty(dcCode)){
            throw new BusinessException("当前门店不存在仓库，请检查！");
        }
        //校验是否允许不填写冷链信息
        GaiaSdSystemPara systemPara = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(), inData.getStoCode(), "RECISALE_LC");
        if (ObjectUtil.isEmpty(systemPara) || "0".equals(systemPara.getGsspPara())) {
            //校验是否含有冷链商品
            String jhdh = acceptWareHouseMapper.checkColdChain(inData);
            if (!StringUtils.isNullOrEmpty(jhdh)) {
                //判断冷链信息是否填写完整
                String coldchainInfo = (String) redisManager.get(COLDCHAIN_KEY + "-" + inData.getClientId() + "-" + inData.getStoCode() + "-" + inData.getPickProId());
                if (StringUtils.isNullOrEmpty(coldchainInfo) || "null".equals(coldchainInfo)) {
                    throw new BusinessException("冷链信息未填写完整！");
                }
                ColdchainInfo info = JSON.parseObject(coldchainInfo, ColdchainInfo.class);
                if (StringUtils.isNullOrEmpty(info.getDeparturePlace())
                        || StringUtils.isNullOrEmpty(info.getDepartureDate())
                        || StringUtils.isNullOrEmpty(info.getDepartureTime())
                        || StringUtils.isNullOrEmpty(info.getDepartureTemperature())
                        || info.getBoxQty() == null
                        || StringUtils.isNullOrEmpty(info.getArriveDate())
                        || StringUtils.isNullOrEmpty(info.getArriveTime())
                        || StringUtils.isNullOrEmpty(info.getArriveTemperature())
                        || StringUtils.isNullOrEmpty(info.getTransportOrganization())
                        || StringUtils.isNullOrEmpty(info.getTransportMode())
                        || StringUtils.isNullOrEmpty(info.getTransportEmp())
                        || StringUtils.isNullOrEmpty(info.getTxFollow())
                        || StringUtils.isNullOrEmpty(info.getTempFit())
                        || StringUtils.isNullOrEmpty(info.getTransportTool())
                        || StringUtils.isNullOrEmpty(info.getTransportTime())
                        || StringUtils.isNullOrEmpty(info.getTempType())
                ) {
                    throw new BusinessException("冷链信息未填写完整！");
                }
            }
        }
        //校验是否在app 端已被审核，被审核后，web端不能审核！
        int qty = acceptWareHouseMapper.countFuHE(inData);
        if (qty > 0) {
            throw new BusinessException("该单已使用手机收货，不能在此审核，请继续用手机收货完成此单！");
        }
        List<Map<String, String>> checkOrder = acceptWareHouseMapper.checkAcceptPro(inData);
        if (checkOrder.size() > 0 && checkOrder != null) {
            for (Map<String, String> item : checkOrder) {
                //收货单不存在
                if (item.get("hasAcceptOrder").equals("1")) {
                    GetAcceptInData data = new GetAcceptInData();
                    data.setClientId(inData.getClientId());
                    data.setGsahBrId(inData.getStoCode());
                    data.setGsahPsVoucherId(item.get("psVoucherId"));
                    data.setGsahFrom(item.get("proFrom"));
                    data.setGsahTo(item.get("proTo"));
                    data.setGsahStatus("1");
                    data.setGsahEmp(inData.getUserId());
                    data.setDcCode(dcCode);
                    String voucherId = "";
                    //冷链信息
                    String coldchainInfo = (String) redisManager.get(COLDCHAIN_KEY + "-" + inData.getClientId() + "-" + inData.getStoCode() + "-" + inData.getPickProId());
                    ColdchainInfo info = null;
                    if (!StringUtils.isNullOrEmpty(coldchainInfo)){
                        info = JSON.parseObject(coldchainInfo, ColdchainInfo.class);
                        info.setEmp(userInfo.getUserId());
                    }
                    if (ObjectUtil.isNotEmpty(inData.getAcceptWareHouseProDetailList())){
                        List<AcceptWareHouseProDetailData> acceptWareHouseProDetailList = inData.getAcceptWareHouseProDetailList();
                        for (AcceptWareHouseProDetailData detailData : acceptWareHouseProDetailList) {
                            //拒收大于0必填拒收原因
                            if (org.apache.commons.lang3.StringUtils.isNotBlank(detailData.getRefuseQty()) && new BigDecimal(detailData.getRefuseQty()).compareTo(BigDecimal.ZERO)>0&& org.apache.commons.lang3.StringUtils.isBlank(detailData.getRejecteReason())) {
                                throw new BusinessException("该单有拒收商品未填写拒收原因");
                            }
                        }
                        data.setAcceptWareHouseProDetailList(inData.getAcceptWareHouseProDetailList());
                        voucherId = saveByRefuseQty(inData.getPickProId(),data,userInfo,info);
                    }else {
                        voucherId = this.save(data,info);
                    }
                    if (ObjectUtil.isNotEmpty(voucherId)) {
                        String isExamineAuto = storeDataMapper.selectStoPriceComparison(inData.getClientId(), inData.getStoCode(), "EXAMINE_AUTO");
                        if (ObjectUtil.isEmpty(isExamineAuto)) {
                            isExamineAuto = "0";
                        }
                        if (isExamineAuto.equals("1")) {
                            GetExamineInData examineInData = new GetExamineInData();
                            examineInData.setClientId(data.getClientId());
                            examineInData.setGsehBrId(data.getGsahBrId());
                            examineInData.setStoreCode(data.getGsahBrId());
                            examineInData.setGsehVoucherAcceptId(voucherId);
                            examineInData.setGsedEmp(inData.getUserId());
                            List<GetExamineDetailInData> list = new ArrayList<>();
                            for (GetAcceptDetailOutData data1 : data.getAcceptDetailInDataList()) {
                                GetExamineDetailInData item1 = new GetExamineDetailInData();
                                item1.setGsedProId(data1.getGsadProId());
                                item1.setGsedBatchNo(data1.getGsadBatchNo());
                                item1.setGsedBatch(data1.getGsadBatch());
                                item1.setGsedRecipientQty(data1.getGsadRecipientQty());
                                item1.setGsedQualifiedQty(data1.getGsadInvoicesQty());
                                item1.setGsedUnqualifiedQty(data1.getGsadRefuseQty());
                                item1.setGsedResult("合格");
                                item1.setGsedValidDate(data1.getGsadValidDate());
                                item1.setGsedStockStatus(data1.getGsadStockStatus());
                                item1.setGsedSerial(data1.getGsadSerial());
                                list.add(item1);
                            }
                            if (list.size() > 0 && list != null) {
                                examineInData.setExamineDetailInDataList(list);
                            }
                            this.examineService.approveEx(examineInData, userInfo);
                        }
                    }
                } else {
                    log.info("<异常单据(未审核已有收货信息)：{}>",JSON.toJSONString(inData));
                    if (item.get("gsahStatus").equals("0")) {
                        GetAcceptInData i = new GetAcceptInData();
                        i.setGsahEmp(inData.getUserId());
                        i.setClientId(inData.getClientId());
                        i.setGsahBrId(inData.getStoCode());
                        i.setStoreCode(inData.getStoCode());
                        i.setGsahPsVoucherId(item.get("psVoucherId"));
                        i.setGsahDate(CommonUtil.getyyyyMMdd());
                        i.setGsahVoucherId(item.get("acceptVoucherId"));
                        this.acceptWareHouseMapper.modifyAcceptStatus(i);
                        List<Map<String, String>> examineList = acceptWareHouseMapper.checkExaminePro(i);
                        if (examineList.size() > 0 && examineList != null) {
                            for (Map<String, String> examineMap : examineList) {
                                if (examineMap.get("gsehStatus").equals("0")) {
                                    acceptWareHouseMapper.modifyExamineStatus(i);
                                }
                            }
                        } else {
                            GetExamineInData examineData = new GetExamineInData();
                            examineData.setClientId(i.getClientId());
                            examineData.setStoreCode(i.getStoreCode());
                            examineData.setGsehBrId(i.getGsahBrId());
                            examineData.setGsehVoucherAcceptId(i.getGsahVoucherId());
                            examineData.setGsedEmp(inData.getUserId());
                            examineData.setSendType("1");
                            this.examineService.save(examineData);
                        }
                    } else {
                        GetAcceptInData i = new GetAcceptInData();
                        i.setGsahEmp(inData.getUserId());
                        i.setClientId(inData.getClientId());
                        i.setGsahBrId(inData.getStoCode());
                        i.setGsahPsVoucherId(item.get("psVoucherId"));
//                        i.setGsahDate(CommonUtil.getyyyyMMdd());
                        i.setGsahVoucherId(item.get("acceptVoucherId"));
                        this.acceptWareHouseMapper.modifyAcceptStatus(i);
                        List<Map<String, String>> examineList = acceptWareHouseMapper.checkExaminePro(i);
                        if (examineList.size() > 0 && examineList != null) {
                            for (Map<String, String> examineMap : examineList) {
                                if (examineMap.get("gsehStatus").equals("0")) {
                                    acceptWareHouseMapper.modifyExamineStatus(i);
                                }
                            }
                        } else {
                            GetExamineInData examineData = new GetExamineInData();
                            examineData.setClientId(i.getClientId());
                            examineData.setStoreCode(i.getGsahBrId());
                            examineData.setGsehBrId(i.getGsahBrId());
                            examineData.setGsehVoucherAcceptId(i.getGsahVoucherId());
                            examineData.setGsedEmp(inData.getUserId());
                            examineData.setSendType("1");
                            this.examineService.save(examineData);
                        }
                    }
                }
                //将地点赋值
                if (ValidateUtil.isEmpty(dcCode)) {
                    dcCode = item.get("proFrom");
                }
            }
            if (ValidateUtil.isEmpty(dcCode)) {
                throw new BusinessException("提示：请先维护配送中心，然后再来收货！");
            }
            inData.setDcCode(dcCode);
            inData.setGsahDate(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            this.acceptWareHouseMapper.modifyPickOrderStatus(inData);
            //更新复核明细表
            List<WmsFuHeOutData> fhxhInfo = acceptWareHouseMapper.getFhxhInfo(inData.getClientId(), inData.getPickProId(), dcCode);
            if (ValidateUtil.isNotEmpty(fhxhInfo)) {
                List<String> list = new ArrayList<>();
                for (WmsFuHeOutData data:fhxhInfo) {
                    list.add(data.getFhxh());
                }
                acceptWareHouseMapper.updateFhxhMStatus("2",inData.getClientId(),dcCode,list);
            }
        } else {
            throw new BusinessException("拣货单不存在，请核查后操作！");
        }
    }

    @Override
    public ColdchainInfo getColdchainInfo(AcceptWareHouseInData inData, GetLoginOutData userInfo) {
        if (StringUtils.isNullOrEmpty(inData.getPickProId())) {
            throw new BusinessException("拣货单号不能为空！");
        }
        String coldchainInfo = (String) redisManager.get(COLDCHAIN_KEY + "-" + inData.getClientId() + "-" + inData.getStoCode() + "-" + inData.getPickProId());
        if (!StringUtils.isNullOrEmpty(coldchainInfo) && !"null".equals(coldchainInfo)) {
            ColdchainInfo info = JSON.parseObject(coldchainInfo, ColdchainInfo.class);
            return info;
        }
        ColdchainInfo info = acceptHMapper.getColdchainInfo(inData);
        info = info == null ? new ColdchainInfo() : info;
        info.setEmp(acceptHMapper.getEmpName(inData.getClientId(), StringUtils.isNullOrEmpty(info.getEmp()) ? userInfo.getUserId() : info.getEmp()));
        info.setTransportMode("汽运");
        Map<String,String> dcInfo = storeDataMapper.getDcCodeByStore(userInfo.getClient(),userInfo.getDepId());
        if (ObjectUtil.isNotEmpty(dcInfo)) {
            if (org.apache.commons.lang3.StringUtils.isEmpty(info.getDeparturePlace())) {
                info.setDeparturePlace(dcInfo.get("dcAdd"));
            }
            if (org.apache.commons.lang3.StringUtils.isEmpty(info.getTransportOrganization())) {
                info.setTransportOrganization(dcInfo.get("dcName"));
            }
        }
        redisManager.set(COLDCHAIN_KEY + "-" + inData.getClientId() + "-" + inData.getStoCode() + "-" + inData.getPickProId(), JSON.toJSONString(info), 3600 * 24l);
        return info;
    }

    @Override
    public void saveColdchainInfo(ColdchainInfo info) {
        if (StringUtils.isNullOrEmpty(info.getJhdh())) {
            throw new BusinessException("拣货单号不能为空！");
        }
        String acceptStatus = acceptHMapper.getAcceptStatus(info.getClient(),info.getStoCode(), info.getJhdh());
        if (StringUtils.isNullOrEmpty(acceptStatus) || "1".equals(acceptStatus)) {
            throw new BusinessException("已审核成功,冷链信息无法修改！");
        }
        if(org.apache.commons.lang3.StringUtils.isNotBlank(info.getTransportCardId())){
            if(!IDCardUtil.isIDCard(info.getTransportCardId())){
                throw new BusinessException("身份证信息不合法！");
            }
        }
        GaiaSdSystemPara systemPara = gaiaSdSystemParaMapper.getAuthByClientAndBrId(info.getClient(), info.getStoCode(), "RECISALE_LC");
        if (ObjectUtil.isEmpty(systemPara) || "0".equals(systemPara.getGsspPara())) {
            if (StringUtils.isNullOrEmpty(info.getDeparturePlace())
                    || StringUtils.isNullOrEmpty(info.getDepartureDate())
                    || StringUtils.isNullOrEmpty(info.getDepartureTime())
                    || StringUtils.isNullOrEmpty(info.getDepartureTemperature())
                    || info.getBoxQty() == null
                    || StringUtils.isNullOrEmpty(info.getArriveDate())
                    || StringUtils.isNullOrEmpty(info.getArriveTime())
                    || StringUtils.isNullOrEmpty(info.getArriveTemperature())
                    || StringUtils.isNullOrEmpty(info.getTransportOrganization())
                    || StringUtils.isNullOrEmpty(info.getTransportMode())
                    || StringUtils.isNullOrEmpty(info.getTransportEmp())
                    || StringUtils.isNullOrEmpty(info.getTxFollow())
                    || StringUtils.isNullOrEmpty(info.getTempFit())
                    || StringUtils.isNullOrEmpty(info.getTransportTool())
                    || StringUtils.isNullOrEmpty(info.getTransportTime())
                    || StringUtils.isNullOrEmpty(info.getTempType())
            ) {
                throw new BusinessException("冷链信息未填写完整！");
            }
        }
        List<String> psdh = acceptHMapper.getPsdh(info);
        if (ObjectUtil.isNotEmpty(psdh)){
            acceptHMapper.updateColdchainInfo(info, psdh);
        }
        info.setEmp(acceptHMapper.getEmpName(info.getClient(), info.getEmp()));
        redisManager.set(COLDCHAIN_KEY + "-" + info.getClient() + "-" + info.getStoCode() + "-" + info.getJhdh(), JSON.toJSONString(info), 3600 * 24l);
    }

    @Override
    public Map<String, String> showWtpsLabel(GetLoginOutData userInfo) {
        Map<String, String> result = new HashMap<>();
        //是否开启委托配送相应功能 0：关 1：开	  默认或没有值为0
        String isWtpsParam = storeDataMapper.selectWtpsParam(userInfo.getClient(), "WTPS_SET");
        if (StringUtils.isNullOrEmpty(isWtpsParam)) {
            isWtpsParam = storeDataMapper.selectWtpsParam(userInfo.getClient(), "SUPPLIER_CODE");
            if (StringUtils.isNullOrEmpty(isWtpsParam)) {
                isWtpsParam = "0";
            } else {
                isWtpsParam = "1";
            }
        }
        result.put("isWtpsParam", isWtpsParam);
        return result;
    }

    @Override
    public Map<String, Object> selectDsfTrustList(TrustSendInData inData) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> totalMap = new HashMap<>();
        if (ObjectUtil.isEmpty(inData.getGswhStatus())) {
            inData.setGswhStatus("1");
        }
        if (ObjectUtil.isEmpty(inData.getGswhVoucherId())){
            if (ObjectUtil.isEmpty(inData.getStartAcceptDate())){
                throw new BusinessException("请选择起始日期");
            }
            if (ObjectUtil.isEmpty(inData.getEndAcceptDate())){
                throw new BusinessException("请选择结束日期");
            }
        }
        List<TrustSendOutData> outData = acceptWareHouseMapper.selectDsfTrustList(inData);
        BigDecimal gswhAmt = BigDecimal.ZERO;
        BigDecimal gswhQty = BigDecimal.ZERO;
        for (TrustSendOutData data : outData) {
            if (ObjectUtil.isNotEmpty(data.getGswhAmt())) {
                gswhAmt = gswhAmt.add(data.getGswhAmt());
            } else {
                gswhAmt = gswhAmt.add(BigDecimal.ZERO);
            }
            if (ObjectUtil.isNotEmpty(data.getGswhQty())) {
                gswhQty = gswhQty.add(data.getGswhQty());
            } else {
                gswhQty = gswhQty.add(BigDecimal.ZERO);
            }
        }
        totalMap.put("gswhAmt", gswhAmt);
        totalMap.put("gswhQty", gswhQty);
        result.put("totalMap", totalMap);
        if (ObjectUtil.isNotEmpty(outData) && outData.size() > 0) {
            result.put("outData", outData);
        }
        return result;
    }

    @Override
    public List<TrustSendOutData> selectWtpsOrderList(String clientId, String stoCode) {
        return acceptWareHouseMapper.selectWtpsOrderList(clientId, stoCode);
    }

    @Override
    public List<TrustDetailOutData> selectWtpsDetail(AcceptWareHouseInData inData) {
        return acceptWareHouseMapper.selectWtpsDetail(inData.getClientId(), inData.getStoCode(), inData.getPsVoucherId());
    }

    @Override
    public JsonResult saveWtpsDetail(List<AcceptWareSaveWtpsRefuseInData> inDataList, String client, String stoCode, String emp) {
        for (AcceptWareSaveWtpsRefuseInData inData : inDataList) {
            List<TrustSendOutData> data = acceptWareHouseMapper.selectWtpsOrderList(client, stoCode);
            if (CollUtil.isNotEmpty(data) && data.stream().map(TrustSendOutData::getGswhStatus).anyMatch("1"::equals)) {
                throw new BusinessException("已收货的不可以修改");
            }
            if (StrUtil.isNotEmpty(inData.getRefuseQty())
                    && new BigDecimal(inData.getRefuseQty()).compareTo(BigDecimal.ZERO) != 0
                    && StrUtil.isEmpty(inData.getRefuseReason())) {
                throw new BusinessException("拒收原因不可以为空");
            }
            inData.setClient(client);
            inData.setStoCode(stoCode);
            acceptWareHouseMapper.saveWtpsDetail(inData);
            //修改状态
            TrustSendUpdateData statusInData = new TrustSendUpdateData();
            statusInData.setPsVoucherId(inData.getPsVoucherId());
            statusInData.setClientId(client);
            statusInData.setStoCode(stoCode);
            statusInData.setAcceptDate(cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.date(), "yyyyMMdd"));
            statusInData.setAcceptTime(cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.date(), "HHmmss"));
            statusInData.setAcceptEmp(emp);
            acceptWareHouseMapper.modifyWtpsOrderStatus(statusInData);
            insertMessageForRefuseWtps(client, stoCode, inData.getPsVoucherId());
        }
        return JsonResult.success(null, "修改成功");
    }

    private void insertMessageForRefuseWtps(String client, String stoCode, String psVoucherId) {
        //增加首页消息提醒
        GaiaSdMessage sdMessage = new GaiaSdMessage();
        Date now = new Date();
        sdMessage.setClient(client);
        sdMessage.setGsmId(stoCode);//针对门店
        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(client, "store");
        sdMessage.setGsmVoucherId(voucherId);//消息流水号
        sdMessage.setGsmPage(SdMessageTypeEnum.CONSIGNMENT_OF_CLEARANCE_WAS_REJECTED.page);
        sdMessage.setGsmType(SdMessageTypeEnum.CONSIGNMENT_OF_CLEARANCE_WAS_REJECTED.code);
        sdMessage.setGsmRemark("【补货单<font color=\"#FF0000\">" + psVoucherId + "</font>已被拒绝，请尽快处理！】");
        sdMessage.setGsmFlag("N");//是否查看
        sdMessage.setGsmWarningDay(psVoucherId);//单号
        sdMessage.setGsmPlatForm("WEB");//消息渠道
        sdMessage.setGsmDeleteFlag("0");
        sdMessage.setGsmArriveDate(com.gys.util.DateUtil.formatDate(now));
        sdMessage.setGsmArriveTime(com.gys.util.DateUtil.dateToString(now, "HHmmss"));
        gaiaSdMessageMapper.insertSelective(sdMessage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approveWtps(AcceptWareHouseInData inData, GetLoginOutData userInfo) {
        Map<String, String> checkOrder = acceptWareHouseMapper.checkWtpsAcceptPro(inData);
        if (ObjectUtil.isNotEmpty(checkOrder)) {
            TrustSendUpdateData mdata = new TrustSendUpdateData();
            mdata.setClientId(inData.getClientId());
            mdata.setStoCode(inData.getStoCode());
            mdata.setPsVoucherId(inData.getPsVoucherId());
            //收货单不存在
            if (checkOrder.get("hasAcceptOrder").equals("1")) {
                GetAcceptInData data = new GetAcceptInData();
                //如果冷链参数 开启 且 温度数据 有值时 插入温度和湿度记录功能
                //冷链开关
                String coldChainParam = storeDataMapper.selectWtpsParam(inData.getClientId(), "COLD_CHAIN");
                if ("1".equals(coldChainParam) && ObjectUtil.isNotEmpty(inData.getColdInfo())) {
                    GaiaColdTransportationInfo coldInfo = inData.getColdInfo();
                    data.setGsahTransportMode(coldInfo.getGsahTransportMode());
                    data.setGsahBoxQty(coldInfo.getGsahBoxQty());
                    data.setGsahInsulationMode(coldInfo.getGsahInsulationMode());
                    data.setGsahTxFollow(coldInfo.getGsahTxFollow());
                    data.setGsahDepartureDate(coldInfo.getGsahDepartureDate());
                    data.setGsahTempFit(coldInfo.getGsahTempFit());
                    data.setGsahArriveDate(coldInfo.getGsahArriveDate());
                    data.setGsahTransportEmp(coldInfo.getGsahTransportEmp());
                    data.setGsahDepartureTemperature(coldInfo.getGsahDepartureTemperature());
                    data.setGsahArriveTemperature(coldInfo.getGsahArriveTemperature());
                    data.setColdFlag(coldChainParam);
                }
                data.setClientId(inData.getClientId());
                data.setGsahBrId(inData.getStoCode());
                data.setGsahPsVoucherId(checkOrder.get("psVoucherId"));
                data.setGsahFrom(checkOrder.get("proFrom"));
                data.setGsahTo(checkOrder.get("proTo"));
                data.setGsahStatus("1");
                data.setGsahEmp(inData.getUserId());
                data.setRefuseInDataList(inData.getRefuseInDataList());
                mdata.setAcceptEmp(inData.getUserId());
                String voucherId = this.saveWtps(data);
                if (ObjectUtil.isNotEmpty(voucherId)) {
                    mdata.setVoucherId(voucherId);
                    String isExamineAuto = storeDataMapper.selectStoPriceComparison(inData.getClientId(), inData.getStoCode(), "EXAMINE_AUTO");
                    if (ObjectUtil.isEmpty(isExamineAuto)) {
                        isExamineAuto = "0";
                    }
                    if (isExamineAuto.equals("1")) {
                        GetExamineInData examineInData = new GetExamineInData();
                        examineInData.setClientId(data.getClientId());
                        examineInData.setGsehBrId(data.getGsahBrId());
                        examineInData.setStoreCode(data.getGsahBrId());
                        examineInData.setGsehVoucherAcceptId(voucherId);
                        examineInData.setGsedEmp(inData.getUserId());
                        List<GetExamineDetailInData> list = new ArrayList<>();
                        for (GetAcceptDetailOutData data1 : data.getAcceptDetailInDataList()) {
                            GetExamineDetailInData item1 = new GetExamineDetailInData();
                            item1.setGsedProId(data1.getGsadProId());
                            item1.setGsedBatchNo(data1.getGsadBatchNo());
                            item1.setGsedBatch(data1.getGsadBatch());
                            item1.setGsedRecipientQty(data1.getGsadRecipientQty());
                            item1.setGsedQualifiedQty(data1.getGsadInvoicesQty());
                            item1.setGsedUnqualifiedQty(data1.getGsadRefuseQty());
                            item1.setGsedResult("合格");
                            item1.setGsedValidDate(data1.getGsadValidDate());
                            item1.setGsedStockStatus(data1.getGsadStockStatus());
                            item1.setGsedSerial(data1.getGsadSerial());
                            list.add(item1);
                        }
                        if (list.size() > 0 && list != null) {
                            examineInData.setExamineDetailInDataList(list);
                        }
                        this.examineService.approveEx(examineInData, userInfo);
                    }
                }
            } else {
                mdata.setVoucherId(checkOrder.get("acceptVoucherId"));
                if (checkOrder.get("gsahStatus").equals("0")) {
                    GetAcceptInData i = new GetAcceptInData();
                    i.setGsahEmp(inData.getUserId());
                    i.setClientId(inData.getClientId());
                    i.setGsahBrId(inData.getStoCode());
                    i.setStoreCode(inData.getStoCode());
                    i.setGsahPsVoucherId(checkOrder.get("psVoucherId"));
                    i.setGsahDate(CommonUtil.getyyyyMMdd());
                    i.setGsahVoucherId(checkOrder.get("acceptVoucherId"));
                    this.acceptWareHouseMapper.modifyAcceptStatus(i);
                    List<Map<String, String>> examineList = acceptWareHouseMapper.checkExaminePro(i);
                    if (examineList.size() > 0 && examineList != null) {
                        for (Map<String, String> examineMap : examineList) {
                            if (examineMap.get("gsehStatus").equals("0")) {
                                acceptWareHouseMapper.modifyExamineStatus(i);
                            }
                        }
                    } else {
                        GetExamineInData examineData = new GetExamineInData();
                        examineData.setClientId(i.getClientId());
                        examineData.setStoreCode(i.getStoreCode());
                        examineData.setGsehBrId(i.getGsahBrId());
                        examineData.setGsehVoucherAcceptId(i.getGsahVoucherId());
                        examineData.setGsedEmp(inData.getUserId());
                        examineData.setSendType("1");
                        this.examineService.save(examineData);
                    }
                } else {
                    GetAcceptInData i = new GetAcceptInData();
                    i.setGsahEmp(inData.getUserId());
                    i.setClientId(inData.getClientId());
                    i.setGsahBrId(inData.getStoCode());
                    i.setGsahPsVoucherId(checkOrder.get("psVoucherId"));
//                        i.setGsahDate(CommonUtil.getyyyyMMdd());
                    i.setGsahVoucherId(checkOrder.get("acceptVoucherId"));
                    this.acceptWareHouseMapper.modifyAcceptStatus(i);
                    List<Map<String, String>> examineList = acceptWareHouseMapper.checkExaminePro(i);
                    if (examineList.size() > 0 && examineList != null) {
                        for (Map<String, String> examineMap : examineList) {
                            if (examineMap.get("gsehStatus").equals("0")) {
                                acceptWareHouseMapper.modifyExamineStatus(i);
                            }
                        }
                    } else {
                        GetExamineInData examineData = new GetExamineInData();
                        examineData.setClientId(i.getClientId());
                        examineData.setStoreCode(i.getGsahBrId());
                        examineData.setGsehBrId(i.getGsahBrId());
                        examineData.setGsehVoucherAcceptId(i.getGsahVoucherId());
                        examineData.setGsedEmp(inData.getUserId());
                        examineData.setSendType("1");
                        this.examineService.save(examineData);
                    }
                }
            }
            mdata.setAcceptDate(CommonUtil.getyyyyMMdd());
            mdata.setAcceptTime(CommonUtil.getHHmmss());
            this.acceptWareHouseMapper.modifyWtpsOrderStatus(mdata);
        } else {
            throw new BusinessException("委托配送单不存在，请核查后操作！");
        }
    }

    public String save(GetAcceptInData inData, ColdchainInfo info) {
        String gsahVoucherId = "";
        //查询配送单主表
        Example example = new Example(GaiaWmsDiaoboZ.class);
//        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId());
//        GaiaWmsDiaoboZ diaoboZ = (GaiaWmsDiaoboZ)this.diaoboZMapper.selectOneByExample(example);
//        String voucherId = "PS1" + inData.getGsahPsVoucherId().substring(2);
        String voucherId = acceptWareHouseMapper.selectNextPsVoucherId(inData.getClientId());
        //查询配送单明细表
        Example exampleM = new Example(GaiaWmsDiaoboM.class);
        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId()).andEqualTo("proSite", inData.getDcCode());
        List<GaiaWmsDiaoboM> diaoboMList = this.diaoboMMapper.selectByExample(exampleM);
        int i = 1;
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal count = BigDecimal.ZERO;
        List<GetAcceptDetailOutData> acceptDetailInDataList = new ArrayList<>();
        for (GaiaWmsDiaoboM diaoboM : diaoboMList) {
            if (diaoboM.getWmGzsl().compareTo(new BigDecimal(0)) == 1) {
                //商品主数据明细表
                Example examplePro = new Example(GaiaProductBusiness.class);
                examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", diaoboM.getWmSpBm()).andEqualTo("proSite", inData.getGsahBrId());
                GaiaProductBusiness productBusiness = (GaiaProductBusiness) this.productBusinessMapper.selectOneByExample(examplePro);
                //商品批次明细表
                Example exampleBat = new Example(GaiaBatchInfo.class);
                exampleBat.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batBatch", diaoboM.getWmPch()).andEqualTo("batProCode", diaoboM.getWmSpBm());
                List<GaiaBatchInfo> batchInfo = this.batchInfoMapper.selectByExample(exampleBat);
                //保存收货明细表
                GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
                acceptD.setClientId(inData.getClientId());
                acceptD.setGsadVoucherId(voucherId);
                acceptD.setGsadBrId(inData.getGsahBrId());
                acceptD.setGsadProId(diaoboM.getWmSpBm());
                acceptD.setGsadSerial(String.valueOf(i++));
                acceptD.setGsadRefuseQty("0");
                acceptD.setGsadRecipientQty(diaoboM.getWmGzsl() == null ? "0" : diaoboM.getWmGzsl().toString());
                acceptD.setGsadInvoicesQty(diaoboM.getWmGzsl() == null ? "0" : diaoboM.getWmGzsl().toString());
                acceptD.setGsadBatch(diaoboM.getWmPch());
                acceptD.setGsadBatchNo(((GaiaBatchInfo) batchInfo.get(0)).getBatBatchNo());
                acceptD.setGsadValidDate(((GaiaBatchInfo) batchInfo.get(0)).getBatExpiryDate());
                acceptD.setGsadMadeDate(batchInfo.get(0).getBatProductDate());
                acceptD.setGsadStockStatus(diaoboM.getWmKcztBh());
                acceptD.setGsadRowRemark("");
                acceptD.setGsadDate(CommonUtil.getyyyyMMdd());
                acceptD.setGsadAcceptPrice(diaoboM.getWmCkj().toString());
                this.acceptDMapper.insert(acceptD);
                amount = amount.add(diaoboM.getWmCkj().multiply(diaoboM.getWmGzsl()));
                count = count.add(diaoboM.getWmGzsl() == null ? BigDecimal.ZERO : diaoboM.getWmGzsl());
                GetAcceptDetailOutData item = new GetAcceptDetailOutData();
                item.setGsadProId(acceptD.getGsadProId());
                item.setGsadBatchNo(acceptD.getGsadBatchNo());
                item.setGsadBatch(acceptD.getGsadBatch());
                item.setGsadInvoicesQty(acceptD.getGsadInvoicesQty());
                item.setGsadRefuseQty(acceptD.getGsadRefuseQty());
                item.setGsadRecipientQty(acceptD.getGsadRecipientQty());
                item.setGsadValidDate(acceptD.getGsadValidDate());
                item.setGsadStockStatus(acceptD.getGsadStockStatus());
                item.setGsadSerial(acceptD.getGsadSerial());
                acceptDetailInDataList.add(item);
            }
        }
        if (acceptDetailInDataList.size() > 0 && acceptDetailInDataList != null) {
            inData.setAcceptDetailInDataList(acceptDetailInDataList);
            //保存收货主表
            GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
            if (!ObjectUtils.isEmpty(info)) {
                acceptH.setGsahDeparturePlace(info.getDeparturePlace());
                acceptH.setGsahDepartureDate(info.getDepartureDate());
                acceptH.setGsahDepartureTime(info.getDepartureTime());
                acceptH.setGsahDepartureTemperature(info.getDepartureTemperature());
                acceptH.setGsahBoxQty(info.getBoxQty());
                acceptH.setGsahArriveDate(info.getArriveDate());
                acceptH.setGsahArriveTime(info.getArriveTime());
                acceptH.setGsahArriveTemperature(info.getArriveTemperature());
                acceptH.setGsahTransportOrganization(info.getTransportOrganization());
                acceptH.setGsahTransportMode(info.getTransportMode());
                acceptH.setGsahTransportEmp(info.getTransportEmp());
                acceptH.setGsahTxFollow(info.getTxFollow());
                acceptH.setGsahTempFit(info.getTempFit());
                acceptH.setGsahEmp(info.getEmp());
                acceptH.setGsahTransportCardId(info.getTransportCardId());
                acceptH.setGsahTransportTool(info.getTransportTool());
                acceptH.setGsahTransportTime(info.getTransportTime());
                acceptH.setGsahTempType(info.getTempType());
                acceptH.setGsahColdchainFlag("1");
                info.setEmp(acceptHMapper.getEmpName(info.getClient(), info.getEmp()));
                redisManager.set(COLDCHAIN_KEY + "-" + info.getClient() + "-" + info.getStoCode() + "-" + info.getJhdh(), JSON.toJSONString(info), 3600 * 24l);
            }
            acceptH.setClientId(inData.getClientId());
            acceptH.setGsahBrId(inData.getGsahBrId());
            acceptH.setGsahVoucherId(voucherId);
            acceptH.setGsahStatus(inData.getGsahStatus());
            acceptH.setGsahType("1");
            acceptH.setGsahAcceptType("3");
            acceptH.setGsahPsVoucherId(inData.getGsahPsVoucherId());
            acceptH.setGsahRemaks("");
            acceptH.setGsahFrom(inData.getGsahFrom());
            acceptH.setGsahTo(inData.getGsahTo());
            acceptH.setGsahTotalAmt(amount);
            acceptH.setGsahEmp(inData.getGsahEmp());
            acceptH.setGsahDate(CommonUtil.getyyyyMMdd());
            acceptH.setGsahTotalQty(count.toString());
            this.acceptHMapper.insert(acceptH);
            gsahVoucherId = acceptH.getGsahVoucherId();
        }
        return gsahVoucherId;
    }

    public String saveByRefuseQty(String pickProId,GetAcceptInData inData, GetLoginOutData userInfo, ColdchainInfo info){
        String gsahVoucherId = "";
        String voucherId = acceptWareHouseMapper.selectNextPsVoucherId(inData.getClientId());
        //查询配送单明细表
        int i = 1;
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal count = BigDecimal.ZERO;
        List<GetAcceptDetailOutData> acceptDetailInDataList = new ArrayList<>();
        Example exampleM = new Example(GaiaWmsDiaoboM.class);
        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId()).andEqualTo("proSite", inData.getDcCode());
        List<GaiaWmsDiaoboM> diaoboMList = this.diaoboMMapper.selectByExample(exampleM);
//        Map<String,AcceptWareHouseProDetailData> inDataMap = inData.getAcceptWareHouseProDetailList().stream().collect(Collectors.toMap(AcceptWareHouseProDetailData::getProCode, x -> x, (a, b) -> b));
        Map<String,AcceptWareHouseProDetailData> inDataMap = inData.getAcceptWareHouseProDetailList().stream().collect(Collectors.toMap(r -> r.getProCode() + "-" + (org.apache.commons.lang3.StringUtils.isNotEmpty(r.getBatBatch())? r.getBatBatch() : "") + "-" + (org.apache.commons.lang3.StringUtils.isNotEmpty(r.getBatchNo())? r.getBatchNo() : ""), x -> x, (a, b) -> b));
        List<CancelStockDetail> list = new ArrayList<>();
        List<GetWfDistributionReturnInData> wfDetailList = new ArrayList<>();
        for (GaiaWmsDiaoboM diaoboM : diaoboMList) {
            if (diaoboM.getWmGzsl().compareTo(new BigDecimal(0)) == 1) {
                //商品主数据明细表
//                Example examplePro = new Example(GaiaProductBusiness.class);
//                examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", diaoboM.getWmSpBm()).andEqualTo("proSite", inData.getGsahBrId());
//                GaiaProductBusiness productBusiness = (GaiaProductBusiness) this.productBusinessMapper.selectOneByExample(examplePro);
                //商品批次明细表
                Example exampleBat = new Example(GaiaBatchInfo.class);
                exampleBat.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batBatch", diaoboM.getWmPch()).andEqualTo("batProCode", diaoboM.getWmSpBm());
                List<GaiaBatchInfo> batchInfo = this.batchInfoMapper.selectByExample(exampleBat);
                //保存收货明细表
                GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
                acceptD.setClientId(inData.getClientId());
                acceptD.setGsadVoucherId(voucherId);
                acceptD.setGsadBrId(inData.getGsahBrId());
                acceptD.setGsadProId(diaoboM.getWmSpBm());
                acceptD.setGsadSerial(String.valueOf(i++));
                acceptD.setGsadRecipientQty(diaoboM.getWmGzsl() == null ? "0" : diaoboM.getWmGzsl().toString());
                acceptD.setGsadInvoicesQty(diaoboM.getWmGzsl() == null ? "0" : diaoboM.getWmGzsl().toString());
                acceptD.setGsadBatch(diaoboM.getWmPch());
                acceptD.setGsadBatchNo(((GaiaBatchInfo) batchInfo.get(0)).getBatBatchNo());
                String refuseQty = "0";
                String key = diaoboM.getWmSpBm()+"-"+(org.apache.commons.lang3.StringUtils.isNotEmpty(diaoboM.getWmPch())? diaoboM.getWmPch() : "")+"-"+(org.apache.commons.lang3.StringUtils.isNotEmpty(acceptD.getGsadBatchNo())? acceptD.getGsadBatchNo() : "");
                AcceptWareHouseProDetailData detailData = inDataMap.get(key);
                if (ObjectUtil.isNotEmpty(detailData)) {
                    if (ObjectUtil.isNotEmpty(inDataMap.get(key).getRefuseQty())) {
                        refuseQty = inDataMap.get(key).getRefuseQty();
                    }
                }
                acceptD.setGsadRefuseQty(refuseQty);
                acceptD.setGsadValidDate(((GaiaBatchInfo) batchInfo.get(0)).getBatExpiryDate());
                acceptD.setGsadMadeDate(batchInfo.get(0).getBatProductDate());
                acceptD.setGsadStockStatus(diaoboM.getWmKcztBh());
                acceptD.setGsadRowRemark("");
                acceptD.setGsadDate(CommonUtil.getyyyyMMdd());
                acceptD.setGsadAcceptPrice(diaoboM.getWmCkj().toString());
                // 不为空保存
                if (ObjectUtil.isNotEmpty(detailData)&& org.apache.commons.lang3.StringUtils.isNotBlank(detailData.getRejecteReason())) {
                    acceptD.setGsadRefuseReason(detailData.getRejecteReason());
                }
                this.acceptDMapper.insert(acceptD);
                if (org.apache.commons.lang3.StringUtils.isNotEmpty(acceptD.getGsadRefuseQty())
                        && new BigDecimal(acceptD.getGsadRefuseQty()).compareTo(BigDecimal.ZERO) == 1){
                    CancelStockDetail cancelStockDetail = new CancelStockDetail();
                    cancelStockDetail.setClientId(inData.getClientId());
                    cancelStockDetail.setCancelQty(acceptD.getGsadRefuseQty());
                    cancelStockDetail.setCancelSerial(acceptD.getGsadSerial());
                    if (ObjectUtil.isNotEmpty(detailData)) {
                        cancelStockDetail.setCancelReason(detailData.getRejecteReason());
                    }
                    cancelStockDetail.setProBatch(diaoboM.getWmPch());
                    cancelStockDetail.setProCode(diaoboM.getWmSpBm());
                    cancelStockDetail.setProBatchNo(acceptD.getGsadBatchNo());
                    cancelStockDetail.setStoCode(inData.getGsahBrId());
                    cancelStockDetail.setTaxPrice(diaoboM.getWmCkj());
                    list.add(cancelStockDetail);
                    //审批流
                    GetWfDistributionReturnInData wfDetail = new GetWfDistributionReturnInData();
                    wfDetail.setProSite(inData.getGsahBrId());
                    wfDetail.setIndex(acceptD.getGsadSerial());
                    wfDetail.setWsdSpBm(diaoboM.getWmSpBm());
//                    wfDetail.setWsdScrq(CommonUtil.getyyyyMMdd());
                    wfDetail.setWsdTkyy("仓库配送差异");
                    wfDetail.setWsdSqsl(acceptD.getGsadRefuseQty());
                    wfDetail.setWsdYpsdh(inData.getGsahPsVoucherId());
                    wfDetail.setWsdSpCj(detailData.getFactoryName());
                    wfDetail.setWsdSpGg(detailData.getProSpecs());
                    wfDetail.setWsdSpMc(detailData.getProName());
                    wfDetail.setWsdPh(acceptD.getGsadBatchNo());
                    wfDetail.setWsdYjhr(userInfo.getUserId());
                    wfDetailList.add(wfDetail);
                }
                amount = amount.add(diaoboM.getWmCkj().multiply(diaoboM.getWmGzsl()));
                count = count.add(diaoboM.getWmGzsl() == null ? BigDecimal.ZERO : diaoboM.getWmGzsl());
                GetAcceptDetailOutData item = new GetAcceptDetailOutData();
                item.setGsadProId(acceptD.getGsadProId());
                item.setGsadBatchNo(acceptD.getGsadBatchNo());
                item.setGsadBatch(acceptD.getGsadBatch());
                item.setGsadInvoicesQty(acceptD.getGsadInvoicesQty());
                item.setGsadRefuseQty(acceptD.getGsadRefuseQty());
                item.setGsadRecipientQty(acceptD.getGsadRecipientQty());
                item.setGsadValidDate(acceptD.getGsadValidDate());
                item.setGsadStockStatus(acceptD.getGsadStockStatus());
                item.setGsadSerial(acceptD.getGsadSerial());
                acceptDetailInDataList.add(item);
            }
        }
        if (acceptDetailInDataList.size() > 0 && acceptDetailInDataList != null) {
            inData.setAcceptDetailInDataList(acceptDetailInDataList);
            //保存收货主表
            GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
            if (!ObjectUtils.isEmpty(info)) {
                acceptH.setGsahDeparturePlace(info.getDeparturePlace());
                acceptH.setGsahDepartureDate(info.getDepartureDate());
                acceptH.setGsahDepartureTime(info.getDepartureTime());
                acceptH.setGsahDepartureTemperature(info.getDepartureTemperature());
                acceptH.setGsahBoxQty(info.getBoxQty());
                acceptH.setGsahArriveDate(info.getArriveDate());
                acceptH.setGsahArriveTime(info.getArriveTime());
                acceptH.setGsahArriveTemperature(info.getArriveTemperature());
                acceptH.setGsahTransportOrganization(info.getTransportOrganization());
                acceptH.setGsahTransportMode(info.getTransportMode());
                acceptH.setGsahTransportEmp(info.getTransportEmp());
                acceptH.setGsahTxFollow(info.getTxFollow());
                acceptH.setGsahTempFit(info.getTempFit());
                acceptH.setGsahEmp(info.getEmp());
                acceptH.setGsahTransportCardId(info.getTransportCardId());
                acceptH.setGsahTransportTool(info.getTransportTool());
                acceptH.setGsahTransportTime(info.getTransportTime());
                acceptH.setGsahTempType(info.getTempType());
                acceptH.setGsahColdchainFlag("1");
                info.setEmp(acceptHMapper.getEmpName(info.getClient(), info.getEmp()));
                redisManager.set(COLDCHAIN_KEY + "-" + info.getClient() + "-" + info.getStoCode() + "-" + info.getJhdh(), JSON.toJSONString(info), 3600 * 24l);
            }
            acceptH.setClientId(inData.getClientId());
            acceptH.setGsahBrId(inData.getGsahBrId());
            acceptH.setGsahVoucherId(voucherId);
            acceptH.setGsahStatus(inData.getGsahStatus());
            acceptH.setGsahType("1");
            acceptH.setGsahAcceptType("3");
            acceptH.setGsahPsVoucherId(inData.getGsahPsVoucherId());
            acceptH.setGsahRemaks("");
            acceptH.setGsahFrom(inData.getGsahFrom());
            acceptH.setGsahTo(inData.getGsahTo());
            acceptH.setGsahTotalAmt(amount);
            acceptH.setGsahEmp(inData.getGsahEmp());
            acceptH.setGsahDate(CommonUtil.getyyyyMMdd());
            acceptH.setGsahTotalQty(count.toString());
            this.acceptHMapper.insert(acceptH);
            gsahVoucherId = acceptH.getGsahVoucherId();
        }
        if (ObjectUtil.isNotEmpty(list) && list.size() > 0){
//            String cancelVoucherId =  commonMapper.selectNextCode(inData.getClientId(),"KD",7);
            String cancelVoucherId = commonService.selectNextCancelId(inData.getClientId());
            CancelStockData cancelStockData = new CancelStockData();
            cancelStockData.setClientId(inData.getClientId());
            cancelStockData.setStoCode(inData.getGsahBrId());
            cancelStockData.setDcCode(inData.getDcCode());
            cancelStockData.setCancelType("1");
            cancelStockData.setCancelReason("仓库配送差异，单号：" + pickProId);
            cancelStockData.setCreateDate(CommonUtil.getyyyyMMdd());
            cancelStockData.setCreateTime(CommonUtil.getHHmmss());
            cancelStockData.setCancelStockCode(cancelVoucherId);
            cancelStockData.setOrderStatus("0");
            cancelStockData.setCreateEmp(userInfo.getUserId());
            for (CancelStockDetail sto : list) {
                sto.setCancelStockCode(cancelVoucherId);
            }
            cancelStockMapper.batchCancelDetail(list);
            cancelStockMapper.addCancelStock(cancelStockData);
            GetWfCreateInData wfInData = new GetWfCreateInData();
            wfInData.setToken(userInfo.getToken());
            wfInData.setWfTitle("配送差异，单号：" + pickProId);
            wfInData.setWfDescription("仓库配送差异，发送工作流任务审批");
            wfInData.setWfOrder(cancelVoucherId);
            wfInData.setWfSite(inData.getGsahBrId());
            wfInData.setWfDefineCode("GAIA_WSD_1003");
            for (GetWfDistributionReturnInData data :wfDetailList) {
                data.setWsdTksqdh(cancelVoucherId);
            }
            wfInData.setWfDetail(wfDetailList);
            String result = authService.createWorkflow(wfInData);
            log.info("请求创建工作流：{}", JSON.toJSONString(wfInData));
            com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(result);
            log.info("工作流回调：{}", JSON.toJSONString(result));
            if (jsonObject.containsKey("code") && !"0".equals(jsonObject.getString("code"))) {
                throw new BusinessException(jsonObject.getString("message"));
            }
        }
        return gsahVoucherId;
    }

    public String saveWtps(GetAcceptInData inData) {
        String gsahVoucherId = "";
        //查询配送单主表
//        Example example = new Example(GaiaWmsDiaoboZ.class);
//        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId());
//        GaiaWmsDiaoboZ diaoboZ = (GaiaWmsDiaoboZ)this.diaoboZMapper.selectOneByExample(example);
//        String voucherId = "PS1" + inData.getGsahPsVoucherId().substring(2);
        String voucherId = acceptWareHouseMapper.selectNextPsVoucherId(inData.getClientId());
        //查询委托配送单明细表
//        Example exampleM = new Example(GaiaWmsDiaoboM.class);
//        exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("wmPsdh", inData.getGsahPsVoucherId());
//        List<GaiaWmsDiaoboM> diaoboMList = this.diaoboMMapper.selectByExample(exampleM);
        List<TrustDetailOutData> detailOutDataList = acceptWareHouseMapper.selectWtpsDetail(inData.getClientId(), inData.getGsahBrId(), inData.getGsahPsVoucherId());
        List<AcceptWareSaveWtpsRefuseInData> refuseInDataList = inData.getRefuseInDataList();
        for (TrustDetailOutData detail : detailOutDataList) {
            if ("1".equals(detail.getHasDsfCode())) {
                throw new BusinessException(detail.getGswdProCode() + "未匹配到对应的商品，请维护商品后再进行审核！");
            }

            if (CollUtil.isNotEmpty(refuseInDataList)) {
                for (AcceptWareSaveWtpsRefuseInData refuseInData : refuseInDataList) {
                    if (Objects.nonNull(refuseInData)
                            && org.apache.commons.lang3.StringUtils.isNotEmpty(refuseInData.getProCode())
                            && org.apache.commons.lang3.StringUtils.isNotEmpty(refuseInData.getGswdBatch())) {
                        if (refuseInData.getProCode().equals(detail.getProCode()) &&
                                refuseInData.getGswdBatch().equals(detail.getGswdBatch())
                        ) {

                            detail.setRefuseQty(org.apache.commons.lang3.StringUtils.isEmpty(refuseInData.getRefuseQty())?BigDecimal.ZERO:new BigDecimal(refuseInData.getRefuseQty()));
                            if (detail.getRefuseQty().compareTo(BigDecimal.ZERO) == 1 &&
                                    org.apache.commons.lang3.StringUtils.isEmpty(refuseInData.getRefuseReason())
                            ) {
                                throw new BusinessException("拒收原因必填");
                            }
                            detail.setRefuseReason(refuseInData.getRefuseReason());
                            //如果拒收数量大于0，生成首页消息
                            if (detail.getRefuseQty().compareTo(BigDecimal.ZERO) == 1) {
                                this.insertMessageForRefuseWtps(inData.getClientId(),inData.getGsahBrId(),refuseInData.getPsVoucherId());
                            }
                        }
                    }
                }
            }
        }
        int i = 1;
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal count = BigDecimal.ZERO;
        List<GetAcceptDetailOutData> acceptDetailInDataList = new ArrayList<>();
        List<GetPoDetailInData> poDetailList = new ArrayList<>();
        for (TrustDetailOutData detail : detailOutDataList) {
            if (detail.getGswdQty().compareTo(new BigDecimal(0)) == 1) {
                //商品主数据明细表
                Example examplePro = new Example(GaiaProductBusiness.class);
                examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", detail.getProCode()).andEqualTo("proSite", inData.getGsahBrId());
//                GaiaProductBusiness productBusiness = (GaiaProductBusiness) this.productBusinessMapper.selectOneByExample(examplePro);
//                //商品批次明细表
//                Example exampleBat = new Example(GaiaBatchInfo.class);
//                exampleBat.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batBatch",detail.getGswdBatchNo()).andEqualTo("batProCode", detail.getProCode());
//                List<GaiaBatchInfo> batchInfo = this.batchInfoMapper.selectByExample(exampleBat);
                //保存收货明细表
                GaiaSdAcceptD acceptD = new GaiaSdAcceptD();
                acceptD.setClientId(inData.getClientId());
                acceptD.setGsadVoucherId(voucherId);
                acceptD.setGsadBrId(inData.getGsahBrId());
                acceptD.setGsadProId(detail.getProCode());
                acceptD.setGsadSerial(String.valueOf(i++));
                acceptD.setGsadRefuseQty(detail.getRefuseQty()==null?"0":detail.getRefuseQty().toString());
                acceptD.setGsadRefuseReason(detail.getRefuseReason());
                acceptD.setGsadRecipientQty(detail.getGswdQty() == null ? "0" : detail.getGswdQty().toString());
                acceptD.setGsadInvoicesQty(detail.getGswdQty() == null ? "0" : detail.getGswdQty().toString());
                acceptD.setGsadBatch(detail.getGswdBatch());
//                acceptD.setGsadBatchNo(((GaiaBatchInfo) batchInfo.get(0)).getBatBatchNo());
//                acceptD.setGsadValidDate(((GaiaBatchInfo) batchInfo.get(0)).getBatExpiryDate());
                acceptD.setGsadBatchNo(detail.getGswdBatchNo());
                acceptD.setGsadValidDate(detail.getGswdExpiryDate());
                acceptD.setGsadMadeDate(detail.getGswdMadeDate());
                acceptD.setGsadStockStatus("1000");
                acceptD.setGsadRowRemark("");
                acceptD.setGsadDate(CommonUtil.getyyyyMMdd());
                acceptD.setGsadAcceptPrice(detail.getPsPrice().toString());
                this.acceptDMapper.insert(acceptD);
                amount = amount.add(detail.getPsAmt());
                //添加采购单明细表信息
                GetPoDetailInData detailInData = new GetPoDetailInData();
                detailInData.setProCode(acceptD.getGsadProId());
                detailInData.setProQty(acceptD.getGsadInvoicesQty());
                detailInData.setProPrice(detail.getPsPrice().toString());
                detailInData.setProUnit(detail.getGswdUnit());
                detailInData.setProRate(detail.getGswdInputTax());
                poDetailList.add(detailInData);
                //添加收货单明细表信息
                GetAcceptDetailOutData item = new GetAcceptDetailOutData();
                item.setGsadProId(acceptD.getGsadProId());
                item.setGsadBatchNo(acceptD.getGsadBatchNo());
                item.setGsadBatch(acceptD.getGsadBatch());
                item.setGsadInvoicesQty(acceptD.getGsadInvoicesQty());
                item.setGsadRefuseQty(acceptD.getGsadRefuseQty());
                item.setGsadRecipientQty(acceptD.getGsadRecipientQty());
                item.setGsadValidDate(acceptD.getGsadValidDate());
                item.setGsadStockStatus(acceptD.getGsadStockStatus());
                item.setGsadSerial(acceptD.getGsadSerial());
                count = count.add(detail.getGswdQty() == null ? BigDecimal.ZERO : detail.getGswdQty());
                acceptDetailInDataList.add(item);
            }
        }
        String poVoucherId = "";
        //添加采购单
        if (ObjectUtil.isNotEmpty(poDetailList) && poDetailList.size() > 0) {
            GetPoInData poInData = new GetPoInData();
            poInData.setStoreId(inData.getGsahBrId());
            poInData.setClientId(inData.getClientId());
            poInData.setStoreCode(inData.getGsahFrom());
            poInData.setUserId(inData.getGsahEmp());
            poInData.setPoDetailList(poDetailList);
            poVoucherId = this.insertPo(poInData);
        }
        if (ObjectUtil.isNotEmpty(acceptDetailInDataList) && acceptDetailInDataList.size() > 0) {
            inData.setAcceptDetailInDataList(acceptDetailInDataList);
            //保存收货主表
            GaiaSdAcceptH acceptH = new GaiaSdAcceptH();
            acceptH.setClientId(inData.getClientId());
            acceptH.setGsahBrId(inData.getGsahBrId());
            acceptH.setGsahVoucherId(voucherId);
            acceptH.setGsahStatus(inData.getGsahStatus());
            acceptH.setGsahType("2");
            acceptH.setGsahAcceptType("2");
            acceptH.setGsahPsVoucherId(inData.getGsahPsVoucherId());
            acceptH.setGsahRemaks("");
            acceptH.setGsahPoid(poVoucherId);
            acceptH.setGsahFrom(inData.getGsahFrom());
            acceptH.setGsahTo(inData.getGsahTo());
            acceptH.setGsahTotalAmt(amount);
            acceptH.setGsahEmp(inData.getGsahEmp());
            acceptH.setGsahDate(CommonUtil.getyyyyMMdd());
            acceptH.setGsahTotalQty(count.toString());
            if ("1".equals(inData.getColdFlag())) {
                log.info("<冷链新增委托配送审核><审核><请求参数：%s>", JSONUtil.toJsonStr(inData));
                acceptH.setGsahTransportMode(inData.getGsahTransportMode());
                acceptH.setGsahBoxQty(inData.getGsahBoxQty());
                acceptH.setGsahInsulationMode(inData.getGsahInsulationMode());
                acceptH.setGsahTxFollow(inData.getGsahTxFollow());
                acceptH.setGsahDepartureDate(StrUtil.isBlank(inData.getGsahDepartureDate())?"":inData.getGsahDepartureDate().substring(0,8));
                acceptH.setGsahDepartureTime(StrUtil.isBlank(inData.getGsahDepartureDate())?"":inData.getGsahDepartureDate().substring(8));
                acceptH.setGsahTempFit(inData.getGsahTempFit());
                acceptH.setGsahArriveDate(StrUtil.isBlank(inData.getGsahArriveDate())?"":inData.getGsahArriveDate().substring(0,8));
                acceptH.setGsahArriveTime(StrUtil.isBlank(inData.getGsahArriveDate())?"":inData.getGsahArriveDate().substring(8));
                acceptH.setGsahTransportEmp(inData.getGsahTransportEmp());
                acceptH.setGsahDepartureTemperature(inData.getGsahDepartureTemperature());
                acceptH.setGsahArriveTemperature(inData.getGsahArriveTemperature());
            }
            this.acceptHMapper.insert(acceptH);
            gsahVoucherId = acceptH.getGsahVoucherId();
        }
        return gsahVoucherId;
    }

    public String insertPo(GetPoInData inData) {
        String voucherId = poHeaderMapper.selectNextVoucherId(inData.getClientId());
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!日志{}" + voucherId);
        synchronized (voucherId) {
            GaiaPoHeader poHeader = new GaiaPoHeader();
            poHeader.setClient(inData.getClientId());
            poHeader.setPoCompanyCode(inData.getStoreId());
            poHeader.setPoSupplierId(inData.getStoreCode());
            poHeader.setPoId(voucherId);
            poHeader.setPoType("Z001");
            poHeader.setPoSubjectType("2");
            //获取当前时间前一天
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1); //得到前一天
            Date date = calendar.getTime();
            DateFormat df = new SimpleDateFormat("yyyyMMdd");
            poHeader.setPoDate(df.format(date));
            poHeader.setPoHeadRemark("");
            poHeader.setPoPaymentId("");
            poHeader.setPoCreateBy(inData.getUserId());
            poHeader.setPoCreateDate(CommonUtil.getyyyyMMdd());
            poHeader.setPoCreateTime(CommonUtil.getHHmmss());
            this.poHeaderMapper.insert(poHeader);
            int i = 1;
            for (GetPoDetailInData detail : inData.getPoDetailList()) {
                GaiaPoItem poItem = new GaiaPoItem();
                poItem.setClient(inData.getClientId());
                poItem.setPoId(voucherId);
                poItem.setPoLineNo(String.valueOf(i));
                poItem.setPoProCode(detail.getProCode());
                poItem.setPoQty(new BigDecimal(detail.getProQty()));
                poItem.setPoLineAmt(new BigDecimal(detail.getProQty()).multiply(new BigDecimal(detail.getProPrice())));
                poItem.setPoUnit(detail.getProUnit());
                poItem.setPoPrice(new BigDecimal(detail.getProPrice()));
                poItem.setPoCompleteFlag("0");
                poItem.setPoLineRemark("0");
                poItem.setPoDeliveredQty(new BigDecimal(detail.getProQty()));
                poItem.setPoDeliveredAmt(new BigDecimal(detail.getProQty()).multiply(new BigDecimal(detail.getProPrice())));
                poItem.setPoSiteCode(inData.getStoreId());
                poItem.setPoLocationCode("1000");
                poItem.setPoRate(detail.getProRate());
                poItem.setPoDeliveryDate(DateUtil.getCurrentDateStr("yyyyMMdd"));
                poItem.setPoLineRemark(detail.getRemark());
                this.poItemMapper.insert(poItem);
                i++;
            }
        }
        return voucherId;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editWtpsDetail(EditWtpsDetailDto dto) {
        boolean hasCold = hasCold(dto.getClient());
        if (!hasCold) {
            throw new BusinessException("该用户未开启冷链商品开关！");
        }
        if ("D".equals(dto.getType())) {
            acceptWareHouseMapper.remove(dto);
        } else if ("U".equals(dto.getType())) {
            acceptWareHouseMapper.editAMT(dto);
        }
        acceptWareHouseMapper.editHAMT(dto);
    }

    @Override
    public boolean hasCold(String client) {
        GaiaClSystemPara gaiaClSystemPara = acceptWareHouseMapper.hasCold(client);
        if (gaiaClSystemPara != null && "1".equals(gaiaClSystemPara.getGcspPara1())) {
            return true;
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void manageGuoYaoOrder() {
        List<GaiaSdWtbhdD> wtbdList = acceptWareHouseMapper.selectWtpsOrderListInWtbd();
        if (ObjectUtil.isEmpty(wtbdList)) {
            XxlJobHelper.log("<国药订单定时任务> 补货订单为空！ 任务结束");
            return;
        }
        List<String> dsfVoucherIds = wtbdList.stream().map(wtbd -> wtbd.getClient() + "-" + wtbd.getGswbdVoucherId() + "-" + wtbd.getGswbdProId()).collect(Collectors.toList());
        List<JSONObject> orderStatusParam = new ArrayList<>();
        for (String voucherId : dsfVoucherIds) {
            JSONObject object = new JSONObject();
            object.set("billno", voucherId);
            orderStatusParam.add(object);
        }
        List<JSONObject> orderStatusList = GuoYaoUtil.getOrderStatus(orderStatusParam);
        if (ObjectUtil.isEmpty(orderStatusList)) {
            XxlJobHelper.log("<国药订单定时任务> 查询订单状态为空！ 任务结束");
            return;
        }

        List<String> shipmentsVoucherIds = orderStatusList.stream().filter(orderStatus -> "30".equals(orderStatus.get("status"))).map(orderStatus -> (String) orderStatus.get("billno")).collect(Collectors.toList());

        List<JSONObject> orderDetailParam = new ArrayList<>();
        for (String voucherId : shipmentsVoucherIds) {
            JSONObject object = new JSONObject();
            object.set("billno", voucherId);
            orderDetailParam.add(object);
        }

        List<JSONObject> orderDetailList = GuoYaoUtil.getOrderDetail(orderDetailParam);

        List<String> billnoList = orderDetailList.stream().map(orderDetail -> (String) orderDetail.get("billno")).collect(Collectors.toList());
        List<GaiaSdWtbhdD> shipmentsList = wtbdList.stream().filter(wtbd -> billnoList.contains(wtbd.getClient() + "-" + wtbd.getGswbdVoucherId() + "-" + wtbd.getGswbdProId())).collect(Collectors.toList());

        String dateStr = DateUtil.getCurrentDateStr("yyyyMMdd");
        String timeStr = DateUtil.getCurrentTimeStr("HHmmss");
        for (JSONObject object : orderDetailList) {
            String billno = object.get("billno") == null ? "" : object.get("billno").toString();
            if (StringUtils.isNullOrEmpty(billno)) {
                continue;
            }
            String[] split = billno.split("-");
            String client = split[0];
            String voucherId = split[1];
            String proId = split[2];
            String cmsbillno = object.get("cmsbillno") == null ? "" : object.get("cmsbillno").toString();

            GaiaSdWtpro gaiaSdWtpro = new GaiaSdWtpro();
            gaiaSdWtpro.setClient(client);
            gaiaSdWtpro.setProSelfCode(proId);
            GaiaSdWtpro proInfo = gaiaSdWtproMapper.getProInfo(gaiaSdWtpro);
            String proDsfCode = proInfo.getProDsfCode();

            List<GaiaSdWtbhdD> collect = wtbdList.stream().filter(wtbd -> voucherId.equals(wtbd.getGswbdVoucherId())).collect(Collectors.toList());
            GaiaSdWtbhdD gaiaSdWtbhdD = collect.get(0);
            GaiaSdWtbhdH gaiaSdWtbhdH = gaiaSdWtbhdHMapper.selectByPid(gaiaSdWtbhdD.getPid());

            //查询第三方门店id
            HashMap<String, Object> map = gaiaSdWtproMapper.getSdWtstoInfo(client, gaiaSdWtbhdD.getGswbdBrId());
            String dsfBrId = map.get("stoDsfSite") == null ? "" : map.get("stoDsfSite").toString();

            GaiaSdWtpsdD wtpsd = new GaiaSdWtpsdD();
            wtpsd.setClient(client);
            wtpsd.setGswdSupId(gaiaSdWtbhdH.getGswbhAddr());
            wtpsd.setGswdVoucherId(cmsbillno);
            wtpsd.setGswdDsfBrId(dsfBrId);
            wtpsd.setGswdDate(gaiaSdWtbhdH.getGswbhDnDate());
            wtpsd.setGswdSerial(gaiaSdWtbhdD.getGswbdSerial());
            wtpsd.setGswdProDsfId(proDsfCode);
            wtpsd.setGswdProId(proId);
            wtpsd.setGswdMadeDate(object.get("prddate") == null ? "" : object.get("prddate").toString().replaceAll("-", ""));
            wtpsd.setGswdBatchNo("");
            wtpsd.setGswdExpiryDate(object.get("enddate") == null ? "" : object.get("enddate").toString().replaceAll("-", ""));
            wtpsd.setGswdBatch(object.get("lotno") == null ? "" : object.get("lotno").toString());
            wtpsd.setGswdQTY(new BigDecimal(object.get("qty") == null ? "0" : object.get("qty").toString()));
            wtpsd.setGswdPRC(new BigDecimal(object.get("prc") == null ? "0" : object.get("prc").toString()));
            wtpsd.setGswdAMT(new BigDecimal(object.get("sumvalue") == null ? "0" : object.get("sumvalue").toString()));
            GaiaProductBusiness business = gaiaProductBusinessMapper.selectByClientAndProCode(client, proId);
            if (ObjectUtil.isNotEmpty(business)){
                wtpsd.setGswdName(business.getProName());
                wtpsd.setGswdCommonName(business.getProCommonname());
                wtpsd.setGswdBarcode(business.getProBarcode());
                wtpsd.setGswdSpecs(business.getProSpecs());
                wtpsd.setGswdUnit(business.getProUnit());
                wtpsd.setGswdFactoryName(business.getProFactoryName());
                wtpsd.setGswdPlace(business.getProPlace());
                wtpsd.setGswdPorm(business.getProForm());
                wtpsd.setGswdRegisterNo(object.get("ratifier") == null?"":object.get("ratifier").toString());
                wtpsd.setGswdTAX(business.getProOutputTax());
                wtpsd.setGswdIfMed(business.getProIfMed());
            }
            //wtpsd.setGswdRemarks();
            wtpsd.setGswdCreEMP(gaiaSdWtbhdH.getGswbhDnBy());
            wtpsd.setGswdCreDate(dateStr);
            wtpsd.setGswdCreTime(timeStr);
            gaiaSdWtpsdDMapper.insert(wtpsd);
            XxlJobHelper.log("<国药订单定时任务> 委托配送明细表添加成功！参数：{}",wtpsd);

            GaiaSdWtpdsH gaiaSdWtpdsH = gaiaSdWtpdsHMapper.selectByClientAndVoucherAndDsfBrId(client, cmsbillno, dsfBrId);
            if (ObjectUtil.isEmpty(gaiaSdWtpdsH)) {
                GaiaSdWtpdsH wtpdsH = new GaiaSdWtpdsH();
                wtpdsH.setClient(client);
                wtpdsH.setGswhSupId(gaiaSdWtbhdH.getGswbhAddr());
                wtpdsH.setGswhVoucherId(cmsbillno);
                wtpdsH.setGswhDsfBrId(dsfBrId);
                wtpdsH.setGswhStatus("0");
                wtpdsH.setGswhDate(gaiaSdWtbhdH.getGswbhDnDate());
                wtpdsH.setGswhBrId(gaiaSdWtbhdD.getGswbdBrId());
                wtpdsH.setGswhCreDate(gaiaSdWtbhdH.getGswbhDnDate());
                wtpdsH.setGswhCreTime(gaiaSdWtbhdH.getGswbhDnTime());
                wtpdsH.setGswhCreEMP(gaiaSdWtbhdH.getGswbhDnBy());
                gaiaSdWtpdsHMapper.insert(wtpdsH);
                XxlJobHelper.log("<国药订单定时任务> 委托配送主表添加成功！参数：{}",wtpdsH);
            }
            gaiaSdWtpdsHMapper.updateAMT(client, cmsbillno, dsfBrId);
            XxlJobHelper.log("<国药订单定时任务> 委托配送主表数目、金额修改成功！");
        }
        for (GaiaSdWtbhdD gaiaSdWtbhdD : shipmentsList) {
            acceptWareHouseMapper.updateDsfStatus(gaiaSdWtbhdD);
            XxlJobHelper.log("<国药订单定时任务> 委托补货明细表配送状态修改成功！");
        }
    }

    @Override
    public void insertMessageForReturning() {
        //删除原有消息
        gaiaSdMessageMapper.deleteMessageByType("shippingManagement_returnSupplierApproval");
        List<String> clientList = acceptWareHouseMapper.listColdClientInfo();
        if (CollectionUtil.isNotEmpty(clientList)) {
            List<HashMap<String, Object>> clientInfoList = acceptWareHouseMapper.listWaitingCheckClient(clientList);
            if (CollectionUtil.isNotEmpty(clientInfoList)) {
                Date now = new Date();
                for (HashMap<String, Object> map : clientInfoList) {
                    int i = acceptWareHouseMapper.listReturnSupplierApproval(map.get("client").toString(), map.get("siteNumber").toString());
                    if (i > 0) {
                        GaiaSdMessage sdMessage = new GaiaSdMessage();
                        sdMessage.setClient(map.get("client").toString());
                        sdMessage.setGsmId(map.get("siteNumber").toString());//针对所有加盟商
                        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(map.get("client").toString(), "company");
                        sdMessage.setGsmVoucherId(voucherId);//消息流水号
                        sdMessage.setGsmPage(SdMessageTypeEnum.RETURN_SUPPLIER_APPROVAL.page);
                        sdMessage.setGsmType(SdMessageTypeEnum.RETURN_SUPPLIER_APPROVAL.code);
                        sdMessage.setGsmRemark("截至目前，"+map.get("siteName").toString()+"，共有<font color=\"#FF0000\">" + i + "</font>条退货通知单需要审核，请尽快处理。");
                        sdMessage.setGsmFlag("N");//是否查看
                        sdMessage.setGsmPlatForm("WEB");//消息渠道
                        sdMessage.setGsmDeleteFlag("0");
                        sdMessage.setGsmArriveDate(com.gys.util.DateUtil.formatDate(now));
                        sdMessage.setGsmArriveTime(com.gys.util.DateUtil.dateToString(now, "HHmmss"));
                        gaiaSdMessageMapper.insertSelective(sdMessage);
                    }
                }
            }
        }


    }

    @Override
    public List<RejecteReasonVo> getgetRejecteReasons() {
        List<GaiaDictionary> gaiaDictionaries = dictionaryUtil.geGaiaDictionaryByType(DICTIONARY_TYPE_REJECTE);
        if (CollectionUtils.isNotEmpty(gaiaDictionaries)) {
            List<RejecteReasonVo> reasonVos = gaiaDictionaries.stream().map(s -> {
                        RejecteReasonVo rejecteReasonVo = new RejecteReasonVo();
                        BeanUtils.copyProperties(s, rejecteReasonVo);
                        return rejecteReasonVo;
                    }
            ).collect(Collectors.toList());
            return reasonVos;
        }
        return null ;
    }


}