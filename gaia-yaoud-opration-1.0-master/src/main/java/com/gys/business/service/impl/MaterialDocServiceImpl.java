package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaMaterialDocLogHMapper;
import com.gys.business.mapper.GaiaMaterialDocLogMapper;
import com.gys.business.mapper.entity.GaiaMaterialDocLog;
import com.gys.business.mapper.entity.GaiaMaterialDocLogH;
import com.gys.business.service.MaterialDocService;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.util.DateUtil;
import com.gys.util.JsonUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class MaterialDocServiceImpl implements MaterialDocService {
    private static final Logger log = LoggerFactory.getLogger(TlSaleServiceImpl.class);
    @Resource
    private GaiaMaterialDocLogMapper gaiaMaterialDocLogMapper;
    @Resource
    private GaiaMaterialDocLogHMapper gaiaMaterialDocLogHMapper;

    @Override
    @Transactional
    public String addLog(List<MaterialDocRequestDto> list) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            log.info("日志数据：{}", JsonUtils.beanToJson(list));
            String logId = UUID.randomUUID().toString().replaceAll("-", "");
            GaiaMaterialDocLogH gaiaMaterialDocLogH = new GaiaMaterialDocLogH();
            gaiaMaterialDocLogH.setGuid(logId);
            gaiaMaterialDocLogH.setGmdlhContent(JsonUtils.beanToJson(list));
            gaiaMaterialDocLogH.setGmdlhSucess(0);//0:ERROR
            gaiaMaterialDocLogH.setGmdlhTime(DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS"));
            gaiaMaterialDocLogHMapper.insertSelective(gaiaMaterialDocLogH);

            // 生成日志数据
            List<GaiaMaterialDocLog> gaiaMaterialDocLogList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                MaterialDocRequestDto materialDocRequestDto = list.get(i);
                GaiaMaterialDocLog gaiaMaterialDocLog = new GaiaMaterialDocLog();
                // 加盟商
                gaiaMaterialDocLog.setClient(materialDocRequestDto.getClient());
                // 主键ID
                gaiaMaterialDocLog.setGuid(materialDocRequestDto.getGuid());
                // 序号
                gaiaMaterialDocLog.setGuidNo(String.valueOf(i + 1));
                // 业务类型
                gaiaMaterialDocLog.setMatType(materialDocRequestDto.getMatType());
                // 过账日期
                gaiaMaterialDocLog.setMatPostDate(materialDocRequestDto.getMatPostDate());
                // 抬头备注
                gaiaMaterialDocLog.setMatHeadRemark(materialDocRequestDto.getMatHeadRemark());
                // 商品编码
                gaiaMaterialDocLog.setMatProCode(materialDocRequestDto.getMatProCode());
                // 配送门店
                gaiaMaterialDocLog.setMatStoCode(materialDocRequestDto.getMatStoCode());
                // 地点
                gaiaMaterialDocLog.setMatSiteCode(materialDocRequestDto.getMatSiteCode());
                // 库存地点
                gaiaMaterialDocLog.setMatLocationCode(materialDocRequestDto.getMatLocationCode());
                // 转移库存地点
                gaiaMaterialDocLog.setMatLocationTo(materialDocRequestDto.getMatLocationTo());
                // 批次
                gaiaMaterialDocLog.setMatBatch(materialDocRequestDto.getMatBatch());
                // 交易数量
                gaiaMaterialDocLog.setMatQty(materialDocRequestDto.getMatQty());
                // 交易金额
                gaiaMaterialDocLog.setMatAmt(materialDocRequestDto.getMatAmt());
                // 交易价格
                gaiaMaterialDocLog.setMatPrice(materialDocRequestDto.getMatPrice());
                // 基本计量单位
                gaiaMaterialDocLog.setMatUint(materialDocRequestDto.getMatUnit());
                // 借/贷标识
                gaiaMaterialDocLog.setMatDebitCredit(materialDocRequestDto.getMatDebitCredit());
                // 订单号
                gaiaMaterialDocLog.setMatPoId(materialDocRequestDto.getMatPoId());
                // 订单行号
                gaiaMaterialDocLog.setMatPoLineno(materialDocRequestDto.getMatPoLineno());
                // 业务单号
                gaiaMaterialDocLog.setMatDnId(materialDocRequestDto.getMatDnId());
                // 业务单行号
                gaiaMaterialDocLog.setMatDnLineno(materialDocRequestDto.getMatDnLineno());
                // 物料凭证行备注
                gaiaMaterialDocLog.setMatLineRemark(materialDocRequestDto.getMatLineRemark());
                // 创建人
                gaiaMaterialDocLog.setMatCreateBy(materialDocRequestDto.getMatCreateBy());
                // 创建日期
                gaiaMaterialDocLog.setMatCreateDate(DateUtil.getCurrentDateStr("yyyyMMdd"));
                // 创建时间
                gaiaMaterialDocLog.setMatCreateTime(DateUtil.getCurrentTimeStr("HHmmss"));
//                gaiaMaterialDocLogMapper.insert(gaiaMaterialDocLog);
                gaiaMaterialDocLogList.add(gaiaMaterialDocLog);
                if (gaiaMaterialDocLogList.size() >= 500) {
                    gaiaMaterialDocLogMapper.insertBatchList(gaiaMaterialDocLogList);
                    gaiaMaterialDocLogList.clear();
                }
            }
            if (!CollectionUtils.isEmpty(gaiaMaterialDocLogList)) {
                gaiaMaterialDocLogMapper.insertBatchList(gaiaMaterialDocLogList);
            }
            return logId;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            log.info("日志插入失败");
            return null;
        }
    }
}
