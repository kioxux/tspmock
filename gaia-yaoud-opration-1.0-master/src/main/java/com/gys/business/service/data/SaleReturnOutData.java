package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SaleReturnOutData {
    @ApiModelProperty(value = "支付方式1")
    private String gsshPaymentNo1;
    @ApiModelProperty(value = "支付方式名称1")
    private String gsshPaymentName1;
    @ApiModelProperty(value = "支付金额1")
    private BigDecimal gsshPaymentAmt1;
    @ApiModelProperty(value = "支付方式2")
    private String gsshPaymentNo2;
    @ApiModelProperty(value = "支付方式名称2")
    private String gsshPaymentName2;
    @ApiModelProperty(value = "支付金额2")
    private BigDecimal gsshPaymentAmt2;
    @ApiModelProperty(value = "支付方式3")
    private String gsshPaymentNo3;
    @ApiModelProperty(value = "支付方式名称3")
    private String gsshPaymentName3;
    @ApiModelProperty(value = "支付金额3")
    private BigDecimal gsshPaymentAmt3;
    @ApiModelProperty(value = "支付方式4")
    private String gsshPaymentNo4;
    @ApiModelProperty(value = "支付方式名称4")
    private String gsshPaymentName4;
    @ApiModelProperty(value = "支付金额4")
    private BigDecimal gsshPaymentAmt4;
    @ApiModelProperty(value = "支付方式5")
    private String gsshPaymentNo5;
    @ApiModelProperty(value = "支付方式名称5")
    private String gsshPaymentName5;
    @ApiModelProperty(value = "支付金额5")
    private BigDecimal gsshPaymentAmt5;
    @ApiModelProperty(value = "应收金额")
    private String gsshYsAmt;
}
