package com.gys.business.service.appservice;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface SalesService {

    /**
     * 商品查询
     * @param inData
     * @return
     */
    QueryProductAppData queryProduct(GetQueryProductInData inData);

    /**
     * 商品批次查询
     * @param inData
     * @return
     */
    List<GetPdAndExpOutData> batchQuery(GetQueryProductInData inData);

    /**
     * 金额计算
     * @param inData
     * @param userInfo
     * @return
     */
    AppAmountCalculationDto amountCalculation(AppAmountVo inData, GetLoginOutData userInfo);


    /**
     * fx app 销售查询
     * @param inData
     * @return
     */
    List<AppParameterDto> getAllListByApp(AppParameterVo inData);


    /**
     * app 销售作废接口
     * @param inData
     */
    void updateAppSalesStatus(AppParameterVo inData);

    /**
     * 根据订单号查询
     * @param inData
     * @return
     */
    List<RebornData> getAllListByAppBillNo(AppParameterVo inData);

    /**
     * app 根据订单号查询V2
     * @param inData
     * @return
     */
    AppMobileOrderInfoDto getAllListByAppBillNoV2(AppParameterVo inData);

    /**
     * app 商品详情
     * @param inData
     * @return
     */
    List<AppParameterDetailsDto> getAppByDetails(AppParameterVo inData);

    /**
     * 商品的药事推荐
     * @param inData
     * @return
     */
    GetServerOutData drugRecommendation(GetQueryProductInData inData);

    /**
     * 添加商品
     * @param inData 商品列表
     * @return 金额计算+商品列表+促销列表
     */
    AppAmountCalculationDto amountCalculation2(AppAmountVo inData, GetLoginOutData userInfo);
}
