package com.gys.business.service.data.integralExchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class IntegralRule {
    private Integer serial; //序号
    private BigDecimal needIntegral;//需要积分
    private BigDecimal serveAmt;//抵用金额
    private String serveRate;//抵用比例
}
