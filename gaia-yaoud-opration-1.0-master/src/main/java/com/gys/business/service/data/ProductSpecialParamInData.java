package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductSpecialParamInData implements Serializable {

    /**
     * 省份
     */
    @ApiModelProperty(value="省份")
    private String proProvCode;

    /**
     * 地市
     */
    @ApiModelProperty(value="地市")
    private String proCityCode;

    /**
     * 日期范围列表
     */
    @ApiModelProperty(value="日期范围列表")
    private List<ProductSpecialDateInData> dateList;

}
