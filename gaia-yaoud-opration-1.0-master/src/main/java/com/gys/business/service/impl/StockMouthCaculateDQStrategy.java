package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaMaterialDocMapper;
import com.gys.business.mapper.StockMouthMapper;
import com.gys.business.mapper.entity.GaiaMaterialDoc;
import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.StockMouthCaculateStrategy;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtil;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 仅更新选择月份初实现推算的计算类
 */
public class StockMouthCaculateDQStrategy implements StockMouthCaculateStrategy {

    private StockMouthMapper stockMouthMapper;

    private GaiaMaterialDocMapper materialDocMapper;

    private StockMouthCaculateCondition condition;

//    private List<GaiaMaterialDoc> lastMonthMaterialDocList;


    public StockMouthCaculateDQStrategy(StockMouthMapper stockMouthMapper, GaiaMaterialDocMapper materialDocMapper, StockMouthCaculateCondition condition) {
        this.stockMouthMapper = stockMouthMapper;
        this.materialDocMapper = materialDocMapper;
        this.condition = condition;
//        this.lastMonthMaterialDocList = lastMonthMaterialDocList;
    }

    @Override
    public void caculate() {
        //先进行验证，用户输入的加盟商是否需要是没有QC
        Map<String,List<String>> clientSiteMap = this.condition.getClientSiteMap();
        List<String> clients = new ArrayList<>();
        clientSiteMap.forEach((x,y)->{
            clients.add(x);

        });
//        List<String> clients = condition.getClients();
        //此处扩展多客户运算的逻辑
        clients.forEach(c -> {
            List<String> sites = clientSiteMap.get(c);
            String chooseYearMonth = this.condition.getChooseDate();
            String lastMonth = DateUtil.getLastMonth(chooseYearMonth);
            //先进行验证，看看月度库存表中是够存在上月数据
            Example example = new Example(StockMouth.class);
            Example.Criteria criteria = example.createCriteria()
                    .andEqualTo("client", c)
                    .andEqualTo("gsmYearMonth", lastMonth);
            if (CollectionUtil.isNotEmpty(sites)) {
                criteria.andIn("gsmSiteCode", sites);
            }
            example.setOrderByClause("GSM_YEAR_MONTH limit 1 ");
            List<StockMouth> stockMouths = this.stockMouthMapper.selectByExample(example);
            if (CollectionUtil.isEmpty(stockMouths)) {
                throw new BusinessException("客户" + lastMonth + "无月度数据，无法推算,客户编号：" + c);
            }

            //根据用户选择的年月获取月度表中上个月的记录
            List<StockMouth> basicList = getStockMouthByChooseMonth(this.stockMouthMapper, c, sites, lastMonth);
            List<GaiaMaterialDoc> changeList = getChangeMaterialDoc(this.materialDocMapper, c, sites, chooseYearMonth);

            //分别将数据进行按维度分组--分组维度为地点+商品+批次
            Map<String, StockMouth> basicGroupMap = new HashMap<>();
            basicList.forEach(b -> {
                String key = b.getClient() + b.getGsmSiteCode() + b.getGsmProCode() + b.getGsmBatch();
                basicGroupMap.put(key, b);
            });
            if (CollectionUtil.isNotEmpty(changeList)) {
                List<String> excludeMatTypeInChange = new ArrayList<>();
                excludeMatTypeInChange.add("LX");
                excludeMatTypeInChange.add("CX");
                excludeMatTypeInChange.add("PX");
                excludeMatTypeInChange.add("QC");
                //过滤一些类型不进行处理
                changeList = changeList.stream().filter(change -> !excludeMatTypeInChange.contains(change.getMatType())).collect(Collectors.toList());
                changeList.forEach(ch -> {
                    String key = ch.getClient() + ch.getMatSiteCode() + ch.getMatProCode() + ch.getMatBatch();
//                            changeGroupMap.put(key,c);
                    if (basicGroupMap.containsKey(key)) {
                        //QC物料凭证中存在，那么需要将本月的进出情况进行加减
//                                String matType = c.getMatType();
                        String matDebitCredit = ch.getMatDebitCredit();//H-减，S-加
                        if (StrUtil.isNotBlank(matDebitCredit)) {
                            StockMouth qCStockMouth = basicGroupMap.get(key);
                            BigDecimal matQtyFinal = null;
                            BigDecimal matBatAmtFinal = null;
                            BigDecimal matRateBatFinal = null;
                            BigDecimal matQtySY = qCStockMouth.getGsmStockQty();//上月物料凭证数量
                            BigDecimal matBatAmtSY = qCStockMouth.getGsmStockAmt();//上月的总金额（批次）
                            BigDecimal matRateBatSY = qCStockMouth.getGsmTaxAmt();//上月的税金（批次）
                            if ("S".equals(matDebitCredit)) {
                                matQtyFinal = matQtySY.add(ch.getMatQty());
                                matBatAmtFinal = matBatAmtSY.add(ch.getMatBatAmt());
                                matRateBatFinal = matRateBatSY.add(ch.getMatRateBat());
                            } else if ("H".equals(matDebitCredit)) {
                                matQtyFinal = matQtySY.subtract(ch.getMatQty());
                                matBatAmtFinal = matBatAmtSY.subtract(ch.getMatBatAmt());
                                matRateBatFinal = matRateBatSY.subtract(ch.getMatRateBat());
                            }
                            qCStockMouth.setGsmStockQty(matQtyFinal);
                            qCStockMouth.setGsmStockAmt(matBatAmtFinal);
                            qCStockMouth.setGsmTaxAmt(matRateBatFinal);
                            basicGroupMap.put(key, qCStockMouth);//将单条的计算结果汇总到qc的计算结果中
                        }
                    }else{
                        //因为取不到对应的key，这意味着，可能是新的单据，而且理论上只能是新增种类
                        //构建月度库存对象
                        StockMouth stockMouth = new StockMouth();
                        stockMouth.setClient(ch.getClient());
                        stockMouth.setGsmProCode(ch.getMatProCode());
                        stockMouth.setGsmSiteCode(ch.getMatSiteCode());
                        stockMouth.setGsmBatch(ch.getMatBatch());
                        stockMouth.setGsmStockQty(ch.getMatQty());
                        stockMouth.setGsmStockAmt(ch.getMatBatAmt());
                        stockMouth.setGsmTaxAmt(ch.getMatRateBat());
                        stockMouth.setLastUpdateTime(new Date());
                        basicGroupMap.put(key, stockMouth);
                    }
                });
            }

            //将basicGroupMap 中处理的当月最终库存处理入库，存储到月度库存表中
            List<StockMouth> insertQCList = new ArrayList<>();
            for (String key : basicGroupMap.keySet()) {
                StockMouth doc = basicGroupMap.get(key);
                //构建月度库存对象
                doc.setGsmYearMonth(chooseYearMonth);
                doc.setLastUpdateTime(new Date());
                insertQCList.add(doc);
            }

            //因为推算数据是进行覆盖更新的，所以在插入数据之前必须要做删除
            Example deleteStockMonthExample = new Example(StockMouth.class);
            Example.Criteria criteria1 = deleteStockMonthExample.createCriteria().andEqualTo("client", c).andEqualTo("gsmYearMonth", chooseYearMonth );
            if (CollectionUtil.isNotEmpty(sites)) {
                criteria1.andIn("gsmSiteCode", sites);
            }
            stockMouthMapper.deleteByExample(deleteStockMonthExample);

            stockMouthMapper.insertList(insertQCList);
        });
    }


    /**
     * 根据条件查询出月度库存表中的数据
     *
     * @param stockMouthMapper
     * @param client
     * @param sites
     * @param yearMonth
     * @return
     */
    private List<StockMouth> getStockMouthByChooseMonth(StockMouthMapper stockMouthMapper, String client, List<String> sites, String yearMonth) {
        List<StockMouth> resList = new ArrayList<>();
        Example example = new Example(StockMouth.class);
        Example.Criteria criteria = example.createCriteria()
                .andEqualTo("client", client)
                .andEqualTo("gsmYearMonth", yearMonth);
        if (CollectionUtil.isNotEmpty(sites)) {
            criteria.andIn("gsmSiteCode", sites);
        }
        List<StockMouth> stockMouthList = stockMouthMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(stockMouthList)) {
            resList = stockMouthList;
        }
        return resList;
    }

    private List<GaiaMaterialDoc> getChangeMaterialDoc(GaiaMaterialDocMapper materialDocMapper, String client, List<String> sites, String yearMonth) {
        List<GaiaMaterialDoc> resList = new ArrayList<>();
        Example example = new Example(GaiaMaterialDoc.class);
        Example.Criteria criteria = example.createCriteria()
                .andEqualTo("client", client)
                .andLike("matPostDate", yearMonth + "%");
        if (CollectionUtil.isNotEmpty(sites)) {
            criteria.andIn("matSiteCode", sites);
        }
        example.orderBy("matCreateTime");
        List<GaiaMaterialDoc> materialDocList = materialDocMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(materialDocList)) {
            resList = materialDocList;
        }
        return resList;
    }



}
