package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/3/22
 */
@Data
public class AppParameterDto implements Serializable {
    private static final long serialVersionUID = 4231150292103311253L;

    /**
     * 销售码
     */
    private String salesCode;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * app销售单号
     */
    private String appRegisterVoucherId;

    /**
     * 会员卡
     */
    private String gsshHykNo;

    /**
     * 会员名称
     */
    private String gsmbName;

    /**
     * 应收金额
     */
    private String gsshYsAmt;

    /**
     * 状态
     */
    private String status;

    /**
     * 收银员
     */
    private String userNam;

    /**
     * 销售单号
     */
    private String gsshBillNo;
}
