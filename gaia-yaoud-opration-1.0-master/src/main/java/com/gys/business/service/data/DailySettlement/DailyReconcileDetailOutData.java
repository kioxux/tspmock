package com.gys.business.service.data.DailySettlement;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DailyReconcileDetailOutData implements Serializable {
    private static final long serialVersionUID = 1871057924855640424L;

    /**
     * 支付编码
     */
    private String pmId;

    /**
     * 支付方式
     */
    private String gspddPayType;

    /**
     * 应收金额
     */
    private String saleYsAmt;

    /**
     * 储值卡充值金额
     */
    private String cardYsAmt;

    /**
     * 订购金额
     */
    private String orderAmt;

    /**
     * 金额对账
     */
    private String amountReconciliation;

    /**
     * 差异
     */
    private String differenceAmt;

    /**
     * 销售单集合
     */
    private List<String> billNo;

    /**
     * 储值卡支付信息单
     */
    private List<String> cardNo;

}

