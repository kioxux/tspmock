package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.gys.business.mapper.GaiaSdPhysicalCountDiffMapper;
import com.gys.business.mapper.GaiaSdPhysicalCountingMapper;
import com.gys.business.mapper.GaiaSdPhysicalHeaderMapper;
import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.entity.GaiaSdPhysicalCountDiff;
import com.gys.business.mapper.entity.GaiaSdPhysicalCounting;
import com.gys.business.mapper.entity.GaiaSdPhysicalHeader;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.IncomeStatementService;
import com.gys.business.service.PhysicalCountService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.NullUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@Slf4j
public class PhysicalCountServiceImpl implements PhysicalCountService {
    @Resource
    private GaiaSdPhysicalCountingMapper physicalCountingMapper;

    @Resource
    private GaiaSdPhysicalCountDiffMapper physicalCountDiffMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Resource
    private IncomeStatementService incomeStatementService;

    @Resource
    private GaiaSdPhysicalHeaderMapper physicalHeaderMapper;

    @Transactional
    @Override
    public List<GetPhysicalCountHeadOutData> listPhysical(GetPhysicalCountInData inData) {
        List<GetPhysicalCountHeadOutData> physicalHeaderList = physicalHeaderMapper.listPhysical(inData);
        List<GetPhysicalCountHeadOutData> physicalCountHeadOutDataList = Lists.newArrayList();

        for (int i = 0; i < physicalHeaderList.size(); i++) {
            GetPhysicalCountHeadOutData physicalHeader = physicalHeaderList.get(i);
            physicalHeader.setGspcVoucherId(physicalHeader.getGspcVoucherId());
            physicalHeader.setIndex(i + 1);
            physicalHeader.setGsphFlag1(physicalHeader.getGsphFlag1().equals("0") ? "部分盘点" : "全盘");
            physicalHeader.setGsphFlag2(physicalHeader.getGsphFlag2().equals("0") ? "明盘" : "盲盘");
            physicalHeader.setGsphFlag3(physicalHeader.getGsphFlag3().equals("0") ? "数量盘点" : "批号盘点");
            physicalHeader.setGsphDate(CommonUtil.parseyyyyMMdd(physicalHeader.getGsphDate()));
            physicalHeader.setGsphTime(CommonUtil.parseHHmmss(physicalHeader.getGsphTime()));
            physicalCountHeadOutDataList.add(physicalHeader);
        }
        return physicalCountHeadOutDataList;
    }

    @Override
    public List<GetPhysicalCountHeadOutData> selectDetailById(GetPhysicalCountInData inData) {
        return null;
    }

    @Transactional
    @Override
    public List<GetPhysicalCountOutData> selectPhysical(GetPhysicalCountInData inData) {
        if (!StrUtil.isBlank(inData.getClientId()) && !StrUtil.isBlank(inData.getGspcBrId())) {
            List<GetPhysicalCountOutData> resultList = physicalCountDiffMapper.selectPhysicalDiff(inData);

            for(GetPhysicalCountOutData outData : resultList){
                outData.setGspcDate(CommonUtil.parseyyyyMMdd(outData.getGspcDate()));
                outData.setGspcdDate(CommonUtil.parseyyyyMMdd(outData.getGspcdDate()));
                outData.setGspcTime(CommonUtil.parseHHmmss(outData.getGspcTime()));
                outData.setGspcdTime(CommonUtil.parseHHmmss(outData.getGspcdTime()));
                outData.setStockQty(CommonUtil.stripTrailingZeros(outData.getStockQty()));
                outData.setGspcQty(CommonUtil.stripTrailingZeros(outData.getGspcQty()));
                outData.setGspcdDiffFirstQty(CommonUtil.stripTrailingZeros(outData.getGspcdDiffFirstQty()));
                outData.setGspcdDiffSecondQty(CommonUtil.stripTrailingZeros(outData.getGspcdDiffSecondQty()));
                outData.setGspcdPcSecondQty(CommonUtil.stripTrailingZeros(outData.getGspcdPcSecondQty()));

                List<String> quickSearchList = Lists.newArrayList();
                if(StrUtil.isNotBlank(outData.getProName())){
                    quickSearchList.add(outData.getProName());
                }

                if(StrUtil.isNotBlank(outData.getProSelfCode())){
                    quickSearchList.add(outData.getProSelfCode());
                }

                if(StrUtil.isNotBlank(outData.getProBarCode())){
                    quickSearchList.add(outData.getProBarCode());
                }

                if(StrUtil.isNotBlank(outData.getProBarCode2())){
                    quickSearchList.add(outData.getProBarCode2());
                }

                if(StrUtil.isNotBlank(outData.getProPym())){
                    quickSearchList.add(outData.getProPym());
                }

                if(StrUtil.isNotBlank(outData.getProRegisterNo())){
                    quickSearchList.add(outData.getProRegisterNo());
                }

                outData.setQuickSearchList(quickSearchList);
            }

            if (CollUtil.isNotEmpty(resultList)) {
                Iterator var3 = resultList.iterator();

                while(var3.hasNext()) {
                    GetPhysicalCountOutData physicalCountOutData = (GetPhysicalCountOutData)var3.next();
                    if (!StrUtil.isBlank(physicalCountOutData.getVaildDate())) {
                        long days = DateUtil.betweenDay(new Date(), DateUtil.parse(physicalCountOutData.getVaildDate(), "yyyyMMdd"), true);
                        physicalCountOutData.setVaild(StrUtil.toString(days));
                    }
                }
            }
//            else {
//                GetProductInfoQueryInData proInData = new GetProductInfoQueryInData();
//                proInData.setStockQty(1);
//                proInData.setClient(inData.getClientId());
//                proInData.setGspp_br_id(inData.getGspcBrId());
//                proInData.setGspgProId(inData.getGspcProId());
//                proInData.setGspgId(inData.getGspgId());
//                proInData.setGsplArea(inData.getGsplArea());
//                proInData.setGsplGroup(inData.getGsplGroup());
//                proInData.setGsplShelf(inData.getGsplShelf());
//                proInData.setGsplStorey(inData.getGsplStorey());
//                proInData.setGsplSeat(inData.getGsplSeat());
//                List<GetProductInfoQueryOutData> proList = this.productBasicMapper.selectPhysicalProList(proInData);
//                int index = 1;
//
//                for(Iterator var6 = proList.iterator(); var6.hasNext(); ++index) {
//                    GetProductInfoQueryOutData product = (GetProductInfoQueryOutData)var6.next();
//                    GetPhysicalCountOutData physicalCountOutData = new GetPhysicalCountOutData();
//                    physicalCountOutData.setGspcDate(inData.getGspcDate());
//                    physicalCountOutData.setGspcType(inData.getGspcType());
//                    physicalCountOutData.setGspcRowNo(StrUtil.toString(index));
//                    physicalCountOutData.setGspcProId(product.getProCode());
//                    physicalCountOutData.setBatchNo(product.getBatchNo());
//                    physicalCountOutData.setGspcStatus("0");
//                    physicalCountOutData.setStockQty(product.getStockQty());
//                    physicalCountOutData.setProName(product.getProName());
//                    physicalCountOutData.setProSpecs(product.getProSpecs());
//                    physicalCountOutData.setProUnit(product.getProUnit());
//                    physicalCountOutData.setProForm(product.getProForm());
//                    physicalCountOutData.setProFactoryName(product.getProFactoryName());
//                    physicalCountOutData.setProPlace(product.getProPlace());
//                    physicalCountOutData.setProRegisterNo(product.getProRegisterNo());
//                    physicalCountOutData.setGsplArea(product.getGsplArea());
//                    physicalCountOutData.setGsplGroup(product.getGsplGroup());
//                    physicalCountOutData.setGsplShelf(product.getGsplShelf());
//                    physicalCountOutData.setGsplStorey(product.getGsplStorey());
//                    physicalCountOutData.setGsplSeat(product.getGsplSeat());
//                    physicalCountOutData.setGsppPriceNormal(product.getGsppPriceNormal());
//                    if (StrUtil.isNotBlank(product.getVaildDate())) {
//                        long days = DateUtil.betweenDay(new Date(), DateUtil.parse(product.getVaildDate(), "yyyyMMdd"), true);
//                        physicalCountOutData.setVaild(StrUtil.toString(days));
//                    }
//
//                    resultList.add(physicalCountOutData);
//                }
//            }

            return resultList;
        } else {
            throw new BusinessException("参数不能为空");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePhysical(GetPhysicalCountInData inData) {
        if (!StrUtil.isBlank(inData.getClientId()) && !StrUtil.isBlank(inData.getGspcBrId())) {
            List<GetPhysicalCountInData> physicals = inData.getPhysicals();
            if (CollUtil.isEmpty(physicals)) {
                throw new BusinessException("盘点商品不能为空");
            } else {
                int index = 1;
//                Iterator var4 = physicals.iterator();

//                while(var4.hasNext()) {
//                    GetPhysicalCountInData physical = (GetPhysicalCountInData)var4.next();
//                    if (StrUtil.isBlank(physical.getGspcProId())) {
//                        throw new BusinessException(StrUtil.format("第{}行商品编码不能为空", new Object[]{Integer.valueOf(index)}));
//                    }
//                }

                Example example = new Example(GaiaSdPhysicalCounting.class);
                example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gspcBrId", inData.getGspcBrId())
                                        .andEqualTo("gspcDate", inData.getGspcDate()).andEqualTo("gspcType", inData.getGspcType());
                List<GaiaSdPhysicalCounting> list = this.physicalCountingMapper.selectByExample(example);
                String gspcVoucherId = "";
                if (CollUtil.isNotEmpty(list)) {
                    GaiaSdPhysicalCounting record = list.get(0);
                    if ("1".equals(record.getGspcStatus())) {
                        throw new BusinessException("当前盘点类型盘点日期已审核，不可重复提交");
                    }

                    gspcVoucherId = record.getGspcVoucherId();
                    this.physicalCountingMapper.deleteByExample(example);
                } else {
                    String codePre = DateUtil.format(new Date(), "yyyy");
                    gspcVoucherId = this.physicalCountingMapper.selectNextVoucherId(inData.getClientId(), codePre);
                }

                index = 1;

                for(Iterator var14 = physicals.iterator(); var14.hasNext(); ++index) {
                    GetPhysicalCountInData physical = (GetPhysicalCountInData)var14.next();
                    GaiaSdPhysicalCounting record = new GaiaSdPhysicalCounting();
                    record.setClientId(inData.getClientId());
                    record.setGspcVoucherId(gspcVoucherId);
                    record.setGspcBrId(inData.getGspcBrId());
                    record.setGspcDate(inData.getGspcDate());
                    record.setGspcType(inData.getGspcType());
                    record.setGspcRowNo(StrUtil.toString(index));
                    record.setGspcTime(inData.getGspcTime());
                    record.setGspcProId(physical.getGspcProId());
                    record.setGspcBatchNo(physical.getGspcBatchNo());
                    record.setGspcStockQty(physical.getGspcStockQty());
                    record.setGspcQty(physical.getGspcQty());
                    record.setGspcStatus("0");
                    this.physicalCountingMapper.insert(record);
                }

            }
        } else {
            throw new BusinessException("参数不能为空");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approvePhysicalDiff(GetPhysicalCountInData inData, GetLoginOutData userInfo) {
        //修改过的差异单
        List<GetPhysicalCountInData> physicalList = inData.getPhysicals();
        IncomeStatementInData incomeStatementInData = new IncomeStatementInData();
        List<IncomeStatementDetailInData> detailList = Lists.newArrayList();
        int i = 1;
        for (GetPhysicalCountInData physical : physicalList) {
            String batchNo = physical.getGspcBatchNo();
            IncomeStatementDetailInData incomeStatementDetailInData = new IncomeStatementDetailInData();
            //当商品没有批号
            inData.setClientId(userInfo.getClient());
            inData.setGspcBrId(userInfo.getDepId());
            inData.setGspcProId(physical.getGspcProId());
            inData.setGspcVoucherId(physical.getGspcVoucherId());
            inData.setGspcBatchNo(batchNo);
            GaiaSdPhysicalCountDiff physicalCountDiff = physicalCountDiffMapper.selectOnlyOne(inData);
            physicalCountDiff.setGspcdPcSecondQty(physical.getGspcQty());
            physicalCountDiff.setGspcdExamineEmp(userInfo.getUserId());
            physicalCountDiff.setGspcdExamineDate(CommonUtil.getyyyyMMdd());
            physicalCountDiff.setGspcdStatus("1");
            physicalCountDiff.setGspcdIsFlag("1");
            physicalCountDiff.setGspcVoucherId(physical.getGspcVoucherId());
            physicalCountDiff.setGspcdProId(physical.getGspcProId());
            physicalCountDiff.setGspcdDiffSecondQty(physical.getGspcdDiffSecondQty());
            if(StrUtil.isNotBlank(batchNo)){
                Example example = new Example(GaiaSdPhysicalCountDiff.class);
                example.createCriteria().andEqualTo("clientId", physicalCountDiff.getClientId()).andEqualTo("gspcdBrId", userInfo.getDepId())
                        .andEqualTo("gspcdVoucherId", physicalCountDiff.getGspcdVoucherId()).andEqualTo("gspcdProId", physicalCountDiff.getGspcdProId())
                        .andEqualTo("gspcdBatchNo", physicalCountDiff.getGspcdBatchNo());
                physicalCountDiffMapper.updateByExampleSelective(physicalCountDiff, example);
            }else{
                physicalCountDiffMapper.updateByPrimaryKey(physicalCountDiff);
            }

            incomeStatementDetailInData.setGsisdProId(physicalCountDiff.getGspcdProId());
            incomeStatementDetailInData.setClientId(userInfo.getClient());
            incomeStatementDetailInData.setGsisdBrId(userInfo.getDepId());
            incomeStatementDetailInData.setGsisdSerial(i);
            incomeStatementDetailInData.setGsisdBatchNo(physicalCountDiff.getGspcdBatchNo());
            //库存数量=复盘时库存数量
            incomeStatementDetailInData.setGsisdQty(physicalCountDiff.getGspcdSecondStochQty());
            //实际数量=复盘时库存数量 + 过账数量
            incomeStatementDetailInData.setGsisdRealQty(CommonUtil.stripTrailingZeros(
                    new BigDecimal(NullUtil.defaultNull(physicalCountDiff.getGspcdSecondStochQty(),"0"))
                            .add(new BigDecimal(NullUtil.defaultNull(physicalCountDiff.getGspcdDiffSecondQty(),"0"))).toPlainString()));
            incomeStatementDetailInData.setGsisdIsCause("盘点差异");
            incomeStatementDetailInData.setGsisdIsQty(physicalCountDiff.getGspcdDiffSecondQty());
            //差异量=过账数量
            incomeStatementDetailInData.setDifference(physicalCountDiff.getGspcdDiffSecondQty());
            incomeStatementDetailInData.setGsisdStockStatus("0");
            detailList.add(incomeStatementDetailInData);
            i++;
            //是否以批号盘点
            if(StrUtil.isNotBlank(batchNo)){
                Example stockBatchExample = new Example(GaiaSdStockBatch.class);
                stockBatchExample.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gssbBrId", userInfo.getDepId())
                                                  .andEqualTo("gssbBatchNo", batchNo).andEqualTo("gssbProId", physical.getGspcProId());
                List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(stockBatchExample);

                if(CollectionUtil.isNotEmpty(stockBatchList)){
                    GaiaSdStockBatch stockBatch = CollectionUtil.getFirst(stockBatchList);
                    if(stockBatch != null) {
                        incomeStatementDetailInData.setGsisdBatch(stockBatch.getGssbBatch());
                    }
                    incomeStatementDetailInData.setGsisdBatchNo(batchNo);
                    incomeStatementDetailInData.setGsisdValidUntil(stockBatch.getGssbVaildDate());
                }
            }
        }

        incomeStatementInData.setClientId(userInfo.getClient());
        incomeStatementInData.setGsishBrId(userInfo.getDepId());
        incomeStatementInData.setGsishStatus("0");
        incomeStatementInData.setGsishIsType("1");
        incomeStatementInData.setGsishPcVoucherId(inData.getGspcVoucherId());
        incomeStatementInData.setGsishVoucherId(inData.getGspcVoucherId().replace("AD", "SYS"));
        incomeStatementInData.setGsishDate(CommonUtil.getyyyyMMdd());
        incomeStatementInData.setDetailOutDataList(detailList);
        incomeStatementInData.setGsishTableType("1");
        incomeStatementInData.setToken(userInfo.getToken());
        incomeStatementService.insert(incomeStatementInData);

        Example physicalHeaderExample = new Example(GaiaSdPhysicalHeader.class);
        physicalHeaderExample.createCriteria().andEqualTo("client", incomeStatementInData.getClientId()).andEqualTo("gsphBrId", incomeStatementInData.getGsishBrId())
                .andEqualTo("gsphVoucherId", incomeStatementInData.getGsishPcVoucherId());
        GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(physicalHeaderExample);
        physicalHeader.setGsphSchedule("6");
        physicalHeader.setGsphUpdateDate(CommonUtil.getyyyyMMdd());
        physicalHeaderMapper.updateByPrimaryKey(physicalHeader);
    }

    @Override
    public GetPhysicalCountOutData selectPhysicalOne(GetPhysicalCountInData inData) {
        if (!StrUtil.isBlank(inData.getClientId()) && !StrUtil.isBlank(inData.getGspcBrId()) && !StrUtil.isBlank(inData.getGspcDate()) && !StrUtil.isBlank(inData.getGspcType())) {
            List<GetPhysicalCountOutData> list = this.physicalCountingMapper.selectPhysical(inData);
            GetPhysicalCountOutData outData = new GetPhysicalCountOutData();
            String status = "0";
            if (CollUtil.isNotEmpty(list)) {
                if ("1".equals((list.get(0)).getGspcStatus())) {
                    status = "2";
                } else {
                    status = "1";
                }
            }

            List<GetPhysicalCountOutData> diffList = this.physicalCountDiffMapper.selectPhysicalDiff(inData);
            if (CollUtil.isNotEmpty(diffList)) {
                status = "3";
                if ("1".equals((diffList.get(0)).getGspcStatus())) {
                    status = "4";
                }
            }

            outData.setStatus(status);
            return outData;
        } else {
            throw new BusinessException("参数不能为空");
        }
    }

    @Transactional
    @Override
    public void approvePhysical(GetPhysicalCountInData inData, GetLoginOutData userInfo) {
        if (!StrUtil.isBlank(userInfo.getClient()) && !StrUtil.isBlank(userInfo.getDepId()) && !StrUtil.isBlank(inData.getGspcDate()) && !StrUtil.isBlank(inData.getGspcType())) {
            Example example = new Example(GaiaSdPhysicalCounting.class);
            example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gspcBrId", userInfo.getDepId()).andEqualTo("gspcDate", inData.getGspcDate()).andEqualTo("gspcType", inData.getGspcType());
            GaiaSdPhysicalCounting record = new GaiaSdPhysicalCounting();
            Date date = new Date();
            record.setGspcExamineDate(DateUtil.format(date, "yyyyMMdd"));
            record.setGspcExamineTime(DateUtil.format(date, "HHmmss"));
            record.setGspcExamineEmp(userInfo.getUserId());
            record.setGspcStatus("1");
            this.physicalCountingMapper.updateByExampleSelective(record, example);
        } else {
            throw new BusinessException("参数不能为空");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePhysicalDiff(GetPhysicalCountInData inData, GetLoginOutData userInfo) {
        if (!StrUtil.isBlank(userInfo.getClient()) && !StrUtil.isBlank(userInfo.getDepId()) && !StrUtil.isBlank(inData.getGspcDate()) && !StrUtil.isBlank(inData.getGspcType())) {
            Example example = new Example(GaiaSdPhysicalCounting.class);
            example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gspcBrId", userInfo.getDepId()).andEqualTo("gspcDate", inData.getGspcDate()).andEqualTo("gspcType", inData.getGspcType());
            List<GaiaSdPhysicalCounting> list = this.physicalCountingMapper.selectByExample(example);
            if (CollUtil.isEmpty(list)) {
                throw new BusinessException("当前盘点单不存在");
            } else {
                example = new Example(GaiaSdPhysicalCountDiff.class);
                example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gspcdBrId", userInfo.getDepId()).andEqualTo("gspcdDate", inData.getGspcDate()).andEqualTo("gspcdType", inData.getGspcType());
                List<GaiaSdPhysicalCountDiff> diffList = this.physicalCountDiffMapper.selectByExample(example);
                if (CollUtil.isNotEmpty(diffList)) {
                    throw new BusinessException("当前盘点类型盘点日期差异已生成，不可重复提交");
                } else {
                    String gspcVoucherId = ((GaiaSdPhysicalCounting)list.get(0)).getGspcVoucherId();
                    String codePre = DateUtil.format(new Date(), "yyyy");
                    String voucherId = this.physicalCountDiffMapper.selectNextVoucherId(userInfo.getClient(), codePre);

                    GaiaSdPhysicalCountDiff diff;
                    for(Iterator var9 = list.iterator(); var9.hasNext(); this.physicalCountDiffMapper.insert(diff)) {
                        GaiaSdPhysicalCounting data = (GaiaSdPhysicalCounting)var9.next();
                        diff = new GaiaSdPhysicalCountDiff();
                        diff.setClientId(data.getClientId());
                        diff.setGspcdVoucherId(voucherId);
//                        diff.setGspcVoucherI(gspcVoucherId);
                        diff.setGspcdBrId(data.getGspcBrId());
                        diff.setGspcdDate(data.getGspcDate());
                        diff.setGspcdType(data.getGspcType());
//                        diff.setGspcdRowNo(data.getGspcRowNo());
                        diff.setGspcdTime(data.getGspcTime());
                        diff.setGspcdProId(data.getGspcProId());
                        diff.setGspcdBatchNo(data.getGspcBatchNo());
                        diff.setGspcdStochQty(data.getGspcStockQty());
                        diff.setGspcdPcFirstQty(data.getGspcQty());
                        diff.setGspcdDiffFirstQty(StrUtil.toString(Integer.valueOf(data.getGspcStockQty()) - Integer.valueOf(data.getGspcQty())));
                        diff.setGspcdPcSecondQty("0");
                        diff.setGspcdDiffSecondQty(data.getGspcStockQty());
                        diff.setGspcdStatus("0");
                        diff.setGspcdIsFlag("0");
                        if (!"0".equals(diff.getGspcdDiffFirstQty())) {
                            diff.setGspcdIsFlag("1");
                        }
                    }

                }
            }
        } else {
            throw new BusinessException("参数不能为空");
        }
    }

    @Override
    public List<GetPhysicalCountOutData> selectPhysicalDiff(GetPhysicalCountInData inData) {
        inData.setDiffQty("1");
        List<GetPhysicalCountOutData> list = this.physicalCountDiffMapper.selectPhysicalDiff(inData);
        if (CollUtil.isNotEmpty(list)) {
            Iterator var3 = list.iterator();

            while(var3.hasNext()) {
                GetPhysicalCountOutData physicalCountOutData = (GetPhysicalCountOutData)var3.next();
                if (StrUtil.isNotBlank(physicalCountOutData.getVaildDate())) {
                    long days = DateUtil.betweenDay(new Date(), DateUtil.parse(physicalCountOutData.getVaildDate(), "yyyyMMdd"), true);
                    physicalCountOutData.setVaild(StrUtil.toString(days));
                }

                if (StrUtil.isBlank(physicalCountOutData.getGspcdPcSecondQty())) {
                    physicalCountOutData.setGspcdPcSecondQty("0");
                    physicalCountOutData.setGspcdDiffSecondQty(physicalCountOutData.getStockQty());
                }
            }
        }

        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePhysicalDiff(GetPhysicalCountInData inData, GetLoginOutData userInfo) {
        if (!CollUtil.isEmpty(inData.getPhysicals())) {
            GaiaSdPhysicalCountDiff diff;
            for(Iterator var3 = inData.getPhysicals().iterator(); var3.hasNext(); this.physicalCountDiffMapper.updateByPrimaryKey(diff)) {
                GetPhysicalCountInData physical = (GetPhysicalCountInData)var3.next();
                GaiaSdPhysicalCountDiff record = new GaiaSdPhysicalCountDiff();
                record.setClientId(userInfo.getClient());
                record.setGspcdBrId(userInfo.getDepId());
                record.setGspcdDate(inData.getGspcDate());
                record.setGspcdType(inData.getGspcType());
                record.setGspcdVoucherId(physical.getGspcdVoucherId());
//                record.setGspcVoucherId(physical.getGspcVoucherId());
//                record.setGspcdRowNo(physical.getGspcRowNo());
                diff = physicalCountDiffMapper.selectByPrimaryKey(record);
                if (ObjectUtil.isNull(diff)) {
                    throw new BusinessException("当前商品不存在差异");
                }

                diff.setGspcdPcSecondQty(physical.getGspcdPcSecondQty());
                diff.setGspcdDiffSecondQty(physical.getGspcdDiffSecondQty());
                if ("0".equals(diff.getGspcdDiffSecondQty())) {
                    diff.setGspcdIsFlag("0");
                } else {
                    diff.setGspcdIsFlag("1");
                }
            }

        }
    }
}
