package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:17 2021/8/4
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@CsvRow("新品铺货计划")
public class NewProductDistributionPlanVo {
    /**
     * 编号
     */
    @JSONField(serialize = false)
    @CsvCell(title = "序号", index = 1, fieldNo = 1)
    private Integer index;

    /**
     * 新品铺货计划时间
     */
    @CsvCell(title = "新品铺货计划时间", index = 2, fieldNo = 1)
    private String planDate;

    /**
     * 新品入库时间
     */
    @CsvCell(title = "新品入库时间", index = 3, fieldNo = 1)
    private String enterStockDate;

    /**
     * 新品铺货计划单号
     */
    @CsvCell(title = "新品铺货计划单号", index = 4, fieldNo = 1)
    private String planCode;

    /**
     * 是否有铺货计划
     */
    @CsvCell(title = "是否有铺货计划", index = 5, fieldNo = 1)
    private String isPlan;

    /**
     * 客户
     */
    @CsvCell(title = "客户", index = 6, fieldNo = 1)
    private String client;

    /**
     * 客户名称
     */
    @CsvCell(title = "客户名称", index = 7, fieldNo = 1)
    private String clientName;

    /**
     * 商品编码
     */
    @CsvCell(title = "商品编码", index = 8, fieldNo = 1)
    private String proSelfCode;

    /**
     * 商品描述
     */
    @CsvCell(title = "商品描述", index = 9, fieldNo = 1)
    private String proDepict;

    /**
     * 规格
     */
    @CsvCell(title = "规格", index = 10, fieldNo = 1)
    private String proSpecs;

    /**
     * 生产厂家
     */
    @CsvCell(title = "生产厂家", index = 11, fieldNo = 1)
    private String proFactoryName;

    /**
     * 单位
     */
    @CsvCell(title = "单位", index = 12, fieldNo = 1)
    private String proUnit;

    /**
     * 计划铺货数量
     */
    @CsvCell(title = "计划铺货数量", index = 13, fieldNo = 1)
    private Integer planQuantity;

    /**
     * 入库数量
     */
    @CsvCell(title = "入库数量", index = 14, fieldNo = 1)
    private Integer enterStockNum;

    /**
     * 计划铺货与入库数量差异
     */
    @CsvCell(title = "计划铺货与入库数量差异", index = 15, fieldNo = 1)
    private Integer planAndActualEnterStockDiff;

    /**
     * 实际铺货数量
     */
    @CsvCell(title = "实际铺货数量", index = 16, fieldNo = 1)
    private Integer actualQuantity;

    /**
     * 计划铺货与实际铺货数量差异
     */
    @CsvCell(title = "计划铺货与实际铺货数量差异", index = 17, fieldNo = 1)
    private Integer planAndActualUseDiff;

    /**
     * 默认有效期天数 ： 90天
     */
    @CsvCell(title = "默认有效期天数", index = 18, fieldNo = 1)
    private String defaultPlanDays;

    /**
     * 实际有效期天数
     */
    @CsvCell(title = "实际有效期天数", index = 19, fieldNo = 1)
    private String actualPlanDays;

    /**
     * 有效期天数是否一致
     */
    @CsvCell(title = "有效期天数是否一致", index = 20, fieldNo = 1)
    private String isPlanDaysEq;

    /**
     * 默认铺货模式 ：一次调取
     */
    @CsvCell(title = "默认铺货模式", index = 21, fieldNo = 1)
    private String defaultDistributionType;

    /**
     * 实际铺货模式
     */
    @CsvCell(title = "实际铺货模式", index = 22, fieldNo = 1)
    private String actualDistributionType;

    /**
     * 铺货模式是否一致
     */
    @CsvCell(title = "铺货模式是否一致", index = 23, fieldNo = 1)
    private String isDistributionTypeEq;

    /**
     * 默认铺货方式 ： 门店属性
     */
    @CsvCell(title = "默认铺货方式", index = 24, fieldNo = 1)
    private String defaultPlanType;

    /**
     * 实际铺货方式
     */
    @CsvCell(title = "实际铺货方式", index = 25, fieldNo = 1)
    private String actualPlanType;

    /**
     * 铺货方式是否一致
     */
    @CsvCell(title = "铺货方式是否一致", index = 26, fieldNo = 1)
    private String isPlanTypeEq;

    /**
     * 调取方式
     */
    @CsvCell(title = "调取方式", index = 28, fieldNo = 1)
    private String useType;

    /**
     * 调取次数
     */
    @CsvCell(title = "调取次数", index = 29, fieldNo = 1)
    private Integer distributionNum;

    /**
     * 首次调取时间
     */
    @CsvCell(title = "首次调取时间", index = 30, fieldNo = 1)
    private String firstDistributionTime;

    /**
     * 末次调取时间
     */
    @CsvCell(title = "末次调取时间", index = 31, fieldNo = 1)
    private String lastDistributionTime;

    /**
     * 是否需要补铺
     */
    @CsvCell(title = "是否需要补铺", index = 32, fieldNo = 1)
    private String isNeedSupplementDistribution;

    /**
     *是否执行补铺
     */
    @CsvCell(title = "是否执行补铺", index = 33, fieldNo = 1)
    private String isExecuteSupplementDistribution;

    /**
     * 补铺开单时间
     */
    @CsvCell(title = "补铺开单时间", index = 34, fieldNo = 1)
    private String supplementDistributionTime;

    /**
     * 是否勾选加盟店
     */
    @CsvCell(title = "是否勾选加盟店" ,index = 27 ,fieldNo = 1)
    private String isIncludeClient;

}
