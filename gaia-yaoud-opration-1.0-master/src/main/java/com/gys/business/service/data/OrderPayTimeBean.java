package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhangdong
 * @date 2021/7/20 14:26
 */
@Data
public class OrderPayTimeBean {

//    @NotEmpty(message = "payTime cannot be empty")
    private String payTime;//支付时间
    @NotNull(message = "orderId cannot be null")
    private String orderId;//订单id

    private String client;
    private String stoCode;
}
