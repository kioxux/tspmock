//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ProEffectOutData {
    private String gssbBrId;
    private String proCode;
    private String proName;
    private String proSpecs;
    private String proUnit;
    private String prcAmount;
    private String gssbBatchNo;
    private String gssbVaildDate;
    private String prcEffectDate;
    private String supplyCode;
    private String supplyName;
    private String proFactoryName;
    private String proPlace;
    private String proForm;
    private String gssbBatch;
    private Integer index;

    public ProEffectOutData() {
    }

    public String getGssbBrId() {
        return this.gssbBrId;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getPrcAmount() {
        return this.prcAmount;
    }

    public String getGssbBatchNo() {
        return this.gssbBatchNo;
    }

    public String getGssbVaildDate() {
        return this.gssbVaildDate;
    }

    public String getPrcEffectDate() {
        return this.prcEffectDate;
    }

    public String getSupplyCode() {
        return this.supplyCode;
    }

    public String getSupplyName() {
        return this.supplyName;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getGssbBatch() {
        return this.gssbBatch;
    }

    public Integer getIndex() {
        return this.index;
    }

    public void setGssbBrId(final String gssbBrId) {
        this.gssbBrId = gssbBrId;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setPrcAmount(final String prcAmount) {
        this.prcAmount = prcAmount;
    }

    public void setGssbBatchNo(final String gssbBatchNo) {
        this.gssbBatchNo = gssbBatchNo;
    }

    public void setGssbVaildDate(final String gssbVaildDate) {
        this.gssbVaildDate = gssbVaildDate;
    }

    public void setPrcEffectDate(final String prcEffectDate) {
        this.prcEffectDate = prcEffectDate;
    }

    public void setSupplyCode(final String supplyCode) {
        this.supplyCode = supplyCode;
    }

    public void setSupplyName(final String supplyName) {
        this.supplyName = supplyName;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setGssbBatch(final String gssbBatch) {
        this.gssbBatch = gssbBatch;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ProEffectOutData)) {
            return false;
        } else {
            ProEffectOutData other = (ProEffectOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label203: {
                    Object this$gssbBrId = this.getGssbBrId();
                    Object other$gssbBrId = other.getGssbBrId();
                    if (this$gssbBrId == null) {
                        if (other$gssbBrId == null) {
                            break label203;
                        }
                    } else if (this$gssbBrId.equals(other$gssbBrId)) {
                        break label203;
                    }

                    return false;
                }

                Object this$proCode = this.getProCode();
                Object other$proCode = other.getProCode();
                if (this$proCode == null) {
                    if (other$proCode != null) {
                        return false;
                    }
                } else if (!this$proCode.equals(other$proCode)) {
                    return false;
                }

                Object this$proName = this.getProName();
                Object other$proName = other.getProName();
                if (this$proName == null) {
                    if (other$proName != null) {
                        return false;
                    }
                } else if (!this$proName.equals(other$proName)) {
                    return false;
                }

                label182: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label182;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label182;
                    }

                    return false;
                }

                label175: {
                    Object this$proUnit = this.getProUnit();
                    Object other$proUnit = other.getProUnit();
                    if (this$proUnit == null) {
                        if (other$proUnit == null) {
                            break label175;
                        }
                    } else if (this$proUnit.equals(other$proUnit)) {
                        break label175;
                    }

                    return false;
                }

                label168: {
                    Object this$prcAmount = this.getPrcAmount();
                    Object other$prcAmount = other.getPrcAmount();
                    if (this$prcAmount == null) {
                        if (other$prcAmount == null) {
                            break label168;
                        }
                    } else if (this$prcAmount.equals(other$prcAmount)) {
                        break label168;
                    }

                    return false;
                }

                Object this$gssbBatchNo = this.getGssbBatchNo();
                Object other$gssbBatchNo = other.getGssbBatchNo();
                if (this$gssbBatchNo == null) {
                    if (other$gssbBatchNo != null) {
                        return false;
                    }
                } else if (!this$gssbBatchNo.equals(other$gssbBatchNo)) {
                    return false;
                }

                label154: {
                    Object this$gssbVaildDate = this.getGssbVaildDate();
                    Object other$gssbVaildDate = other.getGssbVaildDate();
                    if (this$gssbVaildDate == null) {
                        if (other$gssbVaildDate == null) {
                            break label154;
                        }
                    } else if (this$gssbVaildDate.equals(other$gssbVaildDate)) {
                        break label154;
                    }

                    return false;
                }

                Object this$prcEffectDate = this.getPrcEffectDate();
                Object other$prcEffectDate = other.getPrcEffectDate();
                if (this$prcEffectDate == null) {
                    if (other$prcEffectDate != null) {
                        return false;
                    }
                } else if (!this$prcEffectDate.equals(other$prcEffectDate)) {
                    return false;
                }

                label140: {
                    Object this$supplyCode = this.getSupplyCode();
                    Object other$supplyCode = other.getSupplyCode();
                    if (this$supplyCode == null) {
                        if (other$supplyCode == null) {
                            break label140;
                        }
                    } else if (this$supplyCode.equals(other$supplyCode)) {
                        break label140;
                    }

                    return false;
                }

                Object this$supplyName = this.getSupplyName();
                Object other$supplyName = other.getSupplyName();
                if (this$supplyName == null) {
                    if (other$supplyName != null) {
                        return false;
                    }
                } else if (!this$supplyName.equals(other$supplyName)) {
                    return false;
                }

                Object this$proFactoryName = this.getProFactoryName();
                Object other$proFactoryName = other.getProFactoryName();
                if (this$proFactoryName == null) {
                    if (other$proFactoryName != null) {
                        return false;
                    }
                } else if (!this$proFactoryName.equals(other$proFactoryName)) {
                    return false;
                }

                label119: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label119;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label119;
                    }

                    return false;
                }

                label112: {
                    Object this$proForm = this.getProForm();
                    Object other$proForm = other.getProForm();
                    if (this$proForm == null) {
                        if (other$proForm == null) {
                            break label112;
                        }
                    } else if (this$proForm.equals(other$proForm)) {
                        break label112;
                    }

                    return false;
                }

                Object this$gssbBatch = this.getGssbBatch();
                Object other$gssbBatch = other.getGssbBatch();
                if (this$gssbBatch == null) {
                    if (other$gssbBatch != null) {
                        return false;
                    }
                } else if (!this$gssbBatch.equals(other$gssbBatch)) {
                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ProEffectOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gssbBrId = this.getGssbBrId();
        result = result * 59 + ($gssbBrId == null ? 43 : $gssbBrId.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $prcAmount = this.getPrcAmount();
        result = result * 59 + ($prcAmount == null ? 43 : $prcAmount.hashCode());
        Object $gssbBatchNo = this.getGssbBatchNo();
        result = result * 59 + ($gssbBatchNo == null ? 43 : $gssbBatchNo.hashCode());
        Object $gssbVaildDate = this.getGssbVaildDate();
        result = result * 59 + ($gssbVaildDate == null ? 43 : $gssbVaildDate.hashCode());
        Object $prcEffectDate = this.getPrcEffectDate();
        result = result * 59 + ($prcEffectDate == null ? 43 : $prcEffectDate.hashCode());
        Object $supplyCode = this.getSupplyCode();
        result = result * 59 + ($supplyCode == null ? 43 : $supplyCode.hashCode());
        Object $supplyName = this.getSupplyName();
        result = result * 59 + ($supplyName == null ? 43 : $supplyName.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $gssbBatch = this.getGssbBatch();
        result = result * 59 + ($gssbBatch == null ? 43 : $gssbBatch.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        return result;
    }

    public String toString() {
        return "ProEffectOutData(gssbBrId=" + this.getGssbBrId() + ", proCode=" + this.getProCode() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", proUnit=" + this.getProUnit() + ", prcAmount=" + this.getPrcAmount() + ", gssbBatchNo=" + this.getGssbBatchNo() + ", gssbVaildDate=" + this.getGssbVaildDate() + ", prcEffectDate=" + this.getPrcEffectDate() + ", supplyCode=" + this.getSupplyCode() + ", supplyName=" + this.getSupplyName() + ", proFactoryName=" + this.getProFactoryName() + ", proPlace=" + this.getProPlace() + ", proForm=" + this.getProForm() + ", gssbBatch=" + this.getGssbBatch() + ", index=" + this.getIndex() + ")";
    }
}
