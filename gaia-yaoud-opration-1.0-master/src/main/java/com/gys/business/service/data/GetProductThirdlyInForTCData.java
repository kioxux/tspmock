package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class GetProductThirdlyInForTCData {

    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店")
    private String proSite;
    @ApiModelProperty(value = "查询内容")
    private String content;
    @ApiModelProperty(value = "商品集合")
    private String[] proArr;
    @NotNull
    @ApiModelProperty(value = "类型")
    private String type;
    @ApiModelProperty(value = "是否特殊商品 1")
    private String isSpecial;
    @ApiModelProperty(value = "匹配类型 0 模糊匹配 1 精确匹配")
    private String mateType;
    private Integer pageNum;
    private Integer pageSize;

//
//    @ApiModelProperty(value = "商品分类")
    private List<String> proClassList;

    @ApiModelProperty(value = "商品分类-新")
    private String[][] proClass;

    @ApiModelProperty(value = "销售类别")
    private List<String> saleTypes;

    @ApiModelProperty(value = "供应商")
    private String supplier;

    @ApiModelProperty(value = "生产厂家")
    private String factoryName;

    @ApiModelProperty(value = "品牌")
    private String brand;

}
