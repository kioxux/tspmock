package com.gys.business.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.PercentagePlanMapper;
import com.gys.business.mapper.entity.GaiaTichengPlan;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.PercentagePlanService;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PercentagePlanServiceImpl implements PercentagePlanService {
    private static final Logger log = LoggerFactory.getLogger(PercentagePlanServiceImpl.class);
    @Resource
    private PercentagePlanMapper planMapper;
    @Autowired
    private StoreDataService storeDataService;

    @Override
    @Transactional
    public Integer insert(PercentageInData inData) {
        Integer id = 0;
        if(ObjectUtil.isNotEmpty(inData.getPlanSuitMonth())){
            inData.setPlanSuitMonth(inData.getPlanSuitMonth().substring(0,6));
        }
        if (ObjectUtil.isEmpty(inData.getPlanName())){
            throw new BusinessException("方案名称不能为空");
        }
        if (ObjectUtil.isEmpty(inData.getPlanSuitMonth())){
            throw new BusinessException("适用月份必选");
        }
        if (inData.getStoreCodes().length <= 0){
            throw new BusinessException("适用门店必选");
        }
        int year = Integer.valueOf(inData.getPlanSuitMonth().substring(0,4));
        int month = Integer.valueOf(inData.getPlanSuitMonth().substring(4));
        inData.setPlanStartDate(inData.getPlanSuitMonth()+"01");
        inData.setPlanEndDate(DateUtil.format(DateUtil.parse(getLastDayOfMonth(year,month)), DatePattern.PURE_DATE_PATTERN));
        int count1 = planMapper.checkStartDate(inData);
        if (count1 > 0){
            throw new BusinessException("已存在相同时间段内提成方案");
        }else {
            int count2 = planMapper.checkEndDate(inData);
            if (count2 > 0){
                throw new BusinessException("已存在相同时间段内提成方案");
            }
        }
        GaiaTichengPlan tichengPlan = new GaiaTichengPlan();
        if (ObjectUtil.isNotEmpty(inData.getId())){
            tichengPlan.setId(inData.getId());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanAmtWay(inData.getPlanAmtWay());
            tichengPlan.setPlanProductWay(inData.getPlanProductWay());
            tichengPlan.setPlanIfNegative(inData.getPlanIfNegative());
            tichengPlan.setPlanRateWay(inData.getPlanRateWay());
            tichengPlan.setPlanSuitMonth(inData.getPlanSuitMonth());
            tichengPlan.setPlanUpdateDateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            planMapper.updateTichengZ(tichengPlan);
        }else {
            String planCode = planMapper.selectNextPlanCode(inData.getClient());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanCreater(inData.getPlanCreater());
            tichengPlan.setPlanCreateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanAmtWay(inData.getPlanAmtWay());
            tichengPlan.setPlanCode(planCode);
            tichengPlan.setPlanStatus("0");
            tichengPlan.setDeleteFlag("0");
            tichengPlan.setPlanProductWay(inData.getPlanProductWay());
            tichengPlan.setPlanCreaterId(inData.getPlanCreaterId());
            tichengPlan.setPlanIfNegative(inData.getPlanIfNegative());
            tichengPlan.setPlanRateWay(inData.getPlanRateWay());
            tichengPlan.setPlanSuitMonth(inData.getPlanSuitMonth());
            tichengPlan.setPlanUpdateDateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss() + "");
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            planMapper.addPlanMain(tichengPlan);
        }
        id = tichengPlan.getId();
        //初始化加盟商门店
        planMapper.deleteStoreByPid(id);
        //新增
        List<PercentageStoInData> stoList = new ArrayList<>();
        if (inData.getStoreCodes().length > 0){
            for (String  storeCode:inData.getStoreCodes()) {
                PercentageStoInData item = new PercentageStoInData();
                item.setClientId(inData.getClient());
                item.setPid(tichengPlan.getId());
                item.setPlanSuitMouth(inData.getPlanSuitMonth());
                item.setStoCode(storeCode);
                stoList.add(item);
            }
        }
        if (stoList.size() > 0 && stoList != null) {
            planMapper.addPlanStores(stoList);
        }
        //新增销售提成明细
        if (ObjectUtil.isNotEmpty(tichengPlan.getId())){
            planMapper.deleteSalePlanByPid(tichengPlan.getId());
            if (inData.getSaleInDataList() != null && inData.getSaleInDataList().size() > 0){
                List<PercentageSaleInData> saleList = new ArrayList<>();
                for (PercentageSaleInData saleItem : inData.getSaleInDataList()) {
                    saleItem.setClientId(inData.getClient());
                    saleItem.setPid(tichengPlan.getId());
                    if(ObjectUtil.isEmpty(saleItem.getProSaleClass())){
                        saleItem.setProSaleClass("");
                    }
                    saleList.add(saleItem);
                }
                if (saleList.size() > 0 && saleList != null){
                    planMapper.batchSaleTichengList(saleList);
                }
            }
            if (inData.getProInDataList().size() > 0 && inData.getProInDataList() != null){
                List<PercentageProInData> proList = new ArrayList<>();
                for (PercentageProInData proInData : inData.getProInDataList()) {
                    if (ObjectUtil.isNotEmpty(proInData.getProCode())) {
                        if (ObjectUtil.isEmpty(proInData.getTichengAmt())){
                            throw new BusinessException("商品编码为:"+ proInData.getProCode()+"的单品提成金额未填");
                        }
                        proInData.setClientId(inData.getClient());
                        proInData.setPid(tichengPlan.getId());
                        proInData.setPlanProductWay(inData.getPlanProductWay());
                        proList.add(proInData);
                    }
                }
                if (proList.size() > 0 && proList != null){
                    planMapper.batchProTichengList(proList);
                }
            }
        }
        return id;
    }

    @Override
    public PageInfo<PercentageOutData> list(PercentageInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<PercentageOutData> list = planMapper.selectTichengZList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(list)) {
            pageInfo = new PageInfo(list);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public List<Map<String, String>> selectStoList(Long planId) {
        return planMapper.selectTichengStoList(planId);
    }

    @Override
    public PercentageInData tichengDetail(Long planId) {
        PercentageInData outData = planMapper.selectTichengZDetail(planId);
        String[] dateArr = {outData.getPlanStartDate(),outData.getPlanEndDate()};
        outData.setDateArr(dateArr);
        List<Map<String,String>> stoCodeList = planMapper.selectTichengStoList(planId);
        if (stoCodeList.size() > 0 && stoCodeList != null){
            String[] array = new String[stoCodeList.size()];
            for(int i = 0; i < stoCodeList.size();i++){
                array[i] = stoCodeList.get(i).get("stoCode");
            }
            outData.setStoreCodes(array);
        }
        List<PercentageSaleInData> saleList = planMapper.selectTichengSaleList(planId);
        if (saleList.size() > 0 && saleList != null){
            outData.setSaleInDataList(saleList);
        }
        List<PercentageProInData> proList = planMapper.selectTichengProList(planId);
        if (proList.size() > 0 && proList != null){
            outData.setProInDataList(proList);
        }
        return outData;
    }

    @Override
    public void deleteSalePlan(Long saleId) {
        planMapper.deleteSalePlan(saleId);
    }

    @Override
    public void deleteProPlan(Long proPlanId) {
        planMapper.deleteProPlan(proPlanId);
    }


    @Override
    public void approve(Long planId,String planStatus) {
        planMapper.approvePlan(planId,planStatus);
    }

    @Override
    public void deletePlan(Long planId) {
        planMapper.deletePlan(planId);
    }

    @Override
    public PercentageProInData selectProductByClient(String client, String proCode) {
        PercentageProInData proOutData = planMapper.selectProductByClient(client,proCode);
        List<StoreOutData> storeOutDataList = storeDataService.getStoreList(client,null);
        for (StoreOutData outData : storeOutDataList) {
            if (ObjectUtil.isNotEmpty(outData.getStoAttribute())){
                if ("2".equals(outData.getStoAttribute())){
                    String movPrice = planMapper.selectMovPriceByDcCode(client,outData.getStoDcCode(),proCode);
                    if (ObjectUtil.isNotEmpty(movPrice)) {
                        proOutData.setProCostPrice(new BigDecimal(movPrice));
                        proOutData.setGrossProfitAmt(proOutData.getProPrice().subtract(proOutData.getProCostPrice()));
                        if (proOutData.getProPrice().compareTo(BigDecimal.ZERO) == 0){
                            proOutData.setGrossProfitRate("0.00%");
                        }else {
                            proOutData.setGrossProfitRate(proOutData.getGrossProfitAmt().divide(proOutData.getProPrice(),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"))  + "%");
                        }
                    }
                    break;
                }
            }
        }
        return proOutData;
    }

    @Override
    public List<PercentageProInData> getImportExcelDetailList(String clientId, List<Map<String, String>> importInDataList) {
        ImportProInData inData = new ImportProInData();
        inData.setClientId(clientId);
        List<String> proCodes = new ArrayList<>();
        List<PercentageProInData> resultList = new ArrayList<>();
        for (Map<String, String> map: importInDataList) {
            String proCode = map.get("proCode");
            if (ObjectUtil.isNotEmpty(proCode)){
                proCodes.add(proCode);
            }
        }
        if (proCodes.size() > 0 && proCodes != null){
            inData.setProCodes(proCodes);
            resultList = planMapper.selectProductByProCodes(inData);
            if (resultList.size() > 0 && resultList != null) {
                for (Map<String, String> map: importInDataList) {
                    String proCode = map.get("proCode");
                    boolean f = true;
                    for (PercentageProInData result : resultList) {
                        if (result.getProCode().equals(proCode)){
                            f = false;
                            break;
                        }
                    }
                    if (f){
                        throw new BusinessException("商品编码:"+proCode + "信息不存在，请核实");
                    }
                }
                List<StoreOutData> storeOutDataList = storeDataService.getStoreList(clientId,null);
                for (StoreOutData outData : storeOutDataList) {
                    if (ObjectUtil.isNotEmpty(outData.getStoAttribute())) {
                        if ("2".equals(outData.getStoAttribute())) {
                            inData.setDcCode(outData.getStoDcCode());
                            List<Map<String, String>> movPriceList = planMapper.selectMovPriceListByDcCode(inData);
                            if (movPriceList.size() > 0 && movPriceList != null) {
                                for (Map<String, String> movPriceMap : movPriceList) {
                                    String proCode = movPriceMap.get("proCode");
                                    String movPrice = "0";
                                    if (movPriceMap.containsKey("movPrice")) {
                                        movPrice = movPriceMap.get("movPrice");
                                    }
                                    for (PercentageProInData result : resultList) {
                                        if (result.getProCode().equals(proCode)) {
                                            result.setProCostPrice(new BigDecimal(movPrice));
                                            result.setGrossProfitAmt(result.getProPrice().subtract(result.getProCostPrice()));
                                            if (result.getProPrice().compareTo(BigDecimal.ZERO) == 0) {
                                                result.setGrossProfitRate("0.00%");
                                            } else {
                                                result.setGrossProfitRate(result.getGrossProfitAmt().divide(result.getProPrice(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")) + "%");
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }else{
                throw new BusinessException("未查询到对应的商品信息");
            }
            for (PercentageProInData result : resultList) {
                for (Map<String, String> map: importInDataList) {
                    String proCode = map.get("proCode");
                    if (result.getProCode().equals(proCode)){
                        result.setTichengAmt(map.get("tichengAmt"));
                    }
                }
            }
        }
        return resultList;
    }

    /**
     * 获取某月的最后一天
     *
     */
    public static String getLastDayOfMonth(int year,int month)
    {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String lastDayOfMonth = sdf.format(cal.getTime());

        return lastDayOfMonth;
    }

    /***
     * 日期减一天、加一天
     *
     * @param _date
     *            2014-11-24
     * @return 减一天：2014-11-23或(加一天：2014-11-25)
     */
    public static String checkOption(String _date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Date date = null;
        try {
            date = sdf.parse(_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cl = Calendar.getInstance();
        cl.setTime(date);
        cl.add(Calendar.MONTH, 1);
        date = cl.getTime();
        return sdf.format(date);
    }

}
