package com.gys.business.service.data.yaoshibang;

import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/7/25 14:36
 */
@Data
public class TokenUser {

    /**
     * userYsId :
     * userSex :
     * loginAccount :
     * userIdc :
     * userAddr :
     * userLoginSta : 1
     * userTel : 15106138219
     * depName : 店名
     * userId : 10000000
     * token : 0f4a8c8fce5a452594bed0a64d0a9ca2
     * userSta : 0
     * userYsZylb :
     * loginName : 梁志祥
     * client : 10000000
     * depId : 1000000000
     * userYsZyfw :
     */

    private String userYsId;
    private String userSex;
    private String loginAccount;
    private String userIdc;
    private String userAddr;
    private String userLoginSta;
    private String userTel;
    private String depName;
    private String userId;
    private String token;
    private String userSta;
    private String userYsZylb;
    private String loginName;
    private String client;
    private String depId;
    private String userYsZyfw;

    // 加盟商名称
    private String francName;
}
