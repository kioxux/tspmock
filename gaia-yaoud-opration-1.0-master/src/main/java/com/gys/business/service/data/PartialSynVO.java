package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 实时表
 *
 * @author xiaoyuan on 2021/4/28
 */
@Data
public class PartialSynVO implements Serializable {
    private static final long serialVersionUID = 133517096311528555L;

    /**
     * 加盟商
     */
    @NotNull(message = "加盟商不可为空!")
    private List<String> clientList;

    /**
     * 门店
     */
    @NotNull(message = "门店不可为空!")
    private List<PartialSynDetailsVO> brIdList;


    /**
     * 表名集合
     */
    @NotNull(message = "表名不可为空!")
    private List<String> tableName;

}
