package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/12/27 13:17
 */

@Data
public class ProductMadeDateOrValidityDateOutData {
    /**
     *生产日期
     */
    private String proMadeDate;

    /**
     * 有效期
     */
    private String proValiedDate;
}
