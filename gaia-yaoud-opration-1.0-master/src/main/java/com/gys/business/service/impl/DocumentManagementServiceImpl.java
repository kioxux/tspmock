package com.gys.business.service.impl;

import com.gys.business.mapper.DocumentManagementMapper;
import com.gys.business.service.DocumentManagementService;
import com.gys.business.service.data.ReplenishmentRecordInData;
import com.gys.business.service.data.ReplenishmentRecordOutData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaoyuan on 2020/9/11
 */
@Service
public class DocumentManagementServiceImpl implements DocumentManagementService {

    @Autowired
    private DocumentManagementMapper managementMapper;

    @Override
    public List<ReplenishmentRecordOutData> getReplenishmentRecord(ReplenishmentRecordInData inData) {
        return managementMapper.getReplenishmentRecord(inData);
    }
}
