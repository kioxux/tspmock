package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;


/**
 * @author Zhangchi
 * @since 2021/09/27/13:54
 */
@Data
@CsvRow("毛利区间数据")
public class GrossMarginOutDataCSV {
    /**
     * 加盟商
     */
    @CsvCell(title = "加盟商", index = 1, fieldNo = 1)
    private String client;
    /**
     * 客户名称
     */
    @CsvCell(title = "客户名称", index = 2, fieldNo = 1)
    private String customerName;
    /**
     * 毛利区间分类：ABCDE
     */
    @CsvCell(title = "毛利区间分类", index = 3, fieldNo = 1)
    private String intervalType;
    /**
     * 起始毛利率
     */
    @CsvCell(title = "起始毛利率", index = 4, fieldNo = 1)
    private String startValue;
    /**
     * 结束毛利率
     */
    @CsvCell(title = "结束毛利率", index = 5, fieldNo = 1)
    private String endValue;
    /**
     * 更新人
     */
    @CsvCell(title = "更新人", index = 6, fieldNo = 1)
    private String updateUser;
    /**
     * 更新时间
     */
    @CsvCell(title = "更新时间", index = 7, fieldNo = 1)
    private String updateTime;
}
