package com.gys.business.service.data;

import com.gys.business.service.data.Unqualified.UnqualifiedDto;
import com.gys.business.service.data.Unqualified.UnqualifiedSum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UnqualifiedOutData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "单号")
    private String voucherId;
    @ApiModelProperty(value = "门店编码")
    private String brId;
    @ApiModelProperty(value = "门店名称")
    private String brName;
    @ApiModelProperty(value = "创建日期")
    private String gsdhDate;
    @ApiModelProperty(value = "状态")
    private String gsdhStatus;
    @ApiModelProperty(value = "创建人")
    private String createrName;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "是否报损")
    private String lossStatus;
    @ApiModelProperty(value = "是否销毁")
    private String defaceStatus;
    @ApiModelProperty(value = "审批状态")
    private String workFlowStatus;
    @ApiModelProperty(value = "列表展示")
    List<UnqualifiedDto> list;

    @ApiModelProperty(value = "合计")
    UnqualifiedSum listNum;

}
