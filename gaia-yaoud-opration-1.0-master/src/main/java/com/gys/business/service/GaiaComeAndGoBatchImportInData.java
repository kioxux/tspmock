package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaComeAndGoDetailData;
import com.gys.business.service.data.GetReplenishDetailOutData;
import com.gys.business.service.data.ImportInData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:31 2021/7/21
 * @Description：往来管理批量导入excel入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoBatchImportInData {
    private static final long serialVersionUID = 5525835479619990817L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "导入传入参数列表")
    List<GaiaComeAndGoDetailData> importInDataList;
}
