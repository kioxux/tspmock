//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class RemoteAuditHInData {
    private String clientId;
    private String gsrahVoucherId;
    private String gsrahBrId;
    private String gsrahBrName;
    private String gsrahUploadDate;
    private String gsrahUploadTime;
    private String gsrahUploadEmp;
    private String gsrahUploadFlag;
    private String gsrahDate;
    private String gsrahTime;
    private String gsrahResult;
    private String gsrahPharmacistId;
    private String gsrahPharmacistName;
    private String gsrahCustName;
    private String gsrahCustSex;
    private String gsrahCustAge;
    private String gsrahCustIdcard;
    private String gsrahCustMobile;
    private String gsrahStatus;
    private String gsrahRecipelPicAddress;
    private String gsrahBillNo;
    private List<RemoteAuditDInData> auditDs;
    private Integer pageNum;
    private Integer pageSize;

    public RemoteAuditHInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrahVoucherId() {
        return this.gsrahVoucherId;
    }

    public String getGsrahBrId() {
        return this.gsrahBrId;
    }

    public String getGsrahBrName() {
        return this.gsrahBrName;
    }

    public String getGsrahUploadDate() {
        return this.gsrahUploadDate;
    }

    public String getGsrahUploadTime() {
        return this.gsrahUploadTime;
    }

    public String getGsrahUploadEmp() {
        return this.gsrahUploadEmp;
    }

    public String getGsrahUploadFlag() {
        return this.gsrahUploadFlag;
    }

    public String getGsrahDate() {
        return this.gsrahDate;
    }

    public String getGsrahTime() {
        return this.gsrahTime;
    }

    public String getGsrahResult() {
        return this.gsrahResult;
    }

    public String getGsrahPharmacistId() {
        return this.gsrahPharmacistId;
    }

    public String getGsrahPharmacistName() {
        return this.gsrahPharmacistName;
    }

    public String getGsrahCustName() {
        return this.gsrahCustName;
    }

    public String getGsrahCustSex() {
        return this.gsrahCustSex;
    }

    public String getGsrahCustAge() {
        return this.gsrahCustAge;
    }

    public String getGsrahCustIdcard() {
        return this.gsrahCustIdcard;
    }

    public String getGsrahCustMobile() {
        return this.gsrahCustMobile;
    }

    public String getGsrahStatus() {
        return this.gsrahStatus;
    }

    public String getGsrahRecipelPicAddress() {
        return this.gsrahRecipelPicAddress;
    }

    public String getGsrahBillNo() {
        return this.gsrahBillNo;
    }

    public List<RemoteAuditDInData> getAuditDs() {
        return this.auditDs;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrahVoucherId(final String gsrahVoucherId) {
        this.gsrahVoucherId = gsrahVoucherId;
    }

    public void setGsrahBrId(final String gsrahBrId) {
        this.gsrahBrId = gsrahBrId;
    }

    public void setGsrahBrName(final String gsrahBrName) {
        this.gsrahBrName = gsrahBrName;
    }

    public void setGsrahUploadDate(final String gsrahUploadDate) {
        this.gsrahUploadDate = gsrahUploadDate;
    }

    public void setGsrahUploadTime(final String gsrahUploadTime) {
        this.gsrahUploadTime = gsrahUploadTime;
    }

    public void setGsrahUploadEmp(final String gsrahUploadEmp) {
        this.gsrahUploadEmp = gsrahUploadEmp;
    }

    public void setGsrahUploadFlag(final String gsrahUploadFlag) {
        this.gsrahUploadFlag = gsrahUploadFlag;
    }

    public void setGsrahDate(final String gsrahDate) {
        this.gsrahDate = gsrahDate;
    }

    public void setGsrahTime(final String gsrahTime) {
        this.gsrahTime = gsrahTime;
    }

    public void setGsrahResult(final String gsrahResult) {
        this.gsrahResult = gsrahResult;
    }

    public void setGsrahPharmacistId(final String gsrahPharmacistId) {
        this.gsrahPharmacistId = gsrahPharmacistId;
    }

    public void setGsrahPharmacistName(final String gsrahPharmacistName) {
        this.gsrahPharmacistName = gsrahPharmacistName;
    }

    public void setGsrahCustName(final String gsrahCustName) {
        this.gsrahCustName = gsrahCustName;
    }

    public void setGsrahCustSex(final String gsrahCustSex) {
        this.gsrahCustSex = gsrahCustSex;
    }

    public void setGsrahCustAge(final String gsrahCustAge) {
        this.gsrahCustAge = gsrahCustAge;
    }

    public void setGsrahCustIdcard(final String gsrahCustIdcard) {
        this.gsrahCustIdcard = gsrahCustIdcard;
    }

    public void setGsrahCustMobile(final String gsrahCustMobile) {
        this.gsrahCustMobile = gsrahCustMobile;
    }

    public void setGsrahStatus(final String gsrahStatus) {
        this.gsrahStatus = gsrahStatus;
    }

    public void setGsrahRecipelPicAddress(final String gsrahRecipelPicAddress) {
        this.gsrahRecipelPicAddress = gsrahRecipelPicAddress;
    }

    public void setGsrahBillNo(final String gsrahBillNo) {
        this.gsrahBillNo = gsrahBillNo;
    }

    public void setAuditDs(final List<RemoteAuditDInData> auditDs) {
        this.auditDs = auditDs;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RemoteAuditHInData)) {
            return false;
        } else {
            RemoteAuditHInData other = (RemoteAuditHInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label299: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label299;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label299;
                    }

                    return false;
                }

                Object this$gsrahVoucherId = this.getGsrahVoucherId();
                Object other$gsrahVoucherId = other.getGsrahVoucherId();
                if (this$gsrahVoucherId == null) {
                    if (other$gsrahVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsrahVoucherId.equals(other$gsrahVoucherId)) {
                    return false;
                }

                Object this$gsrahBrId = this.getGsrahBrId();
                Object other$gsrahBrId = other.getGsrahBrId();
                if (this$gsrahBrId == null) {
                    if (other$gsrahBrId != null) {
                        return false;
                    }
                } else if (!this$gsrahBrId.equals(other$gsrahBrId)) {
                    return false;
                }

                label278: {
                    Object this$gsrahBrName = this.getGsrahBrName();
                    Object other$gsrahBrName = other.getGsrahBrName();
                    if (this$gsrahBrName == null) {
                        if (other$gsrahBrName == null) {
                            break label278;
                        }
                    } else if (this$gsrahBrName.equals(other$gsrahBrName)) {
                        break label278;
                    }

                    return false;
                }

                label271: {
                    Object this$gsrahUploadDate = this.getGsrahUploadDate();
                    Object other$gsrahUploadDate = other.getGsrahUploadDate();
                    if (this$gsrahUploadDate == null) {
                        if (other$gsrahUploadDate == null) {
                            break label271;
                        }
                    } else if (this$gsrahUploadDate.equals(other$gsrahUploadDate)) {
                        break label271;
                    }

                    return false;
                }

                label264: {
                    Object this$gsrahUploadTime = this.getGsrahUploadTime();
                    Object other$gsrahUploadTime = other.getGsrahUploadTime();
                    if (this$gsrahUploadTime == null) {
                        if (other$gsrahUploadTime == null) {
                            break label264;
                        }
                    } else if (this$gsrahUploadTime.equals(other$gsrahUploadTime)) {
                        break label264;
                    }

                    return false;
                }

                Object this$gsrahUploadEmp = this.getGsrahUploadEmp();
                Object other$gsrahUploadEmp = other.getGsrahUploadEmp();
                if (this$gsrahUploadEmp == null) {
                    if (other$gsrahUploadEmp != null) {
                        return false;
                    }
                } else if (!this$gsrahUploadEmp.equals(other$gsrahUploadEmp)) {
                    return false;
                }

                label250: {
                    Object this$gsrahUploadFlag = this.getGsrahUploadFlag();
                    Object other$gsrahUploadFlag = other.getGsrahUploadFlag();
                    if (this$gsrahUploadFlag == null) {
                        if (other$gsrahUploadFlag == null) {
                            break label250;
                        }
                    } else if (this$gsrahUploadFlag.equals(other$gsrahUploadFlag)) {
                        break label250;
                    }

                    return false;
                }

                Object this$gsrahDate = this.getGsrahDate();
                Object other$gsrahDate = other.getGsrahDate();
                if (this$gsrahDate == null) {
                    if (other$gsrahDate != null) {
                        return false;
                    }
                } else if (!this$gsrahDate.equals(other$gsrahDate)) {
                    return false;
                }

                label236: {
                    Object this$gsrahTime = this.getGsrahTime();
                    Object other$gsrahTime = other.getGsrahTime();
                    if (this$gsrahTime == null) {
                        if (other$gsrahTime == null) {
                            break label236;
                        }
                    } else if (this$gsrahTime.equals(other$gsrahTime)) {
                        break label236;
                    }

                    return false;
                }

                Object this$gsrahResult = this.getGsrahResult();
                Object other$gsrahResult = other.getGsrahResult();
                if (this$gsrahResult == null) {
                    if (other$gsrahResult != null) {
                        return false;
                    }
                } else if (!this$gsrahResult.equals(other$gsrahResult)) {
                    return false;
                }

                Object this$gsrahPharmacistId = this.getGsrahPharmacistId();
                Object other$gsrahPharmacistId = other.getGsrahPharmacistId();
                if (this$gsrahPharmacistId == null) {
                    if (other$gsrahPharmacistId != null) {
                        return false;
                    }
                } else if (!this$gsrahPharmacistId.equals(other$gsrahPharmacistId)) {
                    return false;
                }

                label215: {
                    Object this$gsrahPharmacistName = this.getGsrahPharmacistName();
                    Object other$gsrahPharmacistName = other.getGsrahPharmacistName();
                    if (this$gsrahPharmacistName == null) {
                        if (other$gsrahPharmacistName == null) {
                            break label215;
                        }
                    } else if (this$gsrahPharmacistName.equals(other$gsrahPharmacistName)) {
                        break label215;
                    }

                    return false;
                }

                label208: {
                    Object this$gsrahCustName = this.getGsrahCustName();
                    Object other$gsrahCustName = other.getGsrahCustName();
                    if (this$gsrahCustName == null) {
                        if (other$gsrahCustName == null) {
                            break label208;
                        }
                    } else if (this$gsrahCustName.equals(other$gsrahCustName)) {
                        break label208;
                    }

                    return false;
                }

                Object this$gsrahCustSex = this.getGsrahCustSex();
                Object other$gsrahCustSex = other.getGsrahCustSex();
                if (this$gsrahCustSex == null) {
                    if (other$gsrahCustSex != null) {
                        return false;
                    }
                } else if (!this$gsrahCustSex.equals(other$gsrahCustSex)) {
                    return false;
                }

                Object this$gsrahCustAge = this.getGsrahCustAge();
                Object other$gsrahCustAge = other.getGsrahCustAge();
                if (this$gsrahCustAge == null) {
                    if (other$gsrahCustAge != null) {
                        return false;
                    }
                } else if (!this$gsrahCustAge.equals(other$gsrahCustAge)) {
                    return false;
                }

                label187: {
                    Object this$gsrahCustIdcard = this.getGsrahCustIdcard();
                    Object other$gsrahCustIdcard = other.getGsrahCustIdcard();
                    if (this$gsrahCustIdcard == null) {
                        if (other$gsrahCustIdcard == null) {
                            break label187;
                        }
                    } else if (this$gsrahCustIdcard.equals(other$gsrahCustIdcard)) {
                        break label187;
                    }

                    return false;
                }

                Object this$gsrahCustMobile = this.getGsrahCustMobile();
                Object other$gsrahCustMobile = other.getGsrahCustMobile();
                if (this$gsrahCustMobile == null) {
                    if (other$gsrahCustMobile != null) {
                        return false;
                    }
                } else if (!this$gsrahCustMobile.equals(other$gsrahCustMobile)) {
                    return false;
                }

                Object this$gsrahStatus = this.getGsrahStatus();
                Object other$gsrahStatus = other.getGsrahStatus();
                if (this$gsrahStatus == null) {
                    if (other$gsrahStatus != null) {
                        return false;
                    }
                } else if (!this$gsrahStatus.equals(other$gsrahStatus)) {
                    return false;
                }

                label166: {
                    Object this$gsrahRecipelPicAddress = this.getGsrahRecipelPicAddress();
                    Object other$gsrahRecipelPicAddress = other.getGsrahRecipelPicAddress();
                    if (this$gsrahRecipelPicAddress == null) {
                        if (other$gsrahRecipelPicAddress == null) {
                            break label166;
                        }
                    } else if (this$gsrahRecipelPicAddress.equals(other$gsrahRecipelPicAddress)) {
                        break label166;
                    }

                    return false;
                }

                label159: {
                    Object this$gsrahBillNo = this.getGsrahBillNo();
                    Object other$gsrahBillNo = other.getGsrahBillNo();
                    if (this$gsrahBillNo == null) {
                        if (other$gsrahBillNo == null) {
                            break label159;
                        }
                    } else if (this$gsrahBillNo.equals(other$gsrahBillNo)) {
                        break label159;
                    }

                    return false;
                }

                label152: {
                    Object this$auditDs = this.getAuditDs();
                    Object other$auditDs = other.getAuditDs();
                    if (this$auditDs == null) {
                        if (other$auditDs == null) {
                            break label152;
                        }
                    } else if (this$auditDs.equals(other$auditDs)) {
                        break label152;
                    }

                    return false;
                }

                Object this$pageNum = this.getPageNum();
                Object other$pageNum = other.getPageNum();
                if (this$pageNum == null) {
                    if (other$pageNum != null) {
                        return false;
                    }
                } else if (!this$pageNum.equals(other$pageNum)) {
                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RemoteAuditHInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsrahVoucherId = this.getGsrahVoucherId();
        result = result * 59 + ($gsrahVoucherId == null ? 43 : $gsrahVoucherId.hashCode());
        Object $gsrahBrId = this.getGsrahBrId();
        result = result * 59 + ($gsrahBrId == null ? 43 : $gsrahBrId.hashCode());
        Object $gsrahBrName = this.getGsrahBrName();
        result = result * 59 + ($gsrahBrName == null ? 43 : $gsrahBrName.hashCode());
        Object $gsrahUploadDate = this.getGsrahUploadDate();
        result = result * 59 + ($gsrahUploadDate == null ? 43 : $gsrahUploadDate.hashCode());
        Object $gsrahUploadTime = this.getGsrahUploadTime();
        result = result * 59 + ($gsrahUploadTime == null ? 43 : $gsrahUploadTime.hashCode());
        Object $gsrahUploadEmp = this.getGsrahUploadEmp();
        result = result * 59 + ($gsrahUploadEmp == null ? 43 : $gsrahUploadEmp.hashCode());
        Object $gsrahUploadFlag = this.getGsrahUploadFlag();
        result = result * 59 + ($gsrahUploadFlag == null ? 43 : $gsrahUploadFlag.hashCode());
        Object $gsrahDate = this.getGsrahDate();
        result = result * 59 + ($gsrahDate == null ? 43 : $gsrahDate.hashCode());
        Object $gsrahTime = this.getGsrahTime();
        result = result * 59 + ($gsrahTime == null ? 43 : $gsrahTime.hashCode());
        Object $gsrahResult = this.getGsrahResult();
        result = result * 59 + ($gsrahResult == null ? 43 : $gsrahResult.hashCode());
        Object $gsrahPharmacistId = this.getGsrahPharmacistId();
        result = result * 59 + ($gsrahPharmacistId == null ? 43 : $gsrahPharmacistId.hashCode());
        Object $gsrahPharmacistName = this.getGsrahPharmacistName();
        result = result * 59 + ($gsrahPharmacistName == null ? 43 : $gsrahPharmacistName.hashCode());
        Object $gsrahCustName = this.getGsrahCustName();
        result = result * 59 + ($gsrahCustName == null ? 43 : $gsrahCustName.hashCode());
        Object $gsrahCustSex = this.getGsrahCustSex();
        result = result * 59 + ($gsrahCustSex == null ? 43 : $gsrahCustSex.hashCode());
        Object $gsrahCustAge = this.getGsrahCustAge();
        result = result * 59 + ($gsrahCustAge == null ? 43 : $gsrahCustAge.hashCode());
        Object $gsrahCustIdcard = this.getGsrahCustIdcard();
        result = result * 59 + ($gsrahCustIdcard == null ? 43 : $gsrahCustIdcard.hashCode());
        Object $gsrahCustMobile = this.getGsrahCustMobile();
        result = result * 59 + ($gsrahCustMobile == null ? 43 : $gsrahCustMobile.hashCode());
        Object $gsrahStatus = this.getGsrahStatus();
        result = result * 59 + ($gsrahStatus == null ? 43 : $gsrahStatus.hashCode());
        Object $gsrahRecipelPicAddress = this.getGsrahRecipelPicAddress();
        result = result * 59 + ($gsrahRecipelPicAddress == null ? 43 : $gsrahRecipelPicAddress.hashCode());
        Object $gsrahBillNo = this.getGsrahBillNo();
        result = result * 59 + ($gsrahBillNo == null ? 43 : $gsrahBillNo.hashCode());
        Object $auditDs = this.getAuditDs();
        result = result * 59 + ($auditDs == null ? 43 : $auditDs.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        return result;
    }

    public String toString() {
        return "RemoteAuditHInData(clientId=" + this.getClientId() + ", gsrahVoucherId=" + this.getGsrahVoucherId() + ", gsrahBrId=" + this.getGsrahBrId() + ", gsrahBrName=" + this.getGsrahBrName() + ", gsrahUploadDate=" + this.getGsrahUploadDate() + ", gsrahUploadTime=" + this.getGsrahUploadTime() + ", gsrahUploadEmp=" + this.getGsrahUploadEmp() + ", gsrahUploadFlag=" + this.getGsrahUploadFlag() + ", gsrahDate=" + this.getGsrahDate() + ", gsrahTime=" + this.getGsrahTime() + ", gsrahResult=" + this.getGsrahResult() + ", gsrahPharmacistId=" + this.getGsrahPharmacistId() + ", gsrahPharmacistName=" + this.getGsrahPharmacistName() + ", gsrahCustName=" + this.getGsrahCustName() + ", gsrahCustSex=" + this.getGsrahCustSex() + ", gsrahCustAge=" + this.getGsrahCustAge() + ", gsrahCustIdcard=" + this.getGsrahCustIdcard() + ", gsrahCustMobile=" + this.getGsrahCustMobile() + ", gsrahStatus=" + this.getGsrahStatus() + ", gsrahRecipelPicAddress=" + this.getGsrahRecipelPicAddress() + ", gsrahBillNo=" + this.getGsrahBillNo() + ", auditDs=" + this.getAuditDs() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
