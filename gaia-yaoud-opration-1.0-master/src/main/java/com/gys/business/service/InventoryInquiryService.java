package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface InventoryInquiryService {
    /**
     * FX库存查询
     * @param inData
     * @return
     */
    List<InventoryInquiryOutData> getInventoryInquiryList(InventoryInquiryInData inData);

    /**
     * Web库存查询
     * @param inData
     * @return
     */
    List<InventoryInquiryOutData> getInventoryInquiryListWeb(InventoryInquiryInData inData);

    PageInfo<InventoryInquiryOutData> getEexpiryDateWarning(InventoryInquiryInData inDatam);
    PageInfo<InventoryInquiryOutData> getEexpiryDateWarningAll(InventoryInquiryInData inDatam);

    void getEexpiryDateWarningExport(InventoryInquiryInData inData, HttpServletResponse response) throws Exception;

    PageInfo inventoryInquiryListByClient(InventoryInquiryInData inData);


    PageInfo inventoryInquiryListByStoreAndDc(InventoryInquiryInData inData);


    //查询条件：销售等级
    List<MemberSaleClass> findSaleClass(InventoryInquiryInData inData);

    //查询条件：商品定位
    List<MemberSalePosition> findSalePosition(InventoryInquiryInData inData);

    //查询条件：商品自分类
    List<MemberProsClass> findProsClass(InventoryInquiryInData inData);

    //查询条件：禁止销售
    List<MemberPurchase> findPurchase(InventoryInquiryInData inData);

    //查询条件：自定义1
    List<MemberZDY1> findZDY1(InventoryInquiryInData inData);

    //查询条件：自定义2
    List<MemberZDY2> findZDY2(InventoryInquiryInData inData);

    //查询条件：自定义3
    List<MemberZDY3> findZDY3(InventoryInquiryInData inData);

    //查询条件：自定义4
    List<MemberZDY4> findZDY4(InventoryInquiryInData inData);

    //查询条件：自定义5
    List<MemberZDY5> findZDY5(InventoryInquiryInData inData);

    //搜索条件
    JsonResult searchBox(InventoryInquiryInData inData);

    JsonResult zdyBox(InventoryInquiryInData inData);

    JsonResult search(InventoryInquiryInData inData);
    //自定义匹配
    ZDYNameOutData zdyName(InventoryInquiryInData inData);

    void inventoryInquiryListByStoreExport(InventoryInquiryInData inData, HttpServletResponse response) throws IOException;
}
