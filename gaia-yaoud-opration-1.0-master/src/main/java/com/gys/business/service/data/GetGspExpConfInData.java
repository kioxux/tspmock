package com.gys.business.service.data;

import java.util.List;

import lombok.Data;


@Data
public class GetGspExpConfInData {
    // 加盟商
    private String client;
    // 类型(1-供应商,2-客户,3-商品,4-报表预警天数)
    private String type;
    // 创建人ID
    private String userId;
    // 创建人
    private String createUser;
	// 字段明细内容
    private List<GspExpConfData> gspExpConfs;
}
