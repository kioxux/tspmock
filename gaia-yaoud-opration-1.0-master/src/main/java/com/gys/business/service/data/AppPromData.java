package com.gys.business.service.data;

import lombok.Data;

/**
 * @author shentao
 */
@Data
public class AppPromData {
    private String promId;
    private String promName;
    private String theme;
    private String remarks;
    private String promType;
    private String part;
    private String para1;
    private String para2;
    private String para3;
    private String para4;
    private SinglePromDetail singlePromDetail;
}
