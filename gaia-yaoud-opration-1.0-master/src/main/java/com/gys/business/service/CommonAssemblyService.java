package com.gys.business.service;

import com.gys.business.service.data.ProvCityInData;
import com.gys.common.data.JsonResult;

/**
 *  @author SunJiaNan
 *  @version 1.0
 *  @date 2021/11/16 16:05
 *  @description 公用数据相关接口
 */
public interface CommonAssemblyService {

    JsonResult getProvCityList();

    JsonResult getClientListByProvCity(ProvCityInData inData);

}
