//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetProductBaseInData;
import com.gys.business.service.data.GetProductOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface ProductService {
    List<GetProductOutData> findByCondition(GetProductBaseInData inData);

    PageInfo<GetProductOutData> findByPage(GetProductBaseInData inData);

    Boolean saveProduct(GetProductBaseInData inData);

    Boolean editProduct(GetProductBaseInData inData);

    Boolean deleteProducts(GetProductBaseInData inData);
}
