//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.AddAddBoxOutData;
import com.gys.business.service.data.AddBoxInfoOutData;
import com.gys.business.service.data.GetOneAddBoxOutData;
import java.util.List;

public interface AddBoxService {
    GetOneAddBoxOutData getOneAddBox(AddBoxInfoOutData inData);

    List<AddBoxInfoOutData> getAddBox(AddBoxInfoOutData inData);

    void examine(GetOneAddBoxOutData inData);

    void addAddBox(AddAddBoxOutData inData);
}
