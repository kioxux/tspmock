package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MemberListTotalOutData implements Serializable {

    private static final long serialVersionUID = 1629464568751381941L;

    @ApiModelProperty(value = "门店数量")
    private int storeCount;

    @ApiModelProperty(value = "会员数量")
    private int memberCount;

    @ApiModelProperty(value = "累计总金额汇总")
    private BigDecimal totalAmt;

    @ApiModelProperty(value = "年度累计总金额汇总")
    private BigDecimal yearTotalAmt;

    @ApiModelProperty(value = "累计积分汇总")
    private BigDecimal totalIntegeral;

    @ApiModelProperty(value = "年度累计积分汇总")
    private BigDecimal yearTotalIntegeral;

    @ApiModelProperty(value = "当前积分汇总")
    private BigDecimal currIntegeral;

    @ApiModelProperty(value = "储值卡余额汇总")
    private BigDecimal currCardAmt;

    @ApiModelProperty(value = "门店数量")
    private int brName;

    @ApiModelProperty(value = "会员卡数量")
    private int cardId;
}
