package com.gys.business.service.data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/24 22:07
 */
@Data
public class DcReplenishForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(value = "需求流水号", example = "JCD2021070001")
    private String voucherId;
    @ApiModelProperty(value = "商品编码", example = "100045987")
    private String proSelfCode;
    @ApiModelProperty(value = "需求状态：0-未处理 1-已处理", example = "0")
    private Integer status;
    @ApiModelProperty(value = "起始日期", example = "2021-07-01")
    private String startDate;
    @ApiModelProperty(value = "截止日期", example = "2021-07-09")
    private String endDate;
}
