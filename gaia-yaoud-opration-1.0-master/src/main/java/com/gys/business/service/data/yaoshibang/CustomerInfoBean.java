package com.gys.business.service.data.yaoshibang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/6/18 21:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerInfoBean {

    private String client;
    private String site;

}
