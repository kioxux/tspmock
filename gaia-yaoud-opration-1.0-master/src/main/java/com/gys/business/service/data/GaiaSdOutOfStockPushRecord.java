package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value="com-gys-business-service-data-GaiaSdOutOfStockPushRecord")
@Data
public class GaiaSdOutOfStockPushRecord implements Serializable {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 门店
    */
    @ApiModelProperty(value="门店")
    private String brId;

    /**
    * 消息类型 1-无配送中心 2-有配送中心
    */
    @ApiModelProperty(value="消息类型 1-公司级缺断货推送(无配送中心) 2-(有配送中心)")
    private Integer type;

    /**
    * 是否查看 0-未读 1-已读
    */
    @ApiModelProperty(value="是否查看 0-未读 1-已读")
    private Integer status;

    /**
    * 推送时间
    */
    @ApiModelProperty(value="推送时间")
    private Date pushTime;

    /**
    * 推送人员
    */
    @ApiModelProperty(value="推送人员")
    private String pushEmp;

    /**
    * 读取时间
    */
    @ApiModelProperty(value="读取时间")
    private Date readTime;

    /**
    * 读取人员
    */
    @ApiModelProperty(value="读取人员")
    private String readEmp;

    /**
     * 接收用户ID
     */
    @ApiModelProperty(value="接收用户ID")
    private String receiveEmp;


}