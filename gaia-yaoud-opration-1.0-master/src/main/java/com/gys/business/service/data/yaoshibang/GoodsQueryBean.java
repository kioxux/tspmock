package com.gys.business.service.data.yaoshibang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhangdong
 * @date 2021/6/20 15:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsQueryBean {

    private String client;
    private String name;
    private String proSite;
    private String soCustomerId;
}
