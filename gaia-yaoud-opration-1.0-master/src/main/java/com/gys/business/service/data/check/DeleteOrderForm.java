package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/14 16:18
 **/
@Data
public class DeleteOrderForm {
    @ApiModelProperty(value = "加盟商", example = "10000001", hidden = true)
    private String client;
    @ApiModelProperty(value = "用户ID", example = "1116")
    private String userId;
    @ApiModelProperty(value = "补货单号", example = "PD091232001")
    private String replenishCode;
    @ApiModelProperty(value = "门店编码", example = "10001")
    private String storeId;
    @ApiModelProperty(value = "删除补货明细", example = "[]")
    private List<DeleteProVO> detailList;

    @Data
    public static class DeleteProVO {
        @ApiModelProperty(value = "商品编码", example = "4210003")
        private String proSelfCode;
    }
}
