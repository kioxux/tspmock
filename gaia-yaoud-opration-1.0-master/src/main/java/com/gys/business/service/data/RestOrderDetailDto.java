package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class RestOrderDetailDto implements Serializable {
    private static final long serialVersionUID = 8431509778903834582L;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 货位号
     */
    private String wmHwhBm;

    /**
     * 医生名称
     */
    private String doctorName;

    /**
     * 税率
     */
    private String gssdMovTax;

    /**
     * 商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区
     */
    private String proStorageArea;

    /**
     * 有效期
     */
    private String gssdValidDate;

    /**
     * 批号
     */
    private String gssdBatchNo;

    /**
     * 厂家名称
     */
    private String proFactoryName;
    /**
     * 规格
     */
    private String spec;
    /**
     * 编码
     */
    private String proCode;
    /**
     * 数量
     */
    private String num;
    /**
     * 初始值数量
     */
    private String gssdOriQty;

    /**
     * 贴数
     */
    private String gssdDose;

    /**
     * 合计
     */
    private String total;
    /**
     * 实价
     */
    private String price;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 零售价
     */
    private String prcAmount;
    /**
     *
     */
    private String prcAmountD;
    /**
     * 折扣
     */
    private String disc;
    /**
     * 名称
     */
    private String proName;
    /**
     * 通用名称
     */
    private String proCommonName;
    /**
     * 状态
     */
    private String status;

    /**
     * 处方类别
     */
    private String proPresclass;

    /**
     * (商品主数据)商品定位
     */
    private String proPosition;


    /**
     * 参加促销类型编号
     */
    private String gssdPmId;

    /**
     * 促销单号id
     */
    private String gssdPmActivityId;

    /**
     * 促销参数
     */
    private String gssdPmActivityFlag;

    /**
     * 营业员id
     */
    private String gssdSalerId;

    /**
     * 营业员名称
     */
    private String gssdSalerName;

    /**
     * 行号
     */
    private String gssdSerial;

    /**
     * 医生编码
     */
    private String gssdDoctorId;

    /**
     * 医生加点金额
     */
    private BigDecimal doctorAddAmt;

    /**
     * 医保刷卡数量
     */
    private String proZdy1;

    /**
     * 库存
     */
    private BigDecimal gssQty;

    /**
     * 状态
     */
    private String proStatus;
    /**
     * 医保编码
     */
    private String proIfMed;
    /**
     * 等级
     */
    private String saleClass;

    /**
     * 零售额总计
     */
    private String gsshNormalAmt;

    /**
     * 实收金额总计
     */
    private String gsshYsAmt;

    /**
     * 折让金额总计
     */
    private String gsshZkAmt;

    /**
     * 会员卡号
     */
    private String salesMemberCardNum;

    /**
     * 会员名称
     */
    private String salesMemberName;

    /**
     * 当前积分
     */
    private String pointsThisTime;

    /**
     * 总积分
     */
    private String gsmbcIntegral;

    /**
     * 不打折
     */
    private String proBdz;

    /**
     * (明细)特殊药品购买人身份证
     */
    private String gssdSpecialmedIdcard;

    /**
     * (明细)特殊药品购买人姓名
     */
    private String gssdSpecialmedName;

    /**
     * (明细)特殊药品购买人性别
     */
    private String gssdSpecialmedSex;

    /**
     * (明细)特殊药品购买人出生日期
     */
    private String gssdSpecialmedBirthday;

    /**
     * (明细)特殊药品购买人手机
     */
    private String gssdSpecialmedMobile;

    /**
     * (明细)特殊药品购买人地址
     */
    private String gssdSpecialmedAddress;

    /**
     * (明细)特殊药品电子监管码
     */
    private String gssdSpecialmedEcode;


    /**
     * 备注
     */
    private String remarks;

    /**
     * (明细)兑换单个积分
     */
    private String gssdIntegralExchangeSingle;

    /**
     * (明细)兑换积分汇总
     */
    private String gssdIntegralExchangeCollect;

    /**
     * (明细)单个加价兑换积分金额
     */
    private String gssdIntegralExchangeAmtSingle;

    /**
     * (明细)加价兑换积分金额汇总
     */
    private String gssdIntegralExchangeAmtCollect;


    /**
     * 是否拆零
     */
    private String proIfpart;

    /**
     * (商品主数据)拆零单位
     */
    private String proPartUint;

    /**
     * (商品主数据)拆零比例
     */
    private String proPartRate;

    /**
     * 批次库存
     */
    private String batchNoQty;

    /**
     * 判断当前商品是否挂单
     */
    private String gssdPendingOrder;

    /**
     * 商品成分
     */
    private String proCompclass;

    /**
     * 国家医保编码
     */
    private String proMedProdctcode;

    /**
     * 批次
     */
    private String gsbcBatch;

    /**
     * 虚拟商品
     */
    private String gssdIfXnsp;

    /**
     * 批次移动数量
     */
    private String gsbcQty;


    /**
     * 商品最低价格
     */
    private BigDecimal proLowerPrice;
}
