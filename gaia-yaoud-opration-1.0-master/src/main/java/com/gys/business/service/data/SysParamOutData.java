package com.gys.business.service.data;

import lombok.Data;

@Data
public class SysParamOutData {
    private String clientId;
    private String stoCode;
    private String gsspId;
    private String gsspParam;
    private String gsspName;
    private String dcCode;
}
