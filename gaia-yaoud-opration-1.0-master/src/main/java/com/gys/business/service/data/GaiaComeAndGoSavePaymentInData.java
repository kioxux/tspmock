package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:19 2021/7/22
 * @Description：往来管理保存支付方式入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoSavePaymentInData {

    @ApiModelProperty(value = "用户")
    private String client;
    @ApiModelProperty(value = "往来管理支付方式编码")
    private String paymentId;
    @ApiModelProperty(value = "往来管理支付方式名称")
    private String paymentName;
    @ApiModelProperty(value = "创建人账号")
    private String paymentCreId;
    @ApiModelProperty(value = "创建日期")
    private String paymentCreDate;
    @ApiModelProperty(value = "创建时间")
    private String paymentCreTime;
}
