package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SupPriceRatioOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店编码")
    private String brId;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "供应商编码")
    private String supCode;
    @ApiModelProperty(value = "供应商名称")
    private String supName;
    @ApiModelProperty(value = "已订价格")
    private String proPrice;
    @ApiModelProperty(value = "起订金额")
    private String supMixAmt;
    @ApiModelProperty(value = "已订数量")
    private String proNum;
    @ApiModelProperty(value = "更新日期")
    private String supUpdateDate;
    @ApiModelProperty(value = "更新时间")
    private String supUpdateTime;
}
