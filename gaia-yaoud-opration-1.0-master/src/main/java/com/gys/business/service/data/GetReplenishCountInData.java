package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class GetReplenishCountInData {
    private String clientId;
    private List<GetReplenishInData> itemList;
    private List<String> proIds;
    private String dcCode;
}
