//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class GetGiftCardOutData {
    private String gsphVoucherId;
    private String promotionContent;
    private String promotionName;
    private String promotionTypeId;
    private String promotionType;
    private String proName;
    private String proId;
    private String serial;
    private String gsphBeginDate;
    private String gsphEndDate;
    private String gsphBeginTime;
    private String gsphEndTime;
    private String gsphDateFrequency;
    private String gsphWeekFrequency;
    private String sameOrDiff;
    private String gspcgActNo;
    private String totalNum;
    private List<GiftCardSet> giftCardSetList;
    private String gspscInteFlag;
    private String gspscInteRate;

    public GetGiftCardOutData() {
    }

    public String getGsphVoucherId() {
        return this.gsphVoucherId;
    }

    public String getPromotionContent() {
        return this.promotionContent;
    }

    public String getPromotionName() {
        return this.promotionName;
    }

    public String getPromotionTypeId() {
        return this.promotionTypeId;
    }

    public String getPromotionType() {
        return this.promotionType;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProId() {
        return this.proId;
    }

    public String getSerial() {
        return this.serial;
    }

    public String getGsphBeginDate() {
        return this.gsphBeginDate;
    }

    public String getGsphEndDate() {
        return this.gsphEndDate;
    }

    public String getGsphBeginTime() {
        return this.gsphBeginTime;
    }

    public String getGsphEndTime() {
        return this.gsphEndTime;
    }

    public String getGsphDateFrequency() {
        return this.gsphDateFrequency;
    }

    public String getGsphWeekFrequency() {
        return this.gsphWeekFrequency;
    }

    public String getSameOrDiff() {
        return this.sameOrDiff;
    }

    public String getGspcgActNo() {
        return this.gspcgActNo;
    }

    public String getTotalNum() {
        return this.totalNum;
    }

    public List<GiftCardSet> getGiftCardSetList() {
        return this.giftCardSetList;
    }

    public String getGspscInteFlag() {
        return this.gspscInteFlag;
    }

    public String getGspscInteRate() {
        return this.gspscInteRate;
    }

    public void setGsphVoucherId(final String gsphVoucherId) {
        this.gsphVoucherId = gsphVoucherId;
    }

    public void setPromotionContent(final String promotionContent) {
        this.promotionContent = promotionContent;
    }

    public void setPromotionName(final String promotionName) {
        this.promotionName = promotionName;
    }

    public void setPromotionTypeId(final String promotionTypeId) {
        this.promotionTypeId = promotionTypeId;
    }

    public void setPromotionType(final String promotionType) {
        this.promotionType = promotionType;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProId(final String proId) {
        this.proId = proId;
    }

    public void setSerial(final String serial) {
        this.serial = serial;
    }

    public void setGsphBeginDate(final String gsphBeginDate) {
        this.gsphBeginDate = gsphBeginDate;
    }

    public void setGsphEndDate(final String gsphEndDate) {
        this.gsphEndDate = gsphEndDate;
    }

    public void setGsphBeginTime(final String gsphBeginTime) {
        this.gsphBeginTime = gsphBeginTime;
    }

    public void setGsphEndTime(final String gsphEndTime) {
        this.gsphEndTime = gsphEndTime;
    }

    public void setGsphDateFrequency(final String gsphDateFrequency) {
        this.gsphDateFrequency = gsphDateFrequency;
    }

    public void setGsphWeekFrequency(final String gsphWeekFrequency) {
        this.gsphWeekFrequency = gsphWeekFrequency;
    }

    public void setSameOrDiff(final String sameOrDiff) {
        this.sameOrDiff = sameOrDiff;
    }

    public void setGspcgActNo(final String gspcgActNo) {
        this.gspcgActNo = gspcgActNo;
    }

    public void setTotalNum(final String totalNum) {
        this.totalNum = totalNum;
    }

    public void setGiftCardSetList(final List<GiftCardSet> giftCardSetList) {
        this.giftCardSetList = giftCardSetList;
    }

    public void setGspscInteFlag(final String gspscInteFlag) {
        this.gspscInteFlag = gspscInteFlag;
    }

    public void setGspscInteRate(final String gspscInteRate) {
        this.gspscInteRate = gspscInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetGiftCardOutData)) {
            return false;
        } else {
            GetGiftCardOutData other = (GetGiftCardOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label251: {
                    Object this$gsphVoucherId = this.getGsphVoucherId();
                    Object other$gsphVoucherId = other.getGsphVoucherId();
                    if (this$gsphVoucherId == null) {
                        if (other$gsphVoucherId == null) {
                            break label251;
                        }
                    } else if (this$gsphVoucherId.equals(other$gsphVoucherId)) {
                        break label251;
                    }

                    return false;
                }

                Object this$promotionContent = this.getPromotionContent();
                Object other$promotionContent = other.getPromotionContent();
                if (this$promotionContent == null) {
                    if (other$promotionContent != null) {
                        return false;
                    }
                } else if (!this$promotionContent.equals(other$promotionContent)) {
                    return false;
                }

                Object this$promotionName = this.getPromotionName();
                Object other$promotionName = other.getPromotionName();
                if (this$promotionName == null) {
                    if (other$promotionName != null) {
                        return false;
                    }
                } else if (!this$promotionName.equals(other$promotionName)) {
                    return false;
                }

                label230: {
                    Object this$promotionTypeId = this.getPromotionTypeId();
                    Object other$promotionTypeId = other.getPromotionTypeId();
                    if (this$promotionTypeId == null) {
                        if (other$promotionTypeId == null) {
                            break label230;
                        }
                    } else if (this$promotionTypeId.equals(other$promotionTypeId)) {
                        break label230;
                    }

                    return false;
                }

                label223: {
                    Object this$promotionType = this.getPromotionType();
                    Object other$promotionType = other.getPromotionType();
                    if (this$promotionType == null) {
                        if (other$promotionType == null) {
                            break label223;
                        }
                    } else if (this$promotionType.equals(other$promotionType)) {
                        break label223;
                    }

                    return false;
                }

                label216: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label216;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label216;
                    }

                    return false;
                }

                Object this$proId = this.getProId();
                Object other$proId = other.getProId();
                if (this$proId == null) {
                    if (other$proId != null) {
                        return false;
                    }
                } else if (!this$proId.equals(other$proId)) {
                    return false;
                }

                label202: {
                    Object this$serial = this.getSerial();
                    Object other$serial = other.getSerial();
                    if (this$serial == null) {
                        if (other$serial == null) {
                            break label202;
                        }
                    } else if (this$serial.equals(other$serial)) {
                        break label202;
                    }

                    return false;
                }

                Object this$gsphBeginDate = this.getGsphBeginDate();
                Object other$gsphBeginDate = other.getGsphBeginDate();
                if (this$gsphBeginDate == null) {
                    if (other$gsphBeginDate != null) {
                        return false;
                    }
                } else if (!this$gsphBeginDate.equals(other$gsphBeginDate)) {
                    return false;
                }

                label188: {
                    Object this$gsphEndDate = this.getGsphEndDate();
                    Object other$gsphEndDate = other.getGsphEndDate();
                    if (this$gsphEndDate == null) {
                        if (other$gsphEndDate == null) {
                            break label188;
                        }
                    } else if (this$gsphEndDate.equals(other$gsphEndDate)) {
                        break label188;
                    }

                    return false;
                }

                Object this$gsphBeginTime = this.getGsphBeginTime();
                Object other$gsphBeginTime = other.getGsphBeginTime();
                if (this$gsphBeginTime == null) {
                    if (other$gsphBeginTime != null) {
                        return false;
                    }
                } else if (!this$gsphBeginTime.equals(other$gsphBeginTime)) {
                    return false;
                }

                Object this$gsphEndTime = this.getGsphEndTime();
                Object other$gsphEndTime = other.getGsphEndTime();
                if (this$gsphEndTime == null) {
                    if (other$gsphEndTime != null) {
                        return false;
                    }
                } else if (!this$gsphEndTime.equals(other$gsphEndTime)) {
                    return false;
                }

                label167: {
                    Object this$gsphDateFrequency = this.getGsphDateFrequency();
                    Object other$gsphDateFrequency = other.getGsphDateFrequency();
                    if (this$gsphDateFrequency == null) {
                        if (other$gsphDateFrequency == null) {
                            break label167;
                        }
                    } else if (this$gsphDateFrequency.equals(other$gsphDateFrequency)) {
                        break label167;
                    }

                    return false;
                }

                label160: {
                    Object this$gsphWeekFrequency = this.getGsphWeekFrequency();
                    Object other$gsphWeekFrequency = other.getGsphWeekFrequency();
                    if (this$gsphWeekFrequency == null) {
                        if (other$gsphWeekFrequency == null) {
                            break label160;
                        }
                    } else if (this$gsphWeekFrequency.equals(other$gsphWeekFrequency)) {
                        break label160;
                    }

                    return false;
                }

                Object this$sameOrDiff = this.getSameOrDiff();
                Object other$sameOrDiff = other.getSameOrDiff();
                if (this$sameOrDiff == null) {
                    if (other$sameOrDiff != null) {
                        return false;
                    }
                } else if (!this$sameOrDiff.equals(other$sameOrDiff)) {
                    return false;
                }

                Object this$gspcgActNo = this.getGspcgActNo();
                Object other$gspcgActNo = other.getGspcgActNo();
                if (this$gspcgActNo == null) {
                    if (other$gspcgActNo != null) {
                        return false;
                    }
                } else if (!this$gspcgActNo.equals(other$gspcgActNo)) {
                    return false;
                }

                label139: {
                    Object this$totalNum = this.getTotalNum();
                    Object other$totalNum = other.getTotalNum();
                    if (this$totalNum == null) {
                        if (other$totalNum == null) {
                            break label139;
                        }
                    } else if (this$totalNum.equals(other$totalNum)) {
                        break label139;
                    }

                    return false;
                }

                Object this$giftCardSetList = this.getGiftCardSetList();
                Object other$giftCardSetList = other.getGiftCardSetList();
                if (this$giftCardSetList == null) {
                    if (other$giftCardSetList != null) {
                        return false;
                    }
                } else if (!this$giftCardSetList.equals(other$giftCardSetList)) {
                    return false;
                }

                Object this$gspscInteFlag = this.getGspscInteFlag();
                Object other$gspscInteFlag = other.getGspscInteFlag();
                if (this$gspscInteFlag == null) {
                    if (other$gspscInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspscInteFlag.equals(other$gspscInteFlag)) {
                    return false;
                }

                Object this$gspscInteRate = this.getGspscInteRate();
                Object other$gspscInteRate = other.getGspscInteRate();
                if (this$gspscInteRate == null) {
                    if (other$gspscInteRate != null) {
                        return false;
                    }
                } else if (!this$gspscInteRate.equals(other$gspscInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetGiftCardOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsphVoucherId = this.getGsphVoucherId();
        result = result * 59 + ($gsphVoucherId == null ? 43 : $gsphVoucherId.hashCode());
        Object $promotionContent = this.getPromotionContent();
        result = result * 59 + ($promotionContent == null ? 43 : $promotionContent.hashCode());
        Object $promotionName = this.getPromotionName();
        result = result * 59 + ($promotionName == null ? 43 : $promotionName.hashCode());
        Object $promotionTypeId = this.getPromotionTypeId();
        result = result * 59 + ($promotionTypeId == null ? 43 : $promotionTypeId.hashCode());
        Object $promotionType = this.getPromotionType();
        result = result * 59 + ($promotionType == null ? 43 : $promotionType.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proId = this.getProId();
        result = result * 59 + ($proId == null ? 43 : $proId.hashCode());
        Object $serial = this.getSerial();
        result = result * 59 + ($serial == null ? 43 : $serial.hashCode());
        Object $gsphBeginDate = this.getGsphBeginDate();
        result = result * 59 + ($gsphBeginDate == null ? 43 : $gsphBeginDate.hashCode());
        Object $gsphEndDate = this.getGsphEndDate();
        result = result * 59 + ($gsphEndDate == null ? 43 : $gsphEndDate.hashCode());
        Object $gsphBeginTime = this.getGsphBeginTime();
        result = result * 59 + ($gsphBeginTime == null ? 43 : $gsphBeginTime.hashCode());
        Object $gsphEndTime = this.getGsphEndTime();
        result = result * 59 + ($gsphEndTime == null ? 43 : $gsphEndTime.hashCode());
        Object $gsphDateFrequency = this.getGsphDateFrequency();
        result = result * 59 + ($gsphDateFrequency == null ? 43 : $gsphDateFrequency.hashCode());
        Object $gsphWeekFrequency = this.getGsphWeekFrequency();
        result = result * 59 + ($gsphWeekFrequency == null ? 43 : $gsphWeekFrequency.hashCode());
        Object $sameOrDiff = this.getSameOrDiff();
        result = result * 59 + ($sameOrDiff == null ? 43 : $sameOrDiff.hashCode());
        Object $gspcgActNo = this.getGspcgActNo();
        result = result * 59 + ($gspcgActNo == null ? 43 : $gspcgActNo.hashCode());
        Object $totalNum = this.getTotalNum();
        result = result * 59 + ($totalNum == null ? 43 : $totalNum.hashCode());
        Object $giftCardSetList = this.getGiftCardSetList();
        result = result * 59 + ($giftCardSetList == null ? 43 : $giftCardSetList.hashCode());
        Object $gspscInteFlag = this.getGspscInteFlag();
        result = result * 59 + ($gspscInteFlag == null ? 43 : $gspscInteFlag.hashCode());
        Object $gspscInteRate = this.getGspscInteRate();
        result = result * 59 + ($gspscInteRate == null ? 43 : $gspscInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "GetGiftCardOutData(gsphVoucherId=" + this.getGsphVoucherId() + ", promotionContent=" + this.getPromotionContent() + ", promotionName=" + this.getPromotionName() + ", promotionTypeId=" + this.getPromotionTypeId() + ", promotionType=" + this.getPromotionType() + ", proName=" + this.getProName() + ", proId=" + this.getProId() + ", serial=" + this.getSerial() + ", gsphBeginDate=" + this.getGsphBeginDate() + ", gsphEndDate=" + this.getGsphEndDate() + ", gsphBeginTime=" + this.getGsphBeginTime() + ", gsphEndTime=" + this.getGsphEndTime() + ", gsphDateFrequency=" + this.getGsphDateFrequency() + ", gsphWeekFrequency=" + this.getGsphWeekFrequency() + ", sameOrDiff=" + this.getSameOrDiff() + ", gspcgActNo=" + this.getGspcgActNo() + ", totalNum=" + this.getTotalNum() + ", giftCardSetList=" + this.getGiftCardSetList() + ", gspscInteFlag=" + this.getGspscInteFlag() + ", gspscInteRate=" + this.getGspscInteRate() + ")";
    }
}
