//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PromAssoCondsOutData {
    private String clientId;
    private String gspacVoucherId;
    private String gspacSerial;
    private String gspacProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspacQty;
    private String gspacSeriesId;
    private String gspacMemFlag;
    private String gspacInteFlag;
    private String gspacInteRate;

    public PromAssoCondsOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspacVoucherId() {
        return this.gspacVoucherId;
    }

    public String getGspacSerial() {
        return this.gspacSerial;
    }

    public String getGspacProId() {
        return this.gspacProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGspacQty() {
        return this.gspacQty;
    }

    public String getGspacSeriesId() {
        return this.gspacSeriesId;
    }

    public String getGspacMemFlag() {
        return this.gspacMemFlag;
    }

    public String getGspacInteFlag() {
        return this.gspacInteFlag;
    }

    public String getGspacInteRate() {
        return this.gspacInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspacVoucherId(final String gspacVoucherId) {
        this.gspacVoucherId = gspacVoucherId;
    }

    public void setGspacSerial(final String gspacSerial) {
        this.gspacSerial = gspacSerial;
    }

    public void setGspacProId(final String gspacProId) {
        this.gspacProId = gspacProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGspacQty(final String gspacQty) {
        this.gspacQty = gspacQty;
    }

    public void setGspacSeriesId(final String gspacSeriesId) {
        this.gspacSeriesId = gspacSeriesId;
    }

    public void setGspacMemFlag(final String gspacMemFlag) {
        this.gspacMemFlag = gspacMemFlag;
    }

    public void setGspacInteFlag(final String gspacInteFlag) {
        this.gspacInteFlag = gspacInteFlag;
    }

    public void setGspacInteRate(final String gspacInteRate) {
        this.gspacInteRate = gspacInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromAssoCondsOutData)) {
            return false;
        } else {
            PromAssoCondsOutData other = (PromAssoCondsOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label167;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gspacVoucherId = this.getGspacVoucherId();
                Object other$gspacVoucherId = other.getGspacVoucherId();
                if (this$gspacVoucherId == null) {
                    if (other$gspacVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspacVoucherId.equals(other$gspacVoucherId)) {
                    return false;
                }

                label153: {
                    Object this$gspacSerial = this.getGspacSerial();
                    Object other$gspacSerial = other.getGspacSerial();
                    if (this$gspacSerial == null) {
                        if (other$gspacSerial == null) {
                            break label153;
                        }
                    } else if (this$gspacSerial.equals(other$gspacSerial)) {
                        break label153;
                    }

                    return false;
                }

                Object this$gspacProId = this.getGspacProId();
                Object other$gspacProId = other.getGspacProId();
                if (this$gspacProId == null) {
                    if (other$gspacProId != null) {
                        return false;
                    }
                } else if (!this$gspacProId.equals(other$gspacProId)) {
                    return false;
                }

                label139: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label139;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label139;
                    }

                    return false;
                }

                Object this$gspgSpecs = this.getGspgSpecs();
                Object other$gspgSpecs = other.getGspgSpecs();
                if (this$gspgSpecs == null) {
                    if (other$gspgSpecs != null) {
                        return false;
                    }
                } else if (!this$gspgSpecs.equals(other$gspgSpecs)) {
                    return false;
                }

                label125: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label125;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label118;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label118;
                    }

                    return false;
                }

                Object this$gspacQty = this.getGspacQty();
                Object other$gspacQty = other.getGspacQty();
                if (this$gspacQty == null) {
                    if (other$gspacQty != null) {
                        return false;
                    }
                } else if (!this$gspacQty.equals(other$gspacQty)) {
                    return false;
                }

                label104: {
                    Object this$gspacSeriesId = this.getGspacSeriesId();
                    Object other$gspacSeriesId = other.getGspacSeriesId();
                    if (this$gspacSeriesId == null) {
                        if (other$gspacSeriesId == null) {
                            break label104;
                        }
                    } else if (this$gspacSeriesId.equals(other$gspacSeriesId)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$gspacMemFlag = this.getGspacMemFlag();
                    Object other$gspacMemFlag = other.getGspacMemFlag();
                    if (this$gspacMemFlag == null) {
                        if (other$gspacMemFlag == null) {
                            break label97;
                        }
                    } else if (this$gspacMemFlag.equals(other$gspacMemFlag)) {
                        break label97;
                    }

                    return false;
                }

                Object this$gspacInteFlag = this.getGspacInteFlag();
                Object other$gspacInteFlag = other.getGspacInteFlag();
                if (this$gspacInteFlag == null) {
                    if (other$gspacInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspacInteFlag.equals(other$gspacInteFlag)) {
                    return false;
                }

                Object this$gspacInteRate = this.getGspacInteRate();
                Object other$gspacInteRate = other.getGspacInteRate();
                if (this$gspacInteRate == null) {
                    if (other$gspacInteRate != null) {
                        return false;
                    }
                } else if (!this$gspacInteRate.equals(other$gspacInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromAssoCondsOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspacVoucherId = this.getGspacVoucherId();
        result = result * 59 + ($gspacVoucherId == null ? 43 : $gspacVoucherId.hashCode());
        Object $gspacSerial = this.getGspacSerial();
        result = result * 59 + ($gspacSerial == null ? 43 : $gspacSerial.hashCode());
        Object $gspacProId = this.getGspacProId();
        result = result * 59 + ($gspacProId == null ? 43 : $gspacProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gspacQty = this.getGspacQty();
        result = result * 59 + ($gspacQty == null ? 43 : $gspacQty.hashCode());
        Object $gspacSeriesId = this.getGspacSeriesId();
        result = result * 59 + ($gspacSeriesId == null ? 43 : $gspacSeriesId.hashCode());
        Object $gspacMemFlag = this.getGspacMemFlag();
        result = result * 59 + ($gspacMemFlag == null ? 43 : $gspacMemFlag.hashCode());
        Object $gspacInteFlag = this.getGspacInteFlag();
        result = result * 59 + ($gspacInteFlag == null ? 43 : $gspacInteFlag.hashCode());
        Object $gspacInteRate = this.getGspacInteRate();
        result = result * 59 + ($gspacInteRate == null ? 43 : $gspacInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromAssoCondsOutData(clientId=" + this.getClientId() + ", gspacVoucherId=" + this.getGspacVoucherId() + ", gspacSerial=" + this.getGspacSerial() + ", gspacProId=" + this.getGspacProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gspacQty=" + this.getGspacQty() + ", gspacSeriesId=" + this.getGspacSeriesId() + ", gspacMemFlag=" + this.getGspacMemFlag() + ", gspacInteFlag=" + this.getGspacInteFlag() + ", gspacInteRate=" + this.getGspacInteRate() + ")";
    }
}
