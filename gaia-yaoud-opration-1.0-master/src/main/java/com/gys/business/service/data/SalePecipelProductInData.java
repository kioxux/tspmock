package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc: 处方登记查询：商品明细
 * @author: ZhangChi
 * @createTime: 2021/12/24 10:49
 */
@Data
public class SalePecipelProductInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String gssrBrId;

    /**
     * 销售单号
     */
    private String gssrSaleBillNo;
}
