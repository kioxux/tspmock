package com.gys.business.service.data.mib;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.data.mib.annotation.MedPayType;
import lombok.Data;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author ：gyx
 * @Date ：Created in 17:33 2021/12/27
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class InsuranceSettleTableOutData {
    private String insuredUnit = "";                    //参保单位
    private String userType = "";                       //人员类别
    private String billNo = "";                         //结算单号
    private String medicalType = "";                    //医疗类别
    private String username = "";                       //姓名
    private String socialSecurityNum = "";              //社保号码
    @MedPayType("13")
    private String registrationFee = "0.00";            //挂号费
    @MedPayType("02")
    private String diagnosticFee = "0.00";              //诊察费
    @MedPayType("03")
    private String examinationFee = "0.00";             //检查费
    @MedPayType("04")
    private String assayFee = "0.00";                   //化验费
    @MedPayType("05")
    private String cureFee = "0.00";                    //治疗费
    @MedPayType("06")
    private String surgeryFee = "0.00";                 //手术费
    @MedPayType("08")
    private String materialsFee = "0.00";               //卫生材料费
    @MedPayType("09")
    private String westernMedicalFee = "0.00";          //西药费
    @MedPayType("10")
    private String chineseMedicinePieces = "0.00";      //中药饮片
    @MedPayType("11")
    private String chinesePatentMedicineFee = "0.00";   //中成药费
    @MedPayType("12")
    private String generalDiagnosisFee = "0.00";        //一般诊疗费
    @MedPayType("14")
    private String otherFee = "0.00";                   //其他费
    private String totalFee = "0.00";                   //费用合计（小写）
    private String totalFeeMax = "";                    //大写
    private String prepay = "0.00";                     //本次合计预缴金额
    private String startPrice = "0.00";                 //本次结算起付线
    private String basicFundPay = "0.00";               //基本基金支付
    private String accountPay = "0.00";                 //个人账户、门诊补偿余额支付
    private String seriousIllnessFundPay = "0.00";      //大病基金支付
    private String cashPay = "0.00";                    //个人现金支付
    private String civilServiceFundPay = "0.00";        //公务员补助基金支付
    private String selfPaying = "0.00";                 //其中自费医疗费用
    private String replenishFundPay = "0.00";           //补充基金支付
    private String stillOtherPay = "0.00";              //还应补收/退金额
    private String storeName = "";                      //定点医疗机构
    private String operationUser = "";                  //经办人
    private String operationTime = "";                  //经办时间
    private String remarkRemaining = "0.00";            //备注（余额）

    private List<FeeDetail> feeDetailList;
    private List<ProDetail> proDetailList;

    public static InsuranceSettleTableOutData disposalData(InsuranceSettleTableOutData data){
        //处理商品详情的数字问题
        List<ProDetail> proDetailList = data.getProDetailList();
        for (ProDetail proDetail : proDetailList) {
            proDetail.setPrice(new BigDecimal(proDetail.getPrice()==null?"0.00":proDetail.getPrice()).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
            proDetail.setNum(new BigDecimal(proDetail.getNum()==null?"0.00":proDetail.getNum()).setScale(0).toString());
            proDetail.setAmt(new BigDecimal(proDetail.getAmt()==null?"0.00":proDetail.getAmt()).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
            proDetail.setPriceAndNum(proDetail.getPrice() + "*" +proDetail.getNum());
        }

        //处理费用类型
        Field[] FieldList = data.getClass().getDeclaredFields();
        for (Field field : FieldList) {
            MedPayType annotation = field.getAnnotation(MedPayType.class);
            if (!ObjectUtils.isEmpty(annotation)){
                List<FeeDetail> feeDetailList = data.getFeeDetailList();
                for (FeeDetail feeDetail : feeDetailList) {
                    if (feeDetail.getItem().equals(annotation.value())) {
                        try {
                            field.set(data,new BigDecimal(feeDetail.getFee()==null?"0.00":feeDetail.getFee()).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        //处理数字保留两位小数
        InsuranceSettleTableOutData outData = new InsuranceSettleTableOutData();
        Field[] declaredFields = outData.getClass().getDeclaredFields();
        List<String> fields = Arrays.stream(declaredFields).filter(o -> {
            try {
                return (o.getType() == String.class && "0.00".equals(o.get(outData)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }).map(o -> o.getName()).collect(Collectors.toList());
        Map<String,Object> map = JSON.parseObject(JSON.toJSONString(data), Map.class);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (fields.contains(entry.getKey())){
                entry.setValue(new BigDecimal((String) (StringUtils.isEmpty(entry.getValue()) ?"0.00":entry.getValue())).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
            }
        }
        return JSON.parseObject(JSON.toJSONString(map),data.getClass());
    }
}
