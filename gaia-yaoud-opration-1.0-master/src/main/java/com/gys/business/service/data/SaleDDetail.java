package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaleDDetail implements Serializable {
    private static final long serialVersionUID = -2512773945470149082L;

    private String client;

    /**
     * 商品编码
     */
    private String proId;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 商品价格
     */
    private String proPrice;

    /**
     * 每行合计金额
     */
    private String proAmt;

    /**
     * 营业员
     */
    private String userName;

    /**
     * 数量
     */
    private String proNum;

    /**
     * 营业员id
     */
    private String empId;

    /**
     * 销售单号
     */
    private String billNo;

    /**
     * 规格
     */
    private String proSpece;

    /**
     * 序号
     */
    private String proIndex;

    /**
     * 销售日期
     */
    private String gssdDate;
}

