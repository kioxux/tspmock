package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.StockService;
import com.gys.business.service.SyncService;
import com.gys.business.service.data.*;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.MQReceiveData;
import com.gys.common.enums.StockBillTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;

import static com.gys.common.enums.StockBillTypeEnum.PIHAO_TIAOZHENG;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@SuppressWarnings("all")
@Service
public class SyncServiceImpl implements SyncService {
    private static final Logger log = LoggerFactory.getLogger(SyncServiceImpl.class);
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;
    @Autowired
    private GaiaSdProductPriceMapper sdProductPriceMapper;
    @Autowired
    private GaiaSdProuctLocationMapper sdProuctLocationMapper;
    @Autowired
    private GaiaSdStockMapper sdStockMapper;
    @Autowired
    private GaiaSdStockBatchMapper sdStockBatchMapper;
    @Autowired
    private GaiaSdStoreDataMapper sdStoreDataMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdEmpGroupMapper sdEmpGroupMapper;
    @Autowired
    private GaiaSdSaleHMapper sdSaleHMapper;
    @Autowired
    private GaiaSdSaleDMapper sdSaleDMapper;
    @Autowired
    private GaiaSdExamineHMapper sdExamineHMapper;
    @Autowired
    private GaiaSdBatchChangeMapper sdBatchChangeMapper;
    @Autowired
    private GaiaSdAcceptHMapper sdAcceptHMapper;
    @Autowired
    private GaiaWmsHudiaoZMapper wmsHudiaoZMapper;
    @Autowired
    private CacheService synchronizedService;
    @Autowired
    private RabbitTemplateHelper rabbitTemplate;
    @Autowired
    private BaseService baseService;
    @Autowired
    private GaiaTAccountMapper gaiaTAccountMapper;
    @Autowired
    private StockService stockService;
    @Autowired
    private TakeAwayService takeAwayService;

    @Override
    public GetSyncOutData selectProductInfo(GetLoginOutData userInfo) {
        GetSyncOutData outData = new GetSyncOutData();
        Example example = new Example(GaiaProductBusiness.class);
        example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("proSite", userInfo.getDepId());
        List<GaiaProductBusiness> businessList = this.productBusinessMapper.selectByExample(example);
        List<GetSyncProductBusinessOutData> productBusinessList = new ArrayList();
        Iterator var6 = businessList.iterator();

        while (var6.hasNext()) {
            GaiaProductBusiness business = (GaiaProductBusiness) var6.next();
            GetSyncProductBusinessOutData record = new GetSyncProductBusinessOutData();
            BeanUtil.copyProperties(business, record);
            productBusinessList.add(record);
        }

        outData.setProductBusinessList(productBusinessList);
        example = new Example(GaiaSdProductPrice.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsppBrId", userInfo.getDepId());
        List<GaiaSdProductPrice> priceList = this.sdProductPriceMapper.selectByExample(example);
        List<GetSyncSdProductPriceOutData> productPriceList = new ArrayList();
        Iterator var16 = priceList.iterator();

        while (var16.hasNext()) {
            GaiaSdProductPrice productPrice = (GaiaSdProductPrice) var16.next();
            GetSyncSdProductPriceOutData record = new GetSyncSdProductPriceOutData();
            BeanUtil.copyProperties(productPrice, record);
            productPriceList.add(record);
        }

        outData.setProductPriceList(productPriceList);
        example = new Example(GaiaSdProuctLocation.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsplBrId", userInfo.getDepId());
        List<GaiaSdProuctLocation> locationList = this.sdProuctLocationMapper.selectByExample(example);
        List<GetSyncSdProductLocationOutData> productLocationList = new ArrayList();
        Iterator var13 = locationList.iterator();

        while (var13.hasNext()) {
            GaiaSdProuctLocation prouctLocation = (GaiaSdProuctLocation) var13.next();
            GetSyncSdProductLocationOutData record = new GetSyncSdProductLocationOutData();
            BeanUtil.copyProperties(prouctLocation, record);
            productLocationList.add(record);
        }

        outData.setProductLocationList(productLocationList);
        return outData;
    }

    @Override
    public GetSyncOutData selectStoreInfo(GetLoginOutData userInfo) {
        GetSyncOutData outData = new GetSyncOutData();
        Example example = new Example(GaiaSdStock.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gssBrId", userInfo.getDepId());
        List<GaiaSdStock> stockList = this.sdStockMapper.selectByExample(example);
        List<GetSyncSdStockOutData> sdStockList = new ArrayList();
        Iterator var6 = stockList.iterator();

        while (var6.hasNext()) {
            GaiaSdStock stock = (GaiaSdStock) var6.next();
            GetSyncSdStockOutData record = new GetSyncSdStockOutData();
            BeanUtil.copyProperties(stock, record);
            sdStockList.add(record);
        }

        outData.setSdStockList(sdStockList);
        example = new Example(GaiaSdStockBatch.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gssbBrId", userInfo.getDepId());
        List<GaiaSdStockBatch> stockBatchList = this.sdStockBatchMapper.selectByExample(example);
        List<GetSyncSdStockBatchOutData> sdStockBatchList = new ArrayList();
        Iterator var17 = stockBatchList.iterator();

        while (var17.hasNext()) {
            GaiaSdStockBatch stockBatch = (GaiaSdStockBatch) var17.next();
            GetSyncSdStockBatchOutData record = new GetSyncSdStockBatchOutData();
            BeanUtil.copyProperties(stockBatch, record);
            sdStockBatchList.add(record);
        }

        outData.setSdStockBatchList(sdStockBatchList);
        example = new Example(GaiaSdStoreData.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsstBrId", userInfo.getDepId());
        List<GaiaSdStoreData> storeDataList = this.sdStoreDataMapper.selectByExample(example);
        List<GetSyncSdStoreOutData> sdStoreList = new ArrayList();
        Iterator var20 = storeDataList.iterator();

        while (var20.hasNext()) {
            GaiaSdStoreData storeData = (GaiaSdStoreData) var20.next();
            GetSyncSdStoreOutData record = new GetSyncSdStoreOutData();
            BeanUtil.copyProperties(storeData, record);
            sdStoreList.add(record);
        }

        outData.setSdStoreList(sdStoreList);
        example = new Example(GaiaSdEmpGroup.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsegBrId", userInfo.getDepId());
        List<GaiaSdEmpGroup> empGroupList = this.sdEmpGroupMapper.selectByExample(example);
        List<GetSyncSdEmpGroupOutData> sdEmpGroupList = new ArrayList();
        Iterator var23 = empGroupList.iterator();

        while (var23.hasNext()) {
            GaiaSdEmpGroup empGroup = (GaiaSdEmpGroup) var23.next();
            GetSyncSdEmpGroupOutData record = new GetSyncSdEmpGroupOutData();
            BeanUtil.copyProperties(empGroup, record);
            sdEmpGroupList.add(record);
        }

        outData.setSdEmpGroupList(sdEmpGroupList);
        return outData;
    }

    @Override
    public GetSyncOutData selectSaleInfo(GetLoginOutData userInfo) {
        GetSyncOutData outData = new GetSyncOutData();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(2, -1);
        cal.set(5, cal.getActualMinimum(5));
        String date = DateUtil.format(cal.getTime(), "yyyyMMdd");
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsshBrId", userInfo.getDepId()).andGreaterThanOrEqualTo("gsshDate", date);
        List<GaiaSdSaleH> saleHList = this.sdSaleHMapper.selectByExample(example);
        List<GetSyncSdSaleHOutData> sdSaleHList = new ArrayList();
        Iterator var8 = saleHList.iterator();

        while (var8.hasNext()) {
            GaiaSdSaleH sale = (GaiaSdSaleH) var8.next();
            GetSyncSdSaleHOutData record = new GetSyncSdSaleHOutData();
            BeanUtil.copyProperties(sale, record);
            sdSaleHList.add(record);
        }

        outData.setSdSaleHList(sdSaleHList);
        List<GaiaSdSaleD> saleDList = this.sdSaleDMapper.selectSyncList(userInfo.getClient(), userInfo.getDepId(), date);
        List<GetSyncSdSaleDOutData> sdSaleDList = new ArrayList();
        Iterator var13 = saleDList.iterator();

        while (var13.hasNext()) {
            GaiaSdSaleD sale = (GaiaSdSaleD) var13.next();
            GetSyncSdSaleDOutData record = new GetSyncSdSaleDOutData();
            BeanUtil.copyProperties(sale, record);
            sdSaleDList.add(record);
        }

        outData.setSdSaleDList(sdSaleDList);
        return outData;
    }

    @Override
    @Transactional
    public void uploadSaleInfo(GetSyncSdSaleHInData inData, GetLoginOutData userInfo) {
        GaiaSdSaleH saleRecord = new GaiaSdSaleH();
        saleRecord.setClientId(inData.getClientId());
        saleRecord.setGsshBillNo(inData.getGsshBillNo());
        GaiaSdSaleH sale = (GaiaSdSaleH) this.sdSaleHMapper.selectByPrimaryKey(saleRecord);
        if (ObjectUtil.isNotNull(sale)) {
            if ("0".equals(sale.getGsshHideFlag())) {
                log.info("上传的销售单已存在=" + JSONObject.toJSONString(inData));
                return;
            }

            Example example = new Example(GaiaSdSaleD.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBillNo", inData.getGsshBillNo());
            this.sdSaleDMapper.deleteByExample(example);
            this.sdSaleHMapper.delete(saleRecord);
        }

        GaiaSdSaleH record = new GaiaSdSaleH();
        BeanUtil.copyProperties(inData, record);
        if ("0".equals(inData.getGsshHideFlag())) {
        }

        this.sdSaleHMapper.insert(record);
        Iterator var6 = inData.getDetailList().iterator();

        while (var6.hasNext()) {
            GetSyncSdSaleDInData detail = (GetSyncSdSaleDInData) var6.next();
            GaiaSdSaleD recordDetail = new GaiaSdSaleD();
            BeanUtil.copyProperties(detail, recordDetail);
            this.sdSaleDMapper.insert(recordDetail);
        }

    }

    @Override
    @Transactional
    public void stockSync(StockSyncData inData) {
        log.info("库存更新：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getSite())) {
            throw new BusinessException("提示：门店号为空！");
        }
        if (CollectionUtil.isEmpty(inData.getParams())) {
            throw new BusinessException("提示：参数为空！");
        }
        if (StringUtils.isEmpty(inData.getType())) {
            throw new BusinessException("提示：类型为空！");
        }

        if (StockBillTypeEnum.YAN_SHOU.getCode().equals(inData.getType())
                || StockBillTypeEnum.SHOU_HUO.getCode().equals(inData.getType())
                || StockBillTypeEnum.SUN_YI.getCode().equals(inData.getType())
                || StockBillTypeEnum.TUI_KU_GY.getCode().equals(inData.getType())
                || StockBillTypeEnum.TUI_KU_WT.getCode().equals(inData.getType())
                || StockBillTypeEnum.PIHAO_YANGHU.getCode().equals(inData.getType())
                || StockBillTypeEnum.LING_YONG.getCode().equals(inData.getType())
                || StockBillTypeEnum.BAO_SUN.getCode().equals(inData.getType())
                ||StockBillTypeEnum.MEN_DIAN_BAO_YI.getCode().equals(inData.getType())
                || StockBillTypeEnum.BU_HE_GE.getCode().equals(inData.getType())) {
            List<GaiaSdBatchChange> batchChangeList;
            List<GaiaDataSynchronized> stockList = new ArrayList<>();
            List<GaiaDataSynchronized> batchList = new ArrayList<>();
            List<GaiaDataSynchronized> productBusinessList = new ArrayList<>();
            List<GaiaDataSynchronized> productPriceList = new ArrayList<>();
            Example example;
            for (String billNo : inData.getParams()) {
                example = new Example(GaiaSdBatchChange.class);
                example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("gsbcBrId", inData.getSite()).andEqualTo("gsbcVoucherId", billNo);
                batchChangeList = sdBatchChangeMapper.selectByExample(example);
                if (StrUtil.isNotEmpty(billNo) && (billNo.startsWith("JYS") || billNo.startsWith("JTD"))) {
                    log.info("配送代煎，同步库存接口，billNo:{}, 查询到的批次异动数据:{}", billNo, JSON.toJSONString(batchChangeList));
                }
                //插入同步表
                if (CollectionUtil.isNotEmpty(batchChangeList)) {
                    storeSync(batchChangeList);
                    for (GaiaSdBatchChange item : batchChangeList) {
                        //库存
                        Map<String, Object> stockMap = new HashMap<>(3);
                        stockMap.put("CLIENT", item.getClient());
                        stockMap.put("GSS_BR_ID", item.getGsbcBrId());
                        stockMap.put("GSS_PRO_ID", item.getGsbcProId());
                        GaiaDataSynchronized stock = new GaiaDataSynchronized();
                        stock.setClient(item.getClient());
                        stock.setSite(item.getGsbcBrId());
                        stock.setVersion(IdUtil.randomUUID());
                        stock.setTableName("GAIA_SD_STOCK");
                        stock.setCommand("update");
                        stock.setSourceNo(item.getGsbcVoucherId());
                        stock.setArg(JSON.toJSONString(stockMap));
                        if (!stockList.contains(stock)) {
                            stockList.add(stock);
                        }
                        //批次
                        Map<String, Object> batchMap = new HashMap<>(5);
                        batchMap.put("CLIENT", item.getClient());
                        batchMap.put("GSSB_BR_ID", item.getGsbcBrId());
                        batchMap.put("GSSB_PRO_ID", item.getGsbcProId());
                        batchMap.put("GSSB_BATCH_NO", item.getGsbcBatchNo());
                        batchMap.put("GSSB_BATCH", item.getGsbcBatch());
                        GaiaDataSynchronized batch = new GaiaDataSynchronized();
                        batch.setClient(item.getClient());
                        batch.setSite(item.getGsbcBrId());
                        batch.setVersion(IdUtil.randomUUID());
                        batch.setTableName("GAIA_SD_STOCK_BATCH");
                        batch.setCommand("update");
                        batch.setSourceNo(item.getGsbcVoucherId());
                        batch.setArg(JSON.toJSONString(batchMap));
                        if (!batchList.contains(batch)) {
                            batchList.add(batch);
                        }


//                        //主数据
//                        Map<String, Object> businessMap = new HashMap<>(3);
//                        businessMap.put("CLIENT", item.getClient());
//                        businessMap.put("PRO_SITE", item.getGsbcBrId());
//                        businessMap.put("PRO_SELF_CODE", item.getGsbcProId());
//                        GaiaDataSynchronized business = new GaiaDataSynchronized();
//                        business.setClient(item.getClient());
//                        business.setSite(item.getGsbcBrId());
//                        business.setVersion(IdUtil.randomUUID());
//                        business.setTableName("GAIA_PRODUCT_BUSINESS");
//                        business.setCommand("update");
//                        business.setSourceNo(item.getGsbcVoucherId());
//                        business.setArg(JSON.toJSONString(businessMap));
//                        if (!productBusinessList.contains(business)) {
//                            productBusinessList.add(business);
//                        }
//
//                        //价格表
//                        Map<String, Object> priceMap = new HashMap<>(3);
//                        priceMap.put("CLIENT", item.getClient());
//                        priceMap.put("GSPP_BR_ID", item.getGsbcBrId());
//                        priceMap.put("GSPP_PRO_ID", item.getGsbcProId());
//                        GaiaDataSynchronized price = new GaiaDataSynchronized();
//                        price.setClient(item.getClient());
//                        price.setSite(item.getGsbcBrId());
//                        price.setVersion(IdUtil.randomUUID());
//                        price.setTableName("GAIA_SD_PRODUCT_PRICE");
//                        price.setCommand("update");
//                        price.setSourceNo(item.getGsbcVoucherId());
//                        price.setArg(JSON.toJSONString(priceMap));
//                        if (!productPriceList.contains(price)) {
//                            productPriceList.add(price);
//                        }
                    }
                }
            }
            if (CollectionUtil.isNotEmpty(stockList)) {
                this.synchronizedService.insertLists(stockList);
            }
            if (CollectionUtil.isNotEmpty(batchList)) {
                this.synchronizedService.insertLists(batchList);
            }
//            if (CollUtil.isNotEmpty(productBusinessList)) {
//                this.synchronizedService.insertLists(productBusinessList);
//            }
//            if (CollUtil.isNotEmpty(productPriceList)) {
//                this.synchronizedService.insertLists(productPriceList);
//            }
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("stockSync");
            mqReceiveData.setData(inData.getParams());
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + inData.getSite(), JSON.toJSON(mqReceiveData));
            for (String billNo : inData.getParams()) {
                if (StrUtil.isNotEmpty(billNo) && (billNo.startsWith("JYS") || billNo.startsWith("JTD"))) {
                    log.info("配送代煎，同步库存接口，推送MQ，billNo:{}, client:{}, stoCode:{}, data:{}",
                            billNo, inData.getClient(), inData.getSite(), JSON.toJSONString(mqReceiveData));
                }
            }
        } else if (StockBillTypeEnum.TIAO_JI.getCode().equals(inData.getType())) { //调剂 调剂入库确认	warehousingConfirm
            log.debug("==========调剂==========");
            for (String billNo : inData.getParams()) {
                Example example = new Example(GaiaWmsHudiaoZ.class);
                example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("wmTjdh", billNo);
                GaiaWmsHudiaoZ hudiaoZ = wmsHudiaoZMapper.selectOneByExample(example);
                if (ObjectUtil.isNotEmpty(hudiaoZ)) {
                    //1.调入门店
                    example = new Example(GaiaSdBatchChange.class);
                    example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("gsbcBrId", hudiaoZ.getWmDrmd()).andEqualTo("gsbcVoucherId", billNo);
                    List<GaiaSdBatchChange> batchChangeList = sdBatchChangeMapper.selectByExample(example);
                    //插入同步表
                    if (CollectionUtil.isNotEmpty(batchChangeList)) {
                        storeSync(batchChangeList);
                        List<GaiaDataSynchronized> stockList = new ArrayList<>();
                        List<GaiaDataSynchronized> batchList = new ArrayList<>();
                        List<GaiaDataSynchronized> productBusinessList = new ArrayList<>();
                        List<GaiaDataSynchronized> productPriceList = new ArrayList<>();
                        for (GaiaSdBatchChange item : batchChangeList) {
                            //库存
                            Map<String, Object> stockMap = new HashMap<>(3);
                            stockMap.put("CLIENT", item.getClient());
                            stockMap.put("GSS_BR_ID", item.getGsbcBrId());
                            stockMap.put("GSS_PRO_ID", item.getGsbcProId());
                            GaiaDataSynchronized stock = new GaiaDataSynchronized();
                            stock.setClient(item.getClient());
                            stock.setSite(item.getGsbcBrId());
                            stock.setVersion(IdUtil.randomUUID());
                            stock.setTableName("GAIA_SD_STOCK");
                            stock.setCommand("update");
                            stock.setSourceNo(item.getGsbcVoucherId());
                            stock.setArg(JSON.toJSONString(stockMap));
                            if (!stockList.contains(stock)) {
                                stockList.add(stock);
                            }
                            //批次
                            Map<String, Object> batchMap = new HashMap<>(5);
                            batchMap.put("CLIENT", item.getClient());
                            batchMap.put("GSSB_BR_ID", item.getGsbcBrId());
                            batchMap.put("GSSB_PRO_ID", item.getGsbcProId());
                            batchMap.put("GSSB_BATCH_NO", item.getGsbcBatchNo());
                            batchMap.put("GSSB_BATCH", item.getGsbcBatch());
                            GaiaDataSynchronized batch = new GaiaDataSynchronized();
                            batch.setClient(item.getClient());
                            batch.setSite(item.getGsbcBrId());
                            batch.setVersion(IdUtil.randomUUID());
                            batch.setTableName("GAIA_SD_STOCK_BATCH");
                            batch.setCommand("update");
                            batch.setSourceNo(item.getGsbcVoucherId());
                            batch.setArg(JSON.toJSONString(batchMap));
                            if (!batchList.contains(batch)) {
                                batchList.add(batch);
                            }
                        }
                        if (CollectionUtil.isNotEmpty(stockList)) {
                            this.synchronizedService.insertLists(stockList);
                        }
                        if (CollectionUtil.isNotEmpty(batchList)) {
                            this.synchronizedService.insertLists(batchList);
                        }
                        MQReceiveData mqReceiveData = new MQReceiveData();
                        mqReceiveData.setType("DataSync");
                        mqReceiveData.setCmd("stockSync");
                        mqReceiveData.setData(billNo);
                        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + hudiaoZ.getWmDrmd(), JSON.toJSON(mqReceiveData));
                    }

                    //2.调出门店
                    example = new Example(GaiaSdBatchChange.class);
                    example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("gsbcBrId", hudiaoZ.getWmDqmd()).andEqualTo("gsbcVoucherId", billNo);
                    batchChangeList = sdBatchChangeMapper.selectByExample(example);
                    //插入同步表
                    if (CollectionUtil.isNotEmpty(batchChangeList)) {
                        List<GaiaDataSynchronized> stockList = new ArrayList<>();
                        List<GaiaDataSynchronized> batchList = new ArrayList<>();
                        List<GaiaDataSynchronized> productBusinessList = new ArrayList<>();
                        List<GaiaDataSynchronized> productPriceList = new ArrayList<>();
                        for (GaiaSdBatchChange item : batchChangeList) {
                            //库存
                            Map<String, Object> stockMap = new HashMap<>(3);
                            stockMap.put("CLIENT", item.getClient());
                            stockMap.put("GSS_BR_ID", item.getGsbcBrId());
                            stockMap.put("GSS_PRO_ID", item.getGsbcProId());
                            GaiaDataSynchronized stock = new GaiaDataSynchronized();
                            stock.setClient(item.getClient());
                            stock.setSite(item.getGsbcBrId());
                            stock.setVersion(IdUtil.randomUUID());
                            stock.setTableName("GAIA_SD_STOCK");
                            stock.setCommand("update");
                            stock.setSourceNo(item.getGsbcVoucherId());
                            stock.setArg(JSON.toJSONString(stockMap));
                            if (!stockList.contains(stock)) {
                                stockList.add(stock);
                            }
                            //批次
                            Map<String, Object> batchMap = new HashMap<>(5);
                            batchMap.put("CLIENT", item.getClient());
                            batchMap.put("GSSB_BR_ID", item.getGsbcBrId());
                            batchMap.put("GSSB_PRO_ID", item.getGsbcProId());
                            batchMap.put("GSSB_BATCH_NO", item.getGsbcBatchNo());
                            batchMap.put("GSSB_BATCH", item.getGsbcBatch());
                            GaiaDataSynchronized batch = new GaiaDataSynchronized();
                            batch.setClient(item.getClient());
                            batch.setSite(item.getGsbcBrId());
                            batch.setVersion(IdUtil.randomUUID());
                            batch.setTableName("GAIA_SD_STOCK_BATCH");
                            batch.setCommand("update");
                            batch.setSourceNo(item.getGsbcVoucherId());
                            batch.setArg(JSON.toJSONString(batchMap));
                            if (!batchList.contains(batch)) {
                                batchList.add(batch);
                            }
                        }
                        if (CollectionUtil.isNotEmpty(stockList)) {
                            this.synchronizedService.insertLists(stockList);
                        }
                        if (CollectionUtil.isNotEmpty(batchList)) {
                            this.synchronizedService.insertLists(batchList);
                        }
                        MQReceiveData mqReceiveData = new MQReceiveData();
                        mqReceiveData.setType("DataSync");
                        mqReceiveData.setCmd("stockSync");
                        mqReceiveData.setData(billNo);
                        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + hudiaoZ.getWmDqmd(), JSON.toJSON(mqReceiveData));
                    }
                }
            }
        } else if (PIHAO_TIAOZHENG.getCode().equals(inData.getType())) { //批号调整 insidemgt/approveAdjustList
            List<GaiaDataSynchronized> list = new ArrayList<>();
            for (String id : inData.getParams()) {
                String versionId = IdUtil.randomUUID();
                //插入同步表
                Map<String, Object> batchMap = new HashMap<>(5);
                batchMap.put("ID", id);
                GaiaDataSynchronized batch = new GaiaDataSynchronized();
                batch.setClient(inData.getClient());
                batch.setSite(inData.getSite());
                batch.setVersion(versionId);
                batch.setTableName("GAIA_SD_STOCK_BATCH");
                batch.setCommand("update");
                batch.setSourceNo(PIHAO_TIAOZHENG.getName());
                batch.setArg(JSON.toJSONString(batchMap));
                list.add(batch);
            }
            this.synchronizedService.insertLists(list);
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("stockSync");
            mqReceiveData.setData(inData.getParams());
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + inData.getSite(), JSON.toJSON(mqReceiveData));
        }
    }

    /**
     * 调用外卖-库存更新
     */
    private void storeSync(List<GaiaSdBatchChange> batchChangeList) {
        //stockSync()这个方法是在库存有变动的时候调用的
        //查询库存，调外卖平台更新库存接口
        try {
            List<GaiaSdStock> stockList = sdStockMapper.findByClientAndGssBrIdAndGssProId(batchChangeList.get(0).getClient(),
                    batchChangeList.get(0).getGsbcBrId(), batchChangeList);
            if (!CollectionUtils.isEmpty(stockList)) {
                stockService.syncStock(stockList);
//                List<Map<String, Object>> list = stockService.getPlatformParamMap(stockList);
//                if (list != null) {
//                    log.info("库存更新，调外卖平台库存更新接口======");
//                    for (Map<String, Object> paramMap : list) {
//                        String accessToken = null;
//                        if (Constants.PLATFORMS_MT.equalsIgnoreCase((String) paramMap.get("platform"))) {
//                            GaiaTAccount account = (GaiaTAccount) paramMap.get("account");
//                            accessToken = takeAwayService.getAccessToken(account);
//                        }
//                        String result = baseService.updateStockWithMap(paramMap, accessToken);
//                    }
//                }
            }
        } catch (Exception e) {
            log.error("调外卖平台更新库存接口报错，error:{}", e.getMessage());
        }
    }

    @Override
    @Transactional
    public void batchNoAdjustSync(batchNoAdjustSyncData inData) {
        log.info("批号调整同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getSite())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (CollectionUtil.isEmpty(inData.getIdList())) {
            throw new BusinessException("提示：参数为空！");
        }
        List<GaiaDataSynchronized> list = new ArrayList<>();
        //先按id 查询所有记录
        List<GaiaSdStockBatch> stockBatchList = sdStockBatchMapper.getAllByIds(inData.getClient(), inData.getIdList());
        for (GaiaSdStockBatch stockBatch : stockBatchList) {
            String versionId = IdUtil.randomUUID();
            //插入同步表
            Map<String, Object> batchMap = new HashMap<>(5);
            batchMap.put("ID", stockBatch.getId());
            GaiaDataSynchronized batch = new GaiaDataSynchronized();
            batch.setClient(inData.getClient());
            batch.setSite(stockBatch.getGssbBrId());
            batch.setVersion(versionId);
            batch.setTableName("GAIA_SD_STOCK_BATCH");
            batch.setCommand("update");
            batch.setSourceNo(PIHAO_TIAOZHENG.getName());
            batch.setArg(JSON.toJSONString(batchMap));
            list.add(batch);
        }
        this.synchronizedService.insertLists(list);

        List<GaiaSdStockBatch> distinctRes = stockBatchList.stream().collect(
                collectingAndThen(
                        toCollection(() -> new TreeSet<>(comparing(GaiaSdStockBatch::getGssbBrId))), ArrayList::new)
        );
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("batchNoAdjustSync");
        mqReceiveData.setData(inData.getIdList());
        for (GaiaSdStockBatch item : distinctRes) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + item.getGssbBrId(), JSON.toJSON(mqReceiveData));
        }
    }


    @Override
    @Transactional
    public void memberSync(MemberSyncData inData) {
        log.info("会员异动同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getMemberId())) {
            throw new BusinessException("提示：会员号为空！");
        }
        if (StringUtils.isEmpty(inData.getCardId())) {
            throw new BusinessException("提示：会员卡号为空！");
        }
        //查询此配送中心下的门店
        //查询当前加盟商下所有门店
        List<StoreOutData> storeDataList = this.storeDataMapper.getStoreList(inData.getClient(), null);
        List<GaiaDataSynchronized> list1 = new ArrayList<>();
        List<GaiaDataSynchronized> list2 = new ArrayList<>();
        for (StoreOutData item2 : storeDataList) {
            Map<String, Object> map2 = new HashMap<>(2);
            map2.put("CLIENT", inData.getClient());
            map2.put("GSMB_MEMBER_ID", inData.getMemberId());
            GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
            aSynchronized2.setClient(inData.getClient());
            aSynchronized2.setSite(item2.getStoCode());
            aSynchronized2.setVersion(IdUtil.randomUUID());
            aSynchronized2.setTableName("GAIA_SD_MEMBER_BASIC");
            aSynchronized2.setCommand("update");
            aSynchronized2.setArg(JSON.toJSONString(map2));
            list1.add(aSynchronized2);

            Map<String, Object> map = new HashMap<>(3);
            map.put("CLIENT", inData.getClient());
//        map.put("GSMBC_BR_ID", inData.getBrId());
            map.put("GSMBC_MEMBER_ID", inData.getMemberId());
            map.put("GSMBC_CARD_ID", inData.getCardId());
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(inData.getClient());
            aSynchronized.setSite(item2.getStoCode());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
            aSynchronized.setCommand("update");
            aSynchronized.setArg(JSON.toJSONString(map));
            list2.add(aSynchronized);

        }
        if (CollectionUtil.isNotEmpty(list1)) {
            this.synchronizedService.insertLists(list1);
        }
        if (CollectionUtil.isNotEmpty(list2)) {
            this.synchronizedService.insertLists(list2);
        }
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("memberSync");
        mqReceiveData.setData(inData.getCardId());
        for (StoreOutData item2 : storeDataList) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + item2.getStoCode(), JSON.toJSON(mqReceiveData));
        }
    }

    @Override
    @Transactional
    public void proPriceSync(ProPriceSyncData inData) {
        log.info("商品价格异动同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getSite())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (CollectionUtil.isEmpty(inData.getProIds())) {
            throw new BusinessException("提示：商品号列表为空！");
        }
        if (CollectionUtil.isNotEmpty(inData.getProIds())) {
            List<GaiaDataSynchronized> list = new ArrayList<>();
            List<StoreOutData> storeList = storeDataMapper.getStoreListByDcCodeOrBrId(inData.getClient(), inData.getSite());
            List<String> proIds = inData.getProIds().stream().distinct().collect(Collectors.toList());

            for (StoreOutData store : storeList) {
                for (String item : proIds) {
                    Map<String, Object> map = new HashMap<>(3);
                    map.put("CLIENT", inData.getClient());
                    map.put("GSPP_BR_ID", store.getStoCode());
                    map.put("GSPP_PRO_ID", item);
                    GaiaDataSynchronized dataSynchronized = new GaiaDataSynchronized();
                    dataSynchronized.setClient(inData.getClient());
                    dataSynchronized.setSite(store.getStoCode());
                    dataSynchronized.setVersion(IdUtil.randomUUID());
                    dataSynchronized.setTableName("GAIA_SD_PRODUCT_PRICE");
                    dataSynchronized.setCommand("update");
                    dataSynchronized.setArg(JSON.toJSONString(map));
                    list.add(dataSynchronized);
                }
            }

            this.synchronizedService.insertLists(list);
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("proPriceSync");
            mqReceiveData.setData(proIds);
            for (StoreOutData store : storeList) {
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + store.getStoCode(), JSON.toJSON(mqReceiveData));
            }
        }
    }

    @Override
    @Transactional
    public void mutilpleStoreProPriceSync(ProPriceSyncData2 inData) {
        log.info("批量商品价格异动同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (CollectionUtil.isEmpty(inData.getStoreList())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (CollectionUtil.isEmpty(inData.getProIds())) {
            throw new BusinessException("提示：商品号列表为空！");
        }
        List<GaiaDataSynchronized> list = new ArrayList<>();
        //查询地点是配送中心还是门店
        List<StoreOutData> storeList = storeDataMapper.getStoreListByDcCodesOrBrIds(inData.getClient(), inData.getStoreList());
        List<String> proIds = inData.getProIds().stream().distinct().collect(Collectors.toList());
        for (StoreOutData store : storeList) {
            for (String item2 : proIds) {
                Map<String, Object> map = new HashMap<>(3);
                map.put("CLIENT", inData.getClient());
                map.put("GSPP_BR_ID", store.getStoCode());
                map.put("GSPP_PRO_ID", item2);
                GaiaDataSynchronized dataSynchronized = new GaiaDataSynchronized();
                dataSynchronized.setClient(inData.getClient());
                dataSynchronized.setSite(store.getStoCode());
                dataSynchronized.setVersion(IdUtil.randomUUID());
                dataSynchronized.setTableName("GAIA_SD_PRODUCT_PRICE");
                dataSynchronized.setCommand("update");
                dataSynchronized.setArg(JSON.toJSONString(map));
                list.add(dataSynchronized);
            }
        }

        this.synchronizedService.insertLists(list);
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("multipleStoreProPriceSync");
        mqReceiveData.setData(proIds);
        for (StoreOutData item : storeList) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + item.getStoCode(), JSON.toJSON(mqReceiveData));
        }
    }

    @Override
    public void supplierSync(SupplierSyncData inData) {
        log.info("供应商异动同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getSite())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (StringUtils.isEmpty(inData.getAttribute())) {
            throw new BusinessException("提示：门店属性为空！");
        } else if (!"1".equals(inData.getAttribute()) && !"2".equals(inData.getAttribute())) {
            throw new BusinessException("提示：门店属性错误，1：单体，2：连锁 ！");
        }
        if (CollectionUtil.isEmpty(inData.getSupplierCodes())) {
            throw new BusinessException("提示：供应商编号列表为空！");
        }
        if (CollectionUtil.isNotEmpty(inData.getSupplierCodes())) {
            List<GaiaDataSynchronized> list = new ArrayList<>();
            for (String item : inData.getSupplierCodes()) {
                Map<String, Object> map = new HashMap<>(3);
                map.put("CLIENT", inData.getClient());
                map.put("SUP_SITE", inData.getSite());
                map.put("SUP_SELF_CODE", item);
                GaiaDataSynchronized dataSynchronized = new GaiaDataSynchronized();
                dataSynchronized.setClient(inData.getClient());
                dataSynchronized.setSite(inData.getSite());
                dataSynchronized.setVersion(IdUtil.randomUUID());
                dataSynchronized.setTableName("GAIA_SUPPLIER_BUSINESS");
                dataSynchronized.setCommand("update");
                dataSynchronized.setArg(JSON.toJSONString(map));
                list.add(dataSynchronized);
            }
            this.synchronizedService.insertLists(list);
            if ("2".equals(inData.getAttribute())) { //连锁
                //查询此配送中心下的门店
                List<StoreOutData> storeDataList = this.storeDataMapper.getStoreListByDcCode(inData.getClient(), inData.getSite());
                MQReceiveData mqReceiveData = new MQReceiveData();
                mqReceiveData.setType("DataSync");
                mqReceiveData.setCmd("supplierSync");
                mqReceiveData.setData(inData.getSupplierCodes());
                for (StoreOutData item2 : storeDataList) {
                    this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + item2.getStoCode(), JSON.toJSON(mqReceiveData));
                }
            } else { //单体
                MQReceiveData mqReceiveData = new MQReceiveData();
                mqReceiveData.setType("DataSync");
                mqReceiveData.setCmd("supplierSync");
                mqReceiveData.setData(inData.getSupplierCodes());
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + inData.getSite(), JSON.toJSON(mqReceiveData));
            }
        }
    }

    @Override
    public void batchControlSync(BatchControlSyncData inData) {
        log.info("批号控制同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getSite())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (CollectionUtil.isEmpty(inData.getItems())) {
            throw new BusinessException("提示：批号列表为空！");
        }
        if (CollectionUtil.isNotEmpty(inData.getItems())) {
            List<GaiaDataSynchronized> list = new ArrayList<>();
            //查询地点是配送中心还是门店
            List<StoreOutData> storeList = storeDataMapper.getStoreListByDcCodeOrBrId(inData.getClient(), inData.getSite());
            for (StoreOutData store : storeList) {
                for (BatchControlItemData item : inData.getItems()) {
                    Map<String, Object> map = new HashMap<>(4);
                    map.put("CLIENT", inData.getClient());
                    map.put("BAT_SITE_CODE", inData.getSite());
                    map.put("WM_SP_BM", item.getProId());
                    map.put("WM_PH", item.getBatchNo());
                    GaiaDataSynchronized dataSynchronized = new GaiaDataSynchronized();
                    dataSynchronized.setClient(inData.getClient());
                    dataSynchronized.setSite(inData.getSite());
                    dataSynchronized.setVersion(IdUtil.randomUUID());
                    dataSynchronized.setTableName("GAIA_WMS_PIHAOKONGZHI");
                    dataSynchronized.setCommand("update");
                    dataSynchronized.setArg(JSON.toJSONString(map));
                    list.add(dataSynchronized);
                }
            }
            this.synchronizedService.insertLists(list);
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("batchControlSync");
            mqReceiveData.setData(inData.getItems());
            for (StoreOutData store : storeList) {
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + store.getStoCode(), JSON.toJSON(mqReceiveData));
            }
        }
    }

    @Override
    public void productSync(ProductSyncData inData) {
        log.info("商品资料同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (StringUtils.isEmpty(inData.getSite())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (CollectionUtil.isEmpty(inData.getProIds())) {
            throw new BusinessException("提示：商品列表为空！");
        }
        if (CollectionUtil.isNotEmpty(inData.getProIds())) {
            List<GaiaDataSynchronized> list = new ArrayList<>();
            //查询地点是配送中心还是门店
            List<StoreOutData> storeList = storeDataMapper.getStoreListByDcCodeOrBrId(inData.getClient(), inData.getSite());
            List<String> proIds = inData.getProIds().stream().distinct().collect(Collectors.toList());
            for (StoreOutData store : storeList) {
                for (String item : proIds) {
                    Map<String, Object> map = new HashMap<>(3);
                    map.put("CLIENT", inData.getClient());
                    map.put("PRO_SITE", store.getStoCode());
                    map.put("PRO_SELF_CODE", item);
                    GaiaDataSynchronized dataSynchronized = new GaiaDataSynchronized();
                    dataSynchronized.setClient(inData.getClient());
                    dataSynchronized.setSite(inData.getSite());
                    dataSynchronized.setVersion(IdUtil.randomUUID());
                    dataSynchronized.setTableName("GAIA_PRODUCT_BUSINESS");
                    dataSynchronized.setCommand("insert");
                    dataSynchronized.setArg(JSON.toJSONString(map));
                    list.add(dataSynchronized);

                    GaiaDataSynchronized dataSynchronized2 = new GaiaDataSynchronized();
                    Map<String, Object> map2 = new HashMap<>(3);
                    map2.put("CLIENT", inData.getClient());
                    map2.put("GSPP_BR_ID", store.getStoCode());
                    map2.put("GSPP_PRO_ID", item);
                    dataSynchronized2.setClient(inData.getClient());
                    dataSynchronized2.setSite(item);
                    dataSynchronized2.setVersion(IdUtil.randomUUID());
                    dataSynchronized2.setTableName("GAIA_SD_PRODUCT_PRICE");
                    dataSynchronized2.setCommand("insert");
                    dataSynchronized2.setArg(JSON.toJSONString(map2));
                    list.add(dataSynchronized2);
                }
            }
            this.synchronizedService.insertLists(list);
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("productSync");
            mqReceiveData.setData(proIds);
            for (StoreOutData store : storeList) {
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + store.getStoCode(), JSON.toJSON(mqReceiveData));
            }
        }
    }

    @Override
    public void multipleStoreProductSync(ProductSyncData2 inData) {
        log.info("批量门店商品资料同步：" + inData);
        if (StringUtils.isEmpty(inData.getClient())) {
            throw new BusinessException("提示：供应商为空！");
        }
        if (CollectionUtil.isEmpty(inData.getStoreList())) {
            throw new BusinessException("提示：地点为空！");
        }
        if (CollectionUtil.isEmpty(inData.getProIds())) {
            throw new BusinessException("提示：商品列表为空！");
        }
        List<GaiaDataSynchronized> list = new ArrayList<>();
        List<StoreOutData> storeList = storeDataMapper.getStoreListByDcCodesOrBrIds(inData.getClient(), inData.getStoreList());
        for (StoreOutData store : storeList) {
            for (String item2 : inData.getProIds()) {
                Map<String, Object> map = new HashMap<>(3);
                map.put("CLIENT", inData.getClient());
                map.put("PRO_SITE", store.getStoCode());
                map.put("PRO_SELF_CODE", item2);
                GaiaDataSynchronized dataSynchronized = new GaiaDataSynchronized();
                dataSynchronized.setClient(inData.getClient());
                dataSynchronized.setSite(store.getStoCode());
                dataSynchronized.setVersion(IdUtil.randomUUID());
                dataSynchronized.setTableName("GAIA_PRODUCT_BUSINESS");
                dataSynchronized.setCommand("insert");
                dataSynchronized.setArg(JSON.toJSONString(map));
                list.add(dataSynchronized);

                GaiaDataSynchronized dataSynchronized2 = new GaiaDataSynchronized();
                Map<String, Object> map2 = new HashMap<>(3);
                map2.put("CLIENT", inData.getClient());
                map2.put("GSPP_BR_ID", store.getStoCode());
                map2.put("GSPP_PRO_ID", item2);
                dataSynchronized2.setClient(inData.getClient());
                dataSynchronized2.setSite(store.getStoCode());
                dataSynchronized2.setVersion(IdUtil.randomUUID());
                dataSynchronized2.setTableName("GAIA_SD_PRODUCT_PRICE");
                dataSynchronized2.setCommand("insert");
                dataSynchronized2.setArg(JSON.toJSONString(map2));
                list.add(dataSynchronized2);
            }
        }
        this.synchronizedService.insertLists(list);
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("multipleStoreProductSync");
        mqReceiveData.setData(inData.getProIds());
        for (StoreOutData item : storeList) {
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClient() + "." + item.getStoCode(), JSON.toJSON(mqReceiveData));
        }
    }
}
