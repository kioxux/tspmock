package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MessageTemplateChooseUserReq implements Serializable {
    private static final long serialVersionUID = -3635557990291312192L;

    private String client;

    private String site;

    private List<String> positionIds;

}
