package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromCouponSetInData implements Serializable {
    private static final long serialVersionUID = 8884864006208693778L;
    private String clientId;
    private String gspcsVoucherId;
    private String gspcsSerial;
    private String gspcsActNo;
    private String gspcsCouponType;
    private String gspcsCouponRemarks;
    private String gspcsBeginDate;
    private String gspcsEndDate;
    private String gspcsBeginTime;
    private String gspcsEndTime;
    private BigDecimal gspcsTotalPaycheckAmt;
    private BigDecimal gspcsSinglePaycheckAmt;
    private BigDecimal gspcsSingleUseAmt;
}
