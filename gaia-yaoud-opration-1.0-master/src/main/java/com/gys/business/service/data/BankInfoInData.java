//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class BankInfoInData {
    private String clientId;
    private String gsbsBrId;
    private String gsbsBankId;
    private String gsbsBankName;
    private String gsbsBankAccount;

    public BankInfoInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsbsBrId() {
        return this.gsbsBrId;
    }

    public String getGsbsBankId() {
        return this.gsbsBankId;
    }

    public String getGsbsBankName() {
        return this.gsbsBankName;
    }

    public String getGsbsBankAccount() {
        return this.gsbsBankAccount;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsbsBrId(final String gsbsBrId) {
        this.gsbsBrId = gsbsBrId;
    }

    public void setGsbsBankId(final String gsbsBankId) {
        this.gsbsBankId = gsbsBankId;
    }

    public void setGsbsBankName(final String gsbsBankName) {
        this.gsbsBankName = gsbsBankName;
    }

    public void setGsbsBankAccount(final String gsbsBankAccount) {
        this.gsbsBankAccount = gsbsBankAccount;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof BankInfoInData)) {
            return false;
        } else {
            BankInfoInData other = (BankInfoInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label71;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label71;
                    }

                    return false;
                }

                Object this$gsbsBrId = this.getGsbsBrId();
                Object other$gsbsBrId = other.getGsbsBrId();
                if (this$gsbsBrId == null) {
                    if (other$gsbsBrId != null) {
                        return false;
                    }
                } else if (!this$gsbsBrId.equals(other$gsbsBrId)) {
                    return false;
                }

                label57: {
                    Object this$gsbsBankId = this.getGsbsBankId();
                    Object other$gsbsBankId = other.getGsbsBankId();
                    if (this$gsbsBankId == null) {
                        if (other$gsbsBankId == null) {
                            break label57;
                        }
                    } else if (this$gsbsBankId.equals(other$gsbsBankId)) {
                        break label57;
                    }

                    return false;
                }

                Object this$gsbsBankName = this.getGsbsBankName();
                Object other$gsbsBankName = other.getGsbsBankName();
                if (this$gsbsBankName == null) {
                    if (other$gsbsBankName != null) {
                        return false;
                    }
                } else if (!this$gsbsBankName.equals(other$gsbsBankName)) {
                    return false;
                }

                Object this$gsbsBankAccount = this.getGsbsBankAccount();
                Object other$gsbsBankAccount = other.getGsbsBankAccount();
                if (this$gsbsBankAccount == null) {
                    if (other$gsbsBankAccount == null) {
                        return true;
                    }
                } else if (this$gsbsBankAccount.equals(other$gsbsBankAccount)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BankInfoInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsbsBrId = this.getGsbsBrId();
        result = result * 59 + ($gsbsBrId == null ? 43 : $gsbsBrId.hashCode());
        Object $gsbsBankId = this.getGsbsBankId();
        result = result * 59 + ($gsbsBankId == null ? 43 : $gsbsBankId.hashCode());
        Object $gsbsBankName = this.getGsbsBankName();
        result = result * 59 + ($gsbsBankName == null ? 43 : $gsbsBankName.hashCode());
        Object $gsbsBankAccount = this.getGsbsBankAccount();
        result = result * 59 + ($gsbsBankAccount == null ? 43 : $gsbsBankAccount.hashCode());
        return result;
    }

    public String toString() {
        return "BankInfoInData(clientId=" + this.getClientId() + ", gsbsBrId=" + this.getGsbsBrId() + ", gsbsBankId=" + this.getGsbsBankId() + ", gsbsBankName=" + this.getGsbsBankName() + ", gsbsBankAccount=" + this.getGsbsBankAccount() + ")";
    }
}
