//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PrescriptionInfoInData {
    private String clientId;
    private String commodityCode;
    private String pharmacistName;
    private String patientName;
    private String reviewerDate;
    private String batchNum;
    private String pharmacistNum;
    private String status;
    private String reviewerTime;
    private String auditConclusion;
    private String num;
    private String photoUrl;

    public PrescriptionInfoInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getCommodityCode() {
        return this.commodityCode;
    }

    public String getPharmacistName() {
        return this.pharmacistName;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public String getReviewerDate() {
        return this.reviewerDate;
    }

    public String getBatchNum() {
        return this.batchNum;
    }

    public String getPharmacistNum() {
        return this.pharmacistNum;
    }

    public String getStatus() {
        return this.status;
    }

    public String getReviewerTime() {
        return this.reviewerTime;
    }

    public String getAuditConclusion() {
        return this.auditConclusion;
    }

    public String getNum() {
        return this.num;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setCommodityCode(final String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public void setPharmacistName(final String pharmacistName) {
        this.pharmacistName = pharmacistName;
    }

    public void setPatientName(final String patientName) {
        this.patientName = patientName;
    }

    public void setReviewerDate(final String reviewerDate) {
        this.reviewerDate = reviewerDate;
    }

    public void setBatchNum(final String batchNum) {
        this.batchNum = batchNum;
    }

    public void setPharmacistNum(final String pharmacistNum) {
        this.pharmacistNum = pharmacistNum;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public void setReviewerTime(final String reviewerTime) {
        this.reviewerTime = reviewerTime;
    }

    public void setAuditConclusion(final String auditConclusion) {
        this.auditConclusion = auditConclusion;
    }

    public void setNum(final String num) {
        this.num = num;
    }

    public void setPhotoUrl(final String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PrescriptionInfoInData)) {
            return false;
        } else {
            PrescriptionInfoInData other = (PrescriptionInfoInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$commodityCode = this.getCommodityCode();
                Object other$commodityCode = other.getCommodityCode();
                if (this$commodityCode == null) {
                    if (other$commodityCode != null) {
                        return false;
                    }
                } else if (!this$commodityCode.equals(other$commodityCode)) {
                    return false;
                }

                Object this$pharmacistName = this.getPharmacistName();
                Object other$pharmacistName = other.getPharmacistName();
                if (this$pharmacistName == null) {
                    if (other$pharmacistName != null) {
                        return false;
                    }
                } else if (!this$pharmacistName.equals(other$pharmacistName)) {
                    return false;
                }

                label134: {
                    Object this$patientName = this.getPatientName();
                    Object other$patientName = other.getPatientName();
                    if (this$patientName == null) {
                        if (other$patientName == null) {
                            break label134;
                        }
                    } else if (this$patientName.equals(other$patientName)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$reviewerDate = this.getReviewerDate();
                    Object other$reviewerDate = other.getReviewerDate();
                    if (this$reviewerDate == null) {
                        if (other$reviewerDate == null) {
                            break label127;
                        }
                    } else if (this$reviewerDate.equals(other$reviewerDate)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$batchNum = this.getBatchNum();
                    Object other$batchNum = other.getBatchNum();
                    if (this$batchNum == null) {
                        if (other$batchNum == null) {
                            break label120;
                        }
                    } else if (this$batchNum.equals(other$batchNum)) {
                        break label120;
                    }

                    return false;
                }

                Object this$pharmacistNum = this.getPharmacistNum();
                Object other$pharmacistNum = other.getPharmacistNum();
                if (this$pharmacistNum == null) {
                    if (other$pharmacistNum != null) {
                        return false;
                    }
                } else if (!this$pharmacistNum.equals(other$pharmacistNum)) {
                    return false;
                }

                label106: {
                    Object this$status = this.getStatus();
                    Object other$status = other.getStatus();
                    if (this$status == null) {
                        if (other$status == null) {
                            break label106;
                        }
                    } else if (this$status.equals(other$status)) {
                        break label106;
                    }

                    return false;
                }

                Object this$reviewerTime = this.getReviewerTime();
                Object other$reviewerTime = other.getReviewerTime();
                if (this$reviewerTime == null) {
                    if (other$reviewerTime != null) {
                        return false;
                    }
                } else if (!this$reviewerTime.equals(other$reviewerTime)) {
                    return false;
                }

                label92: {
                    Object this$auditConclusion = this.getAuditConclusion();
                    Object other$auditConclusion = other.getAuditConclusion();
                    if (this$auditConclusion == null) {
                        if (other$auditConclusion == null) {
                            break label92;
                        }
                    } else if (this$auditConclusion.equals(other$auditConclusion)) {
                        break label92;
                    }

                    return false;
                }

                Object this$num = this.getNum();
                Object other$num = other.getNum();
                if (this$num == null) {
                    if (other$num != null) {
                        return false;
                    }
                } else if (!this$num.equals(other$num)) {
                    return false;
                }

                Object this$photoUrl = this.getPhotoUrl();
                Object other$photoUrl = other.getPhotoUrl();
                if (this$photoUrl == null) {
                    if (other$photoUrl != null) {
                        return false;
                    }
                } else if (!this$photoUrl.equals(other$photoUrl)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PrescriptionInfoInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $commodityCode = this.getCommodityCode();
        result = result * 59 + ($commodityCode == null ? 43 : $commodityCode.hashCode());
        Object $pharmacistName = this.getPharmacistName();
        result = result * 59 + ($pharmacistName == null ? 43 : $pharmacistName.hashCode());
        Object $patientName = this.getPatientName();
        result = result * 59 + ($patientName == null ? 43 : $patientName.hashCode());
        Object $reviewerDate = this.getReviewerDate();
        result = result * 59 + ($reviewerDate == null ? 43 : $reviewerDate.hashCode());
        Object $batchNum = this.getBatchNum();
        result = result * 59 + ($batchNum == null ? 43 : $batchNum.hashCode());
        Object $pharmacistNum = this.getPharmacistNum();
        result = result * 59 + ($pharmacistNum == null ? 43 : $pharmacistNum.hashCode());
        Object $status = this.getStatus();
        result = result * 59 + ($status == null ? 43 : $status.hashCode());
        Object $reviewerTime = this.getReviewerTime();
        result = result * 59 + ($reviewerTime == null ? 43 : $reviewerTime.hashCode());
        Object $auditConclusion = this.getAuditConclusion();
        result = result * 59 + ($auditConclusion == null ? 43 : $auditConclusion.hashCode());
        Object $num = this.getNum();
        result = result * 59 + ($num == null ? 43 : $num.hashCode());
        Object $photoUrl = this.getPhotoUrl();
        result = result * 59 + ($photoUrl == null ? 43 : $photoUrl.hashCode());
        return result;
    }

    public String toString() {
        return "PrescriptionInfoInData(clientId=" + this.getClientId() + ", commodityCode=" + this.getCommodityCode() + ", pharmacistName=" + this.getPharmacistName() + ", patientName=" + this.getPatientName() + ", reviewerDate=" + this.getReviewerDate() + ", batchNum=" + this.getBatchNum() + ", pharmacistNum=" + this.getPharmacistNum() + ", status=" + this.getStatus() + ", reviewerTime=" + this.getReviewerTime() + ", auditConclusion=" + this.getAuditConclusion() + ", num=" + this.getNum() + ", photoUrl=" + this.getPhotoUrl() + ")";
    }
}
