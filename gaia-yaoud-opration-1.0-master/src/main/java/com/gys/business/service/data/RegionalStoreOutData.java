package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaRegionalStore;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:59 2021/10/15
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class RegionalStoreOutData implements Serializable {
    /**
     * 区域ID
     */
    private Long id;

    /**
     * 区域编码
     */
    private String regionalCode;

    /**
     * 区域名称
     */
    private String regionalName;

    /**
     * 区域所属门店
     */
    private List<GaiaRegionalStore> stores;

    /**
     * 默认区域ID
     */
    private Long defaultId;

    /**
     * 默认区域编码
     */
    private String defaultRegionalCode;

    /**
     * 默认区域名称
     */
    private String defaultRegionalName;

    /**
     * 默认区域所属门店
     */
    private List<GaiaRegionalStore> defaultStores;
}
