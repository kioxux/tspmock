package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "批次商品信息")
public class GetStockBatchProInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String gssbBrId;
    @ApiModelProperty(value = "商品编码")
    private String gssbProId;
    @ApiModelProperty(value = "供应商")
    private String supplierId;
//    private String gssbBatchNo;
//    private String gssbBatch;
//    private String gssbQty;
//    private String gssbVaildDate;
//    private String gssbUpdateDate;
//    private String gssbUpdateEmp;
    @ApiModelProperty(value = "商品下拉框（预留字段）")
    private String proIdOrName;

}
