//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetSaleScheduleOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private String clientId;
    private Integer index;
    private String gsegBrId;
    private String gsegBrName;
    private String gsegId;
    private String gsegName;
    private String gsegNameId;
    private String gsegBeginTime;
    private String gsegEndTime;
    private String gsegEmp1;
    private String gsegEmpId1;
    private String gsegEmp2;
    private String gsegEmpId2;
    private String gsegEmp3;
    private String gsegEmpId3;
    private String gsegEmp4;
    private String gsegEmpId4;
    private String gsegEmp5;
    private String gsegEmpId5;
    private String gsegUpdateDate;
    private String gsegUpdateEmp;

    public GetSaleScheduleOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public Integer getIndex() {
        return this.index;
    }

    public String getGsegBrId() {
        return this.gsegBrId;
    }

    public String getGsegBrName() {
        return this.gsegBrName;
    }

    public String getGsegId() {
        return this.gsegId;
    }

    public String getGsegName() {
        return this.gsegName;
    }

    public String getGsegNameId() {
        return this.gsegNameId;
    }

    public String getGsegBeginTime() {
        return this.gsegBeginTime;
    }

    public String getGsegEndTime() {
        return this.gsegEndTime;
    }

    public String getGsegEmp1() {
        return this.gsegEmp1;
    }

    public String getGsegEmpId1() {
        return this.gsegEmpId1;
    }

    public String getGsegEmp2() {
        return this.gsegEmp2;
    }

    public String getGsegEmpId2() {
        return this.gsegEmpId2;
    }

    public String getGsegEmp3() {
        return this.gsegEmp3;
    }

    public String getGsegEmpId3() {
        return this.gsegEmpId3;
    }

    public String getGsegEmp4() {
        return this.gsegEmp4;
    }

    public String getGsegEmpId4() {
        return this.gsegEmpId4;
    }

    public String getGsegEmp5() {
        return this.gsegEmp5;
    }

    public String getGsegEmpId5() {
        return this.gsegEmpId5;
    }

    public String getGsegUpdateDate() {
        return this.gsegUpdateDate;
    }

    public String getGsegUpdateEmp() {
        return this.gsegUpdateEmp;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public void setGsegBrId(final String gsegBrId) {
        this.gsegBrId = gsegBrId;
    }

    public void setGsegBrName(final String gsegBrName) {
        this.gsegBrName = gsegBrName;
    }

    public void setGsegId(final String gsegId) {
        this.gsegId = gsegId;
    }

    public void setGsegName(final String gsegName) {
        this.gsegName = gsegName;
    }

    public void setGsegNameId(final String gsegNameId) {
        this.gsegNameId = gsegNameId;
    }

    public void setGsegBeginTime(final String gsegBeginTime) {
        this.gsegBeginTime = gsegBeginTime;
    }

    public void setGsegEndTime(final String gsegEndTime) {
        this.gsegEndTime = gsegEndTime;
    }

    public void setGsegEmp1(final String gsegEmp1) {
        this.gsegEmp1 = gsegEmp1;
    }

    public void setGsegEmpId1(final String gsegEmpId1) {
        this.gsegEmpId1 = gsegEmpId1;
    }

    public void setGsegEmp2(final String gsegEmp2) {
        this.gsegEmp2 = gsegEmp2;
    }

    public void setGsegEmpId2(final String gsegEmpId2) {
        this.gsegEmpId2 = gsegEmpId2;
    }

    public void setGsegEmp3(final String gsegEmp3) {
        this.gsegEmp3 = gsegEmp3;
    }

    public void setGsegEmpId3(final String gsegEmpId3) {
        this.gsegEmpId3 = gsegEmpId3;
    }

    public void setGsegEmp4(final String gsegEmp4) {
        this.gsegEmp4 = gsegEmp4;
    }

    public void setGsegEmpId4(final String gsegEmpId4) {
        this.gsegEmpId4 = gsegEmpId4;
    }

    public void setGsegEmp5(final String gsegEmp5) {
        this.gsegEmp5 = gsegEmp5;
    }

    public void setGsegEmpId5(final String gsegEmpId5) {
        this.gsegEmpId5 = gsegEmpId5;
    }

    public void setGsegUpdateDate(final String gsegUpdateDate) {
        this.gsegUpdateDate = gsegUpdateDate;
    }

    public void setGsegUpdateEmp(final String gsegUpdateEmp) {
        this.gsegUpdateEmp = gsegUpdateEmp;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSaleScheduleOutData)) {
            return false;
        } else {
            GetSaleScheduleOutData other = (GetSaleScheduleOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label263: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label263;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label263;
                    }

                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                label249: {
                    Object this$gsegBrId = this.getGsegBrId();
                    Object other$gsegBrId = other.getGsegBrId();
                    if (this$gsegBrId == null) {
                        if (other$gsegBrId == null) {
                            break label249;
                        }
                    } else if (this$gsegBrId.equals(other$gsegBrId)) {
                        break label249;
                    }

                    return false;
                }

                Object this$gsegBrName = this.getGsegBrName();
                Object other$gsegBrName = other.getGsegBrName();
                if (this$gsegBrName == null) {
                    if (other$gsegBrName != null) {
                        return false;
                    }
                } else if (!this$gsegBrName.equals(other$gsegBrName)) {
                    return false;
                }

                label235: {
                    Object this$gsegId = this.getGsegId();
                    Object other$gsegId = other.getGsegId();
                    if (this$gsegId == null) {
                        if (other$gsegId == null) {
                            break label235;
                        }
                    } else if (this$gsegId.equals(other$gsegId)) {
                        break label235;
                    }

                    return false;
                }

                Object this$gsegName = this.getGsegName();
                Object other$gsegName = other.getGsegName();
                if (this$gsegName == null) {
                    if (other$gsegName != null) {
                        return false;
                    }
                } else if (!this$gsegName.equals(other$gsegName)) {
                    return false;
                }

                label221: {
                    Object this$gsegNameId = this.getGsegNameId();
                    Object other$gsegNameId = other.getGsegNameId();
                    if (this$gsegNameId == null) {
                        if (other$gsegNameId == null) {
                            break label221;
                        }
                    } else if (this$gsegNameId.equals(other$gsegNameId)) {
                        break label221;
                    }

                    return false;
                }

                label214: {
                    Object this$gsegBeginTime = this.getGsegBeginTime();
                    Object other$gsegBeginTime = other.getGsegBeginTime();
                    if (this$gsegBeginTime == null) {
                        if (other$gsegBeginTime == null) {
                            break label214;
                        }
                    } else if (this$gsegBeginTime.equals(other$gsegBeginTime)) {
                        break label214;
                    }

                    return false;
                }

                Object this$gsegEndTime = this.getGsegEndTime();
                Object other$gsegEndTime = other.getGsegEndTime();
                if (this$gsegEndTime == null) {
                    if (other$gsegEndTime != null) {
                        return false;
                    }
                } else if (!this$gsegEndTime.equals(other$gsegEndTime)) {
                    return false;
                }

                label200: {
                    Object this$gsegEmp1 = this.getGsegEmp1();
                    Object other$gsegEmp1 = other.getGsegEmp1();
                    if (this$gsegEmp1 == null) {
                        if (other$gsegEmp1 == null) {
                            break label200;
                        }
                    } else if (this$gsegEmp1.equals(other$gsegEmp1)) {
                        break label200;
                    }

                    return false;
                }

                label193: {
                    Object this$gsegEmpId1 = this.getGsegEmpId1();
                    Object other$gsegEmpId1 = other.getGsegEmpId1();
                    if (this$gsegEmpId1 == null) {
                        if (other$gsegEmpId1 == null) {
                            break label193;
                        }
                    } else if (this$gsegEmpId1.equals(other$gsegEmpId1)) {
                        break label193;
                    }

                    return false;
                }

                Object this$gsegEmp2 = this.getGsegEmp2();
                Object other$gsegEmp2 = other.getGsegEmp2();
                if (this$gsegEmp2 == null) {
                    if (other$gsegEmp2 != null) {
                        return false;
                    }
                } else if (!this$gsegEmp2.equals(other$gsegEmp2)) {
                    return false;
                }

                Object this$gsegEmpId2 = this.getGsegEmpId2();
                Object other$gsegEmpId2 = other.getGsegEmpId2();
                if (this$gsegEmpId2 == null) {
                    if (other$gsegEmpId2 != null) {
                        return false;
                    }
                } else if (!this$gsegEmpId2.equals(other$gsegEmpId2)) {
                    return false;
                }

                label172: {
                    Object this$gsegEmp3 = this.getGsegEmp3();
                    Object other$gsegEmp3 = other.getGsegEmp3();
                    if (this$gsegEmp3 == null) {
                        if (other$gsegEmp3 == null) {
                            break label172;
                        }
                    } else if (this$gsegEmp3.equals(other$gsegEmp3)) {
                        break label172;
                    }

                    return false;
                }

                Object this$gsegEmpId3 = this.getGsegEmpId3();
                Object other$gsegEmpId3 = other.getGsegEmpId3();
                if (this$gsegEmpId3 == null) {
                    if (other$gsegEmpId3 != null) {
                        return false;
                    }
                } else if (!this$gsegEmpId3.equals(other$gsegEmpId3)) {
                    return false;
                }

                Object this$gsegEmp4 = this.getGsegEmp4();
                Object other$gsegEmp4 = other.getGsegEmp4();
                if (this$gsegEmp4 == null) {
                    if (other$gsegEmp4 != null) {
                        return false;
                    }
                } else if (!this$gsegEmp4.equals(other$gsegEmp4)) {
                    return false;
                }

                label151: {
                    Object this$gsegEmpId4 = this.getGsegEmpId4();
                    Object other$gsegEmpId4 = other.getGsegEmpId4();
                    if (this$gsegEmpId4 == null) {
                        if (other$gsegEmpId4 == null) {
                            break label151;
                        }
                    } else if (this$gsegEmpId4.equals(other$gsegEmpId4)) {
                        break label151;
                    }

                    return false;
                }

                Object this$gsegEmp5 = this.getGsegEmp5();
                Object other$gsegEmp5 = other.getGsegEmp5();
                if (this$gsegEmp5 == null) {
                    if (other$gsegEmp5 != null) {
                        return false;
                    }
                } else if (!this$gsegEmp5.equals(other$gsegEmp5)) {
                    return false;
                }

                label137: {
                    Object this$gsegEmpId5 = this.getGsegEmpId5();
                    Object other$gsegEmpId5 = other.getGsegEmpId5();
                    if (this$gsegEmpId5 == null) {
                        if (other$gsegEmpId5 == null) {
                            break label137;
                        }
                    } else if (this$gsegEmpId5.equals(other$gsegEmpId5)) {
                        break label137;
                    }

                    return false;
                }

                Object this$gsegUpdateDate = this.getGsegUpdateDate();
                Object other$gsegUpdateDate = other.getGsegUpdateDate();
                if (this$gsegUpdateDate == null) {
                    if (other$gsegUpdateDate != null) {
                        return false;
                    }
                } else if (!this$gsegUpdateDate.equals(other$gsegUpdateDate)) {
                    return false;
                }

                Object this$gsegUpdateEmp = this.getGsegUpdateEmp();
                Object other$gsegUpdateEmp = other.getGsegUpdateEmp();
                if (this$gsegUpdateEmp == null) {
                    if (other$gsegUpdateEmp == null) {
                        return true;
                    }
                } else if (this$gsegUpdateEmp.equals(other$gsegUpdateEmp)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSaleScheduleOutData;
    }

    public int hashCode() {
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $gsegBrId = this.getGsegBrId();
        result = result * 59 + ($gsegBrId == null ? 43 : $gsegBrId.hashCode());
        Object $gsegBrName = this.getGsegBrName();
        result = result * 59 + ($gsegBrName == null ? 43 : $gsegBrName.hashCode());
        Object $gsegId = this.getGsegId();
        result = result * 59 + ($gsegId == null ? 43 : $gsegId.hashCode());
        Object $gsegName = this.getGsegName();
        result = result * 59 + ($gsegName == null ? 43 : $gsegName.hashCode());
        Object $gsegNameId = this.getGsegNameId();
        result = result * 59 + ($gsegNameId == null ? 43 : $gsegNameId.hashCode());
        Object $gsegBeginTime = this.getGsegBeginTime();
        result = result * 59 + ($gsegBeginTime == null ? 43 : $gsegBeginTime.hashCode());
        Object $gsegEndTime = this.getGsegEndTime();
        result = result * 59 + ($gsegEndTime == null ? 43 : $gsegEndTime.hashCode());
        Object $gsegEmp1 = this.getGsegEmp1();
        result = result * 59 + ($gsegEmp1 == null ? 43 : $gsegEmp1.hashCode());
        Object $gsegEmpId1 = this.getGsegEmpId1();
        result = result * 59 + ($gsegEmpId1 == null ? 43 : $gsegEmpId1.hashCode());
        Object $gsegEmp2 = this.getGsegEmp2();
        result = result * 59 + ($gsegEmp2 == null ? 43 : $gsegEmp2.hashCode());
        Object $gsegEmpId2 = this.getGsegEmpId2();
        result = result * 59 + ($gsegEmpId2 == null ? 43 : $gsegEmpId2.hashCode());
        Object $gsegEmp3 = this.getGsegEmp3();
        result = result * 59 + ($gsegEmp3 == null ? 43 : $gsegEmp3.hashCode());
        Object $gsegEmpId3 = this.getGsegEmpId3();
        result = result * 59 + ($gsegEmpId3 == null ? 43 : $gsegEmpId3.hashCode());
        Object $gsegEmp4 = this.getGsegEmp4();
        result = result * 59 + ($gsegEmp4 == null ? 43 : $gsegEmp4.hashCode());
        Object $gsegEmpId4 = this.getGsegEmpId4();
        result = result * 59 + ($gsegEmpId4 == null ? 43 : $gsegEmpId4.hashCode());
        Object $gsegEmp5 = this.getGsegEmp5();
        result = result * 59 + ($gsegEmp5 == null ? 43 : $gsegEmp5.hashCode());
        Object $gsegEmpId5 = this.getGsegEmpId5();
        result = result * 59 + ($gsegEmpId5 == null ? 43 : $gsegEmpId5.hashCode());
        Object $gsegUpdateDate = this.getGsegUpdateDate();
        result = result * 59 + ($gsegUpdateDate == null ? 43 : $gsegUpdateDate.hashCode());
        Object $gsegUpdateEmp = this.getGsegUpdateEmp();
        result = result * 59 + ($gsegUpdateEmp == null ? 43 : $gsegUpdateEmp.hashCode());
        return result;
    }

    public String toString() {
        return "GetSaleScheduleOutData(clientId=" + this.getClientId() + ", index=" + this.getIndex() + ", gsegBrId=" + this.getGsegBrId() + ", gsegBrName=" + this.getGsegBrName() + ", gsegId=" + this.getGsegId() + ", gsegName=" + this.getGsegName() + ", gsegNameId=" + this.getGsegNameId() + ", gsegBeginTime=" + this.getGsegBeginTime() + ", gsegEndTime=" + this.getGsegEndTime() + ", gsegEmp1=" + this.getGsegEmp1() + ", gsegEmpId1=" + this.getGsegEmpId1() + ", gsegEmp2=" + this.getGsegEmp2() + ", gsegEmpId2=" + this.getGsegEmpId2() + ", gsegEmp3=" + this.getGsegEmp3() + ", gsegEmpId3=" + this.getGsegEmpId3() + ", gsegEmp4=" + this.getGsegEmp4() + ", gsegEmpId4=" + this.getGsegEmpId4() + ", gsegEmp5=" + this.getGsegEmp5() + ", gsegEmpId5=" + this.getGsegEmpId5() + ", gsegUpdateDate=" + this.getGsegUpdateDate() + ", gsegUpdateEmp=" + this.getGsegUpdateEmp() + ")";
    }
}
