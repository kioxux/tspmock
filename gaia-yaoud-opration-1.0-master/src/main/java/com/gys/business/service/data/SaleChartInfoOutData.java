package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SaleChartInfoOutData implements Serializable {

    @ApiModelProperty(value = "月或周")
    private String weekOrMonth;

    @ApiModelProperty(value = "动销品项数")
    private BigDecimal movCount;

    @ApiModelProperty(value = "销售额")
    private BigDecimal saleAmt;

    @ApiModelProperty(value = "毛利率")
    private BigDecimal profitRate;
}
