package com.gys.business.service;


import com.gys.common.data.JsonResult;

import java.util.List;
import java.util.Map;

public interface ApiService {
    Map<String,Object> selectStoreStockList(Map<String,Object> inData);
    JsonResult addWTPSD(Map<String,Object> inData);

    JsonResult addDSFZSStoreInfo(Map<String,Object> inData);
    Map<String,Object> billCommit(Map<String,Object> inData);
}
