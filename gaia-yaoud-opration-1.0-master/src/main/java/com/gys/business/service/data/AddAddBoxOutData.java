//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class AddAddBoxOutData {
    private String emp;
    private List<AddBoxDetailOutData> addBoxDetailOutDataList;
    private String clientId;
    private String brId;

    public AddAddBoxOutData() {
    }

    public String getEmp() {
        return this.emp;
    }

    public List<AddBoxDetailOutData> getAddBoxDetailOutDataList() {
        return this.addBoxDetailOutDataList;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public void setEmp(final String emp) {
        this.emp = emp;
    }

    public void setAddBoxDetailOutDataList(final List<AddBoxDetailOutData> addBoxDetailOutDataList) {
        this.addBoxDetailOutDataList = addBoxDetailOutDataList;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AddAddBoxOutData)) {
            return false;
        } else {
            AddAddBoxOutData other = (AddAddBoxOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$emp = this.getEmp();
                    Object other$emp = other.getEmp();
                    if (this$emp == null) {
                        if (other$emp == null) {
                            break label59;
                        }
                    } else if (this$emp.equals(other$emp)) {
                        break label59;
                    }

                    return false;
                }

                Object this$addBoxDetailOutDataList = this.getAddBoxDetailOutDataList();
                Object other$addBoxDetailOutDataList = other.getAddBoxDetailOutDataList();
                if (this$addBoxDetailOutDataList == null) {
                    if (other$addBoxDetailOutDataList != null) {
                        return false;
                    }
                } else if (!this$addBoxDetailOutDataList.equals(other$addBoxDetailOutDataList)) {
                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AddAddBoxOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $emp = this.getEmp();
        result = result * 59 + ($emp == null ? 43 : $emp.hashCode());
        Object $addBoxDetailOutDataList = this.getAddBoxDetailOutDataList();
        result = result * 59 + ($addBoxDetailOutDataList == null ? 43 : $addBoxDetailOutDataList.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        return result;
    }

    public String toString() {
        return "AddAddBoxOutData(emp=" + this.getEmp() + ", addBoxDetailOutDataList=" + this.getAddBoxDetailOutDataList() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ")";
    }
}
