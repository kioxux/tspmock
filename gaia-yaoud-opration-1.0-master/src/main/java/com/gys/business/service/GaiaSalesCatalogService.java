package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSalesCatalog;
import com.gys.business.service.data.GaiaSalesCatalogInData;
import com.gys.common.data.GetLoginOutData;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 销售目录维护表(GaiaSalesCatalog)表服务接口
 *
 * @author makejava
 * @since 2021-08-25 10:51:56
 */
public interface GaiaSalesCatalogService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaSalesCatalog queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaSalesCatalog> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 实例对象
     */
    GaiaSalesCatalog insert(GaiaSalesCatalog gaiaSalesCatalog);

    /**
     * 修改数据
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 实例对象
     */
    GaiaSalesCatalog update(GaiaSalesCatalog gaiaSalesCatalog);

    boolean insertInfo(GaiaSalesCatalog inData);

    List<GaiaSalesCatalog> list(GaiaSalesCatalogInData inData);

    boolean deleteBatch(GaiaSalesCatalogInData inData);

    boolean updateStatus(GaiaSalesCatalogInData inData);

    boolean enableFunction(GaiaSalesCatalogInData inData);

    Object getParam(GaiaSalesCatalogInData inData);

    Object batchImport(MultipartFile file, GetLoginOutData userInfo);

}
