package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaPriceGet;
import lombok.Data;

import java.util.List;

@Data
public class PriceGetInData {
    String client;
    String brId;
    List<GaiaPriceGet> list;
}
