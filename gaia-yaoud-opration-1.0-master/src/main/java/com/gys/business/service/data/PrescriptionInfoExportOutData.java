//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PrescriptionInfoExportOutData {
    private String num;
    private String reviewerDate;
    private String reviewerTime;
    private String pharmacistNum;
    private String pharmacistName;
    private String status;
    private String auditConclusion;
    private String uploadStore;
    private String billNo;
    private String saleDate;
    private String patientName;
    private String patientSex;
    private String patientAge;
    private String prescriptionAddress;

    public PrescriptionInfoExportOutData() {
    }

    public String getNum() {
        return this.num;
    }

    public String getReviewerDate() {
        return this.reviewerDate;
    }

    public String getReviewerTime() {
        return this.reviewerTime;
    }

    public String getPharmacistNum() {
        return this.pharmacistNum;
    }

    public String getPharmacistName() {
        return this.pharmacistName;
    }

    public String getStatus() {
        return this.status;
    }

    public String getAuditConclusion() {
        return this.auditConclusion;
    }

    public String getUploadStore() {
        return this.uploadStore;
    }

    public String getBillNo() {
        return this.billNo;
    }

    public String getSaleDate() {
        return this.saleDate;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public String getPatientSex() {
        return this.patientSex;
    }

    public String getPatientAge() {
        return this.patientAge;
    }

    public String getPrescriptionAddress() {
        return this.prescriptionAddress;
    }

    public void setNum(final String num) {
        this.num = num;
    }

    public void setReviewerDate(final String reviewerDate) {
        this.reviewerDate = reviewerDate;
    }

    public void setReviewerTime(final String reviewerTime) {
        this.reviewerTime = reviewerTime;
    }

    public void setPharmacistNum(final String pharmacistNum) {
        this.pharmacistNum = pharmacistNum;
    }

    public void setPharmacistName(final String pharmacistName) {
        this.pharmacistName = pharmacistName;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public void setAuditConclusion(final String auditConclusion) {
        this.auditConclusion = auditConclusion;
    }

    public void setUploadStore(final String uploadStore) {
        this.uploadStore = uploadStore;
    }

    public void setBillNo(final String billNo) {
        this.billNo = billNo;
    }

    public void setSaleDate(final String saleDate) {
        this.saleDate = saleDate;
    }

    public void setPatientName(final String patientName) {
        this.patientName = patientName;
    }

    public void setPatientSex(final String patientSex) {
        this.patientSex = patientSex;
    }

    public void setPatientAge(final String patientAge) {
        this.patientAge = patientAge;
    }

    public void setPrescriptionAddress(final String prescriptionAddress) {
        this.prescriptionAddress = prescriptionAddress;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PrescriptionInfoExportOutData)) {
            return false;
        } else {
            PrescriptionInfoExportOutData other = (PrescriptionInfoExportOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$num = this.getNum();
                Object other$num = other.getNum();
                if (this$num == null) {
                    if (other$num != null) {
                        return false;
                    }
                } else if (!this$num.equals(other$num)) {
                    return false;
                }

                Object this$reviewerDate = this.getReviewerDate();
                Object other$reviewerDate = other.getReviewerDate();
                if (this$reviewerDate == null) {
                    if (other$reviewerDate != null) {
                        return false;
                    }
                } else if (!this$reviewerDate.equals(other$reviewerDate)) {
                    return false;
                }

                Object this$reviewerTime = this.getReviewerTime();
                Object other$reviewerTime = other.getReviewerTime();
                if (this$reviewerTime == null) {
                    if (other$reviewerTime != null) {
                        return false;
                    }
                } else if (!this$reviewerTime.equals(other$reviewerTime)) {
                    return false;
                }

                label158: {
                    Object this$pharmacistNum = this.getPharmacistNum();
                    Object other$pharmacistNum = other.getPharmacistNum();
                    if (this$pharmacistNum == null) {
                        if (other$pharmacistNum == null) {
                            break label158;
                        }
                    } else if (this$pharmacistNum.equals(other$pharmacistNum)) {
                        break label158;
                    }

                    return false;
                }

                label151: {
                    Object this$pharmacistName = this.getPharmacistName();
                    Object other$pharmacistName = other.getPharmacistName();
                    if (this$pharmacistName == null) {
                        if (other$pharmacistName == null) {
                            break label151;
                        }
                    } else if (this$pharmacistName.equals(other$pharmacistName)) {
                        break label151;
                    }

                    return false;
                }

                Object this$status = this.getStatus();
                Object other$status = other.getStatus();
                if (this$status == null) {
                    if (other$status != null) {
                        return false;
                    }
                } else if (!this$status.equals(other$status)) {
                    return false;
                }

                label137: {
                    Object this$auditConclusion = this.getAuditConclusion();
                    Object other$auditConclusion = other.getAuditConclusion();
                    if (this$auditConclusion == null) {
                        if (other$auditConclusion == null) {
                            break label137;
                        }
                    } else if (this$auditConclusion.equals(other$auditConclusion)) {
                        break label137;
                    }

                    return false;
                }

                label130: {
                    Object this$uploadStore = this.getUploadStore();
                    Object other$uploadStore = other.getUploadStore();
                    if (this$uploadStore == null) {
                        if (other$uploadStore == null) {
                            break label130;
                        }
                    } else if (this$uploadStore.equals(other$uploadStore)) {
                        break label130;
                    }

                    return false;
                }

                Object this$billNo = this.getBillNo();
                Object other$billNo = other.getBillNo();
                if (this$billNo == null) {
                    if (other$billNo != null) {
                        return false;
                    }
                } else if (!this$billNo.equals(other$billNo)) {
                    return false;
                }

                Object this$saleDate = this.getSaleDate();
                Object other$saleDate = other.getSaleDate();
                if (this$saleDate == null) {
                    if (other$saleDate != null) {
                        return false;
                    }
                } else if (!this$saleDate.equals(other$saleDate)) {
                    return false;
                }

                label109: {
                    Object this$patientName = this.getPatientName();
                    Object other$patientName = other.getPatientName();
                    if (this$patientName == null) {
                        if (other$patientName == null) {
                            break label109;
                        }
                    } else if (this$patientName.equals(other$patientName)) {
                        break label109;
                    }

                    return false;
                }

                label102: {
                    Object this$patientSex = this.getPatientSex();
                    Object other$patientSex = other.getPatientSex();
                    if (this$patientSex == null) {
                        if (other$patientSex == null) {
                            break label102;
                        }
                    } else if (this$patientSex.equals(other$patientSex)) {
                        break label102;
                    }

                    return false;
                }

                Object this$patientAge = this.getPatientAge();
                Object other$patientAge = other.getPatientAge();
                if (this$patientAge == null) {
                    if (other$patientAge != null) {
                        return false;
                    }
                } else if (!this$patientAge.equals(other$patientAge)) {
                    return false;
                }

                Object this$prescriptionAddress = this.getPrescriptionAddress();
                Object other$prescriptionAddress = other.getPrescriptionAddress();
                if (this$prescriptionAddress == null) {
                    if (other$prescriptionAddress != null) {
                        return false;
                    }
                } else if (!this$prescriptionAddress.equals(other$prescriptionAddress)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PrescriptionInfoExportOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $num = this.getNum();
        result = result * 59 + ($num == null ? 43 : $num.hashCode());
        Object $reviewerDate = this.getReviewerDate();
        result = result * 59 + ($reviewerDate == null ? 43 : $reviewerDate.hashCode());
        Object $reviewerTime = this.getReviewerTime();
        result = result * 59 + ($reviewerTime == null ? 43 : $reviewerTime.hashCode());
        Object $pharmacistNum = this.getPharmacistNum();
        result = result * 59 + ($pharmacistNum == null ? 43 : $pharmacistNum.hashCode());
        Object $pharmacistName = this.getPharmacistName();
        result = result * 59 + ($pharmacistName == null ? 43 : $pharmacistName.hashCode());
        Object $status = this.getStatus();
        result = result * 59 + ($status == null ? 43 : $status.hashCode());
        Object $auditConclusion = this.getAuditConclusion();
        result = result * 59 + ($auditConclusion == null ? 43 : $auditConclusion.hashCode());
        Object $uploadStore = this.getUploadStore();
        result = result * 59 + ($uploadStore == null ? 43 : $uploadStore.hashCode());
        Object $billNo = this.getBillNo();
        result = result * 59 + ($billNo == null ? 43 : $billNo.hashCode());
        Object $saleDate = this.getSaleDate();
        result = result * 59 + ($saleDate == null ? 43 : $saleDate.hashCode());
        Object $patientName = this.getPatientName();
        result = result * 59 + ($patientName == null ? 43 : $patientName.hashCode());
        Object $patientSex = this.getPatientSex();
        result = result * 59 + ($patientSex == null ? 43 : $patientSex.hashCode());
        Object $patientAge = this.getPatientAge();
        result = result * 59 + ($patientAge == null ? 43 : $patientAge.hashCode());
        Object $prescriptionAddress = this.getPrescriptionAddress();
        result = result * 59 + ($prescriptionAddress == null ? 43 : $prescriptionAddress.hashCode());
        return result;
    }

    public String toString() {
        return "PrescriptionInfoExportOutData(num=" + this.getNum() + ", reviewerDate=" + this.getReviewerDate() + ", reviewerTime=" + this.getReviewerTime() + ", pharmacistNum=" + this.getPharmacistNum() + ", pharmacistName=" + this.getPharmacistName() + ", status=" + this.getStatus() + ", auditConclusion=" + this.getAuditConclusion() + ", uploadStore=" + this.getUploadStore() + ", billNo=" + this.getBillNo() + ", saleDate=" + this.getSaleDate() + ", patientName=" + this.getPatientName() + ", patientSex=" + this.getPatientSex() + ", patientAge=" + this.getPatientAge() + ", prescriptionAddress=" + this.getPrescriptionAddress() + ")";
    }
}
