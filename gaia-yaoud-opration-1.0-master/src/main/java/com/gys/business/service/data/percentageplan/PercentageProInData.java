package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PercentageProInData {
    @ApiModelProperty(value = "主键ID")
    private Long id;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "提成方案主表主键ID")
    private Integer pid;
    @ApiModelProperty(value = "提成方案主表主键ID")
    private Integer settingId;
    @ApiModelProperty(value = "单品提成方式 0 不参与销售提成 1 参与销售提成")
    private String planProductWay;
    @ApiModelProperty(value = "提成方式 1 按零售额提成 2 按销售额提成 3按毛利率提成")
    private String planPercentageWay;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "成本价")
    private BigDecimal proCostPrice;
    @ApiModelProperty(value = "零售价")
    private BigDecimal proPrice;
    @ApiModelProperty(value = "提成级别 1 一级 2 二级 3 三级")
    private String tichengLevel;
    @ApiModelProperty(value = "达成数量1")
    private String saleQty;
    @ApiModelProperty(value = "提成金额1")
    private String tichengAmt;
    @ApiModelProperty(value = "提成比例1")
    private String tichengRate;
    @ApiModelProperty(value = "达成数量2")
    private String saleQty2;
    @ApiModelProperty(value = "提成金额2")
    private String tichengAmt2;
    @ApiModelProperty(value = "提成比例2")
    private String tichengRate2;
    @ApiModelProperty(value = "达成数量3")
    private String saleQty3;
    @ApiModelProperty(value = "提成金额3")
    private String tichengAmt3;
    @ApiModelProperty(value = "提成比例3")
    private String tichengRate3;
    @ApiModelProperty(value = "操作状态 0 未删除 1 已删除")
    private String deleteFlag;
    @ApiModelProperty(value = "毛利额")
    private BigDecimal grossProfitAmt;
    @ApiModelProperty(value = "毛利率")
    private String grossProfitRate;



    @ApiModelProperty(value = "剔除折扣率 操作符号")
    private String planRejectDiscountRateSymbol;

    @ApiModelProperty(value = "剔除折扣率 百分比值")
    private String planRejectDiscountRate;

    @ApiModelProperty(value = "负毛利率商品是否不参与销售提成 0 是 1 否")
    private String planIfNegative;

}
