package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class PurchaseLimitVo implements Serializable {
    private static final long serialVersionUID = 1451566369976133268L;

    /**
     * 列表的 商品id  和 购买数量
     */
    private Map<String, String> map;

    /**
     * 会员卡号
     */
    private String cardNum;
}
