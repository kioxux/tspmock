package com.gys.business.service.data.disease;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectCardTagResponse {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    private String memberCard;

    @ApiModelProperty(value = "标签编号")
    private String tagId;

    @ApiModelProperty(value = "消费金额")
    private String amt;

    @ApiModelProperty(value = "消费次数")
    private String saleTimes;

    @ApiModelProperty(value = "类型0成分标签1疾病标签")
    private String type;

    @ApiModelProperty(value = "计算月份")
    private String calcMonth;

    @ApiModelProperty(value = "更新日期")
    private String updateDate;
}
