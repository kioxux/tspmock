package com.gys.business.service.yaoshibang.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.gys.business.mapper.GAIAYaoshibangStoreMapper;
import com.gys.business.mapper.GaiaDcDataMapper;
import com.gys.business.mapper.GaiaProductBusinessMapper;
import com.gys.business.mapper.GaiaWmKuCunMapper;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.GaiaSoBatchAccountService;
import com.gys.business.service.GaiaSoBatchDService;
import com.gys.business.service.GaiaSoBatchHService;
import com.gys.business.service.data.GetWfAuditInData;
import com.gys.business.service.data.yaoshibang.*;
import com.gys.business.service.data.yaoshibang.GaiaSoHeader;
import com.gys.business.service.yaoshibang.YaoshibangService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.YaoshibangInvoiceTypeEnum;
import com.gys.common.enums.YaoshibangOrderStatusEnum;
import com.gys.common.exception.BusinessException;
import com.gys.feign.AuthService;
import com.gys.feign.PurchaseService;
import com.gys.util.BigDecimalUtil;
import com.gys.util.ysb.YsbApiUtil;
import com.gys.util.ysb.YsbDrugstoreUpdateBean;
import com.gys.util.ysb.YsbGoodlInfo;
import com.gys.util.ysb.YsbOrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhangdong
 * @date 2021/6/7 11:02
 */
@Service
@Slf4j
public class YaoshibangServiceImpl implements YaoshibangService {

    @Autowired
    private GaiaSoBatchHService gaiaSoBatchHService;
    @Autowired
    private GaiaSoBatchDService gaiaSoBatchDService;
    @Resource
    private PurchaseService purchaseService;
    @Resource
    private AuthService authService;
    @Autowired
    private GaiaSoBatchAccountService accountService;
    @Resource
    private GAIAYaoshibangStoreMapper gaiaYaoshibangStoreMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaWmKuCunMapper gaiaWmKuCunMapper;

    /**
     * 订单同步：
     * 每1-5分钟，调用“订单查询”接口，从药师帮下载新订单；如订单写数据库成功的话，调用“修改状态”接口，把订单状态改为1-正在开单，以通知药师帮系统订单下载处理成功。
     * 线上测试发现，同一条订单它的customerId，存在第一次是空串，第二次有值的情况，如果两次间隔时间长，第一次的订单做了首营并且customerId相同，
     * 则由于唯一索引不会插入第二条，但是如果间隔时间长就会存在两条
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void orderSync() {
        log.info("药师帮订单同步开始==============");
        List<GaiaSoBatchAccount> allOpen = accountService.findAllOpen();
        if (CollUtil.isEmpty(allOpen)) {
            log.info("药师帮订单同步，没有开启的服务，return========");
            return;
        }
        for (GaiaSoBatchAccount account : allOpen) {
            String appId = account.getAppId();
            String authCode = account.getAuthCode();
            String client = account.getClient();

            //查询新订单
            List<YsbOrderDto> dtoList = YsbApiUtil.orderQuery(appId, authCode);
            log.info("药师帮订单定时拉取，data:" + JSON.toJSONString(dtoList));
            if (CollUtil.isEmpty(dtoList)) {
                continue;
            }
            //药师帮的程序问题我们来处理，客户名称trim
            for (YsbOrderDto dto : dtoList) {
                String drugstoreName = dto.getDrugstoreName();
                dto.setDrugstoreName(drugstoreName == null ? drugstoreName : drugstoreName.trim());
            }
            //尚：存在新用户间隔一段时间连续下了几笔单，虽然第一单做的首营上传到了药师帮，但是它并不可信，我们在拉取的时候还是要注意补全customerId，避免重复首营
            Map<String, GaiaSoBatchH> customerIdMap = gaiaSoBatchHService.selectAllCustomerId(client);
            dtoList.forEach(r -> r.setClient(client));
            List<GaiaSoBatchH> batchHList = dtoList.stream().map(GaiaSoBatchH::new).collect(Collectors.toList());
            batchHList.stream().forEach(r -> {
                r.setSoStatus(YaoshibangOrderStatusEnum.OPENING_ORDER.getStatus());
                String soCustomerid = r.getSoCustomerid();
                String soDrugstorename = r.getSoDrugstorename();
                if ("".equals(soCustomerid) && customerIdMap.containsKey(soDrugstorename)) {
                    r.setSoCustomerid(customerIdMap.get(soDrugstorename).getSoCustomerid());
                }
            });
            //药师帮的API很shi，订单修改了状态还是会传过来，so只能我们来control
            List<String> orderIdQueryList = batchHList.stream().map(r -> r.getSoOrderid()).distinct().collect(Collectors.toList());
            List<String> orderIdResultList = gaiaSoBatchHService.queryOrderIdExist(client, orderIdQueryList);
            if (CollUtil.isNotEmpty(orderIdResultList)) {
                Iterator<GaiaSoBatchH> iterator = batchHList.iterator();
                while (iterator.hasNext()) {
                    GaiaSoBatchH next = iterator.next();
                    String soOrderid = next.getSoOrderid();
                    if (orderIdResultList.contains(soOrderid)) {
                        iterator.remove();
                        log.info("药师帮重复推送的订单移除, client:{}, orderId:{}", client, soOrderid);
                    }
                }
            }
            if (CollUtil.isEmpty(batchHList)) {
                continue;
            }
            gaiaSoBatchHService.insertList(batchHList);
            List<GaiaSoBatchD> resultDList = Lists.newArrayList();
            batchHList.stream().filter(r -> CollUtil.isNotEmpty(r.getBatchDList())).map(r -> resultDList.addAll(r.getBatchDList())).collect(Collectors.toList());
            gaiaSoBatchDService.insertList(resultDList);

            List<String> orderIdList = dtoList.stream().map(r -> r.getOrderId()).collect(Collectors.toList());
            //调用“修改状态”接口，把订单状态改为1-正在开单，以通知药师帮系统订单下载处理成功。
            YsbApiUtil.updateOrder(orderIdList, Integer.valueOf(YaoshibangOrderStatusEnum.OPENING_ORDER.getStatus()), "", appId, authCode);
        }
        log.info("药师帮订单同步结束==============");
    }

    /**
     * 订单查询
     */
    @Override
    public PageInfo<GaiaSoBatchH> orderQuery(OrderQueryBean bean) {
        //本系统没有分页 ==!!
        //PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
        List<GaiaSoBatchH> list = gaiaSoBatchHService.orderQuery(bean);
        GaiaSoBatchAccount account = accountService.findByClient(bean.getClientId());
        if (account == null) {
            throw new BusinessException("请检查药师帮相关配置是否正确");
        }
        String appId = account.getAppId();
        String authCode = account.getAuthCode();
        Map<String, String> statusMap = new HashMap<>(2);
        Map<String, String> customerIdMap = new HashMap<>(2);
        //定时任务没有上下文，故改用获取列表时检测首营状态
        for (GaiaSoBatchH batchH : list) {
            if (YaoshibangOrderStatusEnum.CUSTOMER_NO_EXIST.getStatus().equals(batchH.getSoStatus()) ||
                    YaoshibangOrderStatusEnum.FIRST_CAMP_AUDITING.getStatus().equals(batchH.getSoStatus())) {
                GetWfAuditInData inData = new GetWfAuditInData();
                String soDrugstorename = batchH.getSoDrugstorename();
                inData.setWfTitle(soDrugstorename);
                inData.setClient(bean.getClientId());
                try {
                    String myApprovalList = authService.getByClientAndTitle(inData);
                    log.info("药师帮查询工作流审批状态, orderId:{}, soDrugstorename:{}, inData:{}, result:{}", batchH.getSoOrderid(), soDrugstorename, JSON.toJSONString(inData), myApprovalList);
                    //工作流状态 0-取消 1-审批不通过 2-审批中 3-审批通过
                    Map<String, String> map = getWfStatus(myApprovalList, soDrugstorename);
                    String wfStatus = map.get("wfStatus");
                    String cusSelfCode = map.get("cusSelfCode");
                    log.info("药师帮查询工作流审批状态, orderId:{}, soDrugstorename:{}, wfStatus:{}, cusSelfCode:{}", batchH.getSoOrderid(), soDrugstorename, wfStatus, cusSelfCode);
                    if ("2".equals(wfStatus)) {
                        wfStatus = YaoshibangOrderStatusEnum.FIRST_CAMP_AUDITING.getStatus();
                    } else if ("3".equals(wfStatus)) {
                        wfStatus = YaoshibangOrderStatusEnum.FIRST_CAMP_SUCCESS.getStatus();
                    } else if ("1".equals(wfStatus)) {
                        wfStatus = YaoshibangOrderStatusEnum.FIRST_CAMP_FAIL.getStatus();
                    }
                    if (wfStatus != null && !batchH.getSoStatus().equals(wfStatus) && !"0".equals(wfStatus)) {
                        //存在新客户一次性下了很多订单，这时需要把所有该客户的订单customerId和状态统一
                        //gaiaSoBatchHService.updateStatus(Lists.newArrayList(batchH), wfStatus);
                        gaiaSoBatchHService.updateStatusByClientAndCustomerName(batchH.getClient(), soDrugstorename, wfStatus);
                        log.info("药师帮，更新订单状态，client:{}, soDrugstorename:{}, wfStatus:{}", batchH.getClient(), soDrugstorename, wfStatus);
                        statusMap.put(soDrugstorename, wfStatus);
                        //batchH.setSoStatus(wfStatus);
                        if (YaoshibangOrderStatusEnum.FIRST_CAMP_SUCCESS.getStatus().equals(wfStatus)) {
                            //gaiaSoBatchHService.updateCustomerId(batchH.getId(), cusSelfCode);
                            gaiaSoBatchHService.updateCustomerIdClientAndCustomerName(batchH.getClient(), soDrugstorename, cusSelfCode);
                            customerIdMap.put(soDrugstorename, cusSelfCode);
                            log.info("药师帮，更新客户id，client:{}, soDrugstorename:{}, cusSelfCode:{}", batchH.getClient(), soDrugstorename, cusSelfCode);
                            //需要调药师帮上传药店接口，同步客户编码
                            YsbApiUtil.updateDrugstore(appId, authCode, Lists.newArrayList(new YsbDrugstoreUpdateBean(cusSelfCode, soDrugstorename)));
                            //更新药德库的id
                            GAIAYaoshibangStore gaiaYaoshibangStore=new GAIAYaoshibangStore();
                            gaiaYaoshibangStore.setDrugstoreName(soDrugstorename);
                            gaiaYaoshibangStore.setStoreID(cusSelfCode);
                            gaiaYaoshibangStore.setClient(batchH.getClient());
                            gaiaYaoshibangStoreMapper.updateByKey(gaiaYaoshibangStore);
                        }
                    }
                } catch (Exception e) {
                    log.error("药师帮,查询工作流状态报错,indata:{},error:{}", JSON.toJSONString(inData), e.getMessage());
                }
            }
        }
        //补全所有状态、客户id
        list.stream().forEach(r -> {
            String soDrugstorename = r.getSoDrugstorename();
            if (statusMap.containsKey(soDrugstorename)) {
                r.setSoStatus(statusMap.get(soDrugstorename));
            }
            if (customerIdMap.containsKey(soDrugstorename)) {
                r.setSoCustomerid(customerIdMap.get(soDrugstorename));
            }
        });
        convertStatusType(list);
        PageInfo<GaiaSoBatchH> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    private Map<String, String> getWfStatus(String myApprovalList, String soDrugstorename) {
        Map<String, String> resultMap = new HashMap<>();
        Map map = (Map) JSON.parse(myApprovalList);
        Integer code = (Integer) map.get("code");
        if (code == 0) {
            JSONArray list = (JSONArray) map.get("data");
            if (list == null) {
                return resultMap;
            }
            for (Object object : list) {
                JSONObject temp = (JSONObject) object;
                String wfStatus = temp.getString("wfStatus");
                String wfJsonString = temp.getString("wfJsonString");
                JSONArray jsonArray = JSON.parseArray(wfJsonString);
                for (Object tempObject : jsonArray) {
                    JSONObject tempJSONObject = (JSONObject) tempObject;
                    String cusSelfCode = tempJSONObject.getString("cusSelfCode");
                    String cusName = tempJSONObject.getString("cusName");
                    //有点坑这个地方，需要判断cusName
                    if (cusSelfCode != null && soDrugstorename.equals(cusName)) {
                        resultMap.put("wfStatus", wfStatus);
                        resultMap.put("cusSelfCode", cusSelfCode);
                        return resultMap;
                    }
                }
            }
        } else {
            throw new BusinessException((String) map.get("message"));
        }
        return resultMap;
    }

    private void convertStatusType(List<GaiaSoBatchH> list) {
        list.forEach(r -> {
            r.setOrderStatus(YaoshibangOrderStatusEnum.getStatusRemark(r.getSoStatus()));
            r.setInvoiceType(YaoshibangInvoiceTypeEnum.getType(r.getSoInvoicetype()));
        });
    }

    @Override
    public List<OrderDetailDto> orderDetail(Long id, String client) {
        //商品库存需要从批发库存表GAIA_WMS_KUCEN取，一个商品可能有多条记录
        String dcDataList = purchaseService.getDcDataList(client);
        String site = getSite(dcDataList);
        log.info("药师帮orderDetail，获取销售主体:{}, dcDataList:{}", site, dcDataList);
        List<OrderDetailDto> list = gaiaSoBatchHService.orderDetail(id, client, site);
        for (int i = 0; i < list.size(); i++) {
            OrderDetailDto dto = list.get(i);
            dto.setIndex(i + 1);
        }
        return list;
    }

    @Override
    public List<CustomerDto> customerQuery(String name, String client) {
        if (StrUtil.isEmpty(name)) {
            List<GaiaSoBatchH> list = gaiaSoBatchHService.selectAll();
            return list.stream().filter(r -> client.equals(r.getClient())).map(CustomerDto::new).distinct().collect(Collectors.toList());
        }
        return gaiaSoBatchHService.customerQuery(name, client);
    }

    @Override
    public void orderCreate(Long id, GetLoginOutData userInfo) {
        String client = userInfo.getClient();
        String userId = userInfo.getUserId();
        GaiaSoBatchH h = gaiaSoBatchHService.selectByPrimaryKey(id);
        //状态拦截
        if (YaoshibangOrderStatusEnum.CREATE_SUCCESS.getStatus().equals(h.getSoStatus())) {
            throw new BusinessException("重复创建！");
        }
        //检查客户是否存在
        if ("".equals(h.getSoCustomerid())) {
            if (YaoshibangOrderStatusEnum.OPENING_ORDER.getStatus().equals(h.getSoStatus())) {
                gaiaSoBatchHService.updateStatus(Lists.newArrayList(h), YaoshibangOrderStatusEnum.CUSTOMER_NO_EXIST.getStatus());
            }
            throw new BusinessException("客户不存在，请先完成客户首营");
        }
        GaiaSoBatchAccount account = accountService.findByClient(client);
        if (account == null) {
            throw new BusinessException("请检查药师帮相关配置是否正确");
        }
        String appId = account.getAppId();
        String authCode = account.getAuthCode();

        GaiaSoHeader header = new GaiaSoHeader();
        //SOR 批发销售订单; SRE 批发销售退货
        header.setSoType("SOR");
        //送达方 其实就是客户id
        header.setSoCustomerId(h.getSoCustomerid());
        header.setSoCustomer2(h.getSoCustomerid());

        //销售主体
        //尚俊超：销售主体目前是唯一的，但是后期可能会有多个，需要搞个配置，暂时取第一个，和providerName配送商商家名称 并不是同一个东西
        String dcDataList = purchaseService.getDcDataList(client);
        String site = getSite(dcDataList);
        log.info("药师帮生成批发订单，获取销售主体，dcDataList:{}，site:{}，orderId:{}", dcDataList, site, h.getSoOrderid());
        header.setSoCompanyCode(site);
        //送达方
//        String customerInfo = purchaseService.customerInfo(new CustomerInfoBean("", site));
//        String customerId2 = getCustomerId(h, customerInfo);
//        header.setSoCustomer2(customerId2);
//        header.setSoCustomerId(customerId2);
        //凭证日期取当天
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String todayFormat = LocalDate.now().format(formatter);
        header.setSoDate(todayFormat);
        //抬头备注
        header.setSoHeadRemark(h.getSoRemark());

        List<GaiaSoBatchD> dList = gaiaSoBatchDService.selectByOrderId(h.getSoOrderid(), h.getClient());
        //销售订单明细
        List<SoItem> soItemList = new ArrayList<>(dList.size());
        for (GaiaSoBatchD d : dList) {
            String dResult = purchaseService.queryGoodsList(new GoodsQueryBean(h.getClient(), d.getSoDrugcode(), site, ""));
            List<SoItem> soItems = getSoItem(dResult, d, site, todayFormat);
            if (CollUtil.isNotEmpty(soItems)) {
                soItemList.addAll(soItems);
            }
        }
        header.setSoItemList(soItemList);
        //{"code":"0","data":"XD2021000006","message":"","msg":"执行成功","warning":[]}
        log.info("药师帮自动生成批号订单，开始调用，入参:{}，订单号:{}", JSON.toJSONString(header), h.getSoOrderid());
        TokenUser user = new TokenUser();
        user.setClient(client);
        user.setUserId(userId);
        header.setTokenUser(user);
        String saveSalesOrderResult = purchaseService.saveSalesOrder(header);
        log.info("药师帮自动生成批号订单，结束调用，结果:{}，订单号:{}", saveSalesOrderResult, h.getSoOrderid());
        boolean success = getSaveSalesOrderResult(saveSalesOrderResult);
        if (success) {
            gaiaSoBatchHService.updateStatus(Lists.newArrayList(h), YaoshibangOrderStatusEnum.CREATE_SUCCESS.getStatus());
            //erp开了销售单后，使用2.4 updateOrderStatus上传一次数据回来，所有参数都要上传 status给3
            String batchOrderId = getSaleOrderId(saveSalesOrderResult);
            if (StrUtil.isNotEmpty(batchOrderId)) {
                log.info("药师帮自动生成批号订单，返回的订单号：{}，调用修改订单状态接口", batchOrderId);
                gaiaSoBatchHService.updateBatchOrderId(id, batchOrderId);
                YsbApiUtil.updateOrderAll(header, h.getSoOrderid(), batchOrderId, Integer.valueOf(YaoshibangOrderStatusEnum.DIEKING.getStatus()), appId, authCode);
            }
        } else {
            //{"code":"0015","data":null,"message":"","msg":"","warning":["客户食品许可证已过期"]}
            //胡俊林：报错信息需要提示出来
            log.error("药师帮创建批发订单失败, error body:{}", saveSalesOrderResult);
            if (saveSalesOrderResult == null) {
                throw new BusinessException("创建批发订单失败");
            }
            String errorMsg = getErrorMsg(saveSalesOrderResult);
            throw new BusinessException(errorMsg);
        }
    }

    private  String getErrorMsg(String saveSalesOrderResult) {
        JSONObject jsonObject = JSON.parseObject(saveSalesOrderResult);
        String message = jsonObject.getString("message");
        if (StrUtil.isNotEmpty(message)) {
            return message;
        }
        String msg = jsonObject.getString("msg");
        if (StrUtil.isNotEmpty(msg)) {
            return msg;
        }
        JSONArray warning = jsonObject.getJSONArray("warning");
        return Joiner.on(",").join(warning);
    }

    @Override
    public void compensate(String customerName, String customerId, String client) {
        log.info("药师帮compensate begin=====================");
        gaiaSoBatchHService.compensate(customerName, customerId, client);
        //上传药店信息
        GaiaSoBatchAccount account = accountService.findByClient(client);
        if (account == null) {
            throw new BusinessException("请检查药师帮相关配置是否正确");
        }
        String appId = account.getAppId();
        String authCode = account.getAuthCode();
        YsbApiUtil.updateDrugstore(appId, authCode, Lists.newArrayList(new YsbDrugstoreUpdateBean(customerId, customerName)));
        log.info("药师帮compensate end=====================");
    }

    private String getSaleOrderId(String saveSalesOrderResult) {
        String result = "";
        if (saveSalesOrderResult != null) {
            JSONObject jsonObject = JSON.parseObject(saveSalesOrderResult);
            String code = jsonObject.getString("code");
            if ("0".equals(code)) {
                result = jsonObject.getString("data");
            }
        }
        return result;
    }

    private boolean getSaveSalesOrderResult(String saveSalesOrderResult) {
        if (saveSalesOrderResult != null) {
            JSONObject jsonObject = JSON.parseObject(saveSalesOrderResult);
            String code = jsonObject.getString("code");
            if ("0".equals(code)) {
                return true;
            }
        }
        return false;
    }

    private List<SoItem> getSoItem(String dResult, GaiaSoBatchD d, String site, String todayFormat) {
        String soDrugcode = d.getSoDrugcode();
        List<SoItem> resultList = Lists.newArrayList();
        if (dResult != null) {
            JSONObject jsonObject = JSON.parseObject(dResult);
            String code = jsonObject.getString("code");
            if ("0".equals(code)) {
                String data = jsonObject.getString("data");
                //吴老板：批发是基于仓库的，queryGoodsList接口查出来会出现多条的情况
                //采购订单的批号，按最老的批号，先开。数量不足的，再到 另外一个批号中取相差的数量。
                //因为批号比较乱，取效期最近的，效期相同随便取一个就行
                List<YsbGoodlInfo> ysbGoodlInfos;
                if (data == null || CollUtil.isEmpty((ysbGoodlInfos = JSON.parseArray(data, YsbGoodlInfo.class)))) {
                    throw new BusinessException("商品 【" + soDrugcode + "】 库存不足");
                }
                //商品查询是个模糊查，需要做商品过滤
                ysbGoodlInfos = ysbGoodlInfos.stream().filter(r -> d.getSoDrugcode().equals(r.getProSelfCode()))
                        .sorted(Comparator.comparing(r -> Integer.parseInt(r.getBatExpiryDate()))).collect(Collectors.toList());
                BigDecimal totalQty = d.getSoNewqty();//订单数量
                for (YsbGoodlInfo info : ysbGoodlInfos) {
                    SoItem soItem = new SoItem();
                    soItem.setSoProCode(info.getProSelfCode());//商品编码
                    soItem.setSoUnit(info.getProUnit());//订单单位
                    soItem.setSoPrice(d.getSoPrice());//销售订单单价  取药师帮单价
                    soItem.setSoSiteCode(site);//地点
                    soItem.setSoLocationCode(info.getBatLocationCode());//库存地点
                    soItem.setSoBatch(info.getBatBatch());//批次
                    soItem.setSoRate(info.getPoRate());//税率
                    soItem.setSoDeliveryDate(todayFormat);//计划发货日期 //吴老板说放当天
                    soItem.setSoLineRemark(d.getSoDrugremark());//订单行备注
                    soItem.setSoBatchNo(info.getBatBatchNo());//批号
                    soItem.setBatExpiryDate(info.getBatExpiryDate());//有效期至
                    BigDecimal batNormalQty = info.getBatNormalQty();
                    BigDecimal resultQty;
                    if (batNormalQty.compareTo(totalQty) > -1) {
                        soItem.setSoQty(totalQty);//销售订单数量
                        soItem.setSoLineAmt(soItem.getSoPrice().multiply(totalQty));//订单行金额
                        resultList.add(soItem);
                        return resultList;
                    } else {
                        totalQty = totalQty.subtract(batNormalQty);
                        resultQty = batNormalQty;
                        soItem.setSoQty(resultQty);//销售订单数量
                        soItem.setSoLineAmt(soItem.getSoPrice().multiply(resultQty));//订单行金额
                        resultList.add(soItem);
                    }
                }
                //库存不够
                if (totalQty.compareTo(BigDecimal.ZERO) == 1) {
                    throw new BusinessException("商品 【" + soDrugcode + "】 库存不足");
                }
            }
        }
        return resultList;
    }

    private String getCustomerId(GaiaSoBatchH h, String customerInfo) {
        String customerId2 = "";
        if (customerInfo != null) {
            JSONObject jsonObject = JSON.parseObject(customerInfo);
            String code = jsonObject.getString("code");
            if ("0".equals(code)) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (Object object : jsonArray) {
                    JSONObject temp = (JSONObject) object;
                    String label = temp.getString("label");
                    if (label.equals(h.getSoDrugstorename())) {
                        String value = temp.getString("value");
                        customerId2 = value;
                    }
                }
            }
        }
        return customerId2;
    }

    private String getSite(String dcDataList) {
        String site = "";
        if (dcDataList != null) {
            JSONObject jsonObject = JSON.parseObject(dcDataList);
            String code = jsonObject.getString("code");
            if ("0".equals(code)) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                if (jsonArray != null && jsonArray.size() > 0) {
                    JSONObject temp = (JSONObject) jsonArray.get(0);
                    //String label = temp.getString("label");//名称
                    site = temp.getString("value");//DC编码
                }
            }
        }
        return site;
    }


    //@Scheduled(cron = "*/30 * * * * ?")
    //@PostConstruct
    public void mockSchedule() {
        orderSync();
//        try {
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void priceSync(){
        log.info("药师帮-药品价格同步开始==============");
        List<GaiaSoBatchAccount> allOpen = accountService.findAllOpen();
        if (CollUtil.isEmpty(allOpen)) {
            log.info("药师帮药品价格，没有开启的服务，return========");
            return;
        }
        for (GaiaSoBatchAccount account : allOpen) {
            //获取批发仓库
            List<GaiaDcData> dcDataList= gaiaDcDataMapper.getWholeSaleDC(account.getClient());
            for (GaiaDcData dcData :dcDataList) {
                //获取价格上传配置
                List<LinkedHashMap<String,Object>> configList=gaiaProductBusinessMapper.getSystemConfig(account.getClient(),"UPLOAD_YSB_PRICE");
                if(configList.isEmpty()){continue;}
                //获取商品价格
                List<LinkedHashMap<String,Object>> priceList=new ArrayList<>();
                if ("4".equals(configList.get(0).get("GCSP_PARA1").toString())){
                    /**
                     * 从表GAIA_BATCH_INFO中查询所有的商品 where client = 加盟商号 and bat_SITE_CODE = DC_CODE
                     * 取每条商品的最高价BAT_PO_PRICE*(1+GCSP_PARA2/100)四舍五入保留两位小数上传
                     */
                    priceList=gaiaProductBusinessMapper.getPrice_BAT_PO_PRICE(account.getClient(),dcData.getDcCode(),Integer.valueOf(configList.get(0).get("GCSP_PARA2").toString()) );
                }else{
                    /**
                     * GCSP_PARA1=1,取对应GAIA_PRODUCT_BUSINESS.PRO_YSJ1上传；
                     * GCSP_PARA1=2, 取对应GAIA_PRODUCT_BUSINESS.PRO_YSJ2上传;
                     * GCSP_PARA1=3,取对应GAIA_PRODUCT_BUSINESS.PRO_YSJ3上传；
                     */
                    priceList=gaiaProductBusinessMapper.getPrice_YSJ1(account.getClient(),dcData.getDcCode(),Integer.valueOf(configList.get(0).get("GCSP_PARA1").toString()));
                }
                if(!priceList.isEmpty()){
                    YsbApiUtil.updatePrice(account.getAppId(),account.getAuthCode(),priceList);
                }
            }
        }
        log.info("药师帮药品价格同步结束==============");
    }

    @Override
    public void stockSync(){
        log.info("药师帮-药品库存同步开始==============");
        List<GaiaSoBatchAccount> allOpen = accountService.findAllOpen();
        if (CollUtil.isEmpty(allOpen)) {
            log.info("药师帮药品库存，没有开启的服务，return========");
            return;
        }
        for (GaiaSoBatchAccount account : allOpen) {
            //获取批发仓库
            List<GaiaDcData> dcDataList= gaiaDcDataMapper.getWholeSaleDC(account.getClient());
            for (GaiaDcData dcData :dcDataList) {
                //获取仓库库存上传药师帮百分比
                List<LinkedHashMap<String,Object>> configList=gaiaWmKuCunMapper.getWMSConfig(account.getClient(),dcData.getDcCode(),"UPLOAD_YSB_STOCK_PCT");
                if(configList.isEmpty()){continue;}
                //获取商品库存
                List<LinkedHashMap<String,Object>> stockList=gaiaWmKuCunMapper.getKYSL(account.getClient(),dcData.getDcCode(),Integer.valueOf(configList.get(0).get("EXT_VALUE").toString()));
                if(!stockList.isEmpty()){
                    List<LinkedHashMap<String,Object>> stockListNew=new ArrayList<>();
                    for (LinkedHashMap<String,Object> stock:stockList) {
                        LinkedHashMap<String,Object> stockNew=new LinkedHashMap<>();
                        stockNew.put("drugCode",stock.get("drugCode"));
                        stockNew.put("stock", BigDecimalUtil.ToBigDecimal(stock.get("stock")).multiply(new BigDecimal(configList.get(0).get("CONFIG_VALUE").toString())).divide(new BigDecimal(100)).setScale(0,BigDecimal.ROUND_FLOOR));
                        stockListNew.add(stockNew);
                    }
                    YsbApiUtil.updateStock(account.getAppId(),account.getAuthCode(),stockListNew);
                }
            }
        }
        log.info("药师帮药品库存同步结束==============");
    }

}
