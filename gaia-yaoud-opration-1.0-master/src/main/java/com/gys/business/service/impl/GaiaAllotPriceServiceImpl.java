package com.gys.business.service.impl;

import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaAllotPriceMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.AllotPrice;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.GaiaAllotPriceService;
import com.gys.business.service.data.check.OrderOperateForm;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import groovyjarjarpicocli.CommandLine;
import lombok.extern.slf4j.Slf4j;
import org.apache.xmlbeans.impl.xb.xsdschema.All;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/16 14:50
 **/
@Slf4j
@Service("gaiaAllotPriceService")
public class GaiaAllotPriceServiceImpl implements GaiaAllotPriceService {
    @Resource
    private GaiaAllotPriceMapper gaiaAllotPriceMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    private static String CLIENT_CZL = "10000042";

    @Transactional
    @Override
    public void batchUpdatePrice(OrderOperateForm operateForm) {
        if (!CLIENT_CZL.equals(operateForm.getClient())) {
            return;
        }
        log.info(String.format("<请货审核><目录价更新><请求参数：%s>", JSON.toJSONString(operateForm)));
        //维护当前加盟商+商品 的目录价（全部门店）
        for (OrderOperateForm.ReplenishDetail replenishDetail : operateForm.getDetailList()) {
            //先批量更新所有门店+商品的目录价格
            AllotPrice batchAllotPrice = new AllotPrice();
            batchAllotPrice.setClient(operateForm.getClient());
            batchAllotPrice.setAlpCataloguePrice(replenishDetail.getSendPrice());
            batchAllotPrice.setAlpProCode(replenishDetail.getProSelfCode());
            batchAllotPrice.setAlpUpdateBy(operateForm.getUserId());
            updateByClientAndPro(batchAllotPrice);

            //获取加盟商下加盟商+商品（所有门店）
            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.findList(operateForm.getClient());
            if (storeDataList == null || storeDataList.size() == 0) {
                continue;
            }
            //再次判断是否存在遗漏门店-新增
            for (GaiaStoreData storeData : storeDataList) {
                AllotPrice cond = new AllotPrice();
                cond.setClient(operateForm.getClient());
                cond.setAlpReceiveSite(storeData.getStoCode());
                cond.setAlpProCode(replenishDetail.getProSelfCode());
                AllotPrice storeAllotPrice = gaiaAllotPriceMapper.getByUnique(cond);
                if (storeAllotPrice == null) {
                    storeAllotPrice = new AllotPrice();
                    storeAllotPrice.setClient(operateForm.getClient());
                    storeAllotPrice.setAlpProCode(replenishDetail.getProSelfCode());
                    storeAllotPrice.setAlpReceiveSite(storeData.getStoCode());
                    storeAllotPrice.setAlpCataloguePrice(replenishDetail.getSendPrice());
                    storeAllotPrice.setAlpAddAmt(BigDecimal.ZERO);
                    storeAllotPrice.setAlpAddRate(BigDecimal.ZERO);
                    storeAllotPrice.setAlpCreateBy(operateForm.getUserId());
                    storeAllotPrice.setAlpCreateDate(CommonUtil.getyyyyMMdd());
                    storeAllotPrice.setAlpCreateTime(CommonUtil.getHHmmss());
                    add(storeAllotPrice);
                }
            }
        }
    }

    @Transactional
    @Override
    public void updateByClientAndPro(AllotPrice allotPrice) {
        allotPrice.setAlpUpdateDate(CommonUtil.getyyyyMMdd());
        allotPrice.setAlpUpdateTime(CommonUtil.getHHmmss());
        gaiaAllotPriceMapper.updateByClientAndPro(allotPrice);
    }

    @Transactional
    @Override
    public AllotPrice add(AllotPrice allotPrice) {
        try {
            gaiaAllotPriceMapper.add(allotPrice);
        } catch (Exception e) {
            throw new BusinessException("新增商品加点目录价异常", e);
        }
        return allotPrice;
    }
}
