package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/4 15:25
 **/
@Data
@ApiModel
public class PreReplenishVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "商品编码", example = "10012")
    private String proSelfCode;
    @ApiModelProperty(value = "商品名称", example = "阿莫西林胶囊")
    private String proSelfName;
    @ApiModelProperty(value = "规格", example = "10g*30粒")
    private String productSpec;
    @ApiModelProperty(value = "生产厂家", example = "富康药业")
    private String supplierName;
    @ApiModelProperty(value = "单位", example = "盒")
    private String productUnit;
    @ApiModelProperty(value = "门店ID", example = "100001")
    private String storeId;
    @ApiModelProperty(value = "门店名称", example = "白龙路店")
    private String storeName;
    @ApiModelProperty(value = "计划铺货量", example = "10")
    private BigDecimal planQuantity;
    @ApiModelProperty(value = "实际铺货量", example = "9")
    private BigDecimal actualQuantity;
    @ApiModelProperty(value = "需补铺货量", example = "1")
    private BigDecimal needQuantity;

}
