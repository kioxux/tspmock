package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdMemberClass;
import com.gys.business.service.data.GaiaSdIntegralChange;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.member.GetMemberCardUpgrade;
import com.gys.business.service.data.member.MemberCardUpgradeInData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/12 17:49
 * @Version 1.0.0
 **/
public interface GaiaSdMemberClassService {
    List<GaiaSdMemberClass> getAll(Map<String, Object> map);

    GetMemberCardUpgrade getMemberCardUpgrade(GetQueryMemberInData inData);

    void UpdMemberCardChange(GetLoginOutData userInfo, MemberCardUpgradeInData inData);
}
