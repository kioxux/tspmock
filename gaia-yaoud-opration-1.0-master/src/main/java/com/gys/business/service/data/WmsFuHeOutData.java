package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class WmsFuHeOutData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "复核箱号")
    private String fhxh;

    @ApiModelProperty(value = "地点")
    private String site;

    @ApiModelProperty(value = "客户收货状态 0-未收货 1-收货中 2-已收货")
    private String shzt;
}
