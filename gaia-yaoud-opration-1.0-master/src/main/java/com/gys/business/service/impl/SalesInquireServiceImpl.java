package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdPaymentMethodMapper;
import com.gys.business.mapper.GaiaSdSaleDMapper;
import com.gys.business.mapper.GaiaSdSaleHMapper;
import com.gys.business.mapper.entity.GaiaSdPaymentMethod;
import com.gys.business.service.SalesInquireService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.stream.IntStream;

import com.gys.common.data.PageInfo;
import com.gys.util.CopyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SalesInquireServiceImpl implements SalesInquireService {
    @Autowired
    private GaiaSdSaleHMapper saleHMapper;

    @Autowired
    private GaiaSdPaymentMethodMapper methodMapper;

    @Autowired
    private GaiaSdSaleDMapper saleDMapper;



    @Override
    public PageInfo<SalesInquireOutData> getSalesInquireList(SalesInquireInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        List<SalesInquireOutData> outData = this.saleHMapper.getSalesInquireList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            if (outData.size() > 0) {
                for(int i = 0; i < outData.size(); ++i) {
                    ((SalesInquireOutData)outData.get(i)).setIndex(i + 1);
                }
            }

            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<SalesInquireDetailOutData> getSalesInquireDetailList(SalesInquireInData inData) {
        List<SalesInquireDetailOutData> outData = this.saleDMapper.getSalesInquireDetailList(inData);
        if (CollUtil.isNotEmpty(outData)) {
            IntStream.range(0, outData.size()).forEach(i -> ((SalesInquireDetailOutData) outData.get(i)).setIndexDetail(i + 1));
        }
        return outData;
    }

    @Override
    public List<GaiaSdPayMethodOutData> queryPayMethod(GetLoginOutData userInfo) {
        Example example = new Example(GaiaSdPaymentMethod.class);
        example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("gspmBrId", userInfo.getDepId());
        List<GaiaSdPaymentMethod> paymentMethods = methodMapper.selectByExample(example);
        List<GaiaSdPayMethodOutData> outData = CopyUtil.copyList(paymentMethods, GaiaSdPayMethodOutData.class);
        return outData;
    }


}
