//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetCardLoseOrChangeInData {
    private String clientId;
    private String name;
    private String phone;
    private String type;
    private String date;
    private String oldCardNum;
    private String newCardNum;
    private String shopName;

    public GetCardLoseOrChangeInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getName() {
        return this.name;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getType() {
        return this.type;
    }

    public String getDate() {
        return this.date;
    }

    public String getOldCardNum() {
        return this.oldCardNum;
    }

    public String getNewCardNum() {
        return this.newCardNum;
    }

    public String getShopName() {
        return this.shopName;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public void setOldCardNum(final String oldCardNum) {
        this.oldCardNum = oldCardNum;
    }

    public void setNewCardNum(final String newCardNum) {
        this.newCardNum = newCardNum;
    }

    public void setShopName(final String shopName) {
        this.shopName = shopName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetCardLoseOrChangeInData)) {
            return false;
        } else {
            GetCardLoseOrChangeInData other = (GetCardLoseOrChangeInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label107;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label107;
                    }

                    return false;
                }

                Object this$name = this.getName();
                Object other$name = other.getName();
                if (this$name == null) {
                    if (other$name != null) {
                        return false;
                    }
                } else if (!this$name.equals(other$name)) {
                    return false;
                }

                Object this$phone = this.getPhone();
                Object other$phone = other.getPhone();
                if (this$phone == null) {
                    if (other$phone != null) {
                        return false;
                    }
                } else if (!this$phone.equals(other$phone)) {
                    return false;
                }

                label86: {
                    Object this$type = this.getType();
                    Object other$type = other.getType();
                    if (this$type == null) {
                        if (other$type == null) {
                            break label86;
                        }
                    } else if (this$type.equals(other$type)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$date = this.getDate();
                    Object other$date = other.getDate();
                    if (this$date == null) {
                        if (other$date == null) {
                            break label79;
                        }
                    } else if (this$date.equals(other$date)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$oldCardNum = this.getOldCardNum();
                    Object other$oldCardNum = other.getOldCardNum();
                    if (this$oldCardNum == null) {
                        if (other$oldCardNum == null) {
                            break label72;
                        }
                    } else if (this$oldCardNum.equals(other$oldCardNum)) {
                        break label72;
                    }

                    return false;
                }

                Object this$newCardNum = this.getNewCardNum();
                Object other$newCardNum = other.getNewCardNum();
                if (this$newCardNum == null) {
                    if (other$newCardNum != null) {
                        return false;
                    }
                } else if (!this$newCardNum.equals(other$newCardNum)) {
                    return false;
                }

                Object this$shopName = this.getShopName();
                Object other$shopName = other.getShopName();
                if (this$shopName == null) {
                    if (other$shopName != null) {
                        return false;
                    }
                } else if (!this$shopName.equals(other$shopName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetCardLoseOrChangeInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $name = this.getName();
        result = result * 59 + ($name == null ? 43 : $name.hashCode());
        Object $phone = this.getPhone();
        result = result * 59 + ($phone == null ? 43 : $phone.hashCode());
        Object $type = this.getType();
        result = result * 59 + ($type == null ? 43 : $type.hashCode());
        Object $date = this.getDate();
        result = result * 59 + ($date == null ? 43 : $date.hashCode());
        Object $oldCardNum = this.getOldCardNum();
        result = result * 59 + ($oldCardNum == null ? 43 : $oldCardNum.hashCode());
        Object $newCardNum = this.getNewCardNum();
        result = result * 59 + ($newCardNum == null ? 43 : $newCardNum.hashCode());
        Object $shopName = this.getShopName();
        result = result * 59 + ($shopName == null ? 43 : $shopName.hashCode());
        return result;
    }

    public String toString() {
        return "GetCardLoseOrChangeInData(clientId=" + this.getClientId() + ", name=" + this.getName() + ", phone=" + this.getPhone() + ", type=" + this.getType() + ", date=" + this.getDate() + ", oldCardNum=" + this.getOldCardNum() + ", newCardNum=" + this.getNewCardNum() + ", shopName=" + this.getShopName() + ")";
    }
}
