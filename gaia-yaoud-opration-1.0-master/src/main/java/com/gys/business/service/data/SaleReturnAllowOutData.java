package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SaleReturnAllowOutData {
    @ApiModelProperty(value = "是否允许退货")
    private Boolean flag;
    @ApiModelProperty(value = "返回消息")
    private String msg;
}
