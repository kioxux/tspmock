package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetQueryMemberInData implements Serializable {
    private static final long serialVersionUID = -7919809645749595391L;

    /**
     * 加盟商
     */
    private String clientId;

    /**
     * 手机
     */
    private String phone;

    /**
     * 卡号
     */
    private String cardId;


    private String givePoint;

    /**
     * 店号
     */
    private String brId;

    /**
     * 手机号 或者 卡号
     */
    @ApiModelProperty(value = "手机号/卡号")
    private String phoneOrCardId;

    private String memberId;
}
