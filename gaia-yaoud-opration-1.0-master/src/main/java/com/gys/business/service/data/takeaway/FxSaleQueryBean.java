package com.gys.business.service.data.takeaway;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/9/2 10:57
 */
@Data
public class FxSaleQueryBean {

    private String orderId;//订单号

    private String client;
    private String stoCode;
}
