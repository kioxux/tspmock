//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class SkuStock {
    private String outSkuId;
    private Integer stockQty;

    public SkuStock() {
    }

    public String getOutSkuId() {
        return this.outSkuId;
    }

    public Integer getStockQty() {
        return this.stockQty;
    }

    public void setOutSkuId(final String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public void setStockQty(final Integer stockQty) {
        this.stockQty = stockQty;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof SkuStock)) {
            return false;
        } else {
            SkuStock other = (SkuStock)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$outSkuId = this.getOutSkuId();
                Object other$outSkuId = other.getOutSkuId();
                if (this$outSkuId == null) {
                    if (other$outSkuId != null) {
                        return false;
                    }
                } else if (!this$outSkuId.equals(other$outSkuId)) {
                    return false;
                }

                Object this$stockQty = this.getStockQty();
                Object other$stockQty = other.getStockQty();
                if (this$stockQty == null) {
                    if (other$stockQty != null) {
                        return false;
                    }
                } else if (!this$stockQty.equals(other$stockQty)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof SkuStock;
    }

    public int hashCode() {
        
        int result = 1;
        Object $outSkuId = this.getOutSkuId();
        result = result * 59 + ($outSkuId == null ? 43 : $outSkuId.hashCode());
        Object $stockQty = this.getStockQty();
        result = result * 59 + ($stockQty == null ? 43 : $stockQty.hashCode());
        return result;
    }

    public String toString() {
        return "SkuStock(outSkuId=" + this.getOutSkuId() + ", stockQty=" + this.getStockQty() + ")";
    }
}
