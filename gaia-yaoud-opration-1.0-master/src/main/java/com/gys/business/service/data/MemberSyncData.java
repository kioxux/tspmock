package com.gys.business.service.data;

import lombok.Data;

@Data
public class MemberSyncData {
    private String client;
//    private String site;
    //会员号
    private String memberId;
    //会员卡号
    private String CardId;
}
