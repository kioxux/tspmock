package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.alibaba.excel.util.DateUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.*;
import com.gys.business.service.data.*;
import com.gys.business.service.data.check.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.ClientParamsEnum;
import com.gys.common.enums.RequestReviewStatusEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.GuoYaoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 15:19
 **/
@Slf4j
@Service("requestOrderCheckService")
public class RequestOrderCheckServiceImpl implements RequestOrderCheckService {
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private GaiaSdWtbhdDService gaiaSdWtbhdDService;
    @Resource
    private GaiaSdWtbhdHService gaiaSdWtbhdHService;
    @Resource
    private ProductSortService productSortService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private ReplenishWebService replenishWebService;
    @Resource
    private GaiaAllotPriceMapper gaiaAllotPriceMapper;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaAllotPriceService gaiaAllotPriceService;
    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;
    @Resource
    private GaiaSdWtproMapper gaiaSdWtproMapper;
    @Resource
    private ClientParamsService clientParamsService;

    @Override
    public List<SupplierVO> supplierList(String client) {
        List<SupplierVO> list = Lists.newArrayList();
        GaiaSupplierBusiness cond = new GaiaSupplierBusiness();
        cond.setClient(client);
        cond.setSupClass("12");//委托配送供应商
        List<GaiaSupplierBusiness> supplierBusinessList = gaiaSupplierBusinessMapper.findList(cond);
        if (supplierBusinessList != null && supplierBusinessList.size() > 0) {
            for (GaiaSupplierBusiness supplierBusiness : supplierBusinessList) {
                SupplierVO supplierVO = new SupplierVO();
                supplierVO.setValue(supplierBusiness.getSupSelfCode());
                supplierVO.setName(supplierBusiness.getSupName());
                supplierVO.setLabel(String.format("%s-%s", supplierBusiness.getSupSelfCode(), supplierBusiness.getSupName()));
                list.add(supplierVO);
            }
        }
        return list;
    }

    @Transactional
    @Override
    public void saveOrder(OrderOperateForm operateForm) {
        try {
            for (OrderOperateForm.ReplenishDetail replenishDetail : operateForm.getDetailList()) {
                //保存或更新补货明细表
                saveReplenishDetail(operateForm, replenishDetail);
            }
            GaiaSdReplenishH replenishH = getGaiaSdReplenishH(operateForm.getClient(), operateForm.getStoreId(), operateForm.getReplenishCode());
            updateGaiaSdReplenishH(replenishH);

            //异步去更新目录价
            threadPoolTaskExecutor.execute(() -> gaiaAllotPriceService.batchUpdatePrice(operateForm));
        } catch (Exception e) {
            log.error(String.format("<请货审核><保存明细><保存明细异常:%s>", e.getMessage()), e);
        }
    }

    @Override
    public List<RequestOrderDetailVO> getStoreProList(ProductForm productForm) {
        List<RequestOrderDetailVO> list = new ArrayList<>();
        GetProductThirdlyInData inData = new GetProductThirdlyInData();
        inData.setClient(productForm.getClient());
        inData.setProSite(productForm.getStoreId());
        inData.setIsSpecial("Y");
        inData.setType("1");
        inData.setMateType("0");
        inData.setPageNum(0);
        inData.setPageSize(10);
        inData.setContent(productForm.getKeywords());
        PageInfo pageInfo = productSortService.queryProThirdly(inData);
        if (pageInfo != null && pageInfo.getList() != null && pageInfo.getList().size() > 0) {
            List<GetProductThirdlyOutData> productList = JSON.parseArray(JSON.toJSONString(pageInfo.getList()), GetProductThirdlyOutData.class);
            for (GetProductThirdlyOutData productOutData : productList) {
                RequestOrderDetailVO orderDetailVO = transformProductVO(productForm, productOutData);
                list.add(orderDetailVO);
            }
        }
        return list;
    }

    @Override
    public boolean validPrice(String client) {
        try {
            //判断是否关闭委托配送价格校验开关,默认开启配送价格校验【WTPS_PRICE_VALID_CLOSE=1 委托配送价格校验开关】10000098
            GaiaClSystemPara cond = new GaiaClSystemPara();
            cond.setClient(client);
            cond.setGcspId(ClientParamsEnum.WTPS_PRICE_VALID_CLOSE.id);
            GaiaClSystemPara clSystemPara = clientParamsService.getUnique(cond);
            boolean notValid = clSystemPara != null && CommonConstant.IS_OPEN.equals(clSystemPara.getGcspPara1());
            return !notValid;
        } catch (Exception e) {
            log.error("<委托配送><价格验证><获取价格验证开关异常>", e);
            return true;
        }
    }

    private RequestOrderDetailVO transformProductVO(ProductForm productForm, GetProductThirdlyOutData productOutData) {
        RequestOrderDetailVO orderDetailVO = new RequestOrderDetailVO();
        orderDetailVO.setProSelfCode(productOutData.getProCode());
        orderDetailVO.setProName(productOutData.getName());
        orderDetailVO.setProUnit(productOutData.getUnit());
        orderDetailVO.setProSpec(productOutData.getSpecs());
        orderDetailVO.setFactoryName(productOutData.getProFactoryName());
        orderDetailVO.setSalePrice(productOutData.getPriceNormal());
        orderDetailVO.setSaleAmount(BigDecimal.ZERO);
        //获取价格库存信息
        try {
            GetReplenishInData inData = new GetReplenishInData();
            inData.setClientId(productForm.getClient());
            inData.setStoCode(productForm.getStoreId());
            inData.setProductCode(productOutData.getProCode());
            GetReplenishDetailOutData productData = replenishWebService.replenishDetail(inData);
            if (productData != null) {
                orderDetailVO.setSendPrice(StringUtil.isEmpty(productData.getCheckOutAmt()) ? productOutData.getAddAmt() : new BigDecimal(productData.getCheckOutAmt()));
                orderDetailVO.setCostPrice(StringUtil.isEmpty(productData.getPoPrice()) ? BigDecimal.ZERO : new BigDecimal(productData.getPoPrice()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        orderDetailVO.setSendAmount(BigDecimal.ZERO);
        orderDetailVO.setCostPrice(BigDecimal.ZERO);
        orderDetailVO.setRequestQuantity(BigDecimal.ZERO);
        //库存、成本
        orderDetailVO.setStoreStock(BigDecimal.ZERO);
        orderDetailVO.setDcStock(BigDecimal.ZERO);
        orderDetailVO.setWeekSales(BigDecimal.ZERO);
        orderDetailVO.setMonthSales(BigDecimal.ZERO);
        orderDetailVO.setSupplierStock(BigDecimal.ZERO);
        return orderDetailVO;
    }

    private void updateGaiaSdReplenishH(GaiaSdReplenishH replenishH) {
        GaiaSdReplenishD cond = new GaiaSdReplenishD();
        cond.setClientId(replenishH.getClientId());
        cond.setGsrdBrId(replenishH.getGsrhBrId());
        cond.setGsrdVoucherId(replenishH.getGsrhVoucherId());
        List<GaiaSdReplenishD> detailList = gaiaSdReplenishDMapper.findList(cond);
        BigDecimal totalQty = BigDecimal.ZERO;
        BigDecimal totalAmt = BigDecimal.ZERO;
        if (detailList != null && detailList.size() > 0) {
            for (GaiaSdReplenishD replenishD : detailList) {
                totalQty = totalQty.add(StringUtil.isEmpty(replenishD.getGsrdNeedQty()) ? BigDecimal.ZERO : new BigDecimal(replenishD.getGsrdNeedQty()));
                totalAmt = totalAmt.add(replenishD.getGsrdVoucherAmt());
            }
        }
        replenishH.setGsrhTotalQty(totalQty.toString());
        replenishH.setGsrhTotalAmt(totalAmt);
        gaiaSdReplenishHMapper.update(replenishH);
    }

    @Override
    public List<RequestOrderVO> orderList(OrderCheckForm orderCheckForm) {
        orderCheckForm.setEndDate(orderCheckForm.getEndDate().replaceAll("-", ""));
        orderCheckForm.setStartDate(orderCheckForm.getStartDate().replaceAll("-", ""));
        List<RequestOrderVO> list = gaiaSdReplenishHMapper.orderList(orderCheckForm);
        int i = 1;
        for (RequestOrderVO requestOrderVO : list) {
            requestOrderVO.setIndex(i++);
        }
        return list;
    }

    @Override
    public List<RequestOrderVO> callList(CallOrderForm callOrderForm) {
        return gaiaSdReplenishHMapper.getCallList(callOrderForm);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void closeOrder(CloseOrderForm closeOrderForm) {
        //校验补货单状态是否可关闭
        GaiaSdReplenishH replenishH = getGaiaSdReplenishH(closeOrderForm.getClient(), closeOrderForm.getStoreId(), closeOrderForm.getReplenishCode());
        if (replenishH != null && !CheckConstant.DN_FLAG_OK.equals(replenishH.getGsrhDnFlag())) {
            //更新补货单
            replenishH.setGsrhFlag("2");
            replenishH.setGsrhDnFlag(CheckConstant.DN_FLAG_OK);
            replenishH.setGsrhDnBy(closeOrderForm.getUserId());
            replenishH.setGsrhDnDate(CommonUtil.getyyyyMMdd());
            replenishH.setGsrhDnTime(CommonUtil.getHHmmss());
            gaiaSdReplenishHMapper.update(replenishH);
        } else {
            throw new BusinessException("该补货单当前状态不可关闭");
        }
    }

    @Override
    public RequestOrderDetailListVO getOrderDetail(OrderDetailForm orderDetailForm) {
        RequestOrderDetailListVO listVO = new RequestOrderDetailListVO();
        //查询列表
        List<RequestOrderDetailVO> detailVOList = gaiaSdReplenishDMapper.queryOrderDetail(orderDetailForm);
        int i = 1;
        String remarks = "";
        for (RequestOrderDetailVO detailVO : detailVOList) {
            if (i == 1) {
                remarks = detailVO.getRemarks();
            }
            detailVO.setIndex(i++);
            detailVO.setRequestQuantity(detailVO.getRequestQuantity().setScale(2, RoundingMode.HALF_UP));
            detailVO.setSaleAmount(detailVO.getSalePrice().multiply(detailVO.getRequestQuantity()).setScale(2, RoundingMode.HALF_UP));
            //配送价格[如果配送价格为0则实时获取配送价格]
            if (BigDecimal.ZERO.compareTo(detailVO.getSendPrice()) >= 0) {
                BigDecimal sendPrice = getSendPrice(orderDetailForm, detailVO);
                if (sendPrice.compareTo(BigDecimal.ZERO) > 0) {
                    detailVO.setSendPrice(sendPrice);
                }
            }
            detailVO.setSendAmount(detailVO.getSendPrice().multiply(detailVO.getRequestQuantity()).setScale(2, RoundingMode.HALF_UP));
        }
        GaiaSdReplenishH replenishH = getGaiaSdReplenishH(orderDetailForm.getClient(), orderDetailForm.getStoreId(), orderDetailForm.getReplenishCode());
        //表头信息
        HeadInfo headInfo = new HeadInfo();
        headInfo.setReplenishCode(replenishH.getGsrhVoucherId());
        headInfo.setRequestDate(formatDateType(replenishH.getGsrhDate()));
        headInfo.setReplenishType(replenishH.getGsrhPattern());
        StoreOutData storeData = gaiaStoreDataMapper.getStoreData(orderDetailForm.getClient(), orderDetailForm.getStoreId());
        if (storeData != null) {
            headInfo.setStoreName(String.format("%s-%s", storeData.getStoCode(), storeData.getStoShortName()));
        }
        headInfo.setRemarks(remarks);
        if ("1".equals(replenishH.getGsrhDnFlag())) {
            headInfo.setStatus(RequestReviewStatusEnum.REVIEWED.type);
            if (replenishH.getGsrhFlag() != null && "2".equals(replenishH.getGsrhFlag())) {
                headInfo.setStatus(RequestReviewStatusEnum.REJECTED.type);
            }
            headInfo.setReviewDate(formatDateType(replenishH.getGsrhDnDate()));
            headInfo.setReviewUser(replenishH.getGsrhDnBy());
        } else {
            headInfo.setStatus(RequestReviewStatusEnum.WAIT_REVIEW.type);
            headInfo.setReviewDate("");
            headInfo.setReviewUser("");
        }

        listVO.setHeadInfo(headInfo);
        //计算汇总
        listVO.setList(detailVOList);
        OrderDetailCountVO countVO = getOrderCountVO(detailVOList);
        listVO.setListNum(countVO);
        return listVO;
    }

    private BigDecimal getSendPrice(OrderDetailForm orderDetailForm, RequestOrderDetailVO detailVO) {
        AllotPrice cond = new AllotPrice();
        cond.setClient(orderDetailForm.getClient());
        cond.setAlpReceiveSite(orderDetailForm.getStoreId());
        cond.setAlpProCode(detailVO.getProSelfCode());
        AllotPrice allotPrice = gaiaAllotPriceMapper.getByUnique(cond);
        if (allotPrice != null) {
            if (allotPrice.getAlpCataloguePrice() != null && allotPrice.getAlpCataloguePrice().compareTo(BigDecimal.ZERO) > 0) {
                return allotPrice.getAlpCataloguePrice();
            } else if (allotPrice.getAlpCataloguePrice() != null){
                //是否存在商品加点比例、加点金额
                if (detailVO.getCostPrice() != null && detailVO.getCostPrice().compareTo(BigDecimal.ZERO) > 0) {
                    //加点比例
                    if (allotPrice.getAlpAddRate() != null && allotPrice.getAlpAddRate().compareTo(BigDecimal.ZERO) > 0) {
                        return detailVO.getCostPrice().multiply(BigDecimal.ONE.add(allotPrice.getAlpAddRate().divide(BigDecimal.valueOf(100)))).setScale(2, RoundingMode.HALF_UP);
                    }
                    //加点金额
                    if (allotPrice.getAlpAddAmt() != null && allotPrice.getAlpAddAmt().compareTo(BigDecimal.ZERO) > 0) {
                        return detailVO.getCostPrice().add(allotPrice.getAlpAddAmt());
                    }

                    //是否存在门店统一加点比例、加点金额
                    AllotPrice storeCond = new AllotPrice();
                    storeCond.setClient(orderDetailForm.getClient());
                    storeCond.setAlpReceiveSite(orderDetailForm.getStoreId());
                    AllotPrice storeAllotPrice = gaiaAllotPriceMapper.getStoreAllotPrice(storeCond);

                    if (storeAllotPrice != null) {
                        //加点比例
                        if (storeAllotPrice.getAlpAddRate() != null && storeAllotPrice.getAlpAddRate().compareTo(BigDecimal.ZERO) > 0) {
                            return detailVO.getCostPrice().multiply(BigDecimal.ONE.add(storeAllotPrice.getAlpAddRate().divide(BigDecimal.valueOf(100)))).setScale(2, RoundingMode.HALF_UP);
                        }
                        //加点金额
                        if (storeAllotPrice.getAlpAddAmt() != null && storeAllotPrice.getAlpAddAmt().compareTo(BigDecimal.ZERO) > 0) {
                            return detailVO.getCostPrice().add(storeAllotPrice.getAlpAddAmt());
                        }
                    }
                }
            }
        }
        return BigDecimal.ZERO;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void auditOrder(OrderOperateForm orderOperateForm) {
        //补货主表
        GaiaSdReplenishH replenishH = getGaiaSdReplenishH(orderOperateForm.getClient(), orderOperateForm.getStoreId(), orderOperateForm.getReplenishCode());
        //校验状态
        if (CheckConstant.DN_FLAG_OK.equals(replenishH.getGsrhDnFlag())) {
            throw new BusinessException("该订单已审核，无需重复操作。");
        }
        //保存明细
        for (OrderOperateForm.ReplenishDetail replenishDetail : orderOperateForm.getDetailList()) {
            //保存或更新补货明细表
            saveReplenishDetail(orderOperateForm, replenishDetail);
        }


        //补货明细
        GaiaSdReplenishD cond = new GaiaSdReplenishD();
        cond.setClientId(replenishH.getClientId());
        cond.setGsrdBrId(replenishH.getGsrhBrId());
        cond.setGsrdVoucherId(replenishH.getGsrhVoucherId());
        List<GaiaSdReplenishD> list = gaiaSdReplenishDMapper.findList(cond);
        //是否关闭价格校验
        boolean flag = validPrice(orderOperateForm.getClient());
        //检查配送价是否存在0
        for (GaiaSdReplenishD replenishD : list) {
            if (flag && (replenishD.getGsrdSendPrice() == null || BigDecimal.ZERO.compareTo(replenishD.getGsrdSendPrice()) >= 0)) {
                throw new BusinessException(String.format("该商品[%s]的配送价为0，请修改后审核。", replenishD.getGsrdProId()));
            }
        }
        try {
            //更新补货主表
            replenishH.setGsrhDnFlag("1");
            replenishH.setGsrhDnBy(orderOperateForm.getUserId());
            replenishH.setGsrhDnDate(CommonUtil.getyyyyMMdd());
            replenishH.setGsrhDnTime(CommonUtil.getHHmmss());
            updateGaiaSdReplenishH(replenishH);
            //保存审核信息至三方补货表
            saveReplenishToThirdParty(orderOperateForm, replenishH, list);
        } catch (Exception e) {
            log.error("<请货审核><审核订单><审核订单异常：>" + e.getMessage(), e);
        }
        //查询参数 冷链的系统配置参数
        GaiaClSystemPara supplier = gaiaClSystemParaMapper.selectByPrimaryKey(orderOperateForm.getClient(), "SUPPLIER_CODE");
        if (ObjectUtil.isNotEmpty(supplier) && replenishH.getGsrhAddr().equals(supplier.getGcspPara1())) {
            sendBillToGuoYao(orderOperateForm, replenishH);
        } else {
            //异步去更新目录价
            threadPoolTaskExecutor.execute(() -> gaiaAllotPriceService.batchUpdatePrice(orderOperateForm));
        }
    }

    private void sendBillToGuoYao(OrderOperateForm orderOperateForm, GaiaSdReplenishH replenishH) {
        List<JSONObject> info = new ArrayList<>();
        HashMap<String, Object> map = gaiaSdWtproMapper.getSdWtstoInfo(replenishH.getClientId(), replenishH.getGsrhBrId());
        if (ObjectUtil.isEmpty(map)) {
            throw new BusinessException("未查询到该门店在国药系统中的编码，请先维护编码！");
        }
        List<GaiaSdWtbhdD> updateList = new ArrayList<>();
        for (OrderOperateForm.ReplenishDetail detail : orderOperateForm.getDetailList()) {
            GaiaSdWtpro param = new GaiaSdWtpro();
            param.setClient(replenishH.getClientId());
            param.setProSelfCode(detail.getProSelfCode());
            GaiaSdWtpro proInfo = gaiaSdWtproMapper.getProInfo(param);
            JSONObject json = new JSONObject();
            json.set("billno", replenishH.getClientId()+"-"+ replenishH.getGsrhVoucherId()+"-"+proInfo.getProSelfCode());
            json.set("cstcode", map.get("stoDsfSite"));
            json.set("sendaddrcode", map.get("stoNm"));
            json.set("goodstat", proInfo.getProNm());
            json.set("qty", detail.getRequestQuantity());
            json.set("prc", detail.getSendPrice());
            json.set("sumvalue", detail.getRequestQuantity().multiply(detail.getSendPrice()));
            info.add(json);
            GaiaSdWtbhdD wtbhdD = new GaiaSdWtbhdD();
            wtbhdD.setClient(replenishH.getClientId());
            wtbhdD.setGswbdVoucherId(replenishH.getGsrhVoucherId());
            wtbhdD.setGswbdProId(proInfo.getProSelfCode());
            updateList.add(wtbhdD);
        }
        GuoYaoUtil.sendOrder(info);
        for (GaiaSdWtbhdD gaiaSdWtbhdD : updateList) {
            gaiaSdWtproMapper.updateSendSendStatus(gaiaSdWtbhdD);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void rejectOrder(CloseOrderForm closeOrderForm) {
        GaiaSdReplenishH replenishH = getGaiaSdReplenishH(closeOrderForm.getClient(), closeOrderForm.getStoreId(), closeOrderForm.getReplenishCode());
        if (replenishH != null && !CheckConstant.DN_FLAG_OK.equals(replenishH.getGsrhDnFlag())) {
            replenishH.setGsrhFlag("2");
            replenishH.setGsrhDnFlag(CheckConstant.DN_FLAG_OK);
            replenishH.setGsrhDnBy(closeOrderForm.getUserId());
            replenishH.setGsrhDnDate(CommonUtil.getyyyyMMdd());
            replenishH.setGsrhDnTime(CommonUtil.getHHmmss());
            gaiaSdReplenishHMapper.update(replenishH);
        } else {
            throw new BusinessException("当前状态不可拒绝");
        }
    }

    @Override
    public void deleteOrder(DeleteOrderForm deleteOrderForm) {
        GaiaSdReplenishH replenishH = getGaiaSdReplenishH(deleteOrderForm.getClient(), deleteOrderForm.getStoreId(), deleteOrderForm.getReplenishCode());
        if (replenishH != null && !CheckConstant.DN_FLAG_OK.equals(replenishH.getGsrhDnFlag())) {
            for (DeleteOrderForm.DeleteProVO deleteProVO : deleteOrderForm.getDetailList()) {
                GaiaSdReplenishD cond = new GaiaSdReplenishD();
                cond.setGsrdVoucherId(deleteOrderForm.getReplenishCode());
                cond.setClientId(deleteOrderForm.getClient());
                cond.setGsrdBrId(deleteOrderForm.getStoreId());
                cond.setGsrdProId(deleteProVO.getProSelfCode());
                GaiaSdReplenishD replenishD = gaiaSdReplenishDMapper.getUnique(cond);
                if (replenishD != null) {
                    replenishD.setGsrdDeleteFlag(1);
                    gaiaSdReplenishDMapper.deleteByVoucherId(replenishD);
                }
            }
            //更新汇总
            updateGaiaSdReplenishH(replenishH);
        }else {
            throw new BusinessException("当前状态不可删除");
        }
    }

    private GaiaSdReplenishH getGaiaSdReplenishH(String client, String storeId, String replenishCode) {
        GaiaSdReplenishH cond = new GaiaSdReplenishH();
        cond.setClientId(client);
        cond.setGsrhBrId(storeId);
        cond.setGsrhVoucherId(replenishCode);
        return gaiaSdReplenishHMapper.getUnique(cond);
    }

    private String formatDateType(String yyyyMMdd) {
        if (StringUtil.isEmpty(yyyyMMdd)) {
            return "";
        }
        try {
            return DateUtils.format(DateUtils.parseDate(yyyyMMdd, "yyyyMMdd"), "yyyy-MM-dd");
        } catch (ParseException e) {
            return "";
        }
    }

    private OrderDetailCountVO getOrderCountVO(List<RequestOrderDetailVO> detailVOList) {
        OrderDetailCountVO countVO = new OrderDetailCountVO();
        BigDecimal totalQty = BigDecimal.ZERO;
        BigDecimal totalSaleAmount = BigDecimal.ZERO;
        BigDecimal totalSendAmount = BigDecimal.ZERO;
        for (RequestOrderDetailVO detailVO : detailVOList) {
            totalQty = totalQty.add(detailVO.getRequestQuantity());
            totalSendAmount = totalSendAmount.add(detailVO.getSendAmount());
            totalSaleAmount = totalSaleAmount.add(detailVO.getSaleAmount());
        }
        countVO.setRequestQuantity(totalQty);
        countVO.setSaleAmount(totalSaleAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
        countVO.setSendAmount(totalSendAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
        return countVO;
    }

    private GaiaSdReplenishD getUniqueReplenishD(String client, String storeId, String replenishCode, String proSelfCode) {
        GaiaSdReplenishD cond = new GaiaSdReplenishD();
        cond.setClientId(client);
        cond.setGsrdBrId(storeId);
        cond.setGsrdVoucherId(replenishCode);
        cond.setGsrdProId(proSelfCode);
        return gaiaSdReplenishDMapper.getUnique(cond);
    }

    private void saveReplenishDetail(OrderOperateForm operateForm, OrderOperateForm.ReplenishDetail replenishDetail) {
        GaiaSdReplenishD replenishD = getUniqueReplenishD(operateForm.getClient(), operateForm.getStoreId(),
                operateForm.getReplenishCode(), replenishDetail.getProSelfCode());
        if (replenishD == null) {
            //新增明细
            replenishD = new GaiaSdReplenishD();
            replenishD.setGsrdProId(replenishDetail.getProSelfCode());
            replenishD.setGsrdSendPrice(replenishDetail.getSendPrice());
            replenishD.setGsrdNeedQty(replenishDetail.getRequestQuantity().toString());
            replenishD.setGsrdProposeQty(replenishD.getGsrdNeedQty());
            replenishD.setGsrdVoucherId(operateForm.getReplenishCode());
            replenishD.setGsrdBrId(operateForm.getStoreId());
            replenishD.setClientId(operateForm.getClient());
            replenishD.setGsrdDate(CommonUtil.getyyyyMMdd());
            replenishD.setGsrdDeleteFlag(0);
            GetReplenishInData inData = new GetReplenishInData();
            inData.setClientId(replenishD.getClientId());
            inData.setStoCode(replenishD.getGsrdBrId());
            inData.setProductCode(replenishD.getGsrdProId());
            GetReplenishDetailOutData productData = replenishWebService.replenishDetail(inData);
            if (productData != null) {
                replenishD.setGsrdStockDepot(productData.getGsrdStockDepot());
                replenishD.setGsrdStockStore(productData.getGsrdStockStore());
                replenishD.setGsrdSalesDays1(productData.getGsrdSalesDays1());
                replenishD.setGsrdSalesDays2(productData.getGsrdSalesDays2());
                replenishD.setGsrdSalesDays3(productData.getGsrdSalesDays3());
                replenishD.setGsrdProPrice(StringUtil.isEmpty(productData.getGsrdProPrice()) ? BigDecimal.ZERO : new BigDecimal(productData.getGsrdProPrice()));
                replenishD.setGsrdVoucherAmt(replenishD.getGsrdProPrice().multiply(replenishDetail.getRequestQuantity()));
            }
            int num = gaiaSdReplenishDMapper.getCount(replenishD);
            replenishD.setGsrdSerial(String.valueOf(++num));
            gaiaSdReplenishDMapper.insert(replenishD);
        } else {
            //判断是否修改：配送价格、补货数量
            if (replenishDetail.getRequestQuantity().compareTo(new BigDecimal(replenishD.getGsrdNeedQty())) != 0
                    || replenishDetail.getSendPrice().compareTo(replenishD.getGsrdSendPrice()) != 0) {
                replenishD.setGsrdSendPrice(replenishDetail.getSendPrice());
                replenishD.setGsrdNeedQty(replenishDetail.getRequestQuantity().toString());
                //更新补货明细的补货数量和配送价格
                gaiaSdReplenishDMapper.updateReplenishD(replenishD);
            }
        }
    }

    private void saveReplenishToThirdParty(OrderOperateForm orderOperateForm, GaiaSdReplenishH replenishH, List<GaiaSdReplenishD> detailList) {
        GaiaSdWtbhdH wtbhdH = new GaiaSdWtbhdH();
        fillGaisSdWtbhdH(replenishH, wtbhdH);
        wtbhdH = gaiaSdWtbhdHService.add(wtbhdH);
        for (GaiaSdReplenishD replenishD : detailList) {
            GaiaSdWtbhdD wtbhdD = new GaiaSdWtbhdD();
            wtbhdD.setClient(replenishD.getClientId());
            wtbhdD.setGswbdVoucherId(replenishD.getGsrdVoucherId());
            wtbhdD.setPid(wtbhdH.getId());
            wtbhdD.setGswbdDate(replenishD.getGsrdDate());
            wtbhdD.setGswbdSerial(replenishD.getGsrdSerial());
            wtbhdD.setGswbdProId(replenishD.getGsrdProId());
            wtbhdD.setGswbdBrId(replenishD.getGsrdBrId());
            wtbhdD.setGswbdBatch(replenishD.getGsrdBatch());
            wtbhdD.setGswbdBatchNo(replenishD.getGsrdBatchNo());
            wtbhdD.setGswbdGrossMargin(replenishD.getGsrdGrossMargin());
            wtbhdD.setGswbdSalesDays1(StringUtil.isEmpty(replenishD.getGsrdSalesDays1()) ? BigDecimal.ZERO : new BigDecimal(replenishD.getGsrdSalesDays1()));
            wtbhdD.setGswbdSalesDays2(StringUtil.isEmpty(replenishD.getGsrdSalesDays2()) ? BigDecimal.ZERO : new BigDecimal(replenishD.getGsrdSalesDays2()));
            wtbhdD.setGswbdProposeQty(StringUtil.isEmpty(replenishD.getGsrdProposeQty()) ? BigDecimal.ZERO : new BigDecimal(replenishD.getGsrdProposeQty()));
            wtbhdD.setGswbdStockDepot(replenishD.getGsrdStockDepot());
            wtbhdD.setGswbdStockStore(replenishD.getGsrdStockStore());
            wtbhdD.setGswbdDisplayMin(replenishD.getGsrdDisplayMin());
            wtbhdD.setGswbdPackMidsize(replenishD.getGsrdPackMidsize());
            wtbhdD.setGswbdNeedQty(StringUtil.isEmpty(replenishD.getGsrdNeedQty()) ? BigDecimal.ZERO : new BigDecimal(replenishD.getGsrdNeedQty()));
            wtbhdD.setGswbdVoucherAmt(replenishD.getGsrdSendPrice());
            wtbhdD.setGswbdDnQty(wtbhdD.getGswbdNeedQty());
            wtbhdD.setGsspPriceNormal(replenishD.getGsrdProPrice());
            wtbhdD.setGswbdBz(orderOperateForm.getRemarks());
            gaiaSdWtbhdDService.add(wtbhdD);
        }
    }

    private void fillGaisSdWtbhdH(GaiaSdReplenishH replenishH, GaiaSdWtbhdH wtbhdH) {
        wtbhdH.setClient(replenishH.getClientId());
        wtbhdH.setGswbhBrId(replenishH.getGsrhBrId());
        wtbhdH.setGswbhVoucherId(replenishH.getGsrhVoucherId());
        wtbhdH.setGswbhDate(replenishH.getGsrhDate());
        wtbhdH.setGswbhType(replenishH.getGsrhType());
        wtbhdH.setGswbhAddr(replenishH.getGsrhAddr());
        wtbhdH.setGswbhTotalAmt(replenishH.getGsrhTotalAmt());
        wtbhdH.setGswbhTotalQty(new BigDecimal(replenishH.getGsrhTotalQty()));
        wtbhdH.setGswbhEmp(replenishH.getGsrhEmp());
        wtbhdH.setGswbhStatus(replenishH.getGsrhStatus());
        wtbhdH.setGswbhFlag(replenishH.getGsrhFlag());
        wtbhdH.setGswbhPoid(replenishH.getGsrhPoid());
        wtbhdH.setGswbhPattern(replenishH.getGsrhPattern());
        wtbhdH.setGswbhDnBy(replenishH.getGsrhDnBy());
        wtbhdH.setGswbhDnFlag("1");
        wtbhdH.setGswbhDnDate(replenishH.getGsrhDnDate());
        wtbhdH.setGswbhDnTime(replenishH.getGsrhDnTime());
        wtbhdH.setGswbhCreTime(CommonUtil.getHHmmss());
        wtbhdH.setGswbhCheckDate(CommonUtil.getyyyyMMdd());
        wtbhdH.setGswbhSource(replenishH.getGsrhSource());
    }
}
