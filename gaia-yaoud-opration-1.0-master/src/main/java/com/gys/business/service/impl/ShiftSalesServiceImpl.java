//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdSaleHMapper;
import com.gys.business.service.ShiftSalesService;
import com.gys.business.service.data.ShiftSalesOutData;
import com.gys.business.service.data.shiftSalesInData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShiftSalesServiceImpl implements ShiftSalesService {
    @Autowired
    private GaiaSdSaleHMapper saleHMapper;

    public ShiftSalesServiceImpl() {
    }

    public List<ShiftSalesOutData> getShiftSalesList(shiftSalesInData inData) {
        List<ShiftSalesOutData> outData = this.saleHMapper.getShiftSalesList(inData);

        if (ObjectUtil.isNotEmpty(outData) && outData.size() > 0) {
            for(int i = 0; i < outData.size(); ++i) {
                ((ShiftSalesOutData)outData.get(i)).setIndex(i + 1);
            }
        }

        return outData;
    }
}
