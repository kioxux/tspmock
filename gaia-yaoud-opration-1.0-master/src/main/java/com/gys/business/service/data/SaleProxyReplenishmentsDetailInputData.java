package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SaleProxyReplenishmentsDetailInputData {
    private String commodityCode;
    private String batchNo;
    private String expiryDate;
    private BigDecimal costPrice;
    private BigDecimal quantity;
}
