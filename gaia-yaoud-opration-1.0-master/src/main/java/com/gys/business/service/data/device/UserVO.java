package com.gys.business.service.data.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/24 17:29
 **/
@Data
@ApiModel
public class UserVO {
    @ApiModelProperty(value = "姓名", example = "贾老板")
    private String name;
}
