//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class CashPaymentOutData {
    private String clientId;
    private String gsdhVoucherId;
    private String gsdhBrId;
    private String gsdhCheckDate;
    private String gsdhDepositId;
    private String gsdhBankId;
    private String gsdhBankAccount;
    private String gsdhDepositDate;
    private String gsdhDepositAmt;
    private String gsdhStatus;
    private List<CashPaymentDetailOutData> detailOutDataList;
    private Integer index;

    public CashPaymentOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsdhVoucherId() {
        return this.gsdhVoucherId;
    }

    public String getGsdhBrId() {
        return this.gsdhBrId;
    }

    public String getGsdhCheckDate() {
        return this.gsdhCheckDate;
    }

    public String getGsdhDepositId() {
        return this.gsdhDepositId;
    }

    public String getGsdhBankId() {
        return this.gsdhBankId;
    }

    public String getGsdhBankAccount() {
        return this.gsdhBankAccount;
    }

    public String getGsdhDepositDate() {
        return this.gsdhDepositDate;
    }

    public String getGsdhDepositAmt() {
        return this.gsdhDepositAmt;
    }

    public String getGsdhStatus() {
        return this.gsdhStatus;
    }

    public List<CashPaymentDetailOutData> getDetailOutDataList() {
        return this.detailOutDataList;
    }

    public Integer getIndex() {
        return this.index;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsdhVoucherId(final String gsdhVoucherId) {
        this.gsdhVoucherId = gsdhVoucherId;
    }

    public void setGsdhBrId(final String gsdhBrId) {
        this.gsdhBrId = gsdhBrId;
    }

    public void setGsdhCheckDate(final String gsdhCheckDate) {
        this.gsdhCheckDate = gsdhCheckDate;
    }

    public void setGsdhDepositId(final String gsdhDepositId) {
        this.gsdhDepositId = gsdhDepositId;
    }

    public void setGsdhBankId(final String gsdhBankId) {
        this.gsdhBankId = gsdhBankId;
    }

    public void setGsdhBankAccount(final String gsdhBankAccount) {
        this.gsdhBankAccount = gsdhBankAccount;
    }

    public void setGsdhDepositDate(final String gsdhDepositDate) {
        this.gsdhDepositDate = gsdhDepositDate;
    }

    public void setGsdhDepositAmt(final String gsdhDepositAmt) {
        this.gsdhDepositAmt = gsdhDepositAmt;
    }

    public void setGsdhStatus(final String gsdhStatus) {
        this.gsdhStatus = gsdhStatus;
    }

    public void setDetailOutDataList(final List<CashPaymentDetailOutData> detailOutDataList) {
        this.detailOutDataList = detailOutDataList;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof CashPaymentOutData)) {
            return false;
        } else {
            CashPaymentOutData other = (CashPaymentOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gsdhVoucherId = this.getGsdhVoucherId();
                Object other$gsdhVoucherId = other.getGsdhVoucherId();
                if (this$gsdhVoucherId == null) {
                    if (other$gsdhVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsdhVoucherId.equals(other$gsdhVoucherId)) {
                    return false;
                }

                Object this$gsdhBrId = this.getGsdhBrId();
                Object other$gsdhBrId = other.getGsdhBrId();
                if (this$gsdhBrId == null) {
                    if (other$gsdhBrId != null) {
                        return false;
                    }
                } else if (!this$gsdhBrId.equals(other$gsdhBrId)) {
                    return false;
                }

                label134: {
                    Object this$gsdhCheckDate = this.getGsdhCheckDate();
                    Object other$gsdhCheckDate = other.getGsdhCheckDate();
                    if (this$gsdhCheckDate == null) {
                        if (other$gsdhCheckDate == null) {
                            break label134;
                        }
                    } else if (this$gsdhCheckDate.equals(other$gsdhCheckDate)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gsdhDepositId = this.getGsdhDepositId();
                    Object other$gsdhDepositId = other.getGsdhDepositId();
                    if (this$gsdhDepositId == null) {
                        if (other$gsdhDepositId == null) {
                            break label127;
                        }
                    } else if (this$gsdhDepositId.equals(other$gsdhDepositId)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$gsdhBankId = this.getGsdhBankId();
                    Object other$gsdhBankId = other.getGsdhBankId();
                    if (this$gsdhBankId == null) {
                        if (other$gsdhBankId == null) {
                            break label120;
                        }
                    } else if (this$gsdhBankId.equals(other$gsdhBankId)) {
                        break label120;
                    }

                    return false;
                }

                Object this$gsdhBankAccount = this.getGsdhBankAccount();
                Object other$gsdhBankAccount = other.getGsdhBankAccount();
                if (this$gsdhBankAccount == null) {
                    if (other$gsdhBankAccount != null) {
                        return false;
                    }
                } else if (!this$gsdhBankAccount.equals(other$gsdhBankAccount)) {
                    return false;
                }

                label106: {
                    Object this$gsdhDepositDate = this.getGsdhDepositDate();
                    Object other$gsdhDepositDate = other.getGsdhDepositDate();
                    if (this$gsdhDepositDate == null) {
                        if (other$gsdhDepositDate == null) {
                            break label106;
                        }
                    } else if (this$gsdhDepositDate.equals(other$gsdhDepositDate)) {
                        break label106;
                    }

                    return false;
                }

                Object this$gsdhDepositAmt = this.getGsdhDepositAmt();
                Object other$gsdhDepositAmt = other.getGsdhDepositAmt();
                if (this$gsdhDepositAmt == null) {
                    if (other$gsdhDepositAmt != null) {
                        return false;
                    }
                } else if (!this$gsdhDepositAmt.equals(other$gsdhDepositAmt)) {
                    return false;
                }

                label92: {
                    Object this$gsdhStatus = this.getGsdhStatus();
                    Object other$gsdhStatus = other.getGsdhStatus();
                    if (this$gsdhStatus == null) {
                        if (other$gsdhStatus == null) {
                            break label92;
                        }
                    } else if (this$gsdhStatus.equals(other$gsdhStatus)) {
                        break label92;
                    }

                    return false;
                }

                Object this$detailOutDataList = this.getDetailOutDataList();
                Object other$detailOutDataList = other.getDetailOutDataList();
                if (this$detailOutDataList == null) {
                    if (other$detailOutDataList != null) {
                        return false;
                    }
                } else if (!this$detailOutDataList.equals(other$detailOutDataList)) {
                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CashPaymentOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsdhVoucherId = this.getGsdhVoucherId();
        result = result * 59 + ($gsdhVoucherId == null ? 43 : $gsdhVoucherId.hashCode());
        Object $gsdhBrId = this.getGsdhBrId();
        result = result * 59 + ($gsdhBrId == null ? 43 : $gsdhBrId.hashCode());
        Object $gsdhCheckDate = this.getGsdhCheckDate();
        result = result * 59 + ($gsdhCheckDate == null ? 43 : $gsdhCheckDate.hashCode());
        Object $gsdhDepositId = this.getGsdhDepositId();
        result = result * 59 + ($gsdhDepositId == null ? 43 : $gsdhDepositId.hashCode());
        Object $gsdhBankId = this.getGsdhBankId();
        result = result * 59 + ($gsdhBankId == null ? 43 : $gsdhBankId.hashCode());
        Object $gsdhBankAccount = this.getGsdhBankAccount();
        result = result * 59 + ($gsdhBankAccount == null ? 43 : $gsdhBankAccount.hashCode());
        Object $gsdhDepositDate = this.getGsdhDepositDate();
        result = result * 59 + ($gsdhDepositDate == null ? 43 : $gsdhDepositDate.hashCode());
        Object $gsdhDepositAmt = this.getGsdhDepositAmt();
        result = result * 59 + ($gsdhDepositAmt == null ? 43 : $gsdhDepositAmt.hashCode());
        Object $gsdhStatus = this.getGsdhStatus();
        result = result * 59 + ($gsdhStatus == null ? 43 : $gsdhStatus.hashCode());
        Object $detailOutDataList = this.getDetailOutDataList();
        result = result * 59 + ($detailOutDataList == null ? 43 : $detailOutDataList.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        return result;
    }

    public String toString() {
        return "CashPaymentOutData(clientId=" + this.getClientId() + ", gsdhVoucherId=" + this.getGsdhVoucherId() + ", gsdhBrId=" + this.getGsdhBrId() + ", gsdhCheckDate=" + this.getGsdhCheckDate() + ", gsdhDepositId=" + this.getGsdhDepositId() + ", gsdhBankId=" + this.getGsdhBankId() + ", gsdhBankAccount=" + this.getGsdhBankAccount() + ", gsdhDepositDate=" + this.getGsdhDepositDate() + ", gsdhDepositAmt=" + this.getGsdhDepositAmt() + ", gsdhStatus=" + this.getGsdhStatus() + ", detailOutDataList=" + this.getDetailOutDataList() + ", index=" + this.getIndex() + ")";
    }
}
