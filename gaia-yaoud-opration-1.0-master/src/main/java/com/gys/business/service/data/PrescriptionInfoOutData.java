//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PrescriptionInfoOutData {
    private String clientId;
    private String num;
    private String reviewerDate;
    private String reviewerTime;
    private String pharmacistNum;
    private String pharmacistName;
    private String uploadStore;
    private String billNo;
    private String patientName;
    private String patientSex;
    private String patientAge;
    private String status;
    private String auditConclusion;
    private String prescriptionPhoto;

    public PrescriptionInfoOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getNum() {
        return this.num;
    }

    public String getReviewerDate() {
        return this.reviewerDate;
    }

    public String getReviewerTime() {
        return this.reviewerTime;
    }

    public String getPharmacistNum() {
        return this.pharmacistNum;
    }

    public String getPharmacistName() {
        return this.pharmacistName;
    }

    public String getUploadStore() {
        return this.uploadStore;
    }

    public String getBillNo() {
        return this.billNo;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public String getPatientSex() {
        return this.patientSex;
    }

    public String getPatientAge() {
        return this.patientAge;
    }

    public String getStatus() {
        return this.status;
    }

    public String getAuditConclusion() {
        return this.auditConclusion;
    }

    public String getPrescriptionPhoto() {
        return this.prescriptionPhoto;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setNum(final String num) {
        this.num = num;
    }

    public void setReviewerDate(final String reviewerDate) {
        this.reviewerDate = reviewerDate;
    }

    public void setReviewerTime(final String reviewerTime) {
        this.reviewerTime = reviewerTime;
    }

    public void setPharmacistNum(final String pharmacistNum) {
        this.pharmacistNum = pharmacistNum;
    }

    public void setPharmacistName(final String pharmacistName) {
        this.pharmacistName = pharmacistName;
    }

    public void setUploadStore(final String uploadStore) {
        this.uploadStore = uploadStore;
    }

    public void setBillNo(final String billNo) {
        this.billNo = billNo;
    }

    public void setPatientName(final String patientName) {
        this.patientName = patientName;
    }

    public void setPatientSex(final String patientSex) {
        this.patientSex = patientSex;
    }

    public void setPatientAge(final String patientAge) {
        this.patientAge = patientAge;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public void setAuditConclusion(final String auditConclusion) {
        this.auditConclusion = auditConclusion;
    }

    public void setPrescriptionPhoto(final String prescriptionPhoto) {
        this.prescriptionPhoto = prescriptionPhoto;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PrescriptionInfoOutData)) {
            return false;
        } else {
            PrescriptionInfoOutData other = (PrescriptionInfoOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$num = this.getNum();
                Object other$num = other.getNum();
                if (this$num == null) {
                    if (other$num != null) {
                        return false;
                    }
                } else if (!this$num.equals(other$num)) {
                    return false;
                }

                Object this$reviewerDate = this.getReviewerDate();
                Object other$reviewerDate = other.getReviewerDate();
                if (this$reviewerDate == null) {
                    if (other$reviewerDate != null) {
                        return false;
                    }
                } else if (!this$reviewerDate.equals(other$reviewerDate)) {
                    return false;
                }

                label158: {
                    Object this$reviewerTime = this.getReviewerTime();
                    Object other$reviewerTime = other.getReviewerTime();
                    if (this$reviewerTime == null) {
                        if (other$reviewerTime == null) {
                            break label158;
                        }
                    } else if (this$reviewerTime.equals(other$reviewerTime)) {
                        break label158;
                    }

                    return false;
                }

                label151: {
                    Object this$pharmacistNum = this.getPharmacistNum();
                    Object other$pharmacistNum = other.getPharmacistNum();
                    if (this$pharmacistNum == null) {
                        if (other$pharmacistNum == null) {
                            break label151;
                        }
                    } else if (this$pharmacistNum.equals(other$pharmacistNum)) {
                        break label151;
                    }

                    return false;
                }

                Object this$pharmacistName = this.getPharmacistName();
                Object other$pharmacistName = other.getPharmacistName();
                if (this$pharmacistName == null) {
                    if (other$pharmacistName != null) {
                        return false;
                    }
                } else if (!this$pharmacistName.equals(other$pharmacistName)) {
                    return false;
                }

                label137: {
                    Object this$uploadStore = this.getUploadStore();
                    Object other$uploadStore = other.getUploadStore();
                    if (this$uploadStore == null) {
                        if (other$uploadStore == null) {
                            break label137;
                        }
                    } else if (this$uploadStore.equals(other$uploadStore)) {
                        break label137;
                    }

                    return false;
                }

                label130: {
                    Object this$billNo = this.getBillNo();
                    Object other$billNo = other.getBillNo();
                    if (this$billNo == null) {
                        if (other$billNo == null) {
                            break label130;
                        }
                    } else if (this$billNo.equals(other$billNo)) {
                        break label130;
                    }

                    return false;
                }

                Object this$patientName = this.getPatientName();
                Object other$patientName = other.getPatientName();
                if (this$patientName == null) {
                    if (other$patientName != null) {
                        return false;
                    }
                } else if (!this$patientName.equals(other$patientName)) {
                    return false;
                }

                Object this$patientSex = this.getPatientSex();
                Object other$patientSex = other.getPatientSex();
                if (this$patientSex == null) {
                    if (other$patientSex != null) {
                        return false;
                    }
                } else if (!this$patientSex.equals(other$patientSex)) {
                    return false;
                }

                label109: {
                    Object this$patientAge = this.getPatientAge();
                    Object other$patientAge = other.getPatientAge();
                    if (this$patientAge == null) {
                        if (other$patientAge == null) {
                            break label109;
                        }
                    } else if (this$patientAge.equals(other$patientAge)) {
                        break label109;
                    }

                    return false;
                }

                label102: {
                    Object this$status = this.getStatus();
                    Object other$status = other.getStatus();
                    if (this$status == null) {
                        if (other$status == null) {
                            break label102;
                        }
                    } else if (this$status.equals(other$status)) {
                        break label102;
                    }

                    return false;
                }

                Object this$auditConclusion = this.getAuditConclusion();
                Object other$auditConclusion = other.getAuditConclusion();
                if (this$auditConclusion == null) {
                    if (other$auditConclusion != null) {
                        return false;
                    }
                } else if (!this$auditConclusion.equals(other$auditConclusion)) {
                    return false;
                }

                Object this$prescriptionPhoto = this.getPrescriptionPhoto();
                Object other$prescriptionPhoto = other.getPrescriptionPhoto();
                if (this$prescriptionPhoto == null) {
                    if (other$prescriptionPhoto != null) {
                        return false;
                    }
                } else if (!this$prescriptionPhoto.equals(other$prescriptionPhoto)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PrescriptionInfoOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $num = this.getNum();
        result = result * 59 + ($num == null ? 43 : $num.hashCode());
        Object $reviewerDate = this.getReviewerDate();
        result = result * 59 + ($reviewerDate == null ? 43 : $reviewerDate.hashCode());
        Object $reviewerTime = this.getReviewerTime();
        result = result * 59 + ($reviewerTime == null ? 43 : $reviewerTime.hashCode());
        Object $pharmacistNum = this.getPharmacistNum();
        result = result * 59 + ($pharmacistNum == null ? 43 : $pharmacistNum.hashCode());
        Object $pharmacistName = this.getPharmacistName();
        result = result * 59 + ($pharmacistName == null ? 43 : $pharmacistName.hashCode());
        Object $uploadStore = this.getUploadStore();
        result = result * 59 + ($uploadStore == null ? 43 : $uploadStore.hashCode());
        Object $billNo = this.getBillNo();
        result = result * 59 + ($billNo == null ? 43 : $billNo.hashCode());
        Object $patientName = this.getPatientName();
        result = result * 59 + ($patientName == null ? 43 : $patientName.hashCode());
        Object $patientSex = this.getPatientSex();
        result = result * 59 + ($patientSex == null ? 43 : $patientSex.hashCode());
        Object $patientAge = this.getPatientAge();
        result = result * 59 + ($patientAge == null ? 43 : $patientAge.hashCode());
        Object $status = this.getStatus();
        result = result * 59 + ($status == null ? 43 : $status.hashCode());
        Object $auditConclusion = this.getAuditConclusion();
        result = result * 59 + ($auditConclusion == null ? 43 : $auditConclusion.hashCode());
        Object $prescriptionPhoto = this.getPrescriptionPhoto();
        result = result * 59 + ($prescriptionPhoto == null ? 43 : $prescriptionPhoto.hashCode());
        return result;
    }

    public String toString() {
        return "PrescriptionInfoOutData(clientId=" + this.getClientId() + ", num=" + this.getNum() + ", reviewerDate=" + this.getReviewerDate() + ", reviewerTime=" + this.getReviewerTime() + ", pharmacistNum=" + this.getPharmacistNum() + ", pharmacistName=" + this.getPharmacistName() + ", uploadStore=" + this.getUploadStore() + ", billNo=" + this.getBillNo() + ", patientName=" + this.getPatientName() + ", patientSex=" + this.getPatientSex() + ", patientAge=" + this.getPatientAge() + ", status=" + this.getStatus() + ", auditConclusion=" + this.getAuditConclusion() + ", prescriptionPhoto=" + this.getPrescriptionPhoto() + ")";
    }
}
