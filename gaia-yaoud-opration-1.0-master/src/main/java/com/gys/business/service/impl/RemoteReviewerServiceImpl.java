//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaSdRemoteAuditDMapper;
import com.gys.business.mapper.GaiaSdRemoteAuditHMapper;
import com.gys.business.mapper.GaiaSdSaleRecipelMapper;
import com.gys.business.mapper.entity.GaiaSdRemoteAuditH;
import com.gys.business.mapper.entity.GaiaSdSaleRecipel;
import com.gys.business.service.RemoteReviewerService;
import com.gys.business.service.data.GetCosParaOutData;
import com.gys.business.service.data.PrescriptionDetailInfoOutData;
import com.gys.business.service.data.PrescriptionInfoExportOutData;
import com.gys.business.service.data.PrescriptionInfoInData;
import com.gys.business.service.data.PrescriptionInfoOutData;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.CosUtil;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RemoteReviewerServiceImpl implements RemoteReviewerService {
    @Autowired
    private GaiaSdRemoteAuditHMapper sdRemoteAuditHMapper;
    @Autowired
    private GaiaSdRemoteAuditDMapper sdRemoteAuditDMapper;
    @Autowired
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;
    @Autowired
    private com.gys.business.service.impl.ThirdParaServiceImpl thirdParaService;

    public RemoteReviewerServiceImpl() {
    }

    public List<PrescriptionInfoOutData> getPrescriptionList(PrescriptionInfoInData inData) {
        List<PrescriptionInfoOutData> outData = this.sdRemoteAuditHMapper.getPrescriptionList(inData);
        return outData;
    }

    public List<PrescriptionDetailInfoOutData> getPrescriptionDetailList(PrescriptionInfoInData inData) {
        List<PrescriptionDetailInfoOutData> outData = this.sdRemoteAuditDMapper.getPrescriptionDetailList(inData);
        return outData;
    }

    @Transactional
    public void approve(PrescriptionInfoInData inData, GetLoginOutData userInfo) {
        GaiaSdRemoteAuditH remoteAuditH = new GaiaSdRemoteAuditH();
        remoteAuditH.setClientId(userInfo.getClient());
        remoteAuditH.setGsrahVoucherId(inData.getNum());
        GaiaSdRemoteAuditH auditH = (GaiaSdRemoteAuditH)this.sdRemoteAuditHMapper.selectByPrimaryKey(remoteAuditH);
        int ifSaleRecipel = 0;
        if (ObjectUtil.isNotNull(auditH)) {
            if (!"0".equals(auditH.getGsrahStatus())) {
                throw new BusinessException("存在已审核数据");
            }

            auditH.setGsrahStatus(inData.getStatus());
            if (StrUtil.isNotBlank(inData.getPharmacistName())) {
                auditH.setGsrahPharmacistName(inData.getPharmacistName());
            }

            if (StrUtil.isNotBlank(inData.getPharmacistNum())) {
                auditH.setGsrahPharmacistId(inData.getPharmacistNum());
            }

            if (StrUtil.isNotBlank(inData.getAuditConclusion())) {
                auditH.setGsrahResult(inData.getAuditConclusion());
            }

            auditH.setGsrahDate(DateUtil.format(new Date(), "yyyyMMdd"));
            auditH.setGsrahTime(DateUtil.format(new Date(), "HHmmss"));
            ifSaleRecipel = this.sdRemoteAuditHMapper.updateByPrimaryKeySelective(auditH);
        }

        if (ifSaleRecipel > 0) {
            GaiaSdSaleRecipel gaiaSdSaleRecipel = new GaiaSdSaleRecipel();
            gaiaSdSaleRecipel.setClientId(auditH.getClientId());
            gaiaSdSaleRecipel.setGssrVoucherId(auditH.getGsrahVoucherId());
            gaiaSdSaleRecipel.setGssrType("2");
            gaiaSdSaleRecipel.setGssrBrId(auditH.getGsrahBrId());
            gaiaSdSaleRecipel.setGssrBrName(auditH.getGsrahBrName());
            gaiaSdSaleRecipel.setGssrDate(auditH.getGsrahUploadDate());
            gaiaSdSaleRecipel.setGssrTime(auditH.getGsrahUploadTime());
            gaiaSdSaleRecipel.setGssrEmp(auditH.getGsrahUploadEmp());
            gaiaSdSaleRecipel.setGssrUploadFlag(auditH.getGsrahUploadFlag());
            gaiaSdSaleRecipel.setGssrSaleBrId(auditH.getGsrahBrId());
            gaiaSdSaleRecipel.setGssrSaleDate("");
            gaiaSdSaleRecipel.setGssrSaleBillNo(auditH.getGsrahBillNo());
            gaiaSdSaleRecipel.setGssrCustName(auditH.getGsrahCustName());
            gaiaSdSaleRecipel.setGssrCustSex(auditH.getGsrahCustSex());
            gaiaSdSaleRecipel.setGssrCustAge(auditH.getGsrahCustAge());
            gaiaSdSaleRecipel.setGssrCustIdcard(auditH.getGsrahCustIdcard());
            gaiaSdSaleRecipel.setGssrCustMobile(auditH.getGsrahCustMobile());
            gaiaSdSaleRecipel.setGssrPharmacistId(auditH.getGsrahPharmacistId());
            gaiaSdSaleRecipel.setGssrPharmacistName(auditH.getGsrahPharmacistName());
            gaiaSdSaleRecipel.setGssrCheckDate(auditH.getGsrahDate());
            gaiaSdSaleRecipel.setGssrCheckTime(auditH.getGsrahTime());
            gaiaSdSaleRecipel.setGssrCheckStatus(auditH.getGsrahStatus());
            gaiaSdSaleRecipel.setGssrProId("");
            gaiaSdSaleRecipel.setGssrBatchNo("");
            gaiaSdSaleRecipel.setGssrQty("");
            gaiaSdSaleRecipel.setGssrRecipelId("");
            gaiaSdSaleRecipel.setGssrRecipelHospital("");
            gaiaSdSaleRecipel.setGssrRecipelDepartment("");
            gaiaSdSaleRecipel.setGssrRecipelDoctor("");
            gaiaSdSaleRecipel.setGssrRecipelPicAddress(auditH.getGsrahRecipelPicAddress());
            this.gaiaSdSaleRecipelMapper.insert(gaiaSdSaleRecipel);
        }

    }

    public void exportOut(HttpServletResponse response, PrescriptionInfoInData inData) throws Exception {
        List<PrescriptionInfoOutData> outDataList = this.sdRemoteAuditHMapper.getPrescriptionList(inData);
        List<PrescriptionInfoExportOutData> list = new ArrayList();
        if (outDataList.size() > 0) {
            Iterator var5 = outDataList.iterator();

            while(var5.hasNext()) {
                PrescriptionInfoOutData outData = (PrescriptionInfoOutData)var5.next();
                PrescriptionInfoExportOutData exportOutData = new PrescriptionInfoExportOutData();
                exportOutData.setNum(outData.getNum());
                exportOutData.setReviewerDate(outData.getReviewerDate());
                exportOutData.setReviewerTime(outData.getReviewerTime());
                exportOutData.setPharmacistNum(outData.getPharmacistNum());
                exportOutData.setPharmacistName(outData.getPharmacistName());
                if (outData.getStatus().equals("1")) {
                    exportOutData.setStatus("审核通过");
                } else {
                    exportOutData.setStatus("审核不通过");
                }

                exportOutData.setAuditConclusion(outData.getAuditConclusion());
                exportOutData.setUploadStore(outData.getUploadStore());
                exportOutData.setBillNo(outData.getBillNo());
                exportOutData.setSaleDate(outData.getReviewerDate());
                exportOutData.setPatientName(outData.getPatientName());
                if (outData.getPatientSex().equals("1")) {
                    exportOutData.setPatientSex("男");
                } else {
                    exportOutData.setPatientSex("女");
                }

                exportOutData.setPatientAge(outData.getPatientAge());
                exportOutData.setPrescriptionAddress(outData.getPrescriptionPhoto());
                list.add(exportOutData);
            }
        }

        String title = "审方记录";
        String[] headers1 = new String[]{"审方单号", "审方日期", "审方时间", "药师编号", "药师姓名", "审方状态", "审核结论"};
        String[] headers2 = new String[]{"上传门店", "销售单号", "销售日期", "患者姓名", "患者性别", "患者年龄", "处方地址"};
        ExportExcel<PrescriptionInfoExportOutData> excelUtil = new ExportExcel();
        response.setContentType("application/vnd.ms-excel; charset=utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + new String(title.getBytes(), "iso-8859-1") + ".xls");
        //chenhao TODO
        //response.setCharacterEncoding("UTF-8");
        OutputStream outputStream = response.getOutputStream();
        excelUtil.exportExcel2(title, headers1, headers2, list, outputStream, (String)null);
    }

    public String queryPhoto(PrescriptionInfoInData inData) {
        GetCosParaOutData paraOutData = this.thirdParaService.getCosPara();
        return CosUtil.getInstance(paraOutData.getSecretId(), paraOutData.getSecretKey(), paraOutData.getRegionName(), paraOutData.getBucketName()).getFileUrl(inData.getPhotoUrl());
    }
}
