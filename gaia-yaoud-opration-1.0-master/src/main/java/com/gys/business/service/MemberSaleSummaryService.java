package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.JsonResult;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface MemberSaleSummaryService {
    List<GetMemberOutData> findMembersByPage(GetMemberInData inData);

    /**
     * 根据时间日期查询会员销售汇总
     * @param inData
     * @return
     */
    List<MemberSaleSummaryOutData> memberSalesSummary(MemberSaleSummaryInData inData);

    Map<String,Object> queryMemberCardList(GetSalesSummaryOfSalesmenReportInData inData);

    List<Map<String,Object>> selectHykList(Map<String,Object> inData);

    /**
     * 获取当前加盟商下的 会员卡的类型
     * @param client
     * @return
     */
    List<MemberShipTypeData> membershipCardType(String client);

    /**
     * 会员消费汇总表
     * @param inData
     * @return
     */
    Map<String,Object> selectMemberCollectList(MemberSalesInData inData);

    /**
     * 会员消费商品明细表
     * @param inData
     * @return
     */
    Map<String,Object> selectMenberDetailList(MemberSalesInData inData);

    //查询条件：门店编码
    List<MemberSalesOutData> findStoreId(MemberSalesInData inData);

    //查询条件：销售等级
    List<MemberSaleClass> findSaleClass(MemberSalesInData inData);

    //查询条件：商品定位
    List<MemberSalePosition> findSalePosition(MemberSalesInData inData);

    //查询条件：商品自分类
    List<MemberProClass> findProClass(MemberSalesInData inData);

    //查询条件：自定义1
    List<MemberZDY1> findZDY1(MemberSalesInData inData);

    //查询条件：自定义2
    List<MemberZDY2> findZDY2(MemberSalesInData inData);

    //查询条件：自定义3
    List<MemberZDY3> findZDY3(MemberSalesInData inData);

    //查询条件：自定义4
    List<MemberZDY4> findZDY4(MemberSalesInData inData);

    //查询条件：自定义5
    List<MemberZDY5> findZDY5(MemberSalesInData inData);


    //搜索条件
   JsonResult searchBox(MemberSalesInData inData);
    /**
     * 会员消费汇总表查询导出
     * @param inData
     * @return
     */
     List<LinkedHashMap<String, Object>> selectMemberListCSV(MemberSalesInData inData);

    /**
     * 会员消费汇总表查询导出(门店)
     * @param inData
     * @return
     */
    List<LinkedHashMap<String, Object>> selectMemberStoListCSV(MemberSalesInData inData);

    /**
     * 会员消费商品明细表导出
     * @param inData
     * @return
     */
    List<LinkedHashMap<String, Object>> selectMemberProCSV(MemberSalesInData inData);

    /**
     * 会员消费商品明细表导出（门店）
     * @param inData
     * @return
     */
    List<LinkedHashMap<String, Object>> selectMemberProStoCSV(MemberSalesInData inData);
}
