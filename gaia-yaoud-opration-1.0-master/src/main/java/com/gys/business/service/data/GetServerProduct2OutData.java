package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetServerProduct2OutData implements Serializable {
    private static final long serialVersionUID = -7019573760075630830L;

    @ApiModelProperty(value = "序号")
    private Integer index2;

    @ApiModelProperty(value = "批号库存数量")
    private String gssbQty2;

    @ApiModelProperty(value = "总库存数量")
    private String gssQty2;

    @ApiModelProperty(value = "商品编码")
    private String proCode2;

    @ApiModelProperty(value = "商品名称")
    private String proName2;

    @ApiModelProperty(value = "商品通用名")
    private String proCommonName2;

    @ApiModelProperty(value = "商品价格")
    private String proAmount2;

    @ApiModelProperty(value = "规格")
    private String proSpecs2;

    @ApiModelProperty(value = "厂家名称")
    private String proFactoryName2;

    @ApiModelProperty(value = "产地")
    private String proPlace2;

    @ApiModelProperty(value = "剂型")
    private String proForm2;

    private String proBarcode2;
    private String proRegisterNo2;

    @ApiModelProperty(value = " 处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方")
    private String proPresclass2;

    @ApiModelProperty(value = "成分分类")
    private String proCompclass2;
    /**
     * 有效期
     */
    @ApiModelProperty(value = "有效期")
    private String vaildDate2;

    @ApiModelProperty(value = "计量单位")
    private String proUnit2;

    @ApiModelProperty(value = "批号")
    private String batchNo2;

    @ApiModelProperty(value = "批次")
    private String batch2;

    @ApiModelProperty(value = "税率值")
    private String movTax2;

    @ApiModelProperty(value = "批次表库存数量")
    private String proStock2;


    private String priority1;
    private String priority3;

    @ApiModelProperty(value = "商品定位")
    private String proPosition2;
    /**
     * 含税毛利额
     */
    @ApiModelProperty(value = "含税毛利额")
    private String grossProfit2;

    /**
     * 医保刷卡数量
     */
    @ApiModelProperty(value = "含税毛利额")
    private String proZdy1;
    @ApiModelProperty(value = "等级")
    private String saleClass;
    /**
     * 货位号
     */
    @ApiModelProperty(value = "货位号")
    private String huowei;

    /**
     * 是否医保
     */
    @ApiModelProperty(value = "是否医保")
    private String proIfMed;

    @ApiModelProperty(value = "用法用量")
    private String proUsage2;

    @ApiModelProperty(value = "禁忌说明")
    private String proContraindication2;

    @ApiModelProperty(value = "提示")
    private String gsstExplain52;

    @ApiModelProperty(value = "禁忌")
    private String gsscExplain52;

    @ApiModelProperty(value = "商品会员信息")
    private GetPromByProCodeOutData prom;

    @ApiModelProperty(value = "商品促销信息")
    private List<PromotionalConditionData> conditionList2;

    @ApiModelProperty(value = "图片路径")
    private String imageUrl2 = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201609%2F24%2F20160924025917_XKWzG.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1617861328&t=5f4002e61120b4475898362f57380857";

}
