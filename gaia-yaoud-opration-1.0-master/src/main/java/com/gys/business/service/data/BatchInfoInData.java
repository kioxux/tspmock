package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BatchInfoInData {
    @ApiModelProperty(value = "加盟商", required = true)
    private String client;

    @ApiModelProperty(value = "商品编码", required = true)
    private String batProCode;

    @ApiModelProperty(value = "地点", required = true)
    private String proSite;

    @ApiModelProperty(value = "采购订单号", required = true)
    private String batPoId;

    @ApiModelProperty(value = "采购订单行号", required = true)
    private String batPoLineno;

    @ApiModelProperty(value = "生产日期", required = true)
    private String batProductDate;

    @ApiModelProperty(value = "有效期至", required = true)
    private String batExpiryDate;

    @ApiModelProperty(value = "供应商编码", required = true)
    private String batSupplierCode;

    @ApiModelProperty(value = "采购单价", required = true)
    private String batPoPrice;

    @ApiModelProperty(value = "入库数量", required = true)
    private String batReceiveQty;

    @ApiModelProperty(value = "生产批号", required = true)
    private String batBatchNo;

    @ApiModelProperty(value = "创建日期", required = true)
    private String batCreateDate;

    @ApiModelProperty(value = "创建时间", required = true)
    private String batCreateTime;

    @ApiModelProperty(value = "创建人", required = true)
    private String batCreateUser;

    @ApiModelProperty(value = "生产厂家")
    private String batFactoryCode;

    @ApiModelProperty(value = "生产厂家名称")
    private String batFactoryName;

    @ApiModelProperty(value = "供应商名称")
    private String batSupplierName;

    @ApiModelProperty(value = "产地")
    private String batProPlace;

    @ApiModelProperty(value = "付款条件编码")
    private String batPayType;

    @ApiModelProperty(value = "更新日期")
    private String batChangeDate;

    @ApiModelProperty(value = "更新时间")
    private String batChangeTime;

    @ApiModelProperty(value = "更新人")
    private String batChangeUser;

    @ApiModelProperty(value = "更新人id")
    private String batChangeUserId;

    @ApiModelProperty(value = "来源单号，入库单号")
    private String batSourceNo;
}
