package com.gys.business.service.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 11:02 2021/8/20
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductTitleSettings {
    private String field;
    private Integer width;
    private String title;
    private Boolean sortable;
    private String align;
}
