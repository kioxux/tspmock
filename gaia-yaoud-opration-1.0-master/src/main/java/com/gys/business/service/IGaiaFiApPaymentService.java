package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaFiApPayment;

import java.util.List;
import java.util.Map;

import com.gys.business.service.data.fiApPayment.GaiaFiApPaymentDto;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

/**
 * <p>
 * 应付方式维护表 服务类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
public interface IGaiaFiApPaymentService {


    //新增
    Object save(GetLoginOutData userInfo, GaiaFiApPaymentDto inData);

    //获取分页列表
    PageInfo getListPage(GetLoginOutData userInfo);
}
