package com.gys.business.service;

import com.gys.business.service.data.PriceRangeInData;
import com.gys.business.service.data.PriceRangeOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import java.util.List;

/**
 * @author Zhangchi
 * @since 2021/11/18/10:40
 */
public interface GaiaPriceRangeService {
    JsonResult getAreaList();

    List<PriceRangeOutData> getPriceRangeByClient(PriceRangeInData priceRangeInData);

    void savePriceRange(GetLoginOutData userInfo, List<PriceRangeOutData> inDataList);

    void insertPriceRange();
}
