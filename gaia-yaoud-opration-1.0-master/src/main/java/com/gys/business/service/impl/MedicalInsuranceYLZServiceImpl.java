package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaPoItemMapper;
import com.gys.business.service.data.CommenData.InData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description 大同 易联众医保数据交互
 * @Author huxinxin
 * @Date 2021/5/7 14:04
 * @Version 1.0.0
 **/
@Slf4j
@Service
public class MedicalInsuranceYLZServiceImpl {
    @Autowired
    private GaiaPoItemMapper poItemMapper;
    /*
     * @Description 获取采购订单
     * @Author huxinxin
     * @return po
     **/
    public void getPoItem(InData inData) {
//        this.poItemMapper.getPoItem(inData);
    }

}
