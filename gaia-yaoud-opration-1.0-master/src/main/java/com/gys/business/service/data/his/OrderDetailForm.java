package com.gys.business.service.data.his;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/25 17:26
 */
@Data
public class OrderDetailForm {
    @ApiModelProperty(value = "商品编码", example = "1027009")
    private String proCode;
    @ApiModelProperty(value = "商品单价", example = "18")
    private BigDecimal proPrice;
    @ApiModelProperty(value = "支付单价", example = "16")
    private BigDecimal proPayPrice;
    @ApiModelProperty(value = "购买数量", example = "2")
    private BigDecimal quantity;
    @ApiModelProperty(value = "立减金额", example = "4")
    private BigDecimal reduceAmount;
    @ApiModelProperty(value = "支付金额", example = "32")
    private BigDecimal proAmount;
    /**商品批号*/
    private String batchNo;
    /**商品名称*/
    private String proName;
    /**商品重量*/
    private String weight;
    /**his处方单号*/
    private String clinicalSheetSn;
    /**处方医师*/
    private String recipelEmp;
    /**症状*/
    private String symptom;
    /**诊断*/
    private String diagnose;
    /**审核医师*/
    private String checkEmp;
    /**审核时间*/
    private Date checkTime;

    private String proUnit;
    private String proSpec;
    private String factoryName;
}
