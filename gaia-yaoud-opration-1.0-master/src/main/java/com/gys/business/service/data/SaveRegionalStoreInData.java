package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:50 2021/10/15
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class SaveRegionalStoreInData implements Serializable {
    /**
     * 区域id
     */
    @NotNull(message = "区域ID不能为空！")
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改时间
     */
    private Date updateDate;

    /**
     * 区域所属门店关系ID
     */
    private List<Long> regionalStoreIds;

    /**
     * 默认区域所属门店关系ID
     */
    private List<Long> defaultRegionalStoreIds;
}
