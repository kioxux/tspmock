package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MessageData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "消息单号")
    private String voucherId;
    @ApiModelProperty(value = "消息类型")
    private String msType;
    @ApiModelProperty(value = "消息内容")
    private String msRemark;
    @ApiModelProperty(value = "是否已读")
    private String msFlag;
    @ApiModelProperty(value = "跳转页面路由")
    private String msPage;
    @ApiModelProperty(value = "业务单号")
    private String bussinessCode;
    @ApiModelProperty(value = "消息送达日期")
    private String arriveDate;
    @ApiModelProperty(value = "消息送达时间")
    private String arriveTime;
    @ApiModelProperty(value = "消息查看日期")
    private String checkDate;
    @ApiModelProperty(value = "消息查看时间")
    private String checkTime;
    @ApiModelProperty(value = "消息查看人员")
    private String checkEmp;
    @ApiModelProperty(value = "平台类型  APP  WEB  FX")
    private String msPlatForm;
}
