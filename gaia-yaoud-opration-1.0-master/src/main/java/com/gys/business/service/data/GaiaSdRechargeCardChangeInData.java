//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GaiaSdRechargeCardChangeInData {
    private String clientId;
    private String gsrccStatus;
    private String gsrccBrId;
    private String gsrccOldCardId;
    private String gsrccNewCardId;
    private String gsrccRemark;
    private String gsrcUpdateDate;
    private String gsrcUpdateEmp;
    private String gsrccDate;
    private String gsrccTime;

    public GaiaSdRechargeCardChangeInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrccStatus() {
        return this.gsrccStatus;
    }

    public String getGsrccBrId() {
        return this.gsrccBrId;
    }

    public String getGsrccOldCardId() {
        return this.gsrccOldCardId;
    }

    public String getGsrccNewCardId() {
        return this.gsrccNewCardId;
    }

    public String getGsrccRemark() {
        return this.gsrccRemark;
    }

    public String getGsrcUpdateDate() {
        return this.gsrcUpdateDate;
    }

    public String getGsrcUpdateEmp() {
        return this.gsrcUpdateEmp;
    }

    public String getGsrccDate() {
        return this.gsrccDate;
    }

    public String getGsrccTime() {
        return this.gsrccTime;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrccStatus(final String gsrccStatus) {
        this.gsrccStatus = gsrccStatus;
    }

    public void setGsrccBrId(final String gsrccBrId) {
        this.gsrccBrId = gsrccBrId;
    }

    public void setGsrccOldCardId(final String gsrccOldCardId) {
        this.gsrccOldCardId = gsrccOldCardId;
    }

    public void setGsrccNewCardId(final String gsrccNewCardId) {
        this.gsrccNewCardId = gsrccNewCardId;
    }

    public void setGsrccRemark(final String gsrccRemark) {
        this.gsrccRemark = gsrccRemark;
    }

    public void setGsrcUpdateDate(final String gsrcUpdateDate) {
        this.gsrcUpdateDate = gsrcUpdateDate;
    }

    public void setGsrcUpdateEmp(final String gsrcUpdateEmp) {
        this.gsrcUpdateEmp = gsrcUpdateEmp;
    }

    public void setGsrccDate(final String gsrccDate) {
        this.gsrccDate = gsrccDate;
    }

    public void setGsrccTime(final String gsrccTime) {
        this.gsrccTime = gsrccTime;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdRechargeCardChangeInData)) {
            return false;
        } else {
            GaiaSdRechargeCardChangeInData other = (GaiaSdRechargeCardChangeInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsrccStatus = this.getGsrccStatus();
                Object other$gsrccStatus = other.getGsrccStatus();
                if (this$gsrccStatus == null) {
                    if (other$gsrccStatus != null) {
                        return false;
                    }
                } else if (!this$gsrccStatus.equals(other$gsrccStatus)) {
                    return false;
                }

                Object this$gsrccBrId = this.getGsrccBrId();
                Object other$gsrccBrId = other.getGsrccBrId();
                if (this$gsrccBrId == null) {
                    if (other$gsrccBrId != null) {
                        return false;
                    }
                } else if (!this$gsrccBrId.equals(other$gsrccBrId)) {
                    return false;
                }

                label110: {
                    Object this$gsrccOldCardId = this.getGsrccOldCardId();
                    Object other$gsrccOldCardId = other.getGsrccOldCardId();
                    if (this$gsrccOldCardId == null) {
                        if (other$gsrccOldCardId == null) {
                            break label110;
                        }
                    } else if (this$gsrccOldCardId.equals(other$gsrccOldCardId)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gsrccNewCardId = this.getGsrccNewCardId();
                    Object other$gsrccNewCardId = other.getGsrccNewCardId();
                    if (this$gsrccNewCardId == null) {
                        if (other$gsrccNewCardId == null) {
                            break label103;
                        }
                    } else if (this$gsrccNewCardId.equals(other$gsrccNewCardId)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gsrccRemark = this.getGsrccRemark();
                Object other$gsrccRemark = other.getGsrccRemark();
                if (this$gsrccRemark == null) {
                    if (other$gsrccRemark != null) {
                        return false;
                    }
                } else if (!this$gsrccRemark.equals(other$gsrccRemark)) {
                    return false;
                }

                label89: {
                    Object this$gsrcUpdateDate = this.getGsrcUpdateDate();
                    Object other$gsrcUpdateDate = other.getGsrcUpdateDate();
                    if (this$gsrcUpdateDate == null) {
                        if (other$gsrcUpdateDate == null) {
                            break label89;
                        }
                    } else if (this$gsrcUpdateDate.equals(other$gsrcUpdateDate)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gsrcUpdateEmp = this.getGsrcUpdateEmp();
                    Object other$gsrcUpdateEmp = other.getGsrcUpdateEmp();
                    if (this$gsrcUpdateEmp == null) {
                        if (other$gsrcUpdateEmp == null) {
                            break label82;
                        }
                    } else if (this$gsrcUpdateEmp.equals(other$gsrcUpdateEmp)) {
                        break label82;
                    }

                    return false;
                }

                Object this$gsrccDate = this.getGsrccDate();
                Object other$gsrccDate = other.getGsrccDate();
                if (this$gsrccDate == null) {
                    if (other$gsrccDate != null) {
                        return false;
                    }
                } else if (!this$gsrccDate.equals(other$gsrccDate)) {
                    return false;
                }

                Object this$gsrccTime = this.getGsrccTime();
                Object other$gsrccTime = other.getGsrccTime();
                if (this$gsrccTime == null) {
                    if (other$gsrccTime != null) {
                        return false;
                    }
                } else if (!this$gsrccTime.equals(other$gsrccTime)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdRechargeCardChangeInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsrccStatus = this.getGsrccStatus();
        result = result * 59 + ($gsrccStatus == null ? 43 : $gsrccStatus.hashCode());
        Object $gsrccBrId = this.getGsrccBrId();
        result = result * 59 + ($gsrccBrId == null ? 43 : $gsrccBrId.hashCode());
        Object $gsrccOldCardId = this.getGsrccOldCardId();
        result = result * 59 + ($gsrccOldCardId == null ? 43 : $gsrccOldCardId.hashCode());
        Object $gsrccNewCardId = this.getGsrccNewCardId();
        result = result * 59 + ($gsrccNewCardId == null ? 43 : $gsrccNewCardId.hashCode());
        Object $gsrccRemark = this.getGsrccRemark();
        result = result * 59 + ($gsrccRemark == null ? 43 : $gsrccRemark.hashCode());
        Object $gsrcUpdateDate = this.getGsrcUpdateDate();
        result = result * 59 + ($gsrcUpdateDate == null ? 43 : $gsrcUpdateDate.hashCode());
        Object $gsrcUpdateEmp = this.getGsrcUpdateEmp();
        result = result * 59 + ($gsrcUpdateEmp == null ? 43 : $gsrcUpdateEmp.hashCode());
        Object $gsrccDate = this.getGsrccDate();
        result = result * 59 + ($gsrccDate == null ? 43 : $gsrccDate.hashCode());
        Object $gsrccTime = this.getGsrccTime();
        result = result * 59 + ($gsrccTime == null ? 43 : $gsrccTime.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdRechargeCardChangeInData(clientId=" + this.getClientId() + ", gsrccStatus=" + this.getGsrccStatus() + ", gsrccBrId=" + this.getGsrccBrId() + ", gsrccOldCardId=" + this.getGsrccOldCardId() + ", gsrccNewCardId=" + this.getGsrccNewCardId() + ", gsrccRemark=" + this.getGsrccRemark() + ", gsrcUpdateDate=" + this.getGsrcUpdateDate() + ", gsrcUpdateEmp=" + this.getGsrcUpdateEmp() + ", gsrccDate=" + this.getGsrccDate() + ", gsrccTime=" + this.getGsrccTime() + ")";
    }
}
