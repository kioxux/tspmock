package com.gys.business.service.data;

import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 16:07 2021/7/26
 * @Description：往来管理支付方式
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoPaymentData {

    private String paymentId;

    private String paymentName;

    private String realAmt;

    private String amt;

    private String amtRate;
}
