//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetProductOutData implements Serializable {
    private static final long serialVersionUID = -7254184716944375111L;
    private Long id;
    private String productName;
    private Integer productNum;

    public GetProductOutData() {
    }

    public Long getId() {
        return this.id;
    }

    public String getProductName() {
        return this.productName;
    }

    public Integer getProductNum() {
        return this.productNum;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setProductName(final String productName) {
        this.productName = productName;
    }

    public void setProductNum(final Integer productNum) {
        this.productNum = productNum;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductOutData)) {
            return false;
        } else {
            GetProductOutData other = (GetProductOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$id = this.getId();
                    Object other$id = other.getId();
                    if (this$id == null) {
                        if (other$id == null) {
                            break label47;
                        }
                    } else if (this$id.equals(other$id)) {
                        break label47;
                    }

                    return false;
                }

                Object this$productName = this.getProductName();
                Object other$productName = other.getProductName();
                if (this$productName == null) {
                    if (other$productName != null) {
                        return false;
                    }
                } else if (!this$productName.equals(other$productName)) {
                    return false;
                }

                Object this$productNum = this.getProductNum();
                Object other$productNum = other.getProductNum();
                if (this$productNum == null) {
                    if (other$productNum != null) {
                        return false;
                    }
                } else if (!this$productNum.equals(other$productNum)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $id = this.getId();
        result = result * 59 + ($id == null ? 43 : $id.hashCode());
        Object $productName = this.getProductName();
        result = result * 59 + ($productName == null ? 43 : $productName.hashCode());
        Object $productNum = this.getProductNum();
        result = result * 59 + ($productNum == null ? 43 : $productNum.hashCode());
        return result;
    }

    public String toString() {
        return "GetProductOutData(id=" + this.getId() + ", productName=" + this.getProductName() + ", productNum=" + this.getProductNum() + ")";
    }
}
