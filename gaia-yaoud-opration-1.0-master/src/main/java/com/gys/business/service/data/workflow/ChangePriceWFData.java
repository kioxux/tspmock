package com.gys.business.service.data.workflow;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 改价工作流
 * @author XIAOZY
 */
@Data
public class ChangePriceWFData {
    /**
     * 序号
     */
    private String index;
    private String client;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 通用名
     */
    private String proCommonname;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 厂家
     */
    private String proFactoryName;
    /**
     * 批号
     */
    private String batchNo;
    private String stoName;
    /**
     * 效期
     */
    private String validDay;
    /**
     *销售数量
     */
    private BigDecimal saleQty;
    /**
     *零售价
     */
    private BigDecimal price;
    /**
     *零售额
     */
    private BigDecimal saleAmt;
    /**
     *原毛利
     */
    private BigDecimal oriGross;
    /**
     *原毛利率
     */
    private String oriGrossRate;
    /**
     *改后应收价
     */
    private BigDecimal changePrice;
    /**
     *改后应收额
     */
    private BigDecimal changeAmt;
    /**
     *改后毛利率
     */
    private String changGrossRate;
    /**
     * 改后毛利额
     */
    private BigDecimal changeGross;
    /**
     * 成本价
     */
    private BigDecimal cost;
    /**
     *门店号
     */
    private String stoCode;
    /**
     *提交日期
     */
    private String createTime;
    /**
     *单号
     */
    private String billCode;
    /**
     *整单金额
     */
    private String billAmt;
    /**
     *改价备注
     */
    private String remark;
}
