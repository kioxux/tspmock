//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.RechargeCardService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class RechargeCardServiceImpl implements RechargeCardService {
    @Autowired
    private GaiaSdRechargeCardMapper rechargeCardDao;
    @Autowired
    private GaiaSdRechargeCardPaycheckMapper paycheckDao;
    @Autowired
    private GaiaUserDataMapper userDataDao;
    @Autowired
    private GaiaSdStoreDataMapper storeDataDao;
    @Autowired
    private GaiaStoreDataMapper storeDao;
    @Autowired
    private GaiaSdRechargeCardSetMapper cardSetDao;
    @Autowired
    private GaiaSdRechargeCardLoseMapper cardLoseDao;
    @Autowired
    private GaiaSdRechargeCardChangeMapper cardChangeDao;

    public RechargeCardServiceImpl() {
    }

    @Transactional
    public void saveCardInfo(RechargeCardInData inData, GetLoginOutData userInfo) {
        GaiaSdRechargeCard rechargeCard = new GaiaSdRechargeCard();
        rechargeCard.setClientId(userInfo.getClient());
        rechargeCard.setGsrcId(inData.getGsrcId());
        GaiaSdRechargeCard cardInfo = (GaiaSdRechargeCard)this.rechargeCardDao.selectByPrimaryKey(rechargeCard);
        if (inData.isInsert()) {
            if (ObjectUtil.isNotNull(cardInfo)) {
                throw new BusinessException("储值卡号已存在，请重新自动生成卡号");
            }

            this.createCardInfo(inData, userInfo);
        } else {
            this.updateCardInfo(inData, cardInfo, userInfo);
        }

    }

    public String generateCardCode(GetLoginOutData userInfo) {
        GaiaSdRechargeCardSet rechargeCardSet = new GaiaSdRechargeCardSet();
        rechargeCardSet.setClientId(userInfo.getClient());
        rechargeCardSet.setGsrcsBrId(userInfo.getDepId());
        GaiaSdRechargeCardSet cardSet = (GaiaSdRechargeCardSet)this.cardSetDao.selectByPrimaryKey(rechargeCardSet);
        String currentId;
        String gsrcId;
        if (ObjectUtil.isNotNull(cardSet) && StrUtil.isNotBlank(cardSet.getGsrcsCardPrefix())) {
            int length = 10 + cardSet.getGsrcsCardPrefix().length();
            currentId = this.rechargeCardDao.getMaxGsrcIdWithPrefix(userInfo.getClient(), userInfo.getDepId(), cardSet.getGsrcsCardPrefix(), length);
            if (StrUtil.isEmpty(currentId)) {
                return cardSet.getGsrcsCardPrefix() + "1000000000";
            } else {
                gsrcId = currentId.replaceAll(cardSet.getGsrcsCardPrefix(), "");
                gsrcId = StrUtil.toString(Integer.valueOf(gsrcId) + 1);
                return cardSet.getGsrcsCardPrefix() + gsrcId;
            }
        } else {
            int length = 10;
            currentId = this.rechargeCardDao.getMaxGsrcIdWithNoPrefix(userInfo.getClient(), userInfo.getDepId(), length);
            if (StrUtil.isEmpty(currentId)) {
                return "1000000000";
            } else {
                gsrcId = StrUtil.toString(Integer.valueOf(currentId) + 1);
                return gsrcId;
            }
        }
    }

    @Transactional
    public void resetPassword(RechargeCardInData inData, GetLoginOutData userInfo) {
        GaiaSdRechargeCard rechargeCard = new GaiaSdRechargeCard();
        rechargeCard.setClientId(userInfo.getClient());
        rechargeCard.setGsrcId(inData.getGsrcId());
        GaiaSdRechargeCard cardInfo = (GaiaSdRechargeCard)this.rechargeCardDao.selectByPrimaryKey(rechargeCard);
        GaiaSdRechargeCardSet rechargeCardSet = new GaiaSdRechargeCardSet();
        rechargeCardSet.setClientId(userInfo.getClient());
        rechargeCardSet.setGsrcsBrId(userInfo.getDepId());
        GaiaSdRechargeCardSet cardSet = (GaiaSdRechargeCardSet)this.cardSetDao.selectByPrimaryKey(rechargeCardSet);
        String password = "123456";
        if (ObjectUtil.isNotNull(cardSet) && StrUtil.isNotBlank(cardSet.getGsrcsPassword())) {
            password = cardSet.getGsrcsPassword();
        }

        this.updatePassword(cardInfo, password, userInfo);
    }

    @Transactional
    public void modifyPassword(RechargeCardInData inData, GetLoginOutData userInfo) {
        GaiaSdRechargeCard rechargeCard = new GaiaSdRechargeCard();
        rechargeCard.setClientId(userInfo.getClient());
        rechargeCard.setGsrcId(inData.getGsrcId());
        GaiaSdRechargeCard cardInfo = (GaiaSdRechargeCard)this.rechargeCardDao.selectByPrimaryKey(rechargeCard);
        if (StrUtil.equals(inData.getGsrcPassword(), cardInfo.getGsrcPassword())) {
            this.updatePassword(cardInfo, inData.getNewPassword(), userInfo);
        } else {
            throw new BusinessException("原密码不符，请核对后重新输入");
        }
    }

    @Transactional
    public void savePayInfo(RechargeCardPaycheckInData inData, GetLoginOutData userInfo) {
        GaiaSdRechargeCard record = new GaiaSdRechargeCard();
        record.setClientId(userInfo.getClient());
        record.setGsrcId(inData.getGsrcpCardId());
        GaiaSdRechargeCard rechargeCard = (GaiaSdRechargeCard)this.rechargeCardDao.selectByPrimaryKey(record);
        if (ObjectUtil.isNull(rechargeCard)) {
            throw new BusinessException("储值卡号不存在");
        } else {
            if (inData.getIsCheck()) {
                GaiaSdRechargeCardSet rechargeCardSet = new GaiaSdRechargeCardSet();
                rechargeCardSet.setClientId(userInfo.getClient());
                rechargeCardSet.setGsrcsBrId(userInfo.getDepId());
                GaiaSdRechargeCardSet cardSet = (GaiaSdRechargeCardSet)this.cardSetDao.selectByPrimaryKey(rechargeCardSet);
                if (ObjectUtil.isNotNull(cardSet)) {
                    Example example;
                    List list;
                    if (inData.getGsrcpProportion().equals(cardSet.getGsrcsProportion2())) {
                        if ("2".equals(inData.getGsrcpPromotion()) && "0".equals(cardSet.getGsrcsPromotion2())) {
                            throw new BusinessException("当前比例无法活动充值");
                        }

                        if ("1".equals(inData.getGsrcpPromotion()) && "1".equals(cardSet.getGsrcsPromotion2())) {
                            throw new BusinessException("当前比例无法普通充值");
                        }

                        if ("1".equals(cardSet.getGsrcsRepeat2())) {
                            example = new Example(GaiaSdRechargeCardPaycheck.class);
                            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrcpCardId", inData.getGsrcpCardId()).andEqualTo("gsrcpProportion", cardSet.getGsrcsProportion2());
                            list = this.paycheckDao.selectByExample(example);
                            if (ObjectUtil.isNotEmpty(list)) {
                                throw new BusinessException("当前比例无法重复充值");
                            }
                        }
                    }

                    if (inData.getGsrcpProportion().equals(cardSet.getGsrcsProportion3())) {
                        if ("2".equals(inData.getGsrcpPromotion()) && "0".equals(cardSet.getGsrcsPromotion3())) {
                            throw new BusinessException("当前比例无法活动充值");
                        }

                        if ("1".equals(inData.getGsrcpPromotion()) && "1".equals(cardSet.getGsrcsPromotion3())) {
                            throw new BusinessException("当前比例无法普通充值");
                        }

                        if ("1".equals(cardSet.getGsrcsRepeat3())) {
                            example = new Example(GaiaSdRechargeCardPaycheck.class);
                            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrcpCardId", inData.getGsrcpCardId()).andEqualTo("gsrcpProportion", cardSet.getGsrcsProportion3());
                            list = this.paycheckDao.selectByExample(example);
                            if (ObjectUtil.isNotEmpty(list)) {
                                throw new BusinessException("当前比例无法重复充值");
                            }
                        }
                    }
                }
            } else {
                Date now = new Date();
                String codePre = DateUtil.format(now, "yyMMdd");
//                String voucherId = this.paycheckDao.selectNextVoucherId(userInfo.getClient(), codePre);
//                rechargeCard.setGsrcAmt(inData.getGsrcpRechargeAmt());
//                rechargeCard.setGsrcStatus("1");
//                this.rechargeCardDao.updateByPrimaryKey(rechargeCard);
//                GaiaSdRechargeCardPaycheck paycheck = new GaiaSdRechargeCardPaycheck();
//                paycheck.setClientId(userInfo.getClient());
//                paycheck.setGsrcpVoucherId(voucherId);
//                paycheck.setGsrcpCardId(inData.getGsrcpCardId());
//                paycheck.setGsrcpPayAmt(inData.getGsrcpPayAmt());
//                paycheck.setGsrcpBrId(userInfo.getDepId());
//                paycheck.setGsrcpDate(DateUtil.format(now, DatePattern.PURE_DATE_FORMAT));
//                paycheck.setGsrcpTime(DateUtil.format(now, DatePattern.PURE_TIME_FORMAT));
//                paycheck.setGsrcpEmp(userInfo.getUserId());
//                paycheck.setGsrcpBeforeAmt(inData.getGsrcpBeforeAmt());
//                paycheck.setGsrcpProportion(inData.getGsrcpProportion());
//                paycheck.setGsrcpPromotion(inData.getGsrcpPromotion());
//                paycheck.setGsrcpAfterAmt(inData.getGsrcpAfterAmt());
//                paycheck.setGsrcpRechargeAmt(inData.getGsrcpRechargeAmt());
//                paycheck.setGsrcpYsAmt(new BigDecimal(0));
//                this.paycheckDao.insert(paycheck);
            }

        }
    }

    public List<RechargeCardOutData> getCardList(RechargeCardInData inData) {
        return this.rechargeCardDao.selectCardList(inData);
    }

    public List<RechComsuOutData> getRechComsuList(RechComsuInData inData) {
        List<RechComsuOutData> rechComsuList = this.rechargeCardDao.getRechComsuList(inData);
        return rechComsuList;
    }

    @Transactional
    public void saveSet(RechargeCardSetInData inData, GetLoginOutData userInfo) {
        GaiaSdRechargeCardSet setData = new GaiaSdRechargeCardSet();
        setData.setClientId(userInfo.getClient());
        setData.setGsrcsBrId(userInfo.getDepId());
        setData.setGsrcsCardPrefix(StrUtil.isBlank(inData.getGsrcsCardPrefix()) ? "" : inData.getGsrcsCardPrefix());
        setData.setGsrcsPassword(inData.getGsrcsPassword());
//        setData.setGsrcsPaymentMethod(inData.getGsrcsPaymentMethod());
        setData.setGsrcsPwNeedOpt(inData.getGsrcsPwNeedOpt());
//        setData.setGsrcsMinAmtOpt(inData.getGsrcsMinAmtOpt());
//        setData.setGsrcsMaxAmtOpt(inData.getGsrcsMaxAmtOpt());
//        setData.setGsrcsFirstPwOpt(inData.getGsrcsFirstPwOpt());
        setData.setGsrcsRecoverpwOpt(inData.getGsrcsRecoverpwOpt());
        setData.setGsrcsProportion1(inData.getGsrcsProportion1());
        setData.setGsrcsMinAmt1(inData.getGsrcsMinAmt1());
        setData.setGsrcsProportion2(inData.getGsrcsProportion2());
        setData.setGsrcsMinAmt2(inData.getGsrcsMinAmt2());
        setData.setGsrcsPromotion2(inData.getGsrcsPromotion2());
        setData.setGsrcsRepeat2(inData.getGsrcsRepeat2());
        setData.setGsrcsProportion3(inData.getGsrcsProportion3());
        setData.setGsrcsMinAmt3(inData.getGsrcsMinAmt3());
        setData.setGsrcsPromotion3(inData.getGsrcsPromotion3());
        setData.setGsrcsRepeat3(inData.getGsrcsRepeat3());
        setData.setGsrcsUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
        setData.setGsrcsUpdateEmp(userInfo.getUserId());
        GaiaSdRechargeCardSet data = (GaiaSdRechargeCardSet)this.cardSetDao.selectByPrimaryKey(setData);
        if (ObjectUtil.isNotNull(data)) {
            this.cardSetDao.updateByPrimaryKey(setData);
        } else {
            this.cardSetDao.insert(setData);
        }

    }

    public RechargeCardSetOutData getSetData(GetLoginOutData userInfo) {
        GaiaSdRechargeCardSet setData = new GaiaSdRechargeCardSet();
        setData.setClientId(userInfo.getClient());
        setData.setGsrcsBrId(userInfo.getDepId());
        GaiaSdRechargeCardSet data = (GaiaSdRechargeCardSet)this.cardSetDao.selectByPrimaryKey(setData);
        RechargeCardSetOutData outData = new RechargeCardSetOutData();
        if (ObjectUtil.isNotNull(data)) {
            outData.setGsrcsCardPrefix(data.getGsrcsCardPrefix());
            outData.setGsrcsPassword(data.getGsrcsPassword());
//            outData.setGsrcsPaymentMethod(data.getGsrcsPaymentMethod());
            outData.setGsrcsPwNeedOpt(data.getGsrcsPwNeedOpt());
//            outData.setGsrcsMinAmtOpt(data.getGsrcsMinAmtOpt());
//            outData.setGsrcsMaxAmtOpt(data.getGsrcsMaxAmtOpt());
//            outData.setGsrcsFirstPwOpt(data.getGsrcsFirstPwOpt());
            outData.setGsrcsRecoverpwOpt(data.getGsrcsRecoverpwOpt());
            outData.setGsrcsProportion1(data.getGsrcsProportion1());
            outData.setGsrcsMinAmt1(data.getGsrcsMinAmt1());
            outData.setGsrcsProportion2(data.getGsrcsProportion2());
            outData.setGsrcsMinAmt2(data.getGsrcsMinAmt2());
            outData.setGsrcsPromotion2(data.getGsrcsPromotion2());
            outData.setGsrcsRepeat2(data.getGsrcsRepeat2());
            outData.setGsrcsProportion3(data.getGsrcsProportion3());
            outData.setGsrcsMinAmt3(data.getGsrcsMinAmt3());
            outData.setGsrcsPromotion3(data.getGsrcsPromotion3());
            outData.setGsrcsRepeat3(data.getGsrcsRepeat3());
        }

        return outData;
    }

    @Transactional
    public void loseCard(GaiaSdRechargeCardLoseInData inData, GetLoginOutData userInfo) {
        String codePre = DateUtil.format(new Date(), "yyMMdd");
        String voucherId = this.cardLoseDao.selectNextVoucherId(userInfo.getClient(), codePre);
        List<GaiaSdRechargeCardLose> allLostData = this.cardLoseDao.getAllLostDataByCardNo(userInfo.getClient(), inData.getGsrclCardId());
        GaiaSdRechargeCardLose latestData;
        if (CollUtil.isNotEmpty(allLostData)) {
            latestData = (GaiaSdRechargeCardLose)allLostData.get(0);
            if ("1".equals(inData.getGsrclStatus()) && latestData.getGsrclStatus().equals(inData.getGsrclStatus())) {
                throw new BusinessException("该储值卡已挂失，无需再次挂失");
            }

            if ("2".equals(inData.getGsrclStatus()) && latestData.getGsrclStatus().equals(inData.getGsrclStatus())) {
                throw new BusinessException("该储值卡已解挂，无需再次解挂");
            }
        } else if ("2".equals(inData.getGsrclStatus())) {
            throw new BusinessException("该储值卡未挂失，无需解挂");
        }

        latestData = new GaiaSdRechargeCardLose();
        latestData.setClientId(userInfo.getClient());
        latestData.setGsrclBrId(userInfo.getDepId());
        latestData.setGsrclCardId(inData.getGsrclCardId());
        latestData.setGsrclVoucherId(voucherId);
        latestData.setGsrclStatus(inData.getGsrclStatus());
        latestData.setGsrclDate(inData.getGsrclDate());
        latestData.setGsrclTime(inData.getGsrclTime());
        latestData.setGsrclReason(inData.getGsrclReason());
        latestData.setGsrclEmp(userInfo.getUserId());
        latestData.setGsrclStatus(inData.getGsrclStatus());
        this.cardLoseDao.insert(latestData);
    }

    @Transactional
    public void changeCard(GaiaSdRechargeCardChangeInData inData, GetLoginOutData userInfo) {
        String codePre = DateUtil.format(new Date(), "yyMMdd");
        String voucherId = this.cardChangeDao.selectNextVoucherId(userInfo.getClient(), codePre);
        if (!StrUtil.isBlank(inData.getGsrccOldCardId()) && !StrUtil.isBlank(inData.getGsrccNewCardId())) {
            GaiaSdRechargeCard record = new GaiaSdRechargeCard();
            record.setClientId(userInfo.getClient());
            record.setGsrcId(inData.getGsrccOldCardId());
            GaiaSdRechargeCard rechargeCard = (GaiaSdRechargeCard)this.rechargeCardDao.selectByPrimaryKey(record);
            if (ObjectUtil.isNull(rechargeCard)) {
                throw new BusinessException("原储值卡不存在");
            } else {
                record.setGsrcId(inData.getGsrccNewCardId());
                rechargeCard = (GaiaSdRechargeCard)this.rechargeCardDao.selectByPrimaryKey(record);
                if (ObjectUtil.isNotNull(rechargeCard)) {
                    throw new BusinessException("新储值卡已存在");
                } else {
                    GaiaSdRechargeCardChange changeData = new GaiaSdRechargeCardChange();
                    changeData.setClientId(userInfo.getClient());
                    changeData.setGsrccVoucherId(voucherId);
                    changeData.setGsrccBrId(userInfo.getDepId());
                    changeData.setGsrccDate(inData.getGsrccDate());
                    changeData.setGsrccTime(inData.getGsrccTime());
                    changeData.setGsrccEmp(userInfo.getUserId());
                    changeData.setGsrccNewCardId(inData.getGsrccNewCardId());
                    changeData.setGsrccOldCardId(inData.getGsrccOldCardId());
                    changeData.setGsrccStatus("1");
                    changeData.setGsrccRemark(inData.getGsrccRemark());
                    this.cardChangeDao.insert(changeData);
                    GaiaSdRechargeCard cardData = new GaiaSdRechargeCard();
                    cardData.setClientId(userInfo.getClient());
                    cardData.setGsrcId(inData.getGsrccOldCardId());
                    inData.setClientId(userInfo.getClient());
                    inData.setGsrcUpdateEmp(userInfo.getUserId());
                    inData.setGsrcUpdateDate(DateUtil.format(new Date(), "yyyyMMdd"));
                    this.rechargeCardDao.updateRechargeCardNo(inData);
                }
            }
        } else {
            throw new BusinessException("参数不能为空");
        }
    }

    private void updatePassword(GaiaSdRechargeCard cardInfo, String password, GetLoginOutData userInfo) {
        cardInfo.setGsrcUpdateBrId(userInfo.getDepId());
        cardInfo.setGsrcUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
        cardInfo.setGsrcUpdateEmp(userInfo.getUserId());
        cardInfo.setGsrcPassword(password);
        this.rechargeCardDao.updateByPrimaryKey(cardInfo);
    }

    private void updateCardInfo(RechargeCardInData inData, GaiaSdRechargeCard cardInfo, GetLoginOutData userInfo) {
        Example example = new Example(GaiaSdRechargeCard.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsrcMobile", inData.getGsrcMobile()).andNotEqualTo("gsrcId", cardInfo.getGsrcId());
        List<GaiaSdRechargeCard> cards = this.rechargeCardDao.selectByExample(example);
        if (CollUtil.isNotEmpty(cards)) {
            throw new BusinessException("手机号已绑定其他储值卡");
        } else {
            cardInfo.setGsrcName(inData.getGsrcName());
            cardInfo.setGsrcSex(inData.getGsrcSex());
            cardInfo.setGsrcMobile(inData.getGsrcMobile());
            cardInfo.setGsrcTel(inData.getGsrcTel());
            cardInfo.setGsrcAddress(inData.getGsrcAddress());
            cardInfo.setGsrcUpdateBrId(userInfo.getDepId());
            cardInfo.setGsrcUpdateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
            cardInfo.setGsrcUpdateEmp(userInfo.getUserId());
            this.rechargeCardDao.updateByPrimaryKey(cardInfo);
        }
    }

    private void createCardInfo(RechargeCardInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaSdRechargeCard.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsrcMobile", inData.getGsrcMobile());
        List<GaiaSdRechargeCard> cards = this.rechargeCardDao.selectByExample(example);
        if (CollUtil.isNotEmpty(cards)) {
            throw new BusinessException("手机号已绑定其他储值卡");
        } else {
            GaiaSdRechargeCard rechargeCard = new GaiaSdRechargeCard();
            rechargeCard.setClientId(userInfo.getClient());
            rechargeCard.setGsrcId(inData.getGsrcId());
            rechargeCard.setGsrcBrId(userInfo.getDepId());
            rechargeCard.setGsrcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_FORMAT));
            rechargeCard.setGsrcEmp(userInfo.getUserId());
//            rechargeCard.setGsrcUsableBrId(userInfo.getDepId());
            rechargeCard.setGsrcStatus("0");
            rechargeCard.setGsrcName(inData.getGsrcName());
            rechargeCard.setGsrcSex(inData.getGsrcSex());
            rechargeCard.setGsrcMobile(inData.getGsrcMobile());
            rechargeCard.setGsrcTel(inData.getGsrcTel());
            rechargeCard.setGsrcAddress(inData.getGsrcAddress());
            GaiaSdRechargeCardSet rechargeCardSet = new GaiaSdRechargeCardSet();
            rechargeCardSet.setClientId(userInfo.getClient());
            rechargeCardSet.setGsrcsBrId(userInfo.getDepId());
            GaiaSdRechargeCardSet cardSet = (GaiaSdRechargeCardSet)this.cardSetDao.selectByPrimaryKey(rechargeCardSet);
            String password = "123456";
            if (ObjectUtil.isNotNull(cardSet) && StrUtil.isNotBlank(cardSet.getGsrcsPassword())) {
                password = cardSet.getGsrcsPassword();
            }

            rechargeCard.setGsrcId(inData.getGsrcId());
            rechargeCard.setGsrcPassword(password);
            rechargeCard.setGsrcAmt(new BigDecimal(0.0D));
            this.rechargeCardDao.insert(rechargeCard);
        }
    }

    public RechargeCardOutData getCardInfo(RechargeCardInData inData) {
        RechargeCardOutData outData = new RechargeCardOutData();
        Example example = new Example(GaiaSdRechargeCard.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrcId", inData.getGsrcId()).andEqualTo("gsrcStatus", "1");
        GaiaSdRechargeCard rechargeCard = (GaiaSdRechargeCard)this.rechargeCardDao.selectOneByExample(example);
        if (ObjectUtil.isEmpty(rechargeCard)) {
            throw new BusinessException("未查询到储值卡号，请核对后重新输入！");
        } else {
            outData.setGsrcAddress(rechargeCard.getGsrcAddress());
            outData.setGsrcAmt(rechargeCard.getGsrcAmt());
            outData.setGsrcBrId(rechargeCard.getGsrcBrId());
            outData.setGsrcMobile(rechargeCard.getGsrcMobile());
            outData.setGsrcId(rechargeCard.getGsrcId());
            outData.setGsrcName(rechargeCard.getGsrcName());
            outData.setGsrcSex(rechargeCard.getGsrcSex());
            return outData;
        }
    }
}
