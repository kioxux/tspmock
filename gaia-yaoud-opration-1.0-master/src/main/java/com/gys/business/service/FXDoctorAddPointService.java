package com.gys.business.service;

import com.gys.business.service.data.GaiaDoctorAddPointInData;
import com.gys.business.service.data.GaiaDoctorAddPointQueryData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;

public interface FXDoctorAddPointService {
    JsonResult saveDoctorAddPoint(GaiaDoctorAddPointInData inData);

    PageInfo findDoctorAddPointPage(GaiaDoctorAddPointQueryData queryData);

    JsonResult findDoctorNameList(GetLoginOutData loginUser);
}
