//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.FranchiseeInData;
import com.gys.business.service.data.FranchiseeOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;

public interface FranchiseeService {
    FranchiseeOutData getFranchiseeById(FranchiseeInData inData);

    void insertFranchisee(GetLoginOutData userInfo,FranchiseeInData inData);

    PageInfo getFranchisee(FranchiseeInData inData);

    List<FranchiseeOutData> getFranchiseeByName(FranchiseeInData inData);

    String selectStoPriceComparison(String clientId,String stoCode,String value);

}
