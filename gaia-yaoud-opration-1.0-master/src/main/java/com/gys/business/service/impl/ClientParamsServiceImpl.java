package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.service.ClientParamsService;
import com.gys.business.service.data.GaiaClSystemPara;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/12/13 17:05
 */
@Service("clientParamsService")
public class ClientParamsServiceImpl implements ClientParamsService {
    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;

    @Override
    public GaiaClSystemPara getUnique(GaiaClSystemPara cond) {
        return gaiaClSystemParaMapper.selectByPrimaryKey(cond.getClient(), cond.getGcspId());
    }
}
