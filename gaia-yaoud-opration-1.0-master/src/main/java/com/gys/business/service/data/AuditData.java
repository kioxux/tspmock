package com.gys.business.service.data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel(value = "审核数据",description = "")
@Data
public class AuditData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "任务月份")
    private String monthPlan;
    @ApiModelProperty(value = "营业额")
    private String gssdAmt;
    @ApiModelProperty(value = "毛利额")
    private String gossfixAmt;
    @ApiModelProperty(value = "毛利率")
    private String gossfixRate;
    @ApiModelProperty(value = "会员卡")
    private String cardNum;
}
