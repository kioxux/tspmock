//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GiftCardOutData {
    private String clientId;
    private String brId;
    private String gsphVoucherId;
    private String promotionContent;
    private String promotionName;
    private String promotionTypeId;
    private String promotionType;
    private String proName;
    private String proId;
    private String serial;
    private String gsphBeginDate;
    private String gsphEndDate;
    private String gsphBeginTime;
    private String gsphEndTime;
    private String gsphDateFrequency;
    private String gsphWeekFrequency;
    private String sameOrDiff;
    private String gspcsCouponType;
    private String gspcsCouponRemarks;
    private String gspcgActNo;
    private String gspcsBeginDate;
    private String gspcsEndDate;
    private String gspcsBeginTime;
    private String gspcsEndTime;
    private String gspcsSingleUseAmt;
    private String gspcsSinglePaycheckAmt;
    private String gspscInteFlag;
    private String gspscInteRate;

    public GiftCardOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public String getGsphVoucherId() {
        return this.gsphVoucherId;
    }

    public String getPromotionContent() {
        return this.promotionContent;
    }

    public String getPromotionName() {
        return this.promotionName;
    }

    public String getPromotionTypeId() {
        return this.promotionTypeId;
    }

    public String getPromotionType() {
        return this.promotionType;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProId() {
        return this.proId;
    }

    public String getSerial() {
        return this.serial;
    }

    public String getGsphBeginDate() {
        return this.gsphBeginDate;
    }

    public String getGsphEndDate() {
        return this.gsphEndDate;
    }

    public String getGsphBeginTime() {
        return this.gsphBeginTime;
    }

    public String getGsphEndTime() {
        return this.gsphEndTime;
    }

    public String getGsphDateFrequency() {
        return this.gsphDateFrequency;
    }

    public String getGsphWeekFrequency() {
        return this.gsphWeekFrequency;
    }

    public String getSameOrDiff() {
        return this.sameOrDiff;
    }

    public String getGspcsCouponType() {
        return this.gspcsCouponType;
    }

    public String getGspcsCouponRemarks() {
        return this.gspcsCouponRemarks;
    }

    public String getGspcgActNo() {
        return this.gspcgActNo;
    }

    public String getGspcsBeginDate() {
        return this.gspcsBeginDate;
    }

    public String getGspcsEndDate() {
        return this.gspcsEndDate;
    }

    public String getGspcsBeginTime() {
        return this.gspcsBeginTime;
    }

    public String getGspcsEndTime() {
        return this.gspcsEndTime;
    }

    public String getGspcsSingleUseAmt() {
        return this.gspcsSingleUseAmt;
    }

    public String getGspcsSinglePaycheckAmt() {
        return this.gspcsSinglePaycheckAmt;
    }

    public String getGspscInteFlag() {
        return this.gspscInteFlag;
    }

    public String getGspscInteRate() {
        return this.gspscInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public void setGsphVoucherId(final String gsphVoucherId) {
        this.gsphVoucherId = gsphVoucherId;
    }

    public void setPromotionContent(final String promotionContent) {
        this.promotionContent = promotionContent;
    }

    public void setPromotionName(final String promotionName) {
        this.promotionName = promotionName;
    }

    public void setPromotionTypeId(final String promotionTypeId) {
        this.promotionTypeId = promotionTypeId;
    }

    public void setPromotionType(final String promotionType) {
        this.promotionType = promotionType;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProId(final String proId) {
        this.proId = proId;
    }

    public void setSerial(final String serial) {
        this.serial = serial;
    }

    public void setGsphBeginDate(final String gsphBeginDate) {
        this.gsphBeginDate = gsphBeginDate;
    }

    public void setGsphEndDate(final String gsphEndDate) {
        this.gsphEndDate = gsphEndDate;
    }

    public void setGsphBeginTime(final String gsphBeginTime) {
        this.gsphBeginTime = gsphBeginTime;
    }

    public void setGsphEndTime(final String gsphEndTime) {
        this.gsphEndTime = gsphEndTime;
    }

    public void setGsphDateFrequency(final String gsphDateFrequency) {
        this.gsphDateFrequency = gsphDateFrequency;
    }

    public void setGsphWeekFrequency(final String gsphWeekFrequency) {
        this.gsphWeekFrequency = gsphWeekFrequency;
    }

    public void setSameOrDiff(final String sameOrDiff) {
        this.sameOrDiff = sameOrDiff;
    }

    public void setGspcsCouponType(final String gspcsCouponType) {
        this.gspcsCouponType = gspcsCouponType;
    }

    public void setGspcsCouponRemarks(final String gspcsCouponRemarks) {
        this.gspcsCouponRemarks = gspcsCouponRemarks;
    }

    public void setGspcgActNo(final String gspcgActNo) {
        this.gspcgActNo = gspcgActNo;
    }

    public void setGspcsBeginDate(final String gspcsBeginDate) {
        this.gspcsBeginDate = gspcsBeginDate;
    }

    public void setGspcsEndDate(final String gspcsEndDate) {
        this.gspcsEndDate = gspcsEndDate;
    }

    public void setGspcsBeginTime(final String gspcsBeginTime) {
        this.gspcsBeginTime = gspcsBeginTime;
    }

    public void setGspcsEndTime(final String gspcsEndTime) {
        this.gspcsEndTime = gspcsEndTime;
    }

    public void setGspcsSingleUseAmt(final String gspcsSingleUseAmt) {
        this.gspcsSingleUseAmt = gspcsSingleUseAmt;
    }

    public void setGspcsSinglePaycheckAmt(final String gspcsSinglePaycheckAmt) {
        this.gspcsSinglePaycheckAmt = gspcsSinglePaycheckAmt;
    }

    public void setGspscInteFlag(final String gspscInteFlag) {
        this.gspscInteFlag = gspscInteFlag;
    }

    public void setGspscInteRate(final String gspscInteRate) {
        this.gspscInteRate = gspscInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GiftCardOutData)) {
            return false;
        } else {
            GiftCardOutData other = (GiftCardOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label347: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label347;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label347;
                    }

                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                Object this$gsphVoucherId = this.getGsphVoucherId();
                Object other$gsphVoucherId = other.getGsphVoucherId();
                if (this$gsphVoucherId == null) {
                    if (other$gsphVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsphVoucherId.equals(other$gsphVoucherId)) {
                    return false;
                }

                label326: {
                    Object this$promotionContent = this.getPromotionContent();
                    Object other$promotionContent = other.getPromotionContent();
                    if (this$promotionContent == null) {
                        if (other$promotionContent == null) {
                            break label326;
                        }
                    } else if (this$promotionContent.equals(other$promotionContent)) {
                        break label326;
                    }

                    return false;
                }

                label319: {
                    Object this$promotionName = this.getPromotionName();
                    Object other$promotionName = other.getPromotionName();
                    if (this$promotionName == null) {
                        if (other$promotionName == null) {
                            break label319;
                        }
                    } else if (this$promotionName.equals(other$promotionName)) {
                        break label319;
                    }

                    return false;
                }

                label312: {
                    Object this$promotionTypeId = this.getPromotionTypeId();
                    Object other$promotionTypeId = other.getPromotionTypeId();
                    if (this$promotionTypeId == null) {
                        if (other$promotionTypeId == null) {
                            break label312;
                        }
                    } else if (this$promotionTypeId.equals(other$promotionTypeId)) {
                        break label312;
                    }

                    return false;
                }

                Object this$promotionType = this.getPromotionType();
                Object other$promotionType = other.getPromotionType();
                if (this$promotionType == null) {
                    if (other$promotionType != null) {
                        return false;
                    }
                } else if (!this$promotionType.equals(other$promotionType)) {
                    return false;
                }

                label298: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label298;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label298;
                    }

                    return false;
                }

                Object this$proId = this.getProId();
                Object other$proId = other.getProId();
                if (this$proId == null) {
                    if (other$proId != null) {
                        return false;
                    }
                } else if (!this$proId.equals(other$proId)) {
                    return false;
                }

                label284: {
                    Object this$serial = this.getSerial();
                    Object other$serial = other.getSerial();
                    if (this$serial == null) {
                        if (other$serial == null) {
                            break label284;
                        }
                    } else if (this$serial.equals(other$serial)) {
                        break label284;
                    }

                    return false;
                }

                Object this$gsphBeginDate = this.getGsphBeginDate();
                Object other$gsphBeginDate = other.getGsphBeginDate();
                if (this$gsphBeginDate == null) {
                    if (other$gsphBeginDate != null) {
                        return false;
                    }
                } else if (!this$gsphBeginDate.equals(other$gsphBeginDate)) {
                    return false;
                }

                Object this$gsphEndDate = this.getGsphEndDate();
                Object other$gsphEndDate = other.getGsphEndDate();
                if (this$gsphEndDate == null) {
                    if (other$gsphEndDate != null) {
                        return false;
                    }
                } else if (!this$gsphEndDate.equals(other$gsphEndDate)) {
                    return false;
                }

                label263: {
                    Object this$gsphBeginTime = this.getGsphBeginTime();
                    Object other$gsphBeginTime = other.getGsphBeginTime();
                    if (this$gsphBeginTime == null) {
                        if (other$gsphBeginTime == null) {
                            break label263;
                        }
                    } else if (this$gsphBeginTime.equals(other$gsphBeginTime)) {
                        break label263;
                    }

                    return false;
                }

                label256: {
                    Object this$gsphEndTime = this.getGsphEndTime();
                    Object other$gsphEndTime = other.getGsphEndTime();
                    if (this$gsphEndTime == null) {
                        if (other$gsphEndTime == null) {
                            break label256;
                        }
                    } else if (this$gsphEndTime.equals(other$gsphEndTime)) {
                        break label256;
                    }

                    return false;
                }

                Object this$gsphDateFrequency = this.getGsphDateFrequency();
                Object other$gsphDateFrequency = other.getGsphDateFrequency();
                if (this$gsphDateFrequency == null) {
                    if (other$gsphDateFrequency != null) {
                        return false;
                    }
                } else if (!this$gsphDateFrequency.equals(other$gsphDateFrequency)) {
                    return false;
                }

                Object this$gsphWeekFrequency = this.getGsphWeekFrequency();
                Object other$gsphWeekFrequency = other.getGsphWeekFrequency();
                if (this$gsphWeekFrequency == null) {
                    if (other$gsphWeekFrequency != null) {
                        return false;
                    }
                } else if (!this$gsphWeekFrequency.equals(other$gsphWeekFrequency)) {
                    return false;
                }

                label235: {
                    Object this$sameOrDiff = this.getSameOrDiff();
                    Object other$sameOrDiff = other.getSameOrDiff();
                    if (this$sameOrDiff == null) {
                        if (other$sameOrDiff == null) {
                            break label235;
                        }
                    } else if (this$sameOrDiff.equals(other$sameOrDiff)) {
                        break label235;
                    }

                    return false;
                }

                Object this$gspcsCouponType = this.getGspcsCouponType();
                Object other$gspcsCouponType = other.getGspcsCouponType();
                if (this$gspcsCouponType == null) {
                    if (other$gspcsCouponType != null) {
                        return false;
                    }
                } else if (!this$gspcsCouponType.equals(other$gspcsCouponType)) {
                    return false;
                }

                Object this$gspcsCouponRemarks = this.getGspcsCouponRemarks();
                Object other$gspcsCouponRemarks = other.getGspcsCouponRemarks();
                if (this$gspcsCouponRemarks == null) {
                    if (other$gspcsCouponRemarks != null) {
                        return false;
                    }
                } else if (!this$gspcsCouponRemarks.equals(other$gspcsCouponRemarks)) {
                    return false;
                }

                label214: {
                    Object this$gspcgActNo = this.getGspcgActNo();
                    Object other$gspcgActNo = other.getGspcgActNo();
                    if (this$gspcgActNo == null) {
                        if (other$gspcgActNo == null) {
                            break label214;
                        }
                    } else if (this$gspcgActNo.equals(other$gspcgActNo)) {
                        break label214;
                    }

                    return false;
                }

                label207: {
                    Object this$gspcsBeginDate = this.getGspcsBeginDate();
                    Object other$gspcsBeginDate = other.getGspcsBeginDate();
                    if (this$gspcsBeginDate == null) {
                        if (other$gspcsBeginDate == null) {
                            break label207;
                        }
                    } else if (this$gspcsBeginDate.equals(other$gspcsBeginDate)) {
                        break label207;
                    }

                    return false;
                }

                label200: {
                    Object this$gspcsEndDate = this.getGspcsEndDate();
                    Object other$gspcsEndDate = other.getGspcsEndDate();
                    if (this$gspcsEndDate == null) {
                        if (other$gspcsEndDate == null) {
                            break label200;
                        }
                    } else if (this$gspcsEndDate.equals(other$gspcsEndDate)) {
                        break label200;
                    }

                    return false;
                }

                Object this$gspcsBeginTime = this.getGspcsBeginTime();
                Object other$gspcsBeginTime = other.getGspcsBeginTime();
                if (this$gspcsBeginTime == null) {
                    if (other$gspcsBeginTime != null) {
                        return false;
                    }
                } else if (!this$gspcsBeginTime.equals(other$gspcsBeginTime)) {
                    return false;
                }

                label186: {
                    Object this$gspcsEndTime = this.getGspcsEndTime();
                    Object other$gspcsEndTime = other.getGspcsEndTime();
                    if (this$gspcsEndTime == null) {
                        if (other$gspcsEndTime == null) {
                            break label186;
                        }
                    } else if (this$gspcsEndTime.equals(other$gspcsEndTime)) {
                        break label186;
                    }

                    return false;
                }

                Object this$gspcsSingleUseAmt = this.getGspcsSingleUseAmt();
                Object other$gspcsSingleUseAmt = other.getGspcsSingleUseAmt();
                if (this$gspcsSingleUseAmt == null) {
                    if (other$gspcsSingleUseAmt != null) {
                        return false;
                    }
                } else if (!this$gspcsSingleUseAmt.equals(other$gspcsSingleUseAmt)) {
                    return false;
                }

                label172: {
                    Object this$gspcsSinglePaycheckAmt = this.getGspcsSinglePaycheckAmt();
                    Object other$gspcsSinglePaycheckAmt = other.getGspcsSinglePaycheckAmt();
                    if (this$gspcsSinglePaycheckAmt == null) {
                        if (other$gspcsSinglePaycheckAmt == null) {
                            break label172;
                        }
                    } else if (this$gspcsSinglePaycheckAmt.equals(other$gspcsSinglePaycheckAmt)) {
                        break label172;
                    }

                    return false;
                }

                Object this$gspscInteFlag = this.getGspscInteFlag();
                Object other$gspscInteFlag = other.getGspscInteFlag();
                if (this$gspscInteFlag == null) {
                    if (other$gspscInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspscInteFlag.equals(other$gspscInteFlag)) {
                    return false;
                }

                Object this$gspscInteRate = this.getGspscInteRate();
                Object other$gspscInteRate = other.getGspscInteRate();
                if (this$gspscInteRate == null) {
                    if (other$gspscInteRate != null) {
                        return false;
                    }
                } else if (!this$gspscInteRate.equals(other$gspscInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GiftCardOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        Object $gsphVoucherId = this.getGsphVoucherId();
        result = result * 59 + ($gsphVoucherId == null ? 43 : $gsphVoucherId.hashCode());
        Object $promotionContent = this.getPromotionContent();
        result = result * 59 + ($promotionContent == null ? 43 : $promotionContent.hashCode());
        Object $promotionName = this.getPromotionName();
        result = result * 59 + ($promotionName == null ? 43 : $promotionName.hashCode());
        Object $promotionTypeId = this.getPromotionTypeId();
        result = result * 59 + ($promotionTypeId == null ? 43 : $promotionTypeId.hashCode());
        Object $promotionType = this.getPromotionType();
        result = result * 59 + ($promotionType == null ? 43 : $promotionType.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proId = this.getProId();
        result = result * 59 + ($proId == null ? 43 : $proId.hashCode());
        Object $serial = this.getSerial();
        result = result * 59 + ($serial == null ? 43 : $serial.hashCode());
        Object $gsphBeginDate = this.getGsphBeginDate();
        result = result * 59 + ($gsphBeginDate == null ? 43 : $gsphBeginDate.hashCode());
        Object $gsphEndDate = this.getGsphEndDate();
        result = result * 59 + ($gsphEndDate == null ? 43 : $gsphEndDate.hashCode());
        Object $gsphBeginTime = this.getGsphBeginTime();
        result = result * 59 + ($gsphBeginTime == null ? 43 : $gsphBeginTime.hashCode());
        Object $gsphEndTime = this.getGsphEndTime();
        result = result * 59 + ($gsphEndTime == null ? 43 : $gsphEndTime.hashCode());
        Object $gsphDateFrequency = this.getGsphDateFrequency();
        result = result * 59 + ($gsphDateFrequency == null ? 43 : $gsphDateFrequency.hashCode());
        Object $gsphWeekFrequency = this.getGsphWeekFrequency();
        result = result * 59 + ($gsphWeekFrequency == null ? 43 : $gsphWeekFrequency.hashCode());
        Object $sameOrDiff = this.getSameOrDiff();
        result = result * 59 + ($sameOrDiff == null ? 43 : $sameOrDiff.hashCode());
        Object $gspcsCouponType = this.getGspcsCouponType();
        result = result * 59 + ($gspcsCouponType == null ? 43 : $gspcsCouponType.hashCode());
        Object $gspcsCouponRemarks = this.getGspcsCouponRemarks();
        result = result * 59 + ($gspcsCouponRemarks == null ? 43 : $gspcsCouponRemarks.hashCode());
        Object $gspcgActNo = this.getGspcgActNo();
        result = result * 59 + ($gspcgActNo == null ? 43 : $gspcgActNo.hashCode());
        Object $gspcsBeginDate = this.getGspcsBeginDate();
        result = result * 59 + ($gspcsBeginDate == null ? 43 : $gspcsBeginDate.hashCode());
        Object $gspcsEndDate = this.getGspcsEndDate();
        result = result * 59 + ($gspcsEndDate == null ? 43 : $gspcsEndDate.hashCode());
        Object $gspcsBeginTime = this.getGspcsBeginTime();
        result = result * 59 + ($gspcsBeginTime == null ? 43 : $gspcsBeginTime.hashCode());
        Object $gspcsEndTime = this.getGspcsEndTime();
        result = result * 59 + ($gspcsEndTime == null ? 43 : $gspcsEndTime.hashCode());
        Object $gspcsSingleUseAmt = this.getGspcsSingleUseAmt();
        result = result * 59 + ($gspcsSingleUseAmt == null ? 43 : $gspcsSingleUseAmt.hashCode());
        Object $gspcsSinglePaycheckAmt = this.getGspcsSinglePaycheckAmt();
        result = result * 59 + ($gspcsSinglePaycheckAmt == null ? 43 : $gspcsSinglePaycheckAmt.hashCode());
        Object $gspscInteFlag = this.getGspscInteFlag();
        result = result * 59 + ($gspscInteFlag == null ? 43 : $gspscInteFlag.hashCode());
        Object $gspscInteRate = this.getGspscInteRate();
        result = result * 59 + ($gspscInteRate == null ? 43 : $gspscInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "GiftCardOutData(clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ", gsphVoucherId=" + this.getGsphVoucherId() + ", promotionContent=" + this.getPromotionContent() + ", promotionName=" + this.getPromotionName() + ", promotionTypeId=" + this.getPromotionTypeId() + ", promotionType=" + this.getPromotionType() + ", proName=" + this.getProName() + ", proId=" + this.getProId() + ", serial=" + this.getSerial() + ", gsphBeginDate=" + this.getGsphBeginDate() + ", gsphEndDate=" + this.getGsphEndDate() + ", gsphBeginTime=" + this.getGsphBeginTime() + ", gsphEndTime=" + this.getGsphEndTime() + ", gsphDateFrequency=" + this.getGsphDateFrequency() + ", gsphWeekFrequency=" + this.getGsphWeekFrequency() + ", sameOrDiff=" + this.getSameOrDiff() + ", gspcsCouponType=" + this.getGspcsCouponType() + ", gspcsCouponRemarks=" + this.getGspcsCouponRemarks() + ", gspcgActNo=" + this.getGspcgActNo() + ", gspcsBeginDate=" + this.getGspcsBeginDate() + ", gspcsEndDate=" + this.getGspcsEndDate() + ", gspcsBeginTime=" + this.getGspcsBeginTime() + ", gspcsEndTime=" + this.getGspcsEndTime() + ", gspcsSingleUseAmt=" + this.getGspcsSingleUseAmt() + ", gspcsSinglePaycheckAmt=" + this.getGspcsSinglePaycheckAmt() + ", gspscInteFlag=" + this.getGspscInteFlag() + ", gspscInteRate=" + this.getGspscInteRate() + ")";
    }
}
