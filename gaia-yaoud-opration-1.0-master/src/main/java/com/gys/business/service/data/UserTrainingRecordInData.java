package com.gys.business.service.data;

import lombok.Data;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

/**
 * @desc: 新增员工培训查询入参
 * @author: ZhangChi
 * @createTime: 2022/1/3 21:28
 */
@Data
public class UserTrainingRecordInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private List<String> gutrBrId;

    /**
     * 姓名
     */
    private String gutrUserName;

    /**
     * 起始时间
     */
    private String gutrDateStart;
    private Date startDate;

    /**
     * 终止时间
     */
    private String gutrDateEnd;
    private Date endDate;

    /**
     * 培训类别
     */
    private String gutrCategory;

    /**
     * 培训内容
     */
    private String gutrContent;

    /**
     * 培训教师
     */
    private String gutrTrainer;
}
