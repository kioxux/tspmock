package com.gys.business.service;


import com.gys.business.mapper.entity.GaiaSafetyHealthRecord;
import com.gys.common.response.Result;

import java.util.List;

public interface GaiaSafetyHealthRecordService {


    void add(GaiaSafetyHealthRecord inData);

    void delete(GaiaSafetyHealthRecord inData);

    void update(GaiaSafetyHealthRecord inData);

    List<GaiaSafetyHealthRecord> query(GaiaSafetyHealthRecord inData);

    void submit(GaiaSafetyHealthRecord inData);

    Result export(GaiaSafetyHealthRecord inData);

    Result getUserList(GaiaSafetyHealthRecord inData);
}
