package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProductSpecialService {

    JsonResult getSelectFrame(SelectFrameInData inData);

    JsonResult getProductSpecialList(GaiaProductSpecialInData inData);

    JsonResult addProductSpecial(AddProductSpecialInData inData);

    JsonResult invalidProductSpecial(List<GaiaProductSpecial> productList, GetLoginOutData inData);

    JsonResult importData(MultipartFile file, GetLoginOutData userInfo);

    Result exportData(GaiaProductSpecialInData inData);

    JsonResult editProductSpecial(EditProductSpecialInData inData);

    JsonResult editProductSpecialRecord(GaiaProductSpecialEditRecordInData inData);

    Result exportEditProductSpecialRecord(GaiaProductSpecialEditRecordInData inData);

    JsonResult addProductSpecialParam(ProductSpecialParamInData inData, GetLoginOutData userInfo);

    JsonResult editProductSpecialParam(ProductSpecialParamInData inData, GetLoginOutData userInfo);

    JsonResult importProductSpecialParam(MultipartFile file, GetLoginOutData userInfo);

    JsonResult getProductSpecialParamList(GaiaProductSpecialParam inData);

    Result exportProductSpecialParam(GaiaProductSpecialParam inData);

    JsonResult getParamEditRecord(GaiaProductSpecialParam inData);

    Result exportParamEditRecord(GaiaProductSpecialParam inData);

    JsonResult getProductInfo(ProductBasicInfoInData inData);

    List<SpecialClassifcationOutData> specialDrugsInquiry(SpecialClassifcationInData inData);

    Result exportDrugsDetail(SpecialClassifcationInData inData);
}
