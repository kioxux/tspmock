package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.data.*;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.MQReceiveData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.util.CommonUtil;
import com.gys.util.Util;
import com.gys.util.ZipUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 缓存
 *
 * @author xiaoyuan on 2021/1/7
 */
@Service
@Slf4j
public class CacheServiceImpl implements CacheService {

    private Gson gson = new Gson();

    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;
    @Autowired
    private GaiaSdMemberClassMapper memberClassMapper;
    @Autowired
    private GaiaProductBusinessMapper businessMapper;
    @Autowired
    private GaiaWmsPihaoKongzhiMapper pihaoKongzhiMapper;
    @Autowired
    private GaiaWmsMenDianHuoWeiHaoMapper huoWeiHaoMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;
    @Autowired
    private GaiaTaxCodeMapper taxCodeMapper;
    @Autowired
    private GaiaSupplierBusinessMapper supplierBusinessMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private GaiaSdStoreDataMapper gaiaSdStoreDataMapper;
    @Autowired
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Autowired
    private GaiaSdStockMapper sdStockMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetMapper gaiaSdIntegralExchangeSetMapper;
    @Autowired
    private GaiaSdPromHySetMapper gaiaSdPromHySetMapper;
    @Autowired
    private GaiaSdPromHyrDiscountMapper gaiaSdPromHyrDiscountMapper;
    @Autowired
    private GaiaSdPromHyrPriceMapper gaiaSdPromHyrPriceMapper;
    @Autowired
    private GaiaSdProuctLocationMapper gaiaSdProuctLocationMapper;
    @Autowired
    private GaiaSdPromUnitarySetMapper gaiaSdPromUnitarySetMapper;
    @Autowired
    private GaiaSdPromSeriesSetMapper gaiaSdPromSeriesSetMapper;
    @Autowired
    private GaiaSdPromSeriesCondsMapper gaiaSdPromSeriesCondsMapper;
    @Autowired
    private GaiaSdPromHeadMapper gaiaSdPromHeadMapper;
    @Autowired
    private GaiaSdPromGiftSetMapper gaiaSdPromGiftSetMapper;
    @Autowired
    private GaiaSdPromGiftResultMapper gaiaSdPromGiftResultMapper;
    @Autowired
    private GaiaSdPromGiftCondsMapper gaiaSdPromGiftCondsMapper;
    @Autowired
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Autowired
    private GaiaSdPrintSaleMapper gaiaSdPrintSaleMapper;
    @Autowired
    private GaiaSdPaymentMethodMapper gaiaSdPaymentMethodMapper;
    @Autowired
    private GaiaDataSynchronizedMapper synchronizedMapper;
    @Autowired
    private GaiaProductRelateMapper gaiaProductRelateMapper;
    @Autowired
    private GaiaAuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;
    @Autowired
    private GaiaUserUsemapMapper userUsemapMapper;
    @Autowired
    private GaiaSdSystemParaMapper paraMapper;
    @Autowired
    private GaiaClSystemParaMapper clParaMapper;
    @Autowired
    private GaiaSdElectronChangeMapper gaiaSdElectronChangeMapper;//电子券异动表
    @Autowired
    private GaiaSdProductsGroupMapper gaiaSdProductsGroupMapper;
    @Autowired
    private GaiaSynchronousAuditStockMapper gaiaSynchronousAuditStockMapper;
    @Autowired
    private GaiaSdDoctorAddMapper gaiaSdDoctorAddMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetDetailMapper gaiaSdIntegralExchangeSetDetailMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetHeadMapper gaiaSdIntegralExchangeSetHeadMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetProDetailMapper gaiaSdIntegralExchangeSetProDetailMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetTimeMapper gaiaSdIntegralExchangeSetTimeMapper;
    @Autowired
    private GaiaSdRightingRecordMapper rightingRecordMapper;

    private static final ScheduledThreadPoolExecutor EXECUTOR = new ScheduledThreadPoolExecutor(7, r -> {
        Thread thread = new Thread(r, "table sync worker");
        thread.setDaemon(true);
        return thread;
    });
    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Autowired
    private GaiaSynchronousAuditMapper synchronousAuditMapper;

    @Autowired
    private GaiaSdRechargeCardMapper gaiaSdRechargeCardMapper;

    @Autowired
    private GaiaSdIntegralCashSetMapper cashSetMapper;

    @Autowired
    private GaiaSdSaleHMapper gaiaSdSaleHMapper;

    @Autowired
    private RabbitTemplateHelper rabbitTemplate;

    @Autowired
    private GaiaComputerInformationMapper gaiaComputerInformationMapper;

    @Autowired
    private GaiaSdPromChmedSetMapper gaiaSdPromChmedSetMapper;

    @Autowired
    private GaiaSdIntegralRateSetMapper gaiaSdIntegralRateSetMapper;

    @Autowired
    private GaiaSdIntegralRateStoMapper gaiaSIntegralRateStoMapper;

    @Autowired
    private GaiaStoreThirdPaymentMapper gaiaStoreThirdPaymentMapper;

    @Autowired
    private ProductLimitSetMapper productLimitSetMapper;
    //
    @Autowired
    private ProductLimitStoMapper productLimitStoMapper;

    @Autowired
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;

    @Autowired
    private GaiaLiquidationMapper gaiaLiquidationMapper;
//
//    @Autowired
//    private GaiaMaterialControlMapper gaiaMaterialControlMapper;


    @Override
    public void getAllCache(GetLoginOutData userInfo, boolean flag, HttpServletResponse response) {
        long start = System.currentTimeMillis();
        int dirIndex = ThreadLocalRandom.current().nextInt(0000, 9999);
        String dirPrefix = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_PATTERN));
        String dirName = dirPrefix + dirIndex;
        File dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String format = DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN);
        CountDownLatch countDownLatch = new CountDownLatch(flag ? 48 : 37);
        tableFileWriteData(dirName, "GAIA_SD_PROM_HYR_PRICE", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_SERIES_CONDS", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_HEAD", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_GIFT_RESULT", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_HY_SET", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_GIFT_CONDS", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_GIFT_SET", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_UNITARY_SET", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_HYR_DISCOUNT", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROM_SERIES_SET", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_SYSTEM_PARA", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_CL_SYSTEM_PARA", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_MEMBER_CLASS", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_PRODUCT_RELATE", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PRINT_SALE", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_TAX_CODE", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_INTEGRAL_EXCHANGE_SET", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PAYMENT_METHOD", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_PROUCT_LOCATION", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_WMS_MENDIANHUOWEIHAO", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_SD_INTEGRAL_CASH_SET", countDownLatch, userInfo, format);
        tableFileWriteData(dirName, "GAIA_USER_DATA", countDownLatch, userInfo, format);//用户表
        tableFileWriteData(dirName, "GAIA_AUTHCONFI_DATA", countDownLatch, userInfo, format);//权限分配表
        tableFileWriteData(dirName, "GAIA_SD_DOCTOR_ADD", countDownLatch, userInfo, format);//医生加点表
        tableFileWriteData(dirName, "GAIA_SD_PROM_CHMED_SET", countDownLatch, userInfo, format);//中药促销设置表
        tableFileWriteData(dirName, "dateTime", countDownLatch, userInfo, format);//客户端需要有个同步数据表时的服务器时间
        tableFileWriteData(dirName, "GAIA_SD_INTEGRAL_RATE_SET", countDownLatch, userInfo, format);//会员不积分设置表
        tableFileWriteData(dirName, "GAIA_SD_INTEGRAL_RATE_STO", countDownLatch, userInfo, format);//会员不积分门店表
        tableFileWriteData(dirName,"GAIA_STORE_THIRD_PAYMENT",countDownLatch, userInfo, format);//第三方门店配置表
        tableFileWriteData(dirName,"GAIA_SD_PRODUCT_LIMIT_SET",countDownLatch, userInfo, format);//会员限购表
        tableFileWriteData(dirName,"GAIA_SD_PRODUCT_LIMIT_STO",countDownLatch, userInfo, format);//会员限购表
        tableFileWriteData(dirName,"GAIA_SD_STORES_GROUP",countDownLatch, userInfo, format);//会员限购表
        tableFileWriteData(dirName,"GAIA_SD_INTEGRAL_EXCHANGE_SET_DETAIL", countDownLatch, userInfo, format);//新积分换购商品详情表
        tableFileWriteData(dirName,"GAIA_SD_INTEGRAL_EXCHANGE_SET_HEAD", countDownLatch, userInfo, format);//新积分换购商品设置主表
        tableFileWriteData(dirName,"GAIA_SD_INTEGRAL_EXCHANGE_SET_PRO_DETAIL", countDownLatch, userInfo, format);//新积分换购商品生效日期表
        tableFileWriteData(dirName,"GAIA_SD_INTEGRAL_EXCHANGE_SET_TIME", countDownLatch, userInfo, format);//新积分换购商品设置时间段
        tableFileWriteData(dirName,"GAIA_LIQUIDATION", countDownLatch, userInfo, format);//医保清算表

        if (flag) {//11张
            tableFileWriteData(dirName, "GAIA_SD_STORE_DATA", countDownLatch, userInfo, format);//门店主数据配置表
            tableFileWriteData(dirName, "GAIA_WMS_PIHAOKONGZHI", countDownLatch, userInfo, format);//批号控制表
            tableFileWriteData(dirName, "GAIA_SUPPLIER_BUSINESS", countDownLatch, userInfo, format);//供应商主数据表
            tableFileWriteData(dirName, "GAIA_SD_PRODUCT_PRICE", countDownLatch, userInfo, format);//商品价格表
            tableFileWriteData(dirName, "GAIA_FRANCHISEE", countDownLatch, userInfo, format);//加盟商信息表
            tableFileWriteData(dirName, "GAIA_USER_USEMAP", countDownLatch, userInfo, format);//营业员登记信息表
            tableFileWriteData(dirName, "GAIA_SD_MEMBER_BASIC", countDownLatch, userInfo, format);//会员表
            tableFileWriteData(dirName, "GAIA_SD_MEMBER_CARD", countDownLatch, userInfo, format);//会员表
            tableFileWriteData(dirName, "GAIA_PRODUCT_BUSINESS", countDownLatch, userInfo, format);//商品主数据表
            tableFileWriteData(dirName, "GAIA_SD_STOCK", countDownLatch, userInfo, format); //总库存表
            tableFileWriteData(dirName, "GAIA_SD_STOCK_BATCH", countDownLatch, userInfo, format);//批号批次库存表
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        zipFile(response, dirName);
        ZipUtil.deleteDir(dirName);
        log.info("============同步数据表耗时：" + (System.currentTimeMillis() - start) + "ms");
    }

    //压缩文件
    private void zipFile(HttpServletResponse response, String dirName) {
        FileInputStream fileInputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            ZipUtil.zipDirectory(dirName, dirName + ".zip");
            File file = new File(dirName + ".zip");
            fileInputStream = new FileInputStream(file);
            byte[] bytes = ByteStreams.toByteArray(fileInputStream);
            byteArrayInputStream = new ByteArrayInputStream(bytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (byteArrayInputStream != null) {
                try {
                    byteArrayInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void tableFileWriteData(String dirName, String tableName, CountDownLatch latch, GetLoginOutData userInfo, String format) {
        EXECUTOR.execute(() -> {
            FileWriter fileWriter = null;
            PrintWriter printWriter = null;
            try {
                File file = new File(dirName + File.separator + tableName + ".txt");
                fileWriter = new FileWriter(file);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 8 * 1024);
                printWriter = new PrintWriter(bufferedWriter, true);
                if ("GAIA_SD_MEMBER_BASIC".equals(tableName) || "GAIA_SD_MEMBER_CARD".equals(tableName)) {
                    int num = 80000;//每次查询8W条数据
                    int index = 0;//当前查询的次数
                    int count = getTableDataCount(tableName, userInfo);
                    if (count > num) {
                        log.info("当前会员主表数据" + tableName + "  : " + count);
                        if (count > 80000) {
                            int times = count / num; //需要查询的 次数
                            if (count % num != 0) {
                                times = times + 1;
                            }
                            for (int i = 0; i < times; i++) {
                                log.info(tableName + " 表当前次数 : " + i + "=====  index : " + index);
                                List list = getTableDataLimit(tableName, userInfo, index, num);//会员表分页查询
                                for (Object data : list) {
                                    printWriter.println(JSON.toJSONString(data));
                                }
                                index += num;
                                list.clear();
                            }
                        }
                    } else {
                        List list = getTableData(tableName, userInfo, format);
                        if (CollUtil.isEmpty(list)) {
                            log.info("当前表" + tableName + "数据为空直接返回 ~");
                            return;
                        }
                        for (Object data : list) {
                            printWriter.println(JSON.toJSONString(data));
                        }
                    }
                } else {
                    List list = getTableData(tableName, userInfo, format);
                    if (CollUtil.isEmpty(list)) {
                        log.info("当前表" + tableName + "数据为空直接返回 ~");
                        return;
                    }
                    for (Object data : list) {
                        printWriter.println(JSON.toJSONString(data));
                    }
                }
                printWriter.flush();
            } catch (Exception e) {
                log.error("同步数据,写文件报错: error:{}", e.getMessage());
            } finally {
                if (fileWriter != null) {
                    try {
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (printWriter != null) {
                    printWriter.close();
                }
                latch.countDown();
            }
        });
    }

    private int getTableDataCount(String tableName, GetLoginOutData userInfo) {
        switch (tableName) {
            case "GAIA_SD_MEMBER_BASIC":
                return memberBasicMapper.getBasicCountByClient(userInfo.getClient());//查询总量 //会员表
            case "GAIA_SD_MEMBER_CARD":
                return memberBasicMapper.getCardCountByClient(userInfo.getClient());//查询总量 //会员表
//            case "GAIA_PRODUCT_BUSINESS":
//                return businessMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //商品主数据表
//            case "GAIA_SD_STOCK":
//                return sdStockMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //总库存表
//            case "GAIA_SD_STOCK_BATCH":
//                return gaiaSdStockBatchMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //批号批次库存表
            default:
                return 0;
        }
    }

    private List getTableDataLimit(String tableName, GetLoginOutData userInfo, int index, int num) {
        switch (tableName) {
            case "GAIA_SD_MEMBER_BASIC":
                return memberBasicMapper.getBasicAllByClientLimit(userInfo.getClient(), index, num); //会员表
            case "GAIA_SD_MEMBER_CARD":
                return memberBasicMapper.getCardAllByClientLimit(userInfo.getClient(), index, num); //会员表
            case "GAIA_PRODUCT_BUSINESS":
                return businessMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //商品主数据表
            case "GAIA_SD_STOCK":
                return sdStockMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //总库存表
            case "GAIA_SD_STOCK_BATCH":
                return gaiaSdStockBatchMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //批号批次库存表
            default:
                return null;
        }
    }

    private List getTableData(String tableName, GetLoginOutData userInfo, String format) {
        switch (tableName) {
            case "GAIA_SD_PROM_HYR_PRICE":
                return gaiaSdPromHyrPriceMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_SERIES_CONDS":
                return gaiaSdPromSeriesCondsMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_HEAD":
                return gaiaSdPromHeadMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId(), format);
            case "GAIA_SD_PROM_GIFT_RESULT":
                return gaiaSdPromGiftResultMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_HY_SET":
                return gaiaSdPromHySetMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_GIFT_CONDS":
                return gaiaSdPromGiftCondsMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_GIFT_SET":
                return gaiaSdPromGiftSetMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_UNITARY_SET":
                return gaiaSdPromUnitarySetMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_HYR_DISCOUNT":
                return gaiaSdPromHyrDiscountMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_PROM_SERIES_SET":
                return gaiaSdPromSeriesSetMapper.getAllByClient(userInfo.getClient(), format, userInfo.getDepId());
            case "GAIA_SD_SYSTEM_PARA":
                return paraMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId(), null);
            case "GAIA_CL_SYSTEM_PARA":
                return clParaMapper.getAllByClientAndBrId(userInfo.getClient(), null);
            case "GAIA_SD_MEMBER_CLASS":
                return memberClassMapper.getAllByClientAndBrId(userInfo.getClient());
            case "GAIA_PRODUCT_RELATE":
                return gaiaProductRelateMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_SD_PRINT_SALE":
                return gaiaSdPrintSaleMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_TAX_CODE":
                return taxCodeMapper.getAll();
            case "GAIA_SD_INTEGRAL_EXCHANGE_SET":
                return gaiaSdIntegralExchangeSetMapper.getAllByClient(userInfo.getClient());
            case "GAIA_SD_PAYMENT_METHOD":
                return gaiaSdPaymentMethodMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_SD_PROUCT_LOCATION":
                return gaiaSdProuctLocationMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_WMS_MENDIANHUOWEIHAO":
                return huoWeiHaoMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "dateTime":
                return Lists.newArrayList(new DateTimeDto(synchronizedMapper.getMaxDateTime(userInfo.getClient(), userInfo.getDepId())));  //客户端需要有个同步数据表时的服务器时间
            case "GAIA_SD_STORE_DATA":
                return gaiaSdStoreDataMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_WMS_PIHAOKONGZHI":
                return pihaoKongzhiMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_USER_DATA":
                return userDataMapper.getAllByClient(userInfo.getClient());
            case "GAIA_SUPPLIER_BUSINESS":
                return supplierBusinessMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_SD_PRODUCT_PRICE":
                return gaiaSdProductPriceMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_AUTHCONFI_DATA":
                return authconfiDataMapper.getAllByClient(userInfo.getClient());
            case "GAIA_FRANCHISEE":
                return franchiseeMapper.getAllByClient(userInfo.getClient());
            case "GAIA_USER_USEMAP":
                return userUsemapMapper.getAllByClient(userInfo.getClient());
            case "GAIA_SD_MEMBER_BASIC":
                return memberBasicMapper.getAllByClientAndBrId(userInfo.getClient()); //会员表
            case "GAIA_SD_MEMBER_CARD":
                return memberBasicMapper.getCardByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //会员表
            case "GAIA_PRODUCT_BUSINESS":
                return businessMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //商品主数据表
            case "GAIA_SD_STOCK":
                return sdStockMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //总库存表
            case "GAIA_SD_STOCK_BATCH":
                return gaiaSdStockBatchMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId()); //批号批次库存表
            case "GAIA_SD_INTEGRAL_CASH_SET":
                return this.cashSetMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());//积分抵现设置
            case "GAIA_SD_DOCTOR_ADD":
                return this.gaiaSdDoctorAddMapper.getAllByClient(userInfo.getClient());//医生加点表
            case "GAIA_SD_PROM_CHMED_SET":
                return this.gaiaSdPromChmedSetMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId(), format);//中药促销设置表
            case "GAIA_SD_INTEGRAL_RATE_SET":
                return this.gaiaSdIntegralRateSetMapper.getAllByClient(userInfo.getClient(), userInfo.getDepId(), format);//会员不积分设置表
            case "GAIA_SD_INTEGRAL_RATE_STO":
                return this.gaiaSIntegralRateStoMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());//会员不积分门店表
            case "GAIA_STORE_THIRD_PAYMENT":
                return this.gaiaStoreThirdPaymentMapper.getAllByClient(userInfo.getClient());
            case "GAIA_SD_PRODUCT_LIMIT_SET":
                return this.productLimitSetMapper.getAllByClient(userInfo.getClient());//会员限购
            case "GAIA_SD_PRODUCT_LIMIT_STO":
                return this.productLimitStoMapper.getAllByClientAndBrId(userInfo.getClient(), userInfo.getDepId());//会员限购
            case "GAIA_SD_STORES_GROUP":
                return this.gaiaSdStoresGroupMapper.getAllByClient(userInfo.getClient());//门店分类表
            case "GAIA_SD_INTEGRAL_EXCHANGE_SET_DETAIL":
                return this.gaiaSdIntegralExchangeSetDetailMapper.getAllByClient(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_SD_INTEGRAL_EXCHANGE_SET_HEAD":
                return this.gaiaSdIntegralExchangeSetHeadMapper.getAllByClient(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_SD_INTEGRAL_EXCHANGE_SET_PRO_DETAIL":
                return this.gaiaSdIntegralExchangeSetProDetailMapper.getAllByClient(userInfo.getClient(), userInfo.getDepId());
            case "GAIA_SD_INTEGRAL_EXCHANGE_SET_TIME":
                return this.gaiaSdIntegralExchangeSetTimeMapper.getAllByClient(userInfo.getClient());
            case "GAIA_LIQUIDATION" :
                return this.gaiaLiquidationMapper.getAllByClientAndBrId(userInfo.getClient(),userInfo.getDepId());
            default:
                return null;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addOne(GaiaDataSynchronized aSynchronized) {
        this.synchronizedMapper.insertSelective(aSynchronized);
    }

    @Override
    public Map<String, Object> updateContentAll(GaiaDataSynchronized aSyn) {
        if (StrUtil.isBlank(aSyn.getDatetime())) {
            throw new BusinessException("日期时间为空");
        }
        Map<String, Object> maps = new HashMap<>(8);
        List<GaiaDataSynchronized> synchronizedList = new ArrayList<>();
        if (CollUtil.isNotEmpty(aSyn.getList())) {
            //获取当前sql的最大时间
            List<DateTime> dates = aSyn.getList().stream().map(gaiaDataSynchronized -> DateUtil.parse(gaiaDataSynchronized.getDatetime())).collect(Collectors.toList());
            //表示提交的有数据 大数据更新 //循环获取更新信息
            for (GaiaDataSynchronized aSynchronized : aSyn.getList()) {
                if (StrUtil.isNotBlank(aSynchronized.getArg())) {
                    log.info("当前更新表 : " + aSynchronized.getTableName() + " 数据 : " +  aSynchronized.getArg());
                    Map<String, Object> map = Util.jsonToMap(aSynchronized.getArg());
                    Map<String, Object> everyMap = this.synchronizedMapper.getEveryOne(aSynchronized.getTableName(), map);
                    if (CollUtil.isEmpty(everyMap)) {
                        continue;
                    }
                    aSynchronized.setEveryMap(everyMap);
                    synchronizedList.add(aSynchronized);
                }
            }
            log.info("更新集合 :  " + synchronizedList.size());
            maps.put("list", gson.toJson(synchronizedList));
            maps.put("dateTime", CollUtil.max(dates));
        } else {
            List<GaiaDataSynchronized> all = this.synchronizedMapper.getAllByTime(aSyn);
            if (CollUtil.isNotEmpty(all)) {
                for (GaiaDataSynchronized aSynchronized : all) {
                    if (StrUtil.isNotBlank(aSynchronized.getArg())) {
                        log.info("当前更新表 : " + aSynchronized.getTableName() + " 数据 : " +  aSynchronized.getArg());
                        Map<String, Object> map = Util.jsonToMap(aSynchronized.getArg());
                        Map<String, Object> everyMap = this.synchronizedMapper.getEveryOne(aSynchronized.getTableName(), map);
                        if (CollUtil.isEmpty(everyMap)) {
                            continue;
                        }
                        aSynchronized.setEveryMap(everyMap);
                        synchronizedList.add(aSynchronized);
                    }
                }
                log.info("更新集合 :  " + synchronizedList.size());
                maps.put("list", gson.toJson(synchronizedList));
                maps.put("dateTime", synchronizedList.get(0).getDatetime());
            }
        }
        return maps;
    }

    @Override
    public void insertLists(List<GaiaDataSynchronized> synList) {
        if (CollUtil.isNotEmpty(synList)) {
            this.synchronizedMapper.batchInsert(synList);
        }
    }

    @Override
    public Map<String, String> partialUpdate(GetLoginOutData userInfo) {
        Map<String, String> outData = new HashMap<>(16);
        List<GaiaStoreData> storeData = this.gaiaStoreDataMapper.getAllByClientAndBrId(userInfo.getClient());
        if (CollUtil.isNotEmpty(storeData)) {
            outData.put("GAIA_STORE_DATA", gson.toJson(storeData));
        }
        List<GaiaUserData> userData = this.userDataMapper.getAllByClientById(userInfo.getClient(), userInfo.getUserId());
        if (CollUtil.isNotEmpty(userData)) {
            outData.put("GAIA_USER_DATA", gson.toJson(userData));
        }
        String dateTime = synchronizedMapper.getMaxDateTime(userInfo.getClient(), userInfo.getDepId());
        log.info("当前时间 :  " + dateTime);
        if (StrUtil.isNotBlank(dateTime)) {
            outData.put("dateTime", dateTime);
        }
        outData.put("nowTime", DateUtil.now());
        return outData;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void dataAuditSave(Map<String, String> auditMap) {
        //数据审计
        String list = auditMap.get("auditList");
        if (StrUtil.isNotBlank(list)) {
            List<GaiaSynchronousAudit> auditList = JSON.parseArray(list, GaiaSynchronousAudit.class);
            auditList.forEach(audit -> {
                audit.setClient(StrUtil.toString(auditMap.get("client")));
                audit.setBrId(StrUtil.toString(auditMap.get("brId")));
                audit.setUserId(StrUtil.toString(auditMap.get("userId")));
                audit.setComputerLogo(StrUtil.toString(auditMap.get("store_logo")));
            });
            this.synchronousAuditMapper.insertLists(auditList);
        }
        //电脑数据
        String identifies = auditMap.get("identifies");
        if (StrUtil.isNotBlank(identifies)) {
            GaiaComputerInformation information = JSON.parseObject(identifies, GaiaComputerInformation.class);
            this.gaiaComputerInformationMapper.insert(information);
        }
        //库存审计
        String stockAudit = auditMap.get("stockAudit");
        if (StrUtil.isNotBlank(stockAudit)) {
            GaiaSynchronousAuditStock stockOutData = JSON.parseObject(stockAudit, GaiaSynchronousAuditStock.class);
            stockOutData.setBillDate(new Date());
            this.gaiaSynchronousAuditStockMapper.insert(stockOutData);
        }
    }

    @Override
    public String getAuditMaxDate(GetLoginOutData userInfo, Map<String, String> map) {
        return this.synchronousAuditMapper.getAuditMaxDate(userInfo.getClient(), userInfo.getDepId(), map.get("computerLogo").trim());
    }

    @Override
    public Map<String, String> otherInformation(Map<String, String> map) {
        Map<String, String> resultMap = new HashMap<>(4);
        GaiaSdMemberCardData param = new GaiaSdMemberCardData();
        param.setClient(map.get("client"));
        param.setGsmbcBrId(map.get("gsmbcBrId"));
        param.setGsmbcCardId(map.get("gsmbcCardId"));
        List<ElectronDetailOutData> electronDetailOut = this.gaiaSdElectronChangeMapper.queryCanUseElectron(param);
        List<ElectronDetailOutData> electronDetailOutData = new ArrayList<>();
        if (CollUtil.isNotEmpty(electronDetailOut)) {
            Map<String, List<ElectronDetailOutData>> aa = electronDetailOut.stream().collect(Collectors.groupingBy(ElectronDetailOutData::getGsecId));
            aa.forEach((key, value) -> {
                electronDetailOutData.add(value.stream().max(Comparator.comparing(e -> e.getGsebsCreateDate() + e.getGsebsCreateTime())).get());
            });
            electronDetailOutData.forEach(item -> {
                if (StrUtil.isNotBlank(item.getGsecsProGroup())) {
                    Example example = new Example(GaiaSdProductsGroup.class);
                    example.createCriteria().andEqualTo("clientId", map.get("client")).andEqualTo("gspgId", item.getGsecsProGroup());
                    item.setProGroup(this.gaiaSdProductsGroupMapper.selectByExample(example));
                }
            });
        }
        //电子券数量
        resultMap.put("electronic", StrUtil.toString(electronDetailOutData.size()));
        GaiaSdRechargeCard cardId = this.gaiaSdRechargeCardMapper.getAccountByMemberCardId(map.get("client"), map.get("gsmbcCardId"));
        if (ObjectUtil.isNotEmpty(cardId)) {
            //储值卡
            if (cardId.getGsrcAmt() == null) {
                resultMap.put("cardId", "0");
            } else {
                resultMap.put("cardId", StrUtil.toString(cardId.getGsrcAmt().setScale(2, BigDecimal.ROUND_HALF_UP)));
            }
        }
        //获取当前会员最近购买的药品
        List<String> DrugNames = this.gaiaSdSaleHMapper.getRecentMedicine(map.get("client"), map.get("gsmbcBrId"), map.get("gsmbcCardId"));
        if (CollUtil.isNotEmpty(DrugNames)) {
            if (DrugNames.size() > 1) {
                resultMap.put("drugName_Two", StrUtil.isBlank(DrugNames.get(1)) ? null : DrugNames.get(1));
            }
            resultMap.put("drugName_One", StrUtil.isBlank(DrugNames.get(0)) ? null : DrugNames.get(0));
        }
        return resultMap;
    }

    @Override
    public void realTimeFullAmount(GetLoginOutData userInfo, PartialSynVO synVO) {
        List<String> all = CommonUtil.getAll();
        List<String> tableList = new ArrayList<>();
        all.forEach(tableName -> synVO.getTableName().stream().filter(table -> table.equals(tableName)).forEach(tableList::add));
        if (CollUtil.isEmpty(tableList)) {
            throw new BusinessException("请仔细选择表名!");
        }

        log.info("全量同步,发送MQ 表集合 : " + all);
        log.info("全量同步,发送MQ 门店集合 : " + synVO.getBrIdList());
        log.info("全量同步,发送MQ 加盟商集合 : " + synVO.getClientList());

        synVO.getClientList().forEach(client -> synVO.getBrIdList().stream().filter(detailsVO -> client.equals(detailsVO.getClient())).forEach(detailsVO -> {
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("RealTime");
            mqReceiveData.setData(tableList);
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, client + "." + detailsVO.getBrId(), JSON.toJSON(mqReceiveData));
        }));

    }

    @Override
    public void synchronousData(GetLoginOutData userInfo, Map<String, String> map, HttpServletResponse response) {
        //拿到需要同步数据的表
        String allTables = map.get("tables");
        List<String> list = JSONObject.parseArray(allTables, String.class);

        long start = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(list.size());
        int dirIndex = ThreadLocalRandom.current().nextInt(0000, 9999);
        //创建文件夹
        String dirPrefix = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_PATTERN));
        String dirName = dirPrefix + dirIndex;
        File dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String format = DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN);
        //循环写表
        list.forEach(table -> tableFileWriteData(dirName, table, countDownLatch, userInfo, format));
        //等待所有线程 执行完毕
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //压缩文件
        zipFile(response, dirName);
        //删除压缩
        ZipUtil.deleteDir(dirName);
        System.err.println("============全量同步数据表耗时：" + (System.currentTimeMillis() - start) + "ms");
    }

    @Override
    public PartialSynDto getAllTableNames() {
        PartialSynDto synDto = new PartialSynDto();
        synDto.setClientList(this.gaiaStoreDataMapper.getAllClient());
        synDto.setTableNames(CommonUtil.getAll());
        return synDto;
    }

    @Override
    public List<PartialSynDto> getAllBrIds(Map<String, Object> map) {
        List<String> clients = (List<String>) map.get("clients");
        if (CollUtil.isEmpty(clients)) {
            throw new BusinessException("加盟商不可为空 ~! ");
        }
        return this.gaiaStoreDataMapper.getAllBrIds(clients);
    }

    @Override
    public List<GaiaDataSynchronized> getTheDataYouNeed(GaiaDataSynchronized aSynchronized) {
        return this.synchronizedMapper.getTheDataYouNeed(aSynchronized);
    }

    @Override
    public void insertInformation(GaiaComputerInformation information) {
        this.gaiaComputerInformationMapper.insert(information);
    }


    @Override
    public JsonResult selectAuditByDay(StartDayAndEndDayInData inData) {
        //查询
        List<GaiaSynchronousAuditExt> auditList = gaiaSdSaleDMapper.selectAuditByDay(inData);
        //如果查询为空时，会返回空List 不会返回null
        return JsonResult.success(auditList, "success");
    }

    @Override
    public List<GaiaSdSystemPara> getSystemParaByStore(Map<String, Object> map) {
        //查询
        List<GaiaSdSystemPara> auditList = paraMapper.getAllByClientAndBrId((String) map.get("client"), (String) map.get("depId"), (String) map.get("paraId"));
        //如果查询为空时，会返回空List 不会返回null
        return auditList;
    }

    @Override
    public PageInfo<List<HealthCareQueryDto>> healthCareQuery(Map<String, Object> map) {
        PageHelper.startPage(Integer.parseInt((String) map.get("pageNum")), Integer.parseInt((String) map.get("pageSize")));
        List<HealthCareQueryDto> dtos = this.gaiaSdSaleDMapper.healthCareQuery(map);
        if (CollUtil.isNotEmpty(dtos)) {
            IntStream.range(0, dtos.size()).forEach(i -> (dtos.get(i)).setIndex(i + 1));
        }
        return new PageInfo(dtos);
    }


    @Override
    public JsonResult insertRightingRecord(GaiaSdRightingRecord inData) {
        rightingRecordMapper.insert(inData);//插入
        return JsonResult.success("", "success");
    }
}
