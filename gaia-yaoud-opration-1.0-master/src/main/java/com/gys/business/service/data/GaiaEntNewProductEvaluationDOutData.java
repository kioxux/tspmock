package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 公司-新品评估-明细表(GaiaEntNewProductEvaluationD)实体类
 *
 * @author makejava
 * @since 2021-07-19 10:16:25
 */
@Data
public class GaiaEntNewProductEvaluationDOutData implements Serializable {
    private static final long serialVersionUID = 796644975143025513L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品评估单号
     */
    private String billCode;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 门店ID
     */
    private String storeId;
    /**
     * 销售额
     */
    private Double salesAmt;
    /**
     * 销量
     */
    private Double salesQuantity;
    /**
     * 毛利额
     */
    private Double gross;
    /**
     * 建议处置状态：0-淘汰 1-转正常
     */
    private Integer suggestedStatus;
    /**
     * 商品状态：0-淘汰 1-转正常
     */
    private Integer status;
    private Integer stoQty;
    /**
     * 处理状态：0-未处理 1-已处理
     */
    private Integer dealStatus;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    private String manageAttributes;
    /**
     * 更新时间
     */
    private Date updateTime;
    private Date billDate;
    private String billTime;
    private Date finishTime;
    private Date autoTime;
    /**
     * 评估数量
     */
    private int pgQty;
    private int sendRange;
    /**
     * 更新者
     */
    private String updateUser;

    private String commentClass;
    private String proName;
    private String proSpecs;
    private String proFactoryName;
    //必备状态
    private int mustHaveStatus;
    private String suggestion;
    private String stoName;
    private String statusName;
    private String dealStatusName;
    private BigDecimal dpddprice;
    private BigDecimal classAveragePrice;
    private int salesStoreQty;
    /**
     * 门店数量
     */
    private int storeQty;
    /**
     * 实际评估门店数量
     */
    private int pgStoreQty;
    private int invStoreQty;
    private int zdgQty;
    /**
     * 实际转正常数量
     */
    private int ZCQty;
    /**
     * 实际淘汰数量
     */
    private int TTQty;
    /**
     * 建议转正常数量
     */
    private int suggestedZCQty;
    /**
     * 建议淘汰数量
     */
    private int suggestedTTQty;
    private Double saleRate;
    private String param1;
    private String param2;
    private String order;
    private String proCompBigName;
    private String proBigClassName;

    private List<GaiaEntNewProductEvaluationDOutData> detailList;



}
