package com.gys.business.service;

import com.gys.business.service.data.DailySettlement.DailyReconcileDetailOutData;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailVO;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface DailySettlementService {

    /**
     * 根据当前门店 支付明细
     * @param userInfo
     * @param map
     * @return
     */
    List<DailyReconcileDetailOutData> getSalesInquireList(GetLoginOutData userInfo, Map<String, Object> map);

    /**
     * 日结数据提交
     * @param userInfo
     * @param map
     */
    void reconciliationReview(GetLoginOutData userInfo, DailyReconcileDetailVO map);
}
