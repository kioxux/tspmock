package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class TakeAwayOrderOutData {
    private String client;
    private String stoCode;
    private String index;
    private String orderNo;
    private String platformOrderId;     //平台单号
    private String platform;
    private BigDecimal amt;
    private BigDecimal deliveryFee;
    private String name;
    private String isInvoice;// 是否开票（0：不需要发票1：需要发票）
    private String time;//下单时间
    private String payTime;//支付时间
    private String status;
    private String action;
    private String address;
    private String phone;
    private String consumerRemark;
    private String orderRemark;
    private String ticketNo;
    private String other;
    private String daySeq;//外卖员取货码
    private String orderInfo;//优惠总金额
    private BigDecimal originalPrice;//原价(总价)
    private BigDecimal customerPay;//客户支付
    private Integer deliveryTime;//期望送达时间，0 立即送达; 非0 *1000 期望送达时间
    private String gsshBillNo;//销售单号字段
    private String gsshBillNoReturn;//退货单号字段

    private List<TakeAwayOrderDetailOutData> proList;
    private String returnOrderNo;
}
