package com.gys.business.service.data;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:44 2021/10/11
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class ColdchainInfo {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店Code
     */
    private String stoCode;

    /**
     * 拣货单号
     */
    private String jhdh;

    /**
     * 发运地点
     */
    @Length(max = 40,message = "发运地点长度不能超过40个字符")
    private String departurePlace;

    /**
     * 启运日期
     */
    @Length(max = 8,message = "启运日期长度不能超过8个字符")
    private String departureDate;

    /**
     * 启运时间
     */
    @Length(max = 6,message = "启运时间长度不能超过6个字符")
    private String departureTime;

    /**
     * 启运温度
     */
    @Length(max = 10,message = "启运温度长度不能超过10个字符")
    private String departureTemperature;

    /**
     * 到货件数
     */
    @Digits(integer = 12,fraction = 4,message = "到货件数格式有误！(整数位最大不能超过12位,小数位不超过4位)")
    private BigDecimal boxQty;

    /**
     * 到货日期
     */
    @Length(max = 8,message = "到货日期长度不能超过8个字符")
    private String arriveDate;

    /**
     * 到货时间
     */
    @Length(max = 6,message = "到货时间长度不能超过6个字符")
    private String arriveTime;

    /**
     * 到货温度
     */
    @Length(max = 10,message = "到货温度长度不能超过10个字符")
    private String arriveTemperature;

    /**
     * 运输单位
     */
    @Length(max = 50,message = "运输单位长度不能超过50个字符")
    private String transportOrganization;

    /**
     * 运输方式
     */
    @Length(max = 20,message = "运输方式长度不能超过20个字符")
    private String transportMode;

    /**
     * 运输人员
     */
    @Length(max = 20,message = "运输人员长度不能超过20个字符")
    private String transportEmp;

    /**
     * 发票同行 0否，1是
     */
    @Length(max = 2,message = "发票同行长度不能超过2个字符")
    private String txFollow;

    /**
     * 温度符合 0否，1是
     */
    @Length(max = 2,message = "温度符合长度不能超过2个字符")
    private String tempFit;

    /**
     * 收货员ID,名称
     */
    private String emp;
    private String empName;

    /**
     * 身份证
     */
    @Length(max = 18,message = "身份证长度不能超过18个字符")
    private String transportCardId;

    /**
     * 运输工具(1.厢式货车+冷藏箱，2.冷藏车)
     */
    private String transportTool;

    /**
     * 运输时长
     */
    @Length(max = 50,message = "运输时长不能超过50个字符")
    private String transportTime;

    /**
     * 温控方式(1.冷藏箱压缩机制冷，2.冷藏车制冷；)
     */
    private String tempType;

    public ColdchainInfo() {
        this.client = "";
        this.stoCode = "";
        this.jhdh = "";
        this.departurePlace = "";
        this.departureDate = "";
        this.departureTime = "";
        this.departureTemperature = "";
        this.boxQty = BigDecimal.ZERO;
        this.arriveDate = "";
        this.arriveTime = "";
        this.arriveTemperature = "";
        this.transportOrganization = "";
        this.transportMode = "";
        this.transportEmp = "";
        this.txFollow = "1";
        this.tempFit = "0";
        this.emp = "";
        this.transportCardId = "";
        this.transportTool = "";
        this.transportTime = "";
        this.tempType = "";
    }
}
