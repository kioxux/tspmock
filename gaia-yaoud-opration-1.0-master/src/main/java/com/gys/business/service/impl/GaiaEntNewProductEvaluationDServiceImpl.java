package com.gys.business.service.impl;


import com.gys.business.mapper.GaiaEntNewProductEvaluationDMapper;
import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;
import com.gys.business.service.GaiaEntNewProductEvaluationDService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公司-新品评估-明细表(GaiaEntNewProductEvaluationD)表服务实现类
 *
 * @author makejava
 * @since 2021-07-19 10:16:27
 */
@Service("gaiaEntNewProductEvaluationDService")
public class GaiaEntNewProductEvaluationDServiceImpl implements GaiaEntNewProductEvaluationDService {
    @Resource
    private GaiaEntNewProductEvaluationDMapper gaiaEntNewProductEvaluationDMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GaiaEntNewProductEvaluationD queryById(Long id) {
        return this.gaiaEntNewProductEvaluationDMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaEntNewProductEvaluationD> queryAllByLimit(int offset, int limit) {
        return this.gaiaEntNewProductEvaluationDMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaEntNewProductEvaluationD 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaEntNewProductEvaluationD insert(GaiaEntNewProductEvaluationD gaiaEntNewProductEvaluationD) {
        this.gaiaEntNewProductEvaluationDMapper.insert(gaiaEntNewProductEvaluationD);
        return gaiaEntNewProductEvaluationD;
    }

    /**
     * 修改数据
     *
     * @param gaiaEntNewProductEvaluationD 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaEntNewProductEvaluationD update(GaiaEntNewProductEvaluationD gaiaEntNewProductEvaluationD) {
        this.gaiaEntNewProductEvaluationDMapper.update(gaiaEntNewProductEvaluationD);
        return this.queryById(gaiaEntNewProductEvaluationD.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.gaiaEntNewProductEvaluationDMapper.deleteById(id) > 0;
    }
}
