package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSaletaskHplanNew;
import com.gys.business.mapper.entity.GaiaSaletaskPlanClientStoNew;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.data.AuditData;
import com.gys.business.service.data.GetStoreInData;
import com.gys.business.service.data.MonthOutData;
import com.gys.business.service.data.MonthOutDataVo;
import com.gys.common.data.GetLoginOutData;
import java.util.List;
import java.util.Map;

public interface MonthService {

    //查询
    List<MonthOutData> selectPage(MonthOutData outData);
    //新增
    String insertMonthlySalesPlan(GetLoginOutData userInfo,MonthOutData outData);
    //修改保存
    void modifyMonthlySalesPlan(GetLoginOutData userInfo,MonthOutData outData);
    //删除
    void deleteMonthlySalesPlan(MonthOutData outData);
    //查看月销售任务列表集合
    Map<String, Object> viewSalesTask(MonthOutData outData);
    //任务设置保存
    void storeTaskSetting(GetLoginOutData userInfo,MonthOutData outData);

    //推送用户
    String PushMonthlySalesPlanUser(GetLoginOutData userInfo,MonthOutData outData);

    List<MonthOutData> getStoreBySaleTask(GaiaStoreData storeData);

    //用户新增
    void addedMonthlySalesTaskUsers(GetLoginOutData userInfo,MonthOutData outData);

    //目标试算结果
    Map<String,Object> list(GetLoginOutData userInfo, MonthOutData outData);
    Map<String,Object> targetTrial(GetLoginOutData userInfo, MonthOutData outData);//目标试算结果

    //目标试算
    List<MonthOutDataVo> target(GetLoginOutData userInfo,GaiaSaletaskHplanNew gaiaSaletaskHplanNew);


    //设置
    MonthOutDataVo settingStore(MonthOutDataVo inData);

    //用户列表查询
    List<MonthOutData> monthlySalesTaskListUser(MonthOutData outData);

    //审核

    AuditData auditDate(GetLoginOutData userInfo,MonthOutData outData);

    void audit(GetLoginOutData userInfo,MonthOutData outData);


    //停用
    void stop(GetLoginOutData userInfo,MonthOutData outData);

    //忽略
    void lose(GetLoginOutData userInfo,MonthOutData outData);

    //导入门店查询
    List<MonthOutDataVo> exportIn(GetLoginOutData userInfo,List<GetStoreInData> getStoreInData);

    //插入行
    List<GaiaStoreData> insertRow(GetLoginOutData userInfo);

    //删除行
    void deleteRow(GetLoginOutData userInfo,MonthOutData outData);

    //保存
    void saveStoreData(GetLoginOutData login,MonthOutData outData);
}
