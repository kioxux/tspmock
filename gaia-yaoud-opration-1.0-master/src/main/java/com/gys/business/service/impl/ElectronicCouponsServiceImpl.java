package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ElectronicCouponsService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.vo.GaiaProductBusinessZdysVo;
import com.gys.util.*;
import com.gys.util.ysb.StreamFilterUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * @author xiaoyuan on 2020/9/24
 */
@Slf4j
@Service
@SuppressWarnings("all")
public class ElectronicCouponsServiceImpl implements ElectronicCouponsService {

    //电子券信息表
    @Autowired
    private GaiaSdElectronBasicMapper gaiaSdElectronBasicMapper;

    //电子券异动表
    @Autowired
    private GaiaSdElectronChangeMapper gaiaSdElectronChangeMapper;

    //电子券主题设置表
    @Autowired
    private GaiaSdElectronThemeSetMapper gaiaSdElectronThemeSetMapper;

    //电子券业务基础设置表
    @Autowired
    private GaiaSdElectronBusinessSetMapper gaiaSdElectronBusinessSetMapper;

    //电子券业务条件设置表
    @Autowired
    private GaiaSdElectronConditionSetMapper gaiaSdElectronConditionSetMapper;

    //商品组表
    @Autowired
    private GaiaSdElectronicProductsGroupMapper gaiaSdElectronProductsGroupMapper;

    //电子券自动发券设置表
    @Autowired
    private GaiaSdElectronAutoSetMapper gaiaSdElectronAutoSetMapper;

    @Autowired
    private GaiaProductBusinessMapper businessMapper;

    @Autowired
    private GaiaSdProductsGroupMapper gaiaSdProductsGroupMapper;

    @Autowired
    private GaiaProductBasicMapper productBasicMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;


    /**
     * 电子券设置类表
     *
     * @param inData
     * @return
     */
    @Override
    public List<ElectronBasicOutData> getElectronThemeSetByList(ElectronBasicInData inData) {
        List<ElectronBasicOutData> outData = this.gaiaSdElectronBasicMapper.getElectronThemeSetByList(inData);
        if (CollUtil.isNotEmpty(outData)) {
            IntStream.range(0, outData.size()).forEach(i -> (outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }

    /**
     * 新增电子券
     *
     * @param inVo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertToElectronThemeSet(ElectronThemeSetInVo inVo) {
        if (!Util.isNumber2(inVo.getGsebAmt())) {
            throw new BusinessException(UtilMessage.PLEASE_ENTER_THE_CORRECT_AMOUNT);
        }
        GaiaSdElectronBasic basic = CopyUtil.copy(inVo, GaiaSdElectronBasic.class);
        //当体检类型 为1 非固定面额时
        if ("1".equals(basic.getGsebType())) {
            if (StringUtil.isBlank(basic.getGsebAmt())) {
                throw new BusinessException(UtilMessage.ELECTRON_ERROR);
            }
            //折率 / 100
            BigDecimal rate = Util.divide(Util.checkNull(new BigDecimal(inVo.getGsebAmt())), new BigDecimal(100));
            basic.setGsebAmt(rate.toString());
        }
        GaiaSdElectronBasic sdElectronBasic = this.gaiaSdElectronBasicMapper.selectByPrimaryKey(basic);
        if (ObjectUtil.isNotEmpty(sdElectronBasic)) {
            throw new BusinessException(UtilMessage.E_VOUCHER_ALREADY_EXISTS);
        }
        basic.setGsebStatus(UtilMessage.Y);
        this.gaiaSdElectronBasicMapper.insertSelective(basic);
    }


    /**
     * 修改电子券
     *
     * @param inVo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateToElectronThemeSet(ElectronThemeSetInVo inVo) {
        if (!Util.isNumber2(inVo.getGsebAmt())) {
            throw new BusinessException(UtilMessage.PLEASE_ENTER_THE_CORRECT_AMOUNT);
        }
        GaiaSdElectronBasic basic = CopyUtil.copy(inVo, GaiaSdElectronBasic.class);
        //当体检类型 为1 非固定面额时
        if ("1".equals(basic.getGsebType())) {
            if (StringUtil.isBlank(basic.getGsebAmt())) {
                throw new BusinessException(UtilMessage.ELECTRON_ERROR);
            }
            //折率 / 100
            BigDecimal rate = Util.divide(Util.checkNull(new BigDecimal(inVo.getGsebAmt())), new BigDecimal(100));
            basic.setGsebAmt(rate.toString());
        }
        GaiaSdElectronBasic sdElectronBasic = this.gaiaSdElectronBasicMapper.selectByPrimaryKey(basic);
        if (ObjectUtil.isEmpty(sdElectronBasic)) {
            throw new BusinessException(UtilMessage.THE_COUPON_DOES_NOT_EXIST);
        }
        this.gaiaSdElectronBasicMapper.updateByPrimaryKey(basic);
    }

    /**
     * 获得电子券号
     *
     * @param client
     * @return
     */
    @Override
    public String getGsebIdToElectronThemeSet(String client) {
        return this.gaiaSdElectronBasicMapper.getGsebIdToElectronThemeSet(client);
    }


    public GaiaSdElectronicProductsGroup getGaiaSdElectronicProductsGroupTwo(ElectronVoucherInVO inVo, String proId, String gsebsId) {
        GaiaSdElectronicProductsGroup productsGroup = new GaiaSdElectronicProductsGroup();
        productsGroup.setClient(inVo.getClient());
        productsGroup.setGspgId(gsebsId);//商品组 id
        productsGroup.setGspgProId(proId);
        productsGroup.setGspgValid(UtilMessage.Y);//是否有效
        return productsGroup;
    }

    /**
     * 设置商品组信息
     *
     * @param inVo
     * @param gsebsId
     * @param gspgProId
     * @return
     */
    public GaiaSdElectronicProductsGroup getGaiaSdElectronicProductsGroup(ElectronicAllData inVo, String gsebsId, String gspgProId) {
        GaiaSdElectronicProductsGroup productsGroup = new GaiaSdElectronicProductsGroup();
        productsGroup.setClient(inVo.getClient());
        productsGroup.setGspgId(gsebsId);//商品组id
        productsGroup.setGspgProId(gspgProId);
        productsGroup.setGspgValid(UtilMessage.Y);//是否有效
        return productsGroup;
    }

    public GaiaSdElectronConditionSet getGaiaSdElectronConditionSet(ElectronVoucherInVO inVo, String gsebsId, ElectronChildInVO childInVO) {
        if (StrUtil.isBlank(childInVO.getGsecsReachQty1()) && StrUtil.isBlank(childInVO.getGsecsReachAmt1())) {
            throw new BusinessException(UtilMessage.THE_INPUT_INFORMATION_IS_WRONG);
        }
        if (StrUtil.isNotBlank(childInVO.getGsecsReachQty1()) && StrUtil.isNotBlank(childInVO.getGsecsReachAmt1())) {
            throw new BusinessException(UtilMessage.CANNOT_EXIST_AT_THE_SAME_TIME);
        }
        if (StrUtil.isNotBlank(childInVO.getGsecsReachQty1()) && !Util.quantityJudgment(childInVO.getGsecsReachQty1())) {
            throw new BusinessException(UtilMessage.THE_INPUT_QUANTITY_IS_WRONG);
        }
        if (StrUtil.isNotBlank(childInVO.getGsecsReachAmt1()) && !Util.isNumber2(childInVO.getGsecsReachAmt1())) {
            throw new BusinessException(UtilMessage.WRONG_AMOUNT);
        }

        //电子券业务条件设置表
        GaiaSdElectronConditionSet conditionSet = CopyUtil.copy(childInVO, GaiaSdElectronConditionSet.class);
        conditionSet.setGsebsId(gsebsId);//业务单号
        conditionSet.setClient(inVo.getClient());
        conditionSet.setGsetsId(inVo.getGsetsId());//主题号
        conditionSet.setGsecsFlag1(StrUtil.isBlank(childInVO.getGsecsFlag1()) ? UtilMessage.N : childInVO.getGsecsFlag1());
        if (StrUtil.isNotBlank(inVo.getGroupId()) && CollUtil.isNotEmpty(childInVO.getGspgProIds())) {
            conditionSet.setGsecsProGroup(gsebsId);
        }
        conditionSet.setGsecsValid(UtilMessage.Y);//是否有效
        conditionSet.setGsecsCreateDate(inVo.getGsebsCreateDate());
        conditionSet.setGsecsCreateTime(inVo.getGsebsCreateTime());
        return conditionSet;
    }

    public List<String> getBrIds(String client, String brStart, String brEnd, String storeType, String storeGroup, List<String> storeds) {
        List<String> storeIds = new ArrayList();
        Example example;
        List<GaiaStoreData> list;
        if ("1".equals(storeType)) {
            list = this.storeDataMapper.selectByGaiaStoreData(client, brStart, brEnd);
            storeIds = list.stream().map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        } else if ("2".equals(storeType)) {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", client).andEqualTo("stoStatus", "0").andEqualTo("stogCode", storeGroup);
            list = this.storeDataMapper.selectByExample(example);
            storeIds = list.stream().map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        } else if ("3".equals(storeType)) {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", client).andEqualTo("stoStatus", "0").andIn("stoCode", storeds);
            list = this.storeDataMapper.selectByExample(example);
            storeIds = list.stream().map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        }
        return storeIds;
    }

    /**
     * 商品组条件判断去重
     *
     * @param groupList         传参
     * @param productsGroupList 查询出来的
     */
    public void DeduplicationProductsGroup(List<GaiaSdElectronicProductsGroup> groupList, List<GaiaSdElectronicProductsGroup> productsGroupList) {
        if (CollUtil.isNotEmpty(groupList)) {
            groupList.stream().filter(productsGroup -> StrUtil.isBlank(productsGroup.getGspgProId())).forEach(productsGroup -> {
                throw new BusinessException("商品编码不可为空!");
            });
            List<GaiaSdElectronicProductsGroup> groups = groupList.stream().filter(item -> !groupList.contains(item)).collect(toList());
            if (CollUtil.isNotEmpty(groups)) {
                throw new BusinessException(UtilMessage.PRODUCT_GROUP_CODE_IS_DUPLICATED);
            }
//            productsGroupList.forEach(productsGroup -> groupList.stream().filter(group -> group.getClient().equals(productsGroup.getClient()) && group.getGspgId().equals(productsGroup.getGspgId())).forEach(group -> {
//                throw new BusinessException(UtilMessage.PRODUCT_GROUP_CODE_IS_DUPLICATED);
//            }));

        }
    }

    /**
     * 电子券业务条件设置表条件判断去重
     *
     * @param conditionSetList
     * @param sdElectronConditionSets
     */
    public void DeduplicationConditionSet(List<GaiaSdElectronConditionSet> conditionSetList, List<GaiaSdElectronConditionSet> sdElectronConditionSets) {
        if (CollUtil.isNotEmpty(conditionSetList)) {
            List<GaiaSdElectronConditionSet> conditionSets = conditionSetList.stream().filter(item -> !conditionSetList.contains(item)).collect(toList());
            if (CollUtil.isNotEmpty(conditionSets)) {
                throw new BusinessException(UtilMessage.THERE_IS_A_DUPLICATE_E_VOUCHER_ACTIVITY_NUMBER);
            }

            sdElectronConditionSets.stream().filter(sdElectronConditionSet -> conditionSetList.stream().anyMatch(conditionSet -> conditionSet.getClient().equals(sdElectronConditionSet.getClient()) && conditionSet.getGsetsId().equals(sdElectronConditionSet.getGsetsId())
                    && conditionSet.getGsebsId().equals(sdElectronConditionSet.getGsebsId()) && conditionSet.getGsebId().equals(sdElectronConditionSet.getGsebId()))).forEach(sdElectronConditionSet -> {
                throw new BusinessException(UtilMessage.THERE_IS_A_DUPLICATE_E_VOUCHER_ACTIVITY_NUMBER);
            });
        }
    }

    /**
     * 电子券业务基础设置表条件判断去重
     *
     * @param businessSetList 准备添加的参数
     * @param businessSets    查询出来的参数
     */
    public void DeduplicationBusiness(List<GaiaSdElectronBusinessSet> businessSetList, List<GaiaSdElectronBusinessSet> businessSets) {
        if (CollUtil.isNotEmpty(businessSetList)) {
            //自己和自己比较去重
            List<GaiaSdElectronBusinessSet> sets = businessSetList.stream().filter(item -> !businessSetList.contains(item)).collect(toList());
            if (CollUtil.isNotEmpty(sets)) {
                throw new BusinessException(UtilMessage.DUPLICATE_SHOP_NUMBER);
            }
            businessSets.stream().filter(sdElectronBusinessSet -> businessSetList.stream().anyMatch(businessSet -> businessSet.getClient().equals(sdElectronBusinessSet.getClient()) && businessSet.getGsetsId().equals(sdElectronBusinessSet.getGsetsId())
                    && businessSet.getGsebsType().equals(sdElectronBusinessSet.getGsebsType()) && businessSet.getGsebsId().equals(sdElectronBusinessSet.getGsebsId())
                    && businessSet.getGsebsBrId().equals(sdElectronBusinessSet.getGsebsBrId()))).forEach(sdElectronBusinessSet -> {
                throw new BusinessException(UtilMessage.DUPLICATE_SHOP_NUMBER);
            });
        }
    }


    /**
     * 获得主题单号列表
     *
     * @param inData
     * @return
     */
    public List<ThemeSettingOutData> getthemeSettingListById(ThemeSettingListInData inData) {
        List<ThemeSettingOutData> outData = this.gaiaSdElectronThemeSetMapper.getthemeSettingListById(inData.getClient(), inData.getGsetsFlag());
        if (CollUtil.isEmpty(outData)) {
            throw new BusinessException(UtilMessage.NO_SUBJECT_UNDER_CURRENT_FRANCHISEE);
        }
        return outData;
    }

    /**
     * 商品组模糊查询
     *
     * @param inData
     * @return
     */
    @Override
    public List<FuzzyQueryOfProductGroupOutData> fuzzyQueryOfProductGroup(FuzzyQueryOfProductGroupInData inData) {
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }

        List<FuzzyQueryOfProductGroupOutData> outData = this.gaiaSdElectronProductsGroupMapper.fuzzyQueryOfProductGroup(inData);
        if (CollUtil.isNotEmpty(outData)) {
            IntStream.range(0, outData.size()).forEach(i -> (outData.get(i)).setIndex(i + 1));
        }
        List<FuzzyQueryOfProductGroupOutData> collect = outData.stream().filter(StreamFilterUtils.distinctByKey((p) -> (p.getProCode()))).collect(toList());
        return collect;
    }

    /**
     * 查询当前会员在当前门店可用电子券
     *
     * @param inData 会员信息
     * @return
     */
    @Override
    public List<ElectronDetailOutData> queryCanUseElectron(GaiaSdMemberCardData inData) {
        List<ElectronDetailOutData> electronDetailOut = gaiaSdElectronChangeMapper.queryCanUseElectron(inData);
        List<ElectronDetailOutData> electronDetailOutData = new ArrayList<>();
        if (CollUtil.isNotEmpty(electronDetailOut)) {
            Map<String, List<ElectronDetailOutData>> aa = electronDetailOut.stream().collect(Collectors.groupingBy(ElectronDetailOutData::getGsecId));
            aa.forEach((key, value) -> {
                electronDetailOutData.add(value.stream().max(Comparator.comparing(e -> e.getGsebsCreateDate() + e.getGsebsCreateTime())).get());
            });
            electronDetailOutData.forEach(item -> {
                if (StrUtil.isNotBlank(item.getGsecsProGroup())) {
                    Example example = new Example(GaiaSdProductsGroup.class);
                    example.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gspgId", item.getGsecsProGroup());
                    item.setProGroup(gaiaSdProductsGroupMapper.selectByExample(example));
                }
            });
        }
        return electronDetailOutData;
    }

    /**
     * 查询当前门店可赠送电子券
     *
     * @param inData 门店信息
     * @return
     */
    @Override
    public List<ElectronDetailOutData> selectGiftCoupons(GaiaSdStoreData inData) {
        List<ElectronDetailOutData> electronDetailOut = gaiaSdElectronChangeMapper.selectGiftCoupons(inData);
        List<ElectronDetailOutData> electronDetailOutData = new ArrayList<>();
        if (CollUtil.isNotEmpty(electronDetailOut)) {

            Map<String, List<ElectronDetailOutData>> map = new HashMap<>();
            for (ElectronDetailOutData detailOutData : electronDetailOut) {
                List<ElectronDetailOutData> detail = new ArrayList<>();
                for (ElectronDetailOutData outData : electronDetailOut) {
                    if (detailOutData.getGsebId().equals(outData.getGsebId())) {
                        detail.add(outData);
                    }
                }
                map.put(detailOutData.getGsebId(), detail);
            }

            for (Map.Entry<String, List<ElectronDetailOutData>> entry : map.entrySet()) {
                String key = entry.getKey();
                List<ElectronDetailOutData> value = entry.getValue();
                if (CollUtil.isNotEmpty(value)) {
                    electronDetailOutData.add(value.stream().max(Comparator.comparing(e -> e.getGsebsCreateDate() + e.getGsebsCreateTime())).get());
                }
            }

            electronDetailOutData.forEach(item -> {
                if (StrUtil.isNotBlank(item.getGsecsProGroup())) {
                    Example example = new Example(GaiaSdProductsGroup.class);
                    example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gspgId", item.getGsecsProGroup());
                    item.setProGroup(gaiaSdProductsGroupMapper.selectByExample(example));
                }
            });
        }
        return electronDetailOutData;
    }

    @Override
    public List<ElectronDetailOutData> selectGiftCouponsAndUse(GaiaSdStoreData inData) {
        List<ElectronDetailOutData> electronDetailOut = gaiaSdElectronChangeMapper.selectGiftCouponsAndUse(inData);
        List<ElectronDetailOutData> electronDetailOutData = new ArrayList<>();
        List<ElectronDetailOutData> useList = new ArrayList<>();//用

        if (CollUtil.isNotEmpty(electronDetailOut)) {

            Map<String, List<ElectronDetailOutData>> map = new HashMap<>();
            for (ElectronDetailOutData detailOutData : electronDetailOut) {
                List<ElectronDetailOutData> detail = new ArrayList<>();
                for (ElectronDetailOutData outData : electronDetailOut) {
                    if (detailOutData.getGsebId().equals(outData.getGsebId())) {
                        detail.add(outData);
                    }
                }
                map.put(detailOutData.getGsebId(), detail);
            }

            for (Map.Entry<String, List<ElectronDetailOutData>> entry : map.entrySet()) {
                String key = entry.getKey();
                List<ElectronDetailOutData> giveList = new ArrayList<>();//送
                List<ElectronDetailOutData> value = entry.getValue();
                for (ElectronDetailOutData detail : value) {
                    if (detail.getGsebsId().contains("SQ")) {
                        giveList.add(detail);
                    } else if (detail.getGsebsId().contains("YQ")) {
                        useList.add(detail);
                    }
                }

                if (CollUtil.isNotEmpty(giveList)) {
                    //获取送券 时间 最近的
                    electronDetailOutData.add(giveList.stream().max(Comparator.comparing(e -> e.getGsebsCreateDate() + e.getGsebsCreateTime())).get());
                }
            }

            useList.forEach(detail -> electronDetailOutData.stream().filter(datum -> datum.getGsetsId().equals(detail.getGsetsId())).forEach(datum -> datum.setGsecUseExpirationDate(detail.getGsebsEndDate())));

            //送券信息
            electronDetailOutData.forEach(item -> {
                if (StrUtil.isNotBlank(item.getGsecsProGroup())) {
                    Example example = new Example(GaiaSdProductsGroup.class);
                    example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gspgId", item.getGsecsProGroup());
                    item.setProGroup(gaiaSdProductsGroupMapper.selectByExample(example));
                }
            });
        }
        return electronDetailOutData;
    }

    /**
     * 品牌分类/商品分类
     *
     * @param client
     * @return
     */
    @Override
    public Classification getclassification(String client) {
        List<GaiaProductBusiness> businesses = businessMapper.getAll(client);
        Classification classification = new Classification();
        List<String> proClassName = new ArrayList<>();
        List<String> proBrand = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(businesses)) {
            for (GaiaProductBusiness business : businesses) {
                if (StrUtil.isNotBlank(business.getProClassName())) {
                    proClassName.add(business.getProClassName());
                }
                if (StrUtil.isNotBlank(business.getProBrand())) {
                    proBrand.add(business.getProBrand());
                }
            }
        }
        List<String> collect = proBrand.stream().distinct().collect(toList());
        classification.setProClassName(proClassName);
        classification.setProBrand(collect);
        return classification;
    }

    /**
     * 商品组导入商品id
     *
     * @param proCode
     * @param client
     * @param brId
     * @return
     */
    @Override
    public List<FuzzyQueryOfProductGroupOutData> importExcel(List<String> proCode, String client, String brId) {
        if (CollUtil.isEmpty(proCode)) {
            throw new BusinessException(UtilMessage.UPLOAD_CODE_CANNOT_BE_EMPTY);
        }
        List<FuzzyQueryOfProductGroupOutData> outData = this.gaiaSdElectronProductsGroupMapper.importExcel(proCode, client, brId);
        if (CollUtil.isNotEmpty(outData)) {
            IntStream.range(0, outData.size()).forEach(i -> (outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertAll(ElectronicAllData inVo) {
        //1.0校验电子券是否已经被使用过
        checkElectron(inVo);
        //如果主题单号不为空则直接删除所有
        if (StrUtil.isNotBlank(inVo.getGsetsId())) {
            GaiaSdElectronThemeSet themeSet = new GaiaSdElectronThemeSet();
            themeSet.setClient(inVo.getClient());
            themeSet.setGsetsId(inVo.getGsetsId());
            GaiaSdElectronThemeSet electronThemeSet = this.gaiaSdElectronThemeSetMapper.selectByPrimaryKey(themeSet);
            if (ObjectUtil.isNotEmpty(electronThemeSet)) {
                electronThemeSet.setGsetsValid(UtilMessage.N);//设置为删除
                this.gaiaSdElectronThemeSetMapper.update(electronThemeSet);
                //根据加盟商查询主题下所有未审核状态 电子券业务基础
                List<GaiaSdElectronBusinessSet> businessSetList = this.gaiaSdElectronBusinessSetMapper.getAllByClientAndGsetsId(inVo.getClient(), inVo.getGsetsId());
                if (CollUtil.isNotEmpty(businessSetList)) {
                    businessSetList.forEach(businessSet -> businessSet.setGsebsValid(UtilMessage.N));
                    //修改当前主题下的所有删除状态
                    this.gaiaSdElectronBusinessSetMapper.batchUpdate(businessSetList);
                }

                //电子券业务条件
                List<String> gsecsProGroupIds = new ArrayList<>();
                List<GaiaSdElectronConditionSet> conditionSets = this.gaiaSdElectronConditionSetMapper.getAllByClientAndGsetsId(inVo.getClient(), inVo.getGsetsId());
                if (CollUtil.isNotEmpty(conditionSets)) {
                    conditionSets.forEach(conditionSet -> {
                        conditionSet.setGsecsValid(UtilMessage.N);
                        gsecsProGroupIds.add(conditionSet.getGsecsProGroup());
                    });
                    //删除主题下的所有状态
                    if (CollUtil.isNotEmpty(conditionSets)) {
                        this.gaiaSdElectronConditionSetMapper.batchUpdate(conditionSets);
                    }
                }

                //如果不商品组id集合不为空
                if (CollUtil.isNotEmpty(gsecsProGroupIds)) {
                    List<GaiaSdElectronicProductsGroup> groupList = this.gaiaSdElectronProductsGroupMapper.getAllByClientAndProGroupIds(inVo.getClient(), gsecsProGroupIds);
                    if (CollUtil.isNotEmpty(groupList)) {
                        groupList.forEach(productsGroup -> productsGroup.setGspgValid(UtilMessage.N));
                        if (CollUtil.isNotEmpty(groupList)) {
                            this.gaiaSdElectronProductsGroupMapper.batchUpdate(groupList);
                        }
                    }
                }

                //自动发券表
                List<GaiaSdElectronAutoSet> autoSetList = this.gaiaSdElectronAutoSetMapper.selectAll(inVo.getClient(), inVo.getGsetsId());
                if (CollUtil.isNotEmpty(autoSetList)) {
                    autoSetList.forEach(autoSet -> autoSet.setGseasValid(UtilMessage.N));
                    if (CollUtil.isNotEmpty(autoSetList)) {
                        this.gaiaSdElectronAutoSetMapper.batchUpdate(autoSetList);
                    }
                }
            }
        }
        String date = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);
        String time = DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN);
        //主题单号
        String gsetsId = this.gaiaSdElectronThemeSetMapper.getGsebIdToGaiaSdElectronThemeSet(inVo.getClient());
        // 新增主题
        ThemeSettingVO themeSettingVO = this.getThemeSettingVO(inVo, date, time, gsetsId);

        if (UtilMessage.CM.equals(themeSettingVO.getGsetsFlag())) {
            //消费型
            if (ObjectUtil.isEmpty(inVo.getCoupon())) {
                throw new BusinessException(UtilMessage.COUPONS_CANNOT_BE_EMPTY);
            }
            //电子券送券信息
            ElectronCouponsInVO coupon = this.getElectronCouponsInVO(inVo, date, time, gsetsId);
            String gsetsMaxQty = coupon.getGsetsMaxQty();
            if (StrUtil.isNotBlank(gsetsMaxQty) && NumberUtil.isInteger(gsetsMaxQty) && gsetsMaxQty.length() > 10) {//验证整数
                throw new BusinessException("总发券数量请输入整数,最大长度为10");
            }
            //总发券数量 如果为空 传0
            themeSettingVO.setGsetsMaxQty(StrUtil.isBlank(gsetsMaxQty) ? "" : gsetsMaxQty);
            //获取门店号

            //List<String> storeIds = this.getBrIds(coupon.getClient(), coupon.getBrStart(), coupon.getBrEnd(), coupon.getStoreType(), coupon.getStoreGroup(), coupon.getStoreIds());
            List<String> storeIds = coupon.getStoreIds();
            if (CollUtil.isEmpty(storeIds)) {
                throw new BusinessException(UtilMessage.NOT_MATCHED_TO_THE_CORRESPONDING_STORE);
            }

            //查询数据库中当前加盟商下 当前主题下 根据业务类型下的所有
            List<GaiaSdElectronBusinessSet> businessSets = this.gaiaSdElectronBusinessSetMapper.getAll(coupon.getClient(), coupon.getGsetsId(), coupon.getGsebsType());
            //根据当前加盟商下,当前主题单号下 查询所有业务条件
            List<GaiaSdElectronConditionSet> sdElectronConditionSets = this.gaiaSdElectronConditionSetMapper.getAll(coupon.getClient(), coupon.getGsetsId());
            //根据当前加盟商下的 当前商品组id 查询
            List<GaiaSdElectronicProductsGroup> productsGroupList = this.gaiaSdElectronProductsGroupMapper.getAll(coupon.getClient());


            //获得业务基础表的自增id
            String gsebsId = this.gaiaSdElectronBusinessSetMapper.getElectronBusinessBySQId(coupon.getClient());
            List<GaiaSdElectronBusinessSet> businessSetList = new ArrayList<>();
            List<GaiaSdElectronicProductsGroup> groupList = new ArrayList<>();
            List<GaiaSdElectronConditionSet> conditionSetList = new ArrayList<>();
            List<String> gsebIds = new ArrayList<>();
            for (String brId : storeIds) {
                //电子券业务基础设置表
                GaiaSdElectronBusinessSet businessSet = this.getGaiaSdElectronBusinessSet(inVo, coupon, gsebsId, brId);
                businessSetList.add(businessSet);

                //如果商品组id有值并且集合不为空
                for (ElectronChildInVO childInVO : coupon.getChildInVOS()) {
                    GaiaSdElectronConditionSet conditionSet = this.getGaiaSdElectronConditionSet(gsebsId, childInVO, coupon.getClient(), coupon.getGsetsId(), coupon.getGsebsId(), coupon.getGsebsCreateDate(), coupon.getGsebsCreateTime());
                    conditionSetList.add(conditionSet);
                    gsebIds.add(conditionSet.getGsebId());//活动号
                    if (StrUtil.isNotBlank(coupon.getGroupId()) && CollUtil.isNotEmpty(childInVO.getGspgProIds())) {
                        //商品组表
                        childInVO.getGspgProIds().stream().map(gspgProId -> this.getGaiaSdElectronicProductsGroup(inVo, gsebsId, gspgProId)).forEach(groupList::add);
                    }
                }
            }

            //电子券业务基础设置表条件判断去重
            this.DeduplicationBusiness(businessSetList, businessSets);
            //批量添加
            this.gaiaSdElectronBusinessSetMapper.batchInsert(businessSetList);
            //多条件去重
            List<GaiaSdElectronConditionSet> collect = conditionSetList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getClient() + f.getGsetsId() + f.getGsebsId() + f.getGsebId()))), ArrayList::new)
            );
            //电子券业务条件设置表条件判断去重
            this.DeduplicationConditionSet(collect, sdElectronConditionSets);
            if (CollUtil.isNotEmpty(collect)) {
                this.gaiaSdElectronConditionSetMapper.batchInsert(collect);
            }
            //商品组条件判断去重
            List<GaiaSdElectronicProductsGroup> groups = groupList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getClient() + f.getGspgId() + f.getGspgProId()))), ArrayList::new)
            );
            this.DeduplicationProductsGroup(groups, productsGroupList);
            //去重后批量插入
            if (CollUtil.isNotEmpty(groups)) {
                this.gaiaSdElectronProductsGroupMapper.batchInsert(groups);
            }
        } else if (UtilMessage.FM.equals(themeSettingVO.getGsetsFlag()) || UtilMessage.NM.equals(themeSettingVO.getGsetsFlag())) { //新老会员
            // 新增自动发券设置
            if (CollUtil.isNotEmpty(inVo.getToAutoSetVO().getSetInVOS())) {
                ThemeSettingToAutoSetVO toAutoSetVO = this.getToAutoSetVO(inVo, date, time, gsetsId, themeSettingVO);
                List<GaiaSdElectronAutoSet> autoSets = this.getGaiaSdElectronAutoSets(inVo, toAutoSetVO);
                if (CollUtil.isNotEmpty(autoSets)) {
                    this.gaiaSdElectronAutoSetMapper.batchInsert(autoSets);
                }
            }
        }

        //主题
        GaiaSdElectronThemeSet themeSet = this.getGaiaSdElectronThemeSet(inVo, themeSettingVO);
        this.gaiaSdElectronThemeSetMapper.insertSelective(themeSet);

        //用券传参
        ElectronVoucherInVO voucher = this.getElectronVoucherInVO(inVo, date, time, gsetsId);

        //获取门店编码
        //
        List<String> storeIds = inVo.getVoucher().getStoreIds();
        //List<String> storeIds = this.getBrIds(voucher.getClient(), voucher.getBrStart(), voucher.getBrEnd(), voucher.getStoreType(), voucher.getStoreGroup(), voucher.getStoreIds());
        if (CollUtil.isEmpty(storeIds)) {
            throw new BusinessException(UtilMessage.NOT_MATCHED_TO_THE_CORRESPONDING_STORE);
        }

        //查询数据库中当前加盟商下 当前主题下 根据业务类型下的所有
        List<GaiaSdElectronBusinessSet> businessSets = this.gaiaSdElectronBusinessSetMapper.getAll(voucher.getClient(), voucher.getGsetsId(), voucher.getGsebsType());
        //根据当前加盟商下,当前主题单号下 查询所有业务条件
        List<GaiaSdElectronConditionSet> sdElectronConditionSets = this.gaiaSdElectronConditionSetMapper.getAll(voucher.getClient(), voucher.getGsetsId());
        //根据当前加盟商下的 当前商品组id 查询
        List<GaiaSdElectronicProductsGroup> productsGroupList = this.gaiaSdElectronProductsGroupMapper.getAll(voucher.getClient());


        //获得业务基础表的自增id
        String gsebsId = this.gaiaSdElectronBusinessSetMapper.getElectronBusinessByYQId(voucher.getClient());
        List<GaiaSdElectronBusinessSet> businessSetList = new ArrayList<>();
        List<GaiaSdElectronicProductsGroup> groupList = new ArrayList<>();
        List<GaiaSdElectronConditionSet> conditionSetList = new ArrayList<>();
        List<String> gsebIds = new ArrayList<>();
        for (String brId : storeIds) {
            //电子券业务基础设置表
            GaiaSdElectronBusinessSet businessSet = this.getGaiaSdElectronBusinessSetByVoucher(voucher, gsebsId, brId);
            businessSetList.add(businessSet);

            //如果商品组id有值并且集合不为空
            voucher.getChildInVOS().forEach(childInVO -> {
                GaiaSdElectronConditionSet conditionSet = this.getGaiaSdElectronConditionSet(voucher, gsebsId, childInVO);
                conditionSetList.add(conditionSet);
                gsebIds.add(conditionSet.getGsebId());//活动号
                if (StrUtil.isNotBlank(voucher.getGroupId()) && CollUtil.isNotEmpty(childInVO.getGspgProIds())) {
                    //商品组表
                    childInVO.getGspgProIds().stream().map(gspgProId -> this.getGaiaSdElectronicProductsGroupTwo(voucher, gspgProId, gsebsId)).forEach(groupList::add);
                }
            });
        }
        //电子券业务基础设置表条件判断去重
        this.DeduplicationBusiness(businessSetList, businessSets);
        //批量添加
        this.gaiaSdElectronBusinessSetMapper.batchInsert(businessSetList);
        //多条件去重
        List<GaiaSdElectronConditionSet> collect = conditionSetList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getClient() + f.getGsetsId() + f.getGsebsId() + f.getGsebId()))), ArrayList::new)
        );
        //电子券业务条件设置表条件判断去重
        this.DeduplicationConditionSet(collect, sdElectronConditionSets);
        if (CollUtil.isNotEmpty(collect)) {
            this.gaiaSdElectronConditionSetMapper.batchInsert(collect);
        }
        //商品组条件判断去重
        List<GaiaSdElectronicProductsGroup> groups = groupList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getClient() + f.getGspgId() + f.getGspgProId()))), ArrayList::new)
        );
        this.DeduplicationProductsGroup(groups, productsGroupList);
        //去重后批量插入
        if (CollUtil.isNotEmpty(groups)) {
            this.gaiaSdElectronProductsGroupMapper.batchInsert(groups);
        }
        return gsetsId;
    }

    private void checkElectron(ElectronicAllData inVo) {
        List<ElectronChildInVO> coupons = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(inVo.getCoupon()) && CollectionUtil.isNotEmpty(inVo.getCoupon().getChildInVOS())) {
            coupons.addAll(inVo.getCoupon().getChildInVOS());
        }
        if (CollectionUtil.isNotEmpty(inVo.getVoucher().getChildInVOS())) {
            coupons.addAll(inVo.getVoucher().getChildInVOS());
        }
        if (CollectionUtil.isNotEmpty(coupons)) {
            Set<String> gsebIdSet = coupons.stream().map(ElectronChildInVO::getGsebId).collect(Collectors.toSet());
            int count = gaiaSdElectronThemeSetMapper.getUseCount(gsebIdSet, inVo.getClient(), inVo.getGsetsId());
            if (count > 0) {
                throw new BusinessException("所选择电子券，已被使用过，请勿重复使用！");
            }
        }
    }

    /**
     * 用券参数设置
     *
     * @param voucher
     * @param gsebsId
     * @param brId
     * @return
     */
    public GaiaSdElectronBusinessSet getGaiaSdElectronBusinessSetByVoucher(ElectronVoucherInVO voucher, String gsebsId, String brId) {
        GaiaSdElectronBusinessSet businessSet = CopyUtil.copy(voucher, GaiaSdElectronBusinessSet.class);
        businessSet.setGsebsBrId(brId);//门店编码
        businessSet.setGsebsId(gsebsId);//业务单号
        businessSet.setGsebsStatus(UtilMessage.N);//是否审核
        businessSet.setGsebsFlag(UtilMessage.N);//是否停用
        businessSet.setGsebsValid(UtilMessage.Y);//是否有效
        if ("0".equals(voucher.getGsebsRule())) {
            businessSet.setGsebsValidDay(null);
        } else if ("1".equals(voucher.getGsebsRule())) {
            businessSet.setGsebsBeginDate(null);
            businessSet.setGsebsBeginTime(null);
            businessSet.setGsebsEndDate(null);
            businessSet.setGsebsEndTime(null);
        }
        return businessSet;
    }


    /**
     * 设置主题参数
     *
     * @param inVo
     * @param themeSettingVO
     * @return
     */
    public GaiaSdElectronThemeSet getGaiaSdElectronThemeSet(ElectronicAllData inVo, ThemeSettingVO themeSettingVO) {
        GaiaSdElectronThemeSet themeSet = CopyUtil.copy(themeSettingVO, GaiaSdElectronThemeSet.class);
        themeSet.setGsetsMaxQty(themeSettingVO.getGsetsMaxQty());//暂无
        themeSet.setGsetsValid(UtilMessage.Y);
        GaiaSdElectronThemeSet gaiaSdElectronThemeSet = this.gaiaSdElectronThemeSetMapper.selectByPrimaryKey(themeSet);
        if (ObjectUtil.isNotEmpty(gaiaSdElectronThemeSet)) {
            throw new BusinessException(UtilMessage.SUBJECT_TICKET_NUMBER_ALREADY_EXISTS);
        }
        log.info("themeSet{}", themeSet);
        return themeSet;
    }

    public List<GaiaSdElectronAutoSet> getGaiaSdElectronAutoSets(ElectronicAllData inVo, ThemeSettingToAutoSetVO toAutoSetVO) {
        Map<String, Object> map = new HashMap<>();
        List<GaiaSdElectronAutoSet> autoSets = CopyUtil.copyList(toAutoSetVO.getSetInVOS(), GaiaSdElectronAutoSet.class);
        List<GaiaSdElectronAutoSet> autoSetList = autoSets.stream().filter(item -> !autoSets.contains(item)).collect(toList());
        if (CollUtil.isNotEmpty(autoSetList)) {
            throw new BusinessException(UtilMessage.THERE_IS_A_DUPLICATE_E_VOUCHER_ACTIVITY_NUMBER);
        }
        autoSets.forEach(autoSet -> autoSet.setClient(inVo.getClient()));
        List<GaiaSdElectronAutoSet> gaiaSdElectronAutoSets = this.gaiaSdElectronAutoSetMapper.selectAll(inVo.getClient(), inVo.getGsetsId());
        //和数据库比对后 去重后
        if (CollUtil.isNotEmpty(gaiaSdElectronAutoSets)) {
            gaiaSdElectronAutoSets.forEach(autoSet -> autoSets.stream().filter(set -> autoSet.getClient().equals(set.getClient()) && autoSet.getGseasId().equals(set.getGseasId()) && autoSet.getGseasFlag().equals(set.getGseasFlag()) && autoSet.getGsebId().equals(set.getGsebId())).forEach(set -> {
                throw new BusinessException(UtilMessage.THERE_IS_A_DUPLICATE_E_VOUCHER_ACTIVITY_NUMBER);
            }));
        }
        return autoSets;
    }


    /**
     * 用券设置
     *
     * @param inVo
     * @param date
     * @param time
     * @param gsetsId
     * @param groupId
     * @return
     */
    public ElectronVoucherInVO getElectronVoucherInVO(ElectronicAllData inVo, String date, String time, String gsetsId) {
        ElectronVoucherInVO voucher = inVo.getVoucher();
        if ("0".equals(voucher.getGsebsRule())) {
            //判断日期
            if (!Util.belongCalendar(DateUtil.parse(voucher.getGsebsBeginDate()), DateUtil.parse(voucher.getGsebsEndDate()))) {
                throw new BusinessException(UtilMessage.THE_START_DATE_CANNOT_BE_GREATER_THAN_THE_END_DATE);
            }
            //判断时间
            if (!Util.belongCalendarTime(DateUtil.parse(voucher.getGsebsBeginTime()), DateUtil.parse(voucher.getGsebsEndTime()))) {
                throw new BusinessException(UtilMessage.THE_START_TIME_CANNOT_BE_GREATER_THAN_THE_END_TIME);
            }
            voucher.setGsebsBeginDate(DateUtil.format(DateUtil.parse(voucher.getGsebsBeginDate()), UtilMessage.YYYYMMDD));
            voucher.setGsebsEndDate(DateUtil.format(DateUtil.parse(voucher.getGsebsEndDate()), UtilMessage.YYYYMMDD));
            voucher.setGsebsBeginTime(DateUtil.format(DateUtil.parse(voucher.getGsebsBeginTime()), UtilMessage.HHMMSS));
            voucher.setGsebsEndTime(DateUtil.format(DateUtil.parse(voucher.getGsebsEndTime()), UtilMessage.HHMMSS));
        } else if ("1".equals(voucher.getGsebsRule())) {
            voucher.setGsebsValidDay(voucher.getGsebsValidDay());
        }
        voucher.setClient(inVo.getClient());
        voucher.setGsetsId(gsetsId);
        voucher.setGsebsType(UtilMessage.ONE);
        voucher.setGsebsCreateDate(date);
        voucher.setGsebsCreateTime(time);
        voucher.setGroupId(gsetsId);
        return voucher;
    }

    public ThemeSettingToAutoSetVO getToAutoSetVO(ElectronicAllData inVo, String date, String time, String gsetsId, ThemeSettingVO themeSettingVO) {
        ThemeSettingToAutoSetVO toAutoSetVO = inVo.getToAutoSetVO();
        if (StrUtil.isEmpty(toAutoSetVO.getGseasBeginDate()) || StrUtil.isEmpty(toAutoSetVO.getGseasEndDate())) {
            throw new BusinessException(UtilMessage.START_AND_END_CANNOT_BE_EMPTY);
        }
        toAutoSetVO.setClient(inVo.getClient());
        toAutoSetVO.setGsetsId(gsetsId);
        toAutoSetVO.setGseasFlag(themeSettingVO.getGsetsFlag());//主题属性
        List<ElectronAutoSetInVO> setInVOS = toAutoSetVO.getSetInVOS();
        if (ObjectUtil.isEmpty(setInVOS)) {
            throw new BusinessException(UtilMessage.INFORMATION_SUCH_AS_ELECTRONIC_COUPONS_CANNOT_BE_EMPTY);
        }

        for (ElectronAutoSetInVO setInVO : setInVOS) {
            if (StrUtil.isBlank(setInVO.getGsebId()) || StrUtil.isBlank(setInVO.getGseasQty())) {
                throw new BusinessException(UtilMessage.ACTIVITY_NUMBER_OR_SENDING_QUANTITY_CANNOT_BE_EMPTY);
            }
            setInVO.setClient(inVo.getClient());
            setInVO.setGseasId(gsetsId);
            setInVO.setGseasFlag(themeSettingVO.getGsetsFlag());
            setInVO.setGseasBeginDate(toAutoSetVO.getGseasBeginDate());
            setInVO.setGseasEndDate(toAutoSetVO.getGseasEndDate());
            setInVO.setGseasCreateDate(date);
            setInVO.setGseasCreateTime(time);
            setInVO.setGseasValid(UtilMessage.Y);

        }
        return toAutoSetVO;
    }


    public GaiaSdElectronConditionSet getGaiaSdElectronConditionSet(String gsebsId, ElectronChildInVO childInVO, String client, String gsetsId2, String groupId2, String gsebsCreateDate, String gsebsCreateTime) {
        if (StrUtil.isBlank(childInVO.getGsecsReachQty1()) && StrUtil.isBlank(childInVO.getGsecsReachAmt1())) {
            throw new BusinessException(UtilMessage.THE_INPUT_INFORMATION_IS_WRONG);
        }
        if (StrUtil.isNotBlank(childInVO.getGsecsReachQty1()) && StrUtil.isNotBlank(childInVO.getGsecsReachAmt1())) {
            throw new BusinessException(UtilMessage.CANNOT_EXIST_AT_THE_SAME_TIME);
        }
        if (StrUtil.isNotBlank(childInVO.getGsecsReachQty1()) && !Util.quantityJudgment(childInVO.getGsecsReachQty1())) {
            throw new BusinessException(UtilMessage.THE_INPUT_QUANTITY_IS_WRONG);
        }
        if (StrUtil.isNotBlank(childInVO.getGsecsReachAmt1()) && !Util.isNumber2(childInVO.getGsecsReachAmt1())) {
            throw new BusinessException(UtilMessage.WRONG_AMOUNT);
        }

        //电子券业务条件设置表
        GaiaSdElectronConditionSet conditionSet = CopyUtil.copy(childInVO, GaiaSdElectronConditionSet.class);
        conditionSet.setGsebsId(gsebsId);//业务单号
        conditionSet.setClient(client);
        conditionSet.setGsetsId(gsetsId2);//主题号
        conditionSet.setGsecsFlag1(StrUtil.isBlank(childInVO.getGsecsFlag1()) ? UtilMessage.N : childInVO.getGsecsFlag1());
        if (StrUtil.isNotBlank(gsebsId) && CollUtil.isNotEmpty(childInVO.getGspgProIds())) {
            conditionSet.setGsecsProGroup(gsebsId);
        }
        conditionSet.setGsecsValid(UtilMessage.Y);//是否有效
        conditionSet.setGsecsCreateDate(gsebsCreateDate);
        conditionSet.setGsecsCreateTime(gsebsCreateTime);
        return conditionSet;
    }

    public GaiaSdElectronBusinessSet getGaiaSdElectronBusinessSet(ElectronicAllData inVo, ElectronCouponsInVO coupon, String gsebsId, String brId) {
        if (StrUtil.isNotBlank(coupon.getGsebsMaxQty()) && !Util.isNumber2(coupon.getGsebsMaxQty()) && coupon.getGsebsMaxQty().length() > 10) {
            throw new BusinessException(UtilMessage.THE_INPUT_QUANTITY_IS_WRONG);
        }
        GaiaSdElectronBusinessSet businessSet = CopyUtil.copy(coupon, GaiaSdElectronBusinessSet.class);
        businessSet.setGsebsBrId(brId);//门店编码
        businessSet.setGsebsId(gsebsId);//业务单号
        businessSet.setGsebsStatus(UtilMessage.N);//是否审核
        businessSet.setGsebsFlag(UtilMessage.N);//是否停用
        businessSet.setGsebsValid(UtilMessage.Y);//是否有效
        businessSet.setGsebsMaxQty(StrUtil.isBlank(coupon.getGsebsMaxQty()) ? "" : coupon.getGsebsMaxQty());//每家门店发券数量
        return businessSet;
    }

    public ThemeSettingVO getThemeSettingVO(ElectronicAllData inVo, String date, String time, String gsetsId) {
        ThemeSettingVO themeSettingVO = inVo.getThemeSettingVO();
        themeSettingVO.setGsetsId(gsetsId);
        themeSettingVO.setClient(inVo.getClient());
        themeSettingVO.setGsetsCreateEmp(inVo.getUserId());
        themeSettingVO.setGsetsCreateDate(date);
        themeSettingVO.setGsetsCreateTime(time);
        return themeSettingVO;
    }

    /**
     * 设置送券信息
     *
     * @param inVo
     * @param date
     * @param time
     * @param gsetsId
     * @param groupId
     * @return
     */
    public ElectronCouponsInVO getElectronCouponsInVO(ElectronicAllData inVo, String date, String time, String gsetsId) {
        ElectronCouponsInVO coupon = inVo.getCoupon();
        if (StrUtil.isNotBlank(coupon.getGsetsMaxQty()) && !Util.isNumber2(coupon.getGsetsMaxQty()) && coupon.getGsetsMaxQty().length() > 10) {
            throw new BusinessException(UtilMessage.THE_INPUT_QUANTITY_IS_WRONG);
        }
        //判断日期
        if (!Util.belongCalendar(DateUtil.parse(coupon.getGsebsBeginDate(), UtilMessage.YYYYMMDD), DateUtil.parse(coupon.getGsebsEndDate(), UtilMessage.YYYYMMDD))) {
            throw new BusinessException(UtilMessage.THE_START_DATE_CANNOT_BE_GREATER_THAN_THE_END_DATE);
        }
        //判断时间
        if (!Util.belongCalendarTime(DateUtil.parse(coupon.getGsebsBeginTime()), DateUtil.parse(coupon.getGsebsEndTime()))) {
            throw new BusinessException(UtilMessage.THE_START_TIME_CANNOT_BE_GREATER_THAN_THE_END_TIME);
        }

        coupon.setClient(inVo.getClient());
        coupon.setGsetsId(gsetsId);
        coupon.setGsebsType(UtilMessage.ZERO);
        coupon.setGsebsCreateDate(date);
        coupon.setGsebsCreateTime(time);
        coupon.setGsebsBeginDate(DateUtil.format(DateUtil.parse(coupon.getGsebsBeginDate()), UtilMessage.YYYYMMDD));
        coupon.setGsebsEndDate(DateUtil.format(DateUtil.parse(coupon.getGsebsEndDate()), UtilMessage.YYYYMMDD));
        coupon.setGsebsBeginTime(DateUtil.format(DateUtil.parse(coupon.getGsebsBeginTime()), UtilMessage.HHMMSS));
        coupon.setGsebsEndTime(DateUtil.format(DateUtil.parse(coupon.getGsebsEndTime()), UtilMessage.HHMMSS));
        coupon.setGroupId(gsetsId);
        if (ObjectUtil.isNotEmpty(inVo.getVoucher()) && ObjectUtil.isNotEmpty(inVo.getVoucher().getGsebsValidDay())) {
            coupon.setGsebsValidDay(inVo.getVoucher().getGsebsValidDay());
            coupon.setGsebsRule(inVo.getVoucher().getGsebsRule());
        }
        return coupon;
    }


    /**
     * 电子券报表查询
     *
     * @param inData
     * @return
     */
    @Override
    public ElectronicDetailedViewOutData detailedView(ElectronicDetailedViewInData inData) {
        ElectronicDetailedViewOutData outData = new ElectronicDetailedViewOutData();
        GaiaSdElectronThemeSet themeSet = new GaiaSdElectronThemeSet();
        themeSet.setClient(inData.getClient());
        themeSet.setGsetsId(inData.getGsetsId());
        GaiaSdElectronThemeSet electronThemeSet = this.gaiaSdElectronThemeSetMapper.selectByPrimaryKey(themeSet);
        if (ObjectUtil.isEmpty(electronThemeSet)) {
            throw new BusinessException(UtilMessage.SUBJECT_TRACKING_NUMBER_DOES_NOT_EXIST);
        } else {
            outData.setGsetsId(electronThemeSet.getGsetsId());//主题单号
            outData.setGsetsName(electronThemeSet.getGsetsName());//主题描述
            outData.setGsetsFlag(electronThemeSet.getGsetsFlag());//主题属性
            //送券集合
            List<GaiaSdElectronBusinessSet> couponsList = new ArrayList<>();
            List<String> couponGroupIds = new ArrayList<>();
            List<String> couponGsebsId = new ArrayList<>();//送券的业务单号

            //用券集合
            List<GaiaSdElectronBusinessSet> voucherList = new ArrayList<>();
            List<String> voucherGroupIds = new ArrayList<>();
            List<String> voucherGsebsId = new ArrayList<>();//用券的业务单号

            List<GaiaSdElectronBasic> allByClient = this.gaiaSdElectronBasicMapper.getAllByClient(electronThemeSet.getClient());
            if (UtilMessage.CM.equals(electronThemeSet.getGsetsFlag())) {//消费型
                this.propertySave(inData, outData, themeSet, electronThemeSet, couponsList, couponGsebsId, voucherList, voucherGsebsId, allByClient);
            } else if (UtilMessage.FM.equals(electronThemeSet.getGsetsFlag()) || UtilMessage.NM.equals(electronThemeSet.getGsetsFlag())) {//新老会员
                //根据加盟商,主题单号,主题属性
                List<GaiaSdElectronAutoSet> autoSetList = this.gaiaSdElectronAutoSetMapper.getAutoSetByClientAndGseasFlag(electronThemeSet.getClient(), electronThemeSet.getGsetsId(), electronThemeSet.getGsetsFlag());

                List<ElectronAutoSetInVO> setInVOS = new ArrayList<>();
                allByClient.forEach(gaiaSdElectronBasic -> autoSetList.stream().filter(autoSet -> gaiaSdElectronBasic.getGsebId().equals(autoSet.getGsebId())).forEach(autoSet -> {
                    ElectronAutoSetInVO inVO = new ElectronAutoSetInVO();
                    inVO.setGsebId(gaiaSdElectronBasic.getGsebId());//活动号
                    inVO.setGsebName(gaiaSdElectronBasic.getGsebName());//主题描述
                    inVO.setGseasQty(autoSet.getGseasQty());//发送数量
                    inVO.setGsebAmt(gaiaSdElectronBasic.getGsebAmt());//面值
                    setInVOS.add(inVO);
                }));
                if (CollUtil.isNotEmpty(autoSetList)) {
                    List<GaiaSdElectronAutoSet> data = autoSetList.stream().collect(
                            Collectors.collectingAndThen(
                                    Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronAutoSet::getGseasId))), ArrayList::new)
                    );
                    GaiaSdElectronAutoSet autoSet = data.get(0);
                    ThemeSettingToAutoSetVO autoSetVO = new ThemeSettingToAutoSetVO();
                    autoSetVO.setGseasBeginDate(autoSet.getGseasBeginDate());
                    autoSetVO.setGseasEndDate(autoSet.getGseasEndDate());
                    autoSetVO.setSetInVOS(setInVOS);
                    outData.setToAutoSetVO(autoSetVO);

                    List<GaiaSdElectronBusinessSet> businessSets = this.gaiaSdElectronBusinessSetMapper.getAllByClientAndGsetsIdAndStatus(themeSet.getClient(), themeSet.getGsetsId(), inData.getGsebsStatus());

                    businessSets.stream().filter(businessSet -> businessSet.getGsebsType().equals(UtilMessage.ONE)).forEach(businessSet -> {//用券
                        voucherList.add(businessSet);
                        voucherGsebsId.add(businessSet.getGsebsId());
                    });

                    List<GaiaSdElectronConditionSet> voucherConditionSet = new ArrayList<>();
                    List<ElectronicDetailedOutData> voucherdetailed = new ArrayList<>();
                    if (CollUtil.isNotEmpty(couponGsebsId) || CollUtil.isNotEmpty(voucherGsebsId)) {
                        voucherConditionSet = this.gaiaSdElectronConditionSetMapper.getElectronConditionByGsetsIdAndGroupIds(themeSet.getClient(), themeSet.getGsetsId(), voucherGsebsId);
                        voucherdetailed = CopyUtil.copyList(voucherConditionSet, ElectronicDetailedOutData.class);
                    }

                    voucherdetailed.forEach(detailed -> allByClient.stream().filter(basic -> basic.getGsebId().equals(detailed.getGsebId())).forEach(basic -> {
                        detailed.setGsebName(basic.getGsebName());
                        detailed.setGsebAmt(basic.getGsebAmt());
                        detailed.setGsebType(basic.getGsebType());
                        detailed.setGsebTypeName(basic.getGsebTypeName());
                    }));

                    //当前主题下的所有商品组id 用券
                    List<String> voucherProGroupIds = voucherConditionSet.stream().filter(set -> StrUtil.isNotBlank(set.getGsecsProGroup())).map(GaiaSdElectronConditionSet::getGsecsProGroup).collect(toList());
                    if (CollUtil.isNotEmpty(voucherProGroupIds)) {
                        List<GaiaSdElectronicProductsGroup> voucherGroupList = this.gaiaSdElectronProductsGroupMapper.getAllByClientAndProGroupIds(themeSet.getClient(), voucherProGroupIds);
                        if (CollUtil.isNotEmpty(voucherGroupList)) {
                            for (ElectronicDetailedOutData detailedOutData : voucherdetailed) {
                                if (CollUtil.isNotEmpty(voucherGroupList)) {
                                    List<String> proCodes = new ArrayList<>();
                                    for (GaiaSdElectronicProductsGroup productsGroup : voucherGroupList) {
                                        if (detailedOutData.getGsecsProGroup().equals(productsGroup.getGspgId())) {
                                            String gspgProId = productsGroup.getGspgProId();
                                            proCodes.add(gspgProId);
                                        }
                                    }
                                    List<FuzzyQueryOfProductGroupOutData> productGroupOutData = this.gaiaSdElectronProductsGroupMapper.productGroupDetailsView(themeSet.getClient(), proCodes);
                                    detailedOutData.setGroupOutData(productGroupOutData);
                                    if (CollectionUtil.isNotEmpty(productGroupOutData)) {
                                        List<String> collect = productGroupOutData.stream().map(FuzzyQueryOfProductGroupOutData::getProCode).collect(toList());
                                        detailedOutData.setGspgProIds(collect);
                                    }
                                }
                            }
                        }
                    }
                    //添加用券集合
                    this.saveVoucherList(outData, voucherList, voucherdetailed);
                }

            }

        }
        return outData;
    }

    /**
     * 消费型
     *
     * @param inData
     * @param outData
     * @param themeSet
     * @param electronThemeSet
     * @param couponsList
     * @param couponGsebsId
     * @param voucherList
     * @param voucherGsebsId
     */
    public void propertySave(ElectronicDetailedViewInData inData, ElectronicDetailedViewOutData outData, GaiaSdElectronThemeSet themeSet, GaiaSdElectronThemeSet electronThemeSet, List<GaiaSdElectronBusinessSet> couponsList, List<String> couponGsebsId, List<GaiaSdElectronBusinessSet> voucherList, List<String> voucherGsebsId, List<GaiaSdElectronBasic> allByClient) {
        List<GaiaSdElectronBusinessSet> businessSets = this.gaiaSdElectronBusinessSetMapper.getAllByClientAndGsetsIdAndStatus(themeSet.getClient(), themeSet.getGsetsId(), inData.getGsebsStatus());

        for (GaiaSdElectronBusinessSet businessSet : businessSets) {
            if (businessSet.getGsebsType().equals(UtilMessage.ZERO)) { //送券
                couponsList.add(businessSet);
                couponGsebsId.add(businessSet.getGsebsId());
            } else if (businessSet.getGsebsType().equals(UtilMessage.ONE)) {//用券
                voucherList.add(businessSet);
                voucherGsebsId.add(businessSet.getGsebsId());
            }
        }
        List<GaiaSdElectronConditionSet> couponConditionSet = new ArrayList<>();
        List<GaiaSdElectronConditionSet> voucherConditionSet = new ArrayList<>();
        List<ElectronicDetailedOutData> coupondetailed = new ArrayList<>();
        List<ElectronicDetailedOutData> voucherdetailed = new ArrayList<>();
        if (CollUtil.isNotEmpty(couponGsebsId) || CollUtil.isNotEmpty(voucherGsebsId)) {
            couponConditionSet = this.gaiaSdElectronConditionSetMapper.getElectronConditionByGsetsIdAndGroupIds(themeSet.getClient(), themeSet.getGsetsId(), couponGsebsId);
            voucherConditionSet = this.gaiaSdElectronConditionSetMapper.getElectronConditionByGsetsIdAndGroupIds(themeSet.getClient(), themeSet.getGsetsId(), voucherGsebsId);
            coupondetailed = CopyUtil.copyList(couponConditionSet, ElectronicDetailedOutData.class);
            voucherdetailed = CopyUtil.copyList(voucherConditionSet, ElectronicDetailedOutData.class);
        }
        coupondetailed.forEach(detailed -> allByClient.stream().filter(basic -> basic.getGsebId().equals(detailed.getGsebId())).forEach(basic -> {
            detailed.setGsebName(basic.getGsebName());
            detailed.setGsebAmt(basic.getGsebAmt());
            detailed.setGsebType(basic.getGsebType());
            detailed.setGsebTypeName(basic.getGsebTypeName());
        }));

        voucherdetailed.forEach(detailed -> allByClient.stream().filter(basic -> basic.getGsebId().equals(detailed.getGsebId())).forEach(basic -> {
            detailed.setGsebName(basic.getGsebName());
            detailed.setGsebAmt(basic.getGsebAmt());
            detailed.setGsebType(basic.getGsebType());
            detailed.setGsebTypeName(basic.getGsebTypeName());
        }));

        //当前主题下的所有商品组id 送券
        List<String> couponProGroupIds = couponConditionSet.stream().filter(set -> StrUtil.isNotBlank(set.getGsecsProGroup())).map(GaiaSdElectronConditionSet::getGsecsProGroup).collect(toList());
        if (CollUtil.isNotEmpty(couponProGroupIds)) {
            List<GaiaSdElectronicProductsGroup> couponGroupList = this.gaiaSdElectronProductsGroupMapper.getAllByClientAndProGroupIds(themeSet.getClient(), couponProGroupIds);
            if (CollUtil.isNotEmpty(couponGroupList)) {
                coupondetailed.stream().filter(detailedOutData -> CollUtil.isNotEmpty(couponGroupList)).forEach(detailedOutData -> {
                    List<String> proCodes = couponGroupList.stream().filter(productsGroup -> detailedOutData.getGsecsProGroup().equals(productsGroup.getGspgId())).map(GaiaSdElectronicProductsGroup::getGspgProId).collect(toList());
                    List<FuzzyQueryOfProductGroupOutData> productGroupOutData = this.gaiaSdElectronProductsGroupMapper.productGroupDetailsView(themeSet.getClient(), proCodes);
                    detailedOutData.setGroupOutData(productGroupOutData);
                    if (CollectionUtil.isNotEmpty(productGroupOutData)) {
                        List<String> collect = productGroupOutData.stream().map(FuzzyQueryOfProductGroupOutData::getProCode).collect(toList());
                        detailedOutData.setGspgProIds(collect);
                    }
                });
            }
        }

        //当前主题下的所有商品组id 用券
        List<String> voucherProGroupIds = voucherConditionSet.stream().filter(set -> StrUtil.isNotBlank(set.getGsecsProGroup())).map(GaiaSdElectronConditionSet::getGsecsProGroup).collect(toList());
        if (CollUtil.isNotEmpty(voucherProGroupIds)) {
            List<GaiaSdElectronicProductsGroup> voucherGroupList = this.gaiaSdElectronProductsGroupMapper.getAllByClientAndProGroupIds(themeSet.getClient(), voucherProGroupIds);
            if (CollUtil.isNotEmpty(voucherGroupList)) {
                voucherdetailed.stream().filter(detailedOutData -> CollUtil.isNotEmpty(voucherGroupList)).forEach(detailedOutData -> {
                    List<String> proCodes = voucherGroupList.stream().filter(productsGroup -> detailedOutData.getGsecsProGroup().equals(productsGroup.getGspgId())).map(GaiaSdElectronicProductsGroup::getGspgProId).collect(toList());
                    List<FuzzyQueryOfProductGroupOutData> productGroupOutData = this.gaiaSdElectronProductsGroupMapper.productGroupDetailsView(themeSet.getClient(), proCodes);
                    detailedOutData.setGroupOutData(productGroupOutData);
                    if (CollectionUtil.isNotEmpty(productGroupOutData)) {
                        List<String> collect = productGroupOutData.stream().map(FuzzyQueryOfProductGroupOutData::getProCode).collect(toList());
                        detailedOutData.setGspgProIds(collect);
                    }
                });
            }
        }
        //添加送券集合
        this.saveCouponList(outData, electronThemeSet, couponsList, coupondetailed);
        //添加用券集合
        this.saveVoucherList(outData, voucherList, voucherdetailed);
    }


    /**
     * 添加用券集合
     *
     * @param outData
     * @param couponsList
     * @param voucherList
     * @param voucherdetailed
     */
    public void saveVoucherList(ElectronicDetailedViewOutData outData, List<GaiaSdElectronBusinessSet> voucherList, List<ElectronicDetailedOutData> voucherdetailed) {
        List<GaiaSdElectronBusinessSet> data = voucherList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronBusinessSet::getGsetsId))), ArrayList::new)
        );
        GaiaSdElectronBusinessSet businessSet = data.get(0);
        ElectronicAllVoucherOutData voucher = new ElectronicAllVoucherOutData();
        //送券最大店号
        String maxBr = voucherList.stream().map(e -> Long.parseLong(e.getGsebsBrId())).sorted().max(Long::compareTo).get().toString();
        //送券最小店号
        String minBr = voucherList.stream().map(e -> Long.parseLong(e.getGsebsBrId())).sorted().min(Long::compareTo).get().toString();
        List<String> storeIds = voucherList.stream().map(e -> e.getGsebsBrId()).distinct().collect(toList());
        voucher.setStoreIds(storeIds);
        voucher.setBrStart(minBr);
        voucher.setBrEnd(maxBr);
        voucher.setGsebsBeginDate(businessSet.getGsebsBeginDate());
        voucher.setGsebsEndDate(businessSet.getGsebsEndDate());
        voucher.setGsebsBeginTime(businessSet.getGsebsBeginTime());
        voucher.setGsebsEndTime(businessSet.getGsebsEndTime());
        voucher.setGsebsType(businessSet.getGsebsType());
        voucher.setChildInVOS(voucherdetailed);
        voucher.setGsebsProm(businessSet.getGsebsProm());
        if (ObjectUtil.isNotEmpty(businessSet.getGsebsMonth())) {
            voucher.setGsebsMonth(businessSet.getGsebsMonth());
            voucher.setMonthList(businessSet.getGsebsMonth().split("/"));
        }
        if (ObjectUtil.isNotEmpty(businessSet.getGsebsWeek())) {
            voucher.setGsebsWeek(businessSet.getGsebsWeek());
            voucher.setWeekList(businessSet.getGsebsWeek().split("/"));
        }
        voucher.setGsebsRule(ObjectUtil.isNotEmpty(businessSet.getGsebsRule()) ? businessSet.getGsebsRule() : null);
        voucher.setGsebsValidDay(ObjectUtil.isNotEmpty(businessSet.getGsebsValidDay()) ? businessSet.getGsebsValidDay() : null);
        outData.setVoucher(voucher);//用券信息
    }

    /**
     * 添加送券集合
     *
     * @param outData
     * @param electronThemeSet
     * @param couponsList
     * @param coupondetailed
     */
    public void saveCouponList(ElectronicDetailedViewOutData outData, GaiaSdElectronThemeSet electronThemeSet, List<GaiaSdElectronBusinessSet> couponsList, List<ElectronicDetailedOutData> coupondetailed) {
        List<GaiaSdElectronBusinessSet> data = couponsList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronBusinessSet::getGsetsId))), ArrayList::new)
        );
        GaiaSdElectronBusinessSet businessSet = data.get(0);
        ElectronicAllCouponOutData coupon = new ElectronicAllCouponOutData();
        //送券最大店号
        String maxBr = couponsList.stream().map(e -> Long.parseLong(e.getGsebsBrId())).sorted().max(Long::compareTo).get().toString();
        //送券最小店号
        String minBr = couponsList.stream().map(e -> Long.parseLong(e.getGsebsBrId())).sorted().min(Long::compareTo).get().toString();
        coupon.setBrStart(minBr);
        coupon.setBrEnd(maxBr);
        //把参与门店聚集返回
        List<String> storeIds = couponsList.stream().map(e -> e.getGsebsBrId()).distinct().collect(toList());
        coupon.setStoreIds(storeIds);
        coupon.setGsebsBeginDate(businessSet.getGsebsBeginDate());
        coupon.setGsebsEndDate(businessSet.getGsebsEndDate());
        coupon.setGsebsBeginTime(businessSet.getGsebsBeginTime());
        coupon.setGsebsEndTime(businessSet.getGsebsEndTime());
        coupon.setGsebsType(businessSet.getGsebsType());
        coupon.setGsetsMaxQty(electronThemeSet.getGsetsMaxQty());//总共
        coupon.setGsebsMaxQty(businessSet.getGsebsMaxQty());//每家门店
        coupon.setChildInVOS(coupondetailed);
        coupon.setGsebsProm(businessSet.getGsebsProm());
        coupon.setGsebsRule(ObjectUtil.isNotEmpty(businessSet.getGsebsRule()) ? businessSet.getGsebsRule() : null);
        if (ObjectUtil.isNotEmpty(businessSet.getGsebsMonth())) {
            coupon.setGsebsMonth(businessSet.getGsebsMonth());
            coupon.setMonthList(businessSet.getGsebsMonth().split("/"));
        }
        if (ObjectUtil.isNotEmpty(businessSet.getGsebsWeek())) {
            coupon.setGsebsWeek(businessSet.getGsebsWeek());
            coupon.setWeekList(businessSet.getGsebsWeek().split("/"));
        }
        coupon.setGsebsValidDay(ObjectUtil.isNotEmpty(businessSet.getGsebsValidDay()) ? businessSet.getGsebsValidDay() : null);
        outData.setCoupon(coupon);//送券信息
    }

    /**
     * 电子券报表
     *
     * @param inVo
     * @return
     */
    @Override
    public List<ElectronicSubjectQueryOutData> subjectQuery(ElectronicSubjectQueryInData inVo) {
        List<ElectronicSubjectQueryOutData> outDataList = new ArrayList<>();
        List<ElectronicSubjectQueryOutData> outData = this.gaiaSdElectronThemeSetMapper.subjectQuery(inVo);
        List<GaiaSdElectronBusinessSet> mapperAll = this.gaiaSdElectronBusinessSetMapper.getAllByClient(inVo.getClient());
        for (ElectronicSubjectQueryOutData outDatum : outData) {
            //查询用券
            List<GaiaSdElectronBusinessSet> voucherMapperAll = new ArrayList<>();
            List<GaiaSdElectronBusinessSet> couponMapperAll = new ArrayList<>();
            if (CollUtil.isNotEmpty(mapperAll)) {
                for (GaiaSdElectronBusinessSet businessSet : mapperAll) {
                    if (outDatum.getGsetsId().equals(businessSet.getGsetsId()) && UtilMessage.ZERO.equals(businessSet.getGsebsType())) {//送券
                        couponMapperAll.add(businessSet);
                    } else if (outDatum.getGsetsId().equals(businessSet.getGsetsId()) && UtilMessage.ONE.equals(businessSet.getGsebsType())) {//用券
                        voucherMapperAll.add(businessSet);
                    }
                }
            }

            if (UtilMessage.CM.equals(outDatum.getGsetsFlag())) {//消费型
                if (CollUtil.isEmpty(voucherMapperAll)) {
                    throw new BusinessException(UtilMessage.INSUFFICIENT_INFORMATION);
                }
                List<GaiaSdElectronBusinessSet> voucherData = voucherMapperAll.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronBusinessSet::getGsetsId))), ArrayList::new)
                );
                GaiaSdElectronBusinessSet voucherbusinessSet = voucherData.get(0);
                //查询送券
                if (CollUtil.isEmpty(couponMapperAll)) {
                    throw new BusinessException(UtilMessage.INSUFFICIENT_INFORMATION);
                }
                List<GaiaSdElectronBusinessSet> couponData = couponMapperAll.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronBusinessSet::getGsetsId))), ArrayList::new)
                );
                GaiaSdElectronBusinessSet couponBusinessSet = couponData.get(0);
                outDatum.setCouponStartDate(couponBusinessSet.getGsebsBeginDate());
                outDatum.setCouponEndDate(couponBusinessSet.getGsebsEndDate());

                //用券时间
                outDatum.setVoucherStartDate(voucherbusinessSet.getGsebsBeginDate());
                outDatum.setVoucherEndDate(voucherbusinessSet.getGsebsEndDate());
                outDataList.add(outDatum);
            } else if (UtilMessage.FM.equals(outDatum.getGsetsFlag()) || UtilMessage.NM.equals(outDatum.getGsetsFlag())) {//新老会员
                if (CollUtil.isEmpty(voucherMapperAll)) {
                    throw new BusinessException(UtilMessage.INSUFFICIENT_INFORMATION);
                }
                List<GaiaSdElectronBusinessSet> voucherData = voucherMapperAll.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronBusinessSet::getGsetsId))), ArrayList::new)
                );
                GaiaSdElectronBusinessSet voucherbusinessSet = voucherData.get(0);
                //用券时间
                outDatum.setVoucherStartDate(voucherbusinessSet.getGsebsBeginDate());
                outDatum.setVoucherEndDate(voucherbusinessSet.getGsebsEndDate());

                List<GaiaSdElectronAutoSet> autoSetList = this.gaiaSdElectronAutoSetMapper.getAutoSetByClientAndGseasFlag(inVo.getClient(), outDatum.getGsetsId(), outDatum.getGsetsFlag());
                if (CollUtil.isEmpty(autoSetList)) {
                    throw new BusinessException(UtilMessage.INSUFFICIENT_INFORMATION);
                }
                List<GaiaSdElectronAutoSet> autoSets = autoSetList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronAutoSet::getGseasId))), ArrayList::new)
                );
                //用券时间
                GaiaSdElectronAutoSet electronAutoSet = autoSets.get(0);
                outDatum.setCouponStartDate(electronAutoSet.getGseasBeginDate());
                outDatum.setCouponEndDate(electronAutoSet.getGseasEndDate());
                outDataList.add(outDatum);
            }
        }

        return outDataList;
    }

    /**
     * 审核
     * 停用
     *
     * @param inVo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void review(ElectronicReviewInData inVo) {
        List<GaiaSdElectronBusinessSet> businessSets = this.gaiaSdElectronBusinessSetMapper.getBusinessSetByGsebsId(inVo.getClient(), inVo.getGsetsId());
        if (CollUtil.isEmpty(businessSets)) {
            throw new BusinessException(UtilMessage.THE_SUBJECT_NUMBER_IS_WRONG);
        }
        List<GaiaSdElectronBusinessSet> sets = new ArrayList<>();
        for (GaiaSdElectronBusinessSet businessSet : businessSets) {
            if (StrUtil.isNotBlank(inVo.getGsebsStatus()) && UtilMessage.Y.equals(inVo.getGsebsStatus())) {
                //修改审核状态
                businessSet.setGsebsStatus(UtilMessage.Y);
            }
            if (StrUtil.isNotBlank(inVo.getDeactivateStatus()) && UtilMessage.Y.equals(inVo.getDeactivateStatus()) && UtilMessage.Y.equals(businessSet.getGsebsStatus())) {
                //审核后修改停用
                businessSet.setGsebsFlag(UtilMessage.Y);
                businessSet.setGsebsStatus(UtilMessage.D);
            }
            sets.add(businessSet);
        }
        if (CollUtil.isNotEmpty(sets)) {
            this.gaiaSdElectronBusinessSetMapper.batchUpdate(sets);
        }

        //未审核的  删除主题下的所有
        List<GaiaSdElectronBusinessSet> couponData = businessSets.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdElectronBusinessSet::getGsetsId))), ArrayList::new)
        );
        GaiaSdElectronBusinessSet couponBusinessSet = couponData.get(0);
        if (UtilMessage.N.equals(couponBusinessSet.getGsebsStatus())) {
            if (StrUtil.isNotBlank(inVo.getDeactivateStatus()) && UtilMessage.Y.equals(inVo.getDeactivateStatus()) && UtilMessage.N.equals(couponBusinessSet.getGsebsStatus())) {
                GaiaSdElectronThemeSet themeSet = new GaiaSdElectronThemeSet();
                themeSet.setClient(inVo.getClient());
                themeSet.setGsetsId(inVo.getGsetsId());
                GaiaSdElectronThemeSet electronThemeSet = this.gaiaSdElectronThemeSetMapper.selectByPrimaryKey(themeSet);
                if (ObjectUtil.isNotEmpty(electronThemeSet)) {
                    electronThemeSet.setGsetsValid(UtilMessage.N);//设置为删除
                    this.gaiaSdElectronThemeSetMapper.update(electronThemeSet);
                    //根据加盟商查询主题下所有未审核状态 电子券业务基础
                    List<GaiaSdElectronBusinessSet> businessSetList = this.gaiaSdElectronBusinessSetMapper.getAllByClientAndGsetsId(inVo.getClient(), inVo.getGsetsId());
                    if (CollUtil.isNotEmpty(businessSetList)) {
                        businessSetList.forEach(businessSet -> businessSet.setGsebsValid(UtilMessage.N));
                        //修改当前主题下的所有删除状态
                        this.gaiaSdElectronBusinessSetMapper.batchUpdate(businessSetList);
                    }

                    //电子券业务条件
                    List<String> gsecsProGroupIds = new ArrayList<>();
                    List<GaiaSdElectronConditionSet> conditionSets = this.gaiaSdElectronConditionSetMapper.getAllByClientAndGsetsId(inVo.getClient(), inVo.getGsetsId());
                    if (CollUtil.isNotEmpty(conditionSets)) {
                        conditionSets.forEach(conditionSet -> {
                            conditionSet.setGsecsValid(UtilMessage.N);
                            gsecsProGroupIds.add(conditionSet.getGsecsProGroup());
                        });
                        //删除主题下的所有状态
                        if (CollUtil.isNotEmpty(conditionSets)) {
                            this.gaiaSdElectronConditionSetMapper.batchUpdate(conditionSets);
                        }
                    }

                    //如果不商品组id集合不为空
                    if (CollUtil.isNotEmpty(gsecsProGroupIds)) {
                        List<GaiaSdElectronicProductsGroup> groupList = this.gaiaSdElectronProductsGroupMapper.getAllByClientAndProGroupIds(inVo.getClient(), gsecsProGroupIds);
                        if (CollUtil.isNotEmpty(groupList)) {
                            groupList.forEach(productsGroup -> productsGroup.setGspgValid(UtilMessage.N));
                            if (CollUtil.isNotEmpty(groupList)) {
                                this.gaiaSdElectronProductsGroupMapper.batchUpdate(groupList);
                            }
                        }
                    }

                    //自动发券表
                    List<GaiaSdElectronAutoSet> autoSetList = this.gaiaSdElectronAutoSetMapper.selectAll(inVo.getClient(), inVo.getGsetsId());
                    if (CollUtil.isNotEmpty(autoSetList)) {
                        autoSetList.forEach(autoSet -> autoSet.setGseasValid(UtilMessage.N));
                        if (CollUtil.isNotEmpty(autoSetList)) {
                            this.gaiaSdElectronAutoSetMapper.batchUpdate(autoSetList);
                        }
                    }
                }
            }
        }
    }

    @Override
    public Map<String, Object> timeJudgment(ElectronicAllData inVo) {
        Map<String, Object> map = new HashMap<>();
        //主题单号
        ThemeSettingVO themeSettingVO = inVo.getThemeSettingVO();
        if (UtilMessage.CM.equals(themeSettingVO.getGsetsFlag())) { //消费型
            if (ObjectUtil.isEmpty(inVo.getCoupon())) {
                throw new BusinessException(UtilMessage.COUPONS_CANNOT_BE_EMPTY);
            }
            //电子券送券信息
            ElectronCouponsInVO coupon = inVo.getCoupon();
            if (StrUtil.isNotBlank(coupon.getGsetsMaxQty()) && !Util.isNumber2(coupon.getGsetsMaxQty()) && coupon.getGsetsMaxQty().length() > 10) {
                throw new BusinessException(UtilMessage.THE_INPUT_QUANTITY_IS_WRONG);
            }
            List<String> storeIds = inVo.getCoupon().getStoreIds();
            //List<String> storeIds = this.getBrIds(inVo.getClient(), coupon.getBrStart(), coupon.getBrEnd(), coupon.getStoreType(), coupon.getStoreGroup(), coupon.getStoreIds());
            if (CollUtil.isEmpty(storeIds)) {
                throw new BusinessException(UtilMessage.NOT_MATCHED_TO_THE_CORRESPONDING_STORE);
            }
            List<String> couponGsebIds = new ArrayList<>();
            storeIds.forEach(brId -> coupon.getChildInVOS().stream().map(ElectronChildInVO::getGsebId).forEach(couponGsebIds::add));

            //送券表
            List<GaiaSdElectronBusinessSet> sdElectronBusinessSets = this.gaiaSdElectronBusinessSetMapper.determineWhetherTheTimeIsRepeated(inVo.getClient(), storeIds, coupon.getGsebsBeginDate(), coupon.getGsebsEndDate(), couponGsebIds, UtilMessage.ZERO);


            //用券
            ElectronVoucherInVO voucher = inVo.getVoucher();
            List<String> voucherGsebIds = new ArrayList<>();
            storeIds.forEach(brId -> voucher.getChildInVOS().stream().map(ElectronChildInVO::getGsebId).forEach(voucherGsebIds::add));
            List<GaiaSdElectronBusinessSet> electronBusinessSets = this.gaiaSdElectronBusinessSetMapper.determineWhetherTheTimeIsRepeated(inVo.getClient(), storeIds, voucher.getGsebsBeginDate(), voucher.getGsebsEndDate(), voucherGsebIds, UtilMessage.ONE);

            map.put("code", UtilConst.CODE_0);
            map.put("data", UtilConst.CODE_202.toString());

            if (CollUtil.isNotEmpty(electronBusinessSets) && CollUtil.isNotEmpty(sdElectronBusinessSets)) {
                map.put("message", UtilMessage.SET_WITH_COUPON_OR_REPEAT_COUPON_DATE);
                return map;
            }
            if (CollUtil.isNotEmpty(electronBusinessSets)) {
                map.put("message", UtilMessage.VOUCHER_DUPLICATE_DATE);
                return map;
            }
            if (CollUtil.isNotEmpty(sdElectronBusinessSets)) {
                map.put("message", UtilMessage.COUPON_DUPLICATE_DATE);
                return map;
            }
        } else if (UtilMessage.FM.equals(themeSettingVO.getGsetsFlag()) || UtilMessage.NM.equals(themeSettingVO.getGsetsFlag())) { //新老会员
            // 新增自动发券设置
            if (CollUtil.isNotEmpty(inVo.getToAutoSetVO().getSetInVOS())) {
                ThemeSettingToAutoSetVO toAutoSetVO = inVo.getToAutoSetVO();
                if (StrUtil.isEmpty(toAutoSetVO.getGseasBeginDate()) || StrUtil.isEmpty(toAutoSetVO.getGseasEndDate())) {
                    throw new BusinessException(UtilMessage.START_AND_END_CANNOT_BE_EMPTY);
                }
                toAutoSetVO.setClient(inVo.getClient());
                toAutoSetVO.setGseasFlag(themeSettingVO.getGsetsFlag());//主题属性
                List<ElectronAutoSetInVO> setInVOS = toAutoSetVO.getSetInVOS();
                if (ObjectUtil.isEmpty(setInVOS)) {
                    throw new BusinessException(UtilMessage.INFORMATION_SUCH_AS_ELECTRONIC_COUPONS_CANNOT_BE_EMPTY);
                }

                for (ElectronAutoSetInVO setInVO : setInVOS) {
                    if (StrUtil.isBlank(setInVO.getGsebId()) || StrUtil.isBlank(setInVO.getGseasQty())) {
                        throw new BusinessException(UtilMessage.ACTIVITY_NUMBER_OR_SENDING_QUANTITY_CANNOT_BE_EMPTY);
                    }
                    setInVO.setClient(inVo.getClient());
                    setInVO.setGseasFlag(themeSettingVO.getGsetsFlag());
                    setInVO.setGseasBeginDate(toAutoSetVO.getGseasBeginDate());
                    setInVO.setGseasEndDate(toAutoSetVO.getGseasEndDate());
                    setInVO.setGseasValid(UtilMessage.Y);
                }
                //判断时间冲突  //自动发券表
                List<GaiaSdElectronAutoSet> timeList = this.gaiaSdElectronAutoSetMapper.timeComparison(toAutoSetVO);

                //用券
                ElectronVoucherInVO voucher = inVo.getVoucher();
                List<String> storeIds = inVo.getVoucher().getStoreIds();
                //List<String> storeIds = this.getBrIds(inVo.getClient(), voucher.getBrStart(), voucher.getBrEnd(), voucher.getStoreType(), voucher.getStoreGroup(), voucher.getStoreIds());
                if (CollUtil.isEmpty(storeIds)) {
                    throw new BusinessException(UtilMessage.NOT_MATCHED_TO_THE_CORRESPONDING_STORE);
                }
                //用券表
                List<String> voucherGsebIds = new ArrayList<>();
                storeIds.forEach(brId -> voucher.getChildInVOS().stream().map(ElectronChildInVO::getGsebId).forEach(voucherGsebIds::add));
                List<GaiaSdElectronBusinessSet> electronBusinessSets = this.gaiaSdElectronBusinessSetMapper.determineWhetherTheTimeIsRepeated(inVo.getClient(), storeIds, voucher.getGsebsBeginDate(), voucher.getGsebsEndDate(), voucherGsebIds, UtilMessage.ONE);
                map.put("code", UtilConst.CODE_0);
                map.put("data", UtilConst.CODE_202.toString());

                if (CollUtil.isNotEmpty(electronBusinessSets) && CollUtil.isNotEmpty(timeList)) {
                    map.put("message", UtilMessage.SET_WITH_COUPON_OR_SELECT_DATE_TO_REPEAT);
                    return map;
                }

                if (CollUtil.isNotEmpty(timeList)) {
                    map.put("message", UtilMessage.THE_SELECTED_E_COUPON_DATE_IS_REPEATED);
                    return map;
                }

                if (CollUtil.isNotEmpty(electronBusinessSets)) {
                    map.put("message", UtilMessage.VOUCHER_DUPLICATE_DATE);
                    return map;
                }

            }
        }
        map.put("code", UtilConst.CODE_0);
        map.put("data", UtilConst.CODE_200);
        map.put("message", "success");

        return map;
    }

    @Override
    public List<ElectronBasicOutData> listUnusedElectronBasic(ElectronBasicInData inData) {
        return gaiaSdElectronBusinessSetMapper.listUnusedElectronBasic(inData);
    }

    @Override
    public GaiaProductBusinessZdysVo QueryOfProductGroupZdys(GetLoginOutData userInfo) {
        GaiaProductBusinessZdysVo vo = new GaiaProductBusinessZdysVo();
        List<String> listZdy1 = businessMapper.getListZdy1(userInfo.getClient());
        List<String> listZdy2 = businessMapper.getListZdy2(userInfo.getClient());
        List<String> listZdy3 = businessMapper.getListZdy3(userInfo.getClient());
        List<String> listZdy4 = businessMapper.getListZdy4(userInfo.getClient());
        List<String> listZdy5 = businessMapper.getListZdy5(userInfo.getClient());
        if(CollectionUtils.isEmpty(listZdy1)){
            vo.setZdy1(new ArrayList<>());
        }else {
            vo.setZdy1(listZdy1);
        }
        if(CollectionUtils.isEmpty(listZdy2)){
            vo.setZdy2(new ArrayList<>());
        }else {
            vo.setZdy2(listZdy2);
        }
        if(CollectionUtils.isEmpty(listZdy3)){
            vo.setZdy3(new ArrayList<>());
        }else {
            vo.setZdy3(listZdy3);
        }
        if(CollectionUtils.isEmpty(listZdy4)){
            vo.setZdy4(new ArrayList<>());
        }else {
            vo.setZdy4(listZdy4);
        }
        if(CollectionUtils.isEmpty(listZdy5)){
            vo.setZdy5(new ArrayList<>());
        }else {
            vo.setZdy5(listZdy5);
        }
        return vo;
    }

    public void queryStatus(ElectronicStatusInData inData) {
        List<GaiaSdElectronBusinessSet> businessSets = this.gaiaSdElectronBusinessSetMapper.getBusinessSetByStatus(inData.getClient(), inData.getGsetsId());
        if (CollUtil.isEmpty(businessSets)) {
            throw new BusinessException(UtilMessage.THE_SUBJECT_NUMBER_IS_WRONG);
        }
        List<GaiaSdElectronBusinessSet> sets = new ArrayList<>();
        for (GaiaSdElectronBusinessSet businessSet : businessSets) {
            if (StrUtil.isNotBlank(inData.getGsebsStatus()) && UtilMessage.Y.equals(inData.getGsebsStatus())) {
                //修改审核状态
                businessSet.setGsebsStatus(UtilMessage.N);
            }
            if(StrUtil.isNotBlank(inData.getGsebsStatus()) && UtilMessage.D.equals(inData.getGsebsStatus())){
                businessSet.setGsebsStatus(UtilMessage.N);
                businessSet.setGsebsFlag(UtilMessage.N);
            }
            if (StrUtil.isNotBlank(inData.getGsebsFlag()) && UtilMessage.Y.equals(inData.getGsebsFlag()) && UtilMessage.Y.equals(businessSet.getGsebsFlag())) {
                //修改已审核和已停用的状态
                businessSet.setGsebsFlag(UtilMessage.N);
                businessSet.setGsebsStatus(UtilMessage.N);
            }
            sets.add(businessSet);
        }
        if (CollUtil.isNotEmpty(sets)) {
            this.gaiaSdElectronBusinessSetMapper.batchUpdate(sets);
        }
    }
}
