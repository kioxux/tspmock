//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetDeviceCheckInData;
import com.gys.business.service.data.GetDeviceCheckOutData;
import com.gys.business.service.data.device.MaintenanceForm;
import com.gys.common.data.PageInfo;

import java.util.List;
import java.util.Map;

public interface DeviceCheckService {
    PageInfo<GetDeviceCheckOutData> selectList(GetDeviceCheckInData inData);

    void save(GetDeviceCheckInData inData);

    void update(GetDeviceCheckInData inData);

    void approve(GetDeviceCheckInData inData);

    List<GetDeviceCheckOutData> findList(MaintenanceForm maintenanceForm);

}
