package com.gys.business.service.data.mib.mibEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：gyx
 * @Date ：Created in 14:15 2021/12/28
 * @Description：医疗收费项目类别
 * @Modified By：gyx
 * @Version:
 */
@Getter
@AllArgsConstructor
public enum MedPayTypeEnum {
    CWF("01","床位费"),
    ZCF("02","诊查费"),
    JCF("03","检查费"),
    HYF("04","化验费"),
    ZLF("05","治疗费"),
    SSF("06","手术费"),
    HLF("07","护理费"),
    WSCLF("08","卫生材料费"),
    XYF("09","西药费"),
    ZYYPF("10","中药饮片费"),
    ZCYF("11","中成药费"),
    YBZLF("12","一般诊疗费"),
    GHF("13","挂号费"),
    QTF("14","其他费");

    private String code;
    private String value;

    public static MedPayTypeEnum getEnumByCode(String code){
        for (MedPayTypeEnum medPayTypeEnum : values()) {
            if (code.equals(medPayTypeEnum.getCode())) {
                return medPayTypeEnum;
            }
        }
        return null;
    }

}
