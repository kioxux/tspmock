package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/11 11:33 下午
 */
@Data
public class RequestOrderDetailListVO {
    @ApiModelProperty(value = "请货单明细列表", example = "[]")
    private List<RequestOrderDetailVO> list;
    @ApiModelProperty(value = "请货单统计汇总信息", example = "{}")
    private OrderDetailCountVO listNum;
    @ApiModelProperty(value = "表头信息", example = "{}")
    private HeadInfo headInfo;
}
