package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaSdStoresGroupMapper;
import com.gys.business.mapper.GaiaStoreDiscountMapper;
import com.gys.business.mapper.entity.GaiaStoreCategoryType;
import com.gys.business.mapper.entity.GaiaStoreDiscount;
import com.gys.business.mapper.entity.GaiaStoreDiscountData;
import com.gys.business.service.GaiaStoreDiscountService;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.StoreAttributeEnum;
import com.gys.common.enums.StoreDTPEnum;
import com.gys.common.enums.StoreMedicalEnum;
import com.gys.common.enums.StoreTaxClassEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.ValidateUtil;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GaiaStoreDiscountServiceImpl implements GaiaStoreDiscountService {
    @Autowired
    private GaiaStoreDiscountMapper gaiaStoreDiscountMapper;
    @Resource
    public CosUtils cosUtils;
    @Resource
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;
    @Resource
    private SalesSummaryServiceImpl salesSummaryService;

    @Override
    public List<String> getStoTaxRateList(String client) {
        List<String> stoTaxRateList = gaiaStoreDiscountMapper.findStoTaxRateList(client);
        return stoTaxRateList;
    }

    @Override
    public List<Map<String, Object>> getStoCodeList(Map<String, Object> inData) {
        List<Map<String, Object>> stoCodeList = gaiaStoreDiscountMapper.findStoCodeList(inData);
        return stoCodeList;
    }

    @Override
    public Map<String, Object> selectDiscouList(GaiaStoreDiscountData gaiaStoreDiscountData) {
        Map<String, Object> result = new HashMap<>();
        if (ObjectUtil.isEmpty(gaiaStoreDiscountData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(gaiaStoreDiscountData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        String gssgId = gaiaStoreDiscountData.getGssgId();
        if (StringUtils.isNotBlank(gssgId)) {
            gaiaStoreDiscountData.setGssgIds(Arrays.asList(gssgId.split(StrUtil.COMMA)));
        }
        Set<String> stoGssgTypeSet = new HashSet<>();
        String stoGssgType = gaiaStoreDiscountData.getStoGssgType();
        boolean noChooseFlag = true;
        if (stoGssgType != null) {
            List<GaiaStoreCategoryType> stoGssgTypes = new ArrayList<>();
            for (String s : stoGssgType.split(StrUtil.COMMA)) {
                String[] str = s.split(StrUtil.UNDERLINE);
                GaiaStoreCategoryType gaiaStoreCategoryType = new GaiaStoreCategoryType();
                gaiaStoreCategoryType.setGssgType(str[0]);
                gaiaStoreCategoryType.setGssgId(str[1]);
                stoGssgTypes.add(gaiaStoreCategoryType);
                stoGssgTypeSet.add(str[0]);
            }
            gaiaStoreDiscountData.setStoGssgTypes(stoGssgTypes);
            noChooseFlag = false;
        }
        String stoAttribute = gaiaStoreDiscountData.getStoAttribute();
        if (stoAttribute != null) {
            noChooseFlag = false;
            gaiaStoreDiscountData.setStoAttributes(Arrays.asList(stoAttribute.split(StrUtil.COMMA)));
        }
        String stoIfMedical = gaiaStoreDiscountData.getStoIfMedical();
        if (stoIfMedical != null) {
            noChooseFlag = false;
            gaiaStoreDiscountData.setStoIfMedicals(Arrays.asList(stoIfMedical.split(StrUtil.COMMA)));
        }
        String stoTaxClass = gaiaStoreDiscountData.getStoTaxClass();
        if (stoTaxClass != null) {
            noChooseFlag = false;
            gaiaStoreDiscountData.setStoTaxClasss(Arrays.asList(stoTaxClass.split(StrUtil.COMMA)));
        }
        String stoIfDtp = gaiaStoreDiscountData.getStoIfDtp();
        if (stoIfDtp != null) {
            noChooseFlag = false;
            gaiaStoreDiscountData.setStoIfDtps(Arrays.asList(stoIfDtp.split(StrUtil.COMMA)));
        }
        List<Map<String, Object>> list = gaiaStoreDiscountMapper.findSalesSummaryByBrId(gaiaStoreDiscountData);
        List<GaiaStoreCategoryType> storeCategoryByClient = gaiaSdStoresGroupMapper.selectStoreCategoryByClient(gaiaStoreDiscountData.getClient());

        for (Map<String, Object> item : list) {
            String brId = MapUtil.getStr(item, "brId");
            // 转换
            if (stoAttribute != null || noChooseFlag) {
                item.put("stoAttribute", StoreAttributeEnum.getName(MapUtil.getStr(item, "stoAttribute")));
            }
            if (stoIfMedical != null || noChooseFlag) {
                item.put("stoIfMedical", StoreMedicalEnum.getName(MapUtil.getStr(item, "stoIfMedical")));
            }
            if (stoIfDtp != null || noChooseFlag) {
                item.put("stoIfDtp", StoreDTPEnum.getName(MapUtil.getStr(item, "stoIfDtp")));
            }
            if (stoTaxClass != null || noChooseFlag) {
                item.put("stoTaxClass", StoreTaxClassEnum.getName(MapUtil.getStr(item, "stoTaxClass")));
            }
            List<GaiaStoreCategoryType> collect = storeCategoryByClient.stream().filter(t -> t.getGssgBrId().equals(brId)).collect(Collectors.toList());

            Set<String> tmpStoGssgTypeSet = new HashSet<>(stoGssgTypeSet);
            for (GaiaStoreCategoryType gaiaStoreCategoryType : collect) {
                String field = null;
                boolean flag = false;
                String gssgType = gaiaStoreCategoryType.getGssgType();
                if (noChooseFlag) {
                    if (gssgType.contains("DX0001")) {
                        field = "shopType";
                    } else if (gssgType.contains("DX0002")) {
                        field = "storeEfficiencyLevel";
                    } else if (gssgType.contains("DX0003")) {
                        field = "directManaged";
                    } else if (gssgType.contains("DX0004")) {
                        field = "managementArea";
                    }
                    if (StringUtils.isNotBlank(field)) {
                        item.put(field, gaiaStoreCategoryType.getGssgIdName());
                    }
                } else {
                    if (!stoGssgTypeSet.isEmpty()) {
                        if (tmpStoGssgTypeSet.contains("DX0001") && "DX0001".equals(gssgType)) {
                            field = "shopType";
                            flag = true;
                            tmpStoGssgTypeSet.remove("DX0001");
                        } else if (tmpStoGssgTypeSet.contains("DX0002") && "DX0002".equals(gssgType)) {
                            field = "storeEfficiencyLevel";
                            flag = true;
                            tmpStoGssgTypeSet.remove("DX0002");
                        } else if (tmpStoGssgTypeSet.contains("DX0003") && "DX0003".equals(gssgType)) {
                            field = "directManaged";
                            flag = true;
                            tmpStoGssgTypeSet.remove("DX0003");
                        } else if (tmpStoGssgTypeSet.contains("DX0004") && "DX0004".equals(gssgType)) {
                            field = "managementArea";
                            flag = true;
                            tmpStoGssgTypeSet.remove("DX0004");
                        }
                    }
                    if (flag) {
                        item.put(field, gaiaStoreCategoryType.getGssgIdName());
                    }
                }
            }
        }
        String json = JSONUtil.toJsonStr(list);
        JSONArray objects = JSONUtil.parseArray(json);
        List<GaiaStoreDiscountData> maps = JSONUtil.toList(objects, GaiaStoreDiscountData.class);
        result.put("list", maps);
        return result;
    }

    @Override
    public Result exportDiscoun(GetLoginOutData userInfo, GaiaStoreDiscountData gaiaStoreDiscountData) {
        Map<String, Object> stringObjectMap = this.selectDiscouList(gaiaStoreDiscountData);
        List<Map<String, Object>> gauaStoreDiscountData1 = (List<Map<String, Object>>) stringObjectMap.get("list");
        List<GaiaStoreDiscountData> maps = new ArrayList<>();
        if (CollectionUtil.isEmpty(gauaStoreDiscountData1)) {
        } else {
            String json = JSONUtil.toJsonStr(gauaStoreDiscountData1);
            JSONArray objects = JSONUtil.parseArray(json);
            maps = JSONUtil.toList(objects, GaiaStoreDiscountData.class);
            for (GaiaStoreDiscountData gauaStoreDiscount : maps) {
                StringBuilder stringBuilder = new StringBuilder();
                StringBuilder builder = new StringBuilder();
                if (StrUtil.isNotBlank(gauaStoreDiscount.getGsdBeginDate()) && StrUtil.isNotBlank(gauaStoreDiscount.getGsdEndDate())) {
                    gauaStoreDiscount.setDiscountPeriod(stringBuilder.append(gauaStoreDiscount.getGsdBeginDate()).append("-").append(gauaStoreDiscount.getGsdEndDate()).toString());
                } else if (StrUtil.isNotBlank(gauaStoreDiscount.getGsdBeginDate()) && StrUtil.isBlank(gauaStoreDiscount.getGsdEndDate())) {
                    gauaStoreDiscount.setDiscountPeriod(gauaStoreDiscount.getGsdBeginDate());
                } else if (StrUtil.isBlank(gauaStoreDiscount.getGsdBeginDate()) && StrUtil.isNotBlank(gauaStoreDiscount.getGsdEndDate())) {
                    gauaStoreDiscount.setDiscountPeriod(gauaStoreDiscount.getGsdEndDate());
                }
                if (StrUtil.isNotBlank(gauaStoreDiscount.getGsdUserId()) && StrUtil.isNotBlank(gauaStoreDiscount.getGsdUserName())) {
                    gauaStoreDiscount.setUserNameByid(builder.append(gauaStoreDiscount.getGsdUserId()).append("-").append(gauaStoreDiscount.getGsdUserName()).toString());
                } else if (StrUtil.isNotBlank(gauaStoreDiscount.getGsdUserId()) && StrUtil.isBlank(gauaStoreDiscount.getGsdUserName())) {
                    gauaStoreDiscount.setUserNameByid(gauaStoreDiscount.getGsdUserId());
                } else if (StrUtil.isBlank(gauaStoreDiscount.getGsdUserId()) && StrUtil.isNotBlank(gauaStoreDiscount.getGsdUserName())) {
                    gauaStoreDiscount.setUserNameByid(gauaStoreDiscount.getGsdUserName());
                }
                if (StrUtil.isNotBlank(gaiaStoreDiscountData.getStoAttribute()) && StrUtil.isNotBlank(gauaStoreDiscount.getStoAttribute())) {
                    gauaStoreDiscount.setClassification(gauaStoreDiscount.getStoAttribute());
                }
                if (StrUtil.isNotBlank(gaiaStoreDiscountData.getStoIfDtp()) && StrUtil.isNotBlank(gauaStoreDiscount.getStoIfDtp())) {
                    gauaStoreDiscount.setClassification(gauaStoreDiscount.getStoIfDtp());
                }
                if (StrUtil.isNotBlank(gaiaStoreDiscountData.getStoIfMedical()) && StrUtil.isNotBlank(gauaStoreDiscount.getStoIfMedical())) {
                    gauaStoreDiscount.setClassification(gauaStoreDiscount.getStoIfMedical());
                }
                if (StrUtil.isNotBlank(gaiaStoreDiscountData.getStoTaxClass()) && StrUtil.isNotBlank(gauaStoreDiscount.getStoTaxClass())) {
                    gauaStoreDiscount.setClassification(gauaStoreDiscount.getStoTaxClass());
                }

                    if (StrUtil.isNotBlank(gauaStoreDiscount.getStoreEfficiencyLevel())) {
                        gauaStoreDiscount.setClassification(gauaStoreDiscount.getStoreEfficiencyLevel());
                    } else if (StrUtil.isNotBlank(gauaStoreDiscount.getShopType())) {
                        gauaStoreDiscount.setClassification(gauaStoreDiscount.getShopType());
                    } else if (StrUtil.isNotBlank(gauaStoreDiscount.getDirectManaged())) {
                        gauaStoreDiscount.setClassification(gauaStoreDiscount.getDirectManaged());
                    } else if (StrUtil.isNotBlank(gauaStoreDiscount.getManagementArea())) {
                        gauaStoreDiscount.setClassification(gauaStoreDiscount.getManagementArea());
                    }
                    if (StrUtil.isNotBlank(gauaStoreDiscount.getStoAttribute())){
                        gauaStoreDiscount.setClassification(gauaStoreDiscount.getStoAttribute());
                    }
            }
        }
        String fileName = "门店折扣率";
        if (maps.size() > 0) {
            CsvFileInfo csvInfo = null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(maps, fileName, Collections.singletonList((short) 1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据!");
        }
    }
    @Override
    public Boolean insertStore(GetLoginOutData loginUser, List<GaiaStoreDiscountData> gaiaStoreDiscountDataList) {
        if (CollectionUtil.isEmpty(gaiaStoreDiscountDataList)) {
            throw new BusinessException("请填入需要保存的相关信息！");
        }
        for (GaiaStoreDiscountData gaiaStoreDiscountData : gaiaStoreDiscountDataList) {
            gaiaStoreDiscountData.setClient(loginUser.getClient());
            this.insertStoreList(loginUser, gaiaStoreDiscountData);
        }
        return true;
    }


    private Boolean insertStoreList(GetLoginOutData loginUser, GaiaStoreDiscountData gaiaStoreDiscountData) {
        GaiaStoreDiscount gauaStoreDiscount = new GaiaStoreDiscount();
        gaiaStoreDiscountData.setClient(loginUser.getClient());
        if (gaiaStoreDiscountData.getStoCode().isEmpty()){
            throw new BusinessException("门店编码不能为空");
        }
       GaiaStoreDiscountData storeDiscountData = gaiaStoreDiscountMapper.findselectStoCood(gaiaStoreDiscountData);
       if (ObjectUtil.isNotEmpty(storeDiscountData)) {
           throw new BusinessException("当前保存数据门店"+storeDiscountData.getStoCode()+"-"+storeDiscountData.getStoName()+"或折扣期间已存在！");
       }
        if (gaiaStoreDiscountData.getGsdBeginDate().compareTo(gaiaStoreDiscountData.getGsdEndDate())>0){
            throw new BusinessException("开始时间不能大于结束时间");
        }

        String stoName = null;
        Map<String,Object> inData =  new HashMap<>();
        inData.put("clientId",loginUser.getClient());
        inData.put("stoCode",loginUser.getDepId());
        List<Map<String, Object>> maps = salesSummaryService.selectStoreList(inData);
        List<Map<String, Object>> collect = maps.stream().filter(map ->
                map.get("stoCode").equals(gaiaStoreDiscountData.getStoCode()) && map.get("stoName").equals(gaiaStoreDiscountData.getStoName())
        ).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(collect)){
            throw new BusinessException("当前门店编码或门店名称不存在");
        }
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        String format = simpleDateFormat.format(date);
        String[] split = format.split("-");

        gauaStoreDiscount.setGsdUserId(loginUser.getUserId());
        gauaStoreDiscount.setGsdUserName(loginUser.getLoginName());
        gauaStoreDiscount.setGsdStoModiId(loginUser.getUserTel());
        gauaStoreDiscount.setClient(gaiaStoreDiscountData.getClient());
        gauaStoreDiscount.setStoCode(gaiaStoreDiscountData.getStoCode());
        gauaStoreDiscount.setStoName(gaiaStoreDiscountData.getStoName());
        gauaStoreDiscount.setGsdBeginDate(gaiaStoreDiscountData.getGsdBeginDate());
        gauaStoreDiscount.setGsdEndDate(gaiaStoreDiscountData.getGsdEndDate());
        gauaStoreDiscount.setGsdStoRebate(gaiaStoreDiscountData.getGsdStoRebate());
        gauaStoreDiscount.setGsdStoModiDate(split[0]);
        gauaStoreDiscount.setGsdStoModiTime(split[1]);
        gauaStoreDiscount.setGsdUserId(loginUser.getUserId());
        gauaStoreDiscount.setGsdUserName(loginUser.getLoginName());
        gauaStoreDiscount.setGsdStoModiId(loginUser.getUserTel());
        gaiaStoreDiscountMapper.insertStoreList(gauaStoreDiscount);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object importStoreDiscoun(MultipartFile file, GetLoginOutData userInfo) {
        List<GaiaStoreDiscountData> insertDataList = checkFileData(file);
        for (int i = 0; i < insertDataList.size(); i++) {
            GaiaStoreDiscountData billOfArInData = insertDataList.get(i);
            if (StringUtils.isEmpty(billOfArInData.getStoCode())) {
                throw new BusinessException("第" + (i + 2) + "行数据，门店编码不能为空！");
            }
            if (ObjectUtil.isEmpty(billOfArInData.getDiscountPeriod())) {
                throw new BusinessException("第" + (i + 2) + "行数据，折扣日期不能为空！");
            }
            if (ObjectUtil.isEmpty(billOfArInData.getGsdStoRebate())) {
                throw new BusinessException("第" + (i + 2) + "行数据，折扣率不能为空！");
            }
            if (StrUtil.isNotBlank(billOfArInData.getDiscountPeriod())){
                String[] split = billOfArInData.getDiscountPeriod().split("-");
                billOfArInData.setGsdBeginDate(split[0]);
                if (split.length > 0){
                    billOfArInData.setGsdEndDate(split[1]);
                }
            }
            this.insertStoreList(userInfo, billOfArInData);
        }
        return true;
    }



    private List<GaiaStoreDiscountData> checkFileData(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }

        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaStoreDiscountData.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传商品目录,读取excel失败: ", e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<GaiaStoreDiscountData> relationList = new ArrayList<>(excelDataList.size());
        try {
            relationList = JSON.parseArray(JSON.toJSONString(excelDataList), GaiaStoreDiscountData.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：", e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(relationList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //校验表头
        GaiaStoreDiscountData dataHead = relationList.get(0);
        String[] channelManagerExcelHead = CommonConstant.GAIA_STORE_DISCOUNT;
        if (!channelManagerExcelHead[0].equals(dataHead.getStoCode()) && !channelManagerExcelHead[1].equals(dataHead.getStoName())
                && !channelManagerExcelHead[2].equals(dataHead.getDiscountPeriod()) && !channelManagerExcelHead[3].equals(dataHead.getGsdStoRebate())
              ) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<GaiaStoreDiscountData> insertData = relationList.subList(1, relationList.size());
        if (ValidateUtil.isEmpty(insertData)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return insertData;
    }


    @Override
    public String updateStoRebate(GetLoginOutData userInfo, GaiaStoreDiscountData gaiaStoreDiscountData) {
        gaiaStoreDiscountData.setClient(userInfo.getClient());
        GaiaStoreDiscountData storeDiscountData = gaiaStoreDiscountMapper.findselectStoCood(gaiaStoreDiscountData);
        if (ObjectUtil.isNotEmpty(storeDiscountData)) {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
            String format = simpleDateFormat.format(date);
            String[] split = format.split("-");
            gaiaStoreDiscountData.setGsdStoModiDate(split[0]);
            gaiaStoreDiscountData.setGsdStoModiTime(split[1]);
            gaiaStoreDiscountData.setGsdUserId(userInfo.getUserId());
            gaiaStoreDiscountData.setGsdUserName(userInfo.getLoginName());
            gaiaStoreDiscountData.setGsdStoModiId(userInfo.getUserTel());
            gaiaStoreDiscountMapper.updateGsdStoRebate(gaiaStoreDiscountData);
        }else
        {
            throw new BusinessException("提示：当前数据不存在, 请检查数据");
        }
        return null;
    }

}
