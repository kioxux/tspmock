package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaFiApBalance;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

/**
 * <p>
 * 供应商应付期末余额 服务类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
public interface IGaiaFiApBalanceService {

    /**
     * 功能描述: 每月结算供应商应付期末余额
     *
     * @param day
     * @return void
     * @author wangQc
     * @date 2021/8/27 5:13 下午
     */
    void monthlySettlement(LocalDate day);
}
