package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class ModulePositionVo implements Serializable {

    private static final long serialVersionUID = -249053446516343354L;

    private String moduleName;

    private String moduleCode;

    List<CommonVo> positions;
}
