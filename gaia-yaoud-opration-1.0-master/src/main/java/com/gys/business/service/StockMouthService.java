package com.gys.business.service;

import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.data.ClientSiteChoose;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;

public interface StockMouthService {

    void caculateByCondition(StockMouthCaculateCondition condition);

    PageInfo<StockMouth> getStockMouthListPage(StockMouthCaculateCondition condition);

    ClientSiteChoose clientSite();

    Result export(StockMouthCaculateCondition condition);

    void caculateByCron();
}
