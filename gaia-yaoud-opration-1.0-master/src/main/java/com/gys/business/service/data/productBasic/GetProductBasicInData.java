package com.gys.business.service.data.productBasic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author ：gyx
 * @Date ：Created in 10:05 2021/10/14
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class GetProductBasicInData {

    private String client;

    private Integer pageSize=100;

    private Integer pageNum=1;

    private String queryString;

    private List<String> queryStringList;
    //商品编码集合
    private List<String> proCodeList;
    //通用名称
    private String proCommonName;
    //国际条形码1
    private String proBarCode;
    //批准文号
    private String proRegisterNo;
    //生产企业代码
    private String proFactoryName;
    //品牌标识名
    private String proBrand;
    //品牌区分
    private String proBrandClass;
    //商品分类描述集合
    private List<String> proClassCodeList;
    //成分分类描述集合
    private List<String> proCompClassCodeList;
    //用法用量为空
    private String proUsageIsNull;
    //禁忌为空
    private String proContraindicationIsNull;
    //注意事项为空
    private String proNoticeThingsIsNull;

    private List<LinkedHashMap<String,String>> fieldMap;

    private Set<String> fieldSet;

    @ApiModelProperty(hidden = true)
    private List<String> clients;

    @ApiModelProperty(" '' | 1: 客户关系列表页面使用，2: 维护客户关系页面使用")
    private String searchFlag;

}
