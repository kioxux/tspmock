//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GaiaSdRechargeCardLoseInData {
    private String gsrclStatus;
    private String gsrclBrId;
    private String gsrclDate;
    private String gsrclTime;
    private String gsrclEmp;
    private String gsrclCardId;
    private String gsrclReason;

    public GaiaSdRechargeCardLoseInData() {
    }

    public String getGsrclStatus() {
        return this.gsrclStatus;
    }

    public String getGsrclBrId() {
        return this.gsrclBrId;
    }

    public String getGsrclDate() {
        return this.gsrclDate;
    }

    public String getGsrclTime() {
        return this.gsrclTime;
    }

    public String getGsrclEmp() {
        return this.gsrclEmp;
    }

    public String getGsrclCardId() {
        return this.gsrclCardId;
    }

    public String getGsrclReason() {
        return this.gsrclReason;
    }

    public void setGsrclStatus(final String gsrclStatus) {
        this.gsrclStatus = gsrclStatus;
    }

    public void setGsrclBrId(final String gsrclBrId) {
        this.gsrclBrId = gsrclBrId;
    }

    public void setGsrclDate(final String gsrclDate) {
        this.gsrclDate = gsrclDate;
    }

    public void setGsrclTime(final String gsrclTime) {
        this.gsrclTime = gsrclTime;
    }

    public void setGsrclEmp(final String gsrclEmp) {
        this.gsrclEmp = gsrclEmp;
    }

    public void setGsrclCardId(final String gsrclCardId) {
        this.gsrclCardId = gsrclCardId;
    }

    public void setGsrclReason(final String gsrclReason) {
        this.gsrclReason = gsrclReason;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdRechargeCardLoseInData)) {
            return false;
        } else {
            GaiaSdRechargeCardLoseInData other = (GaiaSdRechargeCardLoseInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$gsrclStatus = this.getGsrclStatus();
                    Object other$gsrclStatus = other.getGsrclStatus();
                    if (this$gsrclStatus == null) {
                        if (other$gsrclStatus == null) {
                            break label95;
                        }
                    } else if (this$gsrclStatus.equals(other$gsrclStatus)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gsrclBrId = this.getGsrclBrId();
                Object other$gsrclBrId = other.getGsrclBrId();
                if (this$gsrclBrId == null) {
                    if (other$gsrclBrId != null) {
                        return false;
                    }
                } else if (!this$gsrclBrId.equals(other$gsrclBrId)) {
                    return false;
                }

                Object this$gsrclDate = this.getGsrclDate();
                Object other$gsrclDate = other.getGsrclDate();
                if (this$gsrclDate == null) {
                    if (other$gsrclDate != null) {
                        return false;
                    }
                } else if (!this$gsrclDate.equals(other$gsrclDate)) {
                    return false;
                }

                label74: {
                    Object this$gsrclTime = this.getGsrclTime();
                    Object other$gsrclTime = other.getGsrclTime();
                    if (this$gsrclTime == null) {
                        if (other$gsrclTime == null) {
                            break label74;
                        }
                    } else if (this$gsrclTime.equals(other$gsrclTime)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gsrclEmp = this.getGsrclEmp();
                    Object other$gsrclEmp = other.getGsrclEmp();
                    if (this$gsrclEmp == null) {
                        if (other$gsrclEmp == null) {
                            break label67;
                        }
                    } else if (this$gsrclEmp.equals(other$gsrclEmp)) {
                        break label67;
                    }

                    return false;
                }

                Object this$gsrclCardId = this.getGsrclCardId();
                Object other$gsrclCardId = other.getGsrclCardId();
                if (this$gsrclCardId == null) {
                    if (other$gsrclCardId != null) {
                        return false;
                    }
                } else if (!this$gsrclCardId.equals(other$gsrclCardId)) {
                    return false;
                }

                Object this$gsrclReason = this.getGsrclReason();
                Object other$gsrclReason = other.getGsrclReason();
                if (this$gsrclReason == null) {
                    if (other$gsrclReason != null) {
                        return false;
                    }
                } else if (!this$gsrclReason.equals(other$gsrclReason)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdRechargeCardLoseInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsrclStatus = this.getGsrclStatus();
        result = result * 59 + ($gsrclStatus == null ? 43 : $gsrclStatus.hashCode());
        Object $gsrclBrId = this.getGsrclBrId();
        result = result * 59 + ($gsrclBrId == null ? 43 : $gsrclBrId.hashCode());
        Object $gsrclDate = this.getGsrclDate();
        result = result * 59 + ($gsrclDate == null ? 43 : $gsrclDate.hashCode());
        Object $gsrclTime = this.getGsrclTime();
        result = result * 59 + ($gsrclTime == null ? 43 : $gsrclTime.hashCode());
        Object $gsrclEmp = this.getGsrclEmp();
        result = result * 59 + ($gsrclEmp == null ? 43 : $gsrclEmp.hashCode());
        Object $gsrclCardId = this.getGsrclCardId();
        result = result * 59 + ($gsrclCardId == null ? 43 : $gsrclCardId.hashCode());
        Object $gsrclReason = this.getGsrclReason();
        result = result * 59 + ($gsrclReason == null ? 43 : $gsrclReason.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdRechargeCardLoseInData(gsrclStatus=" + this.getGsrclStatus() + ", gsrclBrId=" + this.getGsrclBrId() + ", gsrclDate=" + this.getGsrclDate() + ", gsrclTime=" + this.getGsrclTime() + ", gsrclEmp=" + this.getGsrclEmp() + ", gsrclCardId=" + this.getGsrclCardId() + ", gsrclReason=" + this.getGsrclReason() + ")";
    }
}
