package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdStoresGroupMapper;
import com.gys.business.mapper.entity.GaiaSdStoresGroup;
import com.gys.business.service.StoresGroupService;
import com.gys.business.service.data.StoresGroupInData;
import com.gys.business.service.data.StoresGroupOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.util.DateUtil;
import com.qcloud.cos.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StoresGroupServiceImpl implements StoresGroupService {


    @Autowired
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;

    @Override
    public List<StoresGroupOutData> listAllStoresGroupByClientId(StoresGroupInData param) {
        return gaiaSdStoresGroupMapper.listAllStoresGroupByClientId(param);
    }

    @Override
    public List<StoresGroupOutData> listAllStoresInfoByClientId(StoresGroupInData param) {
        param.setGroupType(null);
        List<StoresGroupOutData> storesGroupOutData = gaiaSdStoresGroupMapper.listAllStoresInfoByClientId(param);
        param.setGroupType("DX0001");
        List<StoresGroupOutData> storesGroupOutData1 = gaiaSdStoresGroupMapper.listAllStoresInfoByClientId(param);
        param.setGroupType("DX0002");
        List<StoresGroupOutData> storesGroupOutData2 = gaiaSdStoresGroupMapper.listAllStoresInfoByClientId(param);
        for (StoresGroupOutData outData : storesGroupOutData) {
            for (StoresGroupOutData groupOutData : storesGroupOutData1) {
                if (groupOutData.getStoCode().equals(outData.getStoCode())){
                    outData.setGssgId(groupOutData.getGssgId());
                    outData.setGssgIdName(groupOutData.getGssgIdName());
                }
            }
            for (StoresGroupOutData groupOutData : storesGroupOutData2) {
                if (groupOutData.getStoCode().equals(outData.getStoCode())){
                    outData.setEffectId(groupOutData.getGssgId());
                    outData.setEffectName(groupOutData.getGssgIdName());
                }
            }
        }
        return storesGroupOutData;
    }

    @Override
    public boolean insertOrUpdateStoresGroup(GetLoginOutData userInfo, List<GaiaSdStoresGroup> groupList) {
        String date = DateUtil.getCurrentDate();
        String time = cn.hutool.core.date.DateUtil.format(new Date(), "HHmmss");
        if (!CollectionUtils.isNullOrEmpty(groupList)) {
            for (GaiaSdStoresGroup storesGroup : groupList) {
                storesGroup.setClientId(userInfo.getClient());
                storesGroup.setGssgUpdateEmp(userInfo.getUserId());
                storesGroup.setGssgUpdateDate(date);
                storesGroup.setGssgUpdateTime(time);
            }
            gaiaSdStoresGroupMapper.addAllList(groupList);
        }
        return true;
    }
}
