package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaProductSpecialRecordOutData implements Serializable {

    /**
     * 商品编码  药德通用编码
     */
    @ApiModelProperty(value="商品编码  药德通用编码")
    private String proCode;

    /**
     * 商品描述
     */
    @ApiModelProperty(value="商品描述")
    private String proDesc;

    /**
     * 商品规格
     */
    @ApiModelProperty(value="商品规格")
    private String proSpecs;

    /**
     * 商品生产厂家
     */
    @ApiModelProperty(value="商品生产厂家")
    private String proFactoryName;

    /**
     * 单位
     */
    @ApiModelProperty(value="单位")
    private String proUnit;

    /**
     * 成分分类
     */
    @ApiModelProperty(value="成分分类")
    private String proCompClass;

    /**
     * 成分分类描述
     */
    @ApiModelProperty(value="成分分类描述")
    private String proCompClassName;

    /**
     * 商品分类
     */
    @ApiModelProperty(value="商品分类")
    private String proClass;

    /**
     * 商品分类描述
     */
    @ApiModelProperty(value="商品分类描述")
    private String proClassName;

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private String city;

    /**
     * 类型  1-季节，2-品牌
     */
    @ApiModelProperty(value="类型  1-季节，2-品牌")
    private String type;

    /**
     * 更新字段
     */
    @ApiModelProperty(value="修改字段")
    private String changeField;

    /**
     * 更新字段前值
     */
    @ApiModelProperty(value="更新字段前值")
    private String changeFromValue;

    /**
     * 更新字段后值
     */
    @ApiModelProperty(value="更新字段后值")
    private String changeToValue;

    /**
     * 更新日期
     */
    @ApiModelProperty(value="更新日期")
    private String changeDate;

    /**
     * 更新人
     */
    @ApiModelProperty(value="更新人")
    private String changeUser;
}
