package com.gys.business.service.data.zjnb;

import lombok.Data;

import java.math.BigDecimal;


/**
 * @author wuai
 * @description: 5)医疗费用支付结构
 * @date 2021/10/22 16:18
 */
@Data
public class MedicalChargePayDto {
    /**
     * 医疗费用总额	10	保留2位小数，∑医疗费用金额[ 、参见“医疗费用明细单体(反馈)”处说明。]
     */
    private BigDecimal medFeeSumAmt;
    /**
     * 医疗费用自费应付总额	10	保留2位小数
     */
    private BigDecimal fulAmtOwnPayAmt;
    /**
     * 医疗费用自付应付总额	10	保留2位小数
     */
    private BigDecimal selfPayAmt;
    /**
     * 现金支付数(现金A)	10	个人自费的现金部分，保留2位小数
     */
    private BigDecimal cashPayA;
    /**
     * 现金支付数(现金B)	10	个人自付的现金部分，保留2位小数
     */
    private BigDecimal cashPayB;
    /**
     * 现金支付数(现金C)	10	个人自负的现金部分，保留2位小数
     */
    private BigDecimal cashPayC;
    /**
     * 现金支付数(现金D)	10	个人承担的现金部分，保留2位小数
     */
    private BigDecimal cashPayD;
    /**
     * 统筹支付数	10	保留2位小数
     */
    private BigDecimal poolPay;
    /**
     * 救助支付数	10	保留2位小数
     */
    private BigDecimal salvaPay;
    /**
     * 个人当年帐户支付数	10	保留2位小数
     */
    private BigDecimal psnAcctPay;
    /**
     * 个人历年帐户支付数	10	保留2位小数
     */
    private BigDecimal psnAcctHisPay;
    /**
     * 社会救助支付数	10	保留2位小数，2016年5月开始返回职工大病保险支付数；大病特药结算时基金支付数在此字段返回
     */
    private BigDecimal socialSalvaPay;
    /**
     * 公补基金支付数	10	保留2位小数
     */
    private BigDecimal publicFundPay;
    /**
     * 家庭账户支付数	10	保留2位小数 原“其他支付数1”
     */
    private BigDecimal familyAcctPay;
    /**
     * 其他支付项支付数	10	保留2位小数 原“其他支付数2”
     */
    private BigDecimal othPay;
    /**
     * 医疗救（补）助支付数	10	保留2位小数 原“其他支付数3”
     */
    private BigDecimal mafPay;

}
