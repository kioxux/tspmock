
package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GetMemberInData implements Serializable {
    private static final long serialVersionUID = 1629464568751381941L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "会员卡号")
    private String gmbCardId;

    @ApiModelProperty(value = "会员名称")
    private String gmbName;

    @ApiModelProperty(value = "地址")
    private String gmbAddress;

    @ApiModelProperty(value = "旧手机号")
    private String gmbMobile;

    @ApiModelProperty(value = "新手机号")
    private String newMobile;

    @ApiModelProperty(value = "电话")
    private String gmbTel;

    @ApiModelProperty(value = "性别  1:男 / 0 :女")
    private String gmbSex;

    @ApiModelProperty(value = "身份证号")
    private String gmbCredentials;

    @ApiModelProperty(value = "年龄")
    private String gmbAge;

    @ApiModelProperty(value = "出生年月")
    private String gmbBirth;

    @ApiModelProperty(value = "幼儿姓名")
    private String gmbBbName;

    @ApiModelProperty(value = "幼儿性别 1:男 /  0 :女")
    private String gmbBbSex;

    @ApiModelProperty(value = "幼儿年龄")
    private String gmbBbAge;


    private String gmbStatus;

    @ApiModelProperty(value = "备注")
    private String remark;


    private String saler;

    @ApiModelProperty(value = "起始")
    private int pageNum;

    @ApiModelProperty(value = "总共")
    private int pageSize;

    @ApiModelProperty(value = "卡类型")
    private String gmbCardClass;

    @ApiModelProperty(value = "类型Id")
    private String gsmcId;


    private String storedValueCard;

    @ApiModelProperty(value = "渠道 1-可修改积分渠道 else-不可修改积分渠道")
    private String channel;

    @ApiModelProperty(value = "积分")
    private String integral;

    @ApiModelProperty(value = "修改人员")
    private String updateEmp;

    @ApiModelProperty(value = "会员编号")
    private String memberId;

    /**
     * 推荐人
     */
    private String recommender;
    /**
     * 激活状态
     */
    private String gsmbcState;
    /**
     * 激活人工号
     */
    private String gsmbcJhUser;

    /**
     * 激活日期
     */
    private String gsmbcJhDate;

    /**
     * 激活时间
     */
    private String gsmbcJhTime;

    /**
     * 会员商圈编号
     */
    private String gsmbcArea;

    /**
     * 民族名称
     */
    private String gsmbNationality;

    /**
     * 生日历法 默认为0 -公历（阳历）；1-农历（阴历）
     */
    private String gsmbBirthMode;
}
