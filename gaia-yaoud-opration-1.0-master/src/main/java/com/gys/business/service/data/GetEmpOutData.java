package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetEmpOutData implements Serializable {
    private static final long serialVersionUID = 8837828454284666598L;
    private String storName;
    private String no;
    private String orgName;
    private String name;
    private String empId;
    private String id;
}
