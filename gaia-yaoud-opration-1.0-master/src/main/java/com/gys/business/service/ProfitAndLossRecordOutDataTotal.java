package com.gys.business.service;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProfitAndLossRecordOutDataTotal {
    @ApiModelProperty(value = "差异量合计")
    private BigDecimal difference;

    @ApiModelProperty(value = "成本额合计")
    private BigDecimal costAmount;

    @ApiModelProperty(value = "零售额合计")
    private BigDecimal retailSales;

    @ApiModelProperty(value = "库存")
    private BigDecimal gsisdStockQty;

    @ApiModelProperty(value = "实际数量")
    private BigDecimal gsisdRealQty;

    @ApiModelProperty(value = "序号合计")
    private Integer index;
}
