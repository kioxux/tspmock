package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.TianLingSReturnService;
import com.gys.business.service.data.*;
import com.gys.common.exception.BusinessException;
import com.gys.feign.PurchaseService;
import com.gys.util.CommonUtil;
import com.gys.util.CopyUtil;
import com.gys.util.UtilMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xiaoyuan on 2020/12/16
 */
@Service
@SuppressWarnings("all")
public class TianLingSReturnServiceImpl implements TianLingSReturnService {


    @Autowired
    private GaiaSdSaleHMapper saleHMapper;

    @Autowired
    private GaiaSdSaleDMapper saleDMapper;

    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;

    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;

    @Autowired
    private GaiaProductBasicMapper productBasicMapper;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private GaiaSdExamineDMapper examineDMapper;

    @Autowired
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Autowired
    private GaiaSdStockMapper stockMapper;

    @Autowired
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Autowired
    private GaiaSdSalePayMsgMapper payMsgMapper;

    @Autowired
    private GaiaSdPaymentMethodMapper paymentMethodMapper;

    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;

    @Autowired
    private GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;

    @Autowired
    private GaiaMaterialAssessMapper assessMapper;

    @Autowired
    private CacheService synchronizedService;

    @Autowired
    private GaiaSdRechargeCardMapper gaiaSdRechargeCardMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> tianLingReturn(SalesInquireData inData) {
        GaiaSdSaleH sd = this.saleHMapper.selectByBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        if (ObjectUtil.isEmpty(sd)) {
            throw new BusinessException(UtilMessage.SALES_SLIP_ABNORMAL);
        }
        GaiaSdSaleH sdSaleH = this.saleHMapper.getSaleHByReturnBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        if (ObjectUtil.isNotEmpty(sdSaleH)) {
            throw new BusinessException(UtilMessage.SALE_RETURN_HAVE);
        }
        List<MaterialDocRequestDto> listDto = new ArrayList<>();//凭证吧
        //生成的退货单号
        String billNo = CommonUtil.generateBillNo(inData.getBrId());
        String guid = UUID.randomUUID().toString();

        if (sd.getGsshYsAmt().compareTo(BigDecimal.ZERO) == 1) {
            // 支付信息表
            List<GaiaSdSalePayMsg> sdSalePayMsgs = this.payMsgMapper.selectByBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId(), sd.getGsshDate());
            if (CollUtil.isEmpty(sdSalePayMsgs)) {
                throw new BusinessException(UtilMessage.PAYMENT_EXCEPTION);
            }
            //全部退款 怎么来的 怎么退
            List<GaiaSdSalePayMsg> payMsgsList = CopyUtil.copyList(sdSalePayMsgs, GaiaSdSalePayMsg.class);
            for (GaiaSdSalePayMsg salePayMsgs : payMsgsList) {
                salePayMsgs.setGsspmDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                salePayMsgs.setGsspmBillNo(billNo);
                salePayMsgs.setGsspmId(salePayMsgs.getGsspmId());
                if ("1000".equals(salePayMsgs.getGsspmId())) {
                    salePayMsgs.setGsspmRmbAmt(salePayMsgs.getGsspmRmbAmt().negate());//退还现金
                    salePayMsgs.setGsspmAmt(salePayMsgs.getGsspmAmt().negate());//应收金额
                } else if ("5000".equals(salePayMsgs.getGsspmId()) && StrUtil.isNotBlank(salePayMsgs.getGsspmCardNo())) {
                    GaiaSdRechargeCard card = this.gaiaSdRechargeCardMapper.getAccountByCardId(salePayMsgs.getClient(), salePayMsgs.getGsspmCardNo());
                    if (ObjectUtil.isEmpty(card)) {
                        throw new BusinessException(UtilMessage.THE_STORED_VALUE_CARD_DOES_NOT_EXIST);
                    }
                    BigDecimal add = card.getGsrcAmt().add(salePayMsgs.getGsspmAmt());
                    card.setGsrcAmt(add);
                    card.setGsrcUpdateBrId(salePayMsgs.getGsspmBrId());
                    card.setGsrcUpdateDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                    card.setGsrcUpdateEmp(inData.getUserId());
                    this.gaiaSdRechargeCardMapper.updateByCardId(card);

                    salePayMsgs.setGsspmAmt(salePayMsgs.getGsspmAmt().negate());//应收金额
                } else {
                    salePayMsgs.setGsspmAmt(salePayMsgs.getGsspmAmt().negate());//应收金额
                }

                salePayMsgs.setGsspmZlAmt(BigDecimal.ZERO);//找零金额
            }
            this.payMsgMapper.insertLists(payMsgsList);
        }

        //门店销售主表
        GaiaSdSaleH saleReturn = this.getGaiaSdSaleH(inData, sd, billNo);

        List<GaiaSdSaleD> saleDList = new ArrayList<>();
        List<GaiaSdBatchChange> changeList = new ArrayList<>();
        List<GaiaSdStock> stockList = new ArrayList<>();
        List<GaiaSdStockBatch> batchList = new ArrayList<>();
        List<GaiaSdSaleD> sdSaleDList = this.saleDMapper.getAllByBillNo(inData.getBillNo(), inData.getClientId(), inData.getBrId());
        List<String> proList = sdSaleDList.stream().map(GaiaSdSaleD::getGssdProId).collect(Collectors.toList());
        //获得批号异动表
        Map<String, GaiaSdBatchChange> batchChangeMap = this.getBatchChangeMap(inData, proList);
        //商品主数据
        Map<String, GaiaProductBusiness> businessMap = this.getBusinessMap(inData, proList);
        //物料评估表
        Map<String, GaiaMaterialAssess> assessMap = this.getMaterialAssessMap(inData, proList);

        List<GaiaDataSynchronized> synStockList = new ArrayList<>();
        for (GaiaSdSaleD detailInData : sdSaleDList) {
            GaiaSdSaleD saleD = this.getGaiaSdSaleD(inData, billNo, assessMap, detailInData);
            saleDList.add(saleD);
            //加库存
            this.inventory(inData, billNo, saleD, guid, batchChangeMap, changeList, listDto, businessMap,synStockList);
        }

        if (CollUtil.isEmpty(saleDList) || CollUtil.isEmpty(changeList)) {
            throw new BusinessException("此单可能存在一些异常!!!");
        }


        BigDecimal finalPoints = BigDecimal.ZERO;
        if (StrUtil.isNotBlank(saleReturn.getGsshHykNo()) && StrUtil.isNotBlank(inData.getSalesMemberCardNum())) {
            GaiaSdMemberCardToData cardData = this.memberBasicMapper.selectByCardId(inData.getClientId(), inData.getBrId(), inData.getMemberId(),inData.getSalesMemberCardNum());
            if (ObjectUtil.isEmpty(cardData)) {
                return null;
            } else {
                BigDecimal integral = new BigDecimal(StrUtil.isBlank(cardData.getGsmbcIntegral()) ? "0.000" : cardData.getGsmbcIntegral()); //当前积分
                BigDecimal amtSale = new BigDecimal(StrUtil.isBlank(cardData.getGsmcAmtSale()) ? "0.000" : cardData.getGsmcAmtSale());//消费金额
                BigDecimal integralSale = new BigDecimal(StrUtil.isBlank(cardData.getGsmcIntegralSale()) ? "0.000" : cardData.getGsmcIntegralSale());//积分数值

                if (StrUtil.isBlank(inData.getRefundAmount())) {
                    throw new BusinessException(UtilMessage.THE_RETURN_AMOUNT_CANNOT_BE_EMPTY);
                }
                if (amtSale.compareTo(BigDecimal.ZERO) == 0) {
                    cardData.setGsmbcIntegral(BigDecimal.ZERO.toString());
                    cardData.setGsmbcZeroDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                } else {
                    //退货总金额 / 消费金额  * 积分数值 = 积分
                    finalPoints = new BigDecimal(inData.getRefundAmount()).divide(amtSale, 0, RoundingMode.DOWN).multiply(integralSale).setScale(0, BigDecimal.ROUND_DOWN);
                    //当前积分 -  退货积分
                    BigDecimal atlast = integral.subtract(finalPoints);

                    if (integral.compareTo(BigDecimal.ZERO) == 0 || atlast.compareTo(BigDecimal.ZERO) == -1) {
                        cardData.setGsmbcIntegral(BigDecimal.ZERO.toString());
                        cardData.setGsmbcZeroDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                    } else {
                        cardData.setGsmbcIntegral(atlast.toString());
                    }
                }
                cardData.setGsmbcIntegralLastdate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
                this.memberBasicMapper.updateByCardId(cardData);

                GaiaDataSynchronized aSyncStockBatch = this.getGaiaDataSynchronized(inData, cardData);
                this.synchronizedService.addOne(aSyncStockBatch);

            }
        }

        this.saleDMapper.insertLists(saleDList);//批量插入D表
        this.batchChangeMapper.insertLists(changeList);//批量插入 批次异动表

        saleReturn.setGsshIntegralAdd(finalPoints.toString());
        this.saleHMapper.insert(saleReturn);
        this.synchronizedService.insertLists(synStockList);
        sd.setGsshCrStatus("2");
        this.saleHMapper.update(sd);
        Map<String, Object> map = new HashMap<>(8);
        //物料凭证
        if (CollUtil.isNotEmpty(listDto)) {
            map.put("listDto", listDto);
        }
        map.put("billNo",billNo);
        return map;
    }

    public GaiaSdSaleH getGaiaSdSaleH(SalesInquireData inData, GaiaSdSaleH sd, String billNo) {
        GaiaSdSaleH saleReturn = CopyUtil.copy(sd, GaiaSdSaleH.class);
        saleReturn.setClientId(inData.getClientId());//加盟号
        saleReturn.setGsshBrId(sd.getGsshBrId());//店号
        saleReturn.setGsshDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));//日期
        saleReturn.setGsshTime(DateUtil.format(new Date(), UtilMessage.HHMMSS));//时间
        saleReturn.setGsshYsAmt(sd.getGsshYsAmt().negate()); //应收金额 负数(从明细表算出退货数量 和金额)
        saleReturn.setGsshEmp(inData.getUserId());//收银工号(当前人)
        saleReturn.setGsshEmpReturn(inData.getGsshEmpReturn()); //退货审核人
        saleReturn.setGsshHideFlag(sd.getGsshHideFlag());//挂单不显示
        saleReturn.setGsshBillNoReturn(inData.getBillNo());//原销售单号
        saleReturn.setGsshBillNo(billNo);//新销售单号(退货单号)
        saleReturn.setGsshCallAllow(UtilMessage.ONE);
        saleReturn.setGsshReturnStatus(UtilMessage.ZERO);
        saleReturn.setGsshNormalAmt(sd.getGsshNormalAmt().negate());//零售总金额
        saleReturn.setGsshCrStatus("2");
        saleReturn.setGsshReturnStatus(UtilMessage.ONE);
        return saleReturn;
    }

    public GaiaDataSynchronized getGaiaDataSynchronized(SalesInquireData inData, GaiaSdMemberCardToData cardData) {
        Map<String, Object> map = new HashMap<>(8);
        map.put("CLIENT", cardData.getClient());
        map.put("GSMBC_MEMBER_ID", cardData.getGsmbcMemberId());
        map.put("GSMBC_BR_ID", cardData.getGsmbcBrId());
        GaiaDataSynchronized aSyncStockBatch = new GaiaDataSynchronized();
        aSyncStockBatch.setClient(inData.getClientId());
        aSyncStockBatch.setSite(inData.getBrId());
        aSyncStockBatch.setVersion(IdUtil.randomUUID());
        aSyncStockBatch.setTableName("GAIA_SD_MEMBER_CARD");
        aSyncStockBatch.setCommand("update");
        aSyncStockBatch.setSourceNo("天灵退货-退积分");
        aSyncStockBatch.setArg(JSON.toJSONString(map));
        return aSyncStockBatch;
    }

    public GaiaSdSaleD getGaiaSdSaleD(SalesInquireData inData, String billNo, Map<String, GaiaMaterialAssess> assessMap, GaiaSdSaleD detailInData) {
        //门店销售明细表
        GaiaSdSaleD saleD = CopyUtil.copy(detailInData, GaiaSdSaleD.class);
        GaiaMaterialAssess assess = assessMap.get(saleD.getGssdProId());
        //购买数量
        BigDecimal gssdQty = new BigDecimal(detailInData.getGssdQty());
        //退货数量
        BigDecimal Qty = new BigDecimal(NumberUtil.parseNumber(saleD.getGssdQty()).toString());

        if (gssdQty.compareTo(Qty) == 0) {//判断当前行 是否是全部退
            saleD.setGssdAmt(detailInData.getGssdAmt().negate());//退货金额 反转
        } else {//部分退
            //退货数量 乘以 单品应收价格
            BigDecimal gssdAmt = Qty.multiply(detailInData.getGssdPrc2());
            saleD.setGssdAmt(gssdAmt.negate());//汇总应收金额(单品应收价  *  退货数量)  负数
        }
        saleD.setGssdPrc1(saleD.getGssdPrc1().negate());// 单品零售价
        saleD.setGssdPrc2(saleD.getGssdPrc2().negate());//单品应收价  负数
        saleD.setGssdQty("-" + saleD.getGssdQty());//退货数量
        saleD.setGssdSalerId(inData.getUserId());
        saleD.setClientId(inData.getClientId());
        saleD.setGssdBillNo(billNo);
        saleD.setGssdProStatus(UtilMessage.RETURN_STATUS);
        saleD.setGssdDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
        BigDecimal sub = saleD.getGssdPrc1().subtract(saleD.getGssdPrc2()).setScale(4, BigDecimal.ROUND_UP);//减去
        BigDecimal zkAmt = sub.multiply(new BigDecimal(saleD.getGssdQty())).setScale(4, BigDecimal.ROUND_UP);//乘以
        saleD.setGssdZkAmt(zkAmt.negate());
        if (ObjectUtil.isNotEmpty(assess)) {
            saleD.setGssdMovPrices(assess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())));//移动平均总价
            saleD.setGssdTaxRate(assess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())).multiply(saleD.getGssdMovTax()));//移动税额
            saleD.setGssdAddAmt(assess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())));//加点后金额
            saleD.setGssdAddTax(assess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())).multiply(saleD.getGssdMovTax()));//加点后税金
        }
        return saleD;
    }

    /**
     * 查询商品主数据
     *
     * @param inData
     * @return
     */
    private Map<String, GaiaProductBusiness> getBusinessMap(SalesInquireData inData, List<String> proList) {
        Map<String, GaiaProductBusiness> businessMap = new HashMap<>(16);
        Example examplePro = new Example(GaiaProductBusiness.class);
        examplePro.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSite", inData.getBrId()).andIn("proSelfCode", proList);
        List<GaiaProductBusiness> businesses = this.productBusinessMapper.selectByExample(examplePro);
        for (GaiaProductBusiness business : businesses) {
            businessMap.put(business.getProSelfCode(), business);
        }
        return businessMap;
    }

    /**
     * 获取批次异动表当前订单的所有数据
     *
     * @param inData
     * @param detailInData
     * @return
     */
    private Map<String, GaiaSdBatchChange> getBatchChangeMap(SalesInquireData inData, List<String> gsbcProId) {
        Map<String, GaiaSdBatchChange> objectMap;
        Example batchChangeExample = new Example(GaiaSdBatchChange.class);
        batchChangeExample.createCriteria().andEqualTo("gsbcVoucherId", inData.getBillNo()).andEqualTo("client", inData.getClientId())
                .andEqualTo("gsbcBrId", inData.getBrId()).andEqualTo("gsbcDate", inData.getGsshDate()).andIn("gsbcProId", gsbcProId);
        List<GaiaSdBatchChange> batchChanges = this.batchChangeMapper.selectByExample(batchChangeExample);
        objectMap = batchChanges.stream().collect(Collectors.toMap(GaiaSdBatchChange::getGsbcProId, batchChange -> batchChange, (a, b) -> b, () -> new HashMap<>(16)));
        return objectMap;
    }

    /**
     * 查询物料评估表
     * @param inData
     * @param proList
     * @return
     */
    private Map<String, GaiaMaterialAssess> getMaterialAssessMap(SalesInquireData inData, List<String> proList) {
        List<GaiaMaterialAssess> assessList = this.assessMapper.getAllByProIds(inData.getClientId(), inData.getBrId(), proList);
        Map<String, GaiaMaterialAssess> map = assessList.stream().collect(Collectors.toMap(GaiaMaterialAssess::getMatProCode, assess -> assess, (a, b) -> b, () -> new HashMap<>(8)));
        return map;
    }


    /**
     * 更新添加 批次,库存,批号库存表等信息
     *  @param detailInData
     * @param inData
     * @param billNo
     * @param listDto
     * @param synStockList
     */
    @Transactional(rollbackFor = Exception.class)
    public void inventory(SalesInquireData inData, String billNo, GaiaSdSaleD saleD,
                          String guid, Map<String, GaiaSdBatchChange> objectMap,
                          List<GaiaSdBatchChange> changeList, List<MaterialDocRequestDto> listDto,
                          Map<String, GaiaProductBusiness> businessMap,
                          List<GaiaDataSynchronized> synStockList) {

        // 异动批次表
        GaiaSdBatchChange gaiaSdBatchChange = objectMap.get(saleD.getGssdProId());
        if (ObjectUtil.isNotEmpty(gaiaSdBatchChange)) {
            if (StrUtil.isBlank(gaiaSdBatchChange.getGsbcBatch())) {
                throw new BusinessException("该商品 : " + saleD.getGssdProId() + " 批次异动表没有批次信息!");
            }
            //更新批次异动表
            GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
            batchChange.setClient(inData.getClientId());
            batchChange.setGsbcBrId(inData.getBrId());
            batchChange.setGsbcVoucherId(billNo);
            batchChange.setGsbcSerial(saleD.getGssdSerial());
            batchChange.setGsbcDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
            batchChange.setGsbcProId(saleD.getGssdProId());
            batchChange.setGsbcBatchNo(saleD.getGssdBatchNo());
            batchChange.setGsbcBatch(gaiaSdBatchChange.getGsbcBatch());
            batchChange.setGsbcQty(new BigDecimal(saleD.getGssdQty()).negate());
            changeList.add(batchChange);

            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", gaiaSdBatchChange.getClient());
            map.put("GSBC_BR_ID", gaiaSdBatchChange.getGsbcBrId());
            map.put("GSBC_VOUCHER_ID", gaiaSdBatchChange.getGsbcVoucherId());
            map.put("GSBC_DATE", gaiaSdBatchChange.getGsbcDate());
            map.put("GSBC_SERIAL", gaiaSdBatchChange.getGsbcSerial());
            map.put("GSBC_PRO_ID", gaiaSdBatchChange.getGsbcProId());
            map.put("GSBC_BATCH", gaiaSdBatchChange.getGsbcBatch());
            GaiaDataSynchronized aSyncStockBatch = new GaiaDataSynchronized();
            aSyncStockBatch.setClient(inData.getClientId());
            aSyncStockBatch.setSite(inData.getBrId());
            aSyncStockBatch.setVersion(IdUtil.randomUUID());
            aSyncStockBatch.setTableName("GAIA_SD_BATCH_CHANGE");
            aSyncStockBatch.setCommand("update");
            aSyncStockBatch.setSourceNo("退货");
            aSyncStockBatch.setArg(JSON.toJSONString(map));
            synStockList.add(aSyncStockBatch);

        }


        GaiaProductBusiness productBusiness = businessMap.get(saleD.getGssdProId());
        if (ObjectUtil.isNotEmpty(productBusiness)) {
            MaterialDocRequestDto dto = new MaterialDocRequestDto();
            dto.setClient(inData.getClientId());
            dto.setGuid(guid);
            dto.setGuidNo(saleD.getGssdSerial());
            dto.setMatPostDate(saleD.getGssdDate());
            dto.setMatHeadRemark("");
            dto.setMatType("LX");
            dto.setMatProCode(saleD.getGssdProId());
            dto.setMatStoCode("");
            dto.setMatSiteCode(inData.getBrId());
            dto.setMatLocationCode("1000");
            dto.setMatLocationTo("");
            dto.setMatBatch(gaiaSdBatchChange.getGsbcBatch());
            dto.setMatQty(new BigDecimal(saleD.getGssdQty()).negate());//转正数
            dto.setMatUnit(productBusiness.getProUnit());
            dto.setMatDebitCredit("S");
            dto.setMatPrice(new BigDecimal(0));
            dto.setMatPoId(billNo);
            dto.setMatPoLineno(saleD.getGssdSerial());
            dto.setMatDnId(billNo);
            dto.setMatDnLineno(saleD.getGssdSerial());
            dto.setMatCreateBy(inData.getUserId());
            dto.setMatCreateDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
            dto.setMatCreateTime(DateUtil.format(new Date(), UtilMessage.HHMMSS));
            listDto.add(dto);
        }
    }
}
