package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdOutOfStockPushRecordMapper;
import com.gys.business.service.OutOfStockPushRecordService;
import com.gys.business.service.data.GaiaSdOutOfStockPushRecord;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/16 16:00
 */
@Service("outOfStockPushRecordService")
public class OutOfStockPushRecordServiceImpl implements OutOfStockPushRecordService {
    @Resource
    private GaiaSdOutOfStockPushRecordMapper gaiaSdOutOfStockPushRecordMapper;

    @Override
    public void updatePushRecord(String client, String userId) {
        GaiaSdOutOfStockPushRecord cond = new GaiaSdOutOfStockPushRecord();
        cond.setClient(client);
        cond.setReceiveEmp(userId);
        cond.setBrId("out_of_stock");
        GaiaSdOutOfStockPushRecord latestRecord = gaiaSdOutOfStockPushRecordMapper.getLatestRecord(cond);
        if (latestRecord.getStatus() == 0) {
            latestRecord.setStatus(1);
            latestRecord.setReadEmp(userId);
            latestRecord.setReadTime(new Date());
            gaiaSdOutOfStockPushRecordMapper.updateByPrimaryKey(latestRecord);
        }
    }
}
