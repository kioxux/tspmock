package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetPoDetailOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "序号")
    private String indexNo;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "采购订单数量")
    private String proQty;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "零售价")
    private String gssdPrc1;
    @ApiModelProperty(value = "单价")
    private String proPrice;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "行金额")
    private String poLineAmt;
    private String proMadeDate;
    private String proValiedDate;
    private String poLineNo;
    @ApiModelProperty(value = "末次进价")
    private BigDecimal lastTimePoPrice;
}
