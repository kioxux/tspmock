package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaProductBasic;
import com.gys.business.service.ProductSpecialService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.*;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class ProductSpecialServiceImpl implements ProductSpecialService {

    @Autowired
    private GaiaProductSpecialMapper productSpecialMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaProductSpecialChangeMapper productSpecialChangeMapper;
    @Autowired
    private GaiaProductSpecialParamMapper productSpecialParamMapper;
    @Autowired
    private GaiaProductSpecialParamChangeMapper productSpecialParamChangeMapper;
    @Autowired
    private CosUtils cosUtils;
    @Autowired
    private HttpServletRequest request;

    @Override
    public JsonResult getSelectFrame(SelectFrameInData inData) {
        String frameType = inData.getFrameType();

        Map<String, Object> result = new HashMap<>(3);
        //地区列表
        if("all".equals(frameType) || "1".equals(frameType) || ValidateUtil.isEmpty(frameType)) {
            List<AreaOutData> areaList = productSpecialMapper.getAreaList();
            List<ProvinceCityOutData> provinceCityList = getAreInfo(areaList);
            result.put("areaList", provinceCityList);
        }
        //标志列表
        if("all".equals(frameType) || "2".equals(frameType) || ValidateUtil.isEmpty(frameType)) {
            String queryType = inData.getQueryType();
            List<SelectFrameFlagOutData> flagList = productSpecialMapper.getProFlagList(ValidateUtil.isEmpty(queryType) ? "1" : queryType);
            result.put("flagList", flagList);
        }
        //商品成分分类列表
        if("all".equals(frameType) || "3".equals(frameType) || ValidateUtil.isEmpty(frameType)) {
            List<ProductComponentClassOutData> classList = productSpecialMapper.getClassList();
            List<ProductComponentClassCodeVo> classListInfo = getClassInfo(classList);
            result.put("classList", classListInfo);
        }
        return JsonResult.success(result, "success");
    }


    /**
     * 获取省市下拉框数据
     * @param areaList
     * @return
     */
    private List<ProvinceCityOutData> getAreInfo(List<AreaOutData> areaList) {
        List<ProvinceCityOutData> provinceCityList = new ArrayList<>(areaList.size());
        if(ValidateUtil.isNotEmpty(areaList)) {
            //将list根据 level转成map  key为level value为对应的list
            Map<String, List<AreaOutData>> areaListMap = areaList.stream().collect(Collectors.groupingBy(item -> item.getLevel()));
            List<AreaOutData> provinceList = areaListMap.get("1");   //获取省列表
            List<AreaOutData> cityList = areaListMap.get("2");       //获取市列表
            //遍历省集合
            for(AreaOutData province : provinceList) {
                ProvinceCityOutData provinceCity = new ProvinceCityOutData();
                //获取省集合中每个元素的地区编号
                String areaId = province.getAreaId();
                //对应的省下市集合
                List<AreaOutData> cityChildList = cityList.stream().filter(item -> areaId.equals(item.getParentId())).collect(Collectors.toList());

                //组装参数
                provinceCity.setAreaId(areaId);
                provinceCity.setAreaName(province.getAreaName());
                provinceCity.setLevel(province.getLevel());
                provinceCity.setParentId(province.getParentId());
                provinceCity.setCityChildList(cityChildList);
                provinceCityList.add(provinceCity);
            }
        }
        return provinceCityList;
    }


    /**
     * 获取药品成分分类列表
     * @param classList
     * @return
     */
    private List<ProductComponentClassCodeVo> getClassInfo(List<ProductComponentClassOutData> classList) {
        if(ValidateUtil.isEmpty(classList)) {
            return null;
        }

        //获取第一级list  根据成分中类编码去重
        List<ProductComponentClassOutData> midList = classList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(
                                () -> new TreeSet<>(Comparator.comparing(item -> item.getMidCode()))
                        ),
                        ArrayList :: new
                )
        );
        //获取第二级list  根据成分小类编码去重
        List<ProductComponentClassOutData> litList = classList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(
                                () -> new TreeSet<>(Comparator.comparing(item -> item.getLitCode()))
                        ),
                        ArrayList :: new
                )
        );
        //获取第三级list  根据成分分类编码去重
        List<ProductComponentClassOutData> compList = classList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(
                                () -> new TreeSet<>(Comparator.comparing(item -> item.getCompCode()))
                        ),
                        ArrayList :: new
                )
        );

        //处理第三级list
        List<ProductComponentClassCodeVo> threeNodeList = new ArrayList<>();
        for(ProductComponentClassOutData comp : compList) {
            ProductComponentClassCodeVo threeNode = new ProductComponentClassCodeVo();
            threeNode.setCompCode(comp.getCompCode());
            threeNode.setCompName(comp.getCompName());
            threeNodeList.add(threeNode);
        }

        //组装第二级list
        List<ProductComponentClassCodeVo> secondList = new ArrayList<>();
        for(ProductComponentClassOutData componentClass : litList) {
            //获取成分小类编码
            String litCode = componentClass.getLitCode();
            List<ProductComponentClassCodeVo> threeNodeResultList = threeNodeList.stream()
                    .filter(item -> item.getCompCode().startsWith(litCode)).collect(Collectors.toList());
            //二级节点明细列表
            ProductComponentClassCodeVo secondNode = new ProductComponentClassCodeVo();
            secondNode.setCompCode(litCode);
            secondNode.setCompName(componentClass.getLitName());
            secondNode.setChildList(threeNodeResultList);
            secondList.add(secondNode);
        }

        //组装第一级list
        List<ProductComponentClassCodeVo> resultList = new ArrayList<>(classList.size());
        for(ProductComponentClassOutData componentClass : midList) {
            //获取成分中类编码
            String midCode = componentClass.getMidCode();
            List<ProductComponentClassCodeVo> secondNodeResultList = secondList.stream()
                    .filter(item -> item.getCompCode().startsWith(midCode)).collect(Collectors.toList());
            //一级节点明细列表
            ProductComponentClassCodeVo firstNode = new ProductComponentClassCodeVo();
            firstNode.setCompCode(midCode);
            firstNode.setCompName(componentClass.getMidName());
            firstNode.setChildList(secondNodeResultList);
            resultList.add(firstNode);
        }


        /*//第一级
        List<ProductComponentClassOutData> midNodeList = classList.stream()
                .filter(distinctByKey(ProductComponentClassOutData::getMidCode)).map(item -> {
                    ProductComponentClassOutData componentClass = new ProductComponentClassOutData();
                    componentClass.setCompCode(item.getMidCode());
                    componentClass.setCompName(item.getLitName());
                    return componentClass;
                }).collect(Collectors.toList());

        //第二级
        midNodeList.stream().forEach(item ->{
            List<ProductComponentClassOutData> litNodeList = classList.stream().filter(itemClass -> itemClass.getMidCode().equals(item.getCompCode()))
                    .filter(distinctByKey(ProductComponentClassOutData::getLitCode))
                    .map(itemClass -> {
                        ProductComponentClassOutData componentClass = new ProductComponentClassOutData();
                        componentClass.setCompCode(itemClass.getLitCode());
                        componentClass.setCompName(itemClass.getLitName());
                        return componentClass;
                    }).collect(Collectors.toList());
            item.setChildList(litNodeList);

            //第三级
            litNodeList.stream().forEach(item1 -> {
                List<ProductComponentClassOutData> compList1 = classList.stream()
                        .filter(item2 -> item2.getLitCode().equals(item1.getCompCode()))
                        .map(item2 -> {
                            ProductComponentClassOutData componentClass = new ProductComponentClassOutData();
                            componentClass.setCompCode(item2.getCompCode());
                            componentClass.setCompName(item2.getCompName());
                            return componentClass;
                        }).collect(Collectors.toList());
                item1.setChildList(compList1);
            });
        });*/

        return resultList;
    }


    /**
     * 去重key
     * @param keyExtractor
     * @param <T>
     * @return
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }


    @Override
    public JsonResult getProductInfo(ProductBasicInfoInData inData) {
        int pageNum = inData.getPageNum();
        int pageSize = inData.getPageSize();
        String proCodeStr = inData.getContent();
        if(pageNum == 0) {
            pageNum = CommonConstant.PAGE_NUM;
        }
        if(pageSize == 0) {
            pageSize = CommonConstant.PAGE_SIZE;
        }
        if(ValidateUtil.isNotEmpty(proCodeStr)) {
            String[] proCodeArr = proCodeStr.split("\\s+ |\\s+|,");
            inData.setProCodeArr(proCodeArr);
        }
        PageHelper.startPage(pageNum, pageSize);
        List<ProductBasicInfoOutData> productInfoByPage = productBasicMapper.getProductInfoByPage(inData);
        if(ValidateUtil.isEmpty(productInfoByPage)) {
            JsonResult.success(productInfoByPage, "success");
        }

        PageInfo pageData = new PageInfo(productInfoByPage);
        return JsonResult.success(pageData, "success");
    }


    @Override
    public JsonResult getProductSpecialList(GaiaProductSpecialInData inData) {
        String queryType = inData.getQueryType();
        if(ValidateUtil.isEmpty(queryType)) {
            throw new BusinessException("提示：查询类型不能为空");
        }
        //查询季节品列表
        List<GaiaProductSpecialOutData> productSpecialList = productSpecialMapper.getProductSpecialList(inData);
        return JsonResult.success(productSpecialList, "success");
    }


    @Override
    @Transactional
    public JsonResult addProductSpecial(AddProductSpecialInData inData) {
        String addFlag = inData.getAddFlag();
        String proCodeOrCompCode = inData.getProCodeOrCompCode();
        String province = inData.getProvince();
        String city = inData.getCity();
        String proType = inData.getProType();
        String proFlag = inData.getProFlag();
        String createUser = inData.getCreateUser();
        //校验参数
        if(ValidateUtil.isEmpty(addFlag)) {
            throw new BusinessException("提示：新增标志不能为空");
        }
        if(ValidateUtil.isEmpty(proCodeOrCompCode)) {
            throw new BusinessException("提示：商品/成分编码不能为空");
        }
        if(ValidateUtil.isEmpty(province) || ValidateUtil.isEmpty(city)) {
            throw new BusinessException("提示：区域不能为空");
        }
        if(ValidateUtil.isEmpty(proType)) {
            throw new BusinessException("提示：商品类型不能为空");
        }
        if(ValidateUtil.isEmpty(proFlag)) {
            throw new BusinessException("提示：商品标识不能为空");
        }

        //新增商品
        if("1".equals(addFlag)) {
            //查询该商品是否存在
            GaiaProductBasic basic = new GaiaProductBasic();
            basic.setProCode(proCodeOrCompCode);
            GaiaProductBasic productBasic = productBasicMapper.selectByPrimaryKey(basic);
            if(ValidateUtil.isEmpty(productBasic)) {
                throw new BusinessException("提示：该商品编码不存在, 请重新输入");
            }
            //校验该商品在特殊商品表中是否存在
            GaiaProductSpecial productSpecial = new GaiaProductSpecial();
            productSpecial.setProCode(proCodeOrCompCode);
            productSpecial.setProProv(province);
            productSpecial.setProCity(city);
            productSpecial.setProType(proType);
            productSpecial.setProFlag(proFlag);
            GaiaProductSpecial productSpecialExists = productSpecialMapper.selectByPrimaryKey(productSpecial);
            if(ValidateUtil.isNotEmpty(productSpecialExists)) {
                throw new BusinessException("提示：该商品编码已在特殊商品中存在, 请重新输入");
            }

            //新增季节品
            GaiaProductSpecial product = new GaiaProductSpecial();
            product.setProCode(proCodeOrCompCode);
            product.setProProv(province);
            product.setProCity(city);
            product.setProType(proType);
            product.setProFlag(proFlag);
            product.setProStatus("0");
            product.setProCreateUser(createUser);
            product.setProCreateDate(CommonUtil.getyyyyMMdd());
            product.setProCreateTime(CommonUtil.getHHmmss());
            product.setProLastUpdateUser(createUser);
            product.setProLastUpdateDate(CommonUtil.getyyyyMMdd());
            product.setProLastUpdateTime(CommonUtil.getHHmmss());
            productSpecialMapper.insert(product);
        } else if("2".equals(addFlag)) {   //新增成分分类
            //查询该成分是否存在
            List<String> proCodeList = productBasicMapper.getProCodeListByCompClass(proCodeOrCompCode);
            if(ValidateUtil.isEmpty(proCodeList)) {
                throw new BusinessException("提示：该成分分类编码不存在, 请重新输入");
            }

            StringBuilder errorMsg = new StringBuilder("");
            List<GaiaProductSpecial> productSpecialList = new ArrayList<>(proCodeList.size());
            for(String proCode : proCodeList) {
                GaiaProductSpecial product = new GaiaProductSpecial();
                product.setProCode(proCode);
                product.setProProv(province);
                product.setProCity(city);
                product.setProType(proType);
                product.setProFlag(proFlag);
                product.setProStatus("0");
                product.setProCreateUser(createUser);
                product.setProCreateDate(CommonUtil.getyyyyMMdd());
                product.setProCreateTime(CommonUtil.getHHmmss());
                product.setProLastUpdateUser(createUser);
                product.setProLastUpdateDate(CommonUtil.getyyyyMMdd());
                product.setProLastUpdateTime(CommonUtil.getHHmmss());

                //校验该商品是否已经存在记录 如果存在记录则将商品编号追加到错误信息字符串中 否则新增数据
                GaiaProductSpecial productSpecialExists = productSpecialMapper.selectByPrimaryKey(product);
                if(ValidateUtil.isEmpty(productSpecialExists)) {
                    productSpecialList.add(product);
                } else {
                    errorMsg.append(productSpecialExists.getProCode()).append(",");
                }
            }
            //批量新增
            if(ValidateUtil.isNotEmpty(productSpecialList)) {
                productSpecialMapper.batchInsert(productSpecialList);
            }
            String errorMsgStr = errorMsg.toString();
            if(ValidateUtil.isNotEmpty(errorMsgStr)) {
                return JsonResult.error(2001, "提示：以下商品编码有误，请确认：" + errorMsgStr.substring(0, errorMsgStr.length() - 1));
            }
        }
        return JsonResult.success("", "success");
    }


    @Override
    @Transactional
    public JsonResult invalidProductSpecial(List<GaiaProductSpecial> productList, GetLoginOutData inData) {
        if(ValidateUtil.isEmpty(productList)) {
            throw new BusinessException("提示：请选择修改数据");
        }

        //主表 停用状态 更新人 更新日期 时间
        GaiaProductSpecial productSpecial = new GaiaProductSpecial();
        productSpecial.setProStatus("1");                                //状态
        productSpecial.setProLastUpdateUser(inData.getLoginName());      //变更人
        productSpecial.setProLastUpdateDate(CommonUtil.getyyyyMMdd());   //变更日期
        productSpecial.setProLastUpdateTime(CommonUtil.getHHmmss());     //变更时间
        //修改状态
        productSpecialMapper.invalidProductByCodeList(productList, productSpecial);

        //批量插入变更记录表
        List<GaiaProductSpecialChange> changeList = new ArrayList<>(productList.size());
        for(GaiaProductSpecial product : productList) {
            GaiaProductSpecialChange productChange = new GaiaProductSpecialChange();
            productChange.setId(SnowflakeIdUtil.getNextId());            //ID
            productChange.setProCode(product.getProCode());              //商品编码
            productChange.setProProv(product.getProProv());              //省
            productChange.setProCity(product.getProCity());              //市
            productChange.setProType(product.getProType());              //类型
            productChange.setProChangeField("PRO_STATUS");               //修改字段
            productChange.setProChangeFrom("0");                        //修改前值
            productChange.setProChangeTo("1");                           //修改后值
            productChange.setProChangeUser(inData.getLoginName());       //修改人
            productChange.setProChangeDate(CommonUtil.getyyyyMMdd());    //修改日期
            productChange.setProChangeTime(CommonUtil.getHHmmss());      //修改时间
            changeList.add(productChange);
        }
        //批量插入修改记录
        productSpecialChangeMapper.batchInsert(changeList);
        return JsonResult.success("", "success");
    }


    @Override
    @Transactional
    public JsonResult importData(MultipartFile file, GetLoginOutData userInfo) {
        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaProductSpecial.class);
        //获取到 GaiaProductSpecial实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            log.error("上传特殊商品,读取excel失败: {}",e.getMessage(), e);
            throw new BusinessException("提示：读取Excel失败");
        }

        if(ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        //转换list泛型
        List<GaiaProductSpecial> productSpecialList = new ArrayList<>(excelDataList.size());
        for(Object obj : excelDataList) {
            GaiaProductSpecial temp = (GaiaProductSpecial) obj;
            productSpecialList.add(temp);
        }

        if(ValidateUtil.isNotEmpty(productSpecialList)) {
            //校验表头正确性
            String[] excelHead = CommonConstant.PRODUCT_SPECIAL_IMPORT_HEAD;
            GaiaProductSpecial productSpecialHead = productSpecialList.get(0);
            if(!excelHead[0].equals(productSpecialHead.getProCode()) || !excelHead[1].equals(productSpecialHead.getProProv())
                    || !excelHead[2].equals(productSpecialHead.getProCity()) || !excelHead[3].equals(productSpecialHead.getProType())
                    || !excelHead[4].equals(productSpecialHead.getProFlag()) || !excelHead[5].equals(productSpecialHead.getProStatus())) {
                throw new BusinessException("提示：请填写正确的表头");
            }
            //从第二位元素截取到最后一位元素
            List<GaiaProductSpecial> productSpecialDataList = productSpecialList.subList(1, productSpecialList.size());
            if(ValidateUtil.isEmpty(productSpecialDataList)) {
                throw new BusinessException("提示：上传数据为空, 请检查数据");
            }

            //地区列表
            List<AreaOutData> areaList = productSpecialMapper.getAreaList();
            //将list转为map  key为地区名称 value为地区编号
            Map<String, String> areaNameMap = areaList.stream()
                    .collect(Collectors.toMap(AreaOutData:: getAreaName, AreaOutData:: getAreaId));

            List<ProvCityRelationshipOutData> provCityRelationshipList = productSpecialMapper.getProvCityRelationship();
            //查询省市关系并转为map  key为市编号 value为省编号
            Map<String, String> provCityCodeMap = provCityRelationshipList.stream()
                    .collect(Collectors.toMap(ProvCityRelationshipOutData :: getCityCode, ProvCityRelationshipOutData :: getProvCode));

            //查询类型与标识对应关系
            List<SelectFrameFlagOutData> flagList = productSpecialMapper.getProFlagList(null);
            //将list转为map  key为标识 value为类型 用于校验匹配关系是否正确
            Map<String, String> flagMap = flagList.stream()
                    .collect(Collectors.toMap(SelectFrameFlagOutData :: getFlagId, SelectFrameFlagOutData :: getTypeId));
            List<GaiaProductSpecial> batchReplaceInsertList = new ArrayList<>(productSpecialDataList.size());
            List<GaiaProductSpecialChange> batchChangeInsertList = new ArrayList<>();
            StringBuilder errorMsg = new StringBuilder("");
            int row = 2;
            for(GaiaProductSpecial productSpecial : productSpecialDataList) {
                //校验每行数据是否缺少
                if(ValidateUtil.isEmpty(productSpecial.getProCode()) || ValidateUtil.isEmpty(productSpecial.getProProv())
                        || ValidateUtil.isEmpty(productSpecial.getProCity()) || ValidateUtil.isEmpty(productSpecial.getProType())
                        || ValidateUtil.isEmpty(productSpecial.getProFlag())) {
                    throw new BusinessException("提示：通用编码,省,市,类型,标识不能为空, 请检查数据");
                }
                //查询该商品是否存在
                GaiaProductBasic basic = new GaiaProductBasic();
                basic.setProCode(productSpecial.getProCode());
                GaiaProductBasic productBasic = productBasicMapper.selectByPrimaryKey(basic);
                //如果查询为空 则表示此商品编码在主表中存在，进行添加操作，否则为错误商品编码 追加到错误提示中
                if(ValidateUtil.isEmpty(productBasic)) {
                    errorMsg.append(row).append(",");
                    row++;
                    continue;
                }
                //将中文翻译成码值 存入数据库
                String provCode = areaNameMap.get(productSpecial.getProProv());
                String cityCode = areaNameMap.get(productSpecial.getProCity());
                if(ValidateUtil.isEmpty(provCode) || ValidateUtil.isEmpty(cityCode)) {
                    errorMsg.append(row).append(",");
                    row++;
                    continue;
                }

                //判断省市级关系是否正确
                String correctProvCode = provCityCodeMap.get(cityCode);
                if(!provCode.equals(correctProvCode)) {
                    errorMsg.append(row).append(",");
                    row++;
                    continue;
                }

                //判断标识 类型关系是否正确
                String correctTypeCode = flagMap.get(productSpecial.getProFlag());   //该标识对应正确的类型
                if(!productSpecial.getProType().equals(correctTypeCode)) {   //使用上传的类型与正确的类型进行比较 如果不同则表示错误
                    errorMsg.append(row).append(",");
                    row++;
                    continue;
                }
                productSpecial.setProProv(provCode);
                productSpecial.setProCity(cityCode);
                if(ValidateUtil.isEmpty(productSpecial.getProStatus())) {
                    productSpecial.setProStatus("0");
                }
                productSpecial.setProCreateUser(userInfo.getLoginName());
                productSpecial.setProCreateDate(CommonUtil.getyyyyMMdd());
                productSpecial.setProCreateTime(CommonUtil.getHHmmss());
                productSpecial.setProLastUpdateUser(userInfo.getLoginName());
                productSpecial.setProLastUpdateDate(CommonUtil.getyyyyMMdd());
                productSpecial.setProLastUpdateTime(CommonUtil.getHHmmss());
                //校验该商品是否已经存在记录
                GaiaProductSpecial productSpecialExists = productSpecialMapper.selectByPrimaryKey(productSpecial);
                //如果存在记录则将覆盖掉原有记录并且记录修改数据   否则直接新增数据
                if(ValidateUtil.isNotEmpty(productSpecialExists)) {
                    GaiaProductSpecialChange change = getProductChange(productSpecial, productSpecialExists, userInfo);
                    if(ValidateUtil.isNotEmpty(change)) {
                        batchChangeInsertList.add(change);
                    }
                }
                batchReplaceInsertList.add(productSpecial);
                row++;
            }
            //批量覆盖新增
            if(ValidateUtil.isNotEmpty(batchReplaceInsertList)) {
                productSpecialMapper.batchReplaceInsert(batchReplaceInsertList);
            }
            if(ValidateUtil.isNotEmpty(batchChangeInsertList)) {
                productSpecialChangeMapper.batchInsert(batchChangeInsertList);
            }
            //判断是否有错误数据
            String errorMsgStr = errorMsg.toString();
            if(ValidateUtil.isNotEmpty(errorMsgStr)) {
                return JsonResult.error(2001, "提示：以下行数据有误，请确认：" + errorMsgStr.substring(0, errorMsgStr.length() - 1));
            }
        }
        return JsonResult.success("", "success");
    }


    /**
     * 导入时覆盖修改记录
     * @param newProductSpecial
     * @param oldProductSpecial
     * @param userInfo
     */
    private GaiaProductSpecialChange getProductChange(GaiaProductSpecial newProductSpecial, GaiaProductSpecial oldProductSpecial, GetLoginOutData userInfo) {
        String oldProStatus = oldProductSpecial.getProStatus();
        String newProStatus = newProductSpecial.getProStatus();
        if(oldProStatus.equals(newProStatus)) {
            return null;
        }
        GaiaProductSpecialChange change = new GaiaProductSpecialChange();
        change.setId(SnowflakeIdUtil.getNextId());
        change.setProCode(newProductSpecial.getProCode());
        change.setProProv(newProductSpecial.getProProv());
        change.setProCity(newProductSpecial.getProCity());
        change.setProType(newProductSpecial.getProType());
        change.setProChangeUser(userInfo.getLoginName());
        change.setProChangeDate(CommonUtil.getyyyyMMdd());
        change.setProChangeTime(CommonUtil.getHHmmss());
        change.setProChangeField("PRO_STATUS");
        change.setProChangeFrom(oldProStatus);
        change.setProChangeTo(newProStatus);
        return change;
    }



    @Override
    public Result exportData(GaiaProductSpecialInData inData) {
        //调用查询接口 查询数据
        JsonResult result = getProductSpecialList(inData);
        //获取返回码
        Integer code = result.getCode();
        if(!new Integer(0).equals(code)) {
            throw new BusinessException("提示：导出失败, 原因,查询数据异常");
        }

        //获取数据
        List<GaiaProductSpecialOutData> productSpecialList = (List<GaiaProductSpecialOutData>) result.getData();
        if(ValidateUtil.isEmpty(productSpecialList)) {
            throw new BusinessException("提示：导出数据为空");
        }
        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>(productSpecialList.size());
        for(GaiaProductSpecialOutData productSpecial : productSpecialList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //商品编码  药德通用编码
            lineList.add(productSpecial.getProCode());
            //商品描述
            lineList.add(productSpecial.getProDesc());
            //商品规格
            lineList.add(productSpecial.getProSpecs());
            //商品生产厂家
            lineList.add(productSpecial.getProFactoryName());
            //单位
            lineList.add(productSpecial.getProUnit());
            //成分分类
            lineList.add(productSpecial.getProCompClass());
            //成分分类描述
            lineList.add(productSpecial.getProCompClassName());
            //商品分类
            lineList.add(productSpecial.getProClass());
            //商品分类描述
            lineList.add(productSpecial.getProClassName());
            //省
            lineList.add(productSpecial.getProProvName());
            //市
            lineList.add(productSpecial.getProCityName());
            //类型
            lineList.add(productSpecial.getProTypeName());
            //标识
            lineList.add(productSpecial.getFlag());
            //状态
            lineList.add(productSpecial.getProStatus());
            //创建人
            lineList.add(productSpecial.getCreateUser());
            //创建日期
            lineList.add(productSpecial.getCreateDate());
            //最后一次修改人
            lineList.add(productSpecial.getLastUpdateUser());
            //最后一次修改日期
            lineList.add(productSpecial.getLastUpdateDate());
            dataList.add(lineList);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_EXPORT_EXCEL_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_EXPORT_SHEET_NAME);
                }});
        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.PRODUCT_SPECIAL_EXPORT_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }


    @Override
    @Transactional
    public JsonResult editProductSpecial(EditProductSpecialInData inData) {
        //获取参数
        String proCode = inData.getProCode();
        String province = inData.getProvince();
        String city = inData.getCity();
        String proType = inData.getProType();
        String oldProFlag = inData.getOldProFlag();
        String oldProStatus = inData.getOldProStatus();
        String newProFlag = inData.getNewProFlag();
        String newProStatus = inData.getNewProStatus();

        //校验参数
        if(ValidateUtil.isEmpty(proCode)) {
            throw new BusinessException("提示：药德商品编码不能为空");
        }
        if(ValidateUtil.isEmpty(province)) {
            throw new BusinessException("提示：修改前省不能为空");
        }
        if(ValidateUtil.isEmpty(city)) {
            throw new BusinessException("提示：修改前市不能为空");
        }
        if(ValidateUtil.isEmpty(proType)) {
            throw new BusinessException("提示：修改前类型不能为空");
        }
        if(ValidateUtil.isEmpty(oldProFlag)) {
            throw new BusinessException("提示：修改前标志不能为空");
        }
        if(ValidateUtil.isEmpty(oldProStatus)) {
            throw new BusinessException("提示：修改前状态不能为空");
        }
        if(ValidateUtil.isEmpty(newProFlag)) {
            throw new BusinessException("提示：修改后标志不能为空");
        }
        if(ValidateUtil.isEmpty(newProStatus)) {
            throw new BusinessException("提示：修改后状态不能为空");
        }

        //获取地区列表
        List<AreaOutData> areaList = productSpecialMapper.getAreaList();
        //将list转为map  key为地区名称 value为地区编号
        Map<String, String> areaNameMap = areaList.stream()
                .collect(Collectors.toMap(AreaOutData:: getAreaName, AreaOutData:: getAreaId));
        //处理参数 将省市中文进行转码
        String oldProvinceCode = areaNameMap.get(province);
        String oldCityCode = areaNameMap.get(city);
        //如果没匹配到对应的省市 提示报错
        if(ValidateUtil.isEmpty(oldProvinceCode) || ValidateUtil.isEmpty(oldCityCode)) {
            throw new BusinessException("提示：原省市不正确");
        }
        //处理字符串 将1-季节 处理为1  将2-夏 处理为2  将0-正常 处理为0 以便下面查询
        proType = proType.substring(0, 1);            //将类型截取第一位
        oldProFlag = oldProFlag.substring(0, 1);      //将标志截取第一位
        oldProStatus = oldProStatus.substring(0, 1);  //将状态截取第一位

        //组装参数查询
        GaiaProductSpecial productSpecial = new GaiaProductSpecial();
        productSpecial.setProCode(proCode);
        productSpecial.setProProv(oldProvinceCode);
        productSpecial.setProCity(oldCityCode);
        productSpecial.setProType(proType);
        productSpecial.setProFlag(oldProFlag);

        //校验商品是否存在
        GaiaProductSpecial productSpecialExists = productSpecialMapper.selectByPrimaryKey(productSpecial);
        if(ValidateUtil.isEmpty(productSpecialExists)) {
            throw new BusinessException("提示：此商品不存在, 请重新选择");
        }

        //校验商品修改后是否被占用
        GaiaProductSpecial productSpecialEmpty = new GaiaProductSpecial();
        BeanUtil.copyProperties(productSpecial, productSpecialEmpty);
        productSpecialEmpty.setProFlag(newProFlag);
        GaiaProductSpecial productSpecialFlag = productSpecialMapper.selectByPrimaryKey(productSpecialEmpty);
        if(ValidateUtil.isNotEmpty(productSpecialFlag)) {
            throw new BusinessException("提示：此记录已存在，无法修改");
        }

        //变更记录字段列表
        List<String> changeFieldList = new ArrayList<>();
        //校验标志 状态字段 原值与新值是否改变  如果有改变则修改
        if(!oldProFlag.equals(newProFlag)) {
            productSpecial.setChangeProFlag(newProFlag);
            changeFieldList.add("PRO_FLAG");
        }
        if(!oldProStatus.equals(newProStatus)) {
            productSpecial.setChangeProStatus(newProStatus);
            changeFieldList.add("PRO_STATUS");
        }
        productSpecial.setProLastUpdateUser(inData.getUpdateUser());
        productSpecial.setProLastUpdateDate(CommonUtil.getyyyyMMdd());
        productSpecial.setProLastUpdateTime(CommonUtil.getHHmmss());
        //修改商品
        productSpecialMapper.updateByPrimaryKeySelective(productSpecial);

        //新增变更记录
        if(ValidateUtil.isNotEmpty(changeFieldList)) {
            for(String changeField : changeFieldList) {
                GaiaProductSpecialChange productChange = new GaiaProductSpecialChange();
                productChange.setId(SnowflakeIdUtil.getNextId());
                productChange.setProCode(proCode);
                productChange.setProProv(oldProvinceCode);
                productChange.setProCity(oldCityCode);
                productChange.setProType(proType);
                productChange.setProChangeField(changeField);
                //如果变更的为PRO_FLAG字段 则获取PRO_FLAG的原值与新值 否则获取状态原值 新值
                if("PRO_FLAG".equals(changeField)) {
                    productChange.setProChangeFrom(oldProFlag);
                    productChange.setProChangeTo(newProFlag);
                } else {
                    productChange.setProChangeFrom(oldProStatus);
                    productChange.setProChangeTo(newProStatus);
                }
                productChange.setProChangeUser(inData.getUpdateUser());
                productChange.setProChangeDate(CommonUtil.getyyyyMMdd());
                productChange.setProChangeTime(CommonUtil.getHHmmss());
                productSpecialChangeMapper.insert(productChange);
            }
        }
        return JsonResult.success("", "success");
    }


    @Override
    public JsonResult editProductSpecialRecord(GaiaProductSpecialEditRecordInData inData) {
        String queryType = inData.getQueryType();
        if(ValidateUtil.isEmpty(queryType)) {
            throw new BusinessException("提示：查询类型不能为空");
        }
        List<GaiaProductSpecialRecordOutData> productSpecialRecordList = productSpecialMapper.getProductSpecialRecordList(inData);
        return JsonResult.success(productSpecialRecordList, "success");
    }


    @Override
    public Result exportEditProductSpecialRecord(GaiaProductSpecialEditRecordInData inData) {
        JsonResult result = editProductSpecialRecord(inData);
        Integer code = result.getCode();
        if(!new Integer(0).equals(code)) {
            throw new BusinessException("提示：导出失败, 原因,查询数据异常");
        }

        List<GaiaProductSpecialRecordOutData> productSpecialRecordList = (List<GaiaProductSpecialRecordOutData>) result.getData();
        if(ValidateUtil.isEmpty(productSpecialRecordList)) {
            throw new BusinessException("提示：导出数据为空");
        }
        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>(productSpecialRecordList.size());
        for(GaiaProductSpecialRecordOutData recordOutData : productSpecialRecordList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //药德通用编码
            lineList.add(recordOutData.getProCode());
            //商品描述
            lineList.add(recordOutData.getProDesc());
            //规格
            lineList.add(recordOutData.getProSpecs());
            //生产厂家
            lineList.add(recordOutData.getProFactoryName());
            //单位
            lineList.add(recordOutData.getProUnit());
            //成分分类
            lineList.add(recordOutData.getProCompClass());
            //成分分类描述
            lineList.add(recordOutData.getProCompClassName());
            //商品分类
            lineList.add(recordOutData.getProClass());
            //商品分类描述
            lineList.add(recordOutData.getProClassName());
            //类型
            lineList.add(recordOutData.getType());
            //省
            lineList.add(recordOutData.getProvince());
            //市
            lineList.add(recordOutData.getCity());
            //修改字段
            lineList.add(recordOutData.getChangeField());
            //修改前值
            lineList.add(recordOutData.getChangeFromValue());
            //修改后值
            lineList.add(recordOutData.getChangeToValue());
            //更新日期
            lineList.add(recordOutData.getChangeDate());
            //更新人
            lineList.add(recordOutData.getChangeUser());
            dataList.add(lineList);
        }

        //写入文件
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.EXPORT_EDIT_RECORD_EXCEL_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_EDIT_RECORD_SHEET_NAME);
                }});

        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.PRODUCT_SPECIAL_EDIT_RECORD_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }


    @Override
    public JsonResult addProductSpecialParam(ProductSpecialParamInData inData, GetLoginOutData userInfo) {
        //校验参数
        String prov = inData.getProProvCode();
        String city = inData.getProCityCode();
        List<ProductSpecialDateInData> dateList = inData.getDateList();
        if(ValidateUtil.isEmpty(prov)) {
            throw new BusinessException("提示：省不能为空");
        }
        if(ValidateUtil.isEmpty(city)) {
            throw new BusinessException("提示：市不能为空");
        }
        if(ValidateUtil.isEmpty(dateList) || 4 != dateList.size()) {
            throw new BusinessException("提示：该省市下季节日期必须全部选择");
        }

        List<GaiaProductSpecialParam> insertList = new ArrayList<>(4);
        //新增参数列表
        for(ProductSpecialDateInData dateParam : dateList) {
            GaiaProductSpecialParam param = new GaiaProductSpecialParam();
            param.setProProv(prov);
            param.setProCity(city);
            param.setProName(dateParam.getProSeason());
            GaiaProductSpecialParam existsFlag = productSpecialParamMapper.selectByPrimaryKey(prov, city, dateParam.getProSeason());
            if(ValidateUtil.isNotEmpty(existsFlag)) {
                throw new BusinessException("提示：该省市下已经存在季节，无法新增");
            }
            param.setProStartDate(dateParam.getStartDate());
            param.setProEndDate(dateParam.getEndDate());
            param.setProCreateUser(userInfo.getLoginName());
            param.setProCreateDate(CommonUtil.getyyyyMMdd());
            param.setProCreateTime(CommonUtil.getHHmmss());
            param.setLastUpdateUser(userInfo.getLoginName());
            param.setLastUpdateDate(CommonUtil.getyyyyMMdd());
            param.setLastUpdateTime(CommonUtil.getHHmmss());
            insertList.add(param);
        }
        //批量新增
        productSpecialParamMapper.batchInsert(insertList);
        return JsonResult.success("", "success");
    }


    @Override
    @Transactional
    public JsonResult editProductSpecialParam(ProductSpecialParamInData inData, GetLoginOutData userInfo) {
        //校验参数
        String prov = inData.getProProvCode();
        String city = inData.getProCityCode();
        List<ProductSpecialDateInData> dateList = inData.getDateList();
        if(ValidateUtil.isEmpty(prov)) {
            throw new BusinessException("提示：省不能为空");
        }
        if(ValidateUtil.isEmpty(city)) {
            throw new BusinessException("提示：市不能为空");
        }
        if(ValidateUtil.isEmpty(dateList) || 4 != dateList.size()) {
            throw new BusinessException("提示：该省市下季节日期必须全部选择");
        }

        //存储要更新的数据
        List<GaiaProductSpecialParam> updateList = new ArrayList<>(4);
        //存储要插入的变更记录数据
        List<GaiaProductSpecialParamChange> insertChangeList = new ArrayList<>(4);
        //查询是否已经存在 如果已经存在
        for(ProductSpecialDateInData dateParam : dateList) {
            String startDate = dateParam.getStartDate();
            String endDate = dateParam.getEndDate();
            GaiaProductSpecialParam param = new GaiaProductSpecialParam();
            param.setProProv(prov);
            param.setProCity(city);
            param.setProName(dateParam.getProSeason());
            param.setProStartDate(startDate);
            param.setProEndDate(endDate);
            param.setLastUpdateUser(userInfo.getLoginName());
            param.setLastUpdateDate(CommonUtil.getyyyyMMdd());
            param.setLastUpdateTime(CommonUtil.getHHmmss());
            //先使用 省 市 季节 起始日期 结束日期 查询 如果查询不到 则表示要更新数据
            GaiaProductSpecialParam productSpecialParamFlag = productSpecialParamMapper.selectByCondition(param);
            //查询日期是否有变更 如果有 则需要插入修改记录表
            GaiaProductSpecialParam oldSpecialParam = productSpecialParamMapper.selectByPrimaryKey(prov, city, dateParam.getProSeason());
            //如果不存在 则使用 省 市 季节 进行更新
            if(ValidateUtil.isEmpty(productSpecialParamFlag) && ValidateUtil.isNotEmpty(oldSpecialParam)) {
                //更新主表数据
                updateList.add(param);

                //组装修改记录表数据
                GaiaProductSpecialParamChange paramChange = new GaiaProductSpecialParamChange();
                paramChange.setProProv(prov);
                paramChange.setProCity(city);
                paramChange.setProName(dateParam.getProSeason());
                paramChange.setProChangeUser(userInfo.getLoginName());
                paramChange.setProChangeDate(CommonUtil.getyyyyMMdd());
                paramChange.setProChangeTime(CommonUtil.getHHmmss());
                //原起始日期
                String oldStartDate = oldSpecialParam.getProStartDate();
                //原结束日期
                String oldEndDate = oldSpecialParam.getProEndDate();
                //比较起始日期原值与新值是否相同
                if(!oldStartDate.equals(startDate)) {
                    GaiaProductSpecialParamChange paramChangeStartDate = new GaiaProductSpecialParamChange();
                    BeanUtil.copyProperties(paramChange, paramChangeStartDate);
                    paramChangeStartDate.setId(SnowflakeIdUtil.getNextId());
                    paramChangeStartDate.setProChangeField("PRO_START_DATE");
                    paramChangeStartDate.setProChangeFrom(oldStartDate);
                    paramChangeStartDate.setProChangeTo(startDate);
                    insertChangeList.add(paramChangeStartDate);
                }
                //比较结束日期原值与新值是否相同
                if(!oldEndDate.equals(endDate)) {
                    GaiaProductSpecialParamChange paramChangeEndDate = new GaiaProductSpecialParamChange();
                    BeanUtil.copyProperties(paramChange, paramChangeEndDate);
                    paramChangeEndDate.setId(SnowflakeIdUtil.getNextId());
                    paramChangeEndDate.setProChangeField("PRO_END_DATE");
                    paramChangeEndDate.setProChangeFrom(oldEndDate);
                    paramChangeEndDate.setProChangeTo(endDate);
                    insertChangeList.add(paramChangeEndDate);
                }
            }
        }
        //如果存在变更过的数据 则进行更新
        if(ValidateUtil.isNotEmpty(updateList)) {
            for(GaiaProductSpecialParam productSpecialParam : updateList) {
                productSpecialParamMapper.updateByPrimaryKeySelective(productSpecialParam);
            }
            //批量插入变更记录表
            if(ValidateUtil.isNotEmpty(insertChangeList)) {
                productSpecialParamChangeMapper.batchInsert(insertChangeList);
            }
        }
        return JsonResult.success("", "success");
    }



    @Override
    @Transactional
    public JsonResult importProductSpecialParam(MultipartFile file, GetLoginOutData userInfo) {
        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaProductSpecialParam.class);
        //获取到 GaiaProductSpecialParam实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            log.error("上传特殊商品,读取excel失败: {}",e.getMessage(), e);
            throw new BusinessException("提示：读取Excel失败");
        }

        if(ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        //转换list泛型
        List<GaiaProductSpecialParam> productSpecialParamList = new ArrayList<>(excelDataList.size());
        for(Object obj : excelDataList) {
            GaiaProductSpecialParam temp = (GaiaProductSpecialParam) obj;
            productSpecialParamList.add(temp);
        }
        //校验表头正确性
        String[] excelHead = CommonConstant.PRODUCT_SPECIAL_PARAM_IMPORT_HEAD;
        GaiaProductSpecialParam productSpecialParam = productSpecialParamList.get(0);
        if(!excelHead[0].equals(productSpecialParam.getIndex()) || !excelHead[1].equals(productSpecialParam.getProProv())
                || !excelHead[2].equals(productSpecialParam.getProCity()) || !excelHead[3].equals(productSpecialParam.getProName())
                || !excelHead[4].equals(productSpecialParam.getProStartDate()) || !excelHead[5].equals(productSpecialParam.getProEndDate())
                || !excelHead[6].equals(productSpecialParam.getGroup())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //从第二位元素截取到最后一位元素
        List<GaiaProductSpecialParam> paramDataList = productSpecialParamList.subList(1, productSpecialParamList.size());
        if(ValidateUtil.isEmpty(paramDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        Set<String> errorGroupSet = new HashSet<>(10);
        //地区列表
        List<AreaOutData> areaList = productSpecialMapper.getAreaList();
        //将list转为map  key为地区名称 value为地区编号
        Map<String, String> areaNameMap = areaList.stream().collect(Collectors.toMap(AreaOutData:: getAreaName, AreaOutData:: getAreaId));
        //将list转为map  key为分组编号， value为对应的list
        Map<String, List<GaiaProductSpecialParam>> groupMap = paramDataList.stream().collect(Collectors.groupingBy(item -> item.getGroup()));
        //查询省市关系并转为map  key为市编号 value为省编号
        List<ProvCityRelationshipOutData> provCityRelationshipList = productSpecialMapper.getProvCityRelationship();
        Map<String, String> provCityCodeMap = provCityRelationshipList.stream()
                .collect(Collectors.toMap(ProvCityRelationshipOutData :: getCityCode, ProvCityRelationshipOutData :: getProvCode));
        //校验错误数据
        for(GaiaProductSpecialParam paramData : paramDataList) {
            String index = paramData.getIndex();
            String proProv = paramData.getProProv();
            String proCity = paramData.getProCity();
            String proName = paramData.getProName();
            String proStartDate = paramData.getProStartDate();
            String proEndDate = paramData.getProEndDate();
            String group = paramData.getGroup();
            //校验每行数据是否缺少
            if(ValidateUtil.isEmpty(index) || ValidateUtil.isEmpty(proProv) || ValidateUtil.isEmpty(proCity)
                    || ValidateUtil.isEmpty(proName) || ValidateUtil.isEmpty(proStartDate) || ValidateUtil.isEmpty(proEndDate)
                    || ValidateUtil.isEmpty(group)) {
                throw new BusinessException("提示：序号 ,省, 市, 季节, 季节开始日期, 季节结束日期, 分组不能为空!");
            }
            //校验省市填写是否正确
            String provCode = areaNameMap.get(proProv);
            String cityCode = areaNameMap.get(proCity);
            if(ValidateUtil.isEmpty(provCode) || ValidateUtil.isEmpty(cityCode)) {
                errorGroupSet.add(group);
                continue;
            }
            //校验省市关系是否正确
            String correctProvCode = provCityCodeMap.get(cityCode);
            if(!provCode.equals(correctProvCode)) {
                errorGroupSet.add(group);
                continue;
            }
            //校验日期格式是否正确
            String startDate = DateUtil.formatDateStr(proStartDate, "MMdd");
            String endDate = DateUtil.formatDateStr(proEndDate, "MMdd");
            if(ValidateUtil.isEmpty(startDate) || ValidateUtil.isEmpty(endDate)) {
                errorGroupSet.add(group);
                continue;
            }
        }

        //校验每组是否有四条数据 并且季节是否为1234
        for(String group : groupMap.keySet()) {
            List<GaiaProductSpecialParam> groupList = groupMap.get(group);
            //排序转为list 然后将index进行从小到大排序  如果为1234 表示数据正确 否则数据错误
            List<GaiaProductSpecialParam> sortGroupList = groupList.stream()
                    .sorted(Comparator.comparing(GaiaProductSpecialParam::getProName)).collect(Collectors.toList());
            String sortIndexStr = sortGroupList.stream().map(item -> item.getProName()).collect(Collectors.joining(""));
            if(groupList.size() != 4 || !"1234".equals(sortIndexStr)) {
                errorGroupSet.add(group);
                continue;
            }

            //去重省
            List<GaiaProductSpecialParam> provDistinctList = groupList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(
                                    () -> new TreeSet<>(Comparator.comparing(item -> item.getProProv()))
                            ),
                            ArrayList::new
                    )
            );
            //判断去重后 分组中的省是否相同  如果不相同 则无法导入  去重后如果为1条 则表示相同
            if(1 != provDistinctList.size()) {
                errorGroupSet.add(group);
                continue;
            }

            //去重市
            List<GaiaProductSpecialParam> cityDistinctList = groupList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(
                                    () -> new TreeSet<>(Comparator.comparing(item -> item.getProCity()))
                            ),
                            ArrayList::new
                    )
            );
            //判断去重后 分组中的市是否相同  如果不相同 则无法导入  去重后如果为1条 则表示相同
            if(1 != cityDistinctList.size()) {
                errorGroupSet.add(group);
                continue;
            }
        }

        List<GaiaProductSpecialParam> batchReplaceInertList = new ArrayList<>();
        List<GaiaProductSpecialParamChange> batchInertChangeList = new ArrayList<>();
        for(GaiaProductSpecialParam data : paramDataList) {
            String group = data.getGroup();
            if(!errorGroupSet.contains(group)) {
                String provCode = areaNameMap.get(data.getProProv());
                String cityCode = areaNameMap.get(data.getProCity());
                String proName = data.getProName();
                String startDate = DateUtil.formatDateStr(data.getProStartDate(), "MMdd");
                String endDate = DateUtil.formatDateStr(data.getProEndDate(), "MMdd");
                String loginName = userInfo.getLoginName();
                data.setProProv(provCode);
                data.setProCity(cityCode);
                data.setProStartDate(startDate);
                data.setProEndDate(endDate);
                data.setProCreateUser(loginName);
                data.setProCreateDate(CommonUtil.getyyyyMMdd());
                data.setProCreateTime(CommonUtil.getHHmmss());
                data.setLastUpdateUser(loginName);
                data.setLastUpdateDate(CommonUtil.getyyyyMMdd());
                data.setLastUpdateTime(CommonUtil.getHHmmss());
                //没查询到则插入数据 查询到了则覆盖数据，并且记录修改记录
                GaiaProductSpecialParam existsFlag = productSpecialParamMapper.selectByPrimaryKey(provCode, cityCode, proName);
                if(ValidateUtil.isNotEmpty(existsFlag)) {
                    //如果查询结果不为空 则覆盖数据 并且记录到变更表
                    GaiaProductSpecialParamChange paramChange = new GaiaProductSpecialParamChange();
                    paramChange.setProProv(provCode);
                    paramChange.setProCity(cityCode);
                    paramChange.setProName(proName);
                    paramChange.setProChangeUser(loginName);
                    paramChange.setProChangeDate(CommonUtil.getyyyyMMdd());
                    paramChange.setProChangeTime(CommonUtil.getHHmmss());
                    //如果起始日期不同 则记录到表中
                    String oldStartDate = existsFlag.getProStartDate();
                    if(!oldStartDate.equals(startDate)) {
                        GaiaProductSpecialParamChange startDateParamChange = new GaiaProductSpecialParamChange();
                        BeanUtil.copyProperties(paramChange, startDateParamChange);
                        startDateParamChange.setId(SnowflakeIdUtil.getNextId());
                        startDateParamChange.setProChangeField("PRO_START_DATE");
                        startDateParamChange.setProChangeFrom(oldStartDate);
                        startDateParamChange.setProChangeTo(data.getProStartDate());
                        batchInertChangeList.add(startDateParamChange);
                    }
                    //如果结束日期不同 则记录到表中
                    String oldEndDate = existsFlag.getProEndDate();
                    if(!oldEndDate.equals(endDate)) {
                        GaiaProductSpecialParamChange endDateParamChange = new GaiaProductSpecialParamChange();
                        BeanUtil.copyProperties(paramChange, endDateParamChange);
                        endDateParamChange.setId(SnowflakeIdUtil.getNextId());
                        endDateParamChange.setProChangeField("PRO_END_DATE");
                        endDateParamChange.setProChangeFrom(oldEndDate);
                        endDateParamChange.setProChangeTo(data.getProEndDate());
                        batchInertChangeList.add(endDateParamChange);
                    }
                }
                batchReplaceInertList.add(data);
            }
        }
        //批量新增特殊商品参数
        if(ValidateUtil.isNotEmpty(batchReplaceInertList)) {
            productSpecialParamMapper.batchReplaceInsert(batchReplaceInertList);
        }
        //批量新增特殊商品参数修改记录
        if(ValidateUtil.isNotEmpty(batchInertChangeList)) {
            productSpecialParamChangeMapper.batchInsert(batchInertChangeList);
        }
        if(ValidateUtil.isNotEmpty(errorGroupSet)) {
            StringBuilder errorMsg = new StringBuilder("");
            for(String errorIndex : errorGroupSet) {
                errorMsg.append(errorIndex).append(",");
            }
            String errorMsgStr = errorMsg.toString();
            return JsonResult.error(2001, "提示：以下分组数据导入失败，请检查分组数据：" + errorMsgStr.substring(0, errorMsgStr.length() - 1));
        }
        return JsonResult.success("", "success");
    }



    @Override
    public JsonResult getProductSpecialParamList(GaiaProductSpecialParam inData) {
        List<GaiaProductSpecialParam> productSpecialParamList = productSpecialParamMapper.getProductSpecialParamList(inData);
        return JsonResult.success(productSpecialParamList, "success");
    }


    @Override
    public Result exportProductSpecialParam(GaiaProductSpecialParam inData) {
        //调用查询接口 查询数据 然后导出
        JsonResult result = getProductSpecialParamList(inData);
        Integer code = result.getCode();
        if(!new Integer(0).equals(code)) {
            throw new BusinessException("提示：导出失败, 原因,查询数据异常");
        }

        //导出数据
        List<GaiaProductSpecialParam> productSpecialParamList = (List<GaiaProductSpecialParam>) result.getData();
        if(ValidateUtil.isEmpty(productSpecialParamList)) {
            throw new BusinessException("提示：导出数据为空");
        }

        List<List<Object>> dataList = new ArrayList<>(productSpecialParamList.size());
        for(GaiaProductSpecialParam param : productSpecialParamList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //省
            lineList.add(param.getProProv());
            //市
            lineList.add(param.getProCity());
            //季节
            lineList.add(param.getProName());
            //季节开始日期
            lineList.add(param.getProStartDate());
            //季节结束日期
            lineList.add(param.getProEndDate());
            //更新日期
            lineList.add(param.getLastUpdateDate());
            //更新人
            lineList.add(param.getLastUpdateUser());
            dataList.add(lineList);
        }

        //写入文件
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_PARAM_EXPORT_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_PARAM_SHEET_NAME);
                }});

        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.PRODUCT_SPECIAL_PARAM_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }


    @Override
    public JsonResult getParamEditRecord(GaiaProductSpecialParam inData) {
        List<GaiaProductSpecialParamChange> paramEditRecord = productSpecialParamChangeMapper.getParamEditRecord(inData);
        return JsonResult.success(paramEditRecord, "success");
    }


    @Override
    public Result exportParamEditRecord(GaiaProductSpecialParam inData) {
        JsonResult result = getParamEditRecord(inData);
        Integer code = result.getCode();
        if(!new Integer(0).equals(code)) {
            throw new BusinessException("提示：导出失败, 原因,查询数据异常");
        }

        //导出数据
        List<GaiaProductSpecialParamChange> productSpecialParamChangeList = (List<GaiaProductSpecialParamChange>) result.getData();
        if(ValidateUtil.isEmpty(productSpecialParamChangeList)) {
            throw new BusinessException("提示：导出数据为空");
        }
        List<List<Object>> dataList = new ArrayList<>(productSpecialParamChangeList.size());
        for(GaiaProductSpecialParamChange paramChange : productSpecialParamChangeList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //省
            lineList.add(paramChange.getProProv());
            //市
            lineList.add(paramChange.getProCity());
            //季节
            lineList.add(paramChange.getProName());
            //修改字段
            lineList.add(paramChange.getProChangeField());
            //修改前值
            lineList.add(paramChange.getProChangeFrom());
            //修改后值
            lineList.add(paramChange.getProChangeTo());
            //更新日期
            lineList.add(paramChange.getProChangeDate());
            //更新人
            lineList.add(paramChange.getProChangeUser());
            dataList.add(lineList);
        }
        //写入文件
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_PARAM_EDIT_RECORD_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.PRODUCT_SPECIAL_PARAM_EDIT_RECORD_NAME);
                }});

        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.PRODUCT_SPECIAL_PARAM_EDIT_RECORD_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }

    @Override
    public List<SpecialClassifcationOutData> specialDrugsInquiry(SpecialClassifcationInData inData) {
        List<SpecialClassifcationOutData> outData = this.productSpecialMapper.getclassifcation(inData);
        if (CollUtil.isNotEmpty(outData)){
            IntStream.range(0, outData.size()).forEach(i -> ((SpecialClassifcationOutData) outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }




    @Override
    public Result exportDrugsDetail(SpecialClassifcationInData inData) {
        List<SpecialClassifcationOutData> outList = this.productSpecialMapper.getclassifcation(inData);
        String fileName = "特殊分类药品销售表导出";
        if (CollectionUtil.isNotEmpty(outList)) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(outList,fileName, Collections.singletonList((short)1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据！");
        }
    }


}
