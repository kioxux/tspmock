package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetExamineDetailOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "验收单号")
    private String gsedVoucherId;
    @ApiModelProperty(value = "验收日期")
    private String gsedDate;
    @ApiModelProperty(value = "行号")
    private String gsedSerial;
    @ApiModelProperty(value = "编码")
    private String gsedProId;
    @ApiModelProperty(value = "批号")
    private String gsedBatchNo;
    @ApiModelProperty(value = "批次")
    private String gsedBatch;
    @ApiModelProperty(value = "有效期")
    private String gsedValidDate;
    @ApiModelProperty(value = "生产日期")
    private String gsedMadeDate;
    @ApiModelProperty(value = "验收数量")
    private BigDecimal gsedRecipientQty;
    @ApiModelProperty(value = "合格数量")
    private BigDecimal gsedQualifiedQty;
    @ApiModelProperty(value = "不合格数量")
    private BigDecimal gsedUnqualifiedQty;
    @ApiModelProperty(value = "不合格事项")
    private String gsedUnqualifiedCause;
    @ApiModelProperty(value = "验收人员")
    private String gsedEmp;
    @ApiModelProperty(value = "验收结论")
    private String gsedResult;
    @ApiModelProperty(value = "库存状态")
    private String gsedStockStatus;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;
    @ApiModelProperty(value = "单价")
    private String proPrice;
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "收货行金额")
    private String gsedAcceptPrice;
    @ApiModelProperty(value = "收货人")
    private String gsedAcceptEmp;
    @ApiModelProperty(value = "收货单价")
    private String gsedPoPrice;
    @ApiModelProperty(value = "国家贯标编码")
    private String medProductCode;
    @ApiModelProperty(value = "是否含麻黄碱 0-不含 1-含")
    private String isContainEphedrine;
}
