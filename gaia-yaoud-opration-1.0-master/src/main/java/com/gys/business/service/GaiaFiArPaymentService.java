package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaFiArPayment;

import java.util.List;

/**
 * 应收方式维护表(GaiaFiArPayment)表服务接口
 *
 * @author makejava
 * @since 2021-09-18 15:29:32
 */
public interface GaiaFiArPaymentService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaFiArPayment queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaFiArPayment> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gaiaFiArPayment 实例对象
     * @return 实例对象
     */
    GaiaFiArPayment insert(GaiaFiArPayment gaiaFiArPayment);

    /**
     * 修改数据
     *
     * @param gaiaFiArPayment 实例对象
     * @return 实例对象
     */
    GaiaFiArPayment update(GaiaFiArPayment gaiaFiArPayment);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
