package com.gys.business.service.data.DelegationReturn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "委托退货查询")
public class DelegationReturnInData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店")
    private String gsrdhBrId;
    @ApiModelProperty(value = "退库单号")
    private String[] voucherIds;
    @ApiModelProperty(value = "开始日期")
    private String startDate;
    @ApiModelProperty(value = "截至日期")
    private String endDate;
    @ApiModelProperty(value = "商品编码")
    private String[] proIds;
    @ApiModelProperty(value = "连锁总部")
    private String stoChainHead;
    @ApiModelProperty(value = "连锁门店")
    private String stoCodes;
}
