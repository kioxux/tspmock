//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class RechargeCardPaycheckInData {
    private String clientId;
    private String gsrcpVoucherId;
    private String gsrcpCardId;
    private String gsrcpBrId;
    private String gsrcpDate;
    private String gsrcpTime;
    private String gsrcpEmp;
    private BigDecimal gsrcpBeforeAmt;
    private BigDecimal gsrcpPayAmt;
    private String gsrcpProportion;
    private String gsrcpPromotion;
    private BigDecimal gsrcpAfterAmt;
    private BigDecimal gsrcpRechargeAmt;
    private BigDecimal gsrcpYsAmt;
    private BigDecimal gsrcpRmbZlAmt;
    private BigDecimal gsrcpRmbAmt;
    private String gsrcpPaymentNo;
    private BigDecimal gsrcpPaymentAmt;
    private Boolean isCheck;

    public RechargeCardPaycheckInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrcpVoucherId() {
        return this.gsrcpVoucherId;
    }

    public String getGsrcpCardId() {
        return this.gsrcpCardId;
    }

    public String getGsrcpBrId() {
        return this.gsrcpBrId;
    }

    public String getGsrcpDate() {
        return this.gsrcpDate;
    }

    public String getGsrcpTime() {
        return this.gsrcpTime;
    }

    public String getGsrcpEmp() {
        return this.gsrcpEmp;
    }

    public BigDecimal getGsrcpBeforeAmt() {
        return this.gsrcpBeforeAmt;
    }

    public BigDecimal getGsrcpPayAmt() {
        return this.gsrcpPayAmt;
    }

    public String getGsrcpProportion() {
        return this.gsrcpProportion;
    }

    public String getGsrcpPromotion() {
        return this.gsrcpPromotion;
    }

    public BigDecimal getGsrcpAfterAmt() {
        return this.gsrcpAfterAmt;
    }

    public BigDecimal getGsrcpRechargeAmt() {
        return this.gsrcpRechargeAmt;
    }

    public BigDecimal getGsrcpYsAmt() {
        return this.gsrcpYsAmt;
    }

    public BigDecimal getGsrcpRmbZlAmt() {
        return this.gsrcpRmbZlAmt;
    }

    public BigDecimal getGsrcpRmbAmt() {
        return this.gsrcpRmbAmt;
    }

    public String getGsrcpPaymentNo() {
        return this.gsrcpPaymentNo;
    }

    public BigDecimal getGsrcpPaymentAmt() {
        return this.gsrcpPaymentAmt;
    }

    public Boolean getIsCheck() {
        return this.isCheck;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrcpVoucherId(final String gsrcpVoucherId) {
        this.gsrcpVoucherId = gsrcpVoucherId;
    }

    public void setGsrcpCardId(final String gsrcpCardId) {
        this.gsrcpCardId = gsrcpCardId;
    }

    public void setGsrcpBrId(final String gsrcpBrId) {
        this.gsrcpBrId = gsrcpBrId;
    }

    public void setGsrcpDate(final String gsrcpDate) {
        this.gsrcpDate = gsrcpDate;
    }

    public void setGsrcpTime(final String gsrcpTime) {
        this.gsrcpTime = gsrcpTime;
    }

    public void setGsrcpEmp(final String gsrcpEmp) {
        this.gsrcpEmp = gsrcpEmp;
    }

    public void setGsrcpBeforeAmt(final BigDecimal gsrcpBeforeAmt) {
        this.gsrcpBeforeAmt = gsrcpBeforeAmt;
    }

    public void setGsrcpPayAmt(final BigDecimal gsrcpPayAmt) {
        this.gsrcpPayAmt = gsrcpPayAmt;
    }

    public void setGsrcpProportion(final String gsrcpProportion) {
        this.gsrcpProportion = gsrcpProportion;
    }

    public void setGsrcpPromotion(final String gsrcpPromotion) {
        this.gsrcpPromotion = gsrcpPromotion;
    }

    public void setGsrcpAfterAmt(final BigDecimal gsrcpAfterAmt) {
        this.gsrcpAfterAmt = gsrcpAfterAmt;
    }

    public void setGsrcpRechargeAmt(final BigDecimal gsrcpRechargeAmt) {
        this.gsrcpRechargeAmt = gsrcpRechargeAmt;
    }

    public void setGsrcpYsAmt(final BigDecimal gsrcpYsAmt) {
        this.gsrcpYsAmt = gsrcpYsAmt;
    }

    public void setGsrcpRmbZlAmt(final BigDecimal gsrcpRmbZlAmt) {
        this.gsrcpRmbZlAmt = gsrcpRmbZlAmt;
    }

    public void setGsrcpRmbAmt(final BigDecimal gsrcpRmbAmt) {
        this.gsrcpRmbAmt = gsrcpRmbAmt;
    }

    public void setGsrcpPaymentNo(final String gsrcpPaymentNo) {
        this.gsrcpPaymentNo = gsrcpPaymentNo;
    }

    public void setGsrcpPaymentAmt(final BigDecimal gsrcpPaymentAmt) {
        this.gsrcpPaymentAmt = gsrcpPaymentAmt;
    }

    public void setIsCheck(final Boolean isCheck) {
        this.isCheck = isCheck;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RechargeCardPaycheckInData)) {
            return false;
        } else {
            RechargeCardPaycheckInData other = (RechargeCardPaycheckInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label239: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label239;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label239;
                    }

                    return false;
                }

                Object this$gsrcpVoucherId = this.getGsrcpVoucherId();
                Object other$gsrcpVoucherId = other.getGsrcpVoucherId();
                if (this$gsrcpVoucherId == null) {
                    if (other$gsrcpVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsrcpVoucherId.equals(other$gsrcpVoucherId)) {
                    return false;
                }

                Object this$gsrcpCardId = this.getGsrcpCardId();
                Object other$gsrcpCardId = other.getGsrcpCardId();
                if (this$gsrcpCardId == null) {
                    if (other$gsrcpCardId != null) {
                        return false;
                    }
                } else if (!this$gsrcpCardId.equals(other$gsrcpCardId)) {
                    return false;
                }

                label218: {
                    Object this$gsrcpBrId = this.getGsrcpBrId();
                    Object other$gsrcpBrId = other.getGsrcpBrId();
                    if (this$gsrcpBrId == null) {
                        if (other$gsrcpBrId == null) {
                            break label218;
                        }
                    } else if (this$gsrcpBrId.equals(other$gsrcpBrId)) {
                        break label218;
                    }

                    return false;
                }

                label211: {
                    Object this$gsrcpDate = this.getGsrcpDate();
                    Object other$gsrcpDate = other.getGsrcpDate();
                    if (this$gsrcpDate == null) {
                        if (other$gsrcpDate == null) {
                            break label211;
                        }
                    } else if (this$gsrcpDate.equals(other$gsrcpDate)) {
                        break label211;
                    }

                    return false;
                }

                Object this$gsrcpTime = this.getGsrcpTime();
                Object other$gsrcpTime = other.getGsrcpTime();
                if (this$gsrcpTime == null) {
                    if (other$gsrcpTime != null) {
                        return false;
                    }
                } else if (!this$gsrcpTime.equals(other$gsrcpTime)) {
                    return false;
                }

                Object this$gsrcpEmp = this.getGsrcpEmp();
                Object other$gsrcpEmp = other.getGsrcpEmp();
                if (this$gsrcpEmp == null) {
                    if (other$gsrcpEmp != null) {
                        return false;
                    }
                } else if (!this$gsrcpEmp.equals(other$gsrcpEmp)) {
                    return false;
                }

                label190: {
                    Object this$gsrcpBeforeAmt = this.getGsrcpBeforeAmt();
                    Object other$gsrcpBeforeAmt = other.getGsrcpBeforeAmt();
                    if (this$gsrcpBeforeAmt == null) {
                        if (other$gsrcpBeforeAmt == null) {
                            break label190;
                        }
                    } else if (this$gsrcpBeforeAmt.equals(other$gsrcpBeforeAmt)) {
                        break label190;
                    }

                    return false;
                }

                label183: {
                    Object this$gsrcpPayAmt = this.getGsrcpPayAmt();
                    Object other$gsrcpPayAmt = other.getGsrcpPayAmt();
                    if (this$gsrcpPayAmt == null) {
                        if (other$gsrcpPayAmt == null) {
                            break label183;
                        }
                    } else if (this$gsrcpPayAmt.equals(other$gsrcpPayAmt)) {
                        break label183;
                    }

                    return false;
                }

                Object this$gsrcpProportion = this.getGsrcpProportion();
                Object other$gsrcpProportion = other.getGsrcpProportion();
                if (this$gsrcpProportion == null) {
                    if (other$gsrcpProportion != null) {
                        return false;
                    }
                } else if (!this$gsrcpProportion.equals(other$gsrcpProportion)) {
                    return false;
                }

                label169: {
                    Object this$gsrcpPromotion = this.getGsrcpPromotion();
                    Object other$gsrcpPromotion = other.getGsrcpPromotion();
                    if (this$gsrcpPromotion == null) {
                        if (other$gsrcpPromotion == null) {
                            break label169;
                        }
                    } else if (this$gsrcpPromotion.equals(other$gsrcpPromotion)) {
                        break label169;
                    }

                    return false;
                }

                Object this$gsrcpAfterAmt = this.getGsrcpAfterAmt();
                Object other$gsrcpAfterAmt = other.getGsrcpAfterAmt();
                if (this$gsrcpAfterAmt == null) {
                    if (other$gsrcpAfterAmt != null) {
                        return false;
                    }
                } else if (!this$gsrcpAfterAmt.equals(other$gsrcpAfterAmt)) {
                    return false;
                }

                label155: {
                    Object this$gsrcpRechargeAmt = this.getGsrcpRechargeAmt();
                    Object other$gsrcpRechargeAmt = other.getGsrcpRechargeAmt();
                    if (this$gsrcpRechargeAmt == null) {
                        if (other$gsrcpRechargeAmt == null) {
                            break label155;
                        }
                    } else if (this$gsrcpRechargeAmt.equals(other$gsrcpRechargeAmt)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gsrcpYsAmt = this.getGsrcpYsAmt();
                Object other$gsrcpYsAmt = other.getGsrcpYsAmt();
                if (this$gsrcpYsAmt == null) {
                    if (other$gsrcpYsAmt != null) {
                        return false;
                    }
                } else if (!this$gsrcpYsAmt.equals(other$gsrcpYsAmt)) {
                    return false;
                }

                Object this$gsrcpRmbZlAmt = this.getGsrcpRmbZlAmt();
                Object other$gsrcpRmbZlAmt = other.getGsrcpRmbZlAmt();
                if (this$gsrcpRmbZlAmt == null) {
                    if (other$gsrcpRmbZlAmt != null) {
                        return false;
                    }
                } else if (!this$gsrcpRmbZlAmt.equals(other$gsrcpRmbZlAmt)) {
                    return false;
                }

                label134: {
                    Object this$gsrcpRmbAmt = this.getGsrcpRmbAmt();
                    Object other$gsrcpRmbAmt = other.getGsrcpRmbAmt();
                    if (this$gsrcpRmbAmt == null) {
                        if (other$gsrcpRmbAmt == null) {
                            break label134;
                        }
                    } else if (this$gsrcpRmbAmt.equals(other$gsrcpRmbAmt)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gsrcpPaymentNo = this.getGsrcpPaymentNo();
                    Object other$gsrcpPaymentNo = other.getGsrcpPaymentNo();
                    if (this$gsrcpPaymentNo == null) {
                        if (other$gsrcpPaymentNo == null) {
                            break label127;
                        }
                    } else if (this$gsrcpPaymentNo.equals(other$gsrcpPaymentNo)) {
                        break label127;
                    }

                    return false;
                }

                Object this$gsrcpPaymentAmt = this.getGsrcpPaymentAmt();
                Object other$gsrcpPaymentAmt = other.getGsrcpPaymentAmt();
                if (this$gsrcpPaymentAmt == null) {
                    if (other$gsrcpPaymentAmt != null) {
                        return false;
                    }
                } else if (!this$gsrcpPaymentAmt.equals(other$gsrcpPaymentAmt)) {
                    return false;
                }

                Object this$isCheck = this.getIsCheck();
                Object other$isCheck = other.getIsCheck();
                if (this$isCheck == null) {
                    if (other$isCheck != null) {
                        return false;
                    }
                } else if (!this$isCheck.equals(other$isCheck)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RechargeCardPaycheckInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsrcpVoucherId = this.getGsrcpVoucherId();
        result = result * 59 + ($gsrcpVoucherId == null ? 43 : $gsrcpVoucherId.hashCode());
        Object $gsrcpCardId = this.getGsrcpCardId();
        result = result * 59 + ($gsrcpCardId == null ? 43 : $gsrcpCardId.hashCode());
        Object $gsrcpBrId = this.getGsrcpBrId();
        result = result * 59 + ($gsrcpBrId == null ? 43 : $gsrcpBrId.hashCode());
        Object $gsrcpDate = this.getGsrcpDate();
        result = result * 59 + ($gsrcpDate == null ? 43 : $gsrcpDate.hashCode());
        Object $gsrcpTime = this.getGsrcpTime();
        result = result * 59 + ($gsrcpTime == null ? 43 : $gsrcpTime.hashCode());
        Object $gsrcpEmp = this.getGsrcpEmp();
        result = result * 59 + ($gsrcpEmp == null ? 43 : $gsrcpEmp.hashCode());
        Object $gsrcpBeforeAmt = this.getGsrcpBeforeAmt();
        result = result * 59 + ($gsrcpBeforeAmt == null ? 43 : $gsrcpBeforeAmt.hashCode());
        Object $gsrcpPayAmt = this.getGsrcpPayAmt();
        result = result * 59 + ($gsrcpPayAmt == null ? 43 : $gsrcpPayAmt.hashCode());
        Object $gsrcpProportion = this.getGsrcpProportion();
        result = result * 59 + ($gsrcpProportion == null ? 43 : $gsrcpProportion.hashCode());
        Object $gsrcpPromotion = this.getGsrcpPromotion();
        result = result * 59 + ($gsrcpPromotion == null ? 43 : $gsrcpPromotion.hashCode());
        Object $gsrcpAfterAmt = this.getGsrcpAfterAmt();
        result = result * 59 + ($gsrcpAfterAmt == null ? 43 : $gsrcpAfterAmt.hashCode());
        Object $gsrcpRechargeAmt = this.getGsrcpRechargeAmt();
        result = result * 59 + ($gsrcpRechargeAmt == null ? 43 : $gsrcpRechargeAmt.hashCode());
        Object $gsrcpYsAmt = this.getGsrcpYsAmt();
        result = result * 59 + ($gsrcpYsAmt == null ? 43 : $gsrcpYsAmt.hashCode());
        Object $gsrcpRmbZlAmt = this.getGsrcpRmbZlAmt();
        result = result * 59 + ($gsrcpRmbZlAmt == null ? 43 : $gsrcpRmbZlAmt.hashCode());
        Object $gsrcpRmbAmt = this.getGsrcpRmbAmt();
        result = result * 59 + ($gsrcpRmbAmt == null ? 43 : $gsrcpRmbAmt.hashCode());
        Object $gsrcpPaymentNo = this.getGsrcpPaymentNo();
        result = result * 59 + ($gsrcpPaymentNo == null ? 43 : $gsrcpPaymentNo.hashCode());
        Object $gsrcpPaymentAmt = this.getGsrcpPaymentAmt();
        result = result * 59 + ($gsrcpPaymentAmt == null ? 43 : $gsrcpPaymentAmt.hashCode());
        Object $isCheck = this.getIsCheck();
        result = result * 59 + ($isCheck == null ? 43 : $isCheck.hashCode());
        return result;
    }

    public String toString() {
        return "RechargeCardPaycheckInData(clientId=" + this.getClientId() + ", gsrcpVoucherId=" + this.getGsrcpVoucherId() + ", gsrcpCardId=" + this.getGsrcpCardId() + ", gsrcpBrId=" + this.getGsrcpBrId() + ", gsrcpDate=" + this.getGsrcpDate() + ", gsrcpTime=" + this.getGsrcpTime() + ", gsrcpEmp=" + this.getGsrcpEmp() + ", gsrcpBeforeAmt=" + this.getGsrcpBeforeAmt() + ", gsrcpPayAmt=" + this.getGsrcpPayAmt() + ", gsrcpProportion=" + this.getGsrcpProportion() + ", gsrcpPromotion=" + this.getGsrcpPromotion() + ", gsrcpAfterAmt=" + this.getGsrcpAfterAmt() + ", gsrcpRechargeAmt=" + this.getGsrcpRechargeAmt() + ", gsrcpYsAmt=" + this.getGsrcpYsAmt() + ", gsrcpRmbZlAmt=" + this.getGsrcpRmbZlAmt() + ", gsrcpRmbAmt=" + this.getGsrcpRmbAmt() + ", gsrcpPaymentNo=" + this.getGsrcpPaymentNo() + ", gsrcpPaymentAmt=" + this.getGsrcpPaymentAmt() + ", isCheck=" + this.getIsCheck() + ")";
    }
}
